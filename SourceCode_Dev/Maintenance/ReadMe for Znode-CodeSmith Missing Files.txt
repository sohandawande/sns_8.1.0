Please use the zip file "Znode-CodeSmith Missing Files.zip" for replacing the NetTiers code after generation of new CodeSmith DLLs.

As the new CodeSmith DLLs will cause problem in the Znode framework because of some missing properties in the code.

Please compare and paste the changes from these files to the CodeSmith generated files.