/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Znode_Transactions
	DROP CONSTRAINT FK_Znode_Transactions_ZNodePaymentSetting
GO
ALTER TABLE dbo.ZNodePaymentSetting SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_Znode_Transactions
	(
	GUID uniqueidentifier NOT NULL,
	CustomerProfileId nvarchar(MAX) NULL,
	CustomerPaymentId nvarchar(MAX) NULL,
	TransactionId nvarchar(MAX) NULL,
	TransactionDate datetime NULL,
	CaptureTransactionDate datetime NULL,
	RefundTransactionDate datetime NULL,
	ResponseText nvarchar(MAX) NULL,
	ResponseCode nvarchar(50) NULL,
	Custom1 nvarchar(MAX) NULL,
	Custom2 nvarchar(MAX) NULL,
	Custom3 nvarchar(MAX) NULL,
	Amount money NULL,
	PaymentSettingId int NULL,
	CurrencyCode nvarchar(50) NULL,
	SubscriptionId nvarchar(MAX) NULL,
	PaymentStatusId int NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Znode_Transactions SET (LOCK_ESCALATION = TABLE)
GO
IF EXISTS(SELECT * FROM dbo.Znode_Transactions)
	 EXEC('INSERT INTO dbo.Tmp_Znode_Transactions (GUID, CustomerProfileId, CustomerPaymentId, TransactionId, TransactionDate, CaptureTransactionDate, RefundTransactionDate, ResponseText, ResponseCode, Custom1, Custom2, Custom3, Amount, PaymentSettingId, CurrencyCode, SubscriptionId, PaymentStatusId)
		SELECT GUID, CustomerProfileId, CustomerPaymentId, TransactionId, TransactionDate, CaptureTransactionDate, RefundTransactionDate, ResponseText, ResponseCode, Custom1, Custom2, Custom3, CONVERT(money, Amount), PaymentSettingId, CurrencyCode, SubscriptionId, PaymentStatusId FROM dbo.Znode_Transactions WITH (HOLDLOCK TABLOCKX)')
GO
DROP TABLE dbo.Znode_Transactions
GO
EXECUTE sp_rename N'dbo.Tmp_Znode_Transactions', N'Znode_Transactions', 'OBJECT' 
GO
ALTER TABLE dbo.Znode_Transactions ADD CONSTRAINT
	PK_Znode_Transactions PRIMARY KEY CLUSTERED 
	(
	GUID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.Znode_Transactions ADD CONSTRAINT
	FK_Znode_Transactions_ZNodePaymentSetting FOREIGN KEY
	(
	PaymentSettingId
	) REFERENCES dbo.ZNodePaymentSetting
	(
	PaymentSettingID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO



COMMIT
GO 

IF NOT  EXISTS (select * from ZNodeGateway where GatewayName  = 'WorldPay' and GatewayTypeID = 8 )
Begin 	
	Set Identity_insert ZNodeGateway  on 
	insert into ZNodeGateway (GatewayTypeID,GatewayName,WebsiteURL)
	values (8,'WorldPay','http://www.wordlpay.com')
	Set Identity_insert ZNodeGateway  off  
END 
