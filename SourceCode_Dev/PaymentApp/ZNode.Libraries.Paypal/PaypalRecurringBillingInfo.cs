﻿namespace ZNode.Libraries.Paypal
{
    /// <summary>
    /// Paypal Recurring Billing Info
    /// </summary>
    public class PaypalRecurringBillingInfo
    {
        #region Protected Member Variables
        private decimal _Amount = 0;
        private decimal _InitialAmount = 0;

        private bool _InstallmentInd = false;

        private string _Period = string.Empty;
        private string _Frequency = string.Empty;       
        private string _ProfileName = string.Empty;

        private int _TotalCycles = 0;
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets a value indicating whether installment indicator is on or off
        /// </summary>        
        public bool InstallmentInd
        {
            get { return this._InstallmentInd; }
            set { this._InstallmentInd = value; }
        }

        /// <summary>
        /// Gets or sets the period
        /// </summary>        
        public string Period
        {
            get { return this._Period; }
            set { this._Period = value; }
        }

        /// <summary>
        /// Gets or sets the frequency
        /// </summary>        
        public string Frequency
        {
            get { return this._Frequency; }
            set { this._Frequency = value; }
        }

        /// <summary>
        /// Gets or sets the total cycles
        /// </summary>        
        public int TotalCycles
        {
            get { return this._TotalCycles; }
            set { this._TotalCycles = value; }
        }

        /// <summary>
        /// Gets or sets the initial amount
        /// </summary>        
        public decimal InitialAmount
        {
            get { return this._InitialAmount; }
            set { this._InitialAmount = value; }
        }

        /// <summary>
        /// Gets or sets the amount
        /// </summary>        
        public decimal Amount
        {
            get { return this._Amount; }
            set { this._Amount = value; }
        }

        /// <summary>
        /// Gets or sets the profile name
        /// </summary>        
        public string ProfileName
        {
            get { return this._ProfileName; }
            set { this._ProfileName = value; }
        }
        #endregion
    }
}