﻿using System;
using Znode.Multifront.PaymentApplication.Models;

namespace ZNode.Libraries.Paypal
{
    /// <summary>
    /// Paypal response
    /// </summary>
    public class PaypalResponse
    {
        #region Private member variables
        /// <summary>
        /// Represents Express Checkout redirect URL
        /// </summary>
        private string _RedirectURL = string.Empty;

        /// <summary>
        /// Represents the Gateway Response code as a string.
        /// </summary>
        private string _ResponseCode = string.Empty;

        /// <summary>
        ///  Represents the Gateway Response code as a string.
        /// </summary>
        private string _ResponseText = string.Empty;

        /// <summary>
        ///  Represents the Gateway Response code as a string.
        /// </summary>
        private string _PaypalToken = string.Empty;

        /// <summary>
        ///  Represents the Gateway Response code as a string.
        /// </summary>
        private string _HostUrl = "https://{0}/cgi-bin/webscr?cmd=_express-checkout&token={1}";


        /// <summary>
        /// Represents Payment Status
        /// </summary>
        private string _PaymentStatus = string.Empty;

        /// <summary>
        /// Represents Transaction ID
        /// </summary>
        private string _TransactionID = string.Empty;

        /// <summary>
        ///  Represents ProfileID as a string.
        /// </summary>
        private string _ProfileID = string.Empty;

        /// <summary>
        /// Represents payer unique ID
        /// </summary>
        private string _PayerID = string.Empty;

        #endregion

        #region Public properties
        /// <summary>
        /// Gets or sets or retrieves the Gateway Response code
        /// </summary>
        public string ResponseCode
        {
            get { return this._ResponseCode; }
            set { this._ResponseCode = value; }
        }

        /// <summary>
        /// Gets or sets or retrieves the Gateway Response Text
        /// </summary>
        public string ResponseText
        {
            get { return this._ResponseText; }
            set { this._ResponseText = value; }
        }

        /// <summary>
        /// Gets or sets or retrieves the Gateway TransactionID
        /// </summary>
        public string PayalToken
        {
            get { return this._PaypalToken; }
            set { this._PaypalToken = value; }
        }

        /// <summary>
        /// Gets or sets or retrieves the Gateway TransactionID
        /// </summary>
        public string HostUrl
        {
            get { return this._HostUrl; }
            set { this._HostUrl = value; }
        }

        /// <summary>
        /// Gets or sets or retrives the EC Redirect URL.
        /// </summary>
        public string ECRedirectURL
        {
            get { return this._RedirectURL; }
            set { this._RedirectURL = value; }
        }

        /// <summary>
        ///  Gets or sets or retrieves the Paypal Gateway payer ID
        /// </summary>
        public string PayerID
        {
            get { return this._PayerID; }
            set { this._PayerID = value; }
        }


        /// <summary>
        /// Gets or sets or retrieves the Paypal-PaymentStatus
        /// </summary>
        public string PaymentStatus
        {
            get { return this._PaymentStatus; }
            set { this._PaymentStatus = value; }
        }

        /// <summary>
        /// Gets or sets or retrieves the Paypal-TransactionID
        /// </summary>
        public string TransactionID
        {
            get { return this._TransactionID; }
            set { this._TransactionID = value; }
        }

        /// <summary>
        /// Gets or sets or retrieves the Paypal-TransactionID
        /// </summary>
        public string ProfileID
        {
            get { return this._ProfileID; }
            set { this._ProfileID = value; }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Sets or retrieves the PaypalResponse
        /// </summary>
        /// <returns>Payment Status</returns>
        public ZnodePaymentStatus GetPaymentStatus()
        {
            ZnodePaymentStatus result = ZnodePaymentStatus.CREDIT_PENDING;

            switch (this.PaymentStatus)
            {
                case "Completed":
                    {
                        result = ZnodePaymentStatus.CREDIT_CAPTURED;
                        break;
                    }
                case "Canceled-Reversal":
                    {
                        result = ZnodePaymentStatus.CREDIT_DECLINED;
                        break;
                    }
                case "Denied":
                case "Expired":
                case "Failed":
                case "In-Progress":
                case "Partially-Refunded":
                case "Pending":
                    {
                        result = ZnodePaymentStatus.CREDIT_PENDING;
                        break;
                    }
                case "Refunded":
                    {
                        result = ZnodePaymentStatus.CREDIT_REFUNDED;
                        break;
                    }
                case "Reversed":
                case "Processed":
                    {
                        result = ZnodePaymentStatus.CREDIT_AUTHORIZED;
                        break;
                    }
                case "Voided":
                    break;
                default:
                    break;
            }

            return result;
        }

        /// <summary>
        /// Gets the paypal billing period
        /// </summary>
        /// <param name="Unit">Unit of the paypal billing period</param>
        /// <returns>Returns the billing period</returns>
        public string GetPaypalBillingPeriod(string Unit)
        {
            if (Unit.Equals("DAY", StringComparison.OrdinalIgnoreCase))
            {
                return "Day";
            }
            else if (Unit.Equals("WEEK", StringComparison.OrdinalIgnoreCase))
            {
                return "Week";
            }
            else if (Unit.Equals("MONTH", StringComparison.OrdinalIgnoreCase))
            {
                return "Month";
            }
            else if (Unit.Equals("YEAR", StringComparison.OrdinalIgnoreCase))
            {
                return "Year";
            }

            return "NoBillingPeriodType";
        }
        #endregion
    }
}