
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, and Azure
-- --------------------------------------------------
-- Date Created: 04/24/2015 11:48:39
-- Generated from EDMX file: D:\7.2.3\Projects\Payment API\Payment API\Znode.Engine.Payment.API\Znode.Engine.Payment.Api\PaymentTransactionEF.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [aspnet-TestPayment-20150205144857];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_SC_PaymentSetting_SC_Gateway]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ZNodePaymentSettings] DROP CONSTRAINT [FK_SC_PaymentSetting_SC_Gateway];
GO
IF OBJECT_ID(N'[dbo].[FK_Znode_Transactions_ZNodePaymentSetting]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Znode_Transactions] DROP CONSTRAINT [FK_Znode_Transactions_ZNodePaymentSetting];
GO
IF OBJECT_ID(N'[dbo].[FK_Znode_Transactions_ZNodePaymentSetting1]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Znode_Transactions] DROP CONSTRAINT [FK_Znode_Transactions_ZNodePaymentSetting1];
GO
IF OBJECT_ID(N'[dbo].[FK_SC_PaymentSetting_SC_Gateway1]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ZNodePaymentSetting1] DROP CONSTRAINT [FK_SC_PaymentSetting_SC_Gateway1];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Znode_Transactions]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Znode_Transactions];
GO
IF OBJECT_ID(N'[dbo].[ZNodeGateways]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ZNodeGateways];
GO
IF OBJECT_ID(N'[dbo].[ZNodePaymentSettings]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ZNodePaymentSettings];
GO
IF OBJECT_ID(N'[dbo].[ZNodeActivityLogs]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ZNodeActivityLogs];
GO
IF OBJECT_ID(N'[dbo].[ZNodeActivityLog1]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ZNodeActivityLog1];
GO
IF OBJECT_ID(N'[dbo].[ZNodeGateway1]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ZNodeGateway1];
GO
IF OBJECT_ID(N'[dbo].[ZNodePaymentSetting1]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ZNodePaymentSetting1];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Znode_Transactions'
CREATE TABLE [dbo].[Znode_Transactions] (
    [GUID] uniqueidentifier  NOT NULL,
    [CustomerProfileId] nvarchar(max)  NULL,
    [CustomerPaymentId] nvarchar(max)  NULL,
    [TransactionId] nvarchar(max)  NULL,
    [TransactionDate] datetime  NULL,
    [CaptureTransactionDate] datetime  NULL,
    [RefundTransactionDate] datetime  NULL,
    [ResponseText] nvarchar(max)  NULL,
    [ResponseCode] nvarchar(50)  NULL,
    [Custom1] nvarchar(max)  NULL,
    [Custom2] nvarchar(max)  NULL,
    [Custom3] nvarchar(max)  NULL,
    [Amount] decimal(18,0)  NULL,
    [PaymentSettingId] int  NULL,
    [CurrencyCode] nvarchar(50)  NULL,
    [TokenId] nvarchar(max)  NULL,
    [SubscriptionId] nvarchar(max)  NULL,
    [PaymentStatusId] int  NULL
);
GO

-- Creating table 'ZNodeGateways'
CREATE TABLE [dbo].[ZNodeGateways] (
    [GatewayTypeID] int IDENTITY(1,1) NOT NULL,
    [GatewayName] varchar(max)  NOT NULL,
    [WebsiteURL] varchar(max)  NULL
);
GO

-- Creating table 'ZNodePaymentSettings'
CREATE TABLE [dbo].[ZNodePaymentSettings] (
    [PaymentSettingID] int IDENTITY(1,1) NOT NULL,
    [PaymentTypeID] int  NOT NULL,
    [ProfileID] int  NULL,
    [GatewayTypeID] int  NULL,
    [GatewayUsername] nvarchar(max)  NULL,
    [GatewayPassword] nvarchar(max)  NULL,
    [EnableVisa] bit  NULL,
    [EnableMasterCard] bit  NULL,
    [EnableAmex] bit  NULL,
    [EnableDiscover] bit  NULL,
    [EnableRecurringPayments] bit  NULL,
    [EnableVault] bit  NULL,
    [TransactionKey] nvarchar(max)  NULL,
    [ActiveInd] bit  NOT NULL,
    [DisplayOrder] int  NOT NULL,
    [TestMode] bit  NOT NULL,
    [Partner] nvarchar(max)  NULL,
    [Vendor] nvarchar(max)  NULL,
    [PreAuthorize] bit  NOT NULL,
    [IsRMACompatible] bit  NULL
);
GO

-- Creating table 'ZNodeActivityLogs'
CREATE TABLE [dbo].[ZNodeActivityLogs] (
    [ActivityLogID] int IDENTITY(1,1) NOT NULL,
    [PaymentSettingID] int  NULL,
    [CreateDte] datetime  NOT NULL,
    [EndDte] datetime  NULL,
    [PortalID] int  NULL,
    [URL] nvarchar(max)  NULL,
    [Data1] nvarchar(255)  NULL,
    [Data2] nvarchar(255)  NULL,
    [Data3] nvarchar(255)  NULL,
    [Status] nvarchar(255)  NULL,
    [LongData] nvarchar(max)  NULL,
    [Source] nvarchar(255)  NULL,
    [Target] nvarchar(255)  NULL
);
GO

-- Creating table 'ZNodeActivityLog1'
CREATE TABLE [dbo].[ZNodeActivityLog1] (
    [ActivityLogID] int IDENTITY(1,1) NOT NULL,
    [PaymentSettingID] int  NULL,
    [CreateDte] datetime  NOT NULL,
    [EndDte] datetime  NULL,
    [PortalID] int  NULL,
    [URL] nvarchar(max)  NULL,
    [Data1] nvarchar(255)  NULL,
    [Data2] nvarchar(255)  NULL,
    [Data3] nvarchar(255)  NULL,
    [Status] nvarchar(255)  NULL,
    [LongData] nvarchar(max)  NULL,
    [Source] nvarchar(255)  NULL,
    [Target] nvarchar(255)  NULL
);
GO

-- Creating table 'ZNodeGateway1'
CREATE TABLE [dbo].[ZNodeGateway1] (
    [GatewayTypeID] int IDENTITY(1,1) NOT NULL,
    [GatewayName] varchar(max)  NOT NULL,
    [WebsiteURL] varchar(max)  NULL
);
GO

-- Creating table 'ZNodePaymentSetting1'
CREATE TABLE [dbo].[ZNodePaymentSetting1] (
    [PaymentSettingID] int  NOT NULL,
    [PaymentTypeID] int  NOT NULL,
    [ProfileID] int  NULL,
    [GatewayTypeID] int  NULL,
    [GatewayUsername] nvarchar(max)  NULL,
    [GatewayPassword] nvarchar(max)  NULL,
    [EnableVisa] bit  NULL,
    [EnableMasterCard] bit  NULL,
    [EnableAmex] bit  NULL,
    [EnableDiscover] bit  NULL,
    [EnableRecurringPayments] bit  NULL,
    [EnableVault] bit  NULL,
    [TransactionKey] nvarchar(max)  NULL,
    [ActiveInd] bit  NOT NULL,
    [DisplayOrder] int  NOT NULL,
    [TestMode] bit  NOT NULL,
    [Partner] nvarchar(max)  NULL,
    [Vendor] nvarchar(max)  NULL,
    [PreAuthorize] bit  NOT NULL,
    [IsRMACompatible] bit  NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [GUID] in table 'Znode_Transactions'
ALTER TABLE [dbo].[Znode_Transactions]
ADD CONSTRAINT [PK_Znode_Transactions]
    PRIMARY KEY CLUSTERED ([GUID] ASC);
GO

-- Creating primary key on [GatewayTypeID] in table 'ZNodeGateways'
ALTER TABLE [dbo].[ZNodeGateways]
ADD CONSTRAINT [PK_ZNodeGateways]
    PRIMARY KEY CLUSTERED ([GatewayTypeID] ASC);
GO

-- Creating primary key on [PaymentSettingID] in table 'ZNodePaymentSettings'
ALTER TABLE [dbo].[ZNodePaymentSettings]
ADD CONSTRAINT [PK_ZNodePaymentSettings]
    PRIMARY KEY CLUSTERED ([PaymentSettingID] ASC);
GO

-- Creating primary key on [ActivityLogID] in table 'ZNodeActivityLogs'
ALTER TABLE [dbo].[ZNodeActivityLogs]
ADD CONSTRAINT [PK_ZNodeActivityLogs]
    PRIMARY KEY CLUSTERED ([ActivityLogID] ASC);
GO

-- Creating primary key on [ActivityLogID] in table 'ZNodeActivityLog1'
ALTER TABLE [dbo].[ZNodeActivityLog1]
ADD CONSTRAINT [PK_ZNodeActivityLog1]
    PRIMARY KEY CLUSTERED ([ActivityLogID] ASC);
GO

-- Creating primary key on [GatewayTypeID] in table 'ZNodeGateway1'
ALTER TABLE [dbo].[ZNodeGateway1]
ADD CONSTRAINT [PK_ZNodeGateway1]
    PRIMARY KEY CLUSTERED ([GatewayTypeID] ASC);
GO

-- Creating primary key on [PaymentSettingID] in table 'ZNodePaymentSetting1'
ALTER TABLE [dbo].[ZNodePaymentSetting1]
ADD CONSTRAINT [PK_ZNodePaymentSetting1]
    PRIMARY KEY CLUSTERED ([PaymentSettingID] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [GatewayTypeID] in table 'ZNodePaymentSettings'
ALTER TABLE [dbo].[ZNodePaymentSettings]
ADD CONSTRAINT [FK_SC_PaymentSetting_SC_Gateway]
    FOREIGN KEY ([GatewayTypeID])
    REFERENCES [dbo].[ZNodeGateways]
        ([GatewayTypeID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_SC_PaymentSetting_SC_Gateway'
CREATE INDEX [IX_FK_SC_PaymentSetting_SC_Gateway]
ON [dbo].[ZNodePaymentSettings]
    ([GatewayTypeID]);
GO

-- Creating foreign key on [PaymentSettingId] in table 'Znode_Transactions'
ALTER TABLE [dbo].[Znode_Transactions]
ADD CONSTRAINT [FK_Znode_Transactions_ZNodePaymentSetting]
    FOREIGN KEY ([PaymentSettingId])
    REFERENCES [dbo].[ZNodePaymentSettings]
        ([PaymentSettingID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Znode_Transactions_ZNodePaymentSetting'
CREATE INDEX [IX_FK_Znode_Transactions_ZNodePaymentSetting]
ON [dbo].[Znode_Transactions]
    ([PaymentSettingId]);
GO

-- Creating foreign key on [PaymentSettingId] in table 'Znode_Transactions'
ALTER TABLE [dbo].[Znode_Transactions]
ADD CONSTRAINT [FK_Znode_Transactions_ZNodePaymentSetting1]
    FOREIGN KEY ([PaymentSettingId])
    REFERENCES [dbo].[ZNodePaymentSetting1]
        ([PaymentSettingID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Znode_Transactions_ZNodePaymentSetting1'
CREATE INDEX [IX_FK_Znode_Transactions_ZNodePaymentSetting1]
ON [dbo].[Znode_Transactions]
    ([PaymentSettingId]);
GO

-- Creating foreign key on [GatewayTypeID] in table 'ZNodePaymentSetting1'
ALTER TABLE [dbo].[ZNodePaymentSetting1]
ADD CONSTRAINT [FK_SC_PaymentSetting_SC_Gateway1]
    FOREIGN KEY ([GatewayTypeID])
    REFERENCES [dbo].[ZNodeGateway1]
        ([GatewayTypeID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_SC_PaymentSetting_SC_Gateway1'
CREATE INDEX [IX_FK_SC_PaymentSetting_SC_Gateway1]
ON [dbo].[ZNodePaymentSetting1]
    ([GatewayTypeID]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------