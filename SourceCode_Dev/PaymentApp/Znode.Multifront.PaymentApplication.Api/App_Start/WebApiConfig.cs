﻿using System.Web.Http;
using System.Web.Routing;

namespace Znode.Multifront.PaymentApplication.Api
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Uncomment the following line of code to enable query support for actions with an IQueryable or IQueryable<T> return type.
            // To avoid processing unexpected or malicious queries, use the validation settings on QueryableAttribute to validate incoming queries.
            // For more information, visit http://go.microsoft.com/fwlink/?LinkId=279712.
            //config.EnableQuerySupport();

            // To disable tracing in your application, please comment out or remove the following line of code
            // For more information, refer to: http://www.asp.net/web-api
            config.Formatters.Remove(config.Formatters.XmlFormatter);

            config.Routes.MapHttpRoute("script-znodeapijs", "script/znodeapijs", new { controller = "script", action = "znodeapijs" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("payment-getgateways", "payment/getgateways", new { controller = "payment", action = "getgateways" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("payment-paynow", "payment/paynow", new { controller = "payment", action = "paynow" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("payment-paypal", "payment/paypal", new { controller = "payment", action = "paypal" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("payment-paypalexpress", "payment/paypalexpress", new { controller = "payment", action = "paypalexpress" }, new { httpMethod = new HttpMethodConstraint("POST") });

            config.Routes.MapHttpRoute("payment-configurepaymentsettings", "payment/configurepaymentsettings", new { controller = "payment", action = "configurepaymentsettings" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("payment-capture", "payment/capture", new { controller = "payment", action = "capture" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("payment-void", "payment/void", new { controller = "payment", action = "void" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("payment-refund", "payment/refund", new { controller = "payment", action = "refund" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("payment-deletepaymentsettings", "payment/deletepaymentsettings", new { controller = "payment", action = "deletepaymentsettings" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("payment-getpaymentsettingdetails", "payment/getpaymentsettingdetails", new { controller = "payment", action = "getpaymentsettingdetails" }, new { httpMethod = new HttpMethodConstraint("GET") });
        }
    }
}
