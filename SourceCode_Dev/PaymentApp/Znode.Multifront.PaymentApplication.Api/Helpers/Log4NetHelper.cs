﻿using System;
using System.IO;
using Znode.Multifront.PaymentApplication.Api.PaymentProviders;

namespace Znode.Multifront.PaymentApplication.Api.Helpers
{
    public static class Log4NetHelper
    {
        /// <summary>
        /// To replace log4net dll as per requirement in bin folder
        /// For Paymenttech orbital - log4net ver 4.0
        /// & for PayPal Express - log4net ver 1.2
        /// </summary>
        /// <param name="gatewayType">GatewayType gatewayType</param>
        public static void ReplaceLog4NetDLL(string gatewayType)
        {

            if (Equals(gatewayType, Convert.ToInt16(Enum.Parse(typeof(GatewayType), GatewayType.PAYMENTTECH.ToString())).ToString()) ||
                Equals(gatewayType, Convert.ToInt16(Enum.Parse(typeof(GatewayType), GatewayType.PAYPALEXPRESS.ToString())).ToString()))
            {
                string sourceFileName = string.Empty;
                if (Equals(gatewayType, Convert.ToInt16(Enum.Parse(typeof(GatewayType), GatewayType.PAYMENTTECH.ToString())).ToString()))
                {
                    sourceFileName = "\\Lib\\log4net-v4.0\\log4net.dll";
                }
                else if (Equals(gatewayType, Convert.ToInt16(Enum.Parse(typeof(GatewayType), GatewayType.PAYPALEXPRESS.ToString())).ToString()))
                {
                    sourceFileName = "\\Lib\\log4net-v1.2\\log4net.dll";
                }

                string destinationFilePath = "~/bin";
                destinationFilePath = (System.Web.HttpContext.Current.Server.MapPath(destinationFilePath));

                string sourceFilePath = Directory.GetParent(Directory.GetParent(destinationFilePath).FullName).FullName;

                sourceFilePath = string.Concat(sourceFilePath, sourceFileName);
                destinationFilePath = string.Concat(destinationFilePath, "\\log4net.dll");

                File.Copy(sourceFilePath, destinationFilePath, true);
            }
        }
    }
}