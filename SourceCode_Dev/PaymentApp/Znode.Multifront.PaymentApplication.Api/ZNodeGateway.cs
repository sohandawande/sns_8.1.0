
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace Znode.Multifront.PaymentApplication.Api
{

using System;
    using System.Collections.Generic;
    
public partial class ZNodeGateway
{

    public ZNodeGateway()
    {

        this.ZNodePaymentSettings = new HashSet<ZNodePaymentSetting>();

    }


    public int GatewayTypeID { get; set; }

    public string GatewayName { get; set; }

    public string WebsiteURL { get; set; }



    public virtual ICollection<ZNodePaymentSetting> ZNodePaymentSettings { get; set; }

}

}
