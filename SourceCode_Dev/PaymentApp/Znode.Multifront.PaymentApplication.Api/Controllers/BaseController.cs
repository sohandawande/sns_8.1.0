﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Serialization;
using Znode.Multifront.PaymentApplication.Api.Repository;

namespace Znode.Multifront.PaymentApplication.Api.Controllers
{
    public class BaseController : ApiController
    {
        private string _domainName;
        private static QueryStringParser _queryStringParser;
        private static JsonMediaTypeFormatter _jsonMediaTypeFormatter;
        private static XmlMediaTypeFormatter _xmlMediaTypeFormatter;

        protected string RouteTemplate
        {
            get { return ControllerContext.RouteData.Route.RouteTemplate; }
        }

        protected string RouteUri
        {
            get
            {
                if (!String.IsNullOrEmpty(_domainName))
                {
                    return new UriBuilder(ControllerContext.Request.RequestUri.AbsoluteUri) { Host = _domainName }.Uri.ToString();
                }

                return ControllerContext.Request.RequestUri.AbsoluteUri;
            }
        }

        protected static bool Indent
        {
            get
            {
                var indent = false;

                if (_queryStringParser.Indent.HasKeys())
                {
                    if (!String.IsNullOrEmpty(_queryStringParser.Indent.Get("true")))
                    {
                        indent = true;
                    }
                }

                return indent;
            }
        }

        protected static MediaTypeFormatter MediaTypeFormatter
        {
            get
            {
                if (_queryStringParser.Format.HasKeys())
                {
                    if (!String.IsNullOrEmpty(_queryStringParser.Format.Get("xml")))
                    {
                        // XML response format must be done with the XmlSerializer
                        return _xmlMediaTypeFormatter ?? (_xmlMediaTypeFormatter = new XmlMediaTypeFormatter { UseXmlSerializer = true, Indent = Indent });
                    }
                }

                return _jsonMediaTypeFormatter ?? (_jsonMediaTypeFormatter = new JsonMediaTypeFormatter() { Indent = Indent });
                // JSON is the default response format
            }
        }

        protected MediaTypeHeaderValue MediaTypeHeaderValue
        {
            get
            {
                if (MediaTypeFormatter.GetType() == typeof(XmlMediaTypeFormatter))
                {
                    return new MediaTypeHeaderValue("application/xml");
                }

                // JSON is the default
                return new MediaTypeHeaderValue("application/json");
            }
        }

        protected override void Initialize(HttpControllerContext controllerContext)
        {
            var validateAuthHeader = Convert.ToBoolean(ConfigurationManager.AppSettings["ValidateAuthHeader"]);
            if (validateAuthHeader)
            {
                var headerOk = false;
                var authHeader = GetAuthHeader();

                if (Equals(authHeader, null))
                {
                    _domainName = authHeader[0];
                    headerOk = CheckAuthHeader(authHeader[0], authHeader[1]);
                }

                if (!headerOk)
                {
                    HttpContext.Current.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                    HttpContext.Current.Response.StatusDescription = "Domain name and key are either incorrect or missing from the request Authorization header.";
                    HttpContext.Current.Response.SuppressContent = true;
                    HttpContext.Current.Response.SuppressFormsAuthenticationRedirect = true;
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                }
            }

            _queryStringParser = new QueryStringParser(controllerContext.Request.RequestUri.Query);
            base.Initialize(controllerContext);
        }

        protected HttpResponseMessage CreateOKResponse<T>(string data)
        {
            if (Indent)
            {
                // Indentation should only ever be used by humans when viewing a response in a browser,
                // so taking the performance hit (albeit a small one) with the deserialization is fine.
                var dataDeserialized = JsonConvert.DeserializeObject<T>(data);
                return Request.CreateResponse(HttpStatusCode.OK, dataDeserialized, MediaTypeFormatter);
            }

            // We use StringContent to skip the content negotiation and serializaton step (performance improvement).
            var response = new HttpResponseMessage { StatusCode = HttpStatusCode.OK, Content = new StringContent(data) };
            response.Content.Headers.ContentType = MediaTypeHeaderValue;
            return response;
        }

        protected HttpResponseMessage CreateOKResponse<T>(T data)
        {
            return Request.CreateResponse(HttpStatusCode.OK, data, MediaTypeFormatter);
        }

        protected HttpResponseMessage CreateOKResponse()
        {
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        protected HttpResponseMessage CreateCreatedResponse<T>(T data)
        {
            return Request.CreateResponse(HttpStatusCode.Created, data, MediaTypeFormatter);
        }

        protected HttpResponseMessage CreateInternalServerErrorResponse<T>(T data)
        {
            var basedata = data as BaseResponse;

            if (basedata != null)
            {
                var newEx = new Exception(basedata.ErrorCode + ":" + basedata.ErrorMessage);

            }

            return Request.CreateResponse(HttpStatusCode.InternalServerError, data, MediaTypeFormatter);
        }

        protected HttpResponseMessage CreateInternalServerErrorResponse()
        {
            return Request.CreateResponse(HttpStatusCode.InternalServerError);
        }

        protected HttpResponseMessage CreateNotFoundResponse()
        {
            return Request.CreateResponse(HttpStatusCode.NotFound);
        }

        protected HttpResponseMessage CreateNoContentResponse()
        {
            return Request.CreateResponse(HttpStatusCode.NoContent);
        }

        protected HttpResponseMessage CreateUnauthorizedResponse<T>(T data)
        {
            var basedata = data as BaseResponse;

            if (basedata != null)
            {
                var newEx = new Exception(basedata.ErrorCode + ":" + basedata.ErrorMessage);
                // Elmah.ErrorSignal.FromCurrentContext().Raise(newEx);
            }
            return Request.CreateResponse(HttpStatusCode.Unauthorized, data, MediaTypeFormatter);
        }

        private string[] GetAuthHeader()
        {
            var headers = HttpContext.Current.Request.Headers;
            var authValue = headers.AllKeys.Contains("Authorization") ? headers["Authorization"] : String.Empty;

            // If auth value doesn't exist, get out
            if (String.IsNullOrEmpty(authValue)) return null;

            // Strip off the "Basic "
            authValue = authValue.Remove(0, 6);

            // Decode it; if empty then get out
            var authValueDecoded = DecodeBase64(authValue);
            if (String.IsNullOrEmpty(authValueDecoded)) return null;

            // Now split it to get the domain info (index 0 = domain name, index 1 = domain key)
            return authValueDecoded.Split('|');
        }

        private bool CheckAuthHeader(string domainName, string domainKey)
        {
            // If either domain name or domain key are empty, get out
            if (String.IsNullOrEmpty(domainName) || String.IsNullOrEmpty(domainKey)) return false;

            // Get the configured key for the domain
            var configuredDomainKey = GetConfiguredDomainKey(domainName);

            // Now compare the two
            return String.Compare(domainKey, configuredDomainKey, StringComparison.InvariantCulture) == 0;
        }

        private string DecodeBase64(string encodedValue)
        {
            var encodedValueAsBytes = Convert.FromBase64String(encodedValue);
            return Encoding.UTF8.GetString(encodedValueAsBytes);
        }

        private string GetConfiguredDomainKey(string domainName)
        {
            DomainRepository repository = new DomainRepository();
            var domainConfig = repository.GetDomain(domainName);
            return domainConfig != null ? domainConfig.ApiKey : String.Empty;
        }

        protected string EncodeBase64(string value)
        {
            var valueAsBytes = Encoding.UTF8.GetBytes(value);
            return Convert.ToBase64String(valueAsBytes);
        }

    }

    public class BaseResponse
    {
        public int? ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public bool HasError { get; set; }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Newtonsoft.Json.Formatting.Indented);
        }

        public string ToXml()
        {
            var xml = String.Empty;

            var serializer = new XmlSerializer(GetType());
            var ms = new MemoryStream();

            using (var tw = new XmlTextWriter(ms, Encoding.UTF8) { Formatting = System.Xml.Formatting.Indented })
            {
                serializer.Serialize(tw, this);
                ms = tw.BaseStream as MemoryStream;

                if (ms != null)
                {
                    xml = new UTF8Encoding().GetString(ms.ToArray());
                    ms.Dispose();
                }
            }

            return xml;
        }
    }

    public class QueryStringParser
    {
        private readonly string _queryString;

        public NameValueCollection Expands
        {
            get { return GetKeyValuePairs("expand"); }
        }

        public List<Tuple<string, string, string>> Filters
        {
            get { return GetTuples("filter"); }
        }

        public NameValueCollection Sorts
        {
            get { return GetKeyValuePairs("sort"); }
        }

        public NameValueCollection Page
        {
            get { return GetKeyValuePairs("page"); }
        }

        public NameValueCollection Format
        {
            get { return GetKeyValuePairs("format"); }
        }

        public NameValueCollection Indent
        {
            get { return GetKeyValuePairs("indent"); }
        }

        public NameValueCollection Cache
        {
            get { return GetKeyValuePairs("cache"); }
        }

        public QueryStringParser(string queryString)
        {
            _queryString = queryString.ToLower();
        }

        private NameValueCollection GetKeyValuePairs(string param)
        {
            var kvps = new NameValueCollection();
            var query = HttpUtility.ParseQueryString(_queryString);

            var uriItemSeparator = ConfigurationManager.AppSettings["UriItemSeparator"];
            var uriKeyValueSeparator = ConfigurationManager.AppSettings["UriKeyValueSeparator"];

            foreach (var key in query.AllKeys)
            {
                if (key.ToLower() == param)
                {
                    var value = query.Get(key);
                    var items = value.Split(uriItemSeparator.ToCharArray());

                    foreach (var item in items)
                    {
                        if (item.Contains(uriKeyValueSeparator))
                        {
                            var set = item.Split(uriKeyValueSeparator.ToCharArray());
                            kvps.Add(set[0].ToLower(), HttpUtility.HtmlDecode(set[1]));
                        }
                        else
                        {
                            // Just make the value the same as the key, for consistency of code in other places
                            kvps.Add(item.ToLower(), item.ToLower());
                        }
                    }

                    break;
                }
            }

            return kvps;
        }

        private List<Tuple<string, string, string>> GetTuples(string param)
        {
            var filters = new List<Tuple<string, string, string>>();
            var query = HttpUtility.ParseQueryString(_queryString);

            var uriItemSeparator = ConfigurationManager.AppSettings["UriItemSeparator"];
            var uriKeyValueSeparator = ConfigurationManager.AppSettings["UriKeyValueSeparator"];

            foreach (var key in query.AllKeys)
            {
                if (key.ToLower() == param)
                {
                    var value = query.Get(key);
                    var items = value.Split(uriItemSeparator.ToCharArray());

                    foreach (var item in items)
                    {
                        if (item.Contains(uriKeyValueSeparator))
                        {
                            var tuple = item.Split(uriKeyValueSeparator.ToCharArray());
                            var filterKey = tuple[0].ToLower();
                            var filterOperator = tuple[1].ToLower();
                            var filterValue = tuple[2].ToLower();

                            filters.Add(new Tuple<string, string, string>(filterKey, filterOperator, HttpUtility.HtmlDecode(filterValue)));
                        }
                    }

                    break;
                }
            }

            return filters;
        }
    }
}
