﻿﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Web.Http;
using Znode.Multifront.PaymentApplication.Api.PaymentProviders;
using Znode.Multifront.PaymentApplication.Api.Repository;
using Znode.Multifront.PaymentApplication.Models;

namespace Znode.Multifront.PaymentApplication.Api.Controllers
{
    /// <summary>
    /// This class will be responsible for the
    /// </summary>
    public class PaymentController : BaseController
    {
        private GatewayConnector gatewayConnector = new GatewayConnector();

        /// <summary>
        /// Payment model submitted to process credit card data through gateway
        /// </summary>
        /// <param name="paymentModel"></param>
        /// <returns></returns>
        [HttpPost]
        public System.Web.Mvc.JsonResult PayNow(PaymentModel paymentModel)
        {
            string returnUrl;
            try
            {
                if (IsCustomerProfileIdExist(paymentModel))
                {
                    var response = new GatewayResponseModel();
                    try
                    {
                        response = gatewayConnector.GetCustomerResponse(paymentModel);
                    }
                    catch (Exception ex)
                    {
                        Logging.LogMessage(string.Format("Exception Occurred :{0},{1}", ex.Message, ex.StackTrace.ToString()));
                        Logging.LogActivity(paymentModel.PaymentSettingId, ex.Message);
                    }
                    return new System.Web.Mvc.JsonResult { Data = response };
                }
                returnUrl = gatewayConnector.GetResponse(paymentModel);
            }
            catch (Exception ex)
            {
                Logging.LogMessage(string.Format("Exception Occurred :{0},{1}", ex.Message, ex.StackTrace.ToString()));
                Logging.LogActivity(paymentModel.PaymentSettingId, ex.Message);
                returnUrl = paymentModel.CancelUrl + (paymentModel.ReturnUrl.IndexOf('?') > 0 ? "&" : "?") +
                            string.Format("Message={0}", ex.Message);
            }
            return new System.Web.Mvc.JsonResult { Data = returnUrl };
        }

        /// <summary>
        /// Creates a Paypal token and redirects to Paypal website
        /// </summary>
        /// <param name="paymentModel"></param>
        /// <returns></returns>
        [HttpPost]
        public System.Web.Mvc.JsonResult Paypal(PaymentModel paymentModel)
        {
            string returnUrl = string.Empty;
            try
            {
                if (!Equals(paymentModel, null) && !string.IsNullOrEmpty(paymentModel.BillingCity))
                {
                    returnUrl = gatewayConnector.GetPaypalResponse(paymentModel);
                }
                else
                {
                    return new System.Web.Mvc.JsonResult { Data = string.Empty };
                }
            }
            catch (Exception ex)
            {
                Logging.LogMessage(string.Format("Exception Occurred :{0},{1}", ex.Message, ex.StackTrace.ToString()));
                Logging.LogActivity(paymentModel.PaymentSettingId, ex.Message);
                returnUrl = paymentModel.CancelUrl + (paymentModel.ReturnUrl.IndexOf('?') > 0 ? "&" : "?") +
                           string.Format("Message={0}", ex.Message);
            }
            return new System.Web.Mvc.JsonResult { Data = returnUrl };
        }

        /// <summary>
        /// Capture transaction based on GUID given
        /// </summary>
        /// <param name="token"></param>       
        /// <returns></returns>
        [HttpGet]
        public string Capture(string token)
        {
            try
            {
                return gatewayConnector.GetCaptureResponse(token);
            }
            catch (Exception ex)
            {
                Logging.LogMessage(string.Format("Exception Occurred :{0},{1}", ex.Message, ex.StackTrace.ToString()));
                Logging.LogActivity(null, ex.Message);
                return ex.Message;
            }

        }

        /// <summary>
        /// Refund the amount to user based on the given guid and amount
        /// </summary>
        /// <param name="token"></param>
        /// <param name="amount"></param>
        /// <returns></returns>
        [HttpGet]
        public string Refund(string token, decimal amount)
        {
            try
            {
                return gatewayConnector.GetRefundVoidResponse(token, amount);
            }
            catch (Exception ex)
            {
                Logging.LogMessage(ex.StackTrace);
                Logging.LogActivity(null, ex.Message);
                return ex.Message;
            }

        }

        /// <summary>
        /// Void the transaction for the given token
        /// </summary>
        /// <param name="token"></param>
        /// <param name="amount"></param>
        /// <returns></returns>
        [HttpGet]
        public string Void(string token)
        {
            try
            {
                return gatewayConnector.GetRefundVoidResponse(token, 0.00m, true);
            }
            catch (Exception ex)
            {
                Logging.LogMessage(string.Format("Exception Occurred :{0},{1}", ex.Message, ex.StackTrace.ToString()));
                Logging.LogActivity(null, ex.Message);
                return ex.Message;
            }

        }

        /// <summary>
        /// Add/Update payment settings
        /// </summary>
        /// <param name="paymentSetting"></param>
        /// <returns></returns>
        [HttpPost]
        public string ConfigurePaymentSettings([FromBody] ZNodePaymentSetting paymentSetting)
        {
            try
            {
                PaymentSettingsRepository repository = new PaymentSettingsRepository();

                var isExist = repository.GetPaymentSetting(paymentSetting.PaymentSettingID) != null;
                if (isExist)
                {
                    repository.UpdatePaymentSetting(paymentSetting);
                }
                else
                {
                    repository.AddPaymentSetting(paymentSetting);
                }
                return "Success";
            }
            catch (Exception ex)
            {
                Logging.LogMessage(string.Format("Exception Occurred :{0},{1}", ex.Message, ex.StackTrace.ToString()));
                Logging.LogActivity(null, ex.Message);
                return ex.Message;
            }
        }

        /// <summary>
        /// Delete payment settings by Payment settings ID
        /// </summary>
        /// <param name="paymentSettingId"></param>
        /// <returns></returns>
        [HttpGet]
        public string DeletePaymentSettings(int paymentSettingId)
        {
            try
            {
                PaymentSettingsRepository repository = new PaymentSettingsRepository();
                if (repository.DeletePaymentSetting(paymentSettingId))
                    return "Success";
            }
            catch (Exception ex)
            {
                Logging.LogMessage(string.Format("Exception Occurred :{0},{1}", ex.Message, ex.StackTrace.ToString()));
                Logging.LogActivity(null, ex.Message);
            }
            return "Delete Payment Settings failed.";
        }

        /// <summary>
        /// Get payment settings by Payment settings ID
        /// </summary>
        /// <param name="paymentSettingId">int paymentSettingId</param>
        /// <returns>returns complete details of payment setting</returns>
        [HttpGet]
        public string GetPaymentSettingDetails(int paymentSettingId)
        {
            try
            {
                PaymentSettingsRepository paymentrepository = new PaymentSettingsRepository();
                var paymentSetting = paymentrepository.GetPaymentSetting(paymentSettingId);
                if (!Equals(paymentrepository, null))
                {
                    return JsonConvert.SerializeObject(paymentSetting, Formatting.Indented, new JsonSerializerSettings { PreserveReferencesHandling = PreserveReferencesHandling.Objects });
                }
            }
            catch (Exception ex)
            {
                Logging.LogMessage(string.Format("Exception Occurred :{0},{1}", ex.Message, ex.StackTrace.ToString()));
                Logging.LogActivity(null, ex.Message);
            }
            return string.Empty;
        }

        /// <summary>
        /// Get all available gateway names
        /// </summary>
        /// <param name="subscriptionModel"></param>
        /// <returns></returns>
        public IEnumerable<ZNodeGateway> GetGateways()
        {
            try
            {
                GatewayRepository repository = new GatewayRepository();
                return repository.GetAll();
            }
            catch (Exception ex)
            {
                Logging.LogMessage(string.Format("Exception Occurred :{0},{1}", ex.Message, ex.StackTrace.ToString()));
                Logging.LogActivity(null, ex.Message);
                return null;
            }

        }

        /// <summary>
        /// To check Customer Profile Id is already Exist
        /// </summary>
        /// <param name="paymentModel">PaymentModel paymentModel</param>
        /// <returns>returns true/false </returns>
        private bool IsCustomerProfileIdExist(PaymentModel paymentModel)
        {
            if ((string.IsNullOrEmpty(paymentModel.CustomerProfileId)
                       || string.IsNullOrEmpty(paymentModel.CustomerPaymentProfileId))
                       && (!string.IsNullOrEmpty(paymentModel.GatewayType))
                       && (Equals(paymentModel.GatewayType.ToLower(), Convert.ToString(GatewayType.AUTHORIZENET).ToLower())
                       || Equals(paymentModel.GatewayType.ToLower(), Convert.ToString(GatewayType.STRIPE).ToLower())
                       || Equals(paymentModel.GatewayType.ToLower(), Convert.ToString(GatewayType.SECURENET).ToLower())
                       || Equals(paymentModel.GatewayType.ToLower(), Convert.ToString(GatewayType.CYBERSOURCE).ToLower())
                       || Equals(paymentModel.GatewayType.ToLower(), Convert.ToString(GatewayType.PAYMENTTECH).ToLower())
                       || Equals(paymentModel.GatewayType.ToLower(), Convert.ToString(GatewayType.PAYPAL).ToLower())))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}