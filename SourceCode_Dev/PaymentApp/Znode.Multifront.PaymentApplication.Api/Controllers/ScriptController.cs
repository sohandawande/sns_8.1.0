﻿using System;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Web.Http;
using System.Web.Mvc;
using System.Web;
using Znode.Multifront.PaymentApplication.Api.PaymentProviders;
using Znode.Multifront.PaymentApplication.Api.Repository;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Znode.Multifront.PaymentApplication.Api.Controllers
{
    /// <summary>
    /// This controller will only allow such methods which need not have to check for the 
    /// ZnodeDomain table and allow on that basis to access its members.
    /// </summary>
    public class ScriptController : ApiController
    {
        /// <summary>
        /// This method will generate the script in the client UI and this will then give the call
        /// to the Znode Payment Application
        /// </summary>
        /// <param name="gateway">string Payment Gateway name</param>
        /// <param name="profileId">nullable int profile id associated with the payment gateway name</param>
        /// <returns>Returns the script which will be then call the Payment application function(s)</returns>
        [System.Web.Http.HttpGet]
        public HttpResponseMessage ZnodeApiJs(string gateway, int? profileId, string twoCoUrl)
        {
            try
            {
                string tcoMode = "sandbox";
                if (ConfigurationManager.AppSettings["TwoCheckOutPaymentAccountType"] != null)
                {
                    tcoMode = ConfigurationManager.AppSettings["TwoCheckOutPaymentAccountType"].ToString();
                }

                if (!string.IsNullOrEmpty(gateway))
                {
                    var repository = new PaymentSettingsRepository();
                    ZNodePaymentSetting paymentSetting;
                    GatewayType gatewayType;
                    Enum.TryParse(gateway, out gatewayType);
                    int gatewaytypeId = Convert.ToInt16(Enum.Parse(gatewayType.GetType(), gateway.ToUpper()));

                    paymentSetting = repository.GetPaymentSettingByGateway(gatewaytypeId, profileId);
                    if (paymentSetting != null)
                    {
                        var script = @"var apiUrl = '" + Request.RequestUri.ToString().Replace(Request.RequestUri.PathAndQuery, "") + @"/payment/PayNow?callback=?" + @"';
                                        
                                        (function (d, s, id, st, r) {
                                            var js,
                                            stylesheet,
                                            fjs = d.getElementsByTagName(s)[0];

                                            if (d.getElementById(id)) { return; }

                                            js = d.createElement(s);
                                            js.id = id;
                                            js.src = 'https://www.2checkout.com/checkout/api/2co.min.js';                    

                                            fjs.parentNode.insertBefore(js, fjs);
                                        }(document, 'script', 'znodeapi-jssdk', 'link', 'stylesheet'));

                                        var postDataApi = function (pdata) {
					                        pdata['PaymentSettingId'] = " + paymentSetting.PaymentSettingID + @";
					
					                        $.ajax({
					                            type: 'post',
					                            url: apiUrl,
					                            data: pdata,
                                                beforeSend: function(xhrObj){
                                                    xhrObj.setRequestHeader('Authorization', 'Basic bG9jYWxob3N0OjMyODh8NkYzNTUwQzMtN0Q1RC00NkM4LTg4QUQtRTI1MUZCRDMwMUU0');
                                                },
                                                success: function (response) {
								                    successCalBack(response);
					                            },
					                            error: errorCallback
					                        });       
                                        };                             

                                        var errorCallback = function (data) {
                                            if (data.errorCode === 200) {
                                                tokenRequest();
                                            } else {
                                                var error_message = 'Error Code: ' + data.errorCode + '<br />Error Message: ' + data.errorMsg;
                                                successCalBack(error_message);
                                            }
                                        };

                                        var successCallback = function(data) {
                                            postedData['TwoCOToken'] = data.response.token.token;
                                            var tokenString = '" + twoCoUrl.Trim() + @"';
                                            tokenString = tokenString + '&token=' + data.response.token.token;
                                            postTokenToMF(tokenString);
                                        };

                                        var postTokenToMF = function(tokenUrl){
                                            var redirectUrl = tokenUrl;
                                            $.ajax({
					                            type: 'get',
					                            url: redirectUrl,
                                                datatype: 'json',
                                                success: function (response) {
                                                    var newLocation = response.Data;
                                                    if (newLocation != undefined && newLocation.toLowerCase().indexOf('missing card data') >= 0) {
                                                        $('#ajaxProcessPayment').html('');
                                                        $('#ccpopUp [data-payment=""number""]').val('');
                                                        $('#ccpopUp [data-payment=""cvc""]').val('');
                                                        $('#ccpopUp [data-payment=""exp-month""]').val('');
                                                        $('#ccpopUp [data-payment=""exp-year""]').val('');
                                                        $('#ajaxProcessPayment').dialog('close');
                                                        if ($('#ajaxProcessPaymentError').html() == undefined) {
                                                            $('body').append('<div id=""ajaxProcessPaymentError""><p id=""ajaxProcessPaymentErrorMsg"">Error occurred during processing an order. Order could not be placed as card data is missing.</p></div>');
                                                        } else {
                                                            $('#ajaxProcessPaymentError').html('Error occurred during processing an order. Order could not be placed as card data is missing.');
                                                        }
                                                    } else if (newLocation != undefined && newLocation.indexOf('Message=') >= 0) {
                                                        var message = newLocation.substr(newLocation.indexOf('=') + 1);
                                                        $('#ajaxProcessPayment').html('');
                                                        $('#ccpopUp [data-payment=""number""]').val('');
                                                        $('#ccpopUp [data-payment=""cvc""]').val('');
                                                        $('#ccpopUp [data-payment=""exp-month""]').val('');
                                                        $('#ccpopUp [data-payment=""exp-year""]').val('');
                                                        $('#ajaxProcessPayment').dialog('close');

                                                        if ($('#ajaxProcessPaymentError').html() == undefined) {
                                                            $('body').append('<div id=""ajaxProcessPaymentError""><p id=""ajaxProcessPaymentErrorMsg"">We were unable to process your credit card payment. <br /><br />Reason:<br />' + message + '<br /><br />If the problem persists, contact us to complete your order.</p></div>');
                                                        } else {
                                                            $('#ajaxProcessPaymentError').html('We were unable to process your credit card payment. <br /><br />Reason:<br />' + message + '<br /><br />If the problem persists, contact us to complete your order.');
                                                        }
                                                    } else if (newLocation != undefined) {
                                                        window.location = newLocation;
                                                    } else {
                                                        $('#ccpopUp .ErrorMessage').html('');
                                                        $('#ccpopUp [data-payment=""number""]').val('');
                                                        $('#ccpopUp [data-payment=""cvc""]').val('');
                                                        $('#ccpopUp [data-payment=""exp-month""]').val('');
                                                        $('#ccpopUp [data-payment=""exp-year""]').val('');
                                                        $('#ajaxProcessPayment').dialog('close');

                                                        if ($('#ajaxProcessPaymentError').html() == undefined) {
                                                            $('body').append('<div id=""ajaxProcessPaymentError""><p id=""ajaxProcessPaymentErrorMsg"">Error occurred during processing an order. Order could not be placed.</p></div>');
                                                        } else {
                                                            $('#ajaxProcessPaymentError').html('Error occurred during processing an order. Order could not be placed.');
                                                        }
                                                    }

                                                    $('#ajaxProcessPaymentError').dialog({
                                                        title: 'Payment Application',
                                                        resizable: false,
                                                        modal: true,
                                                        create: function () {
                                                            $(this).closest('.ui-dialog').addClass('ui-md-popup ui-popup-top');
                                                        },
                                                        buttons: {
                                                            Ok: function () {
                                                                $(this).dialog('close');
                                                            }
                                                        },
                                                        closeOnEscape: false,
                                                        open: function (event, ui) {
                                                            $('#ajaxProcessPaymentError').parent().find(' .ui-dialog-titlebar-close').hide();
                                                        }
                                                    });

                                                    return false;
					                            },
					                            error: function(response){
                                                }
					                        });  
                                        };

                                        var tokenRequest = function (cno, em, ey, cv, url) {
                                           
                                            var args = {
                                                sellerId: '" + paymentSetting.Vendor + @"',
                                                publishableKey: '" + paymentSetting.Partner + @"',
                                                ccNo: cno,
                                                cvv: cv,
                                                expMonth: em,
                                                expYear: ey
                                            };

                                            TCO.requestToken(successCallback, errorCallback, args);
                                        };

                                        var successCalBack;
                                        var postedData;
                                        function submitCard(data, callBack) {
                                            successCalBack = callBack;     
                                            postedData = data;              
                                            if (" + (paymentSetting.GatewayTypeID == 2).ToString().ToLower() + @") {
                                                TCO.loadPubKey('" + tcoMode + @"', function() {
                                                    tokenRequest(data.CardNumber, data.CardExpirationMonth, data.CardExpirationYear, data.CardSecurityCode, data.TwoCOUrl);
                                                });
                                            } else {
                                                postDataApi(data);
                                            }   
    
                                            return false;    
                                        }";

                        HttpResponseMessage response = Request.CreateResponse();
                        response.Content = new StringContent(script);
                        response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/javascript");
                        
                        return response;
                    }
                }
            }
            catch (Exception ex)
            {
                Logging.LogMessage(ex.Message);
            }
            return null;
        }
    }
}
