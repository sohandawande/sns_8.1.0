﻿using SecureNetRestApiSDK.Api.Controllers;
using SecureNetRestApiSDK.Api.Models;
using SecureNetRestApiSDK.Api.Requests;
using SecureNetRestApiSDK.Api.Responses;
using SNET.Core;
using System;
using Znode.Multifront.PaymentApplication.Models;

namespace Znode.Multifront.PaymentApplication.Api.PaymentProviders
{
    public class SecureNetCustomerProvider : IPaymentProviders
    {
        /// <summary>
        /// Validate the credit card transaction number and returns status
        /// </summary>
        /// <param name="paymentModel"></param>
        /// <returns></returns>
        public GatewayResponseModel ValidateCreditcard(PaymentModel paymentModel)
        {
            if (!string.IsNullOrEmpty(paymentModel.TransactionId))
            {
                return CaptureTransaction(paymentModel);
            }
            return CreateTransaction(paymentModel);
        }

        /// <summary>
        /// Refund payment
        /// </summary>
        /// <param name="paymentModel">PaymentModel paymentModel</param>
        /// <returns>returns Payment Gateway Response</returns>
        public GatewayResponseModel Refund(PaymentModel paymentModel)
        {
            var gatewayResponse = new GatewayResponseModel();
            var request = new RefundRequest
            {
                TransactionId = Convert.ToInt32(paymentModel.TransactionId),
                DeveloperApplication = new DeveloperApplication
                {
                    DeveloperId = Convert.ToInt32(System.Configuration.ConfigurationSettings.AppSettings["SecureNetDeveloperId"]),
                    Version = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["SecureNetVersion"])
                }
            };

            var apiContext = SetAPIDetails(paymentModel);
            var controller = new PaymentsController();
            var response = controller.ProcessRequest<RefundResponse>(apiContext, request);

            if (response.Success)
            {
                gatewayResponse.TransactionId = Convert.ToString(response.Transaction.TransactionId);
                gatewayResponse.PaymentStatus = ZnodePaymentStatus.CREDIT_REFUNDED;
            }

            gatewayResponse.GatewayResponseData = String.Format("code: {0}, msg: {1}", response.ResponseCode, response.Message);
            gatewayResponse.IsSuccess = response.Success;

            return gatewayResponse;
        }

        /// <summary>
        /// Void payment
        /// </summary>
        /// <param name="paymentModel">PaymentModel paymentModel</param>
        /// <returns>returns payment gatway response</returns>
        public GatewayResponseModel Void(PaymentModel paymentModel)
        {
            var gatewayResponse = new GatewayResponseModel();
            var request = new VoidRequest
            {
                TransactionId = Convert.ToInt32(paymentModel.TransactionId),
                DeveloperApplication = new DeveloperApplication
                {
                    DeveloperId = Convert.ToInt32(System.Configuration.ConfigurationSettings.AppSettings["SecureNetDeveloperId"]),
                    Version = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["SecureNetVersion"])
                }
            };

            var apiContext = SetAPIDetails(paymentModel);
            var controller = new PaymentsController();

            var response = controller.ProcessRequest<VoidResponse>(apiContext, request);

            if (response.Success)
            {
                gatewayResponse.GatewayResponseData = String.Format("code: {0}, msg: {1}", response.ResponseCode, response.Message);
                gatewayResponse.TransactionId = Convert.ToString(response.Transaction.TransactionId);
            }
            gatewayResponse.IsSuccess = response.Success;
            gatewayResponse.PaymentStatus = ZnodePaymentStatus.CREDIT_VOIDED;
            return gatewayResponse;
        }

        /// <summary>
        /// To subscript
        /// </summary>
        /// <param name="paymentModel"></param>
        /// <returns></returns>
        public GatewayResponseModel Subscription(Models.PaymentModel paymentModel)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Create a transaciton based on Customer Payment profile Id
        /// </summary>
        private GatewayResponseModel CreateTransaction(PaymentModel paymentModel)
        {
            var gatewayResponse = new GatewayResponseModel();

            if (string.IsNullOrEmpty(paymentModel.CustomerProfileId) && string.IsNullOrEmpty(paymentModel.CustomerPaymentProfileId))
            {
                return CreateCustomer(paymentModel);
            }

            var request = new ChargeRequest
            {
                Amount = Convert.ToDecimal(paymentModel.Total),
                PaymentVaultToken = new PaymentVaultToken
                {
                    CustomerId = paymentModel.CustomerProfileId,
                    PaymentMethodId = paymentModel.CustomerPaymentProfileId,
                    PaymentType = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["SecureNetPaymentType"])
                },
                DeveloperApplication = new DeveloperApplication
                {
                    DeveloperId = Convert.ToInt32(System.Configuration.ConfigurationSettings.AppSettings["SecureNetDeveloperId"]),
                    Version = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["SecureNetVersion"])
                },
                ExtendedInformation = new ExtendedInformation
                {
                    TypeOfGoods = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["SecureNetTypeOfGoods"])
                }               
            };

            var apiContext = SetAPIDetails(paymentModel);
            var controller = new CustomersController();

            var response = controller.ProcessRequest<ChargeResponse>(apiContext, request);

            if (response.Success)
            {
                gatewayResponse.TransactionId = Convert.ToString(response.Transaction.TransactionId);
                gatewayResponse.IsSuccess = true;
                gatewayResponse.GatewayResponseData = String.Format("code: {0}, msg: {1}", response.ResponseCode, response.Message);
                gatewayResponse.CustomerProfileId = paymentModel.CustomerProfileId;
                gatewayResponse.CustomerPaymentProfileId = paymentModel.CustomerPaymentProfileId;
                if (paymentModel.GatewayPreAuthorize && string.IsNullOrEmpty(paymentModel.GUID))
                {
                    gatewayResponse.PaymentStatus = ZnodePaymentStatus.CREDIT_AUTHORIZED;
                }
                else
                {
                    gatewayResponse.PaymentStatus = ZnodePaymentStatus.CREDIT_CAPTURED;
                }
            }
            else
            {
                gatewayResponse.IsSuccess = false;
                gatewayResponse.GatewayResponseData = String.Format("code: {0}, msg: {1}", response.ResponseCode, response.Message);
            }

            return gatewayResponse;
        }

        /// <summary>
        /// Capture payment by transaciton Id
        /// </summary>
        /// <param name="paymentModel"></param>
        /// <returns></returns>
        private GatewayResponseModel CaptureTransaction(PaymentModel paymentModel)
        {
            var gatewayResponse = new GatewayResponseModel();
            var request = new PriorAuthCaptureRequest
            {
                Amount = Convert.ToDecimal(paymentModel.Total),
                TransactionId = Convert.ToInt32(paymentModel.TransactionId),
                DeveloperApplication = new DeveloperApplication
                {
                    DeveloperId = Convert.ToInt32(System.Configuration.ConfigurationSettings.AppSettings["SecureNetDeveloperId"]),
                    Version = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["SecureNetVersion"])
                }
            };

            var apiContext = SetAPIDetails(paymentModel);
            var controller = new PaymentsController();
            var response = controller.ProcessRequest<PriorAuthCaptureResponse>(apiContext, request);

            if (response.Success)
            {
                return new GatewayResponseModel { IsSuccess = true, TransactionId = Convert.ToString(response.Transaction.TransactionId), PaymentStatus = ZnodePaymentStatus.CREDIT_CAPTURED };
            }
            else
            {
                return new GatewayResponseModel { IsSuccess = false, GatewayResponseData = response.Message };
            }
        }

        /// <summary>
        /// Create the Customer
        /// </summary>
        /// <param name="paymentModel">PaymentModel paymentModel</param>
        /// <returns>returns customer response</returns>
        private GatewayResponseModel GetCustomer(PaymentModel paymentModel)
        {
            var gatewayResponse = new GatewayResponseModel();
            var request = new CreateCustomerRequest
            {
                FirstName = paymentModel.BillingFirstName,
                LastName = paymentModel.BillingLastName,
                PhoneNumber = paymentModel.BillingPhoneNumber,
                EmailAddress = paymentModel.BillingEmailId,
                SendEmailReceipts = true,
                Notes = string.Empty,
                Address = new Address
                {
                    Line1 = paymentModel.BillingStreetAddress1,
                    City = paymentModel.BillingCity,
                    State = paymentModel.BillingStateCode,
                    Zip = paymentModel.BillingPostalCode,
                },
                DeveloperApplication = new DeveloperApplication
                {
                    DeveloperId = Convert.ToInt32(System.Configuration.ConfigurationSettings.AppSettings["SecureNetDeveloperId"]),
                    Version = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["SecureNetVersion"])
                }                
            };

            var apiContext = SetAPIDetails(paymentModel);
            var controller = new CustomersController();
            var response = controller.ProcessRequest<CreateCustomerResponse>(apiContext, request);

            gatewayResponse.IsSuccess = response.Success;
            gatewayResponse.CustomerProfileId = response.CustomerId;
            gatewayResponse.ResponseText = response.Message;
            return gatewayResponse;
        }

        /// <summary>
        /// Create the Customer Payment
        /// </summary>
        /// <param name="paymentModel"></param>
        /// <returns>returns customer payment response</returns>
        private GatewayResponseModel CreateCustomerPayment(PaymentModel paymentModel)
        {
            var gatewayResponse = new GatewayResponseModel();
            var request = new AddPaymentMethodRequest
            {
                CustomerId = paymentModel.CustomerProfileId,
                Card = new Card
                {
                    Number = paymentModel.CardNumber,
                    ExpirationDate = string.Concat(paymentModel.CardExpirationMonth, "/", paymentModel.CardExpirationYear),
                    Address = new Address
                    {
                        Line1 = paymentModel.BillingStreetAddress1,
                        City = paymentModel.BillingCity,
                        State = paymentModel.BillingStateCode,
                        Zip = paymentModel.BillingPostalCode
                    },
                    FirstName = paymentModel.BillingFirstName,
                    LastName = paymentModel.BillingLastName
                },
                Phone = paymentModel.BillingPhoneNumber,
                Notes = string.Empty,
                AccountDuplicateCheckIndicator = 0,
                Primary = true,
                DeveloperApplication = new DeveloperApplication
                {
                    DeveloperId = Convert.ToInt32(System.Configuration.ConfigurationSettings.AppSettings["SecureNetDeveloperId"]),
                    Version = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["SecureNetVersion"])
                }
            };

            var apiContext = SetAPIDetails(paymentModel);
            var controller = new CustomersController();
            var response = controller.ProcessRequest<AddPaymentMethodResponse>(apiContext, request);

            gatewayResponse.IsSuccess = response.Success;
            gatewayResponse.CustomerProfileId = paymentModel.CustomerProfileId;
            gatewayResponse.CustomerPaymentProfileId = response.VaultPaymentMethod.PaymentId;
            gatewayResponse.ResponseText = response.Message;
            return gatewayResponse;
        }

        /// <summary>
        /// This method will create customer profile 
        /// </summary>
        /// <param name="paymentModel">PaymentModel paymentModel</param>
        /// <returns>returns customer details response</returns>
        private GatewayResponseModel CreateCustomer(PaymentModel paymentModel)
        {
            var response = GetCustomer(paymentModel);
            if (!string.IsNullOrEmpty(response.CustomerProfileId))
            {
                paymentModel.CustomerProfileId = response.CustomerProfileId;
                response = CreateCustomerPayment(paymentModel);
            }
            return Equals(response, null) ? new GatewayResponseModel() : response;
        }

        /// <summary>
        /// To set Payment API details
        /// </summary>
        /// <param name="model">PaymentModel model</param>
        /// <returns>returns APIContext</returns>
        private APIContext SetAPIDetails(PaymentModel model)
        {
            APIContext apiContext = new APIContext();
            apiContext.SecureNetId = model.GatewayLoginName;
            apiContext.SecureKey = model.GatewayTransactionKey;
            apiContext.Mode = model.GatewayTestMode ? Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["SecureNetTestMode"]) : Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["SecureNetLiveMode"]);
            apiContext.Endpoint = model.GatewayTestMode ? Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["SecureNetSandboxEndpoint"]) : Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["SecureNetLiveEndpoint"]);
            apiContext.Timeout = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["SecureNetTimeout"]);
            apiContext.DeveloperId = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["SecureNetDeveloperId"]);
            apiContext.VersionId = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["SecureNetVersion"]);

            return apiContext;
        }
    }
}