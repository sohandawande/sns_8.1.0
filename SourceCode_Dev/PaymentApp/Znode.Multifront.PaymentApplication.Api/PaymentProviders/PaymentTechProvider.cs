﻿using Paymentech;
using System;
using System.Configuration;
using Znode.Multifront.PaymentApplication.Models;

namespace Znode.Multifront.PaymentApplication.Api.PaymentProviders
{
    public class PaymentTechProvider : IPaymentProviders
    {
        /// <summary>
        /// Validate the credit card and returns the transaction details.
        /// </summary>
        /// <param name="paymentModel"></param>
        /// <returns></returns>
        public GatewayResponseModel ValidateCreditcard(PaymentModel paymentModel)
        {
            try
            {
                if (string.IsNullOrEmpty(paymentModel.CustomerProfileId))
                {
                    return this.CreateCustomer(paymentModel);
                }
                return this.CreateTransaction(paymentModel);                
            }
            catch (Exception e)
            {
                return new GatewayResponseModel { IsSuccess = false, GatewayResponseData = e.Message };
            }
        }

        /// <summary>
        /// Refund the amount for the given transaction
        /// </summary>
        /// <param name="paymentModel"></param>
        /// <returns></returns>
        public GatewayResponseModel Refund(PaymentModel paymentModel)
        {
            var gatewayResponse = new GatewayResponseModel();

            try
            {
                Paymentech.Response response;
                var transaction = new Transaction(System.Web.HttpContext.Current.Server.MapPath(@"~\Paymentech\sdk\DotNet40\7.4.0\"), RequestType.NEW_ORDER_TRANSACTION);

                transaction["OrbitalConnectionUsername"] = paymentModel.GatewayLoginName;
                transaction["OrbitalConnectionPassword"] = paymentModel.GatewayLoginPassword;

                transaction["MerchantID"] = paymentModel.GatewayTransactionKey;
                transaction["MessageType"] = "R";
                transaction["BIN"] = Convert.ToString(ConfigurationManager.AppSettings["PaymentTechBIN"]);
                transaction["OrderID"] = paymentModel.OrderId;
                transaction["TxRefNum"] = paymentModel.TransactionId;
                transaction["IndustryType"] = "EC";

                response = transaction.Process();
                if (response["ProcStatus"].Equals("0"))
                {
                    gatewayResponse.IsSuccess = true;
                    gatewayResponse.TransactionId = response["TxRefNum"];
                    gatewayResponse.PaymentStatus = ZnodePaymentStatus.CREDIT_REFUNDED;
                }
                else
                {
                    gatewayResponse.IsSuccess = false;
                    gatewayResponse.GatewayResponseData = response["StatusMsg"];
                    gatewayResponse.ResponseCode = response["ProcStatus"];
                }

                return gatewayResponse;
            }
            catch (Exception e)
            {
                return new GatewayResponseModel { IsSuccess = false, GatewayResponseData = e.Message };
            }
        }

        /// <summary>
        /// Void the transaction for the given transaction details
        /// </summary>
        /// <param name="paymentModel"></param>
        /// <returns></returns>
        public GatewayResponseModel Void(PaymentModel paymentModel)
        {
            var gatewayResponse = new GatewayResponseModel();
            try
            {
                Paymentech.Response response;
                Transaction transaction = new Transaction(System.Web.HttpContext.Current.Server.MapPath(@"~\Paymentech\sdk\DotNet40\7.4.0\"), RequestType.REVERSE_TRANSACTION);
                transaction["OrbitalConnectionUsername"] = paymentModel.GatewayLoginName;
                transaction["OrbitalConnectionPassword"] = paymentModel.GatewayLoginPassword;

                transaction["MerchantID"] = paymentModel.GatewayTransactionKey;

                transaction["BIN"] = Convert.ToString(ConfigurationManager.AppSettings["PaymentTechBIN"]);
                transaction["OrderID"] = paymentModel.OrderId;
                transaction["TxRefNum"] = paymentModel.TransactionId;
                transaction["TxRefIdx"] = "0";

                response = transaction.Process();
                if (response["ProcStatus"].Equals("0"))
                {
                    gatewayResponse.IsSuccess = true;
                    gatewayResponse.PaymentStatus = ZnodePaymentStatus.CREDIT_VOIDED;
                }
                else
                {
                    gatewayResponse.IsSuccess = false;
                    gatewayResponse.GatewayResponseData = response["StatusMsg"];
                    gatewayResponse.ResponseCode = response["ProcStatus"];
                }

                return gatewayResponse;
            }
            catch (Exception e)
            {
                return new GatewayResponseModel { IsSuccess = false, GatewayResponseData = e.Message };
            }
        }

        /// <summary>
        /// Recurring subscription
        /// </summary>
        /// <param name="paymentModel"></param>
        /// <returns></returns>
        public GatewayResponseModel Subscription(PaymentModel paymentModel)
        {
            var gatewayResponse = new GatewayResponseModel();
            try
            {
                Paymentech.Response response;
                Transaction transaction = new Transaction(RequestType.NEW_ORDER_TRANSACTION);

                transaction["IndustryType"] = "RC";
                transaction["MessageType"] = "AC";

                transaction["OrbitalConnectionUsername"] = paymentModel.GatewayLoginName;
                transaction["OrbitalConnectionPassword"] = paymentModel.GatewayLoginPassword;

                transaction["MerchantID"] = paymentModel.GatewayTransactionKey;
                transaction["Amount"] = decimal.Round((paymentModel.Subscription.Amount * 100), 0).ToString();
                transaction["BIN"] = Convert.ToString(ConfigurationManager.AppSettings["PaymentTechBIN"]);
                transaction["OrderID"] = paymentModel.OrderId;
                transaction["TxRefNum"] = paymentModel.TransactionId;

                transaction["MessageType"] = "AC";

                transaction["CustomerProfileFromOrderInd"] = "S";
                transaction["CustomerRefNum"] = paymentModel.CustomerProfileId;
                transaction["CustomerProfileOrderOverrideInd"] = "NO";
                transaction["Status"] = "A";
                transaction["MBType"] = "R";
                transaction["MBOrderIdGenerationMethod"] = "DI";
                transaction["MBRecurringMaxBillings"] = paymentModel.Subscription.TotalCycles.ToString();
                transaction["MBRecurringFrequency"] = "1 * ?";
                transaction["Exp"] = paymentModel.CardExpirationMonth + paymentModel.CardExpirationYear.Substring(2);
                transaction["AccountNum"] = paymentModel.CardNumber;
                transaction["CardSecVal"] = paymentModel.CardSecurityCode;


                response = transaction.Process();
                if (response["ProcStatus"].Equals("0"))
                {
                    gatewayResponse.IsSuccess = true;
                    gatewayResponse.TransactionId = ((Paymentech.Response)(response)).TxRefNum;
                }
                else
                {
                    gatewayResponse.IsSuccess = false;
                    gatewayResponse.GatewayResponseData = response["StatusMsg"];
                    gatewayResponse.ResponseCode = response["ProcStatus"];
                }

                return gatewayResponse;
            }
            catch (Exception e)
            {
                return new GatewayResponseModel { IsSuccess = false, GatewayResponseData = e.Message };
            }
        }

        /// <summary>
        /// Create payment transaction based on created customer token.
        /// </summary>
        /// <param name="paymentModel">PaymentModel paymentModel</param>
        /// <returns>returns paymenttech response</returns>
        private GatewayResponseModel CreateTransaction(PaymentModel paymentModel)
        {
            try
            {
                var gatewayResponse = new GatewayResponseModel();
                // Declare a response
                Paymentech.Response response;
                // Create an authorize transaction
                Transaction transaction;


                if (string.IsNullOrEmpty(paymentModel.GUID) && paymentModel.GatewayPreAuthorize)
                {
                    transaction = new Transaction(System.Web.HttpContext.Current.Server.MapPath(@"~\Paymentech\sdk\DotNet40\7.4.0\"), RequestType.PC2_RECURRING_AUTH_CAPTURE);
                }
                else if (string.IsNullOrEmpty(paymentModel.GUID) && !paymentModel.GatewayPreAuthorize)
                {
                    transaction = new Transaction(System.Web.HttpContext.Current.Server.MapPath(@"~\Paymentech\sdk\DotNet40\7.4.0\"), RequestType.PC2_AUTH);
                }
                else
                {
                    transaction = new Transaction(RequestType.CC_CAPTURE);
                }

                // Populate the required fields for the given transaction type. You can use’
                // the Paymentech Transaction Appendix to help you populate the transaction’

                transaction["OrbitalConnectionUsername"] = paymentModel.GatewayLoginName;
                transaction["OrbitalConnectionPassword"] = paymentModel.GatewayLoginPassword;

                transaction["MerchantID"] = paymentModel.GatewayTransactionKey;
                transaction["BIN"] = Convert.ToString(ConfigurationManager.AppSettings["PaymentTechBIN"]);
                transaction["Amount"] = decimal.Round((decimal.Parse(paymentModel.Total) * 100), 0).ToString();
                transaction["CustomerRefNum"] = paymentModel.CustomerProfileId;
                if (string.IsNullOrEmpty(paymentModel.GUID))
                {
                    transaction["MessageType"] = paymentModel.GatewayPreAuthorize ? "A" : "AC";

                    Random rnd = new Random();
                    paymentModel.OrderId = rnd.Next(Int32.MaxValue).ToString();
                    transaction["OrderID"] = paymentModel.OrderId;                   
                }
                else
                {
                    transaction["OrderID"] = paymentModel.OrderId;
                    transaction["TxRefNum"] = paymentModel.TransactionId;
                }

                response = transaction.Process();

                if (response["ProcStatus"].Equals("0"))
                {
                    gatewayResponse.IsSuccess = response.Approved;
                    gatewayResponse.TransactionId = response["TxRefNum"];

                    if (paymentModel.GatewayPreAuthorize && string.IsNullOrEmpty(paymentModel.GUID))
                    {
                        gatewayResponse.PaymentStatus = ZnodePaymentStatus.CREDIT_AUTHORIZED;
                    }
                    else
                    {
                        gatewayResponse.PaymentStatus = ZnodePaymentStatus.CREDIT_CAPTURED;
                    }

                    gatewayResponse.CustomerProfileId = paymentModel.CustomerProfileId;
                    gatewayResponse.Token = response["OrderID"];
                    gatewayResponse.ResponseText = response.Message;
                }
                else
                {
                    gatewayResponse.IsSuccess = false;
                    gatewayResponse.GatewayResponseData = response["StatusMsg"];
                    gatewayResponse.ResponseCode = response["ProcStatus"];
                }

                return gatewayResponse;
            }
            catch (Exception e)
            {
                return new GatewayResponseModel { IsSuccess = false, GatewayResponseData = e.Message };
            }
        }

        /// <summary>
        /// To create customer profile for getting CustomerProfileId
        /// </summary>
        /// <param name="paymentModel">PaymentModel paymentModel</param>
        /// <returns>returns paymenttech response</returns>
        private GatewayResponseModel CreateCustomer(PaymentModel paymentModel)
        {
            try
            {
                var gatewayResponse = new GatewayResponseModel();
                // Declare a response
                Paymentech.Response response;
                // Create an authorize transaction
                Transaction transaction;

                transaction = new Transaction(System.Web.HttpContext.Current.Server.MapPath(@"~\Paymentech\sdk\DotNet40\7.4.0\"), RequestType.PROFILE_MANAGEMENT);

                // Populate the required fields for the given transaction type. You can use’

                transaction["OrbitalConnectionUsername"] = paymentModel.GatewayLoginName;
                transaction["OrbitalConnectionPassword"] = paymentModel.GatewayLoginPassword;

                transaction["MerchantID"] = paymentModel.GatewayTransactionKey;
                transaction["CustomerMerchantID"] = paymentModel.GatewayTransactionKey;
                transaction["CustomerBin"] = Convert.ToString(ConfigurationManager.AppSettings["PaymentTechBIN"]);
                transaction["CustomerProfileAction"] = "C";//"create"   => "C","retrieve" => "R","update"   => "U","delete"   => "D"

                Random rnd = new Random();
                string customerRefNo = rnd.Next(Int32.MaxValue).ToString();

                transaction["CustomerName"] = string.Concat(paymentModel.BillingFirstName, " ", paymentModel.BillingLastName);
                transaction["CustomerRefNum"] = customerRefNo;
                transaction["CustomerAddress1"] = paymentModel.BillingStreetAddress1;
                transaction["CustomerAddress2"] = string.Concat(paymentModel.BillingStreetAddress2, customerRefNo);
                transaction["CustomerCity"] = paymentModel.BillingCity;
                transaction["CustomerState"] = paymentModel.BillingStateCode;
                transaction["CustomerZIP"] = paymentModel.BillingPostalCode;
                transaction["CustomerEmail"] = paymentModel.BillingEmailId;
                transaction["CustomerPhone"] = paymentModel.BillingPhoneNumber;
                transaction["CustomerCountryCode"] = paymentModel.BillingCountryCode;

                transaction["CCAccountNum"] = paymentModel.CardNumber;
                transaction["CCExpireDate"] = paymentModel.CardExpirationMonth + paymentModel.CardExpirationYear.Substring(2);
                transaction["CustomerProfileOrderOverrideInd"] = "NO";
                transaction["CustomerProfileFromOrderInd"] = "A";
                transaction["CustomerAccountType"] = "CC";

                transaction["OrderDefaultAmount"] = paymentModel.Total;
                transaction["Status"] = "A";

                response = transaction.Process();

                if (response.Status.Equals(0))
                {
                    gatewayResponse.IsSuccess = true;
                    gatewayResponse.CustomerProfileId = response["CustomerRefNum"];
                    gatewayResponse.GatewayResponseData = response.Message;
                    gatewayResponse.ResponseText = response.Message;
                }
                else
                {
                    gatewayResponse.IsSuccess = false;
                    gatewayResponse.GatewayResponseData = response["StatusMsg"];
                    gatewayResponse.ResponseText = response.Message;
                }

                return gatewayResponse;
            }
            catch (Exception e)
            {
                return new GatewayResponseModel { IsSuccess = false, GatewayResponseData = e.Message };
            }
        }       
    }
}