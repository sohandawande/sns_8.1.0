﻿using AnetApi.Schema;
using System;
using System.Linq;
using Znode.Multifront.PaymentApplication.Api.AuthorizeNetAPI;
using Znode.Multifront.PaymentApplication.Models;

namespace Znode.Multifront.PaymentApplication.Api.PaymentProviders
{
    /// <summary>
    /// This class will have all the methods of implementation of Authorize .Net payment provider 
    /// </summary>
    public class AuthorizeNetCustomerProvider : IPaymentProviders
    {
        /// <summary>
        /// Validate the credit card transaction number and returns status
        /// </summary>
        /// <param name="paymentModel"></param>
        /// <returns></returns>
        public GatewayResponseModel ValidateCreditcard(PaymentModel paymentModel)
        {
            var response = new GatewayResponseModel();
            try
            {
                //If Customer Profile is already created then call Create Transaction to complete the payment process.
                if (!string.IsNullOrEmpty(paymentModel.CustomerProfileId) &&
                    !string.IsNullOrEmpty(paymentModel.CustomerPaymentProfileId))
                {
                    return CreateTransaction(paymentModel);
                }

                //To Create Customer Profile based on user input.
                response = CreateCustomer(paymentModel);
                return response;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.GatewayResponseData = ex.Message;
            }
            return response;
        }

        /// <summary>
        /// Create the customer profile
        /// </summary>
        /// <returns>The result of the operation</returns>
        private GatewayResponseModel CreateCustomerProfile(PaymentModel paymentModel)
        {
            var gatewayResponse = new GatewayResponseModel();
            var request = new createCustomerProfileRequest();
            XmlApiUtilities.PopulateMerchantAuthentication((ANetApiRequest)request, paymentModel.GatewayLoginName, paymentModel.GatewayTransactionKey);

            var customer = new customerProfileType();
            customer.email = paymentModel.BillingEmailId;
            customer.description = "New customer " + (DateTime.Now.Ticks.ToString());

            request.profile = customer;
            request.validationMode = paymentModel.GatewayTestMode ? validationModeEnum.testMode : validationModeEnum.liveMode;

            System.Xml.XmlDocument responseXml = null;
            bool bResult = XmlApiUtilities.PostRequest(request, out responseXml, paymentModel.GatewayTestMode);
            object response = null;
            createCustomerProfileResponse apiResponse = null;

            if (bResult) bResult = XmlApiUtilities.ProcessXmlResponse(responseXml, out response);
            if (!(response is createCustomerProfileResponse))
            {
                var errorResponse = (ANetApiResponse)response;
                gatewayResponse.IsSuccess = false;
                if (!Equals(errorResponse, null))
                {
                    gatewayResponse.GatewayResponseData = String.Format("code: {0}\n	  msg: {1}", errorResponse.messages.message[0].code, errorResponse.messages.message[0].text);
                    gatewayResponse.ResponseText = errorResponse.messages.message[0].text;
                }                
                return gatewayResponse;
            }

            if (bResult)
            {
                apiResponse = (createCustomerProfileResponse)response;
            }

            if (!Equals(apiResponse, null) && !Equals(apiResponse.customerProfileId, null))
            {
                gatewayResponse.CustomerProfileId = apiResponse.customerProfileId;
                gatewayResponse.IsSuccess = true;
            }
            else
            {
                gatewayResponse.IsSuccess = false;
                gatewayResponse.ResponseText = "Unable to create customer profile.";
            }

            return gatewayResponse;
        }

        /// <summary>
        /// This method will create the customer payment profile
        /// </summary>
        /// <param name="paymentModel">payment model</param>
        /// <returns></returns>
        private GatewayResponseModel CreateCustomerPaymentProfile(PaymentModel paymentModel)
        {
            var gatewayResponse = new GatewayResponseModel();
            var newPaymentProfile = new customerPaymentProfileType();
            var newPayment = new paymentType();

            //PRFT Custom Code to pass billing information :Start
            newPaymentProfile.billTo = new customerAddressType()
            {
                firstName = paymentModel.BillingFirstName,
                lastName = paymentModel.BillingLastName,
                address = string.Format("{0} {1}",paymentModel.BillingStreetAddress1,paymentModel.BillingStreetAddress2),
                city=paymentModel.BillingCity,
                state=paymentModel.BillingStateCode,
                zip=paymentModel.BillingPostalCode,
                country=paymentModel.BillingCountryCode,
                phoneNumber=paymentModel.BillingPhoneNumber,
                company=paymentModel.BillingCompanyName
            };
            //PRFT Custom Code to pass billing information :End
            var newCard = new creditCardType();
            newCard.cardNumber = paymentModel.CardNumber;
            newCard.expirationDate = paymentModel.CardExpiration;
            newPayment.Item = newCard;

            newPaymentProfile.payment = newPayment;

            var request = new createCustomerPaymentProfileRequest();
            XmlApiUtilities.PopulateMerchantAuthentication((ANetApiRequest)request, paymentModel.GatewayLoginName, paymentModel.GatewayTransactionKey);

            request.customerProfileId = paymentModel.CustomerProfileId;
            request.paymentProfile = newPaymentProfile;
            request.validationMode = paymentModel.GatewayTestMode ? validationModeEnum.testMode : validationModeEnum.liveMode;

            System.Xml.XmlDocument responseXml = null;
            bool bResult = XmlApiUtilities.PostRequest(request, out responseXml, paymentModel.GatewayTestMode);
            object response = null;
            createCustomerPaymentProfileResponse apiResponse = null;

            if (bResult) bResult = XmlApiUtilities.ProcessXmlResponse(responseXml, out response);
            if (!(response is createCustomerPaymentProfileResponse))
            {
                var errorResponse = (ANetApiResponse)response;
                gatewayResponse.GatewayResponseData = String.Format(
                    "code: {0}\n	  msg: {1}", errorResponse.messages.message[0].code,
                    errorResponse.messages.message[0].text);
                gatewayResponse.IsSuccess = false;
                gatewayResponse.ResponseText = errorResponse.messages.message[0].text;
                return gatewayResponse;
            }
            if (bResult) apiResponse = (createCustomerPaymentProfileResponse)response;
            if (!Equals(apiResponse, null))
            {
                gatewayResponse.IsSuccess = true;
                gatewayResponse.CustomerProfileId = paymentModel.CustomerProfileId;
                gatewayResponse.CustomerPaymentProfileId = apiResponse.customerPaymentProfileId;
            }

            return gatewayResponse;
        }

        /// <summary>
        /// Create a transaciton based on Customer Payment profile Id
        /// </summary>
        /// <param name="paymentModel">payment model</param>
        /// <returns>retunrs the create transaction response.</returns>
        private GatewayResponseModel CreateTransaction(PaymentModel paymentModel)
        {
            var gatewayResponse = new GatewayResponseModel();
            var request = new createCustomerProfileTransactionRequest();
            XmlApiUtilities.PopulateMerchantAuthentication((ANetApiRequest)request, paymentModel.GatewayLoginName, paymentModel.GatewayTransactionKey);

            var newTrans = new profileTransactionType();
            
            if (paymentModel.GatewayPreAuthorize && string.IsNullOrEmpty(paymentModel.GUID))
            {
                var newItem = new profileTransAuthOnlyType
                     {
                         customerProfileId = paymentModel.CustomerProfileId,
                         customerPaymentProfileId = paymentModel.CustomerPaymentProfileId,
                         amount = decimal.Parse(paymentModel.Total)
                     };
                newTrans.Item = newItem;
            }
            else if (paymentModel.GatewayPreAuthorize && !string.IsNullOrEmpty(paymentModel.GUID))
            {
                var newItem = new profileTransPriorAuthCaptureType
                {
                    transId = paymentModel.TransactionId,
                    customerProfileId = paymentModel.CustomerProfileId,
                    customerPaymentProfileId = paymentModel.CustomerPaymentProfileId,
                    amount = decimal.Parse(paymentModel.Total)
                };
                newTrans.Item = newItem;
            }
            else
            {
                var newItem = new profileTransAuthCaptureType
                {
                    customerProfileId = paymentModel.CustomerProfileId,
                    customerPaymentProfileId = paymentModel.CustomerPaymentProfileId,
                    amount = decimal.Parse(paymentModel.Total)
                };
                newTrans.Item = newItem;
            }

            request.transaction = newTrans;

            System.Xml.XmlDocument response_xml = null;
            bool bResult = XmlApiUtilities.PostRequest(request, out response_xml, paymentModel.GatewayTestMode);
            object response = null;
            createCustomerProfileTransactionResponse api_response = null;

            if (bResult) bResult = XmlApiUtilities.ProcessXmlResponse(response_xml, out response);
            if (!(response is createCustomerProfileTransactionResponse))
            {
                var errorResponse = (ANetApiResponse)response;
                if (!Equals(errorResponse, null))
                {
                    gatewayResponse.GatewayResponseData = String.Format("code: {0}, msg: {1}", errorResponse.messages.message[0].code, errorResponse.messages.message[0].text);
                }
                gatewayResponse.IsSuccess = false;
                return gatewayResponse;
            }
            if (bResult) api_response = (createCustomerProfileTransactionResponse)response;
            if (!Equals(api_response, null) && api_response.messages.resultCode.Equals(messageTypeEnum.Ok))
            {
                gatewayResponse.GatewayResponseData = String.Format("code: {0}, msg: {1}", api_response.messages.message[0].code, api_response.messages.message[0].text);

                var responseValues = api_response.directResponse.Split(',');
                if (responseValues.Count() > 6 && !responseValues[6].Equals("0"))
                {
                    gatewayResponse.TransactionId = responseValues[6];
                    gatewayResponse.IsSuccess = true;
                    gatewayResponse.CustomerProfileId = paymentModel.CustomerProfileId;
                    gatewayResponse.CustomerPaymentProfileId = paymentModel.CustomerPaymentProfileId;
                    gatewayResponse.PaymentStatus = (paymentModel.GatewayPreAuthorize && string.IsNullOrEmpty(paymentModel.GUID)) ? ZnodePaymentStatus.CREDIT_AUTHORIZED : ZnodePaymentStatus.CREDIT_CAPTURED;
                }
                else
                {
                    gatewayResponse.IsSuccess = false;
                    gatewayResponse.GatewayResponseData = String.Format("code: {0}, msg: {1}", api_response.messages.message[0].code, api_response.messages.message[0].text);
                }
            }
            return gatewayResponse;
        }

        /// <summary>
        /// This method will call the refund payment of Authorize .Net payment provider
        /// </summary>
        /// <param name="paymentModel">payment model</param>
        /// <returns>retunrs the refund response</returns>
        public GatewayResponseModel Refund(PaymentModel paymentModel)
        {
            var req = new createCustomerProfileTransactionRequest();
            var gatewayResponse = new GatewayResponseModel();
            var trans = new profileTransRefundType();
            trans.amount = decimal.Parse(paymentModel.Total);
            trans.customerProfileId = paymentModel.CustomerProfileId;
            trans.customerPaymentProfileId = paymentModel.CustomerPaymentProfileId;
            trans.transId = paymentModel.TransactionId;

            req.transaction = new profileTransactionType();
            req.transaction.Item = trans;
            XmlApiUtilities.PopulateMerchantAuthentication((ANetApiRequest)req, paymentModel.GatewayLoginName, paymentModel.GatewayTransactionKey);

            System.Xml.XmlDocument responseXml = null;
            bool bResult = XmlApiUtilities.PostRequest(req, out responseXml, paymentModel.GatewayTestMode);
            object response = null;
            createCustomerProfileTransactionResponse api_response = null;

            if (bResult) bResult = XmlApiUtilities.ProcessXmlResponse(responseXml, out response);
            if (!(response is createCustomerProfileTransactionResponse))
            {
                var errorResponse = (ANetApiResponse)response;
                if (!Equals(errorResponse, null))
                {
                    gatewayResponse.GatewayResponseData = String.Format("code: {0}, msg: {1}", errorResponse.messages.message[0].code, errorResponse.messages.message[0].text);
                }
                gatewayResponse.IsSuccess = false;
                return gatewayResponse;
            }
            if (bResult) api_response = (createCustomerProfileTransactionResponse)response;
            if (!Equals(api_response, null))
            {
                gatewayResponse.GatewayResponseData = String.Format("code: {0}, msg: {1}", api_response.messages.message[0].code, api_response.messages.message[0].text);

                var responseValues = api_response.directResponse.Split(',');
                if (responseValues.Count() > 6 && api_response.messages.resultCode.Equals(messageTypeEnum.Ok))
                {
                    gatewayResponse.TransactionId = responseValues[6];
                    gatewayResponse.IsSuccess = true;
                    gatewayResponse.PaymentStatus = ZnodePaymentStatus.CREDIT_REFUNDED;
                }
            }
            return gatewayResponse;
        }

        /// <summary>
        /// This method will call the void payment of Authorize .Net payment provider
        /// </summary>
        /// <param name="paymentModel">payment model</param>
        /// <returns>retunrs the void response</returns>
        public GatewayResponseModel Void(PaymentModel paymentModel)
        {
            var req = new createCustomerProfileTransactionRequest();
            var gatewayResponse = new GatewayResponseModel();
            var trans = new profileTransVoidType();
            trans.customerProfileId = paymentModel.CustomerProfileId;
            trans.customerPaymentProfileId = paymentModel.CustomerPaymentProfileId;
            trans.transId = paymentModel.TransactionId;

            req.transaction = new profileTransactionType();
            req.transaction.Item = trans;
            XmlApiUtilities.PopulateMerchantAuthentication((ANetApiRequest)req, paymentModel.GatewayLoginName, paymentModel.GatewayTransactionKey);

            System.Xml.XmlDocument responseXml = null;
            bool bResult = XmlApiUtilities.PostRequest(req, out responseXml, paymentModel.GatewayTestMode);
            object response = null;
            createCustomerProfileTransactionResponse api_response = null;

            if (bResult) bResult = XmlApiUtilities.ProcessXmlResponse(responseXml, out response);
            if (!(response is createCustomerProfileTransactionResponse))
            {
                var errorResponse = (ANetApiResponse)response;
                if (!Equals(errorResponse, null))
                {
                    gatewayResponse.GatewayResponseData = String.Format("code: {0}, msg: {1}", errorResponse.messages.message[0].code, errorResponse.messages.message[0].text);
                }
                gatewayResponse.IsSuccess = false;
                return gatewayResponse;
            }
            if (bResult) api_response = (createCustomerProfileTransactionResponse)response;
            if (!Equals(api_response, null))
            {
                gatewayResponse.GatewayResponseData = String.Format("code: {0}, msg: {1}", api_response.messages.message[0].code, api_response.messages.message[0].text);
                var responseValues = api_response.directResponse.Split(',');
                if (responseValues.Count() > 6 && !responseValues[6].Equals("0"))
                {
                    gatewayResponse.TransactionId = responseValues[6];
                    gatewayResponse.IsSuccess = true;
                    gatewayResponse.PaymentStatus = ZnodePaymentStatus.CREDIT_VOIDED;
                }
            }
            return gatewayResponse;
        }

        /// <summary>
        /// This method will call the subscription
        /// </summary>
        /// <param name="paymentModel">PaymentModel model</param>
        /// <returns>Response from the payment provider</returns>
        public GatewayResponseModel Subscription(PaymentModel paymentModel)
        {
            var gatewayResponse = new GatewayResponseModel();
            var request = new ARBCreateSubscriptionRequest();

            XmlApiUtilities.PopulateMerchantAuthentication((ANetApiRequest)request, paymentModel.GatewayLoginName, paymentModel.GatewayTransactionKey);

            ARBSubscriptionType sub = new ARBSubscriptionType();
            sub.name = paymentModel.Subscription.ProfileName;

            sub.payment = new paymentType();
            sub.payment.Item = new creditCardType { cardNumber = paymentModel.CardNumber, expirationDate = paymentModel.CardExpirationYear + "-" + paymentModel.CardExpirationMonth };

            // subscription payment schedule
            sub.paymentSchedule = new paymentScheduleType();
            sub.paymentSchedule.startDate = System.DateTime.Today;
            sub.paymentSchedule.startDateSpecified = true;

            // Disable trial
            sub.paymentSchedule.trialOccurrences = 0;
            sub.paymentSchedule.trialOccurrencesSpecified = true;
            sub.trialAmount = 0.00M;
            sub.trialAmountSpecified = true;

            sub.billTo = new nameAndAddressType { firstName = paymentModel.BillingFirstName, lastName = paymentModel.BillingLastName };

            if (paymentModel.Subscription.TotalCycles.Equals(0))
            {
                sub.paymentSchedule.totalOccurrences = 9999;
            }
            else
            {
                sub.paymentSchedule.totalOccurrences = short.Parse(paymentModel.Subscription.TotalCycles.ToString());
            }

            sub.paymentSchedule.totalOccurrencesSpecified = true;

            sub.amount = paymentModel.Subscription.Amount;
            sub.amountSpecified = true;

            int frequencyLength = 0;
            sub.paymentSchedule.interval = new paymentScheduleTypeInterval();
            sub.paymentSchedule.interval.unit = this.BillingPeriod(paymentModel.Subscription.Period, paymentModel.Subscription.Frequency, out frequencyLength);
            sub.paymentSchedule.interval.length = (short)frequencyLength;

            sub.order = new orderType();
            sub.order.invoiceNumber = paymentModel.Subscription.InvoiceNo;

            request.subscription = sub;
            request.refId = paymentModel.TransactionId;

            System.Xml.XmlDocument responseXml = null;
            bool bResult = XmlApiUtilities.PostRequest(request, out responseXml, paymentModel.GatewayTestMode);
            object response = null;
            ARBCreateSubscriptionResponse api_response = null;

            if (bResult) bResult = XmlApiUtilities.ProcessXmlResponse(responseXml, out response);

            if (bResult) api_response = (ARBCreateSubscriptionResponse)response;
            if (!Equals(api_response, null) && api_response.messages.resultCode.Equals(messageTypeEnum.Ok))
            {
                gatewayResponse.GatewayResponseData = String.Format("code: {0}, msg: {1}", api_response.messages.message[0].code, api_response.messages.message[0].text);
                if (!string.IsNullOrEmpty(api_response.subscriptionId))
                {
                    gatewayResponse.TransactionId = api_response.subscriptionId;
                    gatewayResponse.IsSuccess = true;
                }
            }
            else
            {
                gatewayResponse.GatewayResponseData = String.Format("code: {0}, msg: {1}", api_response.messages.message[0].code, api_response.messages.message[0].text);
                gatewayResponse.IsSuccess = false;
            }
            return gatewayResponse;

        }
        /// <summary>
        /// Calculate the billing period
        /// </summary>
        /// <param name="Unit">Unit for the billing</param>
        /// <param name="Frequency">Frequency for the billing</param>
        /// <param name="NewFrequency">New Frequency for the billing</param>
        /// <returns>Returns the ARB Subscription Unit</returns>
        protected ARBSubscriptionUnitEnum BillingPeriod(string Unit, string Frequency, out int NewFrequency)
        {
            int freqValue = int.Parse(Frequency);
            NewFrequency = freqValue;

            if (Unit.Equals("DAY", StringComparison.OrdinalIgnoreCase))
            {
                return ARBSubscriptionUnitEnum.days;
            }
            else if (Unit.Equals("WEEK", StringComparison.OrdinalIgnoreCase))
            {
                NewFrequency = freqValue * 7; // Change it to days.
                return ARBSubscriptionUnitEnum.days;
            }
            else if (Unit.Equals("MONTH", StringComparison.OrdinalIgnoreCase))
            {
                return ARBSubscriptionUnitEnum.months;
            }
            else if (Unit.Equals("YEAR", StringComparison.OrdinalIgnoreCase))
            {
                NewFrequency = freqValue * 12; // Change this value into months
                return ARBSubscriptionUnitEnum.months;
            }
            return ARBSubscriptionUnitEnum.days;
        }

        /// <summary>
        /// Get the customer profile
        /// </summary>
        /// <param name="profile_id">The ID of the customer that we are getting the profile for</param>
        /// <returns>The profile that was returned</returns>
        private customerProfileMaskedType GetCustomerProfile(PaymentModel paymentModel)
        {
            customerProfileMaskedType out_profile = null;

            getCustomerProfileRequest request = new getCustomerProfileRequest();
            XmlApiUtilities.PopulateMerchantAuthentication((ANetApiRequest)request, paymentModel.GatewayLoginName, paymentModel.GatewayTransactionKey);

            request.customerProfileId = paymentModel.CustomerProfileId;

            System.Xml.XmlDocument response_xml = null;
            bool bResult = XmlApiUtilities.PostRequest(request, out response_xml, paymentModel.GatewayTestMode);
            object response = null;
            getCustomerProfileResponse api_response = null;

            if (bResult) bResult = XmlApiUtilities.ProcessXmlResponse(response_xml, out response);
            if (!(response is getCustomerProfileResponse))
            {
                ANetApiResponse ErrorResponse = (ANetApiResponse)response;
                return out_profile;
            }
            if (bResult) api_response = (getCustomerProfileResponse)response;
            if (!Equals(api_response, null))
            {
                out_profile = api_response.profile;
            }

            return out_profile;
        }

        /// <summary>
        /// This method will create customer profile 
        /// </summary>
        /// <param name="paymentModel">PaymentModel paymentModel</param>
        /// <returns>returns customer details response</returns>
        private GatewayResponseModel CreateCustomer(PaymentModel paymentModel)
        {
            GatewayResponseModel response = CreateCustomerProfile(paymentModel);
            if (!string.IsNullOrEmpty(response.CustomerProfileId))
            {
                paymentModel.CustomerProfileId = response.CustomerProfileId;
                response = CreateCustomerPaymentProfile(paymentModel);               
            }
            return response;
        }
    }
}