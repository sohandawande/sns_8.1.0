﻿using System.Runtime.Serialization;
using Znode.Multifront.PaymentApplication.Models;

namespace Znode.Multifront.PaymentApplication.Api.PaymentProviders
{
    /// <summary>
    /// 
    /// </summary>
    public interface IPaymentProviders
    {
        GatewayResponseModel ValidateCreditcard(PaymentModel paymentModel);
        GatewayResponseModel Refund(PaymentModel paymentModel);
        GatewayResponseModel Void(PaymentModel paymentModel);
        GatewayResponseModel Subscription(PaymentModel paymentModel);
    }

    /// <summary>
    /// 
    /// </summary>
    public enum GatewayType
    {
        PAYPALEXPRESS = 0,
        AUTHORIZENET = 1,
        TWOCHECKOUT = 2,
        CYBERSOURCE = 3,
        STRIPE = 4,
        PAYPAL = 5,
        PAYMENTTECH = 6,
        WORLDPAY = 8,
        SECURENET = 9
    }
}
