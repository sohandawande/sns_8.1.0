﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Multifront.PaymentApplication.Api.PaymentProviders
{
    public class PaypalExpressCheckoutProvider : IPaymentProviders
    {
        public Models.GatewayResponseModel ValidateCreditcard(Models.PaymentModel paymentModel)
        {
            var paypal = new ZNode.Libraries.Paypal.PaypalGateway(paymentModel);

            var PaypalPaymentResponse = paypal.DoExpressCheckoutPayment(paymentModel.GatewayToken, paymentModel.CardDataToken);

            return PaypalPaymentResponse;
        }

        public Models.GatewayResponseModel Refund(Models.PaymentModel paymentModel)
        {
            var paypal = new ZNode.Libraries.Paypal.PaypalGateway(paymentModel);
            return paypal.Refund(paymentModel);
        }

        public Models.GatewayResponseModel Void(Models.PaymentModel paymentModel)
        {
            var paypal = new ZNode.Libraries.Paypal.PaypalGateway(paymentModel);
            return paypal.Void(paymentModel);
        }

        public Models.GatewayResponseModel Subscription(Models.PaymentModel paymentModel)
        {
            throw new NotImplementedException();
        }
    }
}