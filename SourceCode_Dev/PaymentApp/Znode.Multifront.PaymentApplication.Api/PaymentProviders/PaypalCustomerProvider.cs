﻿using System;
using Znode.Multifront.PaymentApplication.Api.Repository;
using Znode.Multifront.PaymentApplication.Models;
using ZNode.Libraries.PaypalRest;

namespace Znode.Multifront.PaymentApplication.Api.PaymentProviders
{
    public class PaypalCustomerProvider : IPaymentProviders
    {
        /// <summary>
        /// Validate the credit card based on the input
        /// </summary>
        /// <param name="paymentModel"></param>
        /// <returns></returns>
        public GatewayResponseModel ValidateCreditcard(PaymentModel paymentModel)
        {
            var paypal = new PaypalRestApi();

            // If CustomerProfileId is present then it will process the capture payment
            if (!string.IsNullOrEmpty(paymentModel.CustomerProfileId))
            {
                paymentModel.GatewayCurrencyCode = Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["CurrencyCode"]);
                return paypal.AuthorizePayment(paymentModel);
            }
            else
            {
                return paypal.CreateCustomerInPaypal(paymentModel);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="paymentModel"></param>
        /// <returns></returns>
        public GatewayResponseModel Refund(PaymentModel paymentModel)
        {
            var paypal = new PaypalRestApi();
            return paypal.Refund(paymentModel);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="paymentModel"></param>
        /// <returns></returns>
        public GatewayResponseModel Void(PaymentModel paymentModel)
        {
            var paypal = new PaypalRestApi();
            return paypal.VoidPayment(paymentModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="paymentModel"></param>
        /// <returns></returns>
        public GatewayResponseModel Subscription(PaymentModel paymentModel)
        {
            try
            {
                var paypal = new PaypalRestApi();
                return paypal.CreateSubscription(paymentModel);
            }
            catch (Exception ex)
            {
                Logging.LogActivity(paymentModel.PaymentSettingId, ex.Message, paymentModel.GUID);
                Logging.LogMessage(ex.Message + ex.StackTrace);
                return new GatewayResponseModel { IsSuccess = false, GatewayResponseData = "An error occured" };
            }
        }
    }
}