﻿using CyberSource;
using CyberSource.com.cybersource.api;
using CyberSource.Fluent;
using CyberSource.Tasks;
using CyberSource.Utils;
using System;
using Znode.Multifront.PaymentApplication.Api.Repository;
using Znode.Multifront.PaymentApplication.Models;

namespace Znode.Multifront.PaymentApplication.Api.PaymentProviders
{
    /// <summary>
    /// This class will have all the methods of implementation of Cybersource payment provider 
    /// </summary>
    public class CyberSourceCustomerProviderProvider : IPaymentProviders
    {
        /// <summary>
        /// Validate the credit card through Cyber source
        /// </summary>
        /// <param name="paymentModel"></param>
        /// <returns></returns>
        public GatewayResponseModel ValidateCreditcard(PaymentModel paymentModel)
        {
            if (!string.IsNullOrEmpty(paymentModel.CustomerProfileId))
            {
                if (paymentModel.GatewayPreAuthorize && !string.IsNullOrEmpty(paymentModel.GUID))
                {
                    return CaptureTransaction(paymentModel);
                }
                else
                {
                    return CreateTransaction(paymentModel);
                }
            }
            return CreateCustomer(paymentModel);
        }

        /// <summary>
        /// Create payment transaction based on created customer token.
        /// </summary>
        /// <param name="paymentModel"></param>
        /// <returns></returns>
        private GatewayResponseModel CreateTransaction(PaymentModel paymentModel)
        {
            var paymentGatewayResponse = new GatewayResponseModel();

            var address = new BillTo
            {
                firstName = paymentModel.BillingFirstName,
                lastName = paymentModel.BillingLastName,
                email = paymentModel.BillingEmailId,
                street1 = paymentModel.BillingStreetAddress1,
                street2 = paymentModel.BillingStreetAddress2,
                city = paymentModel.BillingCity,
                state = paymentModel.BillingStateCode,
                postalCode = paymentModel.BillingPostalCode,
                country = paymentModel.BillingCountryCode
            };

            RecurringSubscriptionInfo rsi = new RecurringSubscriptionInfo();
            rsi.subscriptionID = paymentModel.CustomerProfileId;

            try
            {
                ICybsClientFactory factory = new DefautClientFactory();

                CybsClient client = factory.Create(CybsEnvironment.UseConfiguration);
                string merchantReferenceCode = RandomDataUtil.RandomId();
                CreditCardPaymentTaskReply reply;
                if (paymentModel.GatewayPreAuthorize)
                {
                    reply = client.AuthorizeCreditCard(merchantReferenceCode,
                                                                              req =>
                                                                              {
                                                                                  req.WithBillTo(address);
                                                                                  req.WithPurchaseTotals(CybsCurrency.USD, decimal.Parse(paymentModel.Total));
                                                                                  req.WithRecurringSubscriptionInfo(rsi);
                                                                              });

                    if (reply.ReasonCode.Equals("100"))
                        paymentGatewayResponse.TransactionId = reply.AuthorizationReply.reconciliationID;
                }
                else
                {
                    reply = client.CaptureCreditCard(merchantReferenceCode,
                                                                             req =>
                                                                             {
                                                                                 req.WithBillTo(address);
                                                                                 req.WithPurchaseTotals(CybsCurrency.USD, decimal.Parse(paymentModel.Total));
                                                                                 req.WithRecurringSubscriptionInfo(rsi);
                                                                             });
                    if (reply.ReasonCode.Equals("100"))
                    {
                        paymentGatewayResponse.TransactionId = reply.CaptureReply.reconciliationID;
                    }
                }
                paymentGatewayResponse.ResponseCode = reply.ReasonCode;
                paymentGatewayResponse.GatewayResponseData = this.GetReasoncodeDescription(reply.ReasonCode);
                paymentGatewayResponse.CustomerProfileId = reply.RequestId;
                paymentGatewayResponse.CustomerPaymentProfileId = reply.MerchantReferenceCode;
                paymentGatewayResponse.Token = reply.RequestToken;

                if (reply.ReasonCode.Equals("100"))
                {
                    paymentGatewayResponse.IsSuccess = true;

                    paymentGatewayResponse.PaymentStatus = (paymentModel.GatewayPreAuthorize && string.IsNullOrEmpty(paymentModel.GUID)) ? ZnodePaymentStatus.CREDIT_AUTHORIZED : ZnodePaymentStatus.CREDIT_CAPTURED;
                }
            }
            catch (Exception e)
            {
                paymentGatewayResponse.GatewayResponseData = this.GetReasoncodeDescription(string.Empty);
                Logging.LogActivity(paymentModel.PaymentSettingId, e.Message);
            }

            return paymentGatewayResponse;
        }

        /// <summary>
        /// This method will return the Credit Card Type number
        /// </summary>
        /// <param name="cardType"></param>
        /// <returns>returns the Credit Card Type number</returns>
        private string GetCardType(string cardType)
        {
            var ccType = "000";
            switch (cardType.ToLower())
            {
                case "amex":
                case "american express":
                    ccType = "003";//"Amex";
                    break;
                case "visa":
                    ccType = "001";//"Visa";
                    break;
                case "mastercard":
                case "master card":
                    ccType = "002";//"MasterCard";
                    break;
                case "discover":
                    ccType = "004";//"Discover";
                    break;
            }
            return ccType;
        }

        /// <summary>
        /// Create customer by using credit card details.
        /// </summary>
        /// <param name="paymentModel"></param>
        /// <returns></returns>
        private GatewayResponseModel CreateCustomer(PaymentModel paymentModel)
        {
            var paymentGatewayResponse = new GatewayResponseModel();

            var card = new Card
            {
                accountNumber = paymentModel.CardNumber,
                expirationMonth = paymentModel.CardExpirationMonth,
                expirationYear = paymentModel.CardExpirationYear,
                cvNumber = paymentModel.CardSecurityCode,
                cardType = GetCardType(paymentModel.CardType)
            };

            var address = new BillTo
            {
                firstName = paymentModel.BillingFirstName,
                lastName = paymentModel.BillingLastName,
                email = paymentModel.BillingEmailId,
                street1 = paymentModel.BillingStreetAddress1,
                street2 = paymentModel.BillingStreetAddress2,
                city = paymentModel.BillingCity,
                state = paymentModel.BillingStateCode,
                postalCode = paymentModel.BillingPostalCode,
                country = paymentModel.BillingCountryCode
            };

            try
            {
                ICybsClientFactory factory = new DefautClientFactory();

                CybsClient client = factory.Create(CybsEnvironment.UseConfiguration);
                string merchantReferenceCode = RandomDataUtil.RandomId();

                var reply = client.CreatePaySubscription(merchantReferenceCode, CybsCurrency.USD,
                                                                               req =>
                                                                               {
                                                                                   req.WithCard(card);
                                                                                   req.WithBillTo(address);
                                                                               });
                paymentGatewayResponse.ResponseCode = reply.ReasonCode;
                paymentGatewayResponse.GatewayResponseData = this.GetReasoncodeDescription(reply.ReasonCode);
                paymentGatewayResponse.CustomerProfileId = reply.RequestId;
                paymentGatewayResponse.CustomerPaymentProfileId = reply.MerchantReferenceCode;
                paymentGatewayResponse.Token = reply.RequestToken;

                if (reply.ReasonCode.Equals("100"))
                {
                    paymentGatewayResponse.IsSuccess = true;

                    paymentGatewayResponse.PaymentStatus = (paymentModel.GatewayPreAuthorize && string.IsNullOrEmpty(paymentModel.GUID)) ? ZnodePaymentStatus.CREDIT_AUTHORIZED : ZnodePaymentStatus.CREDIT_CAPTURED;
                }
                else if (reply.ReasonCode.Equals("102"))
                {
                    paymentGatewayResponse.IsSuccess = true;
                    paymentGatewayResponse.PaymentStatus = (paymentModel.GatewayPreAuthorize && string.IsNullOrEmpty(paymentModel.GUID)) ? ZnodePaymentStatus.CREDIT_AUTHORIZED : ZnodePaymentStatus.CREDIT_CAPTURED;
                }
            }
            catch (Exception e)
            {
                paymentGatewayResponse.GatewayResponseData = this.GetReasoncodeDescription(string.Empty);
                Logging.LogActivity(paymentModel.PaymentSettingId, e.Message);
            }

            return paymentGatewayResponse;
        }

        /// <summary>
        /// Get the reason code description
        /// </summary>
        /// <param name="reasonCode">Reason code</param>
        /// <returns>Returns the reason description</returns>
        private string GetReasoncodeDescription(string reasonCode)
        {
            switch (reasonCode)
            {
                case "100":
                    return "Successful transaction.";
                case "203":
                    return "General decline of the card.";
                case "204":
                    return "Insufficient funds in the account.";
                case "208":
                    return "Inactive card or card not authorized for card-not-present transactions.";
                case "210":
                    return "The card has reached the credit limit.";
                case "211":
                    return "Invalid card verification number.";
                case "232":
                    return "The card type is not accepted by the payment processor.";
                case "234":
                    return "There is a problem with your CyberSource merchant configuration.";
                case "235":
                    return "The requested amount exceeds the originally authorized amount. Occurs, for example, if you try to capture an amount larger than the original authorization amount.";
                case "237":
                    return "The authorization has already been reversed.";
                case "238":
                    return "The authorization has already been captured.";
                case "239":
                    return "The requested transaction amount must match the previous transaction amount.";
                case "240":
                    return "The card type sent is invalid or does not correlate with the credit card number.";
                case "241":
                    return "The request ID is invalid.";
                case "243":
                    return "The transaction has already been settled or reversed.";
                case "247":
                    return "You requested a credit for a capture that was previously voided.";
                default:
                    break;
            }

            return "Unable to process, please contact customer support.";
        }

        /// <summary>
        /// This method will call the refund payment of Cybersource payment provider
        /// </summary>
        /// <param name="paymentModel">payment model</param>
        /// <returns>retunrs the refund response</returns>
        public GatewayResponseModel Refund(PaymentModel paymentModel)
        {
            var paymentGatewayResponse = new GatewayResponseModel();

            try
            {
                ICybsClientFactory factory = new DefautClientFactory();

                CybsClient client = factory.Create(CybsEnvironment.UseConfiguration);

                RequestMessage request = new RequestMessage();
                request.merchantReferenceCode = paymentModel.CustomerPaymentProfileId;
                request.ccAuthReversalService = new CCAuthReversalService();
                request.ccAuthReversalService.run = "true";
                request.ccAuthReversalService.authRequestID = paymentModel.TransactionId;
                request.orderRequestToken = paymentModel.CardDataToken;
                request.purchaseTotals = new PurchaseTotals();
                request.purchaseTotals.currency = paymentModel.GatewayCurrencyCode;
                request.purchaseTotals.grandTotalAmount = paymentModel.Total;

                var reply = client.RunTransaction(request);

                paymentGatewayResponse.ResponseCode = reply.ccAuthReversalReply.reasonCode;
                paymentGatewayResponse.GatewayResponseData = this.GetReasoncodeDescription(paymentGatewayResponse.ResponseCode);
                paymentGatewayResponse.CustomerProfileId = reply.requestID;
                paymentGatewayResponse.CustomerPaymentProfileId = reply.requestToken;

                if (paymentModel.GatewayPreAuthorize)
                {
                    paymentGatewayResponse.TransactionId = reply.requestID;
                }

                if (reply.ccAuthReversalReply.reasonCode.Equals("100"))
                {
                    paymentGatewayResponse.IsSuccess = true;
                    paymentGatewayResponse.PaymentStatus = ZnodePaymentStatus.CREDIT_REFUNDED;
                }
            }
            catch (Exception e)
            {
                paymentGatewayResponse.GatewayResponseData = this.GetReasoncodeDescription(string.Empty);
                Logging.LogActivity(paymentModel.PaymentSettingId, e.Message);
            }

            return paymentGatewayResponse;
        }

        /// <summary>
        /// This method will call the void payment of Cybersource payment provider
        /// </summary>
        /// <param name="paymentModel">payment model</param>
        /// <returns>retunrs the void response</returns>
        public GatewayResponseModel Void(PaymentModel paymentModel)
        {
            var paymentGatewayResponse = new GatewayResponseModel();

            try
            {
                ICybsClientFactory factory = new DefautClientFactory();

                CybsClient client = factory.Create(CybsEnvironment.UseConfiguration);
                string merchantReferenceCode = paymentModel.CardDataToken;
                CreditCardPaymentTaskReply reply;

                reply = client.Void(merchantReferenceCode, paymentModel.TransactionId, paymentModel.CustomerPaymentProfileId);

                paymentGatewayResponse.ResponseCode = reply.ReasonCode;
                paymentGatewayResponse.GatewayResponseData = this.GetReasoncodeDescription(reply.ReasonCode);
                paymentGatewayResponse.CustomerProfileId = reply.RequestId;
                paymentGatewayResponse.CustomerPaymentProfileId = reply.RequestToken;
                paymentGatewayResponse.Token = reply.MerchantReferenceCode;
                paymentGatewayResponse.TransactionId = reply.RequestId;

                if (reply.ReasonCode.Equals("100"))
                {
                    paymentGatewayResponse.IsSuccess = true;
                    paymentGatewayResponse.PaymentStatus = ZnodePaymentStatus.CREDIT_VOIDED;
                }
            }
            catch (Exception e)
            {
                paymentGatewayResponse.GatewayResponseData = this.GetReasoncodeDescription(string.Empty);
                Logging.LogActivity(paymentModel.PaymentSettingId, e.Message);
            }

            return paymentGatewayResponse;
        }

        /// <summary>
        /// This method will call the Subscription method of Cybersource payment provider
        /// </summary>
        /// <param name="paymentModel">payment model</param>
        /// <returns>retunrs the void response</returns>
        public GatewayResponseModel Subscription(PaymentModel paymentModel)
        {
            var paymentGatewayResponse = new GatewayResponseModel();
            try
            {
                ICybsClientFactory factory = new DefautClientFactory();
                RecurringSubscriptionInfo rsi = new RecurringSubscriptionInfo();
                rsi.amount = paymentModel.Subscription.InitialAmount.ToString();
                rsi.numberOfPayments = paymentModel.Subscription.TotalCycles.ToString();
                rsi.numberOfPaymentsToAdd = paymentModel.Subscription.Frequency.ToString();
                rsi.startDate = DateTime.Now.ToString("yyyyMMdd");
                rsi.frequency = "on-demand";

                switch (paymentModel.Subscription.Period)
                {
                    case "WEEK":
                        rsi.frequency = "weekly";
                        rsi.endDate = DateTime.Now.AddDays((Convert.ToInt32(paymentModel.Subscription.Frequency) * 7 * paymentModel.Subscription.TotalCycles)).ToString("yyyyMMdd");
                        break;
                    case "MONTH":
                        rsi.frequency = "monthly";
                        rsi.endDate = DateTime.Now.AddMonths(Convert.ToInt32(paymentModel.Subscription.Frequency) * paymentModel.Subscription.TotalCycles).ToString("yyyyMMdd");
                        break;
                    case "YEAR":
                        rsi.frequency = "annually";
                        rsi.endDate = DateTime.Now.AddYears(Convert.ToInt32(paymentModel.Subscription.Frequency) * paymentModel.Subscription.TotalCycles).ToString("yyyyMMdd");
                        break;
                }

                PurchaseTotals purchaseTotals = new PurchaseTotals();
                purchaseTotals.currency = paymentModel.GatewayCurrencyCode;
                purchaseTotals.grandTotalAmount = paymentModel.Subscription.InitialAmount.ToString();

                var card = new Card
                {
                    accountNumber = paymentModel.CardNumber,
                    expirationMonth = paymentModel.CardExpirationMonth,
                    expirationYear = paymentModel.CardExpirationYear,
                    cvNumber = paymentModel.CardSecurityCode,
                    cardType = GetCardType(paymentModel.CardType)
                };

                CybsClient client = factory.Create(CybsEnvironment.UseConfiguration);
                string merchantReferenceCode = RandomDataUtil.RandomId();
                var reply = client.CreatePaySubscriptionFromPayment(merchantReferenceCode,
                                                                paymentModel.CustomerProfileId,
                                                                req =>
                                                                {
                                                                    req.WithCard(card);
                                                                    req.WithRecurringSubscriptionInfo(rsi);
                                                                    req.WithPurchaseTotals(purchaseTotals);
                                                                });

                // To retrieve individual reply fields, follow these examples.
                //PaymentGatewayResponse.GatewayResponseData = reply.ToString();
                paymentGatewayResponse.ResponseCode = reply.ReasonCode;
                paymentGatewayResponse.GatewayResponseData = this.GetReasoncodeDescription(reply.ReasonCode);

                if (reply.ReasonCode.Equals("100"))
                {
                    paymentGatewayResponse.IsSuccess = true;
                    paymentGatewayResponse.TransactionId = reply.RequestId;
                    paymentGatewayResponse.CardAuthCode = reply.RequestToken;
                }
            }
            catch (Exception e)
            {
                paymentGatewayResponse.GatewayResponseData = this.GetReasoncodeDescription(string.Empty);
                Logging.LogActivity(paymentModel.PaymentSettingId, e.Message);
            }

            return paymentGatewayResponse;
        }

        /// <summary>
        /// This method will call the capture method of Cybersource payment provider
        /// </summary>
        /// <param name="paymentModel"></param>
        /// <returns></returns>
        private GatewayResponseModel CaptureTransaction(PaymentModel paymentModel)
        {
            var paymentGatewayResponse = new GatewayResponseModel();
            try
            {
                ICybsClientFactory factory = new DefautClientFactory();

                CybsClient client = factory.Create(CybsEnvironment.UseConfiguration);
                string merchantReferenceCode = RandomDataUtil.RandomId();
                var reply = client.CapturePreviouslyAuthorizedCreditCard(merchantReferenceCode,
                                                                paymentModel.CustomerProfileId,
                                                                req => req.WithPurchaseTotals(CybsCurrency.USD, decimal.Parse(paymentModel.Total)));

                // To retrieve individual reply fields, follow these examples.
                paymentGatewayResponse.ResponseCode = reply.ReasonCode;
                paymentGatewayResponse.GatewayResponseData = this.GetReasoncodeDescription(reply.ReasonCode);
                paymentGatewayResponse.TransactionId = reply.RequestId;
                paymentGatewayResponse.CardAuthCode = reply.RequestToken;
                if (reply.ReasonCode.Equals("100"))
                {
                    paymentGatewayResponse.IsSuccess = true;
                    paymentGatewayResponse.PaymentStatus = ZnodePaymentStatus.CREDIT_CAPTURED;
                }
            }
            catch (Exception e)
            {
                paymentGatewayResponse.GatewayResponseData = this.GetReasoncodeDescription(string.Empty);
                Logging.LogActivity(paymentModel.PaymentSettingId, e.Message);
            }

            return paymentGatewayResponse;
        }
    }
}