﻿using Stripe;
using System;
using Znode.Multifront.PaymentApplication.Api.Repository;
using Znode.Multifront.PaymentApplication.Models;

namespace Znode.Multifront.PaymentApplication.Api.PaymentProviders
{
    public class StripeCustomerProvider : IPaymentProviders
    {
        /// <summary>
        /// Validate the credit card through Stripe
        /// </summary>
        /// <param name="paymentModel"></param>
        /// <returns></returns>
        public GatewayResponseModel ValidateCreditcard(PaymentModel paymentModel)
        {
            string key = paymentModel.GatewayLoginName;
            var chargeService = new StripeChargeService(key);
            var gatewayResponse = new GatewayResponseModel();
            StripeCharge currentcharge;
            if (!string.IsNullOrEmpty(paymentModel.TransactionId))
            {
                currentcharge = chargeService.Capture(paymentModel.TransactionId);

                if (currentcharge.Captured.HasValue && currentcharge.Captured.Value)
                {
                    return new GatewayResponseModel { IsSuccess = true, TransactionId = currentcharge.Id, PaymentStatus = ZnodePaymentStatus.CREDIT_CAPTURED };
                }
                else
                {
                    return new GatewayResponseModel { IsSuccess = false, GatewayResponseData = currentcharge.FailureMessage };
                }
            }

            if (string.IsNullOrEmpty(paymentModel.CustomerProfileId))
            {
                return CreateCustomer(paymentModel);
            }            

            var stripeCharge = new StripeChargeCreateOptions
            {
                Amount = Convert.ToInt32(Convert.ToDecimal(paymentModel.Total) * 100),
                Currency = string.IsNullOrEmpty(paymentModel.GatewayCurrencyCode) ? Convert.ToString(System.Configuration.ConfigurationSettings.AppSettings["CurrencyCode"]) : paymentModel.GatewayCurrencyCode,
                CustomerId = paymentModel.CustomerProfileId,
                Capture = !paymentModel.GatewayPreAuthorize
            };

            currentcharge = chargeService.Create(stripeCharge);

            if (!paymentModel.GatewayPreAuthorize && currentcharge.Captured.HasValue)
            {
                gatewayResponse.IsSuccess = true;
                gatewayResponse.CustomerProfileId = paymentModel.CustomerProfileId;
                gatewayResponse.TransactionId = currentcharge.Id;

                gatewayResponse.PaymentStatus = ZnodePaymentStatus.CREDIT_CAPTURED;
                return gatewayResponse;

            }
            if (!string.IsNullOrEmpty(currentcharge.Id))
            {
                gatewayResponse.IsSuccess = true;
                gatewayResponse.CustomerProfileId = paymentModel.CustomerProfileId;
                gatewayResponse.TransactionId = currentcharge.Id;

                gatewayResponse.PaymentStatus = ZnodePaymentStatus.CREDIT_AUTHORIZED;
                return gatewayResponse;
            }
            return new GatewayResponseModel { IsSuccess = false, GatewayResponseData = currentcharge.FailureMessage };
        }

        /// <summary>
        /// Create a Customer using card holder data.
        /// </summary>
        /// <param name="paymentModel"></param>
        /// <returns></returns>
        private StripeCustomer GetCustomer(PaymentModel paymentModel)
        {
            var customer = new StripeCustomerCreateOptions
                {
                    Email = paymentModel.BillingEmailId,
                    Description = paymentModel.BillingEmailId,
                    Card = new StripeCreditCardOptions
                    {
                        CardNumber = paymentModel.CardNumber,
                        CardExpirationMonth = paymentModel.CardExpirationMonth,
                        CardExpirationYear = paymentModel.CardExpirationYear,
                        CardName = paymentModel.CardHolderName,
                        CardAddressCity = paymentModel.BillingCity,
                        CardAddressCountry = paymentModel.BillingCountryCode,
                        CardAddressLine1 = paymentModel.BillingStreetAddress1
                    }
                };

            var customerservice = new StripeCustomerService(paymentModel.GatewayLoginName);
            return customerservice.Create(customer);

        }

        /// <summary>
        /// Refund transaction using the exisitng transaction key with the specified amount.
        /// </summary>
        /// <param name="paymentModel"></param>
        /// <returns></returns>
        public GatewayResponseModel Refund(PaymentModel paymentModel)
        {
            string key = paymentModel.GatewayLoginName;
            var chargeService = new StripeChargeService(key);


            var currentcharge = chargeService.Refund(paymentModel.TransactionId, Convert.ToInt32(Convert.ToDecimal(paymentModel.Total) * 100));

            if (currentcharge.Captured.HasValue && currentcharge.Captured.Value)
            {
                return new GatewayResponseModel { IsSuccess = true, TransactionId = currentcharge.Id, PaymentStatus = ZnodePaymentStatus.CREDIT_REFUNDED };
            }
            else
            {
                return new GatewayResponseModel { IsSuccess = false, GatewayResponseData = currentcharge.FailureMessage };
            }

        }

        /// <summary>
        /// Void is not implemented
        /// </summary>
        /// <param name="paymentModel"></param>
        /// <returns></returns>
        public GatewayResponseModel Void(PaymentModel paymentModel)
        {
            return new GatewayResponseModel { IsSuccess = false, GatewayResponseData = "" };
        }


        /// <summary>
        /// Create a Subscription plan
        /// </summary>
        /// <param name="paymentModel"></param>
        /// <returns></returns>
        public GatewayResponseModel Subscription(PaymentModel paymentModel)
        {
            try
            {
                string key = paymentModel.GatewayLoginName;
                var myPlan = new StripePlanCreateOptions();
                myPlan.Amount = Convert.ToInt32(Convert.ToDecimal(paymentModel.Total) * 100);         // all amounts on Stripe are in cents, pence, etc
                myPlan.Currency = paymentModel.GatewayCurrencyCode;
                myPlan.Interval = paymentModel.Subscription.Period.ToLower();
                myPlan.Name = paymentModel.Subscription.ProfileName;
                myPlan.IntervalCount = paymentModel.Subscription.TotalCycles;    // amount of time that will lapse before the customer is billed

                myPlan.Id = "my-plan-" + Guid.NewGuid();
                var planService = new StripePlanService(key);
                StripePlan planresponse = planService.Create(myPlan);
                if (planresponse != null)
                {
                    var subscriptionService = new StripeSubscriptionService(key);
                    StripeSubscription stripeSubscription = subscriptionService.Create(paymentModel.CustomerProfileId, planresponse.Id);

                    if (stripeSubscription != null)
                    {
                        return new GatewayResponseModel { IsSuccess = true, TransactionId = stripeSubscription.Id };
                    }
                }
            }
            catch (Exception ex)
            {
                Logging.LogActivity(paymentModel.PaymentSettingId, ex.Message);
                Logging.LogMessage(ex.Message + ex.StackTrace);
            }
            return new GatewayResponseModel { IsSuccess = false, GatewayResponseData = "Unable to process your request" };
        }

        /// <summary>
        /// This method will create customer profile 
        /// </summary>
        /// <param name="paymentModel">PaymentModel paymentModel</param>
        /// <returns>returns customer details response</returns>
        private GatewayResponseModel CreateCustomer(PaymentModel paymentModel)
        {
            var response = new GatewayResponseModel();
            var customerResponse = GetCustomer(paymentModel);
            if (!string.IsNullOrEmpty(customerResponse.Id))
            {
                response.CustomerProfileId = customerResponse.Id;
                response.IsSuccess = true;               
            }
            else 
            {
                response.IsSuccess = false;
                response.ResponseText = customerResponse.Description;
            }
            return response;
        }
    }
}