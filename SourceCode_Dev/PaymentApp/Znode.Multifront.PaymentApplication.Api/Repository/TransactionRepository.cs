﻿using System;
using System.Data.Entity.Validation;
using System.Linq;
using Znode.Multifront.PaymentApplication.Models;

namespace Znode.Multifront.PaymentApplication.Api.Repository
{
    public class TransactionRepository
    {
        /// <summary>
        /// Add payment transactions to DB
        /// </summary>
        /// <param name="paymentModel"></param>
        /// <returns></returns>
        public string AddPayment(PaymentModel paymentModel)
        {
            try
            {
                var db = new znode_multifront_paymentEntities();
                var transactions = new Znode_Transactions
                    {
                        GUID = Guid.NewGuid(),
                        CustomerProfileId = paymentModel.CustomerProfileId,
                        CustomerPaymentId = paymentModel.CustomerPaymentProfileId,
                        TransactionDate = DateTime.Now,
                        TransactionId = paymentModel.TransactionId,
                        ResponseText = paymentModel.ResponseText,
                        ResponseCode = paymentModel.ResponseCode,
                        Amount = decimal.Parse(paymentModel.Total),
                        PaymentSettingId = paymentModel.PaymentSettingId,
                        CurrencyCode = paymentModel.GatewayCurrencyCode,
                        Custom1 = paymentModel.CardDataToken,
                        PaymentStatusId = paymentModel.PaymentStatusId
                    };

                db.Znode_Transactions.Add(transactions);
                db.SaveChanges();

                return transactions.GUID.ToString();
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);
                var data = string.Format("TransactionId : {0}, CustomerProfileId: {1}, CustomerPaymentProfile : {2}", paymentModel.TransactionId, paymentModel.CustomerProfileId, paymentModel.CustomerPaymentProfileId);

                Logging.LogActivity(paymentModel.PaymentSettingId, exceptionMessage, data);

                Logging.LogMessage(exceptionMessage);

            }
            return null;
        }

        /// <summary>
        ///  Uodate payment transactions to DB
        /// </summary>
        /// <param name="paymentModel"></param>
        /// <returns></returns>
        public string UpdatePayment(PaymentModel paymentModel)
        {
            try
            {
                var db = new znode_multifront_paymentEntities();
                var objPayment = db.Znode_Transactions.ToList().Single(payment => payment.GUID.ToString().ToLower().Equals(paymentModel.GUID.ToLower()));
                if (paymentModel.Subscriptions.Any())
                    objPayment.SubscriptionId = objPayment.SubscriptionId + "," + paymentModel.TransactionId;
                else
                    if (paymentModel.PaymentStatusId <= 1)
                        objPayment.TransactionId = paymentModel.TransactionId;
                objPayment.ResponseText = paymentModel.ResponseText;
                objPayment.ResponseCode = paymentModel.ResponseCode;
                objPayment.PaymentStatusId = paymentModel.PaymentStatusId;
                if (!string.IsNullOrEmpty(paymentModel.RefundTransactionId))
                    objPayment.Custom2 = paymentModel.RefundTransactionId;


                //If it is refund transaction then update refund date
                if (paymentModel.PaymentStatusId == 3)
                {
                    objPayment.RefundTransactionDate = DateTime.Now;
                }
                //If it is captured transaction then update captured date
                if (paymentModel.PaymentStatusId == 1)
                {
                    objPayment.CaptureTransactionDate = DateTime.Now;
                }
                db.SaveChanges();

                return objPayment.GUID.ToString();
            }
            catch (Exception ex)
            {
                Logging.LogActivity(paymentModel.PaymentSettingId, ex.Message);
                return null;
            }
        }
        /// <summary>
        ///  Get payment transactions from DB
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        public Znode_Transactions GetPayment(string guid)
        {
            try
            {
                var db = new znode_multifront_paymentEntities();
                var paymentTransaction = from p in db.Znode_Transactions.AsEnumerable()
                                         where p.GUID.ToString().ToLower() == guid.ToLower()
                                         select p;

                if (paymentTransaction.Any())
                    return paymentTransaction.FirstOrDefault();
            }
            catch (Exception ex)
            {
                Logging.LogActivity(null, ex.Message);
                return null;
            }
            return null;
        }

    }
}