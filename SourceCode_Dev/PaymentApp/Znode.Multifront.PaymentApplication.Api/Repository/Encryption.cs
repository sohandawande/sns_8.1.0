﻿using System;
using System.Configuration;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Znode.Multifront.PaymentApplication.Api.Repository
{
    public class Encryption
    {
        #region Constant Variables
        public const string _EncryptionKey = "znode123";
        public const string _EncruptionKeyName = "EncryptionKey";
        #endregion

        /// <summary>
        /// Encrpt the given text
        /// </summary>
        /// <param name="clearText">string value to be encrypted</param>
        /// <returns>Encrypted string of given value</returns>
        public static string Encrypt(string clearText)
        {
            if (string.IsNullOrEmpty(clearText))
            {
                return string.Empty;
            }

            string EncryptionKey = _EncryptionKey;

            if (ConfigurationManager.AppSettings[_EncruptionKeyName] != null)
            {
                EncryptionKey = ConfigurationManager.AppSettings[_EncruptionKeyName].ToString();
            }

            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }

        /// <summary>
        /// Decrypt the given encrypted string.
        /// </summary>
        /// <param name="cipherText">encrypted string value</param>
        /// <returns>Decrypted string value</returns>
        public static string Decrypt(string cipherText)
        {
            if (string.IsNullOrEmpty(cipherText))
            {
                return string.Empty;
            }

            string encryptionKey = _EncryptionKey;

            if (ConfigurationManager.AppSettings[_EncruptionKeyName] != null)
            {
                encryptionKey = ConfigurationManager.AppSettings[_EncruptionKeyName].ToString();
            }

            cipherText = cipherText.Replace(" ", "+");
            byte[] cipherBytes = Convert.FromBase64String(cipherText);

            using (Aes encryptor = Aes.Create())
            {
                var pdb = new Rfc2898DeriveBytes(encryptionKey,
                            new byte[]
                                {
                                    0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64,
                                    0x65, 0x76
                                });

                if (encryptor != null)
                {
                    encryptor.Key = pdb.GetBytes(32);
                    encryptor.IV = pdb.GetBytes(16);

                    using (var ms = new MemoryStream())
                    {
                        using (var cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(cipherBytes, 0, cipherBytes.Length);
                            cs.Close();
                        }
                        cipherText = Encoding.Unicode.GetString(ms.ToArray());
                    }
                }
            }
            return cipherText;
        }
    }
}