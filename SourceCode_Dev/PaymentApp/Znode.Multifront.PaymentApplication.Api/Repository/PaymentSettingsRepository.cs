﻿using System.Linq;

namespace Znode.Multifront.PaymentApplication.Api.Repository
{
    public class PaymentSettingsRepository
    {
        /// <summary>
        /// Add the payment settings in paymnet api db.
        /// </summary>
        /// <param name="paymentSetting">ZNodePaymentSetting settings</param>
        public void AddPaymentSetting(ZNodePaymentSetting paymentSetting)
        {
            var db = new znode_multifront_paymentEntities();
            paymentSetting = EncryptPaymentSettings(paymentSetting);
            db.ZNodePaymentSettings.Add(paymentSetting);
            db.SaveChanges();
        }

        /// <summary>
        /// Update the payment settings in paymnet api db.
        /// </summary>
        /// <param name="paymentSetting">ZNodePaymentSetting settings</param>
        public void UpdatePaymentSetting(ZNodePaymentSetting paymentSetting)
        {
            paymentSetting = EncryptPaymentSettings(paymentSetting);
            var db = new znode_multifront_paymentEntities();
            var objPayment = db.ZNodePaymentSettings.ToList().Single(payment => payment.PaymentSettingID.Equals(paymentSetting.PaymentSettingID));

            objPayment.PaymentSettingID = paymentSetting.PaymentSettingID;
            objPayment.GatewayTypeID = paymentSetting.GatewayTypeID;
            objPayment.PaymentTypeID = paymentSetting.PaymentTypeID;
            objPayment.ActiveInd = paymentSetting.ActiveInd;
            objPayment.DisplayOrder = paymentSetting.DisplayOrder;
            objPayment.EnableAmex = paymentSetting.EnableAmex;
            objPayment.EnableDiscover = paymentSetting.EnableDiscover;
            objPayment.EnableMasterCard = paymentSetting.EnableMasterCard;
            objPayment.EnableVisa = paymentSetting.EnableVisa;
            objPayment.TestMode = paymentSetting.TestMode;
            objPayment.PreAuthorize = paymentSetting.PreAuthorize;
            objPayment.TransactionKey = paymentSetting.TransactionKey;
            objPayment.IsRMACompatible = paymentSetting.IsRMACompatible;
            objPayment.GatewayUsername = paymentSetting.GatewayUsername;
            objPayment.GatewayPassword = paymentSetting.GatewayPassword;
            objPayment.Partner = paymentSetting.Partner;
            objPayment.Vendor = paymentSetting.Vendor;
            objPayment.ProfileID = paymentSetting.ProfileID;
            db.SaveChanges();
        }

        /// <summary>
        /// Delete the payment settings based on the payment settings id.
        /// </summary>
        /// <param name="paymentSettingId">int payment settings id</param>
        /// <returns>True if record deleted</returns>
        public bool DeletePaymentSetting(int paymentSettingId)
        {
            var db = new znode_multifront_paymentEntities();

            var paymentSetting = db.ZNodePaymentSettings.First(x => x.PaymentSettingID == paymentSettingId);
            db.ZNodePaymentSettings.Remove(paymentSetting);
            return db.SaveChanges() > 0;
        }

        /// <summary>
        /// Get Payment settings by payment settings id
        /// </summary>
        /// <param name="paymentSettingId">int payment settings id</param>
        /// <returns>All the information of PaymentSettings in the form of ZNodePaymentSetting model</returns>
        public ZNodePaymentSetting GetPaymentSetting(int paymentSettingId)
        {
            var db = new znode_multifront_paymentEntities();
            var paymentTransaction = from p in db.ZNodePaymentSettings
                                     where p.PaymentSettingID == paymentSettingId
                                     select p;

            if (paymentTransaction.Any())
            {
                return DecryptPaymentSettings(paymentTransaction.FirstOrDefault());
            }
            return null;
        }

        /// <summary>
        /// Get Payment settings by payment settings and profile id
        /// </summary>
        /// <param name="paymentSettingId">int gatewaytypeid</param>
        /// <param name="profileId">nullable int profile id</param>
        /// <returns></returns>
        public ZNodePaymentSetting GetPaymentSettingByGateway(int gatewayTypeId, int? profileId)
        {
            var db = new znode_multifront_paymentEntities();
            var paymentTransaction = from p in db.ZNodePaymentSettings
                                     where p.GatewayTypeID == gatewayTypeId && p.ActiveInd && (profileId == null ? p.ProfileID == null : p.ProfileID == profileId.Value)
                                     select p;

            if (paymentTransaction.Any())
            {
                return DecryptPaymentSettings(paymentTransaction.FirstOrDefault());
            }
            return null;
        }

        /// <summary>
        /// Encrypt the important payment fields of ZNodePaymentSetting models
        /// </summary>
        /// <param name="paymentSetting">ZNodePaymentSetting model data</param>
        /// <returns>Encrypted data in the form of ZNodePaymentSetting model</returns>
        public ZNodePaymentSetting EncryptPaymentSettings(ZNodePaymentSetting paymentSetting)
        {
            paymentSetting.GatewayPassword = Encryption.Encrypt(paymentSetting.GatewayPassword);
            paymentSetting.GatewayUsername = Encryption.Encrypt(paymentSetting.GatewayUsername);
            paymentSetting.TransactionKey = Encryption.Encrypt(paymentSetting.TransactionKey);
            paymentSetting.Vendor = Encryption.Encrypt(paymentSetting.Vendor);
            paymentSetting.Partner = Encryption.Encrypt(paymentSetting.Partner);
            return paymentSetting;
        }

        /// <summary>
        /// Decrypt the Payment settings important fields for calling
        /// </summary>
        /// <param name="paymentSetting">ZNodePaymentSetting model</param>
        /// <returns>Decrypted data in the form of ZNodePaymentSetting mdoel</returns>
        public ZNodePaymentSetting DecryptPaymentSettings(ZNodePaymentSetting paymentSetting)
        {
            paymentSetting.GatewayPassword = Encryption.Decrypt(paymentSetting.GatewayPassword);
            paymentSetting.GatewayUsername = Encryption.Decrypt(paymentSetting.GatewayUsername);
            paymentSetting.TransactionKey = Encryption.Decrypt(paymentSetting.TransactionKey);
            paymentSetting.Vendor = Encryption.Decrypt(paymentSetting.Vendor);
            paymentSetting.Partner = Encryption.Decrypt(paymentSetting.Partner);
            return paymentSetting;
        }
    }
}