﻿using System.Linq;

namespace Znode.Multifront.PaymentApplication.Api.Repository
{
    public class DomainRepository
    {
        public ZNodeDomain GetDomain(string domainName)
        {
            var db = new znode_multifront_paymentEntities();
            var domain = from d in db.ZNodeDomains
                         where d.DomainName == domainName
                         select d;

            if (domain.Any())
                return domain.FirstOrDefault();
            return null;
        }
    }
}