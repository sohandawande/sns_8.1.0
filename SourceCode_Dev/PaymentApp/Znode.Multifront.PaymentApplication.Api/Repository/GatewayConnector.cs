﻿using System;
using System.Linq;
using Znode.Multifront.PaymentApplication.Api.Helpers;
using Znode.Multifront.PaymentApplication.Api.PaymentProviders;
using Znode.Multifront.PaymentApplication.Models;

namespace Znode.Multifront.PaymentApplication.Api.Repository
{
    public class GatewayConnector
    {
        private IPaymentProviders _provider = null;

        /// <summary>
        /// Get Payment Settings details from DB and assigned to Payment Model.
        /// </summary>
        /// <param name="paymentModel"></param>
        /// <returns></returns>
        private PaymentModel GetPaymentSettingsModel(PaymentModel paymentModel)
        {
            var paymentrepository = new PaymentSettingsRepository();
            var paymentSetting = paymentrepository.GetPaymentSetting(paymentModel.PaymentSettingId);

            if (Equals(paymentModel, null))
            {
                GatewayType gatewayType;
                Enum.TryParse(paymentModel.GatewayType, out gatewayType);
                int gatewaytypeId = Convert.ToInt16(Enum.Parse(gatewayType.GetType(), paymentModel.GatewayType.ToUpper()));

                paymentSetting = paymentrepository.GetPaymentSettingByGateway(gatewaytypeId, paymentModel.ProfileId);
            }

            if (paymentSetting != null)
            {
                paymentModel.GatewayTestMode = paymentSetting.TestMode;
                paymentModel.GatewayPreAuthorize = paymentSetting.PreAuthorize;
                paymentModel.GatewayLoginName = paymentSetting.GatewayUsername;
                paymentModel.GatewayLoginPassword = paymentSetting.GatewayPassword;
                paymentModel.GatewayTransactionKey = paymentSetting.TransactionKey;
                paymentModel.GatewayPreAuthorize = paymentSetting.PreAuthorize;
                paymentModel.GatewayType = Equals(paymentSetting.GatewayTypeID, null) ? string.Empty : paymentSetting.GatewayTypeID.Value.ToString();
                paymentModel.Vendor = paymentSetting.Vendor;
                paymentModel.Partner = paymentSetting.Partner;
            }
            return paymentModel;

        }

        /// <summary>
        /// Process Credit card based on Gateway type
        /// </summary>
        /// <param name="paymentModel"></param>
        /// <returns></returns>
        private GatewayResponseModel ProcessCreditCard(PaymentModel paymentModel)
        {
            paymentModel = GetPaymentSettingsModel(paymentModel);

            if (!string.IsNullOrEmpty(paymentModel.GatewayType))
            {
                _provider = GetProvider(paymentModel.GatewayType);
                Log4NetHelper.ReplaceLog4NetDLL(paymentModel.GatewayType);
                if (_provider != null)
                    return _provider.ValidateCreditcard(paymentModel);
            }
            return null;
        }
        private GatewayResponseModel ProcessRefundVoid(PaymentModel paymentModel, bool isVoid = false)
        {
            paymentModel = GetPaymentSettingsModel(paymentModel);

            if (!string.IsNullOrEmpty(paymentModel.GatewayType))
            {
                _provider = GetProvider(paymentModel.GatewayType);
                Log4NetHelper.ReplaceLog4NetDLL(paymentModel.GatewayType);
                if (_provider != null)
                {
                    if (isVoid)
                        return _provider.Void(paymentModel);
                    return _provider.Refund(paymentModel);
                }
            }
            return null;
        }

        /// <summary>
        /// Process the subbscription based on Gateway
        /// </summary>
        /// <param name="paymentModel"></param>
        /// <param name="isVoid"></param>
        /// <returns></returns>
        private GatewayResponseModel ProcessSubscription(PaymentModel paymentModel, bool isVoid = false)
        {
            paymentModel = GetPaymentSettingsModel(paymentModel);

            if (!string.IsNullOrEmpty(paymentModel.GatewayType))
            {
                _provider = GetProvider(paymentModel.GatewayType);
                Log4NetHelper.ReplaceLog4NetDLL(paymentModel.GatewayType);
                if (_provider != null)
                    return _provider.Subscription(paymentModel);
            }
            return null;
        }

        /// <summary>
        /// Get Gateway provider name by gateway name
        /// </summary>
        /// <param name="gateway"></param>
        /// <returns></returns>
        private IPaymentProviders GetProvider(string gateway)
        {
            if (!string.IsNullOrEmpty(gateway))
            {
                GatewayType gatewayType;
                Enum.TryParse(gateway, out gatewayType);
                switch (gatewayType)
                {
                    case GatewayType.AUTHORIZENET:
                        _provider = new AuthorizeNetCustomerProvider();
                        break;
                    case GatewayType.TWOCHECKOUT:
                        _provider = new TwoCheckoutProvider();
                        break;
                    case GatewayType.STRIPE:
                        _provider = new StripeCustomerProvider();
                        break;
                    case GatewayType.CYBERSOURCE:
                        _provider = new CyberSourceCustomerProviderProvider();
                        break;
                    case GatewayType.PAYPAL:
                        _provider = new PaypalCustomerProvider();
                        break;
                    case GatewayType.PAYMENTTECH:
                        _provider = new PaymentTechProvider();
                        break;
                    case GatewayType.SECURENET:
                    case GatewayType.WORLDPAY:
                        _provider = new SecureNetCustomerProvider();
                        break;
                    default:
                        return null;
                }
                return _provider;
            }
            return null;
        }

        /// <summary>
        /// Execute and get Credit card processing and returns the GUID
        /// </summary>
        /// <param name="paymentModel"></param>
        /// <returns></returns>
        public string GetResponse(PaymentModel paymentModel)
        {
            try
            {
                var repository = new TransactionRepository();

                if (!string.IsNullOrEmpty(paymentModel.GUID))
                {
                    var transactionDetails = repository.GetPayment(paymentModel.GUID);
                    if (transactionDetails != null)
                    {
                        paymentModel.CustomerProfileId = transactionDetails.CustomerProfileId;
                        paymentModel.CustomerPaymentProfileId = transactionDetails.CustomerPaymentId;
                        paymentModel.TransactionId = transactionDetails.TransactionId;
                        paymentModel.GatewayCurrencyCode = transactionDetails.CurrencyCode;
                        paymentModel.CardDataToken = transactionDetails.Custom1;
                        paymentModel.OrderId = transactionDetails.Custom1;
                    }
                }

                var response = this.ProcessCreditCard(paymentModel);

                if (response == null)
                {
                    return paymentModel.CancelUrl + (paymentModel.ReturnUrl.IndexOf('?') > 0 ? "&" : "?") +
                                string.Format("Message={0}", "Unable to process the request");
                }

                if (response.IsSuccess)
                {
                    if (string.IsNullOrEmpty(paymentModel.GUID))
                    {
                        paymentModel.CustomerProfileId = response.CustomerProfileId;
                        paymentModel.CustomerPaymentProfileId = response.CustomerPaymentProfileId;
                    }

                    paymentModel.TransactionId = response.TransactionId;
                    paymentModel.ResponseText = response.GatewayResponseData;
                    paymentModel.ResponseCode = response.ResponseCode;
                    paymentModel.CardDataToken = response.Token;
                    paymentModel.PaymentStatusId = (int)response.PaymentStatus;
                    paymentModel.GUID = string.IsNullOrEmpty(paymentModel.GUID) ? repository.AddPayment(paymentModel) : repository.UpdatePayment(paymentModel);

                    if (paymentModel.Subscriptions.Any() && paymentModel.GatewayType != Convert.ToInt16(Enum.Parse(typeof(GatewayType), GatewayType.TWOCHECKOUT.ToString())).ToString())
                    {
                        var Subscriptionresponse = GetSubscriptionResponse(paymentModel);
                    }
                    var append = string.Format("token={0}", paymentModel.GUID);
                    return paymentModel.ReturnUrl + (paymentModel.ReturnUrl.IndexOf('?') > 0 ? "&" : "?") + append;

                }
                else
                {
                    string message = !Equals(response.GatewayResponseData,null) ? response.GatewayResponseData.Replace("<br>", string.Empty) : "Unable to contact payment provider.";
                    Logging.LogActivity(paymentModel.PaymentSettingId, "Transaction failed", message);
                    return paymentModel.CancelUrl + (paymentModel.ReturnUrl.IndexOf('?') > 0 ? "&" : "?") +
                                string.Format("Message={0}", message);
                }
            }
            catch (Exception ex)
            {
                Logging.LogActivity(paymentModel.PaymentSettingId, ex.Message);
                Logging.LogMessage(ex.StackTrace);
                return paymentModel.CancelUrl + (paymentModel.ReturnUrl.IndexOf('?') > 0 ? "&" : "?") +
                                string.Format("Message={0}", ex.Message);
            }
        }

        /// <summary>
        /// To create customer profile using Payment API
        /// </summary>
        /// <param name="paymentModel"></param>
        /// <returns></returns>
        public GatewayResponseModel GetCustomerResponse(PaymentModel paymentModel)
        {
            try
            {
                var repository = new TransactionRepository();
                return this.ProcessCreditCard(paymentModel);
            }
            catch (Exception ex)
            {
                Logging.LogActivity(paymentModel.PaymentSettingId, ex.Message);
                Logging.LogMessage(ex.StackTrace);

                GatewayResponseModel errorModel = new GatewayResponseModel();
                errorModel.IsSuccess = false;
                errorModel.ResponseText = ex.Message;

                return errorModel;
            }
        }
        /// <summary>
        /// Execute and Get Capture status from gateway provider
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public string GetCaptureResponse(string token)
        {
            var repository = new TransactionRepository();
            var paymentModel = new PaymentModel { GUID = token };

            if (!string.IsNullOrEmpty(token))
            {
                var transactionDetails = repository.GetPayment(token);
                if (transactionDetails != null)
                {
                    paymentModel.CustomerProfileId = transactionDetails.CustomerProfileId;
                    paymentModel.CustomerPaymentProfileId = transactionDetails.CustomerPaymentId;
                    paymentModel.PaymentSettingId = transactionDetails.PaymentSettingId.Value;
                    paymentModel.TransactionId = transactionDetails.TransactionId;
                    paymentModel.Total = transactionDetails.Amount.ToString();
                    paymentModel.GatewayCurrencyCode = transactionDetails.CurrencyCode;
                    paymentModel.CardDataToken = transactionDetails.Custom1;
                    paymentModel.OrderId = transactionDetails.Custom1;

                    var response = this.ProcessCreditCard(paymentModel);


                    if (response.IsSuccess)
                    {
                        paymentModel.TransactionId = response.TransactionId;
                        paymentModel.PaymentStatusId = (int)response.PaymentStatus;
                        paymentModel.ResponseText = response.GatewayResponseData;
                        paymentModel.ResponseCode = response.ResponseCode;

                        repository.UpdatePayment(paymentModel);
                        Logging.LogActivity(paymentModel.PaymentSettingId, "Transaction Captured");
                        return "Success";
                    }
                    else
                    {
                        string message = response.GatewayResponseData.Replace("<br>", "");
                        Logging.LogActivity(paymentModel.PaymentSettingId, "Transaction Capture failed", message);
                        Logging.LogMessage(message);
                        // return new string[] { string.Format("Message={0}", message) };
                        return string.Format("Message={0}", message);
                    }
                }
            }

            return "Invalid Token";
        }

        /// <summary>
        /// Get Refund/Void status from gateway provider
        /// </summary>
        /// <param name="token"></param>
        /// <param name="amount"></param>
        /// <param name="isVoid"></param>
        /// <returns></returns>
        public string GetRefundVoidResponse(string token, decimal amount, bool isVoid = false)
        {
            var repository = new TransactionRepository();
            var paymentModel = new PaymentModel { GUID = token, Total = amount.ToString() };

            if (!string.IsNullOrEmpty(token))
            {
                var transactionDetails = repository.GetPayment(token);
                if (transactionDetails != null)
                {
                    paymentModel.CustomerProfileId = transactionDetails.CustomerProfileId;
                    paymentModel.CustomerPaymentProfileId = transactionDetails.CustomerPaymentId;
                    paymentModel.PaymentSettingId = transactionDetails.PaymentSettingId.Value;
                    paymentModel.TransactionId = transactionDetails.TransactionId;
                    paymentModel.GatewayCurrencyCode = transactionDetails.CurrencyCode;
                    paymentModel.CardDataToken = transactionDetails.Custom1;
                    paymentModel.OrderId = transactionDetails.Custom1;
                    var response = this.ProcessRefundVoid(paymentModel, isVoid);


                    if (response.IsSuccess)
                    {
                        paymentModel.RefundTransactionId = response.TransactionId;

                        paymentModel.ResponseText = response.GatewayResponseData;
                        paymentModel.ResponseCode = response.ResponseCode;
                        paymentModel.PaymentStatusId = (int)response.PaymentStatus;
                        repository.UpdatePayment(paymentModel);
                        Logging.LogActivity(paymentModel.PaymentSettingId, "Refund transaction");
                        return "Success";
                    }
                    else
                    {
                        string message = response.GatewayResponseData.Replace("<br>", "");
                        Logging.LogActivity(paymentModel.PaymentSettingId, "Transaction Refund failed", message);
                        Logging.LogMessage(message);
                        return string.Format("Message={0}", message);
                    }
                }
            }

            return "Invalid Token";
        }

        public string GetSubscriptionResponse(PaymentModel paymentModel)
        {
            var repository = new TransactionRepository();
            foreach (var subscriptionModel in paymentModel.Subscriptions)
            {
                paymentModel.Subscription = subscriptionModel;
                var response = this.ProcessSubscription(paymentModel);
                if (response.IsSuccess)
                {
                    paymentModel.TransactionId = response.TransactionId;

                    repository.UpdatePayment(paymentModel);
                    Logging.LogActivity(paymentModel.PaymentSettingId, "Subscription Created");
                    // return "Success";
                }
                else
                {
                    string message = response.GatewayResponseData.Replace("<br>", "");
                    Logging.LogActivity(paymentModel.PaymentSettingId, "Subscription failed", message);
                    Logging.LogMessage(message);
                    // return string.Format("Message={0}", message);
                }

            }

            return "";
        }

        /// <summary>
        /// Execute and Get Capture status from gateway provider
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public string GetPaypalResponse(PaymentModel paymentModel)
        {
            paymentModel = GetPaymentSettingsModel(paymentModel);
            var repository = new TransactionRepository();
            Log4NetHelper.ReplaceLog4NetDLL(Convert.ToInt16(Enum.Parse(typeof(GatewayType), GatewayType.PAYPALEXPRESS.ToString())).ToString());
            var paypal = new ZNode.Libraries.Paypal.PaypalGateway(paymentModel);

            paypal.PaymentActionTypeCode = "Sale";

            ZNode.Libraries.Paypal.PaypalResponse response = paypal.DoPaypalExpressCheckout(paymentModel);

            if (response.ResponseCode != "0")
            {
                return response.ResponseText;
            }
            else
            {
                // Redirect to paypal server
                return response.HostUrl;
            }
        }
    }
}