﻿using System.Collections.Generic;
using System.Linq;

namespace Znode.Multifront.PaymentApplication.Api.Repository
{
    public class GatewayRepository
    {
        /// <summary>
        /// Get Gateway type
        /// </summary>
        /// <param name="paymentSettingId"></param>
        /// <returns></returns>
        public List<ZNodeGateway> GetAll()
        {
            var db = new znode_multifront_paymentEntities();
            return db.ZNodeGateways.ToList();
        }
    }
}