﻿using System.Text;
using System.Web;

namespace Znode.Multifront.PaymentApplication.Api.Repository
{
    public static class ActivityLogRepository
    {

        /// <summary>
        /// Adds an entry in the ZnodeActivityLog table.
        /// </summary>
        /// <param name="ActivityLogId">The ActivityLogID from the ZnodeActivityLog table</param>
        /// <param name="PaymentSettingId">Payment setting id.\</param>
        /// <param name="Data1">Custom data to log</param>
        /// <param name="Data2">Custom data to log</param>
        /// <param name="Data3">Custom data to log</param>
        /// <param name="Status">Status message for this activity</param>
        /// <param name="LongData">Custom data to log. Can be up to 4000 characters.</param>
        /// <param name="Source">Source</param>
        /// <param name="Target">Target</param>
        /// <returns>Log Id</returns>
        public static int InsertActivityLog(int? paymentSettingId, string Data1, string Data2, string Data3, string Status, string LongData, string Source, string Target)
        {
            int activityLogId = -1;

            try
            {
                // Do a quick check to see if Activity logging has been disabled.
                if (DatabaseLoggingEnabled())
                {
                    var db = new znode_multifront_paymentEntities();
                    var al = new ZNodeActivityLog();
                    al.PaymentSettingID = paymentSettingId;
                    al.Data1 = Data1;
                    al.Data2 = Data2;
                    al.Data3 = Data3;
                    al.Status = Status;
                    al.LongData = LongData;
                    al.Source = Source;
                    al.Target = Target;

                    al.CreateDte = System.DateTime.Now;

                    al.URL = HttpContext.Current.Request.Url.PathAndQuery;


                    db.ZNodeActivityLogs.Add(al);
                    db.SaveChanges();
                    activityLogId = al.ActivityLogID;
                }
            }
            catch
            {
                StringBuilder sb = new StringBuilder("Error logging message to activity table in LogActivity. ");

                sb.Append("/nData1: " + Data1);
                sb.Append("/nData2: " + Data2);
                sb.Append("/nData3: " + Data3);
                sb.Append("/nStatus: " + Status);
                sb.Append("/nLongData: " + LongData);
                sb.Append("/nSource: " + Source);
                sb.Append("/nTarget: " + Target);
                Logging.LogMessage(sb.ToString());
            }

            return activityLogId;
        }

        public static bool DatabaseLoggingEnabled()
        {
            bool enableLogging = true;

            string configSetting = System.Configuration.ConfigurationManager.AppSettings["EnableLogging"];

            // Even if the entry is not in the web.config we are going to default this value to on.
            if (configSetting != null)
            {
                configSetting = configSetting.Trim();

                if (configSetting.Equals("0"))
                {
                    enableLogging = false;
                }
            }

            return enableLogging;
        }
        /// <summary>
        /// Adds an entry in the ZnodeActivityLog table.
        /// </summary>
        /// <param name="ActivityLogId">The ActivityLogID from the ZnodeActivityLog table</param>
        /// <param name="PaymentSettingId">Payment setting id.\</param>
        /// <param name="Data1">Custom data to log</param>
        /// <param name="Data2">Custom data to log</param>
        /// <param name="Data3">Custom data to log</param>
        /// <param name="Status">Status message for this activity</param>
        /// <param name="LongData">Custom data to log. Can be up to 4000 characters.</param>
        public static int InsertActivityLog(int? paymentSettingId, string Data1, string Data2, string Data3, string Status, string LongData)
        {
            return InsertActivityLog(paymentSettingId, Data1, Data2, Data3, Status, LongData, string.Empty, string.Empty);
        }

    }
}