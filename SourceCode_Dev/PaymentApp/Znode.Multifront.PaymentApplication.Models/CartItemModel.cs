﻿
namespace Znode.Multifront.PaymentApplication.Models
{
    /// <summary>
    /// To show product details on paypal express checkout page
    /// </summary>
    public class CartItemModel
    {
        public decimal ProductAmount { get; set; }

        public string ProductDescription { get; set; }
        public string ProductName { get; set; }
        public string ProductNumber { get; set; }

        public int ProductQuantity { get; set; }
    }
}
