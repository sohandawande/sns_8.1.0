﻿
namespace Znode.Multifront.PaymentApplication.Models
{
    /// <summary>
    /// This is the model for Gateway response
    /// </summary>
    public class GatewayResponseModel
    {
        public string CardAuthCode { get; set; }
        public string CustomerPaymentProfileId { get; set; }
        public string CustomerProfileId { get; set; }
        public string GatewayResponseData{ get; set; }
        public string ReferenceNumber { get; set; }
        public string ResponseCode { get; set; }
        public string ResponseText { get; set; }
        public string Token { get; set; }
        public string TransactionId { get; set; }
        public string HostUrl { get; set; }

        public bool IsSuccess { get; set; }
        public ZnodePaymentStatus PaymentStatus { get; set; }
    }
}