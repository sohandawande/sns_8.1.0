﻿
namespace ZNode.Libraries.ECommerce.Payment
{
    /// <summary>
    /// Gateway Transaction Types
    /// </summary>
    public enum GatewayTransactionType
    {
        /// <summary>
        /// Represents the transaction type none
        /// </summary>
        NONE = 0,

        /// <summary>
        /// Represents the transaction type refund
        /// </summary>
        REFUND = 1,

        /// <summary>
        /// Represents the transaction type void
        /// </summary>
        VOID = 2,
    }
}
