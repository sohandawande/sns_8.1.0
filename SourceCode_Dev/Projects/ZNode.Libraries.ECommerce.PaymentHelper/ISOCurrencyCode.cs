
namespace ZNode.Libraries.ECommerce.Payment
{
    /// <summary>
    /// ISO Currency Code
    /// </summary>
    public enum ISOCurrencyCode
    {
        /// <summary>
        /// Represents the AED currency code
        /// </summary>
        AED = 784,

        /// <summary>
        /// Represents the AFN currency code
        /// </summary>
        AFN = 971,

        /// <summary>
        /// Represents the ALL currency code
        /// </summary>
        ALL = 008,

        /// <summary>
        /// Represents the AMD currency code
        /// </summary>
        AMD = 051,

        /// <summary>
        /// Represents the ANG currency code
        /// </summary>
        ANG = 532,

        /// <summary>
        /// Represents the AOA currency code
        /// </summary>
        AOA = 973,

        /// <summary>
        /// Represents the ARS currency code
        /// </summary>
        ARS = 032,

        /// <summary>
        /// Represents the AUD currency code
        /// </summary>
        AUD = 036,

        /// <summary>
        /// Represents the AWG currency code
        /// </summary>
        AWG = 533,

        /// <summary>
        /// Represents the AZN currency code
        /// </summary>
        AZN = 944,

        /// <summary>
        /// Represents the BAM currency code
        /// </summary>
        BAM = 977,

        /// <summary>
        /// Represents the BBD currency code
        /// </summary>
        BBD = 052,

        /// <summary>
        /// Represents the BDT currency code
        /// </summary>
        BDT = 050,

        /// <summary>
        /// Represents the BGN currency code
        /// </summary>
        BGN = 975,

        /// <summary>
        /// Represents the BHD currency code
        /// </summary>
        BHD = 048,

        /// <summary>
        /// Represents the BIF currency code
        /// </summary>
        BIF = 108,

        /// <summary>
        /// Represents the BMD currency code
        /// </summary>
        BMD = 060,

        /// <summary>
        /// Represents the BND currency code
        /// </summary>
        BND = 096,

        /// <summary>
        /// Represents the BOB currency code
        /// </summary>
        BOB = 068,

        /// <summary>
        /// Represents the BOV currency code
        /// </summary>
        BOV = 984,

        /// <summary>
        /// Represents the BRL currency code
        /// </summary>
        BRL = 986,

        /// <summary>
        /// Represents the BSD currency code
        /// </summary>
        BSD = 044,

        /// <summary>
        /// Represents the BTN currency code
        /// </summary>
        BTN = 064,

        /// <summary>
        /// Represents the BWP currency code
        /// </summary>
        BWP = 072,

        /// <summary>
        /// Represents the BYR currency code
        /// </summary>
        BYR = 974,

        /// <summary>
        /// Represents the BZD currency code
        /// </summary>
        BZD = 084,

        /// <summary>
        /// Represents the CAD currency code
        /// </summary>
        CAD = 124,

        /// <summary>
        /// Represents the CDF currency code
        /// </summary>
        CDF = 976,

        /// <summary>
        /// Represents the CHE currency code
        /// </summary>
        CHE = 947,

        /// <summary>
        /// Represents the CHF currency code
        /// </summary>
        CHF = 756,

        /// <summary>
        /// Represents the CHW currency code
        /// </summary>
        CHW = 948,

        /// <summary>
        /// Represents the CLF currency code
        /// </summary>
        CLF = 990,

        /// <summary>
        /// Represents the CLP currency code
        /// </summary>
        CLP = 152,

        /// <summary>
        /// Represents the CNY currency code
        /// </summary>
        CNY = 156,

        /// <summary>
        /// Represents the COP currency code
        /// </summary>
        COP = 170,

        /// <summary>
        /// Represents the COU currency code
        /// </summary>
        COU = 970,

        /// <summary>
        /// Represents the CRC currency code
        /// </summary>
        CRC = 188,

        /// <summary>
        /// Represents the CUP currency code
        /// </summary>
        CUP = 192,

        /// <summary>
        /// Represents the CVE currency code
        /// </summary>
        CVE = 132,

        /// <summary>
        /// Represents the CZK currency code
        /// </summary>
        CZK = 203,

        /// <summary>
        /// Represents the DJF currency code
        /// </summary>
        DJF = 262,

        /// <summary>
        /// Represents the DKK currency code
        /// </summary>
        DKK = 208,

        /// <summary>
        /// Represents the DOP currency code
        /// </summary>
        DOP = 214,

        /// <summary>
        /// Represents the DZD currency code
        /// </summary>
        DZD = 012,

        /// <summary>
        /// Represents the EEK currency code
        /// </summary>
        EEK = 233,

        /// <summary>
        /// Represents the EGP currency code
        /// </summary>
        EGP = 818,

        /// <summary>
        /// Represents the ERN currency code
        /// </summary>
        ERN = 232,

        /// <summary>
        /// Represents the ETB currency code
        /// </summary>
        ETB = 230,

        /// <summary>
        /// Represents the EUR currency code
        /// </summary>
        EUR = 978,

        /// <summary>
        /// Represents the FJD currency code
        /// </summary>
        FJD = 242,

        /// <summary>
        /// Represents the FKP currency code
        /// </summary>
        FKP = 238,

        /// <summary>
        /// Represents the GBP currency code
        /// </summary>
        GBP = 826,

        /// <summary>
        /// Represents the GEL currency code
        /// </summary>
        GEL = 981,

        /// <summary>
        /// Represents the GHS currency code
        /// </summary>
        GHS = 936,

        /// <summary>
        /// Represents the GIP currency code
        /// </summary>
        GIP = 292,

        /// <summary>
        /// Represents the GMD currency code
        /// </summary>
        GMD = 270,

        /// <summary>
        /// Represents the GNF currency code
        /// </summary>
        GNF = 324,

        /// <summary>
        /// Represents the GTQ currency code
        /// </summary>
        GTQ = 320,

        /// <summary>
        /// Represents the GYD currency code
        /// </summary>
        GYD = 328,

        /// <summary>
        /// Represents the HKD currency code
        /// </summary>
        HKD = 344,

        /// <summary>
        /// Represents the HNL currency code
        /// </summary>
        HNL = 340,

        /// <summary>
        /// Represents the HRK currency code
        /// </summary>
        HRK = 191,

        /// <summary>
        /// Represents the HTG currency code
        /// </summary>
        HTG = 332,

        /// <summary>
        /// Represents the HUF currency code
        /// </summary>
        HUF = 348,

        /// <summary>
        /// Represents the IDR currency code
        /// </summary>
        IDR = 360,

        /// <summary>
        /// Represents the ILS currency code
        /// </summary>
        ILS = 376,

        /// <summary>
        /// Represents the INR currency code
        /// </summary>
        INR = 356,

        /// <summary>
        /// Represents the IQD currency code
        /// </summary>
        IQD = 368,

        /// <summary>
        /// Represents the IRR currency code
        /// </summary>
        IRR = 364,

        /// <summary>
        /// Represents the ISK currency code
        /// </summary>
        ISK = 352,

        /// <summary>
        /// Represents the JMD currency code
        /// </summary>
        JMD = 388,

        /// <summary>
        /// Represents the JOD currency code
        /// </summary>
        JOD = 400,

        /// <summary>
        /// Represents the JPY currency code
        /// </summary>
        JPY = 392,

        /// <summary>
        /// Represents the KES currency code
        /// </summary>
        KES = 404,

        /// <summary>
        /// Represents the KGS currency code
        /// </summary>
        KGS = 417,

        /// <summary>
        /// Represents the KHR currency code
        /// </summary>
        KHR = 116,

        /// <summary>
        /// Represents the KMF currency code
        /// </summary>
        KMF = 174,

        /// <summary>
        /// Represents the KPW currency code
        /// </summary>
        KPW = 408,

        /// <summary>
        /// Represents the KRW currency code
        /// </summary>
        KRW = 410,

        /// <summary>
        /// Represents the KWD currency code
        /// </summary>
        KWD = 414,

        /// <summary>
        /// Represents the KYD currency code
        /// </summary>
        KYD = 136,

        /// <summary>
        /// Represents the KZT currency code
        /// </summary>
        KZT = 398,

        /// <summary>
        /// Represents the LAK currency code
        /// </summary>
        LAK = 418,

        /// <summary>
        /// Represents the LBP currency code
        /// </summary>
        LBP = 422,

        /// <summary>
        /// Represents the LKR currency code
        /// </summary>
        LKR = 144,

        /// <summary>
        /// Represents the LRD currency code
        /// </summary>
        LRD = 430,

        /// <summary>
        /// Represents the LSL currency code
        /// </summary>
        LSL = 426,

        /// <summary>
        /// Represents the LTL currency code
        /// </summary>
        LTL = 440,

        /// <summary>
        /// Represents the LVL currency code
        /// </summary>
        LVL = 428,

        /// <summary>
        /// Represents the LYD currency code
        /// </summary>
        LYD = 434,

        /// <summary>
        /// Represents the MAD currency code
        /// </summary>
        MAD = 504,

        /// <summary>
        /// Represents the MDL currency code
        /// </summary>
        MDL = 498,

        /// <summary>
        /// Represents the MGA currency code
        /// </summary>
        MGA = 969,

        /// <summary>
        /// Represents the MKD currency code
        /// </summary>
        MKD = 807,

        /// <summary>
        /// Represents the MMK currency code
        /// </summary>
        MMK = 104,

        /// <summary>
        /// Represents the MNT currency code
        /// </summary>
        MNT = 496,

        /// <summary>
        /// Represents the MOP currency code
        /// </summary>
        MOP = 446,

        /// <summary>
        /// Represents the MRO currency code
        /// </summary>
        MRO = 478,

        /// <summary>
        /// Represents the MUR currency code
        /// </summary>
        MUR = 480,

        /// <summary>
        /// Represents the MVR currency code
        /// </summary>
        MVR = 462,

        /// <summary>
        /// Represents the AED currency code
        /// </summary>
        MWK = 454,

        /// <summary>
        /// Represents the MXN currency code
        /// </summary>
        MXN = 484,

        /// <summary>
        /// Represents the MXV currency code
        /// </summary>
        MXV = 979,

        /// <summary>
        /// Represents the MYR currency code
        /// </summary>
        MYR = 458,

        /// <summary>
        /// Represents the MZN currency code
        /// </summary>
        MZN = 943,

        /// <summary>
        /// Represents the NAD currency code
        /// </summary>
        NAD = 516,

        /// <summary>
        /// Represents the NGN currency code
        /// </summary>
        NGN = 566,

        /// <summary>
        /// Represents the NIO currency code
        /// </summary>
        NIO = 558,

        /// <summary>
        /// Represents the NOK currency code
        /// </summary>
        NOK = 578,

        /// <summary>
        /// Represents the NPR currency code
        /// </summary>
        NPR = 524,

        /// <summary>
        /// Represents the NZD currency code
        /// </summary>
        NZD = 554,

        /// <summary>
        /// Represents the OMR currency code
        /// </summary>
        OMR = 512,

        /// <summary>
        /// Represents the PAB currency code
        /// </summary>
        PAB = 590,

        /// <summary>
        /// Represents the PEN currency code
        /// </summary>
        PEN = 604,

        /// <summary>
        /// Represents the PGK currency code
        /// </summary>
        PGK = 598,

        /// <summary>
        /// Represents the PHP currency code
        /// </summary>
        PHP = 608,

        /// <summary>
        /// Represents the PKR currency code
        /// </summary>
        PKR = 586,

        /// <summary>
        /// Represents the PLN currency code
        /// </summary>
        PLN = 985,

        /// <summary>
        /// Represents the PYG currency code
        /// </summary>
        PYG = 600,

        /// <summary>
        /// Represents the QAR currency code
        /// </summary>
        QAR = 634,

        /// <summary>
        /// Represents the RON currency code
        /// </summary>
        RON = 946,

        /// <summary>
        /// Represents the RSD currency code
        /// </summary>
        RSD = 941,

        /// <summary>
        /// Represents the RUB currency code
        /// </summary>
        RUB = 643,

        /// <summary>
        /// Represents the RWF currency code
        /// </summary>
        RWF = 646,

        /// <summary>
        /// Represents the SAR currency code
        /// </summary>
        SAR = 682,

        /// <summary>
        /// Represents the SBD currency code
        /// </summary>
        SBD = 090,

        /// <summary>
        /// Represents the SCR currency code
        /// </summary>
        SCR = 690,

        /// <summary>
        /// Represents the SDG currency code
        /// </summary>
        SDG = 938,

        /// <summary>
        /// Represents the SEK currency code
        /// </summary>
        SEK = 752,

        /// <summary>
        /// Represents the SGD currency code
        /// </summary>
        SGD = 702,

        /// <summary>
        /// Represents the SHP currency code
        /// </summary>
        SHP = 654,

        /// <summary>
        /// Represents the SKK currency code
        /// </summary>
        SKK = 703,

        /// <summary>
        /// Represents the SLL currency code
        /// </summary>
        SLL = 694,

        /// <summary>
        /// Represents the SOS currency code
        /// </summary>
        SOS = 706,

        /// <summary>
        /// Represents the SRD currency code
        /// </summary>
        SRD = 968,

        /// <summary>
        /// Represents the STD currency code
        /// </summary>
        STD = 678,

        /// <summary>
        /// Represents the SYP currency code
        /// </summary>
        SYP = 760,

        /// <summary>
        /// Represents the SZL currency code
        /// </summary>
        SZL = 748,

        /// <summary>
        /// Represents the THB currency code
        /// </summary>
        THB = 764,

        /// <summary>
        /// Represents the TJS currency code
        /// </summary>
        TJS = 972,

        /// <summary>
        /// Represents the TMM currency code
        /// </summary>
        TMM = 795,

        /// <summary>
        /// Represents the TND currency code
        /// </summary>
        TND = 788,

        /// <summary>
        /// Represents the TOP currency code
        /// </summary>
        TOP = 776,

        /// <summary>
        /// Represents the TRY currency code
        /// </summary>
        TRY = 949,

        /// <summary>
        /// Represents the TTD currency code
        /// </summary>
        TTD = 780,

        /// <summary>
        /// Represents the TWD currency code
        /// </summary>
        TWD = 901,

        /// <summary>
        /// Represents the TZS currency code
        /// </summary>
        TZS = 834,

        /// <summary>
        /// Represents the UAH currency code
        /// </summary>
        UAH = 980,

        /// <summary>
        /// Represents the UGX currency code
        /// </summary>
        UGX = 800,

        /// <summary>
        /// Represents the USD currency code
        /// </summary>
        USD = 840,

        /// <summary>
        /// Represents the USN currency code
        /// </summary>
        USN = 997,

        /// <summary>
        /// Represents the USS currency code
        /// </summary>
        USS = 998,

        /// <summary>
        /// Represents the UYU currency code
        /// </summary>
        UYU = 858,

        /// <summary>
        /// Represents the UZS currency code
        /// </summary>
        UZS = 860,

        /// <summary>
        /// Represents the VEF currency code
        /// </summary>
        VEF = 937,

        /// <summary>
        /// Represents the VND currency code
        /// </summary>
        VND = 704,

        /// <summary>
        /// Represents the VUV currency code
        /// </summary>
        VUV = 548,

        /// <summary>
        /// Represents the WST currency code
        /// </summary>
        WST = 882,

        /// <summary>
        /// Represents the XAF currency code
        /// </summary>
        XAF = 950,

        /// <summary>
        /// Represents the XAG currency code
        /// </summary>
        XAG = 961,

        /// <summary>
        /// Represents the XAU currency code
        /// </summary>
        XAU = 959,

        /// <summary>
        /// Represents the XBA currency code
        /// </summary>
        XBA = 955,

        /// <summary>
        /// Represents the XBB currency code
        /// </summary>
        XBB = 956,

        /// <summary>
        /// Represents the XBC currency code
        /// </summary>
        XBC = 957,

        /// <summary>
        /// Represents the XBD currency code
        /// </summary>
        XBD = 958,

        /// <summary>
        /// Represents the XCD currency code
        /// </summary>
        XCD = 951,

        /// <summary>
        /// Represents the XDR currency code
        /// </summary>
        XDR = 960,

        /// <summary>
        /// Represents the XOF currency code
        /// </summary>
        XOF = 952,

        /// <summary>
        /// Represents the XPD currency code
        /// </summary>
        XPD = 964,

        /// <summary>
        /// Represents the XPF currency code
        /// </summary>
        XPF = 953,

        /// <summary>
        /// Represents the XPT currency code
        /// </summary>
        XPT = 962,

        /// <summary>
        /// Represents the XTS currency code
        /// </summary>
        XTS = 963,

        /// <summary>
        /// Represents the XXX currency code
        /// </summary>
        XXX = 999,

        /// <summary>
        /// Represents the YER currency code
        /// </summary>
        YER = 886,

        /// <summary>
        /// Represents the ZAR currency code
        /// </summary>
        ZAR = 710,

        /// <summary>
        /// Represents the ZMK currency code
        /// </summary>
        ZMK = 894,

        /// <summary>
        /// Represents the ZWD currency code
        /// </summary>
        ZWD = 716,
    }
}
