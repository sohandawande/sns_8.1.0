﻿using ZNode.Libraries.ECommerce.Entities;

namespace ZNode.Libraries.ECommerce.Payment
{
    /// <summary>
    /// Represents the gateway 2co response
    /// </summary>
    public class Gateway2COResponse
    {
        /// <summary>
        /// Gets or set the Upper Case Text; Indicates type of message (ORDER_CREATED,
        /// FRAUD_STATUS_CHANGED, SHIP_STATUS_CHANGED, INVOICE_STATUS_CHANGED,
        /// REFUND_ISSUED, RECURRING_INSTALLMENT_SUCCESS,
        /// RECURRING_INSTALLMENT_FAILED, RECURRING_STOPPED,
        /// RECURRING_COMPLETE, or RECURRING_RESTARTED )
        /// </summary>
        public string MessageType
        {
            get
            {
                return (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form.Get("message_type"))) ?
                          System.Web.HttpContext.Current.Request.Form.Get("message_type") :
                          System.Web.HttpContext.Current.Request.QueryString.Get("message_type");
            }
        }

        /// <summary>
        /// Gets Text; Human readable description of message_type
        /// </summary>
        public string MessageDescription
        {
            get
            {
                return (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form.Get("message_description"))) ?
                    System.Web.HttpContext.Current.Request.Form.Get("message_description") :
                    System.Web.HttpContext.Current.Request.QueryString.Get("message_description"); 
            }
        }

        /// <summary>
        /// Gets Timestamp of event; format YYYY-MM-DD HH:MM:SS
        /// </summary>
        public System.DateTime TimeStamp
        {
            get
            {
                System.DateTime value = System.DateTime.MinValue;
                System.DateTime.TryParse(System.Web.HttpContext.Current.Request.Form.Get("timestamp"), out value);
                return value;
            }
        }

        /// <summary>
        /// Gets Case Text; UPPERCASE(MD5_ENCRYPTED(sale_id + vendor_id + invoice_id + Secret Word)); Can be used to validate authenticity of message
        /// </summary>
        public string MD5Hash
        {
            get
            {
                return (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form.Get("md5_hash"))) ?
                    System.Web.HttpContext.Current.Request.Form.Get("md5_hash") :
                    System.Web.HttpContext.Current.Request.QueryString.Get("key");
            }
        }

        /// <summary>
        /// Gets Numeric; This number is incremented for each message sent to a given vendor
        /// </summary>
        public string MessageId
        {
            get
            {
                return (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form.Get("message_id"))) ?
                    System.Web.HttpContext.Current.Request.Form.Get("message_id") :
                    System.Web.HttpContext.Current.Request.QueryString.Get("message_id");
            }
        }

        /// <summary>
        /// Gets Numeric; Indicates the number of parameters sent in message
        /// </summary>
        public int KeyCount
        {
            get
            {
                int value = 0;
                int.TryParse(System.Web.HttpContext.Current.Request.Form.Get("key_count"), out value);
                return value;
            }
        }

        /// <summary>
        /// Gets Numeric; Vendor account number
        /// </summary>
        public string VendorId
        {
            get
            {
                return (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form.Get("vendor_id"))) ?
                    System.Web.HttpContext.Current.Request.Form.Get("vendor_id") :
                    System.Web.HttpContext.Current.Request.QueryString.Get("sid");
            }
        }

        /// <summary>
        /// Gets Numeric; 2Checkout sale number
        /// </summary>
        public string SaleId
        {
            get
            {
                return (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form.Get("sale_id"))) ?
                    System.Web.HttpContext.Current.Request.Form.Get("sale_id") :
                    System.Web.HttpContext.Current.Request.QueryString.Get("order_number");
            }
        }

        /// <summary>
        /// Gets Date of sale; format YYYY-MM-DD HH:MM:SS
        /// </summary>
        public System.DateTime SaleDatePlaced
        {
            get
            {
                System.DateTime value = System.DateTime.MinValue;
                System.DateTime.TryParse(System.Web.HttpContext.Current.Request.Form.Get("sale_date_placed"), out value);
                return value;
            }
        }

        /// <summary>
        /// Gets Text; Custom order id provided by vendor, if available. ("merchant_order_id" can be provided in the CGI handoff to 2Checkout's purchase routine)
        /// </summary>
        public string VendorOrderId
        {
            get
            {
                return (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form.Get("vendor_order_id"))) ?
                    System.Web.HttpContext.Current.Request.Form.Get("vendor_order_id") :
                    System.Web.HttpContext.Current.Request.QueryString.Get("merchant_order_id"); 
            }
        }

        /// <summary>
        /// Gets Numeric; 2Checkout invoice number; Each sale can have several invoices, most commonly one per installment billed on a recurring order.
        /// </summary>
        public string InvoiceId
        {
            get
            {
                return (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form.Get("invoice_id"))) ?
                    System.Web.HttpContext.Current.Request.Form.Get("invoice_id") :
                    System.Web.HttpContext.Current.Request.QueryString.Get("invoice_id"); 
            }
        }

        /// <summary>
        /// Gets Numeric; recurring=1 if any item on the invoice is a recurring item, 0 otherwise
        /// </summary>
        public int Recurring
        {
            get
            {
                int value = 0;
                int.TryParse(System.Web.HttpContext.Current.Request.Form.Get("recurring"), out value);
                return value;
            }
        }

        /// <summary>
        /// Gets Lower Case Text; Customer’s payment method (credit card, online check, paypal, paypal ec, OR paypal pay later)
        /// </summary>
        public string PaymentType
        {
            get
            {
                return (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form.Get("payment_type"))) ?
                    System.Web.HttpContext.Current.Request.Form.Get("payment_type") :
                    System.Web.HttpContext.Current.Request.QueryString.Get("pay_method");
            }
        }

        /// <summary>
        /// Gets Upper Case Text; 3-Letter ISO code for vendor currency
        /// </summary>
        public string ListCurrency
        {
            get
            {
                return (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form.Get("list_currency"))) ?
                    System.Web.HttpContext.Current.Request.Form.Get("list_currency") :
                    System.Web.HttpContext.Current.Request.QueryString.Get("list_currency"); 
            }
        }

        /// <summary>
        /// Gets Upper Case Text; 3-Letter ISO code for customer currency
        /// </summary>
        public string CustCurrency
        {
            get
            {
                return (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form.Get("cust_currency"))) ?
                    System.Web.HttpContext.Current.Request.Form.Get("cust_currency") :
                    System.Web.HttpContext.Current.Request.QueryString.Get("cust_currency");
            }
        }

        /// <summary>
        /// Gets The date credit authorization will expire; format YYYY-MM-DD
        /// </summary>
        public string AuthExp
        {
            get
            {
                return (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form.Get("auth_exp"))) ?
                    System.Web.HttpContext.Current.Request.Form.Get("auth_exp") :
                    System.Web.HttpContext.Current.Request.QueryString.Get("auth_exp");
            }
        }

        /// <summary>
        /// Gets Lower Case Text; Status of a transaction (approved, pending, deposited, or declined)
        /// </summary>
        public string InvoiceStatus
        {
            get
            {
                return (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form.Get("invoice_status"))) ?
                    System.Web.HttpContext.Current.Request.Form.Get("invoice_status") :
                    System.Web.HttpContext.Current.Request.QueryString.Get("invoice_status");
            }
        }

        /// <summary>
        /// Gets Lower Case Text; Status of 2Checkout fraud review (pass, fail, or wait); This parameter may be empty for some sales
        /// </summary>
        public string FraudStatus
        {
            get
            {
                return (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form.Get("fraud_status"))) ?
                    System.Web.HttpContext.Current.Request.Form.Get("fraud_status") :
                    System.Web.HttpContext.Current.Request.QueryString.Get("fraud_status");
            }
        }

        /// <summary>
        /// Gets Numeric; Total in vendor pricing currency; format as appropriate to currency (2 decimal places for most, integer for JPY)
        /// </summary>
        public decimal InvoiceListAmount
        {
            get
            {
                decimal value = 0;

                if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form.Get("invoice_list_amount")))
                {
                    decimal.TryParse(System.Web.HttpContext.Current.Request.Form.Get("invoice_list_amount"), out value);
                }
                else
                {
                    decimal.TryParse(System.Web.HttpContext.Current.Request.QueryString.Get("total"), out value);
                }

                return value;
            }
        }

        /// <summary>
        /// Gets Numeric, Total in US Dollars; format with 2 decimal places
        /// </summary>
        public decimal InvoiceUSDAmount
        {
            get
            {
                decimal value = 0;
                decimal.TryParse(System.Web.HttpContext.Current.Request.Form.Get("invoice_usd_amount"), out value);
                return value;
            }
        }

        /// <summary>
        /// Gets Numeric; Total in customer currency; format as appropriate to currency (2 decimal places for most, integer for JPY)
        /// </summary>
        public decimal InvoiceCustAmount
        {
            get
            {
                decimal value = 0;
                decimal.TryParse(System.Web.HttpContext.Current.Request.Form.Get("invoice_cust_amount"), out value);
                return value;
            }
        }

        /// <summary>
        /// Gets Text; Customer’s first name (may not be available on older sales)
        /// </summary>
        public string CustomerFirstName
        {
            get
            {
                return (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form.Get("customer_first_name"))) ?
                    System.Web.HttpContext.Current.Request.Form.Get("customer_first_name") :
                    System.Web.HttpContext.Current.Request.QueryString.Get("first_name");
            }
        }

        /// <summary>
        /// Gets Text; Customer’s last name (may not be available on older sales)
        /// </summary>
        public string CustomerLastName
        {
            get
            {
                return (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form.Get("customer_last_name"))) ?
                    System.Web.HttpContext.Current.Request.Form.Get("customer_last_name") :
                    System.Web.HttpContext.Current.Request.QueryString.Get("last_name");
            }
        }

        /// <summary>
        /// Gets Text; Customer’s full name (name as it appears on credit card)
        /// </summary>
        public string CustomerName
        {
            get
            {
                return (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form.Get("customer_name"))) ?
                    System.Web.HttpContext.Current.Request.Form.Get("customer_name") :
                    System.Web.HttpContext.Current.Request.QueryString.Get("card_holder_name");
            }
        }

        /// <summary>
        /// Gets Text; Customer’s email address
        /// </summary>
        public string CustomerEmail
        {
            get
            {
                return (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form.Get("customer_email"))) ?
                    System.Web.HttpContext.Current.Request.Form.Get("customer_email") :
                    System.Web.HttpContext.Current.Request.QueryString.Get("email");
            }
        }

        /// <summary>
        /// Gets Numeric; Customer’s phone number; all but digits stripped out
        /// </summary>
        public string CustomerPhone
        {
            get
            {
                return (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form.Get("customer_phone"))) ?
                    System.Web.HttpContext.Current.Request.Form.Get("customer_phone") :
                    System.Web.HttpContext.Current.Request.QueryString.Get("phone");
            }
        }

        /// <summary>
        /// Gets Text; Customer’s IP address at time of sale
        /// </summary>
        public string CustomerIP
        {
            get
            {
                return (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form.Get("customer_ip"))) ?
                    System.Web.HttpContext.Current.Request.Form.Get("customer_ip") :
                    System.Web.HttpContext.Current.Request.QueryString.Get("customer_ip");
            }
        }

        /// <summary>
        /// Gets Text; Country of record for customer's IP address at time of sale; Please note in some cases
        /// what is returned is not a country, Ex. Satellite Provider
        /// </summary>
        public string CustomerIPCountry
        {
            get
            {
                return (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form.Get("customer_ip_country"))) ?
                    System.Web.HttpContext.Current.Request.Form.Get("customer_ip_country") :
                    System.Web.HttpContext.Current.Request.QueryString.Get("ip_country");
            }
        }

        /// <summary>
        /// Gets Text; Billing street address
        /// </summary>
        public string BillStreetAddress
        {
            get
            {
                return (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form.Get("bill_street_address"))) ?
                    System.Web.HttpContext.Current.Request.Form.Get("bill_street_address") :
                    System.Web.HttpContext.Current.Request.QueryString.Get("street_address");
            }
        }

        /// <summary>
        /// Gets Text; Billing street address line 2
        /// </summary>
        public string BillStreetAddress2
        {
            get
            {
                return (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form.Get("bill_street_address2"))) ?
                    System.Web.HttpContext.Current.Request.Form.Get("bill_street_address2") :
                    System.Web.HttpContext.Current.Request.QueryString.Get("street_address2");
            }
        }

        /// <summary>
        /// Gets Text; Billing address city
        /// </summary>
        public string BillCity
        {
            get
            {
                return (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form.Get("bill_city"))) ?
                    System.Web.HttpContext.Current.Request.Form.Get("bill_city") :
                    System.Web.HttpContext.Current.Request.QueryString.Get("city");
            }
        }

        /// <summary>
        /// Gets Text; Billing address state or province
        /// </summary>
        public string BillState
        {
            get
            {
                return (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form.Get("bill_state"))) ?
                    System.Web.HttpContext.Current.Request.Form.Get("bill_state") :
                    System.Web.HttpContext.Current.Request.QueryString.Get("state");
            }
        }

        /// <summary>
        /// Gets Text; Billing address postal code
        /// </summary>
        public string BillPostalCode
        {
            get
            {
                return (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form.Get("bill_postal_code"))) ?
                    System.Web.HttpContext.Current.Request.Form.Get("bill_postal_code") :
                    System.Web.HttpContext.Current.Request.QueryString.Get("zip");
            }
        }

        /// <summary>
        /// Gets Text; 3-Letter ISO country code of billing address
        /// </summary>
        public string BillCountry
        {
            get
            {
                return (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form.Get("bill_country"))) ?
                    System.Web.HttpContext.Current.Request.Form.Get("bill_country") :
                    System.Web.HttpContext.Current.Request.QueryString.Get("country");
            }
        }

        /// <summary>
        /// Gets Lower Case Text; Value will be not_shipped, shipped, or empty (if intangible / does not need shipped)
        /// </summary>
        public string ShipStatus
        {
            get
            {
                return (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form.Get("ship_status"))) ?
                    System.Web.HttpContext.Current.Request.Form.Get("ship_status") :
                    System.Web.HttpContext.Current.Request.QueryString.Get("ship_status");
            }
        }

        /// <summary>
        /// Gets Lower Case Text; Value will be shipping name used in the order.)
        /// </summary>
        public string ShipMethod
        {
            get
            {
                return (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form.Get("ship_method"))) ?
                    System.Web.HttpContext.Current.Request.Form.Get("ship_method") :
                    System.Web.HttpContext.Current.Request.QueryString.Get("ship_method");
            }
        }

        /// <summary>
        /// Gets Text; Tracking Number as entered in Vendor Admin
        /// </summary>
        public string ShipTrackingNumber
        {
            get
            {
                return (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form.Get("ship_tracking_number"))) ?
                    System.Web.HttpContext.Current.Request.Form.Get("ship_tracking_number") :
                    System.Web.HttpContext.Current.Request.QueryString.Get("ship_tracking_number");
            }
        }

        /// <summary>
        /// Gets Text; Shipping Recipient’s name (as it should appear on shipping label)
        /// </summary>
        public string ShipName
        {
            get
            {
                return (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form.Get("ship_name"))) ?
                    System.Web.HttpContext.Current.Request.Form.Get("ship_name") :
                    System.Web.HttpContext.Current.Request.QueryString.Get("ship_name");
            }
        }

        /// <summary>
        /// Gets Text; Shipping street address
        /// </summary>
        public string ShipStreetAddress
        {
            get
            {
                return (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form.Get("ship_street_address"))) ?
                    System.Web.HttpContext.Current.Request.Form.Get("ship_street_address") :
                    System.Web.HttpContext.Current.Request.QueryString.Get("ship_street_address");
            }
        }

        /// <summary>
        /// Gets Text; Shipping street address line 2
        /// </summary>
        public string ShipStreetAddress2
        {
            get
            {
                return (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form.Get("ship_street_address2"))) ?
                    System.Web.HttpContext.Current.Request.Form.Get("ship_street_address2") :
                    System.Web.HttpContext.Current.Request.QueryString.Get("ship_street_address2");
            }
        }

        /// <summary>
        /// Gets Text; Shipping address city
        /// </summary>
        public string ShipCity
        {
            get
            {
                return (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form.Get("ship_city"))) ?
                    System.Web.HttpContext.Current.Request.Form.Get("ship_city") :
                    System.Web.HttpContext.Current.Request.QueryString.Get("ship_city");
            }
        }

        /// <summary>
        /// Gets Text; Shipping address state or province
        /// </summary>
        public string ShipState
        {
            get
            {
                return (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form.Get("ship_state"))) ?
                    System.Web.HttpContext.Current.Request.Form.Get("ship_state") :
                    System.Web.HttpContext.Current.Request.QueryString.Get("ship_state");
            }
        }

        /// <summary>
        /// Gets Text; Shipping address postal code
        /// </summary>
        public string ShipPostalCode
        {
            get
            {
                return (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form.Get("ship_postal_code"))) ?
                    System.Web.HttpContext.Current.Request.Form.Get("ship_postal_code") :
                    System.Web.HttpContext.Current.Request.QueryString.Get("ship_zip");
            }
        }

        /// <summary>
        /// Gets Upper Case Text; 3-Letter ISO country code of shipping address
        /// </summary>
        public string ShipCountry
        {
            get
            {
                return (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form.Get("ship_country"))) ?
                    System.Web.HttpContext.Current.Request.Form.Get("ship_country") :
                    System.Web.HttpContext.Current.Request.QueryString.Get("ship_country");
            }
        }

        /// <summary>
        /// Gets Numeric; Indicates how many numbered sets of item parameters to expect
        /// </summary>
        public int ItemCount
        {
            get
            {
                int value = 0;
                int.TryParse(System.Web.HttpContext.Current.Request.Form.Get("item_count"), out value);
                return value;
            }
        }       

        /// <summary>
        /// Gets Text; Product name
        /// </summary>
        /// <param name="idx"></param>
        /// <returns></returns>
        public string GetItemName(int idx)
        {
            string sIdx = string.Empty;
            if (idx > 0) sIdx = idx.ToString();
            return (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form.Get(string.Format("item_name_{0}", sIdx)))) ?
                    System.Web.HttpContext.Current.Request.Form.Get(string.Format("item_name_{0}", sIdx)) :
                    System.Web.HttpContext.Current.Request.QueryString.Get(string.Format("product_description{0}", sIdx));
        }

        /// <summary>
        /// Gets Text; quantity
        /// </summary>
        /// <param name="idx">index value</param>
        /// <returns>Returns the quantity</returns>
        public int GetQuantity(int idx)
        {
            string sIdx = string.Empty;
            if (idx > 0) sIdx = idx.ToString();
            int value = 0;
            int.TryParse(System.Web.HttpContext.Current.Request.QueryString.Get(string.Format("quantity{0}", sIdx)), out value);
            if (value == 0) value = 1;
            return value;
        }

        /// <summary>
        /// Gets Text; Vendor product id
        /// </summary>
        /// <param name="idx">index value</param>
        /// <returns>Returns the item id</returns>
        public string GetItemId(int idx)
        {
            string sIdx = string.Empty;
            if (idx > 0) sIdx = idx.ToString();
            return (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form.Get(string.Format("item_id_{0}", idx)))) ?
                System.Web.HttpContext.Current.Request.Form.Get(string.Format("item_id_{0}", idx)) :
                System.Web.HttpContext.Current.Request.QueryString.Get(string.Format("product_id{0}", sIdx));
        }

        /// <summary>
        /// Gets Numeric; Total in vendor pricing currency; format as appropriate to currency (2 decimal places for most, integer for JPY)
        /// </summary>
        /// <param name="idx">index value</param>
        /// <returns>Returns the get item list amount</returns>
        public decimal GetItemListAmount(int idx)
        {
            decimal value = 0;
            decimal.TryParse(System.Web.HttpContext.Current.Request.Form.Get(string.Format("item_list_amount_{0}", idx)), out value);
            return value;
        }

        /// <summary>
        /// Gets Numeric, Total in US Dollars; format with 2 decimal places
        /// </summary>
        /// <param name="idx">index value</param>
        /// <returns>Returns the item usd amount</returns>
        public decimal GetItemUSDAmount(int idx)
        {
            decimal value = 0;
            decimal.TryParse(System.Web.HttpContext.Current.Request.Form.Get(string.Format("item_usd_amount_{0}", idx)), out value);
            return value;
        }

        /// <summary>
        /// Gets Numeric; Total in customer currency; format as appropriate to currency (2 decimal places for most, integer for JPY)
        /// </summary>
        /// <param name="idx">index value</param>
        /// <returns>Returns the item cust amount</returns>
        public decimal GetItemCustAmount(int idx)
        {
            decimal value = 0;
            decimal.TryParse(System.Web.HttpContext.Current.Request.Form.Get(string.Format("item_cust_amount_{0}", idx)), out value);
            return value;
        }

        /// <summary>
        /// Gets Option Name (Add-On Name)
        /// </summary>
        /// <param name="idx"></param>
        /// <returns></returns>
        public string GetOptionName(int idx)
        {
            string sIdx = string.Empty;
            if (idx > 0) sIdx = idx.ToString();
            return (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form.Get(string.Format("product_id{0}_option_name", sIdx)))) ?
                System.Web.HttpContext.Current.Request.Form.Get(string.Format("product_id{0}_option_name", sIdx)) :
                System.Web.HttpContext.Current.Request.QueryString.Get(string.Format("product_id{0}_option_name", sIdx));
        }

        /// <summary>
        /// Gets Option Value Name (Add-On Value Name)
        /// </summary>
        /// <param name="idx"></param>
        /// <returns></returns>
        public string GetOptionValue(int idx)
        {
            string sIdx = string.Empty;
            if (idx > 0) sIdx = idx.ToString();
            return (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form.Get(string.Format("product_id{0}_option_value", sIdx)))) ?
                System.Web.HttpContext.Current.Request.Form.Get(string.Format("product_id{0}_option_value", sIdx)) :
                System.Web.HttpContext.Current.Request.QueryString.Get(string.Format("product_id{0}_option_value", sIdx));
        }

        /// <summary>
        /// Lower Case Text; Indicates if item is a bill or refund; Value will be bill or refund
        /// </summary>
        /// <param name="idx"></param>
        /// <returns></returns>
        public string GetItemType(int idx)
        {
            return (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form.Get(string.Format("item_type_{0}", idx)))) ?
                System.Web.HttpContext.Current.Request.Form.Get(string.Format("item_type_{0}", idx)) :
                System.Web.HttpContext.Current.Request.QueryString.Get(string.Format("item_type_{0}", idx));
        }

        /// <summary>
        /// Gets Text; Product duration, how long it re-bills for Ex. 1 Year
        /// </summary>
        /// <param name="idx">index value</param>
        /// <returns>Returns the item duration</returns>
        public string GetItemDuration(int idx)
        {
            return (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form.Get(string.Format("item_duration_{0}", idx)))) ?
                System.Web.HttpContext.Current.Request.Form.Get(string.Format("item_duration_{0}", idx)) :
                System.Web.HttpContext.Current.Request.QueryString.Get(string.Format("item_duration_{0}", idx));
        }

        /// <summary>
        /// Gets Text; Product recurrence, how often it re-bills Ex. 1 Month
        /// </summary>
        /// <param name="idx">index value</param>
        /// <returns>Returns the item recurrence</returns>
        public string GetItemRecurrence(int idx)
        {
            return (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form.Get(string.Format("item_recurrence_{0}", idx)))) ?
                System.Web.HttpContext.Current.Request.Form.Get(string.Format("item_recurrence_{0}", idx)) :
                System.Web.HttpContext.Current.Request.QueryString.Get(string.Format("item_recurrence_{0}", idx));
        }

        /// <summary>
        /// Gets Numeric; Product price; format as appropriate to currency (2 decimal places for most, integer for JPY)
        /// </summary>
        /// <param name="idx">index value</param>
        /// <returns>Returns the item rec list amount</returns>
        public decimal GetItemRecListAmount(int idx)
        {
            decimal value = 0;
            decimal.TryParse(System.Web.HttpContext.Current.Request.Form.Get(string.Format("item_rec_list_amount_{0}", idx)), out value);
            return value;
        }

        /// <summary>
        /// Gets Lower Case Text; indicates status of recurring subscription: live, canceled, or completed
        /// </summary>
        /// <param name="idx">index value</param>
        /// <returns>Returns the item rec status</returns>
        public string GetItemRecStatus(int idx)
        {
            return (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form.Get(string.Format("item_rec_status_{0}", idx)))) ?
                System.Web.HttpContext.Current.Request.Form.Get(string.Format("item_rec_status_{0}", idx)) :
                System.Web.HttpContext.Current.Request.QueryString.Get(string.Format("item_rec_status_{0}", idx));
        }

        /// <summary>
        /// Gets Date of next recurring installment; format YYYY-MM-DD
        /// </summary>
        /// <param name="idx">index value</param>
        /// <returns>Returns the item rec date next</returns>
        public System.DateTime GetItemRecDateNext(int idx)
        {
            System.DateTime value = System.DateTime.MinValue;
            System.DateTime.TryParse(System.Web.HttpContext.Current.Request.Form.Get(string.Format("item_rec_date_next_{0}", idx)), out value);
            return value;
        }

        /// <summary>
        /// Gets Numeric; The number of successful recurring installments successfully billed
        /// </summary>
        /// <param name="idx">index value</param>
        /// <returns>Returns the item rec install billed</returns>
        public int GetItemRecInstallBilled(int idx)
        {
            int value = 0;
            int.TryParse(System.Web.HttpContext.Current.Request.Form.Get(string.Format("item_rec_install_billed_{0}", idx)), out value);
            return value;
        }

        /// <summary>
        /// Gets Numeric; The number of successful recurring installments successfully billed
        /// </summary>
        /// <param name="idx">index value</param>
        /// <returns>Returns the item rec install billed</returns>
        public string GetCustomValue(string key)
        {
            return (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.Form.Get(key))) ?
                System.Web.HttpContext.Current.Request.Form.Get(key) :
                System.Web.HttpContext.Current.Request.QueryString.Get(key);
        }

        /// <summary>
        /// Gets a payment status
        /// </summary>
        /// <returns>Payment status</returns>
        public ZNodePaymentStatus GetPaymentStatus()
        {
            ZNodePaymentStatus result = ZNodePaymentStatus.CREDIT_DECLINED;

            if (string.IsNullOrEmpty(this.MessageType))
            {
                if (System.Web.HttpContext.Current.Request.QueryString.Get("credit_card_processed").ToUpperInvariant() == "Y")
                {
                    result = ZNodePaymentStatus.CREDIT_PENDING;
                }
            }
            else
            {
                switch (this.MessageType.ToUpperInvariant())
                {
                    case "ORDER_CREATED":
                        {
                            result = ZNodePaymentStatus.CREDIT_PENDING;
                        }

                        break;
                    case "FRAUD_STATUS_CHANGED":
                        {
                            if (this.FraudStatus == "pass")
                            {
                                if (this.InvoiceStatus == "approved")
                                {
                                    result = ZNodePaymentStatus.CREDIT_CAPTURED;
                                }
                                else
                                {
                                    if (this.PaymentType == "paypal ec")
                                    {
                                        result = ZNodePaymentStatus.CREDIT_CAPTURED;
                                    }
                                }
                            }
                        }

                        break;
                    case "INVOICE_STATUS_CHANGED":
                        {
                        }

                        break;
                    case "REFUND_ISSUED":
                        {
                            result = ZNodePaymentStatus.CREDIT_REFUNDED;
                        }

                        break;
                    case "SHIP_STATUS_CHANGED":
                    case "RECURRING_INSTALLMENT_SUCCESS":
                    case "RECURRING_INSTALLMENT_FAILED":
                    case "RECURRING_STOPPED":
                    case "RECURRING_COMPLETE":
                    case "RECURRING_RESTARTED":
                        break;
                    default:
                        break;
                }
            }

            return result;
        }
    }
}
