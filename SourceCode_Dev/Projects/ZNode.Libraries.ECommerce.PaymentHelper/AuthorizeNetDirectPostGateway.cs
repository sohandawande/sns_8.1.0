﻿using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.ECommerce.Entities;
using System.Collections.Generic;
using ZNode.Libraries.ECommerce.UserAccount;
using System.Web;
using System.Configuration;
using System.Net;
using System;
using System.Text;
using System.Xml;
using System.Text.RegularExpressions;
using System.Collections;
using System.IO;
using System.Collections.Specialized;
public interface IZnodeGatewayAdapter
{
    string TransactionId { get; }
    IZnodeGatewayResult SubmitPaymentRequest(IZnodeGatewayAuguments args);
    IZnodeGatewayResult SubmitVoid(IZnodeGatewayAuguments args);
    IZnodeGatewayResult SubmitCredit(IZnodeGatewayAuguments args);
}

public interface IZnodeGatewayResult
{
    
}

public interface IZnodeGatewayAuguments
{
    
}
namespace ZNode.Libraries.ECommerce.Payment
{
    public class AuthorizeNetDirectPostGatewayAdapter : IZnodeGatewayAdapter
    {
        public string TransactionId
        {
            get { throw new System.NotImplementedException(); }
        }

        public IZnodeGatewayResult SubmitPaymentRequest(IZnodeGatewayAuguments args)
        {
            throw new System.NotImplementedException();
        }
        private string serverURL = System.Configuration.ConfigurationManager.AppSettings["AuthorizeNetDPM_path"];

        /// <summary>
        /// Submits credit card payment to a 2Checkout gateway
        /// </summary>
        /// <param name="Gateway">2CO PaymentSetting object</param>
        /// <param name="returnURL">Return URL after payment</param>
        /// <param name="insURL">INS Handler URL for payment notification</param>
        /// <returns>Returns true if payment is success else false</returns>
        public GatewayResponse SubmitPayment(GatewayInfo gateway, Address BillingAddress,Address ShippingAddress, CreditCard creditcard,ZNodeShoppingCart ShoppingCart)
        {
           
            string AuthNetVersion = "3.1"; // Contains CCV support
          
               string AuthNetLoginID = gateway.GatewayLoginID;
               string AuthNetTransKey = gateway.TransactionKey;
            GatewayResponse response = new GatewayResponse();         

         
            System.Collections.Specialized.NameValueCollection InputObject = new System.Collections.Specialized.NameValueCollection(30);
            System.Collections.Specialized.NameValueCollection ReturnObject = new System.Collections.Specialized.NameValueCollection(30);

            string timestamp = AuthorizeNet.Crypto.GenerateTimestamp().ToString();
            string sequence = AuthorizeNet.Crypto.GenerateSequence();

            string billingAddress = BillingAddress.Street1 + " " + BillingAddress.Street2;
            string shippingaddress = ShippingAddress.Street1 + " " + ShippingAddress.Street2;
            billingAddress = Regex.Replace(billingAddress, "[^0-9a-zA-Z]+", " ");
            shippingaddress = Regex.Replace(shippingaddress, "[^0-9a-zA-Z]+", " ");
            InputObject.Add("x_version", AuthNetVersion);
            InputObject.Add("x_login", AuthNetLoginID);
            InputObject.Add("x_fp_timestamp", timestamp);
            InputObject.Add("x_fp_sequence", sequence);
            InputObject.Add("x_amount",string.Format("{0:0.00}",creditcard.Amount));
           
            InputObject.Add("x_freight", string.Format("{0:0.00}", ShoppingCart.ShippingCost));
            string fp_hash = AuthorizeNet.Crypto.GenerateFingerprint(AuthNetTransKey, AuthNetLoginID, creditcard.Amount, sequence, timestamp);
            
            InputObject.Add("x_fp_hash", fp_hash);
            InputObject.Add("x_invoice_num", creditcard.OrderID.ToString());
           
           
           // InputObject.Add("x_tran_key", AuthNetTransKey);
           // InputObject.Add("x_relay_url", serverURL);
           // InputObject.Add("x_relay_response", "true");
           // InputObject.Add("x_receipt_link_url", ConfigurationManager.AppSettings["OrderReceipt_Path"].ToString());
            InputObject.Add("x_method", "CC");
            InputObject.Add("x_type", "AUTH_CAPTURE");
            InputObject.Add("x_first_name", BillingAddress.FirstName);
            InputObject.Add("x_last_name", BillingAddress.LastName);
            InputObject.Add("x_address", billingAddress);
            InputObject.Add("x_city", BillingAddress.City);
            InputObject.Add("x_state", BillingAddress.StateCode);
            InputObject.Add("x_zip", BillingAddress.PostalCode);
            InputObject.Add("x_country", BillingAddress.CountryCode);

            InputObject.Add("x_ship_to_first_name", ShippingAddress.FirstName);
            InputObject.Add("x_ship_to_last_name", ShippingAddress.LastName);
            InputObject.Add("x_ship_to_company", ShippingAddress.CompanyName);
            InputObject.Add("x_ship_to_address", shippingaddress);
            InputObject.Add("x_ship_to_city", ShippingAddress.City);
            InputObject.Add("x_ship_to_state", ShippingAddress.StateCode);
            InputObject.Add("x_ship_to_zip", ShippingAddress.PostalCode);
            InputObject.Add("x_ship_to_country", ShippingAddress.CountryCode);

            //----------------------Set to False to go Live--------------------
            InputObject.Add("x_test_request", "False");
            //---------------------------------------------------------------------
          

            InputObject.Add("x_email", BillingAddress.EmailId);
            InputObject.Add("x_email_customer", "TRUE");                     //Emails Customer
           

            //Card Details
            InputObject.Add("x_card_num", creditcard.CardNumber);
            InputObject.Add("x_exp_date", creditcard.CreditCardExp);
            InputObject.Add("x_card_code", creditcard.CardSecurityCode);

            ArrayList line_items = new ArrayList();
            string fieldSep = "<|>";

            decimal taxcost = 0.0m;
            foreach (ZNodeShoppingCartItem cartItem in ShoppingCart.ShoppingCartItems)
            {
                string taxable = "N";
                if (cartItem.TaxCost > 0)
                {
                    taxable = "Y";
                    taxcost += cartItem.TaxCost;
                }
                line_items.Add(cartItem.Product.ProductNum + fieldSep + cartItem.Product.Name+fieldSep + fieldSep + cartItem.Quantity.ToString() + fieldSep + string.Format("{0:f2}", cartItem.UnitPrice) + fieldSep + taxable);
            }
            InputObject.Add("x_tax", string.Format("{0:0.00}", taxcost));

            try
            {
                //Actual Server
                //Set above Testmode=off to go live
               string url = "https://test.authorize.net/gateway/transact.dll";

                Encoding enc = Encoding.GetEncoding(1252);
                StreamReader loResponseStream = new StreamReader(PostEx(url, InputObject, line_items).GetResponseStream(), enc);

                string retValue = loResponseStream.ReadToEnd();
                loResponseStream.Close();

                
                if (retValue.Length > 1 && retValue.IndexOf("The following errors have occurred") <= 0)
                {
                    string htmlContent = retValue.Replace("<!--end-->", "==");
                    htmlContent = htmlContent.Replace("<!--", "{");
                    htmlContent = htmlContent.Replace("-->", "}");
                    String result = Regex.Replace(htmlContent, @"<[^>]*>", String.Empty);

                    //Get Transaction ID
                    int stIdx = htmlContent.IndexOf("}", htmlContent.IndexOf("Transaction ID"));
                    int endIdx = htmlContent.IndexOf("==", stIdx);
                    response.IsSuccess = true;
                    response.TransactionId = htmlContent.Substring(stIdx + 1, endIdx - stIdx-1);

                    //Get Auth Code
                    stIdx = htmlContent.IndexOf("}", htmlContent.IndexOf("x_auth_code"));
                    endIdx = htmlContent.IndexOf("==", stIdx);
                    response.IsSuccess = true;
                    response.CardAuthCode = htmlContent.Substring(stIdx + 1, endIdx - stIdx-1);

                    //Get Card Type
                    stIdx = htmlContent.IndexOf("}", htmlContent.IndexOf("x_method"));
                    endIdx = htmlContent.IndexOf("****", stIdx);
                    response.CardTypeID = htmlContent.Substring(stIdx + 1, endIdx - stIdx-1).Trim();

                    //Get Card Number (last 4)
                    stIdx = htmlContent.IndexOf("****", htmlContent.IndexOf("x_method"));
                    endIdx = htmlContent.IndexOf("==", stIdx);

                    response.CardNumber = htmlContent.Substring(stIdx + 4, endIdx - stIdx - 4);

                    response.IsSuccess = true;
                    return response;
                }
                else
                {
                    string htmlContent = retValue;
                    String result = Regex.Replace(htmlContent, @"<[^>]*>", String.Empty);
                    response.IsSuccess = false;
                    response.GatewayResponseData = result;
                    response.ResponseText = response.GatewayResponseData;
                   
                    return response;
                }
            }
            catch (Exception ex)
            {
                response.ResponseText = ex.Message;
                response.IsSuccess = false;
                response.GatewayResponseData = ex.Message;
                return response;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="url"></param>
        /// <param name="values"></param>
        /// <param name="line_items"></param>
        /// <returns></returns>
        private WebResponse PostEx(string url, NameValueCollection values, ArrayList line_items)
        {
            WebRequest request = null;
            StringBuilder builder = null;
            string[] keys = null;
            Stream stream = null;
            byte[] bytes = null;
            request = WebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";

            if (values.Count == 0)
            {
                request.ContentLength = 0;
            }
            else
            {
                builder = new StringBuilder();
                keys = values.AllKeys;
                foreach (string key in keys)
                {
                    if (builder.Length > 0)
                    {
                        builder.Append("&");
                    }
                    builder.Append(HttpUtility.UrlEncode(key));
                    builder.Append("=");
                    builder.Append(HttpUtility.UrlEncode(values[key]));
                }

                string WIPorder = builder.ToString();

                foreach (string value in line_items)
                {
                    WIPorder += ("&x_line_item=" + HttpUtility.UrlEncode(value));
                }


                bytes = Encoding.UTF8.GetBytes(WIPorder.ToString());
                request.ContentLength = bytes.Length;
                stream = request.GetRequestStream();
                stream.Write(bytes, 0, bytes.Length);
                stream.Close();
            }
            return request.GetResponse();
        }
        /// <summary>
        /// Credits/ Refunds Payment
        /// </summary>
        /// <param name="Gateway">Gate way info</param>
        /// <param name="CreditCard">Credit Card</param>
        /// <returns>Returns the gateway response</returns>
        public GatewayResponse RefundPayment(GatewayInfo Gateway, CreditCard CreditCard)
        {
            GatewayResponse PaymentGatewayResponse = new GatewayResponse();

            string AuthNetVersion = "3.1"; // Contains CCV support 
            string AuthNetLoginID = Gateway.GatewayLoginID;
            string AuthNetTransKey = Gateway.TransactionKey;
            WebClient objRequest = new WebClient();
            System.Collections.Specialized.NameValueCollection objInf = new System.Collections.Specialized.NameValueCollection(30);
            System.Collections.Specialized.NameValueCollection objRetInf = new System.Collections.Specialized.NameValueCollection(30);
            byte[] objRetBytes;
            string[] objRetVals;

            // Merchant account Information
            objInf.Add("x_login", AuthNetLoginID);
            objInf.Add("x_tran_key", AuthNetTransKey);
            objInf.Add("x_version", AuthNetVersion);

            // Gateway Response settings            
            objInf.Add("x_delim_data", "True");
            objInf.Add("x_relay_response", "False");
            objInf.Add("x_delim_char", ",");
            objInf.Add("x_encap_char", "|");
            objInf.Add("x_method", "CC"); // Payment method

            // creidt transaction
            objInf.Add("x_type", "CREDIT");
            objInf.Add("x_ref_trans_id", CreditCard.TransactionID);

            // Invoice Information
            objInf.Add("x_description", "Description of Order");
            objInf.Add("x_invoice_num", CreditCard.OrderID.ToString());

            // Credit Card Details 
            objInf.Add("x_card_num", CreditCard.CardNumber);
            objInf.Add("x_exp_date", CreditCard.CreditCardExp);
            objInf.Add("x_card_code", CreditCard.CardSecurityCode); // Authorisation code of the card (CCV)                      
            objInf.Add("x_amount", CreditCard.Amount.ToString());

            try
            {
                // Check for Authorize.NET Test mode
                if (Gateway.TestMode)
                {
                    // Pure Test Server 
                    objInf.Add("x_test_request", "True");
                    objRequest.BaseAddress = "https://certification.authorize.net/gateway/transact.dll";
                }
                else
                {
                    objInf.Add("x_test_request", "False");

                    // Actual Server 
                    objRequest.BaseAddress = "https://secure.authorize.net/gateway/transact.dll";
                }

                // Post the values into Authorize.net Server
                objRetBytes = objRequest.UploadValues(objRequest.BaseAddress, "POST", objInf);
                objRetVals = System.Text.Encoding.ASCII.GetString(objRetBytes).Split(",".ToCharArray());

                // Transaction Approved if the Response code is 1
                if (objRetVals[0].Trim(char.Parse("|")) == "1")
                {
                    // the transaction was approved!
                    if (objRetVals[6].Trim(char.Parse("|")).Length > 0)
                    {
                        PaymentGatewayResponse.TransactionId = objRetVals[6].Trim(char.Parse("|"));
                        PaymentGatewayResponse.IsSuccess = true;
                    }
                }
                else
                {
                    PaymentGatewayResponse.IsSuccess = false;
                }

                // Set Response code and Response Text
                PaymentGatewayResponse.ResponseCode = "Response Code: " + objRetVals[0].Trim(char.Parse("|")) + "<br>";
                PaymentGatewayResponse.ResponseText = "Response Reason Code :" + objRetVals[2].Trim(char.Parse("|")) + " <br> Description: " + objRetVals[3].Trim(char.Parse("|"));
                PaymentGatewayResponse.ReferenceNumber = "MD5 Hash value :" + objRetVals[37].Trim(char.Parse("|"));
            }
            catch (Exception)
            {
                PaymentGatewayResponse.IsSuccess = false;
            }

            // Return Payment GatewayResponse
            return PaymentGatewayResponse;
        }
        public IZnodeGatewayResult SubmitVoid(IZnodeGatewayAuguments args)
        {
            throw new System.NotImplementedException();
        }

        public IZnodeGatewayResult SubmitCredit(IZnodeGatewayAuguments args)
        {
            throw new System.NotImplementedException();
        }
    }


}
