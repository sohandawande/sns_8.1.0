using System;
using System.Collections.Generic;
using System.Web;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.PaymentHelper
{
    /// <summary>
    /// Processes different payment types
    /// </summary>
    [Serializable()]
    public class ZNodePaymentHelper : ZNode.Libraries.ECommerce.Entities.ZNodePayment
    {
        #region Member Variables
        private ZNodeOrder _Order = new ZNodeOrder();
        private string _CurrencyCode = ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.CurrencyCode();
        private IEnumerable<ZNodeShoppingCartItem> _addressCartItems = null;
        private bool _fromApi = false;
        public string Token { get; set; }
        public string PayerId { get; set; }
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the currency code
        /// </summary>
        public string CurrencyCode
        {
            get { return this._CurrencyCode; }
            set { this._CurrencyCode = value; }
        }

        /// <summary>
        /// Gets or sets the order
        /// </summary>
        public ZNodeOrder Order
        {
            get { return this._Order; }
            set { this._Order = value; }
        }

        public IEnumerable<ZNodeShoppingCartItem> AddressCartItems
        {
            get { return this._addressCartItems; }
            set { this._addressCartItems = value; }
        }
        /// <summary>
        /// Gets or Sets MultipleShipping Address is enabled
        /// </summary>
        public bool IsMultipleShipToAddress { get; set; }

        #endregion

        #region Public and Private Methods
        /// <summary>
        /// This method orchestrates the payment submission process
        /// </summary>
        /// <returns>Returns the payment response</returns>
        public ZNodePaymentResponse SubmitPayment(bool fromApi = false)
        {
            _fromApi = fromApi;

            // submit card payment
            if (PaymentSetting.PaymentTypeID == (int)ZNode.Libraries.ECommerce.Entities.PaymentType.CREDIT_CARD)
            {
                Token = _fromApi ? this.ShoppingCart.Token : HttpContext.Current.Request.QueryString.Get("token");

                var paymentResponse = new ZNodePaymentResponse { TransactionId = Token, IsSuccess = true, PaymentStatus = (PaymentSetting.PreAuthorize) ? ZNodePaymentStatus.CREDIT_AUTHORIZED : ZNodePaymentStatus.CREDIT_CAPTURED };

                return paymentResponse;
            }
            else if (PaymentSetting.PaymentTypeID == (int)ZNode.Libraries.ECommerce.Entities.PaymentType.PAYPAL)
            {
                Token = _fromApi ? this.ShoppingCart.Token : HttpContext.Current.Request.QueryString.Get("token");

                var paymentResponse = new ZNodePaymentResponse { TransactionId = Token, IsSuccess = true, PaymentStatus = (PaymentSetting.PreAuthorize) ? ZNodePaymentStatus.CREDIT_AUTHORIZED : ZNodePaymentStatus.CREDIT_CAPTURED };

                return paymentResponse;
            }
            else if (PaymentSetting.PaymentTypeID == (int)ZNode.Libraries.ECommerce.Entities.PaymentType.COD)
            {
                // submit COD (Charge on delivery)
                return this.SubmitCODPayment();
            }
            else if (PaymentSetting.PaymentTypeID == (int)ZNode.Libraries.ECommerce.Entities.PaymentType.PURCHASE_ORDER)
            {
                // submit PURCHASE ORDER (Charge on purchase order)
                return this.SubmitPOPayment();
            }
            else
            {
                ZNodePaymentResponse pr = new ZNodePaymentResponse();
                return pr;
            }
        }

        /// <summary>
        /// Submit cod order payment 
        /// </summary>
        /// <returns>Returns the COD Payment Response</returns>
        public ZNodePaymentResponse SubmitCODPayment()
        {
            ZNodePaymentResponse CODPaymentResponse = new ZNodePaymentResponse();
            CODPaymentResponse.IsSuccess = true;
            CODPaymentResponse.TransactionId = null;
            CODPaymentResponse.PaymentStatus = ZNodePaymentStatus.COD_PENDING;

            // submit payment based on payment settings
            return CODPaymentResponse;
        }

        /// <summary>
        /// Submit purchase order payment 
        /// </summary>
        /// <returns>Returns the PO Payment Response</returns>
        public ZNodePaymentResponse SubmitPOPayment()
        {
            ZNodePaymentResponse POPaymentResponse = new ZNodePaymentResponse();
            POPaymentResponse.IsSuccess = true;
            POPaymentResponse.TransactionId = null;
            POPaymentResponse.PaymentStatus = ZNodePaymentStatus.PO_PENDING;

            // submit payment based on payment settings
            return POPaymentResponse;
        }

        #endregion
    }
}