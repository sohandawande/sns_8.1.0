﻿
namespace ZNode.Libraries.ECommerce.Payment
{
    public class GatewayHelper
    {
        /// <summary>
        /// To get the capability
        /// </summary>
        /// <param name="gatewayType">Gateway typee</param>
        /// <param name="gatewayTransactionType">Gateway transaction type</param>
        /// <returns>returns the capability</returns>
        public bool GetCapability(GatewayType gatewayType, GatewayTransactionType gatewayTransactionType)
        {
            switch (gatewayTransactionType)
            {
                case GatewayTransactionType.NONE:
                    return false;

                case GatewayTransactionType.REFUND:
                    return this.IsRefundAvailable(gatewayType);

                case GatewayTransactionType.VOID:
                    return this.IsVoidAvailable(gatewayType);

                default:
                    break;
            }

            return false;
        }

        /// <summary>
        /// Is the refund available
        /// </summary>
        /// <param name="gatewayType">Gateway Type</param>
        /// <returns>is the refund available</returns>
        private bool IsRefundAvailable(GatewayType gatewayType)
        {
            switch (gatewayType)
            {
                case GatewayType.AUTHORIZE:
                    return true;

                case GatewayType.PAYMENTECH:
                    return false;

                case GatewayType.VERISIGN:
                    return true;

                case GatewayType.CARDSTREAM:
                    return false;

                case GatewayType.CUSTOM:
                    return false;

                case GatewayType.PAYPAL:
                    return false;

                case GatewayType.WORLDPAY:
                    return false;

                case GatewayType.CYBERSOURCE:
                    return true;

                default:
                    break;
            }

            return false;
        }

        /// <summary>
        /// Is void available
        /// </summary>
        /// <param name="gatewayType">Gateway Type</param>
        /// <returns>Is the void available</returns>
        private bool IsVoidAvailable(GatewayType gatewayType)
        {
            switch (gatewayType)
            {
                case GatewayType.AUTHORIZE:
                    return true;

                case GatewayType.PAYMENTECH:
                    return false;

                case GatewayType.VERISIGN:
                    return true;

                case GatewayType.CARDSTREAM:
                    return false;

                case GatewayType.CUSTOM:
                    return false;

                case GatewayType.PAYPAL:
                    return false;

                case GatewayType.WORLDPAY:
                    return false;

                case GatewayType.CYBERSOURCE:
                    return true;

                default:
                    break;
            }

            return false;
        }
    }
}