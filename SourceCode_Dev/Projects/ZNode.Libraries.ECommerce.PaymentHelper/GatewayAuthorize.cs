using System;
using System.IO;
using System.Net;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using AuthorizeNetRecurringBilling;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Payment
{
    /// <summary>
    /// Authorize.net Gateway
    /// </summary>
    public class GatewayAuthorize : ZNodePaymentBase
    {
        private const string _AmericanExpressTestCard = "370000000000002";
        private const string _DiscoverTestCard = "6011000000000012";
        private const string _VisaTestCard = "4007000000027";
        private const string _CCVTest = "123";

        public string AmericanExpressTestCard { get { return _AmericanExpressTestCard; } }
        public string DiscoverTestCard { get { return _DiscoverTestCard; } }
        public string VisaTestCard { get { return _VisaTestCard; } }
        public bool MockCreditCard { get; set; }

        #region Credit Card Payment Methods

        /// <summary>
        /// Submits credit card payment to a Authorize.NET gateway
        /// </summary>
        /// <param name="Gateway">Gateway Info</param>
        /// <param name="BillingAddress">Billing Address</param>
        /// <param name="ShippingAddress">Shipping Address</param>
        /// <param name="CreditCard">Credit Card</param>
        /// <returns>Gateway Response</returns>
        public GatewayResponse SubmitPayment(GatewayInfo Gateway, Address BillingAddress, Address ShippingAddress,
                                             CreditCard CreditCard)
        {
            return SubmitPayment(Gateway, BillingAddress, ShippingAddress, CreditCard, false);
        }

        /// <summary>
        /// Submits credit card payment to a Authorize.NET gateway
        /// </summary>
        /// <param name="Gateway">Gateway Info</param>
        /// <param name="BillingAddress">Billing Address</param>
        /// <param name="ShippingAddress">Shipping Address</param>
        /// <param name="CreditCard">Credit Card</param>
        /// <returns>Gateway Response</returns>
        public GatewayResponse SubmitPayment(GatewayInfo Gateway, Address BillingAddress, Address ShippingAddress, CreditCard CreditCard, bool IsMultipleShipAddress)
        {
            //Znode Version 7.2.2
            //Change hard coded value to web.config items - Start
            //The below items were hard coded. Those were not working for live setup. To resolve this issue we move these items currently to web config.
            this.MockCreditCard = GetMockCreditCard();
            string baseAddress = GetTestAuthorizeNetGatewayAddress();
            string AuthNetVersion = GetAuthorizeNetVersion();
            //Change hard coded value to web.config items - End

            GatewayResponse PaymentGatewayResponse = new GatewayResponse();
            string AuthNetLoginID = Gateway.GatewayLoginID;
            string AuthNetTransKey = Gateway.TransactionKey;
            WebClient objRequest = new WebClient();
            System.Collections.Specialized.NameValueCollection objInf = new System.Collections.Specialized.NameValueCollection(30);
            System.Collections.Specialized.NameValueCollection objRetInf = new System.Collections.Specialized.NameValueCollection(30);
            byte[] objRetBytes;
            string[] objRetVals;

            // Merchant account Information
            objInf.Add("x_login", AuthNetLoginID);
            objInf.Add("x_tran_key", AuthNetTransKey);
            objInf.Add("x_version", AuthNetVersion);

            // Gateway Response settings            
            objInf.Add("x_delim_data", "True");
            objInf.Add("x_relay_response", "False");
            objInf.Add("x_delim_char", ",");
            objInf.Add("x_encap_char", "|");
            objInf.Add("x_method", "CC"); // Payment method

            if (Gateway.PreAuthorize)
            {
                objInf.Add("x_type", "AUTH_ONLY"); // authorize
            }
            else
            {
                objInf.Add("x_type", "AUTH_CAPTURE"); // authorize and capture
            }

            objInf.Add("x_currency_code", Gateway.CurrencyCode);

            // Billing Address 
            objInf.Add("x_first_name", BillingAddress.FirstName);
            objInf.Add("x_last_name", BillingAddress.LastName);
            objInf.Add("x_address", BillingAddress.Street1 + " " + BillingAddress.Street2);
            objInf.Add("x_city", BillingAddress.City);
            objInf.Add("x_state", BillingAddress.StateCode);
            objInf.Add("x_zip", BillingAddress.PostalCode);
            objInf.Add("x_country", BillingAddress.CountryCode);
            objInf.Add("x_phone", BillingAddress.PhoneNumber);
            objInf.Add("x_email", BillingAddress.EmailId);

            // Shipping Information - Optional fields
            if (IsMultipleShipAddress)
            {
                objInf.Add("x_ship_to_first_name", "You have selected multiple addresses");
            }
            else
            {
                objInf.Add("x_ship_to_first_name", ShippingAddress.FirstName);
                objInf.Add("x_ship_to_last_name", ShippingAddress.LastName);
                objInf.Add("x_ship_to_company", ShippingAddress.CompanyName);
                objInf.Add("x_ship_to_address", ShippingAddress.Street1 + " " + ShippingAddress.Street2);
                objInf.Add("x_ship_to_city", ShippingAddress.City);
                objInf.Add("x_ship_to_state", ShippingAddress.StateCode);
                objInf.Add("x_ship_to_zip", ShippingAddress.PostalCode);
                objInf.Add("x_ship_to_country", ShippingAddress.CountryCode);
            }


            // Invoice Information
            objInf.Add("x_description", "Description of Order");
            objInf.Add("x_invoice_num", CreditCard.OrderID.ToString());

            // Credit Card Details 
            if (this.MockCreditCard)
                CreditCard.CardNumber = this.VisaTestCard;

            objInf.Add("x_card_num", CreditCard.CardNumber);
            objInf.Add("x_exp_date", CreditCard.CreditCardExp);
            objInf.Add("x_card_code", CreditCard.CardSecurityCode); // Authorisation code of the card (CCV)                      
            objInf.Add("x_amount", CreditCard.Amount.ToString());

            // Additional Customer Information - Customer IP address, to avoid fraud transactions
            objInf.Add("x_customer_ip", Gateway.CustomerIPAddress);

            try
            {
                // Check for Authorize.NET Test mode
                if (Gateway.TestMode)
                {
                    objInf.Add("x_test_request", "True");
                }
                else
                {
                    objInf.Add("x_test_request", "False");
                    //Znode Version 7.2.2
                    //Change hard coded value to web.config items - Start
                    //The below item was hard coded. This was not working for live setup. To resolve this issue we move this item currently to web config.
                    baseAddress = GetLiveAuthorizeNetGatewayAddress();
                    //Change hard coded value to web.config items - End
                }
                objRequest.BaseAddress = baseAddress;
                // Post the values into Authorize.net Server
                objRetBytes = objRequest.UploadValues(objRequest.BaseAddress, "POST", objInf);
                objRetVals = System.Text.Encoding.ASCII.GetString(objRetBytes).Split(",".ToCharArray());

                // Transaction Approved if the Response code is 1
                if (objRetVals[0].Trim(char.Parse("|")) == "1")
                {
                    // the transaction was approved!
                    if (objRetVals[6].Trim(char.Parse("|")).Length > 0)
                    {
                        PaymentGatewayResponse.TransactionId = objRetVals[6].Trim(char.Parse("|"));
                        PaymentGatewayResponse.IsSuccess = true;
                    }
                }
                else
                {
                    PaymentGatewayResponse.IsSuccess = false;
                }

                // Set Response code and Response Text
                PaymentGatewayResponse.ResponseCode = "Response Code: " + objRetVals[0].Trim(char.Parse("|")) + "<br>";
                PaymentGatewayResponse.ResponseText = "Response Reason Code :" + objRetVals[2].Trim(char.Parse("|")) + " <br> Description: " + objRetVals[3].Trim(char.Parse("|"));
                PaymentGatewayResponse.ReferenceNumber = "MD5 Hash value :" + objRetVals[37].Trim(char.Parse("|"));
            }
            catch (Exception)
            {
                PaymentGatewayResponse.IsSuccess = false;
            }

            // Return Payment GatewayResponse
            return PaymentGatewayResponse;
        }
        /// <summary>
        /// Credits/ Refunds Payment
        /// </summary>
        /// <param name="Gateway">Gate way info</param>
        /// <param name="CreditCard">Credit Card</param>
        /// <returns>Returns the gateway response</returns>
        public GatewayResponse RefundPayment(GatewayInfo Gateway, CreditCard CreditCard)
        {
            //TODO : Change this items to DB driven
            //Znode Version 7.2.2
            //Change hard coded value to web.config items - Start
            //The below items were hard coded. Those were not working for live setup. To resolve this issue we move these items currently to web config.
            this.MockCreditCard = GetMockCreditCard();
            string baseAddress = GetTestAuthorizeNetGatewayAddress();
            string AuthNetVersion = GetAuthorizeNetVersion();
            //Change hard coded value to web.config items - End

            GatewayResponse PaymentGatewayResponse = new GatewayResponse();
            string AuthNetLoginID = Gateway.GatewayLoginID;
            string AuthNetTransKey = Gateway.TransactionKey;
            WebClient objRequest = new WebClient();
            System.Collections.Specialized.NameValueCollection objInf = new System.Collections.Specialized.NameValueCollection(30);
            System.Collections.Specialized.NameValueCollection objRetInf = new System.Collections.Specialized.NameValueCollection(30);
            byte[] objRetBytes;
            string[] objRetVals;

            try
            {
                AuthorizeNet.ReportingGateway gate = new AuthorizeNet.ReportingGateway(AuthNetLoginID, AuthNetTransKey);
                AuthorizeNet.Transaction transactionresponse = gate.GetTransactionDetails(CreditCard.TransactionID);
                if (transactionresponse != null && !string.IsNullOrEmpty(transactionresponse.CardNumber))
                {
                    CreditCard.CardNumber = transactionresponse.CardNumber;
                    this.MockCreditCard = false;
                }
            }
            catch
            {

            }

            if (this.MockCreditCard)
                CreditCard.CardNumber = _VisaTestCard;

            objInf.Add("x_ref_trans_id", CreditCard.TransactionID);
            objInf.Add("x_card_num", CreditCard.CardNumber);

            // Merchant account Information
            objInf.Add("x_login", AuthNetLoginID);
            objInf.Add("x_tran_key", AuthNetTransKey);
            objInf.Add("x_version", AuthNetVersion);

            // Gateway Response settings            
            objInf.Add("x_delim_data", "True");
            objInf.Add("x_relay_response", "False");
            objInf.Add("x_delim_char", ",");
            objInf.Add("x_encap_char", "|");
            objInf.Add("x_method", "CC"); // Payment method

            // creidt transaction
            objInf.Add("x_type", "CREDIT");


            // Invoice Information
            objInf.Add("x_description", "Description of Order");
            objInf.Add("x_invoice_num", CreditCard.OrderID.ToString());

            // Credit Card Details 

            objInf.Add("x_amount", CreditCard.Amount.ToString());

            try
            {
                // Check for Authorize.NET Test mode
                if (Gateway.TestMode)
                {
                    objInf.Add("x_test_request", "True");
                }
                else
                {
                    objInf.Add("x_test_request", "False");
                    //Znode Version 7.2.2
                    //Change hard coded value to web.config items - Start
                    //The below item was hard coded. This was not working for live setup. To resolve this issue we move this item currently to web config.     
                    baseAddress = GetLiveAuthorizeNetGatewayAddress();
                    //Change hard coded value to web.config items - End
                }
                objRequest.BaseAddress = baseAddress;
                // Post the values into Authorize.net Server
                objRetBytes = objRequest.UploadValues(objRequest.BaseAddress, "POST", objInf);
                objRetVals = System.Text.Encoding.ASCII.GetString(objRetBytes).Split(",".ToCharArray());

                // Transaction Approved if the Response code is 1
                if (objRetVals[0].Trim(char.Parse("|")) == "1")
                {
                    // the transaction was approved!
                    if (objRetVals[6].Trim(char.Parse("|")).Length > 0)
                    {
                        PaymentGatewayResponse.TransactionId = objRetVals[6].Trim(char.Parse("|"));
                        PaymentGatewayResponse.IsSuccess = true;
                    }
                }
                else
                {
                    PaymentGatewayResponse.IsSuccess = false;
                }

                // Set Response code and Response Text
                PaymentGatewayResponse.ResponseCode = "Response Code: " + objRetVals[0].Trim(char.Parse("|")) + "<br>";
                PaymentGatewayResponse.ResponseText = "Response Reason Code :" + objRetVals[2].Trim(char.Parse("|")) + " <br> Description: " + objRetVals[3].Trim(char.Parse("|"));
                PaymentGatewayResponse.ReferenceNumber = "MD5 Hash value :" + objRetVals[37].Trim(char.Parse("|"));
            }
            catch (Exception)
            {
                PaymentGatewayResponse.IsSuccess = false;
            }

            // Return Payment GatewayResponse
            return PaymentGatewayResponse;
        }

        /// <summary>
        /// Void transaction
        /// </summary>
        /// <param name="Gateway">Gateway Info</param>
        /// <param name="CreditCard">Credit Card</param>
        /// <returns>Returns the gateway response</returns>
        public GatewayResponse VoidPayment(GatewayInfo Gateway, CreditCard CreditCard)
        {
            GatewayResponse PaymentGatewayResponse = new GatewayResponse();
           
            //Znode Version 7.2.2
            //Change hard coded value to web.config items - Start
            //The below item was hard coded. This was not working for live setup. To resolve this issue we move this item currently to web config.
            string AuthNetVersion = GetAuthorizeNetVersion();
            //Change hard coded value to web.config items - End
            string AuthNetLoginID = Gateway.GatewayLoginID;
            string AuthNetTransKey = Gateway.TransactionKey;
            WebClient objRequest = new WebClient();
            System.Collections.Specialized.NameValueCollection objInf = new System.Collections.Specialized.NameValueCollection(30);
            System.Collections.Specialized.NameValueCollection objRetInf = new System.Collections.Specialized.NameValueCollection(30);
            byte[] objRetBytes;
            string[] objRetVals;

            // Merchant account Information
            objInf.Add("x_login", AuthNetLoginID);
            objInf.Add("x_tran_key", AuthNetTransKey);
            objInf.Add("x_version", AuthNetVersion);

            // Gateway Response settings            
            objInf.Add("x_delim_data", "True");
            objInf.Add("x_relay_response", "False");
            objInf.Add("x_delim_char", ",");
            objInf.Add("x_encap_char", "|");
            objInf.Add("x_method", "CC"); // Payment method

            // creidt transaction
            objInf.Add("x_type", "VOID");
            objInf.Add("x_ref_trans_id", CreditCard.TransactionID);

            // Invoice Information
            objInf.Add("x_description", "Description of Order");
            objInf.Add("x_invoice_num", CreditCard.OrderID.ToString());

            // Credit Card Details 
            objInf.Add("x_card_num", CreditCard.CardNumber);
            objInf.Add("x_exp_date", CreditCard.CreditCardExp);
            objInf.Add("x_card_code", CreditCard.CardSecurityCode); // Authorisation code of the card (CCV)                      
            objInf.Add("x_amount", CreditCard.Amount.ToString());
            try
            {
                string baseAddress = GetTestAuthorizeNetGatewayAddress();
                if (Gateway.TestMode)
                {
                    objInf.Add("x_test_request", "True");
                }
                else
                {
                    objInf.Add("x_test_request", "False");
                    //Znode Version 7.2.2
                    //Change hard coded value to web.config items - Start
                    //The below item was hard coded. This was not working for live setup. To resolve this issue we move this item currently to web config.      
                    baseAddress = GetLiveAuthorizeNetGatewayAddress();
                    //Change hard coded value to web.config items - End 
                }
                objRequest.BaseAddress = baseAddress;

                // Post the values into Authorize.net Server
                objRetBytes = objRequest.UploadValues(objRequest.BaseAddress, "POST", objInf);
                objRetVals = System.Text.Encoding.ASCII.GetString(objRetBytes).Split(",".ToCharArray());

                // Transaction Approved if the Response code is 1
                if (objRetVals[0].Trim(char.Parse("|")) == "1")
                {
                    // the transaction was approved!
                    if (objRetVals[6].Trim(char.Parse("|")).Length > 0)
                    {
                        PaymentGatewayResponse.TransactionId = objRetVals[6].Trim(char.Parse("|"));
                        PaymentGatewayResponse.IsSuccess = true;
                    }
                }
                else
                {
                    PaymentGatewayResponse.IsSuccess = false;
                }

                // Set Response code and Response Text
                PaymentGatewayResponse.ResponseCode = "Response Code: " + objRetVals[0].Trim(char.Parse("|")) + "<br>";
                PaymentGatewayResponse.ResponseText = "Response Reason Code :" + objRetVals[2].Trim(char.Parse("|")) + " <br> Description: " + objRetVals[3].Trim(char.Parse("|"));
                PaymentGatewayResponse.ReferenceNumber = "MD5 Hash value :" + objRetVals[37].Trim(char.Parse("|"));
            }
            catch (Exception)
            {
                PaymentGatewayResponse.IsSuccess = false;
            }

            // Return Payment GatewayResponse
            return PaymentGatewayResponse;
        }

        /// <summary>
        /// Captures a previously authorized transaction
        /// </summary>
        /// <param name="Gateway">Gateway Info</param>
        /// <param name="CreditCard">Credit Card</param>
        /// <returns>Returns the gateway response</returns>
        public GatewayResponse CapturePayment(GatewayInfo Gateway, CreditCard CreditCard)
        {
            GatewayResponse PaymentGatewayResponse = new GatewayResponse();
            //Znode Version 7.2.2
            //Change hard coded value to web.config items - Start
            //The below item was hard coded. This was not working for live setup. To resolve this issue we move this item currently to web config.   
            string AuthNetVersion = GetAuthorizeNetVersion();
            //Change hard coded value to web.config items - End
            string AuthNetLoginID = Gateway.GatewayLoginID;
            string AuthNetTransKey = Gateway.TransactionKey;
            WebClient objRequest = new WebClient();
            System.Collections.Specialized.NameValueCollection objInf = new System.Collections.Specialized.NameValueCollection(30);
            System.Collections.Specialized.NameValueCollection objRetInf = new System.Collections.Specialized.NameValueCollection(30);
            byte[] objRetBytes;
            string[] objRetVals;

            // Merchant account Information
            objInf.Add("x_login", AuthNetLoginID);
            objInf.Add("x_tran_key", AuthNetTransKey);
            objInf.Add("x_version", AuthNetVersion);

            // Gateway Response settings            
            objInf.Add("x_delim_data", "True");
            objInf.Add("x_relay_response", "False");
            objInf.Add("x_delim_char", ",");
            objInf.Add("x_encap_char", "|");
            objInf.Add("x_method", "CC"); // Payment method

            // creidt transaction
            objInf.Add("x_type", "PRIOR_AUTH_CAPTURE");
            objInf.Add("x_ref_trans_id", CreditCard.TransactionID);

            try
            {
                // Check for Authorize.NET Test mode
                if (Gateway.TestMode)
                {
                    // Pure Test Server 
                    objInf.Add("x_test_request", "True");
                    objRequest.BaseAddress = "https://certification.authorize.net/gateway/transact.dll";
                }
                else
                {
                    objInf.Add("x_test_request", "False");
                    //Znode Version 7.2.2
                    //Change hard coded value to web.config items - Start
                    //The below item was hard coded. This was not working for live setup. To resolve this issue we move this item currently to web config.
                    objRequest.BaseAddress = GetLiveAuthorizeNetGatewayAddress();
                    //Change hard coded value to web.config items - End 
                }

                // Post the values into Authorize.net Server
                objRetBytes = objRequest.UploadValues(objRequest.BaseAddress, "POST", objInf);
                objRetVals = System.Text.Encoding.ASCII.GetString(objRetBytes).Split(",".ToCharArray());

                // Transaction Approved if the Response code is 1
                if (objRetVals[0].Trim(char.Parse("|")) == "1")
                {
                    // the transaction was approved!
                    if (objRetVals[6].Trim(char.Parse("|")).Length > 0)
                    {
                        PaymentGatewayResponse.TransactionId = objRetVals[6].Trim(char.Parse("|"));
                        PaymentGatewayResponse.IsSuccess = true;
                    }
                }
                else
                {
                    PaymentGatewayResponse.IsSuccess = false;
                }

                // Set Response code and Response Text
                PaymentGatewayResponse.ResponseCode = "Response Code: " + objRetVals[0].Trim(char.Parse("|")) + "<br>";
                PaymentGatewayResponse.ResponseText = "Response Reason Code :" + objRetVals[2].Trim(char.Parse("|")) + " <br> Description: " + objRetVals[3].Trim(char.Parse("|"));
                PaymentGatewayResponse.ReferenceNumber = "MD5 Hash value :" + objRetVals[37].Trim(char.Parse("|"));
                PaymentGatewayResponse.CardAuthCode = objRetVals[37].Trim(char.Parse("|"));
            }
            catch (Exception)
            {
                PaymentGatewayResponse.IsSuccess = false;
            }

            // Return Payment GatewayResponse
            return PaymentGatewayResponse;
        }

        #endregion

        #region Recurring Billing Subscription
        /// <summary>
        /// Submit the recurring billing subscription
        /// </summary>
        /// <param name="BillingAddress">Billing Address</param>
        /// <param name="CreditCardInfo">Credit Card Info</param>
        /// <param name="GatewayInfo">Gateway Info</param>
        /// <param name="ARBSubscriptionInfo">ARBSubscription Info</param>
        /// <returns>Returns the gateway response</returns>
        public GatewayResponse SubmitRecurringBillingSubscription(Address BillingAddress, CreditCard CreditCardInfo, GatewayInfo GatewayInfo, RecurringBillingInfo ARBSubscriptionInfo)
        {
            bool subscriptionResult = true;

            // This is the API interface object
            ARBCreateSubscriptionRequest createSubscriptionRequest = new ARBCreateSubscriptionRequest();

            ARBSubscriptionType sub = new ARBSubscriptionType();
            sub.name = ARBSubscriptionInfo.ProfileName;

            // Merchant Authentication
            createSubscriptionRequest.merchantAuthentication = new merchantAuthenticationType();
            createSubscriptionRequest.merchantAuthentication.name = GatewayInfo.GatewayLoginID;
            createSubscriptionRequest.merchantAuthentication.transactionKey = GatewayInfo.TransactionKey;
            createSubscriptionRequest.refId = string.Empty;

            // Credit Card Information
            creditCardType creditCard = new creditCardType();
            creditCard.cardNumber = CreditCardInfo.CardNumber;
            creditCard.expirationDate = CreditCardInfo.CreditCardExp;  // required format for API is YYYY-MM
            sub.payment = new paymentType();
            sub.payment.Item = creditCard;

            // Billing Address
            sub.billTo = new nameAndAddressType();
            sub.billTo.firstName = BillingAddress.FirstName;
            sub.billTo.lastName = BillingAddress.LastName;
            sub.billTo.zip = BillingAddress.PostalCode;
            sub.billTo.state = BillingAddress.StateCode;

            // subscription payment schedule
            sub.paymentSchedule = new paymentScheduleType();
            sub.paymentSchedule.startDate = System.DateTime.Today;
            sub.paymentSchedule.startDateSpecified = true;

            // Disable trial
            sub.paymentSchedule.trialOccurrences = 0;
            sub.paymentSchedule.trialOccurrencesSpecified = true;
            sub.trialAmount = 0.00M;
            sub.trialAmountSpecified = true;

            if (ARBSubscriptionInfo.TotalCycles == 0)
            {
                sub.paymentSchedule.totalOccurrences = 9999;
            }
            else
            {
                sub.paymentSchedule.totalOccurrences = short.Parse(ARBSubscriptionInfo.TotalCycles.ToString());
            }

            sub.paymentSchedule.totalOccurrencesSpecified = true;

            sub.amount = ARBSubscriptionInfo.Amount;
            sub.amountSpecified = true;

            int frequencyLength = 0;
            sub.paymentSchedule.interval = new paymentScheduleTypeInterval();
            sub.paymentSchedule.interval.unit = this.BillingPeriod(ARBSubscriptionInfo.Period, ARBSubscriptionInfo.Frequency, out frequencyLength);
            sub.paymentSchedule.interval.length = (short)frequencyLength;

            sub.order = new orderType();
            sub.order.invoiceNumber = ARBSubscriptionInfo.InvoiceNo;

            createSubscriptionRequest.subscription = sub;

            GatewayInfo.GatewayURL = "https://apitest.authorize.net/xml/v1/request.api";

            //// Check for Authorize.NET Test mode
            //if (GatewayInfo.TestMode)
            //{
            //    // Pure Test Server                
            //    GatewayInfo.GatewayURL = "https://apitest.authorize.net/xml/v1/request.api";
            //}
            //else
            //{
            //    // Actual Server 
            //    GatewayInfo.GatewayURL = "https://api.authorize.net/xml/v1/request.api";
            //}

            XmlDocument xmldoc = null;
            subscriptionResult = PostRequest(GatewayInfo.GatewayURL, createSubscriptionRequest, out xmldoc);

            return this.ProcessXmlResponse(xmldoc, null, null);
        }

        /// <summary>
        /// Update the recurring billing subscription
        /// </summary>
        /// <param name="BillingAddress">Billing Address</param>
        /// <param name="CreditCardInfo">Credit Card Info</param>
        /// <param name="GatewayInfo">Gateway Info</param>
        /// <param name="ARBSubscriptionInfo">ARBSubscription Info</param>
        /// <returns>Returns the gateway response</returns>
        public GatewayResponse UpdateRecurringBillingSubscription(Address BillingAddress, CreditCard CreditCardInfo, GatewayInfo GatewayInfo, RecurringBillingInfo ARBSubscriptionInfo)
        {
            ARBUpdateSubscriptionRequest updateSubscriptionReq = new ARBUpdateSubscriptionRequest();

            // Merchant Authentication
            updateSubscriptionReq.merchantAuthentication = new merchantAuthenticationType();
            updateSubscriptionReq.merchantAuthentication.name = GatewayInfo.GatewayLoginID;
            updateSubscriptionReq.merchantAuthentication.transactionKey = GatewayInfo.TransactionKey;
            updateSubscriptionReq.refId = string.Empty;

            updateSubscriptionReq.subscriptionId = CreditCardInfo.TransactionID;

            // Subscription Information
            updateSubscriptionReq.subscription = new ARBSubscriptionType();

            creditCardType creditCard = new creditCardType();
            creditCard.cardNumber = CreditCardInfo.CardNumber;
            creditCard.expirationDate = CreditCardInfo.CreditCardExp;  // required format for API is YYYY-MM
            updateSubscriptionReq.subscription.payment = new paymentType();
            updateSubscriptionReq.subscription.payment.Item = creditCard;

            // Check for Authorize.NET Test mode
            if (GatewayInfo.TestMode)
            {
                // Pure Test Server                
                GatewayInfo.GatewayURL = "https://apitest.authorize.net/xml/v1/request.api";
            }
            else
            {
                // Actual Server 
                GatewayInfo.GatewayURL = "https://api.authorize.net/xml/v1/request.api";
            }

            XmlDocument xmldoc = null;
            bool subscriptionResult = PostRequest(GatewayInfo.GatewayURL, updateSubscriptionReq, out xmldoc);

            return this.ProcessXmlResponse(xmldoc, null, null);
        }

        /// <summary>
        /// Update the subscription payment info
        /// </summary>
        /// <param name="CreditCardInfo">Credit Card Info</param>
        /// <param name="GatewayInfo">Gateway Info</param>
        /// <returns>Returns the gateway response</returns>
        public GatewayResponse UpdateSubscriptionPaymentInfo(CreditCard CreditCardInfo, GatewayInfo GatewayInfo)
        {
            ARBUpdateSubscriptionRequest updateSubscriptionReq = new ARBUpdateSubscriptionRequest();

            // Merchant Authentication
            updateSubscriptionReq.merchantAuthentication = new merchantAuthenticationType();
            updateSubscriptionReq.merchantAuthentication.name = GatewayInfo.GatewayLoginID;
            updateSubscriptionReq.merchantAuthentication.transactionKey = GatewayInfo.TransactionKey;
            updateSubscriptionReq.refId = string.Empty;

            updateSubscriptionReq.subscriptionId = CreditCardInfo.TransactionID;

            // Subscription Information
            updateSubscriptionReq.subscription = new ARBSubscriptionType();

            creditCardType creditCard = new creditCardType();
            creditCard.cardNumber = CreditCardInfo.CardNumber;
            creditCard.expirationDate = CreditCardInfo.CreditCardExp;  // required format for API is YYYY-MM
            updateSubscriptionReq.subscription.payment = new paymentType();
            updateSubscriptionReq.subscription.payment.Item = creditCard;

            // Check for Authorize.NET Test mode
            if (GatewayInfo.TestMode)
            {
                // Pure Test Server                
                GatewayInfo.GatewayURL = "https://apitest.authorize.net/xml/v1/request.api";
            }
            else
            {
                // Actual Server 
                GatewayInfo.GatewayURL = "https://api.authorize.net/xml/v1/request.api";
            }

            XmlDocument xmldoc = null;
            bool subscriptionResult = PostRequest(GatewayInfo.GatewayURL, updateSubscriptionReq, out xmldoc);

            return this.ProcessXmlResponse(xmldoc, null, null);
        }

        /// <summary>
        /// Cancel the recurring billing subscription
        /// </summary>
        /// <param name="GatewayInfo">Gateway Info</param>
        /// <param name="SubscriptionId">Subsciption Id</param>
        /// <returns>Returns the gateway response</returns>
        public GatewayResponse CancelRecurringBillingSubscription(GatewayInfo GatewayInfo, string SubscriptionId)
        {
            GatewayResponse cancelSubscriptionResponse = null;

            ARBCancelSubscriptionRequest cancelSubscriptionRequest = new ARBCancelSubscriptionRequest();

            // Merchant Authentication
            cancelSubscriptionRequest.merchantAuthentication = new merchantAuthenticationType();
            cancelSubscriptionRequest.merchantAuthentication.name = GatewayInfo.GatewayLoginID;
            cancelSubscriptionRequest.merchantAuthentication.transactionKey = GatewayInfo.TransactionKey;
            cancelSubscriptionRequest.refId = string.Empty;

            cancelSubscriptionRequest.subscriptionId = SubscriptionId;

            // Check for Authorize.NET Test mode
            if (GatewayInfo.TestMode)
            {
                // Pure Test Server                
                GatewayInfo.GatewayURL = "https://apitest.authorize.net/xml/v1/request.api";
            }
            else
            {
                // Actual Server 
                GatewayInfo.GatewayURL = "https://api.authorize.net/xml/v1/request.api";
            }

            XmlDocument xmldoc = null;
            bool subscriptionResult = PostRequest(GatewayInfo.GatewayURL, cancelSubscriptionRequest, out xmldoc);

            if (xmldoc != null)
            {
                cancelSubscriptionResponse = this.ProcessXmlResponse(xmldoc, null, null);
            }
            else
            {
                cancelSubscriptionResponse.IsSuccess = subscriptionResult;
                cancelSubscriptionResponse.ResponseCode = "-1";
                cancelSubscriptionResponse.ResponseText = "Unable to process your request.";
            }

            return cancelSubscriptionResponse;
        }

        /// <summary>
        /// Calculate the billing period
        /// </summary>
        /// <param name="Unit">Unit for the billing</param>
        /// <param name="Frequency">Frequency for the billing</param>
        /// <param name="NewFrequency">New Frequency for the billing</param>
        /// <returns>Returns the ARB Subscription Unit</returns>
        protected ARBSubscriptionUnitEnum BillingPeriod(string Unit, string Frequency, out int NewFrequency)
        {
            int freqValue = int.Parse(Frequency);
            NewFrequency = freqValue;

            if (Unit.Equals("DAY", StringComparison.OrdinalIgnoreCase))
            {
                return ARBSubscriptionUnitEnum.days;
            }
            else if (Unit.Equals("WEEK", StringComparison.OrdinalIgnoreCase))
            {
                NewFrequency = freqValue * 7; // Change it to days.
                return ARBSubscriptionUnitEnum.days;
            }
            else if (Unit.Equals("MONTH", StringComparison.OrdinalIgnoreCase))
            {
                return ARBSubscriptionUnitEnum.months;
            }
            else if (Unit.Equals("YEAR", StringComparison.OrdinalIgnoreCase))
            {
                NewFrequency = freqValue * 12; // Change this value into months
                return ARBSubscriptionUnitEnum.months;
            }

            return ARBSubscriptionUnitEnum.days;
        }

        #endregion

        #region Helper Methods
        /// <summary>
        /// Send the request to the API server and load the response into an XML document.
        /// An XmlSerializer is used to form the XML used in the request to the API server. 
        /// The response from the server is also XML. An XmlReader is used to process the
        /// response stream from the API server so that it can be loaded into an XmlDocument.
        /// </summary>
        /// <param name="APIUrl">API URL for the request</param>
        /// <param name="apiRequest">API Request</param>
        /// <param name="xmldoc">XML Document</param>
        /// <returns>
        /// True if successful, false if not. If true then the specified xmldoc will contain the
        /// response received from the API server.
        /// </returns>
        private static bool PostRequest(string APIUrl, object apiRequest, out XmlDocument xmldoc)
        {
            bool result = false;
            XmlSerializer serializer;

            xmldoc = null;

            try
            {
                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(APIUrl);
                webRequest.Method = "POST";
                webRequest.ContentType = "text/xml";
                webRequest.KeepAlive = true;

                // Serialize the request
                serializer = new XmlSerializer(apiRequest.GetType());
                XmlWriter writer = new XmlTextWriter(webRequest.GetRequestStream(), Encoding.UTF8);
                serializer.Serialize(writer, apiRequest);
                writer.Close();

                // Get the response
                WebResponse webResponse = webRequest.GetResponse();

                // Load the response from the API server into an XmlDocument.
                xmldoc = new XmlDocument();
                xmldoc.Load(XmlReader.Create(webResponse.GetResponseStream()));

                result = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.GetType().ToString() + ": " + ex.Message);
                result = false;
            }

            return result;
        }

        /// <summary>
        /// Process the XML Response
        /// </summary>
        /// <param name="xmldoc">XML Document</param>
        /// <param name="ShippingAddress">Shipping Address</param>
        /// <param name="CreditCard">Credit Card</param>
        /// <returns>Returns the gateway response</returns>
        private GatewayResponse ProcessXmlResponse(XmlDocument xmldoc, Address ShippingAddress, CreditCard CreditCard)
        {
            GatewayResponse response = new GatewayResponse();
            XmlSerializer serializer;

            try
            {
                // Use the root node to determine the type of response object to create
                switch (xmldoc.DocumentElement.Name)
                {
                    case "ARBCreateSubscriptionResponse":
                        serializer = new XmlSerializer(typeof(ARBCreateSubscriptionResponse));
                        ARBCreateSubscriptionResponse apiResponse = (ARBCreateSubscriptionResponse)serializer.Deserialize(new StringReader(xmldoc.DocumentElement.OuterXml));

                        if (apiResponse.messages.resultCode == messageTypeEnum.Ok)
                        {
                            response.IsSuccess = true;
                            response.TransactionId = apiResponse.subscriptionId;
                        }

                        response.ResponseCode = apiResponse.messages.message[0].code;
                        response.ResponseText = apiResponse.messages.message[0].text;
                        break;

                    case "ARBUpdateSubscriptionResponse":
                        serializer = new XmlSerializer(typeof(ARBUpdateSubscriptionResponse));
                        ARBUpdateSubscriptionResponse apiUpdateResponse = (ARBUpdateSubscriptionResponse)serializer.Deserialize(new StringReader(xmldoc.DocumentElement.OuterXml));
                        break;

                    case "ARBCancelSubscriptionResponse":
                        serializer = new XmlSerializer(typeof(ARBCancelSubscriptionResponse));
                        ARBCancelSubscriptionResponse apiCancelResponse = (ARBCancelSubscriptionResponse)serializer.Deserialize(new StringReader(xmldoc.DocumentElement.OuterXml));
                        break;

                    case "createCustomerProfileResponse":
                        serializer = new XmlSerializer(typeof(createCustomerProfileResponse));
                        createCustomerProfileResponse apiProfileResponse = (createCustomerProfileResponse)serializer.Deserialize(new StringReader(xmldoc.DocumentElement.OuterXml));

                        if (apiProfileResponse.messages.resultCode == messageTypeEnum.Ok)
                        {
                            response.IsSuccess = true;
                            response.ProfileId = apiProfileResponse.customerProfileId;
                        }

                        response.ResponseCode = apiProfileResponse.messages.message[0].code;
                        response.ResponseText = apiProfileResponse.messages.message[0].text;
                        break;

                    case "createCustomerPaymentProfileResponse":
                        serializer = new XmlSerializer(typeof(createCustomerPaymentProfileResponse));
                        createCustomerPaymentProfileResponse apiPaymentResponse = (createCustomerPaymentProfileResponse)serializer.Deserialize(new StringReader(xmldoc.DocumentElement.OuterXml));

                        if (apiPaymentResponse.messages.resultCode == messageTypeEnum.Ok)
                        {
                            response.IsSuccess = true;
                            response.ProfileId = apiPaymentResponse.customerPaymentProfileId;
                        }

                        response.ResponseCode = apiPaymentResponse.messages.message[0].code;
                        response.ResponseText = apiPaymentResponse.messages.message[0].text;
                        break;

                    case "createCustomerShippingAddressResponse":
                        serializer = new XmlSerializer(typeof(createCustomerShippingAddressResponse));
                        createCustomerShippingAddressResponse apiShippingResponse = (createCustomerShippingAddressResponse)serializer.Deserialize(new StringReader(xmldoc.DocumentElement.OuterXml));
                        if (apiShippingResponse.messages.resultCode == messageTypeEnum.Ok)
                        {
                            response.IsSuccess = true;
                            response.ProfileId = apiShippingResponse.customerAddressId;
                        }

                        response.ResponseCode = apiShippingResponse.messages.message[0].code;
                        response.ResponseText = apiShippingResponse.messages.message[0].text;
                        break;

                    case "createCustomerProfileTransactionResponse":
                        serializer = new XmlSerializer(typeof(createCustomerProfileTransactionResponse));
                        createCustomerProfileTransactionResponse apiCreateResponse = (createCustomerProfileTransactionResponse)serializer.Deserialize(new StringReader(xmldoc.DocumentElement.OuterXml));

                        if (apiCreateResponse.messages.resultCode == messageTypeEnum.Ok)
                        {
                            response.IsSuccess = true;

                            string result = apiCreateResponse.directResponse.ToString();
                            string[] resultarray = result.Split(new char[] { ',' });
                            response.TransactionId = resultarray[6];
                            response.CardAuthCode = resultarray[5];
                            response.CardTypeID = resultarray[51];
                            response.CardNumber = resultarray[50];
                        }

                        response.ResponseCode = apiCreateResponse.messages.message[0].code;
                        response.ResponseText = apiCreateResponse.messages.message[0].text;
                        break;

                    case "updateCustomerShippingAddressResponse":
                        serializer = new XmlSerializer(typeof(updateCustomerShippingAddressResponse));
                        updateCustomerShippingAddressResponse apiUpdateShippingResponse = (updateCustomerShippingAddressResponse)serializer.Deserialize(new StringReader(xmldoc.DocumentElement.OuterXml));

                        if (apiUpdateShippingResponse.messages.resultCode == messageTypeEnum.Ok)
                        {
                            response.IsSuccess = true;
                        }

                        response.ResponseCode = apiUpdateShippingResponse.messages.message[0].code;
                        response.ResponseText = apiUpdateShippingResponse.messages.message[0].text;
                        break;

                    case "getCustomerShippingAddressResponse":
                        serializer = new XmlSerializer(typeof(getCustomerShippingAddressResponse));
                        getCustomerShippingAddressResponse apiGetShipAddressResponse = (getCustomerShippingAddressResponse)serializer.Deserialize(new StringReader(xmldoc.DocumentElement.OuterXml));

                        if (apiGetShipAddressResponse.messages.resultCode == messageTypeEnum.Ok)
                        {
                            response.IsSuccess = true;
                            response.ShippingAddressID = apiGetShipAddressResponse.address.customerAddressId;
                        }

                        response.ResponseCode = apiGetShipAddressResponse.messages.message[0].code;
                        response.ResponseText = apiGetShipAddressResponse.messages.message[0].text;
                        break;

                    case "deleteCustomerShippingAddressResponse":
                        serializer = new XmlSerializer(typeof(deleteCustomerShippingAddressResponse));
                        deleteCustomerShippingAddressResponse apideleteShippingResponse = (deleteCustomerShippingAddressResponse)serializer.Deserialize(new StringReader(xmldoc.DocumentElement.OuterXml));

                        if (apideleteShippingResponse.messages.resultCode == messageTypeEnum.Ok)
                        {
                            response.IsSuccess = true;
                        }

                        response.ResponseCode = apideleteShippingResponse.messages.message[0].code;
                        response.ResponseText = apideleteShippingResponse.messages.message[0].text;
                        break;

                    case "deleteCustomerPaymentProfileResponse":
                        serializer = new XmlSerializer(typeof(deleteCustomerPaymentProfileResponse));
                        deleteCustomerPaymentProfileResponse apidelpaymentprofileResponse = (deleteCustomerPaymentProfileResponse)serializer.Deserialize(new StringReader(xmldoc.DocumentElement.OuterXml));

                        if (apidelpaymentprofileResponse.messages.resultCode == messageTypeEnum.Ok)
                        {
                            response.IsSuccess = true;
                        }

                        response.ResponseCode = apidelpaymentprofileResponse.messages.message[0].code;
                        response.ResponseText = apidelpaymentprofileResponse.messages.message[0].text;
                        break;

                    case "deleteCustomerProfileResponse":
                        serializer = new XmlSerializer(typeof(deleteCustomerProfileResponse));
                        deleteCustomerProfileResponse apidelprofileResponse = (deleteCustomerProfileResponse)serializer.Deserialize(new StringReader(xmldoc.DocumentElement.OuterXml));

                        if (apidelprofileResponse.messages.resultCode == messageTypeEnum.Ok)
                        {
                            response.IsSuccess = true;
                        }

                        response.ResponseCode = apidelprofileResponse.messages.message[0].code;
                        response.ResponseText = apidelprofileResponse.messages.message[0].text;
                        break;

                    case "validateCustomerPaymentProfileResponse":
                        serializer = new XmlSerializer(typeof(validateCustomerPaymentProfileResponse));
                        validateCustomerPaymentProfileResponse apivalidateprofileResponse = (validateCustomerPaymentProfileResponse)serializer.Deserialize(new StringReader(xmldoc.DocumentElement.OuterXml));
                        if (apivalidateprofileResponse.messages.resultCode == messageTypeEnum.Ok)
                        {
                            response.IsSuccess = true;
                        }

                        response.ResponseCode = apivalidateprofileResponse.messages.message[0].code;
                        response.ResponseText = apivalidateprofileResponse.messages.message[0].text;
                        break;

                    case "getCustomerProfileResponse":
                        serializer = new XmlSerializer(typeof(getCustomerProfileResponse));
                        getCustomerProfileResponse getCustProfileResponse = (getCustomerProfileResponse)serializer.Deserialize(new StringReader(xmldoc.DocumentElement.OuterXml));

                        if (ShippingAddress == null && CreditCard == null)
                        {
                            if (getCustProfileResponse.messages.resultCode == messageTypeEnum.Ok)
                            {
                                response.IsSuccess = true;

                                if (getCustProfileResponse.profile.paymentProfiles != null || (getCustProfileResponse.profile.paymentProfiles == null && getCustProfileResponse.profile.shipToList.Length > 1))
                                {
                                    response.IsSuccess = false;
                                }
                            }
                        }

                        if (ShippingAddress != null)
                        {
                            if (getCustProfileResponse.profile.shipToList != null)
                            {
                                foreach (customerAddressExType shipAddress in getCustProfileResponse.profile.shipToList)
                                {
                                    if ((shipAddress.address == ShippingAddress.Street1 + ShippingAddress.Street2) && (shipAddress.city == ShippingAddress.City) && (shipAddress.state == ShippingAddress.StateCode) && (shipAddress.zip == ShippingAddress.PostalCode))
                                    {
                                        response.ShippingAddressID = shipAddress.customerAddressId;
                                        response.IsSuccess = false;
                                        break;
                                    }
                                }
                            }
                        }

                        if (CreditCard != null)
                        {
                            if (getCustProfileResponse.profile.paymentProfiles != null)
                            {
                                foreach (customerPaymentProfileMaskedType payProfile in getCustProfileResponse.profile.paymentProfiles)
                                {
                                    paymentMaskedType new_payment = new paymentMaskedType();
                                    creditCardType new_card = new creditCardType();
                                    string CardNumber = string.Empty;
                                    CardNumber = CreditCard.CardNumber.Trim();
                                    CardNumber = CardNumber.Substring((CardNumber.Length - 4), 4);
                                    new_card.cardNumber = "XXXX" + CardNumber;
                                    new_card.expirationDate = "XXXX";
                                    new_payment.Item = new_card;

                                    if (payProfile.payment.Item == new_card)
                                    {
                                        response.PaymentProfileID = payProfile.customerPaymentProfileId;
                                        response.IsSuccess = false;
                                        break;
                                    }
                                }
                            }
                        }

                        response.ResponseCode = getCustProfileResponse.messages.message[0].code;
                        response.ResponseText = getCustProfileResponse.messages.message[0].text;
                        break;

                    case "ErrorResponse":
                        serializer = new XmlSerializer(typeof(ANetApiResponse));
                        ANetApiResponse apiErrorResponse = (ANetApiResponse)serializer.Deserialize(new StringReader(xmldoc.DocumentElement.OuterXml));

                        response.ResponseCode = apiErrorResponse.messages.message[0].code;
                        response.ResponseText = apiErrorResponse.messages.message[0].text;
                        break;

                    default:
                        response.ResponseText = "Unexpected type of object: " + xmldoc.DocumentElement.Name;
                        response.IsSuccess = false;
                        break;
                }
            }
            catch (Exception ex)
            {
                response.ResponseText = ex.Message;
                response.IsSuccess = false;
            }

            return response;
        }

        //TODO : Change this to DB driven
        /// <summary>
        /// Znode Version 7.2.2
        /// This function will returns Test Authorize Net Gateway Address
        /// </summary>
        /// <returns>Test Authorize Net Gateway Address</returns>
        private string GetTestAuthorizeNetGatewayAddress()
        {
            string baseAddress = String.Empty;
            baseAddress = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["TestAuthorizeNetGatewayAddress"]);
            if (string.IsNullOrEmpty(baseAddress))
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("TestAuthorizeNetGatewayAddress key is missing from the configuration file.");
            }
            return baseAddress;
        }

        //TODO : Change this to DB driven
        /// <summary>
        ///Znode Version 7.2.2
        /// This function will returns Test Authorize Net Gateway Address
        /// </summary>
        /// <returns>Live Authorize Net Gateway Address</returns>
        private string GetLiveAuthorizeNetGatewayAddress()
        {
            string baseAddress = String.Empty;
            baseAddress = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["LiveAuthorizeNetGatewayAddress"]);
            if (string.IsNullOrEmpty(baseAddress))
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("LiveAuthorizeNetGatewayAddress key is missing from the configuration file.");
            }
            return baseAddress;
        }

        //TODO : Change this to DB driven
        /// <summary>
        /// Znode Version 7.2.2
        /// This function will returns whether the cart is used for live or test mode 
        /// Set it to False on Production
        /// </summary>
        /// <returns>bool true/false based on web.config key</returns>
        private bool GetMockCreditCard()
        {
            bool mockCreditCard = true;
            if (string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["MockCreditCard"]))
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("MockCreditCard key is missing from the configuration file.");
            }
            else
            {
                mockCreditCard = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["MockCreditCard"]);
            }
            return mockCreditCard;
        }

        //TODO : Change this to DB driven
        /// <summary>
        /// Znode Version 7.2.2
        /// This function will returns Authorize Net Version
        ///  Contains CCV support 
        /// </summary>
        /// <returns>Authorize Net Version based on web.config key</returns>
        private string GetAuthorizeNetVersion()
        {
            string authNetVersion = String.Empty;
            authNetVersion = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["AuthorizeNetVersion"]);
            if (string.IsNullOrEmpty(authNetVersion))
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("AuthorizeNetVersion key is missing from the configuration file.");
            }
            return authNetVersion;
        }

        #endregion
    }
}
