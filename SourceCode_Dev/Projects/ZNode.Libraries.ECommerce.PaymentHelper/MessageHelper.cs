using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Payment
{
    /// <summary>
    /// XML Message Helper
    /// </summary>
    internal class MessageHelper : ZNodePaymentBase
    {
        /// <summary>
        /// Makes XML out of an object.
        /// </summary>
        /// <param name="ObjectToSerialize">The object to serialize.</param>
        /// <returns>An XML string representing the object.</returns>        
        public static byte[] Serialize(object ObjectToSerialize)
        {
            System.Xml.Serialization.XmlSerializer Ser = new System.Xml.Serialization.XmlSerializer(ObjectToSerialize.GetType());

            using (MemoryStream MS = new MemoryStream())
            {
                System.Xml.XmlTextWriter W = new System.Xml.XmlTextWriter(MS, new UTF8Encoding(false));
                W.Formatting = System.Xml.Formatting.Indented;
                Ser.Serialize(W, ObjectToSerialize);
                W.Flush();
                W.Close();
                return MS.ToArray();
            }
        }

        /// <summary>
        /// Generic Deserialize method that will attempt to deserialize any class
        /// in the GCheckout.AutoGen namespace.
        /// </summary>
        /// <param name="Xml">The XML that should be made into an object.</param>
        /// <param name="ThisType">this type in an object</param>
        /// <returns>The reconstituted object.</returns>
        public static object Deserialize(string Xml, Type ThisType)
        {
            XmlSerializer myDeserializer = new XmlSerializer(ThisType);

            using (StringReader myReader = new StringReader(Xml))
            {
                return myDeserializer.Deserialize(myReader);
            }
        }
        
        /// <summary>
        /// Converts a string to bytes in UTF-8 encoding.
        /// </summary>
        /// <param name="InString">The string to convert.</param>
        /// <returns>The UTF-8 bytes.</returns>
        public static byte[] StringToUtf8Bytes(string InString)
        {
            UTF8Encoding utf8encoder = new UTF8Encoding(false, true);
            return utf8encoder.GetBytes(InString);
        }

        /// <summary>
        /// Converts bytes in UTF-8 encoding to a regular string.
        /// </summary>
        /// <param name="InBytes">The UTF-8 bytes.</param>
        /// <returns>The input bytes as a string.</returns>
        public static string Utf8BytesToString(byte[] InBytes)
        {
            UTF8Encoding utf8encoder = new UTF8Encoding(false, true);
            return utf8encoder.GetString(InBytes);
        }

        /// <summary>
        /// Get the top element in an xml
        /// </summary>
        /// <param name="Xml">XML string</param>
        /// <returns>Returns the top element</returns>
        public static string GetTopElement(string Xml)
        {
            return GetTopElement(StringToUtf8Bytes(Xml));
        }

        /// <summary>
        /// Gets the top element
        /// </summary>
        /// <param name="Xml">XML document Bytes</param>
        /// <returns>Returns the top element of the xml byte</returns>
        public static string GetTopElement(byte[] Xml)
        {
            using (MemoryStream ms = new MemoryStream(Xml))
            {
                return GetTopElement(ms);
            }
        }

        /// <summary>
        /// Returns the top element of the xml
        /// </summary>
        /// <param name="Xml">XML Stream</param>
        /// <returns>Returns the top element of the XML stream</returns>
        public static string GetTopElement(Stream Xml)
        {
            // set the begin postion so we can set the stream back when we are done.
            long beginPos = Xml.Position;
            XmlTextReader XReader = new XmlTextReader(Xml);
            XReader.WhitespaceHandling = WhitespaceHandling.None;            
            XReader.Read();
            XReader.Read();
            string RetVal = XReader.Name;

            // Do not close the stream, we will still need it for additional operations. reposition the stream to where it started
            Xml.Position = beginPos;
            return RetVal;
        }
    }
}
