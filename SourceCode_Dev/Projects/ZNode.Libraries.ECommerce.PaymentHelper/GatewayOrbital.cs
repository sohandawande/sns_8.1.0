
using System;
using System.Text;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.ECommerce.Payment.net.paymentech.wsvar1;

namespace ZNode.Libraries.ECommerce.Payment
{
    /// <summary>
    /// Submit payment to Paymentech Orbital gateway
    /// </summary>
    public class GatewayOrbital : ZNodePaymentBase
    {
        #region Public methods
        /// <summary>
        /// Paymenttech Orbital Gateway
        /// </summary>
        /// <param name="Gateway">Gateway Info</param>
        /// <param name="BillingAddress">Billing Address</param>
        /// <param name="ShippingAddress">Shipping Address</param>
        /// <param name="CreditCard">Credit Card</param>
        /// <returns>Returns the gateway response</returns>
        public GatewayResponse SubmitPayment(GatewayInfo Gateway, Address BillingAddress, Address ShippingAddress, CreditCard CreditCard)
        {
            GatewayResponse PaymentGatewayResponse = new GatewayResponse();
            NewOrderResponseElement _authenticationResponse = new NewOrderResponseElement();
            StringBuilder build = new StringBuilder();

            try
            {
                PaymentechGateway _OrbitalGateway = new PaymentechGateway();

                if (Gateway.GatewayURL.Trim().Length == 0)
                {
                    _OrbitalGateway.Url = this.GetOrbitalGatewayUrl(Gateway.TestMode);
                }
                else
                {
                    _OrbitalGateway.Url = Gateway.GatewayURL.Trim();
                }

                // Create a request bean
                NewOrderRequestElement _authenticationBean = new NewOrderRequestElement();

                // General Settings
                _authenticationBean.orbitalConnectionUsername = Gateway.GatewayLoginID;
                _authenticationBean.orbitalConnectionPassword = Gateway.GatewayPassword;
                _authenticationBean.industryType = "EC";
                _authenticationBean.bmlCustomerIP = Gateway.CustomerIPAddress;

                if (Gateway.PreAuthorize)
                {
                    _authenticationBean.transType = "A"; // Authorization only
                }
                else
                {
                    _authenticationBean.transType = "AC"; // Authorize and capture
                }

                // Gateway Merchant Settings
                _authenticationBean.merchantID = System.Configuration.ConfigurationManager.AppSettings["MerchantID"].ToString();
                _authenticationBean.bin = System.Configuration.ConfigurationManager.AppSettings["BIN"].ToString();
                _authenticationBean.terminalID = "001";

                // Invoice and  Credit card Information
                _authenticationBean.orderID = CreditCard.OrderID.ToString();
                _authenticationBean.ccAccountNum = CreditCard.CardNumber;
                string month = CreditCard.CreditCardExp.Substring(3) + CreditCard.CreditCardExp.Substring(0, 2);
                _authenticationBean.ccExp = month;
                _authenticationBean.ccCardVerifyNum = CreditCard.CardSecurityCode;

                // Customer Information.
                _authenticationBean.avsZip = BillingAddress.PostalCode;
                _authenticationBean.avsAddress1 = BillingAddress.Street1 + BillingAddress.Street2;
                _authenticationBean.avsCity = BillingAddress.City;
                _authenticationBean.avsState = BillingAddress.StateCode;
                _authenticationBean.avsName = BillingAddress.FirstName + BillingAddress.LastName;
                _authenticationBean.avsCountryCode = BillingAddress.CountryCode;
                _authenticationBean.avsPhone = BillingAddress.PhoneNumber;
                _authenticationBean.customerEmail = BillingAddress.EmailId;
                _authenticationBean.addProfileFromOrder = "A";
                
                // Retry Logic
                // retryTrace value should be unique.
                _authenticationBean.retryTrace = CreditCard.OrderID.ToString();

                // Amount Information
                _authenticationBean.amount = Convert.ToString(Math.Round((CreditCard.Amount * 100), 0));
                _authenticationBean.comments = string.Empty;

                // Response from transaction server.
                _authenticationResponse = _OrbitalGateway.NewOrder(_authenticationBean);
                
                // Get Approval status from server
                PaymentGatewayResponse.ResponseCode = _authenticationResponse.approvalStatus;

                // General Response from Payment Gateway
                PaymentGatewayResponse.AVSResponseCode = _authenticationResponse.avsRespCode;
                PaymentGatewayResponse.CCVResponsecode = _authenticationResponse.cvvRespCode;
                PaymentGatewayResponse.ReferenceNumber = _authenticationResponse.txRefNum;

                // Retry Logic Response.
                PaymentGatewayResponse.RetryTrace = _authenticationResponse.retryTrace;
                PaymentGatewayResponse.RetryAttemptCount = _authenticationResponse.retryAttempCount;
                PaymentGatewayResponse.LastRetryDate = _authenticationResponse.lastRetryDate;

                // if Transaction approved
                if (_authenticationResponse.approvalStatus == "1")
                {
                    PaymentGatewayResponse.IsSuccess = true;
                    PaymentGatewayResponse.TransactionId = _authenticationResponse.authorizationCode;
                    PaymentGatewayResponse.CardAuthCode = _authenticationResponse.authorizationCode;
                    PaymentGatewayResponse.ResponseText += _authenticationResponse.procStatusMessage;
                }
                else
                {
                    // if transaction declined
                    build.Append("Response Code: " + _authenticationResponse.respCodeMessage + Environment.NewLine);

                    if (_authenticationResponse.procStatusMessage.Length > 0)
                    {
                        build.Append("Description:" + _authenticationResponse.procStatusMessage + Environment.NewLine);
                    }

                    if (_authenticationResponse.authorizationCode.Length > 0)
                    {
                        build.Append("Authorization Code: " + _authenticationResponse.authorizationCode);
                    }

                    PaymentGatewayResponse.ResponseText = build.ToString();
                }
            }
            catch (Exception SOAPException)
            {
                PaymentGatewayResponse.ResponseText = "Error Code: " + SOAPException.Message + Environment.NewLine + _authenticationResponse.respCodeMessage;
            }

            // Return Payment GatewayResponse
            return PaymentGatewayResponse;
        }
  /// <summary>
        /// Paymenttech Orbital Gateway
        /// </summary>
        /// <param name="Gateway">Gateway Info</param>
        /// <param name="BillingAddress">Billing Address</param>
        /// <param name="ShippingAddress">Shipping Address</param>
        /// <param name="CreditCard">Credit Card</param>
        /// <returns>Returns the gateway response</returns>
        public GatewayResponse SubmitRecurringPayment(GatewayInfo Gateway, Address BillingAddress, Address ShippingAddress, CreditCard CreditCard, RecurringBillingInfo RecurringBillingInfo)
        {
            GatewayResponse PaymentGatewayResponse = new GatewayResponse();
            ZNode.Libraries.ECommerce.Payment.net.paymentech.wsvar1.NewOrderResponseElement _authenticationResponse = new ZNode.Libraries.ECommerce.Payment.net.paymentech.wsvar1.NewOrderResponseElement();
            StringBuilder build = new StringBuilder();

            try
            {
                ZNode.Libraries.ECommerce.Payment.net.paymentech.wsvar1.PaymentechGateway _OrbitalGateway = new ZNode.Libraries.ECommerce.Payment.net.paymentech.wsvar1.PaymentechGateway();

                if (Gateway.GatewayURL.Trim().Length == 0)
                {
                    _OrbitalGateway.Url = this.GetOrbitalGatewayUrl(Gateway.TestMode);
                }
                else
                {
                    _OrbitalGateway.Url = Gateway.GatewayURL.Trim();
                }

                // Create a request bean
                ZNode.Libraries.ECommerce.Payment.net.paymentech.wsvar1.NewOrderRequestElement _authenticationBean = new net.paymentech.wsvar1.NewOrderRequestElement();
            
                // General Settings
                _authenticationBean.industryType = "RC";
                _authenticationBean.bmlCustomerIP = Gateway.CustomerIPAddress;

                if (Gateway.PreAuthorize)
                {
                    _authenticationBean.transType = "A"; // Authorization only
                }
                else
                {
                    _authenticationBean.transType = "AC"; // Authorize and capture
                }

                // Gateway Merchant Settings
                _authenticationBean.merchantID = Gateway.GatewayLoginID;
                _authenticationBean.bin = Gateway.GatewayPassword;
                _authenticationBean.terminalID = "001";

                // Invoice and  Credit card Information
                _authenticationBean.orderID = CreditCard.OrderID.ToString();
                _authenticationBean.ccAccountNum = CreditCard.CardNumber;
                string month = CreditCard.CreditCardExp.Substring(3) + CreditCard.CreditCardExp.Substring(0, 2);
                _authenticationBean.ccExp = month;
                _authenticationBean.ccCardVerifyNum = CreditCard.CardSecurityCode;

                // Customer Information.
                _authenticationBean.avsZip = BillingAddress.PostalCode;
                _authenticationBean.avsAddress1 = BillingAddress.Street1 + BillingAddress.Street2;
                _authenticationBean.avsCity = BillingAddress.City;
                _authenticationBean.avsState = BillingAddress.StateCode;
                _authenticationBean.avsName = BillingAddress.FirstName + BillingAddress.LastName;
                _authenticationBean.avsCountryCode = BillingAddress.CountryCode;
                _authenticationBean.avsPhone = BillingAddress.PhoneNumber;
                _authenticationBean.customerEmail = BillingAddress.EmailId;
                _authenticationBean.addProfileFromOrder = "A";
                
                // Retry Logic
                // retryTrace value should be unique.
                _authenticationBean.retryTrace = CreditCard.OrderID.ToString();

                // Amount Information
                _authenticationBean.amount = Convert.ToString(Math.Round(RecurringBillingInfo.InitialAmount, 0));
                _authenticationBean.recurringInd = "RF";
                _authenticationBean.mbRecurringMaxBillings = RecurringBillingInfo.TotalCycles.ToString();
                _authenticationBean.mbRecurringFrequency = RecurringBillingInfo.Period;
                _authenticationBean.mbRecurringStartDate = DateTime.Now.ToString("MMDDYYYY");
                if (RecurringBillingInfo.Period == "DAY")
                    _authenticationBean.mbRecurringEndDate = DateTime.Now.AddDays(Convert.ToInt32(RecurringBillingInfo.Frequency) * RecurringBillingInfo.TotalCycles).ToString("MMDDYYYY");
                if (RecurringBillingInfo.Period == "WEEK")
                    _authenticationBean.mbRecurringEndDate = DateTime.Now.AddDays(Convert.ToInt32(RecurringBillingInfo.Frequency) * 7 * RecurringBillingInfo.TotalCycles).ToString("MMDDYYYY");

                if (RecurringBillingInfo.Period == "MONTH")
                    _authenticationBean.mbRecurringEndDate = DateTime.Now.AddMonths(Convert.ToInt32(RecurringBillingInfo.Frequency) * RecurringBillingInfo.TotalCycles).ToString("MMDDYYYY");

                if (RecurringBillingInfo.Period == "YEAR")
                    _authenticationBean.mbRecurringEndDate = DateTime.Now.AddYears(Convert.ToInt32(RecurringBillingInfo.Frequency) * RecurringBillingInfo.TotalCycles).ToString("MMDDYYYY");

                _authenticationBean.comments = string.Empty;

                // Response from transaction server.
                _authenticationResponse = _OrbitalGateway.NewOrder(_authenticationBean);
                
                // Get Approval status from server
                PaymentGatewayResponse.ResponseCode = _authenticationResponse.approvalStatus;

                // General Response from Payment Gateway
                PaymentGatewayResponse.AVSResponseCode = _authenticationResponse.avsRespCode;
                PaymentGatewayResponse.CCVResponsecode = _authenticationResponse.cvvRespCode;
                PaymentGatewayResponse.ReferenceNumber = _authenticationResponse.txRefNum;

                // Retry Logic Response.
                PaymentGatewayResponse.RetryTrace = _authenticationResponse.retryTrace;
                PaymentGatewayResponse.RetryAttemptCount = _authenticationResponse.retryAttempCount;
                PaymentGatewayResponse.LastRetryDate = _authenticationResponse.lastRetryDate;

                // if Transaction approved
                if (_authenticationResponse.approvalStatus == "1")
                {
                    PaymentGatewayResponse.IsSuccess = true;
                    PaymentGatewayResponse.TransactionId = _authenticationResponse.authorizationCode;
                    PaymentGatewayResponse.CardAuthCode = _authenticationResponse.authorizationCode;
                    PaymentGatewayResponse.ResponseText += _authenticationResponse.procStatusMessage;
                }
                else
                {
                    // if transaction declined
                    build.Append("Response Code: " + _authenticationResponse.respCodeMessage + Environment.NewLine);

                    if (_authenticationResponse.procStatusMessage.Length > 0)
                    {
                        build.Append("Description:" + _authenticationResponse.procStatusMessage + Environment.NewLine);
                    }

                    if (_authenticationResponse.authorizationCode.Length > 0)
                    {
                        build.Append("Authorization Code: " + _authenticationResponse.authorizationCode);
                    }

                    PaymentGatewayResponse.ResponseText = build.ToString();
                }
            }
            catch (Exception SOAPException)
            {
                PaymentGatewayResponse.ResponseText = "Error Code: " + SOAPException.Message + Environment.NewLine + _authenticationResponse.respCodeMessage;
            }

            // Return Payment GatewayResponse
            return PaymentGatewayResponse;
        }
        /// <summary>
        /// Void/Refund payment
        /// </summary>
        /// <param name="Gateway">Gateway Info</param>
        /// <param name="CreditCardInfo">Credit Card Info</param>
        /// <returns>Returns the gateway response</returns>
        public GatewayResponse ReversePayment(GatewayInfo Gateway, CreditCard CreditCardInfo)
        {
            GatewayResponse PaymentGatewayResponse = new GatewayResponse();
            ReversalElement reversalRequestElement = new ReversalElement();            
            
            try
            {
                // General settings
                reversalRequestElement.terminalID = "001";
                reversalRequestElement.orderID = CreditCardInfo.OrderID.ToString();
                reversalRequestElement.txRefNum = CreditCardInfo.TransactionID;
                reversalRequestElement.adjustedAmount = Convert.ToString(Math.Round((CreditCardInfo.Amount * 100), 0));
                
                // Gateway Merchant Settings
                reversalRequestElement.merchantID = Gateway.GatewayLoginID;
                reversalRequestElement.bin = Gateway.GatewayPassword;

                PaymentechGateway _OrbitalGateway = new PaymentechGateway();

                if (Gateway.GatewayURL.Trim().Length == 0)
                {
                    _OrbitalGateway.Url = this.GetOrbitalGatewayUrl(Gateway.TestMode);
                }
                else
                {
                    _OrbitalGateway.Url = Gateway.GatewayURL.Trim();
                }

                // response object
                ReversalResponseElement reversalResponse;

                // send request to gateway
                reversalResponse = _OrbitalGateway.Reversal(reversalRequestElement);

                // if Transaction approved
                if (reversalResponse.procStatus == "0")
                {
                    PaymentGatewayResponse.IsSuccess = true;                    
                    PaymentGatewayResponse.ResponseText += reversalResponse.procStatusMessage;
                }
                else
                {
                    StringBuilder build = new StringBuilder();

                    // if transaction declined
                    build.Append("Response Code: " + reversalResponse.procStatus + Environment.NewLine);
                    build.Append("Description:" + reversalResponse.procStatusMessage + Environment.NewLine);
                    
                    PaymentGatewayResponse.ResponseText = build.ToString();
                }
            }
            catch (Exception SOAPException)
            {
                PaymentGatewayResponse.ResponseText = "Error Code : " + SOAPException.Message;
            }

            return PaymentGatewayResponse;
        }

        /// <summary>
        /// Capture previously authorized transaction
        /// </summary>
        /// <param name="Gateway">Gateway Info</param>
        /// <param name="CreditCardInfo">Credit Card Info</param>
        /// <returns>Returns the gateway response</returns>
        public GatewayResponse CapturePayment(GatewayInfo Gateway, CreditCard CreditCardInfo)
        {
            GatewayResponse PaymentGatewayResponse = new GatewayResponse();
            MarkForCaptureElement requestElement = new MarkForCaptureElement();            

            try
            {
                // General settings
                requestElement.terminalID = "001";
                requestElement.orderID = CreditCardInfo.OrderID.ToString();
                requestElement.txRefNum = CreditCardInfo.TransactionID;
                requestElement.amount = Convert.ToString(Math.Round((CreditCardInfo.Amount * 100), 0));

                requestElement.taxInd = "1"; // tax included - Optional

                // Gateway Merchant Settings
                requestElement.merchantID = Gateway.GatewayLoginID;
                requestElement.bin = Gateway.GatewayPassword;

                PaymentechGateway _OrbitalGateway = new PaymentechGateway();

                if (Gateway.GatewayURL.Trim().Length == 0)
                {
                    _OrbitalGateway.Url = this.GetOrbitalGatewayUrl(Gateway.TestMode);
                }
                else
                {
                    _OrbitalGateway.Url = Gateway.GatewayURL.Trim();
                }

                // Response object
                MarkForCaptureResponseElement captureResponseElement;

                // Send request to gateway
                captureResponseElement = _OrbitalGateway.MarkForCapture(requestElement);

                // if Transaction approved
                if (captureResponseElement.procStatus == "0")
                {
                    PaymentGatewayResponse.IsSuccess = true;
                    PaymentGatewayResponse.ResponseText += captureResponseElement.procStatusMessage;
                }
                else
                {
                    StringBuilder build = new StringBuilder();

                    // if transaction declined
                    build.Append("Response Code: " + captureResponseElement.procStatus + Environment.NewLine);
                    build.Append("Description:" + captureResponseElement.procStatusMessage + Environment.NewLine);

                    PaymentGatewayResponse.ResponseText = build.ToString();
                }
            }
            catch (Exception SOAPException)
            {
                PaymentGatewayResponse.ResponseText = "Error Code : " + SOAPException.Message;
            }

            return PaymentGatewayResponse;
        }
        #endregion

        #region Private methods
        /// <summary>
        /// Gets the orbital gateway URL
        /// </summary>
        /// <param name="TestMode">Test Mode or not</param>
        /// <returns>Returns the orbital gateway URL</returns>
        private string GetOrbitalGatewayUrl(bool TestMode)
        {
            string _OrbitalGatewayUrl = string.Empty;

            // No URL was set, lets default this to a reasonable value.
            if (TestMode)
            {
                // Set to our Certification server.
                _OrbitalGatewayUrl = "https://wsvar.paymentech.net/PaymentechGateway";
            }
            else
            {
                // Set to the production server.
                _OrbitalGatewayUrl = "https://ws.paymentech.net/PaymentechGateway";
            }

            return _OrbitalGatewayUrl;
        }

        #endregion
    }
}
