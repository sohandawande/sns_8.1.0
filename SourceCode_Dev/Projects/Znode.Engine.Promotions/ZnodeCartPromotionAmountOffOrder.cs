using ZNode.Libraries.ECommerce.Entities;

namespace Znode.Engine.Promotions
{
	public class ZnodeCartPromotionAmountOffOrder : ZnodeCartPromotionType
	{
		public ZnodeCartPromotionAmountOffOrder()
		{
			Name = "Amount Off Order";
			Description = "Applies an amount off an entire order; affects the shopping cart.";
			AvailableForFranchise = false;

			Controls.Add(ZnodePromotionControl.Store);
			Controls.Add(ZnodePromotionControl.Profile);
			Controls.Add(ZnodePromotionControl.DiscountAmount);
			Controls.Add(ZnodePromotionControl.MinimumOrderAmount);
			Controls.Add(ZnodePromotionControl.Coupon);
		}

		/// <summary>
		/// Calculates the amount off an order.
		/// </summary>
		public override void Calculate(int? couponIndex)
		{
			var subTotal = ShoppingCart.SubTotal;
			var itemCount = CountCartItemsAndAddOns();

			if (PromotionBag.MinimumOrderAmount <= subTotal && PromotionBag.Coupon == null)
			{
				if (itemCount > 0)
				{
					var lineItemDiscount = PromotionBag.Discount / itemCount;
					ApplyDiscount(lineItemDiscount);
                    ShoppingCart.IsAnyPromotionApplied = true;
				}
			}
			else if (PromotionBag.Coupon != null)
			{
				var isCouponValid = ValidateCoupon(couponIndex);

				// Apply the discount
				if (PromotionBag.Coupon.MinimumOrderAmount <= subTotal && isCouponValid)
				{
					if (itemCount > 0)
					{
						var lineItemDiscount = PromotionBag.Coupon.Discount / itemCount;
						ApplyDiscount(lineItemDiscount);
					}

					PromotionBag.Coupon.CouponApplied = true;
					ShoppingCart.Coupons[couponIndex.Value].CouponApplied = true;
				}

				AddPromotionMessage(couponIndex.Value);
			}
		}

		private int CountCartItemsAndAddOns()
		{
			var count = 0;

			foreach (ZNodeShoppingCartItem cartItem in ShoppingCart.ShoppingCartItems)
			{
				var lineItemCount = 1;

				foreach (ZNodeAddOnEntity addOn in cartItem.Product.SelectedAddOns.AddOnCollection)
				{
					foreach (ZNodeAddOnValueEntity addOnValue in addOn.AddOnValueCollection)
					{
						if (addOnValue.Price > 0)
						{
							lineItemCount++;
						}
					}
				}

				count += (lineItemCount * cartItem.Quantity);
			}

			return count;
		}

		private void ApplyDiscount(decimal lineItemDiscount)
		{
			foreach (ZNodeShoppingCartItem cartItem in ShoppingCart.ShoppingCartItems)
			{
				cartItem.Product.DiscountAmount += lineItemDiscount;

				foreach (ZNodeAddOnEntity addOn in cartItem.Product.SelectedAddOns.AddOnCollection)
				{
					foreach (ZNodeAddOnValueEntity addOnValue in addOn.AddOnValueCollection)
					{
						if (addOnValue.Price > 0)
						{
							addOnValue.DiscountAmount += lineItemDiscount;
						}
					}
				}
			}
		}
	}
}
