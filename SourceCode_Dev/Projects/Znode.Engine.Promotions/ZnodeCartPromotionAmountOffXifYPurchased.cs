using ZNode.Libraries.ECommerce.Entities;

namespace Znode.Engine.Promotions
{
	public class ZnodeCartPromotionAmountOffXifYPurchased : ZnodeCartPromotionType
    {
		public ZnodeCartPromotionAmountOffXifYPurchased()
		{
			Name = "Amount Off X If Y Purchased";
			Description = "Applies an amount off product X if product Y is purchased; affects the shopping cart.";
			AvailableForFranchise = true;

			Controls.Add(ZnodePromotionControl.Store);
			Controls.Add(ZnodePromotionControl.Profile);
			Controls.Add(ZnodePromotionControl.DiscountAmount);
			Controls.Add(ZnodePromotionControl.RequiredProduct);
			Controls.Add(ZnodePromotionControl.RequiredProductMinimumQuantity);
			Controls.Add(ZnodePromotionControl.DiscountedProduct);
			Controls.Add(ZnodePromotionControl.DiscountedProductQuantity);
			Controls.Add(ZnodePromotionControl.Coupon);
		}

		/// <summary>
		/// Calculates the amount off an item (X) if another item (Y) is purchased.
		/// </summary>
        public override void Calculate(int? couponIndex)
        {
            var isCouponValid = false;
            if (!Equals(couponIndex, null))
            {
                isCouponValid = ValidateCoupon(couponIndex);
                isCouponValid &= PromotionBag.Coupon != null;
            }

            // Loop through each cart Item
            foreach (ZNodeShoppingCartItem cartItem in ShoppingCart.ShoppingCartItems)
            {
				if (cartItem.Product.ProductID == PromotionBag.DiscountedProductId)
                {
                    // Tiered pricing calculation                
                    var unitPrice = cartItem.TieredPricing;
					var requiredProductQtyOrdered = GetQuantityOrdered(PromotionBag.RequiredProductId);
					var requiredProductMinQty = PromotionBag.RequiredProductMinimumQuantity;
                    var discountedProductQty = cartItem.Quantity;
					var extendedPrice = unitPrice * PromotionBag.DiscountedProductQuantity;
					var isLastItem = IsLastItem(cartItem);

					if (PromotionBag.Coupon == null && isLastItem)
                    {
						if (PromotionBag.DiscountedProductId == PromotionBag.RequiredProductId)
                        {
							if (discountedProductQty == requiredProductQtyOrdered)
                            {
                                discountedProductQty = cartItem.Quantity - requiredProductMinQty;
                            }

							requiredProductMinQty += PromotionBag.DiscountedProductQuantity;
                        }

						if (requiredProductQtyOrdered >= requiredProductMinQty && PromotionBag.DiscountedProductQuantity <= discountedProductQty)
                        {
							cartItem.ExtendedPriceDiscount += extendedPrice < PromotionBag.Discount ? extendedPrice : PromotionBag.Discount;
                            ShoppingCart.IsAnyPromotionApplied = true;
                            break;
                        }
                    }
                    else if (isLastItem && isCouponValid)
                    {
						requiredProductMinQty = PromotionBag.Coupon.RequiredProductMinimumQuantity;

						if (PromotionBag.DiscountedProductId == PromotionBag.RequiredProductId)
                        {
							if (discountedProductQty == requiredProductQtyOrdered)
                            {
                                discountedProductQty = cartItem.Quantity - requiredProductMinQty;
                            }

							requiredProductMinQty += PromotionBag.DiscountedProductQuantity;
                        }

						if (requiredProductQtyOrdered >= requiredProductMinQty && PromotionBag.DiscountedProductQuantity <= discountedProductQty)
                        {
							cartItem.ExtendedPriceDiscount += extendedPrice < PromotionBag.Coupon.Discount ? extendedPrice : PromotionBag.Coupon.Discount;

							PromotionBag.Coupon.CouponApplied = true;
                            ShoppingCart.Coupons[couponIndex.Value].CouponApplied = true;
                            break;
                        }
                    }
                }
            }

			// Finally add promo message
            AddPromotionMessage(couponIndex);
        }    
    }
}
