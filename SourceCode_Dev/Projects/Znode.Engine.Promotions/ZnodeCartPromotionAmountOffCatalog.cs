using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.ECommerce.Entities;

namespace Znode.Engine.Promotions
{
    public class ZnodeCartPromotionAmountOffCatalog : ZnodeCartPromotionType
    {
        public ZnodeCartPromotionAmountOffCatalog()
        {
            Name = "Amount Off Catalog";
            Description = "Applies an amount off products for a particular catalog; affects the shopping cart.";
            AvailableForFranchise = false;

            Controls.Add(ZnodePromotionControl.Store);
            Controls.Add(ZnodePromotionControl.Profile);
            Controls.Add(ZnodePromotionControl.DiscountAmount);
            Controls.Add(ZnodePromotionControl.RequiredCatalog);
            Controls.Add(ZnodePromotionControl.RequiredCatalogMinimumQuantity);
            Controls.Add(ZnodePromotionControl.MinimumOrderAmount);
            Controls.Add(ZnodePromotionControl.Coupon);
        }

        /// <summary>
        /// Calculates the amount off products for a particular catalog in the shopping cart.
        /// </summary>
        public override void Calculate(int? couponIndex)
        {
            var isCouponValid = false;
            if (!Equals(couponIndex, null))
            {
                isCouponValid = ValidateCoupon(couponIndex);
                isCouponValid &= PromotionBag.Coupon != null;
            }
            var isPromotionApplied = false;

            // Loop through each cart Item
            foreach (ZNodeShoppingCartItem cartItem in ShoppingCart.ShoppingCartItems)
            {
                // First get the product categories
                var productCategories = DataRepository.ProductCategoryProvider.GetByProductID(cartItem.Product.ProductID);
                foreach (var category in productCategories)
                {
                    // Get the catalogs for the category
                    var catalogs = DataRepository.CategoryNodeProvider.GetByCategoryID(category.CategoryID);
                    foreach (var catalog in catalogs)
                    {
                        if (!PromotionBag.RequiredCatalogId.HasValue || PromotionBag.RequiredCatalogId == catalog.CatalogID)
                        {
                            var subTotal = ShoppingCart.SubTotal;
                            var qtyOrdered = GetQuantityOrdered(cartItem.Product.ProductID);

                            if (PromotionBag.Coupon == null && PromotionBag.RequiredCatalogMinimumQuantity <= qtyOrdered && PromotionBag.MinimumOrderAmount <= subTotal)
                            {
                                // Product discount amount
                                cartItem.Product.DiscountAmount += PromotionBag.Discount;
                                isPromotionApplied = true;
                                ShoppingCart.IsAnyPromotionApplied = isPromotionApplied;
                            }
                            else if (PromotionBag.Coupon != null && isCouponValid)
                            {
                                if (PromotionBag.Coupon.RequiredCatalogMinimumQuantity <= qtyOrdered && PromotionBag.Coupon.MinimumOrderAmount <= subTotal)
                                {
                                    // Product discount amount
                                    cartItem.Product.DiscountAmount += PromotionBag.Coupon.Discount;

                                    PromotionBag.Coupon.CouponApplied = true;
                                    ShoppingCart.Coupons[couponIndex.Value].CouponApplied = true;
                                    isPromotionApplied = true;
                                }
                            }

                            // Break out of the catalogs loop
                            break;
                        }
                    }
                    if (isPromotionApplied)
                        // Break out of the category loop
                        break;
                   
                }
            }

            AddPromotionMessage(couponIndex);
        }
    }
}
