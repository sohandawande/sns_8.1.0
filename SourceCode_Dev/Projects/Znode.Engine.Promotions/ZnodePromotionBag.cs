﻿using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Promotions
{
    /// <summary>
    /// Property bag of settings used by the promotions. 
    /// </summary>
	public class ZnodePromotionBag : ZNodeBusinessBase
    {
	    public ZnodeCoupon Coupon { get; set; }
		public int? PortalId { get; set; }
		public int? ProfileId { get; set; }
		public decimal Discount { get; set; }
		public decimal MinimumOrderAmount { get; set; }
		public int? RequiredBrandId { get; set; }
		public int RequiredBrandMinimumQuantity { get; set; }
		public int? RequiredCatalogId { get; set; }
		public int RequiredCatalogMinimumQuantity { get; set; }
		public int? RequiredCategoryId { get; set; }
		public int RequiredCategoryMinimumQuantity { get; set; }
		public int? RequiredProductId { get; set; }
		public int RequiredProductMinimumQuantity { get; set; }
		public int? DiscountedProductId { get; set; }
	    public int DiscountedProductQuantity { get; set; }
        public bool IsCouponAllowedWithOtherCoupons { get; set; }
    }
}
