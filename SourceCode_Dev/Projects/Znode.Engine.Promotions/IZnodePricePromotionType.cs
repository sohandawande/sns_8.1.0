﻿using ZNode.Libraries.ECommerce.Entities;

namespace Znode.Engine.Promotions
{
	/// <summary>
	/// This is the interface for all price promotion types.
	/// </summary>
	public interface IZnodePricePromotionType : IZnodePromotionType
	{
		void Bind(ZnodePromotionBag promotionBag);
		decimal PromotionalPrice(ZNodeProductBaseEntity product, decimal currentPrice);
		decimal PromotionalPrice(int productId, decimal currentPrice);
	}
}
