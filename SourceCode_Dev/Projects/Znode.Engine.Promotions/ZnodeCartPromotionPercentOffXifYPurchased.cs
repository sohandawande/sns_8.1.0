using ZNode.Libraries.ECommerce.Entities;

namespace Znode.Engine.Promotions
{
	public class ZnodeCartPromotionPercentOffXifYPurchased : ZnodeCartPromotionType
	{
		public ZnodeCartPromotionPercentOffXifYPurchased()
		{
			Name = "Percent Off X If Y Purchased";
			Description = "Applies a percent off product X if product Y is purchased; affects the shopping cart.";
			AvailableForFranchise = true;

			Controls.Add(ZnodePromotionControl.Store);
			Controls.Add(ZnodePromotionControl.Profile);
			Controls.Add(ZnodePromotionControl.DiscountPercent);
			Controls.Add(ZnodePromotionControl.RequiredProduct);
			Controls.Add(ZnodePromotionControl.RequiredProductMinimumQuantity);
			Controls.Add(ZnodePromotionControl.DiscountedProduct);
			Controls.Add(ZnodePromotionControl.DiscountedProductQuantity);
			Controls.Add(ZnodePromotionControl.Coupon);
		}

		/// <summary>
		/// Calculates the percentage off an item (X) if another item (Y) is purchased.
		/// </summary>
        public override void Calculate(int? couponIndex)
		{
            var isCouponValid = false;
            if (!Equals(couponIndex, null))
            {
                isCouponValid = ValidateCoupon(couponIndex);
                isCouponValid &= PromotionBag.Coupon != null;
            }
			// Loop through each cart Item
			foreach (ZNodeShoppingCartItem cartItem in ShoppingCart.ShoppingCartItems)
			{
				if (cartItem.Product.ProductID == PromotionBag.DiscountedProductId)
				{
					// Tiered pricing calculation
					var unitPrice = cartItem.TieredPricing;
					var requiredProductQtyOrdered = GetQuantityOrdered(PromotionBag.RequiredProductId);
					var requiredProductMinQty = PromotionBag.RequiredProductMinimumQuantity;
					var discountedProductQty = cartItem.Quantity;
					var extendedPrice = unitPrice * PromotionBag.DiscountedProductQuantity;

					if (PromotionBag.Coupon == null)
					{
						if (PromotionBag.DiscountedProductId == PromotionBag.RequiredProductId)
						{
							if (discountedProductQty == requiredProductQtyOrdered)
							{
								discountedProductQty = cartItem.Quantity - requiredProductMinQty;
							}

							requiredProductMinQty += PromotionBag.DiscountedProductQuantity;
						}

						if (requiredProductQtyOrdered >= requiredProductMinQty && PromotionBag.DiscountedProductQuantity <= discountedProductQty)
						{
							cartItem.ExtendedPriceDiscount += extendedPrice * (PromotionBag.Discount / 100);
                            ShoppingCart.IsAnyPromotionApplied = true;
							break;
						}
					}
					else if (isCouponValid)
					{
						requiredProductMinQty = PromotionBag.Coupon.RequiredProductMinimumQuantity;

						if (PromotionBag.DiscountedProductId == PromotionBag.RequiredProductId)
						{
							if (discountedProductQty == requiredProductQtyOrdered)
							{
								discountedProductQty = cartItem.Quantity - requiredProductMinQty;
							}

							requiredProductMinQty += PromotionBag.DiscountedProductQuantity;
						}

						if (requiredProductQtyOrdered >= requiredProductMinQty && PromotionBag.DiscountedProductQuantity <= discountedProductQty)
						{
							cartItem.ExtendedPriceDiscount += extendedPrice * (PromotionBag.Coupon.Discount / 100);

							PromotionBag.Coupon.CouponApplied = true;
                            ShoppingCart.Coupons[couponIndex.Value].CouponApplied = true;
							break;
						}
					}
				}
			}

			// Finally add promo message
            AddPromotionMessage(couponIndex);
		}
	}
}
