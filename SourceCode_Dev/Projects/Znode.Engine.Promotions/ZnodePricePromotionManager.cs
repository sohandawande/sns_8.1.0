using System;
using System.Linq;
using System.Reflection;
using System.Web;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Promotions
{
	/// <summary>
	/// Helps manage price promotions.
	/// </summary>
	public class ZnodePricePromotionManager : ZnodePromotionManager
	{
		private ZNodeGenericCollection<IZnodePricePromotionType> _pricePromotions;

		public ZnodePricePromotionManager()
		{
			_pricePromotions = new ZNodeGenericCollection<IZnodePricePromotionType>();

			var currentPortalId = ZNodeConfigManager.SiteConfig.PortalID;

			Profile profile = null;
			int? currentProfileId = null;

			if (HttpContext.Current.Session["ProfileCache"] != null)
			{
				profile = HttpContext.Current.Session["ProfileCache"] as Profile;
			}

			if (profile != null)
			{
				currentProfileId = profile.ProfileID;
			}

			var pricePromotionsFromCache = (TList<Promotion>)PricePromotionCache.Clone();
			ApplyPromotionsFilter(pricePromotionsFromCache, currentPortalId, currentProfileId);

			// Sort the promotions, build the list, then kill the cache clone
			pricePromotionsFromCache.Sort("DisplayOrder");
			BuildPromotionsList(pricePromotionsFromCache, currentPortalId, currentProfileId);
			pricePromotionsFromCache.Dispose();
		}

		/// <summary>
		/// Calculates the final promotional price for the given product.
		/// </summary>
		/// <param name="product">The product on which the promotional price is to be used.</param>
		/// <returns>The promotional price for the product.</returns>
		public decimal PromotionalPrice(ZNodeProductBaseEntity product)
		{
			return PromotionalPrice(product.ProductID, product.ProductPrice);
		}

		/// <summary>
		/// Calculates the final promotional price for the given product.
		/// </summary>
		/// <param name="product">The product on which the promotional price is to be used.</param>
		/// <param name="tieredPrice">The tiered price of the product.</param>
		/// <returns>The promotional price for the product.</returns>
		public decimal PromotionalPrice(ZNodeProductBaseEntity product, decimal tieredPrice)
		{
			return PromotionalPrice(product.ProductID, tieredPrice);
		}

		/// <summary>
		/// Calculates the final promotional price for the given product.
		/// </summary>
		/// <param name="productId">The ID of the product on which the promotional price is to be used.</param>
		/// <param name="currentPrice">The current price of the product.</param>
		/// <returns>The promotional price for the product.</returns>
		public decimal PromotionalPrice(int productId, decimal currentPrice)
		{
			var promoPrice = currentPrice;

			_pricePromotions.Sort("Precedence");

			foreach (ZnodePricePromotionType promo in _pricePromotions)
			{
				promoPrice = promo.PromotionalPrice(productId, promoPrice);
			}

			return promoPrice;
		}

		private void ApplyPromotionsFilter(TList<Promotion> promotionsFromCache, int? currentPortalId, int? currentProfileId)
		{
			promotionsFromCache.ApplyFilter(
				promo => (promo.ProfileID == currentProfileId || promo.ProfileID == null) &&
					     (promo.PortalID == currentPortalId || promo.PortalID == null) &&
						 promo.DiscountTypeIDSource.ClassType.Equals("PRICE", StringComparison.OrdinalIgnoreCase));
		}

		private void BuildPromotionsList(TList<Promotion> promotionsFromCache, int? currentPortalId, int? currentProfileId)
		{
			foreach (var promotion in promotionsFromCache)
			{
				var promoBag = BuildPromotionBag(promotion, currentPortalId, currentProfileId);
				AddPromotionType(promotion, promoBag);
			}
		}

		private ZnodePromotionBag BuildPromotionBag(Promotion promotion, int? currentPortalId, int? currentProfileId)
		{
			var promoBag = new ZnodePromotionBag();
			promoBag.PortalId = currentPortalId;
			promoBag.ProfileId = currentProfileId;
			promoBag.Discount = promotion.Discount;
			promoBag.MinimumOrderAmount = promotion.OrderMinimum.GetValueOrDefault(0);
			promoBag.RequiredProductId = promotion.ProductID.GetValueOrDefault(0);
			promoBag.RequiredProductMinimumQuantity = promotion.QuantityMinimum.GetValueOrDefault(1);
			promoBag.DiscountedProductId = promotion.PromotionProductID.GetValueOrDefault(0);
			promoBag.DiscountedProductQuantity = promotion.PromotionProductQty.GetValueOrDefault(1);
			promoBag.RequiredBrandId = promotion.ManufacturerID;
			promoBag.RequiredBrandMinimumQuantity = promotion.QuantityMinimum.GetValueOrDefault(1);
			promoBag.RequiredCatalogId = promotion.CatalogID;
			promoBag.RequiredCatalogMinimumQuantity = promotion.QuantityMinimum.GetValueOrDefault(1);
			promoBag.RequiredCategoryId = promotion.CategoryID;
			promoBag.RequiredCategoryMinimumQuantity = promotion.QuantityMinimum.GetValueOrDefault(1);

			if (promotion.CouponInd)
			{
				promoBag.Coupon = GetCoupon(promotion);
			}

			if (!String.IsNullOrEmpty(promotion.PromotionMessage) && !promotion.CouponInd)
			{
				if (promoBag.Coupon == null)
				{
					promoBag.Coupon = new ZnodeCoupon();
				}

				promoBag.Coupon.PromotionMessage = promotion.PromotionMessage;
			}

			return promoBag;
		}

		private void AddPromotionType(Promotion promotion, ZnodePromotionBag promotionBag)
		{
			var availablePromoTypes = GetAvailablePromotionTypes();
			foreach (var t in availablePromoTypes.Where(t => t.ClassName == promotion.DiscountTypeIDSource.ClassName))
			{
				try
				{
					var promoTypeAssembly = Assembly.GetAssembly(t.GetType());
					var pricePromo = (IZnodePricePromotionType)promoTypeAssembly.CreateInstance(t.ToString());

					if (pricePromo != null)
					{
						pricePromo.Precedence = promotion.DisplayOrder;
						pricePromo.Bind(promotionBag);
						_pricePromotions.Add(pricePromo);
					}
				}
				catch (Exception ex)
				{
					ZNodeLogging.LogMessage("Error while instantiating promotion type: " + t);
					ZNodeLogging.LogMessage(ex.ToString());
				}
			}
		}
	}
}
