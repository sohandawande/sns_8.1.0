using ZNode.Libraries.ECommerce.Entities;

namespace Znode.Engine.Promotions
{
    public class ZnodeCartPromotionPercentOffProduct : ZnodeCartPromotionType
    {
        public ZnodeCartPromotionPercentOffProduct()
        {
            Name = "Percent Off Product";
            Description = "Applies a percent off a product for an order; affects the shopping cart.";
            AvailableForFranchise = true;

            Controls.Add(ZnodePromotionControl.Store);
            Controls.Add(ZnodePromotionControl.Profile);
            Controls.Add(ZnodePromotionControl.DiscountPercent);
            Controls.Add(ZnodePromotionControl.RequiredProduct);
            Controls.Add(ZnodePromotionControl.RequiredProductMinimumQuantity);
            Controls.Add(ZnodePromotionControl.MinimumOrderAmount);
            Controls.Add(ZnodePromotionControl.Coupon);
        }

        /// <summary>
        /// Calculates the percent off a product in the shopping cart.
        /// </summary>
        public override void Calculate(int? couponIndex)
        {
            var isCouponValid = false;
            if (!Equals(couponIndex, null))
            {
                isCouponValid = ValidateCoupon(couponIndex);
                isCouponValid &= PromotionBag.Coupon != null;
            }
            // Loop through each cart Item
            foreach (ZNodeShoppingCartItem cartItem in ShoppingCart.ShoppingCartItems)
            {
                if (cartItem.Product.ProductID == PromotionBag.RequiredProductId)
                {
                    var subTotal = ShoppingCart.SubTotal;
                    var qtyOrdered = GetQuantityOrdered(cartItem.Product.ProductID);

                    var pricePromoManager = new ZnodePricePromotionManager();
                    var basePrice = pricePromoManager.PromotionalPrice(cartItem.Product, cartItem.TieredPricing);

                    if (PromotionBag.Coupon == null && PromotionBag.RequiredProductMinimumQuantity <= qtyOrdered && PromotionBag.MinimumOrderAmount <= subTotal)
                    {
                        // Product discount percent
                        cartItem.Product.DiscountAmount += basePrice * (PromotionBag.Discount / 100);
                        ShoppingCart.IsAnyPromotionApplied = true;
                    }
                    else if (PromotionBag.Coupon != null && isCouponValid)
                    {
                        if (PromotionBag.Coupon.RequiredProductMinimumQuantity <= qtyOrdered && PromotionBag.Coupon.MinimumOrderAmount <= subTotal)
                        {
                            // Product discount percent
                            cartItem.Product.DiscountAmount += basePrice * (PromotionBag.Coupon.Discount / 100);

                            PromotionBag.Coupon.CouponApplied = true;
                            ShoppingCart.Coupons[couponIndex.Value].CouponApplied = true;
                        }
                    }
                }
            }

            AddPromotionMessage(couponIndex);
        }
    }
}
