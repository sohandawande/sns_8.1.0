namespace Znode.Engine.Promotions
{
	public class ZnodeCartPromotionAmountOffShipping : ZnodeCartPromotionType
	{
		public ZnodeCartPromotionAmountOffShipping()
		{
			Name = "Amount Off Shipping";
			Description = "Applies an amount off shipping for an order; affects the shopping cart.";
			AvailableForFranchise = false;

			Controls.Add(ZnodePromotionControl.Store);
			Controls.Add(ZnodePromotionControl.Profile);
			Controls.Add(ZnodePromotionControl.DiscountAmount);
			Controls.Add(ZnodePromotionControl.MinimumOrderAmount);
			Controls.Add(ZnodePromotionControl.Coupon);
		}

		/// <summary>
		/// Calculates the amount off shipping for the order.
		/// </summary>
		public override void Calculate(int? couponIndex)
		{
			var subTotal = ShoppingCart.SubTotal;

			if (PromotionBag.MinimumOrderAmount <= subTotal && PromotionBag.Coupon == null)
			{
				ShoppingCart.Shipping.ShippingDiscount += PromotionBag.Discount;
                ShoppingCart.IsAnyPromotionApplied = true;
			}
			else if (PromotionBag.Coupon != null)
			{
				var isCouponValid = ValidateCoupon(couponIndex);

				if (PromotionBag.Coupon.MinimumOrderAmount <= subTotal && isCouponValid)
				{
					ShoppingCart.Shipping.ShippingDiscount += PromotionBag.Coupon.Discount;
					PromotionBag.Coupon.CouponApplied = true;
					ShoppingCart.Coupons[couponIndex.Value].CouponApplied = true;
				}

				AddPromotionMessage(couponIndex);
			}
		}
	}
}
