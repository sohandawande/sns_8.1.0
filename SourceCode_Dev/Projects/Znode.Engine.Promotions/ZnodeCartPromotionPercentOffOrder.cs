using ZNode.Libraries.ECommerce.Entities;

namespace Znode.Engine.Promotions
{
	public class ZnodeCartPromotionPercentOffOrder : ZnodeCartPromotionType
	{
		public ZnodeCartPromotionPercentOffOrder()
		{
			Name = "Percent Off Order";
			Description = "Applies a percent off an entire order; affects the shopping cart.";
			AvailableForFranchise = false;

			Controls.Add(ZnodePromotionControl.Store);
			Controls.Add(ZnodePromotionControl.Profile);
			Controls.Add(ZnodePromotionControl.DiscountPercent);
			Controls.Add(ZnodePromotionControl.MinimumOrderAmount);
			Controls.Add(ZnodePromotionControl.Coupon);
		}

		/// <summary>
		/// Calculates the percent off an order.
		/// </summary>
		public override void Calculate(int? couponIndex)
		{
			var subTotal = ShoppingCart.SubTotal;

			if (PromotionBag.MinimumOrderAmount <= subTotal && PromotionBag.Coupon == null)
			{
				var discount = PromotionBag.Discount / 100;
				ApplyDiscount(discount);
                ShoppingCart.IsAnyPromotionApplied = true;
			}
			else if (PromotionBag.Coupon != null)
			{
				var isCouponValid = ValidateCoupon(couponIndex);

				// Apply discount
				if (PromotionBag.Coupon.MinimumOrderAmount <= subTotal && isCouponValid)
				{
					var discount = PromotionBag.Coupon.Discount / 100;
					ApplyDiscount(discount);

					PromotionBag.Coupon.CouponApplied = true;
					ShoppingCart.Coupons[couponIndex.Value].CouponApplied = true;
				}

				AddPromotionMessage(couponIndex);
			}
		}

		private void ApplyDiscount(decimal discount)
		{
			foreach (ZNodeShoppingCartItem cartItem in ShoppingCart.ShoppingCartItems)
			{
				var finalPrice = cartItem.PromotionalPrice;
				var lineItemDiscount = finalPrice * discount;

				cartItem.Product.DiscountAmount += lineItemDiscount;

				foreach (ZNodeAddOnEntity addOn in cartItem.Product.SelectedAddOns.AddOnCollection)
				{
					foreach (ZNodeAddOnValueEntity addOnValue in addOn.AddOnValueCollection)
					{
						lineItemDiscount = addOnValue.Price * discount;
						addOnValue.DiscountAmount += lineItemDiscount;
					}
				}
			}
		}
	}
}
