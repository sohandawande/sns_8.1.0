using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.ECommerce.Entities;

namespace Znode.Engine.Promotions
{
	public class ZnodeCartPromotionPercentOffCatalog : ZnodeCartPromotionType
	{
		public ZnodeCartPromotionPercentOffCatalog()
		{
			Name = "Percent Off Catalog";
			Description = "Applies a percent off products for a particular catalog; affects the shopping cart.";
			AvailableForFranchise = false;

			Controls.Add(ZnodePromotionControl.Store);
			Controls.Add(ZnodePromotionControl.Profile);
			Controls.Add(ZnodePromotionControl.DiscountPercent);
			Controls.Add(ZnodePromotionControl.RequiredCatalog);
			Controls.Add(ZnodePromotionControl.RequiredCatalogMinimumQuantity);
			Controls.Add(ZnodePromotionControl.MinimumOrderAmount);
			Controls.Add(ZnodePromotionControl.Coupon);
		}

		/// <summary>
		/// Calculates the percent off products for a particular catalog in the shopping cart.
		/// </summary>
        public override void Calculate(int? couponIndex)
		{
            var isCouponValid = false;
            if (!Equals(couponIndex, null))
            {
                isCouponValid = ValidateCoupon(couponIndex);
                isCouponValid &= PromotionBag.Coupon != null;
            }
            var isPromotionApplied = false;

			// Loop through each cart Item
			foreach (ZNodeShoppingCartItem cartItem in ShoppingCart.ShoppingCartItems)
			{
				// First get the product categories
				var productCategories = DataRepository.ProductCategoryProvider.GetByProductID(cartItem.Product.ProductID);
				foreach (var category in productCategories)
				{
					// Get the catalogs for the category
					var catalogs = DataRepository.CategoryNodeProvider.GetByCategoryID(category.CategoryID);
					foreach (var catalog in catalogs)
					{
						if (!PromotionBag.RequiredCatalogId.HasValue || PromotionBag.RequiredCatalogId == catalog.CatalogID)
						{
							var subTotal = ShoppingCart.SubTotal;
							var qtyOrdered = GetQuantityOrdered(cartItem.Product.ProductID);

							var pricePromoManager = new ZnodePricePromotionManager();
							var basePrice = pricePromoManager.PromotionalPrice(cartItem.Product, cartItem.TieredPricing);

							if (PromotionBag.Coupon == null && PromotionBag.RequiredCatalogMinimumQuantity <= qtyOrdered && PromotionBag.MinimumOrderAmount <= subTotal)
							{
								// Product discount percent
								cartItem.Product.DiscountAmount += basePrice * (PromotionBag.Discount / 100);
                                isPromotionApplied = true;
							}
							else if (PromotionBag.Coupon != null && isCouponValid)
							{
								if (PromotionBag.Coupon.RequiredCatalogMinimumQuantity <= qtyOrdered && PromotionBag.Coupon.MinimumOrderAmount <= subTotal)
								{
									// Product discount percent
									cartItem.Product.DiscountAmount += basePrice * (PromotionBag.Coupon.Discount / 100);

									PromotionBag.Coupon.CouponApplied = true;
                                    ShoppingCart.Coupons[couponIndex.Value].CouponApplied = true;
                                    isPromotionApplied = true;
								}
							}

							// Break out of the catalogs loop
							break;
						}
					}
                    if (isPromotionApplied)
                        // Break out of the category loop
                        break;
				}
			}

            AddPromotionMessage(couponIndex);
		}
	}
}
