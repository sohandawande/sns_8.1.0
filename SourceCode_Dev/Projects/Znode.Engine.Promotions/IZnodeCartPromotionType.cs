﻿using ZNode.Libraries.ECommerce.Entities;

namespace Znode.Engine.Promotions
{
    /// <summary>
    /// This is the interface for all shopping cart promotion types.
    /// </summary>
    public interface IZnodeCartPromotionType : IZnodePromotionType
    {
        void Bind(ZNodeShoppingCart shoppingCart, ZnodePromotionBag promotionBag);
        void Calculate(int? couponIndex);
        bool PreSubmitOrderProcess();
        void PostSubmitOrderProcess();

        bool IsPromoCouponAvailable
        {
            get;
            set;
        }
    }
}
