using ZNode.Libraries.ECommerce.Entities;

namespace Znode.Engine.Promotions
{
	public class ZnodeCartPromotionAmountOffProduct : ZnodeCartPromotionType
	{
		public ZnodeCartPromotionAmountOffProduct()
		{
			Name = "Amount Off Product";
			Description = "Applies an amount off a product for an order; affects the shopping cart.";
			AvailableForFranchise = true;

			Controls.Add(ZnodePromotionControl.Store);
			Controls.Add(ZnodePromotionControl.Profile);
			Controls.Add(ZnodePromotionControl.DiscountAmount);
			Controls.Add(ZnodePromotionControl.RequiredProduct);
			Controls.Add(ZnodePromotionControl.RequiredProductMinimumQuantity);
			Controls.Add(ZnodePromotionControl.MinimumOrderAmount);
			Controls.Add(ZnodePromotionControl.Coupon);
		}

		/// <summary>
		/// Calculates the amount off a product in the shopping cart.
		/// </summary>
		public override void Calculate(int? couponIndex)
		{
            var isCouponValid = false;
            if (!Equals(couponIndex, null))
            {
                isCouponValid = ValidateCoupon(couponIndex);
                isCouponValid &= PromotionBag.Coupon != null;
            }

			// Loop through each cart Item
			foreach (ZNodeShoppingCartItem cartItem in ShoppingCart.ShoppingCartItems)
			{
				if (cartItem.Product.ProductID == PromotionBag.RequiredProductId)
				{
					var subTotal = ShoppingCart.SubTotal;
					var qtyOrdered = GetQuantityOrdered(cartItem.Product.ProductID);

					if (PromotionBag.Coupon == null && PromotionBag.RequiredProductMinimumQuantity <= qtyOrdered && PromotionBag.MinimumOrderAmount <= subTotal)
					{
						// Product discount amount
						cartItem.Product.DiscountAmount += PromotionBag.Discount;
                        ShoppingCart.IsAnyPromotionApplied = true;
					}
					else if (PromotionBag.Coupon != null && isCouponValid)
					{
						if (PromotionBag.Coupon.RequiredProductMinimumQuantity <= qtyOrdered && PromotionBag.Coupon.MinimumOrderAmount <= subTotal)
						{
							// Product discount amount
							cartItem.Product.DiscountAmount += PromotionBag.Coupon.Discount;

							PromotionBag.Coupon.CouponApplied = true;
							ShoppingCart.Coupons[couponIndex.Value].CouponApplied = true;
						}
					}
				}
			}

			AddPromotionMessage(couponIndex);
		}
	}
}
