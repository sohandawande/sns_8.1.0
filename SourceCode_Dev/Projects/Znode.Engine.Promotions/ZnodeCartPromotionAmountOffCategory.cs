using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.ECommerce.Entities;

namespace Znode.Engine.Promotions
{
	public class ZnodeCartPromotionAmountOffCategory : ZnodeCartPromotionType
	{
		public ZnodeCartPromotionAmountOffCategory()
		{
			Name = "Amount Off Category";
			Description = "Applies an amount off products in a particular category; affects the shopping cart.";
			AvailableForFranchise = false;

			Controls.Add(ZnodePromotionControl.Store);
			Controls.Add(ZnodePromotionControl.Profile);
			Controls.Add(ZnodePromotionControl.DiscountAmount);
			Controls.Add(ZnodePromotionControl.RequiredCategory);
			Controls.Add(ZnodePromotionControl.RequiredCategoryMinimumQuantity);
			Controls.Add(ZnodePromotionControl.MinimumOrderAmount);
			Controls.Add(ZnodePromotionControl.Coupon);
		}

		/// <summary>
		/// Calculates the amount off products in a particular category in the shopping cart.
		/// </summary>
        public override void Calculate(int? couponIndex)
		{
            var isCouponValid = false;
            if (!Equals(couponIndex, null))
            {
                isCouponValid = ValidateCoupon(couponIndex);
                isCouponValid &= PromotionBag.Coupon != null;
            }

			// Loop through each cart Item
			foreach (ZNodeShoppingCartItem cartItem in ShoppingCart.ShoppingCartItems)
			{
				var productCategories = DataRepository.ProductCategoryProvider.GetByProductID(cartItem.Product.ProductID);
				foreach (var item in productCategories)
				{
					if (!PromotionBag.RequiredCategoryId.HasValue || PromotionBag.RequiredCategoryId.Value == item.CategoryID)
					{
						var subTotal = ShoppingCart.SubTotal;
						var qtyOrdered = GetQuantityOrdered(cartItem.Product.ProductID);

						if (PromotionBag.Coupon == null && PromotionBag.RequiredCategoryMinimumQuantity <= qtyOrdered && PromotionBag.MinimumOrderAmount <= subTotal)
						{
							// Product discount amount
							cartItem.Product.DiscountAmount += PromotionBag.Discount;
                            ShoppingCart.IsAnyPromotionApplied = true;
						}
						else if (PromotionBag.Coupon != null && isCouponValid)
						{
							if (PromotionBag.Coupon.RequiredCategoryMinimumQuantity <= qtyOrdered && PromotionBag.Coupon.MinimumOrderAmount <= subTotal)
							{
								// Product discount amount
								cartItem.Product.DiscountAmount += PromotionBag.Coupon.Discount;

								PromotionBag.Coupon.CouponApplied = true;
                                ShoppingCart.Coupons[couponIndex.Value].CouponApplied = true;
							}
						}

						// Get out of the product category loop
						break;
					}
				}
			}

            AddPromotionMessage(couponIndex);
		}
	}
}
