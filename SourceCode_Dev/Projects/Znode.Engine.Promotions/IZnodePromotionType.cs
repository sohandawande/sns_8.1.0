﻿using System.Collections.ObjectModel;
using ZNode.Libraries.ECommerce.Entities;

namespace Znode.Engine.Promotions
{
	/// <summary>
	/// This is the root interface for all promotion types.
	/// </summary>
	public interface IZnodePromotionType : IZnodeProviderType
	{
		int Precedence { get; set; }
		bool AvailableForFranchise { get; set; }
		Collection<ZnodePromotionControl> Controls { get; }
	}
}
