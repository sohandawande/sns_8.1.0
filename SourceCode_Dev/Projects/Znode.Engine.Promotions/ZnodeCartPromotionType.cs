using System;
using System.Collections.Generic;
using ZNode.Libraries.ECommerce.Entities;
using System.Configuration;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Promotions
{
    /// <summary>
    /// Provides the base implementation for all shopping cart promotion types.
    /// </summary>
    public abstract class ZnodeCartPromotionType : ZnodePromotionType, IZnodeCartPromotionType
    {
        private bool _IsPromoCouponAvailable = false;
        private bool _IsPromoAllowsMultipleCoupon = false;

        public ZNodeShoppingCart ShoppingCart { get; set; }
        public ZnodePromotionBag PromotionBag { get; set; }


        public bool IsPromoCouponAvailable
        {
            get
            {
                return _IsPromoCouponAvailable;
            }
            set
            {
                _IsPromoCouponAvailable = value;
            }
        }
        /// <summary>
        /// Binds the shopping cart and promotion data to the promotion.
        /// </summary>
        /// <param name="shoppingCart">The current shopping cart.</param>
        /// <param name="promotionBag">The promotion properties.</param>
        /// <param name="couponIndex">Index of the coupon.</param>
        public virtual void Bind(ZNodeShoppingCart shoppingCart, ZnodePromotionBag promotionBag)
        {
            ShoppingCart = shoppingCart;
            PromotionBag = promotionBag;
        }

        /// <summary>
        /// Calculates the promotion and updates the shopping cart.
        /// </summary>
        public virtual void Calculate(int? couponIndex)
        {
        }

        /// <summary>
        /// Process anything that must be done before the order is submitted.
        /// </summary>
        /// <returns>True if everything is good for submitting the order; otherwise, false.</returns>
        public virtual bool PreSubmitOrderProcess()
        {
            // Most promotions don't need any special verification
            return true;
        }

        /// <summary>
        /// Process anything that must be done after the order is submitted.
        /// </summary>
        public virtual void PostSubmitOrderProcess()
        {
            // Most promotions don't need any further processing after the order is submitted
        }

        /// <summary>
        /// Validates the coupon code on the shopping cart and on the promotion property are the same.
        /// </summary>
        /// <returns>True if the coupon is valid; otherwise, false.</returns>
        public virtual bool ValidateCoupon(int? couponIndex)
        {
            var isValid = false;

            if (PromotionBag.Coupon != null)
            {
                isValid = ShoppingCart.Coupons[couponIndex.Value].Coupon.ToLower() == PromotionBag.Coupon.CouponCode.ToLower();

                if (isValid)
                {
                    PromotionBag.Coupon.CouponValid = true;
                    ShoppingCart.Coupons[couponIndex.Value].CouponValid = true;
                }
            }

            return isValid;
        }

        /// <summary>
        /// Add promotion message to the shopping cart.
        /// </summary>
        public virtual void AddPromotionMessage(int? couponIndex)
        {
            if (ValidateCoupon(couponIndex))
            {
                if (PromotionBag.Coupon.CouponValid && PromotionBag.Coupon.CouponApplied)
                {
                    if (!String.IsNullOrEmpty(PromotionBag.Coupon.PromotionMessage))
                    {
                        this.ShoppingCart.ClearPreviousErrorMessges();
                        ShoppingCart.AddPromoDescription = PromotionBag.Coupon.PromotionMessage + " " + PromotionBag.Coupon.CouponCode;
                    }
                    else
                    {
                        ShoppingCart.AddPromoDescription = "Coupon code accepted " + PromotionBag.Coupon.CouponCode;
                    }
                    this.ShoppingCart.Coupons[couponIndex.Value].CouponMessage = this.ShoppingCart.AddPromoDescription;
                }
                else if (PromotionBag.Coupon.CouponValid)
                {
                    this.ShoppingCart.ClearPreviousErrorMessges();
                    ShoppingCart.AddPromoDescription = "Coupon criteria not met " + PromotionBag.Coupon.CouponCode;
                }
            }
        }

        /// <summary>
        /// Check if currentItem is the last item in the shopping cart.
        /// </summary>
        /// <param name="currentItem">The current item in the shopping cart.</param>
        /// <returns>True if currentItem is the last item in the shopping cart; otherwise, false.</returns>
        public virtual bool IsLastItem(ZNodeShoppingCartItem currentItem)
        {
            var indexes = new List<int>();
            var isLastLineItem = false;
            var index = 0;

            var currentItemIndex = ShoppingCart.ShoppingCartItems.IndexOf(currentItem);

            if (currentItem != null)
            {
                foreach (ZNodeShoppingCartItem item in ShoppingCart.ShoppingCartItems)
                {
                    if (currentItem.Product.ProductID == item.Product.ProductID)
                    {
                        index = ShoppingCart.ShoppingCartItems.IndexOf(item);
                        indexes.Add(index);
                    }
                }

                if (indexes.Count == 1)
                {
                    isLastLineItem = true;
                }

                if (indexes.IndexOf(currentItemIndex) == indexes.LastIndexOf(index))
                {
                    isLastLineItem = true;
                }
            }

            return isLastLineItem;
        }

        /// <summary>
        /// Gets total quantity ordered for the product.
        /// </summary>
        /// <param name="productId">The ID of the product.</param>                
        /// <returns>The total quantity ordered for the product.</returns>
        public virtual int GetQuantityOrdered(int? productId)
        {
            var quantity = 0;

            if (productId.HasValue)
            {
                foreach (ZNodeShoppingCartItem cartItem in ShoppingCart.ShoppingCartItems)
                {
                    if (cartItem.Product.ProductID == productId.Value)
                    {
                        quantity += cartItem.Quantity;
                    }
                }
            }

            return quantity;
        }
    }
}
