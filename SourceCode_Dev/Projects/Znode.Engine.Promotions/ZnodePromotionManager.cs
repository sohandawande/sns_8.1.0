using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StructureMap;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Promotions
{
	/// <summary>
	/// Base class for ZnodeCartPromotionManager, ZnodePricePromotionManager, and ZnodeProductPromotionManager.
	/// </summary>
	public class ZnodePromotionManager : ZNodeBusinessBase
	{
		private static TList<DiscountType> _allDiscountTypes;		

		/// <summary>
		/// Gets the list of all discount types in the database.
		/// </summary>
		public static TList<DiscountType> AllDiscountTypes
		{
			get
			{
				if (_allDiscountTypes == null)
				{
					_allDiscountTypes = DataRepository.DiscountTypeProvider.GetAll();
				}

				return _allDiscountTypes;
			}
		}

		/// <summary>
		/// Gets the list of all promotions in the database.
		/// </summary>
		public static TList<Promotion> AllPromotions
		{
			get
			{
				// TODO: Can we avoid more calls to the database?
				var allPromotions = DataRepository.PromotionProvider.GetAll();
				allPromotions.ForEach(x => x.DiscountTypeIDSource = AllDiscountTypes.FirstOrDefault(y => y.DiscountTypeID == x.DiscountTypeID));

				return allPromotions;
			}
		}

		/// <summary>
		/// Gets the list of active cart promotions from the application cache.
		/// </summary>
		public TList<Promotion> CartPromotionCache
		{
			get
			{
				var promotions = new TList<Promotion>();

				if (HttpRuntime.Cache["CartPromotionCache"] == null)
				{
					CacheActiveCartPromotions();
				}
				else
				{
					promotions = HttpRuntime.Cache["CartPromotionCache"] as TList<Promotion>;
					if (promotions != null)
					{
						promotions.Sort("DisplayOrder");
					}
				}

				return promotions;
			}
		}

		/// <summary>
		/// Gets the list of active price promotions from the application cache.
		/// </summary>
		public TList<Promotion> PricePromotionCache
		{
			get
			{
				var promotions = new TList<Promotion>();

				if (HttpRuntime.Cache["PricePromotionCache"] == null)
				{
					CacheActivePricePromotions();
				}
				else
				{
					promotions = HttpRuntime.Cache["PricePromotionCache"] as TList<Promotion>;
					if (promotions != null)
					{
						promotions.Sort("DisplayOrder");
					}
				}

				return promotions;
			}
		}

		/// <summary>
		/// Gets the list of active product promotions from the application cache.
		/// </summary>
		public TList<Promotion> ProductPromotionCache
		{
			get
			{
				var promotions = new TList<Promotion>();

				if (HttpRuntime.Cache["ProductPromotionCache"] == null)
				{
					CacheActiveProductPromotions();
				}
				else
				{
					promotions = HttpRuntime.Cache["ProductPromotionCache"] as TList<Promotion>;
					if (promotions != null)
					{
						promotions.Sort("DisplayOrder");
					}
				}

				return promotions;
			}
		}

		/// <summary>
		/// Gets the coupon from a promotion.
		/// </summary>
		/// <param name="promotion">The promotion that contains the coupon.</param>
		/// <returns>The coupon for the promotion.</returns>
		public ZnodeCoupon GetCoupon(Promotion promotion)
		{
			var coupon = new ZnodeCoupon
			{
				ProfileId = promotion.ProfileID.GetValueOrDefault(0),
				Discount = promotion.Discount,
				DiscountTypeId = promotion.DiscountTypeID,
				MinimumOrderAmount = promotion.OrderMinimum.GetValueOrDefault(0),
				RequiredBrandId = promotion.ProductID.GetValueOrDefault(0),
				RequiredBrandMinimumQuantity = promotion.QuantityMinimum.GetValueOrDefault(1),
				RequiredCatalogId = promotion.ProductID.GetValueOrDefault(0),
				RequiredCatalogMinimumQuantity = promotion.QuantityMinimum.GetValueOrDefault(1),
				RequiredCategoryId = promotion.ProductID.GetValueOrDefault(0),
				RequiredCategoryMinimumQuantity = promotion.QuantityMinimum.GetValueOrDefault(1),
				RequiredProductId = promotion.ProductID.GetValueOrDefault(0),
				RequiredProductMinimumQuantity = promotion.QuantityMinimum.GetValueOrDefault(1),
				DiscountedProductId = promotion.PromotionProductID,
				DiscountedProductQuantity = promotion.PromotionProductQty.GetValueOrDefault(1),
				CouponId = promotion.PromotionID,
				CouponCode = promotion.CouponCode,
				CouponQuantityAvailable = promotion.CouponQuantityAvailable.GetValueOrDefault(0),
				PromotionMessage = promotion.PromotionMessage
			};

			return coupon;
		}

		/// <summary>
		/// Caches all active promotions in the application cache.
		/// </summary>
		public static void CacheActivePromotions()
		{
			// NOTE: For performance reasons, we split the active promotions into three caches
			// because we don't need to verify cart promotions on product/category pages.

			CacheActiveCartPromotions();
			CacheActivePricePromotions();
			CacheActiveProductPromotions();
		}

		/// <summary>
		/// Caches all active cart promotions in the application cache.
		/// </summary>
		public static void CacheActiveCartPromotions()
		{
			if (HttpRuntime.Cache["CartPromotionCache"] == null)
			{
				// Get active cart promotions
				var cartPromotions = AllPromotions.FindAll(promo => DateTime.Today.Date >= promo.StartDate.Date && DateTime.Today.Date <= promo.EndDate.Date &&
														  ((promo.CouponQuantityAvailable > 0 && promo.CouponInd) || promo.CouponInd == false) &&
														  promo.DiscountTypeIDSource.ClassType.Equals("CART", StringComparison.OrdinalIgnoreCase));

				// Sort them and add to cache
				cartPromotions.Sort("DisplayOrder");
				ZNodeCacheDependencyManager.Insert("CartPromotionCache", cartPromotions, "ZNodePromotion");
			}
		}

		/// <summary>
		/// Caches all active price promotions in the application cache.
		/// </summary>
		public static void CacheActivePricePromotions()
		{
			if (HttpRuntime.Cache["PricePromotionCache"] == null)
			{
				// Get active price promotions
				var pricePromotions = AllPromotions.FindAll(promo => DateTime.Today.Date >= promo.StartDate.Date && DateTime.Today.Date <= promo.EndDate.Date &&
														   ((promo.CouponQuantityAvailable > 0 && promo.CouponInd) || promo.CouponInd == false) &&
														   promo.DiscountTypeIDSource.ClassType.Equals("PRICE", StringComparison.OrdinalIgnoreCase));

				// Sort them and add to cache
				pricePromotions.Sort("DisplayOrder");
				ZNodeCacheDependencyManager.Insert("PricePromotionCache", pricePromotions, "ZNodePromotion");
			}
		}

		/// <summary>
		/// Caches all active product promotions in the application cache.
		/// </summary>
		public static void CacheActiveProductPromotions()
		{
			if (HttpRuntime.Cache["ProductPromotionCache"] == null)
			{
				// Get active product promotions
				var productPromotions = AllPromotions.FindAll(promo => DateTime.Today.Date >= promo.StartDate.Date && DateTime.Today.Date <= promo.EndDate.Date &&
															 ((promo.CouponQuantityAvailable > 0 && promo.CouponInd) || promo.CouponInd == false) &&
															 promo.DiscountTypeIDSource.ClassType.Equals("PRODUCT", StringComparison.OrdinalIgnoreCase));

				// Sort them and add to cache
				productPromotions.Sort("DisplayOrder");
				ZNodeCacheDependencyManager.Insert("ProductPromotionCache", productPromotions, "ZNodePromotion");
			}
		}

		/// <summary>
		/// Caches all available promotion types in the application cache.
		/// </summary>
		public static void CacheAvailablePromotionTypes()
		{
			if (HttpRuntime.Cache["PromotionTypesCache"] == null)
			{
				if (ObjectFactory.Container == null)
				{
					ObjectFactory.Initialize(scanner => scanner.Scan(x =>
					{
						x.AssembliesFromApplicationBaseDirectory();
						x.AddAllTypesOf<IZnodePromotionType>();
					}));
				}
				else
				{
					ObjectFactory.Configure(scanner => scanner.Scan(x =>
					{
						x.AssembliesFromApplicationBaseDirectory();
						x.AddAllTypesOf<IZnodePromotionType>();
					}));
				}

				// Only cache promotion types that have a ClassName and Name; this helps avoid showing base classes in some of the dropdown lists
				var promoTypes = ObjectFactory.GetAllInstances<IZnodePromotionType>().Where(x => !String.IsNullOrEmpty(x.ClassName) && !String.IsNullOrEmpty(x.Name));
				HttpRuntime.Cache["PromotionTypesCache"] = promoTypes.ToList();
			}
		}

		/// <summary>
		/// Gets all available promotion types from the application cache.
		/// </summary>
		/// <returns>A list of the available promotion types.</returns>
		public static List<IZnodePromotionType> GetAvailablePromotionTypes()
		{
			var list = HttpRuntime.Cache["PromotionTypesCache"] as List<IZnodePromotionType>;
			if (list != null)
			{
				list.Sort((promoTypeA, promoTypeB) => String.CompareOrdinal(promoTypeA.Name, promoTypeB.Name));
			}
			else
			{
				list = new List<IZnodePromotionType>();
			}

			return list;
		}
	}
}