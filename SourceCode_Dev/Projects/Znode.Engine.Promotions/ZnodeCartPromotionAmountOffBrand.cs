using ZNode.Libraries.ECommerce.Entities;

namespace Znode.Engine.Promotions
{
	public class ZnodeCartPromotionAmountOffBrand : ZnodeCartPromotionType
	{
		public ZnodeCartPromotionAmountOffBrand()
		{
			Name = "Amount Off Brand";
			Description = "Applies an amount off products for a particular brand or manufacturer; affects the shopping cart.";
			AvailableForFranchise = false;

			Controls.Add(ZnodePromotionControl.Store);
			Controls.Add(ZnodePromotionControl.Profile);
			Controls.Add(ZnodePromotionControl.DiscountAmount);
			Controls.Add(ZnodePromotionControl.RequiredBrand);
			Controls.Add(ZnodePromotionControl.RequiredBrandMinimumQuantity);
			Controls.Add(ZnodePromotionControl.MinimumOrderAmount);
			Controls.Add(ZnodePromotionControl.Coupon);
		}

		/// <summary>
		/// Calculates the amount off products for a particular brand or manufacturer in the shopping cart.
		/// </summary>
		public override void Calculate(int? couponIndex)
		{
            var isCouponValid = false;
            if (!Equals(couponIndex, null))
            {
                isCouponValid = ValidateCoupon(couponIndex);
                isCouponValid &= PromotionBag.Coupon != null;
            }

			// Loop through each cart Item
			foreach (ZNodeShoppingCartItem cartItem in ShoppingCart.ShoppingCartItems)
			{
				if (!PromotionBag.RequiredBrandId.HasValue || PromotionBag.RequiredBrandId.Value == cartItem.Product.ManufacturerID)
				{
					var subTotal = ShoppingCart.SubTotal;
					var qtyOrdered = GetQuantityOrdered(cartItem.Product.ProductID);

					if (PromotionBag.Coupon == null && PromotionBag.RequiredBrandMinimumQuantity <= qtyOrdered && PromotionBag.MinimumOrderAmount <= subTotal)
					{
						// Product discount amount
						cartItem.Product.DiscountAmount += PromotionBag.Discount;
                        ShoppingCart.IsAnyPromotionApplied = true;
					}
					else if (PromotionBag.Coupon != null && isCouponValid)
					{
						if (PromotionBag.Coupon.RequiredBrandMinimumQuantity <= qtyOrdered && PromotionBag.Coupon.MinimumOrderAmount <= subTotal)
						{
							// Product discount amount
							cartItem.Product.DiscountAmount += PromotionBag.Coupon.Discount;

							PromotionBag.Coupon.CouponApplied = true;
                            ShoppingCart.Coupons[couponIndex.Value].CouponApplied = true;
						}
					}
				}
			}

            AddPromotionMessage(couponIndex);
		}
	}
}
