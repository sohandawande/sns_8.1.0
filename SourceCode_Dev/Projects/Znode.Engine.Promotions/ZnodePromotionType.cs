﻿using System;
using System.Collections.ObjectModel;

namespace Znode.Engine.Promotions
{
	/// <summary>
	/// This is the base class for all promotion types.
	/// </summary>
	public class ZnodePromotionType : IZnodePromotionType
	{
		private string _className;
		private Collection<ZnodePromotionControl> _controls;

		public string ClassName
		{
			get
			{
				if (String.IsNullOrEmpty(_className))
				{
					_className = GetType().Name;
				}

				return _className;
			}

			set { _className = value; }
		}

		public string Name { get; set; }
		public string Description { get; set; }
		public int Precedence { get; set; }
		public bool AvailableForFranchise { get; set; }

		public Collection<ZnodePromotionControl> Controls
		{
			get { return _controls ?? (_controls = new Collection<ZnodePromotionControl>()); }
		}
	}
}
