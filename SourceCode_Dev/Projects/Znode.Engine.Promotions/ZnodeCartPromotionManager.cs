using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;
using System.Configuration;

namespace Znode.Engine.Promotions
{
    /// <summary>
    /// Helps manage cart promotions.
    /// </summary>
    public class ZnodeCartPromotionManager : ZnodePromotionManager
    {
        private ZNodeShoppingCart _shoppingCart;
        private ZNodeGenericCollection<IZnodeCartPromotionType> _cartPromotions;
        TList<Promotion> newPromotionsFromCache;

        /// <summary>
        /// Throws a NotImplementedException because this class requires a shopping cart to work.
        /// </summary>
        public ZnodeCartPromotionManager()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Instantiates all the promotions that are required for the current shopping cart.
        /// </summary>
        /// <param name="shoppingCart">The current shopping cart.</param>
        /// <param name="profileId">The current profile ID, if any.</param>
        public ZnodeCartPromotionManager(ZNodeShoppingCart shoppingCart, int? profileId)
        {
            _shoppingCart = shoppingCart;
            _cartPromotions = new ZNodeGenericCollection<IZnodeCartPromotionType>();

            var currentPortalId = ZNodeConfigManager.SiteConfig.PortalID;

            if (!Equals(_shoppingCart, null) && !Equals(_shoppingCart.PortalId, null))
            {
                currentPortalId = _shoppingCart.PortalId.GetValueOrDefault();
            }

            Profile profile = null;
            int? currentProfileId = null;

            if (!profileId.HasValue)
            {
                if (HttpContext.Current.Session["ProfileCache"] != null)
                {
                    profile = HttpContext.Current.Session["ProfileCache"] as Profile;
                }

                if (profile != null)
                {
                    currentProfileId = profile.ProfileID;
                }
            }
            else
            {
                currentProfileId = profileId;
            }

            // Check for default portal
            int total;
            var categoryService = new CategoryService();
            var categoryList = categoryService.GetByPortalID(ZNodeConfigManager.SiteConfig.PortalID, 0, 1, out total);
            var isDefaultPortal = categoryList.Count == 0;

            var cartPromotionsFromCache = (TList<Promotion>)CartPromotionCache.Clone();
            ApplyPromotionsFilter(cartPromotionsFromCache, currentPortalId, currentProfileId, isDefaultPortal);

            // Sort the promotions, build the list, then kill the cache clone
            //cartPromotionsFromCache.Sort("DisplayOrder");
            if (!Equals(newPromotionsFromCache, null))
            {
                BuildPromotionsList(newPromotionsFromCache, currentPortalId, currentProfileId);
                newPromotionsFromCache.Dispose();
            }
            else
            {
                BuildPromotionsList(cartPromotionsFromCache, currentPortalId, currentProfileId);
                cartPromotionsFromCache.Dispose();
            }
        }

        /// <summary>
        /// Calculates the promotion and updates the shopping cart.
        /// </summary>
        public void Calculate()
        {
            // No sense in continuing without a shopping cart
            if (_shoppingCart == null)
                return;

            _shoppingCart.OrderLevelDiscount = 0;
            _shoppingCart.Shipping.ShippingDiscount = 0;
            _shoppingCart.ClearPreviousErrorMessges();
            GetAmountsForCartItemsAndAddOns();
            CalculateEachPromotion();
            if (!Equals(_shoppingCart.Coupons, null))
            {
                for (int couponIndex = 0; couponIndex < _shoppingCart.Coupons.Count; couponIndex++)
                {
                    CheckForInvalidCoupon(couponIndex);
                }
            }
        }

        /// <summary>
        /// Process anything that must be done before the order is submitted.
        /// </summary>
        /// <returns>True if everything is good for submitting the order; otherwise, false.</returns>
        public bool PreSubmitOrderProcess()
        {
            var allPreConditionsOk = true;

            foreach (ZnodeCartPromotionType promo in _cartPromotions)
            {
                // Make sure all pre-conditions are good before letting the customer check out
                allPreConditionsOk &= promo.PreSubmitOrderProcess();
            }

            return allPreConditionsOk;
        }

        /// <summary>
        /// Process anything that must be done after the order is submitted.
        /// </summary>
        public void PostSubmitOrderProcess()
        {
            foreach (ZnodeCartPromotionType promo in _cartPromotions)
            {
                promo.PostSubmitOrderProcess();
            }
        }

        private void GetAmountsForCartItemsAndAddOns()
        {
            var pricePromoManager = new ZnodePricePromotionManager();

            foreach (ZNodeShoppingCartItem cartItem in _shoppingCart.ShoppingCartItems)
            {
                cartItem.PromotionalPrice = pricePromoManager.PromotionalPrice(cartItem.Product, cartItem.TieredPricing);
                cartItem.ExtendedPriceDiscount = 0;
                cartItem.Product.DiscountAmount = 0;

                foreach (ZNodeAddOnEntity addOn in cartItem.Product.SelectedAddOns.AddOnCollection)
                {
                    foreach (ZNodeAddOnValueEntity addOnValue in addOn.AddOnValueCollection)
                    {
                        addOnValue.DiscountAmount = 0;
                    }
                }
            }
        }

        private void CalculateEachPromotion()
        {
            for (int cartPromoIndex = 0, cartPromoCouponIndex = 0; cartPromoIndex < _cartPromotions.Count; cartPromoIndex++)
            {
                if (_cartPromotions[cartPromoIndex].IsPromoCouponAvailable)
                {
                    if (Equals(cartPromoCouponIndex, 0))
                    {
                        CalculateCouponPromotion(cartPromoIndex, cartPromoCouponIndex);
                    }
                    else
                    {
                        if (ZNodeConfigManager.SiteConfig.IsMultipleCouponAllowed && _shoppingCart.CartAllowsMultiCoupon)
                        {
                            if (_shoppingCart.Coupons[cartPromoCouponIndex].AllowsMultipleCoupon)
                            {
                                CalculateCouponPromotion(cartPromoIndex, cartPromoCouponIndex);
                            }
                            else
                            {
                                _shoppingCart.Coupons[cartPromoCouponIndex].CouponApplied = false;
                                _shoppingCart.Coupons[cartPromoCouponIndex].CouponValid = false;
                                _shoppingCart.Coupons[cartPromoCouponIndex].CouponMessage = string.IsNullOrEmpty(_shoppingCart.Coupons[cartPromoCouponIndex].CouponMessage) ? "The coupon you are trying to apply cannot be clubbed with the already applied coupon." : _shoppingCart.Coupons[cartPromoCouponIndex].CouponMessage;
                            }
                        }
                        else
                        {
                            _shoppingCart.Coupons[cartPromoCouponIndex].CouponApplied = false;
                            _shoppingCart.Coupons[cartPromoCouponIndex].CouponValid = false;
                            _shoppingCart.Coupons[cartPromoCouponIndex].CouponMessage = string.IsNullOrEmpty(_shoppingCart.Coupons[cartPromoCouponIndex].CouponMessage) ? "The store is not configured to apply multiple coupons or applied coupon does not allows other coupons to be clubbed with it." : _shoppingCart.Coupons[cartPromoCouponIndex].CouponMessage;
                        }
                    }
                    cartPromoCouponIndex++;
                }
                else
                {
                    _cartPromotions[cartPromoIndex].Calculate(null);
                }
            }
        }

        private void CalculateCouponPromotion(int cartPromoIndex, int cartPromoCouponIndex)
        {
            _cartPromotions[cartPromoIndex].Calculate(cartPromoCouponIndex);
            if (!Equals(_shoppingCart.Coupons, null) && _shoppingCart.Coupons.Count > 0 && !Equals(_shoppingCart.Coupons[cartPromoCouponIndex], null) && _shoppingCart.Coupons[cartPromoCouponIndex].CouponApplied)
            {
                if (cartPromoCouponIndex == 0)
                {
                    _shoppingCart.ClearPreviousErrorMessges();
                }
                _shoppingCart.AddPromoDescription += _shoppingCart.Coupons[cartPromoCouponIndex].CouponMessage;
            }
            else
            {
                _shoppingCart.Coupons[cartPromoCouponIndex].CouponValid = false;
                _shoppingCart.Coupons[cartPromoCouponIndex].CouponApplied = false;
                _shoppingCart.Coupons[cartPromoCouponIndex].CouponMessage = string.IsNullOrEmpty(_shoppingCart.Coupons[cartPromoCouponIndex].CouponMessage) ? "Invalid Coupon Code." : _shoppingCart.Coupons[cartPromoCouponIndex].CouponMessage;
            }
        }

        private void CheckForInvalidCoupon(int couponIndex)
        {
            if (!_shoppingCart.Coupons[couponIndex].CouponApplied && !_shoppingCart.Coupons[couponIndex].CouponValid && !String.IsNullOrEmpty(_shoppingCart.Coupons[couponIndex].Coupon))
            {
                _shoppingCart.AddPromoDescription = "Invalid coupon code " + _shoppingCart.Coupons[couponIndex].Coupon;
                _shoppingCart.Coupons[couponIndex].CouponMessage = !string.IsNullOrEmpty(_shoppingCart.Coupons[couponIndex].CouponMessage) ? _shoppingCart.Coupons[couponIndex].CouponMessage : "Invalid coupon code " + _shoppingCart.Coupons[couponIndex].Coupon;
            }
        }

        private void ApplyPromotionsFilter(TList<Promotion> promotionsFromCache, int? currentPortalId, int? currentProfileId, bool isDefaultPortal)
        {

            if (Equals(_shoppingCart.Coupons, null) || _shoppingCart.Coupons.Count <= 0)
            {
                promotionsFromCache.ApplyFilter(
                    promo => (promo.ProfileID == currentProfileId || promo.ProfileID == null && isDefaultPortal) &&
                             (promo.PortalID == currentPortalId || promo.PortalID == null) &&
                             (promo.CouponInd == false) &&
                             promo.DiscountTypeIDSource.ClassType.Equals("CART", StringComparison.OrdinalIgnoreCase));
            }
            else
            {
                string couponCodes = string.Join(",", _shoppingCart.Coupons.Select(coupon => coupon.Coupon));
                promotionsFromCache.ApplyFilter(
                                    promo => (promo.ProfileID == currentProfileId || promo.ProfileID == null && isDefaultPortal) &&
                                             (promo.PortalID == currentPortalId || promo.PortalID == null) &&
                                             (promo.CouponInd == false || couponCodes.Contains(promo.CouponCode)) &&
                                             promo.DiscountTypeIDSource.ClassType.Equals("CART", StringComparison.OrdinalIgnoreCase));

                string[] coupons = couponCodes.Split(',');
                newPromotionsFromCache = new TList<Promotion>();
                foreach (string coupon in coupons)
                {
                    Promotion promotion = promotionsFromCache.FirstOrDefault(x => Equals(x.CouponCode, coupon));
                    if (!Equals(promotion, null))
                    {
                        newPromotionsFromCache.Add(promotion);
                    }
                }
                foreach (Promotion promo in promotionsFromCache)
                {
                    if (!promo.CouponInd)
                    {
                        newPromotionsFromCache.Add(promo);
                    }
                }

                foreach (ZNodeCoupon coupon in _shoppingCart.Coupons)
                {
                    PromotionBase promotion = (newPromotionsFromCache.Find(x => Equals(x.CouponCode, coupon.Coupon)));
                    if (!Equals(promotion, null))
                    {
                        coupon.AllowsMultipleCoupon = promotion.IsCouponAllowedWithOtherCoupons;
                    }
                    else
                    {
                        coupon.AllowsMultipleCoupon = false;
                    }
                }
                _shoppingCart.CartAllowsMultiCoupon = _shoppingCart.Coupons.FirstOrDefault().AllowsMultipleCoupon;
            }
        }

        private void BuildPromotionsList(TList<Promotion> promotionsFromCache, int? currentPortalId, int? currentProfileId)
        {
            foreach (var promotion in promotionsFromCache)
            {
                var promoBag = BuildPromotionBag(promotion, currentPortalId, currentProfileId);
                AddPromotionType(promotion, promoBag);

            }
        }

        private ZnodePromotionBag BuildPromotionBag(Promotion promotion, int? currentPortalId, int? currentProfileId)
        {
            var promoBag = new ZnodePromotionBag();
            promoBag.PortalId = currentPortalId;
            promoBag.ProfileId = currentProfileId;
            promoBag.Discount = promotion.Discount;
            promoBag.MinimumOrderAmount = promotion.OrderMinimum.GetValueOrDefault(0);
            promoBag.RequiredProductId = promotion.ProductID.GetValueOrDefault(0);
            promoBag.RequiredProductMinimumQuantity = promotion.QuantityMinimum.GetValueOrDefault(1);
            promoBag.DiscountedProductId = promotion.PromotionProductID.GetValueOrDefault(0);
            promoBag.DiscountedProductQuantity = promotion.PromotionProductQty.GetValueOrDefault(1);
            promoBag.RequiredBrandId = promotion.ManufacturerID;
            promoBag.RequiredBrandMinimumQuantity = promotion.QuantityMinimum.GetValueOrDefault(1);
            promoBag.RequiredCatalogId = promotion.CatalogID;
            promoBag.RequiredCatalogMinimumQuantity = promotion.QuantityMinimum.GetValueOrDefault(1);
            promoBag.RequiredCategoryId = promotion.CategoryID;
            promoBag.RequiredCategoryMinimumQuantity = promotion.QuantityMinimum.GetValueOrDefault(1);
            promoBag.IsCouponAllowedWithOtherCoupons = promotion.IsCouponAllowedWithOtherCoupons;

            if (promotion.CouponInd)
            {
                promoBag.Coupon = GetCoupon(promotion);
            }

            return promoBag;
        }

        private void AddPromotionType(Promotion promotion, ZnodePromotionBag promotionBag)
        {
            var availablePromoTypes = GetAvailablePromotionTypes();
            foreach (var t in availablePromoTypes.Where(t => t.ClassName == promotion.DiscountTypeIDSource.ClassName))
            {
                try
                {
                    var promoTypeAssembly = Assembly.GetAssembly(t.GetType());
                    var cartPromo = (IZnodeCartPromotionType)promoTypeAssembly.CreateInstance(t.ToString());

                    if (cartPromo != null)
                    {
                        cartPromo.Precedence = promotion.DisplayOrder;
                        if (!Equals(promotionBag.Coupon, null) && !string.IsNullOrEmpty(promotionBag.Coupon.CouponCode))
                        {
                            cartPromo.IsPromoCouponAvailable = true;
                        }
                        cartPromo.Bind(_shoppingCart, promotionBag);
                        _cartPromotions.Add(cartPromo);
                    }
                }
                catch (Exception ex)
                {
                    ZNodeLogging.LogMessage("Error while instantiating promotion type: " + t);
                    ZNodeLogging.LogMessage(ex.ToString());
                }
            }
        }
    }
}