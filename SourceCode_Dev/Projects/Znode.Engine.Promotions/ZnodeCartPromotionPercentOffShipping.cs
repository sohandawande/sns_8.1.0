namespace Znode.Engine.Promotions
{
	public class ZnodeCartPromotionPercentOffShipping : ZnodeCartPromotionType
	{
		public ZnodeCartPromotionPercentOffShipping()
		{
			Name = "Percent Off Shipping";
			Description = "Applies a percent off shipping for an order; affects the shopping cart.";
			AvailableForFranchise = false;

			Controls.Add(ZnodePromotionControl.Store);
			Controls.Add(ZnodePromotionControl.Profile);
			Controls.Add(ZnodePromotionControl.DiscountPercent);
			Controls.Add(ZnodePromotionControl.MinimumOrderAmount);
			Controls.Add(ZnodePromotionControl.Coupon);
		}

		/// <summary>
		/// Calculates the percent off shipping for the order.
		/// </summary>
		public override void Calculate(int? couponIndex)
		{
			var subTotal = ShoppingCart.SubTotal;
			var shippingCost = ShoppingCart.ShippingCost;

			if (PromotionBag.MinimumOrderAmount <= subTotal && PromotionBag.Coupon == null)
			{
				ShoppingCart.Shipping.ShippingDiscount += shippingCost * (PromotionBag.Discount / 100);
                ShoppingCart.IsAnyPromotionApplied = true;
			}
			else if (PromotionBag.Coupon != null)
			{
                var isCouponValid = ValidateCoupon(couponIndex);

				if (PromotionBag.Coupon.MinimumOrderAmount <= subTotal && isCouponValid)
				{
					ShoppingCart.Shipping.ShippingDiscount += shippingCost * (PromotionBag.Coupon.Discount / 100);
					PromotionBag.Coupon.CouponApplied = true;
                    ShoppingCart.Coupons[couponIndex.Value].CouponApplied = true;
				}

                AddPromotionMessage(couponIndex);
			}
		}
	}
}
