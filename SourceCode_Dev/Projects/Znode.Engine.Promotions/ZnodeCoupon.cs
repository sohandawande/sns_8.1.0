using System;
using System.Xml.Serialization;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Promotions
{
    [Serializable]
    [XmlRoot("ZnodeCoupon")]
    public class ZnodeCoupon : ZNodeBusinessBase
    {
	    private string _promotionMessage;

		[XmlElement]
		public int PortalId { get; set; }

		[XmlElement]
		public int ProfileId { get; set; }

		[XmlElement]
		public decimal Discount { get; set; }

		[XmlElement]
		public int DiscountTypeId { get; set; }

		[XmlElement]
		public decimal MinimumOrderAmount { get; set; }

		[XmlElement]
		public int RequiredBrandId { get; set; }

		[XmlElement]
		public int RequiredBrandMinimumQuantity { get; set; }

		[XmlElement]
		public int RequiredCatalogId { get; set; }

		[XmlElement]
		public int RequiredCatalogMinimumQuantity { get; set; }

		[XmlElement]
		public int RequiredCategoryId { get; set; }

		[XmlElement]
		public int RequiredCategoryMinimumQuantity { get; set; }

		[XmlElement]
		public int RequiredProductId { get; set; }

		[XmlElement]
		public int RequiredProductMinimumQuantity { get; set; }

		[XmlElement]
		public int? DiscountedProductId { get; set; }

		[XmlElement]
		public int DiscountedProductQuantity { get; set; }

	    [XmlElement]
	    public int CouponId { get; set; }

	    [XmlElement]
	    public string CouponCode { get; set; }

		[XmlElement]
		public int CouponQuantityAvailable { get; set; }

		[XmlElement]
	    public bool CouponApplied { get; set; }

		[XmlElement]
	    public bool CouponValid { get; set; }

		[XmlElement]
		public DateTime StartDate { get; set; }

		[XmlElement]
		public DateTime EndDate { get; set; }

		[XmlElement]
		public string PromotionMessage
		{
			get
			{
				if (String.IsNullOrEmpty(_promotionMessage))
				{
					return "Coupon Successfully Applied";
				}

				return _promotionMessage;
			}

			set
			{
				_promotionMessage = value;
			}
		}
    }
}