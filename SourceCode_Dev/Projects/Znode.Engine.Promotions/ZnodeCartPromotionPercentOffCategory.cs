using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.ECommerce.Entities;

namespace Znode.Engine.Promotions
{
	public class ZnodeCartPromotionPercentOffCategory : ZnodeCartPromotionType
	{
		public ZnodeCartPromotionPercentOffCategory()
		{
			Name = "Percent Off Category";
			Description = "Applies a percent off products in a particular category; affects the shopping cart.";
			AvailableForFranchise = false;

			Controls.Add(ZnodePromotionControl.Store);
			Controls.Add(ZnodePromotionControl.Profile);
			Controls.Add(ZnodePromotionControl.DiscountPercent);
			Controls.Add(ZnodePromotionControl.RequiredCategory);
			Controls.Add(ZnodePromotionControl.RequiredCategoryMinimumQuantity);
			Controls.Add(ZnodePromotionControl.MinimumOrderAmount);
			Controls.Add(ZnodePromotionControl.Coupon);
		}

		/// <summary>
		/// Calculates the percent off products in a particular category in the shopping cart.
		/// </summary>
        public override void Calculate(int? couponIndex)
		{
            var isCouponValid = false;
            if (!Equals(couponIndex, null))
            {
                isCouponValid = ValidateCoupon(couponIndex);
                isCouponValid &= PromotionBag.Coupon != null;
            }
			// Loop through each cart Item
			foreach (ZNodeShoppingCartItem cartItem in ShoppingCart.ShoppingCartItems)
			{
				var productCategories = DataRepository.ProductCategoryProvider.GetByProductID(cartItem.Product.ProductID);
				foreach (var item in productCategories)
				{
					if (!PromotionBag.RequiredCategoryId.HasValue || PromotionBag.RequiredCategoryId.Value == item.CategoryID)
					{
						var subTotal = ShoppingCart.SubTotal;
						var qtyOrdered = GetQuantityOrdered(cartItem.Product.ProductID);

						var pricePromoManager = new ZnodePricePromotionManager();
						var basePrice = pricePromoManager.PromotionalPrice(cartItem.Product, cartItem.TieredPricing);

						if (PromotionBag.Coupon == null && PromotionBag.RequiredCategoryMinimumQuantity <= qtyOrdered && PromotionBag.MinimumOrderAmount <= subTotal)
						{
							// Product discount percent
							cartItem.Product.DiscountAmount += basePrice * (PromotionBag.Discount / 100);
                            ShoppingCart.IsAnyPromotionApplied = true;
						}
						else if (PromotionBag.Coupon != null && isCouponValid)
						{
							if (PromotionBag.Coupon.RequiredCategoryMinimumQuantity <= qtyOrdered && PromotionBag.Coupon.MinimumOrderAmount <= subTotal)
							{
								// Product discount percent
								cartItem.Product.DiscountAmount += basePrice * (PromotionBag.Coupon.Discount / 100);

								PromotionBag.Coupon.CouponApplied = true;
                                ShoppingCart.Coupons[couponIndex.Value].CouponApplied = true;
							}
						}

						// Get out of the product category loop
						break;
					}
				}
			}

            AddPromotionMessage(couponIndex);
		}
	}
}
