﻿namespace Znode.Engine.Promotions
{
	/// <summary>
	/// This is the interface for all product promotion types.
	/// </summary>
	public interface IZnodeProductPromotionType : IZnodePromotionType
	{
		void Bind(ZnodePromotionBag promotionBag);
	}
}
