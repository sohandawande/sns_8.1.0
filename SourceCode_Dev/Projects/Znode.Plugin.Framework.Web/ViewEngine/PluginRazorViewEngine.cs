﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace Znode.Plugin.Framework.Web.ViewEngine
{
    public class PluginRazorViewEngine : RazorViewEngine
    {
        public PluginRazorViewEngine()
        {
            AreaMasterLocationFormats = new[]
        {
            "~/Areas/{2}/Views/{1}/{0}.cshtml",
            "~/Areas/{2}/Views/{1}/{0}.vbhtml",
            "~/Areas/{2}/Views/Shared/{0}.cshtml",
            "~/Areas/{2}/Views/Shared/{0}.vbhtml"
        };

            AreaPartialViewLocationFormats = new[]
        {
            "~/Areas/{2}/Views/{1}/{0}.cshtml",
            "~/Areas/{2}/Views/{1}/{0}.vbhtml",
            "~/Areas/{2}/Views/Shared/{0}.cshtml",
            "~/Areas/{2}/Views/Shared/{0}.vbhtml"
        };

            var areaViewAndPartialViewLocationFormats = new List<string>
        {
            "~/Areas/{2}/Views/{1}/{0}.cshtml",
            "~/Areas/{2}/Views/{1}/{0}.vbhtml",
            "~/Areas/{2}/Views/Shared/{0}.cshtml",
            "~/Areas/{2}/Views/Shared/{0}.vbhtml"
        };

            var partialViewLocationFormats = new List<string>
        {
            "~/Views/{1}/{0}.cshtml",
            "~/Views/{1}/{0}.vbhtml",
            "~/Views/Shared/{0}.cshtml",
            "~/Views/Shared/{0}.vbhtml"
        };

            var masterLocationFormats = new List<string>
        {
            "~/Views/{1}/{0}.cshtml",
            "~/Views/{1}/{0}.vbhtml",
            "~/Views/Shared/{0}.cshtml",
            "~/Views/Shared/{0}.vbhtml"
        };


            //var pluginFolder = PluginManager.Current.PluginFolder;
            const string pluginFolder = "Plugins";

            foreach (var plugin in PluginManager<IAdminPlugin>.Current.GetPlugins())
            {
                masterLocationFormats.Add(string.Format("~/{0}/", pluginFolder) + plugin.Name + "/Views/{1}/{0}.cshtml");
                masterLocationFormats.Add(string.Format("~/{0}/", pluginFolder) + "/Views/{1}/{0}.vbhtml");
                masterLocationFormats.Add(string.Format("~/{0}/", pluginFolder) + "/Views/Shared/{1}/{0}.cshtml");
                masterLocationFormats.Add(string.Format("~/{0}/", pluginFolder) + "/Views/Shared/{1}/{0}.vbhtml");

                partialViewLocationFormats.Add(string.Format("~/{0}/", pluginFolder) + "/Views/{1}/{0}.cshtml");
                partialViewLocationFormats.Add(string.Format("~/{0}/", pluginFolder) + "/Views/{1}/{0}.vbhtml");
                partialViewLocationFormats.Add(string.Format("~/{0}/", pluginFolder) + "/Views/Shared/{0}.cshtml");
                partialViewLocationFormats.Add(string.Format("~/{0}/", pluginFolder) + "/Views/Shared/{0}.vbhtml");

                areaViewAndPartialViewLocationFormats.Add(string.Format("~/{0}/", pluginFolder) + plugin.Name + "/Views/{1}/{0}.cshtml");
                areaViewAndPartialViewLocationFormats.Add(string.Format("~/{0}/", pluginFolder) + plugin.Name + "/Views/{1}/{0}.vbhtml");
                areaViewAndPartialViewLocationFormats.Add(string.Format("~/{0}/", pluginFolder) + plugin.Name + "/Areas/{2}/Views/{1}/{0}.cshtml");
                areaViewAndPartialViewLocationFormats.Add(string.Format("~/{0}/", pluginFolder) + plugin.Name + "/Areas/{2}/Views/{1}/{0}.vbhtml");
                areaViewAndPartialViewLocationFormats.Add(string.Format("~/{0}/", pluginFolder) + plugin.Name + "/Areas/{2}/Views/Shared/{0}.cshtml");
                areaViewAndPartialViewLocationFormats.Add(string.Format("~/{0}/", pluginFolder) + plugin.Name + "/Areas/{2}/Views/Shared/{0}.vbhtml");
            }

            ViewLocationFormats = partialViewLocationFormats.ToArray();
            MasterLocationFormats = masterLocationFormats.ToArray();
            PartialViewLocationFormats = partialViewLocationFormats.ToArray();
            AreaPartialViewLocationFormats = areaViewAndPartialViewLocationFormats.ToArray();
            AreaViewLocationFormats = areaViewAndPartialViewLocationFormats.ToArray();
        }
    }
}