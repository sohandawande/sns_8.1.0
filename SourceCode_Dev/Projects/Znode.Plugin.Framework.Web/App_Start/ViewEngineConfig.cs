﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Compilation;
using System.Web.Hosting;
using System.Web.Mvc;
using Znode.Plugin.Framework.Web;
using Znode.Plugin.Framework.Web.ViewEngine;

namespace Znode.Plugin.Framework.Web
{
    /// <summary>
    /// Currently, there is only one PreApplicationInit for both Admin and API. 
    /// A potential improvement would be to create IApiPlugin for api plugins 
    /// and create either separate PreApplicationInit for each hosting app, or 
    /// use a strategy to call the appropriate preinit depending on the host (Admin or Api).
    /// </summary>
    public class ViewEngineConfig
    {
        public static void Register()
        {
            ViewEngines.Engines.Add(new PluginRazorViewEngine());
        }
    }
}