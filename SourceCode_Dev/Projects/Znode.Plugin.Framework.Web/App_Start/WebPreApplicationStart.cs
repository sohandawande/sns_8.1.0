﻿using System.Web;
using Znode.Plugin.Framework.Web;

[assembly: PreApplicationStartMethod(typeof(WebPreApplicationStart), "InitializePlugins")]

namespace Znode.Plugin.Framework.Web
{
    /// <summary>
    /// PreApplicationStartMethod for WEB applications.
    /// </summary>
    public class WebPreApplicationStart
    {
        /// <summary>
        /// Initialize method that registers all plugins
        /// </summary>
        public static void InitializePlugins()
        {
            ViewEngineConfig.Register();
            PluginConfig<IAdminPlugin>.InitializePlugins();
        }
    }
}