﻿namespace Znode.Engine.Api.WebClient
{
    public static class HttpRequestMethod
    {
        public static readonly string Get = "GET";
        public static readonly string Post = "POST";
        public static readonly string Put = "PUT";
        public static readonly string Delete = "DELETE";
    }
}
