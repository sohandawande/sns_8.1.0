﻿namespace Znode.Engine.Api.WebClient
{
    public static class WebApiClient
    {
        public static string WebApiUrl { get; set; }
        public static string ApiKey { get; set; }
        public static HttpRequestEngine Engine { get; set; }
    }
}
