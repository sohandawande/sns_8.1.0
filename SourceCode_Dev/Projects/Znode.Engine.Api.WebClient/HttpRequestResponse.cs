﻿using System.Net;

namespace Znode.Engine.Api.WebClient
{
    public struct HttpRequestResponse
    {
        public HttpStatusCode StatusCode;
        public string Body;
    }
}
