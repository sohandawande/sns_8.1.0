﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace Znode.Engine.Api.WebClient
{
    public class HttpRequestEngine
    {
        public string BaseUrl { get; set; }
        public string ContentType { get; set; }
        public Encoding Encoding { get; set; }
        public Dictionary<string, string> Headers { get; set; }
 
        public HttpRequestEngine()
        {
            BaseUrl = string.Empty;
            ContentType = HttpRequestContentType.Xml;
            Encoding = Encoding.ASCII;
            Headers = new Dictionary<string, string>();
        }

		public HttpRequestResponse Get(string url, Dictionary<string, string> additionalHeaders = null)
        {
            var response = new HttpRequestResponse();
            MakeRequest(url, HttpRequestMethod.Get, null, out response.StatusCode, out response.Body, additionalHeaders);
            return response;
        }

		public HttpRequestResponse Post(string url, string body, Dictionary<string, string> additionalHeaders = null)
        {
            var response = new HttpRequestResponse();
			MakeRequest(url, HttpRequestMethod.Post, body, out response.StatusCode, out response.Body, additionalHeaders);
            return response;
        }

		public HttpRequestResponse Put(string url, string body, Dictionary<string, string> additionalHeaders = null)
        {
            var response = new HttpRequestResponse();
			MakeRequest(url, HttpRequestMethod.Put, body, out response.StatusCode, out response.Body, additionalHeaders);
            return response;
        }

		public HttpRequestResponse Delete(string url, Dictionary<string, string> additionalHeaders = null)
        {
            var response = new HttpRequestResponse();
			MakeRequest(url, HttpRequestMethod.Delete, null, out response.StatusCode, out response.Body, additionalHeaders);
            return response;
        }

		public void MakeRequest(string url, string method, string body, out HttpStatusCode responseStatusCode, out string responseBody, Dictionary<string, string> additionalHeaders)
        {
            var uri = string.Concat(BaseUrl, url);
            var webRequest = WebRequest.Create(uri) as HttpWebRequest;
            if (webRequest == null)
            {
                throw new InvalidOperationException("Only HTTP or HTTPS requests are supported.");
            }

            if (!string.IsNullOrEmpty(body))
            {
                webRequest.ContentLength = body.Length;
            }
            
            webRequest.ContentType = ContentType;
            webRequest.Method = method;
		    webRequest.Timeout = 10000000;
		    webRequest.KeepAlive = false;
            webRequest.Proxy.Credentials = CredentialCache.DefaultCredentials;
            foreach (var header in Headers)
            {
                webRequest.Headers.Add(header.Key, header.Value);
            }

			if (additionalHeaders != null)
			{
				foreach (var header in additionalHeaders)
				{
					webRequest.Headers.Add(header.Key, header.Value);
				}
			}

			if (!string.IsNullOrEmpty(body))
            {
                using (var sw = new StreamWriter(webRequest.GetRequestStream(), Encoding))
                {
                    sw.Write(body);
                }
            }

            HttpWebResponse webResponse;
            responseStatusCode = new HttpStatusCode();
            responseBody = null;
            try
            {
                webResponse = webRequest.GetResponse() as HttpWebResponse;
            }
            catch (WebException exception)
            {
                webResponse = exception.Response as HttpWebResponse;
                if (webResponse == null)
                {
                    throw;
                }
            }
            if (webResponse != null)
            {
                using (webResponse)
                {
                    responseStatusCode = webResponse.StatusCode;
                    var responseStream = webResponse.GetResponseStream();
                    if (responseStream != null)
                    {
                        using (var sr = new StreamReader(responseStream))
                        {
                            responseBody = sr.ReadToEnd();
                        }
                    }
                }
            }
        }
    }
}
