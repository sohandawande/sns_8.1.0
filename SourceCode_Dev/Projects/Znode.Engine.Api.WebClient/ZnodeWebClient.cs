﻿using System.Collections.Generic;
using System.IO;
using System.Net;
using Newtonsoft.Json;

namespace Znode.Engine.Api.WebClient
{
	public class ZnodeWebClient<TS, TD>
	{
		public HttpStatusCode StatusCode { get; set; }

		public TD Get(string url, Dictionary<string, string> additionalHeaders = null)
		{
			var response = WebApiClient.Engine.Get(WebApiClient.WebApiUrl + url, additionalHeaders);
			StatusCode = response.StatusCode;
			return GetResponseOutput(response.Body);
		}

		public TD Post(TS inputObj, string url, Dictionary<string, string> additionalHeaders = null)
		{
			var response = WebApiClient.Engine.Post(WebApiClient.WebApiUrl + url, GetRequestInput(inputObj), additionalHeaders);
			StatusCode = response.StatusCode;
			return GetResponseOutput(response.Body);
		}

		public TD Put(TS inputObj, string url, Dictionary<string, string> additionalHeaders = null)
		{
			var response = WebApiClient.Engine.Put(WebApiClient.WebApiUrl + url, GetRequestInput(inputObj), additionalHeaders);
			StatusCode = response.StatusCode;
			return GetResponseOutput(response.Body);
		}

		public TD Delete(string url, Dictionary<string, string> additionalHeaders = null)
		{
			var response = WebApiClient.Engine.Delete(WebApiClient.WebApiUrl + url, additionalHeaders);
			StatusCode = response.StatusCode;
			return GetResponseOutput(response.Body);
		}

		private TD GetResponseOutput(string response)
		{
			var contentReader = new StringReader(response);
			var jsonReader = new JsonTextReader(contentReader);
			var ser = new JsonSerializer();
			ser.DateFormatHandling = DateFormatHandling.IsoDateFormat;
			var result = ser.Deserialize<TD>(jsonReader);
			return result;
		}

		private string GetRequestInput(TS request)
		{
			var json = new JsonSerializer();
			var textWriter = new StringWriter();
			JsonWriter jsonWriter = new JsonTextWriter(textWriter);
			json.Serialize(jsonWriter, request); //.WriteObject(stream, request);			
			return textWriter.ToString();
		}
	}
}