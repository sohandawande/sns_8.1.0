﻿using System.Collections;
using System.Xml;
using ZNode.Libraries.Google.GoogleCheckoutServices;

namespace ZNode.Libraries.Google.GoogleCallbackServices
{
    /// <summary>
    /// This class contains methods that parse a 
    /// &lt;merchant-calculation-callback&gt; request, allowing you to access 
    /// items in the request, shipping methods, shipping addresses, tax 
    /// information, and coupon and gift certificate codes entered by the 
    /// customer.
    /// </summary>
    public class CallbackProcessor
    {
        private CallbackRules _Rules = null;
        private string DefaultCurrency = ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.CurrencyCode();

        /// <summary>
        /// Initializes a new instance of the CallbackProcessor class.
        /// passing in the rules used to process the request.
        /// </summary>
        /// <param name="Rules">The <see cref="CallbackRules"/> object used for the Process Request.</param>
        public CallbackProcessor(CallbackRules Rules)
        {
            this._Rules = Rules;
        }

        /// <summary>
        /// Process the Xml Message
        /// </summary>
        /// <param name="CallbackXML">The Callback message to process</param>
        /// <returns>The <see cref="MerchantCalculationResults"/> that is returned to Google Checkout</returns>
        public byte[] Process(string CallbackXML)
        {
            // Deserialize the callback request.
            MerchantCalculationCallback Callback = (MerchantCalculationCallback)MessageHelper.Deserialize(CallbackXML, typeof(MerchantCalculationCallback));

            // Create the callback response.
            MerchantCalculationResults RetVal = new MerchantCalculationResults();

            // Create the order.
            Order ThisOrder = new Order(Callback);

            // Are there shipping methods?
            string[] ShippingMethods = new string[1] { string.Empty };

            if (Callback.calculate.shipping != null && Callback.calculate.shipping.Length > 0)
            {
                ShippingMethods = new string[Callback.calculate.shipping.Length];
                for (int s = 0; s < ShippingMethods.Length; s++)
                {
                    ShippingMethods[s] = Callback.calculate.shipping[s].name;
                }
            }

            RetVal.results = new Result[Callback.calculate.addresses.Length * ShippingMethods.Length];
            int ResultIndex = 0;
            for (int a = 0; a < Callback.calculate.addresses.Length; a++)
            {
                for (int s = 0; s < ShippingMethods.Length; s++)
                {
                    Result ThisResult = new Result();
                    ThisResult.addressid = Callback.calculate.addresses[a].id;
                    AnonymousAddress ThisAddress = new AnonymousAddress(Callback.calculate.addresses[a]);

                    // Check shipping, if requested.
                    if (ShippingMethods[s] != string.Empty)
                    {
                        ThisResult.shippingname = ShippingMethods[s];
                        ShippingResult SResult = this._Rules.GetShippingResult(ShippingMethods[s], ThisOrder, ThisAddress);
                        ThisResult.shippableSpecified = true;
                        ThisResult.shippable = SResult.Shippable;
                        ThisResult.shippingrate = new ResultShippingrate();
                        ThisResult.shippingrate.currency = this.DefaultCurrency;
                        ThisResult.shippingrate.Value = SResult.ShippingRate;
                    }

                    // Check tax, if requested.
                    if (Callback.calculate.tax)
                    {
                        ThisResult.totaltax = new ResultTotaltax();
                        ThisResult.totaltax.currency = this.DefaultCurrency;
                        ThisResult.totaltax.Value = this._Rules.GetTaxResult(ThisOrder, ThisAddress, (ThisResult.shippingrate != null ? ThisResult.shippingrate.Value : 0));
                    }

                    // Check merchant codes.
                    if (Callback.calculate.merchantcodestrings != null)
                    {
                        ThisResult.merchantcoderesults = new ResultMerchantcoderesults();
                        ThisResult.merchantcoderesults.Items = new object[Callback.calculate.merchantcodestrings.Length];
                        ArrayList UsedMerchantCodes = new ArrayList();
                        for (int c = 0; c < Callback.calculate.merchantcodestrings.Length; c++)
                        {
                            MerchantCodeResult MCR;
                            string CurrentMerchantCode = Callback.calculate.merchantcodestrings[c].code;
                            if (UsedMerchantCodes.Contains(CurrentMerchantCode.ToUpper()))
                            {
                                CouponResult CouponResult = new CouponResult();
                                CouponResult.calculatedamount = new CouponResultCalculatedamount();
                                CouponResult.calculatedamount.currency = this.DefaultCurrency;
                                CouponResult.calculatedamount.Value = 0;
                                CouponResult.code = CurrentMerchantCode;
                                CouponResult.message = "Code already used.";
                                CouponResult.valid = false;
                                ThisResult.merchantcoderesults.Items[c] = CouponResult;
                            }
                            else
                            {
                                MCR = this._Rules.GetMerchantCodeResult(ThisOrder, ThisAddress, CurrentMerchantCode);
                                if (MCR.Type == MerchantCodeResult.MerchantCodeType.GiftCertificate)
                                {
                                    GiftCertificateResult GCResult = new GiftCertificateResult();
                                    GCResult.calculatedamount = new GiftCertificateResultCalculatedamount();
                                    GCResult.calculatedamount.currency = this.DefaultCurrency;
                                    GCResult.calculatedamount.Value = MCR.Amount;
                                    GCResult.code = CurrentMerchantCode;
                                    GCResult.message = MCR.Message;
                                    GCResult.valid = MCR.Valid;
                                    ThisResult.merchantcoderesults.Items[c] = GCResult;
                                    UsedMerchantCodes.Add(CurrentMerchantCode.ToUpper());
                                }
                                else
                                {
                                    CouponResult CouponResult = new CouponResult();
                                    CouponResult.calculatedamount = new CouponResultCalculatedamount();
                                    CouponResult.calculatedamount.currency = this.DefaultCurrency;
                                    CouponResult.calculatedamount.Value = MCR.Amount;
                                    CouponResult.code = CurrentMerchantCode;
                                    CouponResult.message = MCR.Message;
                                    CouponResult.valid = MCR.Valid;
                                    ThisResult.merchantcoderesults.Items[c] = CouponResult;
                                    UsedMerchantCodes.Add(CurrentMerchantCode.ToUpper());
                                }
                            }
                        }
                    }

                    RetVal.results[ResultIndex] = ThisResult;
                    ResultIndex++;
                }
            }

            return MessageHelper.Serialize(RetVal);
        }
    }
}
