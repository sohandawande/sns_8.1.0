using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.Google
{
    /// <summary>
    /// Google Checkout response
    /// </summary>
    public class GoogleCheckoutResponse : ZNodeBusinessBase
    {
        #region Protected member variables
        /// <summary>
        /// Represents Express Checkout redirect URL
        /// </summary>
        private string _RedirectURL = string.Empty;

        /// <summary>
        /// Represents the Gateway Response code as a string.
        /// </summary>
        private string _ResponseCode = string.Empty;

        /// <summary>
        ///  Represents the Gateway Response code as a string.
        /// </summary>
        private string _ResponseText = string.Empty;

        /// <summary>
        ///  Represents the Gateway Response code as a string.
        /// </summary>
        private string _SerialNumber = string.Empty;

        #endregion

        #region Public properties

        /// <summary>
        /// Gets or sets the Gateway Response code
        /// </summary>
        public string ResponseCode
        {
            get { return this._ResponseCode; }
            set { this._ResponseCode = value; }
        }

        /// <summary>
        /// Gets or sets the Gateway Response Text
        /// </summary>
        public string ResponseText
        {
            get { return this._ResponseText; }
            set { this._ResponseText = value; }
        }

        /// <summary>
        /// Gets or sets the Gateway TransactionID
        /// </summary>
        public string SerialNumber
        {
            get { return this._SerialNumber; }
            set { this._SerialNumber = value; }
        }

        /// <summary>
        /// Gets or sets the EC Redirect URL.
        /// </summary>
        public string ECRedirectURL
        {
            get { return this._RedirectURL; }
            set { this._RedirectURL = value; }
        }
        #endregion
    }
}