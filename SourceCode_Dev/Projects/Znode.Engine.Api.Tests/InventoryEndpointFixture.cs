﻿using System.Collections.ObjectModel;
using NUnit.Framework;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Engine.Api.Tests
{
	[TestFixture]
	public class InventoryEndpointFixture : BaseFixture
	{
		[Test]
		public void get_inventory()
		{
			var route = "inventory/1";
			var response = GetModelFromEndpoint<InventoryResponse>(route);

			Assert.IsNotNull(response.Inventory, "Inventory was null");
			Assert.IsTrue(response.Inventory.GetType() == typeof(InventoryModel), "Inventory type was not InventoryModel");
		}

		[Test]
		public void get_inventory_expand_skus()
		{
			var route = "inventory/1?expand=skus";
			var response = GetModelFromEndpoint<InventoryResponse>(route);

			Assert.IsNotNull(response.Inventory.Skus, "SKUs were null");
			Assert.IsNotEmpty(response.Inventory.Skus, "SKUs were empty");
			Assert.IsTrue(response.Inventory.Skus.GetType() == typeof(Collection<SkuModel>), "SKUs type was not Collection<SkuModel>");
		}

		[Test]
		public void get_inventories()
		{
			var route = "inventory";
			var response = GetModelFromEndpoint<InventoryListResponse>(route);

			Assert.IsNotNull(response.Inventories, "Inventories were null");
			Assert.IsTrue(response.Inventories.GetType() == typeof(Collection<InventoryModel>), "Inventories type was not Collection<InventoryModel>");
		}

		[Test]
		public void get_inventory_filter_quantity_on_hand_equals_1000()
		{
			var route = "inventory?filter=quantityonhand~eq~1000";
			var response = GetModelFromEndpoint<InventoryListResponse>(route);

			Assert.IsNotNull(response.Inventories, "Inventories were null");
			Assert.IsTrue(response.Inventories.GetType() == typeof(Collection<InventoryModel>), "Inventories type was not Collection<InventoryModel>");

			foreach (var i in response.Inventories)
			{
				Assert.AreEqual(1000, i.QuantityOnHand, "Expected quantity on hand 1000, actual quantity on hand " + i.QuantityOnHand);
			}
		}

		[Test]
		public void get_inventory_filter_quantity_on_hand_not_equal_to_1000()
		{
			var route = "inventory?filter=quantityonhand~ne~1000";
			var response = GetModelFromEndpoint<InventoryListResponse>(route);

			Assert.IsNotNull(response.Inventories, "Inventories were null");
			Assert.IsTrue(response.Inventories.GetType() == typeof(Collection<InventoryModel>), "Inventories type was not Collection<InventoryModel>");

			foreach (var i in response.Inventories)
			{
				Assert.AreNotEqual(1000, i.QuantityOnHand, "Expected quantity on hand not equal to 1000");
			}
		}

		[Test]
		public void get_inventory_filter_sku_contains_234()
		{
			var route = "inventory?filter=sku~cn~234";
			var response = GetModelFromEndpoint<InventoryListResponse>(route);

			Assert.IsNotNull(response.Inventories, "Inventories were null");
			Assert.IsTrue(response.Inventories.GetType() == typeof(Collection<InventoryModel>), "Inventories type was not Collection<InventoryModel>");

			foreach (var i in response.Inventories)
			{
				Assert.IsTrue(i.Sku.ToLower().Contains("234"), "Expected SKU to contain '234', actual SKU '" + i.Sku.ToLower() + "'");
			}
		}

		[Test]
		public void get_inventory_filter_sku_starts_with_bb()
		{
			var route = "inventory?filter=sku~sw~bb";
			var response = GetModelFromEndpoint<InventoryListResponse>(route);

			Assert.IsNotNull(response.Inventories, "Inventories were null");
			Assert.IsTrue(response.Inventories.GetType() == typeof(Collection<InventoryModel>), "Inventories type was not Collection<InventoryModel>");

			foreach (var i in response.Inventories)
			{
				Assert.IsTrue(i.Sku.ToLower().StartsWith("bb"), "Expected SKU to start with 'bb', actual SKU started with '" + i.Sku.ToLower() + "'");
			}
		}

		[Test]
		public void get_inventory_filter_sku_ends_with_123()
		{
			var route = "inventory?filter=sku~ew~123";
			var response = GetModelFromEndpoint<InventoryListResponse>(route);

			Assert.IsNotNull(response.Inventories, "Inventories were null");
			Assert.IsTrue(response.Inventories.GetType() == typeof(Collection<InventoryModel>), "Inventories type was not Collection<InventoryModel>");

			foreach (var i in response.Inventories)
			{
				Assert.IsTrue(i.Sku.ToLower().EndsWith("123"), "Expected SKU to end with '123', actual SKU ended with '" + i.Sku.ToLower() + "'");
			}
		}

		[Test]
		public void get_inventory_page_index_0_size_5()
		{
			var route = "inventory?page=index~0,size~5";
			var response = GetModelFromEndpoint<InventoryListResponse>(route);

			Assert.IsNotNull(response.Inventories, "Inventories were null");
			Assert.IsTrue(response.Inventories.GetType() == typeof(Collection<InventoryModel>), "Inventories type was not Collection<InventoryModel>");
			Assert.IsTrue(response.Inventories.Count == 5, "Expected count equal to 5, actual count " + response.Inventories.Count);
		}

		[Test]
		public void create_update_delete_inventory()
		{
			// Create
			var route = "inventory";
			var data = GetDataFromFile("Inventory-Create.json");
			var response = PostModelToEndpoint<InventoryResponse>(route, data);

			Assert.IsNotNull(response.Inventory, "Inventory was null");
			Assert.IsTrue(response.Inventory.InventoryId > 0, "Inventory ID was not greater than 0");
			Assert.IsTrue(response.Inventory.QuantityOnHand == 1000, "Expected quantity on hand 1000, actual quantity on hand " + response.Inventory.QuantityOnHand);

			// Update
			route = "inventory/" + response.Inventory.InventoryId;
			data = GetDataFromFile("Inventory-Update.json");
			response = PutModelToEndpoint<InventoryResponse>(route, data);

			Assert.IsNotNull(response.Inventory, "Inventory was null");
			Assert.IsTrue(response.Inventory.QuantityOnHand == 500, "Expected quantity on hand 500, actual quantity on hand " + response.Inventory.QuantityOnHand);

			// Delete
			route = "inventory/" + response.Inventory.InventoryId;
			var result = DeleteResourceFromEndpoint(route);

			Assert.IsTrue(result, "Inventory was not deleted");
		}
	}
}
