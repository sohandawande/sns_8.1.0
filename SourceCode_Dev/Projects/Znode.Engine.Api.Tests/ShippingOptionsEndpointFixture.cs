﻿using System.Collections.ObjectModel;
using NUnit.Framework;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Engine.Api.Tests
{
	[TestFixture]
	public class ShippingOptionsEndpointFixture : BaseFixture
	{
		[Test]
		public void get_shipping_option()
		{
			var route = "shippingoptions/9";
			var response = GetModelFromEndpoint<ShippingOptionResponse>(route);

			Assert.IsNotNull(response.ShippingOption, "Shipping option was null");
			Assert.IsTrue(response.ShippingOption.GetType() == typeof(ShippingOptionModel), "Shipping option type was not ShippingOptionModel");
		}

		[Test]
		public void get_shipping_options()
		{
			var route = "shippingoptions";
			var response = GetModelFromEndpoint<ShippingOptionListResponse>(route);

			Assert.IsNotNull(response.ShippingOptions, "Shipping options were null");
			Assert.IsTrue(response.ShippingOptions.GetType() == typeof(Collection<ShippingOptionModel>), "ShippingOptions type was not Collection<ShippingOptionModel>");
		}

		[Test]
		public void get_shipping_options_filter_shipping_code_equals_flat()
		{
			var route = "shippingoptions?filter=shippingcode~eq~flat";
			var response = GetModelFromEndpoint<ShippingOptionListResponse>(route);

			Assert.IsNotNull(response.ShippingOptions, "Shipping options were null");
			Assert.IsTrue(response.ShippingOptions.GetType() == typeof(Collection<ShippingOptionModel>), "ShippingOptions type was not Collection<ShippingOptionModel>");

			foreach (var s in response.ShippingOptions)
			{
				Assert.AreEqual("flat", s.ShippingCode.ToLower(), "Expected shipping code 'flat', actual shipping code '" + s.ShippingCode.ToLower() + "'");
			}
		}

		[Test]
		public void get_shipping_options_filter_shipping_code_not_equal_to_flat()
		{
			var route = "shippingoptions?filter=shippingcode~ne~flat";
			var response = GetModelFromEndpoint<ShippingOptionListResponse>(route);

			Assert.IsNotNull(response.ShippingOptions, "Shipping options were null");
			Assert.IsTrue(response.ShippingOptions.GetType() == typeof(Collection<ShippingOptionModel>), "ShippingOptions type was not Collection<ShippingOptionModel>");

			foreach (var s in response.ShippingOptions)
			{
				Assert.AreNotEqual("flat", s.ShippingCode.ToLower(), "Expected shipping code not equal to 'flat'");
			}
		}

		[Test]
		public void get_shipping_options_filter_description_contains_flat()
		{
			var route = "shippingoptions?filter=description~cn~flat";
			var response = GetModelFromEndpoint<ShippingOptionListResponse>(route);

			Assert.IsNotNull(response.ShippingOptions, "Shipping options were null");
			Assert.IsTrue(response.ShippingOptions.GetType() == typeof(Collection<ShippingOptionModel>), "ShippingOptions type was not Collection<ShippingOptionModel>");

			foreach (var s in response.ShippingOptions)
			{
				Assert.IsTrue(s.Description.ToLower().Contains("flat"), "Expected description to contain 'flat', actual description '" + s.Description.ToLower() + "'");
			}
		}

		[Test]
		public void get_shipping_options_filter_description_starts_with_custom()
		{
			var route = "shippingoptions?filter=description~sw~custom";
			var response = GetModelFromEndpoint<ShippingOptionListResponse>(route);

			Assert.IsNotNull(response.ShippingOptions, "Shipping options were null");
			Assert.IsTrue(response.ShippingOptions.GetType() == typeof(Collection<ShippingOptionModel>), "ShippingOptions type was not Collection<ShippingOptionModel>");

			foreach (var s in response.ShippingOptions)
			{
				Assert.IsTrue(s.Description.ToLower().StartsWith("custom"), "Expected description to start with 'custom', actual description started with '" + s.Description.ToLower() + "'");
			}
		}

		[Test]
		public void get_shipping_options_filter_description_ends_with_rate()
		{
			var route = "shippingoptions?filter=description~ew~rate";
			var response = GetModelFromEndpoint<ShippingOptionListResponse>(route);

			Assert.IsNotNull(response.ShippingOptions, "Shipping options were null");
			Assert.IsTrue(response.ShippingOptions.GetType() == typeof(Collection<ShippingOptionModel>), "ShippingOptions type was not Collection<ShippingOptionModel>");

			foreach (var s in response.ShippingOptions)
			{
				Assert.IsTrue(s.Description.ToLower().EndsWith("rate"), "Expected description to end with 'rate', actual description ended with '" + s.Description.ToLower() + "'");
			}
		}

		[Test]
		public void get_shipping_options_page_index_0_size_1()
		{
			var route = "shippingoptions?page=index~0,size~1";
			var response = GetModelFromEndpoint<ShippingOptionListResponse>(route);

			Assert.IsNotNull(response.ShippingOptions, "Shipping options were null");
			Assert.IsTrue(response.ShippingOptions.GetType() == typeof(Collection<ShippingOptionModel>), "ShippingOptions type was not Collection<ShippingOptionModel>");
			Assert.IsTrue(response.ShippingOptions.Count == 1, "Expected count equal to 1, actual count " + response.ShippingOptions.Count);
		}

		[Test]
		public void create_update_delete_shipping_option()
		{
			// Create
			var route = "shippingoptions";
			var data = GetDataFromFile("ShippingOption-Create.json");
			var response = PostModelToEndpoint<ShippingOptionResponse>(route, data);

			Assert.IsNotNull(response.ShippingOption, "Shipping option was null");
            Assert.IsTrue(response.ShippingOption.ShippingId > 0, "Shipping type ID was not greater than 0");
			Assert.IsTrue(response.ShippingOption.Description == "NewShippingOption", "Expected description 'NewShippingOption', actual description '" + response.ShippingOption.Description + "'");

			// Update
            route = "shippingoptions/" + response.ShippingOption.ShippingId;
			data = GetDataFromFile("ShippingOption-Update.json");
			response = PutModelToEndpoint<ShippingOptionResponse>(route, data);

			Assert.IsNotNull(response.ShippingOption, "Shipping option was null");
			Assert.IsTrue(response.ShippingOption.Description == "UpdatedShippingOption", "Expected description 'UpdatedShippingOption', actual description '" + response.ShippingOption.Description + "'");

			// Delete
            route = "shippingoptions/" + response.ShippingOption.ShippingId;
			var result = DeleteResourceFromEndpoint(route);

			Assert.IsTrue(result, "Shipping option was not deleted");
		}
	}
}
