﻿using System.Collections.ObjectModel;
using NUnit.Framework;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Engine.Api.Tests
{
	[TestFixture]
	public class SuppliersEndpointFixture : BaseFixture
	{
		[Test]
		public void get_supplier()
		{
			var route = "suppliers/1";
			var response = GetModelFromEndpoint<SupplierResponse>(route);

			Assert.IsNotNull(response.Supplier, "Supplier was null");
			Assert.IsTrue(response.Supplier.GetType() == typeof(SupplierModel), "Supplier type was not SupplierModel");
		}

		[Test]
		public void get_suppliers()
		{
			var route = "suppliers";
			var response = GetModelFromEndpoint<SupplierListResponse>(route);

			Assert.IsNotNull(response.Suppliers, "Suppliers were null");
			Assert.IsTrue(response.Suppliers.GetType() == typeof(Collection<SupplierModel>), "Suppliers type was not Collection<SupplierModel>");
		}

		[Test]
		public void get_suppliers_filter_name_equals_supplier_email()
		{
			var route = "suppliers?filter=name~eq~supplier%20email";
			var response = GetModelFromEndpoint<SupplierListResponse>(route);

			Assert.IsNotNull(response.Suppliers, "Suppliers were null");
			Assert.IsTrue(response.Suppliers.GetType() == typeof(Collection<SupplierModel>), "Suppliers type was not Collection<SupplierModel>");

			foreach (var s in response.Suppliers)
			{
				Assert.AreEqual("supplier email", s.Name.ToLower(), "Expected name 'supplier email', actual name '" + s.Name.ToLower() + "'");
			}
		}

		[Test]
		public void get_suppliers_filter_name_not_equal_to_supplier_email()
		{
			var route = "suppliers?filter=name~ne~supplier%20email";
			var response = GetModelFromEndpoint<SupplierListResponse>(route);

			Assert.IsNotNull(response.Suppliers, "Suppliers were null");
			Assert.IsTrue(response.Suppliers.GetType() == typeof(Collection<SupplierModel>), "Suppliers type was not Collection<SupplierModel>");

			foreach (var s in response.Suppliers)
			{
				Assert.AreNotEqual("supplier email", s.Name.ToLower(), "Expected name not equal to 'supplier email'");
			}
		}

		[Test]
		public void get_suppliers_filter_name_contains_email()
		{
			var route = "suppliers?filter=name~cn~email";
			var response = GetModelFromEndpoint<SupplierListResponse>(route);

			Assert.IsNotNull(response.Suppliers, "Suppliers were null");
			Assert.IsTrue(response.Suppliers.GetType() == typeof(Collection<SupplierModel>), "Suppliers type was not Collection<SupplierModel>");

			foreach (var s in response.Suppliers)
			{
				Assert.IsTrue(s.Name.ToLower().Contains("email"), "Expected name to contain 'email', actual name '" + s.Name.ToLower() + "'");
			}
		}

		[Test]
		public void get_suppliers_filter_name_starts_with_supplier()
		{
			var route = "suppliers?filter=name~sw~supplier";
			var response = GetModelFromEndpoint<SupplierListResponse>(route);

			Assert.IsNotNull(response.Suppliers, "Suppliers were null");
			Assert.IsTrue(response.Suppliers.GetType() == typeof(Collection<SupplierModel>), "Suppliers type was not Collection<SupplierModel>");

			foreach (var s in response.Suppliers)
			{
				Assert.IsTrue(s.Name.ToLower().StartsWith("supplier"), "Expected name to start with 'supplier', actual name started with '" + s.Name.ToLower() + "'");
			}
		}

		[Test]
		public void get_suppliers_filter_name_ends_with_email()
		{
			var route = "suppliers?filter=name~ew~email";
			var response = GetModelFromEndpoint<SupplierListResponse>(route);

			Assert.IsNotNull(response.Suppliers, "Suppliers were null");
			Assert.IsTrue(response.Suppliers.GetType() == typeof(Collection<SupplierModel>), "Suppliers type was not Collection<SupplierModel>");

			foreach (var s in response.Suppliers)
			{
				Assert.IsTrue(s.Name.ToLower().EndsWith("email"), "Expected name to end with 'email', actual name ended with '" + s.Name.ToLower() + "'");
			}
		}

		[Test]
		public void get_suppliers_page_index_0_size_1()
		{
			var route = "suppliers?page=index~0,size~1";
			var response = GetModelFromEndpoint<SupplierListResponse>(route);

			Assert.IsNotNull(response.Suppliers, "Suppliers were null");
			Assert.IsTrue(response.Suppliers.GetType() == typeof(Collection<SupplierModel>), "Suppliers type was not Collection<SupplierModel>");
			Assert.IsTrue(response.Suppliers.Count == 1, "Expected count equal to 1, actual count " + response.Suppliers.Count);
		}

		[Test]
		public void create_update_delete_supplier()
		{
			// Create
			var route = "suppliers";
			var data = GetDataFromFile("Supplier-Create.json");
			var response = PostModelToEndpoint<SupplierResponse>(route, data);

			Assert.IsNotNull(response.Supplier, "Supplier was null");
			Assert.IsTrue(response.Supplier.SupplierId > 0, "Supplier ID was not greater than 0");
			Assert.IsTrue(response.Supplier.Name == "NewSupplier", "Expected name 'NewSupplier', actual name '" + response.Supplier.Name + "'");

			// Update
			route = "suppliers/" + response.Supplier.SupplierId;
			data = GetDataFromFile("Supplier-Update.json");
			response = PutModelToEndpoint<SupplierResponse>(route, data);

			Assert.IsNotNull(response.Supplier, "Supplier was null");
			Assert.IsTrue(response.Supplier.Name == "UpdatedSupplier", "Expected name 'UpdatedSupplier', actual name '" + response.Supplier.Name + "'");

			// Delete
			route = "suppliers/" + response.Supplier.SupplierId;
			var result = DeleteResourceFromEndpoint(route);

			Assert.IsTrue(result, "Supplier was not deleted");
		}
	}
}
