﻿using System.Collections.ObjectModel;
using NUnit.Framework;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Engine.Api.Tests
{
	[TestFixture]
	public class SkusEndpointFixture : BaseFixture
	{
		[Test]
		public void get_sku()
		{
			var route = "skus/2";
			var response = GetModelFromEndpoint<SkuResponse>(route);

			Assert.IsNotNull(response.Sku, "SKU was null");
			Assert.IsTrue(response.Sku.GetType() == typeof(SkuModel), "SKU type was not SkuModel");
		}

		[Test]
		public void get_sku_expand_attributes()
		{
			var route = "skus/2?expand=attributes";
			var response = GetModelFromEndpoint<SkuResponse>(route);

			Assert.IsNotNull(response.Sku.Attributes, "Attributes were null");
			Assert.IsNotEmpty(response.Sku.Attributes, "Attributes were empty");
			Assert.IsTrue(response.Sku.Attributes.GetType() == typeof(Collection<AttributeModel>), "Atttributes type was not Collection<AttributeModel>");
		}

		[Test]
		public void get_sku_expand_inventory()
		{
			var route = "skus/2?expand=inventory";
			var response = GetModelFromEndpoint<SkuResponse>(route);

			Assert.IsNotNull(response.Sku.Inventory, "Inventory were null");
			Assert.IsTrue(response.Sku.Inventory.GetType() == typeof(InventoryModel), "Inventory type was not InventoryModel");
		}

		[Test]
		public void get_skus_filter_externalid_equals_null()
		{
			var route = "skus?filter=externalid~eq~null";
			var response = GetModelFromEndpoint<SkuListResponse>(route);

			Assert.IsNotNull(response.Skus, "SKUs were null");
			Assert.IsTrue(response.Skus.GetType() == typeof(Collection<SkuModel>), "SKUs type was not Collection<SkuModel>");
		}

		[Test]
		public void get_skus_filter_externalid_not_equal_to_null()
		{
			var route = "skus?filter=externalid~ne~null";
			var response = GetModelFromEndpoint<SkuListResponse>(route);

			Assert.IsNotNull(response.Skus, "SKUs were null");
			Assert.IsTrue(response.Skus.GetType() == typeof(Collection<SkuModel>), "SKUs type was not Collection<SkuModel>");
		}

		[Test]
		public void get_skus_filter_productid_equals_302()
		{
			var route = "skus?filter=productid~eq~302";
			var response = GetModelFromEndpoint<SkuListResponse>(route);

			Assert.IsNotNull(response.Skus, "SKUs were null");
			Assert.IsTrue(response.Skus.GetType() == typeof(Collection<SkuModel>), "SKUs type was not Collection<SkuModel>");

			foreach (var s in response.Skus)
			{
				Assert.AreEqual(302, s.ProductId, "Expected product ID 302, actual product ID " + s.ProductId);
			}
		}

		[Test]
		public void get_skus_filter_productid_not_equal_to_302()
		{
			var route = "skus?filter=productid~ne~302";
			var response = GetModelFromEndpoint<SkuListResponse>(route);

			Assert.IsNotNull(response.Skus, "SKUs were null");
			Assert.IsTrue(response.Skus.GetType() == typeof(Collection<SkuModel>), "SKUs type was not Collection<SkuModel>");

			foreach (var s in response.Skus)
			{
				Assert.AreNotEqual(302, s.ProductId, "Expected product ID not equal to 302");
			}
		}

		[Test]
		public void get_skus_filter_sku_equals_apr234()
		{
			var route = "skus?filter=sku~eq~apr234";
			var response = GetModelFromEndpoint<SkuListResponse>(route);

			Assert.IsNotNull(response.Skus, "SKUs were null");
			Assert.IsTrue(response.Skus.GetType() == typeof(Collection<SkuModel>), "SKUs type was not Collection<SkuModel>");

			foreach (var s in response.Skus)
			{
				Assert.AreEqual("apr234", s.Sku, "Expected SKU to equal 'apr234', actual SKU '" + s.Sku + "'");
			}
		}

		[Test]
		public void get_skus_filter_sku_contains_234()
		{
			var route = "skus?filter=sku~cn~234";
			var response = GetModelFromEndpoint<SkuListResponse>(route);

			Assert.IsNotNull(response.Skus, "SKUs were null");
			Assert.IsTrue(response.Skus.GetType() == typeof(Collection<SkuModel>), "SKUs type was not Collection<SkuModel>");

			foreach (var s in response.Skus)
			{
				Assert.IsTrue(s.Sku.Contains("234"), "Expected SKU to contain '234', actual SKU '" + s.Sku + "'");
			}
		}

		[Test]
		public void get_skus_filter_productid_greater_than_302()
		{
			var route = "skus?filter=productid~gt~302";
			var response = GetModelFromEndpoint<SkuListResponse>(route);

			Assert.IsNotNull(response.Skus, "SKUs were null");
			Assert.IsTrue(response.Skus.GetType() == typeof(Collection<SkuModel>), "SKUs type was not Collection<SkuModel>");

			foreach (var s in response.Skus)
			{
				Assert.IsTrue(s.ProductId > 302, "Expected product ID greater than 302, actual product ID " + s.ProductId);
			}
		}

		[Test]
		public void get_skus_filter_productid_greater_than_or_equal_to_302()
		{
			var route = "skus?filter=productid~ge~302";
			var response = GetModelFromEndpoint<SkuListResponse>(route);

			Assert.IsNotNull(response.Skus, "SKUs were null");
			Assert.IsTrue(response.Skus.GetType() == typeof(Collection<SkuModel>), "SKUs type was not Collection<SkuModel>");

			foreach (var s in response.Skus)
			{
				Assert.IsTrue(s.ProductId >= 302, "Expected product ID greater than or equal to 302, actual product ID " + s.ProductId);
			}
		}

		[Test]
		public void get_skus_filter_productid_less_than_500()
		{
			var route = "skus?filter=productid~lt~500";
			var response = GetModelFromEndpoint<SkuListResponse>(route);

			Assert.IsNotNull(response.Skus, "SKUs were null");
			Assert.IsTrue(response.Skus.GetType() == typeof(Collection<SkuModel>), "SKUs type was not Collection<SkuModel>");

			foreach (var s in response.Skus)
			{
				Assert.IsTrue(s.ProductId < 500, "Expected product ID less than 500, actual product ID " + s.ProductId);
			}
		}

		[Test]
		public void get_skus_filter_productid_less_than_or_equal_to_500()
		{
			var route = "skus?filter=productid~le~500";
			var response = GetModelFromEndpoint<SkuListResponse>(route);

			Assert.IsNotNull(response.Skus, "SKUs were null");
			Assert.IsTrue(response.Skus.GetType() == typeof(Collection<SkuModel>), "SKUs type was not Collection<SkuModel>");

			foreach (var s in response.Skus)
			{
				Assert.IsTrue(s.ProductId <= 500, "Expected product ID less than or equal to 500, actual product ID " + s.ProductId);
			}
		}

		[Test]
		public void get_skus_filter_sku_starts_with_ap()
		{
			var route = "skus?filter=sku~sw~ap";
			var response = GetModelFromEndpoint<SkuListResponse>(route);

			Assert.IsNotNull(response.Skus, "SKUs were null");
			Assert.IsTrue(response.Skus.GetType() == typeof(Collection<SkuModel>), "SKUs type was not Collection<SkuModel>");

			foreach (var s in response.Skus)
			{
				Assert.IsTrue(s.Sku.ToLower().StartsWith("ap"), "Expected SKU to start with 'ap', actual SKU '" + s.Sku.ToLower() + "'");
			}
		}

		[Test]
		public void get_skus_filter_sku_ends_with_34()
		{
			var route = "skus?filter=sku~ew~34";
			var response = GetModelFromEndpoint<SkuListResponse>(route);

			Assert.IsNotNull(response.Skus, "SKUs were null");
			Assert.IsTrue(response.Skus.GetType() == typeof(Collection<SkuModel>), "SKUs type was not Collection<SkuModel>");

			foreach (var s in response.Skus)
			{
				Assert.IsTrue(s.Sku.ToLower().EndsWith("34"), "Expected SKU to end with '34', actual SKU '" + s.Sku.ToLower() + "'");
			}
		}

		[Test]
		public void create_update_delete_sku()
		{
			// Create
			var route = "skus";
			var data = GetDataFromFile("Sku-Create.json");
			var response = PostModelToEndpoint<SkuResponse>(route, data);

			Assert.IsNotNull(response.Sku, "SKU was null");
			Assert.IsTrue(response.Sku.SkuId > 0, "SKU ID was not greater than 0");
			Assert.IsTrue(response.Sku.Custom1 == "NewSku", "Expected name 'NewSku', actual name '" + response.Sku.Custom1 + "'");

			// Update
			route = "skus/" + response.Sku.SkuId;
			data = GetDataFromFile("Sku-Update.json");
			response = PutModelToEndpoint<SkuResponse>(route, data);

			Assert.IsNotNull(response.Sku, "SKU was null");
			Assert.IsTrue(response.Sku.Custom1 == "UpdatedSku", "Expected name 'UpdatedSku', actual name '" + response.Sku.Custom1 + "'");

			// Delete
			route = "skus/" + response.Sku.SkuId;
			var result = DeleteResourceFromEndpoint(route);

			Assert.IsTrue(result, "SKU was not deleted");
		}
	}
}
