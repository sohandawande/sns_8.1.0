﻿using System.Collections.ObjectModel;
using NUnit.Framework;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Engine.Api.Tests
{
	[TestFixture]
	public class CatalogsEndpointFixture : BaseFixture
	{
		[Test]
		public void get_catalog()
		{
			var route = "catalogs/1";
			var response = GetModelFromEndpoint<CatalogResponse>(route);

			Assert.IsNotNull(response.Catalog, "Catalog was null");
			Assert.IsTrue(response.Catalog.GetType() == typeof(CatalogModel), "Catalog type was not CatalogModel");
		}

		[Test]
		public void get_catalogs()
		{
			var route = "catalogs";
			var response = GetModelFromEndpoint<CatalogListResponse>(route);

			Assert.IsNotNull(response.Catalogs, "Catalogs were null");
			Assert.IsTrue(response.Catalogs.GetType() == typeof(Collection<CatalogModel>), "Catalogs type was not Collection<CatalogModel>");
		}

		[Test]
		public void get_catalogs_filter_externalid_equals_null()
		{
			var route = "catalogs?filter=externalid~eq~null";
			var response = GetModelFromEndpoint<CatalogListResponse>(route);

			Assert.IsNotNull(response.Catalogs, "Catalogs were null");
			Assert.IsTrue(response.Catalogs.GetType() == typeof(Collection<CatalogModel>), "Catalogs type was not Collection<CatalogModel>");
		}

		[Test]
		public void get_catalogs_filter_name_not_equal_to_null()
		{
			var route = "catalogs?filter=name~ne~null";
			var response = GetModelFromEndpoint<CatalogListResponse>(route);

			Assert.IsNotNull(response.Catalogs, "Catalogs were null");
			Assert.IsTrue(response.Catalogs.GetType() == typeof(Collection<CatalogModel>), "Catalogs type was not Collection<CatalogModel>");
		}

		[Test]
		public void get_catalogs_filter_name_equals_fine_foods_catalog()
		{
			var route = "catalogs?filter=name~eq~fine%20foods%20catalog";
			var response = GetModelFromEndpoint<CatalogListResponse>(route);

			Assert.IsNotNull(response.Catalogs, "Catalogs were null");
			Assert.IsTrue(response.Catalogs.GetType() == typeof(Collection<CatalogModel>), "Catalogs type was not Collection<CatalogModel>");

			foreach (var c in response.Catalogs)
			{
				Assert.AreEqual("fine foods catalog", c.Name.ToLower(), "Expected name 'fine foods catalog', actual name '" + c.Name.ToLower() + "'");
			}
		}

		[Test]
		public void get_catalogs_filter_name_not_equal_to_fine_foods_catalog()
		{
			var route = "catalogs?filter=name~ne~fine%20foods%20catalog";
			var response = GetModelFromEndpoint<CatalogListResponse>(route);

			Assert.IsNotNull(response.Catalogs, "Catalogs were null");
			Assert.IsTrue(response.Catalogs.GetType() == typeof(Collection<CatalogModel>), "Catalogs type was not Collection<CatalogModel>");

			foreach (var c in response.Catalogs)
			{
				Assert.AreNotEqual("fine foods catalog", c.Name.ToLower(), "Expected name not equal to 'fine foods catalog'");
			}
		}

		[Test]
		public void get_catalogs_filter_name_contains_cheese()
		{
			var route = "catalogs?filter=name~cn~cheese";
			var response = GetModelFromEndpoint<CatalogListResponse>(route);

			Assert.IsNotNull(response.Catalogs, "Catalogs were null");
			Assert.IsTrue(response.Catalogs.GetType() == typeof(Collection<CatalogModel>), "Catalogs type was not Collection<CatalogModel>");

			foreach (var c in response.Catalogs)
			{
				Assert.IsTrue(c.Name.ToLower().Contains("cheese"), "Expected name to contain 'cheese', actual name '" + c.Name.ToLower() + "'");
			}
		}

		[Test]
		public void get_catalogs_filter_name_starts_with_wine()
		{
			var route = "catalogs?filter=name~sw~wine";
			var response = GetModelFromEndpoint<CatalogListResponse>(route);

			Assert.IsNotNull(response.Catalogs, "Catalogs were null");
			Assert.IsTrue(response.Catalogs.GetType() == typeof(Collection<CatalogModel>), "Catalogs type was not Collection<CatalogModel>");

			foreach (var c in response.Catalogs)
			{
				Assert.IsTrue(c.Name.ToLower().StartsWith("wine"), "Expected name to start with 'wine', actual name '" + c.Name.ToLower() + "'");
			}
		}

		[Test]
		public void get_catalogs_filter_name_ends_with_catalog()
		{
			var route = "catalogs?filter=name~ew~catalog";
			var response = GetModelFromEndpoint<CatalogListResponse>(route);

			Assert.IsNotNull(response.Catalogs, "Catalogs were null");
			Assert.IsTrue(response.Catalogs.GetType() == typeof(Collection<CatalogModel>), "Catalogs type was not Collection<CatalogModel>");

			foreach (var c in response.Catalogs)
			{
				Assert.IsTrue(c.Name.ToLower().EndsWith("catalog"), "Expected name to end with 'catalog', actual name '" + c.Name.ToLower() + "'");
			}
		}

		[Test]
		public void get_catalogs_page_index_0_size_2()
		{
			var route = "catalogs?page=index~0,size~2";
			var response = GetModelFromEndpoint<CatalogListResponse>(route);

			Assert.IsNotNull(response.Catalogs, "Catalogs were null");
			Assert.IsTrue(response.Catalogs.GetType() == typeof(Collection<CatalogModel>), "Catalogs type was not Collection<CatalogModel>");
			Assert.IsTrue(response.Catalogs.Count == 2, "Expected count equal to 2, actual count " + response.Catalogs.Count);
		}

		[Test]
		public void create_update_delete_catalog()
		{
			// Create
			var route = "catalogs";
			var data = GetDataFromFile("Catalog-Create.json");
			var response = PostModelToEndpoint<CatalogResponse>(route, data);

			Assert.IsNotNull(response.Catalog, "Catalog was null");
			Assert.IsTrue(response.Catalog.CatalogId > 0, "Catalog ID was not greater than 0");
			Assert.IsTrue(response.Catalog.Name == "NewCatalog", "Expected name 'NewCatalog', actual name '" + response.Catalog.Name + "'");

			// Update
			route = "catalogs/" + response.Catalog.CatalogId;
			data = GetDataFromFile("Catalog-Update.json");
			response = PutModelToEndpoint<CatalogResponse>(route, data);

			Assert.IsNotNull(response.Catalog, "Catalog was null");
			Assert.IsTrue(response.Catalog.Name == "UpdatedCatalog", "Expected name 'UpdatedCatalog', actual name '" + response.Catalog.Name + "'");

			// Delete
			route = "catalogs/" + response.Catalog.CatalogId;
			var result = DeleteResourceFromEndpoint(route);

			Assert.IsTrue(result, "Catalog was not deleted");
		}
	}
}
