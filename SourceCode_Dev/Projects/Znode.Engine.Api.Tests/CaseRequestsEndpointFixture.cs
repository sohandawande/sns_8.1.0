﻿using NUnit.Framework;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Engine.Api.Tests
{
	[TestFixture]
	public class CaseRequestsEndpointFixture : BaseFixture
	{
		[Test]
		public void create_update_delete_case_request()
		{
			// Create
			var route = "caserequests";
			var data = GetDataFromFile("CaseRequest-Create.json");
			var response = PostModelToEndpoint<CaseRequestResponse>(route, data);

			Assert.IsNotNull(response.CaseRequest, "CaseRequest was null");
			Assert.IsTrue(response.CaseRequest.CaseRequestId > 0, "CaseRequest ID was not greater than 0");
			Assert.IsTrue(response.CaseRequest.Title == "NewCaseRequest", "Expected title 'NewCaseRequest', actual title '" + response.CaseRequest.Title + "'");

			// Update
			route = "caserequests/" + response.CaseRequest.CaseRequestId;
			data = GetDataFromFile("CaseRequest-Update.json");
			response = PutModelToEndpoint<CaseRequestResponse>(route, data);

			Assert.IsNotNull(response.CaseRequest, "CaseRequest was null");
			Assert.IsTrue(response.CaseRequest.Title == "UpdatedCaseRequest", "Expected title 'UpdatedCaseRequest', actual title '" + response.CaseRequest.Title + "'");

			// Delete
			route = "caserequests/" + response.CaseRequest.CaseRequestId;
			var result = DeleteResourceFromEndpoint(route);

			Assert.IsTrue(result, "CaseRequest was not deleted");
		}
	}
}
