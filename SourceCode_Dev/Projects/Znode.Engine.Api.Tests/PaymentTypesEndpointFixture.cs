﻿using System.Collections.ObjectModel;
using NUnit.Framework;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Engine.Api.Tests
{
	[TestFixture]
	public class PaymentTypesEndpointFixture : BaseFixture
	{
		[Test]
		public void get_payment_type()
		{
			var route = "paymenttypes/1";
			var response = GetModelFromEndpoint<PaymentTypeResponse>(route);

			Assert.IsNotNull(response.PaymentType, "Payment type was null");
			Assert.IsTrue(response.PaymentType.GetType() == typeof(PaymentTypeModel), "PaymentType type was not PaymentTypeModel");
		}

		[Test]
		public void get_payment_types()
		{
			var route = "paymenttypes";
			var response = GetModelFromEndpoint<PaymentTypeListResponse>(route);

			Assert.IsNotNull(response.PaymentTypes, "Payment types were null");
			Assert.IsTrue(response.PaymentTypes.GetType() == typeof(Collection<PaymentTypeModel>), "PaymentTypes type was not Collection<PaymentTypeModel>");
		}

		[Test]
		public void get_payment_types_filter_name_equals_credit_card()
		{
			var route = "paymenttypes?filter=name~eq~credit%20card";
			var response = GetModelFromEndpoint<PaymentTypeListResponse>(route);

			Assert.IsNotNull(response.PaymentTypes, "Payment types were null");
			Assert.IsTrue(response.PaymentTypes.GetType() == typeof(Collection<PaymentTypeModel>), "PaymentTypes type was not Collection<PaymentTypeModel>");

			foreach (var p in response.PaymentTypes)
			{
				Assert.AreEqual("credit card", p.Name.ToLower(), "Expected name 'credit card', actual name '" + p.Name.ToLower() + "'");
			}
		}

		[Test]
		public void get_payment_types_filter_name_not_equal_to_credit_card()
		{
			var route = "paymenttypes?filter=name~ne~credit%20card";
			var response = GetModelFromEndpoint<PaymentTypeListResponse>(route);

			Assert.IsNotNull(response.PaymentTypes, "Payment types were null");
			Assert.IsTrue(response.PaymentTypes.GetType() == typeof(Collection<PaymentTypeModel>), "PaymentTypes type was not Collection<PaymentTypeModel>");

			foreach (var p in response.PaymentTypes)
			{
				Assert.AreNotEqual("credit card", p.Name.ToLower(), "Expected name not equal to 'credit card'");
			}
		}

		[Test]
		public void get_payment_types_filter_name_contains_express()
		{
			var route = "paymenttypes?filter=name~cn~express";
			var response = GetModelFromEndpoint<PaymentTypeListResponse>(route);

			Assert.IsNotNull(response.PaymentTypes, "Payment types were null");
			Assert.IsTrue(response.PaymentTypes.GetType() == typeof(Collection<PaymentTypeModel>), "PaymentTypes type was not Collection<PaymentTypeModel>");

			foreach (var p in response.PaymentTypes)
			{
				Assert.IsTrue(p.Name.ToLower().Contains("express"), "Expected name to contain 'express', actual name '" + p.Name.ToLower() + "'");
			}
		}

		[Test]
		public void get_payment_types_filter_name_starts_with_google()
		{
			var route = "paymenttypes?filter=name~sw~google";
			var response = GetModelFromEndpoint<PaymentTypeListResponse>(route);

			Assert.IsNotNull(response.PaymentTypes, "Payment types were null");
			Assert.IsTrue(response.PaymentTypes.GetType() == typeof(Collection<PaymentTypeModel>), "PaymentTypes type was not Collection<PaymentTypeModel>");

			foreach (var p in response.PaymentTypes)
			{
				Assert.IsTrue(p.Name.ToLower().StartsWith("google"), "Expected name to start with 'google', actual name started with '" + p.Name.ToLower() + "'");
			}
		}

		[Test]
		public void get_payment_types_filter_name_ends_with_checkout()
		{
			var route = "paymenttypes?filter=name~ew~checkout";
			var response = GetModelFromEndpoint<PaymentTypeListResponse>(route);

			Assert.IsNotNull(response.PaymentTypes, "Payment types were null");
			Assert.IsTrue(response.PaymentTypes.GetType() == typeof(Collection<PaymentTypeModel>), "PaymentTypes type was not Collection<PaymentTypeModel>");

			foreach (var p in response.PaymentTypes)
			{
				Assert.IsTrue(p.Name.ToLower().EndsWith("checkout"), "Expected name to end with 'checkout', actual name ended with '" + p.Name.ToLower() + "'");
			}
		}

		[Test]
		public void get_payment_types_page_index_0_size_5()
		{
			var route = "paymenttypes?page=index~0,size~5";
			var response = GetModelFromEndpoint<PaymentTypeListResponse>(route);

			Assert.IsNotNull(response.PaymentTypes, "Payment types were null");
			Assert.IsTrue(response.PaymentTypes.GetType() == typeof(Collection<PaymentTypeModel>), "PaymentTypes type was not Collection<PaymentTypeModel>");
			Assert.IsTrue(response.PaymentTypes.Count == 5, "Expected count equal to 5, actual count " + response.PaymentTypes.Count);
		}

		[Test]
		public void create_update_delete_payment_type()
		{
			// Create
			var route = "paymenttypes";
			var data = GetDataFromFile("PaymentType-Create.json");
			var response = PostModelToEndpoint<PaymentTypeResponse>(route, data);

			Assert.IsNotNull(response.PaymentType, "Payment type was null");
			Assert.IsTrue(response.PaymentType.PaymentTypeId > 0, "Payment type ID was not greater than 0");
			Assert.IsTrue(response.PaymentType.Name == "NewPaymentType", "Expected name 'NewPaymentType', actual name '" + response.PaymentType.Name + "'");

			// Update
			route = "paymenttypes/" + response.PaymentType.PaymentTypeId;
			data = GetDataFromFile("PaymentType-Update.json");
			response = PutModelToEndpoint<PaymentTypeResponse>(route, data);

			Assert.IsNotNull(response.PaymentType, "Payment type was null");
			Assert.IsTrue(response.PaymentType.Name == "UpdatedPaymentType", "Expected name 'UpdatedPaymentType', actual name '" + response.PaymentType.Name + "'");

			// Delete
			route = "paymenttypes/" + response.PaymentType.PaymentTypeId;
			var result = DeleteResourceFromEndpoint(route);

			Assert.IsTrue(result, "Payment type was not deleted");
		}
	}
}
