﻿using System.Collections.ObjectModel;
using System.Linq;
using NUnit.Framework;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Engine.Api.Tests
{
	[TestFixture]
	public class ProductsEndpointFixture : BaseFixture
	{
		[Test]
		public void get_product()
		{
			var route = "products/302";
			var response = GetModelFromEndpoint<ProductResponse>(route);

			Assert.IsNotNull(response.Product, "Product was null");
			Assert.IsTrue(response.Product.GetType() == typeof(ProductModel), "Product type was not ProductModel");
		}

		[Test]
		public void get_product_expand_addons()
		{
			var route = "products/302?expand=addons";
			var response = GetModelFromEndpoint<ProductResponse>(route);

			Assert.IsNotNull(response.Product.AddOns, "Addons were null");
			Assert.IsNotEmpty(response.Product.AddOns, "Addons were empty");
			Assert.IsTrue(response.Product.AddOns.GetType() == typeof(Collection<AddOnModel>), "Addons type was not Collection<AddOnModel>");
		}

		[Test]
		public void get_product_expand_attributes()
		{
			var route = "products/302?expand=attributes";
			var response = GetModelFromEndpoint<ProductResponse>(route);

			Assert.IsTrue(response.Product.Attributes.Count == response.Product.Attributes.Distinct().Count(), "Attribute entries are not unique");
		}

		[Test]
		public void get_product_expand_categories()
		{
			var route = "products/302?expand=categories";
			var response = GetModelFromEndpoint<ProductResponse>(route);

			Assert.IsNotNull(response.Product.Categories, "Categories were null");
			Assert.IsNotEmpty(response.Product.Categories, "Categories were empty");
			Assert.IsTrue(response.Product.Categories.GetType() == typeof(Collection<CategoryModel>), "Categories type was not Collection<CategoryModel>");
		}

		[Test]
		public void get_product_expand_crosssells()
		{
			var route = "products/303?expand=crosssells";
			;
			var response = GetModelFromEndpoint<ProductResponse>(route);

			Assert.IsNotNull(response.Product.CrossSells, "Cross sells were null");
			Assert.IsNotEmpty(response.Product.CrossSells, "Cross sells were empty");
			Assert.IsTrue(response.Product.CrossSells[0].ProductRelation == ProductRelationCode.FrequentlyBoughtTogether, "ProductRelationCode Enum was not correct");
			Assert.IsTrue(response.Product.CrossSells.GetType() == typeof(Collection<CrossSellModel>), "Cross sells type was not Collection<CrossSellModel>");
		}

		[Test]
		public void get_product_home_specials()
		{
			var route = "products/gethomespecials";
			var response = GetModelFromEndpoint<ProductListResponse>(route);

			Assert.IsNotNull(response.Products, "home specials  were null");
			Assert.IsNotEmpty(response.Products, "home specials were empty");

		}

		[Test]
		public void get_product_expand_frequentlyboughttogether()
		{
			var route = "products/303?expand=frequentlyboughttogether";
			var response = GetModelFromEndpoint<ProductResponse>(route);

			Assert.IsNotNull(response.Product.FrequentlyBoughtTogether, "FrequentlyBoughtTogether  were null");
			Assert.IsNotEmpty(response.Product.FrequentlyBoughtTogether, "FrequentlyBoughtTogether were empty");
			Assert.IsTrue(response.Product.FrequentlyBoughtTogether[0].ProductRelation == ProductRelationCode.FrequentlyBoughtTogether, "ProductRelationCode Enum was not correct");
			Assert.IsTrue(response.Product.FrequentlyBoughtTogether.GetType() == typeof(Collection<CrossSellModel>), "FrequentlyBoughtTogether type was not Collection<CrossSellModel>");
		}

		[Test]
		public void get_product_expand_youmayalsolike()
		{
			var route = "products/303?expand=youmayalsolike";
			var response = GetModelFromEndpoint<ProductResponse>(route);

			Assert.IsNotNull(response.Product.YouMayAlsoLike, "YouMayAlsoLike  were null");
			Assert.IsNotEmpty(response.Product.YouMayAlsoLike, "YouMayAlsoLike were empty");
			Assert.IsTrue(response.Product.YouMayAlsoLike[0].ProductRelation == ProductRelationCode.YouMayAlsoLike, "ProductRelationCode Enum was not correct");
			Assert.IsTrue(response.Product.YouMayAlsoLike.GetType() == typeof(Collection<CrossSellModel>), "YouMayAlsoLike type was not Collection<CrossSellModel>");
		}

		[Test]
		public void get_product_expand_highlights()
		{
			var route = "products/302?expand=highlights";
			var response = GetModelFromEndpoint<ProductResponse>(route);

			Assert.IsNotNull(response.Product.Highlights, "Highlights were null");
			Assert.IsNotEmpty(response.Product.Highlights, "Hghlights were empty");
			Assert.IsTrue(response.Product.Highlights.GetType() == typeof(Collection<HighlightModel>), "Highlights type was not Collection<HighlightModel>");
		}

		[Test]
		public void get_product_expand_images()
		{
			var route = "products/302?expand=images";
			var response = GetModelFromEndpoint<ProductResponse>(route);

			Assert.IsNotNull(response.Product.Images, "Images were null");
			Assert.IsNotEmpty(response.Product.Images, "Images were empty");
			Assert.IsTrue(response.Product.Images.GetType() == typeof(Collection<ImageModel>), "Images type was not Collection<ImageModel>");
		}

		[Test]
		public void get_product_expand_manufacturer()
		{
			var route = "products/302?expand=manufacturer";
			var response = GetModelFromEndpoint<ProductResponse>(route);

			Assert.IsNotNull(response.Product.Manufacturer, "Manufacturer was null");
			Assert.IsTrue(response.Product.Manufacturer.GetType() == typeof(ManufacturerModel), "Manufacturer type was not ManufacturerModel");
		}

		[Test]
		public void get_product_expand_producttype()
		{
			var route = "products/302?expand=producttype";
			var response = GetModelFromEndpoint<ProductResponse>(route);

			Assert.IsNotNull(response.Product.ProductType, "Product type was null");
			Assert.IsTrue(response.Product.ProductType.GetType() == typeof(ProductTypeModel), "Product type was not ProductTypeModel");
		}

		[Test]
		public void get_product_expand_promotions()
		{
			var route = "products/552?expand=promotions";
			var response = GetModelFromEndpoint<ProductResponse>(route);

			Assert.IsNotNull(response.Product.Promotions, "Promotions were null");
			Assert.IsNotEmpty(response.Product.Promotions, "Promotions were empty");
			Assert.IsTrue(response.Product.Promotions.GetType() == typeof(Collection<PromotionModel>), "Promotions type was not Collection<PromotionModel>");
		}

		[Test]
		public void get_product_expand_reviews()
		{
			var route = "products/303?expand=reviews";
			var response = GetModelFromEndpoint<ProductResponse>(route);

			Assert.IsNotNull(response.Product.Reviews, "Reviews were null");
			Assert.IsNotEmpty(response.Product.Reviews, "Reviews were empty");
			Assert.IsTrue(response.Product.Reviews.GetType() == typeof(Collection<ReviewModel>), "Reviews type was not Collection<ReviewModel>");
		}

		[Test]
		public void get_product_expand_skus()
		{
			var route = "products/302?expand=skus";
			var response = GetModelFromEndpoint<ProductResponse>(route);

			Assert.IsNotNull(response.Product.Skus, "Skus were null");
			Assert.IsNotEmpty(response.Product.Skus, "Skus were empty");
			Assert.IsTrue(response.Product.Skus.GetType() == typeof(Collection<SkuModel>), "Skus type was not Collection<SkuModel>");
		}

		[Test]
		public void get_product_expand_tiers()
		{
			var route = "products/302?expand=tiers";
			var response = GetModelFromEndpoint<ProductResponse>(route);

			Assert.IsNotNull(response.Product.Tiers, "Tiers were null");
			Assert.IsNotEmpty(response.Product.Tiers, "Tiers were empty");
			Assert.IsTrue(response.Product.Tiers.GetType() == typeof(Collection<TierModel>), "Tiers type was not Collection<TierModel>");
		}



		[Test]
		public void get_products()
		{
			var route = "products";
			var response = GetModelFromEndpoint<ProductListResponse>(route);

			Assert.IsNotNull(response.Products, "Products were null");
			Assert.IsTrue(response.Products.GetType() == typeof(Collection<ProductModel>), "Products type was not Collection<ProductModel>");
		}

		[Test]
		public void get_products_by_catalog()
		{
			var route = "products/catalog/1";
			var response = GetModelFromEndpoint<ProductListResponse>(route);

			Assert.IsNotNull(response.Products, "Products were null");
			Assert.IsTrue(response.Products.GetType() == typeof(Collection<ProductModel>), "Products type was not Collection<ProductModel>");
		}

		[Test]
		public void get_products_by_category()
		{
			var route = "products/category/81";
			var response = GetModelFromEndpoint<ProductListResponse>(route);

			Assert.IsNotNull(response.Products, "Products were null");
			Assert.IsTrue(response.Products.GetType() == typeof(Collection<ProductModel>), "Products type was not Collection<ProductModel>");
		}

		[Test]
		public void get_products_by_external_ids()
		{
			var route = "products/externalids/2bdb41c4-fd4f-42ce-9288-b9a39a923db1,2393678f-84fa-4273-b1cd-c4aee87443f1";
			var response = GetModelFromEndpoint<ProductListResponse>(route);

			Assert.IsNotNull(response.Products, "Products were null");
			Assert.IsTrue(response.Products.GetType() == typeof(Collection<ProductModel>), "Products type was not Collection<ProductModel>");

			Assert.AreEqual("2bdb41c4-fd4f-42ce-9288-b9a39a923db1", response.Products[0].ExternalId, "Expected product ID 2bdb41c4-fd4f-42ce-9288-b9a39a923db1, actual product ID " + response.Products[0].ExternalId);
			Assert.AreEqual("2393678f-84fa-4273-b1cd-c4aee87443f1", response.Products[1].ExternalId, "Expected product ID 2393678f-84fa-4273-b1cd-c4aee87443f1, actual product ID " + response.Products[1].ExternalId);
		}

		[Test]
		public void get_products_by_homespecials()
		{
			var route = "products/homespecials";
			var response = GetModelFromEndpoint<ProductListResponse>(route);

			Assert.IsNotNull(response.Products, "Products were null");
			Assert.IsTrue(response.Products.GetType() == typeof(Collection<ProductModel>), "Products type was not Collection<ProductModel>");
		}

		[Test]
		public void get_products_by_product_ids()
		{
			var route = "products/302,303,304";
			var response = GetModelFromEndpoint<ProductListResponse>(route);

			Assert.IsNotNull(response.Products, "Products were null");
			Assert.IsTrue(response.Products.GetType() == typeof(Collection<ProductModel>), "Products type was not Collection<ProductModel>");

			Assert.AreEqual(302, response.Products[0].ProductId, "Expected product ID 302, actual product ID " + response.Products[0].ProductId);
			Assert.AreEqual(303, response.Products[1].ProductId, "Expected product ID 303, actual product ID " + response.Products[1].ProductId);
			Assert.AreEqual(304, response.Products[2].ProductId, "Expected product ID 304, actual product ID " + response.Products[2].ProductId);
		}

		[Test]
		public void get_products_filter_externalid_equals_null()
		{
			var route = "products?filter=externalid~eq~null";
			var response = GetModelFromEndpoint<ProductListResponse>(route);

			Assert.IsNotNull(response.Products, "Products were null");
			Assert.IsTrue(response.Products.GetType() == typeof(Collection<ProductModel>), "Products type was not Collection<ProductModel>");
		}

		[Test]
		public void get_products_filter_externalid_not_equal_to_null()
		{
			var route = "products?filter=externalid~ne~null";
			var response = GetModelFromEndpoint<ProductListResponse>(route);

			Assert.IsNotNull(response.Products, "Products were null");
			Assert.IsTrue(response.Products.GetType() == typeof(Collection<ProductModel>), "Products type was not Collection<ProductModel>");
		}

		[Test]
		public void get_products_filter_name_equals_pear()
		{
			var route = "products?filter=name~eq~pear";
			var response = GetModelFromEndpoint<ProductListResponse>(route);

			Assert.IsNotNull(response.Products, "Products were null");
			Assert.IsTrue(response.Products.GetType() == typeof(Collection<ProductModel>), "Products type was not Collection<ProductModel>");

			foreach (var p in response.Products)
			{
				Assert.AreEqual("pear", p.Name.ToLower(), "Expected product name 'pear', actual product name '" + p.Name.ToLower() + "'");
			}
		}

		[Test]
		public void get_products_filter_name_not_equal_to_apple()
		{
			var route = "products?filter=name~ne~apple";
			var response = GetModelFromEndpoint<ProductListResponse>(route);

			Assert.IsNotNull(response.Products, "Products were null");
			Assert.IsTrue(response.Products.GetType() == typeof(Collection<ProductModel>), "Products type was not Collection<ProductModel>");

			foreach (var p in response.Products)
			{
				Assert.AreNotEqual("apple", p.Name.ToLower(), "Expected product name not equal to 'apple'");
			}
		}

		[Test]
		public void get_products_filter_name_contains_apple()
		{
			var route = "products?filter=name~cn~apple";
			var response = GetModelFromEndpoint<ProductListResponse>(route);

			Assert.IsNotNull(response.Products, "Products were null");
			Assert.IsTrue(response.Products.GetType() == typeof(Collection<ProductModel>), "Products type was not Collection<ProductModel>");

			foreach (var p in response.Products)
			{
				Assert.IsTrue(p.Name.ToLower().Contains("apple"), "Expected product name to contain 'apple', actual product name '" + p.Name.ToLower() + "'");
			}
		}

		[Test]
		public void get_products_filter_retailprice_greater_than_2_99()
		{
			var route = "products?filter=retailprice~gt~2.99";
			var response = GetModelFromEndpoint<ProductListResponse>(route);

			Assert.IsNotNull(response.Products, "Products were null");
			Assert.IsTrue(response.Products.GetType() == typeof(Collection<ProductModel>), "Products type was not Collection<ProductModel>");

			foreach (var p in response.Products)
			{
				Assert.IsTrue(p.RetailPrice > 2.99m, "Expected retail price greater than 2.99, actual retail price " + p.RetailPrice);
			}
		}

		[Test]
		public void get_products_filter_retailprice_greater_than_or_equal_to_2_99()
		{
			var route = "products?filter=retailprice~ge~2.99";
			var response = GetModelFromEndpoint<ProductListResponse>(route);

			Assert.IsNotNull(response.Products, "Products were null");
			Assert.IsTrue(response.Products.GetType() == typeof(Collection<ProductModel>), "Products type was not Collection<ProductModel>");

			foreach (var p in response.Products)
			{
				Assert.IsTrue(p.RetailPrice >= 2.99m, "Expected retail price greater than or equal to 2.99, actual retail price " + p.RetailPrice);
			}
		}

		[Test]
		public void get_products_filter_retailprice_less_than_2_99()
		{
			var route = "products?filter=retailprice~lt~2.99";
			var response = GetModelFromEndpoint<ProductListResponse>(route);

			Assert.IsNotNull(response.Products, "Products were null");
			Assert.IsTrue(response.Products.GetType() == typeof(Collection<ProductModel>), "Products type was not Collection<ProductModel>");

			foreach (var p in response.Products)
			{
				Assert.IsTrue(p.RetailPrice < 2.99m, "Expected retail price less than 2.99, actual retail price " + p.RetailPrice);
			}
		}

		[Test]
		public void get_products_filter_retailprice_less_than_or_equal_to_2_99()
		{
			var route = "products?filter=retailprice~le~2.99";
			var response = GetModelFromEndpoint<ProductListResponse>(route);

			Assert.IsNotNull(response.Products, "Products were null");
			Assert.IsTrue(response.Products.GetType() == typeof(Collection<ProductModel>), "Products type was not Collection<ProductModel>");

			foreach (var p in response.Products)
			{
				Assert.IsTrue(p.RetailPrice <= 2.99m, "Expected retail price less than or equal to 2.99, actual retail price " + p.RetailPrice);
			}
		}

		[Test]
		public void get_products_filter_name_starts_with_straw()
		{
			var route = "products?filter=name~sw~straw";
			var response = GetModelFromEndpoint<ProductListResponse>(route);

			Assert.IsNotNull(response.Products, "Products were null");
			Assert.IsTrue(response.Products.GetType() == typeof(Collection<ProductModel>), "Products type was not Collection<ProductModel>");

			foreach (var p in response.Products)
			{
				Assert.IsTrue(p.Name.ToLower().StartsWith("straw"), "Expected product name to start with 'straw', actual product name '" + p.Name.ToLower() + "'");
			}
		}

		[Test]
		public void get_products_filter_name_ends_with_berry()
		{
			var route = "products?filter=name~ew~berry";
			var response = GetModelFromEndpoint<ProductListResponse>(route);

			Assert.IsNotNull(response.Products, "Products were null");
			Assert.IsTrue(response.Products.GetType() == typeof(Collection<ProductModel>), "Products type was not Collection<ProductModel>");

			foreach (var p in response.Products)
			{
				Assert.IsTrue(p.Name.ToLower().EndsWith("berry"), "Expected product name to end with 'berry', actual product name '" + p.Name.ToLower() + "'");
			}
		}

		[Test]
		public void get_products_page_index_0_size_5()
		{
			var route = "products?page=index~0,size~5";
			var response = GetModelFromEndpoint<ProductListResponse>(route);

			Assert.IsNotNull(response.Products, "Products were null");
			Assert.IsTrue(response.Products.GetType() == typeof(Collection<ProductModel>), "Products type was not Collection<ProductModel>");
			Assert.IsTrue(response.Products.Count == 5, "Expected count equal to 5, actual count " + response.Products.Count);
		}

		[Test]
		public void create_update_delete_product()
		{
			// Create
			var route = "products";
			var data = GetDataFromFile("Product-Create.json");
			var response = PostModelToEndpoint<ProductResponse>(route, data);

			Assert.IsNotNull(response.Product, "Product was null");
			Assert.IsTrue(response.Product.ProductId > 0, "Product ID was not greater than 0");
			Assert.IsTrue(response.Product.Name == "NewProduct", "Expected name 'NewProduct', actual name '" + response.Product.Name + "'");

			// Update
			route = "products/" + response.Product.ProductId;
			data = GetDataFromFile("Product-Update.json");
			response = PutModelToEndpoint<ProductResponse>(route, data);

			Assert.IsNotNull(response.Product, "Product was null");
			Assert.IsTrue(response.Product.Name == "UpdatedProduct", "Expected name 'UpdatedProduct', actual name '" + response.Product.Name + "'");

			// Delete
			route = "products/" + response.Product.ProductId;
			var result = DeleteResourceFromEndpoint(route);

			Assert.IsTrue(result, "Product was not deleted");
		}
	}
}
