﻿using System.Collections.ObjectModel;
using NUnit.Framework;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Engine.Api.Tests
{
	[TestFixture]
	public class DomainsEndpointFixture : BaseFixture
	{
		[Test]
		public void get_domain()
		{
			var route = "domains/1";
			var response = GetModelFromEndpoint<DomainResponse>(route);

			Assert.IsNotNull(response.Domain, "Domain was null");
			Assert.IsTrue(response.Domain.GetType() == typeof(DomainModel), "Domain type was not DomainModel");
		}

		[Test]
		public void get_domains()
		{
			var route = "domains";
			var response = GetModelFromEndpoint<DomainListResponse>(route);

			Assert.IsNotNull(response.Domains, "Domains were null");
			Assert.IsTrue(response.Domains.GetType() == typeof(Collection<DomainModel>), "Domains type was not Collection<DomainModel>");
		}

		[Test]
		public void get_domains_filter_domainname_equals_localhost()
		{
			var route = "domains?filter=domainname~eq~localhost";
			var response = GetModelFromEndpoint<DomainListResponse>(route);

			Assert.IsNotNull(response.Domains, "Domains were null");
			Assert.IsTrue(response.Domains.GetType() == typeof(Collection<DomainModel>), "Domains type was not Collection<DomainModel>");

			foreach (var d in response.Domains)
			{
				Assert.AreEqual("localhost", d.DomainName.ToLower(), "Expected domain name 'localhost', actual domain name '" + d.DomainName.ToLower() + "'");
			}
		}

		[Test]
		public void get_domains_filter_domainname_not_equal_to_localhost()
		{
			var route = "domains?filter=domainname~ne~localhost";
			var response = GetModelFromEndpoint<DomainListResponse>(route);

			Assert.IsNotNull(response.Domains, "Domains were null");
			Assert.IsTrue(response.Domains.GetType() == typeof(Collection<DomainModel>), "Domains type was not Collection<DomainModel>");

			foreach (var d in response.Domains)
			{
				Assert.AreNotEqual("localhost", d.DomainName.ToLower(), "Expected domain name not equal to 'localhost'");
			}
		}

		[Test]
		public void get_domains_filter_domainname_contains_localhost()
		{
			var route = "domains?filter=domainname~cn~localhost";
			var response = GetModelFromEndpoint<DomainListResponse>(route);

			Assert.IsNotNull(response.Domains, "Domains were null");
			Assert.IsTrue(response.Domains.GetType() == typeof(Collection<DomainModel>), "Domains type was not Collection<DomainModel>");

			foreach (var d in response.Domains)
			{
				Assert.IsTrue(d.DomainName.ToLower().Contains("localhost"), "Expected domain name to contain 'localhost', actual domain name '" + d.DomainName.ToLower() + "'");
			}
		}

		[Test]
		public void get_domains_filter_domainname_starts_with_localhost()
		{
			var route = "domains?filter=domainname~sw~localhost";
			var response = GetModelFromEndpoint<DomainListResponse>(route);

			Assert.IsNotNull(response.Domains, "Domains were null");
			Assert.IsTrue(response.Domains.GetType() == typeof(Collection<DomainModel>), "Domains type was not Collection<DomainModel>");

			foreach (var d in response.Domains)
			{
				Assert.IsTrue(d.DomainName.ToLower().StartsWith("localhost"), "Expected domain name to start with 'localhost', actual domain name '" + d.DomainName.ToLower() + "'");
			}
		}

		[Test]
		public void get_domains_filter_domainname_ends_with_web()
		{
			var route = "domains?filter=domainname~ew~web";
			var response = GetModelFromEndpoint<DomainListResponse>(route);

			Assert.IsNotNull(response.Domains, "Domains were null");
			Assert.IsTrue(response.Domains.GetType() == typeof(Collection<DomainModel>), "Domains type was not Collection<DomainModel>");

			foreach (var d in response.Domains)
			{
				Assert.IsTrue(d.DomainName.ToLower().EndsWith("web"), "Expected domain name to end with 'web', actual domain name '" + d.DomainName.ToLower() + "'");
			}
		}

		[Test]
		public void get_domains_page_index_0_size_2()
		{
			var route = "domains?page=index~0,size~2";
			var response = GetModelFromEndpoint<DomainListResponse>(route);

			Assert.IsNotNull(response.Domains, "Domains were null");
			Assert.IsTrue(response.Domains.GetType() == typeof(Collection<DomainModel>), "Domains type was not Collection<DomainModel>");
			Assert.IsTrue(response.Domains.Count == 2, "Expected count equal to 2, actual count " + response.Domains.Count);
		}

		[Test]
		public void create_update_delete_domain()
		{
			// Create
			var route = "domains";
			var data = GetDataFromFile("Domain-Create.json");
			var response = PostModelToEndpoint<DomainResponse>(route, data);

			Assert.IsNotNull(response.Domain, "Domain was null");
			Assert.IsTrue(response.Domain.DomainId > 0, "Domain ID was not greater than 0");
			Assert.IsTrue(response.Domain.DomainName == "NewDomain", "Expected domain name 'NewDomain', actual domain name '" + response.Domain.DomainName + "'");

			// Update
			route = "domains/" + response.Domain.DomainId;
			data = GetDataFromFile("Domain-Update.json");
			response = PutModelToEndpoint<DomainResponse>(route, data);

			Assert.IsNotNull(response.Domain, "Domain was null");
			Assert.IsTrue(response.Domain.DomainName == "UpdatedDomain", "Expected domain name 'UpdatedDomain', actual domain name '" + response.Domain.DomainName + "'");

			// Delete
			route = "domains/" + response.Domain.DomainId;
			var result = DeleteResourceFromEndpoint(route);

			Assert.IsTrue(result, "Domain was not deleted");
		}
	}
}
