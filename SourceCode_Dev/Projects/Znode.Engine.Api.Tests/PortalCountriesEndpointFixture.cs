﻿using System.Collections.ObjectModel;
using NUnit.Framework;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Engine.Api.Tests
{
	[TestFixture]
	public class PortalCountriesEndpointFixture : BaseFixture
	{
		[Test]
		public void get_portalcountry()
		{
			var route = "portalcountries/5";
			var response = GetModelFromEndpoint<PortalCountryResponse>(route);

			Assert.IsNotNull(response.PortalCountry, "Portal country was null");
			Assert.IsTrue(response.PortalCountry.GetType() == typeof(PortalCountryModel), "Portal country type was not PortalCountryModel");
		}

		[Test]
		public void get_portalcountry_expand_country()
		{
			var route = "portalcountries/5?expand=country";
			var response = GetModelFromEndpoint<PortalCountryResponse>(route);

			Assert.IsNotNull(response.PortalCountry.Country, "PortalCountry country was null");
			Assert.IsTrue(response.PortalCountry.Country.GetType() == typeof(CountryModel), "PortalCountry country type was not CountryModel");
		}

		[Test]
		public void get_portalcountries()
		{
			var route = "portalcountries";
			var response = GetModelFromEndpoint<PortalCountryListResponse>(route);

			Assert.IsNotNull(response.PortalCountries, "Portal countries were null");
			Assert.IsTrue(response.PortalCountries.GetType() == typeof(Collection<PortalCountryModel>), "PortalCountries type was not Collection<PortalCountryModel>");
		}

		[Test]
		public void get_portalcountries_filter_countrycode_equals_us()
		{
			var route = "portalcountries?filter=countrycode~eq~us";
			var response = GetModelFromEndpoint<PortalCountryListResponse>(route);

			Assert.IsNotNull(response.PortalCountries, "Portal countries were null");
			Assert.IsTrue(response.PortalCountries.GetType() == typeof(Collection<PortalCountryModel>), "PortalCountries type was not Collection<PortalCountryModel>");

			foreach (var p in response.PortalCountries)
			{
				Assert.AreEqual("us", p.CountryCode.ToLower(), "Expected country code 'us', actual country code '" + p.CountryCode.ToLower() + "'");
			}
		}

		[Test]
		public void get_portalcountries_filter_portalid_not_equal_to_1()
		{
			var route = "portalcountries?filter=portalid~ne~1";
			var response = GetModelFromEndpoint<PortalCountryListResponse>(route);

			Assert.IsNotNull(response.PortalCountries, "Portal countries were null");
			Assert.IsTrue(response.PortalCountries.GetType() == typeof(Collection<PortalCountryModel>), "PortalCountries type was not Collection<PortalCountryModel>");

			foreach (var p in response.PortalCountries)
			{
				Assert.AreNotEqual(1, p.PortalId, "Expected portal ID not equal to 1");
			}
		}

		[Test]
		public void get_portalcountries_page_index_0_size_2()
		{
			var route = "portalcountries?page=index~0,size~2";
			var response = GetModelFromEndpoint<PortalCountryListResponse>(route);

			Assert.IsNotNull(response.PortalCountries, "Portal countries were null");
			Assert.IsTrue(response.PortalCountries.GetType() == typeof(Collection<PortalCountryModel>), "PortalCountries type was not Collection<PortalCountryModel>");
			Assert.IsTrue(response.PortalCountries.Count == 2, "Expected count equal to 2, actual count " + response.PortalCountries.Count);
		}

		[Test]
		public void create_update_delete_portalcountry()
		{
			// Create
			var route = "portalcountries";
			var data = GetDataFromFile("PortalCountry-Create.json");
			var response = PostModelToEndpoint<PortalCountryResponse>(route, data);

			Assert.IsNotNull(response.PortalCountry, "Portal country was null");
			Assert.IsTrue(response.PortalCountry.PortalCountryId > 0, "PortalCountry ID was not greater than 0");
			Assert.IsTrue(response.PortalCountry.IsBillingActive, "Expected is billing active true, actual is billing active " + response.PortalCountry.IsBillingActive);

			// Update
			route = "portalcountries/" + response.PortalCountry.PortalCountryId;
			data = GetDataFromFile("PortalCountry-Update.json");
			response = PutModelToEndpoint<PortalCountryResponse>(route, data);

			Assert.IsNotNull(response.PortalCountry, "Portal country was null");
			Assert.IsFalse(response.PortalCountry.IsBillingActive, "Expected is billing active false, actual is billing active " + response.PortalCountry.IsBillingActive);

			// Delete
			route = "portalcountries/" + response.PortalCountry.PortalCountryId;
			var result = DeleteResourceFromEndpoint(route);

			Assert.IsTrue(result, "Portal country was not deleted");
		}
	}
}
