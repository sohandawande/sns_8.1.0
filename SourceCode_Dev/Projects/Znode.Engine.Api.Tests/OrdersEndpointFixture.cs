﻿using System.Collections.ObjectModel;
using NUnit.Framework;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Engine.Api.Tests
{
	[TestFixture]
	public class OrdersEndpointFixture : BaseFixture
	{
		[Test]
		public void get_order()
		{
			var route = "orders/2";
			var response = GetModelFromEndpoint<OrderResponse>(route);

			Assert.IsNotNull(response.Order, "Order was null");
			Assert.IsTrue(response.Order.GetType() == typeof(OrderModel), "Order type was not OrderModel");
		}

		[Test]
		public void get_order_expand_account()
		{
			var route = "orders/2?expand=account";
			var response = GetModelFromEndpoint<OrderResponse>(route);

			Assert.IsNotNull(response.Order.Account, "Account option was null");
			Assert.IsTrue(response.Order.Account.GetType() == typeof(AccountModel), "Account type was not AccountModel");
		}

		[Test]
		public void get_order_expand_orderlineitems()
		{
			var route = "orders/2?expand=orderlineitems";
			var response = GetModelFromEndpoint<OrderResponse>(route);

			Assert.IsNotNull(response.Order.OrderLineItems, "Order line items were null");
			Assert.IsNotEmpty(response.Order.OrderLineItems, "Order line items were empty");
			Assert.IsTrue(response.Order.OrderLineItems.GetType() == typeof(Collection<OrderLineItemModel>), "Addons type was not Collection<OrderLineItemModel>");
		}

		[Test]
		public void get_order_expand_paymentoption()
		{
			var route = "orders/2?expand=paymentoption";
			var response = GetModelFromEndpoint<OrderResponse>(route);

			Assert.IsNotNull(response.Order.PaymentOption, "Payment option was null");
			Assert.IsTrue(response.Order.PaymentOption.GetType() == typeof(PaymentOptionModel), "Payment option type was not PaymentOptionModel");
		}

		[Test]
		public void get_order_expand_paymenttype()
		{
			var route = "orders/2?expand=paymenttype";
			var response = GetModelFromEndpoint<OrderResponse>(route);

			Assert.IsNotNull(response.Order.PaymentType, "Payment type was null");
			Assert.IsTrue(response.Order.PaymentType.GetType() == typeof(PaymentTypeModel), "PaymentType type was not PaymentTypeModel");
		}

		[Test]
		public void get_order_expand_shippingoption()
		{
			var route = "orders/2?expand=shippingoption";
			var response = GetModelFromEndpoint<OrderResponse>(route);

			Assert.IsNotNull(response.Order.ShippingOption, "Shipping option was null");
			Assert.IsTrue(response.Order.ShippingOption.GetType() == typeof(ShippingOptionModel), "Shipping option type was not ShippingOptionModel");
		}

		[Test]
		public void get_orders()
		{
			var route = "orders";
			var response = GetModelFromEndpoint<OrderListResponse>(route);

			Assert.IsNotNull(response.Orders, "Orders were null");
			Assert.IsTrue(response.Orders.GetType() == typeof(Collection<OrderModel>), "Orders type was not Collection<OrderModel>");
		}

		[Test]
		public void create_order_pay_with_purchaseorder_anonymous()
		{
			var route = "orders";
			var data = GetDataFromFile("Order-Create-PurchaseOrder-Anonymous.json");
			var response = PostModelToEndpoint<OrderResponse>(route, data);

			Assert.IsNotNull(response.Order, "Order was null");
			Assert.IsTrue(response.Order.OrderId > 0, "Order ID was not greater than 0");
			Assert.IsTrue(response.Order.AdditionalInstructions == "TestOrderWithPurchaseOrderAnonymous", "Expected additional instructions 'TestOrderWithPurchaseOrderAnonymous', actual additional instructions '" + response.Order.AdditionalInstructions + "'");
			Assert.IsTrue(response.Order.OrderLineItems.Count == 2, "Expected number of order line items to be 2, actual number of order line items " + response.Order.OrderLineItems.Count);
		}
       
		[Test]
		public void create_order_pay_with_purchaseorder_registered()
		{
			var route = "orders";
			var data = GetDataFromFile("Order-Create-PurchaseOrder-Registered.json");
			var response = PostModelToEndpoint<OrderResponse>(route, data);

			Assert.IsNotNull(response.Order, "Order was null");
			Assert.IsTrue(response.Order.OrderId > 0, "Order ID was not greater than 0");
			Assert.IsTrue(response.Order.AdditionalInstructions == "TestOrderWithPurchaseOrderRegistered", "Expected additional instructions 'TestOrderWithPurchaseOrderRegistered', actual additional instructions '" + response.Order.AdditionalInstructions + "'");
			Assert.IsTrue(response.Order.OrderLineItems.Count == 2, "Expected number of order line items to be 2, actual number of order line items " + response.Order.OrderLineItems.Count);
            Assert.IsTrue(response.Order.AdditionalInstructions == "TestOrderWithPurchaseOrderRegistered", "Expected number of order line items to be 2, actual number of order line items " + response.Order.OrderLineItems.Count);
		}

        [Test]
        public void update_order_pay_with_purchaseorder_registered()
        {
            var route = "orders/219";
            var data = GetDataFromFile("Order-Update-PurchaseOrder-Anonymous.json");
            var response = PutModelToEndpoint<OrderResponse>(route, data);

            Assert.IsNotNull(response.Order, "Order was null");
            Assert.IsTrue(response.Order.OrderId > 0, "Order ID was not greater than 0");
        
        }

        [Test]
        public void create_order_pay_with_purchaseorder_registered_multiple_shipto_enabled()
        {
            var route = "orders";
			var data = GetDataFromFile("Order-Create-PurchaseOrder-Registered-Multiple-ShipTo.json");
            var response = PostModelToEndpoint<OrderResponse>(route, data);

            Assert.IsNotNull(response.Order, "Order was null");
            Assert.IsTrue(response.Order.OrderId > 0, "Order ID was not greater than 0");
			Assert.IsTrue(response.Order.AdditionalInstructions == "TestOrderWithPurchaseOrderMultipleShipTo", "Expected additional instructions 'TestOrderWithPurchaseOrderMultipleShipTo', actual additional instructions '" + response.Order.AdditionalInstructions + "'");
            Assert.IsTrue(response.Order.OrderLineItems.Count == 2, "Expected number of order line items to be 2, actual number of order line items " + response.Order.OrderLineItems.Count);
        }
	}
}
