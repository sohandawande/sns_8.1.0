﻿using System.Collections.ObjectModel;
using NUnit.Framework;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Engine.Api.Tests
{
	[TestFixture]
	public class AuditsEndpointFixture : BaseFixture
	{
		[Test]
		public void get_audit()
		{
			var route = "audits/1";
			var response = GetModelFromEndpoint<AuditResponse>(route);

			Assert.IsNotNull(response.Audit, "Audit was null");
			Assert.IsTrue(response.Audit.GetType() == typeof(AuditModel), "Audit type was not AuditModel");
		}

		[Test]
		public void get_audit_expand_audit_source_type()
		{
			var route = "audits/1?expand=auditsourcetype";
			var response = GetModelFromEndpoint<AuditResponse>(route);

			Assert.IsNotNull(response.Audit.AuditSourceType, "Audit source type was null");
			Assert.IsTrue(response.Audit.AuditSourceType.GetType() == typeof(AuditSourceTypeModel), "Audit source type type was not AuditSourceTypeModel");
		}

		[Test]
		public void get_audit_expand_audit_status()
		{
			var route = "audits/1?expand=auditstatus";
			var response = GetModelFromEndpoint<AuditResponse>(route);

			Assert.IsNotNull(response.Audit.AuditStatus, "Audit status was null");
			Assert.IsTrue(response.Audit.AuditStatus.GetType() == typeof(AuditStatusModel), "Audit status type was not AuditStatusModel");
		}

		[Test]
		public void get_audit_expand_audit_type()
		{
			var route = "audits/1?expand=audittype";
			var response = GetModelFromEndpoint<AuditResponse>(route);

			Assert.IsNotNull(response.Audit.AuditType, "Audit type was null");
			Assert.IsTrue(response.Audit.AuditType.GetType() == typeof(AuditTypeModel), "Audit type type was not AuditTypeModel");
		}

		[Test]
		public void get_audits()
		{
			var route = "audits";
			var response = GetModelFromEndpoint<AuditListResponse>(route);

			Assert.IsNotNull(response.Audits, "Audits were null");
			Assert.IsTrue(response.Audits.GetType() == typeof(Collection<AuditModel>), "Audits type was not Collection<AuditModel>");
		}
	}
}
