﻿using System.Collections.ObjectModel;
using NUnit.Framework;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Engine.Api.Tests
{
	[TestFixture]
	public class HighlightTypesEndpointFixture : BaseFixture
	{
		[Test]
		public void get_highlight_type()
		{
			var route = "highlighttypes/1";
			var response = GetModelFromEndpoint<HighlightTypeResponse>(route);

			Assert.IsNotNull(response.HighlightType, "Highlight type was null");
			Assert.IsTrue(response.HighlightType.GetType() == typeof(HighlightTypeModel), "HighlightType type was not HighlightTypeModel");
		}

		[Test]
		public void get_highlight_types()
		{
			var route = "highlighttypes";
			var response = GetModelFromEndpoint<HighlightTypeListResponse>(route);

			Assert.IsNotNull(response.HighlightTypes, "Highlight types were null");
			Assert.IsTrue(response.HighlightTypes.GetType() == typeof(Collection<HighlightTypeModel>), "HighlightTypes type was not Collection<HighlightTypeModel>");
		}

		[Test]
		public void get_highlight_types_filter_name_equals_default_highlight()
		{
			var route = "highlighttypes?filter=name~eq~default%20highlight";
			var response = GetModelFromEndpoint<HighlightTypeListResponse>(route);

			Assert.IsNotNull(response.HighlightTypes, "Highlight types were null");
			Assert.IsTrue(response.HighlightTypes.GetType() == typeof(Collection<HighlightTypeModel>), "HighlightTypes type was not Collection<HighlightTypeModel>");

			foreach (var h in response.HighlightTypes)
			{
				Assert.AreEqual("default highlight", h.Name.ToLower(), "Expected name 'default highlight', actual name '" + h.Name.ToLower() + "'");
			}
		}

		[Test]
		public void get_highlight_types_filter_name_contains_default()
		{
			var route = "highlighttypes?filter=name~cn~default";
			var response = GetModelFromEndpoint<HighlightTypeListResponse>(route);

			Assert.IsNotNull(response.HighlightTypes, "Highlight types were null");
			Assert.IsTrue(response.HighlightTypes.GetType() == typeof(Collection<HighlightTypeModel>), "HighlightTypes type was not Collection<HighlightTypeModel>");

			foreach (var h in response.HighlightTypes)
			{
				Assert.IsTrue(h.Name.ToLower().Contains("default"), "Expected name to contain 'default', actual name '" + h.Name.ToLower() + "'");
			}
		}

		[Test]
		public void get_highlight_types_filter_name_starts_with_default()
		{
			var route = "highlighttypes?filter=name~sw~default";
			var response = GetModelFromEndpoint<HighlightTypeListResponse>(route);

			Assert.IsNotNull(response.HighlightTypes, "Highlight types were null");
			Assert.IsTrue(response.HighlightTypes.GetType() == typeof(Collection<HighlightTypeModel>), "HighlightTypes type was not Collection<HighlightTypeModel>");

			foreach (var h in response.HighlightTypes)
			{
				Assert.IsTrue(h.Name.ToLower().StartsWith("default"), "Expected name to start with 'default', actual name started with '" + h.Name.ToLower() + "'");
			}
		}

		[Test]
		public void get_highlight_types_filter_name_ends_with_highlight()
		{
			var route = "highlighttypes?filter=name~ew~highlight";
			var response = GetModelFromEndpoint<HighlightTypeListResponse>(route);

			Assert.IsNotNull(response.HighlightTypes, "Highlight types were null");
			Assert.IsTrue(response.HighlightTypes.GetType() == typeof(Collection<HighlightTypeModel>), "HighlightTypes type was not Collection<HighlightTypeModel>");

			foreach (var h in response.HighlightTypes)
			{
				Assert.IsTrue(h.Name.ToLower().EndsWith("highlight"), "Expected name to end with 'highlight', actual name ended with '" + h.Name.ToLower() + "'");
			}
		}

		[Test]
		public void get_highlight_types_page_index_0_size_1()
		{
			var route = "highlighttypes?page=index~0,size~1";
			var response = GetModelFromEndpoint<HighlightTypeListResponse>(route);

			Assert.IsNotNull(response.HighlightTypes, "Highlight types were null");
			Assert.IsTrue(response.HighlightTypes.GetType() == typeof(Collection<HighlightTypeModel>), "HighlightTypes type was not Collection<HighlightTypeModel>");
			Assert.IsTrue(response.HighlightTypes.Count == 1, "Expected count equal to 1, actual count " + response.HighlightTypes.Count);
		}

		[Test]
		public void create_update_delete_highlight_type()
		{
			// Create
			var route = "highlighttypes";
			var data = GetDataFromFile("HighlightType-Create.json");
			var response = PostModelToEndpoint<HighlightTypeResponse>(route, data);

			Assert.IsNotNull(response.HighlightType, "Highlight type was null");
			Assert.IsTrue(response.HighlightType.HighlightTypeId > 0, "Highlight type ID was not greater than 0");
			Assert.IsTrue(response.HighlightType.Name == "NewHighlightType", "Expected name 'NewHighlightType', actual name '" + response.HighlightType.Name + "'");

			// Update
			route = "highlighttypes/" + response.HighlightType.HighlightTypeId;
			data = GetDataFromFile("HighlightType-Update.json");
			response = PutModelToEndpoint<HighlightTypeResponse>(route, data);

			Assert.IsNotNull(response.HighlightType, "Highlight type was null");
			Assert.IsTrue(response.HighlightType.Name == "UpdatedHighlightType", "Expected name 'UpdatedHighlightType', actual name '" + response.HighlightType.Name + "'");

			// Delete
			route = "highlighttypes/" + response.HighlightType.HighlightTypeId;
			var result = DeleteResourceFromEndpoint(route);

			Assert.IsTrue(result, "Highlight type was not deleted");
		}
	}
}
