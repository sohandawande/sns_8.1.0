﻿using System.Collections.ObjectModel;
using NUnit.Framework;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Engine.Api.Tests
{
	[TestFixture]
	public class CountriesEndpointFixture : BaseFixture
	{
		[Test]
		public void get_country()
		{
			var route = "countries/US";
			var response = GetModelFromEndpoint<CountryResponse>(route);

			Assert.IsNotNull(response.Country, "Country was null");
			Assert.IsTrue(response.Country.GetType() == typeof(CountryModel), "Country type was not CountryModel");
		}

		[Test]
		public void get_country_expand_states()
		{
			var route = "countries/US?expand=states";
			var response = GetModelFromEndpoint<CountryResponse>(route);

			Assert.IsNotNull(response.Country.States, "States were null");
			Assert.IsNotEmpty(response.Country.States, "States were empty");
			Assert.IsTrue(response.Country.States.GetType() == typeof(Collection<StateModel>), "States type was not Collection<StateModel>");

			foreach (var s in response.Country.States)
			{
				Assert.AreEqual("us", s.CountryCode.ToLower(), "Expected country code 'us', actual country code '" + s.CountryCode.ToLower() + "'");
			}
		}

		[Test]
		public void get_countries()
		{
			var route = "countries";
			var response = GetModelFromEndpoint<CountryListResponse>(route);

			Assert.IsNotNull(response.Countries, "Countries were null");
			Assert.IsTrue(response.Countries.GetType() == typeof(Collection<CountryModel>), "Countries type was not Collection<CountryModel>");
		}

		[Test]
		public void get_countries_filter_code_equals_us()
		{
			var route = "countries?filter=code~eq~us";
			var response = GetModelFromEndpoint<CountryListResponse>(route);

			Assert.IsNotNull(response.Countries, "Countries were null");
			Assert.IsTrue(response.Countries.GetType() == typeof(Collection<CountryModel>), "Countries type was not Collection<CountryModel>");

			foreach (var c in response.Countries)
			{
				Assert.AreEqual("us", c.Code.ToLower(), "Expected code 'us', actual code '" + c.Code.ToLower() + "'");
			}
		}

		[Test]
		public void get_countries_filter_code_not_equal_to_us()
		{
			var route = "countries?filter=code~ne~us";
			var response = GetModelFromEndpoint<CountryListResponse>(route);

			Assert.IsNotNull(response.Countries, "Countries were null");
			Assert.IsTrue(response.Countries.GetType() == typeof(Collection<CountryModel>), "Countries type was not Collection<CountryModel>");

			foreach (var c in response.Countries)
			{
				Assert.AreNotEqual("us", c.Code.ToLower(), "Expected code not equal to 'us'");
			}
		}

		[Test]
		public void get_countries_filter_name_contains_united()
		{
			var route = "countries?filter=name~cn~united";
			var response = GetModelFromEndpoint<CountryListResponse>(route);

			Assert.IsNotNull(response.Countries, "Countries were null");
			Assert.IsTrue(response.Countries.GetType() == typeof(Collection<CountryModel>), "Countries type was not Collection<CountryModel>");

			foreach (var c in response.Countries)
			{
				Assert.IsTrue(c.Name.ToLower().Contains("united"), "Expected name to contain 'united', actual name '" + c.Name.ToLower() + "'");
			}
		}

		[Test]
		public void get_countries_filter_name_starts_with_south()
		{
			var route = "countries?filter=name~sw~south";
			var response = GetModelFromEndpoint<CountryListResponse>(route);

			Assert.IsNotNull(response.Countries, "Countries were null");
			Assert.IsTrue(response.Countries.GetType() == typeof(Collection<CountryModel>), "Countries type was not Collection<CountryModel>");

			foreach (var c in response.Countries)
			{
				Assert.IsTrue(c.Name.ToLower().StartsWith("south"), "Expected name to start with 'south', actual name '" + c.Name.ToLower() + "'");
			}
		}

		[Test]
		public void get_countries_filter_name_ends_with_states()
		{
			var route = "countries?filter=name~ew~states";
			var response = GetModelFromEndpoint<CountryListResponse>(route);

			Assert.IsNotNull(response.Countries, "Countries were null");
			Assert.IsTrue(response.Countries.GetType() == typeof(Collection<CountryModel>), "Countries type was not Collection<CountryModel>");

			foreach (var c in response.Countries)
			{
				Assert.IsTrue(c.Name.ToLower().EndsWith("states"), "Expected name to end with 'states', actual name '" + c.Name.ToLower() + "'");
			}
		}

		[Test]
		public void get_countries_page_index_0_size_10()
		{
			var route = "countries?page=index~0,size~10";
			var response = GetModelFromEndpoint<CountryListResponse>(route);

			Assert.IsNotNull(response.Countries, "Countries were null");
			Assert.IsTrue(response.Countries.GetType() == typeof(Collection<CountryModel>), "Countries type was not Collection<CountryModel>");
			Assert.IsTrue(response.Countries.Count == 10, "Expected count equal to 10, actual count " + response.Countries.Count);
		}

		[Test]
		public void create_update_delete_country()
		{
			// Create
			var route = "countries";
			var data = GetDataFromFile("Country-Create.json");
			var response = PostModelToEndpoint<CountryResponse>(route, data);

			Assert.IsNotNull(response.Country, "Country was null");
			Assert.IsNotNullOrEmpty(response.Country.Code, "Country code was not empty");
			Assert.IsTrue(response.Country.Name == "NewCountry", "Expected name 'NewCountry', actual name '" + response.Country.Name + "'");

			// Update
			route = "countries/" + response.Country.Code;
			data = GetDataFromFile("Country-Update.json");
			response = PutModelToEndpoint<CountryResponse>(route, data);

			Assert.IsNotNull(response.Country, "Country was null");
			Assert.IsTrue(response.Country.Name == "UpdatedCountry", "Expected name 'UpdatedCountry', actual name '" + response.Country.Name + "'");

			// Delete
			route = "countries/" + response.Country.Code;
			var result = DeleteResourceFromEndpoint(route);

			Assert.IsTrue(result, "Country was not deleted");
		}
	}
}
