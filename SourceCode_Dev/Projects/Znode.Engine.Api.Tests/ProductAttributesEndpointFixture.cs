﻿using System.Collections.ObjectModel;
using NUnit.Framework;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Engine.Api.Tests
{
    [TestFixture]
    public class ProductAttributesEndpointFixture : BaseFixture
    {
        [Test]
        public void get_productattribute()
        {
            var route = "productattributes/1";
            var response = GetModelFromEndpoint<ProductAttributesResponse>(route);

            Assert.IsNotNull(response.Attributes, "Attributes was null");
            Assert.IsTrue(response.Attributes.GetType() == typeof(AttributeModel), "Attributes type was not AttributeModel");
        }

        [Test]
        public void get_productattributes()
        {
            var route = "productattributes";
            var response = GetModelFromEndpoint<ProductAttributesListResponse>(route);

            Assert.IsNotNull(response.Attributes, "Attributes were null");
            Assert.IsTrue(response.Attributes.GetType() == typeof(Collection<AttributeModel>), "Attributes type was not Collection<AttributeModel>");
        }

        [Test]
        public void get_productattributes_attributetypeid_equals_1()
        {
            var route = "productattributes?filter=attributetypeid~eq~1";
            var response = GetModelFromEndpoint<ProductAttributesListResponse>(route);

            Assert.IsNotNull(response.Attributes, "Attributes were null");
            Assert.IsTrue(response.Attributes.GetType() == typeof(Collection<AttributeModel>), "Attributes type was not Collection<AttributeModel>");

            foreach (var a in response.Attributes)
            {
                Assert.AreEqual(1, a.AttributeTypeId, "Expected AttributeType ID 1, actual AttributeType ID " + a.AttributeTypeId);
            }           
        }

        [Test]
        public void get_productattributes_attributetypeid_not_equals_1()
        {
            var route = "productattributes?filter=attributetypeid~ne~1";
            var response = GetModelFromEndpoint<ProductAttributesListResponse>(route);

            Assert.IsNotNull(response.Attributes, "Attributes were null");
            Assert.IsTrue(response.Attributes.GetType() == typeof(Collection<AttributeModel>), "Attributes type was not Collection<AttributeModel>");

            foreach (var a in response.Attributes)
            {
                Assert.AreEqual(1, a.AttributeTypeId, "Expected AttributeType ID 1, actual AttributeType ID " + a.AttributeTypeId);
            }
        }

    }
}
