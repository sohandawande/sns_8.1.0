﻿using System.Collections.ObjectModel;
using NUnit.Framework;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Engine.Api.Tests
{
	[TestFixture]
	public class TaxClassesEndpointFixture : BaseFixture
	{
		[Test]
		public void get_tax_class()
		{
			var route = "taxclasses/1";
			var response = GetModelFromEndpoint<TaxClassResponse>(route);

			Assert.IsNotNull(response.TaxClass, "Tax class was null");
			Assert.IsTrue(response.TaxClass.GetType() == typeof(TaxClassModel), "Tax class type was not TaxClassModel");
		}

		[Test]
		public void get_tax_classes()
		{
			var route = "taxclasses";
			var response = GetModelFromEndpoint<TaxClassListResponse>(route);

			Assert.IsNotNull(response.TaxClasses, "Tax classes were null");
			Assert.IsTrue(response.TaxClasses.GetType() == typeof(Collection<TaxClassModel>), "TaxClasses type was not Collection<TaxClassModel>");
		}

		[Test]
		public void get_tax_classes_filter_name_equals_sales_tax()
		{
			var route = "taxclasses?filter=name~eq~sales%20tax";
			var response = GetModelFromEndpoint<TaxClassListResponse>(route);

			Assert.IsNotNull(response.TaxClasses, "Tax classes were null");
			Assert.IsTrue(response.TaxClasses.GetType() == typeof(Collection<TaxClassModel>), "TaxClasses type was not Collection<TaxClassModel>");

			foreach (var t in response.TaxClasses)
			{
				Assert.AreEqual("sales tax", t.Name.ToLower(), "Expected name 'sales tax', actual name '" + t.Name.ToLower() + "'");
			}
		}

		[Test]
		public void get_tax_classes_filter_name_not_equal_to_sales_tax()
		{
			var route = "taxclasses?filter=name~ne~sales%20tax";
			var response = GetModelFromEndpoint<TaxClassListResponse>(route);

			Assert.IsNotNull(response.TaxClasses, "Tax classes were null");
			Assert.IsTrue(response.TaxClasses.GetType() == typeof(Collection<TaxClassModel>), "TaxClasses type was not Collection<TaxClassModel>");

			foreach (var t in response.TaxClasses)
			{
				Assert.AreNotEqual("sales tax", t.Name.ToLower(), "Expected name not equal to 'sales tax'");
			}
		}

		[Test]
		public void get_tax_classes_filter_name_contains_sales()
		{
			var route = "taxclasses?filter=name~cn~sales";
			var response = GetModelFromEndpoint<TaxClassListResponse>(route);

			Assert.IsNotNull(response.TaxClasses, "Tax classes were null");
			Assert.IsTrue(response.TaxClasses.GetType() == typeof(Collection<TaxClassModel>), "TaxClasses type was not Collection<TaxClassModel>");

			foreach (var t in response.TaxClasses)
			{
				Assert.IsTrue(t.Name.ToLower().Contains("sales"), "Expected name to contain 'sales', actual name '" + t.Name.ToLower() + "'");
			}
		}

		[Test]
		public void get_tax_classes_filter_name_starts_with_sales()
		{
			var route = "taxclasses?filter=name~sw~sales";
			var response = GetModelFromEndpoint<TaxClassListResponse>(route);

			Assert.IsNotNull(response.TaxClasses, "Tax classes were null");
			Assert.IsTrue(response.TaxClasses.GetType() == typeof(Collection<TaxClassModel>), "TaxClasses type was not Collection<TaxClassModel>");

			foreach (var t in response.TaxClasses)
			{
				Assert.IsTrue(t.Name.ToLower().StartsWith("sales"), "Expected name to start with 'sales', actual name started with '" + t.Name.ToLower() + "'");
			}
		}

		[Test]
		public void get_tax_classes_filter_name_ends_with_tax()
		{
			var route = "taxclasses?filter=name~ew~tax";
			var response = GetModelFromEndpoint<TaxClassListResponse>(route);

			Assert.IsNotNull(response.TaxClasses, "Tax classes were null");
			Assert.IsTrue(response.TaxClasses.GetType() == typeof(Collection<TaxClassModel>), "TaxClasses type was not Collection<TaxClassModel>");

			foreach (var t in response.TaxClasses)
			{
				Assert.IsTrue(t.Name.ToLower().EndsWith("tax"), "Expected name to end with 'tax', actual name ended with '" + t.Name.ToLower() + "'");
			}
		}

		[Test]
		public void get_tax_classes_page_index_0_size_2()
		{
			var route = "taxclasses?page=index~0,size~2";
			var response = GetModelFromEndpoint<TaxClassListResponse>(route);

			Assert.IsNotNull(response.TaxClasses, "Tax classes were null");
			Assert.IsTrue(response.TaxClasses.GetType() == typeof(Collection<TaxClassModel>), "TaxClasses type was not Collection<TaxClassModel>");
			Assert.IsTrue(response.TaxClasses.Count == 2, "Expected count equal to 2, actual count " + response.TaxClasses.Count);
		}

		[Test]
		public void create_update_delete_tax_class()
		{
			// Create
			var route = "taxclasses";
			var data = GetDataFromFile("TaxClass-Create.json");
			var response = PostModelToEndpoint<TaxClassResponse>(route, data);

			Assert.IsNotNull(response.TaxClass, "Tax class was null");
			Assert.IsTrue(response.TaxClass.TaxClassId > 0, "Tax class ID was not greater than 0");
			Assert.IsTrue(response.TaxClass.Name == "NewTaxClass", "Expected name 'NewTaxClass', actual name '" + response.TaxClass.Name + "'");

			// Update
			route = "taxclasses/" + response.TaxClass.TaxClassId;
			data = GetDataFromFile("TaxClass-Update.json");
			response = PutModelToEndpoint<TaxClassResponse>(route, data);

			Assert.IsNotNull(response.TaxClass, "Tax class was null");
			Assert.IsTrue(response.TaxClass.Name == "UpdatedTaxClass", "Expected name 'UpdatedTaxClass', actual name '" + response.TaxClass.Name + "'");

			// Delete
			route = "taxclasses/" + response.TaxClass.TaxClassId;
			var result = DeleteResourceFromEndpoint(route);

			Assert.IsTrue(result, "Tax class was not deleted");
		}
	}
}
