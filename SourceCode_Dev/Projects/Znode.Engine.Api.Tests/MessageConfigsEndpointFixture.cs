﻿using System.Collections.ObjectModel;
using NUnit.Framework;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Engine.Api.Tests
{
    [TestFixture]
    public class MessageConfigsEndpointFixture : BaseFixture
    {
        [Test]
        public void get_message_config()
        {
			var route = "messageconfigs/1";
            var response = GetModelFromEndpoint<MessageConfigResponse>(route);

            Assert.IsNotNull(response.MessageConfig, "Message config was null");
            Assert.IsTrue(response.MessageConfig.GetType() == typeof(MessageConfigModel), "Message config type was not MessageConfigModel");
        }

        [Test]
        public void get_message_configs()
        {
			var route = "messageconfigs";
            var response = GetModelFromEndpoint<MessageConfigListResponse>(route);

            Assert.IsNotNull(response.MessageConfigs, "Message configs were null");
            Assert.IsTrue(response.MessageConfigs.GetType() == typeof(Collection<MessageConfigModel>), "Message configs type was not Collection<MessageConfigModel>");
        }

        [Test]
        public void get_message_config_by_key()
        {
            var route = "messageconfigs/key/HomePagePromo";
            var response = GetModelFromEndpoint<MessageConfigResponse>(route);

            Assert.IsNotNull(response.MessageConfig, "Message configs were null");
            Assert.IsTrue(response.MessageConfig.GetType() == typeof(MessageConfigModel), "Message config type was not MessageConfigModel");
        }

        [Test]
        public void get_message_configs_filter_key_equals_homepagepromo()
        {
			var route = "messageconfigs?filter=key~eq~homepagepromo";
            var response = GetModelFromEndpoint<MessageConfigListResponse>(route);

            Assert.IsNotNull(response.MessageConfigs, "Message configs were null");
            Assert.IsTrue(response.MessageConfigs.GetType() == typeof(Collection<MessageConfigModel>), "Message configs type was not Collection<MessageConfigModel>");

            foreach (var m in response.MessageConfigs)
            {
                Assert.AreEqual("homepagepromo", m.Key.ToLower(), "Expected name 'homepagepromo', actual name '" + m.Key.ToLower() + "'");
            }
        }

        [Test]
        public void get_message_configs_filter_name_not_equal_to_homepagepromo()
        {
			var route = "messageconfigs?filter=key~ne~homepagepromo";
            var response = GetModelFromEndpoint<MessageConfigListResponse>(route);

            Assert.IsNotNull(response.MessageConfigs, "Message configs were null");
            Assert.IsTrue(response.MessageConfigs.GetType() == typeof(Collection<MessageConfigModel>), "Message configs type was not Collection<MessageConfigModel>");

            foreach (var m in response.MessageConfigs)
            {
                Assert.AreNotEqual("homepagepromo", m.Key.ToLower(), "Expected name not equal to 'homepagepromo'");
            }
        }

        [Test]
        public void get_message_configs_filter_name_contains_homepagepromo()
        {
			var route = "messageconfigs?filter=key~cn~homepagepromo";
            var response = GetModelFromEndpoint<MessageConfigListResponse>(route);

            Assert.IsNotNull(response.MessageConfigs, "Message configs were null");
            Assert.IsTrue(response.MessageConfigs.GetType() == typeof(Collection<MessageConfigModel>), "Message configs type was not Collection<MessageConfigModel>");

            foreach (var m in response.MessageConfigs)
            {
                Assert.IsTrue(m.Key.ToLower().Contains("homepagepromo"), "Expected name to contain 'homepagepromo', actual name '" + m.Key.ToLower() + "'");
            }
        }

        [Test]
        public void get_message_configs_filter_name_starts_with_homepage()
        {
			var route = "messageconfigs?filter=key~sw~homepage";
            var response = GetModelFromEndpoint<MessageConfigListResponse>(route);

            Assert.IsNotNull(response.MessageConfigs, "Message configs were null");
            Assert.IsTrue(response.MessageConfigs.GetType() == typeof(Collection<MessageConfigModel>), "Message configs type was not Collection<MessageConfigModel>");

            foreach (var m in response.MessageConfigs)
            {
                Assert.IsTrue(m.Key.ToLower().StartsWith("homepage"), "Expected name to start with 'homepage', actual name '" + m.Key.ToLower() + "'");
            }
        }

        [Test]
        public void get_message_configs_filter_name_ends_with_promo()
        {
			var route = "messageconfigs?filter=key~ew~promo";
            var response = GetModelFromEndpoint<MessageConfigListResponse>(route);

            Assert.IsNotNull(response.MessageConfigs, "Message configs were null");
            Assert.IsTrue(response.MessageConfigs.GetType() == typeof(Collection<MessageConfigModel>), "Message configs type was not Collection<MessageConfigModel>");

            foreach (var m in response.MessageConfigs)
            {
                Assert.IsTrue(m.Key.ToLower().EndsWith("promo"), "Expected name to end with 'foods', actual name '" + m.Key.ToLower() + "'");
            }
        }

        [Test]
        public void get_message_configs_page_index_0_size_2()
        {
			var route = "messageconfigs?page=index~0,size~2";
            var response = GetModelFromEndpoint<MessageConfigListResponse>(route);

            Assert.IsNotNull(response.MessageConfigs, "Message configs were null");
            Assert.IsTrue(response.MessageConfigs.GetType() == typeof(Collection<MessageConfigModel>), "Message configs type was not Collection<MessageConfigModel>");
            Assert.IsTrue(response.MessageConfigs.Count == 2, "Expected count equal to 2, actual count " + response.MessageConfigs.Count);
        }

        [Test]
        public void create_update_delete_MessageConfig()
        {
            // Create
			var route = "messageconfigs";
            var data = GetDataFromFile("MessageConfig-Create.json");
            var response = PostModelToEndpoint<MessageConfigResponse>(route, data);

            Assert.IsNotNull(response.MessageConfig, "Message config was null");
            Assert.IsTrue(response.MessageConfig.MessageConfigId > 0, "Message config ID was not greater than 0");
            Assert.IsTrue(response.MessageConfig.Key == "NewMessageConfig", "Expected name 'NewMessageConfig', actual name '" + response.MessageConfig.Key + "'");

            // Update
			route = "messageconfigs/" + response.MessageConfig.MessageConfigId;
            data = GetDataFromFile("MessageConfig-Update.json");
            response = PutModelToEndpoint<MessageConfigResponse>(route, data);

            Assert.IsNotNull(response.MessageConfig, "Message config was null");
            Assert.IsTrue(response.MessageConfig.Key == "UpdatedMessageConfig", "Expected name 'UpdatedMessageConfig', actual name '" + response.MessageConfig.Key + "'");

            // Delete
            route = "messageconfigs/" + response.MessageConfig.MessageConfigId;
            var result = DeleteResourceFromEndpoint(route);

            Assert.IsTrue(result, "Message config was not deleted");
        }
    }
}
