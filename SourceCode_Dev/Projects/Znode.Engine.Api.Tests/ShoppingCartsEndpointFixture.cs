﻿using System.Collections.Generic;
using NUnit.Framework;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Engine.Api.Tests
{
	[TestFixture]
	public class ShoppingCartsEndpointFixture : BaseFixture
	{
		[Test]
		public void shopping_cart_calculate()
		{
			var route = "shoppingcarts/calculate";
			var data = GetDataFromFile("ShoppingCart-Calculate.json");
			var response = PostModelToEndpoint<ShoppingCartResponse>(route, data, true, false, false);

			Assert.IsTrue(response.ShoppingCart.SalesTax > 0, "Expected sales tax greater than 0, actual sales tax " + response.ShoppingCart.SalesTax);
			Assert.IsTrue(response.ShoppingCart.Total > 0, "Expected total greater than 0, actual total " + response.ShoppingCart.Total);
		}

        [Test]
        public void shopping_cart_get()
        {
            var route = "shoppingcarts/11522";
	        var additionalHeaders = new Dictionary<string, string> {{"Znode-AccountId", "11522"}};

	        var response = GetModelFromEndpoint<ShoppingCartResponse>(route, additionalHeaders);

            Assert.IsNotNull(response.ShoppingCart, " Shopping cart is null");
            Assert.IsTrue(response.ShoppingCart.ShoppingCartItems.Count > 0, "Expected total greater than 0, actual total " + response.ShoppingCart.ShoppingCartItems.Count);
        }
	}
}
