﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Engine.Api.Tests
{
    [TestFixture]
    public class WishListEnpointFixture : BaseFixture
    {

        [Test]
        public void get_wishlist()
        {
            var route = "wishlists/1";
            var response = GetModelFromEndpoint<WishListResponse>(route);

            Assert.IsNotNull(response.WishList, "WishList was null");
            Assert.IsTrue(response.WishList.GetType() == typeof(WishListModel), "WishList type was not WishListModel");
        }


        [Test]
        public void create_get_update_delete_wishlist()
        {
            // Create
            var route = "wishlists";
            var data = GetDataFromFile("Wishlist-Create.json");
            var response = PostModelToEndpoint<WishListResponse>(route, data);

            Assert.IsNotNull(response.WishList, "Wishlist was null");
            Assert.IsTrue(response.WishList.WishListId > 0, "Wishlist ID was not greater than 0");
            Assert.IsTrue(response.WishList.Custom == "Created from Nunit", "Expected custom property  'Created from Nunit', actual name '" + response.WishList.Custom + "'");

            //
            route = "wishlists/" + response.WishList.WishListId;
            response = GetModelFromEndpoint<WishListResponse>(route);

            Assert.IsNotNull(response.WishList, "WishList was null");
            Assert.IsTrue(response.WishList.GetType() == typeof(WishListModel), "WishList type was not WishListModel");


            // Update
            route = "wishlists/" + response.WishList.WishListId;
            data = GetDataFromFile("WishList-Update.json");
            response = PutModelToEndpoint<WishListResponse>(route, data);

            Assert.IsNotNull(response.WishList, "SKU was null");
            Assert.IsTrue(response.WishList.ProductId == 305, "Expected Id '302', actual Id '" + response.WishList.ProductId + "'");

            // Delete
            route = "wishlists/" + response.WishList.WishListId;
            var result = DeleteResourceFromEndpoint(route);

            Assert.IsTrue(result, "WishList was not deleted");
        }

    }
}
