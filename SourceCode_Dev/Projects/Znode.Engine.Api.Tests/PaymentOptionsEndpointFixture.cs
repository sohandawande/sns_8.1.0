﻿using System.Collections.ObjectModel;
using NUnit.Framework;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Engine.Api.Tests
{
	[TestFixture]
	public class PaymentOptionsEndpointFixture : BaseFixture
	{
		[Test]
		public void get_payment_option()
		{
			var route = "paymentoptions/24";
			var response = GetModelFromEndpoint<PaymentOptionResponse>(route);

			Assert.IsNotNull(response.PaymentOption, "Payment option was null");
			Assert.IsTrue(response.PaymentOption.GetType() == typeof(PaymentOptionModel), "Payment option type was not PaymentOptionModel");
		}

		[Test]
		public void get_payment_option_expand_paymentgateway()
		{
			var route = "paymentoptions/24?expand=paymentgateway";
			var response = GetModelFromEndpoint<PaymentOptionResponse>(route);

			Assert.IsNotNull(response.PaymentOption.PaymentGateway, "Payment gateway was null");
			Assert.IsTrue(response.PaymentOption.PaymentGateway.GetType() == typeof(PaymentGatewayModel), "Payment gateway type was not PaymentGatewayModel");
		}

		[Test]
		public void get_payment_option_expand_paymenttype()
		{
			var route = "paymentoptions/24?expand=paymenttype";
			var response = GetModelFromEndpoint<PaymentOptionResponse>(route);

			Assert.IsNotNull(response.PaymentOption.PaymentType, "Payment type was null");
			Assert.IsTrue(response.PaymentOption.PaymentType.GetType() == typeof(PaymentTypeModel), "PaymentType type was not PaymentTypeModel");
		}

		[Test]
		public void get_payment_options()
		{
			var route = "paymentoptions";
			var response = GetModelFromEndpoint<PaymentOptionListResponse>(route);

			Assert.IsNotNull(response.PaymentOptions, "Payment options were null");
			Assert.IsTrue(response.PaymentOptions.GetType() == typeof(Collection<PaymentOptionModel>), "PaymentOptions type was not Collection<PaymentOptionModel>");
		}

		[Test]
		public void get_payment_options_filter_isactive_equals_true()
		{
			var route = "paymentoptions?filter=isactive~eq~true";
			var response = GetModelFromEndpoint<PaymentOptionListResponse>(route);

			Assert.IsNotNull(response.PaymentOptions, "Payment options were null");
			Assert.IsTrue(response.PaymentOptions.GetType() == typeof(Collection<PaymentOptionModel>), "PaymentOptions type was not Collection<PaymentOptionModel>");

			foreach (var p in response.PaymentOptions)
			{
				Assert.IsTrue(p.IsActive, "Expected payment option to be active");
			}
		}

		[Test]
		public void get_payment_options_page_index_0_size_1()
		{
			var route = "paymentoptions?page=index~0,size~1";
			var response = GetModelFromEndpoint<PaymentOptionListResponse>(route);

			Assert.IsNotNull(response.PaymentOptions, "Payment options were null");
			Assert.IsTrue(response.PaymentOptions.GetType() == typeof(Collection<PaymentOptionModel>), "PaymentOptions type was not Collection<PaymentOptionModel>");
			Assert.IsTrue(response.PaymentOptions.Count == 1, "Expected count equal to 1, actual count " + response.PaymentOptions.Count);
		}

		[Test]
		public void create_update_delete_payment_option()
		{
			// Create
			var route = "paymentoptions";
			var data = GetDataFromFile("PaymentOption-Create.json");
			var response = PostModelToEndpoint<PaymentOptionResponse>(route, data);

			Assert.IsNotNull(response.PaymentOption, "Payment option was null");
			Assert.IsTrue(response.PaymentOption.PaymentOptionId > 0, "Payment option ID was not greater than 0");
			Assert.IsTrue(response.PaymentOption.Partner == "NewPaymentOption", "Expected partner 'NewPaymentOption', actual partner '" + response.PaymentOption.Partner + "'");

			// Update
			route = "paymentoptions/" + response.PaymentOption.PaymentOptionId;
			data = GetDataFromFile("PaymentOption-Update.json");
			response = PutModelToEndpoint<PaymentOptionResponse>(route, data);

			Assert.IsNotNull(response.PaymentOption, "Payment option was null");
			Assert.IsTrue(response.PaymentOption.Partner == "UpdatedPaymentOption", "Expected partner 'UpdatedPaymentOption', actual partner '" + response.PaymentOption.Partner + "'");

			// Delete
			route = "paymentoptions/" + response.PaymentOption.PaymentOptionId;
			var result = DeleteResourceFromEndpoint(route);

			Assert.IsTrue(result, "Payment option was not deleted");
		}
	}
}
