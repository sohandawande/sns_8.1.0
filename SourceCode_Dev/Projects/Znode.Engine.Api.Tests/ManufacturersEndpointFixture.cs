﻿using System.Collections.ObjectModel;
using NUnit.Framework;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Engine.Api.Tests
{
	[TestFixture]
	public class ManufacturersEndpointFixture : BaseFixture
	{
		[Test]
		public void get_manufacturer()
		{
			var route = "manufacturers/1";
			var response = GetModelFromEndpoint<ManufacturerResponse>(route);

			Assert.IsNotNull(response.Manufacturer, "Manufacturer was null");
			Assert.IsTrue(response.Manufacturer.GetType() == typeof(ManufacturerModel), "Manufacturer type was not ManufacturerModel");
		}

		[Test]
		public void get_manufacturers()
		{
			var route = "manufacturers";
			var response = GetModelFromEndpoint<ManufacturerListResponse>(route);

			Assert.IsNotNull(response.Manufacturers, "Manufacturers were null");
			Assert.IsTrue(response.Manufacturers.GetType() == typeof(Collection<ManufacturerModel>), "Manufacturers type was not Collection<ManufacturerModel>");
		}

		[Test]
		public void get_manufacturers_filter_name_equals_whole_foods()
		{
			var route = "manufacturers?filter=name~eq~whole%20foods";
			var response = GetModelFromEndpoint<ManufacturerListResponse>(route);

			Assert.IsNotNull(response.Manufacturers, "Manufacturers were null");
			Assert.IsTrue(response.Manufacturers.GetType() == typeof(Collection<ManufacturerModel>), "Manufacturers type was not Collection<ManufacturerModel>");

			foreach (var m in response.Manufacturers)
			{
				Assert.AreEqual("whole foods", m.Name.ToLower(), "Expected name 'whole foods', actual name '" + m.Name.ToLower() + "'");
			}
		}

		[Test]
		public void get_manufacturers_filter_name_not_equal_to_whole_foods()
		{
			var route = "manufacturers?filter=name~ne~whole%20foods";
			var response = GetModelFromEndpoint<ManufacturerListResponse>(route);

			Assert.IsNotNull(response.Manufacturers, "Manufacturers were null");
			Assert.IsTrue(response.Manufacturers.GetType() == typeof(Collection<ManufacturerModel>), "Manufacturers type was not Collection<ManufacturerModel>");

			foreach (var m in response.Manufacturers)
			{
				Assert.AreNotEqual("whole foods", m.Name.ToLower(), "Expected name not equal to 'whole foods'");
			}
		}

		[Test]
		public void get_manufacturers_filter_name_contains_foods()
		{
			var route = "manufacturers?filter=name~cn~foods";
			var response = GetModelFromEndpoint<ManufacturerListResponse>(route);

			Assert.IsNotNull(response.Manufacturers, "Manufacturers were null");
			Assert.IsTrue(response.Manufacturers.GetType() == typeof(Collection<ManufacturerModel>), "Manufacturers type was not Collection<ManufacturerModel>");

			foreach (var m in response.Manufacturers)
			{
				Assert.IsTrue(m.Name.ToLower().Contains("foods"), "Expected name to contain 'foods', actual name '" + m.Name.ToLower() + "'");
			}
		}

		[Test]
		public void get_manufacturers_filter_name_starts_with_whole()
		{
			var route = "manufacturers?filter=name~sw~whole";
			var response = GetModelFromEndpoint<ManufacturerListResponse>(route);

			Assert.IsNotNull(response.Manufacturers, "Manufacturers were null");
			Assert.IsTrue(response.Manufacturers.GetType() == typeof(Collection<ManufacturerModel>), "Manufacturers type was not Collection<ManufacturerModel>");

			foreach (var m in response.Manufacturers)
			{
				Assert.IsTrue(m.Name.ToLower().StartsWith("whole"), "Expected name to start with 'whole', actual name '" + m.Name.ToLower() + "'");
			}
		}

		[Test]
		public void get_manufacturers_filter_name_ends_with_foods()
		{
			var route = "manufacturers?filter=name~ew~foods";
			var response = GetModelFromEndpoint<ManufacturerListResponse>(route);

			Assert.IsNotNull(response.Manufacturers, "Manufacturers were null");
			Assert.IsTrue(response.Manufacturers.GetType() == typeof(Collection<ManufacturerModel>), "Manufacturers type was not Collection<ManufacturerModel>");

			foreach (var m in response.Manufacturers)
			{
				Assert.IsTrue(m.Name.ToLower().EndsWith("foods"), "Expected name to end with 'foods', actual name '" + m.Name.ToLower() + "'");
			}
		}

		[Test]
		public void get_manufacturers_page_index_0_size_2()
		{
			var route = "manufacturers?page=index~0,size~2";
			var response = GetModelFromEndpoint<ManufacturerListResponse>(route);

			Assert.IsNotNull(response.Manufacturers, "Manufacturers were null");
			Assert.IsTrue(response.Manufacturers.GetType() == typeof(Collection<ManufacturerModel>), "Manufacturers type was not Collection<ManufacturerModel>");
			Assert.IsTrue(response.Manufacturers.Count == 2, "Expected count equal to 2, actual count " + response.Manufacturers.Count);
		}

		[Test]
		public void create_update_delete_manufacturer()
		{
			// Create
			var route = "manufacturers";
			var data = GetDataFromFile("Manufacturer-Create.json");
			var response = PostModelToEndpoint<ManufacturerResponse>(route, data);

			Assert.IsNotNull(response.Manufacturer, "Manufacturer was null");
			Assert.IsTrue(response.Manufacturer.ManufacturerId > 0, "Manufacturer ID was not greater than 0");
			Assert.IsTrue(response.Manufacturer.Name == "NewManufacturer", "Expected name 'NewManufacturer', actual name '" + response.Manufacturer.Name + "'");

			// Update
			route = "manufacturers/" + response.Manufacturer.ManufacturerId;
			data = GetDataFromFile("Manufacturer-Update.json");
			response = PutModelToEndpoint<ManufacturerResponse>(route, data);

			Assert.IsNotNull(response.Manufacturer, "Manufacturer was null");
			Assert.IsTrue(response.Manufacturer.Name == "UpdatedManufacturer", "Expected name 'UpdatedManufacturer', actual name '" + response.Manufacturer.Name + "'");

			// Delete
			route = "manufacturers/" + response.Manufacturer.ManufacturerId;
			var result = DeleteResourceFromEndpoint(route);

			Assert.IsTrue(result, "Manufacturer was not deleted");
		}
	}
}
