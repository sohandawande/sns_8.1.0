﻿using System.Collections.ObjectModel;
using NUnit.Framework;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Engine.Api.Tests
{
	[TestFixture]
	public class AttributeTypesEndpointFixture : BaseFixture
	{
		[Test]
		public void get_attributeType()
		{
			var route = "attributetypes/1";
			var response = GetModelFromEndpoint<AttributeTypeResponse>(route);

			Assert.IsNotNull(response.AttributeType, "Attribute type was null");
			Assert.IsTrue(response.AttributeType.GetType() == typeof(AttributeTypeModel), "AttributeType type was not AttributeTypeModel");
		}

		[Test]
		public void get_attributetypes()
		{
			var route = "attributetypes";
			var response = GetModelFromEndpoint<AttributeTypeListResponse>(route);

			Assert.IsNotNull(response.AttributeTypes, "Attribute types were null");
			Assert.IsTrue(response.AttributeTypes.GetType() == typeof(Collection<AttributeTypeModel>), "AttributeTypes type was not Collection<AttributeTypeModel>");
		}

		[Test]
		public void get_attributetypes_filter_name_equals_color()
		{
			var route = "attributetypes?filter=name~eq~color";
			var response = GetModelFromEndpoint<AttributeTypeListResponse>(route);

			Assert.IsNotNull(response.AttributeTypes, "Attribute types were null");
			Assert.IsTrue(response.AttributeTypes.GetType() == typeof(Collection<AttributeTypeModel>), "AttributeTypes type was not Collection<AttributeTypeModel>");

			foreach (var a in response.AttributeTypes)
			{
				Assert.AreEqual("color", a.Name.ToLower(), "Expected name 'color', actual name '" + a.Name.ToLower() + "'");
			}
		}

		[Test]
		public void get_attributetypes_filter_localeid_equals_43()
		{
			var route = "attributetypes?filter=localeid~eq~43";
			var response = GetModelFromEndpoint<AttributeTypeListResponse>(route);

			Assert.IsNotNull(response.AttributeTypes, "Attribute types were null");
			Assert.IsTrue(response.AttributeTypes.GetType() == typeof(Collection<AttributeTypeModel>), "AttributeTypes type was not Collection<AttributeTypeModel>");

			foreach (var a in response.AttributeTypes)
			{
				Assert.AreEqual(43, a.LocaleId, "Expected locale ID 43, actual locale ID " + a.LocaleId);
			}
		}

		[Test]
		public void create_update_delete_attributeType()
		{
			// Create
			var route = "attributetypes";
			var data = GetDataFromFile("AttributeType-Create.json");
			var response = PostModelToEndpoint<AttributeTypeResponse>(route, data);

			Assert.IsNotNull(response.AttributeType, "Attribute type was null");
			Assert.IsTrue(response.AttributeType.AttributeTypeId > 0, "AttributeType ID was not greater than 0");
			Assert.IsTrue(response.AttributeType.Name == "NewAttributeType", "Expected name 'NewAttributeType', actual name '" + response.AttributeType.Name + "'");

			// Update
			route = "attributetypes/" + response.AttributeType.AttributeTypeId;
			data = GetDataFromFile("AttributeType-Update.json");
			response = PutModelToEndpoint<AttributeTypeResponse>(route, data);

			Assert.IsNotNull(response.AttributeType, "Attribute type was null");
			Assert.IsTrue(response.AttributeType.Name == "UpdatedAttributeType", "Expected name 'UpdatedAttributeType', actual name '" + response.AttributeType.Name + "'");

			// Delete
			route = "attributetypes/" + response.AttributeType.AttributeTypeId;
			var result = DeleteResourceFromEndpoint(route);

			Assert.IsTrue(result, "Attribute type was not deleted");
		}
	}
}
