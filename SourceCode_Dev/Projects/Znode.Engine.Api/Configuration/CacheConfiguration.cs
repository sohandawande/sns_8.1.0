﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Configuration
{
	public class CacheConfiguration
	{
		public Collection<CacheRoute> CacheRoutes { get; set; }
		public bool Enabled { get; set; }

		public CacheConfiguration()
		{
			CacheRoutes = new Collection<CacheRoute>();
		}
	}
}
