﻿namespace Znode.Engine.Api.Attributes
{
	public class SortExternalIdAttribute : SortAttribute
	{
		public override string Name { get { return "externalId"; } }
		public override string Description { get { return "This will sort the response by external ID."; } }
	}
}