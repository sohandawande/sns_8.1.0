﻿
namespace Znode.Engine.Api.Attributes
{
    public class FilterMessageTypeIdAttribute : FilterAttribute
    {
        public override string Name { get { return "messageTypeId"; } }
        public override string Description { get { return "This will filter the request by message Type Id."; } }
    }
}