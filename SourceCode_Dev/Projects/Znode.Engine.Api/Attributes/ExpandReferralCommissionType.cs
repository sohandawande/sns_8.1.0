﻿
namespace Znode.Engine.Api.Attributes
{
    public class ExpandReferralCommissionType : ExpandAttribute
    {
        public override string Name { get { return "referralcommissiontype"; } }
        public override string Description { get { return "This will retrieve the related referral commission type."; } }
    }
}