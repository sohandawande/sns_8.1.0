﻿namespace Znode.Engine.Api.Attributes
{
    public class ExpandPromotionTypeAttribute : ExpandAttribute
    {
        public override string Name { get { return "promotionType"; } }
		public override string Description { get { return "This will retrieve the related promotion type."; } }
    }
}