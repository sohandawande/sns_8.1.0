﻿namespace Znode.Engine.Api.Attributes
{
	public class SortDomainNameAttribute : SortAttribute
	{
		public override string Name { get { return "domainName"; } }
		public override string Description { get { return "This will sort the response by domain name."; } }
	}
}