﻿namespace Znode.Engine.Api.Attributes
{
    
    public class FilterReviewStatusAttribute : FilterAttribute
    {
        public override string Name { get { return "status"; } }
        public override string Description { get { return "This will filter the request by review status."; } }
    }
}