﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterIsVisibleAttribute : FilterAttribute
	{
		public override string Name { get { return "isVisible"; } }
		public override string Description { get { return "This will filter the request by the is visible flag."; } }
	}
}