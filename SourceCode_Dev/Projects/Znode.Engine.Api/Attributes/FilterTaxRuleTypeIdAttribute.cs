﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterTaxRuleTypeIdAttribute : FilterAttribute
	{
		public override string Name { get { return "taxRuleTypeId"; } }
		public override string Description { get { return "This will filter the request by tax rule type ID."; } }
	}
}
