﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterPurchaseOrderNumberAttribute : FilterAttribute
	{
		public override string Name { get { return "purchaseOrderNumber"; } }
		public override string Description { get { return "This will filter the request by purchase order number."; } }
	}
}