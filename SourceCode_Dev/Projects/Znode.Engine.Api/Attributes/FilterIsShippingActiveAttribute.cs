﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterIsShippingActiveAttribute : FilterAttribute
	{
		public override string Name { get { return "isShippingActive"; } }
		public override string Description { get { return "This will filter the request by the is shipping active flag."; } }
	}
}