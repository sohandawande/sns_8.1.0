﻿
namespace Znode.Engine.Api.Attributes
{
    public class ExpandProductTagsAttribute : ExpandAttribute
    {
        public override string Name { get { return "producttags"; } }
        public override string Description { get { return "This will retrieve the product tags."; } }
    }
}