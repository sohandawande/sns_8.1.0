﻿namespace Znode.Engine.Api.Attributes
{
	public class ExpandShippingOptionAttribute : ExpandAttribute
	{
		public override string Name { get { return "shippingOption"; } }
		public override string Description { get { return "This will retrieve the latest shipping option."; } }
	}
}