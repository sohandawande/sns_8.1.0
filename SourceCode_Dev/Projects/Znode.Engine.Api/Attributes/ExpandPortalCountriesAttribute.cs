﻿namespace Znode.Engine.Api.Attributes
{
	public class ExpandPortalCountriesAttribute : ExpandAttribute
	{
		public override string Name { get { return "portalCountries"; } }
		public override string Description { get { return "This will retrieve the related list of portal countries."; } }
	}
}