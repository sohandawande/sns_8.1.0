﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterOrderLineItemIdAttribute : FilterAttribute
	{
		public override string Name { get { return "orderLineItemId"; } }
		public override string Description { get { return "This will filter the request by order line item ID."; } }
	}
}