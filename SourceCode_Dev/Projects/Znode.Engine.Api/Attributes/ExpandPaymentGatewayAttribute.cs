﻿namespace Znode.Engine.Api.Attributes
{
	public class ExpandPaymentGatewayAttribute : ExpandAttribute
	{
		public override string Name { get { return "paymentGateway"; } }
		public override string Description { get { return "This will retrieve the related payment gateway."; } }
	}
}