﻿namespace Znode.Engine.Api.Attributes
{
	public class SortPromotionIdAttribute : SortAttribute
	{
		public override string Name { get { return "promotionId"; } }
		public override string Description { get { return "This will sort the response by promotion ID."; } }
	}
}