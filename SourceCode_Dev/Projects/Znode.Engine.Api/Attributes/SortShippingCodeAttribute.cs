﻿namespace Znode.Engine.Api.Attributes
{
	public class SortShippingCodeAttribute : SortAttribute
	{
		public override string Name { get { return "shippingCode"; } }
		public override string Description { get { return "This will sort the response by shipping code."; } }
	}
}