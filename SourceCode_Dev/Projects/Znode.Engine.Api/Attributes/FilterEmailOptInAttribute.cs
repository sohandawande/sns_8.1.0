﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterEmailOptInAttribute : FilterAttribute
	{
		public override string Name { get { return "emailOptIn"; } }
		public override string Description { get { return "This will filter the request by the email opt in flag."; } }
	}
}
