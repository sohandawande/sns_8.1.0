﻿namespace Znode.Engine.Api.Attributes
{
    public class ExpandCatalogsAttribute : ExpandAttribute
    {
        public override string Name { get { return "catalogs"; } }
        public override string Description { get { return "This will retrieve the related catalogs."; } }
    }
}