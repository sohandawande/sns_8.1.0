﻿namespace Znode.Engine.Api.Attributes
{
	public class ExpandFrequentlyBoughtTogetherAttribute : ExpandAttribute
	{
		public override string Name { get { return "frequentlyboughttogether"; } }
		public override string Description { get { return "This will retrieve the list of products that are frequently bought together."; } }
	}
}