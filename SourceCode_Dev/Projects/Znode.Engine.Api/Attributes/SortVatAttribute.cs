﻿namespace Znode.Engine.Api.Attributes
{
	public class SortVatAttribute : SortAttribute
	{
		public override string Name { get { return "vat"; } }
		public override string Description { get { return "This will sort the response by VAT."; } }
	}
}