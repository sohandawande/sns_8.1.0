﻿
namespace Znode.Engine.Api.Attributes
{
    /// <summary>
    /// Filter for Content page
    /// </summary>
    public class FilterContentPageIdAttribute : FilterAttribute
    {
        public override string Name { get { return "contentPageId"; } }
        public override string Description { get { return "This will filter the request by content page ID."; } }
    }
}