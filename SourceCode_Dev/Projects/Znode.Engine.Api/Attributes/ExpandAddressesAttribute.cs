﻿namespace Znode.Engine.Api.Attributes
{
	public class ExpandAddressesAttribute : ExpandAttribute
	{
		public override string Name { get { return "addresses"; } }
		public override string Description { get { return "This will retrieve the related list of addresses."; } }
	}
}