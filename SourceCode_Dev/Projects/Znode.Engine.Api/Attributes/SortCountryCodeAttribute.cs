﻿namespace Znode.Engine.Api.Attributes
{
	public class SortCountryCodeAttribute : SortAttribute
	{
		public override string Name { get { return "countryCode"; } }
		public override string Description { get { return "This will sort the response by country code."; } }
	}
}