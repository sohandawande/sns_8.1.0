﻿namespace Znode.Engine.Api.Attributes
{
	public class ExpandSkusAttribute : ExpandAttribute
	{
		public override string Name { get { return "skus"; } }
		public override string Description { get { return "This will retrieve the related list of SKUs."; } }
	}
}