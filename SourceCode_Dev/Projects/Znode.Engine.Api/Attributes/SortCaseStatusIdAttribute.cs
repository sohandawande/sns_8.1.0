﻿namespace Znode.Engine.Api.Attributes
{
	public class SortCaseStatusIdAttribute : SortAttribute
	{
		public override string Name { get { return "caseStatusId"; } }
		public override string Description { get { return "This will sort the response by case status ID."; } }
	}
}