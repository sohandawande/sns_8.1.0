﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterAccountIdAttribute : FilterAttribute
	{
		public override string Name { get { return "accountId"; } }
		public override string Description { get { return "This will filter the request by account ID."; } }
	}
}