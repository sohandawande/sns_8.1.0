﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterLastNameAttribute : FilterAttribute
	{
		public override string Name { get { return "lastName"; } }
		public override string Description { get { return "This will filter the request by last name."; } }
	}
}