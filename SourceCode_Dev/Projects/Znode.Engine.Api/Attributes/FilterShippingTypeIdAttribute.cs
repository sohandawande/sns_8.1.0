﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterShippingTypeIdAttribute : FilterAttribute
	{
		public override string Name { get { return "shippingTypeId"; } }
		public override string Description { get { return "This will filter the request by shipping type ID."; } }
	}
}