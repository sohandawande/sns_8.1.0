﻿namespace Znode.Engine.Api.Attributes
{
	public class SortCatalogIdAttribute : SortAttribute
	{
		public override string Name { get { return "catalogId"; } }
		public override string Description { get { return "This will sort the response by catalog ID."; } }
	}
}