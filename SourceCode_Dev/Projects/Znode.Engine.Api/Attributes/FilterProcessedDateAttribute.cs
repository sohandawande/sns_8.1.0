﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterProcessedDateAttribute : FilterAttribute
	{
		public override string Name { get { return "processedDate"; } }
		public override string Description { get { return "This will filter the request by processed date."; } }
	}
}