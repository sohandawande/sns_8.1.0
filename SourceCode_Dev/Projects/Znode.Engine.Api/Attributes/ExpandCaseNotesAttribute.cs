﻿
namespace Znode.Engine.Api.Attributes
{
    public class ExpandCaseNotesAttribute : ExpandAttribute
    {
        public override string Name { get { return "caseNotes"; } }
        public override string Description { get { return "This will retrieve the related case notes."; } }
    }
}