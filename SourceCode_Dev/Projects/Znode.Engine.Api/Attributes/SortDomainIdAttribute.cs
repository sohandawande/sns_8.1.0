﻿namespace Znode.Engine.Api.Attributes
{
	public class SortDomainIdAttribute : SortAttribute
	{
		public override string Name { get { return "domainId"; } }
		public override string Description { get { return "This will sort the response by domain ID."; } }
	}
}