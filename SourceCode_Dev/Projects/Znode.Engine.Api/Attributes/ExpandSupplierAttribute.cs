﻿namespace Znode.Engine.Api.Attributes
{
	public class ExpandSupplierAttribute : ExpandAttribute
	{
		public override string Name { get { return "supplier"; } }
		public override string Description { get { return "This will retrieve the related supplier."; } }
	}
}