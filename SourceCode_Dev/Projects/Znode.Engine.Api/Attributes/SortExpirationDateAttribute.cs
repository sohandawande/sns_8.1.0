﻿namespace Znode.Engine.Api.Attributes
{
	public class SortExpirationDateAttribute : SortAttribute
	{
		public override string Name { get { return "expirationDate"; } }
		public override string Description { get { return "This will sort the response by expiration date."; } }
	}
}