﻿namespace Znode.Engine.Api.Attributes
{
	public class ExpandManufacturerAttribute : ExpandAttribute
	{
		public override string Name { get { return "manufacturer"; } }
		public override string Description { get { return "This will retrieve the related manufacturer."; } }
	}
}