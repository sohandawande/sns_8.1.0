﻿namespace Znode.Engine.Api.Attributes
{
	public class SortShippingRuleTypeIdAttribute : SortAttribute
	{
		public override string Name { get { return "shippingRuleTypeId"; } }
		public override string Description { get { return "This will sort the response by shipping rule type ID."; } }
	}
}