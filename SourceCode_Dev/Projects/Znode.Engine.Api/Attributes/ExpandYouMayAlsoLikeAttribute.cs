﻿namespace Znode.Engine.Api.Attributes
{
	public class ExpandYouMayAlsoLikeAttribute : ExpandAttribute
	{
		public override string Name { get { return "youMayAlsoLike"; } }
		public override string Description { get { return "This will retrieve the list of products that you may also like."; } }
	}
}