﻿namespace Znode.Engine.Api.Attributes
{
	public class SortSalePriceAttribute : SortAttribute
	{
		public override string Name { get { return "salePrice"; } }
		public override string Description { get { return "This will sort the response by sale price."; } }
	}
}