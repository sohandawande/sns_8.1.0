﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterEndDateAttribute : FilterAttribute
	{
		public override string Name { get { return "endDate"; } }
		public override string Description { get { return "This will filter the request by end date."; } }
	}
}