﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterCategoryIdAttribute : FilterAttribute
	{
		public override string Name { get { return "categoryId"; } }
		public override string Description { get { return "This will filter the request by category ID."; } }
	}
}