﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterSkuAttribute : FilterAttribute
	{
		public override string Name { get { return "sku"; } }
		public override string Description { get { return "This will filter the request by SKU."; } }
	}
}