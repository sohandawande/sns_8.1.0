﻿namespace Znode.Engine.Api.Attributes
{
	public class SortPortalCountryIdAttribute : SortAttribute
	{
		public override string Name { get { return "portalCountryId"; } }
		public override string Description { get { return "This will sort the response by portal country ID."; } }
	}
}