﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterAmountAttribute : FilterAttribute
	{
		public override string Name { get { return "amount"; } }
		public override string Description { get { return "This will filter the request by amount."; } }
	}
}