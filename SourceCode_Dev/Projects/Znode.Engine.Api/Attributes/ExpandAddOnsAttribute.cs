﻿namespace Znode.Engine.Api.Attributes
{
	public class ExpandAddOnsAttribute : ExpandAttribute
	{
		public override string Name { get { return "addOns"; } }
		public override string Description { get { return "This will retrieve the related list of addons."; } }
	}
}