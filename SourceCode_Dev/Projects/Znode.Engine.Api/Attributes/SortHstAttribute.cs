﻿namespace Znode.Engine.Api.Attributes
{
	public class SortHstAttribute : SortAttribute
	{
		public override string Name { get { return "hst"; } }
		public override string Description { get { return "This will sort the response by HST."; } }
	}
}