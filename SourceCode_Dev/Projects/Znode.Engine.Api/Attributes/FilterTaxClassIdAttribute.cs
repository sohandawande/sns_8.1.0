﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterTaxClassIdAttribute : FilterAttribute
	{
		public override string Name { get { return "taxClassId"; } }
		public override string Description { get { return "This will filter the request by tax class ID."; } }
	}
}
