﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterOwnerAccountIdAttribute : FilterAttribute
	{
		public override string Name { get { return "ownerAccountId"; } }
		public override string Description { get { return "This will filter the request by owner account ID."; } }
	}
}
