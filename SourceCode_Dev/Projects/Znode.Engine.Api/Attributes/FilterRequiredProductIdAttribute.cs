﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterRequiredProductIdAttribute : FilterAttribute
	{
		public override string Name { get { return "requiredProductId"; } }
		public override string Description { get { return "This will filter the request by required product ID."; } }
	}
}