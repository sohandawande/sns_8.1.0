﻿namespace Znode.Engine.Api.Attributes
{
    public class ExpandTaxClassAttribute : ExpandAttribute
    {
        public override string Name { get { return "taxClass"; } }
		public override string Description { get { return "This will retrieve the related tax class."; } }
    } 
}