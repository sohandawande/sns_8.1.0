﻿namespace Znode.Engine.Api.Attributes
{
	public class ExpandProductAttribute : ExpandAttribute
	{
		public override string Name { get { return "product"; } }
		public override string Description { get { return "This will retrieve the related product."; } }
	}
}