﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterPostalCodeAttribute : FilterAttribute
	{
		public override string Name { get { return "postalCode"; } }
		public override string Description { get { return "This will filter the request by postal code."; } }
	}
}