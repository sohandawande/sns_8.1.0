﻿namespace Znode.Engine.Api.Attributes
{
	public class ExpandCategoriesAttribute : ExpandAttribute
	{
		public override string Name { get { return "categories"; } }
		public override string Description { get { return "This will retrieve the related list of categories."; } }
	}
}