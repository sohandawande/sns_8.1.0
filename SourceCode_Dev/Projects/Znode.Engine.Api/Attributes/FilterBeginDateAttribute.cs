﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterBeginDateAttribute : FilterAttribute
	{
		public override string Name { get { return "beginDate"; } }
		public override string Description { get { return "This will filter the request by begin date."; } }
	}
}