﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterProfileIdAttribute : FilterAttribute
	{
		public override string Name { get { return "profileId"; } }
		public override string Description { get { return "This will filter the request by profile ID."; } }
	}
}