﻿namespace Znode.Engine.Api.Attributes
{
	public class SortShippingOptionIdAttribute : SortAttribute
	{
		public override string Name { get { return "shippingOptionId"; } }
		public override string Description { get { return "This will sort the response by shipping option ID."; } }
	}
}