﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterOrderStateIdAttribute : FilterAttribute
	{
		public override string Name { get { return "orderStateId"; } }
		public override string Description { get { return "This will filter the request by order state ID."; } }
	}
}
