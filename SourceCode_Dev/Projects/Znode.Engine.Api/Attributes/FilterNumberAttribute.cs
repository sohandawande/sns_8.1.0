﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterNumberAttribute : FilterAttribute
	{
		public override string Name { get { return "number"; } }
		public override string Description { get { return "This will filter the request by number."; } }
	}
}