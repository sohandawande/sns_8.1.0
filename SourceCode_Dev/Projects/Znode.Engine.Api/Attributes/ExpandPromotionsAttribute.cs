﻿namespace Znode.Engine.Api.Attributes
{
	public class ExpandPromotionsAttribute : ExpandAttribute
	{
		public override string Name { get { return "promotions"; } }
		public override string Description { get { return "This will retrieve the related list of promotions."; } }
	}
}