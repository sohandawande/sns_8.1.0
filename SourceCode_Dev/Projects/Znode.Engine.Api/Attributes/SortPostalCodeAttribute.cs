﻿namespace Znode.Engine.Api.Attributes
{
	public class SortPostalCodeAttribute : SortAttribute
	{
		public override string Name { get { return "postalCode"; } }
		public override string Description { get { return "This will sort the response by portal code."; } }
	}
}