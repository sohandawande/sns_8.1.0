﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.Api.Attributes
{
    public class FilterCreditApplicationIDAttribute:FilterAttribute
    {
        public override string Name { get { return "CreditApplicationID"; } }
        public override string Description { get { return "This will filter the request by Credit Application ID."; } }
        
    }
}