﻿namespace Znode.Engine.Api.Attributes
{
	public class SortQuantityOnHandAttribute : SortAttribute
	{
		public override string Name { get { return "quantityOnHand"; } }
		public override string Description { get { return "This will sort the response by quantity on hand."; } }
	}
}