﻿namespace Znode.Engine.Api.Attributes
{
	public class SortTaxRuleTypeIdAttribute : SortAttribute
	{
		public override string Name { get { return "taxRuleTypeId"; } }
		public override string Description { get { return "This will sort the response by tax rule type ID."; } }
	}
}