﻿namespace Znode.Engine.Api.Attributes
{
	public class ExpandOrderLineItemsAttribute : ExpandAttribute
	{
		public override string Name { get { return "orderLineItems"; } }
		public override string Description { get { return "This will retrieve the related list of order line items."; } }
	}
}