﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterShippingOptionIdAttribute : FilterAttribute
	{
		public override string Name { get { return "shippingOptionId"; } }
		public override string Description { get { return "This will filter the request by shipping option ID."; } }
	}
}
