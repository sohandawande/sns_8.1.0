﻿namespace Znode.Engine.Api.Attributes
{
    public class ExpandSupplierTypeAttribute : ExpandAttribute
    {
        public override string Name { get { return "supplierType"; } }
		public override string Description { get { return "This will retrieve the related supplier type."; } }
    } 
}