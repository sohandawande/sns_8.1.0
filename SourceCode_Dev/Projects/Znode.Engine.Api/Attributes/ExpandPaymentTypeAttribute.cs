﻿namespace Znode.Engine.Api.Attributes
{
	public class ExpandPaymentTypeAttribute : ExpandAttribute
	{
		public override string Name { get { return "paymentType"; } }
		public override string Description { get { return "This will retrieve the related payment type."; } }
	}
}