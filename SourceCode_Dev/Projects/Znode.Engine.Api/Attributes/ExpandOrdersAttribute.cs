﻿namespace Znode.Engine.Api.Attributes
{
	public class ExpandOrdersAttribute : ExpandAttribute
	{
		public override string Name { get { return "orders"; } }
		public override string Description { get { return "This will retrieve the related list of orders."; } }
	}
}