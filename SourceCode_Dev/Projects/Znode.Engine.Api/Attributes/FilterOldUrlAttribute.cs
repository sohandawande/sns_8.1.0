﻿namespace Znode.Engine.Api.Attributes
{
    /// <summary>
    /// 
    /// </summary>
    public class FilterOldUrlAttribute : FilterAttribute
    {
        /// <summary>
        /// 
        /// </summary>
        public override string Name { get { return "OldUrl"; } }
        /// <summary>
        /// 
        /// </summary>
        public override string Description { get { return "This will filter the request by Old Url."; } }
    }
}