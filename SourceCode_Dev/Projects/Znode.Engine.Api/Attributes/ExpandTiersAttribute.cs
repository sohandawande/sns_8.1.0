﻿namespace Znode.Engine.Api.Attributes
{
	public class ExpandTiersAttribute : ExpandAttribute
	{
		public override string Name { get { return "tiers"; } }
		public override string Description { get { return "This will retrieve the related tiers."; } }
	}
}