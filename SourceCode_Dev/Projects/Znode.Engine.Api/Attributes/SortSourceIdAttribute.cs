﻿namespace Znode.Engine.Api.Attributes
{
	public class SortSourceIdAttribute : SortAttribute
	{
		public override string Name { get { return "sourceId"; } }
		public override string Description { get { return "This will sort the response by source ID."; } }
	}
}