﻿namespace Znode.Engine.Api.Attributes
{
	public class SortNameAttribute : SortAttribute
	{
		public override string Name { get { return "name"; } }
		public override string Description { get { return "This will sort the response by name."; } }
	}
}