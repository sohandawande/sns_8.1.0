﻿namespace Znode.Engine.Api.Attributes
{
	public class SortPortalIdAttribute : SortAttribute
	{
		public override string Name { get { return "portalId"; } }
		public override string Description { get { return "This will sort the response by portal ID."; } }
	}
}