﻿namespace Znode.Engine.Api.Attributes
{
	public class ExpandProductIdsAttribute : ExpandAttribute
	{
		public override string Name { get { return "productIds"; } }
		public override string Description { get { return "This will retrieve the related list of product IDs."; } }
	}
}