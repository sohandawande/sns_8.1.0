﻿namespace Znode.Engine.Api.Attributes
{
	public class SortCaseTypeIdAttribute : SortAttribute
	{
		public override string Name { get { return "caseTypeId"; } }
		public override string Description { get { return "This will sort the response by case type ID."; } }
	}
}