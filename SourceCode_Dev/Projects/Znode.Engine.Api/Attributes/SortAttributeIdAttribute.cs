﻿namespace Znode.Engine.Api.Attributes
{
    public class SortAttributeIdAttribute : SortAttribute
    {
        public override string Name { get { return "attributeId"; } }
        public override string Description { get { return "This will sort the response by attribute ID."; } }
    }
}