﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterClassTypeAttribute : FilterAttribute
	{
		public override string Name { get { return "classType"; } }
		public override string Description { get { return "This will filter the request by class type."; } }
	}
}
