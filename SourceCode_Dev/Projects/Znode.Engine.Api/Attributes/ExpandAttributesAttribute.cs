﻿namespace Znode.Engine.Api.Attributes
{
    public class ExpandAttributesAttribute : ExpandAttribute
    {
        public override string Name { get { return "attributes"; } }
        public override string Description { get { return "This will retrieve the related list of attributes."; } }
    }
}