﻿namespace Znode.Engine.Api.Attributes
{
    public class ExpandTaxRuleTypeAttribute : ExpandAttribute
    {
        public override string Name { get { return "taxRuleType"; } }
		public override string Description { get { return "This will retrieve the related tax rule type."; } }
    } 
}