﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterCaseStatusIdAttribute : FilterAttribute
	{
		public override string Name { get { return "caseStatusId"; } }
		public override string Description { get { return "This will filter the request by case status ID."; } }
	}
}
