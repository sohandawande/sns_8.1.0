﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterCountryCodeAttribute : FilterAttribute
	{
		public override string Name { get { return "countryCode"; } }
		public override string Description { get { return "This will filter the request by country code."; } }
	}
}