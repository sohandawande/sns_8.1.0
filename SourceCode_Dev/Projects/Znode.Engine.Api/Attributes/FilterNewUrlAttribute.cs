﻿namespace Znode.Engine.Api.Attributes
{
    /// <summary>
    /// 
    /// </summary>
    public class FilterNewUrlAttribute :FilterAttribute
    {
        /// <summary>
        /// 
        /// </summary>
        public override string Name { get { return "NewUrl"; } }
        /// <summary>
        /// 
        /// </summary>
        public override string Description { get { return "This will filter the request by New Url."; } }
    }
}