﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterOrderDateAttribute : FilterAttribute
	{
		public override string Name { get { return "orderDate"; } }
		public override string Description { get { return "This will filter the request by order date."; } }
	}
}