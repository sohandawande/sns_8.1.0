﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterExternalSupplierNumberAttribute : FilterAttribute
	{
		public override string Name { get { return "externalSupplierNumber"; } }
		public override string Description { get { return "This will filter the request by external supplier number."; } }
	}
}