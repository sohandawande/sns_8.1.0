﻿namespace Znode.Engine.Api.Attributes
{
	public class SortDescriptionAttribute : SortAttribute
	{
		public override string Name { get { return "description"; } }
		public override string Description { get { return "This will sort the response by description."; } }
	}
}