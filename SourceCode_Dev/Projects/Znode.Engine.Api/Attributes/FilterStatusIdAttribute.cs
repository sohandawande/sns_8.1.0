﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterStatusIdAttribute : FilterAttribute
	{
		public override string Name { get { return "statusId"; } }
		public override string Description { get { return "This will filter the request by status ID."; } }
	}
}
