﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterPromotionTypeIdAttribute : FilterAttribute
	{
		public override string Name { get { return "promotionTypeId"; } }
		public override string Description { get { return "This will filter the request by promotion type ID."; } }
	}
}