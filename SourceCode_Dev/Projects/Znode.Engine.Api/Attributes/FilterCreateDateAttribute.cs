﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterCreateDateAttribute : FilterAttribute
	{
		public override string Name { get { return "createDate"; } }
		public override string Description { get { return "This will filter the request by create date."; } }
	}
}
