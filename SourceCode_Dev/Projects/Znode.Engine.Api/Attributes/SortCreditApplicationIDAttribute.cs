﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.Api.Attributes
{
    public class SortCreditApplicationIDAttribute : SortAttribute
    {
        public override string Name { get { return "CreditApplicationID"; } }
        public override string Description { get { return "This will sort the response by credit application ID."; } }
    }
}