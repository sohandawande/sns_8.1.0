﻿namespace Znode.Engine.Api.Attributes
{
	public class ExpandProductTypeAttribute : ExpandAttribute
	{
		public override string Name { get { return "productType"; } }
		public override string Description { get { return "This will retrieve the related product type."; } }
	}
}