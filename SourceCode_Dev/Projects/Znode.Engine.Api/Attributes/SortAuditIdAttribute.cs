﻿namespace Znode.Engine.Api.Attributes
{
	public class SortAuditIdAttribute : SortAttribute
	{
		public override string Name { get { return "auditId"; } }
		public override string Description { get { return "This will sort the response by audit ID."; } }
	}
}