﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.Api.Attributes
{
    public class SortIsCreditApprovedAttribute : SortAttribute
    {
        public override string Name { get { return "IsCreditApproved"; } }
        public override string Description { get { return "This will sort the response by Is Credit Approved flag."; } }
    }
}