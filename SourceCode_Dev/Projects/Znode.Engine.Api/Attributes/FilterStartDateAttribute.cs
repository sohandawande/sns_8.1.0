﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterStartDateAttribute : FilterAttribute
	{
		public override string Name { get { return "startDate"; } }
		public override string Description { get { return "This will filter the request by start date."; } }
	}
}