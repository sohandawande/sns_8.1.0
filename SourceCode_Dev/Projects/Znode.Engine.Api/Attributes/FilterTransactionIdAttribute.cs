﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterTransactionIdAttribute : FilterAttribute
	{
		public override string Name { get { return "transactionId"; } }
		public override string Description { get { return "This will filter the request by transaction ID."; } }
	}
}