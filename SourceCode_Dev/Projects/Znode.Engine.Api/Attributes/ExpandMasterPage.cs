﻿
namespace Znode.Engine.Api.Attributes
{
    /// <summary>
    /// Expands for master page
    /// </summary>
    public class ExpandMasterPage : ExpandAttribute
    {
        public override string Name { get { return "masterPage"; } }
        public override string Description { get { return "This will retrieve the related list of masterPage."; } }
    }
}