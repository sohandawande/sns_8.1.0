﻿namespace Znode.Engine.Api.Attributes
{
	public class SortDisplayOrderAttribute : SortAttribute
	{
		public override string Name { get { return "displayOrder"; } }
		public override string Description { get { return "This will sort the response by display order."; } }
	}
}