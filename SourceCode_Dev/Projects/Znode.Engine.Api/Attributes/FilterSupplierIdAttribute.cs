﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterSupplierIdAttribute : FilterAttribute
	{
		public override string Name { get { return "supplierId"; } }
		public override string Description { get { return "This will filter the request by supplier ID."; } }
	}
}