﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterEmailAttribute : FilterAttribute
	{
		public override string Name { get { return "email"; } }
		public override string Description { get { return "This will filter the request by email."; } }
	}
}