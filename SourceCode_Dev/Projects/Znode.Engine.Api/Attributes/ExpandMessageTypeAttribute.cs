﻿namespace Znode.Engine.Api.Attributes
{
	public class ExpandMessageTypeAttribute : ExpandAttribute
	{
		public override string Name { get { return "messageType"; } }
		public override string Description { get { return "This will retrieve the related message type."; } }
	}
}