﻿namespace Znode.Engine.Api.Attributes
{
	public class SortPortalCatalogIdAttribute : SortAttribute
	{
		public override string Name { get { return "portalCatalogId"; } }
		public override string Description { get { return "This will sort the response by portal catalog ID."; } }
	}
}