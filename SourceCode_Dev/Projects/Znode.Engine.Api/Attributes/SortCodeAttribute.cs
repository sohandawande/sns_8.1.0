﻿namespace Znode.Engine.Api.Attributes
{
	public class SortCodeAttribute : SortAttribute
	{
		public override string Name { get { return "code"; } }
		public override string Description { get { return "This will sort the response by code."; } }
	}
}