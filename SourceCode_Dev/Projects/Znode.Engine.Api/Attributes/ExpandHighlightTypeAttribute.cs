﻿namespace Znode.Engine.Api.Attributes
{
	public class ExpandHighlightTypeAttribute : ExpandAttribute
	{
		public override string Name { get { return "highlightType"; } }
		public override string Description { get { return "This will retrieve the related highlight type."; } }
	}
}