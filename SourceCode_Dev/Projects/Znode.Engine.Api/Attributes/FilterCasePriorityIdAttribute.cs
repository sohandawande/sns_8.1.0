﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterCasePriorityIdAttribute : FilterAttribute
	{
		public override string Name { get { return "casePriorityId"; } }
		public override string Description { get { return "This will filter the request by case priority ID."; } }
	}
}
