﻿namespace Znode.Engine.Api.Attributes
{
	public class ExpandGiftCardHistoryAttribute : ExpandAttribute
	{
		public override string Name { get { return "giftCardHistory"; } }
		public override string Description { get { return "This will retrieve the related gift card history."; } }
	}
}