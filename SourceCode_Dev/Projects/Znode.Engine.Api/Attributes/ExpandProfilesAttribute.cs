﻿namespace Znode.Engine.Api.Attributes
{
	public class ExpandProfilesAttribute : ExpandAttribute
	{
		public override string Name { get { return "profiles"; } }
		public override string Description { get { return "This will retrieve the related list of profiles."; } }
	}
}