﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterIsNewAttribute : FilterAttribute
	{
		public override string Name { get { return "isNew"; } }
		public override string Description { get { return "This will filter the request by the is new flag."; } }
	}
}
