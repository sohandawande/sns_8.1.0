﻿namespace Znode.Engine.Api.Attributes
{
	public class SortTotalAttribute : SortAttribute
	{
		public override string Name { get { return "total"; } }
		public override string Description { get { return "This will sort the response by total."; } }
	}
}