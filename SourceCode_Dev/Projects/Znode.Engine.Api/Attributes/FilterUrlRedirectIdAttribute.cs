﻿namespace Znode.Engine.Api.Attributes
{
    /// <summary>
    /// 
    /// </summary>
    public class FilterUrlRedirectIdAttribute :FilterAttribute
    {
        /// <summary>
        /// 
        /// </summary>
        public override string Name { get { return "urlRedirectId"; } }
        /// <summary>
        /// 
        /// </summary>
        public override string Description { get { return "This will filter the request by Url Redirect Id."; } }
    }
}