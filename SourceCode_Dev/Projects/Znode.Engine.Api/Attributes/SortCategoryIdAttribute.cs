﻿namespace Znode.Engine.Api.Attributes
{
	public class SortCategoryIdAttribute : SortAttribute
	{
		public override string Name { get { return "categoryId"; } }
		public override string Description { get { return "This will sort the response by category ID."; } }
	}
}