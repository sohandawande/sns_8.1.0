﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.Api.Attributes 
{
    public class FilterIsCreditApprovedAttribute : FilterAttribute
    {
        public override string Name { get { return "IsCreditApproved"; } }
        public override string Description { get { return "This will filter the request by is credit approved flag."; } }
    }
}