﻿namespace Znode.Engine.Api.Attributes
{
	public class SortMessageConfigIdAttribute : SortAttribute
	{
		public override string Name { get { return "messageConfigId"; } }
		public override string Description { get { return "This will sort the response by message config ID."; } }
	}
}