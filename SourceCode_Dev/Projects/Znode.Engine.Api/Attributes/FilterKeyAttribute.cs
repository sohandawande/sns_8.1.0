﻿namespace Znode.Engine.Api.Attributes
{
    /// <summary>
    /// Filter Key Attribute for Message Config
    /// </summary>
    public class FilterKeyAttribute : FilterAttribute
    {
        public override string Name { get { return "Key"; } }
        public override string Description { get { return "This will filter the request by the Message config Key."; } }
    }
}
