﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterManufacturerIdAttribute : FilterAttribute
	{
		public override string Name { get { return "manufacturerId"; } }
		public override string Description { get { return "This will filter the request by manufacturer ID."; } }
	}
}