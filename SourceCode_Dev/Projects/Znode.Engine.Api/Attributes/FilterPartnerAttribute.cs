﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterPartnerAttribute : FilterAttribute
	{
		public override string Name { get { return "partner"; } }
		public override string Description { get { return "This will filter the request by partner."; } }
	}
}