﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterLocaleIdAttribute : FilterAttribute
	{
		public override string Name { get { return "localeId"; } }
		public override string Description { get { return "This will filter the request by locale ID."; } }
	}
}
