﻿namespace Znode.Engine.Api.Attributes
{
	public class ExpandAccountTypeAttribute : ExpandAttribute
	{
		public override string Name { get { return "accountType"; } }
		public override string Description { get { return "This will retrieve the related account type."; } }
	}
}