﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterAccountTypeIdAttribute : FilterAttribute
	{
		public override string Name { get { return "accountTypeId"; } }
		public override string Description { get { return "This will filter the request by account type ID."; } }
	}
}