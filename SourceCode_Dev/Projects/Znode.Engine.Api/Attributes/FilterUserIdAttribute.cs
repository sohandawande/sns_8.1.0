﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterUserIdAttribute : FilterAttribute
	{
		public override string Name { get { return "userId"; } }
		public override string Description { get { return "This will filter the request by user ID."; } }
	}
}
