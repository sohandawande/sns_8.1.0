﻿namespace Znode.Engine.Api.Attributes
{
    public class ExpandPortal: ExpandAttribute
	{
		public override string Name { get { return "portal"; } }
		public override string Description { get { return "This will retrieve the related list of portals."; } }
	}
}