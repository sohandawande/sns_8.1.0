﻿namespace Znode.Engine.Api.Attributes
{
	public class SortRatingAttribute : SortAttribute
	{
		public override string Name { get { return "rating"; } }
		public override string Description { get { return "This will sort the response by rating."; } }
	}
}