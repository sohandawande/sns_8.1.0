﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterShippingRuleTypeIdAttribute : FilterAttribute
	{
		public override string Name { get { return "shippingRuleTypeId"; } }
		public override string Description { get { return "This will filter the request by shipping rule type ID."; } }
	}
}
