﻿namespace Znode.Engine.Api.Attributes
{
	public class SortHighlightTypeIdAttribute : SortAttribute
	{
		public override string Name { get { return "highlightTypeId"; } }
		public override string Description { get { return "This will sort the response by highlight type ID."; } }
	}
}