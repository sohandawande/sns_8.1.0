﻿namespace Znode.Engine.Api.Attributes
{
	public class ExpandAuditStatusAttribute : ExpandAttribute
	{
		public override string Name { get { return "auditStatus"; } }
		public override string Description { get { return "This will retrieve the related audit status."; } }
	}
}