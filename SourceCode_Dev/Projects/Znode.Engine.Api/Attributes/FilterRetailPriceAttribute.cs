﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterRetailPriceAttribute : FilterAttribute
	{
		public override string Name { get { return "retailPrice"; } }
		public override string Description { get { return "This will filter the request by retail price."; } }
	}
}