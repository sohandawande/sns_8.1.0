﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterCaseTypeIdAttribute : FilterAttribute
	{
		public override string Name { get { return "caseTypeId"; } }
		public override string Description { get { return "This will filter the request by case type ID."; } }
	}
}
