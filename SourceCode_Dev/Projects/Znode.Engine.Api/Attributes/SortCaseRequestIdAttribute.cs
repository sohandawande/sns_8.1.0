﻿namespace Znode.Engine.Api.Attributes
{
	public class SortCaseRequestIdAttribute : SortAttribute
	{
		public override string Name { get { return "caseRequestId"; } }
		public override string Description { get { return "This will sort the response by case request ID."; } }
	}
}