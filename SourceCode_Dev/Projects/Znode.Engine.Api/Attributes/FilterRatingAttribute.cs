﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterRatingAttribute : FilterAttribute
	{
		public override string Name { get { return "rating"; } }
		public override string Description { get { return "This will filter the request by rating."; } }
	}
}
