﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterSupplierTypeIdAttribute : FilterAttribute
	{
		public override string Name { get { return "supplierTypeId"; } }
		public override string Description { get { return "This will filter the request by supplier type ID."; } }
	}
}
