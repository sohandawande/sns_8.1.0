﻿namespace Znode.Engine.Api.Attributes
{
	public class SortCasePriorityIdAttribute : SortAttribute
	{
		public override string Name { get { return "casePriorityId"; } }
		public override string Description { get { return "This will sort the response by case priority ID."; } }
	}
}