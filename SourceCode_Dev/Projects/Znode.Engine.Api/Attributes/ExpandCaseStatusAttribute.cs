﻿namespace Znode.Engine.Api.Attributes
{
	public class ExpandCaseStatusAttribute : ExpandAttribute
	{
		public override string Name { get { return "caseStatus"; } }
		public override string Description { get { return "This will retrieve the related case status."; } }
	}
}