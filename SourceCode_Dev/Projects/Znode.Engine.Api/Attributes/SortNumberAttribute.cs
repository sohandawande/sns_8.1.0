﻿namespace Znode.Engine.Api.Attributes
{
	public class SortNumberAttribute : SortAttribute
	{
		public override string Name { get { return "number"; } }
		public override string Description { get { return "This will sort the response by number."; } }
	}
}