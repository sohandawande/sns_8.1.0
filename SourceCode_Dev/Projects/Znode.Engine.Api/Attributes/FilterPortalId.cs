﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterPortalIdAttribute : FilterAttribute
	{
		public override string Name { get { return "portalId"; } }
		public override string Description { get { return "This will filter the request by portal ID."; } }
	}
}