﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterQuantityOnHandAttribute : FilterAttribute
	{
		public override string Name { get { return "quantityOnHand"; } }
		public override string Description { get { return "This will filter the request by quantity on hand."; } }
	}
}