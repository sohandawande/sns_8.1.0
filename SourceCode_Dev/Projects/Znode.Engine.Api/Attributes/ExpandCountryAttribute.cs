﻿namespace Znode.Engine.Api.Attributes
{
	public class ExpandCountryAttribute : ExpandAttribute
	{
		public override string Name { get { return "country"; } }
		public override string Description { get { return "This will retrieve the related country."; } }
	}
}