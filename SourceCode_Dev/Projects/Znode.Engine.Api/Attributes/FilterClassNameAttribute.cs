﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterClassNameAttribute : FilterAttribute
	{
		public override string Name { get { return "className"; } }
		public override string Description { get { return "This will filter the request by class name."; } }
	}
}
