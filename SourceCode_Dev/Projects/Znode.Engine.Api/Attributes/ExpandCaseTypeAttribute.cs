﻿namespace Znode.Engine.Api.Attributes
{
	public class ExpandCaseTypeAttribute : ExpandAttribute
	{
		public override string Name { get { return "caseType"; } }
		public override string Description { get { return "This will retrieve the related case type."; } }
	}
}