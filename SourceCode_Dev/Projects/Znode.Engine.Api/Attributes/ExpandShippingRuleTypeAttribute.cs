﻿namespace Znode.Engine.Api.Attributes
{
	public class ExpandShippingRuleTypeAttribute : ExpandAttribute
	{
		public override string Name { get { return "shippingRuleType"; } }
		public override string Description { get { return "This will retrieve the related shipping rule type."; } }
	}
}