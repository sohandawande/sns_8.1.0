﻿namespace Znode.Engine.Api.Attributes
{
	public class ExpandUserAttribute : ExpandAttribute
	{
		public override string Name { get { return "user"; } }
		public override string Description { get { return "This will retrieve the related user."; } }
	}
}