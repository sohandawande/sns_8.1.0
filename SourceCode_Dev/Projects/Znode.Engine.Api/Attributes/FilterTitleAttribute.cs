﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterTitleAttribute : FilterAttribute
	{
		public override string Name { get { return "title"; } }
		public override string Description { get { return "This will filter the request by title."; } }
	}
}