﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterCodeAttribute : FilterAttribute
	{
		public override string Name { get { return "code"; } }
		public override string Description { get { return "This will filter the request by code."; } }
	}
}
