﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterDescriptionAttribute : FilterAttribute
	{
		public override string Name { get { return "description"; } }
		public override string Description { get { return "This will filter the request by description."; } }
	}
}
