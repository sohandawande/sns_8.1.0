﻿namespace Znode.Engine.Api.Attributes
{
	public class ExpandAccountAttribute : ExpandAttribute
	{
		public override string Name { get { return "account"; } }
		public override string Description { get { return "This will retrieve the related account."; } }
	}
}