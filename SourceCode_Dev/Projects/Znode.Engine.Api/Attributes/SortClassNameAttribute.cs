﻿namespace Znode.Engine.Api.Attributes
{
	public class SortClassNameAttribute : SortAttribute
	{
		public override string Name { get { return "className"; } }
		public override string Description { get { return "This will sort the response by class name."; } }
	}
}