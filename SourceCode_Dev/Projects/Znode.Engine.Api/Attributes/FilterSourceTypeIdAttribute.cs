﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterSourceTypeIdAttribute : FilterAttribute
	{
		public override string Name { get { return "sourceTypeId"; } }
		public override string Description { get { return "This will filter the request by source type ID."; } }
	}
}