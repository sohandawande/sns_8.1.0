﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterSourceIdAttribute : FilterAttribute
	{
		public override string Name { get { return "sourceId"; } }
		public override string Description { get { return "This will filter the request by source ID."; } }
	}
}