﻿namespace Znode.Engine.Api.Attributes
{
    public class SortRelevanceAttribute : SortAttribute
    {
        public override string Name { get { return "relevance"; } }
        public override string Description { get { return "This will sort the response by relevance."; } }
    }
}