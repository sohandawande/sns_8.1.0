﻿namespace Znode.Engine.Api.Attributes
{
	public class ExpandImagesAttribute : ExpandAttribute
	{
		public override string Name { get { return "images"; } }
		public override string Description { get { return "This will retrieve the related list of images."; } }
	}
}