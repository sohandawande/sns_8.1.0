﻿namespace Znode.Engine.Api.Attributes
{
	public class SortPstAttribute : SortAttribute
	{
		public override string Name { get { return "pst"; } }
		public override string Description { get { return "This will sort the response by PST."; } }
	}
}