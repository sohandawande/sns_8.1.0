﻿namespace Znode.Engine.Api.Attributes
{
	public class ExpandSubcategoriesAttribute : ExpandAttribute
	{
		public override string Name { get { return "subcategories"; } }
		public override string Description { get { return "This will retrieve the related list of subcategories."; } }
	}
}
