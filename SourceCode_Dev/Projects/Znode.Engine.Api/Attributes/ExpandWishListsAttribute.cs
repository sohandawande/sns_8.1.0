﻿namespace Znode.Engine.Api.Attributes
{
	public class ExpandWishListAttribute : ExpandAttribute
	{
		public override string Name { get { return "wishList"; } }
		public override string Description { get { return "This will retrieve the related list of wish lists."; } }
	}
}