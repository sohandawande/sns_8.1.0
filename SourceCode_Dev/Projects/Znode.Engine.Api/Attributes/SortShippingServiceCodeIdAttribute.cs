﻿namespace Znode.Engine.Api.Attributes
{
	public class SortShippingServiceCodeIdAttribute : SortAttribute
	{
		public override string Name { get { return "shippingServiceCodeId"; } }
		public override string Description { get { return "This will sort the response by shipping service code ID."; } }
	}
}