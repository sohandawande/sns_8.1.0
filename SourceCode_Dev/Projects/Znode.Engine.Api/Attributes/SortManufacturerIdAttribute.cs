﻿namespace Znode.Engine.Api.Attributes
{
	public class SortManufacturerIdAttribute : SortAttribute
	{
		public override string Name { get { return "manufacturerId"; } }
		public override string Description { get { return "This will sort the response by manufacturer ID."; } }
	}
}