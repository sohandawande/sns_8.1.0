﻿namespace Znode.Engine.Api.Attributes
{
	public class SortReviewIdAttribute : SortAttribute
	{
		public override string Name { get { return "reviewId"; } }
		public override string Description { get { return "This will sort the response by review ID."; } }
	}
}