﻿namespace Znode.Engine.Api.Attributes
{
	public class SortOrderIdAttribute : SortAttribute
	{
		public override string Name { get { return "orderId"; } }
		public override string Description { get { return "This will sort the response by order ID."; } }
	}
}