﻿namespace Znode.Engine.Api.Attributes
{
	public class SortHighlightIdAttribute : SortAttribute
	{
		public override string Name { get { return "highlightId"; } }
		public override string Description { get { return "This will sort the response by highlight ID."; } }
	}
}