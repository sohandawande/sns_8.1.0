﻿namespace Znode.Engine.Api.Attributes
{
	public class SortAmountAttribute : SortAttribute
	{
		public override string Name { get { return "amount"; } }
		public override string Description { get { return "This will sort the response by amount."; } }
	}
}