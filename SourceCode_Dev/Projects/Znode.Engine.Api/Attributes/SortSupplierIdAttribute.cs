﻿namespace Znode.Engine.Api.Attributes
{
	public class SortSupplierIdAttribute : SortAttribute
	{
		public override string Name { get { return "supplierId"; } }
		public override string Description { get { return "This will sort the response by supplier ID."; } }
	}
}