﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterParentAccountIdAttribute : FilterAttribute
	{
		public override string Name { get { return "parentAccountId"; } }
		public override string Description { get { return "This will filter the request by parent account ID."; } }
	}
}
