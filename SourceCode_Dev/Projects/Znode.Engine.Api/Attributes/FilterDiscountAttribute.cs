﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterDiscountAttribute : FilterAttribute
	{
		public override string Name { get { return "discount"; } }
		public override string Description { get { return "This will filter the request by discount."; } }
	}
}