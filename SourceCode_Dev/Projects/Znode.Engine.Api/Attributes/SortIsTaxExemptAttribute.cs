﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.Api.Attributes
{
    public class SortIsTaxExemptAttribute : SortAttribute
    {
        public override string Name { get { return "IsTaxExempt"; } }
        public override string Description { get { return "This will sort the response by Is Tax Exempt."; } }
    }
}