﻿namespace Znode.Engine.Api.Attributes
{
	public class ExpandAuditTypeAttribute : ExpandAttribute
	{
		public override string Name { get { return "auditType"; } }
		public override string Description { get { return "This will retrieve the related audit type."; } }
	}
}