﻿namespace Znode.Engine.Api.Attributes
{
	public class SortCardNumberAttribute : SortAttribute
	{
		public override string Name { get { return "cardNumber"; } }
		public override string Description { get { return "This will sort the response by card number."; } }
	}
}