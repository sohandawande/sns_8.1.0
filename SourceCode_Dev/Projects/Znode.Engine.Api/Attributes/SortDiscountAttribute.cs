﻿namespace Znode.Engine.Api.Attributes
{
	public class SortDiscountAttribute : SortAttribute
	{
		public override string Name { get { return "discount"; } }
		public override string Description { get { return "This will sort the response by discount."; } }
	}
}