﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterCityAttribute : FilterAttribute
	{
		public override string Name { get { return "city"; } }
		public override string Description { get { return "This will filter the request by city."; } }
	}
}
