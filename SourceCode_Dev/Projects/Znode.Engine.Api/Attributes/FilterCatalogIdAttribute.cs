﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterCatalogIdAttribute : FilterAttribute
	{
		public override string Name { get { return "catalogId"; } }
		public override string Description { get { return "This will filter the request by catalog ID."; } }
	}
}