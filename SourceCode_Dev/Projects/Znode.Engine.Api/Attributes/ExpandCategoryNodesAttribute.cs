﻿namespace Znode.Engine.Api.Attributes
{
	public class ExpandCategoryNodesAttribute : ExpandAttribute
	{
		public override string Name { get { return "categoryNodes"; } }
		public override string Description { get { return "This will retrieve the related list of category nodes."; } }
	}
}