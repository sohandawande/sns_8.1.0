﻿namespace Znode.Engine.Api.Attributes
{
	public class SortOrderDateAttribute : SortAttribute
	{
		public override string Name { get { return "orderDate"; } }
		public override string Description { get { return "This will sort the response by order date."; } }
	}
}