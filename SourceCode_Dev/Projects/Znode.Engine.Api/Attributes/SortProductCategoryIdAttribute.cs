﻿namespace Znode.Engine.Api.Attributes
{
	public class SortProductCategoryIdAttribute : SortAttribute
	{
		public override string Name { get { return "productCategoryId"; } }
		public override string Description { get { return "This will sort the response by product category ID."; } }
	}
}