﻿namespace Znode.Engine.Api.Attributes
{
	public class SortPaymentTypeIdAttribute : SortAttribute
	{
		public override string Name { get { return "paymentTypeId"; } }
		public override string Description { get { return "This will sort the response by payment type ID."; } }
	}
}