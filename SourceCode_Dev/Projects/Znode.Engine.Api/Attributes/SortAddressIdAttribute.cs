﻿namespace Znode.Engine.Api.Attributes
{
	public class SortAddressIdAttribute : SortAttribute
	{
		public override string Name { get { return "addressId"; } }
		public override string Description { get { return "This will sort the response by address ID."; } }
	}
}