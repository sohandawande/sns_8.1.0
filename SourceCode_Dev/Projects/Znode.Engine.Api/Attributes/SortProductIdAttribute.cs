﻿namespace Znode.Engine.Api.Attributes
{
	public class SortProductIdAttribute : SortAttribute
	{
		public override string Name { get { return "productId"; } }
		public override string Description { get { return "This will sort the response by product ID."; } }
	}
}