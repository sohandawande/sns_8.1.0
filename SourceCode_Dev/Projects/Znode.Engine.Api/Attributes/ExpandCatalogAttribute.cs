﻿namespace Znode.Engine.Api.Attributes
{
	public class ExpandCatalogAttribute : ExpandAttribute
	{
		public override string Name { get { return "catalog"; } }
		public override string Description { get { return "This will retrieve the related catalog."; } }
	}
}