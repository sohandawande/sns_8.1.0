﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterProductIdAttribute : FilterAttribute
	{
		public override string Name { get { return "productId"; } }
		public override string Description { get { return "This will filter the request by product ID."; } }
	}
}