﻿namespace Znode.Engine.Api.Attributes
{
	public class ExpandAuditSourceTypeAttribute : ExpandAttribute
	{
		public override string Name { get { return "auditSourceType"; } }
		public override string Description { get { return "This will retrieve the related audit source type."; } }
	}
}