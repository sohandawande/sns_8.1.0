﻿namespace Znode.Engine.Api.Attributes
{
	public class SortTaxRuleIdAttribute : SortAttribute
	{
		public override string Name { get { return "taxRuleId"; } }
		public override string Description { get { return "This will sort the response by tax rule ID."; } }
	}
}