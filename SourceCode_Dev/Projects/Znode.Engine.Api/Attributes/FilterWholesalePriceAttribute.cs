﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterWholesalePriceAttribute : FilterAttribute
	{
		public override string Name { get { return "wholesalePrice"; } }
		public override string Description { get { return "This will filter the request by wholesale price."; } }
	}
}