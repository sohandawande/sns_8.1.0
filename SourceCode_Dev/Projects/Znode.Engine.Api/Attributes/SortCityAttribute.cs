﻿namespace Znode.Engine.Api.Attributes
{
	public class SortCityAttribute : SortAttribute
	{
		public override string Name { get { return "city"; } }
		public override string Description { get { return "This will sort the response by city."; } }
	}
}