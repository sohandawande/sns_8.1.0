﻿namespace Znode.Engine.Api.Attributes
{
	public class SortShippingRuleIdAttribute : SortAttribute
	{
		public override string Name { get { return "shippingRuleId"; } }
		public override string Description { get { return "This will sort the response by shipping rule ID."; } }
	}
}