﻿namespace Znode.Engine.Api.Attributes
{
	public class SortCategoryNodeIdAttribute : SortAttribute
	{
		public override string Name { get { return "categoryNodeId"; } }
		public override string Description { get { return "This will sort the response by category node ID."; } }
	}
}