﻿namespace Znode.Engine.Api.Attributes
{
	public class SortBaseCostAttribute : SortAttribute
	{
		public override string Name { get { return "baseCost"; } }
		public override string Description { get { return "This will sort the response by base cost."; } }
	}
}