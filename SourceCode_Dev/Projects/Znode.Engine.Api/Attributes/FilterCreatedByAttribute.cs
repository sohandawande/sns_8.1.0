﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterCreatedByAttribute : FilterAttribute
	{
		public override string Name { get { return "createdBy"; } }
		public override string Description { get { return "This will filter the request by created by."; } }
	}
}
