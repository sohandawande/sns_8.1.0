﻿namespace Znode.Engine.Api.Attributes
{
	public class SortShippingTypeIdAttribute : SortAttribute
	{
		public override string Name { get { return "shippingTypeId"; } }
		public override string Description { get { return "This will sort the response by shipping type ID."; } }
	}
}