﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterProductTypeIdAttribute : FilterAttribute
	{
		public override string Name { get { return "productTypeId"; } }
		public override string Description { get { return "This will filter the request by product type ID."; } }
	}
}