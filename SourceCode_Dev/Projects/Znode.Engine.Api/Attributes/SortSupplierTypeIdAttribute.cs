﻿namespace Znode.Engine.Api.Attributes
{
	public class SortSupplierTypeIdAttribute : SortAttribute
	{
		public override string Name { get { return "supplierTypeId"; } }
		public override string Description { get { return "This will sort the response by supplier type ID."; } }
	}
}