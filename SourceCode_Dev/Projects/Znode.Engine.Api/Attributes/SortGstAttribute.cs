﻿namespace Znode.Engine.Api.Attributes
{
	public class SortGstAttribute : SortAttribute
	{
		public override string Name { get { return "gst"; } }
		public override string Description { get { return "This will sort the response by GST."; } }
	}
}