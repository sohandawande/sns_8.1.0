﻿namespace Znode.Engine.Api.Attributes
{
	public class SortSourceTypeIdAttribute : SortAttribute
	{
		public override string Name { get { return "sourceTypeId"; } }
		public override string Description { get { return "This will sort the response by source type ID."; } }
	}
}