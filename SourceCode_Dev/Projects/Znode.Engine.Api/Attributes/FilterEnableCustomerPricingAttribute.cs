﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterEnableCustomerPricingAttribute : FilterAttribute
	{
		public override string Name { get { return "enableCustomerPricing"; } }
		public override string Description { get { return "This will filter the request by the enable customer pricing flag."; } }
	}
}
