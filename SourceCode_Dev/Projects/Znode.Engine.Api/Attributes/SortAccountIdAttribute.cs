﻿namespace Znode.Engine.Api.Attributes
{
	public class SortAccountIdAttribute : SortAttribute
	{
		public override string Name { get { return "accountId"; } }
		public override string Description { get { return "This will sort the response by account ID."; } }
	}
}