﻿namespace Znode.Engine.Api.Attributes
{
	public class SortStateCodeAttribute : SortAttribute
	{
		public override string Name { get { return "stateCode"; } }
		public override string Description { get { return "This will sort the response by state code."; } }
	}
}