﻿namespace Znode.Engine.Api.Attributes
{
	public class SortCompanyNameAttribute : SortAttribute
	{
		public override string Name { get { return "companyName"; } }
		public override string Description { get { return "This will sort the response by company name."; } }
	}
}