﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterParentCategoryNodeIdAttribute : FilterAttribute
	{
		public override string Name { get { return "parentCategoryNodeId"; } }
		public override string Description { get { return "This will filter the request by parent category node ID."; } }
	}
}
