﻿namespace Znode.Engine.Api.Attributes
{
	public class SortAuditTypeIdAttribute : SortAttribute
	{
		public override string Name { get { return "auditTypeId"; } }
		public override string Description { get { return "This will sort the response by audit type ID."; } }
	}
}