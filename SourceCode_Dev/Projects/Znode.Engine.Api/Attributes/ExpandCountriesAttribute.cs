﻿namespace Znode.Engine.Api.Attributes
{
	public class ExpandCountriesAttribute : ExpandAttribute
	{
		public override string Name { get { return "countries"; } }
		public override string Description { get { return "This will retrieve the related list of countries."; } }
	}
}