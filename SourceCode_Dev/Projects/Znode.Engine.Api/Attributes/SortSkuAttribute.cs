﻿namespace Znode.Engine.Api.Attributes
{
	public class SortSkuAttribute : SortAttribute
	{
		public override string Name { get { return "sku"; } }
		public override string Description { get { return "This will sort the response by SKU."; } }
	}
}