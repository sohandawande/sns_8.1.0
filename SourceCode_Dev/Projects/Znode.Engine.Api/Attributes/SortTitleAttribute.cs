﻿namespace Znode.Engine.Api.Attributes
{
	public class SortTitleAttribute : SortAttribute
	{
		public override string Name { get { return "title"; } }
		public override string Description { get { return "This will sort the response by title."; } }
	}
}