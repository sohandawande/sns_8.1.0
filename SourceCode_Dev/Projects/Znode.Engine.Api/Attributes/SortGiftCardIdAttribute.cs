﻿namespace Znode.Engine.Api.Attributes
{
	public class SortGiftCardIdAttribute : SortAttribute
	{
		public override string Name { get { return "giftCardId"; } }
		public override string Description { get { return "This will sort the response by gift card ID."; } }
	}
}