﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterStoreNameAttribute : FilterAttribute
	{
		public override string Name { get { return "storeName"; } }
		public override string Description { get { return "This will filter the request by store name."; } }
	}
}