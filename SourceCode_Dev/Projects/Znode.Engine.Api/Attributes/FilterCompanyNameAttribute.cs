﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterCompanyNameAttribute : FilterAttribute
	{
		public override string Name { get { return "companyName"; } }
		public override string Description { get { return "This will filter the request by company name."; } }
	}
}