﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterTrackingNumberAttribute : FilterAttribute
	{
		public override string Name { get { return "trackingNumber"; } }
		public override string Description { get { return "This will filter the request by tracking number."; } }
	}
}