﻿namespace Znode.Engine.Api.Attributes
{
    public class ExpandShippingTypeAttribute : ExpandAttribute
    {
        public override string Name { get { return "shippingType"; } }
		public override string Description { get { return "This will retrieve the related shipping type."; } }
    }
}