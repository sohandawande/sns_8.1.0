﻿namespace Znode.Engine.Api.Attributes
{
	public class SortPerItemCostAttribute : SortAttribute
	{
		public override string Name { get { return "perItemCost"; } }
		public override string Description { get { return "This will sort the response by per item cost."; } }
	}
}