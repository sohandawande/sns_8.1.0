﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterTotalAttribute : FilterAttribute
	{
		public override string Name { get { return "total"; } }
		public override string Description { get { return "This will filter the request by total."; } }
	}
}