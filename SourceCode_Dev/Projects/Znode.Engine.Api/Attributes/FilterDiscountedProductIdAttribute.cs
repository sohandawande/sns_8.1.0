﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterDiscountedProductIdAttribute : FilterAttribute
	{
		public override string Name { get { return "discountedProductId"; } }
		public override string Description { get { return "This will filter the request by discounted product ID."; } }
	}
}