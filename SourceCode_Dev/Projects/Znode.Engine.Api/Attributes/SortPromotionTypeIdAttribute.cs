﻿namespace Znode.Engine.Api.Attributes
{
	public class SortPromotionTypeIdAttribute : SortAttribute
	{
		public override string Name { get { return "promotionTypeId"; } }
		public override string Description { get { return "This will sort the response by promotion type ID."; } }
	}
}