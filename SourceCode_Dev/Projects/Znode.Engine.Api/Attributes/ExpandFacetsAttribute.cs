﻿namespace Znode.Engine.Api.Attributes
{
    public class ExpandFacetsAttribute : ExpandAttribute
	{
		public override string Name { get { return "facets"; } }
		public override string Description { get { return "This will retrieve the related list of facets."; } }
	}
   
}