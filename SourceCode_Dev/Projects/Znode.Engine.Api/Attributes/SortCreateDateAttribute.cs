﻿namespace Znode.Engine.Api.Attributes
{
	public class SortCreateDateAttribute : SortAttribute
	{
		public override string Name { get { return "createDate"; } }
		public override string Description { get { return "This will sort the response by create date."; } }
	}
}