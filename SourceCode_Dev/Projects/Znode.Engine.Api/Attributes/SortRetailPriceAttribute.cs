﻿namespace Znode.Engine.Api.Attributes
{
	public class SortRetailPriceAttribute : SortAttribute
	{
		public override string Name { get { return "retailPrice"; } }
		public override string Description { get { return "This will sort the response by retail price."; } }
	}
}