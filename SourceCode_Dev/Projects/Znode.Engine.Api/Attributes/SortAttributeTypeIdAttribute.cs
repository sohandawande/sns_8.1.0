﻿namespace Znode.Engine.Api.Attributes
{
	public class SortAttributeTypeIdAttribute : SortAttribute
	{
		public override string Name { get { return "attributeTypeId"; } }
		public override string Description { get { return "This will sort the response by attribute type ID."; } }
	}
}