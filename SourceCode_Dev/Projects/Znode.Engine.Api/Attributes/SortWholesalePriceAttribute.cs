﻿namespace Znode.Engine.Api.Attributes
{
	public class SortWholesalePriceAttribute : SortAttribute
	{
		public override string Name { get { return "wholesalePrice"; } }
		public override string Description { get { return "This will sort the response by wholesale price."; } }
	}
}