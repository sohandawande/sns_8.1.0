﻿namespace Znode.Engine.Api.Attributes
{
	public class ExpandOrderLineItemAttribute : ExpandAttribute
	{
		public override string Name { get { return "orderLineItem"; } }
		public override string Description { get { return "This will retrieve the related order line item."; } }
	}
}