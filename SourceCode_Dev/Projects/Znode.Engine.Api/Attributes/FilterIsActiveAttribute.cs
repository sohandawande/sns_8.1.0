﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterIsActiveAttribute : FilterAttribute
	{
		public override string Name { get { return "isActive"; } }
		public override string Description { get { return "This will filter the request by the is active flag."; } }
	}
}
