﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterApiKeyAttribute : FilterAttribute
	{
		public override string Name { get { return "apiKey"; } }
		public override string Description { get { return "This will filter the request by API key."; } }
	}
}