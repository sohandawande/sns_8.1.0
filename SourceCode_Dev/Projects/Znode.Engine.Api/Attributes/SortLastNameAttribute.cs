﻿namespace Znode.Engine.Api.Attributes
{
	public class SortLastNameAttribute : SortAttribute
	{
		public override string Name { get { return "lastName"; } }
		public override string Description { get { return "This will sort the response by last name."; } }
	}
}