﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterSalePriceAttribute : FilterAttribute
	{
		public override string Name { get { return "salePrice"; } }
		public override string Description { get { return "This will filter the request by sale price."; } }
	}
}