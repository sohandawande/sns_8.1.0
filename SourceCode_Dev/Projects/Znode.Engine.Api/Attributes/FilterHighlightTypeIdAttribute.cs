﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterHighlightTypeIdAttribute : FilterAttribute
	{
		public override string Name { get { return "highlightTypeId"; } }
		public override string Description { get { return "This will filter the request by highlight type ID."; } }
	}
}