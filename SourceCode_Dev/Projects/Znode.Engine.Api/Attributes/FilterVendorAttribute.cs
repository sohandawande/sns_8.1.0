﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterVendorAttribute : FilterAttribute
	{
		public override string Name { get { return "vendor"; } }
		public override string Description { get { return "This will filter the request by vendor."; } }
	}
}