﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterIsFeaturedAttribute : FilterAttribute
	{
		public override string Name { get { return "isFeatured"; } }
		public override string Description { get { return "This will filter the request by the is featured flag."; } }
	}
}
