﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterExpirationDateAttribute : FilterAttribute
	{
		public override string Name { get { return "expirationDate"; } }
		public override string Description { get { return "This will filter the request by expiration date."; } }
	}
}
