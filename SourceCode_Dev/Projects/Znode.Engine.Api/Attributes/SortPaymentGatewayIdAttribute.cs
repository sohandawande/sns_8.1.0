﻿namespace Znode.Engine.Api.Attributes
{
	public class SortPaymentGatewayIdAttribute : SortAttribute
	{
		public override string Name { get { return "paymentGatewayId"; } }
		public override string Description { get { return "This will sort the response by payment gateway ID."; } }
	}
}