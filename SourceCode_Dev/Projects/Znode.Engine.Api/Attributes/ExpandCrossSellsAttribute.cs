﻿namespace Znode.Engine.Api.Attributes
{
	public class ExpandCrossSellsAttribute : ExpandAttribute
	{
		public override string Name { get { return "crossSells"; } }
		public override string Description { get { return "This will retrieve the related list of cross sells."; } }
	}
}