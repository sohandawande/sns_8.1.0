﻿namespace Znode.Engine.Api.Attributes
{
	public class SortSkuIdAttribute : SortAttribute
	{
		public override string Name { get { return "skuId"; } }
		public override string Description { get { return "This will sort the response by SKU ID."; } }
	}
}