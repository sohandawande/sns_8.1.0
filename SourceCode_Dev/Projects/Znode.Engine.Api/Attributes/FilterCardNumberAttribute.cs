﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterCardNumberAttribute : FilterAttribute
	{
		public override string Name { get { return "cardNumber"; } }
		public override string Description { get { return "This will filter the request by card number."; } }
	}
}