﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterExternalIdAttribute : FilterAttribute
	{
		public override string Name { get { return "externalId"; } }
		public override string Description { get { return "This will filter the request by external ID."; } }
	}
}
