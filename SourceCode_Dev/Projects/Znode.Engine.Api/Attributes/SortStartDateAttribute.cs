﻿namespace Znode.Engine.Api.Attributes
{
	public class SortStartDateAttribute : SortAttribute
	{
		public override string Name { get { return "startDate"; } }
		public override string Description { get { return "This will sort the response by start date."; } }
	}
}