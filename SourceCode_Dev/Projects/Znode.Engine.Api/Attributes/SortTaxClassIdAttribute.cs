﻿namespace Znode.Engine.Api.Attributes
{
	public class SortTaxClassIdAttribute : SortAttribute
	{
		public override string Name { get { return "taxClassId"; } }
		public override string Description { get { return "This will sort the response by tax class ID."; } }
	}
}