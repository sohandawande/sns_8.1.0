﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterIsPrivateAttribute : FilterAttribute
	{
		public override string Name { get { return "isPrivate"; } }
		public override string Description { get { return "This will filter the request by the is private flag."; } }
	}
}