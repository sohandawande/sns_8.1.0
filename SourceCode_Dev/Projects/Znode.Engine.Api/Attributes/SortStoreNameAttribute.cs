﻿namespace Znode.Engine.Api.Attributes
{
	public class SortStoreNameAttribute : SortAttribute
	{
		public override string Name { get { return "storeName"; } }
		public override string Description { get { return "This will sort the response by store name."; } }
	}
}