﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterDiscountTypeIdAttribute : FilterAttribute
	{
		public override string Name { get { return "discountTypeId"; } }
		public override string Description { get { return "This will filter the request by discount type ID."; } }
	}
}