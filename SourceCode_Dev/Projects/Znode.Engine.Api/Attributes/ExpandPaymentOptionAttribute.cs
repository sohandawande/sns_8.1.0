﻿namespace Znode.Engine.Api.Attributes
{
	public class ExpandPaymentOptionAttribute : ExpandAttribute
	{
		public override string Name { get { return "paymentOption"; } }
		public override string Description { get { return "This will retrieve the related payment option."; } }
	}
}