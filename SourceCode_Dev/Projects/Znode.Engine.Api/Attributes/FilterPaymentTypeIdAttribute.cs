﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterPaymentTypeIdAttribute : FilterAttribute
	{
		public override string Name { get { return "paymentTypeId"; } }
		public override string Description { get { return "This will filter the request by payment type ID."; } }
	}
}