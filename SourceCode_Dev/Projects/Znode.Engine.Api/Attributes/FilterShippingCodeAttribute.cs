﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterShippingCodeAttribute : FilterAttribute
	{
		public override string Name { get { return "shippingCode"; } }
		public override string Description { get { return "This will filter the request by shipping code."; } }
	}
}