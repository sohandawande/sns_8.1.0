﻿namespace Znode.Engine.Api.Attributes
{
	public class SortKeyAttribute : SortAttribute
	{
		public override string Name { get { return "key"; } }
		public override string Description { get { return "This will sort the response by key."; } }
	}
}