﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterNameAttribute : FilterAttribute
	{
		public override string Name { get { return "name"; } }
		public override string Description { get { return "This will filter the request by name."; } }
	}
}