﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterStateCodeAttribute : FilterAttribute
	{
		public override string Name { get { return "stateCode"; } }
		public override string Description { get { return "This will filter the request by state code."; } }
	}
}
