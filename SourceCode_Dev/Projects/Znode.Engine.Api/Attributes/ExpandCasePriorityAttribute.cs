﻿namespace Znode.Engine.Api.Attributes
{
	public class ExpandCasePriorityAttribute : ExpandAttribute
	{
		public override string Name { get { return "casePriority"; } }
		public override string Description { get { return "This will retrieve the related case priority."; } }
	}
}