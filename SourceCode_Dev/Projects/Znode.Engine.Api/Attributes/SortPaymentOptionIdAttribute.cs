﻿namespace Znode.Engine.Api.Attributes
{
	public class SortPaymentOptionIdAttribute : SortAttribute
	{
		public override string Name { get { return "paymentOptionId"; } }
		public override string Description { get { return "This will sort the response by payment option ID."; } }
	}
}