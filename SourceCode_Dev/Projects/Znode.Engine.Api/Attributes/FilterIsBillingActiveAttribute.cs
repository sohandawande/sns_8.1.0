﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterIsBillingActiveAttribute : FilterAttribute
	{
		public override string Name { get { return "isBillingActive"; } }
		public override string Description { get { return "This will filter the request by the is billing active flag."; } }
	}
}