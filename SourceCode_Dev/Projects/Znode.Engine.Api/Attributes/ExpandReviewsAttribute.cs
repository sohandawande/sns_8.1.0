﻿namespace Znode.Engine.Api.Attributes
{
	public class ExpandReviewsAttribute : ExpandAttribute
	{
		public override string Name { get { return "reviews"; } }
		public override string Description { get { return "This will retrieve the related list of reviews."; } }
	}
}