﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterPaymentGatewayIdAttribute : FilterAttribute
	{
		public override string Name { get { return "paymentGatewayId"; } }
		public override string Description { get { return "This will filter the request by payment gateway ID."; } }
	}
}