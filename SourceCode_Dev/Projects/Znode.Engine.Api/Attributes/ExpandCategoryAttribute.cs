﻿namespace Znode.Engine.Api.Attributes
{
	public class ExpandCategoryAttribute : ExpandAttribute
	{
		public override string Name { get { return "category"; } }
		public override string Description { get { return "This will retrieve the related category."; } }
	}
}