﻿namespace Znode.Engine.Api.Attributes
{
	public class SortEndDateAttribute : SortAttribute
	{
		public override string Name { get { return "endDate"; } }
		public override string Description { get { return "This will sort the response by end date."; } }
	}
}