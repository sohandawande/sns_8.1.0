﻿namespace Znode.Engine.Api.Attributes
{
	public class ExpandHighlightsAttribute : ExpandAttribute
	{
		public override string Name { get { return "highlights"; } }
		public override string Description { get { return "This will retrieve the related list of highlights."; } }
	}
}