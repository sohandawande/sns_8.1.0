﻿namespace Znode.Engine.Api.Attributes
{
	public class SortProcessedDateAttribute : SortAttribute
	{
		public override string Name { get { return "processedDate"; } }
		public override string Description { get { return "This will sort the response by processed date."; } }
	}
}