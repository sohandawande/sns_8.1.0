﻿namespace Znode.Engine.Api.Attributes
{
	public class FilterAuditTypeIdAttribute : FilterAttribute
	{
		public override string Name { get { return "auditTypeId"; } }
		public override string Description { get { return "This will filter the request by audit type ID."; } }
	}
}