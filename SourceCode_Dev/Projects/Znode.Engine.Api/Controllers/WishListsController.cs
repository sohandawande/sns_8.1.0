﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
	public class WishListsController : BaseController
	{
		private readonly IWishListService _service;
		private readonly IWishListCache _cache;

		public WishListsController()
		{
			_service = new WishListService();
			_cache = new WishListCache(_service);
		}

		/// <summary>
		///  Get a wishlist.
		/// </summary>
		/// <param name="wishListId">The ID of the wishlist.</param>
		/// <returns></returns>
		[HttpGet]
		public HttpResponseMessage Get(int wishListId)
		{
			HttpResponseMessage response;
			try
			{
				var data = _cache.GetWishList(wishListId, RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<WishListResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new AccountResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Gets a list of wishlists.
		/// </summary>
		/// <returns></returns>
		[FilterAccountId, FilterCreateDate, FilterProductId]
		[PageIndex, PageSize]
		[HttpGet]
		public HttpResponseMessage List()
		{
			HttpResponseMessage response;
			try
			{
				var data = _cache.GetWishLists(RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<WishListsListResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new ProductListResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Creates a wishlist.
		/// </summary>
		/// <param name="model">The model of the wishlist.</param>
		/// <returns></returns>
		[HttpPost]
		public HttpResponseMessage Create([FromBody] WishListModel model)
		{
			HttpResponseMessage response;
			try
			{
				var wishList = _service.Create(model);
				if (wishList != null)
				{
					var uri = Request.RequestUri;
					var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + wishList.ProductId;

					response = CreateCreatedResponse(new WishListResponse() { WishList = wishList });
					response.Headers.Add("Location", location);
				}
				else
				{
					response = CreateInternalServerErrorResponse();
				}
			}
			catch (Exception ex)
			{
				var data = new ProductResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Updates an existing wishlist.
		/// </summary>
		/// <param name="wishListId">The ID of the wishlist.</param>
		/// <param name="model">The model of the wishlist.</param>
		/// <returns></returns>
		[HttpPut]
		public HttpResponseMessage Update(int wishListId, [FromBody] WishListModel model)
		{
			HttpResponseMessage response;
			try
			{
				var wishList = _service.UpdateWishlist(wishListId, model);
				response = wishList != null ? CreateOKResponse(new WishListResponse { WishList = wishList }) : CreateInternalServerErrorResponse();
			}
			catch (Exception ex)
			{
				var data = new ProductResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Deletes existing wishlist.
		/// </summary>
		/// <param name="wishListId">The ID of the wishlist.</param>
		/// <returns></returns>
		[HttpDelete]
		public HttpResponseMessage Delete(int wishListId)
		{
			HttpResponseMessage response;
			try
			{
				var deleted = _service.DeleteWishlist(wishListId);
				response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
			}
			catch (Exception ex)
			{
				var data = new WishListResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}
	}
}
