﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    public class ProductsController : BaseController
    {
        #region private variables
        private readonly IProductService _service;
        private readonly IProductCache _cache;
        #endregion

        #region public default constructor
        public ProductsController()
        {
            _service = new ProductService();
            _cache = new ProductCache(_service);
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Gets a product.
        /// </summary>
        /// <param name="productId">The ID of the product.</param>
        /// <returns></returns>
        [ExpandAddOns, ExpandAttributes, ExpandCategories, ExpandCrossSells, ExpandFrequentlyBoughtTogether, ExpandHighlights, ExpandImages, ExpandManufacturer, ExpandProductType, ExpandPromotions, ExpandReviews, ExpandSkus, ExpandTiers, ExpandYouMayAlsoLike, ExpandProductTags]
        [HttpGet]
        public HttpResponseMessage Get(int productId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetProduct(productId, RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<ProductResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets a product with a selected SKU.
        /// </summary>
        /// <param name="productId">The ID of the product.</param>
        /// <param name="skuId">The ID of the selected SKU.</param>
        /// <returns></returns>
        [ExpandAddOns, ExpandAttributes, ExpandCategories, ExpandCrossSells, ExpandFrequentlyBoughtTogether, ExpandHighlights, ExpandImages, ExpandManufacturer, ExpandProductType, ExpandPromotions, ExpandReviews, ExpandSkus, ExpandTiers, ExpandYouMayAlsoLike]
        [HttpGet]
        public HttpResponseMessage GetWithSku(int productId, int skuId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetProductWithSku(productId, skuId, RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<ProductResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets a list of products.
        /// </summary>
        /// <returns></returns>
        [ExpandAddOns, ExpandAttributes, ExpandCategories, ExpandCrossSells, ExpandFrequentlyBoughtTogether, ExpandHighlights, ExpandImages, ExpandManufacturer, ExpandProductType, ExpandPromotions, ExpandReviews, ExpandSkus, ExpandTiers, ExpandYouMayAlsoLike]
        [FilterAccountId, FilterExternalId, FilterIsActive, FilterIsFeatured, FilterIsNew, FilterManufacturerId, FilterName, FilterNumber, FilterPortalId, FilterProductTypeId, FilterRetailPrice, FilterSalePrice, FilterShippingRuleTypeId, FilterSupplierId, FilterTaxClassId, FilterWholesalePrice]
        [SortDisplayOrder, SortName, SortNumber, SortProductId, SortRetailPrice, SortSalePrice, SortWholesalePrice]
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage List()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetProducts(RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<ProductListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets a list of products for a specific catalog.
        /// </summary>
        /// <param name="catalogId">The ID of the catalog.</param>
        /// <returns></returns>
        [ExpandAddOns, ExpandAttributes, ExpandCategories, ExpandCrossSells, ExpandFrequentlyBoughtTogether, ExpandHighlights, ExpandImages, ExpandManufacturer, ExpandProductType, ExpandPromotions, ExpandReviews, ExpandSkus, ExpandTiers, ExpandYouMayAlsoLike]
        [FilterAccountId, FilterExternalId, FilterIsActive, FilterIsFeatured, FilterIsNew, FilterManufacturerId, FilterName, FilterNumber, FilterPortalId, FilterProductTypeId, FilterRetailPrice, FilterSalePrice, FilterShippingRuleTypeId, FilterSupplierId, FilterTaxClassId, FilterWholesalePrice]
        [SortDisplayOrder, SortName, SortNumber, SortProductId, SortRetailPrice, SortSalePrice, SortWholesalePrice]
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage ListByCatalog(int catalogId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetProductsByCatalog(catalogId, RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<ProductListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets a list of products for a specific catalog.
        /// </summary>
        /// <param name="catalogId">The ID of the catalog.</param>
        /// <returns></returns>
        [ExpandAddOns, ExpandAttributes, ExpandCategories, ExpandCrossSells, ExpandFrequentlyBoughtTogether, ExpandHighlights, ExpandImages, ExpandManufacturer, ExpandProductType, ExpandPromotions, ExpandReviews, ExpandSkus, ExpandTiers, ExpandYouMayAlsoLike]
        [FilterAccountId, FilterExternalId, FilterIsActive, FilterIsFeatured, FilterIsNew, FilterManufacturerId, FilterName, FilterNumber, FilterPortalId, FilterProductTypeId, FilterRetailPrice, FilterSalePrice, FilterShippingRuleTypeId, FilterSupplierId, FilterTaxClassId, FilterWholesalePrice]
        [SortDisplayOrder, SortName, SortNumber, SortProductId, SortRetailPrice, SortSalePrice, SortWholesalePrice]
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage ListByCatalogIds(string catalogIds)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetProductsByCatalogIds(catalogIds, RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<ProductListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets a list of products for a specific category.
        /// </summary>
        /// <param name="categoryId">The ID of the category.</param>
        /// <returns></returns>
        [ExpandAddOns, ExpandAttributes, ExpandCategories, ExpandCrossSells, ExpandFrequentlyBoughtTogether, ExpandHighlights, ExpandImages, ExpandManufacturer, ExpandProductType, ExpandPromotions, ExpandReviews, ExpandSkus, ExpandTiers, ExpandYouMayAlsoLike]
        [FilterAccountId, FilterExternalId, FilterIsActive, FilterIsFeatured, FilterIsNew, FilterManufacturerId, FilterName, FilterNumber, FilterPortalId, FilterProductTypeId, FilterRetailPrice, FilterSalePrice, FilterShippingRuleTypeId, FilterSupplierId, FilterTaxClassId, FilterWholesalePrice]
        [SortDisplayOrder, SortName, SortNumber, SortProductId, SortRetailPrice, SortSalePrice, SortWholesalePrice]
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage ListByCategory(int categoryId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetProductsByCategory(categoryId, RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<ProductListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets a list of products for a specific category and promotion type.
        /// </summary>
        /// <param name="categoryId">The ID of the category.</param>
        /// <param name="promotionTypeId">The ID of the promotion type.</param>
        /// <returns></returns>
        [ExpandAddOns, ExpandAttributes, ExpandCategories, ExpandCrossSells, ExpandFrequentlyBoughtTogether, ExpandHighlights, ExpandImages, ExpandManufacturer, ExpandProductType, ExpandPromotions, ExpandReviews, ExpandSkus, ExpandTiers, ExpandYouMayAlsoLike]
        [FilterAccountId, FilterExternalId, FilterIsActive, FilterIsFeatured, FilterIsNew, FilterManufacturerId, FilterName, FilterNumber, FilterPortalId, FilterProductTypeId, FilterRetailPrice, FilterSalePrice, FilterShippingRuleTypeId, FilterSupplierId, FilterTaxClassId, FilterWholesalePrice]
        [SortDisplayOrder, SortName, SortNumber, SortProductId, SortRetailPrice, SortSalePrice, SortWholesalePrice]
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage ListByCategoryByPromotionType(int categoryId, int promotionTypeId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetProductsByCategoryByPromotionType(categoryId, promotionTypeId, RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<ProductListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets a list of products for the specified external IDs.
        /// </summary>
        /// <param name="externalIds">The comma-seperated external IDs of the products.</param>
        /// <returns></returns>
        [ExpandAddOns, ExpandAttributes, ExpandCategories, ExpandCrossSells, ExpandFrequentlyBoughtTogether, ExpandHighlights, ExpandImages, ExpandManufacturer, ExpandProductType, ExpandPromotions, ExpandReviews, ExpandSkus, ExpandTiers, ExpandYouMayAlsoLike]
        [SortDisplayOrder, SortName, SortNumber, SortProductId, SortRetailPrice, SortSalePrice, SortWholesalePrice]
        [HttpGet]
        public HttpResponseMessage ListByExternalIds(string externalIds)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetProductsByExternalIds(externalIds, RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<ProductListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets a list of products designated as home page specials.
        /// </summary>
        /// <returns></returns>
        [ExpandAddOns, ExpandAttributes, ExpandCategories, ExpandCrossSells, ExpandFrequentlyBoughtTogether, ExpandHighlights, ExpandImages, ExpandManufacturer, ExpandProductType, ExpandPromotions, ExpandReviews, ExpandSkus, ExpandTiers, ExpandYouMayAlsoLike]
        [FilterAccountId, FilterExternalId, FilterIsActive, FilterIsFeatured, FilterIsNew, FilterManufacturerId, FilterName, FilterNumber, FilterPortalId, FilterProductTypeId, FilterRetailPrice, FilterSalePrice, FilterShippingRuleTypeId, FilterSupplierId, FilterTaxClassId, FilterWholesalePrice]
        [SortDisplayOrder, SortName, SortNumber, SortProductId, SortRetailPrice, SortSalePrice, SortWholesalePrice]
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage ListByHomeSpecials()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetProductsByHomeSpecials(RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<ProductListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets a list of products for the specified product IDs.
        /// </summary>
        /// <param name="productIds">The comma-separated IDs of the products.</param>
        /// <returns></returns>
        [ExpandAddOns, ExpandAttributes, ExpandCategories, ExpandCrossSells, ExpandFrequentlyBoughtTogether, ExpandHighlights, ExpandImages, ExpandManufacturer, ExpandProductType, ExpandPromotions, ExpandReviews, ExpandSkus, ExpandTiers, ExpandYouMayAlsoLike]
        [SortDisplayOrder, SortName, SortNumber, SortProductId, SortRetailPrice, SortSalePrice, SortWholesalePrice]
        [HttpPost]
        public HttpResponseMessage ListByProductIds([FromBody] EntityIdsModel model)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetProductsByProductIds(model.Ids, RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<ProductListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets a list of products for a specific promotion type.
        /// </summary>
        /// <param name="promotionTypeId">The ID of the promotion type.</param>
        /// <returns></returns>
        [ExpandAddOns, ExpandAttributes, ExpandCategories, ExpandCrossSells, ExpandFrequentlyBoughtTogether, ExpandHighlights, ExpandImages, ExpandManufacturer, ExpandProductType, ExpandPromotions, ExpandReviews, ExpandSkus, ExpandTiers, ExpandYouMayAlsoLike]
        [FilterAccountId, FilterExternalId, FilterIsActive, FilterIsFeatured, FilterIsNew, FilterManufacturerId, FilterName, FilterNumber, FilterPortalId, FilterProductTypeId, FilterRetailPrice, FilterSalePrice, FilterShippingRuleTypeId, FilterSupplierId, FilterTaxClassId, FilterWholesalePrice]
        [SortDisplayOrder, SortName, SortNumber, SortProductId, SortRetailPrice, SortSalePrice, SortWholesalePrice]
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage ListByPromotionType(int promotionTypeId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetProductsByPromotionType(promotionTypeId, RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<ProductListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Creates a new product.
        /// </summary>
        /// <param name="model">The model of the product.</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Create([FromBody] ProductModel model)
        {
            HttpResponseMessage response;

            try
            {
                var product = _service.CreateProduct(model);
                if (product != null)
                {
                    var uri = Request.RequestUri;
                    var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + product.ProductId;

                    response = CreateCreatedResponse(new ProductResponse { Product = product });
                    response.Headers.Add("Location", location);
                }
                else
                {
                    response = CreateInternalServerErrorResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new ProductResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Updates an existing product.
        /// </summary>
        /// <param name="productId">The ID of the product.</param>
        /// <param name="model">The model of the product.</param>
        /// <returns></returns>
        [HttpPut]
        public HttpResponseMessage Update(int productId, [FromBody] ProductModel model)
        {
            HttpResponseMessage response;

            try
            {
                var product = _service.UpdateProduct(productId, model);
                response = product != null ? CreateOKResponse(new ProductResponse { Product = product }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Deletes an existing product.
        /// </summary>
        /// <param name="productId">The ID of the product.</param>
        /// <returns></returns>
        [HttpDelete]
        public HttpResponseMessage Delete(int productId)
        {
            HttpResponseMessage response;

            try
            {
                var deleted = _service.DeleteProduct(productId);
                response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        #region Znode Version 8.0

        #region Product Setting

        /// <summary>       
        /// Updates an existing product.
        /// </summary>
        /// <param name="productId">The ID of the product.</param>
        /// <param name="model">The model of the product.</param>
        /// <returns></returns>
        [HttpPut]
        public HttpResponseMessage UpdateProductSettings(int productId, [FromBody] ProductModel model)
        {
            HttpResponseMessage response;
            try
            {
                var product = _service.UpdateProductSettings(productId, model);
                response = product != null ? CreateOKResponse(new ProductResponse { Product = product }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>       
        /// To get product details by productId
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <returns>returns ProductDetails info for manage/edit</returns>
        public HttpResponseMessage GetProductDetailsByProductId(int productId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetProductDetailsByProductId(productId, RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<ProductResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        [HttpGet]
        public HttpResponseMessage IsSeoUrlExist(int productId, string seoUrl)
        {
            HttpResponseMessage response;

            try
            {
                var isExist = _service.IsSeoUrlExist(productId, seoUrl);
                response = isExist ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        #endregion

        #region Product Category
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage GetProductCategoryByProductId()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetProductCategoryByProductId(RouteUri, RouteTemplate);
                response = (!Equals(data, null)) ? CreateOKResponse<CategoryListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new CategoryListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Get the list of categories which are not associate to a product
        /// </summary>
        /// <param name="productId">The Id of product</param>
        /// <returns>HttpResponseMessage</returns>
        [PageIndex, PageSize]
        [SortCategoryId]
        [HttpGet]
        public HttpResponseMessage GetProductUnAssociatedCategoryByProductId(int productId)
        {
            HttpResponseMessage response;
            try
            {
                string data = _cache.GetProductUnAssociatedCategoryByProductId(productId, RouteUri, RouteTemplate);
                response = (!Equals(data, null)) ? CreateOKResponse<CategoryListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                CategoryListResponse data = new CategoryListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        /// <summary>
        /// To Associate product Category.
        /// </summary>
        /// <param name="model">CategoryModel</param>
        /// <returns>HttpResponseMessage</returns>
        [HttpPost]
        public HttpResponseMessage AssociateProductCategory([FromBody] CategoryModel model)
        {
            HttpResponseMessage response;

            try
            {
                var isExist = _service.AssociateProductCategory(model);
                response = CreateOKResponse(new ProductResponse { IsSuccess = isExist });
            }
            catch (Exception ex)
            {
                var data = new ProductResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        [HttpPost]
        public HttpResponseMessage UnAssociateProductCategory([FromBody] CategoryModel model)
        {
            HttpResponseMessage response;

            try
            {
                var deleted = _service.UnAssociateProductCategory(model);
                response = CreateOKResponse(new ProductResponse { IsSuccess = deleted });
            }
            catch (Exception ex)
            {
                var data = new ProductResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        #endregion

        #region Product SKU

        /// <summary>
        /// To get Product Sku Details
        /// </summary>
        /// <returns></returns>
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage GetProductSkuDetails()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetProductSkuDetails(RouteUri, RouteTemplate);
                response = (!Equals(data, null)) ? CreateOKResponse<ProductSkuListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductSkuListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// To Associate Sku Facets
        /// </summary>
        /// <param name="skuId"></param>
        /// <param name="associateFacetIds"></param>
        /// <param name="unassociateFacetIds"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage AssociateSkuFacets(int? skuId, string associateFacetIds, string unassociateFacetIds)
        {
            HttpResponseMessage response;

            try
            {
                var isExist = _service.AssociateSkuFacets(skuId, associateFacetIds, unassociateFacetIds);
                response = isExist ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// To Delete Sku Associated Facets
        /// </summary>
        /// <param name="skuId"></param>
        /// <param name="facetGroupId"></param>
        /// <returns></returns>
        [HttpDelete]
        public HttpResponseMessage DeleteSkuAssociatedFacets(int skuId, int facetGroupId)
        {
            HttpResponseMessage response;

            try
            {
                var deleted = _service.DeleteSkuAssociatedFacets(skuId, facetGroupId);
                response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// To Get Sku Associated Facets
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <param name="skuId"> int skuId</param>
        /// <param name="facetGroupId">int facetGroupId</param>
        /// <returns></returns>
        public HttpResponseMessage GetSkuAssociatedFacets(int productId, int skuId, int facetGroupId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetSkuAssociatedFacets(productId, skuId, facetGroupId, RouteUri, RouteTemplate);
                response = (!Equals(data, null)) ? CreateOKResponse<ProductFacetsResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductFacetsResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// To Get all the Sku Associated Facets.
        /// </summary>
        /// <returns>Return Sku Associated Facets.</returns>
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage GetSkuFacets()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetSkuFacets(RouteUri, RouteTemplate);
                response = (!Equals(data, null)) ? CreateOKResponse<ProductFacetListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductFacetListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
        #endregion

        #region Product Tags

        /// <summary>
        /// Gets the product tags based on product id.
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        public HttpResponseMessage GetProductTag(int productId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetProductTags(productId, RouteUri, RouteTemplate);
                response = (!Equals(data, null)) ? CreateOKResponse<ProductTagResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductTagResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }


        /// <summary>
        /// Creates a new product tags.
        /// </summary>
        /// <param name="model">The model of the product tags.</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage CreateProductTag([FromBody] ProductTagsModel model)
        {
            HttpResponseMessage response;

            try
            {
                var productTag = _service.CreateProductTag(model);
                if (!Equals(productTag, null))
                {
                    var uri = Request.RequestUri;
                    var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + productTag.ProductId;

                    response = CreateCreatedResponse(new ProductTagResponse { ProductTag = productTag });
                    response.Headers.Add("Location", location);
                }
                else
                {
                    response = CreateInternalServerErrorResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new ProductTagResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Updates an existing product tags.
        /// </summary>
        /// <param name="productId">The ID of the product.</param>
        /// <param name="model">The model of the product tags.</param>
        /// <returns></returns>
        [HttpPut]
        public HttpResponseMessage UpdateProductTag(int tagId, [FromBody] ProductTagsModel model)
        {
            HttpResponseMessage response;

            try
            {
                var productTags = _service.UpdateProductTag(tagId, model);
                response = (!Equals(productTags, null)) ? CreateOKResponse(new ProductTagResponse { ProductTag = productTags }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductTagResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Deletes an existing product tags.
        /// </summary>
        /// <param name="productId">The ID of the product.</param>
        /// <returns></returns>
        [HttpDelete]
        public HttpResponseMessage DeleteProductTag(int tagId)
        {
            HttpResponseMessage response;

            try
            {
                var deleted = _service.DeleteProductTag(tagId);
                response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductTagResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        #endregion

        #region Product Facets

        /// <summary>
        /// To Get all the Product Associated Facets.
        /// </summary>
        /// <returns>Return Product Associated Facets.</returns>
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage GetProductFacetsList()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetProductFacetsList(RouteUri, RouteTemplate);
                response = (!Equals(data, null)) ? CreateOKResponse<ProductFacetListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductFacetListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets the product Facets based on product & facetGroupId id.
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="facetGroupId"></param>
        /// <returns></returns>
        public HttpResponseMessage GetProductFacets(int productId, int facetGroupId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetProductFacets(productId, facetGroupId, RouteUri, RouteTemplate);
                response = (!Equals(data, null)) ? CreateOKResponse<ProductFacetsResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductFacetsResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Bind the Associated facets to the product
        /// </summary>
        /// <param name="productId">The ID of the product.</param>
        /// <param name="model">The model of the product tags.</param>
        /// <returns></returns>
        [HttpPut]
        public HttpResponseMessage BindProductFacets(int productId, [FromBody] ProductFacetModel model)
        {
            HttpResponseMessage response;

            try
            {
                var productFacets = _service.BindProductFacets(productId, model);
                response = (!Equals(productFacets, null)) ? CreateOKResponse(new ProductFacetsResponse { ProductFacets = productFacets }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductTagResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        ///To Delete the product associated facets based on productid & facetgroupid.
        /// </summary>
        /// <param name="productId">The ID of the product.</param>
        /// <param name="facetGroupId">The ID of the facet group.</param>
        /// <returns></returns>
        [HttpDelete]
        public HttpResponseMessage DeleteProductAssociatedFacets(int productId, int facetGroupId)
        {
            HttpResponseMessage response;

            try
            {
                var deleted = _service.DeleteProductAssociatedFacets(productId, facetGroupId);
                response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductAlternateImageResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }


        #endregion

        #region Product Images

        /// <summary>
        /// To Get all the Product Image Types.
        /// </summary>
        /// <returns>Return Product Image Types.</returns>
        [HttpGet]
        public HttpResponseMessage GetProductImageTypes()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetProductImageTypes(RouteUri, RouteTemplate);
                response = (!Equals(data, null)) ? CreateOKResponse<ProductImageTypeListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductImageTypeListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Creates a new product image.
        /// </summary>
        /// <param name="model">The model of the product image.</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage InsertProductAlternateImage([FromBody] ProductAlternateImageModel model)
        {
            HttpResponseMessage response;

            try
            {
                var productImage = _service.InsertProductAlternateImage(model);
                if (productImage != null)
                {
                    var uri = Request.RequestUri;
                    var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + productImage.ProductID;

                    response = CreateCreatedResponse(new ProductAlternateImageResponse { ProductImage = productImage });
                    response.Headers.Add("Location", location);
                }
                else
                {
                    response = CreateInternalServerErrorResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new ProductAlternateImageResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Updates an existing product image.
        /// </summary>
        /// <param name="productImageId">The ID of the product image.</param>
        /// <param name="model">The model of the product image.</param>
        /// <returns></returns>
        [HttpPut]
        public HttpResponseMessage UpdateProductAlternateImage(int productImageId, [FromBody] ProductAlternateImageModel model)
        {
            HttpResponseMessage response;

            try
            {
                var productImage = _service.UpdateProductAlternateImage(productImageId, model);
                response = (!Equals(productImage, null) ? CreateOKResponse(new ProductAlternateImageResponse { ProductImage = productImage }) : CreateInternalServerErrorResponse());
            }
            catch (Exception ex)
            {
                var data = new ProductAlternateImageResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Deletes an existing product image.
        /// </summary>
        /// <param name="productImageId">The ID of the product image.</param>
        /// <returns></returns>
        [HttpDelete]
        public HttpResponseMessage DeleteProductAlternateImage(int productImageId)
        {
            HttpResponseMessage response;

            try
            {
                var deleted = _service.DeleteProductAlternateImage(productImageId);
                response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductAlternateImageResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// To Get all the Product alternate images.
        /// </summary>
        /// <returns></returns>
        [FilterProductId]
        [SortDisplayOrder, SortName, SortProductId]
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage GetAllProductAlternateImage()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetAllProductAlternateImages(RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<ProductAlternateImageListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductAlternateImageListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Get the product image based on product image id.
        /// </summary>
        /// <param name="productImageId"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetProductAlternateImage(int productImageId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetProductAlternateImage(productImageId, RouteUri, RouteTemplate);
                response = (!Equals(data, null) ? CreateOKResponse<ProductAlternateImageResponse>(data) : CreateNotFoundResponse());
            }
            catch (Exception ex)
            {
                var data = new ProductAlternateImageResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
        #endregion

        #region Product Bundles

        [FilterProductId]
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage GetProductBundlesList()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetProductBundlesList(RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<ProductBundlesListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductBundlesListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Deletes an existing product bundle.
        /// </summary>
        /// <param name="productImageId">The ID of the parent child product.</param>
        /// <returns></returns>
        [HttpDelete]
        public HttpResponseMessage DeleteBundleProduct(int parentChildProductID)
        {
            HttpResponseMessage response;

            try
            {
                var deleted = _service.DeleteBundleProduct(parentChildProductID);
                response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductBundlesListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
        /// <summary>
        /// To Get Product List.
        /// </summary>
        /// <returns></returns>
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage GetProductList()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetProductList(RouteUri, RouteTemplate);
                response = (!Equals(data, null)) ? CreateOKResponse<ProductListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// To Associate the Bundle Products.
        /// </summary>
        /// <param name="model">ProductModel</param>
        /// <returns>HttpResponseMessage</returns>
        [HttpPost]
        public HttpResponseMessage AssociateBundleProduct([FromBody] ProductModel model)
        {
            HttpResponseMessage response;

            try
            {
                var status = _service.AssociateBundleProduct(model);
                response = CreateOKResponse(new ProductResponse { IsSuccess = status });
            }
            catch (Exception ex)
            {
                var data = new ProductResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        #endregion

        #region AddOn
        /// <summary>
        /// Removes specified mapping between product and addon. 
        /// </summary>
        /// <param name="productAddOnId">Mapping Id between product and addon</param>
        /// <returns></returns>
        [HttpDelete]
        public HttpResponseMessage RemoveProductAddOn(int productAddOnId)
        {
            HttpResponseMessage response;

            try
            {
                var removed = _service.RemoveProductAddOn(productAddOnId);
                response = removed ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductAddOnResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Associate specified addons with current product.
        /// </summary>
        /// <param name="models"></param>
        /// <returns></returns>
        public HttpResponseMessage AssociateAddOn([FromBody] List<AddOnModel> models)
        {
            HttpResponseMessage response;

            try
            {
                var status = _service.AssociateProductAddOn(models);
                response = status ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductAddOnResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Return associated addons for specified product id.
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage ProductAddOns(int productId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetAssociatedAddOns(productId, RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<AddOnListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductAddOnResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }


        /// <summary>
        /// Return unassociated addons for specified product id.
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="portalId"></param>
        /// <returns></returns>
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage UnassociatedAddOns(int productId, int portalId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetUnassociatedAddOns(productId, RouteUri, RouteTemplate, portalId);
                response = data != null ? CreateOKResponse<AddOnListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductAddOnResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        #endregion

        #region Product Highlights

        /// <summary>
        /// Removes specified mapping between product and highlight. 
        /// </summary>
        /// <param name="productHighlightId">Mapping Id between product and highlight</param>
        /// <returns></returns>
        [HttpDelete]
        public HttpResponseMessage RemoveProductHighlight(int productHighlightId)
        {
            HttpResponseMessage response;

            try
            {
                var removed = _service.RemoveProductHighlight(productHighlightId);
                response = removed ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new HighlightResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Associate specified highlights with current product.
        /// </summary>
        /// <param name="models"></param>
        /// <returns></returns>
        public HttpResponseMessage AssociateHighlight([FromBody] List<HighlightModel> models)
        {
            HttpResponseMessage response;

            try
            {
                var status = _service.AssociateProductHighlight(models);
                response = status ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new HighlightResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Return associated highlights for specified product id.
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage ProductHighlights(int productId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetAssociatedHighlights(productId, RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<HighlightListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new HighlightListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Return unassociated highlights for specified product id.
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="portalId"></param>
        /// <returns></returns>
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage UnassociatedHighlights(int productId, int portalId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetUnassociatedHighlights(productId, RouteUri, RouteTemplate, portalId);
                response = data != null ? CreateOKResponse<HighlightListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new HighlightListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        #endregion

        #region Product Tier Pricing

        /// <summary>
        /// Return product tiers for specified product id.
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage GetProductTiers(int productId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetProductTiers(productId, RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<TierListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new TierListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Creates a new product tier.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage CreateProductTier([FromBody] ProductTierPricingModel model)
        {
            HttpResponseMessage response;

            try
            {
                var productTier = _service.CreateProductTier(model);
                if (productTier != null)
                {
                    var uri = Request.RequestUri;
                    var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + productTier.ProductId;

                    response = CreateCreatedResponse(new ProductTierResponse { ProductTier = productTier });
                    response.Headers.Add("Location", location);
                }
                else
                {
                    response = CreateInternalServerErrorResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new ProductTierResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Updates specified product tier. 
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        public HttpResponseMessage UpdateProductTier([FromBody] ProductTierPricingModel model)
        {
            HttpResponseMessage response;

            try
            {
                var isUpdated = _service.UpdateProductTier(model);
                response = isUpdated ? CreateOKResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductTierResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        /// <summary>
        /// Delete specified product tier.
        /// </summary>
        /// <param name="productTierId"></param>
        /// <returns></returns>
        [HttpDelete]
        public HttpResponseMessage DeleteProductTier(int productTierId)
        {
            HttpResponseMessage response;

            try
            {
                var isDeleted = _service.DeleteProductTier(productTierId);
                response = isDeleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductTierResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        #endregion

        #region Digital Asset

        /// <summary>
        /// Return digital assets for specified product id.
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage GetDigitalAssets(int productId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetDigitalAssets(productId, RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<DigitalAssetListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new DigitalAssetListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Creates a mew Digital Asset.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage CreateDigitalAsset([FromBody] DigitalAssetModel model)
        {
            HttpResponseMessage response;

            try
            {
                var digitalAsset = _service.CreateDigitalAsset(model);
                if (digitalAsset != null)
                {
                    var uri = Request.RequestUri;
                    var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + digitalAsset.ProductID;

                    response = CreateCreatedResponse(new DigitalAssetResponse { DigitalAsset = digitalAsset });
                    response.Headers.Add("Location", location);
                }
                else
                {
                    response = CreateInternalServerErrorResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new DigitalAssetResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Deletes an existing DigitalAsset.
        /// </summary>
        /// <param name="digitalAssetId">The ID of the DigitalAsset.</param>
        /// <returns></returns>
        [HttpDelete]
        public HttpResponseMessage DeleteDigitalAsset(int digitalAssetId)
        {
            HttpResponseMessage response;

            try
            {
                var deleted = _service.DeleteDigitalAsset(digitalAssetId);
                response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new DigitalAssetResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
        #endregion

        #region Category Associated Products
        /// <summary>
        /// Gets all products.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetAllProducts()
        {
            int totalRowCount = 0;
            HttpResponseMessage response;
            try
            {
                var data = _cache.GetAllProducts(RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse(new ProductListResponse { CategoryAssociatedProducts = data, TotalResults = totalRowCount }) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }
        #endregion

        #region Product SEO Information

        [HttpPut]
        public HttpResponseMessage UpdateProductSEOInformation(int productId, [FromBody] ProductModel model)
        {
            HttpResponseMessage response;

            try
            {
                var product = _service.UpdateProductSEOInformation(productId, model);
                response = product != null ? CreateOKResponse(new ProductResponse { Product = product }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        #endregion

        #region  UpdateImage

        /// <summary>
        /// To update image path by id
        /// </summary>
        /// <param name="model">UpdateImageModel model</param>
        /// <returns>returns Updated Image Model</returns>
        [HttpPut]
        public HttpResponseMessage UpdateImage([FromBody] UpdateImageModel model)
        {
            HttpResponseMessage response;

            try
            {
                var image = _service.UpdateImage(model);
                response = image != null ? CreateOKResponse(new UpdateImageResponse { Image = image }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new UpdateImageResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        #endregion UpdateImage


        #region Product Details
        [HttpGet]
        public HttpResponseMessage CopyProduct(int productId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.CopyProduct(productId, RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<ProductResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        [HttpDelete]
        public HttpResponseMessage DeleteByProductId(int productId)
        {
            HttpResponseMessage response;

            try
            {
                var deleted = _service.DeleteByProductId(productId);
                response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
        #endregion

        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage SearchProducts()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.SearchProducts(RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<ProductListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        [HttpGet]
        public HttpResponseMessage CreateOrderProduct(int productId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.CreateOrderProducts(productId, RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<ProductResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        #endregion Znode Version 8.0

        /// <summary>
        /// Get sku product list which contains SKU as a sub string sku.
        /// </summary>
        /// <returns>Returns sku product list.</returns>
        [HttpGet]
        public HttpResponseMessage GetSkuProductListBySku()
        {
            HttpResponseMessage response;

            try
            {
                string data = _cache.GetSkuProductListBySku(RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<ProductListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                ProductListResponse data = new ProductListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        [HttpPost]
        public HttpResponseMessage SendMail(CompareProductModel compareProducts)
        {
            HttpResponseMessage response;
            try
            {
                bool isMailSend = _service.SendMailToFriend(compareProducts);
                response = CreateOKResponse(new ProductResponse { IsSuccess = isMailSend });
            }
            catch (Exception ex)
            {
                ProductResponse data = new ProductResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;           
        }

        //PRFT Custom Code:Start
        /// <summary>
        /// Gets a list of product inventories.
        /// </summary>
        /// <returns></returns>
        
        [FilterProductId, FilterName, FilterNumber, FilterPortalId]
        [SortName, SortNumber, SortProductId]
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage GetInventoryFromErp()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetInventoryFromErp(RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<InventoryListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new InventoryListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
        //PRFT Custom Code:End
        #endregion Public Methods

    }
}