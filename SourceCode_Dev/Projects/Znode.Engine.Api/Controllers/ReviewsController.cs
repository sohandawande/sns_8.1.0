﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
	public class ReviewsController : BaseController
	{
		private readonly IReviewService _service;
		private readonly IReviewCache _cache;

		public ReviewsController()
		{
			_service = new ReviewService();
			_cache = new ReviewCache(_service);
		}

		/// <summary>
		/// Gets a review.
		/// </summary>
		/// <param name="reviewId">The ID of the review.</param>
		/// <returns></returns>
		[ExpandProduct]
		[HttpGet]
		public HttpResponseMessage Get(int reviewId)
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetReview(reviewId,  RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<ReviewResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new ReviewResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Gets a list of reviews.
		/// </summary>
		/// <returns></returns>
		[ExpandProduct]
		[FilterAccountId, FilterProductId, FilterRating, FilterReviewStatus]
		[SortCreateDate, SortProductId, SortRating, SortReviewId]
		[PageIndex, PageSize]
		[HttpGet]
		public HttpResponseMessage List()
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetReviews(RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<ReviewListResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new ReviewListResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Creates a new review.
		/// </summary>
		/// <param name="model">The model of the review.</param>
		/// <returns></returns>
		[HttpPost]
		public HttpResponseMessage Create([FromBody] ReviewModel model)
		{
			HttpResponseMessage response;

			try
			{
				var review = _service.CreateReview(model);
				if (review != null)
				{
					var uri = Request.RequestUri;
					var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + review.ReviewId;

					response = CreateCreatedResponse(new ReviewResponse { Review = review });
					response.Headers.Add("Location", location);
				}
				else
				{
					response = CreateInternalServerErrorResponse();
				}
			}
			catch (Exception ex)
			{
				var data = new ReviewResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Updates an existing review.
		/// </summary>
		/// <param name="reviewId">The ID of the review.</param>
		/// <param name="model">The model of the review.</param>
		/// <returns></returns>
		[HttpPut]
		public HttpResponseMessage Update(int reviewId, [FromBody] ReviewModel model)
		{
			HttpResponseMessage response;

			try
			{
				var review = _service.UpdateReview(reviewId, model);
				response = review != null ? CreateOKResponse(new ReviewResponse { Review = review }) : CreateInternalServerErrorResponse();
			}
			catch (Exception ex)
			{
				var data = new ReviewResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Deletes an existing review.
		/// </summary>
		/// <param name="reviewId">The ID of the review.</param>
		/// <returns></returns>
		[HttpDelete]
		public HttpResponseMessage Delete(int reviewId)
		{
			HttpResponseMessage response;

			try
			{
				var deleted = _service.DeleteReview(reviewId);
				response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
			}
			catch (Exception ex)
			{
				var data = new ReviewResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}
	}
}