﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
	public class DomainsController : BaseController
	{
		private readonly IDomainService _service;
		private readonly IDomainCache _cache;

		public DomainsController()
		{
			_service = new DomainService();
			_cache = new DomainCache(_service);
		}

		/// <summary>
		/// Gets a domain.
		/// </summary>
		/// <param name="domainId">The ID of the domain.</param>
		/// <returns></returns>
		[HttpGet]
		public HttpResponseMessage Get(int domainId)
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetDomain(domainId, RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<DomainResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new DomainResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Gets a list of domains.
		/// </summary>
		/// <returns></returns>
		[FilterApiKey, FilterDomainName, FilterIsActive, FilterPortalId]
		[SortDomainId, SortDomainName]
		[PageIndex, PageSize]
		[HttpGet]
		public HttpResponseMessage List()
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetDomains(RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<DomainListResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new DomainListResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Creates a new domain.
		/// </summary>
		/// <param name="model">The model of the domain.</param>
		/// <returns></returns>
		[HttpPost]
		public HttpResponseMessage Create([FromBody] DomainModel model)
		{
			HttpResponseMessage response;

			try
			{
				var domain = _service.CreateDomain(model);
				if (domain != null)
				{
					var uri = Request.RequestUri;
					var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + domain.DomainId;

					response = CreateCreatedResponse(new DomainResponse { Domain = domain });
					response.Headers.Add("Location", location);
				}
				else
				{
					response = CreateInternalServerErrorResponse();
				}
			}
			catch (Exception ex)
			{
				var data = new DomainResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Updates an existing domain.
		/// </summary>
		/// <param name="domainId">The ID of the domain.</param>
		/// <param name="model">The model of the domain.</param>
		/// <returns></returns>
		[HttpPut]
		public HttpResponseMessage Update(int domainId, [FromBody] DomainModel model)
		{
			HttpResponseMessage response;

			try
			{
				var domain = _service.UpdateDomain(domainId, model);
				response = domain != null ? CreateOKResponse(new DomainResponse { Domain = domain }) : CreateInternalServerErrorResponse();
			}
			catch (Exception ex)
			{
				var data = new DomainResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Deletes an existing domain.
		/// </summary>
		/// <param name="domainId">The ID of the domain.</param>
		/// <returns></returns>
		[HttpDelete]
		public HttpResponseMessage Delete(int domainId)
		{
			HttpResponseMessage response;

			try
			{
				var deleted = _service.DeleteDomain(domainId);
				response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
			}
			catch (Exception ex)
			{
				var data = new DomainResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}
	}
}