﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
	public class SkusController : BaseController
	{
		private readonly ISkuService _service;
		private readonly ISkuCache _cache;

		public SkusController()
		{
			_service = new SkuService();
			_cache = new SkuCache(_service);
		}

		/// <summary>
		/// Gets a SKU.
		/// </summary>
		/// <param name="skuId">The ID of the SKU.</param>
		/// <returns></returns>
		[ExpandAttributes, ExpandInventory, ExpandSupplier]
		[HttpGet]
		public HttpResponseMessage Get(int skuId)
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetSku(skuId,  RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<SkuResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new SkuResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Gets a list of SKUs.
		/// </summary>
		/// <returns></returns>
		[ExpandAttributes, ExpandInventory, ExpandSupplier]
		[FilterExternalId, FilterIsActive, FilterProductId, FilterSku, FilterSupplierId]
		[SortDisplayOrder, SortProductId, SortSku, SortSkuId]
		[PageIndex, PageSize]
		[HttpGet]
		public HttpResponseMessage List()
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetSkus(RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<SkuListResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new SkuListResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

        /// <summary>
        /// Get the list of Sku
        /// </summary>
        /// <param name="productId">productId to retrieve Sku list</param>
        /// <returns>HttpResponseMessage</returns>
        [HttpGet]
        public HttpResponseMessage GetSKUListByProductId(int productId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetSKUListByProductId(productId,RouteUri, RouteTemplate);
				response = !Equals(data,null) ? CreateOKResponse<SkuListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new SkuListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

		/// <summary>
		/// Creates a new SKU.
		/// </summary>
		/// <param name="model">The model of the SKU.</param>
		/// <returns></returns>
		[HttpPost]
		public HttpResponseMessage Create([FromBody] SkuModel model)
		{
			HttpResponseMessage response;

			try
			{
				var sku = _service.CreateSku(model);
                if (!Equals(sku, null))
				{
					var uri = Request.RequestUri;
					var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + sku.SkuId;

					response = CreateCreatedResponse(new SkuResponse { Sku = sku });
					response.Headers.Add("Location", location);
				}
				else
				{
					response = CreateInternalServerErrorResponse();
				}
			}
			catch (Exception ex)
			{
				var data = new SkuResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Updates an existing SKU.
		/// </summary>
		/// <param name="skuId">The ID of the SKU.</param>
		/// <param name="model">The model of the SKU.</param>
		/// <returns></returns>
		[HttpPut]
		public HttpResponseMessage Update(int skuId, [FromBody] SkuModel model)
		{
			HttpResponseMessage response;

			try
			{
				var sku = _service.UpdateSku(skuId, model);
                response = !Equals(sku, null) ? CreateOKResponse(new SkuResponse { Sku = sku }) : CreateInternalServerErrorResponse();
			}
			catch (Exception ex)
			{
				var data = new SkuResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Deletes an existing SKU.
		/// </summary>
		/// <param name="skuId">The ID of the SKU.</param>
		/// <returns></returns>
		[HttpDelete]
		public HttpResponseMessage Delete(int skuId)
		{
			HttpResponseMessage response;

			try
			{
				var deleted = _service.DeleteSku(skuId);
				response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
			}
			catch (Exception ex)
			{
				var data = new SkuResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

        #region Znode Version 8.0
        /// <summary>      
        /// Mapping Sku Attributes
        /// </summary>
        /// <param name="skuId">int skuId</param>
        /// <param name="attributeIds">string comma seperated attributeId</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage AddSkuAttribute(int skuId, string attributeIds)
        {
            HttpResponseMessage response;
            try
            {
                var status = _service.AddSkuAttribute(skuId, attributeIds);
                response = status ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new SkuResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        #endregion
    }
}