﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
	public class ShippingTypesController : BaseController
	{
		private readonly IShippingTypeService _service;
		private readonly IShippingTypeCache _cache;

		public ShippingTypesController()
		{
			_service = new ShippingTypeService();
			_cache = new ShippingTypeCache(_service);
		}

		/// <summary>
		/// Gets a shipping type.
		/// </summary>
		/// <param name="shippingTypeId">The ID of the shipping type.</param>
		/// <returns></returns>
		[HttpGet]
		public HttpResponseMessage Get(int shippingTypeId)
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetShippingType(shippingTypeId, RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<ShippingTypeResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new ShippingTypeResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Gets a list of shipping types.
		/// </summary>
		/// <returns></returns>
		[FilterClassName, FilterIsActive, FilterName]
		[SortClassName, SortName, SortShippingTypeId]
		[PageIndex, PageSize]
		[HttpGet]
		public HttpResponseMessage List()
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetShippingTypes(RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<ShippingTypeListResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new ShippingTypeListResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Creates a new shipping type.
		/// </summary>
		/// <param name="model">The model of the shipping type.</param>
		/// <returns></returns>
		[HttpPost]
		public HttpResponseMessage Create([FromBody] ShippingTypeModel model)
		{
			HttpResponseMessage response;

			try
			{
				var shippingType = _service.CreateShippingType(model);
				if (shippingType != null)
				{
					var uri = Request.RequestUri;
					var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + shippingType.ShippingTypeId;

					response = CreateCreatedResponse(new ShippingTypeResponse { ShippingType = shippingType });
					response.Headers.Add("Location", location);
				}
				else
				{
					response = CreateInternalServerErrorResponse();
				}
			}
			catch (Exception ex)
			{
				var data = new ShippingTypeResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Updates an existing shipping type.
		/// </summary>
		/// <param name="shippingTypeId">The ID of the shipping type.</param>
		/// <param name="model">The model of the shipping type.</param>
		/// <returns></returns>
		[HttpPut]
		public HttpResponseMessage Update(int shippingTypeId, [FromBody] ShippingTypeModel model)
		{
			HttpResponseMessage response;

			try
			{
				var shippingType = _service.UpdateShippingType(shippingTypeId, model);
				response = shippingType != null ? CreateOKResponse(new ShippingTypeResponse { ShippingType = shippingType }) : CreateInternalServerErrorResponse();
			}
			catch (Exception ex)
			{
				var data = new ShippingTypeResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Deletes an existing shipping type.
		/// </summary>
		/// <param name="shippingTypeId">The ID of the shipping type.</param>
		/// <returns></returns>
		[HttpDelete]
		public HttpResponseMessage Delete(int shippingTypeId)
		{
			HttpResponseMessage response;

			try
			{
				var deleted = _service.DeleteShippingType(shippingTypeId);
				response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
			}
			catch (Exception ex)
			{
				var data = new ShippingTypeResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

        #region Znode Version 8.0
        /// <summary>
        /// Get All Shipping Types not in database
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetAllShippingTypesNotInDatabase()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetAllShippingTypesNotInDatabase(RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<ShippingTypeListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ShippingTypeListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
        #endregion
	}
}