﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    public class CustomerUserMappingController : BaseController
    {
        private readonly ICustomerUserMappingService _service;
        private readonly IAccountService _accountService;
        private readonly IAccountCache _cache;

        public CustomerUserMappingController()
        {
            _service = new CustomerUserMappingService();
            _accountService = new AccountService();
            _cache = new AccountCache(_accountService);
        }

        /// <summary>
        /// Deletes Customer associated with user.
        /// </summary>
        /// <param name="customerusermappingId">The ID of the Account Profile.</param>
        /// <returns>True / False when Record is deleted</returns>
        [HttpDelete]
        public HttpResponseMessage Delete(int customerusermappingId)
        {
            HttpResponseMessage response;

            try
            {
                var deleted = _service.DeleteUserAssociatedCustomer(customerusermappingId);
                response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new CustomerUserMappingResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Get Profiles Associated with Account 
        /// </summary>
        /// <param name="accountId">account Id</param>
        /// <param name="customerAccountIds">customerAccountIds</param>
        /// <returns>HttpResponseMessage</returns>
        [HttpGet]
        public HttpResponseMessage InsertUserAssociatedCustomer(int accountId, string customerAccountIds)
        {
            HttpResponseMessage response;
            try
            {
                response = _service.InsertUserAssociatedCustomer(accountId, customerAccountIds) ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new CustomerUserMappingResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Deletes all mapping of user.
        /// </summary>
        /// <param name="userAccountId">The ID of the user to be deleted.</param>
        /// <returns>True / False when Record is deleted</returns>
        [HttpDelete]
        public HttpResponseMessage DeleteAllUserMapping(int userAccountId)
        {
            HttpResponseMessage response;
            try
            {
                response = _service.DeleteAllUserMapping(userAccountId) ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new CustomerUserMappingResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// To Get Sub User List account List.
        /// </summary>
        /// <returns>HttpResponseMessage </returns>
        [PageIndex, PageSize]
        [HttpGet]

        public HttpResponseMessage GetSubUserList()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetSubUserList(RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<AccountResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new AccountResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }


    }
}
