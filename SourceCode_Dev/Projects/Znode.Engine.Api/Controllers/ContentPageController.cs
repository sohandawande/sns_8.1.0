﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    /// <summary>
    /// Content Page Controller.
    /// </summary>
    public class ContentPageController : BaseController
    {
        #region Private Variables
        private readonly IContentPageService _service;
        private readonly IContentPageCache _cache;
        #endregion

        #region Default Constructor

        /// <summary>
        /// Constructor for content page controller.
        /// </summary>
        public ContentPageController()
        {
            _service = new ContentPageService();
            _cache = new ContentPageCache(_service);
        }

        #endregion

        /// <summary>
        /// Gets a list of ContentPage.
        /// </summary>
        /// <returns>List of ContentPage</returns>       
        [ExpandPortal, ExpandMasterPage]
        [FilterContentPageId, FilterIsActive, FilterLocaleId, FilterName, FilterPortalId]
        [SortDisplayOrder, SortName]
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage List()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetContentPages(RouteUri, RouteTemplate);
                response = (!Equals(data, null)) ? CreateOKResponse<ContentPageListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ContentPageListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets a Content page.
        /// </summary>
        /// <param name="contentPageId">The ID of the content page.</param>
        /// <returns></returns>
        [ExpandCategoryNodes, ExpandProductIds, ExpandSubcategories]
        [HttpGet]
        public HttpResponseMessage Get(int contentPageId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetContentPage(contentPageId, RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<CategoryResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new CategoryResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Creates a new Content Page.
        /// </summary>
        /// <param name="model">The model of the Content Page.</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Create([FromBody] ContentPageModel model)
        {
            HttpResponseMessage response;

            try
            {
                var contentPage = _service.CreateContentPage(model);
                if (!Equals(contentPage, null))
                {
                    var uri = Request.RequestUri;
                    var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + contentPage.ContentPageID;

                    response = CreateCreatedResponse(new ContentPageResponse { ContentPage = contentPage });
                    response.Headers.Add("Location", location);
                }
                else
                {
                    response = CreateInternalServerErrorResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new ContentPageResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Copies a Content Page.
        /// </summary>
        /// <param name="model">The model of the Content Page.</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage CopyContentPage([FromBody] ContentPageModel model)
        {
            HttpResponseMessage response;

            try
            {
                ContentPageModel contentPage = _service.Clone(model) as ContentPageModel;
                if (!Equals(contentPage, null))
                {
                    var uri = Request.RequestUri;
                    var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + contentPage.ContentPageID;

                    response = CreateCreatedResponse(new ContentPageResponse { ContentPageCopy = contentPage });
                    response.Headers.Add("Location", location);
                }
                else
                {
                    response = CreateInternalServerErrorResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new ContentPageResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Adds a Content Page.
        /// </summary>
        /// <param name="model">The model of the Content Page.</param>
        /// <returns>HttpResponseMessage</returns>
        [HttpPost]
        public HttpResponseMessage AddPage([FromBody] ContentPageModel model)
        {
            HttpResponseMessage response;

            try
            {
                bool isContentPageAdded = _service.AddPage(model);

                BooleanModel boolModel = new BooleanModel();
                boolModel.disabled = isContentPageAdded;

                TrueFalseResponse resp = new TrueFalseResponse();
                resp.booleanModel = boolModel;

                var uri = Request.RequestUri;
                var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + model.ContentPageID;

                response = CreateOKResponse(resp);
                response.Headers.Add("Location", location);
            }
            catch (Exception ex)
            {
                var data = new ContentPageResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Updates an existing Content page.
        /// </summary>
        /// <param name="contentPageId">The ID of the Content page.</param>
        /// <param name="model">The model of the Content page.</param>
        /// <returns></returns>
        [HttpPut]
        public HttpResponseMessage Update(int contentPageId, [FromBody] ContentPageModel model)
        {
            HttpResponseMessage response;

            try
            {
                bool isUpdated = _service.UpdateContentPage(contentPageId, model);

                BooleanModel boolModel = new BooleanModel();
                boolModel.disabled = isUpdated;

                TrueFalseResponse resp = new TrueFalseResponse();
                resp.booleanModel = boolModel;
                response = CreateOKResponse(resp);
            }
            catch (Exception ex)
            {
                var data = new ContentPageResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Deletes an existing Content Page.
        /// </summary>
        /// <param name="contentPageId">The ID of the Content Page.</param>
        /// <returns></returns>
        [HttpDelete]
        public HttpResponseMessage Delete(int contentPageId)
        {
            HttpResponseMessage response;

            try
            {
                var deleted = _service.DeleteContentPage(contentPageId);
                response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new ContentPageResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets a content page by its name and file extension.
        /// </summary>
        /// <param name="contentPageName">Name of the content page.</param>
        /// <param name="extension">Extension of the content page file.</param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetContentPageByName(string contentPageName, string extension)
        {
            HttpResponseMessage response;
            try
            {
                string data = _cache.GetContentPageByName(contentPageName, extension, RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<ContentPageResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                ContentPageResponse data = new ContentPageResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

    }
}
