﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    public class ProductReviewStateController : BaseController
    {
        private readonly IProductReviewStateService _service;
       private readonly IProductReviewStateCache _cache;

       public ProductReviewStateController()
		{
			_service = new ProductReviewStateService();
            _cache = new ProductReviewStateCache(_service);
		}

        /// <summary>
        /// Gets a list of Product Review States.
        /// </summary>
        /// <returns></returns>
       
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage List()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetProductReviewStates(RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<ProductReviewStateCache>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductReviewStateListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

    }
}
