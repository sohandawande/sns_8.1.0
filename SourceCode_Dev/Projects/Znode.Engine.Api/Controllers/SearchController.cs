﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    public class SearchController : BaseController
    {
        private readonly ISearchService _service;
        private readonly ISearchCache _cache;

        public SearchController()
        {
            _service = new SearchService();
            _cache = new SearchCache(_service);
        }

        /// <summary>
        /// Performs keyword search.
        /// </summary>
        /// <param name="model">The model of the keyword search.</param>
        /// <returns></returns>
        [ExpandCategories, ExpandFacets]
        [SortName, SortRating, SortRelevance]
        [HttpPost]
        public HttpResponseMessage Keyword([FromBody] KeywordSearchModel model)
        {
            HttpResponseMessage response;

            var data = _cache.GetKeywordSearch(model, RouteUri, RouteTemplate);
            if (data != null)
            {
                response = Request.CreateResponse(HttpStatusCode.OK, data, MediaTypeFormatter);
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.NotFound);
            }



            return response;
        }

        /// <summary>
        /// Performs suggested search, to be used for type-ahead search.
        /// </summary>
        /// <param name="model">The model of the suggested search.</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Suggested([FromBody] SuggestedSearchModel model)
        {
            HttpResponseMessage response;


            var data = _cache.GetSearchSuggestions(model, RouteUri, RouteTemplate);
            if (data != null)
            {
                response = Request.CreateResponse(HttpStatusCode.OK, data, MediaTypeFormatter);
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.NotFound);
            }



            return response;
        }

        /// <summary>
        /// Reloads the search index.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage ReloadIndex()
        {
            HttpResponseMessage response;

            _cache.ReloadIndex();
            response = CreateOKResponse();

            return response;
        }

        /// <summary>
        /// Performs Seo Url Search
        /// </summary>
        /// <param name="model">The model of the keyword search.</param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetSeoUrlDetail(string seoUrl)
        {
            HttpResponseMessage response;
            string data = _cache.GetSeoUrlDetail(seoUrl, RouteUri, RouteTemplate);
            response = !Equals(data, null) ? CreateOKResponse<SEOUrlResponse>(data) : CreateNotFoundResponse();
            return response;
        }

        /// <summary>
        /// To Check for the Restricted Seo Urls.
        /// </summary>
        /// <param name="seoUrl">string seoUrl.</param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage IsRestrictedSeoUrl(string seoUrl)
        {
            HttpResponseMessage response;
            try
            {
                bool isExist = _service.IsRestrictedSeoUrl(seoUrl);
                response = isExist ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                SEOUrlResponse data = new SEOUrlResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }
    }
}
