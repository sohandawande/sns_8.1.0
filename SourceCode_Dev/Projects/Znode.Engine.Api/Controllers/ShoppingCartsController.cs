﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    public class ShoppingCartsController : BaseController
    {
        private readonly IShoppingCartCache _cache;
        private readonly IShoppingCartService _service;

        public ShoppingCartsController()
        {
            _service = new ShoppingCartService();
            _cache = new ShoppingCartCache(_service);
        }

        /// <summary>
        /// Gets a shopping cart for an account.
        /// </summary>
        /// <param name="accountId">The ID of the account.</param>
        /// <returns>This endpoint relies on data passed into the request header instead of explicit parameters.</returns>
        [HttpGet]
        public HttpResponseMessage GetByAccount(int accountId)
        {
            HttpResponseMessage response;

            try
            {
                var shoppingCart = _cache.GetCartByAccount(accountId, PortalId);
                response = shoppingCart != null ? CreateOKResponse(shoppingCart) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ShoppingCartResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }


        /// <summary>
        /// Gets a shopping cart for a cookie.
        /// </summary>
        /// <param name="cookieId">The ID of the cookie.</param>
        /// <returns>This endpoint relies on data passed into the request header instead of explicit parameters.</returns>
        [HttpGet]
        public HttpResponseMessage GetByCookie(int cookieId)
        {
            HttpResponseMessage response;

            try
            {
                var shoppingCart = _cache.GetCartByCookie(cookieId);
                response = shoppingCart != null ? CreateOKResponse(shoppingCart) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ShoppingCartResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Creates a new shopping cart and saves it to the database.
        /// </summary>
        /// <param name="model">The model of the shopping cart.</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Create([FromBody] ShoppingCartModel model)
        {
            HttpResponseMessage response;

            try
            {
                int portalId = PortalId;
                if (!Equals(model.PortalId, null))
                {
                    portalId = model.PortalId.GetValueOrDefault();
                }
                var cart = _cache.CreateCart(portalId, model);
                if (cart != null)
                {
                    var uri = Request.RequestUri;
                    var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/account/" + cart.ShoppingCart.Account.AccountId;

                    response = CreateCreatedResponse(new ShoppingCartResponse { ShoppingCart = cart.ShoppingCart });
                    response.Headers.Add("Location", location);
                }
                else
                {
                    response = CreateInternalServerErrorResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new ShoppingCartResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Performs calculations for a shopping cart.
        /// </summary>
        /// <param name="model">The model of the shopping cart.</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Calculate([FromBody] ShoppingCartModel model)
        {
            HttpResponseMessage response;

            try
            {
                var shoppingCart = _cache.Calculate(model);
                response = shoppingCart != null ? CreateOKResponse(shoppingCart) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new ShoppingCartResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
    }
}
