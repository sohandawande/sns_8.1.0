﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    public class PersonalizationController : BaseController
    {
        #region Private Variables
        private readonly IPersonalizationCache _cache;
        private readonly IPersonalizationService _service;
        #endregion

        #region Constructor
        public PersonalizationController()
        {
            _service = new PersonalizationService();
            _cache = new PersonalizationCache(_service);
        }
        #endregion

        #region Public Action Methods
        /// <summary>
        /// Gets a list of frequently bought items.
        /// </summary>
        /// <returns></returns>        
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage FrequentlyBoughtProductsList()
        {
            HttpResponseMessage response;

            try
            {
                string data = _cache.GetFrequentlyBoughtProducts(RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<CrossSellListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                CrossSellListResponse data = new CrossSellListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets list of products by product id.
        /// </summary>
        /// <returns>Http Resonse Message.</returns>
        [HttpGet]
        public HttpResponseMessage ListByProductId()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetProductCrossSellbyProductId(RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<CrossSellListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new CrossSellListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        /// <summary>
        /// Create cross sell products
        /// </summary>
        /// <param name="model">cross sell model</param>
        /// <returns>Returns created cross sell product.</returns>
        [HttpPost]
        public HttpResponseMessage CreateCrossSellProduct([FromBody] CrossSellListModel model)
        {
            HttpResponseMessage response;

            try
            {
                bool isCreated = _service.CreateCrossSellProducts(model);
                BooleanModel boolModel = new BooleanModel { disabled = isCreated };
                response = isCreated ? CreateOKResponse(new TrueFalseResponse { booleanModel = boolModel }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                TrueFalseResponse data = new TrueFalseResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }


        /// <summary>
        /// Deletes an existing cross sell product.
        /// </summary>
        /// <param name="productId">list model of crossSell</param>
        /// <returns>Returns boolean value showing status of delete.</returns>
        [HttpDelete]
        public HttpResponseMessage DeleteCrossSellProduct(int productId, int relationTypeId)
        {
            HttpResponseMessage response;

            try
            {
                bool deleted = _service.DeleteCrossSellProducts(productId, relationTypeId);
                response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse(); ;
            }
            catch (Exception ex)
            {
                CrossSellResponse data = new CrossSellResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }
        #endregion
    }
}