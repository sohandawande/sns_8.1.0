﻿using System;
using System.Net.Http;
using System.Web.Mvc;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    public class DashboardController : BaseController
    {
        #region Private Variables
        private readonly IDashboardService _service;
             private readonly IDashboardCache _cache; 
        #endregion

        #region Default Constructor
        /// <summary>
        /// Constructor for Dashboard controller.
        /// </summary>
        public DashboardController()
        {
            _service = new DashboardService();
            _cache = new DashboardCache(_service);
        }
        #endregion
       
        #region Public Methods
        /// <summary>
        /// Get the Dashboard items by portal id.
        /// </summary>
        /// <param name="portalId">Id of the portal.</param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetDashboardItemsByPortalId(int portalId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetDashboard(portalId,RouteUri,RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<DashboardResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new DashboardResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
        #endregion
    }
}
