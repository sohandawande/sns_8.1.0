﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    /// <summary>
    /// Controller for Highlight
    /// </summary>
	public class HighlightsController : BaseController
	{
        #region Private Variables
        private readonly IHighlightService _service;
        private readonly IHighlightCache _cache; 
        #endregion

        #region Default Constructor
        public HighlightsController()
        {
            _service = new HighlightService();
            _cache = new HighlightCache(_service);
        } 
        #endregion

        #region Public Events
        /// <summary>
        /// Gets a highlight.
        /// </summary>
        /// <param name="highlightId">The ID of the highlight.</param>
        /// <returns></returns>
        [ExpandHighlightType]
        [HttpGet]
        public HttpResponseMessage Get(int highlightId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetHighlight(highlightId, RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<HighlightResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new HighlightResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets a list of highlights.
        /// </summary>
        /// <returns></returns>
        [ExpandHighlightType]
        [FilterHighlightTypeId, FilterIsActive, FilterLocaleId, FilterName, FilterPortalId]
        [SortDisplayOrder, SortHighlightId, SortName]
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage List()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetHighlights(RouteUri, RouteTemplate);
                response =  !Equals(data, null) ? CreateOKResponse<HighlightListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new HighlightListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Creates a new highlight.
        /// </summary>
        /// <param name="model">The model of the highlight.</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Create([FromBody] HighlightModel model)
        {
            HttpResponseMessage response;

            try
            {
                var highlight = _service.CreateHighlight(model);
                if (!Equals(highlight, null))
                {
                    var uri = Request.RequestUri;
                    var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + highlight.HighlightId;

                    response = CreateCreatedResponse(new HighlightResponse { Highlight = highlight });
                    response.Headers.Add("Location", location);
                }
                else
                {
                    response = CreateInternalServerErrorResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new HighlightResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Updates an existing highlight.
        /// </summary>
        /// <param name="highlightId">The ID of the highlight.</param>
        /// <param name="model">The model of the highlight.</param>
        /// <returns></returns>
        [HttpPut]
        public HttpResponseMessage Update(int highlightId, [FromBody] HighlightModel model)
        {
            HttpResponseMessage response;

            try
            {
                var highlight = _service.UpdateHighlight(highlightId, model);
                response =!Equals(highlight, null) ? CreateOKResponse(new HighlightResponse { Highlight = highlight }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new HighlightResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Deletes an existing highlight.
        /// </summary>
        /// <param name="highlightId">The ID of the highlight.</param>
        /// <returns></returns>
        [HttpDelete]
        public HttpResponseMessage Delete(int highlightId)
        {
            HttpResponseMessage response;

            try
            {
                var deleted = _service.DeleteHighlight(highlightId);
                response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new HighlightResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Checks the associated product with highlight
        /// </summary>
        /// <param name="highlightId">The ID of the highlight.</param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage CheckAssociatedProduct(int highlightId)
        {
            HttpResponseMessage response;

            try
            {
                var highlight = _service.CheckAssociateProduct(highlightId);
                response =!Equals(highlight, null) ? CreateOKResponse(new HighlightResponse { Highlight = highlight }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new HighlightResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        } 
        #endregion
	}
}