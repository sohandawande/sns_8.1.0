﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    public class PRFTERPServiceController:BaseController
    {
        #region Private Variables
        private readonly IPRFTERPServices _service;
        private readonly IPRFTERPServicesCache _cache;
        #endregion

        #region Constructor
        public PRFTERPServiceController()
        {
            _service = new PRFTERPServices();
            _cache = new PRFTERPServicesCache(_service);
        }
        #endregion

        [HttpGet]
        public HttpResponseMessage GetERPItemDetails(string associatedCustomerExternalId, string productNum, string customerType)
        {
            HttpResponseMessage response;
            try
            {
                var data = _cache.GetERPItemDetails(associatedCustomerExternalId, productNum, customerType, RouteUri, RouteTemplate);
                if (!Equals(data, null))
                {
                    response = CreateOKResponse<PRFTERPItemDetailsResponse>(data);
                }
                else
                {
                    response = CreateNotFoundResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new PRFTERPItemDetailsResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        public HttpResponseMessage GetInventoryDetails(string productNum)
        {
            HttpResponseMessage response;
            try
            {
                var data = _cache.GetInventoryDetails(productNum, RouteUri, RouteTemplate);
                if (!Equals(data, null))
                {
                    response = CreateOKResponse<PRFTInventoryResponse>(data);
                }
                else
                {
                    response = CreateNotFoundResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new PRFTInventoryResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }
        
    }
}