﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
	public class ProductCategoriesController : BaseController
	{
		private readonly IProductCategoryService _service;
		private readonly IProductCategoryCache _cache;

		public ProductCategoriesController()
		{
			_service = new ProductCategoryService();
			_cache = new ProductCategoryCache(_service);
		}

		/// <summary>
		/// Gets a product category.
		/// </summary>
		/// <param name="productCategoryId">The ID of the product category.</param>
		/// <returns></returns>
		[ExpandCategory, ExpandProduct]
		[HttpGet]
		public HttpResponseMessage Get(int productCategoryId)
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetProductCategory(productCategoryId, RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<ProductCategoryResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new ProductCategoryResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

        /// <summary>
        /// Gets a product category on the basis of categoryId and productId
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        [ExpandCategory, ExpandProduct]
        [HttpGet]
        public HttpResponseMessage GetProductCategory(int productId, int categoryId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetProductCategory(productId,categoryId, RouteUri, RouteTemplate);
                response = (!Equals(data,null)) ? CreateOKResponse<ProductCategoryResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductCategoryResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

		/// <summary>
		/// Gets a list of product categories.
		/// </summary>
		/// <returns></returns>
		[ExpandCategory, ExpandProduct]
		[FilterBeginDate, FilterCategoryId, FilterEndDate, FilterIsActive, FilterProductId]
		[SortCategoryId, SortDisplayOrder, SortProductCategoryId, SortProductId]
		[PageIndex, PageSize]
		[HttpGet]
		public HttpResponseMessage List()
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetProductCategories(RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<ProductCategoryListResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new ProductCategoryListResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Creates a new product category.
		/// </summary>
		/// <param name="model">The model of the product category.</param>
		/// <returns></returns>
		[HttpPost]
		public HttpResponseMessage Create([FromBody] ProductCategoryModel model)
		{
			HttpResponseMessage response;

			try
			{
				var productCategory = _service.CreateProductCategory(model);
				if (productCategory != null)
				{
					var uri = Request.RequestUri;
					var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + productCategory.ProductCategoryId;

					response = CreateCreatedResponse(new ProductCategoryResponse { ProductCategory = productCategory });
					response.Headers.Add("Location", location);
				}
				else
				{
					response = CreateInternalServerErrorResponse();
				}
			}
			catch (Exception ex)
			{
				var data = new ProductCategoryResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Updates an existing product category.
		/// </summary>
		/// <param name="productCategoryId">The ID of the product category.</param>
		/// <param name="model">The model of the product category.</param>
		/// <returns></returns>
		[HttpPut]
		public HttpResponseMessage Update(int productCategoryId, [FromBody] ProductCategoryModel model)
		{
			HttpResponseMessage response;

			try
			{
				var productCategory = _service.UpdateProductCategory(productCategoryId, model);
				response = productCategory != null ? CreateOKResponse(new ProductCategoryResponse { ProductCategory = productCategory }) : CreateInternalServerErrorResponse();
			}
			catch (Exception ex)
			{
				var data = new ProductCategoryResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Deletes an existing product category.
		/// </summary>
		/// <param name="productCategoryId">The ID of the product category.</param>
		/// <returns></returns>
		[HttpDelete]
		public HttpResponseMessage Delete(int productCategoryId)
		{
			HttpResponseMessage response;

			try
			{
				var deleted = _service.DeleteProductCategory(productCategoryId);
				response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
			}
			catch (Exception ex)
			{
				var data = new ProductCategoryResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}
	}
}