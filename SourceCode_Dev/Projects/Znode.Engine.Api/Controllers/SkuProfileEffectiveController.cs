﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;
namespace Znode.Engine.Api.Controllers
{
    /// <summary>
    /// Controller for Sku Profile Effective. ListModel
    /// </summary>
    public class SkuProfileEffectiveController : BaseController
    {
        #region Private Variables
        private readonly ISkuProfileEffectiveService _service;
        private readonly ISkuProfileEffectiveCache _cache;
        #endregion

        #region Default Constructor
        public SkuProfileEffectiveController()
        {
            _service = new SkuProfileEffectiveService();
            _cache = new SkuProfileEffectiveCache(_service);
        }
        #endregion

        /// <summary>
        /// Get the Sku Profile Effective.
        /// </summary>
        /// <param name="skuProfileEffectiveId">Id of the sku profile effective.</param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage Get(int skuProfileEffectiveId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetSkuProfileEffective(skuProfileEffectiveId, RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<SkuProfileEffectiveResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new SkuProfileEffectiveResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Get the list of sku profile effective. 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage List()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetSkuProfileEffectives(RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<SkuProfileEffectiveListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new SkuProfileEffectiveListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Create the Sku Profile Effective .
        /// </summary>
        /// <param name="model">model to create.</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Create([FromBody] SkuProfileEffectiveModel model)
        {
            HttpResponseMessage response;

            try
            {
                var skuProfileEffective = _service.CreateSkuProfileEffective(model);
                if (!Equals(skuProfileEffective, null))
                {
                    var uri = Request.RequestUri;
                    var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + skuProfileEffective.SkuProfileEffectiveID;

                    response = CreateCreatedResponse(new SkuProfileEffectiveResponse { SkuProfileEffective = skuProfileEffective });
                    response.Headers.Add("Location", location);
                }
                else
                {
                    response = CreateInternalServerErrorResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new SkuProfileEffectiveResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Update the existing sku profile Effective.
        /// </summary>
        /// <param name="skuProfileEffectiveId">Id of the Sku profile Effective </param>
        /// <param name="model">model to create.</param>
        /// <returns></returns>
        [HttpPut]
        public HttpResponseMessage Update(int skuProfileEffectiveId, [FromBody] SkuProfileEffectiveModel model)
        {
            HttpResponseMessage response;

            try
            {
                var skuProfileEffective = _service.UpdateSkuProfileEffective(skuProfileEffectiveId, model);
                response = !Equals(skuProfileEffective, null) ? CreateOKResponse(new SkuProfileEffectiveResponse { SkuProfileEffective = skuProfileEffective }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new SkuProfileEffectiveResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Get Sku Profile Effective list By SkuId
        /// </summary>
        /// <param name="skuId">Id of the Sku.</param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetSkuProfileEffectiveBySkuId(int skuId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetSkuProfileEffectivesBySkuId(skuId, RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<SkuProfileEffectiveListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new SkuProfileEffectiveListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Delete existing sku profile effective.
        /// </summary>
        /// <param name="skuProfileEffectiveId">Id of the sku profile effective.</param>
        /// <returns></returns>
        [HttpDelete]
        public HttpResponseMessage Delete(int skuProfileEffectiveId)
        {
            HttpResponseMessage response;

            try
            {
                var deleted = _service.DeleteSkuProfileEffective(skuProfileEffectiveId);
                response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new SkuProfileEffectiveResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
    }
}
