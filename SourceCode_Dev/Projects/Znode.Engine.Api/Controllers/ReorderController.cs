﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    public class ReorderController : BaseController
    {


        private readonly IReorderService _service;
        private readonly IReorderCache _cache;

        public ReorderController()
        {
            _service = new ReorderService();
            _cache = new ReorderCache(_service);
        }

        #region Znode Version 7.2.2 Reorder

        /// <summary>
        /// Znode Version 7.2.2
        /// Get items of order. this function help to place complite order as reorder
        /// </summary>
        /// <param name="orderId">orderId</param>
        /// <returns>returns HttpResponseMessage</returns>
        [ExpandAccountType, ExpandAddresses, ExpandOrders, ExpandProfiles, ExpandUser, ExpandWishList]
        [HttpGet]
        public HttpResponseMessage Getitems(int orderId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetReorderItems(orderId, RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<AccountResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new AccountResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        #region Multiparameter function for reorder TODO
        //[ExpandAccountType, ExpandAddresses, ExpandOrders, ExpandOrderLineItems, ExpandProfiles, ExpandUser, ExpandWishList]
        //[HttpGet]
        //public HttpResponseMessage GetSingleitem(string orderLineItemIds)
        //{
        //    HttpResponseMessage response;

        //    try
        //    {
        //        var data = _cache.GetReorderSingleItem(int.Parse(orderLineItemIds), RouteUri, RouteTemplate);
        //        response = data != null ? CreateOKResponse<AccountResponse>(data) : CreateNotFoundResponse();
        //    }
        //    catch (Exception ex)
        //    {
        //        var data = new AccountResponse { HasError = true, ErrorMessage = ex.Message };
        //        response = CreateInternalServerErrorResponse(data);
        //    }

        //    return response;
        //}

        //[ExpandAccountType, ExpandAddresses, ExpandOrders, ExpandProfiles, ExpandUser, ExpandWishList]
        //[HttpGet]
        //public HttpResponseMessage Getreorderlist(int orderId, int orderLineItemId, bool isOrder)
        //{
        //    HttpResponseMessage response;

        //    try
        //    {
        //        var data = _cache.GetReorderItemsList(orderId, orderLineItemId, isOrder, RouteUri, RouteTemplate);
        //        response = data != null ? CreateOKResponse<AccountResponse>(data) : CreateNotFoundResponse();
        //    }
        //    catch (Exception ex)
        //    {
        //        var data = new AccountResponse { HasError = true, ErrorMessage = ex.Message };
        //        response = CreateInternalServerErrorResponse(data);
        //    }

        //    return response;
        //}

        #endregion

        #endregion
    }
}
