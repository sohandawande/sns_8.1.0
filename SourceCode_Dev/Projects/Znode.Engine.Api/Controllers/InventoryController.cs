﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
	public class InventoryController : BaseController
	{
		private readonly IInventoryService _service;
		private readonly IInventoryCache _cache;

		public InventoryController()
		{
			_service = new InventoryService();
			_cache = new InventoryCache(_service);
		}

		/// <summary>
		/// Gets an inventory item.
		/// </summary>
		/// <param name="inventoryId">The ID of the inventory item.</param>
		/// <returns></returns>
		[ExpandSkus]
		[HttpGet]
		public HttpResponseMessage Get(int inventoryId)
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetInventory(inventoryId,  RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<InventoryResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new InventoryResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Gets a list of inventory items.
		/// </summary>
		/// <returns></returns>
		[ExpandSkus]
		[FilterQuantityOnHand, FilterSku]
		[SortQuantityOnHand, SortSku]
		[PageIndex, PageSize]
		[HttpGet]
		public HttpResponseMessage List()
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetInventories(RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<InventoryListResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new InventoryListResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Creates a new inventory item.
		/// </summary>
		/// <param name="model">The model of the inventory item.</param>
		/// <returns></returns>
		[HttpPost]
		public HttpResponseMessage Create([FromBody] InventoryModel model)
		{
			HttpResponseMessage response;

			try
			{
				var inventory = _service.CreateInventory(model);
				if (inventory != null)
				{
					var uri = Request.RequestUri;
					var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + inventory.InventoryId;

					response = CreateCreatedResponse(new InventoryResponse { Inventory = inventory });
					response.Headers.Add("Location", location);
				}
				else
				{
					response = CreateInternalServerErrorResponse();
				}
			}
			catch (Exception ex)
			{
				var data = new InventoryResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Updates an existing inventory item.
		/// </summary>
		/// <param name="inventoryId">The ID of the inventory item.</param>
		/// <param name="model">The model of the inventory item.</param>
		/// <returns></returns>
		[HttpPut]
		public HttpResponseMessage Update(int inventoryId, [FromBody] InventoryModel model)
		{
			HttpResponseMessage response;

			try
			{
				var inventory = _service.UpdateInventory(inventoryId, model);
				response = inventory != null ? CreateOKResponse(new InventoryResponse { Inventory = inventory }) : CreateInternalServerErrorResponse();
			}
			catch (Exception ex)
			{
				var data = new InventoryResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Deletes an existing inventory item.
		/// </summary>
		/// <param name="inventoryId">The ID of the inventory item.</param>
		/// <returns></returns>
		[HttpDelete]
		public HttpResponseMessage Delete(int inventoryId)
		{
			HttpResponseMessage response;

			try
			{
				var deleted = _service.DeleteInventory(inventoryId);
				response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
			}
			catch (Exception ex)
			{
				var data = new InventoryResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}
	}
}