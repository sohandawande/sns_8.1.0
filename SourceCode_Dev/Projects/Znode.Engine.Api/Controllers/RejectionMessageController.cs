﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    /// <summary>
    /// Controller for Rejection Message.
    /// </summary>
    public class RejectionMessageController : BaseController
    {
       #region Private Variables
        private readonly IRejectionMessageService _service;
        private readonly IRejectionMessageCache _cache; 
        #endregion

        #region Default Constructor
        /// <summary>
        /// Constructor for content page controller.
        /// </summary>
        public RejectionMessageController()
        {
            _service = new RejectionMessageService();
            _cache = new RejectionMessageCache(_service);
        } 
        #endregion

        #region Public Methods
        /// <summary>
        /// List of REjection message.
        /// </summary>
        /// <returns></returns>
        [ExpandPortal]
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage List()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetRejectionMessages(RouteUri, RouteTemplate);
                response = (!Equals(data, null)) ? CreateOKResponse<RejectionMessageListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new RejectionMessageListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets a Rejection Message.
        /// </summary>
        /// <param name="rejectionMessageId">The ID of the rejection message.</param>
        /// <returns></returns>
        [ExpandCategoryNodes, ExpandProductIds, ExpandSubcategories]
        [HttpGet]
        public HttpResponseMessage Get(int rejectionMessageId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetRejectionMessage(rejectionMessageId, RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<RejectionMessageResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new RejectionMessageResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Creates a new Rejection Message.
        /// </summary>
        /// <param name="model">The model of the Rejection Message.</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Create([FromBody] RejectionMessageModel model)
        {
            HttpResponseMessage response;

            try
            {
                var rejectionMessage = _service.CreateRejectionMessage(model);
                if (!Equals(rejectionMessage, null))
                {
                    var uri = Request.RequestUri;
                    var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + rejectionMessage.RejectionMessagesId;

                    response = CreateCreatedResponse(new RejectionMessageResponse { RejectionMessage = rejectionMessage });
                    response.Headers.Add("Location", location);
                }
                else
                {
                    response = CreateInternalServerErrorResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new RejectionMessageResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Updates an existing rejection message.
        /// </summary>
        /// <param name="rejectionMessageId">The ID of the rejection message.</param>
        /// <param name="model">The model of the rejection message.</param>
        /// <returns></returns>
        [HttpPut]
        public HttpResponseMessage Update(int rejectionMessageId, [FromBody] RejectionMessageModel model)
        {
            HttpResponseMessage response;

            try
            {
                var rejectionMessage = _service.UpdateRejectionMessage(rejectionMessageId, model);
                response = !Equals(rejectionMessage, null) ? CreateOKResponse(new RejectionMessageResponse { RejectionMessage = rejectionMessage }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new RejectionMessageResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Deletes an existing rejection message.
        /// </summary>
        /// <param name="rejectionMessageId">The ID of the rejection message.</param>
        /// <returns></returns>
        [HttpDelete]
        public HttpResponseMessage Delete(int rejectionMessageId)
        {
            HttpResponseMessage response;

            try
            {
                var deleted = _service.DeleteRejectionMessage(rejectionMessageId);
                response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new RejectionMessageResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        #endregion
    }
}
