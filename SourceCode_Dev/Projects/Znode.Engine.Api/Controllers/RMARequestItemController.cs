﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;
namespace Znode.Engine.Api.Controllers
{
    public class RMARequestItemController : BaseController
    {
        #region Private Variables
        private readonly IRMARequestItemCache _cache;
        private readonly IRMARequestItemService _service;
        #endregion

        #region Constructor
        public RMARequestItemController()
        {
            _service = new RMARequestItemService();
            _cache = new RMARequestItemCache(_service);
        }
        #endregion

        #region Public Action Methods
        /// <summary>
        /// Gets a list of RMA request items.
        /// </summary>
        /// <returns>List of RMA request item.</returns> 
        [HttpGet]
        public HttpResponseMessage GetRMARequestItemList()
        {
            HttpResponseMessage response;

            try
            {
                string data = _cache.GetRMARequestItems(RouteUri, RouteTemplate);
                response = !Equals(data,null) ? CreateOKResponse<RMARequestItemListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                RMARequestItemListResponse data = new RMARequestItemListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        /// <summary>
        /// Gets a list of RMA item according to order line items.
        /// </summary>
        /// <returns>RMA request item according to order line items.</returns> 
        [HttpGet]
        public HttpResponseMessage GetRMARequestItemsForGiftCard(string orderLineItems)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetRMARequestItemsForGiftCard(RouteUri, RouteTemplate, orderLineItems);
                response = !Equals(data,null) ? CreateOKResponse<RMARequestItemListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new RMARequestItemListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        /// <summary>
        /// Creates a new RMA Request Item.
        /// </summary>
        /// <param name="model">The model of the RMA Request Item.</param>
        /// <returns>Http Response Message</returns>
        [HttpPost]
        public HttpResponseMessage Create([FromBody] RMARequestItemModel model)
        {
            HttpResponseMessage response;

            try
            {
                RMARequestItemModel rmaRequestitemModel = _service.CreateRMARequest(model);
                if (!Equals(rmaRequestitemModel,null))
                {
                    var uri = Request.RequestUri;
                    var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + rmaRequestitemModel.RMARequestItemID;

                    response = CreateCreatedResponse(new RMARequestItemResponse { RMARequestItem = rmaRequestitemModel });
                    response.Headers.Add("Location", location);
                }
                else
                {
                    response = CreateInternalServerErrorResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new RMARequestItemResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }
        #endregion
    }
}
