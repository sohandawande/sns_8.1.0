﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    public class VendorProductsController : BaseController
    {
        #region Private Variables
        private readonly IVendorProductService _service;
        private readonly IVendorProductCache _cache;
        #endregion

        #region Constructor
        public VendorProductsController()
        {
            _service = new VendorProductService();
            _cache = new VendorProductCache(_service);
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// To Get vendor product List.
        /// </summary>
        /// <returns></returns>
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage GetVendorProductList()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetVendorProductList(RouteUri, RouteTemplate);
                response = (!Equals(data, null)) ? CreateOKResponse<VendorProductListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new VendorProductListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

         /// <summary>
        /// To Get mall admin product List.
        /// </summary>
        /// <returns>returns list of mall admin products</returns>
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage GetMallAdminProductList()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetMallAdminProductList(RouteUri, RouteTemplate);
                response = (!Equals(data, null)) ? CreateOKResponse<VendorProductListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new VendorProductListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
       
        
        /// <summary>
        /// To Change Vendor Product Status.
        /// </summary>
        /// <param name="state">string state</param>
        /// <param name="model">RejectProductModel model</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage ChangeProductStatus(string state, RejectProductModel model)
        {
            HttpResponseMessage response;

            try
            {
                var status = _service.ChangeProductStatus(model, state);
                response = status ? CreateOKResponse(new VendorProductListResponse()) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new VendorProductListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        /// <summary>
        /// To Get Vendor Product Rejected Product Details.
        /// </summary>
        /// <param name="productIds">string productIds</param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage BindRejectProduct(string productIds)
        {
            HttpResponseMessage response;
            try
            {
                var data = _service.BindRejectProduct(productIds);
                response = (!Equals(data, null)) ? CreateOKResponse(new RejectProductModelResponse { Product = data }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new VendorProductListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

      
        
        /// <summary>
        /// To Get vendor product Image List.
        /// </summary>
        /// <returns></returns>
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage GetReviewImageList()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetReviewImageList(RouteUri, RouteTemplate);
                response = (!Equals(data, null)) ? CreateOKResponse<ProductAlternateImageListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductAlternateImageListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }


        /// <summary>
        /// To Change Vendor Product images Status.
        /// </summary>
        /// <param name="productIds">string productIds</param>
        /// <param name="state">string state</param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage UpdateProductImageStatus(string productIds, string state)
        {
            HttpResponseMessage response;

            try
            {
                var status = _service.UpdateProductImageStatus(productIds, state);
                response = status ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new VendorProductListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        /// <summary>
        /// Updates an existing product.
        /// </summary>
        /// <param name="productId">The ID of the product.</param>
        /// <param name="model">The model of the product.</param>
        /// <returns></returns>
        [HttpPut]
        public HttpResponseMessage UpdateProduct(int productId, [FromBody] ProductModel model)
        {
            HttpResponseMessage response;

            try
            {
                var product = _service.UpdateVendorProduct(productId, model);
                response = !Equals(product, null) ? CreateOKResponse(new ProductResponse { Product = product }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Updates an existing product Marketing details.
        /// </summary>
        /// <param name="productId">The ID of the product.</param>
        /// <param name="model">The model of the product.</param>
        /// <returns></returns>
        [HttpPut]
        public HttpResponseMessage UpdateMarketingDetails(int productId, [FromBody] ProductModel model)
        {
            HttpResponseMessage response;
            string errorCode;
            try
            {
                var product = _service.UpdateMarketingDetails(productId, model, out errorCode);
                response = !Equals(product, null) ? CreateOKResponse(new ProductResponse { Product = product, ErrorCode = Convert.ToInt32(errorCode), HasError = !Equals(errorCode, "0") }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
        /// <summary>
        /// To Get the Product Caterogy Nodes based on Product Id.
        /// </summary>
        /// <param name="productId">The ID of the product.</param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetCategoryNode(int productId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _service.GetCategoryNode(productId);
                response = (!Equals(data, null)) ? CreateOKResponse(new CategoryNodeListResponse { CategoryNodes = data.CategoryNodes }) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductCategoryListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// To Delete the product details based on productId.
        /// </summary>
        /// <param name="productId">Id for the product</param>
        /// <returns>Return true or false</returns>
         [HttpDelete]
        public HttpResponseMessage Delete(int productId)
        {
            HttpResponseMessage response;

            try
            {
                var deleted = _service.DeleteProduct(productId);
                response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
        #endregion

    }
}
