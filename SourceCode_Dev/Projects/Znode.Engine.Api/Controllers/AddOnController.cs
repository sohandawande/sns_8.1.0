﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    public class AddOnController : BaseController
    {
        #region Private Methods
        private readonly IAddOnService _service;
        private readonly IAddOnCache _cache;
        #endregion

        #region Constructor
        public AddOnController()
        {
            _service = new AddOnService();
            _cache = new AddOnCache(_service);
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Get a Add-On.     
        /// </summary>
        /// <param name="addOnId">The ID of the Add-On.</param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage Get(int addOnId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetAddOn(addOnId, RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<AddOnResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new AddOnResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets a list of Add-On.
        /// </summary>
        /// <returns></returns>
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage List()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetAddOns(RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<AddOnListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new AddOnListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Creates a new Add-On.
        /// </summary>
        /// <param name="model">The model of the Add-On.</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Create([FromBody] AddOnModel model)
        {
            HttpResponseMessage response;

            try
            {
                var addOn = _service.CreateAddOn(model);
                if (!Equals(addOn, null))
                {
                    var uri = Request.RequestUri;
                    var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + addOn.AddOnId;

                    response = CreateCreatedResponse(new AddOnResponse { AddOn = addOn });
                    response.Headers.Add("Location", location);
                }
                else
                {
                    response = CreateInternalServerErrorResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new AddOnResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        /// <summary>
        /// Updates an existing Add-On.
        /// </summary>
        /// <param name="addOnId">The ID of the Add-On.</param>
        /// <param name="model">The model of the Add-On.</param>
        /// <returns></returns>
        [HttpPut]
        public HttpResponseMessage Update(int addOnId, [FromBody] AddOnModel model)
        {
            HttpResponseMessage response;

            try
            {
                var addOn = _service.UpdateAddOn(addOnId, model);
                response = addOn != null ? CreateOKResponse(new AddOnResponse { AddOn = addOn }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new AddOnResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Deletes an existing Add-On.
        /// </summary>
        /// <param name="addOnId">The ID of the Add-On.</param>
        /// <returns></returns>
        [HttpDelete]
        public HttpResponseMessage Delete(int addOnId)
        {
            HttpResponseMessage response;

            try
            {
                var deleted = _service.DeleteAddOn(addOnId);
                response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new AddOnResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
        #endregion
    }
}

