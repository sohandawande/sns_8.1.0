﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    public class PRFTUpdatePriceAndInventoryController : BaseController
    {
        #region private variable
        private readonly IPRFTUpdatePriceAndInventoryService _service;
        #endregion

        #region constructor
       public PRFTUpdatePriceAndInventoryController()
        {
            _service = new PRFTUpdatePriceAndInventoryService();
        }
        #endregion

        [HttpGet]
        public HttpResponseMessage UpdatePriceAndInventory()
        {
            HttpResponseMessage response;
            try
            {
                _service.UpdatePriceAndInventory();
                var data = "ok";
                response = data != null ? CreateOKResponse(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = ex.Message;
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
    }
}
