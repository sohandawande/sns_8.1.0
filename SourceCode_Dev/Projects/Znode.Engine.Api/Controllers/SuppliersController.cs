﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    public class SuppliersController : BaseController
    {
        private readonly ISupplierService _service;
        private readonly ISupplierCache _cache;

        public SuppliersController()
        {
            _service = new SupplierService();
            _cache = new SupplierCache(_service);
        }

        /// <summary>
        /// Gets a supplier.
        /// </summary>
        /// <param name="supplierId">The ID of the supplier.</param>
        /// <returns></returns>
        [ExpandSupplierType]
        [HttpGet]
        public HttpResponseMessage Get(int supplierId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetSupplier(supplierId, RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<SupplierResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new SupplierResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets a list of suppliers.
        /// </summary>
        /// <returns></returns>
        [ExpandSupplierType]
        [FilterExternalId, FilterExternalSupplierNumber, FilterIsActive, FilterName, FilterPortalId, FilterSupplierTypeId]
        [SortDisplayOrder, SortName, SortSupplierId]
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage List()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetSuppliers(RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<SupplierListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new SupplierListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Creates a new supplier.
        /// </summary>
        /// <param name="model">The model of the supplier.</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Create([FromBody] SupplierModel model)
        {
            HttpResponseMessage response;

            try
            {
                var supplier = _service.CreateSupplier(model);
                if (!Equals(supplier, null))
                {
                    var uri = Request.RequestUri;
                    var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + supplier.SupplierId;

                    response = CreateCreatedResponse(new SupplierResponse { Supplier = supplier });
                    response.Headers.Add("Location", location);
                }
                else
                {
                    response = CreateInternalServerErrorResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new SupplierResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Updates an existing supplier.
        /// </summary>
        /// <param name="supplierId">The ID of the supplier.</param>
        /// <param name="model">The model of the supplier.</param>
        /// <returns></returns>
        [HttpPut]
        public HttpResponseMessage Update(int supplierId, [FromBody] SupplierModel model)
        {
            HttpResponseMessage response;

            try
            {
                var supplier = _service.UpdateSupplier(supplierId, model);
                response = !Equals(supplier, null) ? CreateOKResponse(new SupplierResponse { Supplier = supplier }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new SupplierResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Deletes an existing supplier.
        /// </summary>
        /// <param name="supplierId">The ID of the supplier.</param>
        /// <returns></returns>
        [HttpDelete]
        public HttpResponseMessage Delete(int supplierId)
        {
            HttpResponseMessage response;

            try
            {
                var deleted = _service.DeleteSupplier(supplierId);
                response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new SupplierResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
    }
}