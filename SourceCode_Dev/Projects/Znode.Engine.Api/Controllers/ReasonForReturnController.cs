﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    public class ReasonForReturnController : BaseController
    {
        #region Private Variables
        private readonly IReasonForReturnService _service;
        private readonly IReasonForReturnCache _cache; 
        #endregion
        
        #region Controller
        /// <summary>
        /// Api controller for ReasonForReturn
        /// </summary>
        public ReasonForReturnController()
        {
            _service = new ReasonForReturnService();
            _cache = new ReasonForReturnCache(_service);
        } 
        #endregion

        #region Public Action Methods
        /// <summary>
        /// Gets the ReasonForReturn By reasonForReturnId.
        /// </summary>
        /// <param name="reasonForReturnId">The Id of the ReasonForReturn.</param>
        /// <returns>Returns HttpResponseMessage type response.</returns>
        [HttpGet]
        public HttpResponseMessage Get(int reasonForReturnId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetReasonForReturn(reasonForReturnId, RouteUri, RouteTemplate);
                response = (!Equals(data, null)) ? CreateOKResponse<ReasonForReturnResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ReasonForReturnResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        /// <summary>
        /// Gets the ReasonForReturn list.
        /// </summary>       
        /// <returns>Returns HttpResponseMessage type response.</returns>
        [HttpGet]
        public HttpResponseMessage List()
        {
            HttpResponseMessage response;
            try
            {
                var data = _cache.GetListOfReasonForReturn(RouteUri, RouteTemplate);
                response = (!Equals(data, null)) ? CreateOKResponse<ReasonForReturnListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ReasonForReturnListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        /// <summary>
        /// Creates new ReasonForReturn 
        /// </summary>
        /// <param name="model">The model of the reasonForReturn.</param>
        /// <returns>Returns HttpResponseMessage type response.</returns>
        [HttpPost]
        public HttpResponseMessage Create([FromBody] ReasonForReturnModel model)
        {
            HttpResponseMessage response;

            try
            {
                var reasonForReturn = _service.CreateReasonForReturn(model);
                if (!Equals(reasonForReturn, null))
                {
                    var uri = Request.RequestUri;
                    var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + reasonForReturn.ReasonForReturnId;

                    response = CreateCreatedResponse(new ReasonForReturnResponse { ReasonForReturn = reasonForReturn });
                    response.Headers.Add("Location", location);
                }
                else
                {
                    response = CreateInternalServerErrorResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new ReasonForReturnResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        /// <summary>
        /// Updates an existing ReasonForReturn by reasonForReturn Id.
        /// </summary>
        /// <param name="reasonForReturnId">The Id of the reasonForReturn.</param>
        /// <param name="model">The model of the reasonForReturn.</param>
        /// <returns>Returns HttpResponseMessage type response.</returns>
        [HttpPut]
        public HttpResponseMessage Update(int reasonForReturnId, [FromBody] ReasonForReturnModel model)
        {
            HttpResponseMessage response;

            try
            {
                var reasonForReturn = _service.UpdateReasonForReturn(reasonForReturnId, model);
                response = (!Equals(reasonForReturn, null)) ? CreateOKResponse(new ReasonForReturnResponse { ReasonForReturn = reasonForReturn }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new ReasonForReturnResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        /// <summary>
        /// Deletes an existing ReasonForReturn.
        /// </summary>
        /// <param name="reasonForReturnId">The Id of the ReasonForReturn.</param>
        /// <returns>Returns HttpResponseMessage type response.</returns>
        [HttpDelete]
        public HttpResponseMessage Delete(int reasonForReturnId)
        {
            HttpResponseMessage response;

            try
            {
                var deleted = _service.DeleteReasonForReturn(reasonForReturnId);
                response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new ReasonForReturnResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }
        #endregion

    }
}
