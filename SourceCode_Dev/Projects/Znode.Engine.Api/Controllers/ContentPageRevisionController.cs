﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    /// <summary>
    /// Class Content Page Revision
    /// </summary>
    public class ContentPageRevisionController : BaseController
    {
        #region Private Variables
        private readonly IContentPageRevisionService _service;
        private readonly IContentPageRevisionCache _cache; 
        #endregion

        #region Default Constructor
        public ContentPageRevisionController()
        {
            _service = new ContentPageRevisionService();
            _cache = new ContentPageRevisionCache(_service);
        } 
        #endregion

        #region Public Action Methods

        /// <summary>
        /// Gets a list of content page revisions.
        /// </summary>
        /// <returns></returns>
        [FilterContentPageId]
        [HttpGet]
        public HttpResponseMessage List()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetContentPageRevisions(RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<ContentPageRevisionListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ContentPageRevisionListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }


        /// <summary>
        /// Gets a list of content page revisions.
        /// </summary>
        /// <param name="contentPageId">Id of the Content page.</param>
        /// <returns></returns>
        [HttpGet]
        [FilterContentPageId]
        public HttpResponseMessage GetContentPageRevisionsById(int contentPageId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetContentPageRevisionOnId(contentPageId, RouteUri, RouteTemplate);
                response = (!Equals(data, null)) ? CreateOKResponse<ContentPageRevisionListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ContentPageRevisionListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Revert the revision to the previous version.
        /// </summary>
        /// <param name="revisionId">Id of the Content page revision</param>
        /// <param name="model">ContentPageRevisionModel model</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage RevertRevision(int revisionId, ContentPageRevisionModel model)
        {
            HttpResponseMessage response;

            try
            {
                bool isRevertRevision = _service.RevertRevision(revisionId, model);

                response = isRevertRevision ? CreateCreatedResponse(new ContentPageRevisionResponse { IsRevertRevision = true }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new ContentPageRevisionResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        [PageIndex, PageSize]
        [SortCategoryId]
        [HttpGet]
        public HttpResponseMessage GetContentPageRevisionById(int contentPageId, int totalRowCount = 0)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetContentPageRevisionOnId(contentPageId, out totalRowCount, RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<ContentPageRevisionListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ContentPageRevisionListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        } 

        #endregion

    }
}
