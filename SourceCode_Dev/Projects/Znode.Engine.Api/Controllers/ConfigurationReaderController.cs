﻿using System;
using System.Net.Http;
using System.Web.Mvc;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    public class ConfigurationReaderController : BaseController
    {
        private readonly IConfigurationReaderService _service;
        private readonly IConfigurationReaderCache _cache;

        public ConfigurationReaderController()
        {
            _service = new ConfigurationReaderService();
            _cache = new ConfigurationReaderCache(_service);
        }

        /// <summary>
        /// Get dynamic grid configuration XML
        /// </summary>
        /// <param name="itemName">string Item Name</param>
        /// <returns>returns HttpResponseMessage</returns>
        [HttpGet]
        public HttpResponseMessage GetFilterConfigurationXML(string itemName)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetFilterConfigurationXML(itemName, RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<ConfigurationReaderResponce>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new CatalogResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
    }
}
