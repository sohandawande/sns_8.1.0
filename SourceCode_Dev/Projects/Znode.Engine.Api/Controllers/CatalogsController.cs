﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    public class CatalogsController : BaseController
    {
        private readonly ICatalogService _service;
        private readonly ICatalogCache _cache;

        public CatalogsController()
        {
            _service = new CatalogService();
            _cache = new CatalogCache(_service);
        }

        /// <summary>
        /// Gets a catalog.
        /// </summary>
        /// <param name="catalogId">The ID of the catalog.</param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage Get(int catalogId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetCatalog(catalogId, RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<CatalogResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new CatalogResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets a list of catalogs.
        /// </summary>
        /// <returns></returns>
        [FilterExternalId, FilterIsActive, FilterName, FilterPortalId]
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage List()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetCatalogs(RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<CatalogListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new CatalogListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets a list of catalogs for the specified catalog IDs.
        /// </summary>
        /// <param name="catalogIds">The comma-separated IDs of the catalogs.</param>
        /// <returns></returns>
        [SortCatalogId, SortName]
        [HttpGet]
        public HttpResponseMessage ListByCatalogIds(string catalogIds)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetCatalogsByCatalogIds(catalogIds, RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<CatalogListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new CatalogListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }


        /// <summary>
        /// Creates a new catalog.
        /// </summary>
        /// <param name="model">The model of the catalog.</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Create([FromBody] CatalogModel model)
        {
            HttpResponseMessage response;

            try
            {
                var catalog = _service.CreateCatalog(model);
                if (!Equals(catalog, null))
                {
                    var uri = Request.RequestUri;
                    var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + catalog.CatalogId;

                    response = CreateCreatedResponse(new CatalogResponse { Catalog = catalog });
                    response.Headers.Add("Location", location);
                }
                else
                {
                    response = CreateInternalServerErrorResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new CatalogResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Updates an existing catalog.
        /// </summary>
        /// <param name="catalogId">The ID of the catalog.</param>
        /// <param name="model">The model of the catalog.</param>
        /// <returns></returns>
        [HttpPut]
        public HttpResponseMessage Update(int catalogId, [FromBody] CatalogModel model)
        {
            HttpResponseMessage response;

            try
            {
                var catalog = _service.UpdateCatalog(catalogId, model);
                response = !Equals(catalog, null) ? CreateOKResponse(new CatalogResponse { Catalog = catalog }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new CatalogResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Deletes an existing catalog.
        /// </summary>
        /// <param name="catalogId">The ID of the catalog.</param>
        /// <returns></returns>
        [HttpDelete]
        public HttpResponseMessage Delete(int catalogId)
        {
            HttpResponseMessage response;

            try
            {
                var deleted = _service.DeleteCatalog(catalogId);
                response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new CatalogResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        #region Znode Version 8.0
        /// <summary>
        /// Copies an existing catalog.
        /// </summary>
        /// <param name="model">The model of the catalog.</param>
        /// <returns>Returns the copy of existing catalog.</returns>
        [HttpPost]
        public HttpResponseMessage CopyCatalog([FromBody] CatalogModel model)
        {
            HttpResponseMessage response;

            try
            {
                bool isCopied = _service.CopyCatalog(model);
                BooleanModel boolModel = new BooleanModel { disabled = isCopied };
                response = isCopied ? CreateOKResponse(new TrueFalseResponse { booleanModel = boolModel }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new CatalogResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Deletes an existing catalog.
        /// </summary>
        /// <param name="catalogId">The Id of the catalog.</param>
        /// <param name="preserveCategories">boolean value for preserve categories.</param>
        /// <returns>Deletes the catalog.</returns>
        [HttpDelete]
        public HttpResponseMessage DeleteCatalog(int catalogId, bool preserveCategories)
        {
            HttpResponseMessage response;
            try
            {
                response = _service.DeleteCatalogUsingCustomService(catalogId, preserveCategories) ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new CatalogResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        public HttpResponseMessage GetCatalogsByPortalId(int portalId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetCatalogsByPortalId(portalId, RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<CatalogListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new CatalogListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        #endregion
    }
}