﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    public class EmailTemplatesController : BaseController
    {
        private readonly IEmailTemplateService _service;
        private readonly IEmailTemplateCache _cache;

        public EmailTemplatesController()
		{
            _service = new EmailTemplateService();
            _cache = new EmailTemplateCache(_service);
		}

        /// <summary>
        /// Gets a list of EmailTemplates.
        /// </summary>
        /// <returns>HttpResponseMessage</returns>
        [FilterIsActive, FilterName, FilterPortalId]
        [SortDisplayOrder, SortName, SortManufacturerId]
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage List()
        {
            HttpResponseMessage response;

            try
            {
                string data = _cache.GetEmailTemplates(RouteUri, RouteTemplate);
                response = !Equals(data , null) ? CreateOKResponse<EmailTemplateListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                EmailTemplateListResponse data = new EmailTemplateListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
        //GetKeys

        // GET: EmailTemplates
        /// <summary>
        /// Gets a list of deleted templates and their keys in dropdown.
        /// </summary>
        /// <returns>HttpResponseMessage</returns>
        [FilterIsActive, FilterName, FilterPortalId]
        [SortDisplayOrder, SortName, SortManufacturerId]
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage GetKeys()
        {
            HttpResponseMessage response;

            try
            {
                string data = _cache.GetDeletedTemplates(RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<EmailTemplateListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                EmailTemplateListResponse data = new EmailTemplateListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Creates a new template Page.
        /// </summary>
        /// <param name="model">The model of the Email template Page.</param>
        /// <returns>HttpResponseMessage</returns>
        [HttpPost]
        public HttpResponseMessage Create([FromBody] EmailTemplateModel model)
        {
            HttpResponseMessage response;

            try
            {
                EmailTemplateModel templatePage = _service.CreateTemplatePage(model);
                if (!Equals(templatePage, null))
                {
                    Uri uri = Request.RequestUri;
                    string location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + templatePage.TemplateName;

                    response = CreateCreatedResponse(new EmailTemplateResponse { EmailTemplate = templatePage });
                    response.Headers.Add("Location", location);
                }
                else
                {
                    response = CreateInternalServerErrorResponse();
                }
            }
            catch (Exception ex)
            {
                EmailTemplateResponse data = new EmailTemplateResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets a template page.
        /// </summary>
        /// <param name="templateName">The template name of the template page.</param>
        /// <returns></returns>
        [ExpandCategoryNodes, ExpandProductIds, ExpandSubcategories]
        [HttpGet]
        public HttpResponseMessage Get(string templateName, string extension)
        {
            HttpResponseMessage response;

            try
            {
                string data = _cache.GetTemplatePage(templateName,extension, RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<EmailTemplateResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                EmailTemplateResponse data = new EmailTemplateResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Updates an existing Template page.
        /// </summary>
        /// <param name="templateName">The name of the Template</param>
        /// <param name="model">EmailTemplateModel</param>
        /// <returns>HttpResponseMessage</returns>
        [HttpPut]
        public HttpResponseMessage Update([FromBody] EmailTemplateModel model)
        {
            HttpResponseMessage response;

            try
            {
                bool isUpdated = _service.UpdateTemplatePage(model);

                BooleanModel boolModel = new BooleanModel() { disabled = isUpdated };

                TrueFalseResponse trueFalseResponse = new TrueFalseResponse() { booleanModel = boolModel };
                response = CreateOKResponse(trueFalseResponse);
            }
            catch (Exception ex)
            {
                EmailTemplateResponse data = new EmailTemplateResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        /// <summary>
        /// Deletes an existing Template Page.
        /// </summary>
        /// <param name="templateName">Name of the Template Page.</param>
        /// <param name="extension">extension of the Template Page.</param>
        /// <returns>HttpResponseMessage</returns>
        [HttpDelete]
        public HttpResponseMessage Delete(string templateName, string extension)
        {
            HttpResponseMessage response;

            try
            {
                bool deleted = _service.DeleteTemplatePage(templateName,extension);
                response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                EmailTemplateResponse data = new EmailTemplateResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

    }
}