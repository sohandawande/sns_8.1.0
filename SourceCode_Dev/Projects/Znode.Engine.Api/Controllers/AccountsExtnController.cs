﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Engine.Api.Controllers
{
    public partial class AccountsController : BaseController
    {
        [HttpGet]
        public HttpResponseMessage GetSalesRepInfoByRoleName(string roleName)
        {
            HttpResponseMessage response;            
            try
            {
                var data = _service.GetSalesRepInfoByRoleName(roleName);
                response = !Equals(data, null) ? CreateOKResponse<PRFTAccountIdAndUserNameResponse>(new PRFTAccountIdAndUserNameResponse{AccountIdAndUserNameList=data}) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        [HttpGet]
        public HttpResponseMessage GetAccountDetailsFromERP(string ExternalId)
        {
            HttpResponseMessage response;
            try
            {
                var data = _service.GetAccountDetailsFromERP(ExternalId);
                response = !Equals(data, null) ? CreateOKResponse<PRFTERPAccountResponse>(new PRFTERPAccountResponse { ERPAccount = data }) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
    }
}