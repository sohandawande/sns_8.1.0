﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    public class DiagnosticsController : BaseController
    {
        #region Variables

        private IDiagnosticsService _service;
        private IDiagnosticsCache _cache;

        #endregion

        #region Constructor

        public DiagnosticsController()
        {
            _service = new DiagnosticsService();
            _cache = new DiagnosticsCache(_service);
        }

        #endregion

        /// <summary>
        /// This method calls diagnostics cache to check SMTP Account
        /// </summary>
        /// <returns>DiagnosticsResponse in HttpResponseMessage format which will contain the status of SMPT account</returns>
        [HttpGet]
        public HttpResponseMessage CheckEmailAccount()
        {
            HttpResponseMessage response;
            try
            {
                string data = _cache.CheckEmailAccount();
                response = !Equals(data,null) ? CreateOKResponse<DiagnosticsResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                DiagnosticsResponse data = new DiagnosticsResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        /// <summary>
        /// This method calls diagnostics cache to get Version details of product from database
        /// </summary>
        /// <returns>Returns the DiagnosticsResponse in HttpResponseMessage format which contains the version details</returns>
        [HttpGet]
        public HttpResponseMessage GetProductVersionDetails()
        {
            HttpResponseMessage response;
            try
            {
                string data = _cache.GetProductVersionDetails();
                response = !Equals(data, null) ? CreateOKResponse<DiagnosticsResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                DiagnosticsResponse data = new DiagnosticsResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        /// <summary>
        /// This method sends the diagnostics email
        /// </summary>
        /// <param name="model">DiagnosticsEmailModel which should contain case number and merged text (diagnostics data)</param>
        /// <returns>Returns the DiagnosticsResponse in HttpResponseMessage format which contains the email sent status</returns>
        [HttpPost]
        public HttpResponseMessage EmailDiagnostics(DiagnosticsEmailModel model)
        {
            HttpResponseMessage response;
            try
            {
                DiagnosticsResponse data = new DiagnosticsResponse { HasError = !_service.EmailDiagnostics(model) };
                response = !Equals(data, null) ? CreateOKResponse<DiagnosticsResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                DiagnosticsResponse data = new DiagnosticsResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }
    }
}