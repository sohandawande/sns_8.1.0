﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    public class CSSController : BaseController
    {
        #region Private Variables
        private readonly ICSSService _service;
        private readonly ICSSCache _cache; 
        #endregion

        #region Constructor
        public CSSController()
        {
            _service = new CSSService();
            _cache = new CSSCache(_service);
        } 
        #endregion

        #region Public Action Methods
        /// <summary>
        /// Gets a list of CSS.
        /// </summary>
        /// <returns>List of CSS</returns>       
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage List()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetCSSs(RouteUri, RouteTemplate);
                response = !Equals(data,null) ? CreateOKResponse<CSSListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new CSSListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Create new CSS
        /// </summary>
        /// <param name="model">CSSModel</param>
        /// <returns>HttpResponse Message</returns>
        [HttpPost]
        public HttpResponseMessage Create([FromBody] CSSModel model)
        {
            HttpResponseMessage response;

            try
            {
                var css = _service.CreateCSS(model);
                if (!Equals(css, null))
                {
                    var uri = Request.RequestUri;
                    string location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + css.CSSID;

                    response = CreateCreatedResponse(new CSSResponse { CSS = css });
                    response.Headers.Add("Location", location);
                }
                else
                {
                    response = CreateInternalServerErrorResponse();
                }
            }
            catch (Exception ex)
            {
                CSSResponse data = new CSSResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Get css by cssId
        /// </summary>
        /// <param name="cssId">CSSId to get css</param>
        /// <returns>HttpResponse Message</returns>
        [HttpGet]
        public HttpResponseMessage Get(int cssId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetCSS(cssId, RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<CSSResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                CSSResponse data = new CSSResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        /// <summary>
        /// Update the CSS
        /// </summary>
        /// <param name="cssId">cssId to get css</param>
        /// <param name="model">CSSModel</param>
        /// <returns>HttpResponse Message</returns>
        [HttpPut]
        public HttpResponseMessage Update(int cssId, [FromBody] CSSModel model)
        {
            HttpResponseMessage response;

            try
            {
                var css = _service.UpdateCSS(cssId, model);
                response = !Equals(css, null) ? CreateOKResponse(new CSSResponse { CSS = css }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                CSSResponse data = new CSSResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Deletes an existing CSS by CSS ID.
        /// </summary>
        /// <param name="cssId">The ID of the CSS.</param>
        /// <returns>HttpResponse Message</returns>
        [HttpDelete]
        public HttpResponseMessage Delete(int cssId)
        {
            HttpResponseMessage response;

            try
            {
                var deleted = _service.DeleteCSS(cssId);
                response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                CSSResponse data = new CSSResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }
        #endregion
    }
}
