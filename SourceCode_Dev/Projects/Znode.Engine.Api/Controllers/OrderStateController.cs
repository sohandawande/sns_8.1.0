﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    public class OrderStateController : BaseController
    {
        private readonly IOrderStateService _service;
        private readonly IOrderStateCache _cache;

        public OrderStateController()
        {
            _service = new OrderStateService();
            _cache = new OrderStateCache(_service);
        }

        /// <summary>
        /// Gets the Order state List.
        /// </summary>
        /// <returns></returns>
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage List()
        {
            HttpResponseMessage response;
            try
            {
                var data = _cache.GetOrderStates(RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<OrderStateListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new OrderStateListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }
    }
}