﻿using System;
using System.Net.Http;
using System.Web.Hosting;
using System.Web.Http;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Engine.Api.Controllers
{
    public class LoadPluginsController : BaseController
    {

        /// <summary>
        /// Load Api Plugins
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage LoadPlugins()
        {
            HttpResponseMessage response;
            try
            {
                UpdateGlobalAsax();
                response = CreateNoContentResponse();
            }
            catch (Exception ex)
            {
                var data = new BaseResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        /// <summary>
        /// Update Global.asax file time
        /// </summary>
        private void UpdateGlobalAsax()
        {
            var path = HostingEnvironment.MapPath("~/global.asax");
            System.IO.File.SetLastWriteTimeUtc(path, DateTime.UtcNow);
        }
    }
}