﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    public class ThemeController : BaseController
    {
        #region Private Variables
        private readonly IThemeService _service;
        private readonly IThemeCache _cache;
        #endregion

        #region Constructor
        /// <summary>
        /// Constuctor for ThemeController
        /// </summary>
        public ThemeController()
        {
            _service = new ThemeService();
            _cache = new ThemeCache(_service);
        }
        #endregion

        #region Public methods
        /// <summary>
        /// Gets the list of Themes.
        /// </summary>
        /// <returns>List of themes.</returns>
        [HttpGet]
        public HttpResponseMessage List()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetThemes(RouteUri, RouteTemplate);
                response = !Equals(data,null) ? CreateOKResponse<ThemeListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ThemeListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
        #endregion

        /// <summary>
        /// Create new Theme
        /// </summary>
        /// <param name="model">ThemeModel</param>
        /// <returns>HttpResponse Message</returns>
        [HttpPost]
        public HttpResponseMessage Create([FromBody] ThemeModel model)
        {
            HttpResponseMessage response;

            try
            {
                var theme = _service.CreateTheme(model);
                if (!Equals(theme,null))
				{
					var uri = Request.RequestUri;
                    string location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + theme.ThemeID;

                    response = CreateCreatedResponse(new ThemeResponse { Theme = theme });
					response.Headers.Add("Location", location);
				}
				else
				{
					response = CreateInternalServerErrorResponse();
				}
            }
            catch (Exception ex)
            {
                var data = new ThemeResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Get theme by themeId
        /// </summary>
        /// <param name="themeId">ThemeId to get theme</param>
        /// <returns>HttpResponse Message</returns>
        [HttpGet]
        public HttpResponseMessage Get(int themeId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetTheme(themeId, RouteUri, RouteTemplate);
                response = !Equals(data,null) ? CreateOKResponse<ThemeResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ThemeResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        /// <summary>
        /// Update the Theme
        /// </summary>
        /// <param name="themeId">themeId to get theme</param>
        /// <param name="model">ThemeModel</param>
        /// <returns>HttpResponse Message</returns>
        [HttpPut]
        public HttpResponseMessage Update(int themeId, [FromBody] ThemeModel model)
        {
            HttpResponseMessage response;

            try
            {
                var theme = _service.UpdateTheme(themeId, model);
                response = !Equals(theme,null)? CreateOKResponse(new ThemeResponse { Theme = theme }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new ThemeResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Deletes an existing Theme by Theme ID.
        /// </summary>
        /// <param name="themeId">The ID of the Theme.</param>
        /// <returns>HttpResponse Message</returns>
        [HttpDelete]
        public HttpResponseMessage Delete(int themeId)
        {
            HttpResponseMessage response;

            try
            {
                var deleted = _service.DeleteTheme(themeId);
                response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                ThemeResponse data = new ThemeResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }
    }
}
