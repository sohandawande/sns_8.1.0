﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    public class RequestStatusController : BaseController
    {
        #region Private variables
        private readonly IRequestStatusService _service;
        private readonly IRequestStatusCache _cache;        
        #endregion

        #region Constructor
        public RequestStatusController()
        {
            _service = new RequestStatusService();
            _cache = new RequestStatusCache(_service);
        } 
        #endregion
        
        #region Public Action Methods

        /// <summary>
        /// Get the RequestStatus By requestStatusId.
        /// </summary>
        /// <param name="requestStatusId">The Id of the RequestStatus.</param>
        /// <returns>Returns HttpResponseMessage type response.</returns>
        [HttpGet]
        public HttpResponseMessage Get(int requestStatusId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetRequestStatus(requestStatusId, RouteUri, RouteTemplate);
                response = (!Equals(data, null)) ? CreateOKResponse<RequestStatusResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new RequestStatusResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }        

        /// <summary>
        /// Gets a list of Request status.
        /// </summary>
        /// <returns>List of Request status</returns>   
        [HttpGet]
        public HttpResponseMessage List()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetRequestStatusList(RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<RequestStatusListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new CSSListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Creates new RequestStatus 
        /// </summary>
        /// <param name="model">The model of the requestStatus.</param>
        /// <returns>Returns HttpResponseMessage type response.</returns>
        [HttpPost]
        public HttpResponseMessage Create([FromBody] RequestStatusModel model)
        {
            HttpResponseMessage response;

            try
            {
                var requestStatus = _service.CreateRequestStatus(model);
                if (!Equals(requestStatus, null))
                {
                    var uri = Request.RequestUri;
                    var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + requestStatus.RequestStatusID;

                    response = CreateCreatedResponse(new RequestStatusResponse { RequestStatus = requestStatus });
                    response.Headers.Add("Location", location);
                }
                else
                {
                    response = CreateInternalServerErrorResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new RequestStatusResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        /// <summary>
        /// Updates an existing RequestStatus by requestStatus Id.
        /// </summary>
        /// <param name="requestStatusId">The Id of the requestStatus.</param>
        /// <param name="model">The model of the requestStatus.</param>
        /// <returns>Returns HttpResponseMessage type response.</returns>
        [HttpPut]
        public HttpResponseMessage Update(int requestStatusId, [FromBody] RequestStatusModel model)
        {
            HttpResponseMessage response;

            try
            {
                var requestStatus = _service.UpdateRequestStatus(requestStatusId, model);
                response = (!Equals(requestStatus, null)) ? CreateOKResponse(new RequestStatusResponse { RequestStatus = requestStatus }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new RequestStatusResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        /// <summary>
        /// Deletes an existing RequestStatus.
        /// </summary>
        /// <param name="requestStatusId">The Id of the RequestStatus.</param>
        /// <returns>Returns HttpResponseMessage type response.</returns>
        [HttpDelete]
        public HttpResponseMessage Delete(int requestStatusId)
        {
            HttpResponseMessage response;

            try
            {
                var deleted = _service.DeleteRequestStatus(requestStatusId);
                response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new RequestStatusResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }
        #endregion
    }
}
