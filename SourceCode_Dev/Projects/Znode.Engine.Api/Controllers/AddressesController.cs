﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
	public class AddressesController : BaseController
	{
		private readonly IAddressService _service;
		private readonly IAddressCache _cache;

		public AddressesController()
		{
			_service = new AddressService();
			_cache = new AddressCache(_service);
		}

		/// <summary>
		/// Gets an address.
		/// </summary>
		/// <param name="addressId">The ID of the address.</param>
		/// <returns></returns>
		[HttpGet]
		public HttpResponseMessage Get(int addressId)
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetAddress(addressId, RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<AddressResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new AddressResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Gets a list of addresses.
		/// </summary>
		/// <returns></returns>
		[FilterAccountId, FilterCity, FilterCompanyName, FilterCountryCode, FilterLastName, FilterPostalCode, FilterStateCode]
		[SortAddressId, SortCountryCode, SortPostalCode, SortStateCode]
		[PageIndex, PageSize]
		[HttpGet]
		public HttpResponseMessage List()
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetAddresses(RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<AddressListResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new AddressListResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Creates a new address.
		/// </summary>
		/// <param name="model">The model of the address.</param>
		/// <returns></returns>
		[HttpPost]
		public HttpResponseMessage Create([FromBody] AddressModel model)
		{
			HttpResponseMessage response;

			try
			{
				var address = _service.CreateAddress(model);
				if (address != null)
				{
					var uri = Request.RequestUri;
					var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + address.AddressId;

					response = CreateCreatedResponse(new AddressResponse { Address = address });
					response.Headers.Add("Location", location);
				}
				else
				{
					response = CreateInternalServerErrorResponse();
				}
			}
			catch (Exception ex)
			{
				var data = new AddressResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Updates an existing address.
		/// </summary>
		/// <param name="addressId">The ID of the address.</param>
		/// <param name="model">The model of the address.</param>
		/// <returns></returns>
		[HttpPut]
		public HttpResponseMessage Update(int addressId, [FromBody] AddressModel model)
		{
			HttpResponseMessage response;

			try
			{
				var address = _service.UpdateAddress(addressId, model);
				response = address != null ? CreateOKResponse(new AddressResponse { Address = address }) : CreateInternalServerErrorResponse();
			}
			catch (Exception ex)
			{
				var data = new AddressResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

        //new method
        [HttpPut]
        public HttpResponseMessage UpdateAddress(int addressId, [FromBody] AddressModel model)
        {
            HttpResponseMessage response;
            string errorCode;
            try
            {
                var address = _service.UpdateAddressDefault(addressId,model, out errorCode);
                response = !Equals(address , null) ? CreateOKResponse(new AddressResponse { Address = address, ErrorCode = Convert.ToInt32(errorCode), HasError = errorCode != "0" }) : null;
            }
            catch (Exception ex)
            {
                var data = new AddressResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// To Update the Existing Customer address.
        /// </summary>
        /// <param name="accountId">Customer account id</param>
        /// <param name="listModel">AddressListModel with address data</param>
        /// <returns>HttpResponseMessage response</returns>
        [HttpPost]
        public HttpResponseMessage UpdateCustomerAddress(int accountId , [FromBody] AddressListModel listModel)
        {
            HttpResponseMessage response;

            try
            {
                AddressListModel list = _service.UpdateCustomerAddress(accountId,listModel);
                response = !Equals(list, null) ? CreateOKResponse(new AddressListResponse { AddressListModel = list }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new AddressListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }


		/// <summary>
		/// Deletes an existing address.
		/// </summary>
		/// <param name="addressId">The ID of the address.</param>
		/// <returns></returns>
		[HttpDelete]
		public HttpResponseMessage Delete(int addressId)
		{
			HttpResponseMessage response;

			try
			{
				var deleted = _service.DeleteAddress(addressId);
				response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
			}
			catch (Exception ex)
			{
				var data = new AddressResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}
	}
}
