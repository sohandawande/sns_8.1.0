﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    public class StoreLocatorController : BaseController
    {
        #region Private Variables
        private readonly IStoreLocatorService _service;
        private readonly IStoreCache _cache; 
        #endregion

        #region Public Constructor
        public StoreLocatorController()
        {
            _service = new StoreLocatorService();
            _cache = new StoreCache(_service);
        } 
        #endregion

        #region Public Methods
        /// <summary>
        /// Gets a Store Locator.
        /// </summary>
        /// <param name="storeId">The ID of the Store.</param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage Get(int storeId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetStoreLocator(storeId, RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<StoreLocatorResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new StoreLocatorResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets a list of manufacturers.
        /// </summary>
        /// <returns></returns>
        [FilterIsActive, FilterName, FilterPortalId]
        [SortDisplayOrder, SortName, SortManufacturerId]
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage List()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetStoreLocators(RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<StoreLocatorListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new StoreLocatorListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Creates a new store locator.
        /// </summary>
        /// <param name="model">The model of the store.</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Create([FromBody] StoreModel model)
        {
            HttpResponseMessage response;

            try
            {
                var storeLocator = _service.CreateStoreLocator(model);
                if (!Equals(storeLocator, null))
                {
                    var uri = Request.RequestUri;
                    var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + storeLocator.StoreID;

                    response = CreateCreatedResponse(new StoreLocatorResponse { StoreLocator = storeLocator });
                    response.Headers.Add("Location", location);
                }
                else
                {
                    response = CreateInternalServerErrorResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new StoreLocatorResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Updates an existing highlight.
        /// </summary>
        /// <param name="storeId">The ID of the highlight.</param>
        /// <param name="model">The model of the highlight.</param>
        /// <returns></returns>
        [HttpPut]
        public HttpResponseMessage Update(int storeId, [FromBody] StoreModel model)
        {
            HttpResponseMessage response;

            try
            {
                var storeLocator = _service.UpdateStoreLocator(storeId, model);
                response = storeLocator != null ? CreateOKResponse(new StoreLocatorResponse { StoreLocator = storeLocator }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new StoreLocatorResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Deletes an existing manufacturer.
        /// </summary>
        /// <param name="storeId">The ID of the manufacturer.</param>
        /// <returns></returns>
        [HttpDelete]
        public HttpResponseMessage Delete(int storeId)
        {
            HttpResponseMessage response;

            try
            {
                var deleted = _service.DeleteStoreLocator(storeId);
                response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new StoreLocatorResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        [HttpPost]
        public HttpResponseMessage GetStoresList([FromBody] StoreModel model)
        {
            HttpResponseMessage response;

            try
            {
                string storeList = _cache.GetStoresList(model, RouteUri, RouteTemplate);
                response = !Equals(storeList, null)? CreateOKResponse<StoreLocatorListResponse>(storeList) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                StoreLocatorListResponse data = new StoreLocatorListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data); 
            }
            return response;
        }
        #endregion
    }
}
