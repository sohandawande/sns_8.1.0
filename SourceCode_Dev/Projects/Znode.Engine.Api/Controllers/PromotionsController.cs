﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
	public class PromotionsController : BaseController
	{
		private readonly IPromotionService _service;
		private readonly IPromotionCache _cache;

		public PromotionsController()
		{
			_service = new PromotionService();
			_cache = new PromotionCache(_service);
		}

		/// <summary>
		/// Gets a promotion.
		/// </summary>
		/// <param name="promotionId">The ID of the promotion.</param>
		/// <returns></returns>
		[ExpandPromotionType]
		[HttpGet]
		public HttpResponseMessage Get(int promotionId)
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetPromotion(promotionId,  RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<PromotionResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new PromotionResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Gets a list of promotions.
		/// </summary>
		/// <returns></returns>
        [ExpandPromotionType, ExpandPortal, ExpandProfiles] 
		[FilterAccountId, FilterCatalogId, FilterCategoryId, FilterDiscount, FilterDiscountedProductId, FilterEndDate, FilterExternalId, FilterManufacturerId, FilterName, FilterPortalId, FilterProfileId, FilterPromotionTypeId, FilterRequiredProductId, FilterStartDate]
		[SortDiscount, SortDisplayOrder, SortEndDate, SortName, SortPromotionId, SortStartDate]
		[PageIndex, PageSize]
		[HttpGet]
		public HttpResponseMessage List()
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetPromotions(RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<PromotionListResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new PromotionListResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

        /// <summary>
        /// Get the list of products
        /// </summary>
        /// <returns>HttpResponse Message</returns>
        [HttpGet]
        public HttpResponseMessage GetProductsList()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetProductList(RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<ProductListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

		/// <summary>
		/// Creates a new promotion.
		/// </summary>
		/// <param name="model">The model of the promotion.</param>
		/// <returns></returns>
		[HttpPost]
		public HttpResponseMessage Create([FromBody] PromotionModel model)
		{
			HttpResponseMessage response;

			try
			{
				var promotion = _service.CreatePromotion(model);
				if (promotion != null)
				{
					var uri = Request.RequestUri;
					var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + promotion.PromotionId;

					response = CreateCreatedResponse(new PromotionResponse { Promotion = promotion });
					response.Headers.Add("Location", location);
				}
				else
				{
					response = CreateInternalServerErrorResponse();
				}
			}
			catch (Exception ex)
			{
				var data = new PromotionResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Updates an existing promotion.
		/// </summary>
		/// <param name="promotionId">The ID of the promotion.</param>
		/// <param name="model">The model of the promotion.</param>
		/// <returns></returns>
		[HttpPut]
		public HttpResponseMessage Update(int promotionId, [FromBody] PromotionModel model)
		{
			HttpResponseMessage response;

			try
			{
				var promotion = _service.UpdatePromotion(promotionId, model);
				response = promotion != null ? CreateOKResponse(new PromotionResponse { Promotion = promotion }) : CreateInternalServerErrorResponse();
			}
			catch (Exception ex)
			{
				var data = new PromotionResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Deletes an existing promotion.
		/// </summary>
		/// <param name="promotionId">The ID of the promotion.</param>
		/// <returns></returns>
		[HttpDelete]
		public HttpResponseMessage Delete(int promotionId)
		{
			HttpResponseMessage response;

			try
			{
				var deleted = _service.DeletePromotion(promotionId);
				response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
			}
			catch (Exception ex)
			{
				var data = new PromotionResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}
	}
}