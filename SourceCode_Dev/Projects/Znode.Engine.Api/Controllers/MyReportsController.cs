﻿using System;
using System.Net.Http;
using System.Web.Mvc;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    public class MyReportsController : BaseController
    {
        #region Private Variables
        private readonly IReportsService _service;
        private readonly IReportsCache _cache;
        #endregion


        /// <summary>
        /// Constructor 
        /// </summary>
        public MyReportsController()
        {
            _service = new ReportsService();
            _cache = new ReportsCache(_service);
        }

        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage GetReportDataSet(string reportName)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetReportDataSet(reportName, RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<ReportsResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ReportsResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        [HttpGet]
        public HttpResponseMessage GetOrderDetails(int orderId, string reportName)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetOrderDetails(orderId,reportName, RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<ReportsResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ReportsResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
    }
}
