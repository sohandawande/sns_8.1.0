﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    public class ApplicationSettingController : BaseController
    {
        #region Private Methods
        private readonly IApplicationSettingService _service;
        private readonly IApplicationSettingCache _cache;
        #endregion

        #region Constructor
        public ApplicationSettingController()
        {
            _service = new ApplicationSettingService();
            _cache = new ApplicationSettingCache(_service);
        }
        #endregion



        /// <summary>
        /// Gets a list of Xml configuration
        /// </summary>
        /// <returns></returns>
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage List()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetApplicationSettings(RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<ApplicationSettingListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ApplicationSettingListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Get list of column names
        /// </summary>
        /// <param name="entityType">Type of entity</param>
        /// <param name="entityName">Name of entity</param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetColumnList(string entityType, string entityName = "")
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetColumnList(RouteUri, RouteTemplate, entityType, string.IsNullOrEmpty(Convert.ToString(entityName)) ? string.Empty : entityName);
                response = !Equals(data, null) ? CreateOKResponse<ApplicationSettingListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ApplicationSettingListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Create XML
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Create([FromBody] ApplicationSettingDataModel model)
        {
            HttpResponseMessage response;

            try
            {
                var status = _service.SaveXmlConfiguration(model);
                if (!Equals(status, null))
                {
                    var uri = Request.RequestUri;
                    var location = string.Format("{0}{1}{2}{3}{4}{5}", uri.Scheme, "://", uri.Host, uri.AbsolutePath, "/", status);

                    response = CreateCreatedResponse(new ApplicationSettingListResponse { CreateStatus = status });
                    response.Headers.Add("Location", location);
                }
                else
                {
                    response = CreateInternalServerErrorResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new ApplicationSettingListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
    }
}