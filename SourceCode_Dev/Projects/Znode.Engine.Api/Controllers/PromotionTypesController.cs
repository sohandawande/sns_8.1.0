﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
	public class PromotionTypesController : BaseController
	{
		private readonly IPromotionTypeService _service;
		private readonly IPromotionTypeCache _cache;

		public PromotionTypesController()
		{
			_service = new PromotionTypeService();
			_cache = new PromotionTypeCache(_service);
		}

		/// <summary>
		/// Gets a promotion type.
		/// </summary>
		/// <param name="promotionTypeId">The ID of the promotion type.</param>
		/// <returns></returns>
		[HttpGet]
		public HttpResponseMessage Get(int promotionTypeId)
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetPromotionType(promotionTypeId, RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<PromotionTypeResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new PromotionTypeResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Gets a list of promotion types.
		/// </summary>
		/// <returns></returns>
		[FilterClassName, FilterClassType, FilterIsActive, FilterName]
		[SortClassName, SortName, SortPromotionTypeId]
		[PageIndex, PageSize]
		[HttpGet]
		public HttpResponseMessage List()
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetPromotionTypes( RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<PromotionTypeListResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new PromotionTypeListResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Creates a new promotion type.
		/// </summary>
		/// <param name="model">The model of the promotion type.</param>
		/// <returns></returns>
		[HttpPost]
		public HttpResponseMessage Create([FromBody] PromotionTypeModel model)
		{
			HttpResponseMessage response;

			try
			{
				var promotionType = _service.CreatePromotionType(model);
				if (promotionType != null)
				{
					var uri = Request.RequestUri;
					var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + promotionType.PromotionTypeId;

					response = CreateCreatedResponse(new PromotionTypeResponse { PromotionType = promotionType });
					response.Headers.Add("Location", location);
				}
				else
				{
					response = CreateInternalServerErrorResponse();
				}
			}
			catch (Exception ex)
			{
				var data = new PromotionTypeResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Updates an existing promotion type.
		/// </summary>
		/// <param name="promotionTypeId">The ID of the promotion type.</param>
		/// <param name="model">The model of the promotion type.</param>
		/// <returns></returns>
		[HttpPut]
		public HttpResponseMessage Update(int promotionTypeId, [FromBody] PromotionTypeModel model)
		{
			HttpResponseMessage response;

			try
			{
				var promotionType = _service.UpdatePromotionType(promotionTypeId, model);
				response = promotionType != null ? CreateOKResponse(new PromotionTypeResponse { PromotionType = promotionType }) : CreateInternalServerErrorResponse();
			}
			catch (Exception ex)
			{
				var data = new PromotionTypeResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Deletes an existing promotion type.
		/// </summary>
		/// <param name="promotionTypeId">The ID of the promotion type.</param>
		/// <returns></returns>
		[HttpDelete]
		public HttpResponseMessage Delete(int promotionTypeId)
		{
			HttpResponseMessage response;

			try
			{
				var deleted = _service.DeletePromotionType(promotionTypeId);
				response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
			}
			catch (Exception ex)
			{
				var data = new PromotionTypeResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

        #region Znode Version 8.0
        /// <summary>
        /// Get All Promotion Types not in database
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetAllPromotionTypesNotInDatabase()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetAllPromotionTypesNotInDatabase(RouteUri, RouteTemplate);
                response = !Equals(data,null) ? CreateOKResponse<PromotionTypeListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new PromotionTypeListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Get Franchise Admin Access Promotion Types.
        /// </summary>
        /// <returns>HttpResponseMessage</returns>
        [HttpGet]
        public HttpResponseMessage GetFranchisePromotionTypes()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetFranchisePromotionTypes(RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<PromotionTypeListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new PromotionTypeListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
        #endregion
    }
}