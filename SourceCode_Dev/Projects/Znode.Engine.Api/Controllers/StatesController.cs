﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
	public class StatesController : BaseController
	{
		private readonly IStateService _service;
		private readonly IStateCache _cache;

		public StatesController()
		{
			_service = new StateService();
			_cache = new StateCache(_service);
		}

		/// <summary>
		/// Gets a state.
		/// </summary>
		/// <param name="stateCode">The 2-character state code.</param>
		/// <returns></returns>
		[HttpGet]
		public HttpResponseMessage Get(string stateCode)
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetState(stateCode, RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<StateResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new StateResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Gets a list of states.
		/// </summary>
		/// <returns></returns>
		[FilterCode, FilterCountryCode, FilterName]
		[SortCode, SortName]
		[PageIndex, PageSize]
		[HttpGet]
		public HttpResponseMessage List()
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetStates(RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<StateListResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new StateListResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Creates a new state.
		/// </summary>
		/// <param name="model">The model of the state.</param>
		/// <returns></returns>
		[HttpPost]
		public HttpResponseMessage Create([FromBody] StateModel model)
		{
			HttpResponseMessage response;

			try
			{
				var state = _service.CreateState(model);
				if (state != null)
				{
					var uri = Request.RequestUri;
					var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + state.Code;

					response = CreateCreatedResponse(new StateResponse { State = state });
					response.Headers.Add("Location", location);
				}
				else
				{
					response = CreateInternalServerErrorResponse();
				}
			}
			catch (Exception ex)
			{
				var data = new StateResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Updates an existing state.
		/// </summary>
		/// <param name="stateCode">The 2-character state code.</param>
		/// <param name="model">The model of the state.</param>
		/// <returns></returns>
		[HttpPut]
		public HttpResponseMessage Update(string stateCode, [FromBody] StateModel model)
		{
			HttpResponseMessage response;

			try
			{
				var state = _service.UpdateState(stateCode, model);
				response = state != null ? CreateOKResponse(new StateResponse { State = state }) : CreateInternalServerErrorResponse();
			}
			catch (Exception ex)
			{
				var data = new StateResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Deletes an existing state.
		/// </summary>
		/// <param name="stateCode">The 2-character state code.</param>
		/// <returns></returns>
		[HttpDelete]
		public HttpResponseMessage Delete(string stateCode)
		{
			HttpResponseMessage response;

			try
			{
				var deleted = _service.DeleteState(stateCode);
				response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
			}
			catch (Exception ex)
			{
				var data = new StateResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}
	}
}