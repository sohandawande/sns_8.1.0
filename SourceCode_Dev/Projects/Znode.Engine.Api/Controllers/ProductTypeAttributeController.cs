﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    public class ProductTypeAttributeController : BaseController
    {
        #region Private Variables
        private readonly IProductTypeAttributeService _service;
        private readonly IProductTypeAttributeCache _cache; 
        #endregion

        #region Public Default Constructor
        /// <summary>
        /// default controller
        /// </summary>
        public ProductTypeAttributeController()
        {
            _service = new ProductTypeAttributeService();
            _cache = new ProductTypeAttributeCache(_service);
        } 
        #endregion

        #region Public Methods
        /// <summary>
        /// Gets a Product Type.
        /// </summary>
        /// <param name="productTypeId">The ID of the Product Type.</param>
        /// <returns>Returns HttpResponseMessage</returns>
        [HttpGet]
        public HttpResponseMessage Get(int productTypeId)
        {
            HttpResponseMessage response;
            try
            {
                var data = _cache.GetAttributeType(productTypeId, RouteUri, RouteTemplate);
                response = !Equals(data , null) ? CreateOKResponse<ProductTypeResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductTypeResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        /// <summary>
        /// Gets a list of Product Type.
        /// </summary>
        /// <returns></returns>
        [ExpandShippingType]
        [FilterCountryCode, FilterExternalId, FilterIsActive, FilterProfileId, FilterShippingCode, FilterShippingTypeId]
        [SortCountryCode, SortDescription, SortDisplayOrder, SortShippingCode, SortShippingOptionId]
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage List()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetAttributeTypes(RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<ProductTypeAttributeResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductTypeAttributeResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Creates a new Product Type.
        /// </summary>
        /// <param name="model">The model of the Product Type.</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Create([FromBody] ProductTypeAttributeModel model)
        {
            HttpResponseMessage response;

            try
            {
                var attributeType = _service.CreateAttributeType(model);
                if (!Equals(attributeType,null))
                {
                    var uri = Request.RequestUri;
                    var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + attributeType.ProductTypeId;

                    response = CreateCreatedResponse(new ProductTypeAttributeResponse { ProductTypeAttribute = attributeType });
                    response.Headers.Add("Location", location);
                }
                else
                {
                    response = CreateInternalServerErrorResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new ProductTypeResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Update an existing  Product Type.
        /// </summary>
        /// <param name="productTypeId">The ID of the  Product Type.</param>
        /// <param name="model">The model of the  Product Type.</param>
        /// <returns></returns>
        [HttpPut]
        public HttpResponseMessage Update(int productTypeId, [FromBody] ProductTypeAttributeModel model)
        {
            HttpResponseMessage response;

            try
            {
                var attributeType = _service.UpdateAttributeType(productTypeId, model);
                response = !Equals(attributeType , null) ? CreateOKResponse(new ProductTypeAttributeResponse { ProductTypeAttribute = attributeType }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductTypeAttributeResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Delete an existing  Product Type.
        /// </summary>
        /// <param name="productTypeAttributId">The ID of the  Product Type.</param>
        /// <returns></returns>

        [HttpDelete]
        public HttpResponseMessage Delete(int productTypeAttributId)
        {
            HttpResponseMessage response;

            try
            {
                response = _service.DeleteAttributeType(productTypeAttributId) ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductTypeAttributeResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }


        /// <summary>
        /// Gets categories by catalog Id.
        /// </summary>
        /// <param name="productTypeId">The Id of Product Type.</param>
        /// <param name="totalRowCount">Total Row Count.</param>
        /// <returns>Returns Attributes by Product Type Id.</returns>
        [PageIndex, PageSize]
        [SortCategoryId]
        [HttpGet]
        public HttpResponseMessage GetAttributeTypesByProductTypeId()
        {
            HttpResponseMessage response;

            try
            {
                ProductTypeAssociatedAttributeTypesListModel attribute = _cache.GetAttributeTypeByProductTypeIdUsingCustomService(RouteUri, RouteTemplate);

                response = !Equals(attribute, null) ? CreateOKResponse(new ProductTypeAttributeListResponse { ProductTypeAssociatedAttributeTypes = attribute }) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductTypeAttributeResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Get Existance of Products associated with Product Type.
        /// </summary>
        /// <param name="productTypeId">Product Type Id</param>
        /// <returns>Returns product is associated or not</returns>
        public HttpResponseMessage IsProductAssociatedProductType(int productTypeId)
        {
            HttpResponseMessage response;
            try
            {
                response = _service.IsProductAssociatedProductType(productTypeId) ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new CatalogResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        #endregion

    }
}
