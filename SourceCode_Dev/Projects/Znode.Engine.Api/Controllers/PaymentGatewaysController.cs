﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
	public class PaymentGatewaysController : BaseController
	{
		private readonly IPaymentGatewayService _service;
		private readonly IPaymentGatewayCache _cache;

		public PaymentGatewaysController()
		{
			_service = new PaymentGatewayService();
			_cache = new PaymentGatewayCache(_service);
		}

		/// <summary>
		/// Gets a payment gateway.
		/// </summary>
		/// <param name="paymentGatewayId">The ID of the payment gateway.</param>
		/// <returns></returns>
		[HttpGet]
		public HttpResponseMessage Get(int paymentGatewayId)
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetPaymentGateway(paymentGatewayId, RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<PaymentGatewayResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new PaymentGatewayResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Gets a list of payment gateways.
		/// </summary>
		/// <returns></returns>
		[FilterName]
		[SortName, SortPaymentGatewayId]
		[PageIndex, PageSize]
		[HttpGet]
		public HttpResponseMessage List()
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetPaymentGateways( RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<PaymentGatewayListResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new PaymentGatewayListResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Creates a new payment gateway.
		/// </summary>
		/// <param name="model">The model of the payment gateway.</param>
		/// <returns></returns>
		[HttpPost]
		public HttpResponseMessage Create([FromBody] PaymentGatewayModel model)
		{
			HttpResponseMessage response;

			try
			{
				var paymentGateway = _service.CreatePaymentGateway(model);
				if (paymentGateway != null)
				{
					var uri = Request.RequestUri;
					var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + paymentGateway.PaymentGatewayId;

					response = CreateCreatedResponse(new PaymentGatewayResponse { PaymentGateway = paymentGateway });
					response.Headers.Add("Location", location);
				}
				else
				{
					response = CreateInternalServerErrorResponse();
				}
			}
			catch (Exception ex)
			{
				var data = new PaymentGatewayResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Updates an existing payment gateway.
		/// </summary>
		/// <param name="paymentGatewayId">The ID of the payment gateway.</param>
		/// <param name="model">The model of the payment gateway.</param>
		/// <returns></returns>
		[HttpPut]
		public HttpResponseMessage Update(int paymentGatewayId, [FromBody] PaymentGatewayModel model)
		{
			HttpResponseMessage response;

			try
			{
				var paymentGateway = _service.UpdatePaymentGateway(paymentGatewayId, model);
				response = paymentGateway != null ? CreateOKResponse(new PaymentGatewayResponse { PaymentGateway = paymentGateway }) : CreateInternalServerErrorResponse();
			}
			catch (Exception ex)
			{
				var data = new PaymentGatewayResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Deletes an existing payment gateway.
		/// </summary>
		/// <param name="paymentGatewayId">The ID of the payment gateway.</param>
		/// <returns></returns>
		[HttpDelete]
		public HttpResponseMessage Delete(int paymentGatewayId)
		{
			HttpResponseMessage response;

			try
			{
				var deleted = _service.DeletePaymentGateway(paymentGatewayId);
				response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
			}
			catch (Exception ex)
			{
				var data = new PaymentGatewayResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}
	}
}