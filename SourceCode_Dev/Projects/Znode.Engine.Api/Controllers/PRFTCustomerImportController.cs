﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    public class PRFTCustomerImportController : BaseController
    {

        #region Private Variables
        private readonly IPRFTCustomerImportService _service;
        #endregion

        #region Constructor
        public PRFTCustomerImportController()
        {
            _service = new PRFTCustomerImportService();
        }
        #endregion

        [HttpGet]
        public HttpResponseMessage CreateUser()
        {
            HttpResponseMessage response;
            try
            {
                _service.CreateAccount();
                var data = "ok";
                response = data != null ? CreateOKResponse(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = ex.Message;
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
    }
}
