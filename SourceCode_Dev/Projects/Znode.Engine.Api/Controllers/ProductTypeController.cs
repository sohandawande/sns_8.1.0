﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    /// <summary>
    /// API Controller For ProductTypeController
    /// </summary>
    public class ProductTypeController : BaseController
    {
        #region Private Varible
        private readonly IProductTypeService _service;
        private readonly IProductTypeCache _cache;
        #endregion

        #region Public Constractor
        public ProductTypeController()
        {
            _service = new ProductTypeService();
            _cache = new ProductTypeCache(_service);
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Gets a Product Type.
        /// </summary>
        /// <param name="productTypeId">The ID of the Product Type.</param>
        /// <returns>Returns HttpResponseMessage</returns>
        [HttpGet]
        public HttpResponseMessage Get(int productTypeId)
        {
            HttpResponseMessage response;
            try
            {
                var data = _cache.GetProductType(productTypeId, RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<ProductTypeResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductTypeResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        /// <summary>
        /// Gets a list of Product Type.
        /// </summary>
        /// <returns></returns>
        [ExpandShippingType]
        [FilterCountryCode, FilterExternalId, FilterIsActive, FilterProfileId, FilterShippingCode, FilterShippingTypeId]
        [SortCountryCode, SortDescription, SortDisplayOrder, SortShippingCode, SortShippingOptionId]
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage List()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetProductTypes(RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<ProductTypeResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductTypeResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Creates a new Product Type.
        /// </summary>
        /// <param name="model">The model of the Product Type.</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Create([FromBody] ProductTypeModel model)
        {
            HttpResponseMessage response;

            try
            {
                var productType = _service.CreateProductType(model);
                if (!Equals(productType, null))
                {
                    var uri = Request.RequestUri;
                    var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + productType.ProductTypeId;

                    response = CreateCreatedResponse(new ProductTypeResponse { ProductType = productType });
                    response.Headers.Add("Location", location);
                }
                else
                {
                    response = CreateInternalServerErrorResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new ProductTypeResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Update an existing  Product Type.
        /// </summary>
        /// <param name="productTypeId">The ID of the  Product Type.</param>
        /// <param name="model">The model of the  Product Type.</param>
        /// <returns></returns>
        [HttpPut]
        public HttpResponseMessage Update(int productTypeId, [FromBody] ProductTypeModel model)
        {
            HttpResponseMessage response;

            try
            {
                var productType = _service.UpdateProductType(productTypeId, model);
                response = !Equals(productType, null) ? CreateOKResponse(new ProductTypeResponse { ProductType = productType }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductTypeResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Delete an existing  Product Type.
        /// </summary>
        /// <param name="productTypeId">The ID of the  Product Type.</param>
        /// <returns></returns>
        [HttpDelete]
        public HttpResponseMessage Delete(int productTypeId)
        {
            HttpResponseMessage response;

            try
            {
                response = _service.DeleteProductType(productTypeId) ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductTypeResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Get Product Attributes By Product Type Id
        /// </summary>
        /// <param name="productTypeId">Product Type Id</param>
        /// <returns></returns>
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage GetProductAttributesByProductTypeId()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetProductAttributesByProductTypeId(RouteUri, RouteTemplate);
                response = (!Equals(data, null)) ? CreateOKResponse<ProductTypeListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductTypeListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
        #endregion

    }
}
