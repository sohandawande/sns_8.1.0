﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;


namespace Znode.Engine.Api.Controllers
{
    public class ProductSearchSettingController : BaseController
    {
        #region Private Methods
        private readonly IProductSearchSettingService _service;
        private readonly IProductSearchSettingCache _cache;
        #endregion

        #region Constructor
        public ProductSearchSettingController()
        {
            _service = new ProductSearchSettingService();
            _cache = new ProductSearchSettingCache(_service);
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Get all product level settings
        /// </summary>
        /// <returns></returns>
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage GetProductLevelSettings()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetProductLevelSettings(RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<ProductSearchSettingResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductSearchSettingResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        /// <summary>
        /// Get all category level settings
        /// </summary>
        /// <returns></returns>
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage GetCategoryLevelSettings()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetCategoryLevelSettings(RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<ProductSearchSettingResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductSearchSettingResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        /// <summary>
        /// Get all field level settings
        /// </summary>
        /// <returns></returns>
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage GetFieldLevelSettings()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetFieldLevelSettings(RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<ProductSearchSettingResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductSearchSettingResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        /// <summary>
        /// Save product search boost values.
        /// </summary>
        /// <param name="model">BoostSearchSetting Model</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage SaveBoostValues([FromBody] BoostSearchSettingModel model)
        {
            HttpResponseMessage response;

            try
            {
                var saveflag = _service.SaveBoostValues(model);
                if (!Equals(saveflag, null))
                {
                    var uri = Request.RequestUri;
                    var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + saveflag;

                    response = CreateCreatedResponse(new ProductSearchSettingResponse { SaveFlag = saveflag });
                    response.Headers.Add("Location", location);
                }
                else
                {
                    response = CreateInternalServerErrorResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new ProductSearchSettingResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        
        #endregion

    }
}
