﻿using System.Web.Mvc;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Api.Controllers
{
	public class HomeController : Controller
	{
		private ZNodeLicenseManager licenseMgr;

		public ActionResult Index()
		{
			licenseMgr = new ZNodeLicenseManager();
			licenseMgr.Validate();

			if (licenseMgr.LicenseType == ZNodeLicenseType.Invalid)
			{
				return RedirectToAction("Index", "Activate", new { area = "Activate" });
			}

			return View();
		}
	}
}
