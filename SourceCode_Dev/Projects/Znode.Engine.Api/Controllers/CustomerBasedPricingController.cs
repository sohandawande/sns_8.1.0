﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    public class CustomerBasedPricingController : BaseController
    {
        #region Private Varibles
        private readonly ICustomerBasedPricingService _service;
        private readonly ICustomerBasedPricingCache _cache; 
        #endregion

        #region Public delaut constructor
        public CustomerBasedPricingController()
        {
            _service = new CustomerBasedPricingService();
            _cache = new CustomerBasedPricingCache(_service);
        } 
        #endregion

        #region public methods
        /// <summary>
        /// To Get all the Product Associated Facets.
        /// </summary>
        /// <returns>Return Product Associated Facets.</returns>
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage GetCustomerBasedPricingList(int productId)
        {
            HttpResponseMessage response;

            try
            {
                CustomerBasedPricingListModel customerPricing = _cache.GetCustomerBasedPricing(RouteUri, RouteTemplate);

                response = !Equals(customerPricing, null) ? CreateOKResponse(new CustomerBasedPricingListResponse { CustomerBasedPricingList = customerPricing.CustomerBasedPricing }) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                CustomerBasedPricingModelResponse data = new CustomerBasedPricingModelResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        } 

        /// <summary>
        /// To Get all the Product Associated Facets.
        /// </summary>
        /// <returns>Return Product Associated Facets.</returns>
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage GetCustomerPricingProductList()
        {
            HttpResponseMessage response;

            try
            {
                CustomerBasedPricingListModel customerPricing = _cache.GetCustomerPricingProduct(RouteUri, RouteTemplate);

                response = !Equals(customerPricing, null) ? CreateOKResponse(new CustomerBasedPricingListResponse { CustomerBasedPricingList = customerPricing.CustomerBasedPricing }) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                CustomerBasedPricingModelResponse data = new CustomerBasedPricingModelResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        #endregion

    }
}
