﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    /// <summary>
    /// Locale Controller
    /// </summary>
    public class LocaleController : BaseController
    {
        #region Private Variables
        private readonly ILocaleService _service;
        private readonly ILocaleCache _cache;
        #endregion

        #region Constructor
        public LocaleController()
        {
            _service = new LocaleService();
            _cache = new LocaleCache(_service);
        }
        #endregion

        #region Public Action Methods
        /// <summary>
        /// Gets a list of Locales.
        /// </summary>
        /// <returns></returns>        
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage List()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetLocales(RouteUri, RouteTemplate);
                response = (!Equals(data, null)) ? CreateOKResponse<LocaleListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new LocaleListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }
        #endregion
    }
}
