﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    public class CountriesController : BaseController
    {
        private readonly ICountryService _service;
        private readonly ICountryCache _cache;

        public CountriesController()
        {
            _service = new CountryService();
            _cache = new CountryCache(_service);
        }

        /// <summary>
        /// Gets a country.
        /// </summary>
        /// <param name="countryCode">The 2-character country code.</param>
        /// <returns></returns>
        [ExpandStates]
        [HttpGet]
        public HttpResponseMessage Get(string countryCode)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetCountry(countryCode, RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<CountryResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new CountryResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets a list of countries.
        /// </summary>
        /// <returns></returns>
        [ExpandStates]
        [FilterCode, FilterIsActive, FilterName]
        [SortCode, SortDisplayOrder, SortName]
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage List()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetCountries(RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<CountryListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new CountryListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Creates a new country.
        /// </summary>
        /// <param name="model">The model of the country.</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Create([FromBody] CountryModel model)
        {
            HttpResponseMessage response;

            try
            {
                var country = _service.CreateCountry(model);
                if (!Equals(country, null))
                {
                    var uri = Request.RequestUri;
                    var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + country.Code;

                    response = CreateCreatedResponse(new CountryResponse { Country = country });
                    response.Headers.Add("Location", location);
                }
                else
                {
                    response = CreateInternalServerErrorResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new CountryResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Updates an existing country.
        /// </summary>
        /// <param name="countryCode">The 2-character country code.</param>
        /// <param name="model">The model of the country.</param>
        /// <returns></returns>
        [HttpPut]
        public HttpResponseMessage Update(string countryCode, [FromBody] CountryModel model)
        {
            HttpResponseMessage response;

            try
            {
                var country = _service.UpdateCountry(countryCode, model);
                response = !Equals(country, null) ? CreateOKResponse(new CountryResponse { Country = country }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new CountryResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Deletes an existing country.
        /// </summary>
        /// <param name="countryCode">The 2-character country code.</param>
        /// <returns></returns>
        [HttpDelete]
        public HttpResponseMessage Delete(string countryCode)
        {
            HttpResponseMessage response;

            try
            {
                var deleted = _service.DeleteCountry(countryCode);
                response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new CountryResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        #region Znode Version 7.2.2
        /// <summary>
        /// This function returns list of all active billing and shipping countries on the basis of portal id.
        /// </summary>
        /// <param name="portalId">int portalId</param>
        /// <param name="billingShippingFlag">int billingShippingFlag</param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetActiveCountryByPortalId(int portalId, int billingShippingFlag = 0)
        {
            HttpResponseMessage response;

            try
            {
                var data = _service.GetActiveCountryByPortalId(portalId, billingShippingFlag);
                response = !Equals(data, null) ? CreateOKResponse<CountryListModel>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new CountryListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// This function used to  Get all countries for portalId.
        /// </summary>
        /// <param name="portalId">int portalId</param>
        /// <returns>Returns the country list</returns>
        public HttpResponseMessage GetCountryByPortalId(int portalId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _service.GetCountryByPortalId(portalId);
                response = !Equals(data, null) ? CreateOKResponse<CountryListModel>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new CountryListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }


        /// <summary>
        /// This function used to  Get all countries for portalId.
        /// </summary>
        /// <param name="portalId">int portalId</param>
        /// <returns>Returns the country list</returns>
        public HttpResponseMessage GetShippingActiveCountryByPortalId(int portalId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _service.GetShippingActiveCountryByPortalId(portalId);
                response = !Equals(data, null) ? CreateOKResponse<CountryListModel>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new CountryListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
        #endregion
    }
}