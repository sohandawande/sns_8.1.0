﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    /// <summary>
    /// Message Config Controller
    /// </summary>
	public class MessageConfigsController : BaseController
	{
        #region Private Variables
        private readonly IMessageConfigService _service;
        private readonly IMessageConfigCache _cache; 
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor for MessageConfigsController
        /// </summary>
        public MessageConfigsController()
        {
            _service = new MessageConfigService();
            _cache = new MessageConfigCache(_service);
        }
        
        #endregion

        #region Public Action Methods
        /// <summary>
        /// Gets a message config.
        /// </summary>
        /// <param name="messageConfigId">The ID of the message config.</param>
        /// <returns></returns>
        [ExpandMessageType, ExpandPortal]
        [HttpGet]
        public HttpResponseMessage Get(int messageConfigId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetMessageConfig(messageConfigId, RouteUri, RouteTemplate);
                response = !Equals(data,null) ? CreateOKResponse<MessageConfigResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new MessageConfigResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets a message config.
        /// </summary>
        /// <returns></returns>
        [ExpandMessageType]
        [HttpGet]
        public HttpResponseMessage GetByKey(string key)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetMessageConfigByKey(key, PortalId, RouteUri, RouteTemplate);
                response = !Equals(data , null) ? CreateOKResponse<MessageConfigResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new MessageConfigResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets a list of message configs.
        /// </summary>
        /// <returns></returns>
        [ExpandMessageType]
        [FilterKey, FilterLocaleId, FilterPortalId, FilterMessageTypeId]
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage List()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetMessageConfigs(RouteUri, RouteTemplate);
                response = !Equals(data,null) ? CreateOKResponse<MessageConfigListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new MessageConfigListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets a list of message configs for the specified keys.
        /// </summary>
        /// <param name="keys">The comma-seperated keys of the message configs.</param>
        /// <returns></returns>
        [ExpandMessageType]
        [FilterKey, FilterLocaleId, FilterPortalId]
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage ListByKeys(string keys)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetMessageConfigsByKeys(keys, RouteUri, RouteTemplate);
                response = !Equals(data,null) ? CreateOKResponse<ProductListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Creates a new message config.
        /// </summary>
        /// <param name="model">The model of the message config.</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Create([FromBody] MessageConfigModel model)
        {
            HttpResponseMessage response;

            try
            {
                var messageConfig = _service.CreateMessageConfig(model);
                if (!Equals(messageConfig,null))
                {
                    var uri = Request.RequestUri;
                    var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + messageConfig.MessageConfigId;

                    response = CreateCreatedResponse(new MessageConfigResponse { MessageConfig = messageConfig });
                    response.Headers.Add("Location", location);
                }
                else
                {
                    response = CreateInternalServerErrorResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new MessageConfigResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Updates an existing message config.
        /// </summary>
        /// <param name="messageConfigId">The ID of the message config.</param>
        /// <param name="model">The model of the message config.</param>
        /// <returns></returns>
        [HttpPut]
        public HttpResponseMessage Update(int messageConfigId, [FromBody] MessageConfigModel model)
        {
            HttpResponseMessage response;

            try
            {
                var messageConfig = _service.UpdateMessageConfig(messageConfigId, model);
                response = !Equals(messageConfig,null) ? CreateOKResponse(new MessageConfigResponse { MessageConfig = messageConfig }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new MessageConfigResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Deletes an existing message config.
        /// </summary>
        /// <param name="messageConfigId">The ID of the message config.</param>
        /// <returns></returns>
        [HttpDelete]
        public HttpResponseMessage Delete(int messageConfigId)
        {
            HttpResponseMessage response;

            try
            {
                var deleted = _service.DeleteMessageConfig(messageConfigId);
                response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new MessageConfigResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        } 
        #endregion
	}
}