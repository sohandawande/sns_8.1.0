﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    public class ShippingOptionsController : BaseController
    {
        #region Private Variables
        private readonly IShippingOptionService _service;
        private readonly IShippingOptionCache _cache; 
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor for ShippingOptionsController
        /// </summary>
        public ShippingOptionsController()
        {
            _service = new ShippingOptionService();
            _cache = new ShippingOptionCache(_service);
        }
        
        #endregion

        /// <summary>
        /// Gets a shipping option.
        /// </summary>
        /// <param name="shippingServiceCodeId">The ID of the shipping option.</param>
        /// <returns></returns>
        [ExpandShippingType]
        [HttpGet]
        public HttpResponseMessage Get(int shippingOptionId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetShippingOption(shippingOptionId, RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<ShippingOptionResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ShippingOptionResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets a list of shipping options.
        /// </summary>
        /// <returns></returns>       
        [ExpandShippingType]
        [FilterCountryCode, FilterExternalId, FilterIsActive, FilterProfileId, FilterShippingCode, FilterShippingTypeId]
        [SortCountryCode, SortDescription, SortDisplayOrder, SortShippingCode, SortShippingOptionId]
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage List()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetShippingOptions(RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<ShippingOptionListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ShippingOptionListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Creates a new shipping option.
        /// </summary>
        /// <param name="model">The model of the shipping option.</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Create([FromBody] ShippingOptionModel model)
        {
            HttpResponseMessage response;

            try
            {
                var shippingOption = _service.CreateShippingOption(model);
                if (shippingOption != null)
                {
                    var uri = Request.RequestUri;
                    var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + shippingOption.ShippingId;

                    response = CreateCreatedResponse(new ShippingOptionResponse { ShippingOption = shippingOption });
                    response.Headers.Add("Location", location);
                }
                else
                {
                    response = CreateInternalServerErrorResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new ShippingOptionResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Updates an existing shipping option.
        /// </summary>
        /// <param name="shippingOptionId">The ID of the shipping option.</param>
        /// <param name="model">The model of the shipping option.</param>
        /// <returns></returns>
        [HttpPut]
        public HttpResponseMessage Update(int shippingOptionId, [FromBody] ShippingOptionModel model)
        {
            HttpResponseMessage response;

            try
            {
                var shippingOption = _service.UpdateShippingOption(shippingOptionId, model);
                response = shippingOption != null ? CreateOKResponse(new ShippingOptionResponse { ShippingOption = shippingOption }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new ShippingOptionResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Deletes an existing shipping option.
        /// </summary>
        /// <param name="shippingOptionId">The ID of the shipping option.</param>
        /// <returns></returns>
        [HttpDelete]
        public HttpResponseMessage Delete(int shippingOptionId)
        {
            HttpResponseMessage response;

            try
            {
                var deleted = _service.DeleteShippingOption(shippingOptionId);
                response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new ShippingOptionResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets a list of shipping options.
        /// </summary>
        /// <returns></returns>       
        [ExpandShippingType]
        [HttpGet]
        public HttpResponseMessage GetFranchiseShippingOptionList()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetFranchiseShippingOptionList(RouteUri, RouteTemplate);
                response = !Equals(data,null) ? CreateOKResponse<ShippingOptionListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ShippingOptionListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
    }
}
