﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    public class RotateKeysController : BaseController
    {
        #region Private Variables
        private readonly IRotateKeyService _service; 
        #endregion

        #region Constructor 
        /// <summary>
        /// Constructor For RotateKeysController
        /// </summary>
        public RotateKeysController()
        {
            _service = new RotateKeyService();
        } 
        #endregion

        #region Public Methods
        /// <summary>
        /// Creates a new category.
        /// </summary>        
        /// <returns>HttpResponseMessage</returns>
        [HttpGet]
        public HttpResponseMessage GenerateRotateKey()
        {
            HttpResponseMessage response;
           
            try
            {
                var status = _service.GenerateNewRotateKey();
                response = status ? CreateNoContentResponse() : CreateInternalServerErrorResponse();                
            }
            catch (Exception ex)
            {
                var data = new RotateKeyResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        } 
        #endregion
    }
}
