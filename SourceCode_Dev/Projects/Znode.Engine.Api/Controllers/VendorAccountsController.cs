﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    public class VendorAccountsController : BaseController
    {
        #region Private Variables
        private readonly IVendorAccountService _service;
        private readonly IVendorAccountCache _cache;
        #endregion

        #region Constructor
        public VendorAccountsController()
        {
            _service = new VendorAccountService();
            _cache = new VendorAccountCache(_service);
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// To Get vendor account List.
        /// </summary>
        /// <returns></returns>
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage GetVendorAccountList()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetVendorAccountList(RouteUri, RouteTemplate);
                response = (!Equals(data, null)) ? CreateOKResponse<VendorAccountListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new VendorAccountListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Get vendor account details by account id.
        /// </summary>
        /// <param name="accountId">Int account id</param>
        /// <returns>HttpResponseMessage</returns>
        [HttpGet]
        public HttpResponseMessage GetVendorAccountById(int accountId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetVendorAccountById(accountId, RouteUri, RouteTemplate);
                if (!Equals(data, null))
                {
                    response = CreateOKResponse<VendorAccountResponse>(data);
                }
                else
                {
                    response = CreateNotFoundResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new VendorAccountResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
        
        #endregion

        

    }
}
