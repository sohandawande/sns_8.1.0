﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    public class ProductAttributesController : BaseController
    {
        private readonly IProductAttributesService _service;
        private readonly IProductAttributesCache _cache;

        public ProductAttributesController()
        {
            _service = new ProductAttributesService();
            _cache = new ProductAttributesCache(_service);
        }

        /// <summary>
        /// Gets an attribute .
        /// </summary>
        /// <param name="attributeId">The ID of the attribute.</param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage Get(int attributeId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetAttribute(attributeId, RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<ProductAttributesResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductAttributesResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets a list of attributes.
        /// </summary>
        /// <returns></returns>
        [FilterIsPrivate, FilterLocaleId, FilterName, FilterPortalId]
        [SortAttributeId, SortDisplayOrder, SortName]
        [HttpGet]
        public HttpResponseMessage List()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetAttributes(RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<ProductAttributesListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductAttributesListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Creates a new attributes.
        /// </summary>
        /// <param name="model">The model of the attributes.</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Create([FromBody] AttributeModel model)
        {
            HttpResponseMessage response;

            try
            {
                var attributes = _service.CreateAttributes(model);
                if (attributes != null)
                {
                    var uri = Request.RequestUri;
                    var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + attributes.AttributeId;

                    response = CreateCreatedResponse(new ProductAttributesResponse { Attributes = attributes });
                    response.Headers.Add("Location", location);
                }
                else
                {
                    response = CreateInternalServerErrorResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new ProductAttributesResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Updates an existing attributes.
        /// </summary>
        /// <param name="attributeId">The ID of the attributes .</param>
        /// <param name="model">The model of the attributes.</param>
        /// <returns></returns>
        [HttpPut]
        public HttpResponseMessage Update(int attributeId, [FromBody] AttributeModel model)
        {
            HttpResponseMessage response;

            try
            {
                var attributes = _service.UpdateAttributes(attributeId, model);
                response = attributes != null ? CreateOKResponse(new ProductAttributesResponse { Attributes = attributes }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductAttributesResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Deletes an existing attribute .
        /// </summary>
        /// <param name="attributeId">The ID of the attributes.<param>
        /// <returns></returns>
        [HttpDelete]
        public HttpResponseMessage Delete(int attributeId)
        {
            HttpResponseMessage response;

            try
            {
                var deleted = _service.DeleteAttributes(attributeId);
                response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductAttributesResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }


        /// <summary>
        /// To get Attributes by AttributeTypeId
        /// </summary>
        /// <param name="attributeTypeId">int attributeTypeId</param>
        /// <returns>returns list of attributes</returns>
        [HttpGet]
        public HttpResponseMessage GetAttributesByAttributeTypeId()
        {
            HttpResponseMessage response;
            try
            {
                var attributeList = _cache.GetAttributesByAttributeTypeId(RouteUri, RouteTemplate);
                response = !Equals(attributeList, null) ? CreateOKResponse<ProductAttributesListResponse>(attributeList) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductAttributesListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
    }
}