﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    public partial class AccountsController : BaseController
    {
        private readonly IAccountService _service;
        private readonly IAccountCache _cache;

        public AccountsController()
        {
            _service = new AccountService();
            _cache = new AccountCache(_service);
        }

        /// <summary>
        /// Gets an account.
        /// </summary>
        /// <param name="accountId">The ID of the account.</param>
        /// <returns></returns>
        [ExpandAccountType, ExpandAddresses, ExpandOrders, ExpandProfiles, ExpandUser, ExpandWishList, ExpandReferralCommissionType]
        [HttpGet]
        public HttpResponseMessage Get(int accountId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetAccount(accountId, RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<AccountResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new AccountResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets an account.
        /// </summary>
        /// <param name="userId">The user ID (guid) of the account.</param>
        /// <returns></returns>
        [ExpandAccountType, ExpandAddresses, ExpandOrders, ExpandProfiles, ExpandUser, ExpandWishList]
        [HttpGet]
        public HttpResponseMessage GetByUserId(Guid userId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetAccountByUserId(userId, RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<AccountResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new AccountResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets an account.
        /// </summary>
        /// <param name="model">Model of type AccountModel</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage GetByUsername([FromBody] AccountModel model)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetAccountByUsername(model.UserName, RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<AccountResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new AccountResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets a list of accounts.
        /// </summary>
        /// <returns></returns>
        [ExpandAccountType, ExpandAddresses, ExpandOrders, ExpandProfiles, ExpandWishList]
        [FilterAccountTypeId, FilterCompanyName, FilterEmail, FilterEmailOptIn, FilterEnableCustomerPricing, FilterExternalId, FilterIsActive, FilterParentAccountId, FilterProfileId, FilterUserId]
        [SortAccountId, SortCompanyName, SortCreateDate]
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage List()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetAccounts(RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<AccountListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new AccountListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Creates a new account.
        /// </summary>
        /// <param name="model">The model of the account.</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Create([FromBody] AccountModel model)
        {
            HttpResponseMessage response;
            try
            {
                var account = _service.CreateAccount(PortalId, model);
                if (account != null)
                {
                    var uri = Request.RequestUri;
                    var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + account.AccountId;

                    response = CreateCreatedResponse(new AccountResponse { Account = account });
                    response.Headers.Add("Location", location);
                }
                else
                {
                    response = CreateInternalServerErrorResponse();
                }
            }
            catch (ZnodeException ex)
            {
                var data = new AccountResponse { HasError = true, ErrorCode = ex.ErrorCode, ErrorMessage = ex.ErrorMessage };
                response = CreateInternalServerErrorResponse(data);
            }
            catch (Exception ex)
            {
                var data = new AccountResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Updates an existing account.
        /// </summary>
        /// <param name="accountId">The ID of the account.</param>
        /// <param name="model">The model of the account.</param>
        /// <returns></returns>
        [HttpPut]
        public HttpResponseMessage Update(int accountId, [FromBody] AccountModel model)
        {
            HttpResponseMessage response;

            try
            {
                var account = _service.UpdateAccount(accountId, model);
                response = account != null ? CreateOKResponse(new AccountResponse { Account = account }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new AccountResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Deletes an existing account.
        /// </summary>
        /// <param name="accountId">The ID of the account.</param>
        /// <returns></returns>
        [HttpDelete]
        public HttpResponseMessage Delete(int accountId)
        {
            HttpResponseMessage response;

            try
            {
                var deleted = _service.DeleteAccount(accountId);
                response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new AccountResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Logs in an existing user.
        /// </summary>
        /// <param name="model">The model of the account.</param>
        /// <returns></returns>
        [ExpandAccountType, ExpandAddresses, ExpandOrders, ExpandProfiles, ExpandWishList]
        [HttpPost]
        public HttpResponseMessage Login([FromBody] AccountModel model)
        {
            HttpResponseMessage response;

            string errorCode;
            var account = _cache.Login(PortalId, model, out errorCode);
            if (account != null)
            {
                var data = new AccountResponse { Account = account, ErrorCode = Convert.ToInt32(errorCode), HasError = errorCode != "0" };
                response = CreateOKResponse(data);
            }
            else
            {
                var data = new AccountResponse { Account = null, ErrorCode = 10002, HasError = true, ErrorMessage = "Login Unsucessfull" };
                response = CreateUnauthorizedResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Resets the password for an existing user.
        /// </summary>
        /// <param name="model">The model of the account.</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage ResetPassword([FromBody] AccountModel model)
        {
            HttpResponseMessage response;

            try
            {
                var account = _service.ResetPassword(model);
                response = account != null ? CreateOKResponse(new AccountResponse { Account = account }) : null;
                //CreateUnauthorizedResponse();
            }
            catch (Exception ex)
            {
                var data = new AccountResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Changes the password of an existing user.
        /// </summary>
        /// <param name="model">The model of the account.</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage ChangePassword([FromBody] AccountModel model)
        {
            HttpResponseMessage response;

            try
            {
                var account = _service.ChangePassword(PortalId, model);
                response = account != null ? CreateOKResponse(new AccountResponse { Account = account }) : null;
                //CreateUnauthorizedResponse();
            }
            catch (Exception ex)
            {
                var data = new AccountResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        #region Znode Version 7.2.2
        /// <summary>
        /// Checks Reset Password Link status.
        /// </summary>
        /// <param name="model">The model of the account.</param>
        /// <returns>Returns the link status</returns>
        [HttpPost]
        public HttpResponseMessage CheckResetPasswordLinkStatus([FromBody] AccountModel model)
        {
            HttpResponseMessage response;
            string errorCode;
            try
            {
                var account = _service.CheckResetPasswordLinkStatus(model, out errorCode);
                response = account != null ? CreateOKResponse(new AccountResponse { Account = account, ErrorCode = Convert.ToInt32(errorCode), HasError = errorCode != "0" }) : null;
            }
            catch (Exception ex)
            {
                var data = new AccountResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        #endregion

        #region Znode Version 8.0
        /// <summary>
        /// Check for the user role based on role name.
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Returns status for the user based on role name</returns>
        [HttpPost]
        public HttpResponseMessage CheckUserRole([FromBody] AccountModel model)
        {
            HttpResponseMessage response;
            try
            {
                var data = _service.CheckUserRole(model.UserName, model.RoleName);
                response = data != null ? CreateOKResponse<AccountResponse>(new AccountResponse { Account = data }) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new AccountResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// To check Address id Valid
        /// </summary>
        /// <param name="model">AddressModel model</param>
        /// <returns>returns true/false</returns>
        [HttpPost]
        public HttpResponseMessage IsAddressValid([FromBody] AddressModel model)
        {
            HttpResponseMessage response;
            try
            {
                var data = _service.IsAddressValid(model);
                response = data != null ? CreateOKResponse<AddressResponse>(new AddressResponse { Address = data }) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new AddressResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
        /// <summary>
        /// Method used to Reset Admin Details in case of first default admin.
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Returns status for reset of admin details</returns>
        [HttpPost]
        public HttpResponseMessage ResetAdminDetails([FromBody] AccountModel model)
        {
            HttpResponseMessage response;
            try
            {
                var data = _service.ResetAdminDetails(model);
                response = data != null ? CreateOKResponse<AccountResponse>(new AccountResponse { Account = data }) : CreateNotFoundResponse();
            }
            catch (ZnodeException ex)
            {
                var data = new AccountResponse { HasError = true, ErrorCode = ex.ErrorCode, ErrorMessage = ex.ErrorMessage };
                response = CreateInternalServerErrorResponse(data);
            }
            catch (Exception ex)
            {
                var data = new AccountResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Creates a new account.
        /// </summary>
        /// <param name="model">The model of the account.</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage CreateAdminAccount([FromBody] AccountModel model)
        {
            HttpResponseMessage response;
            try
            {
                var data = _service.CreateAdminAccount(PortalId, model);
                response = data != null ? CreateOKResponse<AccountResponse>(new AccountResponse { Account = data }) : CreateNotFoundResponse();
            }
            catch (ZnodeException ex)
            {
                var data = new AccountResponse { HasError = true, ErrorCode = ex.ErrorCode, ErrorMessage = ex.ErrorMessage };
                response = CreateInternalServerErrorResponse(data);
            }
            catch (Exception ex)
            {
                var data = new AccountResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// This method will get the account details by Role Name
        /// </summary>
        /// <param name="roleName">string User Role name</param>
        /// <returns></returns>
        [PageIndex, PageSize, ExpandUser]
        [HttpGet]
        public HttpResponseMessage GetAccountDetailsByRoleName(string roleName)
        {
            HttpResponseMessage response;
            int totalRowCount = 0;
            try
            {
                var data = _cache.GetAccountDetailsByRoleName(roleName, out totalRowCount, RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<AccountListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new AccountResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        #region Vendor Account
        /// <summary>
        /// Create New Vendor Account.
        /// </summary>
        /// <param name="model">AccountModel</param>
        /// <returns>HttpResponseMessage</returns>
        [HttpPost]
        public HttpResponseMessage CreateVendorAccount([FromBody] AccountModel model)
        {
            HttpResponseMessage response;
            try
            {
                var data = _service.CreateVendorAccount(PortalId, model);
                response = data != null ? CreateOKResponse<AccountResponse>(new AccountResponse { Account = data }) : CreateNotFoundResponse();
            }
            catch (ZnodeException ex)
            {
                var data = new AccountResponse { HasError = true, ErrorCode = ex.ErrorCode, ErrorMessage = ex.ErrorMessage };
                response = CreateInternalServerErrorResponse(data);
            }
            catch (Exception ex)
            {
                var data = new AccountResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Updates an existing Account.
        /// </summary>
        /// <param name="accountId">The ID of the product.</param>
        /// <param name="model">The model of the product.</param>
        /// <returns></returns>
        [HttpPut]
        public HttpResponseMessage UpdateVendorAccount(int accountId, AccountModel model)
        {
            HttpResponseMessage response;

            try
            {
                var account = _service.UpdateVendorAccount(accountId, model);
                response = !Equals(account, null) ? CreateOKResponse(new AccountResponse { Account = account }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
        #endregion

        #region Franchise Account
        /// <summary>
        /// Create Franchise Account.
        /// </summary>
        /// <param name="model">AccountModel</param>
        /// <returns>HttpResponseMessage</returns>
        [HttpPost]
        public HttpResponseMessage CreateFranchiseAccount([FromBody] AccountModel model)
        {
            HttpResponseMessage response;
            try
            {
                var account = _service.CreateFranchiseAccount(PortalId, model);
                if (!Equals(account, null))
                {
                    var uri = Request.RequestUri;
                    var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + account.AccountId;

                    response = CreateCreatedResponse(new AccountResponse { Account = account });
                }

                else
                {
                    response = CreateInternalServerErrorResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new AccountResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        #endregion

        #region Customer Account
        /// <summary>
        /// To Create the Customer Account
        /// </summary>
        /// <param name="model">Model of type AccountModel</param>
        /// <returns>Returns HttpResponseMessage</returns>
        public HttpResponseMessage CreateCustomerAccount([FromBody] AccountModel model)
        {
            HttpResponseMessage response;
            string errorCode;
            try
            {
                var data = _service.CreateCustomerAccount(PortalId, model, out errorCode);
                response = !Equals(data, null) ? CreateOKResponse<AccountResponse>(new AccountResponse { Account = data }) : CreateNotFoundResponse();
            }
            catch (ZnodeException ex)
            {
                var data = new AccountResponse { HasError = true, ErrorCode = ex.ErrorCode, ErrorMessage = ex.ErrorMessage };
                response = CreateInternalServerErrorResponse(data);
            }
            catch (Exception ex)
            {
                var data = new AccountResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Updates an existing customer Account Details.
        /// </summary>
        /// <param name="accountId">The ID of the Customer.</param>
        /// <param name="model">The model of the Account.</param>
        /// <returns></returns>
        [HttpPut]
        public HttpResponseMessage UpdateCustomerAccount(int accountId, AccountModel model)
        {
            HttpResponseMessage response;
            string errorCode;
            try
            {
                var account = _service.UpdateCustomerAccount(accountId, model, out errorCode);
                response = !Equals(account, null) ? CreateOKResponse(new AccountResponse { Account = account, ErrorCode = Convert.ToInt32(errorCode), HasError = errorCode != "0" }) : CreateInternalServerErrorResponse();
            }
            catch (ZnodeException ex)
            {
                var data = new AccountResponse { HasError = true, ErrorCode = ex.ErrorCode, ErrorMessage = ex.ErrorMessage };
                response = CreateInternalServerErrorResponse(data);
            }
            catch (Exception ex)
            {
                var data = new AccountResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
        #endregion

        #region Custome Account Payment
        /// <summary>
        /// Get the list of Account Payments
        /// </summary>
        /// <returns>HttpResponseMessage</returns>
        [HttpGet]
        public HttpResponseMessage GetAccountPaymentList(int accountId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetAccountPaymentList(accountId, RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<AccountPaymentListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new AccountPaymentListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Creates a new AccountPayment.
        /// </summary>
        /// <param name="model">The model of the AccountPayment.</param>
        /// <returns>HttpResponseMessage</returns>
        [HttpPost]
        public HttpResponseMessage CreateAccountPayment([FromBody] AccountPaymentModel model)
        {
            HttpResponseMessage response;

            try
            {
                var accountPayment = _service.CreateAccountPayment(model);
                if (!Equals(accountPayment, null))
                {
                    var uri = Request.RequestUri;
                    var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + accountPayment.AccountPaymentId;

                    response = CreateCreatedResponse(new AccountPaymentResponse { AccountPayment = accountPayment });
                    response.Headers.Add("Location", location);
                }
                else
                {
                    response = CreateInternalServerErrorResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new AccountPaymentResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
        #endregion

        /// <summary>
        /// This function is used to check Role Exist For Profile
        /// </summary>
        /// <param name="porfileId">int porfileId - default porfileId</param>
        /// <param name="roleName">string roleName</param>
        /// <returns>returns true/false</returns>       
        [HttpGet]
        public HttpResponseMessage IsRoleExistForProfile(int porfileId, string roleName)
        {
            HttpResponseMessage response;

            try
            {
                bool isRoleExist = _service.IsRoleExistForProfile(porfileId, roleName);
                BooleanModel boolModel = new BooleanModel { disabled = isRoleExist };
                TrueFalseResponse resp = new TrueFalseResponse { booleanModel = boolModel };
                string data = JsonConvert.SerializeObject(resp);
                response = CreateOKResponse<TrueFalseResponse>(data);
            }
            catch (Exception ex)
            {
                var data = new TrueFalseResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// This method will disable the store admin account
        /// </summary>
        /// <param name="accountId">id for the user Account</param>
        /// <returns>Return the HttpResponseMessage</returns>
        [HttpPost]
        public HttpResponseMessage EnableDisable(int accountId)
        {
            HttpResponseMessage response;

            try
            {
                string resp = _cache.EnableDisableAccount(accountId);
                response = !string.IsNullOrEmpty(resp) ? CreateOKResponse<TrueFalseResponse>(resp) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new AccountResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// To Send an email to the user
        /// </summary>
        /// <param name="model">Model of type SendMailModel</param>
        /// <returns>Returns the HttpResponseMessage</returns>
        [HttpPost]
        public HttpResponseMessage SendEmail([FromBody] SendMailModel model)
        {
            HttpResponseMessage response;

            try
            {
                var email = _service.SendEmail(model);
                if (email != null)
                {
                    var uri = Request.RequestUri;
                    var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + email.EmailId;

                    response = CreateCreatedResponse(new SendMailResponse { Email = email });
                    response.Headers.Add("Location", location);
                }
                else
                {
                    response = CreateInternalServerErrorResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new SendMailResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// To Get Role Permissions by User Name
        /// </summary>
        /// <param name="model">Model of type AccountModel</param>
        /// <returns>Return the User Permission list.</returns>
        [HttpPost]
        public HttpResponseMessage GetRolePermissionByUserName([FromBody] AccountModel model)
        {
            HttpResponseMessage response;
            try
            {
                var data = _service.GetRolePermission(model.UserName);
                response = (!Equals(data, null) && !Equals(data.UserPermissionList, null)) ? CreateOKResponse(new RolePermissionListResponse { UserPermissionList = data.UserPermissionList }) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new RolePermissionListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }


        /// <summary>
        /// To Get Role Menu List by User Name
        /// </summary>
        /// <param name="model">Model of type AccountModel</param>
        /// <returns>Return the User Menu list.</returns>
        [HttpPost]
        public HttpResponseMessage GetRoleMenuListByUserName([FromBody] AccountModel model)
        {
            HttpResponseMessage response;
            try
            {
                var data = _service.GetRoleMenuList(model);
                response = (!Equals(data, null) && !Equals(data.RoleMenuList, null)) ? CreateOKResponse(new RoleMenuListResponse { RoleMenuList = data.RoleMenuList }) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new RoleMenuListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        /// <summary>
        /// To Sign up the user for News letters
        /// </summary>
        /// <param name="model">Model of type NewsLetterSignUpModel</param>
        /// <returns>Return the status.</returns>
        [HttpPost]
        public HttpResponseMessage SignUpForNewsLetter([FromBody] NewsLetterSignUpModel model)
        {
            HttpResponseMessage response;
            try
            {
                bool status = _service.SignUpForNewsLetter(model);
                string data = JsonConvert.SerializeObject(new TrueFalseResponse() { booleanModel = new BooleanModel { disabled = status } });
                response = CreateOKResponse<TrueFalseResponse>(data);
            }
            catch (ZnodeException ex)
            {
                var data = new TrueFalseResponse { HasError = true, ErrorMessage = ex.ErrorMessage };
                response = CreateInternalServerErrorResponse(data);
            }
            catch (Exception ex)
            {
                var data = new TrueFalseResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// To Validate the Social Media User Login
        /// </summary>
        /// <param name="model">Model of type SocialLoginModel</param>
        /// <returns>Return the status.</returns>
        [HttpPost]
        public HttpResponseMessage SocialUserLogin([FromBody] SocialLoginModel model)
        {
            HttpResponseMessage response;
            try
            {
                var accountDetails = _cache.SocialUserLogin(PortalId, model);
                if (!Equals(accountDetails,null))
                {
                    var data = new AccountResponse { Account = accountDetails };
                    response = CreateOKResponse(data);
                }
                else
                {
                    var data = new AccountResponse { Account = null, ErrorCode = 1003, HasError = true, ErrorMessage = "Social Login Unsucessfull" };
                    response = CreateUnauthorizedResponse(data);
                }
            }
            catch (ZnodeException ex)
            {
                var data = new AccountResponse { HasError = true, ErrorMessage = ex.ErrorMessage, ErrorCode = ex.ErrorCode };
                response = CreateInternalServerErrorResponse(data);
            }
            catch (Exception ex)
            {
                var data = new AccountResponse { HasError = true, ErrorMessage = ex.Message, ErrorCode = 1003 };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        /// <summary>
        /// Creates/Updated the Social User Account
        /// </summary>
        /// <param name="model">Model of type SocialLoginModel</param>
        /// <returns>Return the status.</returns>
        [HttpPost]
        public HttpResponseMessage SocialUserCreateOrUpdateAccount([FromBody] SocialLoginModel model)
        {
            HttpResponseMessage response;
            try
            {
               
                bool status = _service.SocialUserCreateOrUpdateAccount(model);
                string data = JsonConvert.SerializeObject(new TrueFalseResponse() { booleanModel = new BooleanModel { disabled = status } });
                response = CreateOKResponse(data);
            }
            catch (ZnodeException ex)
            {
                var data = new TrueFalseResponse { HasError = true, ErrorMessage = ex.ErrorMessage };
                response = CreateInternalServerErrorResponse(data);
            }
            catch (Exception ex)
            {
                var data = new TrueFalseResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        /// <summary>
        /// To Get the Registered Social Client Details list.
        /// This Method needs to call only once to Get the Registered Client Details.
        /// This Method Gets called from Application_Start Event from MVCDemos Global.asax
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetRegisteredSocialClientDetailsList()
        {
            HttpResponseMessage response;
            try
            {
                var data = _service.GetRegisteredSocialClientDetailsList();
                response = !Equals(data, null) ? CreateOKResponse(new RegisteredSocialClientListResponse { SocialClients = data.SocialClients }) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new RegisteredSocialClientListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
        #endregion
    }
}
