﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
	public class ShippingRulesController : BaseController
	{
		private readonly IShippingRuleService _service;
		private readonly IShippingRuleCache _cache;

		public ShippingRulesController()
		{
			_service = new ShippingRuleService();
			_cache = new ShippingRuleCache(_service);
		}

		/// <summary>
		/// Gets a shipping rule.
		/// </summary>
		/// <param name="shippingRuleId">The ID of the shipping rule.</param>
		/// <returns></returns>
		[ExpandShippingOption, ExpandShippingRuleType]
		[HttpGet]
		public HttpResponseMessage Get(int shippingRuleId)
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetShippingRule(shippingRuleId,  RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<ShippingRuleResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new ShippingRuleResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Gets a list of shipping rules.
		/// </summary>
		/// <returns></returns>
		[ExpandShippingOption, ExpandShippingRuleType]
		[FilterExternalId, FilterShippingOptionId, FilterShippingRuleTypeId]
		[SortBaseCost, SortPerItemCost, SortShippingRuleId]
		[PageIndex, PageSize]
		[HttpGet]
		public HttpResponseMessage List()
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetShippingRules(RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<ShippingRuleListResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new ShippingRuleListResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Creates a new shipping rule.
		/// </summary>
		/// <param name="model">The model of the shipping rule.</param>
		/// <returns></returns>
		[HttpPost]
		public HttpResponseMessage Create([FromBody] ShippingRuleModel model)
		{
			HttpResponseMessage response;

			try
			{
				var shippingRule = _service.CreateShippingRule(model);
				if (shippingRule != null)
				{
					var uri = Request.RequestUri;
					var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + shippingRule.ShippingRuleId;

					response = CreateCreatedResponse(new ShippingRuleResponse { ShippingRule = shippingRule });
					response.Headers.Add("Location", location);
				}
				else
				{
					response = CreateInternalServerErrorResponse();
				}
			}
			catch (Exception ex)
			{
				var data = new ShippingRuleResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Updates an existing shipping rule.
		/// </summary>
		/// <param name="shippingRuleId">The ID of the shipping rule.</param>
		/// <param name="model">The model of the shipping rule.</param>
		/// <returns></returns>
		[HttpPut]
		public HttpResponseMessage Update(int shippingRuleId, [FromBody] ShippingRuleModel model)
		{
			HttpResponseMessage response;

			try
			{
				var shippingRule = _service.UpdateShippingRule(shippingRuleId, model);
				response = shippingRule != null ? CreateOKResponse(new ShippingRuleResponse { ShippingRule = shippingRule }) : CreateInternalServerErrorResponse();
			}
			catch (Exception ex)
			{
				var data = new ShippingRuleResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Deletes an existing shipping rule.
		/// </summary>
		/// <param name="shippingRuleId">The ID of the shipping rule.</param>
		/// <returns></returns>
		[HttpDelete]
		public HttpResponseMessage Delete(int shippingRuleId)
		{
			HttpResponseMessage response;

			try
			{
				var deleted = _service.DeleteShippingRule(shippingRuleId);
				response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
			}
			catch (Exception ex)
			{
				var data = new ShippingRuleResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}
	}
}