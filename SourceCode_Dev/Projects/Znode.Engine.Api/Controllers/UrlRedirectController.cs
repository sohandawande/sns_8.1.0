﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class UrlRedirectsController : BaseController
    {
        #region Private Members
        private readonly IUrlRedirectService _service;
        private readonly IUrlRedirectCache _cache; 
        #endregion

        #region Constructor
        /// <summary>
        /// Default controller.
        /// </summary>
        public UrlRedirectsController()
        {
            _service = new UrlRedirectService();
            _cache = new UrlRedirectCache(_service);
        } 
        #endregion

        /// <summary>
        /// Gets a Url Redirect.
        /// </summary>
        /// <param name="urlRedirectId">The ID of the Url Redirect.</param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage Get(int urlRedirectId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetUrlRedirect(urlRedirectId, RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<UrlRedirectResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new UrlRedirectResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets a list of Url Redirects.
        /// </summary>
        /// <returns></returns>
        [FilterUrlRedirectId, FilterNewUrl, FilterOldUrl, FilterIsActive]
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage List()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetUrlRedirects(RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<UrlRedirectListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new UrlRedirectListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Creates a new Url Redirect.
        /// </summary>
        /// <param name="model">The model of the Url Redirect.</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Create([FromBody] UrlRedirectModel model)
        {
            HttpResponseMessage response;

            try
            {
                var urlRedirect = _service.CreateUrlRedirect(model);
                if (!Equals(urlRedirect, null))
                {
                    var uri = Request.RequestUri;
                    var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + urlRedirect.UrlRedirectId;

                    response = CreateCreatedResponse(new UrlRedirectResponse { UrlRedirect = urlRedirect });
                    response.Headers.Add("Location", location);
                }
                else
                {
                    response = CreateInternalServerErrorResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new UrlRedirectResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Updates an existing Url Redirect.
        /// </summary>
        /// <param name="urlRedirectId">The ID of the Url Redirect.</param>
        /// <param name="model">The model of the Url Redirect.</param>
        /// <returns></returns>
        [HttpPut]
        public HttpResponseMessage Update(int urlRedirectId, [FromBody] UrlRedirectModel model)
        {
            HttpResponseMessage response;

            try
            {
                var urlRedirect = _service.UpdateUrlRedirect(urlRedirectId, model);
                response = !Equals(urlRedirect, null) ? CreateOKResponse(new UrlRedirectResponse { UrlRedirect = urlRedirect }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new UrlRedirectResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Delete an existing Url Redirect.
        /// </summary>
        /// <param name="urlRedirectId">The ID of the Url Redirect.</param>
        /// <returns></returns>
        [HttpDelete]
        public HttpResponseMessage Delete(int urlRedirectId)
        {
            HttpResponseMessage response;

            try
            {
                var deleted = _service.DeleteUrlRedirect(urlRedirectId);
                response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new UrlRedirectResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
    }
}
