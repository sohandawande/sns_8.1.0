﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
	public class AuditsController : BaseController
	{
		private readonly IAuditService _service;
		private readonly IAuditCache _cache;

		public AuditsController()
		{
			_service = new AuditService();
			_cache = new AuditCache(_service);
		}

		/// <summary>
		/// Gets an audit record.
		/// </summary>
		/// <param name="auditId">The ID of the audit record.</param>
		/// <returns></returns>
		[ExpandAuditStatus, ExpandAuditSourceType, ExpandAuditType, ExpandProduct, ExpandCategories, ExpandCatalogs, ExpandPortal]
		[HttpGet]
		public HttpResponseMessage Get(int auditId)
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetAudit(auditId,  RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<AuditResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new AuditResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Gets a list of audit records.
		/// </summary>
		/// <returns></returns>
        [ExpandAuditStatus, ExpandAuditSourceType, ExpandAuditType, ExpandProduct, ExpandCategories, ExpandCatalogs, ExpandPortal]
        [FilterAuditTypeId, FilterCreateDate, FilterCreatedBy, FilterExternalId, FilterProcessedDate, FilterSourceId, FilterSourceTypeId, FilterStatusId]
		[SortAuditId, SortAuditTypeId, SortCreateDate, SortExternalId, SortProcessedDate, SortSourceId, SortSourceTypeId]
		[PageIndex, PageSize]
		[HttpGet]
		public HttpResponseMessage List()
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetAudits( RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<AuditListResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new AuditListResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Updates an existing audit record.
		/// </summary>
		/// <param name="auditId">The ID of the audit record.</param>
		/// <param name="model">The model of the audit record.</param>
		/// <returns></returns>
		[HttpPut]
		public HttpResponseMessage Update(int auditId, [FromBody] AuditModel model)
		{
			HttpResponseMessage response;

			try
			{
				var audit = _service.UpdateAudit(auditId, model);
				response = audit != null ? CreateOKResponse(new AuditResponse { Audit = audit }) : CreateInternalServerErrorResponse();
			}
			catch (Exception ex)
			{
				var data = new AuditResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Deletes an existing audit record.
		/// </summary>
		/// <param name="auditId">The ID of the audit record.</param>
		/// <returns></returns>
		[HttpDelete]
		public HttpResponseMessage Delete(int auditId)
		{
			HttpResponseMessage response;

			try
			{
				var deleted = _service.DeleteAudit(auditId);
				response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
			}
			catch (Exception ex)
			{
				var data = new AuditResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}
	}
}
