﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
	public class PaymentOptionsController : BaseController
	{
		private readonly IPaymentOptionService _service;
		private readonly IPaymentOptionCache _cache;

		public PaymentOptionsController()
		{
			_service = new PaymentOptionService();
			_cache = new PaymentOptionCache(_service);
		}

		/// <summary>
		/// Gets a payment option.
		/// </summary>
		/// <param name="paymentOptionId">The ID of the payment option.</param>
		/// <returns></returns>
		[ExpandPaymentGateway, ExpandPaymentType]
		[HttpGet]
		public HttpResponseMessage Get(int paymentOptionId)
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetPaymentOption(paymentOptionId,  RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<PaymentOptionResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new PaymentOptionResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Gets a list of payment options.
		/// </summary>
		/// <returns></returns>
		[ExpandPaymentGateway, ExpandPaymentType]
		[FilterIsActive, FilterPartner, FilterPaymentGatewayId, FilterPaymentTypeId, FilterProfileId, FilterVendor]
		[SortDisplayOrder, SortPaymentOptionId]
		[PageIndex, PageSize]
		[HttpGet]
		public HttpResponseMessage List()
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetPaymentOptions( RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<PaymentOptionListResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new PaymentOptionListResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Creates a new payment option.
		/// </summary>
		/// <param name="model">The model of the payment option.</param>
		/// <returns></returns>
		[HttpPost]
		public HttpResponseMessage Create([FromBody] PaymentOptionModel model)
		{
			HttpResponseMessage response;

			try
			{
				var paymentOption = _service.CreatePaymentOption(model);
				if (paymentOption != null)
				{
					var uri = Request.RequestUri;
					var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + paymentOption.PaymentOptionId;

					response = CreateCreatedResponse(new PaymentOptionResponse { PaymentOption = paymentOption });
					response.Headers.Add("Location", location);
				}
				else
				{
					response = CreateInternalServerErrorResponse();
				}
			}
			catch (Exception ex)
			{
				var data = new PaymentOptionResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Updates an existing payment option.
		/// </summary>
		/// <param name="paymentOptionId">The ID of the payment option.</param>
		/// <param name="model">The model of the payment option.</param>
		/// <returns></returns>
		[HttpPut]
		public HttpResponseMessage Update(int paymentOptionId, [FromBody] PaymentOptionModel model)
		{
			HttpResponseMessage response;

			try
			{
				var paymentOption = _service.UpdatePaymentOption(paymentOptionId, model);
				response = paymentOption != null ? CreateOKResponse(new PaymentOptionResponse { PaymentOption = paymentOption }) : CreateInternalServerErrorResponse();
			}
			catch (Exception ex)
			{
				var data = new PaymentOptionResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Deletes an existing payment option.
		/// </summary>
		/// <param name="paymentOptionId">The ID of the payment option.</param>
		/// <returns></returns>
		[HttpDelete]
		public HttpResponseMessage Delete(int paymentOptionId)
		{
			HttpResponseMessage response;

			try
			{
				var deleted = _service.DeletePaymentOption(paymentOptionId);
				response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
			}
			catch (Exception ex)
			{
				var data = new PaymentOptionResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}
	}
}