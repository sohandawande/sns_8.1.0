﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    public class OrdersController : BaseController
    {
        private readonly IOrderService _service;
        private readonly ICustomerService _customerService;
        private readonly IOrderCache _cache;

        public OrdersController()
        {
            _service = new OrderService();
            _customerService = new CustomerService();

            _cache = new OrderCache(_service);
        }

        /// <summary>
        /// Gets an order.
        /// </summary>
        /// <param name="orderId">The ID of the order.</param>
        /// <returns></returns>
        [ExpandAccount, ExpandOrderLineItems, ExpandPaymentOption, ExpandPaymentType, ExpandShippingOption]
        [HttpGet]
        public HttpResponseMessage Get(int orderId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetOrder(orderId, RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<OrderResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new OrderResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets a list of orders.
        /// </summary>
        /// <returns></returns>
        [ExpandAccount, ExpandOrderLineItems, ExpandPaymentOption, ExpandPaymentType, ExpandShippingOption]
        [FilterAccountId, FilterCompanyName, FilterCountryCode, FilterEmail, FilterExternalId, FilterLastName, FilterOrderDate, FilterOrderStateId, FilterPortalId, FilterPostalCode, FilterPurchaseOrderNumber, FilterStateCode, FilterTotal, FilterTrackingNumber, FilterTransactionId]
        [SortCity, SortCompanyName, SortCountryCode, SortLastName, SortOrderDate, SortOrderId, SortPostalCode, SortStateCode, SortTotal]
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage List()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetOrders(RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<OrderListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new OrderListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Creates a new order.
        /// </summary>
        /// <param name="model">The model of the order.</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Create([FromBody] ShoppingCartModel model)
        {
            HttpResponseMessage response;

            try
            {
                int portalId = PortalId;
                if (!Equals(model.PortalId, null))
                {
                    portalId = model.PortalId.GetValueOrDefault();
                }
                var order = _service.CreateOrder(portalId, model);
                if (!Equals(order, null))
                {
                    var uri = Request.RequestUri;
                    var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + order.OrderId;

                    if (order.IsOffsitePayment)
                    {
                        response = CreateOKResponse(new OrderResponse { Order = order });
                    }
                    else
                    {
                        response = CreateCreatedResponse(new OrderResponse { Order = order });
                    }

                    response.Headers.Add("Location", location);
                }
                else
                {
                    response = CreateInternalServerErrorResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new OrderResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Updates an existing order.
        /// </summary>
        /// <param name="orderId">The ID of the order.</param>
        /// <param name="model">The model of the order.</param>
        /// <returns></returns>
        [HttpPut]
        public HttpResponseMessage Update(int orderId, [FromBody] OrderModel model)
        {
            HttpResponseMessage response;

            try
            {
                var order = _service.UpdateOrder(orderId, model);
                response = !Equals(order, null) ? CreateOKResponse(new OrderResponse { Order = order }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new OrderResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }


        #region ZNode Version 8.0
        /// <summary>
        /// Gets Order list from database using stored procedure.
        /// </summary>
        /// <returns>Returns HttpResponseMessage.</returns>
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage GetOrderList()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetOrderList(RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<AdminOrderListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new AdminOrderListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets Order details from database using stored procedure.
        /// </summary>
        /// <returns>Returns HttpResponseMessage.</returns>
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage GetOrderDetails()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetOrderDetails(RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<OrderResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new OrderResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }


        /// <summary>
        /// Updates an existing order status.
        /// </summary>
        /// <param name="orderId">The ID of the order.</param>
        /// <param name="model">The model of the order.</param>
        /// <returns>Returns HttpResponseMessage.</returns>
        [HttpPut]
        public HttpResponseMessage UpdateOrderStatus(int orderId, [FromBody] OrderModel model)
        {
            HttpResponseMessage response;

            try
            {
                var order = _service.UpdateOrderStatus(orderId, model);
                response = !Equals(order, null) ? CreateOKResponse(new OrderResponse { Order = order }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new OrderResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }


        /// <summary>
        /// Updates an existing order with void payment.
        /// </summary>
        /// <param name="orderId">The ID of the order.</param>
        /// <param name="model">The model of the order.</param>
        /// <returns>Returns HttpResponseMessage.</returns>
        [HttpPut]
        public HttpResponseMessage VoidPayment(int orderId, [FromBody] OrderModel model)
        {
            HttpResponseMessage response;

            try
            {
                var order = _service.VoidPayment(orderId, model);
                response = !Equals(order, null) ? CreateOKResponse(new OrderResponse { Order = order }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new OrderResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }


        /// <summary>
        /// Updates an existing order with refund payment.
        /// </summary>
        /// <param name="orderId">The ID of the order.</param>
        /// <param name="model">The model of the order.</param>
        /// <returns>Returns HttpResponseMessage.</returns>
        [HttpPut]
        public HttpResponseMessage RefundPayment(int orderId, [FromBody] OrderModel model)
        {
            HttpResponseMessage response;

            try
            {
                var order = _service.RefundPayment(orderId, model);
                response = !Equals(order, null) ? CreateOKResponse(new OrderResponse { Order = order }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new OrderResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Downloads the order data.
        /// </summary>
        /// <returns>Returns HttpResponseMessage response.</returns>
        [HttpGet]
        public HttpResponseMessage DownloadOrder()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.DownloadOrder(RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<OrderResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new OrderResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }


        /// <summary>
        /// Downloads the order line item data.
        /// </summary>
        /// <returns>Returns HttpResponseMessage response.</returns>
        [HttpGet]
        public HttpResponseMessage DownloadOrderLineItemData()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.DownloadOrderLineItems(RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<OrderResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new OrderResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// To Create the New Customer.
        /// </summary>
        /// <param name="listModel">Model of the AddressListModel</param>
        /// <returns>Returns HttpResponseMessage response.</returns>
        public HttpResponseMessage AddNewCustomer([FromBody] AddressListModel listModel)
        {
            HttpResponseMessage response;

            try
            {
                AddressListModel list = _customerService.AddCustomer(listModel);
                response = !Equals(list, null) ? CreateOKResponse(new AddressListResponse { AddressListModel = list }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new AddressListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets Order Line Item details.
        /// </summary>
        /// <param name="orderLineItemId">int id of order line item</param>
        /// <returns>Returns HttpResponseMessage.</returns>
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage GetOrderLineItems(int orderLineItemId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetOrderLineItemDetails(orderLineItemId, RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<OrderLineItemResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new OrderLineItemResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }


        /// <summary>
        /// Updates an existing order status.
        /// </summary>
        /// <param name="orderLineItemId">The ID of the order.</param>
        /// <param name="model">The model of the order.</param>
        /// <returns>Returns HttpResponseMessage.</returns>
        [HttpPut]
        public HttpResponseMessage UpdateOrderLineItemStatus(int orderLineItemId, [FromBody] OrderLineItemModel model)
        {
            HttpResponseMessage response;

            try
            {
                var order = _service.UpdateOrderLineItem(orderLineItemId, model);
                response = !Equals(order, null) ? CreateOKResponse(new OrderLineItemResponse { OrderLineItem = order }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new OrderLineItemResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="paymentStatus"></param>
        /// <returns>True if update succeeded</returns>
        [HttpGet]
        public HttpResponseMessage updateorderpaymentstatus(int orderId, string paymentStatus)
        {
            HttpResponseMessage response;

            try
            {
                bool isUpdated = _service.updateorderpaymentstatus(orderId, paymentStatus);
                BooleanModel boolModel = new BooleanModel { disabled = isUpdated };
                TrueFalseResponse resp = new TrueFalseResponse { booleanModel = boolModel };
                string data = JsonConvert.SerializeObject(resp);
                response = isUpdated ? CreateOKResponse<TrueFalseResponse>(data) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new TrueFalseResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }



        /// <summary>
        /// Sends email for the shipped status change and also tracking number of order line item.       
        /// </summary>
        /// <returns>Returns HttpResponseMessage response.</returns>
        [HttpGet]
        public HttpResponseMessage SendEmail()
        {
            HttpResponseMessage response;

            try
            {
                string data = _cache.SendEmail(RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<OrderResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                OrderResponse data = new OrderResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        #endregion

        #region PRFT Custom Code
        [HttpPost]
        public HttpResponseMessage GetOrderReceiptJavascript([FromBody] OrderModel model)
        {
            HttpResponseMessage response;

            try
            {
                var data = _service.GetEcommerceTrackingOrderReceiptJavascript(model);
                response = !Equals(data, null) ? CreateOKResponse(new PRFTOrderReceiptJavascriptResponse { EcommerceTrackingJavascript = data }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new OrderResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Resubmit orders to ERP
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage ReSubmitOrderToERP(int orderId)
        {
            HttpResponseMessage response;

            try
            {
                var orderSumissionModel = _service.ReSubmitOrderToERP(orderId);

                response = !Equals(orderSumissionModel, null) ? CreateOKResponse(new PRFTERPOrderSubmissionResponse { OrderSubmissionResponse = orderSumissionModel }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new PRFTERPOrderSubmissionResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets Order details from ERP.
        /// </summary>
        /// <returns>Returns HttpResponseMessage.</returns>        
        [HttpGet]
        public HttpResponseMessage GetInvoiceDetails(string externalId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetInvoiceDetails(externalId, RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<PRFTERPOrderResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new PRFTERPOrderResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
        #endregion
    }
}
