﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
	public class ShippingRuleTypesController : BaseController
	{
		private readonly IShippingRuleTypeService _service;
		private readonly IShippingRuleTypeCache _cache;

		public ShippingRuleTypesController()
		{
			_service = new ShippingRuleTypeService();
			_cache = new ShippingRuleTypeCache(_service);
		}

		/// <summary>
		/// Gets a shipping rule type.
		/// </summary>
		/// <param name="shippingRuleTypeId">The ID of the shipping rule type.</param>
		/// <returns></returns>
		[HttpGet]
		public HttpResponseMessage Get(int shippingRuleTypeId)
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetShippingRuleType(shippingRuleTypeId, RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<ShippingRuleTypeResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new ShippingRuleTypeResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Gets a list of shipping rule types.
		/// </summary>
		/// <returns></returns>
		[FilterClassName, FilterIsActive, FilterName]
		[SortName, SortShippingRuleTypeId]
		[PageIndex, PageSize]
		[HttpGet]
		public HttpResponseMessage List()
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetShippingRuleTypes(RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<ShippingRuleTypeListResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new ShippingRuleTypeListResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Creates a new shipping rule type.
		/// </summary>
		/// <param name="model">The model of the shipping rule type.</param>
		/// <returns></returns>
		[HttpPost]
		public HttpResponseMessage Create([FromBody] ShippingRuleTypeModel model)
		{
			HttpResponseMessage response;

			try
			{
				var shippingRuleType = _service.CreateShippingRuleType(model);
				if (shippingRuleType != null)
				{
					var uri = Request.RequestUri;
					var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + shippingRuleType.ShippingRuleTypeId;

					response = CreateCreatedResponse(new ShippingRuleTypeResponse { ShippingRuleType = shippingRuleType });
					response.Headers.Add("Location", location);
				}
				else
				{
					response = CreateInternalServerErrorResponse();
				}
			}
			catch (Exception ex)
			{
				var data = new ShippingRuleTypeResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Updates an existing shipping rule type.
		/// </summary>
		/// <param name="shippingRuleTypeId">The ID of the shipping rule type.</param>
		/// <param name="model">The model of the shipping rule type.</param>
		/// <returns></returns>
		[HttpPut]
		public HttpResponseMessage Update(int shippingRuleTypeId, [FromBody] ShippingRuleTypeModel model)
		{
			HttpResponseMessage response;

			try
			{
				var shippingRuleType = _service.UpdateShippingRuleType(shippingRuleTypeId, model);
				response = shippingRuleType != null ? CreateOKResponse(new ShippingRuleTypeResponse { ShippingRuleType = shippingRuleType }) : CreateInternalServerErrorResponse();
			}
			catch (Exception ex)
			{
				var data = new ShippingRuleTypeResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Deletes an existing shipping rule type.
		/// </summary>
		/// <param name="shippingRuleTypeId">The ID of the shipping rule type.</param>
		/// <returns></returns>
		[HttpDelete]
		public HttpResponseMessage Delete(int shippingRuleTypeId)
		{
			HttpResponseMessage response;

			try
			{
				var deleted = _service.DeleteShippingRuleType(shippingRuleTypeId);
				response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
			}
			catch (Exception ex)
			{
				var data = new ShippingRuleTypeResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}
	}
}