﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    public class FacetGroupsController : BaseController
    {
        private readonly IFacetGroupService _service;
        private readonly IFacetGroupCache _cache;

        public FacetGroupsController()
        {
            _service = new FacetGroupService();
            _cache = new FacetGroupCache(_service);
        }

        /// <summary>
        /// Gets the facet group.
        /// </summary>
        /// <param name="facetGroupId">The ID of the facet group.</param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage Get(int facetGroupId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetFacetGroup(facetGroupId, RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<FacetGroupResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new FacetGroupResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets a list of Facet groups.
        /// </summary>
        /// <returns></returns>
        
        //[ExpandAccountType, ExpandAddresses, ExpandOrders, ExpandProfiles, ExpandWishList]
        //[FilterAccountTypeId, FilterCompanyName, FilterEmail, FilterEmailOptIn, FilterEnableCustomerPricing, FilterExternalId, FilterIsActive, FilterParentAccountId, FilterProfileId, FilterUserId]
        //[SortAccountId, SortCompanyName, SortCreateDate]
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage List()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetFacetGroups(RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<FacetGroupListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new FacetGroupListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Get List of Facet Group Control Types.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetControlTypeList()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetFacetControlTypes(RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<FacetControlTypeListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new FacetControlTypeListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Creates a new facet group.
        /// </summary>
        /// <param name="model">The model of the facet group.</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Create([FromBody] FacetGroupModel model)
        {
            HttpResponseMessage response;

            try
            {
                var facetGroup = _service.CreateFacetGroup(model);
                if (facetGroup != null)
                {
                    var uri = Request.RequestUri;
                    var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + facetGroup.FacetGroupID;

                    response = CreateCreatedResponse(new FacetGroupResponse { FacetGroup = facetGroup });
                    response.Headers.Add("Location", location);
                }
                else
                {
                    response = CreateInternalServerErrorResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new FacetGroupResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Updates an existing facet group.
        /// </summary>
        /// <param name="productId">The ID of the facet group.</param>
        /// <param name="model">The model of the facet group.</param>
        /// <returns></returns>
        [HttpPut]
        public HttpResponseMessage Update(int facetGroupId, [FromBody] FacetGroupModel model)
        {
            HttpResponseMessage response;

            try
            {
                var facetGroup = _service.UpdateFacetGroup(facetGroupId, model);
                response = facetGroup != null ? CreateOKResponse(new FacetGroupResponse { FacetGroup = facetGroup }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new FacetGroupResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Deletes an existing facet group.
        /// </summary>
        /// <param name="productId">The ID of the facet group.</param>
        /// <returns></returns>
        [HttpDelete]
        public HttpResponseMessage Delete(int facetGroupId)
        {
            HttpResponseMessage response;

            try
            {
                var deleted = _service.DeleteFacetGroup(facetGroupId);
                response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new FacetGroupResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Creates a new facet group category.
        /// </summary>
        /// <param name="model">The model of the facet group.</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage InsertFacetGroupCategory([FromBody] FacetGroupCategoryListModel model)
        {
            HttpResponseMessage response;

            try
            {
                var facetGroupCategory = _service.CreateFacetGroupCategory(model);
                if (facetGroupCategory != null)
                {
                    var uri = Request.RequestUri;
                    var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + facetGroupCategory.FacetGroupCategoryList;

                    response = CreateCreatedResponse(new FacetGroupCategoryListResponse { FacetGroupCategoryList = facetGroupCategory.FacetGroupCategoryList });
                    response.Headers.Add("Location", location);
                }
                else
                {
                    response = CreateInternalServerErrorResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new FacetGroupResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        } 

        /// <summary>
        /// Deletes an existing facet group.
        /// </summary>
        /// <param name="productId">The ID of the facet group.</param>
        /// <returns></returns>
        [HttpDelete]
        public HttpResponseMessage DeleteFacetGroupCategory(int facetGroupId)
        {
            HttpResponseMessage response;

            try
            {
                var deleted = _service.DeleteFacetGroupCategory(facetGroupId);
                response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new FacetGroupResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Creates a new facet.
        /// </summary>
        /// <param name="model">The model of the facet.</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage CreateFacet([FromBody] FacetModel model)
        {
            HttpResponseMessage response;

            try
            {
                var facetGroup = _service.CreateFacet(model);
                if (!Equals(facetGroup, null))
                {
                    var uri = Request.RequestUri;
                    var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + facetGroup.FacetGroupID;

                    response = CreateCreatedResponse(new FacetResponse { Facet = facetGroup });
                    response.Headers.Add("Location", location);
                }
                else
                {
                    response = CreateInternalServerErrorResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new FacetResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Updates an existing facet.
        /// </summary>
        /// <param name="productId">The ID of the facet.</param>
        /// <param name="model">The model of the facet.</param>
        /// <returns></returns>
        [HttpPut]
        public HttpResponseMessage UpdateFacet(int facetId, [FromBody] FacetModel model)
        {
            HttpResponseMessage response;

            try
            {
                var facet = _service.UpdateFacet(facetId, model);
                response = facet != null ? CreateOKResponse(new FacetResponse { Facet = facet }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new FacetResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Deletes an existing facet.
        /// </summary>
        /// <param name="productId">The ID of the facet.</param>
        /// <returns></returns>
        [HttpDelete]
        public HttpResponseMessage DeleteFacet(int facetId)
        {
            HttpResponseMessage response;

            try
            {
                var deleted = _service.DeleteFacet(facetId);
                response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new FacetResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets the facet group.
        /// </summary>
        /// <param name="facetGroupId">The ID of the facet group.</param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetFacet(int facetId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetFacet(facetId, RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<FacetResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new FacetResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// To Get Facet List.
        /// </summary>
        /// <returns>Return facet List.</returns>
      
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage Facetlist()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetFacetList(RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<FacetGroupListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new FacetGroupListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
    }
}
