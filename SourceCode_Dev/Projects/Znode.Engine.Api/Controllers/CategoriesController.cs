﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;


namespace Znode.Engine.Api.Controllers
{
    public class CategoriesController : BaseController
    {
        private readonly ICategoryService _service;
        private readonly ICategoryCache _cache;

        public CategoriesController()
        {
            _service = new CategoryService();
            _cache = new CategoryCache(_service);
        }

        /// <summary>
        /// Gets a category.
        /// </summary>
        /// <param name="categoryId">The ID of the category.</param>
        /// <returns></returns>
        [ExpandCategoryNodes, ExpandProductIds, ExpandSubcategories]
        [HttpGet]
        public HttpResponseMessage Get(int categoryId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetCategory(categoryId, RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<CategoryResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new CategoryResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets a list of categories.
        /// </summary>
        /// <returns></returns>
        [ExpandCategoryNodes, ExpandProductIds, ExpandSubcategories]
        [FilterExternalId, FilterIsVisible, FilterName, FilterPortalId, FilterTitle]
        [SortCategoryId, SortDisplayOrder, SortName, SortTitle]
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage List()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetCategories(RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<CategoryListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new CategoryListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets a list of products for the specified external IDs.
        /// </summary>
        /// <param name="categoryIds">The comma-seperated category IDs of the categories.</param>
        /// <returns></returns>
        [ExpandCategoryNodes, ExpandProductIds, ExpandSubcategories]
        [FilterExternalId, FilterIsVisible, FilterName, FilterPortalId, FilterTitle]
        [SortCategoryId, SortDisplayOrder, SortName, SortTitle]
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage ListByCategoryIds(string categoryIds)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetCategoriesByCategoryIds(categoryIds, RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<CategoryListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new CategoryListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets a list of categories for a specific catalog.
        /// </summary>
        /// <param name="catalogId">The ID of the catalog.</param>
        /// <returns></returns>
        [ExpandCategoryNodes, ExpandProductIds, ExpandSubcategories]
        [FilterExternalId, FilterIsVisible, FilterName, FilterPortalId, FilterTitle]
        [SortCategoryId, SortDisplayOrder, SortName, SortTitle]
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage ListByCatalog(int catalogId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetCategoriesByCatalog(catalogId, RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<CategoryListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new CategoryListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets a list of products for the specified external IDs.
        /// </summary>
        /// <param name="catalogIds">The comma-seperated external IDs of the products.</param>
        /// <returns></returns>
        [ExpandCategoryNodes, ExpandProductIds, ExpandSubcategories]
        [FilterExternalId, FilterIsVisible, FilterName, FilterPortalId, FilterTitle]
        [SortCategoryId, SortDisplayOrder, SortName, SortTitle]
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage ListByCatalogIds(string catalogIds)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetCategoriesByCatalogIds(catalogIds, RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<CategoryListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new CategoryListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Creates a new category.
        /// </summary>
        /// <param name="model">The model of the category.</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Create([FromBody] CategoryModel model)
        {
            HttpResponseMessage response;

            try
            {
                var category = _service.CreateCategory(model);
                if (!Equals(category, null))
                {
                    var uri = Request.RequestUri;
                    var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + category.CategoryId;

                    response = CreateCreatedResponse(new CategoryResponse { Category = category });
                    response.Headers.Add("Location", location);
                }
                else
                {
                    response = CreateInternalServerErrorResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new CategoryResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Updates an existing category.
        /// </summary>
        /// <param name="categoryId">The ID of the category.</param>
        /// <param name="model">The model of the category.</param>
        /// <returns></returns>
        [HttpPut]
        public HttpResponseMessage Update(int categoryId, [FromBody] CategoryModel model)
        {
            HttpResponseMessage response;

            try
            {
                var category = _service.UpdateCategory(categoryId, model);
                response = !Equals(category, null) ? CreateOKResponse(new CategoryResponse { Category = category }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new CategoryResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Deletes an existing profile by profile Id.
        /// </summary>
        /// <param name="productId">The ID of the profile.</param>
        /// <returns></returns>
        [HttpDelete]
        public HttpResponseMessage Delete(int categoryId)
        {
            HttpResponseMessage response;

            try
            {
                var deleted = _service.DeleteCategory(categoryId);
                response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new CategoryResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        #region Znode Version 8.0
        /// <summary>
        /// Gets categories by catalog Id.
        /// </summary>
        /// <param name="catalogId">The Id of catalog.</param>
        /// <returns>Returns categories by catalog Id.</returns>
        [PageIndex, PageSize]
        [FilterName]
        [SortCategoryId]
        [HttpGet]
        public HttpResponseMessage GetCategoriesByCatalogId(int catalogId = 0, int totalRowCount = 0)
        {
            HttpResponseMessage response;

            try
            {
                var category = _cache.GetCategoriesByCatalogIdUsingCustomService(out totalRowCount, catalogId, RouteUri, RouteTemplate);

                response = !Equals(category, null) ? CreateOKResponse<CategoryListResponse>(category) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new CategoryResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets all categories.
        /// </summary>
        /// <param name="categoryName">Name of category.</param>
        /// <returns>Returns all categories.</returns>
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage GetAllCategories()
        {
            HttpResponseMessage response;

            try
            {
                var category = _cache.GetAllCategories(RouteUri, RouteTemplate);
                response = !Equals(category, null) ? CreateOKResponse<CategoryListResponse>(category) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new CategoryResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Get Products Associated with category 
        /// </summary>
        /// <param name="categoryId"></param>
        /// <param name="productIds"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage CategoryAssociatedProducts(int categoryId, string productIds)
        {
            HttpResponseMessage response;
            try
            {
                response = _service.CategoryAssociatedProducts(categoryId, productIds) ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new CategoryResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        [HttpPut]
        public HttpResponseMessage UpdateCategorySEODetails(int categoryId, [FromBody] CategoryModel model)
        {
            HttpResponseMessage response;

            try
            {
                var category = _service.UpdateCategorySEODetails(categoryId, model);
                response = !Equals(category, null) ? CreateOKResponse(new CategoryResponse { Category = category }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new CategoryResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        [HttpGet]
        public HttpResponseMessage GetCategoryAssociatedProducts()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetCategoryAssociatedProducts(RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<ProductListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Get the List of Category Sub-Category & its Products
        /// </summary>
        /// <returns></returns>
        [ExpandCategoryNodes, ExpandProductIds, ExpandSubcategories, ExpandProduct]
        [FilterExternalId, FilterIsVisible, FilterName, FilterPortalId, FilterTitle]
        [SortCategoryId, SortDisplayOrder, SortName, SortTitle]
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage GetCategoryTree()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetCategoryTree(RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<CategoryListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new CategoryListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
        #endregion
    }
}
