﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    /// <summary>
    /// Api Controller for profile 
    /// </summary>
    public class ProfileController : BaseController
    {
        #region Private Variables
        private readonly IProfileService _service;
        private readonly IProfileCache _cache;
        #endregion

        #region Controller
        /// <summary>
        /// Api controller for Profile
        /// </summary>
        public ProfileController()
        {
            _service = new ProfileService();
            _cache = new ProfileCache(_service);
        }
        #endregion

        #region Action methods
        /// <summary>
        /// Gets the profile By ProfileId.
        /// </summary>
        /// <param name="facetGroupId">The ID of the profile.</param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage Get(int profileId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetProfile(profileId, RouteUri, RouteTemplate);
                response = (!Equals(data, null)) ? CreateOKResponse<ProfileResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProfileResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        /// <summary>
        /// Gets the profile list.
        /// </summary>       
        /// <returns> Returns HttpResponseMessage type response.</returns>
        [HttpGet]
        public HttpResponseMessage GetProfileList()
        {
            HttpResponseMessage response;

            try
            {
                var data = _service.GetProfiles();
                response = (!Equals(data, null)) ? CreateOKResponse(new ProfileListResponse { Profiles = data.Profiles }) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProfileListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        /// <summary>
        /// Gets a list of Profiles.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage List()
        {
            HttpResponseMessage response;
            try
            {
                var data = _cache.GetProfiles(RouteUri, RouteTemplate);
                response = (!Equals(data, null)) ? CreateOKResponse<ProfileListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProfileListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        /// <summary>
        /// Get Customer Account List.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage CustomerProfileList()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetCustomerProfile(RouteUri, RouteTemplate);
                response = (!Equals(data, null)) ? CreateOKResponse<ProfileListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProfileListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        /// <summary>
        /// Get Profiles list that not associated with account.
        /// </summary>
        /// <param name="accountId">account Id</param>
        /// <returns>HttpResponseMessage</returns>
        [HttpGet]
        public HttpResponseMessage CustomerNotAssociatedProfiles()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetAccountNotAssociatedProfile(RouteUri, RouteTemplate);
                response = (!Equals(data, null)) ? CreateOKResponse<ProfileListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProfileListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        /// <summary>
        /// Creates a new profile.
        /// </summary>
        /// <param name="model">The model of the profile.</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Create([FromBody] ProfileModel model)
        {
            HttpResponseMessage response;

            try
            {
                var profile = _service.CreateProfile(model);
                if (!Equals(profile, null))
                {
                    var uri = Request.RequestUri;
                    var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + profile.ProfileId;

                    response = CreateCreatedResponse(new ProfileResponse { Profile = profile });
                    response.Headers.Add("Location", location);
                }
                else
                {
                    response = CreateInternalServerErrorResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new ProfileResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        /// <summary>
        /// Updates an existing profile by profile Id.
        /// </summary>
        /// <param name="profileId">The ID of the profile.</param>
        /// <param name="model">The model of the profile.</param>
        /// <returns></returns>
        [HttpPut]
        public HttpResponseMessage Update(int profileId, [FromBody] ProfileModel model)
        {
            HttpResponseMessage response;

            try
            {
                var profile = _service.UpdateProfile(profileId, model);
                response = (!Equals(profile, null)) ? CreateOKResponse(new ProfileResponse { Profile = profile }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new ProfileResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        /// <summary>
        /// Deletes an existing profile by profile Id.
        /// </summary>
        /// <param name="profileId">The ID of the profile.</param>
        /// <returns></returns>
        [HttpDelete]
        public HttpResponseMessage Delete(int profileId)
        {
            HttpResponseMessage response;

            try
            {
                var deleted = _service.DeleteProfile(profileId);
                response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new ProfileResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        #region Znode Version 8.0
        /// <summary>
        /// Znode Version 8.0
        /// Gets the profile list.
        /// </summary>       
        /// <returns> Returns HttpResponseMessage type response.</returns>
        [HttpGet]
        public HttpResponseMessage GetProfileListByProfileId(int portalId)
        {
            HttpResponseMessage response;
            try
            {
                var data = _cache.GetProfileListByProfileId(portalId, RouteUri, RouteTemplate);
                response = (!Equals(data, null)) ? CreateOKResponse<ProfileListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProfileListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        /// <summary>
        /// Znode Version 8.0
        /// Gets the profile list.
        /// </summary>       
        /// <returns> Returns HttpResponseMessage type response.</returns>
        [HttpGet]
        public HttpResponseMessage GetProfileListByPortalId(int portalId)
        {
            HttpResponseMessage response;
            try
            {
                var data = _cache.GetProfileListByPortalId(portalId, RouteUri, RouteTemplate);
                response = (!Equals(data, null)) ? CreateOKResponse<ProfileListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProfileListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        /// <summary>
        /// Znode Version 8.0
        /// Get available profile list by sku id.
        /// </summary>
        /// <param name="userName">user name.</param>
        /// <param name="skuId">sku id.</param>
        /// <param name="categoryId">category id.</param>
        /// <returns>Returns HttpResponseMessage type response.</returns>
        [HttpPost]
        public HttpResponseMessage GetAvailableProfileListBySkuIdCategoryId([FromBody] AssociatedSkuCategoryProfileModel model)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetAvailableProfileListBySkuIdCategoryId(model, RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<ProfileListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProfileListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
        /// <summary>
        /// Get Znode profile
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetZNodeProfile()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetZNodeProfile(RouteUri, RouteTemplate);
                response = (!Equals(data, null)) ? CreateOKResponse<ProfileResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProfileResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        /// <summary>
        /// Get Profiles List by userName.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage GetProfilesAssociatedWithUsers([FromBody] AccountModel model)
        {
            HttpResponseMessage response;
            try
            {
                var data = _cache.GetProfilesAssociatedWithUsers(model.UserName, RouteUri, RouteTemplate);
                response = (!Equals(data, null)) ? CreateOKResponse<ProfileListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProfileListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }
        #endregion
        #endregion
    }
}
