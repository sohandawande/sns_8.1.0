﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
	public class ManufacturersController : BaseController
	{
		private readonly IManufacturerService _service;
		private readonly IManufacturerCache _cache;

		public ManufacturersController()
		{
			_service = new ManufacturerService();
			_cache = new ManufacturerCache(_service);
		}

		/// <summary>
		/// Gets a manufacturer.
		/// </summary>
		/// <param name="manufacturerId">The ID of the manufacturer.</param>
		/// <returns></returns>
		[HttpGet]
		public HttpResponseMessage Get(int manufacturerId)
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetManufacturer(manufacturerId, RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<ManufacturerResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new ManufacturerResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Gets a list of manufacturers.
		/// </summary>
		/// <returns></returns>
		[FilterIsActive, FilterName, FilterPortalId]
		[SortDisplayOrder, SortName, SortManufacturerId]
		[PageIndex, PageSize]
		[HttpGet]
		public HttpResponseMessage List()
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetManufacturers(RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<ManufacturerListResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new ManufacturerListResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Creates a new manufacturer.
		/// </summary>
		/// <param name="model">The model of the manufacturer.</param>
		/// <returns></returns>
		[HttpPost]
		public HttpResponseMessage Create([FromBody] ManufacturerModel model)
		{
			HttpResponseMessage response;

			try
			{
				var manufacturer = _service.CreateManufacturer(model);
				if (manufacturer != null)
				{
					var uri = Request.RequestUri;
					var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + manufacturer.ManufacturerId;

					response = CreateCreatedResponse(new ManufacturerResponse { Manufacturer = manufacturer });
					response.Headers.Add("Location", location);
				}
				else
				{
					response = CreateInternalServerErrorResponse();
				}
			}
			catch (Exception ex)
			{
				var data = new ManufacturerResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Updates an existing manufacturer.
		/// </summary>
		/// <param name="manufacturerId">The ID of the manufacturer.</param>
		/// <param name="model">The model of the manufacturer.</param>
		/// <returns></returns>
		[HttpPut]
		public HttpResponseMessage Update(int manufacturerId, [FromBody] ManufacturerModel model)
		{
			HttpResponseMessage response;

			try
			{
				var manufacturer = _service.UpdateManufacturer(manufacturerId, model);
				response = manufacturer != null ? CreateOKResponse(new ManufacturerResponse { Manufacturer = manufacturer }) : CreateInternalServerErrorResponse();
			}
			catch (Exception ex)
			{
				var data = new ManufacturerResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Deletes an existing manufacturer.
		/// </summary>
		/// <param name="manufacturerId">The ID of the manufacturer.</param>
		/// <returns></returns>
		[HttpDelete]
		public HttpResponseMessage Delete(int manufacturerId)
		{
			HttpResponseMessage response;

			try
			{
				var isDeleted = _service.DeleteManufacturer(manufacturerId);
                BooleanModel boolModel = new BooleanModel { disabled = isDeleted };
                TrueFalseResponse resp = new TrueFalseResponse { booleanModel = boolModel };
                string data = JsonConvert.SerializeObject(resp);
                response =  isDeleted? CreateOKResponse<TrueFalseResponse>(data) : CreateInternalServerErrorResponse();
			}
			catch (Exception ex)
			{
                var data = new TrueFalseResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}
	}
}