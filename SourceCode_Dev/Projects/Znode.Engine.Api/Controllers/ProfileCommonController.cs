﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    public class ProfileCommonController : BaseController
    {
        private readonly ICommonProfileService _service;

        public ProfileCommonController()
		{
			_service = new CommonProfileService();
		}
        /// <summary>
        /// Gets Profile Store access list.
        /// </summary>
        /// <param name="model">Model of type AccountModel</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage GetProfileStoreAccess([FromBody] AccountModel model)
        {
            HttpResponseMessage response;

            try
            {
                var data = _service.GetProfileStoreAccess(model.UserName);
                response = (!Equals(data, null)) ? CreateOKResponse<ProfileCommonListResponse>(new ProfileCommonListResponse { Profiles = data.Profiles }) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProfileCommonListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

    }
}
