﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
	public class PortalCountriesController : BaseController
	{
        #region Private Variables
        private readonly IPortalCountryService _service;
        private readonly IPortalCountryCache _cache; 
        #endregion

        #region Constructor
        public PortalCountriesController()
        {
            _service = new PortalCountryService();
            _cache = new PortalCountryCache(_service);
        } 
        #endregion

        #region Public Action Methods
        /// <summary>
        /// Gets a portal country.
        /// </summary>
        /// <param name="portalCountryId">The ID of the portal country.</param>
        /// <returns></returns>
        [ExpandCountry]
        [HttpGet]
        public HttpResponseMessage Get(int portalCountryId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetPortalCountry(portalCountryId, RouteUri, RouteTemplate);
                response = !Equals(data,null) ? CreateOKResponse<PortalCountryResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new PortalCountryResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets a list of portal countries.
        /// </summary>
        /// <returns></returns>
        [ExpandCountry]
        [FilterCountryCode, FilterIsBillingActive, FilterIsShippingActive, FilterPortalId]
        [SortCountryCode, SortPortalCountryId]
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage List()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetPortalCountries(RouteUri, RouteTemplate);
                response = !Equals(data,null) ? CreateOKResponse<PortalCountryListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new PortalCountryListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Creates a new portal country.
        /// </summary>
        /// <param name="model">The model of the portal country.</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Create([FromBody] PortalCountryModel model)
        {
            HttpResponseMessage response;

            try
            {
                var portalCountry = _service.CreatePortalCountry(model);
                if (!Equals(portalCountry,null))
                {
                    var uri = Request.RequestUri;
                    var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + portalCountry.PortalCountryId;

                    response = CreateCreatedResponse(new PortalCountryResponse { PortalCountry = portalCountry });
                    response.Headers.Add("Location", location);
                }
                else
                {
                    response = CreateInternalServerErrorResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new PortalCountryResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Updates an existing portal country.
        /// </summary>
        /// <param name="portalCountryId">The ID of the portal country.</param>
        /// <param name="model">The model of the portal country.</param>
        /// <returns></returns>
        [HttpPut]
        public HttpResponseMessage Update(int portalCountryId, [FromBody] PortalCountryModel model)
        {
            HttpResponseMessage response;

            try
            {
                var portalCountry = _service.UpdatePortalCountry(portalCountryId, model);
                response = !Equals(portalCountry,null) ? CreateOKResponse(new PortalCountryResponse { PortalCountry = portalCountry }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new PortalCountryResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Deletes an existing portal country.
        /// </summary>
        /// <param name="portalCountryId">The ID of the portal country.</param>
        /// <returns></returns>
        [HttpDelete]
        public HttpResponseMessage Delete(int portalCountryId)
        {
            HttpResponseMessage response;

            try
            {
                var deleted = _service.DeletePortalCountry(portalCountryId);
                response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new PortalCountryResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        } 
        #endregion

        #region ZNode version 8.0

        /// <summary>
        /// Znode Version 8.0
        /// Get all Portal Countries
        /// </summary>
        /// <returns></returns>
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage GetAllPortalCountries()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetAllPortalCountries(RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<PortalCountryListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new PortalCountryListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Znode Version 8.0
        /// Deletes an existing portal country if it is not assciated with any addres.
        /// </summary>
        /// <param name="portalCountryId">The ID of the portal country.</param>
        /// <returns>Http Response.</returns>
        [HttpDelete]
        public HttpResponseMessage DeleteIfNotAssociated(int portalCountryId)
        {
            HttpResponseMessage response;

            try
            {
                var deleted = _service.DeletePortalCountryIfNotAssociated(portalCountryId);
                response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new PortalCountryResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Znode Version 8.0
        /// Creates a portal country if not exists.
        /// </summary>
        /// <param name="portalCountry">Portal Country Model.</param>
        /// <returns>Http Response.</returns>
        [HttpGet]
        public HttpResponseMessage CreatePortalCountries(int portalId,string shipableCountryCodes, string billableCountryCodes)
        {
            HttpResponseMessage response;

            try
            {
                bool isPortalCountryAdded = _service.CreatePortalCountries(portalId,billableCountryCodes,shipableCountryCodes);

                var uri = Request.RequestUri;
                var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + portalId;

                response = CreateCreatedResponse(new PortalCountryResponse { IsPortalCountryAdded = isPortalCountryAdded });
                response.Headers.Add("Location", location);
            }
            catch (Exception ex)
            {
                var data = new ContentPageResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
        #endregion

        
	}
}