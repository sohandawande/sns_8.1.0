﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;
namespace Znode.Engine.Api.Controllers
{
    public class ProductReviewHistoryController : BaseController
    {
        #region Private Variables
        private readonly IProductReviewHistoryService _service;
        private readonly IProductReviewHistoryCache _cache;
        #endregion

        #region Constructor
        public ProductReviewHistoryController()
        {
            _service = new ProductReviewHistoryService();
            _cache = new ProductReviewHistoryCache(_service);
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// To Get Product Review history List.
        /// </summary>
        /// <returns></returns>
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage List()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetProductReviewHistory(RouteUri, RouteTemplate);
                response = (!Equals(data, null)) ? CreateOKResponse<VendorProductListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new VendorProductListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }


        /// <summary>
        /// Get a Producct review history details.     
        /// </summary>
        /// <param name="productReviewHistoryID">The ID of the Product Review History.</param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage Get(int productReviewHistoryID)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetProductReviewHistoryById(productReviewHistoryID, RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<ProductReviewHistoryResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductReviewHistoryResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
        #endregion

    }
}
