﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;
using Znode.Engine.Services;


namespace Znode.Engine.Api.Controllers
{
    public class ReorderOrderLineItemController : BaseController
    {
        private readonly IReorderService _service;
        private readonly IReorderCache _cache;

        public ReorderOrderLineItemController()
        {
            _service = new ReorderService();
            _cache = new ReorderCache(_service);
        }

        #region Znode Version 7.2.2

        /// <summary>
        /// Znode Version 7.2.2
        ///Get reorder single item.
        ///This function helps to reorder single items from the order
        /// </summary>
        /// <param name="orderLineItemIds">orderLineItemIds</param>
        /// <returns>returns HttpResponseMessage</returns>
        [ExpandAccountType, ExpandAddresses, ExpandOrders, ExpandProfiles, ExpandUser, ExpandWishList]
        [HttpGet]
        public HttpResponseMessage GetReorderLineItem(int orderLineItemIds)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetReorderLineItem(orderLineItemIds, RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<AccountResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new AccountResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        #endregion
    }
}
