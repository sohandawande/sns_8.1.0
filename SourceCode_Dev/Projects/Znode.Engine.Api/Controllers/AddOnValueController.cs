﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    public class AddOnValueController : BaseController
    {
        #region Private Variables
        private readonly IAddOnValueService _service;
        private readonly IAddOnValueCache _cache;
        #endregion

        #region Constructor
        public AddOnValueController()
        {
            _service = new AddOnValueService();
            _cache = new AddOnValueCache(_service);
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Get a Add-OnValue.     
        /// </summary>
        /// <param name="addOnId">The ID of the Add-OnValue.</param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage Get(int addOnValueId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetAddOnValue(addOnValueId, RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<AddOnValueResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new AddOnValueResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Creates a new Add-OnValues.
        /// </summary>
        /// <param name="model">The model of the Add-OnValues.</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Create([FromBody] AddOnValueModel model)
        {
            HttpResponseMessage response;

            try
            {
                var addOnValue = _service.CreateAddOnValue(model);
                if (!Equals(addOnValue, null))
                {
                    var uri = Request.RequestUri;
                    var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + addOnValue.AddOnValueId;

                    response = CreateCreatedResponse(new AddOnValueResponse { AddOnValue = addOnValue });
                    response.Headers.Add("Location", location);
                }
                else
                {
                    response = CreateInternalServerErrorResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new AddOnValueResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        /// <summary>
        /// Updates an existing Add-OnValue.
        /// </summary>
        /// <param name="addOnId">The ID of the Add-OnValue.</param>
        /// <param name="model">The model of the Add-OnValue.</param>
        /// <returns></returns>
        [HttpPut]
        public HttpResponseMessage Update(int addOnValueId, [FromBody] AddOnValueModel model)
        {
            HttpResponseMessage response;

            try
            {
                var addOnValue = _service.UpdateAddOnValue(addOnValueId, model);
                response = addOnValue != null ? CreateOKResponse(new AddOnValueResponse { AddOnValue = addOnValue }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new AddOnValueResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Deletes an existing Add-OnValue.
        /// </summary>
        /// <param name="addOnId">The ID of the Add-OnValue.</param>
        /// <returns></returns>
        [HttpDelete]
        public HttpResponseMessage Delete(int addOnValueId)
        {
            HttpResponseMessage response;

            try
            {
                var deleted = _service.DeleteAddOnValue(addOnValueId);
                response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new AddOnValueResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Get list of AddOn Values
        /// </summary>
        /// <param name="addOnId">int addOnId</param>
        /// <param name="totalRowCount">int totalRowCount</param>
        /// <returns></returns>
        [PageIndex, PageSize]
        [SortCategoryId]
        [HttpGet]
        public HttpResponseMessage GetAddOnValueByAddOnId()
        {
            HttpResponseMessage response;
            try
            {
                var addOnValues = _cache.GetAddOnValueByAddOnId(RouteUri, RouteTemplate);
                response = !Equals(addOnValues, null) ? CreateOKResponse<AddOnListResponse>(addOnValues) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new AddOnValueResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }
        #endregion
    }
}
