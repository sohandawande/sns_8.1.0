﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
	public class TaxRuleTypesController : BaseController
	{
		private readonly ITaxRuleTypeService _service;
		private readonly ITaxRuleTypeCache _cache;

		public TaxRuleTypesController()
		{
			_service = new TaxRuleTypeService();
			_cache = new TaxRuleTypeCache(_service);
		}

		/// <summary>
		/// Gets a tax rule type.
		/// </summary>
		/// <param name="taxRuleTypeId">The ID of the tax rule type.</param>
		/// <returns></returns>
		[HttpGet]
		public HttpResponseMessage Get(int taxRuleTypeId)
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetTaxRuleType(taxRuleTypeId, RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<TaxRuleTypeResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new TaxRuleTypeResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Gets a list of tax rule types.
		/// </summary>
		/// <returns></returns>
		[FilterClassName, FilterIsActive, FilterName, FilterPortalId]
		[SortClassName, SortName, SortTaxRuleTypeId]
		[PageIndex, PageSize]
		[HttpGet]
		public HttpResponseMessage List()
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetTaxRuleTypes(RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<TaxRuleTypeListResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new TaxRuleTypeListResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Creates a new tax rule type.
		/// </summary>
		/// <param name="model">The model of the tax rule type.</param>
		/// <returns></returns>
		[HttpPost]
		public HttpResponseMessage Create([FromBody] TaxRuleTypeModel model)
		{
			HttpResponseMessage response;

			try
			{
				var taxRuleType = _service.CreateTaxRuleType(model);
				if (taxRuleType != null)
				{
					var uri = Request.RequestUri;
					var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + taxRuleType.TaxRuleTypeId;

					response = CreateCreatedResponse(new TaxRuleTypeResponse { TaxRuleType = taxRuleType });
					response.Headers.Add("Location", location);
				}
				else
				{
					response = CreateInternalServerErrorResponse();
				}
			}
			catch (Exception ex)
			{
				var data = new TaxRuleTypeResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Updates an existing tax rule type.
		/// </summary>
		/// <param name="taxRuleTypeId">The ID of the tax rule type.</param>
		/// <param name="model">The model of the tax rule type.</param>
		/// <returns></returns>
		[HttpPut]
		public HttpResponseMessage Update(int taxRuleTypeId, [FromBody] TaxRuleTypeModel model)
		{
			HttpResponseMessage response;

			try
			{
				var taxRuleType = _service.UpdateTaxRuleType(taxRuleTypeId, model);
				response = taxRuleType != null ? CreateOKResponse(new TaxRuleTypeResponse { TaxRuleType = taxRuleType }) : CreateInternalServerErrorResponse();
			}
			catch (Exception ex)
			{
				var data = new TaxRuleTypeResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Deletes an existing tax rule type.
		/// </summary>
		/// <param name="taxRuleTypeId">The ID of the tax rule type.</param>
		/// <returns></returns>
		[HttpDelete]
		public HttpResponseMessage Delete(int taxRuleTypeId)
		{
			HttpResponseMessage response;

			try
			{
				var deleted = _service.DeleteTaxRuleType(taxRuleTypeId);
				response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
			}
			catch (Exception ex)
			{
				var data = new TaxRuleTypeResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

        #region Znode Version 8.0
        /// <summary>
        /// Get All Tax Rule Types not in database
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetAllTaxRuleTypesNotInDatabase()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetAllTaxRuleTypesNotInDatabase(RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<TaxRuleTypeListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new TaxRuleTypeListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
        #endregion
	}
}