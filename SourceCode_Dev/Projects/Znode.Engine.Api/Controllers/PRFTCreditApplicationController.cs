﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    public class PRFTCreditApplicationController : BaseController
    {
        private readonly IPRFTCreditApplicationService _service;
        private readonly IPRFTCreditApplicationCache _cache;

        public PRFTCreditApplicationController()
        {
            _service = new PRFTCreditApplicationService();
            _cache = new PRFTCreditApplicationCache(_service);
        }

        /// <summary>
        /// Gets a credit Application.
        /// </summary>
        /// <param name="caseRequestId">The ID of the Credit Application.</param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage Get(int creditApplicationId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetCreditApplication(creditApplicationId, RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<PRFTCreditApplicationResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new PRFTCreditApplicationResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets a list of Credit Application.
        /// </summary>
        /// <returns></returns>
        [FilterIsCreditApproved, FilterBusinessName, FilterCreditApplicationID]
        [SortCreditApplicationID, SortBusinessName, SortIsTaxExempt, SortRequestDate, SortIsCreditApproved, SortCreditDays]
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage List()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetCreditApplications(RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<PRFTCreditApplicationListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new PRFTCreditApplicationListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }


        /// <summary>
        /// Creates a new credit application.
        /// </summary>
        /// <param name="model">The model of the credit application.</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Create([FromBody] PRFTCreditApplicationModel model)
        {
            HttpResponseMessage response;

            try
            {
                var creditApplication = _service.CreateCreditApplication(model);
                if (creditApplication != null)
                {
                    var uri = Request.RequestUri;
                    var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + creditApplication.CreditApplicationID;

                    response = CreateCreatedResponse(new PRFTCreditApplicationResponse { CreditApplication = creditApplication });
                    response.Headers.Add("Location", location);
                }
                else
                {
                    response = CreateInternalServerErrorResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new CaseRequestResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Updates an existing credit application.
        /// </summary>
        /// <param name="caseRequestId">The ID of the credit application.</param>
        /// <param name="model">The model of the credit application.</param>
        /// <returns></returns>
        [HttpPut]
        public HttpResponseMessage Update(int creditApplicationId, [FromBody] PRFTCreditApplicationModel model)
        {
            HttpResponseMessage response;

            try
            {
                var creditApplication = _service.UpdateCreditRequest(creditApplicationId, model);
                response = creditApplication != null ? CreateOKResponse(new PRFTCreditApplicationResponse {CreditApplication  = creditApplication }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new PRFTCreditApplicationResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

	}
}