﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    public class RMARequestController : BaseController
    {
        #region Private Variables
        private readonly IRMARequestCache _cache;
        private readonly IRMARequestService _service;
        #endregion

        #region Constructor
        public RMARequestController()
        {
            _service = new RMARequestService();
            _cache = new RMARequestCache(_service);
        }
        #endregion

        #region Public Action Methods
        /// <summary>
        /// Gets a list of RMA requests.
        /// </summary>
        /// <returns>List of RMA request.</returns> 
        [PageIndex]
        [PageSize]
        [HttpGet]
        public HttpResponseMessage GetRMARequestList()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetRMARequests(RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<RMARequestListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new RMARequestListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        /// <summary>
        /// Gets RMA request.
        /// </summary>
        /// <param name="rmaRequestId">RMA Request ID.</param>
        /// <returns>HTTP Response for RMA request.</returns>
        [HttpGet]
        public HttpResponseMessage GetRMARequest(int rmaRequestId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetRMARequest(rmaRequestId, RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<RMARequestResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new RMARequestResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Updates an existing RMA Request.
        /// </summary>
        /// <param name="portalId">The ID of the RMA Request.</param>
        /// <param name="model">The model of the RMA Request.</param>
        /// <returns></returns>
        [HttpPut]
        public HttpResponseMessage Update(int rmaRequestId, [FromBody] RMARequestModel model)
        {
            HttpResponseMessage response;

            try
            {
                var rmaRequest = _service.UpdateRMARequest(rmaRequestId, model);
                response = !Equals(rmaRequest, null) ? CreateOKResponse(new RMARequestResponse { RMARequest = rmaRequest }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new RMARequestResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Deletes an existing RMA request.
        /// </summary>
        /// <param name="reviewId">The ID of the RMA Request.</param>
        /// <returns></returns>
        [HttpDelete]
        public HttpResponseMessage Delete(int rmaRequestId)
        {
            HttpResponseMessage response;

            try
            {
                var deleted = _service.DeleteRMARequest(rmaRequestId);
                response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new RMARequestResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets RMA Request Gift card details.
        /// </summary> 
        /// <param name="rmaRequestId">RMA Request ID for which issued gift cards will be fetched.</param>
        /// <returns>Returns RMA Request Gift card details</returns>
        [HttpGet]
        public HttpResponseMessage GetRMARequestGiftCardDetails(int rmaRequestId)
        {
            HttpResponseMessage response;
            try
            {
                string data = _cache.GetIssuedGiftCards(rmaRequestId, RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<IssuedGiftCardListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                IssuedGiftCardListResponse data = new IssuedGiftCardListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        /// <summary>
        /// Creates a new RMA Request.
        /// </summary>
        /// <param name="model">The model of the RMA Request.</param>
        /// <returns>RMA Request model.</returns>
        [HttpPost]
        public HttpResponseMessage Create([FromBody] RMARequestModel model)
        {
            HttpResponseMessage response;

            try
            {
                var rmaRequest = _service.CreateRMARequest(model);
                if (!Equals(rmaRequest, null))
                {
                    var uri = Request.RequestUri;
                    var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + rmaRequest.RMARequestID;

                    response = CreateCreatedResponse(new RMARequestResponse { RMARequest = rmaRequest });
                    response.Headers.Add("Location", location);
                }
                else
                {
                    response = CreateInternalServerErrorResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new PortalResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// To Get Order RMA Display Flag by Order Id.
        /// </summary>
        /// <param name="OrderId">Order Id to Get RMA display Flag.</param>        
        /// <returns>Returns true if Order RMA  is 1 else returns false.</returns>
        [HttpGet]
        public HttpResponseMessage GetOrderRMAFlag(int orderId)
        {
            HttpResponseMessage response;

            try
            {
                string resp = _cache.GetOrderRMAFlag(orderId, RouteUri, RouteTemplate);
                response = !string.IsNullOrEmpty(resp) ? CreateOKResponse<TrueFalseResponse>(resp) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new RMARequestResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Sends Mail to Customer
        /// </summary>
        /// <param name="rmaRequestId">RMA Request id according to which mail is sent or not.</param>
        /// <returns>HttpResponseMessage containing bool value according to email sent status.</returns>
        [HttpGet]
        public HttpResponseMessage SendStatusMail(int rmaRequestId)
        {
            HttpResponseMessage response;

            try
            {
                bool isStatusMailSent = _service.SendStatusMail(rmaRequestId);

                BooleanModel boolModel = new BooleanModel();
                boolModel.disabled = isStatusMailSent;

                TrueFalseResponse resp = new TrueFalseResponse();
                resp.booleanModel = boolModel;

                response = CreateOKResponse(resp);
            }
            catch (Exception ex)
            {
                TrueFalseResponse data = new TrueFalseResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Send Gift card mail to customer.
        /// </summary>
        /// <param name="model">Gift card model from where email data will be retrieved.</param>
        /// <returns>HTTP Response message containg bool value if the mail is sent or not.</returns>
        [HttpPost]
        public HttpResponseMessage SendGiftCardMail([FromBody] GiftCardModel model)
        {
            HttpResponseMessage response;

            try
            {
                BooleanModel boolModel = new BooleanModel();
                boolModel.disabled = _service.SendMail_Giftcard(model);

                TrueFalseResponse resp = new TrueFalseResponse();
                resp.booleanModel = boolModel;

                response = CreateOKResponse(resp);
            }
            catch (Exception ex)
            {
                TrueFalseResponse data = new TrueFalseResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
        #endregion
    }
}
