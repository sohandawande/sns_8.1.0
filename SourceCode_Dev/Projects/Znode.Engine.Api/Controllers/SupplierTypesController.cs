﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
	public class SupplierTypesController : BaseController
	{
		private readonly ISupplierTypeService _service;
		private readonly ISupplierTypeCache _cache;

		public SupplierTypesController()
		{
			_service = new SupplierTypeService();
			_cache = new SupplierTypeCache(_service);
		}

		/// <summary>
		/// Gets a supplier type.
		/// </summary>
		/// <param name="supplierTypeId">The ID of the supplier type.</param>
		/// <returns></returns>
		[HttpGet]
		public HttpResponseMessage Get(int supplierTypeId)
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetSupplierType(supplierTypeId, RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<SupplierTypeResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new SupplierTypeResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Gets a list of supplier types.
		/// </summary>
		/// <returns></returns>
		[FilterClassName, FilterIsActive, FilterName]
		[SortClassName, SortName, SortSupplierTypeId]
		[PageIndex, PageSize]
		[HttpGet]
		public HttpResponseMessage List()
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetSupplierTypes(RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<SupplierTypeListResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new SupplierTypeListResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Creates a new supplier type.
		/// </summary>
		/// <param name="model">The model of the supplier type.</param>
		/// <returns></returns>
		[HttpPost]
		public HttpResponseMessage Create([FromBody] SupplierTypeModel model)
		{
			HttpResponseMessage response;

			try
			{
				var supplierType = _service.CreateSupplierType(model);
				if (supplierType != null)
				{
					var uri = Request.RequestUri;
					var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + supplierType.SupplierTypeId;

					response = CreateCreatedResponse(new SupplierTypeResponse { SupplierType = supplierType });
					response.Headers.Add("Location", location);
				}
				else
				{
					response = CreateInternalServerErrorResponse();
				}
			}
			catch (Exception ex)
			{
				var data = new SupplierTypeResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Updates an existing supplier type.
		/// </summary>
		/// <param name="supplierTypeId">The ID of the supplier type.</param>
		/// <param name="model">The model of the supplier type.</param>
		/// <returns></returns>
		[HttpPut]
		public HttpResponseMessage Update(int supplierTypeId, [FromBody] SupplierTypeModel model)
		{
			HttpResponseMessage response;

			try
			{
				var supplierType = _service.UpdateSupplierType(supplierTypeId, model);
				response = supplierType != null ? CreateOKResponse(new SupplierTypeResponse { SupplierType = supplierType }) : CreateInternalServerErrorResponse();
			}
			catch (Exception ex)
			{
				var data = new SupplierTypeResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Deletes an existing supplier type.
		/// </summary>
		/// <param name="supplierTypeId">The ID of the supplier type.</param>
		/// <returns></returns>
		[HttpDelete]
		public HttpResponseMessage Delete(int supplierTypeId)
		{
			HttpResponseMessage response;

			try
			{
				var deleted = _service.DeleteSupplierType(supplierTypeId);
				response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
			}
			catch (Exception ex)
			{
				var data = new SupplierTypeResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

        #region Znode Version 8.0
        /// <summary>
        /// Get All Supplier Types not in database
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetAllSupplierTypesNotInDatabase()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetAllSupplierTypesNotInDatabase(RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<SupplierTypeListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new SupplierTypeListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
        #endregion

	}
}