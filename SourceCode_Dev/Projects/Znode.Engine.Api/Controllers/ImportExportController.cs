﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;
using Znode.Engine.Services;
using ZNode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.Api.Controllers
{
    public class ImportExportController : BaseController
    {

        private readonly IImportExportService _service;

        public ImportExportController()
		{
            _service = new ImportExportService();
		}


        /// <summary>
        /// Get the Exports Details.
        /// </summary>
        /// <param name="model">The model of the Export.</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage GetExports([FromBody] ExportModel model)
        {
            HttpResponseMessage response;
            try
            {
                var data = _service.GetExports(model);
                response = !Equals(data, null) ? CreateOKResponse(new ImportExportResponse() { Exports=data}) : CreateNotFoundResponse();
            }
            catch (ZnodeException ex)
            {
                var data = new ImportExportResponse { HasError = true, ErrorCode = ex.ErrorCode, ErrorMessage = ex.ErrorMessage };
                response = CreateInternalServerErrorResponse(data);
            }
            catch (Exception ex)
            {
                var data = new ImportExportResponse { HasError = true, ErrorMessage = "Failed to process your request. Please try again." };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }
         /// <summary>
        /// Get the Exports Details.
        /// </summary>
        /// <param name="model">The model of the Export.</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage ImportsData([FromBody] ImportModel model)
        {
            HttpResponseMessage response;
            try
            {
                //ZNodeLogging.LogMessage("Step4 - ImportExportController In API  - ImportsData() is started.");
                var data = _service.ImportData(model);
                //ZNodeLogging.LogMessage("Step24 - ImportExportController In API  - ImportsData() is end. Status = "+data.Status);
                response = !Equals(data, null) ? CreateOKResponse(new ImportExportResponse() { Imports = data}) : CreateNotFoundResponse();
            }
            catch (ZnodeException ex)
            {
                
                var data = new ImportExportResponse { HasError = true, ErrorCode = ex.ErrorCode, ErrorMessage = ex.ErrorMessage };
                response = CreateInternalServerErrorResponse(data);
                ZNodeLogging.LogMessage("Exception in ImportExportController In API  - ImportsData() znode exception. ex = " + ex.ToString());
            }
            catch (Exception ex)
            {

                var data = new ImportExportResponse { HasError = true, ErrorMessage = "Failed to process your request. Please try again." };
                response = CreateInternalServerErrorResponse(data);
                ZNodeLogging.LogMessage("Step5 - ImportExportController In API  - ImportsData() znode exception. ex = " + ex.ToString());
            }
            return response;
        }


       
        /// <summary>
		/// Gets Import Default Details.
		/// </summary>
        /// <param name="type">Type of the Import.</param>
		/// <returns>Return the Import Default Details</returns>
		[HttpGet]
        public HttpResponseMessage GetImportDetails(string type)
		{
			HttpResponseMessage response;

			try
			{
                var data = _service.GetImportDetails(type);
                response = !Equals(data, null) ? CreateOKResponse(new ImportExportResponse() { Imports = data }) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
                var data = new ImportExportResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}
        
    }
}
