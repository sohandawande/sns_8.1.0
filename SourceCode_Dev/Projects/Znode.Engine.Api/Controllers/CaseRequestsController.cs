﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    public class CaseRequestsController : BaseController
    {
        private readonly ICaseRequestService _service;
        private readonly ICaseRequestCache _cache;

        public CaseRequestsController()
        {
            _service = new CaseRequestService();
            _cache = new CaseRequestCache(_service);
        }

        /// <summary>
        /// Gets a case request.
        /// </summary>
        /// <param name="caseRequestId">The ID of the case request.</param>
        /// <returns></returns>
        [ExpandCasePriority, ExpandCaseStatus, ExpandCaseType]
        [HttpGet]
        public HttpResponseMessage Get(int caseRequestId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetCaseRequest(caseRequestId, RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<CaseRequestResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new CaseRequestResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets a list of case requests.
        /// </summary>
        /// <returns></returns>
        [ExpandCasePriority, ExpandCaseStatus, ExpandCaseType]
        [FilterAccountId, FilterCasePriorityId, FilterCaseStatusId, FilterCaseTypeId, FilterCreateDate, FilterEmail, FilterLastName, FilterOwnerAccountId, FilterPortalId]
        [SortCasePriorityId, SortCaseRequestId, SortCaseStatusId, SortCaseTypeId, SortCreateDate, SortLastName]
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage List()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetCaseRequests(RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<CaseRequestListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new CaseRequestListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Creates a new case request.
        /// </summary>
        /// <param name="model">The model of the case request.</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Create([FromBody] CaseRequestModel model)
        {
            HttpResponseMessage response;

            try
            {
                var caseRequest = _service.CreateCaseRequest(model);
                if (caseRequest != null)
                {
                    var uri = Request.RequestUri;
                    var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + caseRequest.CaseRequestId;

                    response = CreateCreatedResponse(new CaseRequestResponse { CaseRequest = caseRequest });
                    response.Headers.Add("Location", location);
                }
                else
                {
                    response = CreateInternalServerErrorResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new CaseRequestResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Updates an existing case request.
        /// </summary>
        /// <param name="caseRequestId">The ID of the case request.</param>
        /// <param name="model">The model of the case request.</param>
        /// <returns></returns>
        [HttpPut]
        public HttpResponseMessage Update(int caseRequestId, [FromBody] CaseRequestModel model)
        {
            HttpResponseMessage response;

            try
            {
                var caseRequest = _service.UpdateCaseRequest(caseRequestId, model);
                response = caseRequest != null ? CreateOKResponse(new CaseRequestResponse { CaseRequest = caseRequest }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new CaseRequestResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Deletes an existing case request.
        /// </summary>
        /// <param name="caseRequestId">The ID of the case request.</param>
        /// <returns></returns>
        [HttpDelete]
        public HttpResponseMessage Delete(int caseRequestId)
        {
            HttpResponseMessage response;

            try
            {
                var deleted = _service.DeleteCaseRequest(caseRequestId);
                response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new CaseRequestResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        #region Znode Version 8.0

        /// <summary>
        /// To add Case Note
        /// </summary>
        /// <param name="model">NoteModel model</param>
        /// <returns>returns NoteModel</returns>
        [HttpPost]
        public HttpResponseMessage CreateCaseNote([FromBody] NoteModel model)
        {
            HttpResponseMessage response;

            try
            {
                var caseNote = _service.CreateCaseNote(model);
                if (caseNote != null)
                {
                    var uri = Request.RequestUri;
                    var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + caseNote.NoteId;

                    response = CreateCreatedResponse(new CaseNoteResponse { CaseNote = caseNote });
                    response.Headers.Add("Location", location);
                }
                else
                {
                    response = CreateInternalServerErrorResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new CaseNoteResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }


        /// <summary>
        /// To get Case Status List
        /// </summary>
        /// <returns>returns Case Status List</returns>
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage CaseStatusList()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetCaseStatus(RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<CaseStatusListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new CaseStatusListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// to get Case Priority List
        /// </summary>
        /// <returns>returns Case Priority List</returns>
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage CasePriorityList()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetCasePriority(RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<CasePrioritiesListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new CasePrioritiesListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        ///  to get CaseRequestList
        /// </summary>
        /// <returns>returns CaseRequestList</returns>
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage CaseNoteList()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetCaseNote(RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<CaseNoteListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new CaseNoteListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        #endregion
    }
}
