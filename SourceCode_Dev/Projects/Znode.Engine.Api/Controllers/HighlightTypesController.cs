﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
	public class HighlightTypesController : BaseController
	{
		private readonly IHighlightTypeService _service;
		private readonly IHighlightTypeCache _cache;

		public HighlightTypesController()
		{
			_service = new HighlightTypeService();
			_cache = new HighlightTypeCache(_service);
		}

		/// <summary>
		/// Gets a highlight type.
		/// </summary>
		/// <param name="highlightTypeId">The ID of the highlight type.</param>
		/// <returns></returns>
		[HttpGet]
		public HttpResponseMessage Get(int highlightTypeId)
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetHighlightType(highlightTypeId, RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<HighlightTypeResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new HighlightTypeResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Gets a list of highlight types.
		/// </summary>
		/// <returns></returns>
		[FilterName]
		[SortHighlightTypeId, SortName]
		[PageIndex, PageSize]
		[HttpGet]
		public HttpResponseMessage List()
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetHighlightTypes(RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<HighlightTypeListResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new HighlightTypeListResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Creates a new highlight type.
		/// </summary>
		/// <param name="model">The model of the highlight type.</param>
		/// <returns></returns>
		[HttpPost]
		public HttpResponseMessage Create([FromBody] HighlightTypeModel model)
		{
			HttpResponseMessage response;

			try
			{
				var highlightType = _service.CreateHighlightType(model);
				if (highlightType != null)
				{
					var uri = Request.RequestUri;
					var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + highlightType.HighlightTypeId;

					response = CreateCreatedResponse(new HighlightTypeResponse { HighlightType = highlightType });
					response.Headers.Add("Location", location);
				}
				else
				{
					response = CreateInternalServerErrorResponse();
				}
			}
			catch (Exception ex)
			{
				var data = new HighlightTypeResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Updates an existing highlight type.
		/// </summary>
		/// <param name="highlightTypeId">The ID of the highlight type.</param>
		/// <param name="model">The model of the highlight type.</param>
		/// <returns></returns>
		[HttpPut]
		public HttpResponseMessage Update(int highlightTypeId, [FromBody] HighlightTypeModel model)
		{
			HttpResponseMessage response;

			try
			{
				var highlightType = _service.UpdateHighlightType(highlightTypeId, model);
				response = highlightType != null ? CreateOKResponse(new HighlightTypeResponse { HighlightType = highlightType }) : CreateInternalServerErrorResponse();
			}
			catch (Exception ex)
			{
				var data = new HighlightTypeResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Deletes an existing highlight type.
		/// </summary>
		/// <param name="highlightTypeId">The ID of the highlight type.</param>
		/// <returns></returns>
		[HttpDelete]
		public HttpResponseMessage Delete(int highlightTypeId)
		{
			HttpResponseMessage response;

			try
			{
				var deleted = _service.DeleteHighlightType(highlightTypeId);
				response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
			}
			catch (Exception ex)
			{
				var data = new HighlightTypeResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}
	}
}