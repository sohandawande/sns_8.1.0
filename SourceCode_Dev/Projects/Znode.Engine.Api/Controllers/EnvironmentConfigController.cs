﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    public class EnvironmentConfigController : BaseController
    {
        #region Private Variables
        private readonly IEnvironmentConfigCache _cache;
        private readonly IEnvironmentConfigService _service; 
        #endregion

        #region Constructor
        public EnvironmentConfigController()
        {
            _service = new EnvironmentConfigService();
            _cache = new EnvironmentConfigCache(_service);
        } 
        #endregion

        #region Public Action Methods
        /// <summary>
        /// Gets the EnvironmentConfig Settings.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage Get()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetEnvironmentConfig(RouteUri, RouteTemplate);
                response = !Equals(data,null) ? CreateOKResponse<EnvironmentConfigResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new EnvironmentConfigResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        } 
        #endregion

    }
}
