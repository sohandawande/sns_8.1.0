﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    public class CurrencyTypeController : BaseController
    {
        #region Private Variables
        private readonly ICurrencyTypeService _service;
        private readonly ICurrencyTypeCache _cache; 
        #endregion

        #region Constructor
        public CurrencyTypeController()
        {
            _service = new CurrencyTypeService();
            _cache = new CurrencyTypeCache(_service);
        } 
        #endregion

        #region Public Action Methods
        /// <summary>
        /// Gets a list of CurrencyType.
        /// </summary>
        /// <returns>List of CurrencyType</returns>       
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage List()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetCurrencyTypes(RouteUri, RouteTemplate);
                response = !Equals(data,null) ? CreateOKResponse<CurrencyTypeListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new CurrencyTypeListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets a Currency type.
        /// </summary>
        /// <param name="portalId">Currency Type Id</param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage Get(int currencyTypeId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetCurrencyType(currencyTypeId, RouteUri, RouteTemplate);
                response = (!Equals(data, null)) ? CreateOKResponse<CurrencyTypeResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new CurrencyTypeResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Updates an existing Currency Type.
        /// </summary>
        /// <param name="currencyTypeID">The ID of the currency Type.</param>
        /// <param name="model">The model of the currency type.</param>
        /// <returns></returns>
        [HttpPut]
        public HttpResponseMessage Update(int currencyTypeId, [FromBody] CurrencyTypeModel model)
        {
            HttpResponseMessage response;

            try
            {
                var currencyType = _service.UpdateCurrencyType(currencyTypeId, model);
                response = !Equals(currencyType, null) ? CreateOKResponse(new CurrencyTypeResponse { CurrencyType = currencyType }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new CurrencyTypeResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        } 
        #endregion
    }
}
