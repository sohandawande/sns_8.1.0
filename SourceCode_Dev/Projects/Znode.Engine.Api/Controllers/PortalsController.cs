﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    public class PortalsController : BaseController
    {
        #region Private Variables
        private readonly IPortalService _service;
        private readonly IPortalCache _cache; 
        #endregion

        #region Constructor
        public PortalsController()
        {
            _service = new PortalService();
            _cache = new PortalCache(_service);
        } 
        #endregion

        #region Public Action Method
        /// <summary>
        /// Gets a portal.
        /// </summary>
        /// <param name="portalId">The ID of the portal.</param>
        /// <returns></returns>
        [ExpandPortalCountries]
        [ExpandCatalogs]
        [HttpGet]
        public HttpResponseMessage Get(int portalId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetPortal(portalId, RouteUri, RouteTemplate);
                if (!Equals(data,null))
                {
                    response = CreateOKResponse<PortalResponse>(data);
                }
                else
                {
                    response = CreateNotFoundResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new PortalResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets a list of portals.
        /// </summary>
        /// <returns></returns>
        [ExpandCatalogs, ExpandPortalCountries]
        [FilterCompanyName, FilterStoreName, FilterExternalId, FilterIsActive, FilterLocaleId]
        [SortCompanyName, SortPortalId, SortStoreName]
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage List()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetPortals(RouteUri, RouteTemplate);
                response = !Equals(data ,null) ? CreateOKResponse<PortalListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new PortalListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets a list of portals for the specified portal IDs.
        /// </summary>
        /// <param name="portalIds">The comma-separated IDs of the portals.</param>
        /// <returns></returns>
        [ExpandCatalogs, ExpandPortalCountries]
        [SortCompanyName, SortPortalId, SortStoreName]
        [HttpGet]
        public HttpResponseMessage ListByPortalIds(string portalIds)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetPortalsByPortalIds(portalIds, RouteUri, RouteTemplate);
                response = !Equals(data,null) ? CreateOKResponse<PortalListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new PortalListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Creates a new portal.
        /// </summary>
        /// <param name="model">The model of the portal.</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Create([FromBody] PortalModel model)
        {
            HttpResponseMessage response;

            try
            {
                var portal = _service.CreatePortal(model);
                if (!Equals(portal,null))
                {
                    var uri = Request.RequestUri;
                    var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + portal.PortalId;

                    response = CreateCreatedResponse(new PortalResponse { Portal = portal });
                    response.Headers.Add("Location", location);
                }
                else
                {
                    response = CreateInternalServerErrorResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new PortalResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Updates an existing portal.
        /// </summary>
        /// <param name="portalId">The ID of the portal.</param>
        /// <param name="model">The model of the portal.</param>
        /// <returns></returns>
        [HttpPut]
        public HttpResponseMessage Update(int portalId, [FromBody] PortalModel model)
        {
            HttpResponseMessage response;

            try
            {
                var portal = _service.UpdatePortal(portalId, model);
                response = !Equals(portal,null) ? CreateOKResponse(new PortalResponse { Portal = portal }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new PortalResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Deletes an existing portal.
        /// </summary>
        /// <param name="portalId">The ID of the portal.</param>
        /// <returns></returns>
        [HttpDelete]
        public HttpResponseMessage Delete(int portalId)
        {
            HttpResponseMessage response;

            try
            {
                var deleted = _service.DeletePortal(portalId);
                response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new PortalResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        } 
        #endregion
       
        /// <summary>
        /// Gets Fedex keys for a portal.
        /// </summary>       
        /// <returns>Returns the Fedex keys of the portal.</returns>
        [HttpGet]
        public HttpResponseMessage GetFedexKey()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetFedexKeys(RouteUri, RouteTemplate);
                response = !Equals(data,null) ? CreateOKResponse<FedexKeysResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new FedexKeysResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Creates messages for the newly created portal.
        /// </summary>
        /// <param name="portalId">Portal Id of portal</param>
        /// <param name="localeId">Locale id for the portl.</param>
        /// <returns>HttpResponsMessage.</returns>
        [HttpGet]
        public HttpResponseMessage CreateMessage(int portalId, int localeId)
        {
            HttpResponseMessage response;

            try
            {
               var isMessagesCreated = _service.CreateMessages(portalId, localeId);

                var uri = Request.RequestUri;
                var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + portalId;

                response = isMessagesCreated ? CreateOKResponse(new PortalResponse { IsMessagesCreated = isMessagesCreated }) : CreateInternalServerErrorResponse();

            }
            catch (Exception ex)
            {

                var data = new PortalResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse();
            }
            return response;
        }

        /// <summary>
        /// Copies a store.
        /// </summary>
        /// <param name="portalId">Portal id of the store to be copied.</param>
        /// <returns>HttpResponseMessage.</returns>
        [HttpGet]
        public HttpResponseMessage CopyStore(int portalId)
        {
            HttpResponseMessage response;

            try
            {
                var isPortalCopied = _service.CopyStore(portalId);

                var uri = Request.RequestUri;
                var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + portalId;

                response = isPortalCopied ? CreateOKResponse(new PortalResponse { IsStoreCopied = isPortalCopied }) : CreateInternalServerErrorResponse();

            }
            catch (Exception ex)
            {

                var data = new PortalResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse();
            }
            return response;
        }

        /// <summary>
        /// Deletes a portal specified by Portal Id.
        /// </summary>
        /// <param name="portalId">Portal id of the portal to be deleted.</param>
        /// <returns>HttpResponseMessage.</returns>
        [HttpDelete]
        public HttpResponseMessage DeleteByPortalId(int portalId)
        {
            HttpResponseMessage response;

            try
            {
                var deleted = _service.DeletePortalByPortalId(portalId);
                response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new PortalResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        [HttpGet]
        public HttpResponseMessage GetPortalInformationByPortalId(int portalId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetPortalInformationByPortalId(portalId, RouteUri, RouteTemplate);
                if (!Equals(data,null))
                {
                    response = CreateOKResponse<PortalResponse>(data);
                }
                else
                {
                    response = CreateNotFoundResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new PortalResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets the list of portal according to profile access.
        /// </summary>
        /// <returns>HTTP response containing list of portals.</returns>
        [HttpGet]        
        public HttpResponseMessage GetPortalListByProfileAccess()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetPortalListByProfileAccess(RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<PortalListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new PortalListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
    }
}