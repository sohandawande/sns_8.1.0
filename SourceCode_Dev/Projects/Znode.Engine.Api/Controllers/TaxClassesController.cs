﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    public class TaxClassesController : BaseController
    {
        private readonly ITaxClassService _service;
        private readonly ITaxClassCache _cache;

        public TaxClassesController()
        {
            _service = new TaxClassService();
            _cache = new TaxClassCache(_service);
        }

        /// <summary>
        /// Gets a tax class.
        /// </summary>
        /// <param name="taxClassId">The ID of the tax class.</param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage Get(int taxClassId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetTaxClass(taxClassId, RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<TaxClassResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new TaxClassResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets a list of tax classes.
        /// </summary>
        /// <returns></returns>
        [FilterExternalId, FilterIsActive, FilterName, FilterPortalId]
        [SortDisplayOrder, SortName, SortTaxClassId]
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage List()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetTaxClasses(RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<TaxClassListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new TaxClassListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets a list of tax classes.
        /// </summary>
        /// <returns></returns>
        [FilterExternalId, FilterIsActive, FilterName, FilterPortalId]
        [SortDisplayOrder, SortName, SortTaxClassId]
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage TaxClassList()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetTaxClassList(RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<TaxClassListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new TaxClassListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Creates a new tax class.
        /// </summary>
        /// <param name="model">The model of the tax class.</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Create([FromBody] TaxClassModel model)
        {
            HttpResponseMessage response;

            try
            {
                var taxClass = _service.CreateTaxClass(model);
                if (!Equals(taxClass, null))
                {
                    var uri = Request.RequestUri;
                    var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + taxClass.TaxClassId;

                    response = CreateCreatedResponse(new TaxClassResponse { TaxClass = taxClass });
                    response.Headers.Add("Location", location);
                }
                else
                {
                    response = CreateInternalServerErrorResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new TaxClassResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Updates an existing tax class.
        /// </summary>
        /// <param name="taxClassId">The ID of the tax class.</param>
        /// <param name="model">The model of the tax class.</param>
        /// <returns></returns>
        [HttpPut]
        public HttpResponseMessage Update(int taxClassId, [FromBody] TaxClassModel model)
        {
            HttpResponseMessage response;

            try
            {
                var taxClass = _service.UpdateTaxClass(taxClassId, model);
                response = taxClass != null ? CreateOKResponse(new TaxClassResponse { TaxClass = taxClass }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new TaxClassResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Deletes an existing tax class.
        /// </summary>
        /// <param name="taxClassId">The ID of the tax class.</param>
        /// <returns></returns>
        [HttpDelete]
        public HttpResponseMessage Delete(int taxClassId)
        {
            HttpResponseMessage response;

            try
            {
                var deleted = _service.DeleteTaxClass(taxClassId);
                response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new TaxClassResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        #region Znode Version 8.0
        /// <summary>
        /// This function used to  Get all active countries for portalId.
        /// </summary>
        /// <param name="portalId">int portalId</param>
        /// <returns>Returns the country list</returns>
        [HttpGet]
        public HttpResponseMessage GetActiveTaxClassByPortalId(int portalId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetActiveTaxClassByPortalId(portalId, RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<TaxClassListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new TaxClassListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets Tax Class Details
        /// </summary>
        /// <returns>Returns Tax Class Details</returns>
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage TaxClassDetails()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetTaxClassDetails(RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<TaxClassResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new TaxClassResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
        #endregion
    }

}