﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    public class CategoryNodesController : BaseController
    {
        private readonly ICategoryNodeService _service;
        private readonly ICategoryNodeCache _cache;

        public CategoryNodesController()
        {
            _service = new CategoryNodeService();
            _cache = new CategoryNodeCache(_service);
        }

        /// <summary>
        /// Gets a category node.
        /// </summary>
        /// <param name="categoryNodeId">The ID of the category node.</param>
        /// <returns></returns>
        [ExpandCatalog, ExpandCategory]
        [HttpGet]
        public HttpResponseMessage Get(int categoryNodeId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetCategoryNode(categoryNodeId, RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<CategoryNodeResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new CategoryNodeResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets a list of category nodes.
        /// </summary>
        /// <returns></returns>
        [ExpandCatalog, ExpandCategory]
        [FilterBeginDate, FilterCatalogId, FilterCategoryId, FilterEndDate, FilterIsActive, FilterParentCategoryNodeId]
        [SortCatalogId, SortCategoryId, SortCategoryNodeId, SortDisplayOrder]
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage List()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetCategoryNodes(RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<CategoryNodeListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new CategoryNodeListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Creates a new category node.
        /// </summary>
        /// <param name="model">The model of the category node.</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Create([FromBody] CategoryNodeModel model)
        {
            HttpResponseMessage response;

            try
            {
                var categoryNode = _service.CreateCategoryNode(model);
                if (categoryNode != null)
                {
                    var uri = Request.RequestUri;
                    var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + categoryNode.CategoryNodeId;

                    response = CreateCreatedResponse(new CategoryNodeResponse { CategoryNode = categoryNode });
                    response.Headers.Add("Location", location);
                }
                else
                {
                    response = CreateInternalServerErrorResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new CategoryNodeResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Updates an existing category node.
        /// </summary>
        /// <param name="categoryNodeId">The ID of the category node.</param>
        /// <param name="model">The model of the category node.</param>
        /// <returns></returns>
        [HttpPut]
        public HttpResponseMessage Update(int categoryNodeId, [FromBody] CategoryNodeModel model)
        {
            HttpResponseMessage response;

            try
            {
                var categoryNode = _service.UpdateCategoryNode(categoryNodeId, model);
                response = categoryNode != null ? CreateOKResponse(new CategoryNodeResponse { CategoryNode = categoryNode }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new CategoryNodeResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Deletes an existing category node.
        /// </summary>
        /// <param name="categoryNodeId">The ID of the category node.</param>
        /// <returns></returns>
        [HttpDelete]
        public HttpResponseMessage Delete(int categoryNodeId)
        {
            HttpResponseMessage response;

            try
            {
                var deleted = _service.DeleteCategoryNode(categoryNodeId);
                response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new CategoryNodeResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Deletes an existing category node.
        /// </summary>
        /// <param name="categoryNodeId">The ID of the category node.</param>
        /// <returns></returns>
        [HttpDelete]
        public HttpResponseMessage DeleteAssociatedCategoryNode(int categoryNodeId)
        {
            HttpResponseMessage response;

            try
            {
                var deleted = _service.DeleteAssociatedCategoryNode(categoryNodeId);
                response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new CategoryNodeResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets a list of parent category nodes.
        /// </summary>
        /// <returns>Returns response of type HttpResponseMessage.</returns>       
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage GetParentCategoryList(int catalogId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetParentCategoryNodes(catalogId, RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<CategoryNodeListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new CategoryNodeListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Get BreadCrumb
        /// </summary>
        /// <param name="model">BreadCrumbModel model</param>
        /// <returns>Returns response of type HttpResponseMessage.</returns> 
        [HttpPost]
        public HttpResponseMessage GetBreadCrumb([FromBody] BreadCrumbModel model)
        {
            HttpResponseMessage response;

            try
            {
                var data = _service.GetBreadCrumb(model.CategoryID, model.SEOUrl, model.IsProductPresent, model.CatalogId);
                response = !Equals(data, null) ? CreateCreatedResponse(new BreadCrumbResponse { BreadCrumb = data }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new BreadCrumbResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }
    }
}
