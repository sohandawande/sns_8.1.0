﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
	public class PaymentTypesController : BaseController
	{
		private readonly IPaymentTypeService _service;
		private readonly IPaymentTypeCache _cache;

		public PaymentTypesController()
		{
			_service = new PaymentTypeService();
			_cache = new PaymentTypeCache(_service);
		}

		/// <summary>
		/// Gets a payment type.
		/// </summary>
		/// <param name="paymentTypeId">The ID of the payment type.</param>
		/// <returns></returns>
		[HttpGet]
		public HttpResponseMessage Get(int paymentTypeId)
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetPaymentType(paymentTypeId, RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<PaymentTypeResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new PaymentTypeResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Gets a list of payment types.
		/// </summary>
		/// <returns></returns>
		[FilterIsActive, FilterName]
		[SortName, SortPaymentTypeId]
		[PageIndex, PageSize]
		[HttpGet]
		public HttpResponseMessage List()
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetPaymentTypes(RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<PaymentTypeListResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new PaymentTypeListResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Creates a new payment type.
		/// </summary>
		/// <param name="model">The model of the payment type.</param>
		/// <returns></returns>
		[HttpPost]
		public HttpResponseMessage Create([FromBody] PaymentTypeModel model)
		{
			HttpResponseMessage response;

			try
			{
				var paymentType = _service.CreatePaymentType(model);
				if (paymentType != null)
				{
					var uri = Request.RequestUri;
					var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + paymentType.PaymentTypeId;

					response = CreateCreatedResponse(new PaymentTypeResponse { PaymentType = paymentType });
					response.Headers.Add("Location", location);
				}
				else
				{
					response = CreateInternalServerErrorResponse();
				}
			}
			catch (Exception ex)
			{
				var data = new PaymentTypeResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Updates an existing payment type.
		/// </summary>
		/// <param name="paymentTypeId">The ID of the payment type.</param>
		/// <param name="model">The model of the payment type.</param>
		/// <returns></returns>
		[HttpPut]
		public HttpResponseMessage Update(int paymentTypeId, [FromBody] PaymentTypeModel model)
		{
			HttpResponseMessage response;

			try
			{
				var paymentType = _service.UpdatePaymentType(paymentTypeId, model);
				response = paymentType != null ? CreateOKResponse(new PaymentTypeResponse { PaymentType = paymentType }) : CreateInternalServerErrorResponse();
			}
			catch (Exception ex)
			{
				var data = new PaymentTypeResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Deletes an existing payment type.
		/// </summary>
		/// <param name="paymentTypeId">The ID of the payment type.</param>
		/// <returns></returns>
		[HttpDelete]
		public HttpResponseMessage Delete(int paymentTypeId)
		{
			HttpResponseMessage response;

			try
			{
				var deleted = _service.DeletePaymentType(paymentTypeId);
				response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
			}
			catch (Exception ex)
			{
				var data = new PaymentTypeResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}
	}
}