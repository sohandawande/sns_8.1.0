﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    public class CategoryProfileController : BaseController
    {
        #region Private Variables
        private readonly ICategoryProfileService _service;
        private readonly ICategoryProfileCache _cache;

        #endregion

        #region Constructor
        public CategoryProfileController()
        {
            _service = new CategoryProfileService();
            _cache = new CategoryProfileCache(_service);
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Gets category profiles by category profile id.
        /// </summary>
        /// <param name="categoryProfileId">The id of the category profile.</param>
        /// <returns>Returns HttpResponseMessage response</returns>
        [HttpGet]
        public HttpResponseMessage GetCategoryProfilesByCategoryProfileId(int categoryProfileId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetCategoryProfilesByCategoryProfileId(categoryProfileId, RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<CategoryProfileResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new CategoryProfileResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Creates a new category profile.
        /// </summary>
        /// <param name="model">The model of type CategoryProfileModel.</param>
        /// <returns>Returns HttpResponseMessage type of response</returns>
        [HttpPost]
        public HttpResponseMessage Create([FromBody] CategoryProfileModel model)
        {
            HttpResponseMessage response;

            try
            {
                var categoryProfile = _service.Create(model);
                if (!Equals(categoryProfile, null))
                {
                    var uri = Request.RequestUri;
                    var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + categoryProfile.CategoryProfileID;

                    response = CreateCreatedResponse(new CategoryProfileResponse { CategoryProfile = categoryProfile });
                    response.Headers.Add("Location", location);
                }
                else
                {
                    response = CreateInternalServerErrorResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new CategoryProfileResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets category profiles by category id.
        /// </summary>
        /// <param name="categoryId">The id of category.</param>
        /// <returns>Returns HttpResponseMessage type of response.</returns>
        [HttpGet]
        public HttpResponseMessage GetCategoryProfilesByCategoryId(int categoryId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetCategoryProfilesByCategoryId(categoryId, RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<CategoryProfileListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new CategoryProfileResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Updates an existing manufacturer.
        /// </summary>
        /// <param name="categoryProfileId">The ID of the Category profile</param>
        /// <param name="model">The model of the CategoryProfile</param>
        /// <returns>Updated records for the category profile</returns>
        [HttpPut]
        public HttpResponseMessage Update(int categoryProfileId, [FromBody] CategoryProfileModel model)
        {
            HttpResponseMessage response;

            try
            {
                bool isUpdated = _service.UpdateCategoryProfile(categoryProfileId, model);
                BooleanModel boolModel = new BooleanModel { disabled = isUpdated };
                TrueFalseResponse resp = new TrueFalseResponse { booleanModel = boolModel };
                string data = JsonConvert.SerializeObject(resp);
                response = isUpdated ? CreateOKResponse<TrueFalseResponse>(data) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new TrueFalseResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Deletes an existing manufacturer.
        /// </summary>
        /// <param name="categoryProfileId">The ID of the categoryProfile.</param>
        /// <returns></returns>
        [HttpDelete]
        public HttpResponseMessage Delete(int categoryProfileId)
        {
            HttpResponseMessage response;

            try
            {
                var isDeleted = _service.DeleteCategoryProfile(categoryProfileId);
                BooleanModel boolModel = new BooleanModel { disabled = isDeleted };
                TrueFalseResponse resp = new TrueFalseResponse { booleanModel = boolModel };
                string data = JsonConvert.SerializeObject(resp);
                response = isDeleted ? CreateOKResponse<TrueFalseResponse>(data) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new TrueFalseResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        #endregion

    }
}