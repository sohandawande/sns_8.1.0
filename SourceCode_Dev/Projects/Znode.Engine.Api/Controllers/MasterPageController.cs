﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    public class MasterPageController : BaseController
    {
        #region Private variables
        private readonly IMasterPageService _service;
        private readonly IMasterPageCache _cache;
        #endregion

        #region Constructor
        public MasterPageController()
		{
			_service = new MasterPageService();
			_cache = new MasterPageCache(_service);
		}
        #endregion

        #region Public Methods
        /// <summary>
        /// Gets a list of Master Page.
        /// </summary>
        /// <returns>List of Master Page</returns>       
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage List()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetMasterPages(RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<MasterPageListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new MasterPageListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets a list of Master Page.
        /// </summary>
        /// <returns>List of Master Page</returns>       
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage GetMasterPageByThemeId(int themeId, string pageType)
        {
            HttpResponseMessage response;
            
            try
            {
                var data = _cache.GetMasterPageByThemeId(themeId, pageType, RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<MasterPageListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new MasterPageListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
        #endregion
    }
}
