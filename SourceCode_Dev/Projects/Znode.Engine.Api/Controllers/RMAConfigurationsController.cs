﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    public class RMAConfigurationsController : BaseController
    {
        #region Private Variables
        private readonly IRMAConfigurationCache _cache;
        private readonly IRMAConfigurationService _service;
        #endregion

        public RMAConfigurationsController()
        {
            _service = new RMAConfigurationService();
            _cache = new RMAConfigurationCache(_service);
        }

        /// <summary>
        /// Gets the profile By ProfileId.
        /// </summary>
        /// <param name="facetGroupId">The ID of the profile.</param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage Get(int rmaConfigId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetRMAConfiguration(rmaConfigId, RouteUri, RouteTemplate);
                response = (!Equals(data, null)) ? CreateOKResponse<RMAConfigurationResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new RMAConfigurationResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        /// <summary>
        /// Get List of all RMAConfigurations
        /// </summary>
        /// <returns>list of RMAConfigurations</returns>
        [HttpGet]
        public HttpResponseMessage List()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetRMAConfigurations(RouteUri, RouteTemplate);
                response = !Equals(data,null) ? CreateOKResponse<RMAConfigurationListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new PromotionListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Creates a new RMAConfiguration.
        /// </summary>
        /// <param name="model">The model of the RMAConfiguration.</param>
        /// <returns>Model of RMAConfiguration</returns>
        [HttpPost]
        public HttpResponseMessage Create([FromBody] RMAConfigurationModel model)
        {
            HttpResponseMessage response;

            try
            {
                var rmaConfig = _service.CreateRMAConfiguration(model);
                if (!Equals(rmaConfig, null))
                {
                    var uri = Request.RequestUri;
                    var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + rmaConfig.RMAConfigId;

                    response = CreateCreatedResponse(new RMAConfigurationResponse { RMAConfiguraton = rmaConfig });
                    response.Headers.Add("Location", location);
                }
                else
                {
                    response = CreateInternalServerErrorResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new RMAConfigurationResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        /// <summary>
        /// Updates an existing RMAConfiguration by rmaConfigId Id.
        /// </summary>
        /// <param name="profileId">The ID of the RMAConfiguration.</param>
        /// <param name="model">The model of the RMAConfiguration.</param>
        /// <returns>Model of RMAConfiguration</returns>
        [HttpPut]
        public HttpResponseMessage Update(int rmaConfigId, [FromBody] RMAConfigurationModel model)
        {
            HttpResponseMessage response;

            try
            {
                var rmaConfig = _service.UpdateRMAConfiguration(rmaConfigId, model);
                response = (!Equals(rmaConfig, null)) ? CreateOKResponse(new RMAConfigurationResponse { RMAConfiguraton = rmaConfig }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new RMAConfigurationResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        [HttpGet]
        public HttpResponseMessage GetAllRMAConfiguration()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetAllRMAConfiguration(RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<RMAConfigurationListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new PromotionListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
    }
}
