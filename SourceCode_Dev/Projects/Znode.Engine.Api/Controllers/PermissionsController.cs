﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    /// <summary>
    /// This is controller used for Permissions and Roles
    /// </summary>
    public class PermissionsController : BaseController
    {
        #region Private Variables
        public readonly IPermissionsService _service;
        public readonly IPermissionsCache _cache;
        #endregion

        #region Constructor
        public PermissionsController()
        {
            _service = new PermissionsService();
            _cache = new PermissionsCache(_service);
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// This method will fetch the data for Roles and Permissions for the relevant Account Id
        /// </summary>
        /// <param name="accountId"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetRolesAndPerminssions(int accountId)
        {
            HttpResponseMessage response;
            try
            {
                var data = _cache.GetRolesAndPerminssions(accountId, RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<PermissionsResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new PermissionsResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// This method will update the roles and permissions for the relevant account id
        /// </summary>
        /// <param name="model">PermissionsModel model</param>
        /// <returns>True if records updated in DB</returns>
        [HttpPut]
        public HttpResponseMessage UpdateRolesAndPermissions([FromBody]PermissionsModel model)
        {
            HttpResponseMessage response;
            try
            {
                var isUpdated = _service.UpdateRolesAndPermissions(model);


                BooleanModel boolModel = new BooleanModel { disabled = isUpdated };
                TrueFalseResponse responseModel = new TrueFalseResponse { booleanModel = boolModel };
                string data = JsonConvert.SerializeObject(responseModel);

                response = !Equals(data, null) ? CreateOKResponse<TrueFalseResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new TrueFalseResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        #endregion        

    }
}
