﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Engine.Api.Controllers
{
    /// <summary>
    /// This controller will clear the API cache.
    /// </summary>
    public class ClearCacheController : BaseController
    {
        /// <summary>
        /// This method will clear the API Cache
        /// </summary>
        /// <returns>Returns the response of the clear cache</returns>
        [HttpGet]
        public HttpResponseMessage ClearAPICache()
        {
            HttpResponseMessage response = null;

            try
            {
                System.Web.HttpRuntime.UnloadAppDomain();
                BooleanModel boolModel = new BooleanModel { disabled = true };
                TrueFalseResponse resp = new TrueFalseResponse { booleanModel = boolModel };
                string data = JsonConvert.SerializeObject(resp);
                response = CreateOKResponse<TrueFalseResponse>(resp);
            }
            catch (Exception ex)
            {
                response = CreateInternalServerErrorResponse();
            }
            
            return response;
        }
    }
}
