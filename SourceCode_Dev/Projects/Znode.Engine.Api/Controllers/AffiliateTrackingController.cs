﻿using System;
using System.Net.Http;
using System.Web.Mvc;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    public class AffiliateTrackingController : BaseController
    {
        #region Private Variables
        private readonly IAffiliateTrackingService _service;
        private readonly IAffiliateTrackingCache _cache;
        #endregion

        #region Constructor
        public AffiliateTrackingController()
        {
            _service = new AffiliateTrackingService();
            _cache = new AffiliateTrackingCache(_service);

        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Gets the affiliate tracking data.
        /// </summary>
        /// <returns>Returns HttpResponseMessage response.</returns>
        [HttpGet]
        public HttpResponseMessage GetTrackingData()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetAffiliateTrackingData(RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<AffiliateTrackingResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new AffiliateTrackingResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
        #endregion

    }
}
