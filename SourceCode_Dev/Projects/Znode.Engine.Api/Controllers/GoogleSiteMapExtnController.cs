﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Controllers
{
    public partial class GoogleSiteMapController : BaseController
    {
        [HttpGet]
        public HttpResponseMessage CreateForSchedular(string rootTag,string portalIDs)
        {
            HttpResponseMessage response;
            try
            {
                GoogleSiteMapModel model = new GoogleSiteMapModel() {
                    ChangeFreq = "Daily", //It can be empty as it is not used in google or bing feed
                    Date = string.Empty,
                    GoogleSiteMapFeedDescription = ConfigurationManager.AppSettings["GoogleSiteMapFeedDescription"],
                    GoogleSiteMapFeedLink = ConfigurationManager.AppSettings["DemoWebsiteUrl"],
                    GoogleSiteMapFeedTitle = ConfigurationManager.AppSettings["GoogleSiteMapFeedTitle"],
                    LastModified = "None", //It can be empty as it is not used in google or bing feed
                    PortalId = (portalIDs ?? "").Split(',').Select<string, int>(int.Parse).ToArray<int>(),
                    Priority = 1.0M, //It can be empty as it is not used in google or bing feed
                    RootTag = rootTag,
                    RootTagValue="Category", //It can be empty as it is not used in google or bing feed
                    SuccessXMLGenerationMessage =string.Empty,
                    XmlFileName= ConfigurationManager.AppSettings["XmlFileName"]
                };
                
                var googleSiteMap = _service.CreateGoogleSiteMap(model, string.Empty); //Domain Name is sent blank as it is not used in google or bing feed
                var data = googleSiteMap.SuccessXMLGenerationMessage;
                response = data != null ? CreateOKResponse(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = ex.Message;
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
    }
}
