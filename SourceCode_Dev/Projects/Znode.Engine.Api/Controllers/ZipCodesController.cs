﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    public class ZipCodesController : BaseController
	{
		private readonly IZipCodeService _service;
		private readonly IZipCodeCache _cache;

        public ZipCodesController()
		{
            _service = new ZipCodeService();
			_cache = new ZipCodeCache(_service);
		}

        /// <summary>
        /// Gets a list of zip codes.
        /// </summary>
        /// <returns></returns>        
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage List()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetZipCodes(RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<ZipCodeListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ZipCodeListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
    }
}
