﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    public class ManageSearchIndexController : BaseController
    {
        private readonly IManageSearchIndexService _service;
        private readonly IManageSearchIndexCache _cache;

        public ManageSearchIndexController()
        {
            _service = new ManageSearchIndexService();
            _cache = new ManageSearchIndexChache(_service);
        }



        /// <summary>
        /// Gets a list of Lucene Index server status.
        /// </summary>
        /// <returns></returns>
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage Get()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetLuceneIndexStatus(RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<ManageSearchIndexResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ManageSearchIndexResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }


        /// <summary>
        /// Create Lucene Index 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Create()
        {
            HttpResponseMessage response;

            try
            {
                bool status = _service.CreateIndex();
                if (status)
                {
                    var uri = Request.RequestUri;
                    var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + status;

                    response = CreateCreatedResponse(new ManageSearchIndexResponse { CreateIndex = status });
                    response.Headers.Add("Location", location);
                }
                else
                {
                    response = CreateInternalServerErrorResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new AddOnResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        /// <summary>
        /// Update Trigger mode Enabled/Disabled
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        public HttpResponseMessage Update(int id, [FromBody] LuceneIndexModel model)
        {
            HttpResponseMessage response;

            try
            {
                int disFlag = _service.DisableTriggers(id);
                response = !Equals(disFlag, null) ? CreateOKResponse(new ManageSearchIndexResponse { DisableFlag = disFlag }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new AddOnResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Enable/Disable Lucene windows service
        /// </summary>
        /// <param name="serviceflag">0/1 Service activation flag</param>
        /// <returns></returns>
        [HttpPut]
        public HttpResponseMessage DisableWinService(int serviceflag)
        {
            HttpResponseMessage response;
            try
            {
                int disFlag = _service.DisableWinService(serviceflag);
                response = !Equals(disFlag, null) ? CreateOKResponse(new ManageSearchIndexResponse { DisableFlag = disFlag }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new AddOnResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Get Index Server status details
        /// </summary>
        /// <param name="indexId">index Id</param>
        /// <returns></returns>
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage GetIndexServerStatus(int indexId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetIndexServerStatus(indexId, RouteUri, RouteTemplate);
                response = !Equals(data ,null) ? CreateOKResponse<ManageSearchIndexResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ManageSearchIndexResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }


    }
}
