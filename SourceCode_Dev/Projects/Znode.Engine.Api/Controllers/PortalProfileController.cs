﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    public class PortalProfileController : BaseController
    {
        #region Private Variables
        private readonly IPortalProfileService _service;
        private readonly IPortalProfileCache _cache;
        #endregion

        #region Consrtuctor
        public PortalProfileController()
        {
            _service = new PortalProfilesService();
            _cache = new PortalProfileCache(_service);
        }
        #endregion

        #region API Action Methods
        /// <summary>
        /// Gets a Portal profiles.
        /// </summary>
        /// <param name="portalProfileId">The ID of the portalProfile.</param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage Get(int portalProfileId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetPortalProfile(portalProfileId, RouteUri, RouteTemplate);
                response = !Equals(data,null) ? CreateOKResponse<PortalProfileResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new PortalProfileResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets a list of portal profiles.
        /// </summary>
        /// <returns></returns>      
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage List()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetPortalProfiles(RouteUri, RouteTemplate);
                response = !Equals(data,null) ? CreateOKResponse<PortalProfileListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new PortalProfileListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Creates a new portal profile.
        /// </summary>
        /// <param name="model">The model of the portal profile.</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Create([FromBody] PortalProfileModel model)
        {
            HttpResponseMessage response;

            try
            {
                var portalProfile = _service.CreatePortalProfile(model);
                if (!Equals(portalProfile, null))
                {
                    var uri = Request.RequestUri;
                    var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + portalProfile.PortalProfileID;

                    response = CreateCreatedResponse(new PortalProfileResponse { PortalProfile = portalProfile });
                    response.Headers.Add("Location", location);
                }
                else
                {
                    response = CreateInternalServerErrorResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new PortalProfileResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Updates an existing portalProfile.
        /// </summary>
        /// <param name="portalProfileId">The ID of the portalProfile.</param>
        /// <param name="model">The model of the portalProfile.</param>
        /// <returns></returns>
        [HttpPut]
        public HttpResponseMessage Update(int portalProfileId, [FromBody] PortalProfileModel model)
        {
            HttpResponseMessage response;

            try
            {
                var portalProfile = _service.UpdatePortalProfile(portalProfileId, model);
                response = !Equals(portalProfile,null) ? CreateOKResponse(new PortalProfileResponse { PortalProfile = portalProfile }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new PortalProfileResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Deletes an existing portal profile.
        /// </summary>
        /// <param name="portalCountryId">The ID of the portal country.</param>
        /// <returns></returns>
        [HttpDelete]
        public HttpResponseMessage Delete(int portalProfileId)
        {
            HttpResponseMessage response;

            try
            {
                var deleted = _service.DeletePortalProfile(portalProfileId);
                response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new PortalProfileResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Znode 8.0 Version
        /// Gets list of portal profiles with profile name and serial numbers.
        /// </summary>
        /// <returns>Http Response message.</returns>
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage GetPortalProfileListDetails()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetPortalProfilesDetails(RouteUri, RouteTemplate);
                response = !Equals(data,null) ? CreateOKResponse<PortalProfileListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new PortalProfileListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        [HttpGet]
        public HttpResponseMessage GetPortalProfilesByPortalId(int portalId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetPortalProfilesByPortalId(portalId,RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<PortalProfileListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new PortalProfileListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
        #endregion

    }
}
