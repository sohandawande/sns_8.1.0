﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
	public class ShippingServiceCodesController : BaseController
	{
		private readonly IShippingServiceCodeService _service;
		private readonly IShippingServiceCodeCache _cache;

		public ShippingServiceCodesController()
		{
			_service = new ShippingServiceCodeService();
			_cache = new ShippingServiceCodeCache(_service);
		}

		/// <summary>
		/// Gets a shipping service code.
		/// </summary>
		/// <param name="shippingServiceCodeId">The ID of the shipping service code.</param>
		/// <returns></returns>
		[ExpandShippingType]
		[HttpGet]
		public HttpResponseMessage Get(int shippingServiceCodeId)
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetShippingServiceCode(shippingServiceCodeId,  RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<ShippingServiceCodeResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new ShippingServiceCodeResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Gets a list of shipping service codes.
		/// </summary>
		/// <returns></returns>
		[ExpandShippingType]
		[FilterCode, FilterDescription, FilterIsActive, FilterShippingTypeId]
		[SortCode, SortDescription, SortDisplayOrder, SortShippingServiceCodeId]
		[PageIndex, PageSize]
		[HttpGet]
		public HttpResponseMessage List()
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetShippingServiceCodes(RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<ShippingServiceCodeListResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new ShippingServiceCodeListResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Creates a new shipping service code.
		/// </summary>
		/// <param name="model">The model of the shipping service code.</param>
		/// <returns></returns>
		[HttpPost]
		public HttpResponseMessage Create([FromBody] ShippingServiceCodeModel model)
		{
			HttpResponseMessage response;

			try
			{
				var shippingServiceCode = _service.CreateShippingServiceCode(model);
				if (shippingServiceCode != null)
				{
					var uri = Request.RequestUri;
					var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + shippingServiceCode.ShippingServiceCodeId;

					response = CreateCreatedResponse(new ShippingServiceCodeResponse { ShippingServiceCode = shippingServiceCode });
					response.Headers.Add("Location", location);
				}
				else
				{
					response = CreateInternalServerErrorResponse();
				}
			}
			catch (Exception ex)
			{
				var data = new ShippingServiceCodeResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Updates an existing shipping service code.
		/// </summary>
		/// <param name="shippingServiceCodeId">The ID of the shipping service code.</param>
		/// <param name="model">The model of the shipping service code.</param>
		/// <returns></returns>
		[HttpPut]
		public HttpResponseMessage Update(int shippingServiceCodeId, [FromBody] ShippingServiceCodeModel model)
		{
			HttpResponseMessage response;

			try
			{
				var shippingServiceCode = _service.UpdateShippingServiceCode(shippingServiceCodeId, model);
				response = shippingServiceCode != null ? CreateOKResponse(new ShippingServiceCodeResponse { ShippingServiceCode = shippingServiceCode }) : CreateInternalServerErrorResponse();
			}
			catch (Exception ex)
			{
				var data = new ShippingServiceCodeResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Deletes an existing shipping service code.
		/// </summary>
		/// <param name="shippingServiceCodeId">The ID of the shipping service code.</param>
		/// <returns></returns>
		[HttpDelete]
		public HttpResponseMessage Delete(int shippingServiceCodeId)
		{
			HttpResponseMessage response;

			try
			{
				var deleted = _service.DeleteShippingServiceCode(shippingServiceCodeId);
				response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
			}
			catch (Exception ex)
			{
				var data = new ShippingServiceCodeResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}
	}
}