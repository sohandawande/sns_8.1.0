﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
	public class AttributeTypesController : BaseController
	{
		private readonly IAttributeTypeService _service;
		private readonly IAttributeTypeCache _cache;

		public AttributeTypesController()
		{
			_service = new AttributeTypeService();
			_cache = new AttributeTypeCache(_service);
		}

		/// <summary>
		/// Gets an attribute type.
		/// </summary>
		/// <param name="attributeTypeId">The ID of the attribute type.</param>
		/// <returns></returns>
		[HttpGet]
		public HttpResponseMessage Get(int attributeTypeId)
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetAttributeType(attributeTypeId, RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<AttributeTypeResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new AttributeTypeResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Gets a list of attribute types.
		/// </summary>
		/// <returns></returns>
		[FilterIsPrivate, FilterLocaleId, FilterName, FilterPortalId]
		[SortAttributeTypeId, SortDisplayOrder, SortName]
		[HttpGet]
		public HttpResponseMessage List()
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetAttributeTypes(RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<AttributeTypeListResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new AttributeTypeListResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Creates a new attribute type.
		/// </summary>
		/// <param name="model">The model of the attribute type.</param>
		/// <returns></returns>
		[HttpPost]
		public HttpResponseMessage Create([FromBody] AttributeTypeModel model)
		{
			HttpResponseMessage response;

			try
			{
				var attributeType = _service.CreateAttributeType(model);
				if (attributeType != null)
				{
					var uri = Request.RequestUri;
					var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + attributeType.AttributeTypeId;

					response = CreateCreatedResponse(new AttributeTypeResponse { AttributeType = attributeType });
					response.Headers.Add("Location", location);
				}
				else
				{
					response = CreateInternalServerErrorResponse();
				}
			}
			catch (Exception ex)
			{
				var data = new AttributeTypeResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Updates an existing attribute type.
		/// </summary>
		/// <param name="attributeTypeId">The ID of the attribute type.</param>
		/// <param name="model">The model of the attribute type.</param>
		/// <returns></returns>
		[HttpPut]
		public HttpResponseMessage Update(int attributeTypeId, [FromBody] AttributeTypeModel model)
		{
			HttpResponseMessage response;

			try
			{
				var attributeType = _service.UpdateAttributeType(attributeTypeId, model);
				response = attributeType != null ? CreateOKResponse(new AttributeTypeResponse { AttributeType = attributeType }) : CreateInternalServerErrorResponse();
			}
			catch (Exception ex)
			{
				var data = new AttributeTypeResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Deletes an existing attribute type.
		/// </summary>
		/// <param name="attributeTypeId">The ID of the attribute type.</param>
		/// <returns></returns>
		[HttpDelete]
		public HttpResponseMessage Delete(int attributeTypeId)
		{
			HttpResponseMessage response;

			try
			{
				var deleted = _service.DeleteAttributeType(attributeTypeId);
				response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
			}
			catch (Exception ex)
			{
				var data = new AttributeTypeResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

        /// <summary>
        /// Gets an attribute type based on CatalogId.
        /// </summary>
        /// <param name="catalogId">The ID of the catalog.</param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetByCatalogId(int catalogId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetAttributeTypesByCatalogId(catalogId, RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<AttributeTypeListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new AttributeTypeListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
	}
}
