﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    public class AccountProfileController : BaseController
    {
        private readonly IAccountsProfilesService _service;

        public AccountProfileController()
        {
            _service = new AccountsProfilesService();
        }

        /// <summary>
        /// Get Profiles Associated with Account 
        /// </summary>
        /// <param name="accountId">account Id</param>
        /// <param name="profileIds">profileIds</param>
        /// <returns>HttpResponseMessage</returns>
        [HttpGet]
        public HttpResponseMessage AccountAssociatedProfiles(int accountId, string profileIds)
        {
            HttpResponseMessage response;
            try
            {
                response = _service.AccountAssociatedProfiles(accountId, profileIds) ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new AccountProfileResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Deletes an existing Account Profile.
        /// </summary>
        /// <param name="accountProfileId">The ID of the Account Profile.</param>
        /// <returns>True / False when Record is deleted</returns>
        [HttpDelete]
        public HttpResponseMessage Delete(int accountProfileId)
        {
            HttpResponseMessage response;

            try
            {
                var deleted = _service.DeleteAccountProfile(accountProfileId);
                response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new AccountProfileResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
    }
}
