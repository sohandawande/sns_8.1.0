﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;
namespace Znode.Engine.Api.Controllers
{
    /// <summary>
    /// Google Site Map Controller
    /// </summary>
    public partial class GoogleSiteMapController : BaseController
    {
        #region Private Variables

        private readonly IGoogleSiteMapService _service;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor for Google Site Map
        /// </summary>
        public GoogleSiteMapController()
        {
            _service = new GoogleSiteMapService();
        }

        #endregion

        #region Public Method
        /// <summary>
        /// Generates an XML file 
        /// </summary>
        /// <param name="model">GoogleSiteMapModel model</param>
        /// <returns></returns>        
        [HttpPost]
        public HttpResponseMessage Create([FromBody] GoogleSiteMapModel model)
        {
            HttpResponseMessage response;
            try
            {
                string domainname = model.SuccessXMLGenerationMessage;
                var googleSiteMap = _service.CreateGoogleSiteMap(model, domainname);
                if (googleSiteMap != null)
                {
                    var uri = Request.RequestUri;
                    var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + googleSiteMap.GoogleSiteMapId;

                    response = CreateCreatedResponse(new GoogleSiteMapResponse { GoogleSiteMap = googleSiteMap });
                    response.Headers.Add("Location", location);
                }
                else
                {
                    response = CreateInternalServerErrorResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new GoogleSiteMapResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }
        #endregion
    }
}
