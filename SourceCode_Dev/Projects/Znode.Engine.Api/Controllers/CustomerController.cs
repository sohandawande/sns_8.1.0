﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Attributes;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Controllers
{
    public class CustomerController : BaseController
    {
        #region Private Variables
        private readonly ICustomerService _service;
        private readonly ICustomerCache _cache;
        #endregion

        #region Constructor
        public CustomerController()
        {
            _service = new CustomerService();
            _cache = new CustomerCache(_service);
        } 
        #endregion

        #region Public Methods

        /// <summary>
        /// To Get Customer account List.
        /// </summary>
        /// <returns>HttpResponseMessage </returns>
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage List()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetCustomerList(RouteUri, RouteTemplate);
                response = (!Equals(data, null)) ? CreateOKResponse<CustomerListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new CustomerListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        /// <summary>
        /// To Get Customer Affiliate List.
        /// </summary>
        /// <param name="accountId"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetCustomerAffiliate(int accountId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetCustomerAffiliate(accountId, RouteUri, RouteTemplate);
                response = !Equals(data, null) ? CreateOKResponse<CustomerAffiliateResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new CustomerAffiliateResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        /// <summary>
        /// To Get Referral Commission Type List.
        /// </summary>
        /// <returns>HttpResponse Message</returns>
        [HttpGet]
        public HttpResponseMessage GetReferralCommissionTypeList()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetReferralCommissionTypeList(RouteUri, RouteTemplate);
                response = (!Equals(data, null)) ? CreateOKResponse<ReferralCommissionTypeListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ReferralCommissionTypeListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        /// <summary>
        /// To Get Referral Commission List.
        /// </summary>
        /// <returns>HttpResponse Message</returns>
        [HttpGet]
        public HttpResponseMessage GetReferralCommissionList(int accountId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetReferralCommissionList(accountId, RouteUri, RouteTemplate);
                response = (!Equals(data, null)) ? CreateOKResponse<ReferralCommissionListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ReferralCommissionListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        //PRFT Custom Code : Start
        /// <summary>
        /// To Get Customer account List.
        /// </summary>
        /// <returns>HttpResponseMessage </returns>
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage GetAssociatedSuperUser()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetAssociatedSuperUserList(RouteUri, RouteTemplate);
                response = (!Equals(data, null)) ? CreateOKResponse<CustomerListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new CustomerListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        public HttpResponseMessage GetNotAssociatedSuperUser(int accountId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetNotAssociatedSuperUserList(accountId,RouteUri, RouteTemplate);
                response = (!Equals(data, null)) ? CreateOKResponse<CustomerListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new CustomerListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }
        
        //PRFT Custom Code : End
        #endregion
    }
}
    