using System.Collections.ObjectModel;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Engine.Api.Areas.HelpPage.App_Start
{
	/// <summary>
	/// Use this class to customize the Help Page.
	/// For example you can set a custom <see cref="System.Web.Http.Description.IDocumentationProvider"/> to supply the documentation
	/// or you can provide the samples for the requests/responses.
	/// </summary>
	public static class HelpPageConfig
	{
		public static void Register(HttpConfiguration config)
		{
			// Use the XML documentation provider
			config.SetDocumentationProvider(new XmlDocumentationProvider(HttpContext.Current.Server.MapPath("~/App_Data/Znode.Engine.Api.xml")));

			// Set JSON media type
			var jsonMediaType = new MediaTypeHeaderValue("application/json");

			// Sample account
			var accountResponse = new AccountResponse { Account = new AccountModel().Sample() };
			accountResponse.Account.AccountType = new AccountTypeModel().Sample();
			var accountListResponse = new AccountListResponse { Accounts = new Collection<AccountModel>() };
			accountListResponse.Accounts.Add(accountResponse.Account);

			// Sample address
			var addressResponse = new AddressResponse { Address = new AddressModel().Sample() };
			var addressListResponse = new AddressListResponse { Addresses = new Collection<AddressModel>() };
			addressListResponse.Addresses.Add(addressResponse.Address);

			// Sample attribute type
			var attributeTypeResponse = new AttributeTypeResponse { AttributeType = new AttributeTypeModel().Sample() };
			var attributeTypeListResponse = new AttributeTypeListResponse { AttributeTypes = new Collection<AttributeTypeModel>() };
			attributeTypeListResponse.AttributeTypes.Add(attributeTypeResponse.AttributeType);

			// Sample audit
			var auditResponse = new AuditResponse { Audit = new AuditModel().Sample() };
			auditResponse.Audit.AuditSourceType = new AuditSourceTypeModel().Sample();
			auditResponse.Audit.AuditStatus = new AuditStatusModel().Sample();
			auditResponse.Audit.AuditType = new AuditTypeModel().Sample();
			var auditListResponse = new AuditListResponse { Audits = new Collection<AuditModel>() };
			auditListResponse.Audits.Add(auditResponse.Audit);

			// Sample case request
			var caseRequestResponse = new CaseRequestResponse { CaseRequest = new CaseRequestModel().Sample() };
			caseRequestResponse.CaseRequest.CasePriority = new CasePriorityModel().Sample();
			caseRequestResponse.CaseRequest.CaseStatus = new CaseStatusModel().Sample();
			caseRequestResponse.CaseRequest.CaseType = new CaseTypeModel().Sample();
			var caseRequestListResponse = new CaseRequestListResponse { CaseRequests = new Collection<CaseRequestModel>() };
			caseRequestListResponse.CaseRequests.Add(caseRequestResponse.CaseRequest);

			// Sample catalog
			var catalogResponse = new CatalogResponse { Catalog = new CatalogModel().Sample() };
			var catalogListResponse = new CatalogListResponse { Catalogs = new Collection<CatalogModel>() };
			catalogListResponse.Catalogs.Add(catalogResponse.Catalog);

			// Sample category
			var categoryResponse = new CategoryResponse { Category = new CategoryModel().Sample() };
			categoryResponse.Category.CategoryNodes.Add(new CategoryNodeModel().Sample());
			categoryResponse.Category.Subcategories.Add(new CategoryModel().Sample());
			var categoryListResponse = new CategoryListResponse { Categories = new Collection<CategoryModel>() };
			categoryListResponse.Categories.Add(categoryResponse.Category);

			// Sample category node
			var categoryNodeResponse = new CategoryNodeResponse { CategoryNode = new CategoryNodeModel().Sample() };
			categoryNodeResponse.CategoryNode.Catalog = new CatalogModel().Sample();
			categoryNodeResponse.CategoryNode.Category = new CategoryModel().Sample();
			var categoryNodeListResponse = new CategoryNodeListResponse { CategoryNodes = new Collection<CategoryNodeModel>() };
			categoryNodeListResponse.CategoryNodes.Add(categoryNodeResponse.CategoryNode);

			// Sample country
			var countryResponse = new CountryResponse { Country = new CountryModel().Sample() };
			var countryListResponse = new CountryListResponse { Countries = new Collection<CountryModel>() };
			countryListResponse.Countries.Add(countryResponse.Country);

			// Sample domain
			var domainResponse = new DomainResponse { Domain = new DomainModel().Sample() };
			var domainListResponse = new DomainListResponse { Domains = new Collection<DomainModel>() };
			domainListResponse.Domains.Add(domainResponse.Domain);

			// Sample gift card
			var giftCardResponse = new GiftCardResponse { GiftCard = new GiftCardModel().Sample() };
			giftCardResponse.GiftCard.History.Add(new GiftCardHistoryModel().Sample());
			giftCardResponse.GiftCard.OrderLineItem = new OrderLineItemModel().Sample();
			var giftCardListResponse = new GiftCardListResponse { GiftCards = new Collection<GiftCardModel>() };
			giftCardListResponse.GiftCards.Add(giftCardResponse.GiftCard);

			// Sample highlight
			var highlightResponse = new HighlightResponse { Highlight = new HighlightModel().Sample() };
			highlightResponse.Highlight.HighlightType = new HighlightTypeModel().Sample();
			var highlightListResponse = new HighlightListResponse { Highlights = new Collection<HighlightModel>() };
			highlightListResponse.Highlights.Add(highlightResponse.Highlight);

			// Sample highlight type
			var highlightTypeResponse = new HighlightTypeResponse { HighlightType = new HighlightTypeModel().Sample() };
			var highlightTypeListResponse = new HighlightTypeListResponse { HighlightTypes = new Collection<HighlightTypeModel>() };
			highlightTypeListResponse.HighlightTypes.Add(highlightTypeResponse.HighlightType);

			// Sample inventory
			var inventoryResponse = new InventoryResponse { Inventory = new InventoryModel().Sample() };
			inventoryResponse.Inventory.Skus.Add(new SkuModel().Sample());
			var inventoryListResponse = new InventoryListResponse { Inventories = new Collection<InventoryModel>() };
			inventoryListResponse.Inventories.Add(inventoryResponse.Inventory);

			// Sample manufacturer
			var manufacturerResponse = new ManufacturerResponse { Manufacturer = new ManufacturerModel().Sample() };
			var manufacturerListResponse = new ManufacturerListResponse { Manufacturers = new Collection<ManufacturerModel>() };
			manufacturerListResponse.Manufacturers.Add(manufacturerResponse.Manufacturer);

			// Sample message config
			var messageConfigResponse = new MessageConfigResponse { MessageConfig = new MessageConfigModel().Sample() };
			messageConfigResponse.MessageConfig.MessageType = new MessageTypeModel().Sample();
			var messageConfigListResponse = new MessageConfigListResponse { MessageConfigs = new Collection<MessageConfigModel>() };
			messageConfigListResponse.MessageConfigs.Add(messageConfigResponse.MessageConfig);

			// Sample order
			var orderResponse = new OrderResponse { Order = new OrderModel().Sample() };
			orderResponse.Order.Account = new AccountModel().Sample();
			orderResponse.Order.OrderLineItems.Add(new OrderLineItemModel().Sample());
			orderResponse.Order.PaymentOption = new PaymentOptionModel().Sample();
			orderResponse.Order.PaymentType = new PaymentTypeModel().Sample();
			orderResponse.Order.ShippingOption = new ShippingOptionModel().Sample();
			var orderListResponse = new OrderListResponse { Orders = new Collection<OrderModel>() };
			orderListResponse.Orders.Add(orderResponse.Order);

			// Sample payment gateway
			var paymentGatewayResponse = new PaymentGatewayResponse { PaymentGateway = new PaymentGatewayModel().Sample() };
			var paymentGatewayListResponse = new PaymentGatewayListResponse { PaymentGateways = new Collection<PaymentGatewayModel>() };
			paymentGatewayListResponse.PaymentGateways.Add(paymentGatewayResponse.PaymentGateway);

			// Sample payment option
			var paymentOptionResponse = new PaymentOptionResponse { PaymentOption = new PaymentOptionModel().Sample() };
			paymentOptionResponse.PaymentOption.PaymentGateway = new PaymentGatewayModel().Sample();
			paymentOptionResponse.PaymentOption.PaymentType = new PaymentTypeModel().Sample();
			var paymentOptionListResponse = new PaymentOptionListResponse { PaymentOptions = new Collection<PaymentOptionModel>() };
			paymentOptionListResponse.PaymentOptions.Add(paymentOptionResponse.PaymentOption);

			// Sample payment type
			var paymentTypeResponse = new PaymentTypeResponse { PaymentType = new PaymentTypeModel().Sample() };
			var paymentTypeListResponse = new PaymentTypeListResponse { PaymentTypes = new Collection<PaymentTypeModel>() };
			paymentTypeListResponse.PaymentTypes.Add(paymentTypeResponse.PaymentType);

			// Sample portal
			var portalResponse = new PortalResponse { Portal = new PortalModel().Sample() };
			portalResponse.Portal.PortalCountries.Add(new PortalCountryModel().Sample());
			var portalListResponse = new PortalListResponse { Portals = new Collection<PortalModel>() };
			portalListResponse.Portals.Add(portalResponse.Portal);

			// Sample portal catalog
			var portalCatalogResponse = new PortalCatalogResponse { PortalCatalog = new PortalCatalogModel().Sample() };
			portalCatalogResponse.PortalCatalog.Catalog = new CatalogModel().Sample();
			portalCatalogResponse.PortalCatalog.Portal = new PortalModel().Sample();
			var portalCatalogListResponse = new PortalCatalogListResponse { PortalCatalogs = new Collection<PortalCatalogModel>() };
			portalCatalogListResponse.PortalCatalogs.Add(portalCatalogResponse.PortalCatalog);

			// Sample portal country
			var portalCountryResponse = new PortalCountryResponse { PortalCountry = new PortalCountryModel().Sample() };
			portalCountryResponse.PortalCountry.Country = new CountryModel().Sample();
			var portalCountryListResponse = new PortalCountryListResponse { PortalCountries = new Collection<PortalCountryModel>() };
			portalCountryListResponse.PortalCountries.Add(portalCountryResponse.PortalCountry);

			// Sample product
			var productResponse = new ProductResponse { Product = new ProductModel().Sample() };
			productResponse.Product.AddOns.Add(new AddOnModel().Sample());
			productResponse.Product.Attributes.Add(new AttributeModel().Sample());
			productResponse.Product.Attributes[0].AttributeType = new AttributeTypeModel().Sample();
			productResponse.Product.Categories.Add(new CategoryModel().Sample());
			productResponse.Product.CrossSells.Add(new CrossSellModel().Sample());
			productResponse.Product.FrequentlyBoughtTogether.Add(new CrossSellModel().Sample());
			productResponse.Product.Highlights.Add(new HighlightModel().Sample());
			productResponse.Product.Images.Add(new ImageModel().Sample());
			productResponse.Product.Manufacturer = new ManufacturerModel().Sample();
			productResponse.Product.ProductType = new ProductTypeModel().Sample();
			productResponse.Product.Promotions.Add(new PromotionModel().Sample());
			productResponse.Product.Reviews.Add(new ReviewModel().Sample());
			productResponse.Product.SelectedSku = new SkuModel().Sample();
			productResponse.Product.Skus.Add(new SkuModel().Sample());
			productResponse.Product.Tiers.Add(new TierModel().Sample());
			productResponse.Product.YouMayAlsoLike.Add(new CrossSellModel().Sample());
			var productListResponse = new ProductListResponse { Products = new Collection<ProductModel>() };
			productListResponse.Products.Add(productResponse.Product);

			// Sample product category
			var productCategoryResponse = new ProductCategoryResponse { ProductCategory = new ProductCategoryModel().Sample() };
			productCategoryResponse.ProductCategory.Category = new CategoryModel().Sample();
			productCategoryResponse.ProductCategory.Product = new ProductModel().Sample();
			var productCategoryListResponse = new ProductCategoryListResponse { ProductCategories = new Collection<ProductCategoryModel>() };
			productCategoryListResponse.ProductCategories.Add(productCategoryResponse.ProductCategory);

			// Sample promotion
			var promotionResponse = new PromotionResponse { Promotion = new PromotionModel().Sample() };
			promotionResponse.Promotion.PromotionType = new PromotionTypeModel().Sample();
			var promotionListResponse = new PromotionListResponse { Promotions = new Collection<PromotionModel>() };
			promotionListResponse.Promotions.Add(promotionResponse.Promotion);

			// Sample promotion type
			var promotionTypeResponse = new PromotionTypeResponse { PromotionType = new PromotionTypeModel().Sample() };
			var promotionTypeListResponse = new PromotionTypeListResponse { PromotionTypes = new Collection<PromotionTypeModel>() };
			promotionTypeListResponse.PromotionTypes.Add(promotionTypeResponse.PromotionType);

			// Sample review
			var reviewResponse = new ReviewResponse { Review = new ReviewModel().Sample() };
			reviewResponse.Review.Product = new ProductModel().Sample();
			var reviewListResponse = new ReviewListResponse { Reviews = new Collection<ReviewModel>() };
			reviewListResponse.Reviews.Add(reviewResponse.Review);

			// Sample shipping options
			var shippingOptionResponse = new ShippingOptionResponse { ShippingOption = new ShippingOptionModel().Sample() };
			shippingOptionResponse.ShippingOption.ShippingType = new ShippingTypeModel().Sample();
			var shippingOptionListResponse = new ShippingOptionListResponse { ShippingOptions = new Collection<ShippingOptionModel>() };
			shippingOptionListResponse.ShippingOptions.Add(shippingOptionResponse.ShippingOption);

			// Sample shipping rule
			var shippingRuleResponse = new ShippingRuleResponse { ShippingRule = new ShippingRuleModel().Sample() };
			shippingRuleResponse.ShippingRule.ShippingOption = new ShippingOptionModel().Sample();
			shippingRuleResponse.ShippingRule.ShippingRuleType = new ShippingRuleTypeModel().Sample();
			var shippingRuleListResponse = new ShippingRuleListResponse { ShippingRules = new Collection<ShippingRuleModel>() };
			shippingRuleListResponse.ShippingRules.Add(shippingRuleResponse.ShippingRule);

			// Sample shipping rule type
			var shippingRuleTypeResponse = new ShippingRuleTypeResponse { ShippingRuleType = new ShippingRuleTypeModel().Sample() };
			var shippingRuleTypeListResponse = new ShippingRuleTypeListResponse { ShippingRuleTypes = new Collection<ShippingRuleTypeModel>() };
			shippingRuleTypeListResponse.ShippingRuleTypes.Add(shippingRuleTypeResponse.ShippingRuleType);

			// Sample shipping service codes
			var shippingServiceCodeResponse = new ShippingServiceCodeResponse { ShippingServiceCode = new ShippingServiceCodeModel().Sample() };
			shippingServiceCodeResponse.ShippingServiceCode.ShippingType = new ShippingTypeModel().Sample();
			var shippingServiceCodeListResponse = new ShippingServiceCodeListResponse { ShippingServiceCodes = new Collection<ShippingServiceCodeModel>() };
			shippingServiceCodeListResponse.ShippingServiceCodes.Add(shippingServiceCodeResponse.ShippingServiceCode);

			// Sample shipping type
			var shippingTypeResponse = new ShippingTypeResponse { ShippingType = new ShippingTypeModel().Sample() };
			var shippingTypeListResponse = new ShippingTypeListResponse { ShippingTypes = new Collection<ShippingTypeModel>() };
			shippingTypeListResponse.ShippingTypes.Add(shippingTypeResponse.ShippingType);

			// Sample SKU
			var skuResponse = new SkuResponse { Sku = new SkuModel().Sample() };
			skuResponse.Sku.Attributes.Add(new AttributeModel().Sample());
			skuResponse.Sku.Inventory = new InventoryModel().Sample();
			skuResponse.Sku.Supplier = new SupplierModel().Sample();
			var skuListResponse = new SkuListResponse { Skus = new Collection<SkuModel>() };
			skuListResponse.Skus.Add(skuResponse.Sku);

			// Sample state
			var stateResponse = new StateResponse { State = new StateModel().Sample() };
			var stateListResponse = new StateListResponse { States = new Collection<StateModel>() };
			stateListResponse.States.Add(stateResponse.State);

			// Sample supplier
			var supplierResponse = new SupplierResponse { Supplier = new SupplierModel().Sample() };
			supplierResponse.Supplier.SupplierType = new SupplierTypeModel().Sample();
			var supplierListResponse = new SupplierListResponse { Suppliers = new Collection<SupplierModel>() };
			supplierListResponse.Suppliers.Add(supplierResponse.Supplier);

			// Sample supplier type
			var supplierTypeResponse = new SupplierTypeResponse { SupplierType = new SupplierTypeModel().Sample() };
			var supplierTypeListResponse = new SupplierTypeListResponse { SupplierTypes = new Collection<SupplierTypeModel>() };
			supplierTypeListResponse.SupplierTypes.Add(supplierTypeResponse.SupplierType);

			// Sample tax class
			var taxClassResponse = new TaxClassResponse { TaxClass = new TaxClassModel().Sample() };
			var taxClassListResponse = new TaxClassListResponse { TaxClasses = new Collection<TaxClassModel>() };
			taxClassListResponse.TaxClasses.Add(taxClassResponse.TaxClass);

			// Sample tax rule
			var taxRuleResponse = new TaxRuleResponse { TaxRule = new TaxRuleModel().Sample() };
			var taxRuleListResponse = new TaxRuleListResponse { TaxRules = new Collection<TaxRuleModel>() };
			taxRuleListResponse.TaxRules.Add(taxRuleResponse.TaxRule);

			// Sample tax rule type
			var taxRuleTypeResponse = new TaxRuleTypeResponse { TaxRuleType = new TaxRuleTypeModel().Sample() };
			var taxRuleTypeListResponse = new TaxRuleTypeListResponse { TaxRuleTypes = new Collection<TaxRuleTypeModel>() };
			taxRuleTypeListResponse.TaxRuleTypes.Add(taxRuleTypeResponse.TaxRuleType);

			// Set all sample responses
			config.SetSampleResponse(accountResponse.ToJson(), jsonMediaType, "Accounts", "Get", "accountId");
			config.SetSampleResponse(accountListResponse.ToJson(), jsonMediaType, "Accounts", "List");
			config.SetSampleResponse(addressResponse.ToJson(), jsonMediaType, "Addresses", "Get", "addressId");
			config.SetSampleResponse(addressListResponse.ToJson(), jsonMediaType, "Addresses", "List");
			config.SetSampleResponse(attributeTypeResponse.ToJson(), jsonMediaType, "AttributeTypes", "Get", "attributeTypeId");
			config.SetSampleResponse(attributeTypeListResponse.ToJson(), jsonMediaType, "AttributeTypes", "List");
			config.SetSampleResponse(auditResponse.ToJson(), jsonMediaType, "Audits", "Get", "auditId");
			config.SetSampleResponse(auditListResponse.ToJson(), jsonMediaType, "Audits", "List");
			config.SetSampleResponse(caseRequestResponse.ToJson(), jsonMediaType, "CaseRequests", "Get", "caseRequestId");
			config.SetSampleResponse(caseRequestListResponse.ToJson(), jsonMediaType, "CaseRequests", "List");
			config.SetSampleResponse(catalogResponse.ToJson(), jsonMediaType, "Catalogs", "Get", "catalogId");
			config.SetSampleResponse(catalogListResponse.ToJson(), jsonMediaType, "Catalogs", "List");
			config.SetSampleResponse(categoryResponse.ToJson(), jsonMediaType, "Categories", "Get", "categoryId");
			config.SetSampleResponse(categoryListResponse.ToJson(), jsonMediaType, "Categories", "List");
			config.SetSampleResponse(categoryListResponse.ToJson(), jsonMediaType, "Categories", "ListByCatalog", "catalogId");
			config.SetSampleResponse(categoryNodeResponse.ToJson(), jsonMediaType, "CategoryNodes", "Get", "categoryNodeId");
			config.SetSampleResponse(categoryNodeListResponse.ToJson(), jsonMediaType, "CategoryNodes", "List");
			config.SetSampleResponse(countryResponse.ToJson(), jsonMediaType, "Countries", "Get", "countryCode");
			config.SetSampleResponse(countryListResponse.ToJson(), jsonMediaType, "Countries", "List");
			config.SetSampleResponse(domainResponse.ToJson(), jsonMediaType, "Domains", "Get", "domainId");
			config.SetSampleResponse(domainListResponse.ToJson(), jsonMediaType, "Domains", "List");
			config.SetSampleResponse(giftCardResponse.ToJson(), jsonMediaType, "GiftCards", "Get", "giftCardId");
			config.SetSampleResponse(giftCardListResponse.ToJson(), jsonMediaType, "GiftCards", "List");
			config.SetSampleResponse(highlightResponse.ToJson(), jsonMediaType, "Highlights", "Get", "highlightId");
			config.SetSampleResponse(highlightListResponse.ToJson(), jsonMediaType, "Highlights", "List");
			config.SetSampleResponse(highlightTypeResponse.ToJson(), jsonMediaType, "HighlightTypes", "Get", "highlightTypeId");
			config.SetSampleResponse(highlightTypeListResponse.ToJson(), jsonMediaType, "HighlightTypes", "List");
			config.SetSampleResponse(inventoryResponse.ToJson(), jsonMediaType, "Inventory", "Get", "inventoryId");
			config.SetSampleResponse(inventoryListResponse.ToJson(), jsonMediaType, "Inventory", "List");
			config.SetSampleResponse(manufacturerResponse.ToJson(), jsonMediaType, "Manufacturers", "Get", "manufacturerId");
			config.SetSampleResponse(manufacturerListResponse.ToJson(), jsonMediaType, "Manufacturers", "List");
			config.SetSampleResponse(messageConfigResponse.ToJson(), jsonMediaType, "MessageConfigs", "Get", "messageConfigId");
			config.SetSampleResponse(messageConfigListResponse.ToJson(), jsonMediaType, "MessageConfigs", "List");
			config.SetSampleResponse(messageConfigListResponse.ToJson(), jsonMediaType, "MessageConfigs", "ListByKeys", "keys");
			config.SetSampleResponse(orderResponse.ToJson(), jsonMediaType, "Orders", "Get", "orderId");
			config.SetSampleResponse(orderListResponse.ToJson(), jsonMediaType, "Orders", "List");
			config.SetSampleResponse(paymentGatewayResponse.ToJson(), jsonMediaType, "PaymentGateways", "Get", "paymentGatewayId");
			config.SetSampleResponse(paymentGatewayListResponse.ToJson(), jsonMediaType, "PaymentGateways", "List");
			config.SetSampleResponse(paymentOptionResponse.ToJson(), jsonMediaType, "PaymentOptions", "Get", "paymentOptionId");
			config.SetSampleResponse(paymentOptionListResponse.ToJson(), jsonMediaType, "PaymentOptions", "List");
			config.SetSampleResponse(paymentTypeResponse.ToJson(), jsonMediaType, "PaymentTypes", "Get", "paymentTypeId");
			config.SetSampleResponse(paymentTypeListResponse.ToJson(), jsonMediaType, "PaymentTypes", "List");
			config.SetSampleResponse(portalResponse.ToJson(), jsonMediaType, "Portals", "Get", "portalId");
			config.SetSampleResponse(portalListResponse.ToJson(), jsonMediaType, "Portals", "List");
			config.SetSampleResponse(portalListResponse.ToJson(), jsonMediaType, "Portals", "ListByPortalIds", "portalIds");
			config.SetSampleResponse(portalCatalogResponse.ToJson(), jsonMediaType, "PortalCatalogs", "Get", "portalCatalogId");
			config.SetSampleResponse(portalCatalogListResponse.ToJson(), jsonMediaType, "PortalCatalogs", "List");
			config.SetSampleResponse(portalCountryResponse.ToJson(), jsonMediaType, "PortalCountries", "Get", "portalCountryId");
			config.SetSampleResponse(portalCountryListResponse.ToJson(), jsonMediaType, "PortalCountries", "List");
			config.SetSampleResponse(productResponse.ToJson(), jsonMediaType, "Products", "Get", "productId");
			config.SetSampleResponse(productResponse.ToJson(), jsonMediaType, "Products", "GetWithSku", "productId", "skuId");
			config.SetSampleResponse(productListResponse.ToJson(), jsonMediaType, "Products", "List");
			config.SetSampleResponse(productListResponse.ToJson(), jsonMediaType, "Products", "ListByCatalog", "catalogId");
			config.SetSampleResponse(productListResponse.ToJson(), jsonMediaType, "Products", "ListByCategory", "categoryId");
			config.SetSampleResponse(productListResponse.ToJson(), jsonMediaType, "Products", "ListByCategoryByPromotionType", "categoryId", "promotionTypeId");
			config.SetSampleResponse(productListResponse.ToJson(), jsonMediaType, "Products", "ListByExternalIds", "externalIds");
			config.SetSampleResponse(productListResponse.ToJson(), jsonMediaType, "Products", "ListByHomeSpecials");
			config.SetSampleResponse(productListResponse.ToJson(), jsonMediaType, "Products", "ListByProductIds", "productIds");
			config.SetSampleResponse(productListResponse.ToJson(), jsonMediaType, "Products", "ListByPromotionType", "promotionTypeId");
			config.SetSampleResponse(productCategoryResponse.ToJson(), jsonMediaType, "ProductCategories", "Get", "productCategoryId");
			config.SetSampleResponse(productCategoryListResponse.ToJson(), jsonMediaType, "ProductCategories", "List");
			config.SetSampleResponse(promotionResponse.ToJson(), jsonMediaType, "Promotions", "Get", "promotionId");
			config.SetSampleResponse(promotionListResponse.ToJson(), jsonMediaType, "Promotions", "List");
			config.SetSampleResponse(promotionTypeResponse.ToJson(), jsonMediaType, "PromotionTypes", "Get", "promotionTypeId");
			config.SetSampleResponse(promotionTypeListResponse.ToJson(), jsonMediaType, "PromotionTypes", "List");
			config.SetSampleResponse(reviewResponse.ToJson(), jsonMediaType, "Reviews", "Get", "reviewId");
			config.SetSampleResponse(reviewListResponse.ToJson(), jsonMediaType, "Reviews", "List");
			config.SetSampleResponse(shippingOptionResponse.ToJson(), jsonMediaType, "ShippingOptions", "Get", "shippingOptionId");
			config.SetSampleResponse(shippingOptionListResponse.ToJson(), jsonMediaType, "ShippingOptions", "List");
			config.SetSampleResponse(shippingRuleResponse.ToJson(), jsonMediaType, "ShippingRules", "Get", "shippingRuleId");
			config.SetSampleResponse(shippingRuleListResponse.ToJson(), jsonMediaType, "ShippingRules", "List");
			config.SetSampleResponse(shippingRuleTypeResponse.ToJson(), jsonMediaType, "ShippingRuleTypes", "Get", "shippingRuleTypeId");
			config.SetSampleResponse(shippingRuleTypeListResponse.ToJson(), jsonMediaType, "ShippingRuleTypes", "List");
			config.SetSampleResponse(shippingServiceCodeResponse.ToJson(), jsonMediaType, "ShippingServiceCodes", "Get", "shippingServiceCodeId");
			config.SetSampleResponse(shippingServiceCodeListResponse.ToJson(), jsonMediaType, "ShippingServiceCodes", "List");
			config.SetSampleResponse(shippingTypeResponse.ToJson(), jsonMediaType, "ShippingTypes", "Get", "shippingTypeId");
			config.SetSampleResponse(shippingTypeListResponse.ToJson(), jsonMediaType, "ShippingTypes", "List");
			config.SetSampleResponse(skuResponse.ToJson(), jsonMediaType, "Skus", "Get", "skuId");
			config.SetSampleResponse(skuListResponse.ToJson(), jsonMediaType, "Skus", "List");
			config.SetSampleResponse(stateResponse.ToJson(), jsonMediaType, "States", "Get", "stateCode");
			config.SetSampleResponse(stateListResponse.ToJson(), jsonMediaType, "States", "List");
			config.SetSampleResponse(supplierResponse.ToJson(), jsonMediaType, "Suppliers", "Get", "supplierId");
			config.SetSampleResponse(supplierListResponse.ToJson(), jsonMediaType, "Suppliers", "List");
			config.SetSampleResponse(supplierTypeResponse.ToJson(), jsonMediaType, "SupplierTypes", "Get", "supplierTypeId");
			config.SetSampleResponse(supplierTypeListResponse.ToJson(), jsonMediaType, "SupplierTypes", "List");
			config.SetSampleResponse(taxClassResponse.ToJson(), jsonMediaType, "TaxClasses", "Get", "taxClassId");
			config.SetSampleResponse(taxClassListResponse.ToJson(), jsonMediaType, "TaxClasses", "List");
			config.SetSampleResponse(taxRuleResponse.ToJson(), jsonMediaType, "TaxRules", "Get", "taxRuleId");
			config.SetSampleResponse(taxRuleListResponse.ToJson(), jsonMediaType, "TaxRules", "List");
			config.SetSampleResponse(taxRuleTypeResponse.ToJson(), jsonMediaType, "TaxRuleTypes", "Get", "taxRuleTypeId");
			config.SetSampleResponse(taxRuleTypeListResponse.ToJson(), jsonMediaType, "TaxRuleTypes", "List");
			config.SetSampleResponse(taxRuleTypeListResponse.ToJson(), jsonMediaType, "WishLists", "Get", "wishListId");

			//// Uncomment the following to use "sample string" as the sample for all actions that have string as the body parameter or return type.
			//// Also, the string arrays will be used for IEnumerable<string>. The sample objects will be serialized into different media type 
			//// formats by the available formatters.
			//config.SetSampleObjects(new Dictionary<Type, object>
			//{
			//    {typeof(string), "sample string"},
			//    {typeof(IEnumerable<string>), new string[]{"sample 1", "sample 2"}}
			//});

			//// Uncomment the following to use "[0]=foo&[1]=bar" directly as the sample for all actions that support form URL encoded format
			//// and have IEnumerable<string> as the body parameter or return type.
			//config.SetSampleForType("[0]=foo&[1]=bar", new MediaTypeHeaderValue("application/x-www-form-urlencoded"), typeof(IEnumerable<string>));

			//// Uncomment the following to use "1234" directly as the request sample for media type "text/plain" on the controller named "Values"
			//// and action named "Put".
			//config.SetSampleRequest("1234", new MediaTypeHeaderValue("text/plain"), "Values", "Put");

			//// Uncomment the following to use the image on "../images/aspNetHome.png" directly as the response sample for media type "image/png"
			//// on the controller named "Values" and action named "Get" with parameter "id".
			//config.SetSampleResponse(new ImageSample("../images/aspNetHome.png"), new MediaTypeHeaderValue("image/png"), "Values", "Get", "id");

			//// Uncomment the following to correct the sample request when the action expects an HttpRequestMessage with ObjectContent<string>.
			//// The sample will be generated as if the controller named "Values" and action named "Get" were having string as the body parameter.
			//config.SetActualRequestType(typeof(string), "Values", "Get");

			//// Uncomment the following to correct the sample response when the action returns an HttpResponseMessage with ObjectContent<string>.
			//// The sample will be generated as if the controller named "Values" and action named "Post" were returning a string.
			//config.SetActualResponseType(typeof(string), "Values", "Post");
		}
	}
}