using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Http.Headers;
using System.Web.Http.Description;

namespace Znode.Engine.Api.Areas.HelpPage.Models
{
    /// <summary>
    /// The model that represents an API displayed on the help page.
    /// </summary>
    public class HelpPageApiModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="HelpPageApiModel"/> class.
        /// </summary>
        public HelpPageApiModel()
        {
            SampleRequests = new Dictionary<MediaTypeHeaderValue, object>();
            SampleResponses = new Dictionary<MediaTypeHeaderValue, object>();
            ErrorMessages = new Collection<string>();
	        Expands = new Dictionary<string, string>();
			Filters = new Dictionary<string, string>();
			Sorts = new Dictionary<string, string>();
			Page = new Dictionary<string, string>();
        }

        /// <summary>
        /// Gets or sets the <see cref="ApiDescription"/> that describes the API.
        /// </summary>
        public ApiDescription ApiDescription { get; set; }

        /// <summary>
        /// Gets the sample requests associated with the API.
        /// </summary>
        public IDictionary<MediaTypeHeaderValue, object> SampleRequests { get; private set; }

        /// <summary>
        /// Gets the sample responses associated with the API.
        /// </summary>
        public IDictionary<MediaTypeHeaderValue, object> SampleResponses { get; private set; }

        /// <summary>
        /// Gets the error messages associated with this model.
        /// </summary>
        public Collection<string> ErrorMessages { get; private set; }

		/// <summary>
		/// Gets the expand items associated with this model.
		/// </summary>
		public IDictionary<string, string> Expands { get; private set; }

		/// <summary>
		/// Gets the filter items associated with this model.
		/// </summary>
		public IDictionary<string, string> Filters { get; private set; }

		/// <summary>
		/// Gets the order by items associated with this model.
		/// </summary>
		public IDictionary<string, string> Sorts { get; private set; }

		/// <summary>
		/// Gets the paging items associated with this model.
		/// </summary>
		public IDictionary<string, string> Page { get; private set; }
    }
}