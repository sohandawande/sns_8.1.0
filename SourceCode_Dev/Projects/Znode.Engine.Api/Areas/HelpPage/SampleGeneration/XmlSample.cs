using System;

namespace Znode.Engine.Api.Areas.HelpPage.SampleGeneration
{
	/// <summary>
	/// This represents a preformatted XML sample on the help page. There's a display template named XmlSample associated with this class.
	/// </summary>
	public class XmlSample
	{
		public XmlSample(string xml)
		{
			if (xml == null)
			{
				throw new ArgumentNullException("xml");
			}

			Xml = xml;
		}

		public string Xml { get; private set; }

		public override bool Equals(object obj)
		{
			var other = obj as XmlSample;
			return other != null && Xml == other.Xml;
		}

		public override int GetHashCode()
		{
			return Xml.GetHashCode();
		}

		public override string ToString()
		{
			return Xml;
		}
	}
}