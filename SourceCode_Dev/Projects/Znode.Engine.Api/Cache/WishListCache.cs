﻿using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
	public class WishListCache : BaseCache, IWishListCache
	{
		private readonly IWishListService _service;

		public WishListCache(IWishListService wishListService)
		{
			_service = wishListService;
		}

		public string GetWishList(int wishListId, string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var wishList = _service.GetWishList(wishListId);
				if (wishList != null)
				{
					var response = new WishListResponse { WishList = wishList };
					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}

		public string GetWishLists(string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var list = _service.GetWishLists(Expands, Filters, Sorts, Page);
				if (list.WishLists.Count > 0)
				{
					var response = new WishListsListResponse { WishLists = list.WishLists };
					response.MapPagingDataFromModel(list);

					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}
	}
}