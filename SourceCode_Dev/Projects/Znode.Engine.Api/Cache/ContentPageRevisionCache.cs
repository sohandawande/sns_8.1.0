﻿using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
    /// <summary>
    /// Cache for content page revision.
    /// </summary>
    public class ContentPageRevisionCache : BaseCache, IContentPageRevisionCache
    {
        #region Private Readonly Variables

        private readonly IContentPageRevisionService _service; 

        #endregion

        #region Constructor

        public ContentPageRevisionCache(IContentPageRevisionService contentPageRevisionService)
        {
            _service = contentPageRevisionService;
        } 

        #endregion

        #region Public Methods

        public string GetContentPageRevisions(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetContentPageRevisions(Expands, Filters, Sorts, Page);
                if (list.ContentPageRevisions.Count > 0)
                {
                    var response = new ContentPageRevisionListResponse { ContentPageRevisions = list.ContentPageRevisions };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetContentPageRevisionOnId(int contentPageId, out int totalRowCount, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            totalRowCount = 0;
            if (Equals(data, null))
            {
                ContentPageRevisionListModel list = _service.GetContentPageRevisionsById(contentPageId, Sorts, Page, out totalRowCount);
                if (!Equals(list, null) && list.ContentPageRevisions.Count > 0)
                {
                    var response = new ContentPageRevisionListResponse { ContentPageRevisions = list.ContentPageRevisions };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }

        public string GetContentPageRevisionOnId(int contentPageId, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                ContentPageRevisionListModel list = _service.GetContentPageRevisionsById(contentPageId);
                if (!Equals(list, null) && list.ContentPageRevisions.Count > 0)
                {
                    var response = new ContentPageRevisionListResponse { ContentPageRevisions = list.ContentPageRevisions };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }
        #endregion
    }
}