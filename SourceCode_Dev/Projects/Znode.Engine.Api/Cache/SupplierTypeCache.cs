﻿using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
	public class SupplierTypeCache : BaseCache, ISupplierTypeCache
	{
		private readonly ISupplierTypeService _service;

		public SupplierTypeCache(ISupplierTypeService supplierTypeService)
		{
			_service = supplierTypeService;
		}

		public string GetSupplierType(int supplierTypeId, string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var supplierType = _service.GetSupplierType(supplierTypeId);
				if (supplierType != null)
				{
					var response = new SupplierTypeResponse { SupplierType = supplierType };
					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}

		public string GetSupplierTypes(string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var list = _service.GetSupplierTypes(Filters, Sorts, Page);
				if (list.SupplierTypes.Count > 0)
				{
					var response = new SupplierTypeListResponse { SupplierTypes = list.SupplierTypes };
					response.MapPagingDataFromModel(list);

					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}

        #region ZNode Version 8.0
        public string GetAllSupplierTypesNotInDatabase(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (data == null)
            {
                var list = _service.GetAllSupplierTypesNotInDatabase();
                if (list.SupplierTypes.Count > 0)
                {
                    var response = new SupplierTypeListResponse { SupplierTypes = list.SupplierTypes };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }
        #endregion
    }
}