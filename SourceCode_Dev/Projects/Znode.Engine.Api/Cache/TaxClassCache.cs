﻿using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
    public class TaxClassCache : BaseCache, ITaxClassCache
    {
        #region Private Variables

        private readonly ITaxClassService _service; 

        #endregion

        #region Public Methods

        public TaxClassCache(ITaxClassService taxClassService)
        {
            _service = taxClassService;
        }

        public string GetTaxClass(int taxClassId, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var taxClass = _service.GetTaxClass(taxClassId);
                if (!Equals(taxClass, null))
                {
                    var response = new TaxClassResponse { TaxClass = taxClass };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }

        public string GetTaxClasses(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetTaxClasses(Filters, Sorts, Page);
                if (list.TaxClasses.Count > 0)
                {
                    var response = new TaxClassListResponse { TaxClasses = list.TaxClasses };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }

        public string GetTaxClassList(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetTaxClassList(Filters, Sorts, Page);
                if (list.TaxClasses.Count > 0)
                {
                    var response = new TaxClassListResponse { TaxClasses = list.TaxClasses };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }

        public string GetTaxClassDetails(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var taxClass = _service.ViewTaxClassDetails(Filters, Sorts, Page);
                if (!Equals(taxClass, null))
                {
                    var response = new TaxClassResponse { TaxClass = taxClass };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }

        public string GetActiveTaxClassByPortalId(int portalId, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetActiveTaxClassByPortalId(portalId);
                if (list.TaxClasses.Count > 0)
                {
                    var response = new TaxClassListResponse { TaxClasses = list.TaxClasses };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        } 

        #endregion
    }
}