﻿
namespace Znode.Engine.Api.Cache
{
    public interface ICategoryProfileCache
    {
        /// <summary>
        /// Gets the category profiles by category profile Id.
        /// </summary>
        /// <param name="categoryProfileId">int categoryProfileId</param>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns>Returns string</returns>
        string GetCategoryProfilesByCategoryProfileId(int categoryProfileId, string routeUri, string routeTemplate);

        /// <summary>
        /// Gets the category profiles by category Id.
        /// </summary>
        /// <param name="categoryId">int categoryId</param>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns>Returns string</returns>
        string GetCategoryProfilesByCategoryId(int categoryId, string routeUri, string routeTemplate);
    }
}
