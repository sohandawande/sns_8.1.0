﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Cache
{
    public interface IPRFTCreditApplicationCache
    {
        string GetCreditApplication(int creditApplicationId, string routeUri, string routeTemplate);
        string GetCreditApplications(string routeUri, string routeTemplate);
    }
}
