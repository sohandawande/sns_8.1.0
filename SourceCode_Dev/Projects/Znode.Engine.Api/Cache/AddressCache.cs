﻿using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
	public class AddressCache : BaseCache, IAddressCache
	{
		private readonly IAddressService _service;

		public AddressCache(IAddressService addressService)
		{
			_service = addressService;
		}

		public string GetAddress(int addressId, string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var address = _service.GetAddress(addressId);
				if (address != null)
				{
					var response = new AddressResponse { Address = address };
					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}

		public string GetAddresses(string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var list = _service.GetAddresses(Filters, Sorts, Page);
				if (list.Addresses.Count > 0)
				{
					var response = new AddressListResponse { Addresses = list.Addresses };
					response.MapPagingDataFromModel(list);

					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}
	}
}