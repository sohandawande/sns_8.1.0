﻿using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;


namespace Znode.Engine.Api.Cache
{
    public class ReorderCache : BaseCache, IReorderCache
    {
        private readonly IReorderService _service;

        public ReorderCache(IReorderService reorderService)
        {
            _service = reorderService;
        }

        public string GetReorderItems(int orderId, string routeUri, string routeTemplate)
        {
            //Check Cache
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                //Get reorder items from service
                var reorderItems = _service.GetReorderItems(orderId, Expands);
                if (reorderItems != null)
                {
                    //Create response
                    var response = new ReorderResponse { ReorderItems = reorderItems };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetReorderLineItem(int orderLineItemId, string routeUri, string routeTemplate)
        {
            //Check Cache
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                //Get item from service
                var reorderSingleItems = _service.GetReorderLineItem(orderLineItemId, Expands);
                if (reorderSingleItems != null)
                {
                    //Create responce
                    var response = new ReorderResponse { Reorder = reorderSingleItems };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        //TODO : Function implementation not yet done . using this function we can manage reorder both way ie single item and complete order.
        public string GetReorderItemsList(int orderId, int orderLineItemId, bool isOrder, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {

                if (isOrder)
                {

                    var reorderItems = _service.GetReorderItems(orderId, Expands);
                    if (reorderItems != null)
                    {
                        var response = new ReorderResponse { ReorderItems = reorderItems };
                        data = InsertIntoCache(routeUri, routeTemplate, response);
                    }
                }
            }

            return data;
        }


    }
}