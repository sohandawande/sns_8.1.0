﻿
namespace Znode.Engine.Api.Cache
{
    public interface IPortalProfileCache
    {
        /// <summary>
        /// Get Portal profiles.
        /// </summary>
        /// <param name="routeUri">string routeUri</param>
        /// <param name="routeTemplate">string routeTemplate</param>
        /// <returns>string data.</returns>
        string GetPortalProfiles(string routeUri, string routeTemplate);

        /// <summary>
        /// Get portal profiles.
        /// </summary>
        /// <param name="portalProfileId">Portal profile id.</param>
        /// <param name="routeUri">string routeUri</param>
        /// <param name="routeTemplate">string routeTemplate</param>
        /// <returns>string data.</returns>
        string GetPortalProfile(int portalProfileId, string routeUri, string routeTemplate);

        /// <summary>
        /// Gets portal profile lis twith profile name and serial number.
        /// </summary>
        /// <param name="routeUri">String Route URI.</param>
        /// <param name="routeTemplate">Route Template.</param>
        /// <returns>String data.</returns>
        string GetPortalProfilesDetails(string routeUri, string routeTemplate);

        /// <summary>
        /// Get the List of PortalProfiles by portalId
        /// </summary>
        /// <param name="portalId">portalId to retrive portalProfileList</param>
        /// <param name="routeUri">string routeuri</param>
        /// <param name="routeTemplate">string routeTemplate</param>
        /// <returns>returns String</returns>
        string GetPortalProfilesByPortalId(int portalId, string routeUri, string routeTemplate);
    }
}
