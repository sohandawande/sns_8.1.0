﻿using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
	public class PortalCountryCache : BaseCache, IPortalCountryCache
	{
		private readonly IPortalCountryService _service;

		public PortalCountryCache(IPortalCountryService portalCountryService)
		{
			_service = portalCountryService;
		}

		public string GetPortalCountry(int portalCountryId, string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var portalCountry = _service.GetPortalCountry(portalCountryId, Expands);
				if (portalCountry != null)
				{
					var response = new PortalCountryResponse { PortalCountry = portalCountry };
					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}

		public string GetPortalCountries(string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var list = _service.GetPortalCountries(Expands, Filters, Sorts, Page);
				if (list.PortalCountries.Count > 0)
				{
					var response = new PortalCountryListResponse { PortalCountries = list.PortalCountries };
					response.MapPagingDataFromModel(list);

					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
        }

        #region Znode Version 8.0

        public string GetAllPortalCountries(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetAllPortalCountries(Expands, Filters, Sorts, Page);
                if (list.PortalCountries.Count > 0)
                {
                    var response = new PortalCountryListResponse { PortalCountries = list.PortalCountries };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }
        #endregion
    }
}