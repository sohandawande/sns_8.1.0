﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Newtonsoft.Json;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.UserAccount;
using Znode.Engine.Api.Configuration;
using Znode.Engine.Api.Parser;
using Znode.Engine.Services;
using ZNode.Libraries.Framework.Business;
using Znode.Engine.Services.Maps;

namespace Znode.Engine.Api.Cache
{
	public abstract class BaseCache
	{
		protected CacheConfiguration CacheConfig;
		protected QueryStringParser QueryStringParser;

		protected NameValueCollection Expands
		{
			get { return QueryStringParser.Expands; }
		}

		protected List<Tuple<string, string, string>> Filters
		{
			get { return QueryStringParser.Filters; }
		}

		protected NameValueCollection Sorts
		{
			get { return QueryStringParser.Sorts; }
		}

		protected NameValueCollection Page
		{
			get { return QueryStringParser.Page; }
		}

		protected NameValueCollection Cache
		{
			get { return QueryStringParser.Cache; }
		}

		protected bool RefreshCache
		{
			get
			{
				var refreshCache = false;

				if (Cache.HasKeys())
				{
					if (!String.IsNullOrEmpty(Cache.Get("refresh")))
					{
						refreshCache = true;
					}
				}

				return refreshCache;
			}
		}

		protected BaseCache()
		{
			CacheConfig = (CacheConfiguration)ConfigurationManager.GetSection("znodeApiCache");
			if (CacheConfig == null)
			{
				throw new ConfigurationErrorsException("Configuration section for znodeApiCache does not exist.");
			}

			QueryStringParser = new QueryStringParser(HttpContext.Current.Request.Url.Query);
		}

		protected string GetFromCache(string routeUri)
		{
			// IMPORTANT: Must remove cache=refresh from the route
			routeUri = RemoveCacheParameterFromRouteUri(routeUri);

			// If cache=refresh for the route then remove the value from cache so that the
			// call is forced to retrieve data from the service and re-insert into cache.
			if (RefreshCache) HttpRuntime.Cache.Remove(routeUri);

			// If cache=refresh was used then this will always return null, which is the
			// expected result.
			return HttpRuntime.Cache.Get(routeUri) as string;
		}

		protected string InsertIntoCache(string routeUri, string routeTemplate, object data)
		{
			// IMPORTANT: Must remove cache=refresh from the route
			routeUri = RemoveCacheParameterFromRouteUri(routeUri);

			// We serialize the data as a string because we want to store just the JSON string
			// itself in the cache, not its response object. This is a big performance gain.
			var json = JsonConvert.SerializeObject(data);

			// Otherwise check if caching is enabled overall and for the route itself
			if (CacheConfig.Enabled)
			{
				var cacheRoute = GetCacheRoute(routeTemplate);
				if (cacheRoute != null)
				{
					if (cacheRoute.Enabled)
					{
						if (cacheRoute.Sliding)
						{
							HttpRuntime.Cache.Insert(routeUri, json, null, System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromSeconds(cacheRoute.Duration));
						}
						else
						{
							// IMPORTANT: Use UtcNow to avoid issues with local time, such as changes for daylight savings
							HttpRuntime.Cache.Insert(routeUri, json, null, DateTime.UtcNow.AddSeconds(cacheRoute.Duration), System.Web.Caching.Cache.NoSlidingExpiration);
						}
					}
				}
			}

			return json;
		}

		protected CacheRoute GetCacheRoute(string routeTemplate)
		{
			if (!String.IsNullOrEmpty(routeTemplate))
			{
				foreach (var item in CacheConfig.CacheRoutes)
				{
					if (item.Template == routeTemplate)
					{
						return item;
					}
				}
			}

			return null;
		}

		/// <summary>
		/// Gets the account ID and portal ID from the request headers and puts the profile into session and cache.
		/// </summary>
		/// <returns></returns>
		protected string UpdateCacheForProfile()
		{
			const string headerAccountId = "Znode-AccountId";

			var accountId = 0;
			var portalId = 1;

			// Get account ID and domain name from request header
			var headers = HttpContext.Current.Request.Headers;
			int.TryParse(headers[headerAccountId], out accountId);
			var domainName = GetDomainNameFromAuthHeader();

			if (!String.IsNullOrEmpty(domainName))
			{
				var domain = ZNodeConfigManager.GetSiteConfigFromCache(domainName);
				if (domain != null)
				{
					portalId = domain.PortalID;
				}
			}

			// If we have account ID then get customer profile
			if (accountId > 0)
			{
				GetCustomerProfile(portalId, accountId);

				return accountId.ToString(CultureInfo.InvariantCulture);
			}

			// Otherwise use default profile
			GetDefaultProfile(portalId);
			return String.Empty;
		}

		private string GetDomainNameFromAuthHeader()
		{
			var headers = HttpContext.Current.Request.Headers;
			var authValue = headers.AllKeys.Contains("Authorization") ? headers["Authorization"] : String.Empty;

			// If auth value doesn't exist, return empty
			if (String.IsNullOrEmpty(authValue)) return String.Empty;

			// Strip off the "Basic "
			authValue = authValue.Remove(0, 6);

			// Decode it; if empty return empty
			var authValueDecoded = DecodeBase64(authValue);
			if (String.IsNullOrEmpty(authValueDecoded)) return String.Empty;

			// Now split it to get the domain info (index 0 = domain name, index 1 = domain key)
			var domainInfo = authValueDecoded.Split('|');

			return !String.IsNullOrEmpty(domainInfo[0]) ? domainInfo[0] : String.Empty;
		}

		private string DecodeBase64(string encodedValue)
		{
			var encodedValueAsBytes = Convert.FromBase64String(encodedValue);
			return Encoding.UTF8.GetString(encodedValueAsBytes);
		}

		private void GetCustomerProfile(int portalId, int accountId)
		{
			if (HttpContext.Current.Session["ProfileCache"] == null)
			{
				// Session is going to always be null so check cache
				if (HttpContext.Current.Cache["ProfileCache_" + accountId] != null)
				{
					HttpContext.Current.Session["ProfileCache"] = HttpContext.Current.Cache["ProfileCache_" + accountId];
				}
				else
				{
					var accountService = new AccountService();                                        

					var profile = accountService.GetCustomerProfile(accountId, portalId);
					if (profile != null)
					{
						HttpContext.Current.Session["ProfileCache"] = profile;
						HttpContext.Current.Cache["ProfileCache_" + accountId] = profile;
					}
				}                
			}

            // Added to validate the gift card and tax services.
            var expands = new NameValueCollection();
            expands.Add("addresses", "addresses");
            var model = new AccountService().GetAccount(accountId, expands);
            if (model != null)
            {
                var account = AccountMap.ToEntity(model);
                account.AddressCollection = new TList<Address>(model.Addresses.Select(AddressMap.ToEntity).ToList());
                System.Web.HttpContext.Current.Session["AliasUserAccount"] = new ZNodeUserAccount(account);
            }
		}

		private void GetDefaultProfile(int portalId)
		{
			if (HttpContext.Current.Session["ProfileCache"] == null)
			{
				// Session is going to always be null so check cache
				if (HttpContext.Current.Cache["ProfileCache"] != null)
				{
					HttpContext.Current.Session["ProfileCache"] = HttpContext.Current.Cache["ProfileCache"];
				}
				else
				{
					var accountService = new AccountService();

					var profile = accountService.GetCustomerProfile(0, portalId);
					if (profile != null)
					{
						HttpContext.Current.Session["ProfileCache"] = profile;
						HttpContext.Current.Cache["ProfileCache"] = profile;
					}
				}
			}
		}

		private string RemoveCacheParameterFromRouteUri(string routeUri)
		{
			// Gotta check to remove it both ways
			routeUri = routeUri.Replace("?cache=refresh", "");
			routeUri = routeUri.Replace("&cache=refresh", "");
			return routeUri;
		}
	}
}