﻿namespace Znode.Engine.Api.Cache
{
    /// <summary>
    /// 
    /// </summary>
    public interface IUrlRedirectCache
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="urlRedirectId"></param>
        /// <param name="routeUri"></param>
        /// <param name="routeTemplate"></param>
        /// <returns></returns>
        string GetUrlRedirect(int urlRedirectId, string routeUri, string routeTemplate);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="routeUri"></param>
        /// <param name="routeTemplate"></param>
        /// <returns></returns>
        string GetUrlRedirects(string routeUri, string routeTemplate);
    }
}