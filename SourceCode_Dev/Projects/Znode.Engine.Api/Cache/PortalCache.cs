﻿using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
    public class PortalCache : BaseCache, IPortalCache
    {
        private readonly IPortalService _service;

        public PortalCache(IPortalService portalService)
        {
            _service = portalService;
        }

        public string GetPortal(int portalId, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (data == null)
            {
                var portal = _service.GetPortal(portalId, Expands);
                if (portal != null)
                {
                    var response = new PortalResponse { Portal = portal };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetPortals(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (data == null)
            {
                var list = _service.GetPortals(Expands, Filters, Sorts, Page);
                if (list.Portals.Count > 0)
                {
                    var response = new PortalListResponse { Portals = list.Portals };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetPortalsByPortalIds(string portalIds, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (data == null)
            {
                var list = _service.GetPortalsByPortalIds(portalIds, Expands, Sorts);
                if (list.Portals.Count > 0)
                {
                    var response = new PortalListResponse { Portals = list.Portals };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetPortalInformationByPortalId(int portalId, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (data == null)
            {
                var portalInfo = _service.GetPortalInformationByPortalId(portalId);
                if (!Equals(portalInfo, null))
                {
                    var response = new PortalResponse { Portal = portalInfo };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        #region Znode Version 8.0
        public string GetFedexKeys(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data,null))
            {
                var fedexKeys = _service.GetFedexKey();
                if (!Equals(fedexKeys, null) && fedexKeys.Tables.Count > 0 && fedexKeys.Tables[0].Rows.Count > 0)
                {
                    var response = new FedexKeysResponse { fedexkeys = fedexKeys };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetPortalListByProfileAccess(string routeUri,string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data,null))
            {
                var list = _service.GetPortalsByProfileAccess(Expands, Filters, Sorts, Page);
                if (list.Portals.Count > 0)
                {
                    var response = new PortalListResponse { Portals = list.Portals };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }
        #endregion
    }
}