﻿using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Engine.Api.Cache
{
	public interface IShoppingCartCache
	{
		ShoppingCartResponse GetCartByAccount(int accountId, int portalId);
		ShoppingCartResponse GetCartByCookie(int cookieId);
		ShoppingCartResponse CreateCart(int portalId, ShoppingCartModel model);
		ShoppingCartResponse Calculate(ShoppingCartModel model);
	}
}
