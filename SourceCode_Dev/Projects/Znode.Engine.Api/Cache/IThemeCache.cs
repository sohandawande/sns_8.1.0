﻿
namespace Znode.Engine.Api.Cache
{
    public interface IThemeCache
    {
        /// <summary>
        /// Gets theme List.
        /// </summary>
        /// <param name="routeUri">Route URI.</param>
        /// <param name="routeTemplate">Route template.</param>
        /// <returns>String Data.</returns>
        string GetThemes(string routeUri, string routeTemplate);

        /// <summary>
        /// Get theme by themeId
        /// </summary>
        /// <param name="themeId">themeId to get theme</param>
        /// <param name="routeUri">route URL</param>
        /// <param name="routeTemplate">route Template</param>
        /// <returns>data in string format</returns>
        string GetTheme(int themeId, string routeUri, string routeTemplate);
    }
}
