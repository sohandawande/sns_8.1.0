﻿namespace Znode.Engine.Api.Cache
{
    public interface IOrderCache
    {
        /// <summary>
        /// Get orders by order id
        /// </summary>
        /// <param name="orderId">int id of the order</param>
        /// <param name="routeUri">String routeUri</param>
        /// <param name="routeTemplate">String routeTemplate</param>
        /// <returns>Returns string result.</returns>
        string GetOrder(int orderId, string routeUri, string routeTemplate);

        /// <summary>
        /// Get Orders list.
        /// </summary>
        /// <param name="routeUri">String routeUri</param>
        /// <param name="routeTemplate">String routeTemplate</param>
        /// <returns>Returns string result.</returns>
        string GetOrders(string routeUri, string routeTemplate);

        /// <summary>
        /// Gets order list form db using sp.
        /// ZNode Version 8.0
        /// </summary>
        /// <param name="routeUri">String routeUri</param>
        /// <param name="routeTemplate">String routeTemplate</param>
        /// <returns>Returns string result.</returns>
        string GetOrderList(string routeUri, string routeTemplate);

        /// <summary>
        /// Gets order details from db using sp.
        /// ZNode Version 8.0
        /// </summary>
        /// <param name="routeUri">String routeUri</param>
        /// <param name="routeTemplate">String routeTemplate</param>
        /// <returns>Returns string result.</returns>
        string GetOrderDetails(string routeUri, string routeTemplate);

        /// <summary>
        /// Downloads order data from db using sp.
        /// ZNode Version 8.0
        /// </summary>
        /// <param name="routeUri">String routeUri</param>
        /// <param name="routeTemplate">String routeTemplate</param>
        /// <returns>Returns string result.</returns>
        string DownloadOrder(string routeUri, string routeTemplate);

        /// <summary>
        /// Downloads order line items data from db using sp.
        /// ZNode Version 8.0
        /// </summary>
        /// <param name="routeUri">String routeUri</param>
        /// <param name="routeTemplate">String routeTemplate</param>
        /// <returns>Returns string result.</returns>
        string DownloadOrderLineItems(string routeUri, string routeTemplate);

        /// <summary>
        /// Get order line item details on the basis of order line item id.
        /// </summary>
        /// <param name="orderLineItemId">int id of order line item</param>
        /// <param name="routeUri">String routeUri</param>
        /// <param name="routeTemplate">String routeTemplate</param>
        /// <returns>Returns order line item details</returns>
        string GetOrderLineItemDetails(int orderLineItemId, string routeUri, string routeTemplate);

        /// <summary>
        /// Sends email for the shipped status change and also tracking number of order line item.
        /// </summary>
        /// <param name="routeUri">RouteUri</param>
        /// <param name="routeTemplate">RouteTemplate</param>
        /// <returns>Returns message whether the email was sent or not.</returns>
        string SendEmail(string routeUri, string routeTemplate);

        string GetInvoiceDetails(string externalId, string routeUri, string routeTemplate);
    }
}