﻿using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
	public class ShippingTypeCache : BaseCache, IShippingTypeCache
	{
		private readonly IShippingTypeService _service;

		public ShippingTypeCache(IShippingTypeService shippingTypeService)
		{
			_service = shippingTypeService;
		}

		public string GetShippingType(int shippingTypeId, string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var shippingType = _service.GetShippingType(shippingTypeId);
				if (shippingType != null)
				{
					var response = new ShippingTypeResponse { ShippingType = shippingType };
					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}

		public string GetShippingTypes(string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var list = _service.GetShippingTypes(Filters, Sorts, Page);
				if (list.ShippingTypes.Count > 0)
				{
					var response = new ShippingTypeListResponse { ShippingTypes = list.ShippingTypes };
					response.MapPagingDataFromModel(list);

					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}

        #region ZNode Version 8.0
        public string GetAllShippingTypesNotInDatabase(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (data == null)
            {
                var list = _service.GetAllShippingTypesNotInDatabase();
                if (list.ShippingTypes.Count > 0)
                {
                    var response = new ShippingTypeListResponse { ShippingTypes = list.ShippingTypes };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }
        #endregion
	}
}