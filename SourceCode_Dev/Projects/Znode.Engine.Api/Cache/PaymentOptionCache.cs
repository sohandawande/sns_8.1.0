﻿using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
	public class PaymentOptionCache : BaseCache, IPaymentOptionCache
	{
		private readonly IPaymentOptionService _service;

		public PaymentOptionCache(IPaymentOptionService paymentOptionService)
		{
			_service = paymentOptionService;
		}

		public string GetPaymentOption(int paymentOptionId, string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var paymentOption = _service.GetPaymentOption(paymentOptionId, Expands);
				if (paymentOption != null)
				{
					var response = new PaymentOptionResponse { PaymentOption = paymentOption };
					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}

		public string GetPaymentOptions(string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var list = _service.GetPaymentOptions(Expands, Filters, Sorts, Page);
				if (list.PaymentOptions.Count > 0)
				{
					var response = new PaymentOptionListResponse { PaymentOptions = list.PaymentOptions };
					response.MapPagingDataFromModel(list);

					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}
	}
}