﻿
namespace Znode.Engine.Api.Cache
{
    public interface IVendorAccountCache
    {
        /// <summary>
        /// This method will fetch all the vendor accounts
        /// </summary>
        /// <param name="routeUri">string Route URI</param>
        /// <param name="routeTemplate">string Route Template</param>
        /// <returns>Retunrs the vendor account details</returns>
        string GetVendorAccountList(string routeUri, string routeTemplate);

        /// <summary>
        /// Get vendor account by account id
        /// </summary>
        /// <param name="accountId">Int accountId to get vendor account id</param>
        /// <param name="routeUri">string Route URI</param>
        /// <param name="routeTemplate">string Route Template</param>
        /// <returns>Retunrs the vendor account details</returns>
        string GetVendorAccountById(int accountId, string routeUri, string routeTemplate);
    }
}