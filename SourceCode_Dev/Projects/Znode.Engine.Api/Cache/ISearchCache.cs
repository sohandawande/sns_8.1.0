﻿using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Engine.Api.Cache
{
	public interface ISearchCache
	{
		KeywordSearchResponse GetKeywordSearch(KeywordSearchModel model, string routeUri, string routeTemplate);
		SuggestedSearchListResponse GetSearchSuggestions(SuggestedSearchModel model, string routeUri, string routeTemplate);
		void ReloadIndex();

        /// <summary>
        /// Get the Seo Url Details.
        /// </summary>
        /// <param name="seoUrl">string seoUrl</param>
        /// <param name="routeUri">string routeUri</param>
        /// <param name="routeTemplate">string routeTemplate</param>
        /// <returns>Return Seo Url Details based on provided seoUrl.</returns>
        string GetSeoUrlDetail(string seoUrl, string routeUri, string routeTemplate);
	}
}