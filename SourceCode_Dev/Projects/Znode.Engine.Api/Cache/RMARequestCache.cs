﻿using Newtonsoft.Json;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
    public class RMARequestCache : BaseCache, IRMARequestCache
    {
        #region Private Variables
        private readonly IRMARequestService _service;
        #endregion

        #region Constructor
        public RMARequestCache(IRMARequestService rmaRequestService)
        {
            _service = rmaRequestService;
        }
        #endregion

        #region Public Methods
        public string GetRMARequests(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetRMARequestList(Expands, Filters, Sorts, Page);
                if (list.RMARequests.Count > 0)
                {
                    var response = new RMARequestListResponse { RMARequestList = list.RMARequests };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetRMARequest(int rmaRequestid, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var rmaRequest = _service.GetRMARequest(rmaRequestid);
                if (!Equals(rmaRequest, null))
                {
                    var response = new RMARequestResponse { RMARequest = rmaRequest };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetOrderRMAFlag(int orderId, string routeUri, string routeTemplate)
        {
            string data = string.Empty;
            bool isEnabled = _service.GetOrderRMAFlag(orderId);
            BooleanModel boolModel = new BooleanModel { disabled = isEnabled };
            TrueFalseResponse response = new TrueFalseResponse { booleanModel = boolModel };
            data = JsonConvert.SerializeObject(response);
            return data;
        }

        public string GetIssuedGiftCards(int rmaRequestId, string routeUri, string routeTemplate)
        {
            string data = GetFromCache(routeUri);
            
            if (Equals(data, null))
            {
                IssuedGiftCardListModel issuedGiftCardModels = _service.GetRMAGiftCardDetails(rmaRequestId);
                if (!Equals(issuedGiftCardModels, null))
                {
                    IssuedGiftCardListResponse response = new IssuedGiftCardListResponse { IssuedGiftCards = issuedGiftCardModels.IssuedGiftCardModels };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }
        #endregion
    }
}