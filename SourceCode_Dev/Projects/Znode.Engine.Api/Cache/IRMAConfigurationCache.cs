﻿
namespace Znode.Engine.Api.Cache
{
    public interface IRMAConfigurationCache
    {
        /// <summary>
        /// Method returns RMAConfiguration details.
        /// </summary>
        /// <param name="profileId">RMAConfiguration ID</param>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns>Return rmaconfiguration details.</returns>
        string GetRMAConfiguration(int rmaConfigId, string routeUri, string routeTemplate);

        /// <summary>
        /// Method returns RMAConfiguration List.
        /// </summary>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns>Return the RMAConfiguration list.</returns>
        string GetRMAConfigurations(string routeUri, string routeTemplate);

        /// <summary>
        /// To GetAllRMAConfiguration
        /// </summary>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns>Return the RMAConfiguration all list</returns>
        string GetAllRMAConfiguration(string routeUri, string routeTemplate);
    }
}
