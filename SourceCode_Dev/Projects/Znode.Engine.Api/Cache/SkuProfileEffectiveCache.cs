﻿using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
    /// <summary>
    /// Cache for Sku profile effective
    /// </summary>
    public class SkuProfileEffectiveCache : BaseCache, ISkuProfileEffectiveCache
    {
        #region Private Variable
        private readonly ISkuProfileEffectiveService _service; 
        #endregion

        #region Constructor
        public SkuProfileEffectiveCache(ISkuProfileEffectiveService skuProfileEffectiveService)
        {
            _service = skuProfileEffectiveService;
        } 
        #endregion

        #region Public Methods

        public string GetSkuProfileEffective(int skuProfileEffectiveId, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var skuProfileEffective = _service.GetSkuProfileEffective(skuProfileEffectiveId, Expands);
                if (!Equals(skuProfileEffective, null))
                {
                    var response = new SkuProfileEffectiveResponse { SkuProfileEffective = skuProfileEffective };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetSkuProfileEffectives(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetSkuProfileEffectives(Expands,Filters, Sorts, Page);
                if (list.SkuProfileEffectives.Count > 0)
                {
                    var response = new SkuProfileEffectiveListResponse { SkuProfileEffectives = list.SkuProfileEffectives };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetSkuProfileEffectivesBySkuId(int skuId,string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetSkuProfileEffectivesBySkuId(Filters, Sorts, Page); 
                if (list.SkuProfileEffectives.Count > 0)
                {
                    var response = new SkuProfileEffectiveListResponse { SkuProfileEffectives = list.SkuProfileEffectives };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        } 

        #endregion
    }
}