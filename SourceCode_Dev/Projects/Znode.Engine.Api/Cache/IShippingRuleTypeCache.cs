﻿namespace Znode.Engine.Api.Cache
{
    /// <summary>
    /// Shipping Rule Type Cache Interface
    /// </summary>
	public interface IShippingRuleTypeCache
	{
        /// <summary>
        /// Get Shipping Rule Type on the basis of shippingRuleTypeId
        /// </summary>
        /// <param name="shippingRuleTypeId">shippingRuleTypeId</param>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns>Returns shipping rule type</returns>
		string GetShippingRuleType(int shippingRuleTypeId, string routeUri, string routeTemplate);

        /// <summary>
        /// Get list of shipping rule types 
        /// </summary>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns>Returns list of shipping rule types</returns>
		string GetShippingRuleTypes(string routeUri, string routeTemplate);
	}
}