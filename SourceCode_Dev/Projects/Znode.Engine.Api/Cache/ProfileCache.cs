﻿using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
    public class ProfileCache : BaseCache, IProfileCache
    {
        #region Private Variables
        private readonly IProfileService _service; 
        #endregion

        #region Constructor
        /// <summary>
        /// constructor for profile service
        /// </summary>
        /// <param name="profileService"></param>
        public ProfileCache(IProfileService profileService)
        {
            _service = profileService;
        } 
        #endregion

        #region Public Methods

        public string GetProfile(int profileId, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var profile = _service.GetProfile(profileId, Expands);
                if (!Equals(profile, null))
                {
                    var response = new ProfileResponse { Profile = profile };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }

        public string GetProfiles(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetProfiles(Expands, Filters, Sorts, Page);
                if (list.Profiles.Count > 0)
                {
                    var response = new ProfileListResponse { Profiles = list.Profiles };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }

        public string GetProfileListByProfileId(int portalId, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetProfileListByProfileId(portalId);
                if (list.Profiles.Count > 0)
                {
                    var response = new ProfileListResponse { Profiles = list.Profiles };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }

        public string GetCustomerProfile(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetCustomerProfile(Expands, Filters, Sorts, Page);
                if (list.Profiles.Count > 0)
                {
                    var response = new ProfileListResponse { Profiles = list.Profiles };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetAccountNotAssociatedProfile(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetAccountNotAssociatedProfiles (Expands, Filters, Sorts, Page);
                if (list.Profiles.Count > 0)
                {
                    var response = new ProfileListResponse { Profiles = list.Profiles };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetProfileListByPortalId(int portalId, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetProfileListByPortalId(portalId);
                if (!Equals(list,null)&&!Equals(list.Profiles,null)&&list.Profiles.Count > 0)
                {
                    var response = new ProfileListResponse { Profiles = list.Profiles };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }

        public string GetAvailableProfileListBySkuIdCategoryId(AssociatedSkuCategoryProfileModel model, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetAvailableProfileListBySkuIdCategoryId(model);
                if (!Equals(list, null))
                {
                    var response = new ProfileListResponse { Profiles = list.Profiles };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetZNodeProfile(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var profile = _service.GetZNodeProfile();
                if (!Equals(profile, null))
                {
                    var response = new ProfileResponse { Profile = profile };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }

        public string GetProfilesAssociatedWithUsers(string userName, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetProfilesAssociatedWithUsers(userName);
                if (list.Profiles.Count > 0)
                {
                    var response = new ProfileListResponse { Profiles = list.Profiles };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }

        #endregion        
    }
}