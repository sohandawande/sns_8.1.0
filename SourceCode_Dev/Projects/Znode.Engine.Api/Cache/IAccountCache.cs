﻿using System;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Cache
{
	public interface IAccountCache
	{
        /// <summary>
        /// This method will get the account details by account id
        /// </summary>
        /// <param name="accountId">int Account ID</param>
        /// <param name="routeUri">string Route URI</param>
        /// <param name="routeTemplate">string Route Template</param>
        /// <returns>Retunrs the account details</returns>
		string GetAccount(int accountId, string routeUri, string routeTemplate);

        /// <summary>
        /// This method will get the account details by user id
        /// </summary>
        /// <param name="userId">Guid User Id/param>
        /// <param name="routeUri">string Route URI</param>
        /// <param name="routeTemplate">string Route Template</param>
        /// <returns>Retunrs the account details</returns>
		string GetAccountByUserId(Guid userId, string routeUri, string routeTemplate);

        /// <summary>
        /// This method will get the account details by user name
        /// </summary>
        /// <param name="username">string User Name/param>
        /// <param name="routeUri">string Route URI</param>
        /// <param name="routeTemplate">string Route Template</param>
        /// <returns>Retunrs the account details</returns>
	    string GetAccountByUsername(string username, string routeUri, string routeTemplate);

        /// <summary>
        /// This method will fetch all the account details
        /// </summary>
        /// <param name="routeUri">string Route URI</param>
        /// <param name="routeTemplate">string Route Template</param>
        /// <returns>Retunrs the account details</returns>
		string GetAccounts(string routeUri, string routeTemplate);

        /// <summary>
        /// This method will get the account details by user role
        /// </summary>
        /// <param name="roleName">string User role name/param>
        /// <param name="totalRowCount">Out int Total number of rows/param>
        /// <param name="routeUri">string Route URI</param>
        /// <param name="routeTemplate">string Route Template</param>
        /// <returns>Retunrs the account details</returns>
        string GetAccountDetailsByRoleName(string roleName, out int totalRowCount, string routeUri, string routeTemplate);

        /// <summary>
        /// This method will useful for the login purpose
        /// </summary>
        /// <param name="portalId">int Portal Id</param>
        /// <param name="model">AccountModel model</param>
        /// <param name="errorCode">out string error code</param>
        /// <returns></returns>
        AccountModel Login(int portalId, AccountModel model, out string errorCode);

        /// <summary>
        /// This method will Enable/Disable the account
        /// </summary>
        /// <param name="accountId">int Account Id</param>
        /// <param name="portalId">int Portal Id</param>
        /// <returns></returns>
        string EnableDisableAccount(int accountId);

        /// <summary>
        /// Get the List of Account Payments
        /// </summary>
        /// <param name="routeUri">string Route URI</param>
        /// <param name="routeTemplate">string Route Template</param>
        /// <returns>Returns details of all the Account Payments</returns>
        string GetAccountPaymentList(int accountId, string routeUri, string routeTemplate);

        /// <summary>
        /// Validate the Social User Login
        /// </summary>
        /// <param name="portalId">integer Portal ID</param>
        /// <param name="model">SocialLoginModel</param>
        /// <returns>return details in AccountModel format</returns>
        AccountModel SocialUserLogin(int portalId, SocialLoginModel model);

        #region PRFT Custom code : Start
        /// <summary>
        /// Get Sub User List
        /// </summary>
        /// <param name="routeUri">string Route URI</param>
        /// <param name="routeTemplate">string Route Template</param>
        /// <returns>Returns details of all the Account Payments</returns>
        string GetSubUserList(string routeUri, string routeTemplate);
        #endregion
    }
}