﻿using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
	public class PaymentTypeCache : BaseCache, IPaymentTypeCache
	{
		private readonly IPaymentTypeService _service;

		public PaymentTypeCache(IPaymentTypeService paymentTypeService)
		{
			_service = paymentTypeService;
		}

		public string GetPaymentType(int paymentTypeId, string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var paymentType = _service.GetPaymentType(paymentTypeId);
				if (paymentType != null)
				{
					var response = new PaymentTypeResponse { PaymentType = paymentType };
					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}

		public string GetPaymentTypes(string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var list = _service.GetPaymentTypes(Filters, Sorts, Page);
				if (list.PaymentTypes.Count > 0)
				{
					var response = new PaymentTypeListResponse { PaymentTypes = list.PaymentTypes };
					response.MapPagingDataFromModel(list);

					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}
	}
}