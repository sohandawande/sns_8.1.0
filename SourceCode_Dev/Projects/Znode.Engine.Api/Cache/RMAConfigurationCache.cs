﻿using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
    public class RMAConfigurationCache : BaseCache, IRMAConfigurationCache
    {
        #region Private Variables
        private readonly IRMAConfigurationService _service; 
        #endregion

        #region Constructor
        public RMAConfigurationCache(IRMAConfigurationService rmaConfigurationService)
        {
            _service = rmaConfigurationService;
        } 
        #endregion

        #region Public Methods
        public string GetRMAConfiguration(int rmaConfigId, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var rmaConfiguration = _service.GetRMAConfiguration(rmaConfigId, Expands);
                if (!Equals(rmaConfiguration, null))
                {
                    var response = new RMAConfigurationResponse { RMAConfiguraton = rmaConfiguration };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }

        public string GetRMAConfigurations(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetRMAConfigurations();
                if (list.RMAConfigurations.Count > 0)
                {
                    var response = new RMAConfigurationListResponse { RMAConfiguratons = list.RMAConfigurations };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }

        public string GetAllRMAConfiguration(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetAllRMAConfiguration();
                if (list.RMAConfigurations.Count > 0)
                {
                    var response = new RMAConfigurationListResponse { RMAConfiguratons = list.RMAConfigurations };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        } 
        #endregion
    }
}