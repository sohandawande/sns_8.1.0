﻿using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
    /// <summary>
    /// Zip Code Cache
    /// </summary>
    public class ZipCodeCache : BaseCache, IZipCodeCache
    {
        #region Private Variables

        private readonly IZipCodeService _service; 

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor for Zip Code 
        /// </summary>
        /// <param name="zipCodeService"></param>
        public ZipCodeCache(IZipCodeService zipCodeService)
        {
            _service = zipCodeService;
        }

        #endregion

        #region Public Method

        public string GetZipCodes(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetZipCodes(Filters, Sorts, Page);
                if (list.ZipCode.Count > 0)
                {
                    var response = new ZipCodeListResponse { ZipCode = list.ZipCode };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        } 

        #endregion
    }
}