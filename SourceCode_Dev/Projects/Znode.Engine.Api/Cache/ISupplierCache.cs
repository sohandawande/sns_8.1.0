﻿namespace Znode.Engine.Api.Cache
{
	public interface ISupplierCache
	{
		string GetSupplier(int supplierId, string routeUri, string routeTemplate);
		string GetSuppliers(string routeUri, string routeTemplate);
	}
}