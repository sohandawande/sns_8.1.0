﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;
using Znode.Engine.Api.Models.Extensions;

namespace Znode.Engine.Api.Cache
{
    public class PRFTCreditApplicationCache : BaseCache, IPRFTCreditApplicationCache
    {
        private readonly IPRFTCreditApplicationService _service;

        public PRFTCreditApplicationCache(IPRFTCreditApplicationService creditApplicationService)
        {
            _service = creditApplicationService;
        }

        public string GetCreditApplication(int creditApplicationId, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var creditApplication = _service.GetCreditApplication(creditApplicationId,Expands);
                if (!Equals(creditApplication, null))
                {
                    var response = new PRFTCreditApplicationResponse { CreditApplication = creditApplication };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetCreditApplications(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetCreditApplications(Expands, Filters, Sorts, Page);
                if (list.CreditApplications.Count > 0)
                {
                    var response = new PRFTCreditApplicationListResponse { CreditApplications = list.CreditApplications };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }
    }
}