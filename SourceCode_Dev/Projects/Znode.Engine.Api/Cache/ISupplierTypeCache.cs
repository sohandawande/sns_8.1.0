﻿namespace Znode.Engine.Api.Cache
{
	public interface ISupplierTypeCache
	{
		string GetSupplierType(int supplierTypeId, string routeUri, string routeTemplate);
		string GetSupplierTypes(string routeUri, string routeTemplate);

        /// <summary>
        /// ZNode Version 8.0
        /// Get all Supplier Types not present in database.
        /// </summary>
        /// <param name="routeUri">string routeUri</param>
        /// <param name="routeTemplate">string routeTemplate</param>
        /// <returns></returns>
        string GetAllSupplierTypesNotInDatabase(string routeUri, string routeTemplate);
	}
}