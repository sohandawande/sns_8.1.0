﻿
namespace Znode.Engine.Api.Cache
{
    public interface IProductSearchSettingCache
    {
        /// <summary>
        /// Get product level settings
        /// </summary>
        /// <param name="routeUri">string routeUri</param>
        /// <param name="routeTemplate">string routeTemplate</param>
        /// <returns></returns>
        string GetProductLevelSettings(string routeUri, string routeTemplate);

        /// <summary>
        /// Get category level settings
        /// </summary>
        /// <param name="routeUri">string routeUri</param>
        /// <param name="routeTemplate">string routeTemplate</param>
        /// <returns></returns>
        string GetCategoryLevelSettings(string routeUri, string routeTemplate);

        /// <summary>
        /// Get field level settings
        /// </summary>
        /// <param name="routeUri">string routeUri</param>
        /// <param name="routeTemplate">string routeTemplate</param>
        /// <returns></returns>
        string GetFieldLevelSettings(string routeUri, string routeTemplate);
    }
}
