﻿using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Cache
{
    /// <summary>
    /// Interface for Product Type Attribute Cache
    /// </summary>
    public interface IProductTypeAttributeCache
    {
        /// <summary>
        /// Get Attribute Type
        /// </summary>
        /// <param name="productTypeId">Product Type Id</param>
        /// <param name="routeUri">route url</param>
        /// <param name="routeTemplate">string url</param>
        /// <returns>string</returns>
        string GetAttributeType(int productTypeId, string routeUri, string routeTemplate);

        /// <summary>
        /// Get Attribute Types
        /// </summary>
        /// <param name="routeUri">route url</param>
        /// <param name="routeTemplate">string url</param>
        /// <returns>string</returns>
        string GetAttributeTypes(string routeUri, string routeTemplate);

        /// <summary>
        /// Gets the attribute Type list by productType Id using custom service.
        /// </summary>
        /// <param name="routeUri">route url</param>
        /// <param name="routeTemplate">string url</param>
        /// <returns>ProductTypeAssociatedAttributeTypesListModel</returns>
        ProductTypeAssociatedAttributeTypesListModel GetAttributeTypeByProductTypeIdUsingCustomService(string routeUri, string routeTemplate);
    }
}