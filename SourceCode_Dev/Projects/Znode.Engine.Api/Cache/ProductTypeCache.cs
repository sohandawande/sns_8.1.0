﻿using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
    public class ProductTypeCache : BaseCache, IProductTypeCache
    {
        #region Private variables
        private readonly IProductTypeService _service;
        #endregion

        #region Public constructor
        public ProductTypeCache(IProductTypeService productTypeService)
        {
            _service = productTypeService;
        }
        #endregion

        #region Public methods


        public string GetProductType(int productTypeId, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var productType = _service.GetProductType(productTypeId);
                if (!Equals(productType, null))
                {
                    var response = new ProductTypeResponse { ProductType = productType };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }

        public string GetProductTypes(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetProductTypes(Filters, Sorts, Page);
                if (list.ProductTypes.Count > 0)
                {
                    var response = new ProductTypeListResponse { ProductTypes = list.ProductTypes };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }

        public string GetProductAttributesByProductTypeId(string routeUri, string routeTemplate)
        {
            var accountId = UpdateCacheForProfile();

            var data = GetFromCache(routeUri + accountId);
            if (Equals(data, null))
            {
                var list = _service.GetProductAttributesByProductTypeId(Expands, Filters, Sorts, Page);
                if (list.AttributeTypeValueList.Count > 0)
                {
                    var response = new ProductTypeListResponse { AttributeTypeValuesList = list.AttributeTypeValueList };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri + accountId, routeTemplate, response);
                }
            }

            return data;
        }
        #endregion
    }
}