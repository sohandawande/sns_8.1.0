﻿using Newtonsoft.Json;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
    public class CategoryProfileCache : BaseCache, ICategoryProfileCache
    {
        #region Private Variables
        private readonly ICategoryProfileService _service;
        #endregion

        #region Constructor
        public CategoryProfileCache(ICategoryProfileService categoryService)
        {
            _service = categoryService;
        }

        #endregion

        #region Public Methods
        public string GetCategoryProfilesByCategoryProfileId(int categoryProfileId, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var categoryProfileData = _service.GetCategoryProfileByCategoryProfileId(categoryProfileId);
                if (!Equals(categoryProfileData, null))
                {
                    var response = new CategoryProfileResponse { CategoryProfile = categoryProfileData };
                    data = JsonConvert.SerializeObject(response);
                }
            }

            return data;
        }

        public string GetCategoryProfilesByCategoryId(int categoryId, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var categoryProfileData = _service.GetCategoryProfileByCategoryId(categoryId);
                if (!Equals(categoryProfileData, null))
                {
                    var response = new CategoryProfileListResponse { CategoryProfiles = categoryProfileData.CategoryProfiles };
                    data = JsonConvert.SerializeObject(response);
                }
            }

            return data;
        }
        #endregion
    }
}