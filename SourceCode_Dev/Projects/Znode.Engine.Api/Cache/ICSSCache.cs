﻿
namespace Znode.Engine.Api.Cache
{
    public interface ICSSCache
    {
        /// <summary>
        /// Gets list of Css.
        /// </summary>
        /// <param name="routeUri">Route URI.</param>
        /// <param name="routeTemplate">Route template.</param>
        /// <returns>String Data.</returns>
        string GetCSSs(string routeUri, string routeTemplate);

        /// <summary>
        /// Get CSS by cssID
        /// </summary>
        /// <param name="cssId">CSS ID to get CSS</param>
        /// <param name="routeUri">route URL</param>
        /// <param name="routeTemplate">route Template</param>
        /// <returns>data in string format</returns>
        string GetCSS(int cssId, string routeUri, string routeTemplate);
    }
}
