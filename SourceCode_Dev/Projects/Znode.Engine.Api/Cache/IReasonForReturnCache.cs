﻿
namespace Znode.Engine.Api.Cache
{
    public interface IReasonForReturnCache
    {
        /// <summary>
        /// Get Reason for return by reasonForReturnId
        /// </summary>
        /// <param name="reasonForReturnId">ReasonForReturn Id by which get ReasonForReturn </param>
        /// <param name="routeUri">Route URI</param>
        /// <param name="routeTemplate">Route template</param>
        /// <returns>ReasonForReturn model</returns>
        string GetReasonForReturn(int reasonForReturnId, string routeUri, string routeTemplate);

        /// <summary>
        /// Get List of Reason for return
        /// </summary>
        /// <param name="routeUri">Route URI</param>
        /// <param name="routeTemplate">Route template</param>
        /// <returns>List of ReasonForReturn</returns>
        string GetListOfReasonForReturn(string routeUri, string routeTemplate);
    }
}
