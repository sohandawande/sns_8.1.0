﻿namespace Znode.Engine.Api.Cache
{
    /// <summary>
    /// Shipping Rule Cache Interface
    /// </summary>
	public interface IShippingRuleCache
	{
        /// <summary>
        /// Get Shipping Rule on the basis of shippingRuleId
        /// </summary>
        /// <param name="shippingRuleId">shippingRuleId</param>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns>Returns shipping rule</returns>
		string GetShippingRule(int shippingRuleId, string routeUri, string routeTemplate);

        /// <summary>
        /// Get list of shipping rules
        /// </summary>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns>Returns list of shipping rules</returns>
		string GetShippingRules(string routeUri, string routeTemplate);
	}
}