﻿using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
    public class RequestStatusCache : BaseCache,IRequestStatusCache
    {
        #region Private variables
        private readonly IRequestStatusService _service; 
        #endregion

        #region Constructor
        public RequestStatusCache(IRequestStatusService requestStatusService)
        {
            _service = requestStatusService;
        } 
        #endregion

        #region Public methods
        public string GetRequestStatus(int requestStatusId, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var requestStatus = _service.GetRequestStatus(requestStatusId, Expands);
                if (!Equals(requestStatus, null))
                {
                    var response = new RequestStatusResponse { RequestStatus = requestStatus };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }

        public string GetRequestStatusList(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetRequestStatusList(Filters, Sorts, Page);
                if (list.RequestStatusList.Count > 0)
                {
                    var response = new RequestStatusListResponse { RequestStatusList = list.RequestStatusList };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        } 
        #endregion
    }
}