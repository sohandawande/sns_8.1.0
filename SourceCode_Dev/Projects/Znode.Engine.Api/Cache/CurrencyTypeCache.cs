﻿using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
    public class CurrencyTypeCache : BaseCache, ICurrencyTypeCache
    {
        #region Private variables
        private readonly ICurrencyTypeService _service; 
        #endregion

        #region Constructor
        public CurrencyTypeCache(ICurrencyTypeService currencyTypeService)
        {
            _service = currencyTypeService;
        } 
        #endregion

        #region Public Region
        public string GetCurrencyTypes(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data,null))
            {
                var list = _service.GetCurrencyTypes(Expands, Filters, Sorts, Page);
                if (list.Currency.Count > 0)
                {
                    var response = new CurrencyTypeListResponse { CurrencyTypes = list.Currency };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }

        public string GetCurrencyType(int currencyTypeId, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var currencyType = _service.GetCurrencyType(currencyTypeId, Expands);
                if (!Equals(currencyType, null))
                {
                    var response = new CurrencyTypeResponse { CurrencyType = currencyType };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        } 
        #endregion
    }
}