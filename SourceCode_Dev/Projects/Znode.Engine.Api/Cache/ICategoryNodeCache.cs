﻿namespace Znode.Engine.Api.Cache
{
    public interface ICategoryNodeCache
    {

        /// <summary>
        /// This method will get the category node on the basis of category node id.
        /// </summary>
        /// <param name="categoryNodeId">int CategoryNodeId</param>
        /// <param name="routeUri">string Route URI</param>
        /// <param name="routeTemplate">string Route Template</param>
        /// <returns>Returns category node details.</returns>
        string GetCategoryNode(int categoryNodeId, string routeUri, string routeTemplate);

        /// <summary>
        /// This methods gets the list of category nodes available.
        /// </summary>
        /// <param name="routeUri">string Route URI</param>
        /// <param name="routeTemplate">string Route Template</param>
        /// <returns>Returns the list of category nodes available.</returns>
        string GetCategoryNodes(string routeUri, string routeTemplate);

        /// <summary>
        /// This method will gets the list of parent category node on the basis of catalog id.
        /// </summary>
        /// <param name="catalogId">int catalogId</param>
        /// <param name="routeUri">string Route URI</param>
        /// <param name="routeTemplate">string Route Template</param>
        /// <returns>Returns the list of parent category node on the basis of catalog id.</returns>       
        string GetParentCategoryNodes(int catalogId, string routeUri, string routeTemplate);
    }
}