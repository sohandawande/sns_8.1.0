﻿using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
	public class TaxRuleCache : BaseCache, ITaxRuleCache
	{
        #region Private Variables

        private readonly ITaxRuleService _service; 

        #endregion

        #region Public Methods

        public TaxRuleCache(ITaxRuleService taxRuleService)
        {
            _service = taxRuleService;
        }

        public string GetTaxRule(int taxRuleId, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var taxRule = _service.GetTaxRule(taxRuleId, Expands);
                if (!Equals(taxRule, null))
                {
                    var response = new TaxRuleResponse { TaxRule = taxRule };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetTaxRules(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetTaxRules(Expands, Filters, Sorts, Page);
                if (list.TaxRules.Count > 0)
                {
                    var response = new TaxRuleListResponse { TaxRules = list.TaxRules };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        } 

        #endregion
	}
}