﻿using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
	public class ShippingRuleCache : BaseCache, IShippingRuleCache
	{
        #region Private Variables

        private readonly IShippingRuleService _service; 

        #endregion

        #region Public Methods

        public ShippingRuleCache(IShippingRuleService shippingRuleService)
        {
            _service = shippingRuleService;
        }

        public string GetShippingRule(int shippingRuleId, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var shippingRule = _service.GetShippingRule(shippingRuleId, Expands);
                if (!Equals(shippingRule, null))
                {
                    var response = new ShippingRuleResponse { ShippingRule = shippingRule };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetShippingRules(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetShippingRules(Expands, Filters, Sorts, Page);
                if (list.ShippingRules.Count > 0)
                {
                    var response = new ShippingRuleListResponse { ShippingRules = list.ShippingRules };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        } 

        #endregion
	}
}