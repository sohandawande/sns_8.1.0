﻿
using Znode.Engine.Api.Models;
namespace Znode.Engine.Api.Cache
{
    public interface IContentPageRevisionCache
    {
        /// <summary>
        /// Gets the list of content page revision.
        /// </summary>
        /// <param name="routeUri">string routeUri</param>
        /// <param name="routeTemplate">string routeTemplate</param>
        /// <returns>string</returns>
        string GetContentPageRevisions(string routeUri, string routeTemplate);

        /// <summary>
        /// Get content page revision on Id.
        /// </summary>
        /// <param name="contentPageId">Id of the content page.</param>
        /// <param name="totalRowCount">total reow count</param>
        /// <param name="routeUri">string routeUri</param>
        /// <param name="routeTemplate">string routeTemplate</param>
        /// <returns>list of content page revision</returns>
        string GetContentPageRevisionOnId(int contentPageId, out int totalRowCount, string routeUri, string routeTemplate);

        /// <summary>
        ///  Get content page revision on Id.
        /// </summary>
        /// <param name="contentPageId">Id of the content page.</param>
        /// <param name="routeUri">route Uri</param>
        /// <param name="routeTemplate">route Template</param>
        /// <returns></returns>
        string GetContentPageRevisionOnId(int contentPageId, string routeUri, string routeTemplate);
    }
}