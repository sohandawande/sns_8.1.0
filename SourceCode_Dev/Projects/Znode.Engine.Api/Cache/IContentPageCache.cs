﻿
namespace Znode.Engine.Api.Cache
{
    public interface IContentPageCache
    {
        /// <summary>
        /// Gets the content pages
        /// </summary>
        /// <param name="routeUri">String Route URI</param>
        /// <param name="routeTemplate">Route Template.</param>
        /// <returns>String Data.</returns>
        string GetContentPages(string routeUri, string routeTemplate);

        /// <summary>
        /// Get the content page on content pageId
        /// </summary>
        /// <param name="contentPageId">Id of the content page.</param>
        /// <param name="routeUri">String routeUri</param>
        /// <param name="routeTemplate">String routeTemplate</param>
        /// <returns>String Data.</returns>
        string GetContentPage(int contentPageId, string routeUri, string routeTemplate);

        /// <summary>
        /// Get the content page on the basis of content page name and its file extension.
        /// </summary>
        /// <param name="contentPageName">Name of the content page.</param>
        /// <param name="extension">Extension of the content page file.</param>
        /// <param name="routeUri">String routeUri</param>
        /// <param name="routeTemplate">String routeTemplate</param>
        /// <returns>Returns data of the content page.</returns>
        string GetContentPageByName(string contentPageName, string extension, string routeUri, string routeTemplate);
    }
}
