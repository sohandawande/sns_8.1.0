﻿using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
	public class PromotionCache : BaseCache, IPromotionCache
	{
		private readonly IPromotionService _service;

		public PromotionCache(IPromotionService promotionService)
		{
			_service = promotionService;
		}

		public string GetPromotion(int promotionId, string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var promotion = _service.GetPromotion(promotionId, Expands);
				if (promotion != null)
				{
					var response = new PromotionResponse { Promotion = promotion };
					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}

		public string GetPromotions(string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var list = _service.GetPromotions(Expands, Filters, Sorts, Page);
				if (list.Promotions.Count > 0)
				{
					var response = new PromotionListResponse { Promotions = list.Promotions };
					response.MapPagingDataFromModel(list);

					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}

        public string GetProductList(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data,null))
            {
                var list = _service.GetProductsList(Expands, Filters, Sorts, Page);
                if (list.Products.Count > 0)
                {
                    var response = new ProductListResponse { Products = list.Products };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }
	}
}