﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Cache
{
    public class StoreCache : BaseCache , IStoreCache
    {
        #region Private Variables
        private readonly IStoreLocatorService _service; 
        #endregion

        #region Public Constructor
        public StoreCache(IStoreLocatorService storeLocatorService)
        {
            _service = storeLocatorService;
        } 
        #endregion

        #region Public Methods
        public string GetStoreLocator(int storeLocatorId, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var storeLocator = _service.GetStoreLocator(storeLocatorId);
                if (!Equals(storeLocator, null))
                {
                    var response = new StoreLocatorResponse { StoreLocator = storeLocator };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetStoreLocators(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetStoreLocators(Filters, Sorts, Page);
                if (list.Stores.Count > 0)
                {
                    var response = new StoreLocatorListResponse { StoreLocators = list.Stores };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetStoresList(StoreModel model, string routeUri, string routeTemplate)
        {
            string data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetStoresList(model);
                if (list.Stores.Count > 0)
                {
                    var response = new StoreLocatorListResponse { StoreLocators = list.Stores };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }
        #endregion
    }
}