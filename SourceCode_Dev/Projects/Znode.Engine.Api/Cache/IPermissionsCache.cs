﻿using Znode.Engine.Api.Models;
namespace Znode.Engine.Api.Cache
{
    public interface IPermissionsCache
    {
        /// <summary>
        /// This method will get the Roles and Perminssions  for the relevant Account id
        /// </summary>
        /// <param name="accountId">int Account Id</param>
        /// <param name="routeUri">string Route URL</param>
        /// <param name="routeTemplate">string Route Template</param>
        /// <returns>List of Roles and Permissions</returns>
        string GetRolesAndPerminssions(int accountId, string routeUri, string routeTemplate);

        /// <summary>
        /// This method will Update the Roles and Permissions for the relevant account id
        /// </summary>
        /// <param name="model">PermissionsModel model</param>
        /// <param name="routeUri">string Route URL</param>
        /// <param name="routeTemplate">string Route Template</param>
        /// <returns>True if Data updated</returns>
        string UpdateRolesAndPermissions(PermissionsModel model, string routeUri, string routeTemplate);
    }
}
