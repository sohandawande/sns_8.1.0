﻿using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Api.Cache
{
	public interface IDomainCache
	{
		string GetDomain(int domainId, string routeUri, string routeTemplate);
		Domain GetDomain(string routeUri);
		string GetDomains(string routeUri, string routeTemplate);
	}
}