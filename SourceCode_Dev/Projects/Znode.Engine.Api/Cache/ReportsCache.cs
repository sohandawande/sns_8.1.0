﻿using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;
namespace Znode.Engine.Api.Cache
{
    public class ReportsCache : BaseCache, IReportsCache
    {
        private readonly IReportsService _service;

        public ReportsCache(IReportsService ReportsService)
        {
            _service = ReportsService;
        }

        public string GetReportDataSet(string reportName, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetReportDataSet(reportName,Expands, Filters, Sorts, Page);
                if (list.RecordSet.Count > 0)
                {
                    var response = new ReportsResponse { ReportData = list };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }

        public string GetOrderDetails(int orderId, string reportName,string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetOrderDetails(orderId, reportName);
                if (list.RecordSet.Count > 0)
                {
                    var response = new ReportsResponse { ReportData = list };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }
    }
}