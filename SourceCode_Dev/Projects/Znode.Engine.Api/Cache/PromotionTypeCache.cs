﻿using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
	public class PromotionTypeCache : BaseCache, IPromotionTypeCache
	{
		private readonly IPromotionTypeService _service;

		public PromotionTypeCache(IPromotionTypeService promotionTypeService)
		{
			_service = promotionTypeService;
		}

		public string GetPromotionType(int promotionTypeId, string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
            if (Equals(data, null))
			{
				var promotionType = _service.GetPromotionType(promotionTypeId);
				if (promotionType != null)
				{
					var response = new PromotionTypeResponse { PromotionType = promotionType };
					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}

		public string GetPromotionTypes(string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (Equals(data,null))
			{
				var list = _service.GetPromotionTypes(Filters, Sorts, Page);
				if (list.PromotionTypes.Count > 0)
				{
					var response = new PromotionTypeListResponse { PromotionTypes = list.PromotionTypes };
					response.MapPagingDataFromModel(list);

					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}

        #region ZNode Version 8.0
        public string GetAllPromotionTypesNotInDatabase(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data,null))
            {
                var list = _service.GetAllPromotionTypesNotInDatabase();
                if (list.PromotionTypes.Count > 0)
                {
                    var response = new PromotionTypeListResponse { PromotionTypes = list.PromotionTypes };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetFranchisePromotionTypes(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data,null))
            {
                var list = _service.GetFranchisePromotionTypes(Filters,Sorts,Page);
                if (list.PromotionTypes.Count > 0)
                {
                    var response = new PromotionTypeListResponse { PromotionTypes = list.PromotionTypes };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }
        #endregion
    }
}