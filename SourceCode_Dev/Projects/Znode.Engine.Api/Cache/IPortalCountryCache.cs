﻿namespace Znode.Engine.Api.Cache
{
	public interface IPortalCountryCache
	{
		string GetPortalCountry(int portalCountryId, string routeUri, string routeTemplate);
		string GetPortalCountries(string routeUri, string routeTemplate);

        /// <summary>
        /// ZNode version 8.0
        /// Get all Portal Countries
        /// </summary>
        /// <param name="routeUri">string routeUri</param>
        /// <param name="routeTemplate">string routeTemplate</param>
        /// <returns></returns>
        string GetAllPortalCountries(string routeUri, string routeTemplate);
	}
}