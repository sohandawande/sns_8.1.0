﻿namespace Znode.Engine.Api.Cache
{
	public interface IProductCategoryCache
	{
        /// <summary>
        /// Get Product Category on the basis of productCategoryId
        /// </summary>
        /// <param name="productCategoryId"></param>
        /// <param name="routeUri"></param>
        /// <param name="routeTemplate"></param>
        /// <returns></returns>
		string GetProductCategory(int productCategoryId, string routeUri, string routeTemplate);

        /// <summary>
        /// Get the list of all product categories
        /// </summary>
        /// <param name="routeUri"></param>
        /// <param name="routeTemplate"></param>
        /// <returns></returns>
		string GetProductCategories(string routeUri, string routeTemplate);

        /// <summary>
        /// Get Product Category on the basis of productId and CategoryId
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="categoryId"></param>
        /// <param name="routeUri"></param>
        /// <param name="routeTemplate"></param>
        /// <returns></returns>
        string GetProductCategory(int productId, int categoryId, string routeUri, string routeTemplate);
    }
}