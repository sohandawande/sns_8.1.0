﻿
namespace Znode.Engine.Api.Cache
{
    public interface ISkuProfileEffectiveCache
    {
        /// <summary>
        /// Get Sku profile Effective.
        /// </summary>
        /// <param name="skuProfileEffectiveId">Id of the sku profile effective.</param>
        /// <param name="routeUri">Route URI</param>
        /// <param name="routeTemplate">Route Template</param>
        /// <returns>string</returns>
        string GetSkuProfileEffective(int skuProfileEffectiveId, string routeUri, string routeTemplate);

        /// <summary>
        /// Get Sku profile Effective list.
        /// </summary>
        /// <param name="routeUri">Route URI</param>
        /// <param name="routeTemplate">Route Template</param>
        /// <returns>string</returns>
        string GetSkuProfileEffectives(string routeUri, string routeTemplate);

        /// <summary>
        /// Get Sku profile Effective list by sku id.
        /// </summary>
        /// <param name="skuId">Id of the sku.</param>
        /// <param name="routeUri">Route URI</param>
        /// <param name="routeTemplate">Route Template</param>
        /// <returns>string</returns>
        string GetSkuProfileEffectivesBySkuId(int skuId, string routeUri, string routeTemplate);
    }
}
