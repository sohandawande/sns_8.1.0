﻿
namespace Znode.Engine.Api.Cache
{
    public interface IFacetGroupCache
    {
        /// <summary>
        /// Method returns Facet Group details.
        /// </summary>
        /// <param name="facetGroupId"></param>
        /// <param name="routeUri"></param>
        /// <param name="routeTemplate"></param>
        /// <returns>Return facet group details.</returns>
        string GetFacetGroup(int facetGroupId,string routeUri,string routeTemplate);

        /// <summary>
        /// Method returns Facet Group List.
        /// </summary>
        /// <param name="routeUri"></param>
        /// <param name="routeTemplate"></param>
        /// <returns>Return the Facet Group list.</returns>
        string GetFacetGroups(string routeUri, string routeTemplate);

        /// <summary>
        /// Method returns Facet control type list.
        /// </summary>
        /// <param name="routeUri"></param>
        /// <param name="routeTemplate"></param>
        /// <returns>Return Control type list.</returns>
        string GetFacetControlTypes(string routeUri, string routeTemplate);
                
        /// <summary>
        /// Method returns Facet details.
        /// </summary>
        /// <param name="facetId"></param>
        /// <param name="routeUri"></param>
        /// <param name="routeTemplate"></param>
        /// <returns>Return facet details.</returns>
        string GetFacet(int facetId, string routeUri, string routeTemplate);

        /// <summary>
        /// Method Get the Facet List.
        /// </summary>
        /// <param name="routeUri">string routeUri</param>
        /// <param name="routeTemplate">string routeTemplate</param>
        /// <returns>Return the Facet list.</returns>
        string GetFacetList(string routeUri, string routeTemplate);
    }
}
