﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Cache
{
    public interface IReorderCache
    {
        // <summary>
        /// Znode Version 7.2.2
        /// This function returns the response of order items collection on the basis of orderId.
        /// </summary>
        /// <param name="orderId">orderId</param>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns>returns response data as string</returns>
        string GetReorderItems(int orderId, string routeUri, string routeTemplate);

        /// <summary>
        /// Znode Version 7.2.2
        /// This function return the response of order item on the basis of orderLineItemId
        /// </summary>
        /// <param name="orderLineItemId">orderLineItemId</param>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns>returns response data as string</returns>
        string GetReorderLineItem(int orderLineItemId, string routeUri, string routeTemplate);

        /// <summary>
        /// Znode Version 7.2.2
        /// Get reorder items list.
        /// This function helps to reorder complite order as well single item. 
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="orderLineItemId"></param>
        /// <param name="isOrder"></param>
        /// <param name="routeUri"></param>
        /// <param name="routeTemplate"></param>
        /// <returns></returns>
        string GetReorderItemsList(int orderId, int orderLineItemId, bool isOrder, string routeUri, string routeTemplate);


    }
}
