﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
    public class ManageSearchIndexChache : BaseCache, IManageSearchIndexCache
    {
        #region Private Variables
        private readonly IManageSearchIndexService _service;
        #endregion
        #region Constructor
        public ManageSearchIndexChache(IManageSearchIndexService contentPageService)
        {
            _service = contentPageService;
        }
        #endregion

        public string GetLuceneIndexStatus(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var luceneindexlist = _service.GetLuceneIndexStatus(Filters, Sorts, Page);
                if (!Equals(luceneindexlist, null))
                {
                    var response = new ManageSearchIndexResponse { LuceneIndexList = luceneindexlist };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }

        public string GetIndexServerStatus(int indexId,string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var luceneindexlist = _service.GetIndexServerStatus(indexId);
                if (!Equals(luceneindexlist, null))
                {
                    var response = new ManageSearchIndexResponse {IndexServerList = luceneindexlist.IndexServerStatusList };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }

        
    }
}