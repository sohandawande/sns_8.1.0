﻿
using Znode.Engine.Api.Models;
namespace Znode.Engine.Api.Cache
{
    /// <summary>
    /// AddOn Value Cache Interface
    /// </summary>
    public interface IAddOnValueCache
    {
        /// <summary>
        /// Get AddOn Value
        /// </summary>
        /// <param name="addonValueId">int addonValueId</param>
        /// <param name="routeUri">string routeUri</param>
        /// <param name="routeTemplate">string routeTemplate</param>
        /// <returns></returns>
        string GetAddOnValue(int addonValueId, string routeUri, string routeTemplate);

        /// <summary>
        /// Get List og addon values associated by addon id
        /// </summary>
        /// <param name="routeUri">string routeUri</param>
        /// <param name="routeTemplate">string routeTemplate</param>
        /// <returns>Returns list model of addon values</returns>
        string GetAddOnValueByAddOnId(string routeUri, string routeTemplate);
    }
}
