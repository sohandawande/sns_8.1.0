﻿namespace Znode.Engine.Api.Cache
{
    /// <summary>
    /// Tax class Cache Interface
    /// </summary>
    public interface ITaxClassCache
    {
        /// <summary>
        /// Get tax class by tax class id
        /// </summary>
        /// <param name="taxClassId">taxClassId</param>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns>Returns tax class based on tax class id</returns>
        string GetTaxClass(int taxClassId, string routeUri, string routeTemplate);

        /// <summary>
        /// Gets list of tax classes 
        /// </summary>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns>Returns list of tax classes</returns>
        string GetTaxClasses(string routeUri, string routeTemplate);

        /// <summary>
        /// Gets tax class details 
        /// </summary>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns>Returns tax class details</returns>
        string GetTaxClassDetails(string routeUri, string routeTemplate);

        /// <summary>
        /// Gets list of tax classes using SP along with store name
        /// </summary>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns>Returns list of tax classes</returns>
        string GetTaxClassList(string routeUri, string routeTemplate);

        /// <summary>
        /// Gets list of tax classes by portalId
        /// </summary>
        /// <param name="portalId"></param>       
        /// <returns></returns>
        string GetActiveTaxClassByPortalId(int portalId, string routeUri, string routeTemplate);
    }
}