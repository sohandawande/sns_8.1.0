﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Cache
{
   public interface IApplicationSettingCache
    {
       /// <summary>
       /// Get Xml configuration list from database.
       /// </summary>
       /// <param name="routeUri"></param>
       /// <param name="routeTemplate"></param>
       /// <returns>Returns Xml string</returns>
       string GetApplicationSettings(string routeUri, string routeTemplate);

       /// <summary>
       /// Get column list of selected object it can be Table / View / Procedure .
       /// </summary>
       /// <param name="routeUri"></param>
       /// <param name="routeTemplate"></param>
       /// <param name="entityType"></param>
       /// <param name="entityName"></param>
       /// <returns>Returns Collections of Columns</returns>
       string GetColumnList(string routeUri, string routeTemplate, string entityType, string entityName);
    }
}
