﻿using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
    /// <summary>
    /// Cache for content page.
    /// </summary>
    public class ContentPageCache : BaseCache, IContentPageCache
    {
        #region Private Variables
        private readonly IContentPageService _service; 
        #endregion

        #region Constructor
        public ContentPageCache(IContentPageService contentPageService)
        {
            _service = contentPageService;
        } 
        #endregion

        #region Public Methods
        public string GetContentPage(int contentPageId, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var contentPage = _service.GetContentPage(contentPageId, Expands);
                if (!Equals(contentPage, null))
                {
                    var response = new ContentPageResponse { ContentPage = contentPage };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }

        public string GetContentPages(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data,null))
            {
                var list = _service.GetContentPages(Expands, Filters, Sorts, Page);
                if (list.ContentPages.Count > 0)
                {
                    var response = new ContentPageListResponse { ContentPages = list.ContentPages };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }

        public string GetContentPageByName(string contentPageName, string extension, string routeUri, string routeTemplate)
        {
            string data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                string contentPage = _service.GetContentPageByName(contentPageName, extension);
                if (!Equals(contentPage, null))
                {
                    ContentPageResponse response = new ContentPageResponse { Html = contentPage };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }
        #endregion
    }
}