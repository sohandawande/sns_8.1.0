﻿using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
    public class ThemeCache : BaseCache, IThemeCache
    {
        #region Private Variables
        private readonly IThemeService _service; 
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor for ThemeCache.
        /// </summary>
        /// <param name="themeService"></param>
        public ThemeCache(IThemeService themeService)
        {
            _service = themeService;
        } 
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets the list of themes.
        /// </summary>
        /// <param name="routeUri"></param>
        /// <param name="routeTemplate"></param>
        /// <returns></returns>
        public string GetThemes(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data,null))
            {
                ThemeListModel list = (!Equals(Page, null) && Page.Keys.Count > 1) ?_service.GetThemes(Filters, Sorts, Page) :  _service.GetThemes(Filters, Sorts);                
                if (list.Themes.Count > 0)
                {
                    var response = new ThemeListResponse { Themes = list.Themes };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetTheme(int themeId, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data,null))
            {
                var theme = _service.GetTheme(themeId);
                if (!Equals(theme, null))
                {
                    ThemeResponse response = new ThemeResponse { Theme = theme };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }
        #endregion

    }
}