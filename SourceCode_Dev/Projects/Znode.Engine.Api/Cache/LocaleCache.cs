﻿using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
    /// <summary>
    /// Locale Cache
    /// </summary>
    public class LocaleCache : BaseCache, ILocaleCache
    {
        #region Private Variables
        private readonly ILocaleService _service;
        #endregion

        #region Constructor
        public LocaleCache(ILocaleService localeService)
        {
            _service = localeService;
        }
        #endregion

        #region Public Methods
        
        public string GetLocales(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetLocales(Expands, Filters, Sorts, Page);
                if (list.Locales.Count > 0)
                {
                    var response = new LocaleListResponse { Locales = list.Locales };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }
        #endregion
    }
}