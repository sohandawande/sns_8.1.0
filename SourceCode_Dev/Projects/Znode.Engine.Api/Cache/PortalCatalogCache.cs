﻿using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
    public class PortalCatalogCache : BaseCache, IPortalCatalogCache
    {
        private readonly IPortalCatalogService _service;

        public PortalCatalogCache(IPortalCatalogService portalCatalogService)
        {
            _service = portalCatalogService;
        }

        public string GetPortalCatalog(int portalCatalogId, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (data == null)
            {
                var portalCatalog = _service.GetPortalCatalog(portalCatalogId, Expands);
                if (portalCatalog != null)
                {
                    var response = new PortalCatalogResponse { PortalCatalog = portalCatalog };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetPortalCatalogsByPortalId(int portalId, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (data == null)
            {
               var list = _service.GetPortalCatalogByPortalId(portalId);
                if (list.PortalCatalogs.Count>0)
                {
                    var response = new PortalCatalogListResponse { PortalCatalogs = list.PortalCatalogs };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetPortalCatalogs(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (data == null)
            {
                var list = _service.GetPortalCatalogs(Expands, Filters, Sorts, Page);
                if (list.PortalCatalogs.Count > 0)
                {
                    var response = new PortalCatalogListResponse { PortalCatalogs = list.PortalCatalogs };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }
    }
}