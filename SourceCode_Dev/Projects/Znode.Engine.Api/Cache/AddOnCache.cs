﻿using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
    /// <summary>
    /// AddOn Cache
    /// </summary>
    public class AddOnCache : BaseCache, IAddOnCache
    {
        #region Private Variables
        private readonly IAddOnService _service;
        #endregion

        #region Constructor
        public AddOnCache(IAddOnService addOnService)
        {
            _service = addOnService;
        }
        #endregion

        #region Public Methods
        public string GetAddOn(int addonId, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var addOn = _service.GetAddOn(addonId, Expands);
                if (!Equals(addOn, null))
                {
                    var response = new AddOnResponse { AddOn = addOn };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetAddOns(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetAddOns(Expands, Filters, Sorts, Page);
                if (list.AddOns.Count > 0)
                {
                    var response = new AddOnListResponse { AddOns = list.AddOns };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }
        #endregion
    }
}

