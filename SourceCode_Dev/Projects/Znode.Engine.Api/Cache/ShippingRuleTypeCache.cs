﻿using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
	public class ShippingRuleTypeCache : BaseCache, IShippingRuleTypeCache
	{
        #region Private Variables

        private readonly IShippingRuleTypeService _service; 

        #endregion

        #region Private Methods

        public ShippingRuleTypeCache(IShippingRuleTypeService shippingRuleTypeService)
        {
            _service = shippingRuleTypeService;
        }

        public string GetShippingRuleType(int shippingRuleTypeId, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var shippingRuleType = _service.GetShippingRuleType(shippingRuleTypeId);
                if (!Equals(shippingRuleType, null))
                {
                    var response = new ShippingRuleTypeResponse { ShippingRuleType = shippingRuleType };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetShippingRuleTypes(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetShippingRuleTypes(Filters, Sorts, Page);
                if (list.ShippingRuleTypes.Count > 0)
                {
                    var response = new ShippingRuleTypeListResponse { ShippingRuleTypes = list.ShippingRuleTypes };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        } 

        #endregion
	}
}