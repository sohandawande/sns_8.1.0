﻿
namespace Znode.Engine.Api.Cache
{
    public interface IEmailTemplateCache
    {
        /// <summary>
        /// Get template details.
        /// </summary>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns>return template details</returns>
        string GetEmailTemplates(string routeUri, string routeTemplate);

        /// <summary>
        /// Get the template page on template Name.
        /// </summary>
        /// <param name="templateName">name of template</param>
        /// <param name="extension">template file extension</param>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns>return page content</returns>
        string GetTemplatePage(string templateName, string extension, string routeUri, string routeTemplate);

        /// <summary>
        /// Get Deleted templates.
        /// </summary>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns>return deleted templates list</returns>
        string GetDeletedTemplates(string routeUri, string routeTemplate);
    }
}