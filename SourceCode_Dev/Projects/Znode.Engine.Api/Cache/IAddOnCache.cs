﻿
namespace Znode.Engine.Api.Cache
{
    /// <summary>
    /// AddOn Cache Interface
    /// </summary>
    public interface IAddOnCache
    {
        /// <summary>
        /// Get AddOn
        /// </summary>
        /// <param name="addonId">int addonId</param>
        /// <param name="routeUri">string routeUri</param>
        /// <param name="routeTemplate">string routeTemplate</param>
        /// <returns></returns>
        string GetAddOn(int addonId, string routeUri, string routeTemplate);

        /// <summary>
        /// Get AddOns
        /// </summary>
        /// <param name="routeUri">string routeUri</param>
        /// <param name="routeTemplate">string routeTemplate</param>
        /// <returns></returns>
        string GetAddOns(string routeUri, string routeTemplate);
    }
}
