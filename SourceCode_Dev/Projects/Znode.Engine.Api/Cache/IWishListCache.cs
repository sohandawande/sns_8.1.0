﻿namespace Znode.Engine.Api.Cache
{
    public interface IWishListCache
    {
        string GetWishList(int wishListId, string routeUri, string routeTemplate);
        string GetWishLists(string routeUri, string routeTemplate);
    }
}