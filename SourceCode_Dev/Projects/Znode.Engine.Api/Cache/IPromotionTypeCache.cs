﻿namespace Znode.Engine.Api.Cache
{
	public interface IPromotionTypeCache
	{
		string GetPromotionType(int promotionTypeId, string routeUri, string routeTemplate);

		string GetPromotionTypes(string routeUri, string routeTemplate);
        
        /// <summary>
        /// ZNode Version 8.0
        /// Get all Promotion Types not present in database.
        /// </summary>
        /// <param name="routeUri">string routeUri</param>
        /// <param name="routeTemplate">string routeTemplate</param>
        /// <returns></returns>
        string GetAllPromotionTypesNotInDatabase(string routeUri, string routeTemplate);

        /// <summary>
        /// ZNode Version 8.0
        /// Get Franchise Admin Access Promotion Types.
        /// </summary>
        /// <param name="routeUri">string routeUri</param>
        /// <param name="routeTemplate">string routeTemplate</param>
        /// <returns>string value</returns>
        string GetFranchisePromotionTypes(string routeUri, string routeTemplate);
	}
}