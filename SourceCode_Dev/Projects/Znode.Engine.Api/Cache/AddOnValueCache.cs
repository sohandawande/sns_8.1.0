﻿using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
    /// <summary>
    /// AddOn Value Cache
    /// </summary>
    public class AddOnValueCache : BaseCache, IAddOnValueCache
    {
        #region Private Variables
        private readonly IAddOnValueService _service;
        #endregion

        #region Constructor
        public AddOnValueCache(IAddOnValueService addOnValueService)
        {
            _service = addOnValueService;
        }
        #endregion

        #region Public Methods
        public string GetAddOnValue(int addonValueId, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var addOnValue = _service.GetAddOnValue(addonValueId, Expands);
                if (!Equals(addOnValue, null))
                {
                    var response = new AddOnValueResponse { AddOnValue = addOnValue };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }

        public string GetAddOnValueByAddOnId(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetAddOnValueByAddOnId(Expands, Filters, Sorts, Page);
                if (list.AddOnValues.Count > 0)
                {
                    var response = new AddOnValueListResponse { AddOnValuesList = list };
                    response.MapPagingDataFromModel(list);
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }
        #endregion
    }
}
