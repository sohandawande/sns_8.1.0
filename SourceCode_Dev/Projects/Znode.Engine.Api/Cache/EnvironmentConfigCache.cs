﻿using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
    public class EnvironmentConfigCache:BaseCache,IEnvironmentConfigCache
    {
        #region Private Variables
        private readonly IEnvironmentConfigService _service; 
        #endregion

        #region Public Regions
        public EnvironmentConfigCache(IEnvironmentConfigService environmentConfigService)
        {
            _service = environmentConfigService;
        }

        public string GetEnvironmentConfig(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var environmentConfig = _service.GetEnvironmentConfig();
                if (!Equals(environmentConfig, null))
                {
                    var response = new EnvironmentConfigResponse { EnvironmentConfig = environmentConfig };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        } 
        #endregion
    }
}