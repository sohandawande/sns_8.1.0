﻿
namespace Znode.Engine.Api.Cache
{
    /// <summary>
    /// Locale Cache Interface
    /// </summary>
   public interface ILocaleCache
    {
        /// <summary>
        /// Method Get all locales.
        /// </summary>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns>Return the Locale list.</returns>
        string GetLocales(string routeUri, string routeTemplate);
    }
}
