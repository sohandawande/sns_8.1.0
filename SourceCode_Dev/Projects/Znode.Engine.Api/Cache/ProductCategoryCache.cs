﻿using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
	public class ProductCategoryCache : BaseCache, IProductCategoryCache
	{
        #region Private variables
        private readonly IProductCategoryService _service; 
        #endregion

        #region Constructor
        public ProductCategoryCache(IProductCategoryService productCategoryService)
        {
            _service = productCategoryService;
        } 
        #endregion

        #region Public Methods
        public string GetProductCategory(int productCategoryId, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var productCategory = _service.GetProductCategory(productCategoryId, Expands);
                if (!Equals(productCategory, null))
                {
                    var response = new ProductCategoryResponse { ProductCategory = productCategory };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetProductCategories(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetProductCategories(Expands, Filters, Sorts, Page);
                if (list.ProductCategories.Count > 0)
                {
                    var response = new ProductCategoryListResponse { ProductCategories = list.ProductCategories };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }

        public string GetProductCategory(int productId, int categoryId, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var productCategory = _service.GetProductCategories(productId, categoryId, Expands);
                if (!Equals(productCategory, null))
                {
                    var response = new ProductCategoryResponse { ProductCategory = productCategory };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        } 
        #endregion
    }
}