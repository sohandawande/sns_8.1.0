﻿namespace Znode.Engine.Api.Cache
{
	public interface ISkuCache
	{
		string GetSku(int skuId, string routeUri, string routeTemplate);
		string GetSkus(string routeUri, string routeTemplate);

        /// <summary>
        /// Get the list Skus
        /// </summary>
        /// <param name="productId">productId to retrieve list of skus</param>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns>List of SKUs</returns>
        string GetSKUListByProductId(int productId, string routeUri, string routeTemplate);
	}
}