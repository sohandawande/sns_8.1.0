﻿using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
    public class ProductAttributesCache : BaseCache, IProductAttributesCache
    {
        private readonly IProductAttributesService _service;

		public ProductAttributesCache(IProductAttributesService attributesService)
		{
			_service = attributesService;
		}

		public string GetAttribute(int attributeId, string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var attributes = _service.GetAttribute(attributeId);
				if (attributes != null)
				{
					var response = new ProductAttributesResponse { Attributes = attributes };
					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}

		public string GetAttributes(string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var list = _service.GetAttributes(Filters, Sorts, Page);
				if (list.Attributes.Count > 0)
				{
					var response = new ProductAttributesListResponse { Attributes = list.Attributes };
					response.MapPagingDataFromModel(list);

					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}

        public string GetAttributesByAttributeTypeId(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
             if (Equals(data, null))
            {
                var  list = _service.GetAttributesByAttributeTypeId(Filters, Sorts, Page);
                if (list.Attributes.Count > 0)
                {
                    var response = new ProductAttributesListResponse { Attributes = list.Attributes };
                    response.MapPagingDataFromModel(list);
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
             return data;
        }
    }
}