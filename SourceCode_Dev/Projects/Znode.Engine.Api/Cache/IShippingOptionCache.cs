﻿namespace Znode.Engine.Api.Cache
{
    /// <summary>
    /// Shipping Option Cache Interface
    /// </summary>
	public interface IShippingOptionCache
	{
        /// <summary>
        /// Get Shipping Option on the basis of shippingOptionId
        /// </summary>
        /// <param name="shippingOptionId">shippingOptionId</param>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns>Returns shipping option</returns>
		string GetShippingOption(int shippingOptionId, string routeUri, string routeTemplate);

        /// <summary>
        /// Get list of shipping options
        /// </summary>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns>Returns list of shipping options</returns>
		string GetShippingOptions(string routeUri, string routeTemplate);

        /// <summary>
        /// Get the list of shipping option list for Franchise
        /// </summary>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns>Returns list of shipping options</returns>
        string GetFranchiseShippingOptionList(string routeUri, string routeTemplate);
	}
}