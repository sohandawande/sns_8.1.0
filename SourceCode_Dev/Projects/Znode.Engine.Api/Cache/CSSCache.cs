﻿using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
    public class CSSCache : BaseCache, ICSSCache
    {
        #region Private variables
        private readonly ICSSService _service;
        #endregion

        #region Constructor
        public CSSCache(ICSSService cssService)
        {
            _service = cssService;
        }
        #endregion

        #region Public Methods
        public string GetCSSs(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetCSSs(Expands, Filters, Sorts, Page);
                if (list.CSSs.Count > 0)
                {
                    var response = new CSSListResponse { CSSs = list.CSSs };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }

        public string GetCSS(int cssId, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var css = _service.GetCSS(cssId);
                if (!Equals(css, null))
                {
                    CSSResponse response = new CSSResponse { CSS = css };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }
        #endregion
    }
}