﻿using Newtonsoft.Json;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
    public class CategoryNodeCache : BaseCache, ICategoryNodeCache
    {
        private readonly ICategoryNodeService _service;

        public CategoryNodeCache(ICategoryNodeService categoryNodeService)
        {
            _service = categoryNodeService;
        }

        public string GetCategoryNode(int categoryNodeId, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (data == null)
            {
                var categoryNode = _service.GetCategoryNode(categoryNodeId, Expands);
                if (categoryNode != null)
                {
                    var response = new CategoryNodeResponse { CategoryNode = categoryNode };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetCategoryNodes(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (data == null)
            {
                var list = _service.GetCategoryNodes(Expands, Filters, Sorts, Page);
                if (list.CategoryNodes.Count > 0)
                {
                    var response = new CategoryNodeListResponse { CategoryNodes = list.CategoryNodes };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetParentCategoryNodes(int catalogId, string routeUri, string routeTemplate)
        {
            string data = string.Empty;
            var list = _service.GetParentCategoryNodes(catalogId, Filters, Sorts, Page);
            if (!Equals(list, null) && list.CategoryNodes.Count > 0)
            {
                var response = new CategoryNodeListResponse { CategoryNodes = list.CategoryNodes };
                response.MapPagingDataFromModel(list);

                data = JsonConvert.SerializeObject(response);
            }

            return data;
        }
    }
}