﻿namespace Znode.Engine.Api.Cache
{
	public interface IPortalCatalogCache
	{
		string GetPortalCatalog(int portalCatalogId, string routeUri, string routeTemplate);
		string GetPortalCatalogs(string routeUri, string routeTemplate);

        /// <summary>
        /// Znode 8.0 Version
        /// </summary>
        /// <param name="portalId">Portal ID according to which PortalCatalogList will be returned.</param>
        /// <param name="routeUri"></param>
        /// <param name="routeTemplate"></param>
        /// <returns></returns>
        string GetPortalCatalogsByPortalId(int portalId, string routeUri, string routeTemplate);
    }
}