﻿
using Znode.Engine.Api.Models;
namespace Znode.Engine.Api.Cache
{
    public interface IStoreCache
    {
        /// <summary>
        /// Get Store Locator
        /// </summary>
        /// <param name="storeLocatorId">store Locator Id</param>
        /// <param name="routeUri">route url</param>
        /// <param name="routeTemplate">string url</param>
        /// <returns>string</returns>
        string GetStoreLocator(int storeLocatorId, string routeUri, string routeTemplate);

        /// <summary>
        /// Get Store Locator List
        /// </summary>
        /// <param name="routeUri">route url</param>
        /// <param name="routeTemplate">string url</param>
        /// <returns>string</returns>
        string GetStoreLocators(string routeUri, string routeTemplate);

        /// <summary>
        /// Get List of stores within the area
        /// </summary>
        /// <param name="model">Store Model</param>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns>data in string format</returns>
        string GetStoresList(StoreModel model, string routeUri, string routeTemplate);
    }
}