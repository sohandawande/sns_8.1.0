﻿namespace Znode.Engine.Api.Cache
{
    /// <summary>
    /// Message Config Cache Interface
    /// </summary>
	public interface IMessageConfigCache
	{
        /// <summary>
        /// Get Message Config by messageConfigId
        /// </summary>
        /// <param name="messageConfigId">messageConfigId</param>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns>Returns message config</returns>
		string GetMessageConfig(int messageConfigId, string routeUri, string routeTemplate);

        /// <summary>
        /// Get Message Config By Key
        /// </summary>
        /// <param name="key">key</param>
        /// <param name="portalId">portalId</param>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns>Returns message config</returns>
	    string GetMessageConfigByKey(string key, int portalId, string routeUri, string routeTemplate);

        /// <summary>
        /// Get Message Configs
        /// </summary>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns>Returns message configs</returns>
		string GetMessageConfigs(string routeUri, string routeTemplate);

        /// <summary>
        /// Get Message Configs By Keys
        /// </summary>
        /// <param name="keys">keys</param>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns>Returns message configs</returns>
		string GetMessageConfigsByKeys(string keys, string routeUri, string routeTemplate);
	}
}