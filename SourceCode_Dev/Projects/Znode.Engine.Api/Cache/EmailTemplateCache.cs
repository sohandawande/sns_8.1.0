﻿using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
    public class EmailTemplateCache : BaseCache, IEmailTemplateCache
    {
        private readonly IEmailTemplateService _service;

        public EmailTemplateCache(IEmailTemplateService emailTemplateService)
        {
            _service = emailTemplateService;
        }

        public string GetEmailTemplates(string routeUri, string routeTemplate)
        {
            string data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                EmailTemplateListModel model = _service.GetEmailTemplates(Filters, Sorts, Page);
                if (model.EmailTemplates.Count > 0)
                {
                    EmailTemplateListResponse response = new EmailTemplateListResponse { EmailTemplates = model.EmailTemplates };
                    response.MapPagingDataFromModel(model);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetDeletedTemplates(string routeUri, string routeTemplate)
        {
            string data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                EmailTemplateListModel model = _service.GetDeletedTemplates(Filters, Sorts, Page);
                if (model.EmailTemplates.Count > 0)
                {
                    EmailTemplateListResponse response = new EmailTemplateListResponse { EmailTemplates = model.EmailTemplates };
                    response.MapPagingDataFromModel(model);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetTemplatePage(string templateName, string extension, string routeUri, string routeTemplate)
        {
            string data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                EmailTemplateModel templatePage = _service.GetTemplatePageByName(templateName, extension, Expands);
                if (!Equals(templatePage, null))
                {
                    EmailTemplateResponse response = new EmailTemplateResponse { EmailTemplate = templatePage };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }
    }
}