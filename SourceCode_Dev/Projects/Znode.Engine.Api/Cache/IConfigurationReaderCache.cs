﻿
namespace Znode.Engine.Api.Cache
{
    public interface IConfigurationReaderCache
    {
        /// <summary>
        /// Get dynamic grid column and filters configuration XML.
        /// </summary>
        /// <param name="itemName">string ItemName for XMl</param>
        /// <param name="routeUri">routeUri to maintain cache</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns>Returns XML string</returns>
        string GetFilterConfigurationXML(string itemName, string routeUri, string routeTemplate);
    }
}
