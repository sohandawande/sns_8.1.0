﻿
namespace Znode.Engine.Api.Cache
{
    public interface IRejectionMessageCache 
    {
        /// <summary>
        /// Get the Rejection Message List.
        /// </summary>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns>string</returns>
        string GetRejectionMessages(string routeUri, string routeTemplate);

        /// <summary>
        /// Get Rejection Message By Id. 
        /// </summary>
        /// <param name="rejectionMessageId">Id of the Rejection Message.</param>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns>string</returns>
        string GetRejectionMessage(int rejectionMessageId, string routeUri, string routeTemplate);
    }
}
