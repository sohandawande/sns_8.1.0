﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.Api.Cache
{
    public interface IPRFTERPServicesCache
    {
        string GetERPItemDetails(string associatedCustomerExternalId, string productNum, string customerType, string routeUri, string routeTemplate);

        string GetInventoryDetails(string productNum, string routeUri, string routeTemplate);

    }
}