﻿using Newtonsoft.Json;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
    public class CategoryCache : BaseCache, ICategoryCache
    {
        #region Private Variables
        private readonly ICategoryService _service;
        #endregion

        #region Constructor
        public CategoryCache(ICategoryService categoryService)
        {
            _service = categoryService;
        }
        #endregion

        #region Public Methods
        public string GetCategory(int categoryId, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var category = _service.GetCategory(categoryId, Expands);
                if (!Equals(category, null))
                {
                    var response = new CategoryResponse { Category = category };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetCategories(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetCategories(Expands, Filters, Sorts, Page);
                if (list.Categories.Count > 0)
                {
                    var response = new CategoryListResponse { Categories = list.Categories };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetCategoryTree(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetCategoryTree(Expands, Filters, Sorts, Page);
                if (list.Categories.Count > 0)
                {
                    var response = new CategoryListResponse { Categories = list.Categories };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetCategoriesByCatalog(int catalogId, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetCategoriesByCatalog(catalogId, Expands, Filters, Sorts, Page);
                if (list.Categories.Count > 0)
                {
                    var response = new CategoryListResponse { Categories = list.Categories };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetCategoriesByCatalogIds(string catalogIds, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetCategoriesByCatalogIds(catalogIds, Expands, Filters, Sorts, Page);
                if (list.Categories.Count > 0)
                {
                    var response = new CategoryListResponse { Categories = list.Categories };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetCategoriesByCategoryIds(string categoryIds, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetCategoriesByCategoryIds(categoryIds, Expands, Filters, Sorts, Page);
                if (list.Categories.Count > 0)
                {
                    var response = new CategoryListResponse { Categories = list.Categories };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        #region Znode Version 8.0
        public string GetCategoriesByCatalogIdUsingCustomService(out int totalRowCount, int catalogId = 0, string routeUri = "", string routeTemplate = "")
        {
            string data = string.Empty;
            CatalogAssociatedCategoriesListModel list = _service.GetCategoryByCatalogId(Filters, Sorts, Page, out totalRowCount, catalogId);

            if (!Equals(list, null) && list.CategoryList.Count > 0)
            {
                var response = new CategoryListResponse { CatalogAssociatedCategories = list };
                response.MapPagingDataFromModel(list);
                data = JsonConvert.SerializeObject(response);
            }
            return data;
        }

        public string GetAllCategories(string routeUri, string routeTemplate)
        {
            string data = string.Empty;
            CatalogAssociatedCategoriesListModel list = _service.GetAllCategories(Expands, Filters, Sorts, Page);
            if (!Equals(list, null) && list.CategoryList.Count > 0)
            {
                var response = new CategoryListResponse { CatalogAssociatedCategories = list };
                response.MapPagingDataFromModel(list);
                data = JsonConvert.SerializeObject(response);
            }
            return data;
        }

        public string GetCategoryAssociatedProducts(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetCategoryAssociatedProducts(Expands, Filters, Sorts, Page);
                if (list.Products.Count > 0)
                {
                    var response = new ProductListResponse { Products = list.Products };
                    response.MapPagingDataFromModel(list);
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }

        #endregion
        #endregion
    }
}

