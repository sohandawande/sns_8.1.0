﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
    public class PRFTERPServicesCache : BaseCache, IPRFTERPServicesCache
    {
        #region Private variables
        private readonly IPRFTERPServices _service;
        #endregion

        #region Constructor
        public PRFTERPServicesCache(IPRFTERPServices erpService)
        {
            _service = erpService;
        }
        #endregion
        #region Public Methods
        public string GetERPItemDetails(string associatedCustomerExternalId, string productNum, string customerType, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var response = _service.GetERPItemDetails(associatedCustomerExternalId, productNum, customerType);
                var itemDetailsResponse = new PRFTERPItemDetailsResponse { ERPItemDetails = response };
                data = InsertIntoCache(routeUri, routeTemplate, itemDetailsResponse);
            }
            return data;
        }

        public string GetInventoryDetails(string productNum, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var response = _service.GetInventoryDetails(productNum);
                var inventoryItemResponse = new PRFTInventoryResponse { Inventory = response };
                data = InsertIntoCache(routeUri, routeTemplate, inventoryItemResponse);
            }
            return data;
        }
        #endregion
    }
}