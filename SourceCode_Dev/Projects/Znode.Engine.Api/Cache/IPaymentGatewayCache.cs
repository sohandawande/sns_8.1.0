﻿namespace Znode.Engine.Api.Cache
{
	public interface IPaymentGatewayCache
	{
		string GetPaymentGateway(int paymentGatewayId, string routeUri, string routeTemplate);
		string GetPaymentGateways(string routeUri, string routeTemplate);
	}
}