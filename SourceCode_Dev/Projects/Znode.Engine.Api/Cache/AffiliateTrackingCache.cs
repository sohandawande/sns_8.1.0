﻿using Newtonsoft.Json;
using System.Data;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
    public class AffiliateTrackingCache : BaseCache, IAffiliateTrackingCache
    {
        #region Private Variables

        private readonly IAffiliateTrackingService _service;

        #endregion

        #region Constructor
        public AffiliateTrackingCache(IAffiliateTrackingService _affiliateService)
        {
            _service = _affiliateService;
        }
        #endregion

        #region Public Methods

        public string GetAffiliateTrackingData(string routeUri, string routeTemplate)
        {
            string data = string.Empty;
            DataSet trackingList = _service.GetAffiliateTrackingData(Filters);
            if (!Equals(trackingList, null))
            {
                var response = new AffiliateTrackingResponse { TrackingData = trackingList };
                data = JsonConvert.SerializeObject(response);
            }
            return data;
        }

        #endregion
    }
}