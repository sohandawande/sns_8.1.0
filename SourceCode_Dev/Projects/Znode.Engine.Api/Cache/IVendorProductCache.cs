﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Cache
{
    public interface IVendorProductCache
    {
        /// <summary>
        /// This method will fetch all the vendor products
        /// </summary>
        /// <param name="routeUri">string Route URI</param>
        /// <param name="routeTemplate">string Route Template</param>
        /// <returns>Retunrs the vendor product details</returns>
        string GetVendorProductList(string routeUri, string routeTemplate);

        /// <summary>
        /// This method will fetch all the vendor image list.
        /// </summary>
        /// <param name="routeUri">string Route URI</param>
        /// <param name="routeTemplate">string Route Template</param>
        /// <returns>Retunrs the vendor product details</returns>
        string GetReviewImageList(string routeUri, string routeTemplate);

        /// <summary>
        /// This method will fetch all the mall admin products
        /// </summary>
        /// <param name="routeUri">string routeUri</param>
        /// <param name="routeTemplate">string routeTemplate</param>
        /// <returns>returns mall admin products</returns>
        string GetMallAdminProductList(string routeUri, string routeTemplate);
    }
}
