﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Services;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Cache
{
    public class RejectionMessageCache : BaseCache, IRejectionMessageCache
    {
        #region Private Readonly Variables
        private readonly IRejectionMessageService _service;
        #endregion

        #region Constructor
        /// <summary>
        /// constructor.
        /// </summary>
        /// <param name="rejectionmessageService">IRejectionMessageService to instantiate.</param>
        public RejectionMessageCache(IRejectionMessageService rejectionmessageService)
        {
            _service = rejectionmessageService;
        }
        #endregion

        #region Public Methods

        public string GetRejectionMessages(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (data == null)
            {
                var list = _service.GetRejectionMessages(Expands, Filters, Sorts, Page);
                if (list.RejectionMessages.Count > 0)
                {
                    var response = new RejectionMessageListResponse { RejectionMessages = list.RejectionMessages };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetRejectionMessage(int rejectionMessageId, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var rejectionMessage = _service.GetRejectionMessage(rejectionMessageId);
                if (!Equals(rejectionMessage, null))
                {
                    var response = new RejectionMessageResponse { RejectionMessage = rejectionMessage };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        #endregion
    }
}