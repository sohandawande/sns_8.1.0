﻿namespace Znode.Engine.Api.Cache
{
	public interface IShippingTypeCache
	{
		string GetShippingType(int shippingTypeId, string routeUri, string routeTemplate);
		string GetShippingTypes(string routeUri, string routeTemplate);

        /// <summary>
        /// ZNode Version 8.0
        /// Get all Shipping Types not present in database.
        /// </summary>
        /// <param name="routeUri">string routeUri</param>
        /// <param name="routeTemplate">string routeTemplate</param>
        /// <returns></returns>
        string GetAllShippingTypesNotInDatabase(string routeUri, string routeTemplate);
	}
}