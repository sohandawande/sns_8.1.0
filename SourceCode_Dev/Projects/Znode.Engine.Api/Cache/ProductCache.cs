﻿using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Api.Cache
{
    public class ProductCache : BaseCache, IProductCache
    {
        #region private variables
        private readonly IProductService _service;
        #endregion

        #region public constructor
        public ProductCache(IProductService productService)
        {
            _service = productService;
        }
        #endregion

        #region Public Methods
        public string GetProduct(int productId, string routeUri, string routeTemplate)
        {
            var accountId = UpdateCacheForProfile();

            var data = GetFromCache(routeUri + accountId);
            if (Equals(data, null))
            {
                var product = _service.GetProduct(productId, Expands);

                if (!Equals(product, null))
                {
                    var response = new ProductResponse { Product = product };
                    data = InsertIntoCache(routeUri + accountId, routeTemplate, response);
                }
            }
            return data;
        }

        public string GetProductWithSku(int productId, int skuId, string routeUri, string routeTemplate)
        {
            var accountId = UpdateCacheForProfile();

            var data = GetFromCache(routeUri + accountId);
            if (Equals(data, null))
            {
                var product = _service.GetProductWithSku(productId, skuId, Expands);
                if (!Equals(product, null))
                {
                    var response = new ProductResponse { Product = product };
                    data = InsertIntoCache(routeUri + accountId, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetProducts(string routeUri, string routeTemplate)
        {
            var accountId = UpdateCacheForProfile();

            var data = GetFromCache(routeUri + accountId);
            if (Equals(data, null))
            {
                var list = _service.GetProductsList(Expands, Filters, Sorts, Page);//GetProducts(Expands, Filters, Sorts, Page);
                if (!Equals(list, null) && !Equals(list.Products, null))
                {
                    var response = new ProductListResponse { Products = list.Products };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri + accountId, routeTemplate, response);
                }
            }
            return data;
        }

        public string GetProductsByCatalog(int catalogId, string routeUri, string routeTemplate)
        {
            var accountId = UpdateCacheForProfile();

            var data = GetFromCache(routeUri + accountId);
            if (Equals(data, null))
            {
                var list = _service.GetProductsByCatalog(catalogId, Expands, Filters, Sorts, Page);
                if (list.Products.Count > 0)
                {
                    var response = new ProductListResponse { Products = list.Products };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri + accountId, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetProductsByCatalogIds(string catalogIds, string routeUri, string routeTemplate)
        {
            var accountId = UpdateCacheForProfile();

            var data = GetFromCache(routeUri + accountId);
            if (Equals(data, null))
            {
                var list = _service.GetProductsByCatalogIds(catalogIds, Expands, Filters, Sorts, Page);
                if (list.Products.Count > 0)
                {
                    var response = new ProductListResponse { Products = list.Products };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri + accountId, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetProductsByCategory(int categoryId, string routeUri, string routeTemplate)
        {
            var accountId = UpdateCacheForProfile();

            var data = GetFromCache(routeUri + accountId);
            if (Equals(data, null))
            {
                var list = _service.GetProductsByCategory(categoryId, Expands, Filters, Sorts, Page);
                if (list.Products.Count > 0)
                {
                    var response = new ProductListResponse { Products = list.Products };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri + accountId, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetProductsByCategoryByPromotionType(int categoryId, int promotionTypeId, string routeUri, string routeTemplate)
        {
            var accountId = UpdateCacheForProfile();

            var data = GetFromCache(routeUri + accountId);
            if (Equals(data, null))
            {
                var list = _service.GetProductsByCategoryByPromotionType(categoryId, promotionTypeId, Expands, Filters, Sorts, Page);
                if (list.Products.Count > 0)
                {
                    var response = new ProductListResponse { Products = list.Products };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri + accountId, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetProductsByExternalIds(string externalIds, string routeUri, string routeTemplate)
        {
            var accountId = UpdateCacheForProfile();

            var data = GetFromCache(routeUri + accountId);
            if (Equals(data, null))
            {
                var list = _service.GetProductsByExternalIds(externalIds, Expands, Sorts);
                if (list.Products.Count > 0)
                {
                    var response = new ProductListResponse { Products = list.Products };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri + accountId, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetProductsByHomeSpecials(string routeUri, string routeTemplate)
        {
            var accountId = UpdateCacheForProfile();

            var data = GetFromCache(routeUri + accountId);
            if (Equals(data, null))
            {
                var list = _service.GetProductsByHomeSpecials(ZNodeConfigManager.SiteConfig.PortalID, Expands, Filters, Sorts, Page);
                if (list.Products.Count > 0)
                {
                    var response = new ProductListResponse { Products = list.Products };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri + accountId, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetProductsByProductIds(string productIds, string routeUri, string routeTemplate)
        {
            var accountId = UpdateCacheForProfile();

            var data = GetFromCache(routeUri + accountId);
            if (Equals(data, null))
            {
                var list = _service.GetProductsByProductIds(productIds, Expands, Sorts);
                if (list.Products.Count > 0)
                {
                    var response = new ProductListResponse { Products = list.Products };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri + accountId, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetProductsByPromotionType(int promotionTypeId, string routeUri, string routeTemplate)
        {
            var accountId = UpdateCacheForProfile();

            var data = GetFromCache(routeUri + accountId);
            if (Equals(data, null))
            {
                var list = _service.GetProductsByPromotionType(promotionTypeId, Expands, Filters, Sorts, Page);
                if (list.Products.Count > 0)
                {
                    var response = new ProductListResponse { Products = list.Products };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri + accountId, routeTemplate, response);
                }
            }

            return data;
        }

        #region Znode Version 8.0

        #region Product Category

        public string GetProductCategoryByProductId(string routeUri, string routeTemplate)
        {
            var accountId = UpdateCacheForProfile();

            var data = GetFromCache(routeUri + accountId);
            if (Equals(data, null))
            {
                var list = _service.GetProductCategoryByProductId(Expands, Filters, Sorts, Page);
                if (list.Categories.Count > 0)
                {
                    var response = new CategoryListResponse { Categories = list.Categories };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri + accountId, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetProductUnAssociatedCategoryByProductId(int productId, string routeUri, string routeTemplate)
        {
            var accountId = UpdateCacheForProfile();

            var data = GetFromCache(routeUri + accountId);
            if (Equals(data, null))
            {
                var list = _service.GetProductUnAssociatedCategoryByProductId(productId, Expands, Filters, Sorts, Page);
                if (list.Categories.Count > 0)
                {
                    var response = new CategoryListResponse { Categories = list.Categories };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri + accountId, routeTemplate, response);
                }
            }

            return data;
        }

        #endregion

        #region Product Sku

        public string GetProductSkuDetails(string routeUri, string routeTemplate)
        {
            var accountId = UpdateCacheForProfile();

            var data = GetFromCache(routeUri + accountId);
            if (Equals(data, null))
            {
                var list = _service.GetProductSkuDetails(Expands, Filters, Sorts, Page);
                if (list.ProductSkus.Count > 0)
                {
                    var response = new ProductSkuListResponse { Skus = list.ProductSkus };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri + accountId, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetSkuAssociatedFacets(int productId, int skuId, int facetGroupId, string routeUri, string routeTemplate)
        {
            var accountId = UpdateCacheForProfile();

            var data = GetFromCache(routeUri + accountId);
            if (Equals(data, null))
            {
                var productFacets = _service.GetSkuAssociatedFacets(productId, skuId, facetGroupId, Expands);

                if (!Equals(productFacets, null))
                {
                    var response = new ProductFacetsResponse { ProductFacets = productFacets };
                    data = InsertIntoCache(routeUri + accountId, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetSkuFacets(string routeUri, string routeTemplate)
        {
            var accountId = UpdateCacheForProfile();

            var data = GetFromCache(routeUri + accountId);
            if (Equals(data, null))
            {
                var list = _service.GetSkuFacets(Expands, Filters, Sorts, Page);
                if (list.FacetGroups.Count > 0)
                {
                    var response = new ProductFacetListResponse { FacetGroups = list.FacetGroups };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri + accountId, routeTemplate, response);
                }
            }

            return data;
        }

        #endregion

        public string GetProductTags(int productId, string routeUri, string routeTemplate)
        {
            var accountId = UpdateCacheForProfile();

            var data = GetFromCache(routeUri + accountId);
            if (Equals(data, null))
            {
                var productTags = _service.GetProductTags(productId, Expands);

                if (!Equals(productTags, null))
                {
                    var response = new ProductTagResponse { ProductTag = productTags };
                    data = InsertIntoCache(routeUri + accountId, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetProductFacetsList(string routeUri, string routeTemplate)
        {
            var accountId = UpdateCacheForProfile();

            var data = GetFromCache(routeUri + accountId);
            if (Equals(data, null))
            {
                var list = _service.GetProductFacetsList(Expands, Filters, Sorts, Page);
                if (list.FacetGroups.Count > 0)
                {
                    var response = new ProductFacetListResponse { FacetGroups = list.FacetGroups };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri + accountId, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetProductFacets(int productId, int facetGroupId, string routeUri, string routeTemplate)
        {
            var accountId = UpdateCacheForProfile();

            var data = GetFromCache(routeUri + accountId);
            if (Equals(data, null))
            {
                var productFacets = _service.GetProductFacets(productId, facetGroupId, Expands);

                if (!Equals(productFacets, null))
                {
                    var response = new ProductFacetsResponse { ProductFacets = productFacets };
                    data = InsertIntoCache(routeUri + accountId, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetProductDetailsByProductId(int productId, string routeUri, string routeTemplate)
        {
            var accountId = UpdateCacheForProfile();

            var data = GetFromCache(routeUri + accountId);
            if (Equals(data, null))
            {
                var product = _service.GetProductDetailsByProductId(productId);

                if (!Equals(product != null))
                {
                    var response = new ProductResponse { Product = product };
                    data = InsertIntoCache(routeUri + accountId, routeTemplate, response);
                }
            }
            return data;
        }

        public string GetProductImageTypes(string routeUri, string routeTemplate)
        {
            var accountId = UpdateCacheForProfile();

            var data = GetFromCache(routeUri + accountId);
            if (Equals(data, null))
            {
                var list = _service.GetProductImageTypes();
                if (list.ProductImageTypes.Count > 0)
                {
                    var response = new ProductImageTypeListResponse { ImageTypes = list.ProductImageTypes };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri + accountId, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetAllProductAlternateImages(string routeUri, string routeTemplate)
        {
            var accountId = UpdateCacheForProfile();

            var data = GetFromCache(routeUri + accountId);
            if (Equals(data, null))
            {
                var list = _service.GetAllProductAlternateImages(Expands, Filters, Sorts, Page);
                if (list.ProductImages.Count > 0)
                {
                    var response = new ProductAlternateImageListResponse { ProductImages = list.ProductImages };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri + accountId, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetProductAlternateImage(int productImageId, string routeUri, string routeTemplate)
        {
            var accountId = UpdateCacheForProfile();

            var data = GetFromCache(routeUri + accountId);
            if (Equals(data, null))
            {
                var productImage = _service.GetProductAlternateImageById(productImageId, Expands);

                if (!Equals(productImage, null))
                {
                    var response = new ProductAlternateImageResponse { ProductImage = productImage };
                    data = InsertIntoCache(routeUri + accountId, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetProductBundlesList(string routeUri, string routeTemplate)
        {
            var accountId = UpdateCacheForProfile();

            var data = GetFromCache(routeUri + accountId);
            if (Equals(data, null))
            {
                var list = _service.GetProductBundlesList(Expands, Filters, Sorts, Page);
                if (list.ProductBundles.Count > 0)
                {
                    var response = new ProductBundlesListResponse { ProductBundles = list.ProductBundles };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri + accountId, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetProductList(string routeUri, string routeTemplate)
        {
            var accountId = UpdateCacheForProfile();

            var data = GetFromCache(routeUri + accountId);
            if (Equals(data, null))
            {
                var list = _service.GetProductList(Expands, Filters, Sorts, Page);
                if (list.Products.Count > 0)
                {
                    var response = new ProductListResponse { Products = list.Products };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri + accountId, routeTemplate, response);
                }
            }

            return data;
        }

        #region AddOns

        public string GetAssociatedAddOns(int productId, string routeUri, string routeTemplate)
        {
            string accountId = UpdateCacheForProfile();
            string data = GetFromCache(routeUri + accountId);

            AddOnListModel list = _service.GetAssociatedAddOns(productId, Filters, Sorts, Page);
            AddOnListResponse response = null;
            if (list.AddOns.Count > 0)
            {
                response = new AddOnListResponse { AddOns = list.AddOns };
                response.MapPagingDataFromModel(list);
                data = InsertIntoCache(routeUri + accountId, routeTemplate, response);
            }
            return data;
        }

        public string GetUnassociatedAddOns(int productId, string routeUri, string routeTemplate, int portalId = 0)
        {
            string accountId = UpdateCacheForProfile();
            string data = GetFromCache(routeUri + accountId);

            AddOnListModel list = _service.GetUnassociatedAddOns(productId, Filters, Sorts, Page, portalId);

            AddOnListResponse response = null;
            if (list.AddOns.Count > 0)
            {
                response = new AddOnListResponse { AddOns = list.AddOns };
                response.MapPagingDataFromModel(list);
                data = InsertIntoCache(routeUri + accountId, routeTemplate, response);
            }
            return data;
        }
        #endregion

        #region Highlights

        public string GetAssociatedHighlights(int productId, string routeUri, string routeTemplate)
        {
            string accountId = UpdateCacheForProfile();
            string data = GetFromCache(routeUri + accountId);

            HighlightListModel list = _service.GetAssociatedHighlights(productId, Filters, Sorts, Page);
            HighlightListResponse response = null;
            if (list.Highlights.Count > 0)
            {
                response = new HighlightListResponse { Highlights = list.Highlights };
                response.MapPagingDataFromModel(list);
                data = InsertIntoCache(routeUri + accountId, routeTemplate, response);
            }
            return data;
        }

        public string GetUnassociatedHighlights(int productId, string routeUri, string routeTemplate, int portalId = 0)
        {
            string accountId = UpdateCacheForProfile();
            string data = GetFromCache(routeUri + accountId);

            HighlightListModel list = _service.GetUnassociatedHighlights(productId, Filters, Sorts, Page, portalId);
            HighlightTypeListModel highlightTypeListModel = new HighlightTypeService().GetHighlightTypes(Filters, Sorts, Page);

            HighlightListResponse response = new HighlightListResponse { Highlights = list.Highlights, HighlightTypes = highlightTypeListModel.HighlightTypes };
            response.MapPagingDataFromModel(list);
            data = InsertIntoCache(routeUri + accountId, routeTemplate, response);

            return data;
        }

        #endregion

        #region Category Associated Products

        public CategoryAssociatedProductsListModel GetAllProducts(string routeUri, string routeTemplate)
        {
            CategoryAssociatedProductsListModel list = _service.GetAllProducts(Expands, Filters, Sorts, Page);
            if (!Equals(list, null) && !Equals(list.ProductList, null) && list.ProductList.Count > 0)
            {
                var response = new ProductListResponse { CategoryAssociatedProducts = list };
                response.MapPagingDataFromModel(list);
            }
            return list;
        }
        #endregion

        #region Product Tier Pricing

        public string GetProductTiers(int productId, string routeUri, string routeTemplate)
        {
            string accountId = UpdateCacheForProfile();
            string data = GetFromCache(routeUri + accountId);

            var list = _service.GetProductTiers(productId, Filters, Sorts, Page);
            TierListResponse response = null;

            if (list.Tiers.Count > 0)
            {
                response = new TierListResponse { Tiers = list.Tiers };
                response.MapPagingDataFromModel(list);
                data = InsertIntoCache(routeUri + accountId, routeTemplate, response);
            }
            return data;
        }

        #endregion

        #region Digital Asset

        public string GetDigitalAssets(int productId, string routeUri, string routeTemplate)
        {
            string accountId = UpdateCacheForProfile();
            string data = GetFromCache(routeUri + accountId);

            var list = _service.GetDigitalAssets(productId, Filters, Sorts, Page);
            DigitalAssetListResponse response = null;

            if (list.DigitalAssets.Count > 0)
            {
                response = new DigitalAssetListResponse { DigitalAssets = list.DigitalAssets };
                response.MapPagingDataFromModel(list);
                data = InsertIntoCache(routeUri + accountId, routeTemplate, response);
            }
            return data;
        }

        #endregion

        public string CopyProduct(int productId, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var product = _service.CopyProduct(productId);
                if (!Equals(product, null))
                {
                    var response = new ProductResponse { Product = product };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        public string SearchProducts(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.SearchProducts(Expands, Filters, Sorts, Page);
                if (list.Products.Count > 0)
                {
                    var response = new ProductListResponse { Products = list.Products };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }
        public string CreateOrderProducts(int productId, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var model = _service.CreateOrderProduct(productId);
                if (!Equals(model != null))
                {
                    var response = new ProductResponse { Product = model };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }

            }
            return data;
        }

        #endregion

        public string GetSkuProductListBySku(string routeUri, string routeTemplate)
        {
            string accountId = UpdateCacheForProfile();

            string dataFromCache = GetFromCache(routeUri + accountId);
            if (Equals(dataFromCache, null))
            {
                SkuProductListModel skuProductList = _service.GetSkuProductListBySku(Filters);
                if (skuProductList.SkuProductList.Count > 0)
                {
                    SkuProductListResponse response = new SkuProductListResponse { SkuProducts = skuProductList.SkuProductList };
                    response.MapPagingDataFromModel(skuProductList);

                    dataFromCache = InsertIntoCache(routeUri + accountId, routeTemplate, response);
                }
            }

            return dataFromCache;
        }

        //PRFT Custom Code:Start
        public string GetInventoryFromErp(string routeUri, string routeTemplate)
        {
            var accountId = UpdateCacheForProfile();

            var data = GetFromCache(routeUri + accountId);
            if (Equals(data, null))
            {
                var list = _service.GetInventoryFromErp(Expands, Filters, Sorts, Page);
                if (!Equals(list, null) && !Equals(list.Inventories, null))
                {
                    var response = new PRFTInventoryListResponse { Inventories = list.Inventories };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri + accountId, routeTemplate, response);
                }
            }
            return data;
        }
        //PRFT Custom Code:End
        #endregion
    }
}