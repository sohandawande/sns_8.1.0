﻿using Newtonsoft.Json;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
    public class OrderCache : BaseCache, IOrderCache
    {
        #region Private Variables
        private readonly IOrderService _service;
        private readonly IPRFTERPOrderServices _erpOrderService;
        #endregion

        #region Constructor
        public OrderCache(IOrderService orderService)
        {
            _service = orderService;
            _erpOrderService = new PRFTERPOrderServices();
        }
        #endregion

        #region Public Methods

        public string GetOrder(int orderId, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var order = _service.GetOrder(orderId, Expands);
                if (!Equals(order, null))
                {
                    var response = new OrderResponse { Order = order };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetOrders(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetOrders(Expands, Filters, Sorts, Page);
                if (list.Orders.Count > 0)
                {
                    var response = new OrderListResponse { Orders = list.Orders };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        #region Znode Version 8.0
        public string GetOrderList(string routeUri, string routeTemplate)
        {
            var data = string.Empty;
            var list = _service.GetOrderList(Expands, Filters, Sorts, Page);
            if (list.OrderList.Count > 0)
            {
                var response = new AdminOrderListResponse { OrderList = list.OrderList };
                response.MapPagingDataFromModel(list);

                data = JsonConvert.SerializeObject(response);
            }
            return data;
        }

        public string GetOrderDetails(string routeUri, string routeTemplate)
        {
            OrderModel model = new OrderModel();
            var data = string.Empty;
            model = _service.GetOrderDetails(Expands, Filters, Sorts, Page);

            if (!Equals(model, null))
            {
                var response = new OrderResponse { Order = model, };
                data = JsonConvert.SerializeObject(response);
            }

            return data;
        }

        public string GetOrderLineItemDetails(int orderLineItemId, string routeUri, string routeTemplate)
        {
            OrderLineItemModel model = new OrderLineItemModel();
            var data = string.Empty;
            model = _service.GetOrderLineItem(orderLineItemId, Expands);

            if (!Equals(model, null))
            {
                var response = new OrderLineItemResponse { OrderLineItem = model, };
                data = JsonConvert.SerializeObject(response);
            }

            return data;
        }

        public string DownloadOrder(string routeUri, string routeTemplate)
        {
            var data = string.Empty;
            var dataset = _service.DownloadOrderData(Filters);
            if (!Equals(dataset, null))
            {
                var response = new OrderResponse { OrderDownloadData = dataset, };
                data = JsonConvert.SerializeObject(response);
            }
            return data;
        }

        public string DownloadOrderLineItems(string routeUri, string routeTemplate)
        {
            var data = string.Empty;
            var dataset = _service.DownloadOrderLineItemsData(Filters);

            if (!Equals(dataset, null))
            {
                var response = new OrderResponse { OrderDownloadData = dataset, };
                data = JsonConvert.SerializeObject(response);
            }
            return data;
        }

        public string SendEmail(string routeUri, string routeTemplate)
        {
            string data = string.Empty;
            string emailSentMessage = _service.SendEmail(Filters);

            if (!string.IsNullOrEmpty(emailSentMessage))
            {
                OrderResponse response = new OrderResponse { EmailSentMessage = emailSentMessage, };
                data = JsonConvert.SerializeObject(response);
            }
            return data;
        }

        //PRFT Custom Code : Start
        public string GetInvoiceDetails(string externalId, string routeUri, string routeTemplate)
        {
            PRFTOEHeaderHistoryModel model = new PRFTOEHeaderHistoryModel();
            var data = string.Empty;
            model = _erpOrderService.GetOrderHistory(externalId);

            if (!Equals(model, null))
            {
                var response = new PRFTERPOrderResponse { OEHeaderHistoryModel = model, };
                data = JsonConvert.SerializeObject(response);
            }

            return data;
        }
        //PRFT Custom Code: End 
        #endregion
        #endregion
    }
}