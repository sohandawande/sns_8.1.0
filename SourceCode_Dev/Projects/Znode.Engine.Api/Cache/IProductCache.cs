﻿using Znode.Engine.Api.Models;
namespace Znode.Engine.Api.Cache
{
    public interface IProductCache
    {
        /// <summary>
        /// Get product
        /// </summary>
        /// <param name="productId">product id</param>
        /// <param name="routeUri">route uri</param>
        /// <param name="routeTemplate">route template</param>
        /// <returns></returns>
        string GetProduct(int productId, string routeUri, string routeTemplate);

        /// <summary>
        /// Get product with sku
        /// </summary>
        /// <param name="productId">product id</param>
        /// <param name="skuId">sku id</param>
        /// <param name="routeUri">route uri</param>
        /// <param name="routeTemplate">route template</param>
        /// <returns></returns>
        string GetProductWithSku(int productId, int skuId, string routeUri, string routeTemplate);

        /// <summary>
        /// Get Products
        /// </summary>
        /// <param name="routeUri">route uri</param>
        /// <param name="routeTemplate">route template</param>
        /// <returns></returns>
        string GetProducts(string routeUri, string routeTemplate);

        /// <summary>
        /// Get products by catalog
        /// </summary>
        /// <param name="catalogId">catalog id</param>
        /// <param name="routeUri">route uri</param>
        /// <param name="routeTemplate">route template</param>
        /// <returns></returns>
        string GetProductsByCatalog(int catalogId, string routeUri, string routeTemplate);

        /// <summary>
        /// Get products by catalog ids
        /// </summary>
        /// <param name="catalogIds">catalog ids</param>
        /// <param name="routeUri">route uri</param>
        /// <param name="routeTemplate">route template</param>
        /// <returns></returns>
        string GetProductsByCatalogIds(string catalogIds, string routeUri, string routeTemplate);

        /// <summary>
        /// Get products by category
        /// </summary>
        /// <param name="categoryId">category id</param>
        /// <param name="routeUri">route uri</param>
        /// <param name="routeTemplate">route template</param>
        /// <returns></returns>
        string GetProductsByCategory(int categoryId, string routeUri, string routeTemplate);

        /// <summary>
        /// Get products by category by promotion type
        /// </summary>
        /// <param name="categoryId">category id</param>
        /// <param name="promotionTypeId">promotion type d</param>
        /// <param name="routeUri">route uri</param>
        /// <param name="routeTemplate">route template</param>
        /// <returns></returns>
        string GetProductsByCategoryByPromotionType(int categoryId, int promotionTypeId, string routeUri, string routeTemplate);

        /// <summary>
        /// Get products by external ids
        /// </summary>
        /// <param name="externalIds">external ids</param>
        /// <param name="routeUri">route uri</param>
        /// <param name="routeTemplate">route template</param>
        /// <returns></returns>
        string GetProductsByExternalIds(string externalIds, string routeUri, string routeTemplate);

        /// <summary>
        /// Get products by home specials
        /// </summary>
        /// <param name="routeUri">route uri</param>
        /// <param name="routeTemplate">route template</param>
        /// <returns></returns>
        string GetProductsByHomeSpecials(string routeUri, string routeTemplate);

        /// <summary>
        /// Get products by product ids
        /// </summary>
        /// <param name="productIds">product ids</param>
        /// <param name="routeUri">route uri</param>
        /// <param name="routeTemplate">route template</param>
        /// <returns></returns>
        string GetProductsByProductIds(string productIds, string routeUri, string routeTemplate);

        /// <summary>
        /// Get products by promotion type
        /// </summary>
        /// <param name="promotionTypeId"></param>
        /// <param name="routeUri"></param>
        /// <param name="routeTemplate"></param>
        /// <returns></returns>
        string GetProductsByPromotionType(int promotionTypeId, string routeUri, string routeTemplate);

        /// <summary>
        /// To get Product Category By ProductId
        /// </summary>
        /// <param name="routeUri">string routeUri</param>
        /// <param name="routeTemplate">string routeTemplate</param>
        /// <returns>returns Product Category</returns>
        string GetProductCategoryByProductId(string routeUri, string routeTemplate);

        /// <summary>
        /// To get unassociated Category By ProductId
        /// </summary>
        /// <param name="routeUri"></param>
        /// <param name="routeTemplate"></param>
        /// <returns>returns unassociated category</returns>
        string GetProductUnAssociatedCategoryByProductId(int productId, string routeUri, string routeTemplate);

        /// <summary>
        /// Znode Version 8.0
        /// To get product details by productId
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <param name="routeUri">string routeUri</param>
        /// <param name="routeTemplate">string routeTemplate</param>
        /// <returns>returns product details</returns>
        string GetProductDetailsByProductId(int productId, string routeUri, string routeTemplate);

        /// <summary>
        /// Znode Version 8.0.
        /// Method Return the Product tags, based on product Id.
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="routeUri"></param>
        /// <param name="routeTemplate"></param>
        /// <returns>Return Product Tags.</returns>
        string GetProductTags(int productId, string routeUri, string routeTemplate);


        /// <summary>
        /// ZNode Version 8.0
        /// Method Return the Product associated facets.
        /// </summary>
        /// <param name="routeUri"></param>
        /// <param name="routeTemplate"></param>
        /// <returns>Return product associated facets.</returns>
        string GetProductFacetsList(string routeUri, string routeTemplate);

        /// <summary>
        /// ZNode Version 8.0
        /// Method Return the Sku associated facets.
        /// </summary>
        /// <param name="routeUri"></param>
        /// <param name="routeTemplate"></param>
        /// <returns>Return sku associated facets.</returns>
        string GetSkuFacets(string routeUri, string routeTemplate);

        /// <summary>
        /// Znode Version 8.0.
        /// Method Return the Product Facets Details, based on product & facet group Id.
        /// </summary>
        /// <param name="productId">productId</param>
        /// <param name="facetGroupId">facetGroupId</param>
        /// <param name="routeUri"></param>
        /// <param name="routeTemplate"></param>
        /// <returns></returns>
        string GetProductFacets(int productId, int facetGroupId, string routeUri, string routeTemplate);

        /// <summary>
        /// To Get Sku Associated Facets
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <param name="skuId">int skuId</param>
        /// <param name="facetGroupId">int facetGroupId</param>
        /// <param name="routeUri">string routeUri</param>
        /// <param name="routeTemplate"> string routeTemplate</param>
        /// <returns></returns>
        string GetSkuAssociatedFacets(int productId, int skuId, int facetGroupId, string routeUri, string routeTemplate);
        /// <summary>
        /// ZNode Version 8.0
        /// Method Return the Product image types.
        /// </summary>
        /// <param name="routeUri"></param>
        /// <param name="routeTemplate"></param>
        /// <returns>Return product image types.</returns>
        string GetProductImageTypes(string routeUri, string routeTemplate);

        /// <summary>
        /// Znode Version 8.0
        /// Method Return all Product Alternate images.
        /// </summary>
        /// <param name="routeUri"></param>
        /// <param name="routeTemplate"></param>
        /// <returns></returns>
        string GetAllProductAlternateImages(string routeUri, string routeTemplate);

        /// <summary>
        /// Znode Version 8.0
        /// Method Return the Product alternate images based on Product image id.
        /// </summary>
        /// <param name="productImageId"></param>
        /// <param name="routeUri"></param>
        /// <param name="routeTemplate"></param>
        /// <returns></returns>
        string GetProductAlternateImage(int productImageId, string routeUri, string routeTemplate);

        /// <summary>
        /// Znode Version 8.0
        /// Return associated addons for specified product id.
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="routeUri"></param>
        /// <param name="routeTemplate"></param>
        /// <returns></returns>
        string GetAssociatedAddOns(int productId, string routeUri, string routeTemplate);

        /// <summary>
        /// Znode Version 8.0
        /// Return unassociated addons for specified product id.
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="portalId"></param>
        /// <returns></returns>
        string GetUnassociatedAddOns(int productId, string routeUri, string routeTemplate, int portalId = 0);

        /// <summary>
        /// Znode Version 8.0
        /// Return associated highlights for specified product id.
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="routeUri"></param>
        /// <param name="routeTemplate"></param>
        /// <returns></returns>
        string GetAssociatedHighlights(int productId, string routeUri, string routeTemplate);

        /// <summary>
        /// Znode Version 8.0
        /// Return unassociated highlights for specified product id.
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="portalId"></param>
        /// <returns></returns>
        string GetUnassociatedHighlights(int productId, string routeUri, string routeTemplate, int portalId = 0);

        /// <summary>
        /// Znode Version 8.0
        /// Method Return all Product Bundles List.
        /// </summary>
        /// <param name="routeUri"></param>
        /// <param name="routeTemplate"></param>
        /// <returns></returns>
        string GetProductBundlesList(string routeUri, string routeTemplate);

        #region Product Sku

        /// <summary>
        /// To get product sku details 
        /// </summary>
        /// <param name="routeUri"></param>
        /// <param name="routeTemplate"></param>
        /// <returns>returns  product sku details </returns>
        string GetProductSkuDetails(string routeUri, string routeTemplate);

        #endregion

        /// <summary>
        /// Znode Version 8.0
        /// Method Return all Products List.
        /// </summary>
        /// <param name="routeUri"></param>
        /// <param name="routeTemplate"></param>
        /// <returns></returns>
        string GetProductList(string routeUri, string routeTemplate);

        /// <summary>
        /// Znode Version 8.0
        /// Gets all products. 
        /// </summary>
        /// <param name="routeUri"></param>
        /// <param name="routeTemplate"></param>
        /// <returns></returns>
        CategoryAssociatedProductsListModel GetAllProducts(string routeUri, string routeTemplate);

        /// <summary>
        /// Znode Version 8.0
        /// Return product tiers for specified product id.
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="routeUri"></param>
        /// <param name="routeTemplate"></param>
        /// <returns></returns>
        string GetProductTiers(int productId, string routeUri, string routeTemplate);

        /// <summary>
        /// Znode Version 8.0
        /// Return digital assets for specified product id.
        /// </summary>
        /// <param name="productId">product id</param>
        /// <param name="routeUri"></param>
        /// <param name="routeTemplate"></param>
        /// <returns></returns>
        string GetDigitalAssets(int productId, string routeUri, string routeTemplate);

        /// <summary>
        /// Search products
        /// </summary>
        /// <param name="routeUri"></param>
        /// <param name="routeTemplate"></param>
        /// <returns></returns>
        string SearchProducts(string routeUri, string routeTemplate);

        /// <summary>
        /// Create order roducts
        /// </summary>
        /// <param name="productId">Product Id of the product to be copied.</param>
        /// <param name="routeUri">Route URI.</param>
        /// <param name="routeTemplate">Route template.</param>
        /// <returns>String data.</returns>
        string CreateOrderProducts(int productId, string routeUri, string routeTemplate);

        /// <summary>
        /// Znode Verion 8.0
        /// Copies a product.
        /// </summary>
        /// <param name="productId">Product Id of the product to be copied.</param>
        /// <param name="routeUri">Route URI.</param>
        /// <param name="routeTemplate">Route template.</param>
        /// <returns>Copied product string data.</returns>
        string CopyProduct(int productId, string routeUri, string routeTemplate);

        /// <summary>
        /// Gets the sku product list which contains sku as a substring.
        /// </summary>
        /// <param name="routeUri">Route uri</param>
        /// <param name="routeTemplate">Route template</param>
        /// <returns>Returns the sku product list.</returns>
        string GetSkuProductListBySku(string routeUri, string routeTemplate);

        //PRFT Custom Code: Start
         /// <summary>
        /// Get Products Inventory
        /// </summary>
        /// <param name="routeUri">route uri</param>
        /// <param name="routeTemplate">route template</param>
        /// <returns></returns>
        string GetInventoryFromErp(string routeUri, string routeTemplate);
        //PRFT Custom Code: End 
    }
}