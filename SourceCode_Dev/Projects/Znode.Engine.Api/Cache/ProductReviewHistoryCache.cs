﻿using System;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
    public class ProductReviewHistoryCache:BaseCache,IProductReviewHistoryCache
    {
         private readonly IProductReviewHistoryService _service;

         public ProductReviewHistoryCache(IProductReviewHistoryService productReviewHistoryService)
		{
            _service = productReviewHistoryService;
		}

        public string GetProductReviewHistory(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetProductReviewHistory(Expands, Filters, Sorts, Page);
                if (list.ProductReviewHistory.Count > 0)
                {
                    var response = new ProductReviewHistoryListResponse { ProductsHistory = list.ProductReviewHistory };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }
        public string GetProductReviewHistoryById(int productReviewHistoryID, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var addOn = _service.GetProductReviewHistoryById(productReviewHistoryID);
                if (!Equals(addOn, null))
                {
                    var response = new ProductReviewHistoryResponse { ProductReview = addOn };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }
    }
}