﻿namespace Znode.Engine.Api.Cache
{
	public interface IHighlightTypeCache
	{
		string GetHighlightType(int highlightTypeId, string routeUri, string routeTemplate);
		string GetHighlightTypes(string routeUri, string routeTemplate);
	}
}