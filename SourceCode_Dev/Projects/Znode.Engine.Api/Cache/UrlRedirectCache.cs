﻿using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
    /// <summary>
    /// 
    /// </summary>
    public class UrlRedirectCache : BaseCache, IUrlRedirectCache
    {
        private readonly IUrlRedirectService _service;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="urlRedirectService"></param>
        public UrlRedirectCache(IUrlRedirectService urlRedirectService)
        {
            _service = urlRedirectService;
        }

        public string GetUrlRedirect(int urlRedirectId, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var urlRedirect = _service.GetUrlRedirect(urlRedirectId);
                if (!Equals(urlRedirect, null))
                {
                    var response = new UrlRedirectResponse { UrlRedirect = urlRedirect };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetUrlRedirects(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetUrlRedirects(Expands, Filters, Sorts, Page);
                if (list.UrlRedirects.Count > 0)
                {
                    var response = new UrlRedirectListResponse { UrlRedirects = list.UrlRedirects };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }
    }
}