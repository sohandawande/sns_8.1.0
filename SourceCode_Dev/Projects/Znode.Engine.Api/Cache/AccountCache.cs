﻿using Microsoft.Web.WebPages.OAuth;
using Newtonsoft.Json;
using System;
using System.Configuration;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
	public class AccountCache : BaseCache, IAccountCache
	{
		private readonly IAccountService _service;        

		public AccountCache(IAccountService accountService)
		{
			_service = accountService;            
		}

		public string GetAccount(int accountId, string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var account = _service.GetAccount(accountId, Expands);
				if (account != null)
				{
					var response = new AccountResponse { Account = account };
					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}

		public string GetAccountByUserId(Guid userId, string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var account = _service.GetAccountByUserId(userId, Expands);
				if (account != null)
				{
					var response = new AccountResponse { Account = account };
					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}

		public string GetAccountByUsername(string username, string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
                var account = _service.GetAccountByUsername(username, Expands);
				if (account != null)
				{
					var response = new AccountResponse { Account = account };
					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}

		public string GetAccounts(string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var list = _service.GetAccounts(Expands, Filters, Sorts, Page);
				if (list.Accounts.Count > 0)
				{
					var response = new AccountListResponse { Accounts = list.Accounts };
					response.MapPagingDataFromModel(list);

					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}

		public AccountModel Login(int portalId, AccountModel model, out string errorCode)
		{
			return _service.Login(portalId, model, out errorCode, Expands);
		}

        public string GetAccountDetailsByRoleName(string roleName, out int totalRowCount, string routeUri, string routeTemplate)
        {
            string data = string.Empty;
            var accountList = _service.GetAccountDetailsByRoleName(Filters, Sorts, Page, out totalRowCount);
            if (!Equals(accountList, null) && accountList.Accounts.Count >0)
            {
                var response = new AccountListResponse { Accounts = accountList.Accounts };
                response.MapPagingDataFromModel(accountList);

                data = JsonConvert.SerializeObject(response);
            }

            return data;
        }

        public string EnableDisableAccount(int accountId)
        {
            string data = string.Empty;
            bool isEnabled = _service.EnableDisableAccount(accountId);
            BooleanModel boolModel = new BooleanModel { disabled = isEnabled };
            TrueFalseResponse response = new TrueFalseResponse { booleanModel = boolModel };
            data = JsonConvert.SerializeObject(response);
            return data;
        }

        public string GetAccountPaymentList(int accountId,string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data,null))
            {
                var list = _service.GetAccountPaymentList(accountId,Filters, Sorts, Page);
                if (list.AccountPayments.Count > 0)
                {
                    var response = new AccountPaymentListResponse { AccountPayments = list.AccountPayments };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }

        public AccountModel SocialUserLogin(int portalId, SocialLoginModel model)
        {
            return _service.SocialUserLogin(portalId, model, Expands);
        }

        #region PRFTCustom Code: Start
        public string GetSubUserList(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {               
                var list = _service.GetSubUserList(Expands, Filters, Sorts, Page);
                if (list.Accounts.Count > 0)
                {
                    var response = new AccountListResponse { Accounts = list.Accounts };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }
        #endregion PRFTCustom Code: End
    }
}