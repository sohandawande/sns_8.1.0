﻿using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
    /// <summary>
    /// Message Config Cache
    /// </summary>
	public class MessageConfigCache : BaseCache, IMessageConfigCache
	{
        #region Private Variables

        private readonly IMessageConfigService _service; 

        #endregion

        #region Public Methods

        public MessageConfigCache(IMessageConfigService messageConfigService)
        {
            _service = messageConfigService;
        }

        public string GetMessageConfig(int messageConfigId, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var messageConfig = _service.GetMessageConfig(messageConfigId, Expands);
                if (!Equals(messageConfig, null))
                {
                    var response = new MessageConfigResponse { MessageConfig = messageConfig };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetMessageConfigByKey(string key, int portalId, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var messageConfig = _service.GetMessageConfig(key, portalId, 43, 1);
                if (!Equals(messageConfig, null))
                {
                    var response = new MessageConfigResponse { MessageConfig = messageConfig };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetMessageConfigs(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetMessageConfigs(Expands, Filters, Sorts, Page);
                if (list.MessageConfigs.Count > 0)
                {
                    var response = new MessageConfigListResponse { MessageConfigs = list.MessageConfigs };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetMessageConfigsByKeys(string keys, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetMessageConfigsByKeys(keys, Expands, Sorts);
                if (list.MessageConfigs.Count > 0)
                {
                    var response = new MessageConfigListResponse { MessageConfigs = list.MessageConfigs };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        } 

        #endregion
	}
}