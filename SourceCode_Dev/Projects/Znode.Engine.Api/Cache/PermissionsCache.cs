﻿using Newtonsoft.Json;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
    /// <summary>
    /// This is the class created to talk to Permission services
    /// </summary>
    public class PermissionsCache : BaseCache, IPermissionsCache
    {
        #region Private Variables
        private readonly IPermissionsService _service;
        #endregion

        #region Constructors
        public PermissionsCache(IPermissionsService permissionService)
        {
            _service = permissionService; 
        }
        #endregion

        #region Public Methods
        public string GetRolesAndPerminssions(int accountId, string routeUri, string routeTemplate)
        {
            string data = string.Empty;
            var permissions = _service.GetRolesAndPerminssions(accountId, Filters, Sorts);
            if (!Equals(permissions, null) && permissions.roleModel.Permissions.Count > 0)
            {
                var response = new PermissionsResponse { Permissions = permissions };
                data = JsonConvert.SerializeObject(response);
            }

            return data;
        }

        public string UpdateRolesAndPermissions(PermissionsModel model, string routeUri, string routeTemplate)
        {
            string data = string.Empty;
            bool isUpdated = _service.UpdateRolesAndPermissions(model);

            BooleanModel boolModel = new BooleanModel { disabled = isUpdated };
            TrueFalseResponse response = new TrueFalseResponse { booleanModel = boolModel };
            data = JsonConvert.SerializeObject(response);

            return data;
        }
        #endregion
    }
}