﻿namespace Znode.Engine.Api.Cache
{
    /// <summary>
    /// Tax Rule Type Cache Interface
    /// </summary>
	public interface ITaxRuleTypeCache
	{
        /// <summary>
        /// Get Tax Rule Type
        /// </summary>
        /// <param name="taxRuleTypeId">taxRuleTypeId</param>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns>Returns tax rule type</returns>
		string GetTaxRuleType(int taxRuleTypeId, string routeUri, string routeTemplate);

        /// <summary>
        /// Get list of tax rule types
        /// </summary>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns>Returns list of tax rule typess</returns>
		string GetTaxRuleTypes(string routeUri, string routeTemplate);

        /// <summary>
        /// ZNode Version 8.0
        /// Get all Tax Rule Types not present in database.
        /// </summary>
        /// <param name="routeUri">string routeUri</param>
        /// <param name="routeTemplate">string routeTemplate</param>
        /// <returns></returns>
        string GetAllTaxRuleTypesNotInDatabase(string routeUri, string routeTemplate);
	}
}