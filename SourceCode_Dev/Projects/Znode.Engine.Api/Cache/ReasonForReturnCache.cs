﻿using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
    public class ReasonForReturnCache : BaseCache, IReasonForReturnCache
    {
        #region Private Variables
        private readonly IReasonForReturnService _service; 
        #endregion

        #region Constructor
        public ReasonForReturnCache(IReasonForReturnService reasonForReturnService)
        {
            _service = reasonForReturnService;
        } 
        #endregion

        #region Public Methods
        public string GetReasonForReturn(int reasonForReturnId, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var reasonForReturn = _service.GetReasonForReturn(reasonForReturnId, Expands);
                if (!Equals(reasonForReturn,null))
                {
                    var response = new ReasonForReturnResponse { ReasonForReturn = reasonForReturn };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }

        public string GetListOfReasonForReturn(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetListOfReasonForReturn(Expands, Filters, Sorts, Page);
                if (list.ReasonsForReturn.Count > 0)
                {
                    var response = new ReasonForReturnListResponse { ReasonForReturnList = list.ReasonsForReturn };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        } 
        #endregion
    }
}