﻿namespace Znode.Engine.Api.Cache
{
	public interface ICaseRequestCache
	{
		string GetCaseRequest(int caseRequestId, string routeUri, string routeTemplate);
		string GetCaseRequests(string routeUri, string routeTemplate);

        /// <summary>
        /// To GetCaseStatus 
        /// </summary>
        /// <param name="routeUri"></param>
        /// <param name="routeTemplate"></param>
        /// <returns></returns>
        string GetCaseStatus(string routeUri, string routeTemplate);

        /// <summary>
        /// To GetCasePriority
        /// </summary>
        /// <param name="routeUri"></param>
        /// <param name="routeTemplate"></param>
        /// <returns></returns>
        string GetCasePriority(string routeUri, string routeTemplate);

        /// <summary>
        /// To GetCaseNote
        /// </summary>
        /// <param name="routeUri"></param>
        /// <param name="routeTemplate"></param>
        /// <returns></returns>
        string GetCaseNote(string routeUri, string routeTemplate);
	}
}