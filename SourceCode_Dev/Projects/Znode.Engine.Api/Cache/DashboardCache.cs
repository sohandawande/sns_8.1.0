﻿using Newtonsoft.Json;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
    /// <summary>
    /// Cache for Dashboard.
    /// </summary>
    public class DashboardCache : BaseCache, IDashboardCache
    {
        #region Private Variables
        private readonly IDashboardService _service;
        #endregion

        #region Constructor
        public DashboardCache(IDashboardService dashboardService)
        {
            _service = dashboardService;
        }
        #endregion

        #region Public methods
        public string GetDashboard(int portalId, string routeUri, string routeTemplate)
        {
            var data = string.Empty;
            var dashboard = _service.GetDashboardItemsByPortalId(portalId);

            if (!Equals(dashboard, null))
            {
                var response = new DashboardResponse { Dashboard = dashboard };
                data = JsonConvert.SerializeObject(response);
            }

            return data;
        }
        #endregion
    }
}