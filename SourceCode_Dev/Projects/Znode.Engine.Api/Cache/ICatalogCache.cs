﻿namespace Znode.Engine.Api.Cache
{
	public interface ICatalogCache
	{
        /// <summary>
        /// Gets the catalog details by catalog id.
        /// </summary>
        /// <param name="catalogId">int catalog Id</param>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns>Returns string</returns>
		string GetCatalog(int catalogId, string routeUri, string routeTemplate);

        /// <summary>
        /// Gets the list of catalog.
        /// </summary>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns>Returns string</returns>
		string GetCatalogs(string routeUri, string routeTemplate);

        /// <summary>
        /// Gets the catalogs list by catalog ids.
        /// </summary>
        /// <param name="catalogIds">string catalog ids.</param>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns>Returns string</returns>
        string GetCatalogsByCatalogIds(string catalogIds, string routeUri, string routeTemplate);

        /// <summary>
        /// Get the List of Catalogs
        /// </summary>
        /// <param name="portalId">PortalID by which retrieve catalogList</param>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns></returns>
        string GetCatalogsByPortalId(int portalId, string routeUri, string routeTemplate);
    }
}