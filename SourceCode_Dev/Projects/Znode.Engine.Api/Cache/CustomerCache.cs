﻿using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
    public class CustomerCache : BaseCache, ICustomerCache
    {
        #region Private Variables

        private readonly ICustomerService _service; 
        #endregion

        #region Default Constructor

        public CustomerCache(ICustomerService customerService)
        {
            _service = customerService;
        } 
        #endregion

        #region Public Methods

        public string GetCustomerList(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetCustomerList(Expands, Filters, Sorts, Page);
                if (list.CustomerList.Count > 0)
                {
                    var response = new CustomerListResponse { CustomerAccount = list.CustomerList };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }        
        #endregion

        #region Customer Affiliate
        public string GetCustomerAffiliate(int accountId, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var customerAffiliate = _service.GetCustomerAffiliate(accountId, Expands);

                if (!Equals(customerAffiliate, null))
                {
                    var response = new CustomerAffiliateResponse { CustomerAffiliate = customerAffiliate };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }

        public string GetReferralCommissionTypeList(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetReferralCommissionTypeList(Expands, Filters, Sorts, Page);
                if (list.ReferralCommissionTypes.Count > 0)
                {
                    var response = new ReferralCommissionTypeListResponse { ReferralCommissionTypes = list.ReferralCommissionTypes };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }

        public string GetReferralCommissionList(int accountId,string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetReferralCommissionList(accountId,Expands, Filters, Sorts, Page);
                if (list.ReferralCommissions.Count > 0)
                {
                    var response = new ReferralCommissionListResponse { ReferralCommissions = list.ReferralCommissions };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }
        #endregion

        #region PRFT Custom Code
        public string GetAssociatedSuperUserList(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetAssociatedSuperUserList(Expands, Filters, Sorts, Page);
                if (list.CustomerList.Count > 0)
                {
                    var response = new CustomerListResponse { CustomerAccount = list.CustomerList };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetNotAssociatedSuperUserList(int accountId,string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetNotAssociatedSuperUserList(accountId, Expands, Filters, Sorts, Page);
                if (list.CustomerList.Count > 0)
                {
                    var response = new CustomerListResponse { CustomerAccount = list.CustomerList };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }
        #endregion
    }
}