﻿using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
    public class PortalProfileCache : BaseCache, IPortalProfileCache
    {
        #region Private variables
        private readonly IPortalProfileService _service;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor for PortalProfileCache
        /// </summary>
        /// <param name="portalProfileService"></param>
        public PortalProfileCache(IPortalProfileService portalProfileService)
        {
            _service = portalProfileService;
        }
        #endregion

        #region Public Methods
        public string GetPortalProfile(int portalProfileId, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data,null))
            {
                var portalProfile = _service.GetPortalProfile(portalProfileId, Expands);
                if (portalProfile != null)
                {
                    var response = new PortalProfileResponse { PortalProfile = portalProfile };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetPortalProfiles(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetPortalProfiles(Expands, Filters, Sorts, Page);
                if (list.PortalProfiles.Count > 0)
                {
                    var response = new PortalProfileListResponse { PortalProfiles = list.PortalProfiles };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetPortalProfilesDetails(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetPortalProfileListDetails(Expands, Filters, Sorts, Page);
                if (list.PortalProfiles.Count > 0)
                {
                    var response = new PortalProfileListResponse { PortalProfiles = list.PortalProfiles };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetPortalProfilesByPortalId(int portalId,string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetPortalProfilesByPortalId(portalId);
                if (list.PortalProfiles.Count > 0)
                {
                    var response = new PortalProfileListResponse { PortalProfiles = list.PortalProfiles };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }
        #endregion
    }
}