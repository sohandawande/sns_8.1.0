﻿using Znode.Engine.Api.Models;
namespace Znode.Engine.Api.Cache
{
    public interface IProductAttributesCache
    {
        string GetAttribute(int attributeId, string routeUri, string routeTemplate);
        string GetAttributes(string routeUri, string routeTemplate);
        
        /// <summary>
        /// Get all Attributes by AttributetypeId
        /// </summary>
        /// <param name="routeUri">string routeUri</param>
        /// <param name="routeTemplate">string routeTemplate</param>
        /// <returns></returns>
        string GetAttributesByAttributeTypeId(string routeUri, string routeTemplate);
    }
}
