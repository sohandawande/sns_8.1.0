﻿
namespace Znode.Engine.Api.Cache
{
    public interface ICurrencyTypeCache
    {
        /// <summary>
        /// CurrencyType list.
        /// </summary>
        /// <param name="routeUri">Route URI</param>
        /// <param name="routeTemplate">Route Template</param>
        /// <returns>String Data.</returns>
        string GetCurrencyTypes(string routeUri, string routeTemplate);

        /// <summary>
        /// Currency type.
        /// </summary>
        /// <param name="currencyTypeId">Currency Type Id.</param>
        /// <param name="routeUri">String Route URI.</param>
        /// <param name="routeTemplate">Route Template.</param>
        /// <returns>String Data.</returns>
        string GetCurrencyType(int currencyTypeId, string routeUri, string routeTemplate);
    }
}
