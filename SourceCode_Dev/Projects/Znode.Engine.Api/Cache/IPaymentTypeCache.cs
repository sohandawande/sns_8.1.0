﻿namespace Znode.Engine.Api.Cache
{
	public interface IPaymentTypeCache
	{
		string GetPaymentType(int paymentTypeId, string routeUri, string routeTemplate);
		string GetPaymentTypes(string routeUri, string routeTemplate);
	}
}