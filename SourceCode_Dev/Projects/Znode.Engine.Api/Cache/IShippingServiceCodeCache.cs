﻿namespace Znode.Engine.Api.Cache
{
    /// <summary>
    /// Shipping Service Code Cache Interface
    /// </summary>
	public interface IShippingServiceCodeCache
	{
        /// <summary>
        /// Get Shipping Service Code on the basis of shippingServiceCodeId
        /// </summary>
        /// <param name="shippingServiceCodeId">shippingServiceCodeId</param>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns>Returns Shipping Service Code</returns>
		string GetShippingServiceCode(int shippingServiceCodeId, string routeUri, string routeTemplate);

        /// <summary>
        /// Get list of Shipping Service Codes
        /// </summary>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns>Returns list of Shipping Service Code</returns>
		string GetShippingServiceCodes(string routeUri, string routeTemplate);
	}
}