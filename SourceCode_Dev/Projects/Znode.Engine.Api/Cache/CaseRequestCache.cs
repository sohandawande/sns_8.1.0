﻿using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
    public class CaseRequestCache : BaseCache, ICaseRequestCache
    {
        private readonly ICaseRequestService _service;

        public CaseRequestCache(ICaseRequestService caseRequestService)
        {
            _service = caseRequestService;
        }

        public string GetCaseRequest(int caseRequestId, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var caseRequest = _service.GetCaseRequest(caseRequestId, Expands);
                if (!Equals(caseRequest, null))
                {
                    var response = new CaseRequestResponse { CaseRequest = caseRequest };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetCaseRequests(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetCaseRequestList(Expands, Filters, Sorts, Page);
                if (list.CaseRequests.Count > 0)
                {
                    var response = new CaseRequestListResponse { CaseRequests = list.CaseRequests };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        #region Znode Version 8.0

        public string GetCaseStatus(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetCaseStatus(Expands, Filters, Sorts, Page);
                if (list.CaseStatuses.Count > 0)
                {
                    var response = new CaseStatusListResponse { CaseStatus = list.CaseStatuses };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetCasePriority(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetCasePriority(Expands, Filters, Sorts, Page);
                if (list.CasePriorities.Count > 0)
                {
                    var response = new CasePrioritiesListResponse { CasePriorities = list.CasePriorities };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetCaseNote(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetCaseNote(Expands, Filters, Sorts, Page);
                if (list.Notes.Count > 0)
                {
                    var response = new CaseNoteListResponse { CaseNotes = list.Notes };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        #endregion
    }
}