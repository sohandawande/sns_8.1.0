﻿
namespace Znode.Engine.Api.Cache
{
    public interface IMasterPageCache
    {
        /// <summary>
        /// Gets the master pages.
        /// </summary>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns>Returns string</returns>
        string GetMasterPages(string routeUri, string routeTemplate);

        /// <summary>
        /// Gets the master page by themeId.
        /// </summary>
        /// <param name="themeId">int themeId</param>
        /// <param name="pageType">string pageType</param>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns>Returns string</returns>
        string GetMasterPageByThemeId(int themeId, string pageType, string routeUri, string routeTemplate);
    }
}
