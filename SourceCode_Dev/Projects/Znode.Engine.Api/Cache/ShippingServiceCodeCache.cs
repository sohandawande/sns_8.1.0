﻿using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
	public class ShippingServiceCodeCache : BaseCache, IShippingServiceCodeCache
	{
        #region Private Variables

        private readonly IShippingServiceCodeService _service; 

        #endregion

		#region MyRegion

		public ShippingServiceCodeCache(IShippingServiceCodeService shippingServiceCodeService)
		{
			_service = shippingServiceCodeService;
		}

		public string GetShippingServiceCode(int shippingServiceCodeId, string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (Equals(data, null))
			{
				var shippingServiceCode = _service.GetShippingServiceCode(shippingServiceCodeId, Expands);
				if (!Equals(shippingServiceCode, null))
				{
					var response = new ShippingServiceCodeResponse { ShippingServiceCode = shippingServiceCode };
					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}

		public string GetShippingServiceCodes(string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
            if (Equals(data, null))
			{
				var list = _service.GetShippingServiceCodes(Expands, Filters, Sorts, Page);
				if (list.ShippingServiceCodes.Count > 0)
				{
					var response = new ShippingServiceCodeListResponse { ShippingServiceCodes = list.ShippingServiceCodes };
					response.MapPagingDataFromModel(list);

					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		} 

	#endregion
	}
}