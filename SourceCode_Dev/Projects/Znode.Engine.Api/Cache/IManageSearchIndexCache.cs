﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Cache
{
    public interface IManageSearchIndexCache
    {
        /// <summary>
        /// Get Lucene Index Windows service status list
        /// </summary>
        /// <param name="routeUri"></param>
        /// <param name="routeTemplate"></param>
        /// <returns></returns>
        string GetLuceneIndexStatus(string routeUri, string routeTemplate);

        /// <summary>
        /// Get index server status log details by ServerIndexId
        /// </summary>
        /// <param name="indexId">Index Id</param>
        /// <param name="routeUri"></param>
        /// <param name="routeTemplate"></param>
        /// <returns></returns>
        string GetIndexServerStatus(int indexId,string routeUri, string routeTemplate);
        
    }
}
