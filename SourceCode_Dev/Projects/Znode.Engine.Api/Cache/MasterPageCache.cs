﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
    public class MasterPageCache : BaseCache, IMasterPageCache
    {
        #region Private Variables
        private readonly IMasterPageService _service; 
        #endregion

        #region Constructor
        public MasterPageCache(IMasterPageService masterPageService)
        {
            _service = masterPageService;
        } 
        #endregion

        #region Public Methods
        public string GetMasterPages(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetMasterPages(Expands, Filters, Sorts, Page);
                if (list.MasterPages.Count > 0)
                {
                    var response = new MasterPageListResponse { MasterPages = list.MasterPages };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }


        public string GetMasterPageByThemeId(int themeId, string pageType, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetMasterPageListByThemeId(themeId, pageType);
                if (list.MasterPages.Count > 0)
                {
                    var response = new MasterPageListResponse { MasterPages = list.MasterPages };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        } 
        #endregion
    }
}