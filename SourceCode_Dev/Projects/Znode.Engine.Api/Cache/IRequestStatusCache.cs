﻿
namespace Znode.Engine.Api.Cache
{
   public interface IRequestStatusCache
    {
        /// <summary>
        /// Get RequestStatus by reasonForReturnId
        /// </summary>
        /// <param name="requestStatusId">RequestStatus Id by which get RequestStatus </param>
        /// <param name="routeUri">Route URI</param>
        /// <param name="routeTemplate">Route template</param>
        /// <returns>RequestStatus model</returns>
        string GetRequestStatus(int requestStatusId, string routeUri, string routeTemplate);

       /// <summary>
       /// Gets list of Request status.
       /// </summary>
       /// <param name="routeUri">Route URI.</param>
       /// <param name="routeTemplate">Route Template.</param>
       /// <returns>String data.</returns>
       string GetRequestStatusList(string routeUri, string routeTemplate);

       
    }
}
