﻿
namespace Znode.Engine.Api.Cache
{
    public interface IDashboardCache
    {
        /// <summary>
        /// Get the dashboard items.
        /// </summary>
        /// <param name="portalId">Id of the portal.</param>
        /// <param name="routeUri">Route URI.</param>
        /// <param name="routeTemplate">Route template.</param>
        /// <returns></returns>
        string GetDashboard(int portalId, string routeUri, string routeTemplate);
    }
}
