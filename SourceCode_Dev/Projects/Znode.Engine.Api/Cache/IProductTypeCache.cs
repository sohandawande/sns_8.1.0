﻿using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Cache
{
    public interface IProductTypeCache
    {
        /// <summary>
        /// To Get product Type by productTypeId
        /// </summary>
        /// <param name="productTypeId"></param>
        /// <param name="routeUri"></param>
        /// <param name="routeTemplate"></param>
        /// <returns>Return String</returns>
        string GetProductType(int productTypeId, string routeUri, string routeTemplate);

        /// <summary>
        /// To get Product Types List.
        /// </summary>
        /// <param name="routeUri"></param>
        /// <param name="routeTemplate"></param>
        /// <returns>Return Product Types.</returns>
        string GetProductTypes(string routeUri, string routeTemplate);

        /// <summary>
        /// To get products and its value by productTypeId
        /// </summary>
        /// <param name="productTypeId">int productTypeId</param>
        /// <returns>returns AttributeTypeValueListModel</returns>
        string GetProductAttributesByProductTypeId(string routeUri, string routeTemplate);
    }
}