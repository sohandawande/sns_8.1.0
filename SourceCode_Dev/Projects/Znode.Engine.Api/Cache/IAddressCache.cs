﻿namespace Znode.Engine.Api.Cache
{
	public interface IAddressCache
	{
		string GetAddress(int addressId, string routeUri, string routeTemplate);
		string GetAddresses(string routeUri, string routeTemplate);
	}
}