﻿namespace Znode.Engine.Api.Cache
{
    public interface ICategoryCache
    {
        /// <summary>
        /// Gets the category details.
        /// </summary>
        /// <param name="categoryId">int category Id</param>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns>Returns string</returns>
        string GetCategory(int categoryId, string routeUri, string routeTemplate);

        /// <summary>
        /// Gets the categories list.
        /// </summary>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns>Returns string</returns>
        string GetCategories(string routeUri, string routeTemplate);

        /// <summary>
        /// Gets the ctageories by catalogId.
        /// </summary>
        /// <param name="catalogId">int catalogId</param>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns>Returns string</returns>
        string GetCategoriesByCatalog(int catalogId, string routeUri, string routeTemplate);

        /// <summary>
        /// Gets the categories by catalogIds.
        /// </summary>
        /// <param name="catalogIds">string catalogIds</param>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns>Returns string</returns>
        string GetCategoriesByCatalogIds(string catalogIds, string routeUri, string routeTemplate);

        /// <summary>
        /// Gets the categories list by categoryids
        /// </summary>
        /// <param name="categoryIds">string category ids</param>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns>Returns string</returns>
        string GetCategoriesByCategoryIds(string categoryIds, string routeUri, string routeTemplate);

        /// <summary>
        /// Znode Version 8.0
        /// Gets the category list by catalog Id using custom service.       
        /// </summary>
        /// <param name="totalRowCount">totalRowCount</param>
        /// <param name="catalogId">catalogId</param>       
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns>Returns string</returns>
        string GetCategoriesByCatalogIdUsingCustomService(out int totalRowCount, int catalogId = 0, string routeUri = "", string routeTemplate = "");

        /// <summary>
        ///  Znode Version 8.0
        /// Gets all categories.  
        /// </summary>
        /// <param name="routeUri">string routeUri</param>
        /// <param name="routeTemplate">string routeTemplate</param>
        /// <returns>Returns string</returns>
        string GetAllCategories(string routeUri, string routeTemplate);

        /// <summary>
        /// Get the List of Category Associated Products
        /// </summary>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns>Returns string</returns>
        string GetCategoryAssociatedProducts(string routeUri, string routeTemplate);

        /// <summary>
        /// Get the List of Category Sub-Category & its Products
        /// </summary>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns>Returns string</returns>
        string GetCategoryTree(string routeUri, string routeTemplate);
    }
}