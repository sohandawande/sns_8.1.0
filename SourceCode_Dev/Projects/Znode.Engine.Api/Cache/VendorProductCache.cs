﻿using System;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;
namespace Znode.Engine.Api.Cache
{
    public class VendorProductCache : BaseCache, IVendorProductCache
    {
        private readonly IVendorProductService _service;

        public VendorProductCache(IVendorProductService vendorProductService)
		{
            _service = vendorProductService;
		}

        public string GetVendorProductList(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data,null))
            {
                var list = _service.GetVendorProductList(Expands, Filters, Sorts, Page);
                if (list.Products.Count > 0)
                {
                    var response = new VendorProductListResponse { Products = list.Products };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetMallAdminProductList(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetMallAdminProductList(Expands, Filters, Sorts, Page);
                if (list.Products.Count > 0)
                {
                    var response = new VendorProductListResponse { Products = list.Products };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetReviewImageList(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetReviewImageList(Expands, Filters, Sorts, Page);
                if (list.ProductImages.Count > 0)
                {
                    var response = new ProductAlternateImageListResponse { ProductImages = list.ProductImages };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }
    }
}