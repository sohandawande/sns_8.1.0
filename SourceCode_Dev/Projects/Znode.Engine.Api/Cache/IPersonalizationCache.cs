﻿
namespace Znode.Engine.Api.Cache
{
    public interface IPersonalizationCache
    {
         /// <summary>
        /// Gets Product list with Frequently bought products.
        /// </summary>
        /// <param name="routeUri">String route Uri.</param>
        /// <param name="routeTemplate">String Route Template.</param>
        /// <returns>String data.</returns>
        string GetFrequentlyBoughtProducts(string routeUri, string routeTemplate);

        /// <summary>
        /// Gets cross sell product by product id.
        /// </summary>
        /// <param name="routeUri">Route URI</param>
        /// <param name="routeTemplate">Route Templated</param>
        /// <returns>String data</returns>
        string GetProductCrossSellbyProductId(string routeUri, string routeTemplate);
    }
}
