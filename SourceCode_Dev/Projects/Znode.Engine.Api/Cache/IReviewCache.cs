﻿namespace Znode.Engine.Api.Cache
{
	public interface IReviewCache
	{
		string GetReview(int reviewId, string routeUri, string routeTemplate);
		string GetReviews(string routeUri, string routeTemplate);
	}
}