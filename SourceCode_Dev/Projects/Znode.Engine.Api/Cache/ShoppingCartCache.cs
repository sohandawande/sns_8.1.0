﻿using ZNode.Libraries.DataAccess.Entities;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;
using System.Web;

namespace Znode.Engine.Api.Cache
{
	public class ShoppingCartCache : BaseCache, IShoppingCartCache
	{
		private readonly IShoppingCartService _service;

		public ShoppingCartCache(IShoppingCartService shoppingCartService)
		{
			_service = shoppingCartService;
		}

		public ShoppingCartResponse GetCartByAccount(int accountId, int portalId)
		{
			UpdateCacheForProfile();

			var data = _service.GetCartByAccount(accountId, portalId);

		    var response = new ShoppingCartResponse {ShoppingCart = data};

		    return response;
		}

		public ShoppingCartResponse GetCartByCookie(int cookieId)
		{
			UpdateCacheForProfile();

			var data = _service.GetCartByCookie(cookieId);
			return data != null ? new ShoppingCartResponse { ShoppingCart = data } : null;
		}

		public ShoppingCartResponse CreateCart(int portalId, ShoppingCartModel model)
		{
			UpdateCacheForProfile();

			var data = _service.CreateCart(portalId, model);
			return data != null ? new ShoppingCartResponse { ShoppingCart = data } : null;
		}

		public ShoppingCartResponse Calculate(ShoppingCartModel model)
		{
			UpdateCacheForProfile();

			var profileId = 0;
			if (model.Account != null && HttpContext.Current.Cache["ProfieCache_" + model.Account.AccountId] != null)
			{
				var profile = (Profile)HttpContext.Current.Cache["ProfieCache_" + model.Account.AccountId];
				profileId = profile.ProfileID;
			}
			else
			{
				var profile = (Profile)HttpContext.Current.Session["ProfileCache"];
				profileId = profile.ProfileID;
			}

			var data = _service.Calculate(profileId, model);
			return data != null ? new ShoppingCartResponse { ShoppingCart = data } : null;
		}
	}
}