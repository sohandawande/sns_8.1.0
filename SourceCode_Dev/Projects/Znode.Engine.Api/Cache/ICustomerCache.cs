﻿
namespace Znode.Engine.Api.Cache
{
    /// <summary>
    /// Interface for Customer Cache
    /// </summary>
    public interface ICustomerCache
    {
        /// <summary>
        /// Get All customer list
        /// </summary>
        /// <param name="routeUri">string Route URI</param>
        /// <param name="routeTemplate">string Route Template</param>
        /// <returns>Returns Customer Details</returns>
        string GetCustomerList(string routeUri, string routeTemplate);

        /// <summary>
        /// This method will get the customeraffiliate details by account id
        /// </summary>
        /// <param name="accountId">int Account ID</param>
        /// <param name="routeUri">string Route URI</param>
        /// <param name="routeTemplate">string Route Template</param>
        /// <returns>Retunrs the customeraffiliate details</returns>
        string GetCustomerAffiliate(int accountId, string routeUri, string routeTemplate);

        /// <summary>
        /// Get All ReferralCommissionType list
        /// </summary>
        /// <param name="routeUri">string Route URI</param>
        /// <param name="routeTemplate">string Route Template</param>
        /// <returns>Returns ReferralCommissionType Details</returns>
        string GetReferralCommissionTypeList(string routeUri, string routeTemplate);

        /// <summary>
        /// Get All ReferralCommission list
        /// </summary>
        /// <param name="routeUri">string Route URI</param>
        /// <param name="routeTemplate">string Route Template</param>
        /// <returns>Returns ReferralCommission Details</returns>
        string GetReferralCommissionList(int accountId, string routeUri, string routeTemplate);

        #region  PRFT Custom Code
        /// <summary>
        /// Get All associated super user list
        /// </summary>
        /// <param name="routeUri">string Route URI</param>
        /// <param name="routeTemplate">string Route Template</param>
        /// <returns>Returns Customer Details</returns>
        string GetAssociatedSuperUserList(string routeUri, string routeTemplate);

        /// <summary>
        /// Get list of not associated customer to user
        /// </summary>
        /// <param name="routeUri">string Route URI</param>
        /// <param name="routeTemplate">string Route Template</param>
        /// <returns>Returns Customer Details</returns>
        string GetNotAssociatedSuperUserList(int accountId,string routeUri, string routeTemplate);
        
        #endregion PRFT Custom Code
    }
}