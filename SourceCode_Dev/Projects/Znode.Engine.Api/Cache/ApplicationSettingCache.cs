﻿using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
    public class ApplicationSettingCache : BaseCache, IApplicationSettingCache
    {
        #region Private Variables
        private readonly IApplicationSettingService _service;
        #endregion

        #region Constructor
        public ApplicationSettingCache(IApplicationSettingService applicationSetting)
        {
            _service = applicationSetting;
        }
        #endregion
        
        public string GetApplicationSettings(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetApplicationSettings(Expands, Filters, Sorts, Page);
                if (list.ApplicationSettingList.Count > 0)
                {
                    var response = new ApplicationSettingListResponse { List = list.ApplicationSettingList};
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetColumnList(string routeUri, string routeTemplate, string entityType, string entityName)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var dataset = _service.GetColumnList(entityType, entityName);
                if (!Equals(dataset,null))
                {
                    var response = new ApplicationSettingListResponse { ColumnList = dataset };

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }
    }
}