﻿
namespace Znode.Engine.Api.Cache
{
    public interface IReportsCache
    {
        /// <summary>
        /// Get report data set by report name
        /// </summary>
        /// <param name="reportName">Report Name</param>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns></returns>
        string GetReportDataSet(string reportName, string routeUri, string routeTemplate);

        /// <summary>
        /// Get order details by order id.
        /// </summary>
        /// <param name="orderId">int order id</param>
        /// <param name="routeUri"></param>
        /// <param name="routeTemplate"></param>
        /// <returns></returns>
        string GetOrderDetails(int orderId, string reportName , string routeUri, string routeTemplate);
    }
}
