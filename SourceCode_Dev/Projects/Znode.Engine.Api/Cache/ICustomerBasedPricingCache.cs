﻿using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Cache
{
    public interface ICustomerBasedPricingCache
    {
        /// <summary>
        /// Get Customer Based Pricing Cache.
        /// </summary>
        /// <param name="routeUri">route url</param>
        /// <param name="routeTemplate">string url</param>
        /// <returns>CustomerBasedPricingListModel</returns>
        CustomerBasedPricingListModel GetCustomerBasedPricing(string routeUri, string routeTemplate);

        /// <summary>
        /// Get Customer Based Pricing Product Cache.
        /// </summary>
        /// <param name="routeUri">route url</param>
        /// <param name="routeTemplate">string url</param>
        /// <returns>CustomerBasedPricingListModel</returns>
        CustomerBasedPricingListModel GetCustomerPricingProduct(string routeUri, string routeTemplate);
    }
}