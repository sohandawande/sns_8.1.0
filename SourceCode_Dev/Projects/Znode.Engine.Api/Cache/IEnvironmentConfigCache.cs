﻿
namespace Znode.Engine.Api.Cache
{
    public interface IEnvironmentConfigCache
    {
        /// <summary>
        /// Gets EnvironmentConfig values.
        /// </summary>
        /// <param name="routeUri">string</param>
        /// <param name="routeTemplate">string</param>
        /// <returns>string.</returns>
        string GetEnvironmentConfig(string routeUri, string routeTemplate);
    }
}
