﻿
namespace Znode.Engine.Api.Cache
{
    public interface IAffiliateTrackingCache
    {
        /// <summary>
        ///  Gets the tracking data on basis of dates provided in filters.
        /// </summary>
        /// <param name="routeUri">string routeUri</param>
        /// <param name="routeTemplate">string routeTemplate</param>
        ///<returns>Returns string.</returns>
        string GetAffiliateTrackingData(string routeUri, string routeTemplate);
    }
}
