﻿namespace Znode.Engine.Api.Cache
{
	public interface ICountryCache
	{
		string GetCountry(string countryCode, string routeUri, string routeTemplate);
		string GetCountries(string routeUri, string routeTemplate);
	}
}