﻿namespace Znode.Engine.Api.Cache
{
	public interface IPortalCache
	{
		string GetPortal(int portalId, string routeUri, string routeTemplate);
		string GetPortals(string routeUri, string routeTemplate);
		string GetPortalsByPortalIds(string portalIds, string routeUri, string routeTemplate);
        string GetPortalInformationByPortalId(int portalId, string routeUri, string routeTemplate);

        /// <summary>
        /// Gets fedex keys.
        /// </summary>
        /// <param name="routeUri">Route URI.</param>
        /// <param name="routeTemplate">Route Template.</param>
        /// <returns>String Data.</returns>
        string GetFedexKeys(string routeUri, string routeTemplate);

        /// <summary>
        /// Gets list of portals according to profile access to portal.
        /// </summary>
        /// <param name="routeUri">String Route URI.</param>
        /// <param name="routeTemplate">String Route Template.</param>
        /// <returns>String data containing the list of portals.</returns>
        string GetPortalListByProfileAccess(string routeUri, string routeTemplate);
	}
}
