﻿using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
    public class VendorAccountCache : BaseCache , IVendorAccountCache
    {
        private readonly IVendorAccountService _service;

        public VendorAccountCache(IVendorAccountService vendorAccountService)
		{
            _service = vendorAccountService;
		}

        public string GetVendorAccountList(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetVendorAccountList(Expands, Filters, Sorts, Page);
                if (list.VendorAccount.Count > 0)
                {
                    var response = new VendorAccountListResponse { VendorAccounts = list.VendorAccount };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetVendorAccountById(int accountId, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data , null))
            {
                var account = _service.GetVendorAccountById(accountId);
                if (!Equals(account , null))
                {
                    var response = new VendorAccountResponse { VendorAccount = account };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }
    }
}