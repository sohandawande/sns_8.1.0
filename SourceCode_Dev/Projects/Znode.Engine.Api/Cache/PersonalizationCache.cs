﻿using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
    public class PersonalizationCache : BaseCache, IPersonalizationCache
    {
        #region Private Variables
        private readonly IPersonalizationService _service; 
        #endregion

        #region Constructor
        public PersonalizationCache(IPersonalizationService personalizationService)
        {
            _service = personalizationService;
        } 
        #endregion

        #region Public methods
        public string GetFrequentlyBoughtProducts(string routeUri, string routeTemplate)
        {
            string data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                CrossSellListModel list = _service.GetFrequentlyBoughtProductList(Expands, Filters, Sorts, Page);
                if (list.CrossSellProducts.Count > 0)
                {
                    CrossSellListResponse response = new CrossSellListResponse { ProductCrossSells = list.CrossSellProducts };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetProductCrossSellbyProductId(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var productCrossSell = _service.GetCrossSellProductsByProductId(Filters, Sorts, Page);
                if (!Equals(productCrossSell, null))
                {
                    CrossSellListResponse response = new CrossSellListResponse { ProductCrossSells = productCrossSell.CrossSellProducts };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        } 
        #endregion
    }
}