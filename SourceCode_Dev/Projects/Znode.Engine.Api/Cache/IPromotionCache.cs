﻿namespace Znode.Engine.Api.Cache
{
	public interface IPromotionCache
	{
		string GetPromotion(int promotionId, string routeUri, string routeTemplate);
		string GetPromotions(string routeUri, string routeTemplate);

        /// <summary>
        /// Get the List of Products
        /// </summary>
        /// <param name="routeUri">string routeUri</param>
        /// <param name="routeTemplate">string routeTemplate</param>
        /// <returns>List of Products</returns>
        string GetProductList(string routeUri, string routeTemplate);
	}
}