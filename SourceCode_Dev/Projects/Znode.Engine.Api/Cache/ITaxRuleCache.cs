﻿namespace Znode.Engine.Api.Cache
{
    /// <summary>
    /// Tax Rule Cache Interface
    /// </summary>
	public interface ITaxRuleCache
	{
        /// <summary>
        /// Get Tax Rule on the basis of tax rule id
        /// </summary>
        /// <param name="taxRuleId">taxRuleId</param>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns>Returns tax rule</returns>
		string GetTaxRule(int taxRuleId, string routeUri, string routeTemplate);

        /// <summary>
        /// Get list of tax rules
        /// </summary>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns>Returns list of tax rules</returns>
		string GetTaxRules(string routeUri, string routeTemplate);
	}
}