﻿using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
    public class CustomerBasedPricingCache : BaseCache, ICustomerBasedPricingCache
    {
        #region Private Variables
        private readonly ICustomerBasedPricingService _service;
        #endregion

        #region Public Constructor
        public CustomerBasedPricingCache(ICustomerBasedPricingService customerBasedPricingService)
        {
            _service = customerBasedPricingService;
        }
        #endregion

        #region Public Methods
        public CustomerBasedPricingListModel GetCustomerBasedPricing(string routeUri, string routeTemplate)
        {
            CustomerBasedPricingListModel list = _service.GetCustomerBasedPricing(Expands, Filters, Sorts, Page);

            if (list.CustomerBasedPricing.Count > 0)
            {
                CustomerBasedPricingListResponse response = new CustomerBasedPricingListResponse { CustomerBasedPricingList = list.CustomerBasedPricing };
                response.MapPagingDataFromModel(list);
            }
            return list;

        }

        public CustomerBasedPricingListModel GetCustomerPricingProduct(string routeUri, string routeTemplate)
        {
            CustomerBasedPricingListModel list = _service.GetCustomerPricingProduct(Expands, Filters, Sorts, Page);

            if (list.CustomerBasedPricing.Count > 0)
            {
                CustomerBasedPricingListResponse response = new CustomerBasedPricingListResponse { CustomerBasedPricingList = list.CustomerBasedPricing };
                response.MapPagingDataFromModel(list);
            }
            return list;

        } 
        #endregion
    }
}