﻿using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
    public class ProductSearchSettingCache : BaseCache, IProductSearchSettingCache
    {
        #region Private Variables
        private readonly IProductSearchSettingService _service;
        #endregion

        #region Constructor
        public ProductSearchSettingCache(IProductSearchSettingService ProductSearchSettingService)
        {
            _service = ProductSearchSettingService;
        }
        #endregion

        #region Public Methods

        public string GetProductLevelSettings(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetProductLevelSettings(Expands, Filters, Sorts, Page);
                if (list.Products.Count > 0)
                {
                    var response = new ProductSearchSettingResponse { Products = list.Products };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }

        public string GetCategoryLevelSettings(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetCategoryLevelSettings(Expands, Filters, Sorts, Page);
                if (list.Categories.Count > 0)
                {
                    var response = new ProductSearchSettingResponse { Categories = list.Categories };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }

        public string GetFieldLevelSettings(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (data == null)
            {
                var list = _service.GetFieldLevelSettings(Expands, Filters, Sorts, Page);
                if (list.FieldLevelSettings.Count > 0)
                {
                    var response = new ProductSearchSettingResponse { Fields = list.FieldLevelSettings };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }
        #endregion
    }
}