﻿using System;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
    public class FacetGroupCache : BaseCache, IFacetGroupCache
    {
        #region Private Variables
        private readonly IFacetGroupService _service;
        #endregion

        #region Constructor
        public FacetGroupCache(IFacetGroupService facetGroupService)
		{
            _service = facetGroupService;
		}
        #endregion

        #region Public Methods

        /// <summary>
        /// Method Gets the Facet group based on facet group Id.
        /// </summary>
        /// <param name="facetGroupId"></param>
        /// <param name="routeUri"></param>
        /// <param name="routeTemplate"></param>
        /// <returns>Return Facet Group detail.</returns>
        public string GetFacetGroup(int facetGroupId, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data,null))
            {
                var facetFroup = _service.GetFacetGroup(facetGroupId, Expands);
                if (!Equals(facetFroup,null))
                {
                    var response = new FacetGroupResponse { FacetGroup = facetFroup };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }
        
        /// <summary>
        /// Method Get all facet groups.
        /// </summary>
        /// <param name="routeUri"></param>
        /// <param name="routeTemplate"></param>
        /// <returns>Returns facet group.</returns>
        public string GetFacetGroups(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetFacetGroups(Expands, Filters, Sorts, Page);
                if (list.FacetGroups.Count > 0)
                {
                    var response = new FacetGroupListResponse { FacetGroups = list.FacetGroups };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        /// <summary>
        /// Method get all facet control types.
        /// </summary>
        /// <param name="routeUri"></param>
        /// <param name="routeTemplate"></param>
        /// <returns>Return facet control types.</returns>
        public string GetFacetControlTypes(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (data == null)
            {
                var list = _service.GetFacetControlTypes(Expands, Filters, Sorts, Page);
                if (list.FacetControlTypes.Count > 0)
                {
                    var response = new FacetControlTypeListResponse { FacetControlTypes = list.FacetControlTypes };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        /// <summary>
        /// Method Gets the Facet details based on facet Id.
        /// </summary>
        /// <param name="facetId"></param>
        /// <param name="routeUri"></param>
        /// <param name="routeTemplate"></param>
        /// <returns>Return Facet detail.</returns>
        public string GetFacet(int facetId, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var facet = _service.GetFacet(facetId, Expands);
                if (!Equals(facet, null))
                {
                    var response = new FacetResponse { Facet = facet };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }
        /// <summary>
        /// Method Get the Facet List.
        /// </summary>
        /// <param name="routeUri">string routeUri</param>
        /// <param name="routeTemplate">string routeTemplate</param>
        /// <returns></returns>
        public string GetFacetList(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetFacetList(Expands, Filters, Sorts, Page);
                if (list.Facets.Count > 0)
                {
                    var response = new FacetListResponse { Facets = list.Facets };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        #endregion
    }
}