﻿
namespace Znode.Engine.Api.Cache
{
    /// <summary>
    /// Interface for ZipCode Cache
    /// </summary>
    public interface IZipCodeCache
    {
        /// <summary>
        /// Get Zip Codes
        /// </summary>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns>Returns zip code</returns>
        string GetZipCodes(string routeUri, string routeTemplate);
    }
}
