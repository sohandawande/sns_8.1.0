﻿using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
	public class CatalogCache : BaseCache, ICatalogCache
	{
		private readonly ICatalogService _service;

		public CatalogCache(ICatalogService catalogService)
		{
			_service = catalogService;
		}

		public string GetCatalog(int catalogId, string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
            if (Equals(data, null))
			{
				var catalog = _service.GetCatalog(catalogId);
				if (!Equals(catalog,null))
				{
					var response = new CatalogResponse { Catalog = catalog };
					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}

		public string GetCatalogs(string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
            if (Equals(data, null))
			{
				var list = _service.GetCatalogs(Filters, Sorts, Page);
				if (list.Catalogs.Count > 0)
				{
					var response = new CatalogListResponse { Catalogs = list.Catalogs };
					response.MapPagingDataFromModel(list);

					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}
			return data;
		}

	    public string GetCatalogsByCatalogIds(string catalogIds, string routeUri, string routeTemplate)
	    {
            var data = GetFromCache(routeUri);
            if (Equals(data,null))
            {
                var list = _service.GetCatalogsByCatalogIds(catalogIds, Expands, Sorts);
                if (list.Catalogs.Count > 0)
                {
					var response = new CatalogListResponse { Catalogs = list.Catalogs };
					response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
	    }

        public string GetCatalogsByPortalId(int portalId, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data,null))
            {
                var list = _service.GetCatalogListByPortalId(portalId);
                if (!Equals(list,null) && !Equals(list.Catalogs,null))
                {
                    var response = new CatalogListResponse { Catalogs = list.Catalogs };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }
	}
}