﻿using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
	public class SkuCache : BaseCache, ISkuCache
	{
		private readonly ISkuService _service;

		public SkuCache(ISkuService skuService)
		{
			_service = skuService;
		}

		public string GetSku(int skuId, string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var sku = _service.GetSku(skuId, Expands);
				if (sku != null)
				{
					var response = new SkuResponse { Sku = sku };
					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}

		public string GetSkus(string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var list = _service.GetSkus(Expands, Filters, Sorts, Page);
				if (list.Skus.Count > 0)
				{
					var response = new SkuListResponse { Skus = list.Skus };
					response.MapPagingDataFromModel(list);

					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}

        public string GetSKUListByProductId(int productId,string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data,null))
            {
                var list = _service.GetSKUListByProductId(productId);
                if (list.Skus.Count > 0)
                {
                    var response = new SkuListResponse { Skus = list.Skus };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }
	}
}