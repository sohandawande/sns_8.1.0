﻿namespace Znode.Engine.Api.Cache
{
	public interface IAttributeTypeCache
	{
        /// <summary>
        /// To Get Attribute Type details based on Attribute Type Id.
        /// </summary>
        /// <param name="attributeTypeId">Id of the Attribute Type</param>
        /// <param name="routeUri"></param>
        /// <param name="routeTemplate"></param>
        /// <returns>Return Attribute Type Details.</returns>
		string GetAttributeType(int attributeTypeId, string routeUri, string routeTemplate);

        /// <summary>
        /// To Get Attribute Types details.
        /// </summary>
        /// <param name="routeUri"></param>
        /// <param name="routeTemplate"></param>
        /// <returns>Return Attribute Types Details.</returns>
		string GetAttributeTypes(string routeUri, string routeTemplate);

        /// <summary>
        /// To Get Attribute Type details based on Catalog Id.
        /// </summary>
        /// <param name="catalogId">Id of the Catalog<</param>
        /// <param name="routeUri"></param>
        /// <param name="routeTemplate"></param>
        /// <returns>Return Attribute Types Details.</returns>
        string GetAttributeTypesByCatalogId(int catalogId, string routeUri, string routeTemplate);
	}
}