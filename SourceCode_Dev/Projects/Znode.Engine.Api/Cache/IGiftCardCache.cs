﻿namespace Znode.Engine.Api.Cache
{
	public interface IGiftCardCache
	{
		string GetGiftCard(int giftCardId, string routeUri, string routeTemplate);
		string GetGiftCards(string routeUri, string routeTemplate);
	}
}