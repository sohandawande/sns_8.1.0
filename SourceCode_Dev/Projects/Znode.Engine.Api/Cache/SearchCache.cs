﻿using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
	public class SearchCache : BaseCache, ISearchCache
	{
		private readonly ISearchService _service;

		public SearchCache(ISearchService searchService)
		{
			_service = searchService;
		}

		public KeywordSearchResponse GetKeywordSearch(KeywordSearchModel model, string routeUri, string routeTemplate)
		{
			var data = _service.GetKeywordSearch(model, Expands, Sorts, Page);
			if (data != null && data.Products.Count > 0)
			{
				return new KeywordSearchResponse { Search = data };
			}

			return null;
		}

		public SuggestedSearchListResponse GetSearchSuggestions(SuggestedSearchModel model, string routeUri, string routeTemplate)
		{
			var data = _service.GetSearchSuggestions(model);
			if (data != null && data.SuggestedSearchResults.Count > 0)
			{
				return new SuggestedSearchListResponse { SuggestedSearchResults = data.SuggestedSearchResults };
			}

			return null;
		}

		public void ReloadIndex()
		{
			_service.ReloadIndex();
		}

        public string GetSeoUrlDetail(string seoUrl, string routeUri, string routeTemplate)
        {
            string data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                SEOUrlModel urlModel = _service.GetSeoUrlDetail(seoUrl);
                if (!Equals(urlModel, null))
                {
                    SEOUrlResponse response = new SEOUrlResponse { SeoUrl = urlModel };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }
	}
}