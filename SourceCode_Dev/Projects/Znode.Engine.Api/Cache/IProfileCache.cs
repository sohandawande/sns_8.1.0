﻿
using Znode.Engine.Api.Models;
namespace Znode.Engine.Api.Cache
{
    public interface IProfileCache
    {
        /// <summary>
        /// Method returns Profile details.
        /// </summary>
        /// <param name="profileId">Porfile ID</param>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns>Return profile details.</returns>
        string GetProfile(int profileId, string routeUri, string routeTemplate);

        /// <summary>
        /// Method returns Profile List.
        /// </summary>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns>Return the Profile list.</returns>
        string GetProfiles(string routeUri, string routeTemplate);

        /// <summary>
        /// Method returns Profile List by portalId.
        /// </summary>
          /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns>Return the Profile list by portalId.</returns>
        string GetProfileListByProfileId(int portalId, string routeUri, string routeTemplate);

        /// <summary>
        /// Get Customer Profile Account.
        /// </summary>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns>Customer Profile</returns>
        string GetCustomerProfile(string routeUri, string routeTemplate);

        /// <summary>
        /// Get Profiles that not associated with selected account Id
        /// </summary>
        /// <param name="accountId">account Id</param>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns>Account not associated with profiles List</returns>
        string GetAccountNotAssociatedProfile(string routeUri, string routeTemplate);

        /// <summary>
        /// Get the list of profileon the basis of portalId.
        /// </summary>
        /// <param name="portalId">Portal Id</param>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns>profiles List</returns>
        string GetProfileListByPortalId(int portalId, string routeUri, string routeTemplate);

        /// <summary>
        /// Get the list of available profile.
        /// </summary>
        /// <param name="model">Specifies the criteria on which the profiles selected.</param>
        /// <param name="routeUri">route uri.</param>
        /// <param name="routeTemplate">route template.</param>
        /// <returns>List of available profiles.</returns>
        string GetAvailableProfileListBySkuIdCategoryId(AssociatedSkuCategoryProfileModel model, string routeUri, string routeTemplate);

        /// <summary>
        /// Get ZNode Profile Details
        /// </summary>
        /// <param name="routeUri">string routeUri</param>
        /// <param name="routeTemplate">string routeTemplate</param>
        /// <returns>Returns ZnodeProfile</returns>
        string GetZNodeProfile(string routeUri, string routeTemplate);

        /// <summary>
        /// Get Profiles List by userName
        /// </summary>
        /// <param name="userName">string userName</param>
        /// <param name="routeUri">string routeUri</param>
        /// <param name="routeTemplate">string routeTemplate</param>
        /// <returns>Return list of profiles</returns>
        string GetProfilesAssociatedWithUsers(string userName, string routeUri, string routeTemplate);
    }
}
