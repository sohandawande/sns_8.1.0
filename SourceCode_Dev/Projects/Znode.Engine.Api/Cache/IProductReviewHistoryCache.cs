﻿
namespace Znode.Engine.Api.Cache
{
    public interface IProductReviewHistoryCache
    {
        /// <summary>
        /// This method will fetch all the product review history list.
        /// </summary>
        /// <param name="routeUri">string Route URI</param>
        /// <param name="routeTemplate">string Route Template</param>
        /// <returns>Retunrs the product review history</returns>
        string GetProductReviewHistory(string routeUri, string routeTemplate);


        /// <summary>
        /// This method will fetch the product review history
        /// </summary>
        /// <param name="productReviewHistoryID">int productReviewHistoryID</param>
        /// <param name="routeUri">string Route URI</param>
        /// <param name="routeTemplate">string Route Template</param>
        /// <returns>Retunrs the product review history</returns>
        string GetProductReviewHistoryById(int productReviewHistoryID, string routeUri, string routeTemplate);
    }
}
