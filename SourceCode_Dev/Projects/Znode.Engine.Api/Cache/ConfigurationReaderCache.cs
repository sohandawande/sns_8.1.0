﻿using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
    public class ConfigurationReaderCache : BaseCache,IConfigurationReaderCache
    {
        private readonly IConfigurationReaderService _service;

        public ConfigurationReaderCache(IConfigurationReaderService configurationReaderService)
		{
            _service = configurationReaderService;
		}

        public string GetFilterConfigurationXML(string itemName, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data,null))
            {
                var filterxml = _service.GetFilterConfigurationXML(itemName);
                if (!Equals(filterxml, null))
                {
                    var response = new ConfigurationReaderResponce { FilterXML = filterxml };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }
    }
}