﻿using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
	public class HighlightCache : BaseCache, IHighlightCache
	{
		private readonly IHighlightService _service;

		public HighlightCache(IHighlightService highlightService)
		{
			_service = highlightService;
		}

		public string GetHighlight(int highlightId, string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var highlight = _service.GetHighlight(highlightId, Expands);
				if (highlight != null)
				{
					var response = new HighlightResponse { Highlight = highlight };
					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}

		public string GetHighlights(string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var list = _service.GetHighlights(Expands, Filters, Sorts, Page);
				if (list.Highlights.Count > 0)
				{
					var response = new HighlightListResponse { Highlights = list.Highlights };
					response.MapPagingDataFromModel(list);

					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}
	}
}