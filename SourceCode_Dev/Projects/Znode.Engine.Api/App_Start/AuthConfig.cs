﻿using System;
using System.Collections.Generic;
using Microsoft.Web.WebPages.OAuth;
using DotNetOpenAuth.GoogleOAuth2;
using System.Configuration;

namespace Znode.Engine.Api.App_Start
{
    public static class AuthConfig
    {
        public static void RegisterAuth()
        {
            // To let users of this site log in using their accounts from other sites such as Microsoft, Facebook, and Twitter,
            // you must update this site. For more information visit http://go.microsoft.com/fwlink/?LinkID=252166

            //OAuthWebSecurity.RegisterMicrosoftClient(
            //    clientId: "",
            //    clientSecret: "");

            //OAuthWebSecurity.RegisterTwitterClient(
            //    consumerKey: "",
            //    consumerSecret: "");
            
            OAuthWebSecurity.RegisterFacebookClient(
                appId: Convert.ToString(ConfigurationManager.AppSettings["FacebookAppId"]),
                appSecret: Convert.ToString(ConfigurationManager.AppSettings["FacebookAppSecret"]));

            var client = new GoogleOAuth2Client(Convert.ToString(ConfigurationManager.AppSettings["GoogleClientId"]), Convert.ToString(ConfigurationManager.AppSettings["GoogleClientSecret"]));
            var extraData = new Dictionary<string, object>();
            OAuthWebSecurity.RegisterClient(client, "Google", extraData);                
        }
    }
}
