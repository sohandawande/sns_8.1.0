﻿using System.Web.Http.Filters;
using System.Web.Mvc;
using Znode.Engine.Api.Attributes;

namespace Znode.Engine.Api.App_Start
{
	public class FilterConfig
	{
		public static void RegisterGlobalFilters(GlobalFilterCollection filters)
		{
			filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterHttpFilters(HttpFilterCollection filters)
        {
            filters.Add(new OnExceptionFilterAttribute());
        }
	}
}