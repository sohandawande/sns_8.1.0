﻿using System.Web.Http;
using System.Web.Routing;

namespace Znode.Engine.Api.App_Start
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Uncomment the following line of code to enable query support for actions with an IQueryable or IQueryable<T> return type.
            // To avoid processing unexpected or malicious queries, use the validation settings on QueryableAttribute to validate incoming queries.
            // For more information, visit http://go.microsoft.com/fwlink/?LinkId=279712.
            //config.EnableQuerySupport();

            // To disable tracing in your application, please comment out or remove the following line of code
            // For more information, refer to: http://www.asp.net/web-api
            //config.EnableSystemDiagnosticsTracing();

            // Remove XML formatter
            config.Formatters.Remove(config.Formatters.XmlFormatter);

            //config.Routes.MapHttpRoute(
            //	name: "DefaultApi",
            //	routeTemplate: "{controller}/{id}",
            //	defaults: new { id = RouteParameter.Optional }
            //);

            // Account routes
            config.Routes.MapHttpRoute("account-getbyusername", "accounts/getbyusername", new { controller = "accounts", action = "getbyusername" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("account-get", "accounts/{accountId}", new { controller = "accounts", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET"), accountId = @"^\d+$" });
            config.Routes.MapHttpRoute("account-getbyuserid", "accounts/{userId}", new { controller = "accounts", action = "getbyuserid" }, new { httpMethod = new HttpMethodConstraint("GET"), userId = @"^[A-Za-z0-9]{8}-[A-Za-z0-9]{4}-[A-Za-z0-9]{4}-[A-Za-z0-9]{4}-[A-Za-z0-9]{12}$" });
            config.Routes.MapHttpRoute("account-getaccountdetailsbyrolename", "accounts/getaccountdetailsbyrolename/{roleName}", new { controller = "accounts", action = "getaccountdetailsbyrolename" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("account-list", "accounts", new { controller = "accounts", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("account-create", "accounts", new { controller = "accounts", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("account-update", "accounts/{accountId}", new { controller = "accounts", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("account-delete", "accounts/{accountId}", new { controller = "accounts", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });
            config.Routes.MapHttpRoute("account-enabledisable", "accounts/enabledisable/{accountId}", new { controller = "accounts", action = "enabledisable" }, new { httpMethod = new HttpMethodConstraint("POST"), accountId = @"^\d+$" });
            config.Routes.MapHttpRoute("account-login", "accounts/login", new { controller = "accounts", action = "login" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("account-resetpassword", "accounts/resetpassword", new { controller = "accounts", action = "resetpassword" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("account-changepassword", "accounts/changepassword", new { controller = "accounts", action = "changepassword" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("account-checkresetpasswordlinkstatus", "accounts/checkresetpasswordlinkstatus", new { controller = "accounts", action = "checkresetpasswordlinkstatus" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("account-checkuserrole", "accounts/checkuserrole", new { controller = "accounts", action = "checkuserrole" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("account-isaddressvalid", "accounts/isaddressvalid", new { controller = "accounts", action = "isaddressvalid" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("account-createadminaccount", "accounts/createadminaccount", new { controller = "accounts", action = "createadminaccount" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("account-resetadmindetails", "accounts/resetadmindetails", new { controller = "accounts", action = "resetadmindetails" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("account-updatecustomeraccount", "accounts/updatecustomeraccount/{accountId}", new { controller = "accounts", action = "updatecustomeraccount" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("account-createcustomeraccount", "accounts/createcustomeraccount", new { controller = "accounts", action = "createcustomeraccount" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("account-sendemail", "accounts/sendemail", new { controller = "accounts", action = "sendemail" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("getaccountpaymentlist-list", "getaccountpaymentlist/{accountId}", new { controller = "accounts", action = "getaccountpaymentlist" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("createaccountpayment-create", "createaccountpayment", new { controller = "accounts", action = "createaccountpayment" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("account-getrolepermissionbyusername", "accounts/getrolepermissionbyusername", new { controller = "accounts", action = "getrolepermissionbyusername" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("account-getrolemenulistbyusername", "accounts/getrolemenulistbyusername", new { controller = "accounts", action = "getrolemenulistbyusername" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("account-isroleexistforprofile", "isroleexistforprofile/{porfileId}/{roleName}", new { controller = "accounts", action = "isroleexistforprofile" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("account-signupfornewsletter", "accounts/signupfornewsletter", new { controller = "accounts", action = "signupfornewsletter" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("account-socialuserlogin", "accounts/socialuserlogin", new { controller = "accounts", action = "socialuserlogin" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("account-socialusercreateorupdateaccount", "accounts/socialusercreateorupdateaccount", new { controller = "accounts", action = "socialusercreateorupdateaccount" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("account-getregisteredsocialclientdetailslist", "accounts/getregisteredsocialclientdetailslist", new { controller = "accounts", action = "getregisteredsocialclientdetailslist" }, new { httpMethod = new HttpMethodConstraint("GET") });

            //PRFT Custom Code 
            config.Routes.MapHttpRoute("account-getsalesrepinfobyrolename", "accounts/getsalesrepinfobyrolename/{roleName}", new { controller = "accounts", action = "getsalesrepinfobyrolename" }, new { httpMethod = new HttpMethodConstraint("GET") });

            //Account Profiles
            config.Routes.MapHttpRoute("accountprofile-accountassociatedprofiles", "accountprofile/accountassociatedprofiles/{accountId}/{profileIds}", new { controller = "accountprofile", action = "accountassociatedprofiles" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("accountprofile-delete", "accountprofile/{accountProfileId}", new { controller = "accountprofile", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            // Address routes
            config.Routes.MapHttpRoute("address-get", "addresses/{addressId}", new { controller = "addresses", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("address-list", "addresses", new { controller = "addresses", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("address-create", "addresses", new { controller = "addresses", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("address-update", "addresses/{addressId}", new { controller = "addresses", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("address-updatedefaultaddress", "updatedefaultaddress/{addressId}", new { controller = "addresses", action = "updateaddress" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("address-delete", "addresses/{addressId}", new { controller = "addresses", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });
            config.Routes.MapHttpRoute("address-updatecustomeraddress", "updatecustomeraddress/{accountId}", new { controller = "addresses", action = "updatecustomeraddress" }, new { httpMethod = new HttpMethodConstraint("POST") });

            //Affiliate Tracking
            config.Routes.MapHttpRoute("affiliate-get", "affiliatetracking/", new { controller = "affiliatetracking", action = "gettrackingdata" }, new { httpMethod = new HttpMethodConstraint("GET") });

            // Attribute type routes
            config.Routes.MapHttpRoute("attributetype-get", "attributetypes/{attributeTypeId}", new { controller = "attributetypes", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("attributetype-list", "attributetypes", new { controller = "attributetypes", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("attributetype-create", "attributetypes", new { controller = "attributetypes", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("attributetype-update", "attributetypes/{attributeTypeId}", new { controller = "attributetypes", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("attributetype-delete", "attributetypes/{attributeTypeId}", new { controller = "attributetypes", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });
            config.Routes.MapHttpRoute("attributetype-getbycatalogid", "attributetypesbycatalogid/{catalogId}", new { controller = "attributetypes", action = "getbycatalogid" }, new { httpMethod = new HttpMethodConstraint("GET") });

            //Application setting
            config.Routes.MapHttpRoute("applicationsetting-list", "applicationsetting", new { controller = "applicationsetting", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("applicationsetting-columnlist", "applicationsetting/{entityType}/{entityName}", new { controller = "applicationsetting", action = "GetColumnList" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("applicationsetting-create", "applicationsetting", new { controller = "applicationsetting", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });

            // Audit routes
            config.Routes.MapHttpRoute("audit-get", "audits/{auditId}", new { controller = "audits", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("audit-list", "audits", new { controller = "audits", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("audit-create", "audits", new { controller = "audits", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("audit-update", "audits/{auditId}", new { controller = "audits", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("audit-delete", "audits/{auditId}", new { controller = "audits", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            // AddOn routes
            config.Routes.MapHttpRoute("addon-get", "addons/{addonid}", new { controller = "addon", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("addon-list", "addons", new { controller = "addon", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("addon-create", "addons", new { controller = "addon", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("addon-update", "addons/{addonid}", new { controller = "addon", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("addon-delete", "addons/{addonid}", new { controller = "addon", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            // AddOnValue routes
            config.Routes.MapHttpRoute("addonvalue-get", "addonvalues/{addonvalueid}", new { controller = "addonvalue", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("addonvalue-list", "addonValues", new { controller = "addonvalue", action = "getaddonvaluebyaddonid" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("addonvalue-create", "addonvalues", new { controller = "addonvalue", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("addonvalue-update", "addonvalues/{addonvalueid}", new { controller = "addonvalue", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("addonvalue-delete", "addonvalues/{addonvalueid}", new { controller = "addonvalue", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            // Case request routes
            config.Routes.MapHttpRoute("caserequest-get", "caserequests/{caseRequestId}", new { controller = "caserequests", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("caserequest-list", "caserequests", new { controller = "caserequests", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("caserequest-create", "caserequests", new { controller = "caserequests", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("caserequest-update", "caserequests/{caseRequestId}", new { controller = "caserequests", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("caserequest-delete", "caserequests/{caseRequestId}", new { controller = "caserequests", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });
            config.Routes.MapHttpRoute("getcasestatusrequests-casestatuslist", "getcasestatusrequests", new { controller = "caserequests", action = "casestatuslist" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("getcaseprioritylist-caseprioritylist", "getcaseprioritylist", new { controller = "caserequests", action = "caseprioritylist" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("getcasenotelist-casenotelist", "getcasenotelist", new { controller = "caserequests", action = "casenotelist" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("caserequest-createcasenote", "caserequests/createcasenote", new { controller = "caserequests", action = "createcasenote" }, new { httpMethod = new HttpMethodConstraint("POST") });

            
            // Catalog routes
            config.Routes.MapHttpRoute("catalog-get", "catalogs/{catalogId}", new { controller = "catalogs", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET"), catalogId = @"^\d+$" });
            config.Routes.MapHttpRoute("catalog-list", "catalogs", new { controller = "catalogs", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("catalog-listbycatalogids", "catalogs/{catalogIds}", new { controller = "catalogs", action = "listbycatalogids" }, new { httpMethod = new HttpMethodConstraint("GET"), catalogIds = @"^(\d+,?)+$" });
            config.Routes.MapHttpRoute("catalog-create", "catalogs", new { controller = "catalogs", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("catalog-update", "catalogs/{catalogId}", new { controller = "catalogs", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("catalog-delete", "catalogs/{catalogId}", new { controller = "catalogs", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });
            config.Routes.MapHttpRoute("catalog-copycatalog", "copycatalog", new { controller = "catalogs", action = "copycatalog" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("catalog-deletecatalog", "catalogs/{catalogId}/{preserveCategories}", new { controller = "catalogs", action = "deletecatalog" }, new { httpMethod = new HttpMethodConstraint("DELETE") });
            config.Routes.MapHttpRoute("getcatalogsbyportalid-list", "getcatalogsbyportalid/{portalId}", new { controller = "catalogs", action = "getcatalogsbyportalid" }, new { httpMethod = new HttpMethodConstraint("GET") });

            // Category routes 
            config.Routes.MapHttpRoute("category-get", "categories/{categoryId}", new { controller = "categories", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET"), categoryId = @"^\d+$" });
            config.Routes.MapHttpRoute("category-list", "categories", new { controller = "categories", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("catalog-listbycategoryids", "categories/{categoryIds}", new { controller = "categories", action = "listbycategoryids" }, new { httpMethod = new HttpMethodConstraint("GET"), categoryIds = @"^(\d+,?)+$" });
            config.Routes.MapHttpRoute("category-listbycatalog", "categories/catalog/{catalogId}", new { controller = "categories", action = "listbycatalog" }, new { httpMethod = new HttpMethodConstraint("GET"), catalogId = @"^\d+$" });
            config.Routes.MapHttpRoute("category-listbycatalogids", "categories/catalog/{catalogIds}", new { controller = "categories", action = "listbycatalogids" }, new { httpMethod = new HttpMethodConstraint("GET"), catalogIds = @"^(\d+,?)+$" });
            config.Routes.MapHttpRoute("category-create", "categories", new { controller = "categories", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("category-update", "categories/{categoryId}", new { controller = "categories", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("category-updatecategoryseodetails", "categories/updatecategoryseodetails/{categoryId}", new { controller = "categories", action = "updatecategoryseodetails" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("category-getcategoriesbycatalogid", "categories/getcategoriesbycatalogid/{catalogId}", new { controller = "categories", action = "getcategoriesbycatalogid" }, new { httpMethod = new HttpMethodConstraint("GET"), catalogId = @"^\d+$" });
            config.Routes.MapHttpRoute("category-getcategorytree", "categories/getcategorytree", new { controller = "categories", action = "getcategorytree" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("category-getallcategories", "categories/getallcategories", new { controller = "categories", action = "getallcategories" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("category-checkassociatecategory", "categories/checkassociatecategory/{categoryId}", new { controller = "categories", action = "checkassociatecategory" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("category-delete", "categories/{categoryId}", new { controller = "categories", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });
            config.Routes.MapHttpRoute("category-categoryassociatedproducts", "categories/categoryassociatedproducts/{categoryId}/{productIds}", new { controller = "categories", action = "categoryassociatedproducts" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("categoryprofile-getcategoryprofilebyid", "categoryprofile/getcategoryprofilesbycategoryprofileid/{categoryProfileId}", new { controller = "categoryprofile", action = "getcategoryprofilesbycategoryprofileid" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("categoryprofile-getcategoryprofilebycategoryid", "categoryprofile/getcategoryprofilesbycategoryid/{categoryId}", new { controller = "categoryprofile", action = "getcategoryprofilesbycategoryid" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("categoryprofile-create", "categoryprofile", new { controller = "categoryprofile", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("categoryprofile-update", "categoryprofile/{categoryprofileId}", new { controller = "categoryprofile", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("categoryprofile-delete", "categoryprofile/{categoryprofileId}", new { controller = "categoryprofile", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });
            config.Routes.MapHttpRoute("getcategoryassociatedproducts-list", "getcategoryassociatedproducts", new { controller = "categories", action = "getcategoryassociatedproducts" }, new { httpMethod = new HttpMethodConstraint("GET") });

            // Category node routes
            config.Routes.MapHttpRoute("categorynode-get", "categorynodes/{categoryNodeId}", new { controller = "categorynodes", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET"), categoryNodeId = @"^\d+$" });
            config.Routes.MapHttpRoute("categorynode-list", "categorynodes", new { controller = "categorynodes", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("categorynode-create", "categorynodes", new { controller = "categorynodes", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("categorynode-update", "categorynodes/{categoryNodeId}", new { controller = "categorynodes", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("categorynode-delete", "categorynodes/{categoryNodeId}", new { controller = "categorynodes", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });
            config.Routes.MapHttpRoute("categorynode-deleteassociatedcategory", "categorynodes/deleteassociatedcategorynode/{categoryNodeId}", new { controller = "categorynodes", action = "deleteassociatedcategorynode" }, new { httpMethod = new HttpMethodConstraint("DELETE") });
            config.Routes.MapHttpRoute("parentcategorynode-list", "getparentcategorylist/{catalogId}", new { controller = "categorynodes", action = "getparentcategorylist" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("categorynode-getbreadcrumb", "breadcrumb", new { controller = "categorynodes", action = "getbreadcrumb" }, new { httpMethod = new HttpMethodConstraint("POST") });


            //Content Page Routes
            config.Routes.MapHttpRoute("contentpage-list", "contentpages", new { controller = "contentpage", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("contentpage-get", "contentpages/{contentPageId}", new { controller = "contentpage", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("contentpage-create", "contentpages", new { controller = "contentpage", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("contentpage-copy", "copycontentpage", new { controller = "contentpage", action = "copycontentpage" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("contentpage-addpage", "addpage", new { controller = "contentpage", action = "addpage" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("contentpage-delete", "contentpages/{contentPageId}", new { controller = "contentpage", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });
            config.Routes.MapHttpRoute("contentpage-update", "contentpages/{contentPageId}", new { controller = "contentpage", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("contentpage-contentpagebyname", "contentpage/getcontentpagebyname/{contentPageName}/{extension}", new { controller = "contentpage", action = "getcontentpagebyname" }, new { httpMethod = new HttpMethodConstraint("GET") });

            //Content Page Revisions Routes
            config.Routes.MapHttpRoute("contentpagerevision-list", "contentpagerevisions", new { controller = "contentpagerevision", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("contentpagerevision-revertrevision", "contentpagerevisions/{revisionId}", new { controller = "contentpagerevision", action = "RevertRevision" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("contentpagerevision-getcontentpagerevisionsbyid", "contentpagerevisions/{contentPageId}", new { controller = "contentpagerevision", action = "getcontentpagerevisionsbyid" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("contentpagerevision-getcontentpagerevisionbyid", "contentpagerevisions/getcontentpagerevisionbyid/{contentPageId}", new { controller = "contentpagerevision", action = "getcontentpagerevisionbyid" }, new { httpMethod = new HttpMethodConstraint("GET") });


            // Country routes
            config.Routes.MapHttpRoute("country-get", "countries/{countryCode}", new { controller = "countries", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("country-list", "countries", new { controller = "countries", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("country-getactivecountrybyportalid", "countries/getactivecountrybyportalid/{portalId}/{billingShippingFlag}", new { controller = "countries", action = "getactivecountrybyportalid" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("country-getcountrybyportalid", "countries/getcountrybyportalid/{portalId}", new { controller = "countries", action = "getcountrybyportalid" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("country-create", "countries", new { controller = "countries", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("country-update", "countries/{countryCode}", new { controller = "countries", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("country-delete", "countries/{countryCode}", new { controller = "countries", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });
            config.Routes.MapHttpRoute("country-GetShippingActiveCountryByPortalId", "countries/GetShippingActiveCountryByPortalId/{portalId}", new { controller = "countries", action = "GetShippingActiveCountryByPortalId" }, new { httpMethod = new HttpMethodConstraint("GET") });


            //Currency Type Routes
            config.Routes.MapHttpRoute("currencytype-list", "currencytypes", new { controller = "currencytype", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("currencytype-get", "currencytype/{currencyTypeId}", new { controller = "currencytype", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("currencytype-update", "currencytype/{currencyTypeId}", new { controller = "currencytype", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });

            //Customers
            config.Routes.MapHttpRoute("customer-list", "customer", new { controller = "customer", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("customerbasedpricingproduct-list", "customers/getcustomerpricingproduct/{accountId}", new { controller = "customerbasedpricing", action = "getcustomerpricingproductlist" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("customer-getcustomeraffiliate", "customer/{accountId}", new { controller = "customer", action = "getcustomeraffiliate" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("getreferralcommissiontypelist-list", "getreferralcommissiontypelist", new { controller = "customer", action = "getreferralcommissiontypelist" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("getreferralcommissionlist-list", "getreferralcommissionlist/{accountId}", new { controller = "customer", action = "getreferralcommissionlist" }, new { httpMethod = new HttpMethodConstraint("GET") });


            //Customer Based Pricing 
            config.Routes.MapHttpRoute("customerbasedpricing-list", "products/getCustomerpricing/{productId}", new { controller = "customerbasedpricing", action = "getcustomerbasedpricinglist" }, new { httpMethod = new HttpMethodConstraint("GET") });

            //Clear API Cache
            config.Routes.MapHttpRoute("clearapicache", "clearcache/clearapicache", new { controller = "clearcache", action = "clearapicache" }, new { httpMethod = new HttpMethodConstraint("GET") });

            // CSS routes
            config.Routes.MapHttpRoute("css-get", "getcss/{cssId}", new { controller = "css", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("css-list", "csslist", new { controller = "css", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("css-create", "createcss", new { controller = "css", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("css-update", "updatecss/{cssId}", new { controller = "css", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("css-delete", "deletecss/{cssId}", new { controller = "css", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            //Configuration Reader routes
            config.Routes.MapHttpRoute("configuration-reader", "configurationreader/{itemName}", new { controller = "configurationreader", action = "getfilterconfigurationxml" }, new { httpMethod = new HttpMethodConstraint("GET") });

            // Dashboard routes
            config.Routes.MapHttpRoute("dashboard-getdashboarditemsbyportalid", "dashboard/getdashboarditemsbyportalid/{portalId}", new { controller = "dashboard", action = "getdashboarditemsbyportalid" }, new { httpMethod = new HttpMethodConstraint("GET") });

            // Diagnostics routes
            config.Routes.MapHttpRoute("diagnostics-checkemailaccount", "diagnostics/checkemailaccount", new { controller = "diagnostics", action = "checkemailaccount" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("diagnostics-getproductversiondetails", "diagnostics/getproductversiondetails", new { controller = "diagnostics", action = "getproductversiondetails" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("diagnostics-emaildiagnostics", "diagnostics/emaildiagnostics", new { controller = "diagnostics", action = "emaildiagnostics" }, new { httpMethod = new HttpMethodConstraint("POST") });

            // Domain routes
            config.Routes.MapHttpRoute("domain-get", "domains/{domainId}", new { controller = "domains", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("domain-list", "domains", new { controller = "domains", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("domain-create", "domains", new { controller = "domains", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("domain-update", "domains/{domainId}", new { controller = "domains", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("domain-delete", "domains/{domainId}", new { controller = "domains", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            //Email Templates
            config.Routes.MapHttpRoute("emailtemplate-list", "emailtemplates", new { controller = "emailtemplates", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("emailtemplate-create", "emailtemplates", new { controller = "emailtemplates", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("emailtemplate-get", "emailtemplates/{templateName}/{extension}", new { controller = "emailtemplates", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("emailtemplate-update", "updateemailtemplates", new { controller = "emailtemplates", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("emailtemplate-delete", "emailtemplate/{templateName}/{extension}", new { controller = "emailtemplates", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });
            config.Routes.MapHttpRoute("emailtemplate-getkeys", "deletedemailtemplates", new { controller = "emailtemplates", action = "getkeys" }, new { httpMethod = new HttpMethodConstraint("GET") });
            //GetKeys
            //EnvironmentConfig Routes
            config.Routes.MapHttpRoute("environmentconfig-get", "environmentconfig", new { controller = "environmentconfig", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });

            //Facet routes
            config.Routes.MapHttpRoute("facet-list", "facet", new { controller = "facetgroups", action = "facetlist" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("facet-get", "facet/{facetId}", new { controller = "facetgroups", action = "getfacet" }, new { httpMethod = new HttpMethodConstraint("GET"), facetId = @"^\d+$" });
            config.Routes.MapHttpRoute("facet-create", "facet", new { controller = "facetgroups", action = "createfacet" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("facet-update", "facet/{facetid}", new { controller = "facetgroups", action = "updatefacet" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("facet-delete", "facet/{facetid}", new { controller = "facetgroups", action = "deletefacet" }, new { httpMethod = new HttpMethodConstraint("DELETE") });


            // Facet Groups routes
            config.Routes.MapHttpRoute("facetgroup-get", "facetgroups/{facetGroupId}", new { controller = "facetgroups", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET"), facetGroupId = @"^\d+$" });
            config.Routes.MapHttpRoute("facetgroup-list", "facetgroups", new { controller = "facetgroups", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("facetgroup-create", "facetgroups", new { controller = "facetgroups", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("facetgroup-update", "facetgroups/{facetgroupid}", new { controller = "facetgroups", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("facetgroup-delete", "facetgroups/{facetgroupid}", new { controller = "facetgroups", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });
            config.Routes.MapHttpRoute("facetcontroltypes-list", "facetcontroltypes", new { controller = "facetgroups", action = "getcontroltypelist" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("facetgroupcategory-create", "facetgroupcategory", new { controller = "facetgroups", action = "insertfacetgroupcategory" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("facetgroupcategory-delete", "facetgroupcategory/{facetgroupid}", new { controller = "facetgroups", action = "deletefacetgroupcategory" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            // Gift card routes
            config.Routes.MapHttpRoute("giftcard-get", "giftcards/{giftCardId}", new { controller = "giftcards", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("giftcard-list", "giftcards", new { controller = "giftcards", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("giftcard-create", "giftcards", new { controller = "giftcards", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("giftcard-update", "giftcards/{giftCardId}", new { controller = "giftcards", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("giftcard-delete", "giftcards/{giftCardId}", new { controller = "giftcards", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });
            config.Routes.MapHttpRoute("giftcard-getnextgiftcardnumber", "giftcardnumber", new { controller = "giftcards", action = "getnextgiftcardnumber" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("giftcard-isgiftcardvalid", "giftcardnumber/{giftCard}/{accountId}", new { controller = "giftcards", action = "isvalidgiftcard" }, new { httpMethod = new HttpMethodConstraint("GET") });
            //Google site map routes
            config.Routes.MapHttpRoute("googlesitemap-create", "googlesitemap", new { controller = "googlesitemap", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });

            // Highlight routes
            config.Routes.MapHttpRoute("highlight-get", "highlights/{highlightId}", new { controller = "highlights", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("highlight-list", "highlights", new { controller = "highlights", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("highlight-create", "highlights", new { controller = "highlights", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("highlight-update", "highlights/{highlightId}", new { controller = "highlights", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("highlight-delete", "highlights/{highlightId}", new { controller = "highlights", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });
            config.Routes.MapHttpRoute("highlight-checkassociatedproduct", "highlights/checkassociatedproduct/{highlightId}", new { controller = "highlights", action = "checkassociatedproduct" }, new { httpMethod = new HttpMethodConstraint("GET") });

            // Highlight type routes
            config.Routes.MapHttpRoute("highlighttype-get", "highlighttypes/{highlightTypeId}", new { controller = "highlighttypes", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("highlighttype-list", "highlighttypes", new { controller = "highlighttypes", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("highlighttype-create", "highlighttypes", new { controller = "highlighttypes", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("highlighttype-update", "highlighttypes/{highlightTypeId}", new { controller = "highlighttypes", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("highlighttype-delete", "highlighttypes/{highlightTypeId}", new { controller = "highlighttypes", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            // Inventory routes
            config.Routes.MapHttpRoute("inventory-get", "inventory/{inventoryId}", new { controller = "inventory", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("inventory-list", "inventory", new { controller = "inventory", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("inventory-create", "inventory", new { controller = "inventory", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("inventory-update", "inventory/{inventoryId}", new { controller = "inventory", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("inventory-delete", "inventory/{inventoryId}", new { controller = "inventory", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            //Import Export routes
            config.Routes.MapHttpRoute("exports-getexports", "exports", new { controller = "importexport", action = "getexports" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("imports-importsdata", "imports", new { controller = "importexport", action = "importsdata" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("imports-getimportdetails", "imports/{type}", new { controller = "importexport", action = "getimportdetails" }, new { httpMethod = new HttpMethodConstraint("GET") });

            // Locale routes
            config.Routes.MapHttpRoute("locale-list", "locales", new { controller = "locale", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });

            // Manufacturer routes
            config.Routes.MapHttpRoute("manufacturer-get", "manufacturers/{manufacturerId}", new { controller = "manufacturers", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("manufacturer-list", "manufacturers", new { controller = "manufacturers", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("manufacturer-create", "manufacturers", new { controller = "manufacturers", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("manufacturer-update", "manufacturers/{manufacturerId}", new { controller = "manufacturers", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("manufacturer-delete", "manufacturers/{manufacturerId}", new { controller = "manufacturers", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            // MasterPage routes
            config.Routes.MapHttpRoute("masterpage-list", "masterpages", new { controller = "masterpage", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("masterpage-getmasterpagebythemeid", "masterpages/getmasterpagebythemeid/{themeId}/{pageType}", new { controller = "masterpage", action = "getmasterpagebythemeid" }, new { httpMethod = new HttpMethodConstraint("GET") });


            // MessageConfig routes
            config.Routes.MapHttpRoute("messageconfig-get", "messageconfigs/{messageConfigId}", new { controller = "messageconfigs", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("messageconfig-list", "messageconfigs", new { controller = "messageconfigs", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("messageconfig-listbykeys", "messageconfigs/keys/{keys}", new { controller = "messageconfigs", action = "listbykeys" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("messageconfig-getbykey", "messageconfigs/key/{key}", new { controller = "messageconfigs", action = "GetByKey" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("messageconfig-create", "messageconfigs", new { controller = "messageconfigs", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("messageconfig-update", "messageconfigs/{messageConfigId}", new { controller = "messageconfigs", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("messageconfig-delete", "messageconfigs/{messageConfigId}", new { controller = "messageconfigs", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            //Manage Search Index
            config.Routes.MapHttpRoute("managesearchindex-list", "managesearchindex", new { controller = "managesearchindex", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("managesearchindex-getserverstatus", "managesearchindex/{indexId}", new { controller = "managesearchindex", action = "getindexserverstatus" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("managesearchindex-create", "managesearchindex", new { controller = "managesearchindex", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("managesearchindex-disabled", "managesearchindex/{id}", new { controller = "managesearchindex", action = "Update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("messageconfig-disablewinservice", "managesearchindex/serviceflag/{serviceflag}", new { controller = "managesearchindex", action = "disablewinservice" }, new { httpMethod = new HttpMethodConstraint("PUT") });

            // Order routes
            config.Routes.MapHttpRoute("order-orderdetails", "getorderdetails", new { controller = "orders", action = "getorderdetails" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("order-orderlist", "order/getorderlist", new { controller = "orders", action = "getorderlist" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("order-get", "orders/{orderId}", new { controller = "orders", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("order-list", "orders", new { controller = "orders", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("order-create", "orders", new { controller = "orders", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("order-update", "orders/{orderId}", new { controller = "orders", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("order-updateorderstatus", "updateorderstatus/{orderId}", new { controller = "orders", action = "updateorderstatus" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("order-voidpayment", "voidpayment/{orderId}", new { controller = "orders", action = "voidpayment" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("order-refundpayment", "refundpayment/{orderId}", new { controller = "orders", action = "refundpayment" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("order-download", "downloadorder/", new { controller = "orders", action = "downloadorder" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("orderlineitem-download", "downloadorderlineitemdata/", new { controller = "orders", action = "downloadorderlineitemdata" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("orderlineitem-sendemail", "sendemail/", new { controller = "orders", action = "sendemail" }, new { httpMethod = new HttpMethodConstraint("GET") });

            config.Routes.MapHttpRoute("order-addnewcustomer", "addnewcustomer", new { controller = "orders", action = "AddNewCustomer" }, new { httpMethod = new HttpMethodConstraint("POST") });

            config.Routes.MapHttpRoute("order-updateorderpaymentstatus", "updateorderpaymentstatus/{orderId}/{paymentStatus}", new { controller = "orders", action = "updateorderpaymentstatus" }, new { httpMethod = new HttpMethodConstraint("GET") });
            //Order States routes
            config.Routes.MapHttpRoute("orderstate-list", "orderstates", new { controller = "orderstate", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("order-getorderlineitems", "getorderlineitems/{orderLineItemId}", new { controller = "orders", action = "getorderlineitems" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("order-updateorderlineitemstatus", "updateorderlineitemstatus/{orderLineItemId}", new { controller = "orders", action = "updateorderlineitemstatus" }, new { httpMethod = new HttpMethodConstraint("PUT") });

            //Znode Version 7.2.2
            //Reorder routes
            config.Routes.MapHttpRoute("reorder-items", "reorder/{orderId}", new { controller = "reorder", action = "getitems" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("reorder-singleitems", "reorderorderlineitem/{orderLineItemIds}", new { controller = "reorderorderlineitem", action = "getreorderlineitem" }, new { httpMethod = new HttpMethodConstraint("GET") });

            // Payment gateway routes
            config.Routes.MapHttpRoute("paymentgateway-get", "paymentgateways/{paymentGatewayId}", new { controller = "paymentgateways", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("paymentgateway-list", "paymentgateways", new { controller = "paymentgateways", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("paymentgateway-create", "paymentgateways", new { controller = "paymentgateways", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("paymentgateway-update", "paymentgateways/{paymentGatewayId}", new { controller = "paymentgateways", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("paymentgateway-delete", "paymentgateways/{paymentGatewayId}", new { controller = "paymentgateways", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            // Payment option routes
            config.Routes.MapHttpRoute("paymentoption-get", "paymentoptions/{paymentOptionId}", new { controller = "paymentoptions", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("paymentoption-list", "paymentoptions", new { controller = "paymentoptions", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("paymentoption-create", "paymentoptions", new { controller = "paymentoptions", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("paymentoption-update", "paymentoptions/{paymentOptionId}", new { controller = "paymentoptions", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("paymentoption-delete", "paymentoptions/{paymentOptionId}", new { controller = "paymentoptions", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            // Payment type routes
            config.Routes.MapHttpRoute("paymenttype-get", "paymenttypes/{paymentTypeId}", new { controller = "paymenttypes", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("paymenttype-list", "paymenttypes", new { controller = "paymenttypes", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("paymenttype-create", "paymenttypes", new { controller = "paymenttypes", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("paymenttype-update", "paymenttypes/{paymentTypeId}", new { controller = "paymenttypes", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("paymenttype-delete", "paymenttypes/{paymentTypeId}", new { controller = "paymenttypes", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            //Roles and Permissions route
            config.Routes.MapHttpRoute("perminssions-getbyaccountid", "permissions/getrolesandperminssions/{accountId}", new { Controller = "Permissions", action = "getrolesandperminssions" }, new { httpMethod = new HttpMethodConstraint("GET"), accountId = @"^\d+$" });
            config.Routes.MapHttpRoute("perminssions-updaterolesandpermissions", "permissions/updaterolesandpermissions", new { Controller = "Permissions", action = "updaterolesandpermissions" }, new { httpMethod = new HttpMethodConstraint("PUT") });

            //Personalization routes           
            config.Routes.MapHttpRoute("personalization-listbyproductid", "crosssells", new { controller = "personalization", action = "listbyproductid" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("personalization-create", "crosssellproducts", new { controller = "personalization", action = "createcrosssellproduct" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("personalization-delete", "crosssellproducts/{productId}/{relationTypeId}", new { controller = "personalization", action = "deletecrosssellproduct" }, new { httpMethod = new HttpMethodConstraint("DELETE") });
            config.Routes.MapHttpRoute("personalization-list", "frequentlyboughtproducts", new { controller = "personalization", action = "frequentlyboughtproductslist" }, new { httpMethod = new HttpMethodConstraint("GET") });

            // Portal routes
            config.Routes.MapHttpRoute("portal-get", "portals/{portalId}", new { controller = "portals", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET"), portalId = @"^\d+$" });
            config.Routes.MapHttpRoute("portal-list", "portals", new { controller = "portals", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("portal-listbyportalids", "portals/{portalIds}", new { controller = "portals", action = "listbyportalids" }, new { httpMethod = new HttpMethodConstraint("GET"), portalIds = @"^(\d+,?)+$" });
            config.Routes.MapHttpRoute("portal-create", "portals", new { controller = "portals", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("portal-update", "portals/{portalId}", new { controller = "portals", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("portal-delete", "portals/{portalId}", new { controller = "portals", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });
            config.Routes.MapHttpRoute("portal-getfedexkeys", "getfedexkey", new { controller = "portals", action = "getfedexkey" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("portal-createmessage", "createmessages/{portalId}/{localeId}", new { controller = "portals", action = "createmessage" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("portal-copy", "copystore/{portalId}", new { controller = "portals", action = "copystore" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("portal-deletebyid", "deleteportalbyportalid/{portalId}", new { controller = "portals", action = "deletebyportalid" }, new { httpMethod = new HttpMethodConstraint("DELETE") });
            config.Routes.MapHttpRoute("portalinformation-get", "portalinformationbyportalid/{portalId}", new { controller = "portals", action = "getportalinformationbyportalid" }, new { httpMethod = new HttpMethodConstraint("GET"), portalId = @"^\d+$" });
            config.Routes.MapHttpRoute("portal-listbyprofileaccess", "portalsbyprofileaccess", new { controller = "portals", action = "getportallistbyprofileaccess" }, new { httpMethod = new HttpMethodConstraint("GET") });

            //Portal Profile Routes
            config.Routes.MapHttpRoute("portalprofile-get", "portalprofiles/{portalProfileId}", new { controller = "portalprofile", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET"), portalProfileId = @"^\d+$" });
            config.Routes.MapHttpRoute("portalprofile-list", "portalprofiles", new { controller = "portalprofile", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("portalprofile-create", "portalprofile", new { controller = "portalprofile", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("portalprofile-update", "updateportalprofile/{portalProfileId}", new { controller = "portalprofile", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("portalprofile-delete", "portalprofile/{portalProfileId}", new { controller = "portalprofile", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });
            config.Routes.MapHttpRoute("portalprofile-listdetails", "portalprofilesdetails", new { controller = "portalprofile", action = "getportalprofilelistdetails" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("getportalprofilesbyportalid-list", "getportalprofilesbyportalid/{portalId}", new { controller = "portalprofile", action = "getportalprofilesbyportalid" }, new { httpMethod = new HttpMethodConstraint("GET") });

            // Portal catalog routes
            config.Routes.MapHttpRoute("portalcatalog-get", "portalcatalogs/{portalCatalogId}", new { controller = "portalcatalogs", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET"), portalCatalogId = @"^\d+$" });
            config.Routes.MapHttpRoute("portalcatalog-list", "portalcatalogs", new { controller = "portalcatalogs", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("portalcatalog-create", "portalcatalogs", new { controller = "portalcatalogs", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("portalcatalog-update", "portalcatalogs/{portalCatalogId}", new { controller = "portalcatalogs", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("portalcatalog-delete", "portalcatalogs/{portalCatalogId}", new { controller = "portalcatalogs", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });
            config.Routes.MapHttpRoute("portalcatalog-getbyportalid", "portalcatalogsbyportalid/{portalId}", new { controller = "portalcatalogs", action = "getlistbyportalid" }, new { httpMethod = new HttpMethodConstraint("GET") });

            // Portal country routes
            config.Routes.MapHttpRoute("portalcountry-get", "portalcountries/{portalCountryId}", new { controller = "portalcountries", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("portalcountry-list", "portalcountries", new { controller = "portalcountries", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("portalcountry-create", "portalcountries", new { controller = "portalcountries", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("portalcountry-update", "portalcountries/{portalCountryId}", new { controller = "portalcountries", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("portalcountry-delete", "portalcountries/{portalCountryId}", new { controller = "portalcountries", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });
            config.Routes.MapHttpRoute("portalcountry-getallportalcountries", "getallportalcountries", new { controller = "portalcountries", action = "getallportalcountries" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("portalcountry-deleteifnotassociated", "deleteportalcountryifnotassociated/{portalCountryId}", new { controller = "portalcountries", action = "deleteifnotassociated" }, new { httpMethod = new HttpMethodConstraint("DELETE") });
            config.Routes.MapHttpRoute("portalcountry-createportalcountries", "createsportalcountries/{portalId}/{shipableCountryCodes}/{billableCountryCodes}", new { controller = "portalcountries", action = "createportalcountries" }, new { httpMethod = new HttpMethodConstraint("GET") });

            // Product routes
            config.Routes.MapHttpRoute("product-get", "products/{productId}", new { controller = "products", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET"), productId = @"^\d+$" });
            config.Routes.MapHttpRoute("product-getproductdetailsbyproductid", "products/getproductdetailsbyproductid/{productId}", new { controller = "products", action = "getproductdetailsbyproductid" }, new { httpMethod = new HttpMethodConstraint("GET"), productId = @"^\d+$" });
            config.Routes.MapHttpRoute("product-getwithsku", "products/{productId}/{skuId}", new { controller = "products", action = "getwithsku" }, new { httpMethod = new HttpMethodConstraint("GET"), productId = @"^\d+$" });
            config.Routes.MapHttpRoute("product-list", "products", new { controller = "products", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("product-listbycatalog", "products/catalog/{catalogId}", new { controller = "products", action = "listbycatalog" }, new { httpMethod = new HttpMethodConstraint("GET"), catalogId = @"^\d+$" });
            config.Routes.MapHttpRoute("product-listbycatalogids", "products/catalog/{catalogIds}", new { controller = "products", action = "listbycatalogids" }, new { httpMethod = new HttpMethodConstraint("GET"), catalogIds = @"^(\d+,?)+$" });
            config.Routes.MapHttpRoute("product-listbycategory", "products/category/{categoryId}", new { controller = "products", action = "listbycategory" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("product-listbycategorybypromotiontype", "products/category/{categoryId}/promotiontype/{promotionTypeId}", new { controller = "products", action = "listbycategorybypromotiontype" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("product-listbyexternalids", "products/externalids/{externalIds}", new { controller = "products", action = "listbyexternalids" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("product-listbyhomespecials", "products/homespecials", new { controller = "products", action = "listbyhomespecials" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("product-listbyproductids", "products/listbyproductids", new { controller = "products", action = "listbyproductids" }, new { httpMethod = new HttpMethodConstraint("POST")});
            config.Routes.MapHttpRoute("product-listbypromotiontype", "products/promotiontype/{promotionTypeId}", new { controller = "products", action = "listbypromotiontype" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("product-create", "products", new { controller = "products", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("product-update", "products/{productId}", new { controller = "products", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("product-updateproductsettings", "products/updateproductsettings/{productId}", new { controller = "products", action = "updateproductsettings" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("product-updateproductseoinformation", "products/updateproductseoinformation/{productId}", new { controller = "products", action = "updateproductseoinformation" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("product-delete", "products/{productId}", new { controller = "products", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });
            config.Routes.MapHttpRoute("product-isseourlexist", "products/isseourlexist/{productId}/{seoUrl}", new { controller = "products", action = "isseourlexist" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("product-getproductcategorybyproductid'", "products/getproductcategorybyproductid", new { controller = "products", action = "getproductcategorybyproductid" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("product-getproductunassociatedcategorybyproductid'", "products/getproductunassociatedcategorybyproductid/{productId}", new { controller = "products", action = "getproductunassociatedcategorybyproductid" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("product-associateproductcategory", "products/associateproductcategory", new { controller = "products", action = "associateproductcategory" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("product-unassociateproductcategory", "products/unassociateproductcategory", new { controller = "products", action = "unassociateproductcategory" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("product-getallproducts", "products/getallproducts", new { controller = "products", action = "getallproducts" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("product-getproductskudetails", "products/getproductskudetails", new { controller = "products", action = "getproductskudetails" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("product-associateskufacets", "products/associateskufacets/{skuId}/{associateFacetIds}/{unassociateFacetIds}", new { controller = "products", action = "associateskufacets" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("product-deleteskuassociatedfacets", "products/deleteskuassociatedfacets/{skuId}/{facetGroupId}", new { controller = "products", action = "deleteskuassociatedfacets" }, new { httpMethod = new HttpMethodConstraint("DELETE") });
            config.Routes.MapHttpRoute("product-getskuassociatedfacets", "products/getskuassociatedfacets/{productId}/{skuId}/{facetGroupId}", new { controller = "products", action = "getskuassociatedfacets" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("product-getskufacets", "products/getskufacets", new { controller = "products", action = "getskufacets" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("product-search", "searchproducts", new { controller = "products", action = "searchproducts" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("product-createorderproduct", "createorderproduct/{productId}", new { controller = "products", action = "createorderproduct" }, new { httpMethod = new HttpMethodConstraint("GET"), productId = @"^\d+$" });
            config.Routes.MapHttpRoute("product-copyproduct", "copyproduct/{productId}", new { controller = "products", action = "copyproduct" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("product-deletebyproductid", "products/deletesbyproductid/{productId}", new { controller = "products", action = "deletebyproductid" }, new { httpMethod = new HttpMethodConstraint("DELETE") });
            config.Routes.MapHttpRoute("product-getskuproductlistbysku", "products/getskuproductlistbysku", new { controller = "products", action = "getskuproductlistbysku" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("product-sendmail", "products/sendmail", new { controller = "products", action = "sendmail" }, new { httpMethod = new HttpMethodConstraint("POST") });

            // Attribute routes
            config.Routes.MapHttpRoute("productattribute-get", "productattributes/{attributeId}", new { controller = "productattributes", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("productattribute-getattributesbyattributetypeid", "productattributesbyattributetypeid", new { controller = "productattributes", action = "getattributesbyattributetypeid" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("productattribute-list", "productattributes", new { controller = "productattributes", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("productattribute-create", "productattributes", new { controller = "productattributes", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("productattribute-update", "productattributes/{attributeId}", new { controller = "productattributes", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("productattribute-delete", "productattributes/{attributeId}", new { controller = "productattributes", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            //Product Bundles routes
            config.Routes.MapHttpRoute("productbundles-list", "productbundles", new { controller = "products", action = "getproductbundleslist" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("productbundles-delete", "productbundles/{parentChildProductID}", new { controller = "products", action = "deletebundleproduct" }, new { httpMethod = new HttpMethodConstraint("DELETE") });
            config.Routes.MapHttpRoute("productbundles-customproductlist", "customproductlist", new { controller = "products", action = "getproductlist" }, new { httpMethod = new HttpMethodConstraint("GET") });
            //config.Routes.MapHttpRoute("productbundles-associatebundleproduct", "productbundles/associatebundleproduct/{productId}/{productIds}", new { controller = "products", action = "associatebundleproduct" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("productbundles-associatebundleproduct", "productbundles/associatebundleproduct", new { controller = "products", action = "associatebundleproduct" }, new { httpMethod = new HttpMethodConstraint("POST") });

            // Product category routes
            config.Routes.MapHttpRoute("productcategory-get", "productcategories/{productCategoryId}", new { controller = "productcategories", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET"), productCategoryId = @"^\d+$" });
            config.Routes.MapHttpRoute("productcategory-getproductcategory", "productcategories/{productId}/{categoryId}", new { controller = "productcategories", action = "getproductcategory" }, new { httpMethod = new HttpMethodConstraint("GET"), productId = @"^\d+$" });
            config.Routes.MapHttpRoute("productcategory-list", "productcategories", new { controller = "productcategories", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("productcategory-create", "productcategories", new { controller = "productcategories", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("productcategory-update", "productcategories/{productCategoryId}", new { controller = "productcategories", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("productcategory-delete", "productcategories/{productCategoryId}", new { controller = "productcategories", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            //Product Facets routes
            config.Routes.MapHttpRoute("productfacets-list", "productfacets", new { controller = "products", action = "getproductfacetslist" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("productfacets-get", "productfacets/{productId}/{facetGroupId}", new { controller = "products", action = "getproductfacets" }, new { httpMethod = new HttpMethodConstraint("GET"), productId = @"^\d+$" });
            config.Routes.MapHttpRoute("productfacets-update", "productfacets/{productId}", new { controller = "products", action = "bindproductfacets" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("productfacets-delete", "productfacets/{productId}/{facetGroupId}", new { controller = "products", action = "deleteproductassociatedfacets" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            //Product Image Types
            config.Routes.MapHttpRoute("productimagetypes-list", "productimagetypes", new { controller = "products", action = "getproductimagetypes" }, new { httpMethod = new HttpMethodConstraint("GET") });

            //Product Alternate images
            config.Routes.MapHttpRoute("productalternateimage-get", "productalternateimage/{productImageId}", new { controller = "products", action = "getproductalternateimage" }, new { httpMethod = new HttpMethodConstraint("GET"), productImageId = @"^\d+$" });
            config.Routes.MapHttpRoute("productalternateimage-list", "productalternateimage", new { controller = "products", action = "getallproductalternateimage" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("productalternateimage-create", "productalternateimage", new { controller = "products", action = "insertproductalternateimage" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("productalternateimage-update", "productalternateimage/{productImageId}", new { controller = "products", action = "updateproductalternateimage" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("productalternateimage-delete", "productalternateimage/{productImageId}", new { controller = "products", action = "deleteproductalternateimage" }, new { httpMethod = new HttpMethodConstraint("DELETE") });


            //Product Tags routes
            config.Routes.MapHttpRoute("producttags-get", "producttags/{productId}", new { controller = "products", action = "getproducttag" }, new { httpMethod = new HttpMethodConstraint("GET"), productId = @"^\d+$" });
            config.Routes.MapHttpRoute("producttags-create", "producttags", new { controller = "products", action = "createproducttag" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("producttags-update", "producttags/{tagId}", new { controller = "products", action = "updateproducttag" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("producttags-delete", "producttags/{tagId}", new { controller = "products", action = "deleteproducttag" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            // Product Addons routes
            config.Routes.MapHttpRoute("productaddons-remove", "removeproductaddon/{productAddOnId}", new { controller = "products", action = "RemoveProductAddOn" }, new { httpMethod = new HttpMethodConstraint("DELETE") });
            config.Routes.MapHttpRoute("productaddons-associate", "associateaddon/", new { controller = "products", action = "AssociateAddOn" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("productaddons-getassociatedaddons", "productaddons/{productId}/", new { controller = "products", action = "ProductAddOns" }, new { httpMethod = new HttpMethodConstraint("GET"), productId = @"^\d+$" });
            config.Routes.MapHttpRoute("productaddons-getunassociatedaddons", "unassociatedaddons/{productId}/{portalId}", new { controller = "products", action = "UnassociatedAddOns" }, new { httpMethod = new HttpMethodConstraint("GET"), productId = @"^\d+$" });

            // Product Highlight routes
            config.Routes.MapHttpRoute("producthighlight-remove", "removeproducthighlight/{productHighlightId}", new { controller = "products", action = "RemoveProductHighlight" }, new { httpMethod = new HttpMethodConstraint("DELETE") });
            config.Routes.MapHttpRoute("producthighlight-associate", "associatehighlight", new { controller = "products", action = "AssociateHighlight" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("producthighlight-getassociatedhighlights", "producthighlights/{productId}", new { controller = "products", action = "ProductHighlights" }, new { httpMethod = new HttpMethodConstraint("GET"), productId = @"^\d+$" });
            config.Routes.MapHttpRoute("producthighlight-getunassociatedhighlights", "unassociatedhighlights/{productId}/{portalId}", new { controller = "products", action = "UnassociatedHighlights" }, new { httpMethod = new HttpMethodConstraint("GET"), productId = @"^\d+$" });

            // Product Tier pricing routes
            config.Routes.MapHttpRoute("producttier-get", "producttiers/{productId}", new { controller = "products", action = "getproducttiers" }, new { httpMethod = new HttpMethodConstraint("GET"), productId = @"^\d+$" });
            config.Routes.MapHttpRoute("producttier-create", "createproducttier", new { controller = "products", action = "createproducttier" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("producttier-update", "updateproducttier", new { controller = "products", action = "updateproducttier" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("producttier-delete", "deleteproducttier/{productTierId}", new { controller = "products", action = "deleteproducttier" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            // Product Digital Asset route
            config.Routes.MapHttpRoute("digitalasset-get", "digitalassets/{productId}", new { controller = "products", action = "getdigitalassets" }, new { httpMethod = new HttpMethodConstraint("GET"), productId = @"^\d+$" });
            config.Routes.MapHttpRoute("digitalasset-create", "createdigitalasset", new { controller = "products", action = "createdigitalasset" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("digitalasset-delete", "deletedigitalasset/{digitalassetid}", new { controller = "products", action = "deletedigitalasset" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            // Product Type routes
            config.Routes.MapHttpRoute("producttype-get", "producttype/{productTypeId}", new { controller = "producttype", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("producttype-list", "producttype", new { controller = "producttype", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("producttype-getproductattributesbyproducttypeid", "getproductattributesbyproducttypeid", new { controller = "producttype", action = "getproductattributesbyproducttypeid" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("producttype-create", "producttypes", new { controller = "producttype", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("producttype-update", "producttype/{productTypeId}", new { controller = "producttype", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("producttype-delete", "producttypes/{productTypeId}", new { controller = "producttype", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            // Product Type Attribute routes
            config.Routes.MapHttpRoute("producttypeattribute-get", "producttypeattributs/{productTypeAttributId}", new { controller = "categories", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET"), attributeId = @"^\d+$" });
            config.Routes.MapHttpRoute("producttypeattribute-create", "producttypeattributs", new { controller = "ProductTypeAttribute", action = "Create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("producttypeattribute-getattributetypesbyproducttypeid", "producttypeattribute/getattributetypesbyproducttypeid/{productTypeId}", new { controller = "producttypeattribute", action = "getattributetypesbyproducttypeid" }, new { httpMethod = new HttpMethodConstraint("GET"), productTypeId = @"^\d+$" });
            config.Routes.MapHttpRoute("producttypeattribute-isproductassociatedproducttype", "producttypeattribute/{productTypeId}", new { controller = "producttypeattribute", action = "IsProductAssociatedProductType" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("producttypeattribute-delete", "producttypeattribute/{productTypeAttributId}", new { controller = "producttypeattribute", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });
            config.Routes.MapHttpRoute("producttypeattribute-list", "producttypeattribute", new { controller = "producttypeattribute", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });

            //Product Review State routes
            config.Routes.MapHttpRoute("productreviewstate-list", "productreviewstates", new { controller = "productreviewstate", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });

            //Product Search Setting
            config.Routes.MapHttpRoute("productsearchsetting-productlevelsetting", "getproductlevelsetting", new { controller = "productsearchsetting", action = "getproductlevelsettings" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("productsearchsetting-categorylevelsetting", "getcategorylevelsettings", new { controller = "productsearchsetting", action = "getcategorylevelsettings" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("productsearchsetting-fieldlevelsetting", "getfieldlevelsettings", new { controller = "productsearchsetting", action = "getfieldlevelsettings" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("productsearchsetting-saveboostvalues", "saveboostvalues", new { controller = "productsearchsetting", action = "saveboostvalues" }, new { httpMethod = new HttpMethodConstraint("POST") });

            //Profile route
            config.Routes.MapHttpRoute("profile-get", "profiles/{profileId}", new { controller = "profile", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET"), profileId = @"^\d+$" });
            config.Routes.MapHttpRoute("profile-list", "profiles", new { controller = "profile", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("profile-create", "profiles", new { controller = "profile", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("profile-update", "profiles/{profileId}", new { controller = "profile", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("profile-delete", "profiles/{profileId}", new { controller = "profile", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });
            config.Routes.MapHttpRoute("profile-categorylist", "profile/getprofilelist", new { controller = "profile", action = "getprofilelist" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("profile-listbyportalid", "getprofilelistbyid/{portalId}", new { controller = "profile", action = "getprofilelistbyprofileid" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("customerprofile-list", "customerprofiles/{accountId}", new { controller = "profile", action = "customerprofilelist" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("customernotassociatedprofile-list", "customernotassociatedprofiles", new { controller = "profile", action = "customernotassociatedprofiles" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("getprofilelistbyportalid-list", "getprofilelistbyportalid/{portalId}", new { controller = "profile", action = "getprofilelistbyportalid" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("GetAvailableProfileListBySkuIdCategoryId-list", "GetAvailableProfileListBySkuIdCategoryId", new { controller = "profile", action = "GetAvailableProfileListBySkuIdCategoryId" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("profile-getznodeprofile", "profiles/getznodeprofile", new { controller = "profile", action = "getznodeprofile" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("profile-getprofilesassociatedwithusers", "getprofilesassociatedwithusers", new { controller = "profile", action = "getprofilesassociatedwithusers" }, new { httpMethod = new HttpMethodConstraint("POST") });

            //Profile Common route
            config.Routes.MapHttpRoute("profilecommon-getprofilestoreaccess", "profilecommon/getprofilestoreaccess", new { controller = "profilecommon", action = "getprofilestoreaccess" }, new { httpMethod = new HttpMethodConstraint("POST") });

            // Promotion routes
            config.Routes.MapHttpRoute("promotion-get", "promotions/{promotionId}", new { controller = "promotions", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("promotion-list", "promotions", new { controller = "promotions", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("promotion-create", "promotions", new { controller = "promotions", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("promotion-update", "promotions/{promotionId}", new { controller = "promotions", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("promotion-delete", "promotions/{promotionId}", new { controller = "promotions", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });
            config.Routes.MapHttpRoute("getproductslist-list", "getproductslist", new { controller = "promotions", action = "getproductslist" }, new { httpMethod = new HttpMethodConstraint("GET") });

            // Promotion type routes
            config.Routes.MapHttpRoute("promotiontype-get", "promotiontypes/{promotionTypeId}", new { controller = "promotiontypes", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("promotiontype-list", "promotiontypes", new { controller = "promotiontypes", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("promotiontype-create", "promotiontypes", new { controller = "promotiontypes", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("promotiontype-update", "promotiontypes/{promotionTypeId}", new { controller = "promotiontypes", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("promotiontype-delete", "promotiontypes/{promotionTypeId}", new { controller = "promotiontypes", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });
            config.Routes.MapHttpRoute("promotiontype-getallpromotiontypesnotindatabase", "getallpromotiontypesnotindatabase", new { controller = "promotiontypes", action = "getallpromotiontypesnotindatabase" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("getfranchisepromotiontypes-list", "getfranchisepromotiontypes", new { controller = "promotiontypes", action = "getfranchisepromotiontypes" }, new { httpMethod = new HttpMethodConstraint("GET") });

            //Plugins Load
            config.Routes.MapHttpRoute("LoadPlugins", "loadplugins", new { controller = "LoadPlugins", action = "LoadPlugins" }, new { httpMethod = new HttpMethodConstraint("GET") });
            
            // Review routes
            config.Routes.MapHttpRoute("review-get", "reviews/{reviewId}", new { controller = "reviews", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("review-list", "reviews", new { controller = "reviews", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("review-create", "reviews", new { controller = "reviews", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("review-update", "reviews/{reviewId}", new { controller = "reviews", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("review-delete", "reviews/{reviewId}", new { controller = "reviews", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            //ReasonForReturn routes
            config.Routes.MapHttpRoute("reasonforreturn-get", "reasonforreturn/{reasonForReturnId}", new { controller = "reasonforreturn", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("reasonforreturn-list", "reasonforreturn", new { controller = "reasonforreturn", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("reasonforreturn-create", "reasonforreturn", new { controller = "reasonforreturn", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("reasonforreturn-update", "reasonforreturn/{reasonForReturnId}", new { controller = "reasonforreturn", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("reasonforreturn-delete", "reasonforreturn/{reasonForReturnId}", new { controller = "reasonforreturn", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            // Rejection Message routes
            config.Routes.MapHttpRoute("rejectionmessage-get", "rejectionmessage/{rejectionMessageId}", new { controller = "rejectionmessage", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("rejectionmessage-list", "rejectionmessage", new { controller = "rejectionmessage", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("rejectionmessage-create", "rejectionmessage", new { controller = "rejectionmessage", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("rejectionmessage-update", "rejectionmessage/{rejectionMessageId}", new { controller = "rejectionmessage", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("rejectionmessage-delete", "rejectionmessage/{rejectionMessageId}", new { controller = "rejectionmessage", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            //RMAConfiguration routes
            config.Routes.MapHttpRoute("rmaconfiguration-get", "rmaconfigurations/{rmaConfigId}", new { controller = "rmaconfigurations", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("rmaconfiguration-list", "rmaconfigurations", new { controller = "rmaconfigurations", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("rmaconfiguration-getAll", "allrmaconfigurations", new { controller = "rmaconfigurations", action = "getallrmaconfiguration" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("rmaconfiguration-create", "rmaconfigurations", new { controller = "rmaconfigurations", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("rmaconfiguration-update", "rmaconfigurations/{rmaConfigId}", new { controller = "rmaconfigurations", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });

            //RMA Request routes
            config.Routes.MapHttpRoute("rmarequest-list", "rmarequests", new { controller = "rmarequest", action = "getrmarequestlist" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("rmarequest-update", "rmarequestupdate/{rmaRequestId}", new { controller = "rmarequest", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("rmarequest-get", "rmarequest/{rmaRequestId}", new { controller = "rmarequest", action = "getrmarequest" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("rmarequest-delete", "deletermarequest/{rmaRequestId}", new { controller = "rmarequest", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });
            config.Routes.MapHttpRoute("rmarequest-getgiftcarddetails", "rmarequestgiftcarddetails/{rmaRequestId}", new { controller = "rmarequest", action = "getrmarequestgiftcarddetails" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("rmarequest-create", "creatermarequest", new { controller = "rmarequest", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("rmarequest-getorderrmaflag", "getorderrmaflag/{orderId}", new { controller = "rmarequest", action = "getorderrmaflag" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("rmarequest-sendemailstatus", "sendstatusmail/{rmaRequestId}", new { controller = "rmarequest", action = "sendstatusmail" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("rmarequest-sendgiftcardemailstatus", "sendgiftcardmail", new { controller = "rmarequest", action = "sendgiftcardmail" }, new { httpMethod = new HttpMethodConstraint("Post") });

            //RMA Request items routes
            config.Routes.MapHttpRoute("rmarequestitem-list", "rmarequestitems", new { controller = "rmarequestitem", action = "getrmarequestitemlist" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("rmarequestitem-get", "rmarequestitem/{orderLineItems}", new { controller = "rmarequestitem", action = "getrmarequestitemsforgiftcard" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("rmarequestitem-create", "rmarequestitem", new { controller = "rmarequestitem", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });

            //Reports
            config.Routes.MapHttpRoute("reports-reportdata", "myreports/{reportName}", new { controller = "myreports", action = "getreportdataset" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("reports-getorderdetails", "myreports/{orderId}/{reportName}", new { controller = "myreports", action = "getorderdetails" }, new { httpMethod = new HttpMethodConstraint("GET") });

            //Request status routes
            config.Routes.MapHttpRoute("requeststatus-get", "requeststatus/{requestStatusId}", new { controller = "requeststatus", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("requeststatus-list", "requeststatuslist", new { controller = "requeststatus", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("requeststatus-create", "requeststatus", new { controller = "requeststatus", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("requeststatus-update", "requeststatus/{requestStatusId}", new { controller = "requeststatus", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("requeststatus-delete", "requeststatus/{requestStatusId}", new { controller = "requeststatus", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            //Rotate Key Route
            config.Routes.MapHttpRoute("generaterotatekey-create", "generaterotatekey", new { controller = "rotatekeys", action = "generaterotatekey" }, new { httpMethod = new HttpMethodConstraint("GET") });

            // Search routes
            config.Routes.MapHttpRoute("search-keyword", "search/keyword", new { controller = "search", action = "keyword" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("search-suggested", "search/suggested", new { controller = "search", action = "suggested" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("search-reloadindex", "search/reloadindex", new { controller = "search", action = "reloadindex" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("search-seourl", "search/seourl/{seourl}", new { controller = "search", action = "getseourldetail" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("search-isrestrictedseourl", "search/isrestrictedseourl/{seoUrl}", new { controller = "search", action = "isrestrictedseourl" }, new { httpMethod = new HttpMethodConstraint("GET") });

            // Shipping option routes
            config.Routes.MapHttpRoute("shippingoption-get", "shippingoptions/{shippingOptionId}", new { controller = "shippingoptions", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("shippingoption-list", "shippingoptions", new { controller = "shippingoptions", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("shippingoption-create", "shippingoptions", new { controller = "shippingoptions", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("shippingoption-update", "shippingoptions/{shippingOptionId}", new { controller = "shippingoptions", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("shippingoption-delete", "shippingoptions/{shippingOptionId}", new { controller = "shippingoptions", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });
            config.Routes.MapHttpRoute("getfranchiseshippingoptionlist-list", "getfranchiseshippingoptionlist", new { controller = "shippingoptions", action = "getfranchiseshippingoptionlist" }, new { httpMethod = new HttpMethodConstraint("GET") });

            // Shipping rule routes
            config.Routes.MapHttpRoute("shippingrule-get", "shippingrules/{shippingRuleId}", new { controller = "shippingrules", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("shippingrule-list", "shippingrules", new { controller = "shippingrules", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("shippingrule-create", "shippingrules", new { controller = "shippingrules", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("shippingrule-update", "shippingrules/{shippingRuleId}", new { controller = "shippingrules", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("shippingrule-delete", "shippingrules/{shippingRuleId}", new { controller = "shippingrules", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            // Shipping rule type routes
            config.Routes.MapHttpRoute("shippingruletype-get", "shippingruletypes/{shippingRuleTypeId}", new { controller = "shippingruletypes", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("shippingruletype-list", "shippingruletypes", new { controller = "shippingruletypes", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("shippingruletype-create", "shippingruletypes", new { controller = "shippingruletypes", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("shippingruletype-update", "shippingruletypes/{shippingRuleTypeId}", new { controller = "shippingruletypes", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("shippingruletype-delete", "shippingruletypes/{shippingRuleTypeId}", new { controller = "shippingruletypes", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            // Shipping service code routes
            config.Routes.MapHttpRoute("shippingservicecode-get", "shippingservicecodes/{shippingServiceCodeId}", new { controller = "shippingservicecodes", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("shippingservicecode-list", "shippingservicecodes", new { controller = "shippingservicecodes", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("shippingservicecode-create", "shippingservicecodes", new { controller = "shippingservicecodes", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("shippingservicecode-update", "shippingservicecodes/{shippingServiceCodeId}", new { controller = "shippingservicecodes", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("shippingservicecode-delete", "shippingservicecodes/{shippingServiceCodeId}", new { controller = "shippingservicecodes", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            // Shipping type routes
            config.Routes.MapHttpRoute("shippingtype-get", "shippingtypes/{shippingTypeId}", new { controller = "shippingtypes", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("shippingtype-list", "shippingtypes", new { controller = "shippingtypes", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("shippingtype-create", "shippingtypes", new { controller = "shippingtypes", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("shippingtype-update", "shippingtypes/{shippingTypeId}", new { controller = "shippingtypes", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("shippingtype-delete", "shippingtypes/{shippingTypeId}", new { controller = "shippingtypes", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });
            config.Routes.MapHttpRoute("shippingtype-getallshippingtypesnotindatabase", "getallshippingtypesnotindatabase", new { controller = "shippingtypes", action = "getallshippingtypesnotindatabase" }, new { httpMethod = new HttpMethodConstraint("GET") });

            // Shopping cart routes
            config.Routes.MapHttpRoute("shoppingcart-getbyaccount", "shoppingcarts/account/{accountId}", new { controller = "shoppingcarts", action = "getbyaccount" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("shoppingcart-getbycookie", "shoppingcarts/cookie/{cookieId}", new { controller = "shoppingcarts", action = "getbycookie" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("shoppingcart-create", "shoppingcarts", new { controller = "shoppingcarts", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("shoppingcart-calculate", "shoppingcarts/calculate", new { controller = "shoppingcarts", action = "calculate" }, new { httpMethod = new HttpMethodConstraint("POST") });

            // SKU routes
            config.Routes.MapHttpRoute("sku-get", "skus/{skuId}", new { controller = "skus", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("sku-list", "skus", new { controller = "skus", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("sku-create", "skus", new { controller = "skus", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("sku-update", "skus/{skuId}", new { controller = "skus", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("sku-delete", "skus/{skuId}", new { controller = "skus", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });
            config.Routes.MapHttpRoute("sku-addskuattribute", "skus/addskuattribute/{skuId}/{attributeIds}", new { controller = "skus", action = "addskuattribute" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("getskulistbyproductid-list", "getskulistbyproductid/{productId}", new { controller = "skus", action = "getskulistbyproductid" }, new { httpMethod = new HttpMethodConstraint("GET") });

            //SKU Profile Effective routes
            config.Routes.MapHttpRoute("skuprofileeffective-get", "skuprofileeffective/{skuProfileEffectiveId}", new { controller = "skuprofileeffective", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("skuprofileeffective-list", "skuprofileeffective", new { controller = "skuprofileeffective", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("skuprofileeffective-create", "skuprofileeffective", new { controller = "skuprofileeffective", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("skuprofileeffective-update", "skuprofileeffective/{skuProfileEffectiveId}", new { controller = "skuprofileeffective", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("skuprofileeffective-delete", "skuprofileeffective/{skuProfileEffectiveId}", new { controller = "skuprofileeffective", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });
            config.Routes.MapHttpRoute("skuprofileeffective-getskuprofileeffectivebyskuid", "skuprofileeffective/getskuprofileeffectivebyskuid/{skuId}", new { controller = "skuprofileeffective", action = "getskuprofileeffectivebyskuid" }, new { httpMethod = new HttpMethodConstraint("GET") });

            // State routes
            config.Routes.MapHttpRoute("state-get", "states/{stateCode}", new { controller = "states", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("state-list", "states", new { controller = "states", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("state-create", "states", new { controller = "states", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("state-update", "states/{stateCode}", new { controller = "states", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("state-delete", "states/{stateCode}", new { controller = "states", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            //Store Locator
            config.Routes.MapHttpRoute("storelocator-list", "storelocators", new { controller = "storelocator", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("storelocator-get", "storelocators/{storeId}", new { controller = "storelocator", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("storelocator-create", "storelocator", new { controller = "storelocator", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("storelocator-update", "storelocator/{storeId}", new { controller = "storelocator", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("storelocator-delete", "storelocator/{storeId}", new { controller = "storelocator", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });
            config.Routes.MapHttpRoute("storelocator-getstoreslist", "storelocators", new { controller = "storelocator", action = "getstoreslist" }, new { httpMethod = new HttpMethodConstraint("Post") });
            // Supplier routes
            config.Routes.MapHttpRoute("supplier-get", "suppliers/{supplierId}", new { controller = "suppliers", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("supplier-list", "suppliers", new { controller = "suppliers", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("supplier-create", "suppliers", new { controller = "suppliers", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("supplier-update", "suppliers/{supplierId}", new { controller = "suppliers", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("supplier-delete", "suppliers/delete/{supplierId}", new { controller = "suppliers", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            // Supplier type routes
            config.Routes.MapHttpRoute("suppliertype-get", "suppliertypes/{supplierTypeId}", new { controller = "suppliertypes", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("suppliertype-list", "suppliertypes", new { controller = "suppliertypes", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("suppliertype-create", "suppliertypes", new { controller = "suppliertypes", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("suppliertype-update", "suppliertypes/{supplierTypeId}", new { controller = "suppliertypes", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("suppliertype-delete", "suppliertypes/{supplierTypeId}", new { controller = "suppliertypes", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });
            config.Routes.MapHttpRoute("suppliertype-getallsuppliertypesnotindatabase", "getallsuppliertypesnotindatabase", new { controller = "suppliertypes", action = "getallsuppliertypesnotindatabase" }, new { httpMethod = new HttpMethodConstraint("GET") });

            // Tax class routes
            config.Routes.MapHttpRoute("taxclass-get", "taxclasses/{taxClassId}", new { controller = "taxclasses", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("taxclass-list", "taxclasses", new { controller = "taxclasses", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("taxclass-getactivetaxclassbyportalid", "taxclasses/getactivetaxclassbyportalid/{portalId}", new { controller = "taxclasses", action = "getactivetaxclassbyportalid" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("taxclass-create", "taxclasses", new { controller = "taxclasses", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("taxclass-update", "taxclasses/{taxClassId}", new { controller = "taxclasses", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("taxclass-delete", "taxclasses/{taxClassId}", new { controller = "taxclasses", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });
            config.Routes.MapHttpRoute("taxclassdetails-list", "taxclassdetails", new { controller = "taxclasses", action = "taxclassdetails" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("taxclasses-list", "taxclasslist", new { controller = "taxclasses", action = "taxclasslist" }, new { httpMethod = new HttpMethodConstraint("GET") });

            // Tax rule routes
            config.Routes.MapHttpRoute("taxrule-get", "taxrules/{taxRuleId}", new { controller = "taxrules", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("taxrule-list", "taxrules", new { controller = "taxrules", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("taxrule-create", "taxrules", new { controller = "taxrules", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("taxrule-update", "taxrules/{taxRuleId}", new { controller = "taxrules", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("taxrule-delete", "taxrules/{taxRuleId}", new { controller = "taxrules", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });


            // Tax rule type routes
            config.Routes.MapHttpRoute("taxruletype-get", "taxruletypes/{taxRuleTypeId}", new { controller = "taxruletypes", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("taxruletype-list", "taxruletypes", new { controller = "taxruletypes", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("taxruletype-create", "taxruletypes", new { controller = "taxruletypes", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("taxruletype-update", "taxruletypes/{taxRuleTypeId}", new { controller = "taxruletypes", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("taxruletype-delete", "taxruletypes/{taxRuleTypeId}", new { controller = "taxruletypes", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });
            config.Routes.MapHttpRoute("taxruletype-getalltaxruletypesnotindatabase", "getalltaxruletypesnotindatabase", new { controller = "taxruletypes", action = "getalltaxruletypesnotindatabase" }, new { httpMethod = new HttpMethodConstraint("GET") });

            //Theme routes
            config.Routes.MapHttpRoute("theme-get", "themes/{themeId}", new { controller = "theme", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("theme-list", "themes", new { controller = "theme", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("theme-create", "themes", new { controller = "theme", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("theme-update", "themes/{themeId}", new { controller = "theme", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("theme-delete", "themes/{themeId}", new { controller = "theme", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            // Url Redirect routes
            config.Routes.MapHttpRoute("urlredirect-get", "urlredirects/{urlRedirectId}", new { controller = "urlredirects", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("urlredirect-list", "urlredirects", new { controller = "urlredirects", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("urlredirect-create", "urlredirects", new { controller = "urlredirects", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("urlredirect-update", "urlredirects/{urlRedirectId}", new { controller = "urlredirects", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("urlredirect-delete", "urlredirects/{urlRedirectId}", new { controller = "urlredirects", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            //Update Image
            config.Routes.MapHttpRoute("image-update", "image", new { controller = "products", action = "UpdateImage" }, new { httpMethod = new HttpMethodConstraint("PUT") });

            //Vendor Account routes
            config.Routes.MapHttpRoute("vendoraccounts-list", "vendoraccounts", new { controller = "vendoraccounts", action = "GetVendorAccountList" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("vendoraccounts-create", "vendoraccounts", new { controller = "accounts", action = "createvendoraccount" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("vendoraccount-update", "vendoraccount/{accountId}", new { controller = "Accounts", action = "UpdateVendorAccount" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("vendoraccount-getvendoraccountbyid", "vendoraccount/getvendoraccountbyid/{accountId}", new { controller = "vendoraccounts", action = "getvendoraccountbyid" }, new { httpMethod = new HttpMethodConstraint("GET") });

            //Vendor Product routes
            config.Routes.MapHttpRoute("vendorproducts-list", "vendorproducts", new { controller = "vendorproducts", action = "getvendorproductlist" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("vendorproducts-changeproductstatus", "vendorproducts/changeproductstatus/{state}", new { controller = "vendorproducts", action = "changeproductstatus" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("vendorproducts-bindrejectproduct", "vendorproducts/bindrejectproduct/{productIds}", new { controller = "vendorproducts", action = "bindrejectproduct" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("vendorproducts-reviewimagelist", "vendorproducts/reviewimagelist", new { controller = "vendorproducts", action = "getreviewimagelist" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("vendorproducts-updateproductimagestatus", "vendorproducts/updateproductimagestatus/{productIds}/{state}", new { controller = "vendorproducts", action = "updateproductimagestatus" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("vendorproducts-update", "vendorproducts/{productId}", new { controller = "vendorproducts", action = "updateproduct" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("vendorproducts-updatemarketingdetails", "vendorproducts/updatemarketingdetails/{productId}", new { controller = "vendorproducts", action = "updatemarketingdetails" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("vendorproducts-getcategorynode", "vendorproducts/getcategorynode/{productId}", new { controller = "vendorproducts", action = "getcategorynode" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("vendorproducts-delete", "vendorproducts/{productId}", new { controller = "vendorproducts", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });
            config.Routes.MapHttpRoute("malladminproduct-list", "malladminproducts", new { controller = "vendorproducts", action = "getmalladminproductlist" }, new { httpMethod = new HttpMethodConstraint("GET") });

            //Vendor Product History routes
            config.Routes.MapHttpRoute("productshistory-list", "productshistory", new { controller = "productreviewhistory", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("productshistory-get", "productshistory/{productReviewHistoryID}", new { controller = "productreviewhistory", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });

            //Vendor Franchise Account routes 
            config.Routes.MapHttpRoute("franchiseaccounts-create", "franchiseadmin", new { controller = "accounts", action = "createfranchiseaccount" }, new { httpMethod = new HttpMethodConstraint("POST") });

            // Wishlist routes
            config.Routes.MapHttpRoute("wishlist-get", "wishlists/{wishListId}", new { controller = "wishlists", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("wishlist-list", "wishlists", new { controller = "wishlists", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("wishlist-create", "wishlists", new { controller = "wishlists", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("wishlist-update", "wishlists/{wishListId}", new { controller = "wishlists", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("wishlist-delete", "wishlists/{wishListId}", new { controller = "wishlists", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            // ZipCode routes
            config.Routes.MapHttpRoute("zipcode-list", "zipcodes", new { controller = "zipcodes", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });

            // Credit Application routes
            config.Routes.MapHttpRoute("creditapplication-get", "creditapplication/{creditApplicationId}", new { controller = "PRFTCreditApplication", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("creditapplication-list", "creditapplication", new { controller = "PRFTCreditApplication", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("creditapplication-create", "creditapplication", new { controller = "PRFTCreditApplication", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("creditapplication-update", "creditapplication/{creditApplicationId}", new { controller = "PRFTCreditApplication", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            //config.Routes.MapHttpRoute("caserequest-delete", "caserequests/{caseRequestId}", new { controller = "caserequests", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            //Customer Import
            config.Routes.MapHttpRoute("Import-customerimport", "importcustomer/createuser", new { controller = "PRFTCustomerImport", action = "CreateUser" }, new { httpMethod = new HttpMethodConstraint("GET") });

            //Product Inventory
            config.Routes.MapHttpRoute("inventoryfromerp-list", "products/getinventoryfromerp", new { controller = "products", action = "GetInventoryFromErp" }, new { httpMethod = new HttpMethodConstraint("GET") });

            //PRFT Custom Code :  Start
            //CustomerUserMapping Routes            
            config.Routes.MapHttpRoute("getAssociatedSuperUser-list", "getAssociatedSuperUser/{accountId}", new { controller = "customer", action = "getAssociatedSuperUser" }, new { httpMethod = new HttpMethodConstraint("GET") });            
            config.Routes.MapHttpRoute("customerusermapping-delete", "customerusermapping/{customerusermappingId}", new { controller = "customerusermapping", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });
            config.Routes.MapHttpRoute("getNotAssociatedSuperUser-list", "getNotAssociatedSuperUser/{accountId}", new { controller = "customer", action = "getNotAssociatedSuperUser" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("customerusermapping-insert", "customerusermapping/insertuserassociatedcustomer/{accountId}/{customerAccountIds}", new { controller = "customerusermapping", action = "insertuserassociatedcustomer" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("getSubUser-list", "getSubUser/{parentAccountId}", new { controller = "customerusermapping", action = "GetSubUserList" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("customerusermapping-deleteallmapping", "customerusermapping/deleteallusermapping/{userAccountId}", new { controller = "customerusermapping", action = "deleteallusermapping" }, new { httpMethod = new HttpMethodConstraint("DELETE") });
            //ERPAccountDetails Routes
            config.Routes.MapHttpRoute("getaccountdetailsfromerp-list", "accounts/getaccountdetailsfromerp/{externalId}", new { controller = "accounts", action = "GetAccountDetailsFromERP" }, new { http = new HttpMethodConstraint("GET") });

            //ERP Product and Inventory Routes
            config.Routes.MapHttpRoute("ERP-geterpitemdetails", "geterpitemdetails/{productnum}/{associatedCustomerExternalId}/{customertype}", new { controller = "PRFTERPService", action = "GetERPItemDetails" }, new { httpMethod = new HttpMethodConstraint("GET"), associatedCustomerExternalId = @"^[a-zA-Z0-9_ -]*$" });
            config.Routes.MapHttpRoute("ERP-getinventorydetails", "getinventorydetails/{productnum}", new { controller = "PRFTERPService", action = "GetInventoryDetails" }, new { httpMethod = new HttpMethodConstraint("GET")});

            config.Routes.MapHttpRoute("order-getorderreceiptjs", "orders/getorderreceiptjavascript", new { controller = "orders", action = "GetOrderReceiptJavascript" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("order-submitordertoerp", "orders/resubmitordertoerp/{orderId}", new { controller = "orders", action = "ReSubmitOrderToERP" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("order-getinvoicedetails", "orders/getinvoicedetails/{externalId}", new { controller = "orders", action = "getinvoicedetails" }, new { httpMethod = new HttpMethodConstraint("GET") });

            //UpdatePriceAndInventoryToZnode Routes
            config.Routes.MapHttpRoute("updatepriceandinventory", "updateerpdetails/updatepriceandinventory", new { controller = "PRFTUpdatePriceAndInventory", action = "UpdatePriceAndInventory" }, new { httpMethod = new HttpMethodConstraint("GET") });

            //Create Google Feed through direct API call
            //Google site map routes
            config.Routes.MapHttpRoute("googlesitemap-createwithget", "googlesitemap/createforschedular/{rootTag}/{portalIDs}", new { controller = "googlesitemap", action = "createforschedular" }, new { httpMethod = new HttpMethodConstraint("GET") });
            //PRFT Custom Code :  End
        }
    }
}
