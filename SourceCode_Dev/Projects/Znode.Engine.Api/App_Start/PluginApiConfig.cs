using System.Web.Http;
using Microsoft.Practices.EnterpriseLibrary.Common.Utility;
using Znode.Plugin;
using Znode.Plugin.Framework;

namespace Znode.Engine.Api.App_Start
{
    /// <summary>
    /// PluginApiConfig for routes
    /// </summary>
    public class PluginApiConfig
    {
        /// <summary>
        /// Register plugin routes
        /// </summary>
        /// <param name="config"></param>
        public void Register(HttpConfiguration config)
        {
            foreach (var plugin in PluginManager<IApiPlugin>.Current.GetPlugins())
            {
                plugin.RegisterRoutes(config);
            }
        }
    }
}