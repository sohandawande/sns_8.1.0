﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Web;

namespace Znode.Engine.Api.Parser
{
	public class QueryStringParser
	{
		private readonly string _queryString;

		public NameValueCollection Expands
		{
			get { return GetKeyValuePairs("expand"); }
		}

		public List<Tuple<string, string, string>> Filters
		{
			get { return GetTuples("filter"); }
		}

		public NameValueCollection Sorts
		{
			get { return GetKeyValuePairs("sort"); }
		}

		public NameValueCollection Page
		{
			get { return GetKeyValuePairs("page"); }
		}

		public NameValueCollection Format
		{
			get { return GetKeyValuePairs("format"); }
		}

		public NameValueCollection Indent
		{
			get { return GetKeyValuePairs("indent"); }
		}

		public NameValueCollection Cache
		{
			get { return GetKeyValuePairs("cache"); }
		}

		public QueryStringParser(string queryString)
		{
			_queryString = queryString.ToLower();
		}

		private NameValueCollection GetKeyValuePairs(string param)
		{
			var kvps = new NameValueCollection();
			var query = HttpUtility.ParseQueryString(_queryString);

			var uriItemSeparator = ConfigurationManager.AppSettings["UriItemSeparator"];
			var uriKeyValueSeparator = ConfigurationManager.AppSettings["UriKeyValueSeparator"];

			foreach (var key in query.AllKeys)
			{
				if (key.ToLower() == param)
				{
					var value = query.Get(key);
					var items = value.Split(uriItemSeparator.ToCharArray());

					foreach (var item in items)
					{
						if (item.Contains(uriKeyValueSeparator))
						{
							var set = item.Split(uriKeyValueSeparator.ToCharArray());
							kvps.Add(set[0].ToLower(), HttpUtility.HtmlDecode(set[1]));
						}
						else
						{
							// Just make the value the same as the key, for consistency of code in other places
							kvps.Add(item.ToLower(), item.ToLower());
						}
					}

					break;
				}
			}

			return kvps;
		}

		private List<Tuple<string, string, string>> GetTuples(string param)
		{
			var filters = new List<Tuple<string, string, string>>();
			var query = HttpUtility.ParseQueryString(_queryString);

			var uriItemSeparator = ConfigurationManager.AppSettings["UriItemSeparator"];
			var uriKeyValueSeparator = ConfigurationManager.AppSettings["UriKeyValueSeparator"];

			foreach (var key in query.AllKeys)
			{
				if (key.ToLower() == param)
				{
					var value = query.Get(key);
					var items = value.Split(uriItemSeparator.ToCharArray());

					foreach (var item in items)
					{
						if (item.Contains(uriKeyValueSeparator))
						{
							var tuple = item.Split(uriKeyValueSeparator.ToCharArray());
							var filterKey = tuple[0].ToLower();
							var filterOperator = tuple[1].ToLower();
							var filterValue = tuple[2].ToLower();

							filters.Add(new Tuple<string, string, string>(filterKey, filterOperator, HttpUtility.HtmlDecode(filterValue)));
						}
					}

					break;
				}
			}

			return filters;
		}
	}
}