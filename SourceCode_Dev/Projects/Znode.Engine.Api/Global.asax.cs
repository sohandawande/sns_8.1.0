﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.SessionState;
using Newtonsoft.Json;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;
using Znode.Engine.Api.App_Start;
using Znode.Engine.Api.Cache;
using Znode.Engine.Promotions;
using Znode.Engine.Services;
using Znode.Engine.Shipping;
using Znode.Engine.Suppliers;
using Znode.Engine.Taxes;
using WebMatrix.WebData;

namespace Znode.Engine.Api
{
	public class WebApiApplication : HttpApplication
	{
		protected void Application_Start()
		{
			AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            // callling non-static methods, so we need an instance of the class
            (new PluginApiConfig()).Register(GlobalConfiguration.Configuration);

			FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            FilterConfig.RegisterHttpFilters(GlobalConfiguration.Configuration.Filters);
			RouteConfig.RegisterRoutes(RouteTable.Routes);
			BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterAuth();
            //Znode Version 8.0 Method initialize the database connection for web security.
            WebSecurity.InitializeDatabaseConnection("ZNodeECommerceDB", "webpages_Users", "UserId", "UserName", autoCreateTables: true);
			// Required to handle circular object references. For example, SkuModel contains InventoryModel and
			// InventoryModel contains a list of SkuModels. Doesn't hurt anything, so no danger here.
			var json = GlobalConfiguration.Configuration.Formatters.JsonFormatter;
			json.SerializerSettings.PreserveReferencesHandling = PreserveReferencesHandling.All;

            TryDatabaseConnection();
            TryCacheActivePromotions();
            TryCacheAvailablePromotionTypes();
            TryCacheAvailableShippingTypes();
            TryCacheAvailableSupplierTypes();
            TryCacheAvailableTaxTypes();
            TryLogApplicationStart();
		}

		protected void Application_BeginRequest(Object sender, EventArgs e)
		{
			SetSiteConfig();
            TryCacheCurrencySettings();
		}

		protected void Application_PostAuthorizeRequest()
		{
			HttpContext.Current.SetSessionStateBehavior(SessionStateBehavior.Required);
		}

		void Session_Start(object sender, EventArgs e)
		{
            string id = Session.SessionID;
		    ;
            var domainName = GetDomainNameFromAuthHeader();

			if (!String.IsNullOrEmpty(domainName))
			{
				if (ZNodeConfigManager.CheckSiteConfigCache(domainName))
				{
					var domain = ZNodeConfigManager.GetSiteConfigFromCache(domainName);
					if (domain != null)
					{
						HttpContext.Current.Session.Add(ZNodeConfigManager.AliasedConfigSessionKey, domain);
					}
				}
			}
		}

		private void TryDatabaseConnection()
		{
			try
			{
				var cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString);
				cnn.Open();
				cnn.Close();
			}
			catch (Exception ex)
			{
				ZNodeLoggingBase.LogMessage("Could not connect to the database. Additional information: " + ex.Message);
				throw new ApplicationException("Could not connect to the database. Additional information: " + ex.Message);
			}
		}

		private void TryCacheActivePromotions()
		{
			try
			{
				ZnodePromotionManager.CacheActivePromotions();
			}
			catch (Exception ex)
			{
				ZNodeLoggingBase.LogMessage("Could not cache promotions. Additional information: " + ex.Message);
				throw new ApplicationException("Could not cache promotions. Additional information: " + ex.Message);
			}
		}

		private void TryCacheAvailablePromotionTypes()
		{
			try
			{
				ZnodePromotionManager.CacheAvailablePromotionTypes();
			}
			catch (Exception ex)
			{
				ZNodeLoggingBase.LogMessage("Could not cache promotion types. Additional information: " + ex.Message);
				throw new ApplicationException("Could not cache promotion types. Additional information: " + ex.Message);
			}
		}

		private void TryCacheAvailableShippingTypes()
		{
			try
			{
				ZnodeShippingManager.CacheAvailableShippingTypes();
			}
			catch (Exception ex)
			{
				ZNodeLoggingBase.LogMessage("Could not cache shipping types. Additional information: " + ex.Message);
				throw new ApplicationException("Could not cache shipping types. Additional information: " + ex.Message);
			}
		}

		private void TryCacheAvailableSupplierTypes()
		{
			try
			{
				ZnodeSupplierManager.CacheAvailableSupplierTypes();
			}
			catch (Exception ex)
			{
				ZNodeLoggingBase.LogMessage("Could not cache supplier types. Additional information: " + ex.Message);
				throw new ApplicationException("Could not cache supplier types. Additional information: " + ex.Message);
			}
		}

		private void TryCacheAvailableTaxTypes()
		{
			try
			{
				ZnodeTaxManager.CacheAvailableTaxTypes();
			}
			catch (Exception ex)
			{
				ZNodeLoggingBase.LogMessage("Could not cache tax types. Additional information: " + ex.Message);
				throw new ApplicationException("Could not cache tax types. Additional information: " + ex.Message);
			}
		}

		private void TryLogApplicationStart()
		{
			try
			{
				ZNodeLoggingBase.LogActivity(2000);
				ZNodeLoggingBase.LogMessage("Application Start");
			}
			catch (Exception ex)
			{
				ZNodeLoggingBase.LogMessage("Could not log the application start. Additional information: " + ex.Message);
				throw new ApplicationException("Could not log the application start. Additional information: " + ex.Message);
			}
		}

		private void TryCacheCurrencySettings()
		{
			try
			{
				ZNodeCurrencyManager.CacheCurrencySetting();
			}
			catch (Exception ex)
			{
				ZNodeLogging.LogMessage("Could not cache currency settings. Additional information: " + ex.Message);
				throw new ApplicationException("Could not cache currency settings. Additional information: " + ex.Message);
			}
		}

		private void SetSiteConfig()
		{
			var domainName = GetDomainNameFromAuthHeader();

			if (!String.IsNullOrEmpty(domainName))
			{
				if (!ZNodeConfigManager.CheckSiteConfigCache(domainName))
				{
					var cache = new DomainCache(new DomainService());
					var domainConfig = cache.GetDomain(domainName);

					if (domainConfig == null || !domainConfig.IsActive)
					{
						// The URL was not found in our config, send out a 404 error
						HttpContext.Current.Response.StatusCode = 404;
						HttpContext.Current.Response.SuppressContent = true;
						HttpContext.Current.ApplicationInstance.CompleteRequest();
						return;
					}

					ZNodeConfigManager.SetDomainConfig(domainName, domainConfig);

					var portalService = new PortalService();
					var portal = portalService.GetPortalById(domainConfig.PortalID);

					ZNodeConfigManager.SetSiteConfig(domainName, portal);
				}
			}
		}

		private string GetDomainNameFromAuthHeader()
		{
			var headers = HttpContext.Current.Request.Headers;
			var authValue = headers.AllKeys.Contains("Authorization") ? headers["Authorization"] : String.Empty;

			// If auth value doesn't exist, return empty
			if (String.IsNullOrEmpty(authValue)) return String.Empty;

			// Strip off the "Basic "
		    authValue = authValue.Remove(0, 6);

			// Decode it; if empty return empty
			var authValueDecoded = DecodeBase64(authValue);
			if (String.IsNullOrEmpty(authValueDecoded)) return String.Empty;

			// Now split it to get the domain info (index 0 = domain name, index 1 = domain key)
			var domainInfo = authValueDecoded.Split('|');

			return !String.IsNullOrEmpty(domainInfo[0]) ? domainInfo[0] : String.Empty;
		}

		private string DecodeBase64(string encodedValue)
		{
			var encodedValueAsBytes = Convert.FromBase64String(encodedValue);
			return Encoding.UTF8.GetString(encodedValueAsBytes);
		}
	}
}