using System;
using System.Collections.Generic;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Entities;
using Znode.Engine.Promotions;
using Znode.Engine.Taxes;

namespace ZNode.Libraries.ECommerce.ShoppingCart
{
    /// <summary>
    /// Represents a product items in the shopping cart
    /// </summary>
    [Serializable()]
    public class ZNodeShoppingCartItem : ZNode.Libraries.ECommerce.Entities.ZNodeShoppingCartItem
    {
        #region Public Properties
        /// <summary>
        /// Gets or sets the product object for this cart item
        /// </summary>        
        public new ZNodeProductBase Product
        {
            get { return (ZNode.Libraries.ECommerce.Catalog.ZNodeProductBase)base.Product; }
            set { base.Product = value; }
        }

        /// <summary>
        /// Gets the unit price of this line item.
        /// </summary>        
        public new decimal UnitPrice
        {
            get
            {
	            var pricePromoManager = new ZnodePricePromotionManager();

                decimal basePrice = pricePromoManager.PromotionalPrice(this.Product, TieredPricing);

                if (basePrice > 0)
                {
                    // Calculate sales tax on discounted price.
                    ZnodeInclusiveTax salesTax = new ZnodeInclusiveTax();
					basePrice = salesTax.GetInclusivePrice(Product.TaxClassID, basePrice, Product.AddressToShip);
                }

                basePrice = basePrice + Product.AddOnPrice;

                for (int idx = 0; idx < Product.ZNodeBundleProductCollection.Count; idx++)
                {
                    basePrice += Product.ZNodeBundleProductCollection[idx].AddOnPrice;
                }

                return Math.Round(basePrice, 2);
            }
        }

        /// <summary>
        /// Gets or sets the product extended price
        /// </summary>       
        public new decimal ExtendedPrice
        {
            get
            {
                this._ExtendedPrice = this.UnitPrice * Quantity;

                return Math.Round( this._ExtendedPrice,2);
            }

            set
            {
                this._ExtendedPrice = value;
            }
        }

		/// <summary>
		/// Unique identifier for external integration.
		/// </summary>
		public string ExternalId { get; set; }
		
		public int ShippingOptionId { get; set; }

		/// <summary>
		/// Sku Attribute Ids associated with this item.
		/// </summary>
		public int[] AttributeIds { get; set; }

		public int[] AddOnValueIds { get; set; }

		public Dictionary<int, string> AddOnValuesCustomText { get; set; }

        #endregion

        public ZNodeShoppingCartItem()
        {
            ExternalId = Guid.NewGuid().ToString();            
        }

		public new ZNodeShoppingCartItem Clone()
		{
			var copiedItem = this.MemberwiseClone() as ZNodeShoppingCartItem;
			copiedItem.Product = this.Product.Clone();
			return copiedItem;
		}
    }
}
