using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using System.Xml.Serialization;
using ZNode.Libraries.ECommerce.Catalog;
using Znode.Engine.Promotions;
using Znode.Engine.Shipping;
using Znode.Engine.Taxes;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.ShoppingCart
{
    /// <summary>
    /// Represents Shopping cart and shopping cart items
    /// </summary>
    [Serializable()]
    public class ZNodeShoppingCart : ZNode.Libraries.ECommerce.Entities.ZNodeShoppingCart
    {
        private SavedCartLineItemService _cartLineItemService = new SavedCartLineItemService();

        #region Znode Version 7.2.2 Private Variables

        private Dictionary<string, int> SKUQuantity;
        private const string AccountCustom2Value = "1";

        #endregion

        #region Public Properties
        List<ZNodePortalCart> portalCarts = new List<ZNodePortalCart>();

        /// <summary>
        /// Get Portal based cart items.
        /// </summary>
        public List<ZNodePortalCart> PortalCarts
        {
            get
            {
                if (!portalCarts.Any() ||
                    portalCarts.Sum(x => x.ShoppingCartItems.Count) != this.ShoppingCartItems.Count
                    ||
                    portalCarts.Sum(x => x.ShoppingCartItems.Cast<ZNodeShoppingCartItem>().Sum(s => s.Quantity)) !=
                    this.ShoppingCartItems.Cast<ZNodeShoppingCartItem>().Sum(s => s.Quantity))
                {
                    portalCarts =
                        this.ShoppingCartItems.Cast<ZNodeShoppingCartItem>()
                            .Select(x => x.Product.PortalID > 0 ? x.Product.PortalID : ZNodeConfigManager.SiteConfig.PortalID)
                            .Distinct()
                            .Select(x => new ZNodePortalCart()
                            {
                                PortalID = x,
                                ShoppingCartItems = GetAddressCartItems(x)
                            }).ToList();
                }

                var portalCart = portalCarts.FirstOrDefault();

                if (portalCart != null && this.Coupons != null)
                {
                    for (int couponIndex = 0; couponIndex < this.Coupons.Count; couponIndex++)
                    {
                        if (portalCart.Coupons.Count > 0 && portalCart.Coupons[couponIndex].Coupon != this.Coupons[couponIndex].Coupon)
                        {
                            if (!string.IsNullOrEmpty(this.Coupons[couponIndex].Coupon))
                            {
                                portalCart.AddCouponCode(this.Coupons[couponIndex].Coupon);
                            }
                            else
                            {
                                portalCart.AddCouponCode(string.Empty);
                            }
                        }
                    }
                }

                return portalCarts;
            }
        }

        private ZNodeGenericCollection<ZNodeShoppingCartItem> GetAddressCartItems(int x)
        {
            var items = this.ShoppingCartItems.Cast<ZNodeShoppingCartItem>().Where(y => (y.Product.PortalID > 0 ? y.Product.PortalID : ZNodeConfigManager.SiteConfig.PortalID) == x).ToList();
            var returnItems = new ZNodeGenericCollection<ZNodeShoppingCartItem>();
            items.ForEach(y => returnItems.Add(y));
            return returnItems;
        }

        /// <summary>
        /// Gets or sets the Shoppingcart Items
        /// </summary>
        [XmlIgnore()]
        public new ZNodeGenericCollection<ZNodeShoppingCartItem> ShoppingCartItems
        {
            get
            {
                ZNodeGenericCollection<ZNodeShoppingCartItem> _cartItems = new ZNodeGenericCollection<ZNodeShoppingCartItem>();

                foreach (ZNode.Libraries.ECommerce.Entities.ZNodeShoppingCartItem item in base.ShoppingCartItems)
                {
                    _cartItems.Add((ZNodeShoppingCartItem)item);
                }

                return _cartItems;
            }

            set
            {
                foreach (ZNodeShoppingCartItem item in value)
                {
                    base.ShoppingCartItems.Add(item);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool IsMultipleShipToAddress
        {
            get
            {
                return PortalCarts.SelectMany(x => x.AddressCarts).Count() > PortalCarts.Count;
            }
        }

        /// <summary>
        /// Gets or sets the Shoppingcart Items
        /// </summary>
        [XmlIgnore()]
        public ZNodeGenericCollection<ZNodeShoppingCartItem> SplittedShoppingCartItems
        {
            get
            {
                ZNodeGenericCollection<ZNodeShoppingCartItem> _cartItems = new ZNodeGenericCollection<ZNodeShoppingCartItem>();

                foreach (ZNode.Libraries.ECommerce.Entities.ZNodeShoppingCartItem item in ShoppingCartItems)
                {
                    int i = 0;
                    while (i < item.Quantity)
                    {
                        _cartItems.Add((ZNodeShoppingCartItem)item);
                        i++;
                    }
                }

                return _cartItems;
            }
        }

        /// <summary>
        /// Gets total cost of items in the shopping cart before shipping and taxes
        /// </summary>
        public override decimal SubTotal
        {
            get
            {
                decimal subTotal = 0;

                foreach (ZNodeShoppingCartItem item in ShoppingCartItems)
                {
                    //PRFT Custom Code: Start
                    //decimal itemTotal = item.ExtendedPrice;
                    decimal itemTotal = item.ERPExtendedPrice;
                    //PRFT Custom Code: End
                    subTotal += itemTotal;
                }

                return Math.Round(subTotal, 2);
            }
        }

        /// <summary>
        /// Gets the total discount of applied to the items in the shopping cart.
        /// </summary>
        public override decimal Discount
        {
            get
            {
                decimal subTotal = this.SubTotal;

                decimal totalDiscount = OrderLevelDiscount;

                foreach (ZNodeShoppingCartItem item in ShoppingCartItems)
                {
                    totalDiscount += item.DiscountAmount + item.ExtendedPriceDiscount;
                }

                if (totalDiscount > subTotal)
                {
                    return Math.Round(subTotal, 2);
                }

                return Math.Round(totalDiscount, 2);
            }
        }

        /// <summary>
        /// Gets the Total cost after shipping, taxes and promotions
        /// </summary>        
        public override decimal Total
        {
            get
            {
                decimal total = 0;

                total = (this.SubTotal - this.Discount) + ShippingCost + OrderLevelTaxes - GiftCardAmount;

                return total;
            }
        }

        public int? ProfileId { get; set; }

        //public string CustomerExternalId { get; set; }
        #endregion

        #region static methods
        /// <summary>
        /// Returns the current shopping cart
        /// </summary>
        /// <returns>Returns the current shopping cart details</returns>
        public static new ZNodeShoppingCart CurrentShoppingCart()
        {
            ZNodeShoppingCart shoppingCart;

            // get the user account from session
            shoppingCart = (ZNodeShoppingCart)HttpContext.Current.Session[ZNodeSessionKeyType.ShoppingCart.ToString()];

            // not in session
            if (shoppingCart == null)
            {
                return null;
            }
            else
            {
                // return the value from session
                return shoppingCart;
            }
        }

        /// <summary>
        /// Returns the current shopping cart
        /// </summary>
        /// <param name="portalId">current Portal Id</param>
        /// <returns>Retuens the current shopping cart based on the portal id</returns>
        public static ZNodeShoppingCart CurrentShoppingCart(int portalId)
        {
            ZNodeShoppingCart shoppingCart;

            // get the user account from session
            shoppingCart = (ZNodeShoppingCart)HttpContext.Current.Session[string.Format("{0}_{1}", ZNodeSessionKeyType.ShoppingCart.ToString(), portalId)];

            // not in session
            if (shoppingCart == null)
            {
                return null;
            }
            else
            {
                // return the value from session
                return shoppingCart;
            }
        }

        public static ZNodeShoppingCart LoadFromDatabase(int accountId, int portalId)
        {
            var accountService = new AccountService();


            var account = accountService.DeepLoadByAccountID(accountId, true, DeepLoadType.IncludeChildren, typeof(List<CookieMapping>));

            var cookieMappings = account.CookieMappingCollection.Where(mapping => mapping.PortalID.Equals(portalId));

            if (!cookieMappings.Any()) return null;

            var cookieMapping = cookieMappings.First();

            var savedCartService = new SavedCartService();
            var savedCart = savedCartService.GetByCookieMappingID(cookieMapping.CookieMappingID);

            // it is a valid scenario to have a cookie mapping without a saved cart
            if (!savedCart.Any()) return null;

            var savedCartId = savedCart.First().SavedCartID;

            ZNodeSavedCart zNodeSavedCart = new ZNodeSavedCart(false);
            zNodeSavedCart.SetCookieMapping(cookieMapping);


            var cartLineItemService = new SavedCartLineItemService();
            var cartLineItems = cartLineItemService.GetBySavedCartID(savedCartId);

            foreach (var savedCartLineItem in cartLineItems.Where(lineItem => lineItem.ParentSavedCartLineItemID == null))
            {
                zNodeSavedCart.AddtoShoppingCart(savedCartLineItem);
            }
            return CurrentShoppingCart();
        }
        /// <summary>
        /// Loads the Cart using cookie Mapping Id 
        /// </summary>
        /// <param name="cookieMappingId"></param>
        /// <returns></returns>
        public static ZNodeShoppingCart LoadFromDatabase(int cookieMappingId)
        {
            var cookieService = new CookieMappingService();
            var cookieMapping = cookieService.GetByCookieMappingID(cookieMappingId);

            var savedCartService = new SavedCartService();
            var savedCart = savedCartService.GetByCookieMappingID(cookieMappingId);

            // it is a valid scenario to have a cookie mapping without a saved cart
            if (!savedCart.Any()) return null;

            var savedCartId = savedCart.First().SavedCartID;

            ZNodeSavedCart zNodeSavedCart = new ZNodeSavedCart(false);
            zNodeSavedCart.SetCookieMapping(cookieMapping);


            var cartLineItemService = new SavedCartLineItemService();
            var cartLineItems = cartLineItemService.GetBySavedCartID(savedCartId);

            foreach (var savedCartLineItem in cartLineItems.Where(lineItem => lineItem.ParentSavedCartLineItemID == null))
            {
                zNodeSavedCart.AddtoShoppingCart(savedCartLineItem);

            }
            return CurrentShoppingCart();
        }
        #endregion

        #region Public and Private Methods
        /// <summary>
        /// Returns the current shopping cart
        /// </summary>
        /// <param name="portalId">current Portal Id</param>
        /// <returns>Retuens the current shopping cart based on the portal id</returns>
        public ZNodeShoppingCart AddShoppingCartToSession(int portalId)
        {
            ZNodeShoppingCart shoppingCart = this;

            // get the user account from session
            HttpContext.Current.Session[string.Format("{0}_{1}", ZNodeSessionKeyType.ShoppingCart.ToString(), portalId)] = shoppingCart;

            // not in session
            if (shoppingCart == null)
            {
                return null;
            }
            else
            {
                // return the value from session
                return shoppingCart;
            }
        }

        /// <summary>
        /// Represents the current session key count
        /// </summary>
        public void Test()
        {
            HttpContext.Current.Response.Write(HttpContext.Current.Session.Keys.Count.ToString());
        }

        /// <summary>
        /// Calculates final pricing, shipping and taxes in the cart.
        /// </summary>
        public virtual void Calculate()
        {
            // Pass profile ID as null to the overload
            Calculate(null);
        }

        public virtual void Calculate(int? profileId)
        {
            // Clear previous error message
            this.ClearPreviousErrorMessges();
            bool isCouponAvailable = this.Coupons.Count > 0;

            List<KeyValuePair<int, decimal>> productDiscount = new List<KeyValuePair<int, decimal>>();
            // ShippingRules
            var shippingManager = new ZnodeShippingManager(this);
            shippingManager.Calculate();
            foreach (ZNodeShoppingCartItem item in this.ShoppingCartItems)
            {
                if (item.Product.DiscountAmount > 0.0M)
                {
                    productDiscount.Add(new KeyValuePair<int, decimal>(item.Product.ProductID, item.Product.DiscountAmount));
                }
                foreach (var addOnValueEntity in item.Product.SelectedAddOns.AddOnCollection.Cast<ZNodeAddOnEntity>()
                                                                 .SelectMany(addOnEntity => addOnEntity.AddOnValueCollection.Cast<ZNodeAddOnValueEntity>()))
                {
                    if (addOnValueEntity.DiscountAmount > 0.0M)
                    {
                        productDiscount.Add(new KeyValuePair<int, decimal>(addOnValueEntity.AddOnValueID, addOnValueEntity.DiscountAmount));
                    }
                }
            }
            // Promotions
            var cartPromoManager = new ZnodeCartPromotionManager(this, profileId);
            cartPromoManager.Calculate();
            if (isCouponAvailable || this.IsAnyPromotionApplied)
            {
                productDiscount.Clear();
                foreach (ZNodeShoppingCartItem item in this.ShoppingCartItems)
                {
                    if (item.Product.DiscountAmount > 0.0M)
                    {
                        productDiscount.Add(new KeyValuePair<int, decimal>(item.Product.ProductID, item.Product.DiscountAmount));
                    }
                    foreach (var addOnValueEntity in item.Product.SelectedAddOns.AddOnCollection.Cast<ZNodeAddOnEntity>()
                                                                     .SelectMany(addOnEntity => addOnEntity.AddOnValueCollection.Cast<ZNodeAddOnValueEntity>()))
                    {
                        if (addOnValueEntity.DiscountAmount > 0.0M)
                        {
                            productDiscount.Add(new KeyValuePair<int, decimal>(addOnValueEntity.AddOnValueID, addOnValueEntity.DiscountAmount));
                        }
                    }
                }
            }
            foreach (KeyValuePair<int, decimal> item in productDiscount)
            {
                foreach (ZNodeShoppingCartItem cartItem in this.ShoppingCartItems)
                {
                    if (Equals(item.Key, cartItem.Product.ProductID) && item.Value > 0.0M)
                    {
                        cartItem.Product.DiscountAmount = item.Value;
                    }
                }
            }
            foreach (KeyValuePair<int, decimal> item in productDiscount)
            {
                foreach (ZNodeShoppingCartItem cartItem in this.ShoppingCartItems)
                {
                    foreach (var addOnValueEntity in cartItem.Product.SelectedAddOns.AddOnCollection.Cast<ZNodeAddOnEntity>()
                                         .SelectMany(addOnEntity => addOnEntity.AddOnValueCollection.Cast<ZNodeAddOnValueEntity>()))
                    {
                        if (Equals(item.Key, addOnValueEntity.AddOnValueID) && item.Value > 0.0M)
                        { addOnValueEntity.DiscountAmount = item.Value; }
                    }
                }
            }
            // TaxRules
            var taxManager = new ZnodeTaxManager(this);
            taxManager.Calculate(this);

            productDiscount = null;

            this.GiftCardAmount = 0;

            if (this.GiftCardNumber != string.Empty)
            {
                this.AddGiftCard(this.GiftCardNumber);
            }
        }

        /// <summary>
        /// Saves the <see cref="ZNodeShoppingCart"/> instance to the database.
        /// </summary>
        /// <param name="accountId">AccountID to associate to the cart.</param>
        /// <param name="portalId">PortalId to associate to the cart.</param>
        /// <returns>Cookie Mapping Id</returns>
        public virtual int Save(int? accountId, int portalId, int cookieMappingId)
        {
            // the datetime seed to be used in case new entities need to be created.
            var newEntityDateTimeSeed = DateTime.Now;


            // find/create the cookie mapping entity. this acts as the core key to saved carts.
            var cookieMappingService = new CookieMappingService();
            var cookieMappings = cookieMappingService.GetByAccountID(accountId);

            if (!cookieMappings.Any() && accountId.HasValue)
            {
                var cookieMapping = cookieMappingService.GetByCookieMappingID(cookieMappingId);
                if (cookieMapping != null)
                {
                    cookieMapping.AccountID = accountId;
                    cookieMappingService.Update(cookieMapping);
                    cookieMappings = cookieMappingService.GetByAccountID(accountId);
                }
            }

            cookieMappings.Filter = string.Format("PortalID = {0}", portalId);

            if (!cookieMappings.Any())
            {
                var newCookie = cookieMappingService.Save(new CookieMapping()
                    {
                        AccountID = accountId,
                        CreatedDate = newEntityDateTimeSeed,
                        LastUpdatedDate = newEntityDateTimeSeed,
                        PortalID = portalId
                    });

                cookieMappings = new TList<CookieMapping>() { newCookie };
            }

            cookieMappingId = cookieMappings.First().CookieMappingID;

            // find/create the saved cart that is associated to the cookie cart.
            var savedCartService = new SavedCartService();
            var savedCart = savedCartService.GetByCookieMappingID(cookieMappingId);

            if (!savedCart.Any())
            {
                var newCart = savedCartService.Save(new SavedCart()
                    {
                        CookieMappingID = cookieMappingId,
                        CreatedDate = newEntityDateTimeSeed,
                        LastUpdatedDate = newEntityDateTimeSeed
                    });

                savedCart = new TList<SavedCart>() { newCart };
            }

            var savedCartId = savedCart.First().SavedCartID;

            var success = false;

            try
            {
                // update/create cart line items associated to the saved cart.
                success = SaveAllCartLineItems(savedCartId);
            }
            catch
            {
                // the saved cart line item service is excluded as each individual service commit has its own transaction.
                // this is here as nettiers has no facility for cross-service transactions.
                if (!success)
                {
                    if (savedCart.Any(cart => cart.CreatedDate.Equals(newEntityDateTimeSeed))) savedCartService.Delete(savedCartId);
                    if (cookieMappings.Any(mapping => mapping.CreatedDate.Equals(newEntityDateTimeSeed))) cookieMappingService.Delete(cookieMappingId);
                }

                throw;
            }

            return cookieMappingId;
        }

        private bool CheckIsNewItem(ZNodeShoppingCartItem shoppingCartItem, SavedCartLineItem parentItem, TList<SavedCartLineItem> lineItems, out List<SavedCartLineItem> existingSavedCartLineItems)
        {
            bool isNewItem = parentItem == null;
            existingSavedCartLineItems = null;
            if (parentItem != null && (shoppingCartItem.AddOnValueIds != null && shoppingCartItem.AddOnValueIds.Length > 0))
            {
                var existingItems =
                    lineItems.Where(item => item.ParentSavedCartLineItemID == parentItem.SavedCartLineItemID
                                            &&
                                            item.OrderLineItemRelationshipTypeID ==
                                            (int)ZNodeOrderLineItemRelationshipType.Addons);

                existingSavedCartLineItems = lineItems.Where(item => item.ParentSavedCartLineItemID == parentItem.SavedCartLineItemID
                                            &&
                                            item.OrderLineItemRelationshipTypeID ==
                                            (int)ZNodeOrderLineItemRelationshipType.Addons).ToList();

                if (existingItems != null && existingItems.Any())
                {
                    isNewItem = existingItems.All(x => shoppingCartItem.AddOnValueIds.Any(y => y == x.SKUID));
                }
            }

            if (parentItem != null && shoppingCartItem.Product.ZNodeBundleProductCollection != null)
            {
                var existingItems =
                    lineItems.Where(item => item.ParentSavedCartLineItemID == parentItem.SavedCartLineItemID
                                            &&
                                            item.OrderLineItemRelationshipTypeID ==
                                            (int)ZNodeOrderLineItemRelationshipType.ProductBundles);

                if (existingItems != null && existingItems.Any())
                {
                    isNewItem =
                        existingItems.All(
                            x =>
                            shoppingCartItem.Product.ZNodeBundleProductCollection.Cast<ZNodeProductBaseEntity>()
                                            .Any(y => y.SelectedSKUvalue.SKUID == x.SKUID));

                    foreach (ZNodeProductBaseEntity bundle in shoppingCartItem.Product.ZNodeBundleProductCollection)
                    {
                        if (bundle.SelectedAddOns != null && bundle.SelectedAddOns.SelectedAddOnValueIds.Length > 0)
                        {
                            var bundleParentItem =
                                lineItems.FirstOrDefault(
                                    item =>
                                    item.ParentSavedCartLineItemID == parentItem.SavedCartLineItemID &&
                                    item.SKUID == bundle.SelectedSKUvalue.SKUID);

                            var existingBAItems =
                                lineItems.Where(
                                    item => item.ParentSavedCartLineItemID == bundleParentItem.SavedCartLineItemID
                                            &&
                                            item.OrderLineItemRelationshipTypeID ==
                                            (int)ZNodeOrderLineItemRelationshipType.Addons) as
                                TList<SavedCartLineItem>;

                            if (existingBAItems != null && existingBAItems.Any())
                            {
                                var addOnIds = bundle.SelectedAddOns.SelectedAddOnValueIds.Split(',').Select(int.Parse);

                                isNewItem = existingBAItems.All(x => addOnIds.Any(y => y == x.SKUID));
                            }
                        }
                    }
                }
            }

            return isNewItem;
        }

        private bool SaveAllCartLineItems(int savedCartId)
        {

            //var cartLineItemService = new SavedCartLineItemService();
            var lineItems = _cartLineItemService.GetBySavedCartID(savedCartId);

            // manifest that contains new, updated and deleted cart line item entities for commit later in the method.
            var itemChangeManifest = new Collection<SavedCartLineItem>();

            foreach (ZNodeShoppingCartItem shoppingCartItem in ShoppingCartItems)
            {
                var itemSkuId = shoppingCartItem.Product.SelectedSKU.SKUID;

                var parentItem = lineItems.Except(itemChangeManifest).FirstOrDefault(item => item.SKUID.Equals(itemSkuId) && item.ParentSavedCartLineItemID == null);

                TList<SavedCartLineItem> existingLineItems = null;

                List<SavedCartLineItem> existingSavedCartLineItems = null;

                bool isNewItem = CheckIsNewItem(shoppingCartItem, parentItem, lineItems, out existingSavedCartLineItems);

                if (isNewItem)
                {
                    // add new parent line item
                    var newItem = new SavedCartLineItem()
                    {
                        SavedCartID = savedCartId,
                        SKUID = itemSkuId,
                        Quantity = shoppingCartItem.Quantity
                    };

                    parentItem = _cartLineItemService.Save(newItem);

                    if (parentItem == null)
                        throw new Exception("Unable to insert new shopping cart line item");

                    itemChangeManifest.Add(parentItem);
                }
                else
                {
                    parentItem.Quantity = shoppingCartItem.Quantity;
                    itemChangeManifest.Add(parentItem);

                    existingLineItems = lineItems.Where(item => item.ParentSavedCartLineItemID == parentItem.SavedCartLineItemID
                                && item.OrderLineItemRelationshipTypeID == (int)ZNodeOrderLineItemRelationshipType.Addons) as TList<SavedCartLineItem>;

                }

                //add or update any child line items if shopping cart item has selected add-on values.
                if (shoppingCartItem.Product.SelectedAddOns != null && shoppingCartItem.Product.SelectedAddOns.AddOnCollection.Count > 0)
                {
                    var addOnValueLineItemsToBeInsertedOrUpdated = GetAddOnValueCartLineItemsToBeInsertedOrUpdated(shoppingCartItem.Product, shoppingCartItem.Quantity, savedCartId, parentItem.SavedCartLineItemID, existingLineItems, existingSavedCartLineItems);

                    foreach (var addOnValueLineItem in addOnValueLineItemsToBeInsertedOrUpdated)
                    {
                        itemChangeManifest.Add(addOnValueLineItem);
                    }
                }


                //todo:  add code to persist any bundle line cart items here.
                foreach (ZNodeBundleProductEntity bundleProduct in shoppingCartItem.Product.ZNodeBundleProductCollection)
                {
                    var bundleItemSkuId = bundleProduct.SelectedSKUvalue.SKUID;
                    var buddleParentlineItem = lineItems.Except(itemChangeManifest).FirstOrDefault(item => item.SKUID.Equals(bundleItemSkuId) && item.ParentSavedCartLineItemID == parentItem.SavedCartLineItemID);
                    //buddleParentlineItem = savedCart.AddToSavedCart(bundleProduct.SelectedSKUvalue.SKUID, shoppingCartItem.Quantity, parentlineItem, (int)ZNodeOrderLineItemRelationshipType.ProductBundles);

                    if (buddleParentlineItem == null)
                    {
                        // add new parent line item
                        var newItem = new SavedCartLineItem()
                        {
                            SavedCartID = savedCartId,
                            SKUID = bundleItemSkuId,
                            Quantity = shoppingCartItem.Quantity,
                            ParentSavedCartLineItemID = parentItem.SavedCartLineItemID,
                            OrderLineItemRelationshipTypeID = (int)ZNodeOrderLineItemRelationshipType.ProductBundles
                        };

                        buddleParentlineItem = _cartLineItemService.Save(newItem);

                        if (buddleParentlineItem == null)
                            throw new Exception("Unable to insert new shopping cart bundle line item");

                        itemChangeManifest.Add(buddleParentlineItem);
                    }
                    else
                    {
                        buddleParentlineItem.Quantity = shoppingCartItem.Quantity;
                        itemChangeManifest.Add(buddleParentlineItem);

                        existingLineItems = lineItems.Where(item => item.ParentSavedCartLineItemID == buddleParentlineItem.SavedCartLineItemID
                                    && item.OrderLineItemRelationshipTypeID == (int)ZNodeOrderLineItemRelationshipType.Addons) as TList<SavedCartLineItem>;

                    }

                    //add or update any child line items if shopping cart item has selected add-on values.
                    if (bundleProduct.SelectedAddOns != null && bundleProduct.SelectedAddOns.AddOnCollection.Count > 0)
                    {
                        var addOnValueLineItemsToBeInsertedOrUpdated = GetAddOnValueCartLineItemsToBeInsertedOrUpdated(bundleProduct, shoppingCartItem.Quantity, savedCartId, buddleParentlineItem.SavedCartLineItemID, existingLineItems);

                        foreach (var addOnValueLineItem in addOnValueLineItemsToBeInsertedOrUpdated)
                        {
                            itemChangeManifest.Add(addOnValueLineItem);
                        }
                    }
                }
            }

            // mark any existing orphan line items in DB to be deleted.
            var dbCartLineItemsToBeDeleted = lineItems.Except(itemChangeManifest);

            foreach (var dbCartLineItemToBeDeleted in dbCartLineItemsToBeDeleted.Where(x => x.ParentSavedCartLineItemID != null).OrderByDescending(x => x.ParentSavedCartLineItemID))
            {
                dbCartLineItemToBeDeleted.MarkToDelete();
                itemChangeManifest.Add(dbCartLineItemToBeDeleted);
            }

            foreach (var dbCartLineItemToBeDeleted in dbCartLineItemsToBeDeleted.Where(x => x.ParentSavedCartLineItemID == null))
            {

                dbCartLineItemToBeDeleted.MarkToDelete();
                itemChangeManifest.Add(dbCartLineItemToBeDeleted);
            }

            var savedItems = _cartLineItemService.Save(new TList<SavedCartLineItem>(itemChangeManifest));

            return savedItems.Count().Equals(itemChangeManifest.Count);

        }

        private IEnumerable<SavedCartLineItem> GetAddOnValueCartLineItemsToBeInsertedOrUpdated(ZNodeProductBaseEntity product, int quantity, int savedCartId, int parentCartLineItemId, TList<SavedCartLineItem> existingAddOnValueLineItems = null, List<SavedCartLineItem> existingSavedCartLineItems = null)
        {

            var addOnValueLineItemsToBeInsertedOrUpdated = new Collection<SavedCartLineItem>();

            foreach (ZNodeAddOnEntity addOn in product.SelectedAddOns.AddOnCollection)
            {
                foreach (ZNodeAddOnValueEntity addOnValue in addOn.AddOnValueCollection)
                {
                    var foundExistingAddOnValueCartItem = existingAddOnValueLineItems == null ? null :
                        existingAddOnValueLineItems.FirstOrDefault(
                        cartItem => cartItem.OrderLineItemRelationshipTypeID == (int)ZNodeOrderLineItemRelationshipType.Addons
                                    && cartItem.SavedCartID == savedCartId
                                    && cartItem.SKUID == addOnValue.AddOnValueID);

                    var existingAddOnValueCartItem = existingSavedCartLineItems == null ? null :
                        existingSavedCartLineItems.FirstOrDefault(
                        cartItem => cartItem.OrderLineItemRelationshipTypeID == (int)ZNodeOrderLineItemRelationshipType.Addons
                                    && cartItem.SavedCartID == savedCartId
                                    && cartItem.SKUID == addOnValue.AddOnValueID);

                    if (Equals(foundExistingAddOnValueCartItem, null))
                    {
                        var newItemForAddOnValue = new SavedCartLineItem()
                        {
                            SavedCartID = savedCartId,
                            SKUID = addOnValue.AddOnValueID,
                            CustomeText = !string.IsNullOrEmpty(addOnValue.CustomText) ? addOnValue.CustomText : (!Equals(existingAddOnValueCartItem, null)) ? existingAddOnValueCartItem.CustomeText : string.Empty,
                            Quantity = quantity,
                            ParentSavedCartLineItemID = parentCartLineItemId,
                            OrderLineItemRelationshipTypeID = (int)ZNodeOrderLineItemRelationshipType.Addons
                        };
                        addOnValueLineItemsToBeInsertedOrUpdated.Add(newItemForAddOnValue);
                    }
                    else
                    {
                        foundExistingAddOnValueCartItem.Quantity = quantity;

                        addOnValueLineItemsToBeInsertedOrUpdated.Add(foundExistingAddOnValueCartItem);
                    }
                }
            }

            return addOnValueLineItemsToBeInsertedOrUpdated;

        }

        /// <summary>
        /// Process anything that must be done before the order is submitted.
        /// </summary>
        /// <returns>True if the order should be submitted. False if there is anything that will prevent the order from submitting correctly.</returns>
        public bool PreSubmitOrderProcess()
        {
            // Clear previous error message
            this.ClearPreviousErrorMessges();

            bool returnVal = true;

            // ShippingRules
            var shippingManager = new ZnodeShippingManager(this);
            returnVal &= shippingManager.PreSubmitOrderProcess();

            // TaxRules
            var taxManager = new ZnodeTaxManager(this);
            returnVal &= taxManager.PreSubmitOrderProcess(this);

            // Promotions
            var cartPromoManager = new ZnodeCartPromotionManager(this, null);
            returnVal &= cartPromoManager.PreSubmitOrderProcess();

            if (!Equals(this.Coupons, null))
            {
                foreach (ZNodeCoupon coupon in this.Coupons)
                {
                    returnVal &= this.IsCouponQuantityAvailable(coupon.Coupon);
                }
            }

            //Znode Version 7.2.2
            //To check Inventory of product is available - Start
            //This code will execute only if ShippingRules, TaxRules and Promotions are valid
            if (returnVal)
            {    
                //PRFT custom code : Start            
                var currentOrderAccount = UserAccount.ZNodeUserAccount.CurrentAccount();
                if (currentOrderAccount != null && currentOrderAccount.Custom2 == AccountCustom2Value)
                {
                    returnVal = true;
                }
                else
                {
                    returnVal = this.IsInventoryInStock();
                }
                //PRFT custom code : End 

                //returnVal = this.IsInventoryInStock();// PRFT commented code

            }
            //To check Inventory of product is available - End
            return returnVal;
        }

        /// <summary>
        /// Process anything that must be done after the order has been submitted.
        /// </summary>
        public void PostSubmitOrderProcess()
        {
            // ShippingRules
            var shippingManager = new ZnodeShippingManager(this);
            shippingManager.PostSubmitOrderProcess();

            // TaxRules
            var taxManager = new ZnodeTaxManager(this);
            taxManager.PostSubmitOrderProcess(this);

            // Promotions
            var cartPromoManager = new ZnodeCartPromotionManager(this, null);
            cartPromoManager.PostSubmitOrderProcess();

            var promotionService = new PromotionService();

            if (!Equals(this.Coupons, null))
            {
                foreach (ZNodeCoupon coupon in this.Coupons)
                {
                    if (coupon.CouponApplied)
                    {
                        var query = new PromotionQuery();
                        query.Append(PromotionColumn.CouponCode, coupon.Coupon);

                        // Find promotions with the coupon code
                        var promotions = promotionService.Find(query.GetParameters());

                        foreach (var promo in promotions)
                        {
                            // Reduce the quantity of available coupons
                            if (promo.CouponQuantityAvailable.GetValueOrDefault() > 0)
                            {
                                promo.CouponQuantityAvailable -= 1;
                                promotionService.Update(promo);
                            }
                        }
                    }
                }
            }

            // Reduce Inventory
            AddOnValueService AddOnValueServ = new AddOnValueService();
            ProductService productService = new ProductService();
            SKUService skuService = new SKUService();
            int quantity = 0;

            SKUInventoryService skuInventoryService = new SKUInventoryService();

            // Reduce the Product add-ons quantityAvailable by 1 ,if the order is placed successfully        
            // Loop through the Order Line Items
            foreach (ZNodeShoppingCartItem Item in ShoppingCartItems)
            {
                quantity = Item.Quantity;

                if (Item.Product.TrackInventoryInd)
                {
                    SKUInventory skuInventory = skuInventoryService.GetBySKU(Item.Product.SKU);

                    if (skuInventory != null)
                    {
                        // Subtract Order quantity from the inventory quantity
                        skuInventory.QuantityOnHand = skuInventory.QuantityOnHand - quantity;

                        // update into SKU table
                        skuInventoryService.Update(skuInventory);
                    }
                }

                // Loop through the Selected addons for each Shopping cartItem
                foreach (ZNodeAddOnEntity AddOn in Item.Product.SelectedAddOns.AddOnCollection)
                {
                    // Add-On value collection
                    foreach (ZNodeAddOnValueEntity AddOnValue in AddOn.AddOnValueCollection)
                    {
                        if (AddOn.TrackInventoryInd)
                        {
                            // Update Quantity into Inventory Table.
                            SKUInventory addOnInventory = null;
                            addOnInventory = skuInventoryService.GetBySKU(AddOnValue.SKU);

                            if (addOnInventory != null)
                            {
                                addOnInventory.QuantityOnHand = addOnInventory.QuantityOnHand - quantity;

                                skuInventoryService.Update(addOnInventory);
                            }
                        }
                    }
                }

                // Bundle Product Inventory
                foreach (ZNodeProductBaseEntity bundleProductItem in Item.Product.ZNodeBundleProductCollection)
                {
                    if (bundleProductItem.TrackInventoryInd)
                    {
                        SKUInventory skuInventory = skuInventoryService.GetBySKU(bundleProductItem.SKU);

                        if (skuInventory != null)
                        {
                            // Subtract Order quantity from the inventory quantity
                            skuInventory.QuantityOnHand = skuInventory.QuantityOnHand - quantity;

                            // update into SKU table
                            skuInventoryService.Update(skuInventory);
                        }
                    }

                    // Loop through the Selected addons for each Shopping cartItem
                    foreach (ZNodeAddOnEntity AddOn in bundleProductItem.SelectedAddOns.AddOnCollection)
                    {
                        // Add-On value collection
                        foreach (ZNodeAddOnValueEntity AddOnValue in AddOn.AddOnValueCollection)
                        {
                            if (AddOn.TrackInventoryInd)
                            {
                                // Update Quantity into Inventory Table.
                                SKUInventory addOnInventory = null;
                                addOnInventory = skuInventoryService.GetBySKU(AddOnValue.SKU);

                                if (addOnInventory != null)
                                {
                                    addOnInventory.QuantityOnHand = addOnInventory.QuantityOnHand - quantity;

                                    skuInventoryService.Update(addOnInventory);
                                }
                            }
                        }
                    }
                }
            }

            // Update the gift card
            if (!string.IsNullOrEmpty(this.GiftCardNumber))
            {
                GiftCardService giftCardService = new GiftCardService();
                GiftCard giftCard = giftCardService.GetByCardNumber(this.GiftCardNumber);

                // Deduct the balance.
                giftCard.Amount = giftCard.Amount - this.GiftCardAmount;

                // Update the Account Id.                 
                giftCard.AccountId = this.GetAccountId(true);

                giftCardService.Update(giftCard);
            }

        }

        /// <summary>
        /// Adds a coupon code to the shopping cart.
        /// </summary>
        /// <param name="CouponCode">Coupon Code</param>
        public void AddCouponCode(string CouponCode)
        {
            ZNodeCoupon coupon = new ZNodeCoupon()
            {
                Coupon = CouponCode,
                CouponApplied = false,
                CouponValid = false
            };
            this.Coupons.Add(coupon);
        }

        /// <summary>
        /// Add Gift Card to the shopping cart.
        /// </summary>
        /// <param name="giftCardNumber">Unique gift card number.</param>        
        public bool AddGiftCard(string giftCardNumber)
        {
            var success = false;
            string response; //Gets set to GiftCard Message
            var invalidGiftCard = string.Format("Invalid gift card: '{0}' ", giftCardNumber);
            var invalidAccountAsspciation = string.Format("Gift card '{0}' is not associated with this account.", giftCardNumber);
            var invalidStoreAssociation = string.Format("Gift card '{0}' is not associated with this store.", giftCardNumber);

            if (string.IsNullOrEmpty(giftCardNumber))
            {

                IsGiftCardApplied = false;
                response = string.Empty;
                GiftCardNumber = string.Empty;
                GiftCardAmount = 0;
            }

            var giftCardService = new GiftCardService();
            var giftCard = giftCardService.GetByCardNumber(giftCardNumber);
            var accountId = GetAccountId();

            // Reset previous value.
            GiftCardAmount = 0;
            GiftCardBalance = 0;

            if (giftCard != null & accountId != null)
            {
                // Check for specific account Gift Card.
                if (giftCard.AccountId != null && giftCard.AccountId != accountId)
                {
                    response = invalidAccountAsspciation;
                    IsGiftCardApplied = false;
                }
                else if (giftCard.PortalId != ZNodeConfigManager.SiteConfig.PortalID)
                {
                    response = invalidStoreAssociation;
                    IsGiftCardApplied = false;
                }
                else
                {
                    var availableBalance = Convert.ToDecimal(giftCard.Amount);
                    decimal remainingBalance = 0;
                    if (availableBalance > 0)
                    {
                        // Validate the giftcard expiration date.
                        if (giftCard.ExpirationDate == null || giftCard.ExpirationDate.Value < DateTime.Today.Date)
                        {
                            IsGiftCardValid = false;
                            IsGiftCardApplied = false;
                            response = "Gift card expired.";
                            GiftCardMessage = response;
                            return success;
                        }

                        if (Total > availableBalance)
                        {
                            // Set all available balance to Gift Card Amount.
                            GiftCardAmount = availableBalance;
                            remainingBalance = 0;
                        }
                        else if (Total <= availableBalance)
                        {
                            remainingBalance = Convert.ToDecimal(giftCard.Amount) - this.Total;
                            GiftCardAmount = Total;
                        }

                        response = string.Format("You currently have a balance of {0}{1:0.00}", ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix(), remainingBalance.ToString("c"));
                        GiftCardNumber = giftCardNumber;
                        IsGiftCardValid = true;
                        IsGiftCardApplied = true;
                        GiftCardBalance = remainingBalance;
                        success = true;
                    }
                    else
                    {
                        IsGiftCardValid = false;
                        IsGiftCardApplied = false;
                        response = "No balance amount on gift card";
                    }
                }
            }
            else
            {
                GiftCardNumber = string.Empty;
                IsGiftCardValid = false;
                IsGiftCardApplied = false;
                response = invalidGiftCard;
            }

            GiftCardMessage = response;
            return success;
        }

        public ZNodeShoppingCartItem Exists(ZNodeShoppingCartItem ShoppingCartItem, bool isAddToCart = false)
        {
            bool skuFound = false;
            bool addonsFound = false;
            ZNodeProductBaseEntity newProduct = ShoppingCartItem.Product;

            // loop through Shopping cart items
            foreach (ZNodeShoppingCartItem item in ShoppingCartItems)
            {
                ZNodeProductBaseEntity _product = item.Product;

                skuFound = false;
                addonsFound = false;

                // Ensure that the product exists.
                if (_product.ProductID == newProduct.ProductID)
                {
                    // Check Product has any attributes
                    if (_product.SelectedSKUvalue.SKUID > 0)
                    {
                        // Ensure that the product skuId is same
                        if (_product.SelectedSKUvalue.SKUID == newProduct.SelectedSKUvalue.SKUID)
                        {
                            skuFound = true;
                        }
                    }
                    else
                    {
                        skuFound = true;
                    }

                    // Check Product has any add-ons
                    addonsFound = string.Compare(_product.SelectedAddOns.SelectedAddOnValueIds, newProduct.SelectedAddOns.SelectedAddOnValueIds, true) == 0;

                    // Ensure that the addons are matching.
                    addonsFound &= string.Compare(_product.SelectedAddOns.ShoppingCartAddOnsDescription, newProduct.SelectedAddOns.ShoppingCartAddOnsDescription, true) == 0;

                    // Ensure that bundle products are matching.
                    addonsFound &= string.Compare(_product.ShoppingCartDescription, newProduct.ShoppingCartDescription, true) == 0;

                    if (skuFound && addonsFound)
                    {
                        // The user clicked Add on an item that is already in the cart.
                        if (isAddToCart)
                            item.Product = ShoppingCartItem.Product;

                        return item;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Search an item in our shopping cart.
        /// Determines whether the specified item exists
        /// </summary>
        /// <param name="ShoppingCartItem">The Shopping CartI tem to find.</param>
        /// <returns>The shopping cart item if BOTH the SKU and Addons are found in a the cart. Otherwise returns null</returns>
        public ZNodeShoppingCartItem Exists(ZNodeShoppingCartItem ShoppingCartItem)
        {
            return Exists(ShoppingCartItem, false);
        }

        /// <summary>
        /// Returns Total Qunatity ordered for each Shopping cart Item
        /// </summary>
        /// <param name="ShoppingCartItem">Shopping Cart Item</param>
        /// <returns>Returns the Quantty ordered</returns>
        public int GetQuantityOrdered(ZNodeShoppingCartItem ShoppingCartItem)
        {
            int QuantityOrdered = 0;

            // Loop through Shopping cart items
            foreach (ZNodeShoppingCartItem item in ShoppingCartItems)
            {
                if (item.Product.ProductID == ShoppingCartItem.Product.ProductID)
                {
                    // Check Product has any attributes
                    if (item.Product.SelectedSKU.SKUID > 0)
                    {
                        if (item.Product.SelectedSKU.SKUID == ShoppingCartItem.Product.SelectedSKU.SKUID)
                        {
                            if (item.GUID != ShoppingCartItem.GUID)
                            {
                                QuantityOrdered += item.Quantity;
                            }
                        }
                    }
                    else
                    {
                        if (item.GUID != ShoppingCartItem.GUID)
                        {
                            QuantityOrdered += item.Quantity;
                        }
                    }
                }
            }

            return QuantityOrdered;
        }

        /// <summary>
        /// Returns Total Qunatity ordered for each Shopping cart Item
        /// </summary>
        /// <param name="shoppingCartItem">Shopping Cart Item</param>
        /// <returns>Returns the Quantty ordered</returns>
        public int GetQuantityAddOnOrdered(ZNodeShoppingCartItem shoppingCartItem)
        {
            // Loop through Shopping cart items
            return (from ZNodeShoppingCartItem item in ShoppingCartItems
                    where item.GUID != shoppingCartItem.GUID
                    from ZNodeAddOnEntity itemAddOn in item.Product.SelectedAddOns.AddOnCollection
                    from ZNodeAddOnValueEntity itemAddOnValue in itemAddOn.AddOnValueCollection
                    from currentItemAddOnValue in shoppingCartItem.Product.SelectedAddOns.AddOnCollection.Cast<ZNodeAddOnEntity>().SelectMany(x => x.AddOnValueCollection.Cast<ZNodeAddOnValueEntity>())
                    where currentItemAddOnValue.AddOnValueID == itemAddOnValue.AddOnValueID
                    select item.Quantity).Sum();
        }

        /// <summary>
        /// Returns Total Qunatity ordered for each Shopping cart Item
        /// </summary>
        /// <param name="addonValueId"></param>
        /// <returns>Returns the Quantty ordered</returns>
        public int GetQuantityAddOnOrdered(int addonValueId)
        {
            // Loop through Shopping cart items
            return (from ZNodeShoppingCartItem item in ShoppingCartItems
                    from ZNodeAddOnEntity itemAddOn in item.Product.SelectedAddOns.AddOnCollection
                    from ZNodeAddOnValueEntity itemAddOnValue in itemAddOn.AddOnValueCollection
                    where itemAddOnValue.AddOnValueID == addonValueId
                    select item.Quantity).Sum();
        }
        /// <summary>
        /// Returns Total Qunatity ordered for each Shopping cart Item
        /// </summary>
        /// <param name="shoppingCartItem">Shopping Cart Item</param>
        /// <param name="addonValueId"></param>
        /// <returns>Returns the Quantty ordered</returns>
        public int GetQuantityAddOnOrdered(ZNodeShoppingCartItem shoppingCartItem, int addonValueId)
        {
            // Loop through Shopping cart items
            return (from ZNodeShoppingCartItem item in ShoppingCartItems
                    where item.GUID != shoppingCartItem.GUID
                    from ZNodeAddOnEntity itemAddOn in item.Product.SelectedAddOns.AddOnCollection
                    from ZNodeAddOnValueEntity itemAddOnValue in itemAddOn.AddOnValueCollection
                    where itemAddOnValue.AddOnValueID == addonValueId
                    select item.Quantity).Sum();
        }

        /// <summary>
        /// Adds a gift card item to the shopping cart
        /// </summary>
        /// <param name="ShoppingCartItem">Shopping Cart Item</param>
        /// <param name="isGiftCard">If product type is gift card then add the item.</param>
        /// <returns>Returns true if inserted else return false</returns>
        public bool AddToCart(ZNodeShoppingCartItem ShoppingCartItem, bool isGiftCard)
        {
            bool isAdded = false;
            if (isGiftCard)
            {
                // This is a new item added to the cart.
                base.ShoppingCartItems.Add(ShoppingCartItem);
                isAdded = true;
            }
            else
            {
                int quantityOrdered = this.GetQuantityOrdered(ShoppingCartItem) + ShoppingCartItem.Quantity;
                ZNodeShoppingCartItem cartItem = this.Exists(ShoppingCartItem, true);
                int maxQty = ShoppingCartItem.Product.MaxQty;
                int newQuantity = ShoppingCartItem.Quantity;

                if (maxQty == 0)
                {
                    maxQty = 10;
                }

                if (cartItem != null)
                {
                    newQuantity = ShoppingCartItem.Quantity + cartItem.Quantity;
                }

                if (quantityOrdered <= ShoppingCartItem.Product.QuantityOnHand && newQuantity <= maxQty)
                {
                    if (cartItem != null)
                    {
                        // The user clicked Add on an item that is already in the cart. Just update the quantity                    
                        this.UpdateItemQuantity(cartItem.GUID, newQuantity);
                    }
                    else
                    {
                        // This is a new item added to the cart.
                        base.ShoppingCartItems.Add(ShoppingCartItem);
                    }

                    isAdded = true;
                }
                //else if (newQuantity <= maxQty && (ShoppingCartItem.Product.AllowBackOrder || !ShoppingCartItem.Product.TrackInventoryInd))
                else if (newQuantity <= maxQty && (ShoppingCartItem.AllowBackOrder || !ShoppingCartItem.Product.TrackInventoryInd))
                {
                    // If AllowBack Order is enabled ,then it will add this product into cart or 
                    // TrackInventory is disabled ,then it will add this product into cart
                    if (cartItem != null)
                    {
                        // The user clicked Add on an item that is already in the cart. Just update the quantity                       
                        this.UpdateItemQuantity(cartItem.GUID, newQuantity);
                    }
                    else
                    {
                        // This is a new item added to the cart.
                        base.ShoppingCartItems.Add(ShoppingCartItem);
                    }

                    isAdded = true;
                }
            }

            if (!string.IsNullOrEmpty(this.GiftCardNumber))
            {
                this.AddGiftCard(this.GiftCardNumber);
            }
            //For Ship to Multiple Address
            if (ShoppingCartItem.Quantity != ShoppingCartItem.OrderShipments.Sum(x => x.Quantity) && !isGiftCard)
                this.LoadShipmentData(ShoppingCartItem);
            portalCarts = new List<ZNodePortalCart>();
            return isAdded;
        }

        /// <summary>
        /// Adds an item to the shopping cart
        /// </summary>
        /// <param name="ShoppingCartItem">Shopping Cart Item</param>
        /// <returns>Returns true if inserted else return false</returns>
        public bool AddToCart(ZNodeShoppingCartItem ShoppingCartItem)
        {
            return this.AddToCart(ShoppingCartItem, false);
        }

        /// <summary>
        /// Removes an item from the shopping cart
        /// </summary>
        /// <param name="GUID">GUID of the cart</param>
        public void RemoveFromCart(string GUID)
        {
            // first locate the item in the collection
            ZNodeShoppingCartItem itemToRemove = this.GetItem(GUID);

            if (itemToRemove != null)
            {
                base.ShoppingCartItems.Remove(itemToRemove);
            }

            if (this.GiftCardNumber != string.Empty)
            {
                this.AddGiftCard(this.GiftCardNumber);
            }
            portalCarts = new List<ZNodePortalCart>();
        }

        /// <summary>
        /// Empty the shopping cart
        /// </summary>
        public void EmptyCart()
        {
            this.AddGiftCard(String.Empty);
            base.ShoppingCartItems.Clear();
        }

        /// <summary>
        /// Locates a shopping cart item by it's GUID
        /// </summary>
        /// <param name="GUID">GUID of the item</param>
        /// <returns>Returns the item</returns>
        public ZNodeShoppingCartItem GetItem(string GUID)
        {
            foreach (ZNodeShoppingCartItem item in ShoppingCartItems)
            {
                if (item.GUID.Equals(GUID))
                {
                    return item;
                }
            }

            return null;
        }

        /// <summary>
        /// Updates a specific shopping cart item
        /// </summary>
        /// <param name="GUID">GUID of the item</param>
        /// <param name="UpdatedQuantity">Updated Quantity</param>
        public void UpdateItemQuantity(string GUID, int UpdatedQuantity)
        {
            // first locate the item in the collection
            ZNodeShoppingCartItem itemToUpdate = this.GetItem(GUID);
            itemToUpdate.Quantity = UpdatedQuantity;
            LoadShipmentData(itemToUpdate);
        }

        /// <summary>
        /// Updates a specific shopping cart item
        /// </summary>
        /// <param name="GUID">GUID of the item</param>
        /// <param name="UpdatedQuantity">Updated Quantity</param>
        public void UpdateAddOnItemQuantity(string GUID, int UpdatedQuantity)
        {
            // first locate the item in the collection
            ZNodeShoppingCartItem itemToUpdate = this.GetItem(GUID);

            foreach (ZNode.Libraries.ECommerce.Entities.ZNodeAddOnEntity AddOn in itemToUpdate.Product.SelectedAddOns.AddOnCollection)
            {
                foreach (ZNode.Libraries.ECommerce.Entities.ZNodeAddOnValueEntity AddOnValue in AddOn.AddOnValueCollection)
                {
                    AddOnValue.QuantityOnHand = UpdatedQuantity;
                }
            }
        }

        /// <summary>
        /// Get the account Id.
        /// </summary>
        /// <returns>Returns the account Id</returns>
        private int? GetAccountId()
        {
            return this.GetAccountId(false);
        }

        /// <summary>
        /// Get the account Id.
        /// </summary>
        /// <param name="checkLogin">True to check the login for the current user else false.</param>
        /// <returns>Returns the account Id</returns>
        private int? GetAccountId(bool checkLogin)
        {
            int? accountId = null;

            if (HttpContext.Current.Session["AliasUserAccount"] != null)
            {
                // Order from Order Desk.
                ZNodeUserAccount account = (ZNodeUserAccount)HttpContext.Current.Session["AliasUserAccount"];
                if (account != null)
                {
                    // If accout has no login (checkout without registraion) then return the account Id is NULL.
                    if (account.UserID == null && checkLogin)
                    {
                        accountId = null;
                    }
                    else
                    {
                        accountId = Convert.ToInt32(account.AccountID);
                    }
                }
            }
            else
            {
                // Order from catalog side.
                ZNodeUserAccount account = ZNodeUserAccount.CurrentAccount();
                if (account != null)
                {
                    // If accout has no login (checkout without registraion) then return the account Id is NULL.
                    if (account.UserID == null && checkLogin)
                    {
                        accountId = null;
                    }
                    else
                    {
                        accountId = Convert.ToInt32(account.AccountID);
                    }
                }
            }

            return accountId;
        }

        /// <summary>
        /// Remove Shipment item
        /// </summary>
        /// <param name="SlNo"></param>
        public void RemoveShipmentProduct(string slNo)
        {
            foreach (ZNodeShoppingCartItem item in ShoppingCartItems)
            {

                var ordershipment = item.OrderShipments.Find(x => x.SlNo == slNo);
                if (ordershipment != null)
                {
                    item.OrderShipments.Remove(ordershipment);
                    item.Quantity = item.OrderShipments.Sum(y => y.Quantity);
                }

                if (!item.OrderShipments.Any())
                {
                    RemoveFromCart(item.GUID);
                    break;
                }
            }
        }



        /// <summary>
        /// Znode Version 7.2.2
        /// This function will check inventory of products, add on products, bundled products and add ons of bundled products in the shopping cart. 
        /// In case of 'Out of Stock Options' is set to 'Disable purchasing for out of stock product' then it will not allow to Submit the Order as 
        /// negative inventory. If stock is available then this fucntion returns true else false
        /// </summary>
        /// <returns>TRUE if Inventory is in hand else FALSE</returns>
        public bool IsInventoryInStock()
        {
            bool isInventoryInStock = true;
            SKUQuantity = new Dictionary<string, int>();
            try
            {
                // To check Inventory Loop through the Order Line Items
                foreach (ZNodeShoppingCartItem Item in ShoppingCartItems)
                {
                    //To check Product
                    if (!IsProductInStock(Item))
                    {
                        _ErrorMessage.Append("Sorry, this product is now out of the stock.");
                        return false;
                    }

                    //To check AddOnValue
                    if (!IsAddOnValueInStock(Item))
                    {
                        _ErrorMessage.Append("Sorry, this product addon is now out of the stock.");
                        return false;
                    }

                    //To check Bundle Product
                    if (!IsBundleProductInventoryInStock(Item))
                    {
                        _ErrorMessage.Append("Sorry, this product is now out of the stock.");
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                SKUQuantity = null;
                throw ex;
            }
            SKUQuantity = null;
            return isInventoryInStock;
        }

        /// <summary>
        /// Checks if the coupon quantity is available.
        /// </summary>
        /// <param name="couponCode">Coupon Code.</param>
        /// <returns>Bool value if the coupon code is available.</returns>
        public bool IsCouponQuantityAvailable(string couponCode)
        {
            bool isCouponQuantityAvailable = true;
            PromotionService promotionService = new PromotionService();
            var query = new PromotionQuery();
            query.Append(PromotionColumn.CouponCode, couponCode);

            // Find promotions with the coupon code
            var promotions = promotionService.Find(query.GetParameters());

            foreach (var promo in promotions)
            {
                if (promo.CouponQuantityAvailable.GetValueOrDefault(0) > 0)
                {
                    // Reduce the quantity of available coupons
                    promo.CouponQuantityAvailable -= 1;
                    isCouponQuantityAvailable = promotionService.Update(promo);
                }
                else
                {
                    _ErrorMessage.Append("Sorry, this Coupon is not available.");
                    isCouponQuantityAvailable = false;
                }
            }
            return isCouponQuantityAvailable;
        }

        /// <summary>
        /// This function will check inventory of products in the shopping cart.
        /// In case of 'Out of Stock Options' is set to 'Disable purchasing for out of stock product' then it will not allow to Submit the Order as 
        /// negative inventory. If stock is available then this fucntion returns true else false
        /// </summary>
        /// <param name="Item"></param>
        /// <returns>TRUE if Inventory is in hand else FALSE</returns>
        private bool IsProductInStock(ZNodeShoppingCartItem Item)
        {
            bool isProductInStock = true;
            int quantity = Item.Quantity;
            SKUInventoryService skuInventoryService = new SKUInventoryService();
            if (Item.Product.TrackInventoryInd && Item.Product.AllowBackOrder.Equals(false))
            {
                SKUInventory skuInventory = skuInventoryService.GetBySKU(Item.Product.SKU);

                if (skuInventory != null)
                {
                    AddUpdateSKUQuantity(Item.Product.SKU, quantity);
                    int currentQuantity = GetQuantityBySKU(Item.Product.SKU);
                    // Subtract Order quantity from the inventory quantity
                    skuInventory.QuantityOnHand = skuInventory.QuantityOnHand - currentQuantity;

                    //PRFT custom code: start
                    var currentOrderAccount = UserAccount.ZNodeUserAccount.CurrentAccount();
                    if (currentOrderAccount != null && currentOrderAccount.Custom2 == AccountCustom2Value)
                    {
                        return true;
                    }
                    else if (skuInventory.QuantityOnHand < 0)
                    {
                        return false;
                    }
                    //PRFT custom code: end
                    //PRFT Commented Code: Start
                    //if (skuInventory.QuantityOnHand < 0)
                    //{
                    //    return false;
                    //}
                    //PRFT Commented Code: End

                }
            }
            return isProductInStock;
        }

        /// <summary>
        /// This function will check inventory of AddOnValue in the shopping cart.
        /// In case of 'Out of Stock Options' is set to 'Disable purchasing for out of stock product' then it will not allow to Submit the Order as 
        /// negative inventory. If stock is available then this fucntion returns true else false
        /// </summary>
        /// <param name="Item"></param>
        /// <returns>TRUE if Inventory is in hand else FALSE</returns>
        private bool IsAddOnValueInStock(ZNodeShoppingCartItem Item)
        {
            bool isAddOnValueInStock = true;
            int quantity = Item.Quantity;
            SKUInventoryService skuInventoryService = new SKUInventoryService();
            // Loop through the Selected addons for each Shopping cartItem
            foreach (ZNodeAddOnEntity AddOn in Item.Product.SelectedAddOns.AddOnCollection)
            {
                // Add-On value collection
                foreach (ZNodeAddOnValueEntity AddOnValue in AddOn.AddOnValueCollection)
                {
                    if (AddOn.TrackInventoryInd && AddOn.AllowBackOrder.Equals(false))
                    {
                        SKUInventory addOnInventory = null;
                        addOnInventory = skuInventoryService.GetBySKU(AddOnValue.SKU);
                        if (addOnInventory != null)
                        {
                            AddUpdateSKUQuantity(AddOnValue.SKU, quantity);
                            int currentQuantity = GetQuantityBySKU(AddOnValue.SKU);
                            // Subtract Order quantity from the inventory quantity
                            addOnInventory.QuantityOnHand = addOnInventory.QuantityOnHand - currentQuantity;
                            if (addOnInventory.QuantityOnHand < 0)
                            {
                                return false;
                            }
                        }
                    }
                }
            }
            return isAddOnValueInStock;
        }

        /// <summary>
        /// This function will check inventory of bundled products in the shopping cart.
        /// In case of 'Out of Stock Options' is set to 'Disable purchasing for out of stock product' then it will not allow to Submit the Order as 
        /// negative inventory. If stock is available then this fucntion returns true else false
        /// </summary>
        /// <param name="Item"></param>
        /// <returns>TRUE if Inventory is in hand else FALSE</returns>
        private bool IsBundleProductInventoryInStock(ZNodeShoppingCartItem Item)
        {
            bool isBundleProductInventoryInStock = true;
            // Bundle Product Inventory
            int quantity = Item.Quantity;
            SKUInventoryService skuInventoryService = new SKUInventoryService();

            foreach (ZNodeProductBaseEntity bundleProductItem in Item.Product.ZNodeBundleProductCollection)
            {
                if (bundleProductItem.TrackInventoryInd && bundleProductItem.AllowBackOrder.Equals(false))
                {
                    SKUInventory skuInventory = skuInventoryService.GetBySKU(bundleProductItem.SKU);

                    if (skuInventory != null)
                    {
                        AddUpdateSKUQuantity(bundleProductItem.SKU, quantity);
                        int currentQuantity = GetQuantityBySKU(bundleProductItem.SKU);
                        // Subtract Order quantity from the inventory quantity
                        skuInventory.QuantityOnHand = skuInventory.QuantityOnHand - currentQuantity;
                        if (skuInventory.QuantityOnHand < 0)
                        {
                            return false;
                        }
                    }
                }

                // Loop through the Selected addons for each Shopping cartItem
                foreach (ZNodeAddOnEntity AddOn in bundleProductItem.SelectedAddOns.AddOnCollection)
                {
                    // Add-On value collection
                    foreach (ZNodeAddOnValueEntity AddOnValue in AddOn.AddOnValueCollection)
                    {
                        if (AddOn.TrackInventoryInd && AddOn.AllowBackOrder.Equals(false))
                        {
                            SKUInventory addOnInventory = null;
                            // Subtract Order quantity from the inventory quantity
                            addOnInventory = skuInventoryService.GetBySKU(AddOnValue.SKU);
                            if (addOnInventory != null)
                            {
                                AddUpdateSKUQuantity(AddOnValue.SKU, quantity);
                                int currentAddOnQuantity = GetQuantityBySKU(AddOnValue.SKU);
                                addOnInventory.QuantityOnHand = addOnInventory.QuantityOnHand - currentAddOnQuantity;
                                if (addOnInventory.QuantityOnHand < 0)
                                {
                                    return false;
                                }
                            }
                        }
                    }
                }
            }
            return isBundleProductInventoryInStock;
        }

        /// <summary>
        /// To add all products distinct SKU and its total quantity in dictonary.
        /// This function will add the quantities of the products in the cart. 
        /// This function will return the exact number of total quantity of the SKU in cart.
        /// </summary>
        /// <param name="sku">SKU</param>
        /// <param name="quantity">int Qunatity</param>
        private void AddUpdateSKUQuantity(string sku, int quantity)
        {
            if (SKUQuantity.Equals(null) || SKUQuantity.Count > 0)
            {
                if (SKUQuantity.ContainsKey(sku))
                {
                    int currentQuantity = GetQuantityBySKU(sku);
                    currentQuantity += quantity;
                    SKUQuantity.Remove(sku);
                    SKUQuantity.Add(sku, currentQuantity);
                }
                else
                {
                    SKUQuantity.Add(sku, quantity);
                }
            }
            else
            {
                SKUQuantity.Add(sku, quantity);
            }
        }

        /// <summary>
        /// This function will return total quantity of SKU in current cart.
        /// </summary>
        /// <param name="sku">SKU</param>
        /// <returns>Retunr latest SKU quantity</returns>
        private int GetQuantityBySKU(string sku)
        {
            int quantity = 0;
            foreach (KeyValuePair<string, int> pair in SKUQuantity)
            {
                if (pair.Key.Equals(sku))
                {
                    quantity = pair.Value;
                    break;
                }
            }
            return quantity;
        }



        #endregion

        #region "Multi ship to Methods"

        /// <summary>
        /// Returns the Default Shipping address id
        /// </summary>
        /// <returns></returns>
        private int GetDefaultShipping()
        {
            if (HttpContext.Current.Session["DefaultShippingAddressId"] == null)
            {
                int accountId = GetAccountId().GetValueOrDefault(0);
                if (accountId > 0)
                {
                    var defaultShippingAddress = DataRepository.AddressProvider.GetByAccountID(accountId).FirstOrDefault(x => x.IsDefaultShipping);
                    if (defaultShippingAddress != null)
                    {
                        HttpContext.Current.Session["DefaultShippingAddressId"] = defaultShippingAddress.AddressID;
                        return defaultShippingAddress.AddressID;
                    }
                }
            }
            else
            {
                return Convert.ToInt32(HttpContext.Current.Session["DefaultShippingAddressId"].ToString());
            }
            return 0;
        }

        /// <summary>
        /// Load Order Shipment for the given shopping cart item
        /// </summary>
        /// <param name="shoppingCartItem"></param>
        public void LoadShipmentData(ZNodeShoppingCartItem shoppingCartItem)
        {
            int i = 0;
            int OrderShipmentQuantity = shoppingCartItem.OrderShipments.Sum(x => x.Quantity);
            if (shoppingCartItem.Quantity > OrderShipmentQuantity)
            {
                i = OrderShipmentQuantity;
            }
            else
            {
                shoppingCartItem.OrderShipments.Clear();
            }
            int DefaultShippingAddressId = GetDefaultShipping();
            while (i < shoppingCartItem.Quantity)
            {
                shoppingCartItem.OrderShipments.Add(new ZNodeOrderShipment(DefaultShippingAddressId, 1, shoppingCartItem.GUID));
                i++;
            }

        }

        /// <summary>
        /// Locates a shopping cart item by Order shipment SlNo
        /// </summary>
        /// <param name="slNo"></param>
        /// <returns></returns>
        public ZNodeShoppingCartItem GetItemBySlNo(string slNo)
        {
            return ShoppingCartItems.Cast<ZNodeShoppingCartItem>().Where(x => x.OrderShipments.Any(y => y.SlNo == slNo)).FirstOrDefault();

        }

        /// <summary>
        /// Remove Shipment item
        /// </summary>
        /// <param name="SlNo"></param>
        public bool RemoveShipmentItem(string slNo)
        {
            var item = GetItemBySlNo(slNo);
            var ordershipment = item.OrderShipments.Find(x => x.SlNo == slNo);
            if (ordershipment != null)
            {
                if (item.Product.MinQty < item.OrderShipments.Count())
                {
                    item.OrderShipments.Remove(ordershipment);
                    item.Quantity = item.OrderShipments.Sum(y => y.Quantity);
                }
                else
                    return false;
            }

            if (!item.OrderShipments.Any())
            {
                RemoveFromCart(item.GUID);
                ZNodeSavedCart savedCart = new ZNodeSavedCart();
                savedCart.RemoveSavedCartLineItem(item.Product.SelectedSKU.SKUID);
            }
            return true;
        }
        /// <summary>
        /// Load Order Shipment for the given shopping cart item
        /// </summary>
        /// <param name="shoppingCartItem"></param>
        public void UpdateShipmentQuantity(ZNodeOrderShipment shipment)
        {
            ZNodeShoppingCartItem item = GetItemBySlNo(shipment.SlNo);
            int orderShipmentCount = item.OrderShipments.FindAll(x => x.AddressID == shipment.AddressID).Count();
            int maxQty = item.Product.MaxQty == 0 ? 10 : item.Product.MaxQty;
            if (orderShipmentCount > shipment.Quantity)
            {
                item.OrderShipments.RemoveAll(x => x.AddressID == shipment.AddressID);

                for (int i = 1; i <= shipment.Quantity; i++)
                {
                    if (maxQty == item.OrderShipments.Count())
                        break;
                    item.OrderShipments.Add(new ZNodeOrderShipment(shipment.AddressID, 1, item.GUID));
                }
            }
            else
            {
                var ordershipment = item.OrderShipments.Find(x => x.SlNo == shipment.SlNo);
                if (ordershipment != null)
                {
                    ordershipment.AddressID = shipment.AddressID;
                    ordershipment.Quantity = 1;
                    int addtionalQty = shipment.Quantity - orderShipmentCount;
                    for (int i = 1; i <= addtionalQty; i++)
                    {
                        if (maxQty == item.OrderShipments.Count())
                            break;
                        item.OrderShipments.Add(new ZNodeOrderShipment(shipment.AddressID, 1, item.GUID));
                    }
                }

            }
            this.portalCarts = new List<ZNodePortalCart>();

        }


        /// <summary>
        /// Update the Multi ship to cart items if the Quantity is changed
        /// </summary>
        /// <param name="shipment"></param>
        public bool UpdateShipment(ZNodeOrderShipment shipment)
        {

            ZNodeShoppingCartItem item = GetItemBySlNo(shipment.SlNo);
            int shippingAddressID = shipment.AddressID == 0 ? GetDefaultShipping() : shipment.AddressID;
            var ordershipment = item.OrderShipments.Find(x => x.SlNo == shipment.SlNo);
            if (ordershipment != null)
            {
                // If Max quantity is not set in admin , set it to 10
                int maxQty = item.Product.MaxQty == 0 ? 10 : item.Product.MaxQty;

                if (maxQty < (item.OrderShipments.Count() + shipment.Quantity - 1))
                    return false;

                ordershipment.AddressID = shipment.AddressID;
                ordershipment.Quantity = 1;

                for (int i = 2; i <= shipment.Quantity; i++)
                {
                    item.OrderShipments.Add(new ZNodeOrderShipment(shipment.AddressID, 1, item.GUID));
                }

            }
            item.Quantity = item.OrderShipments.Sum(y => y.Quantity);

            this.portalCarts = new List<ZNodePortalCart>();
            return true;
        }

        /// <summary>
        /// Locates a Order shipment items by its SlNo
        /// </summary>
        /// <param name="slNo"></param>
        /// <returns></returns>
        public void ResetPortalCart()
        {
            portalCarts = new List<ZNodePortalCart>();
        }

        /// <summary>
        /// Locates a Order shipment items by its SlNo
        /// </summary>
        /// <param name="slNo"></param>
        /// <returns></returns>
        public ZNodeOrderShipment GetOrderShipment(string slNo)
        {
            return ShoppingCartItems.Cast<ZNodeShoppingCartItem>().SelectMany(x => x.OrderShipments).First(y => y.SlNo == slNo);

        }

        /// <summary>
        /// Reset the Address id to default shipping address id in Order shipment if Address id deleted/address id is not set
        /// </summary>
        /// <param name="addressID"></param>
        public void ResetShippingAddress(int addressID)
        {
            var shipmentitems = ShoppingCartItems.Cast<ZNodeShoppingCartItem>().SelectMany(x => x.OrderShipments).Where(y => y.AddressID == addressID).ToList();
            if (shipmentitems.Any())
            {
                shipmentitems.ForEach(x => x.AddressID = GetDefaultShipping());
            }
        }

        /// <summary>
        /// Reset the Address id to default shipping address id in Order shipment if Address id deleted/address id is not set
        /// </summary>
        /// <param name="addressID"></param>
        public void ResetAllShippingAddress()
        {
            var shipmentitems = ShoppingCartItems.Cast<ZNodeShoppingCartItem>().SelectMany(x => x.OrderShipments).ToList();
            if (shipmentitems.Any())
            {
                shipmentitems.ForEach(x => x.AddressID = GetDefaultShipping());
            }
        }

        #endregion

    }
}