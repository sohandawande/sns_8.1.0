﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.ShoppingCart
{
    /// <summary>
    /// Represents a saved cart
    /// </summary>
    public class ZNodeSavedCart : ZNodeBusinessBase
    {
        #region Private Variables

        private CookieMapping _CookieMapping = null;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the ZNodeSavedCart class.
        /// </summary>
        public ZNodeSavedCart()
        {
            this.InitializeCookieMapping(false);
        }

        /// <summary>
        /// Initializes a new instance of the ZNodeSavedCart class.
        /// </summary>
        /// <param name="createCookie">Specify true if you want create new cookie if not exists</param>
        public ZNodeSavedCart(bool createCookie)
        {
            this.InitializeCookieMapping(createCookie);
        }

        #endregion

        #region Properties
        /// <summary>
        /// Gets a value indicating whether Portal PersistentCartEnabled field
        /// </summary>
        public bool IsEnabled
        {
            get { return ZNode.Libraries.Framework.Business.ZNodeConfigManager.SiteConfig.PersistentCartEnabled.GetValueOrDefault(false); }
        }

        #endregion

        #region Public Static Methods

        /// <summary>
        /// Add the shopping cart items into SavedCart.
        /// </summary>
        /// <param name="ShoppingCartItem">Specify ZNodeShoppingCartItem object</param>
        public static void AddToSavedCart(ZNodeShoppingCartItem ShoppingCartItem)
        {
            ZNodeSavedCart savedCart = new ZNodeSavedCart(true);
            SavedCartLineItem parentlineItem;
            parentlineItem = savedCart.AddToSavedCart(ShoppingCartItem.Product.SelectedSKU.SKUID, ShoppingCartItem.Quantity, null, null);

            foreach (ZNodeAddOn addOn in ShoppingCartItem.Product.SelectedAddOns.AddOnCollection)
            {
                foreach (ZNodeAddOnValueEntity addOnValue in addOn.AddOnValueCollection)
                {
                    savedCart.AddToSavedCart(addOnValue.AddOnValueID, ShoppingCartItem.Quantity, parentlineItem, (int)ZNodeOrderLineItemRelationshipType.Addons);
                }
            }

            foreach (ZNodeBundleProductEntity bundleProduct in ShoppingCartItem.Product.ZNodeBundleProductCollection)
            {
                SavedCartLineItem buddleParentlineItem;
                buddleParentlineItem = savedCart.AddToSavedCart(bundleProduct.SelectedSKUvalue.SKUID, ShoppingCartItem.Quantity, parentlineItem, (int)ZNodeOrderLineItemRelationshipType.ProductBundles);

                foreach (ZNodeAddOnEntity addOn in bundleProduct.SelectedAddOns.AddOnCollection)
                {
                    foreach (ZNodeAddOnValueEntity addOnValue in addOn.AddOnValueCollection)
                    {
                        savedCart.AddToSavedCart(addOnValue.AddOnValueID, ShoppingCartItem.Quantity, buddleParentlineItem, (int)ZNodeOrderLineItemRelationshipType.Addons);
                    }
                }
            }
        }

        //Web API
        public static string AddToWebAPISavedCart(ZNodeShoppingCartItem ShoppingCartItem, int savedCartId)
        {
            ZNodeSavedCart savedCart = new ZNodeSavedCart(true);
            SavedCartLineItem parentlineItem;
            parentlineItem = savedCart.AddToWebAPISavedCart(ShoppingCartItem.Product.SelectedSKU.SKUID, ShoppingCartItem.Quantity, null, null,savedCartId);

            foreach (ZNodeAddOnEntity addOn in ShoppingCartItem.Product.SelectedAddOns.AddOnCollection)
            {
                foreach (ZNodeAddOnValueEntity addOnValue in addOn.AddOnValueCollection)
                {
                    savedCart.AddToSavedCart(addOnValue.AddOnValueID, ShoppingCartItem.Quantity, parentlineItem, (int)ZNodeOrderLineItemRelationshipType.Addons);
                }
            }

            foreach (ZNodeBundleProductEntity bundleProduct in ShoppingCartItem.Product.ZNodeBundleProductCollection)
            {
                SavedCartLineItem buddleParentlineItem;
                buddleParentlineItem = savedCart.AddToWebAPISavedCart(bundleProduct.SelectedSKUvalue.SKUID, ShoppingCartItem.Quantity, parentlineItem, (int)ZNodeOrderLineItemRelationshipType.ProductBundles,savedCartId);

                foreach (ZNodeAddOnEntity addOn in bundleProduct.SelectedAddOns.AddOnCollection)
                {
                    foreach (ZNodeAddOnValueEntity addOnValue in addOn.AddOnValueCollection)
                    {
                        savedCart.AddToSavedCart(addOnValue.AddOnValueID, ShoppingCartItem.Quantity, buddleParentlineItem, (int)ZNodeOrderLineItemRelationshipType.Addons);
                    }
                }
            }

            return parentlineItem.SavedCartID.ToString();
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Initialize Shopping Cart by adding saved cart items to current shopping cart.
        /// </summary>
        public void InitializeShoppingCart(bool isRedirect = true)
        {
            if (this._CookieMapping == null)
            {
                return;
            }

            SavedCartService savedCartService = new SavedCartService();
            SavedCartLineItemService sclService = new SavedCartLineItemService();
            TList<SavedCart> savedCartList = savedCartService.GetByCookieMappingID(this._CookieMapping.CookieMappingID);
            if (savedCartList.Count != 0)
            {
                TList<SavedCartLineItem> lineItems = sclService.GetBySavedCartID(savedCartList[0].SavedCartID);
                sclService.DeepLoad(lineItems, true, DeepLoadType.IncludeChildren, typeof(List<SavedCartLineItem>));

                // fetch parent line items and add to shopping cart.
                foreach (SavedCartLineItem lineItem in lineItems.FindAll(SavedCartLineItemColumn.ParentSavedCartLineItemID, null))
                {
                    this.AddtoShoppingCart(lineItem);
                }

				if (!string.IsNullOrEmpty(Convert.ToString(System.Web.HttpContext.Current.Session["IsCartChanged"])) && isRedirect)
                {
                    System.Web.HttpContext.Current.Response.Redirect("~/shoppingcart.aspx");
                }
            }
        }

        public void InitializeShoppingCart(int savedCartId, out ZNodeShoppingCart zNodeShoppingCart)
        {
            zNodeShoppingCart = null;
            var savedCartService = new SavedCartService();
            var sclService = new SavedCartLineItemService();
            var savedCart = savedCartService.GetBySavedCartID(savedCartId);
            if (savedCart != null)
            {
                TList<SavedCartLineItem> lineItems = sclService.GetBySavedCartID(savedCart.SavedCartID);
                sclService.DeepLoad(lineItems, true, DeepLoadType.IncludeChildren, typeof(List<SavedCartLineItem>));

                zNodeShoppingCart = new ZNodeShoppingCart();
                // fetch parent line items and add to shopping cart.
                foreach (SavedCartLineItem lineItem in lineItems)
                {
                    this.AddtoShoppingCart(lineItem, zNodeShoppingCart);
                }
            }

        }

        /// <summary>
        /// Add the cart item to SavedCart table
        /// </summary>
        /// <param name="itemId">SKUID or selected AddOnID of the product</param>
        /// <param name="quantity">Quantity of the product</param>
        /// <param name="parentItem">Specify Parent Product SKUID if it is BuddleProduct or AddOn, otherwise specify null value</param>
        /// <param name="lineItemType">If parentItemId specified, specify Item Type, either BuddleProduct or AddOn as a int value, otherwise specify null value</param>
        /// <returns>Returns the inserted Saved Cart line item</returns>
        public SavedCartLineItem AddToSavedCart(int itemId, int quantity, SavedCartLineItem parentItem, int? lineItemType)
        {
            SavedCartService savedCartService = new SavedCartService();
            SavedCartLineItemService sclService = new SavedCartLineItemService();
            SavedCartLineItem lineItem = null;
            if (this._CookieMapping != null)
            {
                TList<SavedCart> savedCartList = savedCartService.GetByCookieMappingID(this._CookieMapping.CookieMappingID);
                SavedCart savedCart = null;
                if (savedCartList.Count == 0)
                {
                    savedCart = new SavedCart();
                    savedCart.CookieMappingID = this._CookieMapping.CookieMappingID;
                    savedCart.CreatedDate = DateTime.Now;
                    savedCart.LastUpdatedDate = DateTime.Now;
                    this._CookieMapping.SavedCartCollection.Add(savedCart);
                }
                else
                {
                    savedCart = savedCartList[0];
                    savedCartService.DeepLoad(savedCart, true, DeepLoadType.IncludeChildren, typeof(List<SavedCartLineItem>));
                }

                if (lineItemType.HasValue)
                {
                    lineItem = savedCart.SavedCartLineItemCollection.FindAll(SavedCartLineItemColumn.ParentSavedCartLineItemID, parentItem.SavedCartLineItemID).Find(SavedCartLineItemColumn.SKUID, itemId);
                }			

                if (lineItem == null)
                {
                    lineItem = new SavedCartLineItem();
                    lineItem.SavedCartID = savedCart.SavedCartID;
                    savedCart.SavedCartLineItemCollection.Add(lineItem);
                }

                lineItem.SKUID = itemId;
                lineItem.Quantity = quantity;
                lineItem.OrderLineItemRelationshipTypeID = lineItemType;
                lineItem.SavedCartID = savedCart.SavedCartID;

                if (parentItem != null)
                {
                    lineItem.ParentSavedCartLineItemID = parentItem.SavedCartLineItemID;
                }

                savedCart.LastUpdatedDate = DateTime.Now;
                savedCartService.DeepSave(savedCart);
            }

            return lineItem;
        }

        //Web API
        public SavedCartLineItem AddToWebAPISavedCart(int itemId, int quantity, SavedCartLineItem parentItem, int? lineItemType, int savedCartId)
        {
            SavedCartService savedCartService = new SavedCartService();
            SavedCartLineItemService sclService = new SavedCartLineItemService();
            SavedCartLineItem lineItem = null;

            Libraries.DataAccess.Entities.SavedCart savedCartEntity = null;
            if (savedCartId == 0)
            {
                savedCartEntity = new Libraries.DataAccess.Entities.SavedCart();

                savedCartEntity.CreatedDate = DateTime.Now;
                savedCartEntity.LastUpdatedDate = DateTime.Now;

                var cms = new CookieMappingService();
                var cookieMappingEntity = new CookieMapping
                {
                    PortalID = Libraries.Framework.Business.ZNodeConfigManager.DomainConfig.PortalID,
                    CreatedDate = DateTime.Now,
                    LastUpdatedDate = DateTime.Now
                };

                cookieMappingEntity = cms.Save(cookieMappingEntity);
                savedCartEntity.CookieMappingID = cookieMappingEntity.CookieMappingID;
            }
            else
            {
                var shoppingCartService = new SavedCartService();
                savedCartEntity = shoppingCartService.GetBySavedCartID(savedCartId);
            }

            if (lineItemType.HasValue)
            {
                lineItem = savedCartEntity.SavedCartLineItemCollection.FindAll(SavedCartLineItemColumn.SavedCartLineItemID, parentItem.SavedCartLineItemID).Find(SavedCartLineItemColumn.SKUID, itemId);
            }
            else
            {
                lineItem = savedCartEntity.SavedCartLineItemCollection.Find(SavedCartLineItemColumn.SKUID, itemId);
            }

            if (lineItem == null)
            {
                lineItem = new SavedCartLineItem();
                lineItem.SavedCartID = savedCartEntity.SavedCartID;
                savedCartEntity.SavedCartLineItemCollection.Add(lineItem);
            }

            lineItem.SKUID = itemId;
            lineItem.Quantity = quantity;
            lineItem.OrderLineItemRelationshipTypeID = lineItemType;
            lineItem.SavedCartID = savedCartEntity.SavedCartID;

            if (parentItem != null)
            {
                lineItem.ParentSavedCartLineItemID = parentItem.SavedCartLineItemID;
            }

            savedCartEntity.LastUpdatedDate = DateTime.Now;
            savedCartService.DeepSave(savedCartEntity);


            return lineItem;
        }

        /// <summary>
        /// Remove selected item from SavedCart
        /// </summary>
        /// <param name="itemId">Specify SKUID of the Product</param>
        public void RemoveSavedCartLineItem(int itemId)
        {
            if (this._CookieMapping != null && this._CookieMapping.SavedCartCollection.Count > 0)
            {
	            var lineItem =
		            this._CookieMapping.SavedCartCollection[0].SavedCartLineItemCollection.Where(
			            x => x.SKUID == itemId && !x.ParentSavedCartLineItemID.HasValue).ToList();


	            lineItem.ForEach(this.RemoveLineItem);

				// Re-initalize the session cookiemapping object.
				this.InitializeCookieMapping(false);
            }
        }

        /// <summary>
        /// Merge the SavedCart items when user logging to the site.  It merges the Cookie based items with Account based items. 
        /// </summary>
        public void Merge()
        {
			if (this._CookieMapping == null)
            {
                return;
            }

            CookieMappingService cookieMappingService = new CookieMappingService();
            ZNodeUserAccount userAccount = ZNodeUserAccount.CurrentAccount();

            SavedCartService savedCartService = new SavedCartService();
            SavedCartLineItemService sclService = new SavedCartLineItemService();

            TList<SavedCart> accountSavedCartList = new TList<SavedCart>();
            TList<SavedCart> cookieSavedCartList = new TList<SavedCart>();

            if (userAccount.AccountID > 0)
            {
                // Check Account based SavedCart exists.
                TList<CookieMapping> cookieMappingList = cookieMappingService.GetByAccountID(userAccount.AccountID);
                CookieMapping cookieMapping = null;
                if (cookieMappingList.Count > 0)
                {
                    cookieMapping = cookieMappingList[0];
                    accountSavedCartList = savedCartService.GetByCookieMappingID(cookieMapping.CookieMappingID);

                    TList<SavedCartLineItem> accountItems = sclService.GetBySavedCartID(accountSavedCartList[0].SavedCartID);
                    foreach (SavedCartLineItem lineItem in accountItems.FindAll(SavedCartLineItemColumn.ParentSavedCartLineItemID, null))
                    {
                        // Also update current shopping cart to reflect.
                        this.AddtoShoppingCart(lineItem);
                    }
                }

                if (this._CookieMapping != null)
                {
                    cookieSavedCartList = savedCartService.GetByCookieMappingID(this._CookieMapping.CookieMappingID);
                }

                // If not exists, update the existing cookie based to account based SavedCart.
                if (cookieMapping == null && this._CookieMapping != null)
                {
                    this._CookieMapping.AccountID = userAccount.AccountID;
                    cookieMappingService.Update(this._CookieMapping);
                }
                else if (cookieSavedCartList.Count > 0 && accountSavedCartList.Count > 0)
                {
                    // if account based SavedCart exists, append cookie based items to account based items.                    
                    TList<SavedCartLineItem> cookieItems = sclService.GetBySavedCartID(cookieSavedCartList[0].SavedCartID);
                    foreach (SavedCartLineItem lineItem in cookieItems)
                    {
                        lineItem.SavedCartID = accountSavedCartList[0].SavedCartID;
                        sclService.Update(lineItem);
                    }

                    // delete the cookie based SavedCart object
                    savedCartService.Delete(cookieSavedCartList[0].SavedCartID);

                    // delete the cookie based CookieMapping object
                    cookieMappingService.Delete(this._CookieMapping);

                    // Reassign new account based cookie to Session object.
                    this._CookieMapping = cookieMapping;
                    ZNodeEncryption encrypt = new ZNodeEncryption();
                    System.Web.HttpContext.Current.Session["CookieMappingID"] = encrypt.EncryptData(this._CookieMapping.CookieMappingID.ToString());
                }

                this.RemoveCookie();

				if (ZNodeShoppingCart.CurrentShoppingCart() != null)
				{
					ZNodeShoppingCart.CurrentShoppingCart().EmptyCart();
				}

				InitializeShoppingCart(false);

            }
        }

        /// <summary>
        /// Remove the SavedCart and its line items from the database and also delete CookieMapping record
        /// </summary>
        public void RemoveSavedCart()
        {
            if (this._CookieMapping != null)
            {
                // Check SavedCart exists
                SavedCartService savedCartService = new SavedCartService();
                TList<SavedCart> savedCartList = savedCartService.GetByCookieMappingID(this._CookieMapping.CookieMappingID);

                if (savedCartList.Count > 0)
                {
                    // delete lineitems first
                    SavedCartLineItemService sclService = new SavedCartLineItemService();
                    TList<SavedCartLineItem> lineItems = sclService.GetBySavedCartID(savedCartList[0].SavedCartID);
                    foreach (SavedCartLineItem lineItem in lineItems.FindAll(SavedCartLineItemColumn.ParentSavedCartLineItemID, null))
                    {
                        // call recurive function to remove the lineitem and its children.
                        this.RemoveLineItem(lineItem);
                    }

                    // delete the SavedCart object                    
                    savedCartService.Delete(savedCartList[0].SavedCartID);
                }

                // delete the cookie based CookieMapping object
                CookieMappingService cookieMappingService = new CookieMappingService();
                cookieMappingService.Delete(this._CookieMapping);

                // Remove cookie if exists.
                this.RemoveCookie();

                // Remove Session object.
                System.Web.HttpContext.Current.Session.Remove("CookieMappingID");
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Get CookieMapping record by session or account or cookie.
        /// </summary>
        /// <param name="createCookie">Create Cookie or not</param>
        private void InitializeCookieMapping(bool createCookie)
        {
            if (!this.IsEnabled)
            {
                return;
            }

            CookieMappingService cookieMappingService = new CookieMappingService();
            ZNodeUserAccount userAccount = ZNodeUserAccount.CurrentAccount();

            // Check session value
            this._CookieMapping = this.GetExistingCookieMappingBySession();

            // If new session, try get by logged in account or browser cookie or create new one for this session.
            if (this._CookieMapping == null)
            {
                if (userAccount != null && userAccount.AccountID > 0)
                {
                    TList<CookieMapping> cookieMappingList = cookieMappingService.GetByAccountID(userAccount.AccountID);

                    if (cookieMappingList.Count > 0)
                    {
                        this._CookieMapping = cookieMappingList[0];
                    }

                    cookieMappingService.DeepLoad(this._CookieMapping, true, DeepLoadType.IncludeChildren, typeof(List<SavedCart>), typeof(List<SavedCartLineItem>));
                }

                if (this._CookieMapping == null)
                {
                    this._CookieMapping = this.GetExistingCookieMappingByCookie(userAccount);
                }

                if (this._CookieMapping == null && createCookie)
                {
                    this._CookieMapping = this.CreateCookieMapping(userAccount);
                }

                if (this._CookieMapping != null && System.Web.HttpContext.Current.Session["CookieMappingID"] == null)
                {
                    // Add this cookieMappingId to Session.
                    ZNodeEncryption encrypt = new ZNodeEncryption();
                    string cookieValue = encrypt.EncryptData(this._CookieMapping.CookieMappingID.ToString());
                    System.Web.HttpContext.Current.Session.Add("CookieMappingID", cookieValue);
                }

            }
        }

        /// <summary>
        /// Setting up the cookieMapping for API 
        /// </summary>
        /// <param name="cookieMapping"></param>
        public void SetCookieMapping(CookieMapping cookieMapping)
        {
            this._CookieMapping = cookieMapping;
        }

        /// <summary>
        /// Create CookieMapping record in the DB and store the ID to browser cookie.
        /// </summary>
        /// <param name="userAccount">Logged in UserAccount</param>
        /// <returns>Cookie Mapping</returns>
        private CookieMapping CreateCookieMapping(ZNodeUserAccount userAccount)
        {
            CookieMappingService cookieMappingService = new CookieMappingService();

            // Initialize CookieMapping record object. 
            CookieMapping cookieMapping = new CookieMapping();
            cookieMapping.PortalID = ZNodeConfigManager.SiteConfig.PortalID;
            cookieMapping.CreatedDate = DateTime.Now;
            cookieMapping.LastUpdatedDate = DateTime.Now;

            if (userAccount != null && userAccount.AccountID > 0)
            {
                cookieMapping.AccountID = userAccount.AccountID;
            }

            cookieMappingService.Insert(cookieMapping);

            // If not logged in, create browser cookie.
            if (cookieMapping.AccountID == null)
            {
                System.Web.HttpCookie cookie = new System.Web.HttpCookie("CookieMappingID");
                // Add this cookieMappingId to Session.
                ZNodeEncryption encrypt = new ZNodeEncryption();
                cookie.Value = encrypt.EncryptData(cookieMapping.CookieMappingID.ToString());

                // Set Permanent Cookie, until user delete this from his computer.
                cookie.Expires = DateTime.Now.AddYears(1);
                System.Web.HttpContext.Current.Response.Cookies.Add(cookie);
            }

            return cookieMapping;
        }

        /// <summary>
        /// Check the broser cookie exists, if yes, fetch the CookieMapping record from the DB.
        /// </summary>
        /// <param name="userAccount">User Account</param>
        /// <returns>Cookie Mapping</returns>
        private CookieMapping GetExistingCookieMappingByCookie(ZNodeUserAccount userAccount)
        {
            CookieMappingService cookieMappingService = new CookieMappingService();
            CookieMapping cookieMapping = null;

            // Check cookie existing in the current system.
            System.Web.HttpCookie cookie = System.Web.HttpContext.Current.Request.Cookies["CookieMappingID"];

            if (cookie != null && !string.IsNullOrEmpty(cookie.Value))
            {
                ZNodeEncryption encrypt = new ZNodeEncryption();
                int cookieMappingId;
                string cookieValue = "0";
                // To Avoid decryption error while rotating the key in admin.
                try
                {
                    cookieValue = encrypt.DecryptData(cookie.Value);
                }
                catch (Exception)
                {
                    
                }
                
                int.TryParse(cookieValue, out cookieMappingId);

                // Get the existing CookieMapping record from DB
                if (cookieMappingId > 0)
                {
                    cookieMapping = cookieMappingService.DeepLoadByCookieMappingID(cookieMappingId, true, DeepLoadType.IncludeChildren, typeof(List<SavedCart>), typeof(List<SavedCartLineItem>));
                    if (cookieMapping != null && cookieMapping.PortalID == ZNodeConfigManager.SiteConfig.PortalID)
                    {
                        return cookieMapping;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Check the session exists, if yes, fetch the CookieMapping record from the DB.
        /// </summary>
        /// <returns>Cookie Mapping</returns>
        private CookieMapping GetExistingCookieMappingBySession()
        {
            CookieMappingService cookieMappingService = new CookieMappingService();
            CookieMapping cookieMapping = null;

            // Check cookiemapping existing in the current session.
            string sessionValue = System.Web.HttpContext.Current.Session["CookieMappingID"] as string;

            if (!string.IsNullOrEmpty(sessionValue))
            {
                ZNodeEncryption encrypt = new ZNodeEncryption();
                int cookieMappingId;
                sessionValue = encrypt.DecryptData(sessionValue);
                int.TryParse(sessionValue, out cookieMappingId);

                // Get the existing CookieMapping record from DB
                if (cookieMappingId > 0)
                {
                    cookieMapping = cookieMappingService.DeepLoadByCookieMappingID(cookieMappingId, true, DeepLoadType.IncludeChildren, typeof(List<SavedCart>), typeof(List<SavedCartLineItem>));
                }
            }

            return cookieMapping;
        }

        /// <summary>
        /// remove existing cookie in the current system.
        /// </summary>
        private void RemoveCookie()
        {
            System.Web.HttpCookie cookie = System.Web.HttpContext.Current.Request.Cookies["CookieMappingID"];

            if (cookie != null)
            {
                // Set cookie expire date to y'day and re-add to response object.
                cookie.Expires = DateTime.Now.AddDays(-1);
                System.Web.HttpContext.Current.Response.Cookies.Add(cookie);
            }
        }

        /// <summary>
        /// Remove the SavedCartLineItem by deleting child items first using recursive method
        /// </summary>
        /// <param name="lineItem">Specify SavedCartLineItem object to delete</param>
        private void RemoveLineItem(SavedCartLineItem lineItem)
        {
            SavedCartLineItemService sclService = new SavedCartLineItemService();

            // loop through each child items and call recursively
            foreach (SavedCartLineItem childLineItem in sclService.GetByParentSavedCartLineItemID(lineItem.SavedCartLineItemID))
            {
                this.RemoveLineItem(childLineItem);
            }

            // delete the parent (aka deep) child item.            
            sclService.Delete(lineItem);
        }

        private void AddtoShoppingCart(SavedCartLineItem lineItem, ZNodeShoppingCart shoppingCart)
        {
            SKUService skuService = new SKUService();
            SKU sku = skuService.GetBySKUID(lineItem.SKUID);

            // check if product exists.
            if (sku == null)
            {
                return;
            }

            ZNodeProduct _product = ZNodeProduct.Create(sku.ProductID, ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId);

            // Check if product is active
            if (!_product.IsActive || _product.CallForPricing)
            {
                return;
            }

            // Set Selected SKU/Atttribute
            _product.SelectedSKU = ZNodeSKU.CreateBySKU(sku.SKU);

            // Create shopping cart item
            ZNodeShoppingCartItem item = new ZNodeShoppingCartItem();
            item.LineItemId = lineItem.SavedCartLineItemID;
            item.Product = new ZNodeProductBase(_product);
            item.Quantity = lineItem.Quantity;

            SavedCartLineItemProductExtensionOptionService productExtensionOptionService = new SavedCartLineItemProductExtensionOptionService();
            TList<SavedCartLineItemProductExtensionOption> productExtensionOptions = productExtensionOptionService.GetAll();

            // Add product to cart
            //ZNodeShoppingCart shoppingCart = ZNodeShoppingCart.CurrentShoppingCart();

            // if shopping cart is null, create in session
            if (shoppingCart == null)
            {
                shoppingCart = new ZNodeShoppingCart();
                shoppingCart.AddToSession(ZNodeSessionKeyType.ShoppingCart);
            }
            shoppingCart.AddToCart(item);
        }

        /// <summary>
        /// Adds Item to Shopping Cart
        /// </summary>
        /// <param name="lineItem">Line Items</param>
        public void AddtoShoppingCart(SavedCartLineItem lineItem)
        {
            if (this._CookieMapping == null)
            {
                return;
            }

            SKUService skuService = new SKUService();
            SKU sku = skuService.GetBySKUID(lineItem.SKUID);

            // check if product exists.
            if (sku == null)
            {
                return;
            }

            ZNodeProduct _product = ZNodeProduct.Create(sku.ProductID, ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId);

            // Check if product is active
            if (_product == null || !_product.IsActive || _product.CallForPricing)
            {
                return;
            }

            // Set Selected SKU/Atttribute
            _product.SelectedSKU = ZNodeSKU.CreateBySKU(sku.SKU);

            if (_product.SelectedSKU == null || _product.SelectedSKU.SKUID == 0)
            {
                return;
            }

	        
			
			var addOnValueIdsList = new List<int>();
            Dictionary<int, string> addOnValueCustomeTextList = new Dictionary<int, string>();

	        // Check if product has attributes

	        // If product has Add-Ons, then validate that all Add-Ons Values have been selected by the customer
			if (_product.ZNodeAddOnCollection.Count > 0)
            {
				var addOnValueIdsStringBuilder = new StringBuilder();    
                var sclService = new SavedCartLineItemService();
                var childLineItems = sclService.GetByParentSavedCartLineItemID(lineItem.SavedCartLineItemID);

                foreach (var childLineItem in childLineItems.FindAll(SavedCartLineItemColumn.OrderLineItemRelationshipTypeID, (int)ZNodeOrderLineItemRelationshipType.Addons))
                {
					//currently the add-on value id is stored in the sku id column in the ZNodeSavedCartLineItem db table.
					if (!addOnValueIdsList.Contains(childLineItem.SKUID))
					{
						addOnValueIdsList.Add(childLineItem.SKUID);
                        addOnValueCustomeTextList.Add(childLineItem.SKUID, childLineItem.CustomeText);
					}
                }
				
                addOnValueIdsStringBuilder.Append(string.Join(",", addOnValueIdsList));
               
                if (addOnValueIdsStringBuilder.Length > 0)
                {
					_product.SelectedAddOns = ZNodeAddOnList.CreateByProductAndAddOns(_product.ProductID, addOnValueIdsStringBuilder.ToString(),lineItem.SavedCartLineItemID);
					_product.SelectedAddOns.SelectedAddOnValueIds = addOnValueIdsStringBuilder.ToString();
                    _product.SelectedAddOns.SelectedAddOnValueCustomText = addOnValueCustomeTextList;
                }
            }

            // Check quantity of product and ignore if not available.
            int qtyAvailable = this.HasQuantity(_product, lineItem.Quantity);

            if (qtyAvailable == 0)
            {
                return;
            }

            if (lineItem.Quantity > qtyAvailable)
            {
                lineItem.Quantity = qtyAvailable;
            }

            if (_product.ZNodeBundleProductCollection.Count > 0)
            {
                StringBuilder addOnValues = new StringBuilder();
                //ZNodeAddOnList SelectedAddOnList = new ZNodeAddOnList();

                SavedCartLineItemService sclService = new SavedCartLineItemService();
                TList<SavedCartLineItem> childLineItems = sclService.GetByParentSavedCartLineItemID(lineItem.SavedCartLineItemID);

                foreach (SavedCartLineItem childLineItem in childLineItems.FindAll(SavedCartLineItemColumn.OrderLineItemRelationshipTypeID, (int)ZNodeOrderLineItemRelationshipType.ProductBundles))
                {
                    for (int idx = 0; idx < _product.ZNodeBundleProductCollection.Count; idx++)
                    {
                        addOnValues = new StringBuilder();
                        SKU bundleSku = skuService.GetBySKUID(childLineItem.SKUID);
                        if (_product.ZNodeBundleProductCollection[idx].ProductID == bundleSku.ProductID)
                        {
                            TList<SavedCartLineItem> bundleChildLineItems = sclService.GetByParentSavedCartLineItemID(childLineItem.SavedCartLineItemID);
                            foreach (SavedCartLineItem bundleChildLineItem in bundleChildLineItems.FindAll(SavedCartLineItemColumn.OrderLineItemRelationshipTypeID, (int)ZNodeOrderLineItemRelationshipType.Addons))
                            {
                                if (addOnValues.Length > 0)
                                {
                                    addOnValues.Append(",");
                                }

                                addOnValues.Append(bundleChildLineItem.SKUID);
                            }

                            _product.ZNodeBundleProductCollection[idx].SelectedSKUvalue = ZNodeSKU.CreateBySKU(bundleSku.SKU);

                            if (addOnValues.Length > 0)
                            {
                                _product.ZNodeBundleProductCollection[idx].SelectedAddOns = ZNodeAddOnList.CreateByProductAndAddOns(_product.ZNodeBundleProductCollection[idx].ProductID, addOnValues.ToString());
                                _product.ZNodeBundleProductCollection[idx].SelectedAddOns.SelectedAddOnValueIds = addOnValues.ToString();
                            }

                            // Check quantity of product and ignore if not available.
                            qtyAvailable = this.HasQuantity(_product.ZNodeBundleProductCollection[idx], lineItem.Quantity);

                            if (qtyAvailable == 0)
                            {
                                return;
                            }

                            if (lineItem.Quantity > qtyAvailable)
                            {
                                lineItem.Quantity = qtyAvailable;
                            }
                        }
                    }
                }
            }
            // Create shopping cart item
            ZNodeShoppingCartItem item = new ZNodeShoppingCartItem();
            item.Product = new ZNodeProductBase(_product);
            item.Quantity = lineItem.Quantity;

			if (addOnValueIdsList.Any())
			{
				item.AddOnValueIds = addOnValueIdsList.ToArray();
                item.AddOnValuesCustomText = addOnValueCustomeTextList;
			}
            
            // Add product to cart
            ZNodeShoppingCart shoppingCart = ZNodeShoppingCart.CurrentShoppingCart();

            // if shopping cart is null, create in session
            if (shoppingCart == null)
            {
                shoppingCart = new ZNodeShoppingCart();
                shoppingCart.AddToSession(ZNodeSessionKeyType.ShoppingCart);
            }

            // add item to cart
            if (shoppingCart.AddToCart(item))
            {
                // add session value to know the shopping cart changed.
                System.Web.HttpContext.Current.Session["IsCartChanged"] = "1";
            }
        }

        /// <summary>
        /// Check quantity availability of Bundle Product and its AddOns that customer selected
        /// </summary>
        /// <param name="product">bundle product</param>
        /// <param name="quantityNeeded">Quantity Needed</param>
        /// <returns>Returns the quantity needed</returns>
        private int HasQuantity(ZNodeBundleProductEntity product, int quantityNeeded)
        {
            // Check stock    
            if (product.SelectedSKUvalue.QuantityOnHand <= quantityNeeded && (!product.AllowBackOrder) && product.TrackInventoryInd)
            {
                quantityNeeded = product.SelectedSKUvalue.QuantityOnHand;
            }

            // Check for quantity on hand and back-order,track inventory settings                                        
            int addOnCount = product.SelectedAddOns.AddOnCollection.Count;
            if (addOnCount > 0)
            {
                for (int idx = 0; idx < addOnCount; idx++)
                {
                    ZNodeAddOnEntity addOn = product.SelectedAddOns.AddOnCollection[idx];
                    if (addOn.AllowBackOrder == false && addOn.TrackInventoryInd)
                    {
                        int addOnValueCount = addOn.AddOnValueCollection.Count;
                        for (int indx = 0; indx < addOnValueCount; indx++)
                        {
                            if (addOn.AddOnValueCollection[indx].QuantityOnHand <= 0)
                            {
                                // Remove this addOnValue item from collection.
                                addOn.AddOnValueCollection.RemoveAt(indx);
                            }
                            else if (addOn.AddOnValueCollection[indx].QuantityOnHand <= quantityNeeded)
                            {
                                // note min quantity available.
                                quantityNeeded = addOn.AddOnValueCollection[indx].QuantityOnHand;
                            }
                        }

                        // Remove AddOn if no addonvalue available.
                        if (addOn.AddOnValueCollection.Count == 0 && addOn.OptionalInd)
                        {
                            product.SelectedAddOns.AddOnCollection.RemoveAt(idx);
                        }
                        else if (addOn.AddOnValueCollection.Count == 0 && !addOn.OptionalInd)
                        {
                            return 0;
                        }
                    }
                }
            }

            // return min quantity available.
            return quantityNeeded;
        }

        /// <summary>
        /// Check quantity availability of Product and its AddOns that customer selected
        /// </summary>
        /// <param name="product">Specify ZNodeAddOnList object</param>
        /// <param name="quantityNeeded">Specify ShoppingCart Quantity</param>
        /// <returns>return min quantity available</returns>
        private int HasQuantity(ZNodeProduct product, int quantityNeeded)
        {
            // Check stock    
            if (product.SelectedSKU.QuantityOnHand < quantityNeeded && (!product.AllowBackOrder) && product.TrackInventoryInd)
            {
                quantityNeeded = product.SelectedSKU.QuantityOnHand;
            }

            if (product.ZNodeAddOnCollection.Count > 0)
            {
                // Check for quantity on hand and back-order,track inventory settings                                        
                int addOnCount = product.SelectedAddOnItems.ZNodeAddOnCollection.Count;
                for (int idx = 0; idx < addOnCount; idx++)
                {
                    ZNodeAddOn addOn = product.SelectedAddOnItems.ZNodeAddOnCollection[idx];
                    if (addOn.AllowBackOrder == false && addOn.TrackInventoryInd)
                    {
                        int addOnValueCount = addOn.ZNodeAddOnValueCollection.Count;
                        for (int indx = 0; indx < addOnValueCount; indx++)
                        {
                            if (addOn.ZNodeAddOnValueCollection[indx].QuantityOnHand <= 0)
                            {
                                // Remove this addOnValue item from collection.
                                addOn.ZNodeAddOnValueCollection.RemoveAt(indx);
                            }
                            else if (addOn.ZNodeAddOnValueCollection[indx].QuantityOnHand <= quantityNeeded)
                            {
                                // note min quantity available.
                                quantityNeeded = addOn.ZNodeAddOnValueCollection[indx].QuantityOnHand;
                            }
                        }

                        // Remove AddOn if no addonvalue available.
                        if (addOn.ZNodeAddOnValueCollection.Count == 0)
                        {
                            if (!addOn.OptionalInd)
                            {
                                return 0;
                            }

                            product.SelectedAddOnItems.ZNodeAddOnCollection.RemoveAt(idx);
                        }
                    }
                }
            }

            if (quantityNeeded > product.MaxQty)
            {
                quantityNeeded = product.MaxQty;
            }

            // return min quantity available.
            return quantityNeeded;
        }

        #endregion
    }
}
