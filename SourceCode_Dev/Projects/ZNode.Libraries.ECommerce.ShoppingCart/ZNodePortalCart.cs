﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZNode.Libraries.ECommerce.Entities;
using Znode.Engine.Promotions;
using Znode.Engine.Shipping;
using Znode.Engine.Taxes;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.ShoppingCart
{
	[Serializable()]
	public class ZNodePortalCart : ZNodeShoppingCart
	{
		public int PortalID { get; set; }
		List<ZNodeMultipleAddressCart> _addressCarts = new List<ZNodeMultipleAddressCart>();

		/// <summary>
		/// Get Address based cart items.
		/// </summary>
		public List<ZNodeMultipleAddressCart> AddressCarts
		{
			get
			{
				var noAddressCarts = !_addressCarts.Any();
				var countNotEqual = _addressCarts.Count() != this.ShoppingCartItems.Cast<ZNodeShoppingCartItem>()
				                           .SelectMany(x => x.OrderShipments.Select(y => y.AddressID))
				                           .Distinct()
				                           .Count();

				var  quantityNotEqual =  _addressCarts.Sum(x => x.ShoppingCartItems.Cast<ZNodeShoppingCartItem>().Sum(s => s.Quantity)) != this.ShoppingCartItems.Cast<ZNodeShoppingCartItem>().Sum(s => s.Quantity);
				
				if (noAddressCarts || countNotEqual || quantityNotEqual)
				{
					var allOrderShipments = ShoppingCartItems.Cast<ZNodeShoppingCartItem>().SelectMany(x => x.OrderShipments).ToList();

					_addressCarts = allOrderShipments.GroupBy(x => new {x.AddressID, x.ShippingID},
					                                          (orderShipmentKey, orderShipmentKeyGroup) => new ZNodeMultipleAddressCart()
						                                                          {
							                                                          AddressID = orderShipmentKey.AddressID,
																					  Shipping = GetShippingMethod(orderShipmentKey.ShippingID, orderShipmentKeyGroup.FirstOrDefault()),
							                                                          ShoppingCartItems = GetAddressCartItems(orderShipmentKey.AddressID)
						                                                          }
																).ToList();
				}

				return _addressCarts;
			}
		}

		private static ZnodeShipping GetShippingMethod(int shippingId, ZNodeOrderShipment orderShipment)
		{

			if (shippingId > 0 && orderShipment != null)
			{
					return new ZnodeShipping()
					{
						ShippingID = shippingId,
						ShippingName = orderShipment.ShippingName
					};
			}

			return new ZnodeShipping();

		}

		public override decimal ShippingCost
		{
			get
			{
			    var addressCarts = this.AddressCarts.Where(x => x.Shipping != null).ToList();
                decimal totalShippingCost = addressCarts.Sum(x => x.ShippingCost);
                decimal shippingDiscount = addressCarts.Sum(x => x.Shipping.ShippingDiscount) + this.Shipping.ShippingDiscount;
				return totalShippingCost < shippingDiscount ? 0 : Math.Round((totalShippingCost - shippingDiscount), 2);
			}
		}

	    public override decimal TaxCost
		{
			get
			{
				decimal totalTaxCost = this.AddressCarts.Sum(x => x.OrderLevelTaxes);

				return Math.Round((totalTaxCost), 2);
			}
		}

		/// <summary>
		/// Gets total cost of items in the shopping cart before shipping and taxes
		/// </summary>
		public override decimal SubTotal
		{
			get
			{
                //PRFT Custom Code : Start
                //var subTotal = _addressCarts.SelectMany(x => x.ShoppingCartItems.Cast<ZNodeShoppingCartItem>()).Sum(item => item.ExtendedPrice);
                var subTotal = _addressCarts.SelectMany(x => x.ShoppingCartItems.Cast<ZNodeShoppingCartItem>()).Sum(item => item.ERPExtendedPrice);
                //PRFT Custom Code : End

                return Math.Round(subTotal, 2);
			}
		}
		
		/// <summary>
		/// Gets the total cost after shipping, taxes and promotions
		/// </summary>        
		public override decimal Total
		{
			get
			{
				decimal tempCalc = 0;
				tempCalc = (this.SubTotal - this.Discount) + this.ShippingCost + this.TaxCost + this.OrderLevelTaxes - this.GiftCardAmount;
				return tempCalc;
			}
		}

		public ZNodePortalCart() : base() { }

		public bool RemoveAddressCartItem(string addressId, string guid)
		{
			var item = this.ShoppingCartItems.Cast<ZNodeShoppingCartItem>().FirstOrDefault(x => x.GUID == guid); 
			var removeItems = item.OrderShipments.Where(y => y.AddressID == int.Parse(addressId)).ToList();
			// Remove from the SavedCart table
			if (item != null)
			{
				var savedCart = new ZNodeSavedCart();
				savedCart.RemoveSavedCartLineItem(item.Product.SelectedSKU.SKUID);
			}

			if ((item.Product.MinQty > (item.Quantity - removeItems.Sum(x => x.Quantity)) && item.Product.MinQty !=1 ))
				return false;

			removeItems.ForEach(x =>
			{				
					if (item.OrderShipments.Contains(x))
					{
						item.Quantity -= x.Quantity;
						item.OrderShipments.Remove(x);
					}
                    if (item.Quantity == 0)
                    {
                        ZNodeShoppingCart.CurrentShoppingCart().RemoveFromCart(guid);
                    }
			}); 
			return true;
		}

		/// <summary>
		/// Calculates final pricing, shipping and taxes in the cart.
		/// </summary>
		public override void Calculate()
		{
			// Pass profile ID as null to the overload
			Calculate(null);
		}

		/// <summary>
		/// Calculates final pricing, shipping and taxes in the cart.
		/// </summary>
		public override void Calculate(int? profileId)
		{
			// Clear previous messages
			this._ErrorMessage = new StringBuilder();

			// ShippingRules
			//var shipping = new ZnodeShippingManager(this);
			//shipping.Calculate();

			// Promotions
			var cartPromoManager = new ZnodeCartPromotionManager(this, profileId);
			cartPromoManager.Calculate();

            // TaxRules
            //var taxManager = new ZnodeTaxManager(this);
            //taxManager.Calculate(this);
            //PRFT Custom Code:Start
            //var addressShippingPayments = _addressCarts.Select(x => new {x.AddressID, x.AddressCartID, x.Shipping, x.Payment}).ToList();
            var addressShippingPayments = _addressCarts.Select(x => new { x.AddressID, x.AddressCartID, x.Shipping, x.Payment,x.CustomerExternalId }).ToList();

            _addressCarts = new List<ZNodeMultipleAddressCart>();

			addressShippingPayments.ForEach(x =>
				{
					var item = AddressCarts.FirstOrDefault(y => y.AddressID == x.AddressID);
					if (item != null)
					{
					    item.AddressCartID = x.AddressCartID;
						item.Shipping = x.Shipping;
						item.Payment = x.Payment;
                        item.CustomerExternalId = x.CustomerExternalId; //PRFT custom Code
						item.Calculate();
					}
				});

            this.GiftCardAmount = 0;

            if (this.GiftCardNumber != string.Empty)
            {
                this.AddGiftCard(this.GiftCardNumber);
            }
		}

		private ZNodeGenericCollection<ZNodeShoppingCartItem> GetAddressCartItems(int addressId)
		{
			var currentOrderAccount = System.Web.HttpContext.Current.Session["AliasUserAccount"] as UserAccount.ZNodeUserAccount;
			if (currentOrderAccount == null)
			{
				currentOrderAccount = UserAccount.ZNodeUserAccount.CurrentAccount();
				if(currentOrderAccount == null) return new ZNodeGenericCollection<ZNodeShoppingCartItem>();
			}
		    var addresses = currentOrderAccount.Addresses;
		    if (addresses != null)
		    {
		        var address = addresses.FirstOrDefault(x => x.AddressID == addressId);
		        var items = this.ShoppingCartItems.Cast<ZNodeShoppingCartItem>().Where(y => y.OrderShipments.Any(z => z.AddressID == addressId)).Select(c => c.Clone()).ToList();
		        var returnItems = new ZNodeGenericCollection<ZNodeShoppingCartItem>();
		        items.ForEach(y =>
		            {
		                y.Product.AddressToShip = address;
		                y.Quantity = y.OrderShipments.Where(z => z.AddressID == addressId).Sum(s => s.Quantity);                        
		                returnItems.Add(y);
		            });
		        return returnItems;
		    }

		    return new ZNodeGenericCollection<ZNodeShoppingCartItem>();
		}

        //public string CustomerExternalId { get; set; }//PRFT Custom Code
	}
}
