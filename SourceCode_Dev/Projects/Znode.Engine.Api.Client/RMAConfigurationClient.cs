﻿using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    public class RMAConfigurationClient : BaseClient, IRMAConfigurationClient
    {
        #region Public Methods
        public RMAConfigurationModel GetRMAConfiguration(int profileId)
        {
            return GetRMAConfiguration(profileId, null);
        }

        public RMAConfigurationModel GetRMAConfiguration(int rmaConfigId, ExpandCollection expands)
        {
            var endpoint = RMAConfigurationsEndpoint.Get(rmaConfigId);
            endpoint += BuildEndpointQueryString(expands);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<RMAConfigurationResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return Equals(response, null) ? null : response.RMAConfiguraton;
        }

        public RMAConfigurationListModel GetRMAConfigurations()
        {
            var endpoint = RMAConfigurationsEndpoint.List();
            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<RMAConfigurationListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new RMAConfigurationListModel { RMAConfigurations = (Equals(response, null)) ? null : response.RMAConfiguratons };

            return list;
        }

        public RMAConfigurationListModel GetAllRMAConfigurations()
        {
            var endpoint = RMAConfigurationsEndpoint.GetAllRMAConfigurations();
            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<RMAConfigurationListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new RMAConfigurationListModel { RMAConfigurations = (Equals(response, null)) ? null : response.RMAConfiguratons };

            return list;
        }

        public RMAConfigurationModel CreateRMAConfiguration(RMAConfigurationModel model)
        {
            var endpoint = RMAConfigurationsEndpoint.Create();

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<RMAConfigurationResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return (Equals(response, null)) ? null : response.RMAConfiguraton;
        }

        public RMAConfigurationModel UpdateRMAConfiguration(int rmaConfigId, RMAConfigurationModel model)
        {
            var endpoint = RMAConfigurationsEndpoint.Update(rmaConfigId);

            var status = new ApiStatus();
            var response = PutResourceToEndpoint<RMAConfigurationResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return (Equals(response, null)) ? null : response.RMAConfiguraton;
        } 
        #endregion
    }
}
