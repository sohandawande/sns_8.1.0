﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IRMARequestItemClient:IBaseClient
    {
        /// <summary>
        /// Gets list of RMA request item list.
        /// </summary>       
        /// <param name="expands">Expand collection for RMA request item list.</param>
        /// <param name="filters">Filter for RMA request item list.</param>
        /// <param name="sorts">Sort collection for RMA request item list.</param>
        /// <returns>RMA request item list model.</returns>
        RMARequestItemListModel GetRMARequestItemList(ExpandCollection expands, FilterCollection filters, SortCollection sorts);

        /// <summary>
        /// Gets list of RMA request item list.
        /// </summary>       
        /// <param name="expands">Expand collection for RMA request item list.</param>
        /// <param name="filters">Filter for RMA request item list.</param>
        /// <param name="sorts">Sort collection for RMA request item list.</param>
        /// <param name="pageIndex">Page index for RMA request item list.</param>
        /// <param name="pageSize">Page size for RMA request item list.</param>
        /// <returns>RMA request item list model.</returns>
        RMARequestItemListModel GetRMARequestItemList(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Gets list of RMA request item list.
        /// </summary>
        /// <param name="orderLineitems">comma seperated order line items string.</param>
        /// <returns>RMA request item model.</returns>
        RMARequestItemListModel GetRMARequestItemsForGiftCard(string orderLineitems);

        /// <summary>
        /// Creates an RMA Request item.
        /// </summary>
        /// <param name="model">RMA Request Item Model.</param>
        /// <returns>RMA Request Item Model.</returns>
        RMARequestItemModel CreateRMARequestItem(RMARequestItemModel model);

    }
}
