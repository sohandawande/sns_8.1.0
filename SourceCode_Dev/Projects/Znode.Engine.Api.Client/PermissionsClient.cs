﻿using Newtonsoft.Json;
using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    public class PermissionsClient : BaseClient, IPermissionsClient
    {
        public PermissionsModel GetAllRolesAndPermissionsByAccountId(int accountId)
        {
            string endpoint = PermissionsEndPoint.GetRolesAndPermissions(accountId);
            ApiStatus status = new ApiStatus();
            var response = GetResourceFromEndpoint<PermissionsResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return Equals(response, null) ? null : response.Permissions;
        }

        public bool UpdateRolesAndPermissions(PermissionsModel model)
        {
            string endpoint = PermissionsEndPoint.UpdateRolesAndPermissions();
            ApiStatus status = new ApiStatus();
            var response = PutResourceToEndpoint<TrueFalseResponse>(endpoint, JsonConvert.SerializeObject(model), status);
            return response.booleanModel.disabled;
        }
    }
}
