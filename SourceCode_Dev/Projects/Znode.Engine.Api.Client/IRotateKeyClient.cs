﻿
namespace Znode.Engine.Api.Client
{
    public interface IRotateKeyClient : IBaseClient
    {
        /// <summary>
        /// Generate a Rotation Key
        /// </summary>
        bool GenerateRotateKey();
    }
}
