﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IProductsClient : IBaseClient
    {
        /// <summary>
        /// Get product.
        /// </summary>
        /// <param name="productId">Id of the product.</param>
        /// <returns>product model</returns>
        ProductModel GetProduct(int productId);

        /// <summary>
        /// Get product.
        /// </summary>
        /// <param name="productId">Id of the product.</param>
        /// <param name="expands">exapnds</param>
        /// <returns>product model</returns>
        ProductModel GetProduct(int productId, ExpandCollection expands);

        /// <summary>
        /// Get product with sku.
        /// </summary>
        /// <param name="productId">Id of the product.</param>
        /// <param name="skuId">Id of the sku.</param>
        /// <returns>product model</returns>
        ProductModel GetProductWithSku(int productId, int skuId);

        /// <summary>
        /// Get product list.
        /// </summary>
        /// <param name="expands">expands</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <returns>product list model</returns>
        ProductListModel GetProducts(ExpandCollection expands, FilterCollection filters, SortCollection sorts);

        /// <summary>
        /// Get product list.
        /// </summary>
        /// <param name="expands">expands</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <param name="pageIndex">pageIndex</param>
        /// <param name="pageSize">pageSize</param>
        /// <returns>product list model</returns>
        ProductListModel GetProducts(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Get Products By Catalog.
        /// </summary>
        /// <param name="catalogId">Id of the product.</param>
        /// <param name="expands">expands</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <returns>product list model</returns>
        ProductListModel GetProductsByCatalog(int catalogId, ExpandCollection expands, FilterCollection filters, SortCollection sorts);

        /// <summary>
        /// Get Products By Catalog.
        /// </summary>
        /// <param name="catalogId">Id of the catalog.</param>
        /// <param name="expands">expands</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <param name="pageIndex">page index</param>
        /// <param name="pageSize">page size</param>
        /// <returns>product list model</returns>
        ProductListModel GetProductsByCatalog(int catalogId, ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Get products by catalog ids.
        /// </summary>
        /// <param name="catalogIds">catalog ids</param>
        /// <param name="expands">expands</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <param name="pageIndex">pageIndex</param>
        /// <param name="pageSize">pageSize</param>
        /// <returns>product list model</returns>
        ProductListModel GetProductsByCatalogIds(string catalogIds, ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Get products by category.
        /// </summary>
        /// <param name="categoryId">category id</param>
        /// <param name="expands">expands</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <returns>product list model</returns>
        ProductListModel GetProductsByCategory(int categoryId, ExpandCollection expands, FilterCollection filters, SortCollection sorts);

        /// <summary>
        /// Get products by category.
        /// </summary>
        /// <param name="categoryId">category id</param>
        /// <param name="expands">expands</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <param name="pageIndex">page index</param>
        /// <param name="pageSize">page size</param>
        /// <returns>product list model</returns>
        ProductListModel GetProductsByCategory(int categoryId, ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Get products by category by promotion type.
        /// </summary>
        /// <param name="categoryId">category id</param>
        /// <param name="promotionTypeId">promotion type id</param>
        /// <param name="expands">expands</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <returns>product list model</returns>
        ProductListModel GetProductsByCategoryByPromotionType(int categoryId, int promotionTypeId, ExpandCollection expands, FilterCollection filters, SortCollection sorts);

        /// <summary>
        /// Get products by category by promotion type
        /// </summary>
        /// <param name="categoryId">category id</param>
        /// <param name="promotionTypeId">promotion type id</param>
        /// <param name="expands">expands</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <param name="pageIndex">page index</param>
        /// <param name="pageSize">page size</param>
        /// <returns>product list model</returns>
        ProductListModel GetProductsByCategoryByPromotionType(int categoryId, int promotionTypeId, ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Get products by external ids
        /// </summary>
        /// <param name="externalIds">external ids</param>
        /// <param name="expands">expands</param>
        /// <param name="sorts">sorts</param>
        /// <returns>product list model</returns>
        ProductListModel GetProductsByExternalIds(string externalIds, ExpandCollection expands, SortCollection sorts);

        /// <summary>
        /// Get products by home specials
        /// </summary>
        /// <param name="expands">expands</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <param name="pageIndex">page index</param>
        /// <param name="pageSize">page size</param>
        /// <returns>product list model</returns>
        ProductListModel GetProductsByHomeSpecials(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Get products by product ids
        /// </summary>
        /// <param name="productIds">product ids</param>
        /// <param name="expands">expands</param>
        /// <param name="sorts">sorts</param>
        /// <returns>product list model</returns>
        ProductListModel GetProductsByProductIds(string productIds, ExpandCollection expands, SortCollection sorts);

        /// <summary>
        /// Get products by product ids
        /// </summary>
        /// <param name="productIds">product ids</param>
        /// <param name="expands">expands</param>
        /// <param name="sorts">sorts</param>
        /// <param name="pageIndex">page index</param>
        /// <param name="pageSize">page size</param>
        /// <returns>product list model</returns>
        ProductListModel GetProductsByProductIds(string productIds, ExpandCollection expands, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Get products by promotion type
        /// </summary>
        /// <param name="promotionTypeId">promotion type id</param>
        /// <param name="expands">expands</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <returns>product list model</returns>
        ProductListModel GetProductsByPromotionType(int promotionTypeId, ExpandCollection expands, FilterCollection filters, SortCollection sorts);

        /// <summary>
        /// Get products by promotion type
        /// </summary>
        /// <param name="promotionTypeId">promotion type id</param>
        /// <param name="expands">expands</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <param name="pageIndex">page index</param>
        /// <param name="pageSize">page size</param>
        /// <returns>product list model</returns>
        ProductListModel GetProductsByPromotionType(int promotionTypeId, ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Get products by query
        /// </summary>
        /// <param name="query">query</param>
        /// <returns>product list model</returns>
        ProductListModel GetProductsByQuery(string query);

        /// <summary>
        /// create product
        /// </summary>
        /// <param name="model">model to create.</param>
        /// <returns>created model</returns>
        ProductModel CreateProduct(ProductModel model);

        /// <summary>
        /// update product.
        /// </summary>
        /// <param name="productId">product id</param>
        /// <param name="model">updated model</param>
        /// <returns>updated model</returns>
        ProductModel UpdateProduct(int productId, ProductModel model);

        /// <summary>
        /// Delete existing product
        /// </summary>
        /// <param name="productId">product id</param>
        /// <returns>true or false</returns>
        bool DeleteProduct(int productId);

        /// <summary>
        /// Get products by product ids async
        /// </summary>
        /// <param name="productIds">product ids</param>
        /// <param name="accountId">account id</param>
        /// <param name="expands">expands</param>
        /// <param name="sorts">sorts</param>
        /// <returns>Task<ProductListModel></returns>
        Task<ProductListModel> GetProductsByProductIdsAsync(string productIds, int accountId, ExpandCollection expands, SortCollection sorts);

        /// <summary>
        /// Get products by home specials async
        /// </summary>
        /// <param name="expands">expands</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <param name="pageIndex">page index</param>
        /// <param name="pageSize">page size</param>
        /// <returns>Task<ProductListModel></returns>
        Task<ProductListModel> GetProductsByHomeSpecialsAsync(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        #region Product Settings
        /// <summary>
        /// Znode Version 8.0
        /// To update product setting
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <param name="model">ProductModel model</param>
        /// <returns>returns ProductModel</returns>
        ProductModel UpdateProductSettings(int productId, ProductModel model);

        /// <summary>
        /// To check seo url is exist
        /// </summary>
        /// <param name="seoUrl">string seoUrl</param>
        /// <param name="productId">int productId</param>
        /// <returns>returns true/false</returns>
        bool IsSeoUrlExist(int productId, string seoUrl);

        #endregion

        #region Product Category

        /// <summary>
        /// To get Product assosiated Category By ProductId
        /// </summary>
        /// <param name="expands">ExpandCollection</param>
        /// <param name="filters">FilterCollection</param>
        /// <param name="sorts">SortCollection</param>
        /// <param name="pageIndex">pageIndex</param>
        /// <param name="pageSize">pageSize</param>
        /// <returns>returns CategoryListModel</returns>
        CategoryListModel GetProductCategoryByProductId(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// To get unassociated Category By ProductId
        /// </summary>
        /// <param name="expands">ExpandCollection</param>
        /// <param name="filters">FilterCollection</param>
        /// <param name="sorts">SortCollection</param>
        /// <param name="pageIndex">pageIndex</param>
        /// <param name="pageSize">pageSize</param>
        /// <returns>returns CategoryListModel</returns>
        CategoryListModel GetProductUnAssociatedCategoryByProductId(int productId, ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// To Associate Product Category
        /// </summary>
        /// <param name="model">CategoryModel</param>
        /// <returns>true / false</returns>
        bool AssociateProductCategory(CategoryModel model);

        /// <summary>
        /// To unassociate Product Category
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <param name="categoryId">int categoryId</param>
        /// <returns>returns true/false</returns>
        bool UnAssociateProductCategory(CategoryModel model);
        #endregion

        #region Product Sku

        /// <summary>
        /// To get product sku details 
        /// </summary>
        /// <param name="expands">expands</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <param name="pageIndex">page index</param>
        /// <param name="pageSize">page size</param>
        /// <returns>returns product sku list</returns>
        ProductSkuListModel GetProductSkuDetails(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// To Associate Sku Facets
        /// </summary>
        /// <param name="skuId">int? skuId</param>
        /// <param name="associateFacetIds">string associateFacetIds</param>
        /// <param name="unassociateFacetIds">string unassociateFacetIds</param>
        /// <returns>returns true/false</returns>
        bool AssociateSkuFacets(int? skuId, string associateFacetIds, string unassociateFacetIds);

        /// <summary>
        /// To delete Sku Associated Facets 
        /// </summary>
        /// <param name="skuId">int skuId</param>
        /// <param name="facetGroupId"> int facetGroupId</param>
        /// <returns>returns true/false</returns>
        bool DeleteSkuAssociatedFacets(int skuId, int facetGroupId);

        /// <summary>
        /// To Get Sku Associated Facets
        /// </summary>
        /// <param name="productId">int productId,</param>
        /// <param name="skuId">int skuId</param>
        /// <param name="facetGroupId">int facetGroupId</param>
        /// <returns>returns list of ProductFacetModel</returns>
        ProductFacetModel GetSkuAssociatedFacets(int productId, int skuId, int facetGroupId);

        /// <summary>
        /// Znode Version 8.0
        /// To Get the Product associated facets.
        /// </summary>
        /// <param name="expands">ExpandCollection expands</param>
        /// <param name="filters"> FilterCollection filters</param>
        /// <param name="sorts">SortCollection sorts</param>
        /// <returns>returns facets list</returns>
        ProductFacetListModel GetSkuFacets(ExpandCollection expands, FilterCollection filters, SortCollection sorts);

        /// <summary>
        /// Znode Version 8.0
        /// Get Product Associated Facets
        /// </summary>
        /// <param name="expands">ExpandCollection expands</param>
        /// <param name="filters"> FilterCollection filters</param>
        /// <param name="sorts">SortCollection sorts</param>
        /// <param name="pageIndex">int? pageIndex</param>
        /// <param name="pageSize">int? pageSize</param>
        /// <returns>returns facets list</returns>
        ProductFacetListModel GetSkuFacets(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
        #endregion

        #region Product Tags
        /// <summary>
        /// Znode Version 8.0
        /// Get the Product Tags based on product Id.
        /// </summary>
        /// <param name="productId"></param>
        /// <returns>Return product tags</returns>
        ProductTagsModel GetProductTags(int productId);

        /// <summary>
        /// Znode Version 8.0
        /// Create the Product tags for the Product
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        ProductTagsModel CreateProductTags(ProductTagsModel model);

        /// <summary>
        /// Znode Version 8.0
        /// Updates the Product tags for the Product
        /// </summary>
        /// <param name="tagId"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        ProductTagsModel UpdateProductTags(int tagId, ProductTagsModel model);

        /// <summary>
        /// Znode Version 8.0
        /// Deletes the Product tags based on product id.
        /// </summary>
        /// <param name="tagId"></param>
        /// <returns></returns>
        bool DeleteProductTags(int tagId);

        #endregion

        #region Product Facet
        /// <summary>
        /// Znode Version 8.0
        /// To Get the Product associated facets.
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="facetGroupId"></param>
        /// <returns></returns>
        ProductFacetModel GetAssociatedFacets(int productId, int facetGroupId);

        /// <summary>
        /// Znode Version 8.0
        /// Get Product Associated Facets
        /// </summary>
        /// <param name="expands"></param>
        /// <param name="filters"></param>
        /// <param name="sorts"></param>
        /// <returns></returns>
        ProductFacetListModel GetProductFacets(ExpandCollection expands, FilterCollection filters, SortCollection sorts);

        /// <summary>
        /// Znode Version 8.0
        /// Get Product Associated Facets
        /// </summary>
        /// <param name="expands"></param>
        /// <param name="filters"></param>
        /// <param name="sorts"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        ProductFacetListModel GetProductFacets(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        ///  Znode Version 8.0
        /// To Associcate the Facets to the Product.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        bool BindAssociatedFacets(ProductFacetModel model);

        /// <summary>
        /// Znode Version 8.0
        /// To Delete the product associated facets based on productid & facetgroupid.
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="facetGroupId"></param>
        /// <returns></returns>
        bool DeleteProductAssociatedFacets(int productId, int facetGroupId);

        #endregion

        #region Product Details
        /// <summary>
        /// Znode Version 8.0
        /// To get product details by productId
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <returns>returns product details</returns>
        ProductModel GetProductDetailsByProductId(int productId);
        #endregion

        #region Product Image
        /// <summary>
        /// Znode Version 8.0
        /// To Get all Product image types.
        /// </summary>
        /// <returns>Return Product image types</returns>
        ProductImageTypeListModel GetProductImageTypes();

        /// <summary>
        /// Znode Version 8.0
        /// Insert the Product alternate image.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        ProductAlternateImageModel InsertProductAlternateImage(ProductAlternateImageModel model);

        /// <summary>
        /// Znode Version 8.0
        /// Updates the Product alternate image for the Product
        /// </summary>
        /// <param name="productImageId"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        ProductAlternateImageModel UpdateProductAlternateImage(int productImageId, ProductAlternateImageModel model);

        /// <summary>
        /// Znode Version 8.0
        /// Deletes the Product alternate image based on product image id.
        /// </summary>
        /// <param name="productImageId"></param>
        /// <returns></returns>
        bool DeleteProductAlternateImage(int productImageId);

        /// <summary>
        /// Znode Version 8.0
        /// Get Product Alternate Images.
        /// </summary>
        /// <param name="expands">expands</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <returns>ProductAlternateImageListModel</returns>
        ProductAlternateImageListModel GetProductAlternateImages(ExpandCollection expands, FilterCollection filters, SortCollection sorts);

        /// <summary>
        /// Znode Version 8.0
        /// Get Product Alternate Images.
        /// </summary>
        /// <param name="expands">expands</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <param name="pageIndex">page index</param>
        /// <param name="pageSize">page size</param>
        /// <returns>ProductAlternateImageListModel</returns>
        ProductAlternateImageListModel GetProductAlternateImages(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Znode Version 8.0
        /// Get Product Alternate Image based on Product image id.
        /// </summary>
        /// <param name="productImageId">product image id</param>
        /// <returns>ProductAlternateImageModel</returns>
        ProductAlternateImageModel GetProductAlternateImageByProductImageId(int productImageId);

        #endregion

        #region Product Bundle
        /// <summary>
        /// Znode Version 8.0
        /// Get Product Bundles List.
        /// </summary>
        /// <param name="expands">expands</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <returns>ProductBundlesListModel</returns>
        ProductBundlesListModel GetProductBundleList(ExpandCollection expands, FilterCollection filters, SortCollection sorts);

        /// <summary>
        /// Znode Version 8.0
        /// Get Product Bundles List.
        /// </summary>
        /// <param name="expands">expands</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <param name="pageIndex">page index</param>
        /// <param name="pageSize">page size</param>
        /// <returns>ProductBundlesListModel</returns>
        ProductBundlesListModel GetProductBundleList(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Znode Version 8.0
        /// Deletes the bundle product based on parent Child ProductID.
        /// </summary>
        /// <param name="parentChildProductID">parent child product id</param>
        /// <returns></returns>
        bool DeleteBundleProduct(int parentChildProductID);

        /// <summary>
        /// Znode Version 8.0
        /// Get Product List to associate with bundle.
        /// </summary>
        /// <param name="expands">expands</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <param name="pageIndex">pageIndex</param>
        /// <param name="pageSize">pageSize</param>
        /// <returns>ProductListModel</returns>
        ProductListModel GetProductList(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// To Associate Bundle Product.
        /// </summary>
        /// <param name="model">ProductModel</param>
        /// <returns>true / false</returns>
        bool AssociateBundleProduct(ProductModel model);


        #endregion

        #region Prdoduct Addon
        /// <summary>
        /// Znode Version 8.0
        /// Removes specified mapping between product and addon.
        /// </summary>
        /// <param name="productAddOnId">mapping id of product and addon.</param>
        /// <returns></returns>
        bool RemoveProductAddOn(int productAddOnId);

        /// <summary>
        /// Znode Version 8.0
        /// Associate specified addons with current product.
        /// </summary>
        /// <param name="models">list of models</param>
        /// <returns>true or false</returns>
        bool AssociateAddOns(List<AddOnModel> models);

        /// <summary>
        /// Znode Version 8.0
        /// Return associated addons for specified product id.
        /// </summary>
        /// <param name="productId">product id</param>
        /// <param name="expands">expands</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <param name="pageIndex">page index</param>
        /// <param name="pageSize">page size</param>
        /// <returns></returns>
        AddOnListModel GetProductAddOns(int productId, ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Znode Version 8.0
        /// Return unassociated addons for specified product id.
        /// </summary>
        /// <param name="productId">product id</param>
        /// <param name="portalId">portal id</param>
        /// <param name="expands">expands</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <param name="pageIndex">page index</param>
        /// <param name="pageSize">page size</param>
        /// <returns>AddOnListModel</returns>
        AddOnListModel GetUnassociatedAddOns(int productId, int portalId, ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        #endregion

        #region Product Highlight
        /// <summary>
        /// Znode Version 8.0
        /// Removes specified mapping between product and highlight.
        /// </summary>
        /// <param name="productHighlightId">mapping id of product and highlight.</param>
        /// <returns>return true or false</returns>
        bool RemoveProductHighlight(int productHighlightId);

        /// <summary>
        /// Znode Version 8.0
        /// Associate specified highlight with current product.
        /// </summary>
        /// <param name="models">list of model</param>
        /// <returns>return true or false</returns>
        bool AssociateHighlights(List<HighlightModel> models);

        /// <summary>
        /// Znode Version 8.0
        /// Return associated highlights for specified product id.
        /// </summary>
        /// <param name="productId">product id</param>
        /// <param name="expands">expands</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <param name="pageIndex">page index</param>
        /// <param name="pageSize">page size</param>
        /// <returns>HighlightListModel</returns>
        HighlightListModel GetProductHighlights(int productId, ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Znode Version 8.0
        /// Return unassociated highlights for specified product id.
        /// </summary>
        /// <param name="productId">product id</param>
        /// <param name="portalId">portalId</param>
        /// <param name="expands">expands</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <param name="pageIndex">page index</param>
        /// <param name="pageSize">page size</param>
        /// <returns>HighlightListModel</returns>
        HighlightListModel GetUnassociatedHighlights(int productId, int portalId, ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Znode Version 8.0
        /// Gets all products.
        /// </summary>
        /// <param name="expands">expands</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <param name="pageIndex">page index</param>
        /// <param name="pageSize">page size</param>
        /// <returns>CategoryAssociatedProductsListModel</returns>
        CategoryAssociatedProductsListModel GetAllProducts(ExpandCollection expands, FilterCollection filters,
                                                            SortCollection sorts, int? pageIndex, int? pageSize);
        #endregion

        #region Product Tiers

        /// <summary>
        /// Znode Version 8.0
        /// Return product tiers for specified product id.
        /// </summary>
        /// <param name="productId">product id</param>
        /// <param name="expands">expands</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <param name="pageIndex">page index</param>
        /// <param name="pageSize">page size</param>
        /// <returns>TierListModel</returns>
        TierListModel GetProductTiers(int productId, ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Znode Version 8.0
        /// Insert new product tier.
        /// </summary>
        /// <param name="model">model</param>
        /// <returns>ProductTierPricingModel</returns>
        ProductTierPricingModel CreateProductTier(ProductTierPricingModel model);

        /// <summary>
        /// Znode Version 8.0
        /// Updates specified product tier.
        /// </summary>
        /// <param name="model">model</param>
        /// <returns>ProductTierPricingModel</returns>
        ProductTierPricingModel UpdateProductTier(ProductTierPricingModel model);

        /// <summary>
        /// Znode Version 8.0
        /// Deletes specified product tier.
        /// </summary>
        /// <param name="productTierId">product tier id</param>
        /// <returns>return true or false</returns>
        bool DeleteProductTier(int productTierId);

        #endregion

        #region Digital Assets

        /// <summary>
        /// Znode Version 8.0
        /// Return digital assets for specified product id.
        /// </summary>
        /// <param name="productId">product id</param>
        /// <param name="expands">expands</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <param name="pageIndex">page index</param>
        /// <param name="pageSize">page size</param>
        /// <returns>DigitalAssetLisModel</returns>
        DigitalAssetLisModel GetProductDigitalAssets(int productId, ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Insert a new digital asset.
        /// </summary>
        /// <param name="model">model</param>
        /// <returns></returns>
        DigitalAssetModel CreateDigitalAsset(DigitalAssetModel model);

        /// <summary>
        /// Delete digital asset.
        /// </summary>
        /// <param name="digitlAssetId">int digitlAssetId</param>
        /// <returns></returns>
        bool DeleteDigitlAsset(int digitlAssetId);

        #endregion

        #region Product SEO Information
        /// <summary>
        /// To update product SEO Information
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <param name="model">ProductModel model</param>
        /// <returns>returns ProductModel model</returns>
        ProductModel UpdateProductSEOInformation(int productId, ProductModel model);
        #endregion

        #region Update Image

        /// <summary>
        /// Update Image
        /// </summary>
        /// <param name="model">model</param>
        /// <returns>UpdateImageModel</returns>
        UpdateImageModel UpdateImage(UpdateImageModel model);

        #endregion

        #region Product Details
        /// <summary>
        /// Copies a product.
        /// </summary>
        /// <param name="productId">Product id.</param>
        /// <returns>Product Model.</returns>
        ProductModel CopyProduct(int productId);

        /// <summary>
        /// Deletes a product.
        /// </summary>
        /// <param name="productId">Product id of the product to be deleted.</param>
        /// <returns>Returns true or false.</returns>
        bool DeleteProductByProductId(int productId);
        #endregion

        /// <summary>
        /// Search product
        /// </summary>
        /// <param name="expands">expands</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <param name="pageIndex">page index</param>
        /// <param name="pageSize">page size</param>
        /// <returns>Product List Model</returns>
        ProductListModel SearchProduct(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Get order product
        /// </summary>
        /// <param name="productId">product id</param>
        /// <returns>ProductModel</returns>
        ProductModel GetOrderProduct(int productId);

        /// <summary>
        /// Get sku product list on the filter criteria.
        /// </summary>
        /// <param name="filters">Filters to filter the list.</param>
        /// <returns>Returns sku product list.</returns>
        SkuProductListModel GetSkuProductListBySku(FilterCollection filters);

        /// <summary>
        /// Send a mail of compared product to friend
        /// </summary>
        /// <param name="model">Compare Product model contains productIds</param>
        /// <returns>Boolean value true/false</returns>
        bool SendMailToFriend(CompareProductModel model);


        //PRFT Custom Code:start
        /// <summary>
        /// Get the inventory lists from ERP
        /// </summary>
        /// <param name="expands"></param>
        /// <param name="filters"></param>
        /// <param name="sorts"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        PRFTInventoryListModel GetInventoryFromERP(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Get the product details from ERP
        /// </summary>
        /// <param name="productNum">Product Number</param>
        /// <param name="associatedCustomerExternalId">Associated Customer External ID</param>
        /// <param name="customerType">Customer Type</param>
        /// <returns></returns>
        PRFTERPItemDetailsModel GetItemDetailsFromERP(string productNum, string associatedCustomerExternalId, string customerType);

        PRFTInventoryModel GetInventoryDetailsFromERP(string productNum);
        //PRFT Custom Code:end
    }
}
