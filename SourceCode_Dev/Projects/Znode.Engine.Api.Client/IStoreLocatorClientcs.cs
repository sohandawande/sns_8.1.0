﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IStoreLocatorClient : IBaseClient
    {
        /// <summary>
        /// To Create New Store Locator.
        /// </summary>
        /// <param name="model">StoreModel</param>
        /// <returns>Returns StoreModel</returns>
        StoreModel CreateStoreLocator(StoreModel model);

        /// <summary>
        /// GEt Store Locator by Store id.
        /// </summary>
        /// <param name="storeId">Store id</param>
        /// <returns>Returns StoreModel.</returns>
        StoreModel GetStoreLocator(int storeId);

        /// <summary>
        /// Get list of Store Location.
        /// </summary>
        /// <param name="filters">FilterCollection</param>
        /// <param name="sorts">SortCollection</param>
        /// <returns>Returns StoreListModel</returns>
        StoreListModel GetStoreLocators(ExpandCollection expands,FilterCollection filters, SortCollection sorts);
        
        /// <summary>
        /// Get list of Store Location.
        /// </summary>
        /// <param name="expands">ExpandCollection for Store Locators</param>
        /// <param name="filters">FilterCollection for Store Locators</param>
        /// <param name="sorts">SOrting on Store Locators</param>
        /// <param name="pageIndex">nullable pageIndex</param>
        /// <param name="pageSize">nullable pageSize</param>
        /// <returns>Returns StoreListModel</returns>
        StoreListModel GetStoreLocators(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// To Update existing store locator by store id.
        /// </summary>
        /// <param name="storeId">store id</param>
        /// <param name="model">StoreModel</param>
        /// <returns>Returns true / false.</returns>
        StoreModel UpdateStoreLocator(int storeId, StoreModel model);

        /// <summary>
        /// To Delete existing Store locator.
        /// </summary>
        /// <param name="storeId">Store Id</param>
        /// <returns>Retuns True/ false.</returns>
        bool DeleteStoreLocator(int storeId);

        /// <summary>
        /// Get List of stores within the area
        /// </summary>
        /// <param name="model">Store Model</param>
        /// <returns>StoreListModel</returns>
        StoreListModel GetStoresList(StoreModel model);
    }
}
