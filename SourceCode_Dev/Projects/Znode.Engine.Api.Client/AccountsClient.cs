﻿using Newtonsoft.Json;
using System;
using System.Collections.ObjectModel;
using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    public partial class AccountsClient : BaseClient, IAccountsClient
    {
        public AccountModel GetAccount(int accountId)
        {
            return GetAccount(accountId, null);
        }

        public AccountModel GetAccount(int accountId, ExpandCollection expands)
        {
            var endpoint = AccountsEndpoint.Get(accountId);
            endpoint += BuildEndpointQueryString(expands);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<AccountResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (response == null) ? null : response.Account;
        }

        public AccountModel GetAccountByUser(Guid userId)
        {
            return GetAccountByUser(userId, null);
        }

        public AccountModel GetAccountByUser(Guid userId, ExpandCollection expands)
        {
            var endpoint = AccountsEndpoint.GetByUserId(userId.ToString());
            endpoint += BuildEndpointQueryString(expands);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<AccountResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (response == null) ? null : response.Account;
        }

        public AccountModel GetAccountByUser(string username)
        {
            return GetAccountByUser(username, null);
        }

        public AccountModel GetAccountByUser(string username, ExpandCollection expands)
        {
            var endpoint = AccountsEndpoint.GetByUsername();
            endpoint += BuildEndpointQueryString(expands);
            var status = new ApiStatus();
            AccountModel model = new AccountModel();
            model.UserName = username;
            var response = PostResourceToEndpoint<AccountResponse>(endpoint, JsonConvert.SerializeObject(model), status);
            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);
            return (Equals(response, null)) ? null : response.Account;
        }

        public AccountListModel GetAccounts(ExpandCollection expands, FilterCollection filters, SortCollection sorts)
        {
            return GetAccounts(expands, filters, sorts, null, null);
        }

        public AccountListModel GetAccounts(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = AccountsEndpoint.List();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<AccountListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new AccountListModel { Accounts = (response == null) ? null : response.Accounts };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public AccountModel CreateAccount(AccountModel model)
        {
            var endpoint = AccountsEndpoint.Create();

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<AccountResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return (response == null) ? null : response.Account;
        }

        public AccountModel UpdateAccount(int accountId, AccountModel model)
        {
            var endpoint = AccountsEndpoint.Update(accountId);

            var status = new ApiStatus();
            var response = PutResourceToEndpoint<AccountResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return (response == null) ? null : response.Account;
        }

        public bool DeleteAccount(int accountId)
        {
            var endpoint = AccountsEndpoint.Delete(accountId);

            var status = new ApiStatus();
            var deleted = DeleteResourceFromEndpoint<AccountResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

            return deleted;
        }

        public AccountModel Login(AccountModel model)
        {
            return Login(model, null);
        }

        public AccountModel Login(AccountModel model, ExpandCollection expands)
        {
            var endpoint = AccountsEndpoint.Login();
            endpoint += BuildEndpointQueryString(expands);

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<AccountResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.Unauthorized };
            CheckStatusAndThrow<ZnodeUnauthorizedException>(status, expectedStatusCodes);

            return (response == null) ? null : response.Account;
        }

        public AccountModel ResetPassword(AccountModel model)
        {
            var endpoint = AccountsEndpoint.ResetPassword();

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<AccountResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return (response == null) ? null : response.Account;
        }

        public AccountModel ChangePassword(AccountModel model)
        {
            var endpoint = AccountsEndpoint.ChangePassword();

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<AccountResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return (response == null) ? null : response.Account;
        }

        /// <summary>
        /// This function is used to check Role Exist For Profile
        /// </summary>
        /// <param name="porfileId">int porfileId - default porfileId</param>
        /// <param name="roleName">string roleName</param>
        /// <returns>returns true/false</returns>       
        public bool IsRoleExistForProfile(int porfileId, string roleName)
        {
            string endpoint = AccountsEndpoint.IsRoleExistForProfile(porfileId, roleName);
            ApiStatus status = new ApiStatus();
            var response = GetResourceFromEndpoint<TrueFalseResponse>(endpoint, status);
            return response.booleanModel.disabled;
        }

        public AccountModel CheckResetPasswordLinkStatus(AccountModel model)
        {
            var endpoint = AccountsEndpoint.CheckResetPasswordLinkStatus();

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<AccountResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return (response == null) ? null : response.Account;
        }

        /// <summary>
        /// Function checks for the User Role.
        /// </summary>
        /// <param name="model">AccountModel model</param>
        /// <returns></returns>
        public AccountModel CheckUserRole(AccountModel model)
        {
            var endpoint = AccountsEndpoint.CheckUserRole();
            var status = new ApiStatus();
            var response = PostResourceToEndpoint<AccountResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return (response == null) ? null : response.Account;
        }

        /// <summary>
        /// To check Address id Valid
        /// </summary>
        /// <param name="model">AddressModel model</param>
        /// <returns>returns AddressModel</returns>
        public AddressModel IsAddressValid(AddressModel model)
        {
            var endpoint = AccountsEndpoint.IsAddressValid();
            var status = new ApiStatus();
            var response = PostResourceToEndpoint<AddressResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return (response == null) ? null : response.Address;
        }
        /// <summary>
        /// Function used for Reset admin details for the first default login.
        /// </summary>
        /// <param name="model">AccountModel model</param>
        /// <returns></returns>
        public AccountModel ResetAdminDetails(AccountModel model)
        {
            var endpoint = AccountsEndpoint.ResetAdminDetails();
            var status = new ApiStatus();
            var response = PostResourceToEndpoint<AccountResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return (response == null) ? null : response.Account;
        }

        /// <summary>
        /// Znode Version 8.0
        /// This is the method to create the SiteAdmin user
        /// </summary>
        /// <param name="model">AccountModel</param>
        /// <returns>Returns admin details in AccountModel format.</returns>
        public AccountModel CreateAdminAccount(AccountModel model)
        {
            var endpoint = AccountsEndpoint.CreateAdminAccount();
            var status = new ApiStatus();
            var response = PostResourceToEndpoint<AccountResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return Equals(response, null) ? null : response.Account;
        }

        /// <summary>
        /// This method will fetch the account details by role name
        /// </summary>
        /// <param name="expands">Expand Collection expands</param>
        /// <param name="filters">Filter criteria</param>
        /// <param name="sorts">sort criteria</param>
        /// <param name="page">page collection</param>
        /// <param name="roleName">page collection</param>
        /// <returns>Returns all account detaisl in AccountListModel format</returns>
        public AccountListModel GetAccountDetailsByRoleName(string roleName, ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = AccountsEndpoint.GetByRoleName(roleName);
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<AccountListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new AccountListModel { Accounts = (response == null) ? null : response.Accounts };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        /// <summary>
        /// This method will Enable/Disable the Admin account
        /// </summary>
        /// <param name="accountId">int account id</param>
        /// <returns></returns>
        public bool EnableDisableAdminAccount(int accountId)
        {
            var endpoint = AccountsEndpoint.EnableDisable(accountId);
            var status = new ApiStatus();
            var response = PostResourceToEndpoint<TrueFalseResponse>(endpoint, JsonConvert.SerializeObject(accountId), status);
            return response.booleanModel.disabled;
        }

        #region Customer Account
        public AccountModel CreateCustomerAccount(AccountModel model)
        {
            var endpoint = AccountsEndpoint.CreateCustomerAccount();

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<AccountResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return Equals(response, null) ? null : response.Account;
        }

        /// <summary>
        /// To Update Customer Account Details
        /// </summary>
        /// <param name="model">AccountModel model</param>
        /// <returns>Return Updated Customer Account Details in AcountModel Format</returns>
        public AccountModel UpdateCustomerAccount(AccountModel model)
        {
            var endpoint = AccountsEndpoint.UpdateCustomerAccount(model.AccountId);
            var status = new ApiStatus();
            var response = PutResourceToEndpoint<AccountResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return Equals(response, null) ? null : response.Account;
        }

        /// <summary>
        /// To send mail with attchment
        /// </summary>
        /// <param name="model">SendMailModel model</param>
        /// <returns>returns SendMailModel</returns>
        public SendMailModel SendEmail(SendMailModel model)
        {
            var endpoint = AccountsEndpoint.SendEmail();

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<SendMailResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return (response == null) ? null : response.Email;
        }
        #endregion

        #region Customer Account Payment
        public AccountPaymentListModel GetAccountPaymentList(int accountId, ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = AccountsEndpoint.GetAccountPaymentList(accountId);
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<AccountPaymentListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new AccountPaymentListModel { AccountPayments = (Equals(response, null)) ? null : response.AccountPayments };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public AccountPaymentModel CreateAccountPayment(AccountPaymentModel model)
        {
            var endpoint = AccountsEndpoint.CreateAccountPayment();
            var status = new ApiStatus();
            var response = PostResourceToEndpoint<AccountPaymentResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return Equals(response, null) ? null : response.AccountPayment;
        }
        #endregion

        /// <summary>
        /// Get list of Role Permissions based on userName.
        /// </summary>
        /// <param name="userName">UserName for the user</param>
        /// <returns>Return the Role Permission list in RolePermissionListModel format</returns>
        public RolePermissionListModel GetRolePermission(string userName)
        {
            var endpoint = AccountsEndpoint.GetRolePermission();
            var status = new ApiStatus();
            AccountModel model = new AccountModel();
            model.UserName = userName;
            var response = PostResourceToEndpoint<RolePermissionListResponse>(endpoint, JsonConvert.SerializeObject(model), status);
            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);
            var list = new RolePermissionListModel { UserPermissionList = (Equals(response, null)) ? null : response.UserPermissionList };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        /// <summary>
        /// Get list of Role Menu based on userName.
        /// </summary>
        /// <param name="userName">UserName for the user</param>
        /// <returns>Return the Role Menu list in RoleMenuListModel format</returns>
        public RoleMenuListModel GetRoleMenuList(string userName)
        {
            var endpoint = AccountsEndpoint.GetRoleMenuList();
            var status = new ApiStatus();
            AccountModel model = new AccountModel();
            model.UserName = userName;
            var response = PostResourceToEndpoint<RoleMenuListResponse>(endpoint, JsonConvert.SerializeObject(model), status);
            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);
            var list = new RoleMenuListModel { RoleMenuList = (Equals(response, null)) ? null : response.RoleMenuList };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        /// <summary>
        /// Sign Up the User for NewsLetters
        /// </summary>
        /// <param name="model">NewsLetterSignUpModel</param>
        /// <returns>returns true/false</returns>
        public bool SignUpForNewsLetter(NewsLetterSignUpModel model)
        {
            string endpoint = AccountsEndpoint.SignUpForNewsLetter();
            ApiStatus status = new ApiStatus();
            var response = PostResourceToEndpoint<TrueFalseResponse>(endpoint, JsonConvert.SerializeObject(model), status);
            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return (Equals(response, null)) ? false : response.booleanModel.disabled;
        }

        /// <summary>
        /// Validate the Social User Login
        /// </summary>
        /// <param name="model">SocialLoginModel</param>
        /// <returns>return details in AccountModel Format</returns>
        public AccountModel SocialUserLogin(SocialLoginModel model, ExpandCollection expands)
        {
            string endpoint = AccountsEndpoint.SocialUserLogin();
            endpoint += BuildEndpointQueryString(expands);
            ApiStatus status = new ApiStatus();
            var response = PostResourceToEndpoint<AccountResponse>(endpoint, JsonConvert.SerializeObject(model), status);
            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.Unauthorized };
            CheckStatusAndThrow<ZnodeUnauthorizedException>(status, expectedStatusCodes);
            return (Equals(response,null)) ? null : response.Account;
        }

        /// <summary>
        /// Creates/Updated the Social User Account
        /// </summary>
        /// <param name="model">SocialLoginModel</param>
        /// <returns>Return true or false</returns>
        public bool SocialUserCreateOrUpdateAccount(SocialLoginModel model)
        {
            string endpoint = AccountsEndpoint.SocialUserCreateOrUpdateAccount();
            ApiStatus status = new ApiStatus();
            var response = PostResourceToEndpoint<TrueFalseResponse>(endpoint, JsonConvert.SerializeObject(model), status);
            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return (Equals(response, null)) ? false : response.booleanModel.disabled;
        }

        /// <summary>
        /// To Get the Registered Social Client Details.
        /// </summary>
        /// <returns>Return details in RegisteredSocialClientListModel format</returns>
        public RegisteredSocialClientListModel GetRegisteredSocialClientDetails()
        {
            string endpoint = AccountsEndpoint.GetRegisteredSocialClientDetailsList();

            ApiStatus status = new ApiStatus();
            var response = GetResourceFromEndpoint<RegisteredSocialClientListResponse>(endpoint, status);
            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);
            return new RegisteredSocialClientListModel { SocialClients = (Equals(response, null)) ? null : response.SocialClients };
        }
    }
}