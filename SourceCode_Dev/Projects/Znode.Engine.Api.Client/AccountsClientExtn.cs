﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    public partial class AccountsClient : BaseClient, IAccountsClient
    {
        public Collection<PRFTSalesRepresentative> GetSalesRepInfoByRoleName(string roleName)
        {
            Collection<PRFTSalesRepresentative> salesRepList = new Collection<PRFTSalesRepresentative>();
            var endpoint = AccountsEndpoint.GetSalesRepInfoByRoleName(roleName);
            try
            {

                var status = new ApiStatus();
                var response = GetResourceFromEndpoint<PRFTAccountIdAndUserNameResponse>(endpoint, status);

                var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
                CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

                salesRepList = (response == null) ? null : response.AccountIdAndUserNameList;
            }
            catch (Exception)
            {
            }
            return salesRepList;
        }
    }
}
