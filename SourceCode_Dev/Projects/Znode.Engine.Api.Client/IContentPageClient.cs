﻿using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IContentPageClient : IBaseClient
    {
        /// <summary>
        /// List of Content Pages.
        /// </summary>
        /// <param name="expands">Expand values for content pages.</param>
        /// <param name="filters">Filter values for content pages.</param>
        /// <param name="sorts">Sort parameters for content pages.</param>
        /// <returns>List of Content page models.</returns>
        ContentPageListModel GetContentPages(ExpandCollection expands, FilterCollection filters, SortCollection sorts);

        /// <summary>
        /// List of content pages.
        /// </summary>
        /// <param name="expands">Expand values for content pages.</param>
        /// <param name="filters">Filter values for content pages.</param>
        /// <param name="sorts">Sort parameters for content pages.</param>
        /// <param name="pageIndex">Start page index of content pages.</param>
        /// <param name="pageSize">Page size of content page list.</param>
        /// <returns>List of Content page models.</returns>
        ContentPageListModel GetContentPages(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Creates a content page.
        /// </summary>
        /// <param name="model">ContentPageModel model</param>
        /// <returns>Created model.</returns>
        ContentPageModel CreateContentPage(ContentPageModel model);

        /// <summary>
        /// Copies a content Page.
        /// </summary>
        /// <param name="model">Content Page model.</param>
        /// <returns>Object.</returns>
        object CopyContentPage(ContentPageModel model);

        /// <summary>
        /// Get the content page by content page id
        /// </summary>
        /// <param name="contentPageId">Id of the content page</param>
        /// <returns>Returns ContentPageModel</returns>
        ContentPageModel GetContentPage(int contentPageId);

        /// <summary>
        /// Adds a content page.
        /// </summary>
        /// <param name="model">Content page model.</param>
        /// <returns>Bool value if contentpage is added or not.</returns>
        bool AddPage(ContentPageModel model);

        /// <summary>
        /// Upadates the existing content page
        /// </summary>
        /// <param name="contentPageId">content page id</param>
        /// <param name="model">content page model to update</param>
        /// <returns>Returns ContentPageModel</returns>
        bool UpdateContentPage(int contentPageId, ContentPageModel model);

        /// <summary>
        /// Delete the existing content page
        /// </summary>
        /// <param name="contentPageId">content page id</param>
        /// <returns>Returns true if content page is deleted else return false</returns>
        bool DeleteContentPage(int contentPageId);

        /// <summary>
        /// Get the content page by content page id
        /// </summary>
        /// <param name="contentPageId">Id of the content page</param>
        /// <param name="expands">expands</param>
        /// <returns>Returns ContentPageModel</returns>
        ContentPageModel GetContentPage(int contentPageId, ExpandCollection expands);

        /// <summary>
        /// Get the content page on the basis of content page name and file extension.
        /// </summary>
        /// <param name="contentPageName">Name of content page.</param>
        /// <param name="extension">Extension of content page file.</param>
        /// <returns>Returns string for content page.</returns>
        string GetContentPageByName(string contentPageName, string extension);
    }
}
