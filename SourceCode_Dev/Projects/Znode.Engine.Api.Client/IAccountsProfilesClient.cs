﻿
namespace Znode.Engine.Api.Client
{
    public interface IAccountsProfilesClient
    {
        /// <summary>
        /// Save multiple profiles Ids with single accountId.
        /// </summary>
        /// <param name="accountId">accountId</param>
        /// <param name="profileIds">profileIds</param>
        /// <returns>True / False</returns>
        bool AccountAssociatedProfiles(int accountId, string profileIds);

        /// <summary>
        /// Delete Existing Account Profile.
        /// </summary>
        /// <param name="accountProfileId">accountProfileId</param>
        /// <returns>True / False</returns>
        bool DeleteAccountProfile(int accountProfileId);
    }
}
