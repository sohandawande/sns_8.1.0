﻿using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;
namespace Znode.Engine.Api.Client
{
    public class ManageSearchIndexClient : BaseClient, IManageSearchIndexClient
    {

        public LuceneIndexListModel GetLuceneIndexStatus(FilterCollection filters, SortCollection sorts)
        {
            return GetLuceneIndexStatus(filters, sorts, null, null);
        }

        public LuceneIndexListModel GetLuceneIndexStatus(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = ManageSearchIndexEndpoint.List();
            endpoint += BuildEndpointQueryString(null, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ManageSearchIndexResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new LuceneIndexListModel { LuceneIndexList = (Equals(response, null)) ? null : response.LuceneIndexList.LuceneIndexList };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public bool CreateIndex()
        {
            var endpoint = ManageSearchIndexEndpoint.Create();

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<ManageSearchIndexResponse>(endpoint, "", status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return (Equals(response, null)) ? false : response.CreateIndex;
        }

        public int DisableTriggers(int flag)
        {
            var endpoint = ManageSearchIndexEndpoint.DisableTriggers(flag);

            var status = new ApiStatus();
            var response = PutResourceToEndpoint<ManageSearchIndexResponse>(endpoint, JsonConvert.SerializeObject(new LuceneIndexModel()), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return (Equals(response, null)) ? 0 : response.DisableFlag;
        }


        public int DisableWinservice(int flag)
        {
            var endpoint = ManageSearchIndexEndpoint.DisableWinservice(flag);

            var status = new ApiStatus();
            var response = PutResourceToEndpoint<ManageSearchIndexResponse>(endpoint, JsonConvert.SerializeObject(new LuceneIndexModel()), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return (Equals(response, null)) ? 0 : response.DisableFlag;
        }


        public LuceneIndexServerListModel GetSearchIndexStatus(int indexId)
        {
            var endpoint = ManageSearchIndexEndpoint.SearchIndexStatus(indexId);
            endpoint += BuildEndpointQueryString(null);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ManageSearchIndexResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new LuceneIndexServerListModel { IndexServerStatusList = (Equals(response, null)) ? null : response.IndexServerList };
            list.MapPagingDataFromResponse(response);

            return list;
        }

    }
}
