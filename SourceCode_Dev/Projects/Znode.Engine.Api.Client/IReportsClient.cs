﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
   public interface IReportsClient:IBaseClient
    {
       /// <summary>
       /// Get Report data set from service by report name
       /// </summary>
       /// <param name="reportName">Report Name</param>
        /// <param name="expands">expands collection</param>
       /// <param name="filters">data filters</param>
       /// <param name="sorts">sort collection</param>
        /// <returns>Returns ReportsDataModel</returns>
       ReportsDataModel GetReportData(string reportName,ExpandCollection expands, FilterCollection filters, SortCollection sorts);

       /// <summary>
       /// Get Report data set from service by report name
       /// </summary>
       /// <param name="reportName">Report Name</param>
       /// <param name="expands">expands collection</param>
       /// <param name="filters">data filters</param>
       /// <param name="sorts">sort collection</param>
       /// <returns>Returns ReportsDataModel</returns>
       ReportsDataModel GetReportData(string reportName,ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
    }
}
