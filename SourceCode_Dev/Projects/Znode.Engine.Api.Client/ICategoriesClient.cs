﻿using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface ICategoriesClient : IBaseClient
	{
		CategoryModel GetCategory(int categoryId);
		CategoryModel GetCategory(int categoryId, ExpandCollection expands);
		CategoryListModel GetCategories(ExpandCollection expands, FilterCollection filters, SortCollection sorts);
		CategoryListModel GetCategories(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
		CategoryListModel GetCategoriesByCatalog(int catalogId, ExpandCollection expands, FilterCollection filters, SortCollection sorts);
		CategoryListModel GetCategoriesByCatalog(int catalogId, ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        CategoryListModel GetCategoriesByCatalogIds(string catalogIds, ExpandCollection expands,
                                                    FilterCollection filters, SortCollection sorts, int? pageIndex,
                                                    int? pageSize);
        CategoryListModel GetCategoriesByCategoryIds(string categoryIds, ExpandCollection expands,
                                                    FilterCollection filters, SortCollection sorts, int? pageIndex,
                                                    int? pageSize);
        CategoryModel CreateCategory(CategoryModel model);
		CategoryModel UpdateCategory(int categoryId, CategoryModel model);

        /// <summary>
        /// Delete category based on category Id
        /// </summary>
        /// <param name="categoryId">Category Id</param>
        /// <returns></returns>
        bool DeleteCategory(int categoryId);

        /// <summary>
        /// Znode Version 8.0
        /// Gets categories associated with specific catalog.
        /// </summary>
        /// <param name="catalogId">The Id of catalog.</param>
        /// <returns>Returns list of categories associated with a catalog.</returns>
        CatalogAssociatedCategoriesListModel GetCategoryByCatalogIdFromCustomService(int catalogId=0,  FilterCollection filters=null,
                                                            SortCollection sorts=null, int? pageIndex=1, int? pageSize=10);

        /// <summary>
        /// Znode Version 8.0
        /// Gets all categories.
        /// </summary>
        /// <param name="expands">ExpandCollection expands</param>
        /// <param name="filters">FilterCollection filters</param>
        /// <param name="sorts">SortCollection sorts</param>
        /// <param name="pageIndex">Nullable int pageIndex</param>
        /// <param name="pageSize">Nullable int pageSize</param>
        /// <returns>Returns List of all categories</returns>
        CatalogAssociatedCategoriesListModel GetAllCategories(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
        
        /// <summary>
        /// Get Product Category on the basis of categoryId and productId
        /// </summary>
        /// <param name="categoryId">int CategoryId</param>
        /// <param name="productIds">int productId</param>
        /// <returns></returns>
        bool CategoryAssociatedProducts(int categoryId, string productIds);

        /// <summary>
        /// To update category SEO Details
        /// </summary>
        /// <param name="categoryId">int categoryId</param>
        /// <param name="model">CategoryModel model</param>
        /// <returns>returns </returns>
        CategoryModel UpdateCategorySEODetails(int categoryId, CategoryModel model);

        /// <summary>
        /// Get the List of Category Associated Products
        /// </summary>
        /// <param name="expands">Expand collection</param>
        /// <param name="filters">Filter collection</param>
        /// <param name="sorts">Sort collection</param>
        /// <param name="pageIndex">Index Of Page</param>
        /// <param name="pageSize">Record Per Page</param>
        /// <returns>List of Products</returns>
        ProductListModel GetCategoryAssociatedProducts(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Get the List of Category Sub-Category & its Products
        /// </summary>
        /// <param name="expands">ExpandCollection expands</param>
        /// <param name="filters">FilterCollection filters</param>
        /// <param name="sorts">SortCollection sorts</param>
        /// <param name="pageIndex">Index Of Page</param>
        /// <param name="pageSize">Record Per Page</param>
        /// <returns>List of Category Sub-Category & its Products</returns>
        CategoryListModel GetCategoryTree(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
	}
}
