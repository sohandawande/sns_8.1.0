﻿using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;


namespace Znode.Engine.Api.Client
{
    public interface IProductReviewHistoryClient
    {
        /// <summary>
        /// This method will get the product review history list.
        /// </summary>
        /// <param name="expands">Expand Collection expands</param>
        /// <param name="filters">Filter criteria</param>
        /// <param name="sorts">sort criteria</param>
        /// <returns>Returns all product review history in ProductReviewHistoryListModel format</returns>
        ProductReviewHistoryListModel GetProductReviewHistory(ExpandCollection expands, FilterCollection filters, SortCollection sorts);

        /// <summary>
        /// This method will get the product review history list.
        /// </summary>
        /// <param name="expands">Expand Collection expands</param>
        /// <param name="filters">Filter criteria</param>
        /// <param name="sorts">sort criteria</param>
        /// <param name="page">page collection</param>
        /// <returns>Returns all product review history in ProductReviewHistoryListModel format</returns>
        ProductReviewHistoryListModel GetProductReviewHistory(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// This method will get the product review history details by productReviewHistoryID.
        /// </summary>
        /// <param name="productReviewHistoryID">int productReviewHistoryID</param>
        /// <returns>Return product review history details in ProductReviewHistoryModel format.</returns>
        ProductReviewHistoryModel GetReviewHistoryById(int productReviewHistoryID);
    }
}
