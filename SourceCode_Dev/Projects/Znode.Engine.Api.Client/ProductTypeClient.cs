﻿using System.Collections.ObjectModel;
using System.Net;
using Newtonsoft.Json;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    public class ProductTypeClient : BaseClient, IProductTypeClient
    {
        public ProductTypeModel GetProductType(int productTypeId)
        {
            return GetProductType(productTypeId, null);
        }

        public ProductTypeModel GetProductType(int productTypeId, ExpandCollection expands)
        {
            var endpoint = ProductTypeEndpoint.Get(productTypeId);
            endpoint += BuildEndpointQueryString(expands);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ProductTypeResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (response == null) ? null : response.ProductType;
        }

        public ProductTypeListModel GetProductTypes(ExpandCollection expands, FilterCollection filters, SortCollection sorts)
        {
            return GetProductTypes(expands, filters, sorts, null, null);
        }

        public ProductTypeListModel GetProductTypes(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = ProductTypeEndpoint.List();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ProductTypeListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new ProductTypeListModel { ProductTypes = (response == null) ? null : response.ProductTypes };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public ProductTypeModel CreateProductType(ProductTypeModel model)
        {
            var endpoint = ProductTypeEndpoint.Create();

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<ProductTypeResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return (response == null) ? null : response.ProductType;
        }

        public ProductTypeModel UpdateProductType(int productTypeId, ProductTypeModel model)
        {
            var endpoint = ProductTypeEndpoint.Update(productTypeId);

            var status = new ApiStatus();
            var response = PutResourceToEndpoint<ProductTypeResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return (response == null) ? null : response.ProductType;
        }

        public bool DeleteProductType(int productTypeId)
        {
            var endpoint = ProductTypeEndpoint.Delete(productTypeId);

            var status = new ApiStatus();
            var deleted = DeleteResourceFromEndpoint<ProductTypeResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

            return deleted;
        }

        public AttributeTypeValueListModel GetProductAttributesByProductTypeId(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = ProductTypeEndpoint.GetProductAttributesByProductTypeId();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ProductTypeListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new AttributeTypeValueListModel { AttributeTypeValueList = (Equals(response, null)) ? null : response.AttributeTypeValuesList };
            list.MapPagingDataFromResponse(response);

            return list;
        }

    }
}
