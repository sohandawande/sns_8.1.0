﻿using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    /// <summary>
    /// Addon Client
    /// </summary>
    public class AddOnClient : BaseClient, IAddOnClient
    {
        #region Public Methods
        public AddOnModel GetAddOn(int addOnId)
        {
            return GetAddOn(addOnId, null);
        }

        public AddOnModel GetAddOn(int addOnId, ExpandCollection expands)
        {
            var endpoint = AddOnEndpoint.Get(addOnId);
            endpoint += BuildEndpointQueryString(expands);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<AddOnResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (Equals(response, null)) ? null : response.AddOn;
        }

        public AddOnListModel GetAddOns(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = AddOnEndpoint.List();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<AddOnListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new AddOnListModel { AddOns = (Equals(response, null)) ? null : response.AddOns };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public AddOnModel CreateAddOn(AddOnModel model)
        {
            var endpoint = AddOnEndpoint.Create();

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<AddOnResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return (Equals(response, null)) ? null : response.AddOn;
        }

        public AddOnModel UpdateAddOn(int addOnId, AddOnModel model)
        {
            var endpoint = AddOnEndpoint.Update(addOnId);

            var status = new ApiStatus();
            var response = PutResourceToEndpoint<AddOnResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return (Equals(response, null)) ? null : response.AddOn;
        }

        public bool DeleteAddOn(int addOnId)
        {
            var endpoint = AddOnEndpoint.Delete(addOnId);

            var status = new ApiStatus();
            var deleted = DeleteResourceFromEndpoint<AddOnResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

            return deleted;
        }
        #endregion
    }
}
