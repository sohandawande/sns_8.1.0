﻿using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IPromotionTypesClient : IBaseClient
	{
		PromotionTypeModel GetPromotionType(int promotionTypeId);
		PromotionTypeListModel GetPromotionTypes(FilterCollection filters, SortCollection sorts);
		PromotionTypeListModel GetPromotionTypes(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
		PromotionTypeModel CreatePromotionType(PromotionTypeModel model);
		PromotionTypeModel UpdatePromotionType(int promotionTypeId, PromotionTypeModel model);
		bool DeletePromotionType(int promotionTypeId);

        /// <summary>
        /// ZNode Version 8.0
        /// Get all Promotion Types not present in database.
        /// </summary>
        /// <param name="filters">FilterCollection filters</param>
        /// <param name="sorts">SortCollection sorts</param>
        /// <param name="pageIndex">Nullable int pageIndex</param>
        /// <param name="pageSize">Nullable int pageSize</param>
        /// <returns>Returns promotion type list</returns>
        PromotionTypeListModel GetAllPromotionTypesNotInDatabase(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// ZNode Version 8.0
        /// Get Franchise Admin Access Promotion Types.
        /// </summary>
        /// <param name="filters">Filter collection</param>
        /// <param name="sorts">Sort Collection</param>
        /// <param name="pageIndex">int page index</param>
        /// <param name="pageSize">int page size</param>
        /// <returns></returns>
        PromotionTypeListModel GetFranchisePromotionTypes(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
	}
}
