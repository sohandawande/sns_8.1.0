﻿using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Engine.Api.Client
{
    public class LoadPluginsClient : BaseClient, ILoadPluginsClient
    {
        public bool IsPluginsLoaded()
        {
            var endpoint = LoadPluginsEndpoint.IsPluginsLoaded();
            var status = new ApiStatus();
            var isValid = GetBooleanResourceFromEndpoint<BaseResponse>(endpoint, status);
            return isValid;
        }
    }
}
