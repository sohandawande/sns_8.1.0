﻿using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IPaymentTypesClient : IBaseClient
	{
		PaymentTypeModel GetPaymentType(int paymentTypeId);
		PaymentTypeListModel GetPaymentTypes(FilterCollection filters, SortCollection sorts);
		PaymentTypeListModel GetPaymentTypes(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
		PaymentTypeModel CreatePaymentType(PaymentTypeModel model);
		PaymentTypeModel UpdatePaymentType(int paymentTypeId, PaymentTypeModel model);
		bool DeletePaymentType(int paymentTypeId);
	}
}
