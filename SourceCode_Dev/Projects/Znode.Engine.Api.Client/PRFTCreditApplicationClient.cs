﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;
using Znode.Engine.Api.Models.Extensions;

namespace Znode.Engine.Api.Client
{
    public class PRFTCreditApplicationClient : BaseClient, IPRFTCreditApplicationClient
    {
        public PRFTCreditApplicationModel GetCreditApplication(int creditApplicationId)
        {
            var endpoint = PRFTCreditApplicationEndpoint.Get(creditApplicationId);
            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<PRFTCreditApplicationResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (response == null) ? null : response.CreditApplication;
        }

        public PRFTCreditApplicationListModel GetCreditApplications(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = PRFTCreditApplicationEndpoint.List();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<PRFTCreditApplicationListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new PRFTCreditApplicationListModel { CreditApplications = (response == null) ? null : response.CreditApplications };
            list.MapPagingDataFromResponse(response);
            return list;
        }
        
        public PRFTCreditApplicationModel CreateCreditApplication(PRFTCreditApplicationModel model)
        {
            var endpoint = PRFTCreditApplicationEndpoint.Create();

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<PRFTCreditApplicationResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return (response == null) ? null : response.CreditApplication;
        }
        
        public PRFTCreditApplicationModel UpdateCreditApplication(int creditApplicationId, PRFTCreditApplicationModel model)
        {
            var endpoint = PRFTCreditApplicationEndpoint.Update(creditApplicationId);

            var status = new ApiStatus();
            var response = PutResourceToEndpoint<PRFTCreditApplicationResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return (response == null) ? null : response.CreditApplication;
        }
    }
}
