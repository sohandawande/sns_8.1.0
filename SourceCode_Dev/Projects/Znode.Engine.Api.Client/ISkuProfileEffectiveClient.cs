﻿using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface ISkuProfileEffectiveClient : IBaseClient
    {
        /// <summary>
        /// Get sku profile effective by sku profile effective id.
        /// </summary>
        /// <param name="skuProfileEffectiveId">Id of sku Profile Effective to fetch the sku profile effective</param>
        /// <returns>Returns SkuProfileEffectiveModel with data related to sku profile.</returns>
        SkuProfileEffectiveModel GetSkuProfileEffective(int skuProfileEffectiveId);

        /// <summary>
        /// Get sku profile effective list by sku profile effective id.
        /// </summary>
        /// <param name="skuId">Id of the sku to get the list.</param>
        /// <returns>List of sku profile effective on the sku id.</returns>
        SkuProfileEffectiveListModel GetSkuProfileEffectiveBySkuId(int skuId, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Get sku profile effective by sku profile effective id with exapnds.
        /// </summary>
        /// <param name="skuProfileEffectiveId">Id of sku Profile Effective to get the sku profile effective model.</param>
        /// <param name="expands">Exapnd collections for sku profile effective.</param>
        /// <returns>Returns SkuProfileEffectiveModel with data related to sku profile with expands.</returns>
        SkuProfileEffectiveModel GetSkuProfileEffective(int skuProfileEffectiveId, ExpandCollection expands);

        /// <summary>
        /// Get sku profile effective list with expands, filters and sort collection.
        /// </summary>
        /// <param name="expands">Exapnd collection for sku profile effective.</param>
        /// <param name="filters">Filter collection for sku profile effective.</param>
        /// <param name="sorts">Sort collection for sku profile effective.</param>
        /// <returns>Returns list of SkuProfileEffectiveListModel with expands, filters, and sorts collection.</returns>
        SkuProfileEffectiveListModel GetSkuProfileEffectives(ExpandCollection expands, FilterCollection filters, SortCollection sorts);

        /// <summary>
        /// Get sku profile effective list with expands, filters, sorts, page index, and page size.
        /// </summary>
        /// <param name="expands">Exapnd collection for sku profile effective.</param>
        /// <param name="filters">Filter collection for sku profile effective.</param>
        /// <param name="sorts">Sort collection for sku profile effective.</param>
        /// <param name="pageIndex">Page index.</param>
        /// <param name="pageSize">Page size.</param>
        /// <returns>Returns list of SkuProfileEffectiveListModel with expands, filters, and sorts collection.</returns>
        SkuProfileEffectiveListModel GetSkuProfileEffectives(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Create Sku Profile Effective.
        /// </summary>
        /// <param name="model">Model to create.</param>
        /// <returns>Returns the created SkuProfileEffectiveModel</returns>
        SkuProfileEffectiveModel CreateSkuProfileEffective(SkuProfileEffectiveModel model);

        /// <summary>
        ///  Update existing Sku Profile Effective.
        /// </summary>
        /// <param name="skuProfileEffectiveId">Id of sku profile effective to update.</param>
        /// <param name="model">model to update.</param>
        /// <returns>Returns the newly updated SkuProfileEffectiveModel</returns>
        SkuProfileEffectiveModel UpdateSkuProfileEffective(int skuProfileEffectiveId, SkuProfileEffectiveModel model);

        /// <summary>
        /// Delete existing Sku Profile Effective.
        /// </summary>
        /// <param name="skuProfileEffectiveId">Id of sku Profile Effective to delete.</param>
        /// <returns>Returns true if model is deleted else return false.</returns>
        bool DeleteSkuProfileEffective(int skuProfileEffectiveId);
    }
}
