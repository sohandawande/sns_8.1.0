﻿using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    /// <summary>
    /// AddOn Value Interface 
    /// </summary>
    public interface IAddOnValueClient : IBaseClient
    {
        /// <summary>
        /// Get AddOn value By AddOn value Id 
        /// </summary>
        /// <param name="addOnValueId">int addOnValueId</param>
        /// <returns>Returns the model of AddOn value</returns>
        AddOnValueModel GetAddOnValue(int addOnValueId);

        /// <summary>
        /// Get AddOn value By AddOn value Id and Expands
        /// </summary>
        /// <param name="addOnValueId">int addOnValueId</param>
        /// <param name="expands">ExpandCollection expands</param>
        /// <returns>Returns the model of AddOn value</returns>
        AddOnValueModel GetAddOnValue(int addOnValueId, ExpandCollection expands);

        /// <summary>
        /// Get Addon vlaues associated with addon Id
        /// </summary>
        /// <param name="expands">ExpandCollection expands</param>
        /// <param name="filters">FilterCollection filters</param>
        /// <param name="sorts">SortCollection sorts</param>
        /// <param name="pageIndex">Nullable int pageIndex</param>
        /// <param name="pageSize">Nullable int pageSize</param>
        /// <returns>Returns Addon value list model</returns>
        AddOnValueListModel GetAddOnValueByAddOnId(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Create Addon Value
        /// </summary>
        /// <param name="model">AddOnValueModel model</param>
        /// <returns>Returns the model of AddOn value</returns>
        AddOnValueModel CreateAddOnValue(AddOnValueModel model);

        /// <summary>
        /// Update AddOn value
        /// </summary>
        /// <param name="addOnValueId">int addOnValueId</param>
        /// <param name="model">AddOnValueModel model</param>
        /// <returns>Returns the model of AddOn value</returns>
        AddOnValueModel UpdateAddOnValue(int addOnValueId, AddOnValueModel model);

        /// <summary>
        /// Delete AddOn value
        /// </summary>
        /// <param name="addOnValueId">int addOnValueId</param>
        /// <returns>Returns true or false</returns>
        bool DeleteAddOnValue(int addOnValueId);
    }
}
