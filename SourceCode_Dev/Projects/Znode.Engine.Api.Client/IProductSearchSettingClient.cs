﻿using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IProductSearchSettingClient : IBaseClient
    {
        /// <summary>
        /// Get product level settings
        /// </summary>
        /// <param name="expands">ExpandCollection expands</param>
        /// <param name="filters">FilterCollection filters</param>
        /// <param name="sorts">SortCollection sorts</param>
        /// <param name="pageIndex">Nullable int pageIndex</param>
        /// <param name="pageSize">Nullable int pageSize</param>
        /// <returns>Return Product level setting list model</returns>
        ProductLevelSettingListModel GetProductLevelSettings(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Get category level settings
        /// </summary>
        /// <param name="expands">ExpandCollection expands</param>
        /// <param name="filters">FilterCollection filters</param>
        /// <param name="sorts">SortCollection sorts</param>
        /// <param name="pageIndex">Nullable int pageIndex</param>
        /// <param name="pageSize">Nullable int pageSize</param>
        /// <returns>Return Category level setting list model</returns>
        CategoryLevelSettingListModel GetCategoryLevelSettings(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Get field level settings
        /// </summary>
        /// <param name="expands">ExpandCollection expands</param>
        /// <param name="filters">FilterCollection filters</param>
        /// <param name="sorts">SortCollection sorts</param>
        /// <param name="pageIndex">Nullable int pageIndex</param>
        /// <param name="pageSize">Nullable int pageSize</param>
        /// <returns>Return field level setting list model</returns>
        FieldLevelSettingListModel GetFieldLevelSettings(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Save product search boost values for all sections like Category, Product and Field.
        /// </summary>
        /// <param name="model">BoostSearchSetting model</param>
        /// <returns>true/false</returns>
        bool SaveBoostValues(BoostSearchSettingModel model);
    }
}
