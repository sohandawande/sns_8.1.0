﻿using System.Collections.ObjectModel;
using System.Net;
using Newtonsoft.Json;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
	public class GiftCardsClient : BaseClient, IGiftCardsClient
	{
		public GiftCardModel GetGiftCard(int giftCardId)
		{
			return GetGiftCard(giftCardId, null);
		}

		public GiftCardModel GetGiftCard(int giftCardId, ExpandCollection expands)
		{
			var endpoint = GiftCardsEndpoint.Get(giftCardId);
			endpoint += BuildEndpointQueryString(expands);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<GiftCardResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

			return (response == null) ? null : response.GiftCard;
		}

		public GiftCardListModel GetGiftCards(ExpandCollection expands, FilterCollection filters, SortCollection sorts)
		{
			return GetGiftCards(expands, filters, sorts, null, null);
		}

		public GiftCardListModel GetGiftCards(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
		{
			var endpoint = GiftCardsEndpoint.List();
			endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<GiftCardListResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new GiftCardListModel { GiftCards = (response == null) ? null : response.GiftCards };
			list.MapPagingDataFromResponse(response);

			return list;
		}

		public GiftCardModel CreateGiftCard(GiftCardModel model)
		{
			var endpoint = GiftCardsEndpoint.Create();

			var status = new ApiStatus();
			var response = PostResourceToEndpoint<GiftCardResponse>(endpoint, JsonConvert.SerializeObject(model), status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

			return (response == null) ? null : response.GiftCard;
		}

		public GiftCardModel UpdateGiftCard(int giftCardId, GiftCardModel model)
		{
			var endpoint = GiftCardsEndpoint.Update(giftCardId);

			var status = new ApiStatus();
			var response = PutResourceToEndpoint<GiftCardResponse>(endpoint, JsonConvert.SerializeObject(model), status);
			
			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

			return (response == null) ? null : response.GiftCard;
		}

		public bool DeleteGiftCard(int giftCardId)
		{
			var endpoint = GiftCardsEndpoint.Delete(giftCardId);

			var status = new ApiStatus();
			var deleted = DeleteResourceFromEndpoint<GiftCardResponse>(endpoint, status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

			return deleted;
		}

        public bool IsGiftCardValid(string giftCard,int accountId)
        {
            var endpoint = GiftCardsEndpoint.IsGiftCardValid(giftCard,accountId);
            var status = new ApiStatus();
            var isValid = GetBooleanResourceFromEndpoint<GiftCardResponse>(endpoint, status);
            return isValid;
        }

        public GiftCardModel GetNextGiftCardNumber()
        {
            var endpoint = GiftCardsEndpoint.GetNextGiftCardNumber();
            var status = new ApiStatus();
            
            var response = GetResourceFromEndpoint<GiftCardResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (Equals(response, null)) ? null : response.GiftCard;
        }
	}
}
