﻿using Newtonsoft.Json;
using System;
using System.Collections.ObjectModel;
using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    public class ProductReviewHistoryClient:BaseClient,IProductReviewHistoryClient
    {

        public ProductReviewHistoryListModel GetProductReviewHistory(ExpandCollection expands, FilterCollection filters, SortCollection sorts)
        {
            return GetProductReviewHistory(expands, filters, sorts, null, null);
        }

        public ProductReviewHistoryListModel GetProductReviewHistory(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = VendorProductEndpoint.GetProductReviewHistory();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ProductReviewHistoryListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new ProductReviewHistoryListModel { ProductReviewHistory = (Equals(response, null)) ? null : response.ProductsHistory };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public ProductReviewHistoryModel GetReviewHistoryById(int productReviewHistoryID)
        {
            var endpoint = VendorProductEndpoint.GetReviewHistoryById(productReviewHistoryID);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ProductReviewHistoryResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (Equals(response, null)) ? null : response.ProductReview;
        }
    }
}
