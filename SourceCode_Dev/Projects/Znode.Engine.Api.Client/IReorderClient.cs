﻿using System;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IReorderClient : IBaseClient
    {
        /// <summary>
        /// Znode Version 7.2.2
        /// Get the reorder cart items list. on the basis of orderId. 
        /// function helps to place complite order as reorder.
        /// </summary>
        /// <param name="orderId">orderId</param>
        /// <returns>returns CartItemsListModel as response</returns>
        CartItemsListModel GetReorderItems(int orderId);

        /// <summary>
        /// Znode Version 7.2.2
        /// Get the reorder cart single item . on the basis of orderLineItemId.
        /// function help to place order item as reorder.
        /// </summary>
        /// <param name="orderLineItemId">orderLineItemId</param>
        /// <returns>returns CartItemsModel as response</returns>
        CartItemsModel GetReorderLineItem(int orderLineItemId);

        /// <summary>
        /// Znode Version 7.2.2
        /// Get reorder items list.
        /// This function helps to reorder complite order as well single item. 
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="orderLineItemId"></param>
        /// <param name="isOrder"></param>
        /// <param name="routeUri"></param>
        /// <param name="routeTemplate"></param>
        /// <returns></returns>
        CartItemsListModel GetReorderItemsList(int orderId, int orderLineItemId, bool isOrder);

    }
}
