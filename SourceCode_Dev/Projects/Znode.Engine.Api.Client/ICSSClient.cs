﻿using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface ICSSClient : IBaseClient
    {
        /// <summary>
        /// Gets Css List.
        /// </summary>
        /// <param name="expands">Expands for Css List.</param>
        /// <param name="filters">Filters for Css List.</param>
        /// <param name="sorts">Sort for Css List.</param>
        /// <returns>Css List Model.</returns>
        CSSListModel GetCSSs(ExpandCollection expands, FilterCollection filters, SortCollection sorts);

        /// <summary>
        /// Gets CSS List.
        /// </summary>
        /// <param name="expands">Expands for Css List.</param>
        /// <param name="filters">Filters for Css List.</param>
        /// <param name="sorts">Sorts for Css List.</param>
        /// <param name="pageIndex">Page index for Css List.</param>
        /// <param name="pageSize">Page Size for Css List.</param>
        /// <returns>CSS List.</returns>
        CSSListModel GetCSSs(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Create new CSS
        /// </summary>
        /// <param name="model">CSS Model</param>
        /// <returns>CSS Model</returns>
        CSSModel CreateCSS(CSSModel model);

        /// <summary>
        /// Get css by cssID
        /// </summary>
        /// <param name="cssId">cssID to get CSS</param>
        /// <returns>CSS Model</returns>
        CSSModel GetCSS(int cssId);

        /// <summary>
        /// Update CSS
        /// </summary>
        /// <param name="cssId">cssID to get CSS</param>
        /// <param name="model">CSSModel</param>
        /// <returns>Css Model</returns>
        CSSModel UpdateCSS(int cssId, CSSModel model);

        /// <summary>
        /// Delete CSS
        /// </summary>
        /// <param name="cssId">CSS ID to delete CSS</param>
        /// <returns>boolean value true/false</returns>
        bool DeleteCSS(int cssId);
    }
}
