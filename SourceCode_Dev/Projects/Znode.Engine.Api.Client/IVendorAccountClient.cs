﻿using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IVendorAccountClient:IBaseClient
    {
        /// <summary>
        /// /// This method will get the vendor Account list
        /// </summary>
        /// <param name="expands">Expand Collection expands</param>
        /// <param name="filters">Filter criteria</param>
        /// <param name="sorts">sort criteria</param>
        /// <param name="page">page collection</param>
        /// <returns>Returns all vendor account details in VendorAccountListModel format</returns>
        VendorAccountListModel GetVendorAccountList(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Create new Vendor Account.
        /// </summary>
        /// <param name="model">AccountModel</param>
        /// <returns>AccountModel</returns>
        AccountModel CreateVendorAccount(AccountModel model);

        /// <summary>
        /// Update existing vendor account.
        /// </summary>
        /// <param name="accountId">account id</param>
        /// <param name="model">Account Model</param>
        /// <returns>returns AccountModel</returns>
        AccountModel UpdateVendorAccount(int accountId, AccountModel model);

        /// <summary>
        /// Get vendor account details by account id.
        /// </summary>
        /// <param name="accountid">Int accountId to get vendor account id</param>
        /// <returns>VendorAccountModel</returns>
        VendorAccountModel GetVendorAccountById(int accountid);
    }
}
