﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IManageSearchIndexClient : IBaseClient
    {
        /// <summary>
        /// Get Lucene index list
        /// </summary>
        /// <param name="filters"> filter collection</param>
        /// <param name="sorts">sort collection</param>
        /// <returns>Returns LuceneIndexListModel</returns>
        LuceneIndexListModel GetLuceneIndexStatus(FilterCollection filters, SortCollection sorts);

        /// <summary>
        ///Get Lucene index list
        /// </summary>
        /// <param name="filters">filter collection</param>
        /// <param name="sorts">sort collection</param>
        /// <param name="pageIndex">page index</param>
        /// <param name="pageSize">paging size</param>
        /// <returns></returns>
        LuceneIndexListModel GetLuceneIndexStatus(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Create Lucene index
        /// </summary>
        /// <returns>true/flase</returns>
        bool CreateIndex();

        /// <summary>
        /// Disable/Enable Lucene trigger
        /// </summary>
        /// <param name="flag">int flag</param>
        /// <returns>int flag</returns>
        int DisableTriggers(int flag);

        /// <summary>
        /// Disable/Enable Lucene windows service
        /// </summary>
        /// <param name="flag">int flag</param>
        /// <returns>int flag</returns>
        int DisableWinservice(int flag);

        /// <summary>
        /// Get Index Server log list
        /// </summary>
        /// <param name="indexId">index Id</param>
        /// <returns>Returns LuceneIndexServerListModel</returns>
        LuceneIndexServerListModel GetSearchIndexStatus(int indexId);
    }
}
