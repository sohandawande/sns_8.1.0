﻿using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    public class UrlRedirectClient : BaseClient, IUrlRedirectClient
    {
        public UrlRedirectListModel Get301Redirects(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = UrlRedirectEndPoint.List();
            endpoint += BuildEndpointQueryString(null, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<UrlRedirectListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new UrlRedirectListModel { UrlRedirects = Equals(response, null) ? null : response.UrlRedirects };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public UrlRedirectModel GetUrlRedirect(int urlRedirectId)
        {
            return GetUrlRedirect(urlRedirectId, null);
        }

        public UrlRedirectModel GetUrlRedirect(int urlRedirectId, ExpandCollection expands)
        {
            var endpoint = UrlRedirectEndPoint.Get(urlRedirectId);
            endpoint += BuildEndpointQueryString(expands);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<UrlRedirectResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return Equals(response, null) ? null : response.UrlRedirect;
        }

        public UrlRedirectModel CreateUrlRedirect(UrlRedirectModel model)
        {
            var endpoint = UrlRedirectEndPoint.Create();

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<UrlRedirectResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return Equals(response, null) ? null : response.UrlRedirect;
        }

        public UrlRedirectModel UpdateUrlRedirect(int urlRedirectId, UrlRedirectModel model)
        {
            var endpoint = UrlRedirectEndPoint.Update(urlRedirectId);

            var status = new ApiStatus();
            var response = PutResourceToEndpoint<UrlRedirectResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return Equals(response, null) ? null : response.UrlRedirect;
        }

        public bool DeleteUrlRedirect(int urlRedirectId)
        {
            var endpoint = UrlRedirectEndPoint.Delete(urlRedirectId);

            var status = new ApiStatus();
            var deleted = DeleteResourceFromEndpoint<UrlRedirectResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

            return deleted;
        }
    }
}
