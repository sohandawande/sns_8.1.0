﻿using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IGiftCardsClient : IBaseClient
	{
		GiftCardModel GetGiftCard(int giftCardId);
		GiftCardModel GetGiftCard(int giftCardId, ExpandCollection expands);
		GiftCardListModel GetGiftCards(ExpandCollection expands, FilterCollection filters, SortCollection sorts);
		GiftCardListModel GetGiftCards(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
		GiftCardModel CreateGiftCard(GiftCardModel model);
		GiftCardModel UpdateGiftCard(int giftCardId, GiftCardModel model);
		bool DeleteGiftCard(int giftCardId);
        
        /// <summary>
        ///  Znode Version 8.0
        ///  Get Next GiftCardNumber to create new GiftCard.
        /// </summary>
        /// <returns>Returns giftcard model</returns>
        GiftCardModel GetNextGiftCardNumber();

        /// <summary>
        /// To check gift card is valid or not.
        /// </summary>
        /// <param name="giftCard">Gift card number</param>
        /// <param name="accountId">logged in user account id</param>
        /// <returns>true / false</returns>
        bool IsGiftCardValid(string giftCard, int accountId);
	}
}
