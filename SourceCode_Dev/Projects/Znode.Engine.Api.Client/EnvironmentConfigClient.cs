﻿using System.Collections.ObjectModel;
using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    public class EnvironmentConfigClient : BaseClient, IEnvironmentConfigClient
    {
        public EnvironmentConfigModel GetEnvironmentConfig()
        {
            var endpoint = EnvironmentConfigEndpoint.GetEnvironmentConfig();

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<EnvironmentConfigResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return Equals(response, null) ? null : response.EnvironmentConfig;
        }
    }
}
