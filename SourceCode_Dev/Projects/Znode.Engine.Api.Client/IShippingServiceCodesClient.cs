﻿using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    /// <summary>
    /// Shipping Service Codes Client Interface
    /// </summary>
    public interface IShippingServiceCodesClient : IBaseClient
    {
        /// <summary>
        /// Get Shipping Service Code by Shipping Service Code Id
        /// </summary>
        /// <param name="shippingServiceCodeId">shippingServiceCodeId</param>
        /// <returns>Returns Shipping Service Code</returns>
        ShippingServiceCodeModel GetShippingServiceCode(int shippingServiceCodeId);

        /// <summary>
        /// Get Shipping Service Code by Shipping Service Code Id
        /// </summary>
        /// <param name="shippingServiceCodeId">shippingServiceCodeId</param>
        /// <param name="expands">expands</param>
        /// <returns>Returns Shipping Service Code</returns>
        ShippingServiceCodeModel GetShippingServiceCode(int shippingServiceCodeId, ExpandCollection expands);

        /// <summary>
        /// Gets list of Shipping Service Codes
        /// </summary>
        /// <param name="expands">expands</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <returns>Returns list of Shipping Service Codes</returns>
        ShippingServiceCodeListModel GetShippingServiceCodes(ExpandCollection expands, FilterCollection filters, SortCollection sorts);

        /// <summary>
        /// Gets list of Shipping Service Codes 
        /// </summary>
        /// <param name="expands">expands</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <param name="pageIndex">Index of page</param>
        /// <param name="pageSize">Size of page</param>
        /// <returns>Returns list of Shipping Service Codes</returns>
        ShippingServiceCodeListModel GetShippingServiceCodes(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Create Shipping Service Code
        /// </summary>
        /// <param name="model">ShippingServiceCodeModel model</param>
        /// <returns>Returns created Shipping Service Code Model</returns>
        ShippingServiceCodeModel CreateShippingServiceCode(ShippingServiceCodeModel model);

        /// <summary>
        /// Update Shipping Service Code
        /// </summary>
        /// <param name="shippingServiceCodeId">shippingServiceCodeId</param>
        /// <param name="model">ShippingServiceCodeModel model</param>
        /// <returns>Returns updated Shipping Service Code Model</returns>
        ShippingServiceCodeModel UpdateShippingServiceCode(int shippingServiceCodeId, ShippingServiceCodeModel model);

        /// <summary>
        /// Delete Shipping Service Code on the basis of shipping service code id
        /// </summary>
        /// <param name="shippingServiceCodeId">shippingServiceCodeId</param>
        /// <returns>Returns true/false</returns>
        bool DeleteShippingServiceCode(int shippingServiceCodeId);
    }
}
