﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IRMAConfigurationClient : IBaseClient
    {
        /// <summary>
        /// Method gets the RMAConfiguration details.
        /// </summary>
        /// <param name="rmaConfigId">The Id of RMAConfiguration</param>
        /// <returns>Returns RMAConfiguration details.</returns>
        RMAConfigurationModel GetRMAConfiguration(int rmaConfigId);

        /// <summary>
        /// Method gets the RMAConfiguration details.
        /// </summary>
        /// <param name="rmaConfigId">The Id of RMAConfiguration</param>
        /// <param name="expands">Expands collection</param>
        /// <returns>Returns RMAConfiguration details.</returns>
        RMAConfigurationModel GetRMAConfiguration(int rmaConfigId, ExpandCollection expands);

        /// <summary>
        /// Get the lsit of all RMAConfigurations
        /// </summary>
        /// <returns>ListModel of RMAConfiguration</returns>
        RMAConfigurationListModel GetRMAConfigurations();

        /// <summary>
        /// Method Creates the Profile.
        /// </summary>
        /// <param name="model">Model of type RMAConfigurationModel</param>
        /// <returns>Return the RMAConfiguration.</returns>
        RMAConfigurationModel CreateRMAConfiguration(RMAConfigurationModel model);

        /// <summary>
        /// Method Updates the RMAConfiguration.
        /// </summary>
        /// <param name="rmaConfigId">The Id of RMAConfiguration</param>
        /// <param name="model">Model of type RMAConfigurationModel</param>
        /// <returns>Returns Updated RMAConfiguration.</returns>
        RMAConfigurationModel UpdateRMAConfiguration(int rmaConfigId, RMAConfigurationModel model);

        /// <summary>
        /// Get the lsit of all RMAConfigurations
        /// </summary>
        /// <returns>returns ListModel of RMAConfiguration</returns>
        RMAConfigurationListModel GetAllRMAConfigurations();
    }
}
