﻿using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    public class ThemeClient : BaseClient, IThemeClient
    {
        public ThemeListModel GetThemes(FilterCollection filters, SortCollection sorts)
        {
            string endpoint = ThemeEndPoint.List();
            endpoint += BuildEndpointQueryString(null,filters, sorts,null,null);

            ApiStatus status = new ApiStatus();
            var response = GetResourceFromEndpoint<ThemeListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            ThemeListModel list = new ThemeListModel { Themes = Equals(response, null) ? null : response.Themes };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public ThemeListModel GetThemes(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            string endpoint = ThemeEndPoint.List();
            endpoint += BuildEndpointQueryString(null, filters, sorts, pageIndex, pageSize);

            ApiStatus status = new ApiStatus();
            var response = GetResourceFromEndpoint<ThemeListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            ThemeListModel list = new ThemeListModel { Themes = Equals(response, null) ? null : response.Themes };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public ThemeModel CreateTheme(ThemeModel model)
        {
            string endpoint = ThemeEndPoint.Create();

            ApiStatus status = new ApiStatus();
            var response = PostResourceToEndpoint<ThemeResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return (Equals(response, null)) ? null : response.Theme;
        }

        public ThemeModel GetTheme(int themeId)
        {
            var endpoint = ThemeEndPoint.Get(themeId);

            ApiStatus status = new ApiStatus();
            var response = GetResourceFromEndpoint<ThemeResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (Equals(response, null)) ? null : response.Theme;
        }

        public ThemeModel UpdateTheme(int themeId, ThemeModel model)
        {
            var endpoint = ThemeEndPoint.Update(themeId);

            ApiStatus status = new ApiStatus();
            var response = PutResourceToEndpoint<ThemeResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return (Equals(response, null)) ? null : response.Theme;
        }

        public bool DeleteTheme(int themeId)
        {
            var endpoint = ThemeEndPoint.Delete(themeId);

            ApiStatus status = new ApiStatus();
            var deleted = DeleteResourceFromEndpoint<ThemeResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

            return deleted;
        }
    }
}
