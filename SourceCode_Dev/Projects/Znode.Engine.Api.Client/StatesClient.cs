﻿using System.Collections.ObjectModel;
using System.Net;
using Newtonsoft.Json;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
	public class StatesClient : BaseClient, IStatesClient
	{
		public StateModel GetState(string stateCode)
		{
			var endpoint = StatesEndpoint.Get(stateCode);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<StateResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

			return (response == null) ? null : response.State;
		}

		public StateListModel GetStates(FilterCollection filters, SortCollection sorts)
		{
			return GetStates(filters, sorts, null, null);
		}

		public StateListModel GetStates(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
		{
			var endpoint = StatesEndpoint.List();
			endpoint += BuildEndpointQueryString(null, filters, sorts, pageIndex, pageSize);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<StateListResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new StateListModel { States = (response == null) ? null : response.States };
			list.MapPagingDataFromResponse(response);

			return list;
		}

		public StateModel CreateState(StateModel model)
		{
			var endpoint = StatesEndpoint.Create();

			var status = new ApiStatus();
			var response = PostResourceToEndpoint<StateResponse>(endpoint, JsonConvert.SerializeObject(model), status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

			return (response == null) ? null : response.State;
		}

		public StateModel UpdateState(string stateCode, StateModel model)
		{
			var endpoint = StatesEndpoint.Update(stateCode);

			var status = new ApiStatus();
			var response = PutResourceToEndpoint<StateResponse>(endpoint, JsonConvert.SerializeObject(model), status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

			return (response == null) ? null : response.State;
		}

		public bool DeleteState(string stateCode)
		{
			var endpoint = StatesEndpoint.Delete(stateCode);

			var status = new ApiStatus();
			var deleted = DeleteResourceFromEndpoint<StateResponse>(endpoint, status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

			return deleted;
		}
	}
}
