﻿using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.Data;
using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    public partial class OrdersClient : BaseClient, IOrdersClient
    {
        #region Public Methods
        public OrderModel GetOrder(int orderId)
        {
            return GetOrder(orderId, null);
        }

        public OrderModel GetOrder(int orderId, ExpandCollection expands)
        {
            var endpoint = OrdersEndpoint.Get(orderId);
            endpoint += BuildEndpointQueryString(expands);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<OrderResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return Equals(response, null) ? null : response.Order;
        }

        public OrderListModel GetOrders(ExpandCollection expands, FilterCollection filters, SortCollection sorts)
        {
            return GetOrders(expands, filters, sorts, null, null);
        }

        public OrderListModel GetOrders(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = OrdersEndpoint.List();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<OrderListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new OrderListModel { Orders = (Equals(response, null)) ? null : response.Orders };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public OrderModel CreateOrder(ShoppingCartModel model)
        {
            var endpoint = OrdersEndpoint.Create();

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<OrderResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.Created };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return Equals(response, null) ? null : response.Order;
        }

        public OrderModel UpdateOrder(int orderId, OrderModel model)
        {
            var endpoint = OrdersEndpoint.Update(orderId);

            var status = new ApiStatus();
            var response = PutResourceToEndpoint<OrderResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return Equals(response, null) ? null : response.Order;
        }
        #endregion

        #region ZNode Version 8.0
        public AdminOrderListModel GetOrderList(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = OrdersEndpoint.OrderList();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<AdminOrderListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new AdminOrderListModel { OrderList = (Equals(response, null)) ? null : response.OrderList };
            list.MapPagingDataFromResponse(response);
            return list;

        }

        public OrderModel GetOrderDetails(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = OrdersEndpoint.OrderDetails();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<OrderResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return Equals(response, null) ? null : response.Order;

        }

        public OrderModel UpdateOrderStatus(int orderId, OrderModel model)
        {
            var endpoint = OrdersEndpoint.UpdateOrderStatus(orderId);

            var status = new ApiStatus();
            var response = PutResourceToEndpoint<OrderResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return Equals(response, null) ? null : response.Order;
        }

        public OrderResponse VoidPayment(int orderId, OrderModel model)
        {
            var endpoint = OrdersEndpoint.VoidPayment(orderId);
            ZnodeDeleteModel responseDelete = new ZnodeDeleteModel();
            var status = new ApiStatus();
            var response = PutResourceToEndpoint<OrderResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            responseDelete.IsDeleted = Equals(response.Order, null) ? false : true;
            response.ErrorMessage = responseDelete.IsDeleted ? string.Empty : status.ErrorMessage;       

            return response;
        }

        public OrderResponse RefundPayment(int orderId, OrderModel model)
        {
            var endpoint = OrdersEndpoint.RefundPayment(orderId);
            ZnodeDeleteModel responseDelete = new ZnodeDeleteModel();
            var status = new ApiStatus();
            var response = PutResourceToEndpoint<OrderResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            responseDelete.IsDeleted = Equals(response.Order, null) ? false : true;
            response.ErrorMessage = responseDelete.IsDeleted ? string.Empty : status.ErrorMessage;

            return response;
        }

        public DataSet DownloadOrderData(FilterCollection filters = null)
        {
            var endpoint = OrdersEndpoint.OrderDownload();
            endpoint += BuildEndpointQueryString(null, filters, null, null, null);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<OrderResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return Equals(response, null) ? null : response.OrderDownloadData;
        }

        public DataSet DownloadOrderLineItemData(FilterCollection filters = null)
        {
            var endpoint = OrdersEndpoint.OrderLineItemDownload();
            endpoint += BuildEndpointQueryString(null, filters, null, null, null);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<OrderResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return Equals(response, null) ? null : response.OrderDownloadData;
        } 

        public AddressListResponse AddNewCustomer(AddressListModel listModel)
        {
            var endpoint = OrdersEndpoint.AddNewCustomer();
            var status = new ApiStatus();
            var response = PostResourceToEndpoint<AddressListResponse>(endpoint, JsonConvert.SerializeObject(listModel), status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.Created };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (Equals(response, null)) ? null : response;
        }

        public AddressListResponse UpdateCustomerAddress(int accountId , AddressListModel listModel)
        {
            var endpoint = OrdersEndpoint.UpdateCustomerAddress(accountId);
            var status = new ApiStatus();
            var response = PostResourceToEndpoint<AddressListResponse>(endpoint, JsonConvert.SerializeObject(listModel), status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.Created };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (Equals(response, null)) ? null : response;
        }

        public OrderLineItemModel GetOrderLineItems(int orderLineItemId)
        {
            var endpoint = OrdersEndpoint.GetOrderLineItem(orderLineItemId);
           
            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<OrderLineItemResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return Equals(response, null) ? null : response.OrderLineItem;

        }

        public OrderLineItemModel UpdateOrderLineItemStatus(int orderLineItemId, OrderLineItemModel model)
        {
            var endpoint = OrdersEndpoint.UpdateOrderLineItemStatus(orderLineItemId);

            var status = new ApiStatus();
            var response = PutResourceToEndpoint<OrderLineItemResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return Equals(response, null) ? null : response.OrderLineItem;
        }

        public bool UpdateOrderPaymentStatus(int orderId, string paymentStatus)
        {
            var endpoint = OrdersEndpoint.UpdateOrderPaymentStatus(orderId, paymentStatus);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<TrueFalseResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return response.booleanModel.disabled;
        }


        public string SendEmail(FilterCollection filters = null)
        {
            string endpoint = OrdersEndpoint.SendEmail();
            endpoint += BuildEndpointQueryString(null, filters, null, null, null);

            ApiStatus status = new ApiStatus();
            OrderResponse response = GetResourceFromEndpoint<OrderResponse>(endpoint, status);

            Collection<HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return Equals(response, null) ? null : response.EmailSentMessage;
        }
        #endregion
        
    }
}
