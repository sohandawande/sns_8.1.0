﻿using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IMasterPageClient : IBaseClient
    {
        /// <summary>
        /// Method Returns the MasterPages.
        /// </summary>
        /// <param name="expands">ExpandCollection expands</param>
        /// <param name="filters">FilterCollection filters</param>
        /// <param name="sorts">SortCollection sorts</param>
        /// <returns>Returns the MasterPages</returns>
        MasterPageListModel GetMasterPages(ExpandCollection expands, FilterCollection filters, SortCollection sorts);

        /// <summary>
        /// Method Returns the MasterPages.
        /// </summary>
        /// <param name="expands">ExpandCollection expands</param>
        /// <param name="filters">FilterCollection filters</param>
        /// <param name="sorts">SortCollection sorts</param>
        /// <param name="pageIndex">pageIndex</param>
        /// <param name="pageSize">pageSize</param>
        /// <returns>Returns the MasterPages</returns>
        MasterPageListModel GetMasterPages(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Method Returns the MasterPages by theme Id
        /// </summary>
        /// <param name="themeId">Id of the Theme.</param>
        /// <param name="pageType">page type</param>
        /// <returns>Returns the MasterPages</returns>
        MasterPageListModel GetMasterPageByThemeId(int themeId, string pageType);
    }
}
