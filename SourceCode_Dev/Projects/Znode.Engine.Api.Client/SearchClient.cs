﻿using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
	public class SearchClient : BaseClient, ISearchClient
	{
		public SearchClient() { }

		public SearchClient(int accountId)
		{
			AccountId = accountId;
		}

		public SuggestedSearchListModel GetTypeAheadResponse(SuggestedSearchModel model)
		{
			var endpoint = SearchEndpoint.Suggested();

			var status = new ApiStatus();
			var response = PostResourceToEndpoint<SuggestedSearchListResponse>(endpoint, JsonConvert.SerializeObject(model), status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

			//if (status.HasError || status.StatusCode != HttpStatusCode.OK || status.StatusCode != HttpStatusCode.NotFound)
			//{
			//	throw new ZnodeException(status.ErrorCode, status.ErrorMessage, status.StatusCode);
			//}

            var list = new SuggestedSearchListModel { SuggestedSearchResults = (response == null) ? null : response.SuggestedSearchResults };
			list.MapPagingDataFromResponse(response);

			return list;
		}

		public KeywordSearchModel Search(KeywordSearchModel model, ExpandCollection expands, SortCollection sorts)
		{
			var endpoint = SearchEndpoint.Keyword();
			endpoint += BuildEndpointQueryString(expands, null, sorts, null, null);

			var status = new ApiStatus();
			var response = PostResourceToEndpoint<KeywordSearchResponse>(endpoint, JsonConvert.SerializeObject(model), status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

			//if (status.HasError || status.StatusCode != HttpStatusCode.OK || (status.StatusCode != HttpStatusCode.OK && status.StatusCode != HttpStatusCode.NotFound) )
			//{
			//	throw new ZnodeException(status.ErrorCode, status.ErrorMessage, status.StatusCode);
			//}

			return response != null ? response.Search : new KeywordSearchModel { Products = new Collection<SearchProductModel>() };
		}

        /// <summary>
        /// To Get Seo Url Details.
        /// </summary>
        /// <param name="model">SEOUrlModel model</param>
        /// <returns>Returns Seo Url Details having SEOUrlModel format.</returns>
        public SEOUrlModel GetSeoUrlDetail(SEOUrlModel model)
        {
            string endpoint = SearchEndpoint.SeoUrl(model.SeoUrl);
            ApiStatus status = new ApiStatus();
            SEOUrlResponse response = GetResourceFromEndpoint<SEOUrlResponse>(endpoint, status);
            Collection<HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);
            return Equals(response, null) ? null : response.SeoUrl;
        }

        /// <summary>
        /// To Check whether provided Seo Url is restricted or not.
        /// </summary>
        /// <param name="seoUrl">string seoUrl</param>
        /// <returns>Return true or false.</returns>
        public bool IsRestrictedSeoUrl(string seoUrl)
        {
            string endpoint = SearchEndpoint.IsRestrictedSeoUrl(seoUrl);
            ApiStatus status = new ApiStatus();
            return GetBooleanResourceFromEndpoint<SEOUrlResponse>(endpoint, status);
        }
	}
}
