﻿using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    /// <summary>
    /// Tax Rule Types Client Interface
    /// </summary>
    public interface ITaxRuleTypesClient : IBaseClient
	{
        /// <summary>
        /// Get Tax Rule Type on the basis of tax rule type id
        /// </summary>
        /// <param name="taxRuleTypeId">taxRuleTypeId</param>
        /// <returns>Returns TaxRuleTypeModel</returns>
		TaxRuleTypeModel GetTaxRuleType(int taxRuleTypeId);

        /// <summary>
        /// Get list of Tax Rule Types
        /// </summary>
        /// <param name="filters">FilterCollection filters</param>
        /// <param name="sorts">SortCollection sorts</param>
        /// <returns>Returns Tax RuleTypeListModel</returns>
		TaxRuleTypeListModel GetTaxRuleTypes(FilterCollection filters, SortCollection sorts);

        /// <summary>
        /// Get list of Tax Rule Types
        /// </summary>
        /// <param name="filters">FilterCollection filters</param>
        /// <param name="sorts">SortCollection sorts</param>
        /// <param name="pageIndex">Index of page</param>
        /// <param name="pageSize">Size of page</param>
        /// <returns>Returns TaxRuleTypeListModel</returns>
		TaxRuleTypeListModel GetTaxRuleTypes(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Create Tax Rule Type
        /// </summary>
        /// <param name="model">TaxRuleTypeModel model</param>
        /// <returns>Returns TaxRuleTypeModel</returns>
		TaxRuleTypeModel CreateTaxRuleType(TaxRuleTypeModel model);

        /// <summary>
        /// Update Tax Rule Type
        /// </summary>
        /// <param name="taxRuleTypeId">taxRuleTypeId</param>
        /// <param name="model">TaxRuleTypeModel model</param>
        /// <returns>Returns TaxRuleTypeModel</returns>
		TaxRuleTypeModel UpdateTaxRuleType(int taxRuleTypeId, TaxRuleTypeModel model);

        /// <summary>
        /// Delete Tax Rule Type on the basis of tax rule type id
        /// </summary>
        /// <param name="taxRuleTypeId">taxRuleTypeId</param>
        /// <returns>Returns true/false</returns>
		bool DeleteTaxRuleType(int taxRuleTypeId);

        /// <summary>
        /// ZNode Version 8.0
        /// Get all Tax Rule Types not present in database.
        /// </summary>
        /// <param name="filters">FilterCollection filters</param>
        /// <param name="sorts">SortCollection sorts</param>
        /// <param name="pageIndex">Nullable int pageIndex</param>
        /// <param name="pageSize">Nullable int pageSize</param>
        /// <returns>Returns Tax Rule types list</returns>
        TaxRuleTypeListModel GetAllTaxRuleTypesNotInDatabase(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
	}
}
