﻿using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    /// <summary>
    /// Shipping Rule Type Client
    /// </summary>
	public class ShippingRuleTypesClient : BaseClient, IShippingRuleTypesClient
	{
        #region Public Methods

        public ShippingRuleTypeModel GetShippingRuleType(int shippingRuleTypeId)
        {
            var endpoint = ShippingRuleTypesEndpoint.Get(shippingRuleTypeId);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ShippingRuleTypeResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (Equals(response, null)) ? null : response.ShippingRuleType;
        }

        public ShippingRuleTypeListModel GetShippingRuleTypes(FilterCollection filters, SortCollection sorts)
        {
            return GetShippingRuleTypes(filters, sorts, null, null);
        }

        public ShippingRuleTypeListModel GetShippingRuleTypes(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = ShippingRuleTypesEndpoint.List();
            endpoint += BuildEndpointQueryString(null, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ShippingRuleTypeListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new ShippingRuleTypeListModel { ShippingRuleTypes = (response == null) ? null : response.ShippingRuleTypes };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public ShippingRuleTypeModel CreateShippingRuleType(ShippingRuleTypeModel model)
        {
            var endpoint = ShippingRuleTypesEndpoint.Create();

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<ShippingRuleTypeResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return (Equals(response, null)) ? null : response.ShippingRuleType;
        }

        public ShippingRuleTypeModel UpdateShippingRuleType(int shippingRuleTypeId, ShippingRuleTypeModel model)
        {
            var endpoint = ShippingRuleTypesEndpoint.Update(shippingRuleTypeId);

            var status = new ApiStatus();
            var response = PutResourceToEndpoint<ShippingRuleTypeResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return (Equals(response, null)) ? null : response.ShippingRuleType;
        }

        public bool DeleteShippingRuleType(int shippingRuleTypeId)
        {
            var endpoint = ShippingRuleTypesEndpoint.Delete(shippingRuleTypeId);

            var status = new ApiStatus();
            var deleted = DeleteResourceFromEndpoint<ShippingRuleTypeResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

            return deleted;
        } 

        #endregion
	}
}
