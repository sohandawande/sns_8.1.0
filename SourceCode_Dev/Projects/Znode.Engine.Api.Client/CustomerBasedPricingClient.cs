﻿using System.Collections.ObjectModel;
using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    public class CustomerBasedPricingClient : BaseClient, ICustomerBasedPricingClient
    {
        public CustomerBasedPricingListModel GetCustomerBasedPricing(int productId,ExpandCollection expands, FilterCollection filters, SortCollection sorts)
        {
            return GetCustomerBasedPricing(productId,expands, filters, sorts, null, null);
        }

        public CustomerBasedPricingListModel GetCustomerBasedPricing(int productId,ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = CustomerBasedPricingEndpoint.GetCustomerPricingId(productId);
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<CustomerBasedPricingListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new CustomerBasedPricingListModel { CustomerBasedPricing = (Equals(response, null)) ? null : response.CustomerBasedPricingList };
            list.MapPagingDataFromResponse(response);

            return list;
        }
    }
}
