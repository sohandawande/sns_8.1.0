﻿using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IPortalProfileClient : IBaseClient
    {
        /// <summary>
        /// Creates a portal profile.
        /// </summary>
        /// <param name="model">Portal profile model.</param>
        /// <returns>PortalProfileModel</returns>
        PortalProfileModel CreatePortalProfile(PortalProfileModel model);

        /// <summary>
        /// Deletes a portal profile.
        /// </summary>
        /// <param name="portalProfileId">Portal profile ID</param>
        /// <returns>Bool value whether delete is succes or not.</returns>
        bool DeletePortalProfile(int portalProfileId);

        /// <summary>
        /// Gets a portal profile based on profile id.
        /// </summary>
        /// <param name="portalProfileId">Portal profile Id.</param>
        /// <returns>PortalProfileModel</returns>
        PortalProfileModel GetPortalProfile(int portalProfileId);

        /// <summary>
        /// Gets Portal Profile by portal profile ID.
        /// </summary>
        /// <param name="portalProfileId">Portal Profile Id.</param>
        /// <param name="expands">Expands collection</param>
        /// <returns>PortalProfileModel</returns>
        PortalProfileModel GetPortalProfile(int portalProfileId, ExpandCollection expands);

        /// <summary>
        /// /Get list of portal profile.
        /// </summary>
        /// <param name="expands">Expands collection</param>
        /// <param name="filters">Filter collection</param>
        /// <param name="sorts">Sort collection</param>
        /// <returns></returns>
        PortalProfileListModel GetPortalProfiles(ExpandCollection expands, FilterCollection filters, SortCollection sorts);

        /// <summary>
        /// Gets portal profiles.
        /// </summary>
        /// <param name="expands">Expand Collection</param>
        /// <param name="filters">Filter collection</param>
        /// <param name="sorts">Sort collection</param>
        /// <param name="pageIndex">Page Index.</param>
        /// <param name="pageSize">Page size</param>
        /// <returns></returns>
        PortalProfileListModel GetPortalProfiles(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Updates portal profile.
        /// </summary>
        /// <param name="portalProfileId">Portal Profile ID.</param>
        /// <param name="model">Portal profile model.</param>
        /// <returns>PortalProfileModel</returns>
        PortalProfileModel UpdatePortalProfile(int portalProfileId, PortalProfileModel model);

        /// <summary>
        /// Get portal profile list.
        /// </summary>
        /// <param name="expands">Expands collection</param>
        /// <param name="filters">Filter collection</param>
        /// <param name="sorts">Sort collection</param>
        /// <returns>Portal Profile List.</returns>
        PortalProfileListModel GetPortalProfilesDetails(ExpandCollection expands, FilterCollection filters, SortCollection sorts);

        /// <summary>
        /// Portal Profile List model.
        /// </summary>
        /// <param name="expands">Expands collection</param>
        /// <param name="filters">Filter collection</param>
        /// <param name="sorts">Sort collection</param>
        /// <param name="pageIndex">Page index of List.</param>
        /// <param name="pageSize">Page Size of the List.</param>
        /// <returns>Portal Profile List.</returns>
        PortalProfileListModel GetPortalProfilesDetails(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Get the list of portalProfiles by portalId
        /// </summary>
        /// <param name="portalId">portalId to retrieve protalProfileList</param>
        /// <returns>PortalProfileListModel</returns>
        PortalProfileListModel GetPortalProfilesByPortalId(int portalId);

    }
}
