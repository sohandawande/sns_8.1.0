﻿using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface ISkusClient : IBaseClient
	{
		SkuModel GetSku(int skuId);
		SkuModel GetSku(int skuId, ExpandCollection expands);
		SkuListModel GetSkus(ExpandCollection expands, FilterCollection filters, SortCollection sorts);
		SkuListModel GetSkus(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
		SkuModel CreateSku(SkuModel model);
		SkuModel UpdateSku(int skuId, SkuModel model);
		bool DeleteSku(int skuId);

        /// <summary>
        /// Znode Version 8.0
        /// To Save Sku Attribute in ZnodeSkuAttribute
        /// </summary>
        /// <param name="skuId">skuId</param>
        /// <param name="attributeIds">string comma seperated attributeId</param>
        /// <returns>return true/false</returns>
        bool AddSkuAttribute(int skuId, string attributeIds);

        /// <summary>
        /// Get list of Skus
        /// </summary>
        /// <param name="productId">productId to retrive Sku list</param>
        /// <returns>SkuListModel</returns>
        SkuListModel GetSKUListByProductId(int productId);
	}
}
