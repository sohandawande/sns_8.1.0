﻿using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    /// <summary>
    /// Tax Rules Client
    /// </summary>
	public class TaxRulesClient : BaseClient, ITaxRulesClient
	{
        #region Public Methods

        public TaxRuleModel GetTaxRule(int taxRuleId)
        {
            return GetTaxRule(taxRuleId, null);
        }

        public TaxRuleModel GetTaxRule(int taxRuleId, ExpandCollection expands)
        {
            var endpoint = TaxRulesEndpoint.Get(taxRuleId);
            endpoint += BuildEndpointQueryString(expands);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<TaxRuleResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (Equals(response, null)) ? null : response.TaxRule;
        }

        public TaxRuleListModel GetTaxRules(ExpandCollection expands, FilterCollection filters, SortCollection sorts)
        {
            return GetTaxRules(expands, filters, sorts, null, null);
        }

        public TaxRuleListModel GetTaxRules(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = TaxRulesEndpoint.List();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<TaxRuleListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new TaxRuleListModel { TaxRules = (Equals(response, null)) ? null : response.TaxRules };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public TaxRuleModel CreateTaxRule(TaxRuleModel model)
        {
            var endpoint = TaxRulesEndpoint.Create();

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<TaxRuleResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return (Equals(response, null)) ? null : response.TaxRule;
        }

        public TaxRuleModel UpdateTaxRule(int taxRuleId, TaxRuleModel model)
        {
            var endpoint = TaxRulesEndpoint.Update(taxRuleId);

            var status = new ApiStatus();
            var response = PutResourceToEndpoint<TaxRuleResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return (Equals(response, null)) ? null : response.TaxRule;
        }

        public bool DeleteTaxRule(int taxRuleId)
        {
            var endpoint = TaxRulesEndpoint.Delete(taxRuleId);

            var status = new ApiStatus();
            var deleted = DeleteResourceFromEndpoint<TaxRuleResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

            return deleted;
        } 

        #endregion
	}
}
