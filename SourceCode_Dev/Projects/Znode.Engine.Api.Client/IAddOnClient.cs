﻿using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    /// <summary>
    /// AddOn Client Interface
    /// </summary>
    public interface IAddOnClient : IBaseClient
    {
        /// <summary>
        /// Get AddOn by AddOn ID
        /// </summary>
        /// <param name="addOnId">int addOnId</param>
        /// <returns>Returns the model of AddOn</returns>
        AddOnModel GetAddOn(int addOnId);

        /// <summary>
        /// Get AddOn by AddOn ID and expands
        /// </summary>
        /// <param name="addOnId">int addOnId</param>
        /// <param name="expands">ExpandCollection expands</param>
        /// <returns>Returns the model of AddOn</returns>
        AddOnModel GetAddOn(int addOnId, ExpandCollection expands);

        /// <summary>
        /// Get all Addons
        /// </summary>
        /// <param name="expands">ExpandCollection expands</param>
        /// <param name="filters">FilterCollection filters</param>
        /// <param name="sorts">SortCollection sorts</param>
        /// <param name="pageIndex">Nullable int pageIndex</param>
        /// <param name="pageSize">Nullable int pageSize</param>
        /// <returns>Returns the list of AddOns</returns>
        AddOnListModel GetAddOns(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
        
        /// <summary>
        /// Create AddOn
        /// </summary>
        /// <param name="model">AddOnModel model</param>
        /// <returns>Returns the model of AddOn</returns>
        AddOnModel CreateAddOn(AddOnModel model);

        /// <summary>
        /// Update AddOn
        /// </summary>
        /// <param name="addOnId">int addOnId</param>
        /// <param name="model">AddOnModel model</param>
        /// <returns>Returns the model of AddOn</returns>
        AddOnModel UpdateAddOn(int addOnId, AddOnModel model);

        /// <summary>
        /// Delete AddOn
        /// </summary>
        /// <param name="addOnId">int addOnId</param>
        /// <returns>Returns true or false</returns>
        bool DeleteAddOn(int addOnId);
    }
}