﻿using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    public class CatalogsClient : BaseClient, ICatalogsClient
    {
        #region Public Methods
        public CatalogModel GetCatalog(int catalogId)
        {
            var endpoint = CatalogsEndpoint.Get(catalogId);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<CatalogResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (Equals(response, null)) ? null : response.Catalog;
        }

        public CatalogListModel GetCatalogs(FilterCollection filters, SortCollection sorts)
        {
            return GetCatalogs(filters, sorts, null, null);
        }

        public CatalogListModel GetCatalogs(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = CatalogsEndpoint.List();
            endpoint += BuildEndpointQueryString(null, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<CatalogListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new CatalogListModel { Catalogs = (Equals(response, null)) ? null : response.Catalogs };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public CatalogListModel GetCatalogsByCatalogIds(string catalogIds, SortCollection sorts)
        {
            var endpoint = CatalogsEndpoint.ListByCatalogIds(catalogIds);

            endpoint += BuildEndpointQueryString(null, null, sorts, null, null);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<CatalogListResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            var list = new CatalogListModel { Catalogs = (Equals(response, null)) ? null : response.Catalogs };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public CatalogModel CreateCatalog(CatalogModel model)
        {
            var endpoint = CatalogsEndpoint.Create();

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<CatalogResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return (Equals(response, null)) ? null : response.Catalog;
        }

        public CatalogModel UpdateCatalog(int catalogId, CatalogModel model)
        {
            var endpoint = CatalogsEndpoint.Update(catalogId);

            var status = new ApiStatus();
            var response = PutResourceToEndpoint<CatalogResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return (Equals(response, null)) ? null : response.Catalog;
        }

        public bool DeleteCatalog(int catalogId)
        {
            var endpoint = CatalogsEndpoint.Delete(catalogId);

            var status = new ApiStatus();
            var deleted = DeleteResourceFromEndpoint<TrueFalseResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

            return deleted;
        }
        #endregion

        #region Znode Version 8.0

        public bool CopyCatalog(CatalogModel model)
        {
            var endpoint = CatalogsEndpoint.CopyCatalog();

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<TrueFalseResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return response.booleanModel.disabled;

        }

        public bool DeleteCatalog(int catalogId, bool preserveCategories)
        {
            var endpoint = CatalogsEndpoint.DeleteCatalog(catalogId, preserveCategories);

            var status = new ApiStatus();
            var deleted = DeleteResourceFromEndpoint<TrueFalseResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

            return deleted;
        }

        public CatalogListModel GetCatalogListByPortalId(int portalId)
        {
            var endpoint = CatalogsEndpoint.GetCatalogsByPortalId(portalId);
            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<CatalogListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new CatalogListModel { Catalogs = response.Catalogs };
            list.MapPagingDataFromResponse(response);

            return list;
        }
        #endregion

    }
}
