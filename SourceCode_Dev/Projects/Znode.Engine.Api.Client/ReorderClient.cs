﻿using System.Collections.ObjectModel;
using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    public class ReorderClient : BaseClient, IReorderClient
    {
        #region Znode Version 7.2.2 Reorder

        public CartItemsListModel GetReorderItems(int orderId)
        {
            return GetReorderItems(orderId, null);
        }

        public CartItemsListModel GetReorderItems(int orderId, ExpandCollection expands)
        {
            var endpoint = ReorderEndpoint.GetItems(orderId);
            endpoint += BuildEndpointQueryString(expands);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ReorderResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (response == null) ? null : response.ReorderItems;
        }

        public CartItemsModel GetReorderLineItem(int orderLineItemId)
        {
            return GetReorderLineItem(orderLineItemId, null);
        }

        public CartItemsModel GetReorderLineItem(int orderLineItemId, ExpandCollection expands)
        {
            var endpoint = ReorderEndpoint.GetReorderLineItem(orderLineItemId.ToString());
            endpoint += BuildEndpointQueryString(expands);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ReorderResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (response == null) ? null : response.Reorder;
        }


        public CartItemsListModel GetReorderItemsList(int orderId, int orderLineItemId, bool isOrder)
        {
            return GetReorderItemsList(orderId, orderLineItemId, isOrder, null);
        }

        public CartItemsListModel GetReorderItemsList(int orderId, int orderLineItemId, bool isOrder, ExpandCollection expands)
        {
            var endpoint = ReorderEndpoint.GetReorderItemsList(orderId, orderLineItemId, isOrder);
            endpoint += BuildEndpointQueryString(expands);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ReorderResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (response == null) ? null : response.ReorderItems;
        }

        #endregion
    }
}
