﻿using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IProductAttributesClient : IBaseClient
    {
        AttributeModel GetAttributes(int attributeId);
        ProductAttributeListModel GetAttributes(FilterCollection filters, SortCollection sorts);
        AttributeModel CreateAttributes(AttributeModel model);
        AttributeModel UpdateAttributes(int attributeId, AttributeModel model);
        bool DeleteAttributes(int attributeId);

        /// <summary>
        /// Get all Attributes by AttributeTypeId
        /// </summary>
        /// <param name="filters">FilterCollection filters</param>
        /// <param name="sorts">SortCollection sorts</param>
        /// <param name="pageIndex">Nullable int pageIndex</param>
        /// <param name="pageSize">Nullable int pageSize</param>
        /// <returns>Return list of Attributes </returns>
        ProductAttributeListModel GetAttributesByAttributeTypeId(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
    }
}
