﻿using Znode.Engine.Api.Models;


namespace Znode.Engine.Api.Client
{
    public interface IImportExportClient : IBaseClient
    {
        /// <summary>
        /// To Get Export Details.
        /// </summary>
        /// <param name="model">Model of the ExportModel</param>
        /// <returns>Return the ExportDetails in ExportModel format.</returns>
        ExportModel GetExportData(ExportModel model);

        /// <summary>
        /// To Upload the Import Data.
        /// </summary>
        /// <param name="model">Model of the ImportModel</param>
        /// <returns>Return the ImportDetails in ImportModel format.</returns>
        ImportModel ImportData(ImportModel model);

        /// <summary>
        /// To Get Import Type Details based on template type.
        /// </summary>
        /// <param name="type">Type of the import Template</param>
        /// <returns>Return Import Type Details in ImportModel Format</returns>
        ImportModel GetImportDefaultData(string type);
    }
}
