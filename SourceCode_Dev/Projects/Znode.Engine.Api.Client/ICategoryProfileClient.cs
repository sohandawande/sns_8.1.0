﻿using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface ICategoryProfileClient : IBaseClient
    {
        /// <summary>
        /// Gets category profiles.
        /// </summary>
        /// <param name="categoryProfileId">The id of category profile</param>
        /// <returns>Returns model of type CategoryProfileModel.</returns>
        CategoryProfileModel GetCategoryProfile(int categoryProfileId);

        /// <summary>
        /// Creates category profile.
        /// </summary>
        /// <param name="model">Model of type CategoryProfileModel</param>
        /// <returns>Returns model of type CategoryProfileModel.</returns>
        CategoryProfileModel CreateCategoryProfile(CategoryProfileModel model);

        /// <summary>
        /// Gets category profile by category id.
        /// </summary>
        /// <param name="categoryId">The id of category profile</param>
        /// <returns>Returns model of type CategoryProfileListModel.</returns>
        CategoryProfileListModel GetCategoryProfileByCategoryId(int categoryId);

        /// <summary>
        /// This method will update the category profile
        /// </summary>
        /// <param name="categoryProfileId">int categoryProfileId</param>
        /// <param name="model">CategoryProfileModel model</param>
        /// <returns>Returns the updated data</returns>
        bool UpdateCategoryProfile(int categoryProfileId, CategoryProfileModel model);

        /// <summary>
        /// This method will delete the data of the respective category profile id
        /// </summary>
        /// <param name="categoryProfileId">int categoryProfileId</param>
        /// <returns>Return true if data deleted</returns>
        bool DeleteCategoryProfile(int categoryProfileId);
    }
}
