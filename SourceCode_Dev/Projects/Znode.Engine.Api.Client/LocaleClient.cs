﻿using System.Collections.ObjectModel;
using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    /// <summary>
    /// Locale Client
    /// </summary>
    public class LocaleClient : BaseClient, ILocaleClient
    {
        #region Public Methods
        public LocaleListModel GetLocales(ExpandCollection expands, FilterCollection filters, SortCollection sorts)
        {
            return GetLocales(expands, filters, sorts, null, null);
        }
        public LocaleListModel GetLocales(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = LocaleEndpoint.List();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<LocaleListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new LocaleListModel { Locales = (Equals(response, null)) ? null : response.Locales };
            list.MapPagingDataFromResponse(response);

            return list;
        } 
        #endregion
    }
}
