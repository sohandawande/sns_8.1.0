﻿using System.Data;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IXmlGeneratorClient : IBaseClient
    {
        /// <summary>
        /// Get Xml configuration List
        /// </summary>
        /// <param name="filters">Filter collection</param>
        /// <param name="sorts">Sort collection</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Recored per page</param>
        /// <returns>Retirns list of ApplicationSetting</returns>
        ApplicationSettingListModel GetApplicationSettings(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
       
        /// <summary>
        /// Get Column names list
        /// </summary>
        /// <param name="entityType">Type of entity</param>
        /// <param name="entityName">Name of entity</param>
        /// <returns></returns>
        DataSet GetColumnList(string entityType, string entityName);

        /// <summary>
        /// Inser /Update /Delete Xml configuration
        /// </summary>
        /// <param name="model">Application Setting Data Model</param>
        /// <returns>Returns true/false</returns>
        bool SaveXmlConfiguration(ApplicationSettingDataModel model);
    }
}
