﻿using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IRequestStatusClient:IBaseClient
    {
        /// <summary>
        /// Gets request status list.
        /// </summary>
        /// <param name="expands">Expands for request status list.</param>
        /// <param name="filters">Filter for request status list.</param>
        /// <param name="sorts">Sorting for request status list.</param>
        /// <returns>List of request status</returns>
        RequestStatusListModel GetRequestStatus(ExpandCollection expands, FilterCollection filters, SortCollection sorts);

        /// <summary>
        /// Gets request status list.
        /// </summary>
        /// <param name="expands">Expands for request status list.</param>
        /// <param name="filters">Filter for request status list.</param>
        /// <param name="sorts">Sorting for request status list.</param>
        /// <param name="pageIndex">Page index of list.</param>
        /// <param name="pageSize">Page size of list.</param>
        /// <returns>List of request status</returns>
        RequestStatusListModel GetRequestStatus(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Method gets the RequestStatus details.
        /// </summary>
        /// <param name="requestStatusId">The Id of RequestStatus</param>
        /// <returns>Returns RequestStatus details.</returns>
        RequestStatusModel GetRequestStatus(int requestStatusId);

        /// <summary>
        /// Method gets the RequestStatus details.
        /// </summary>
        /// <param name="requestStatusId">The Id of RequestStatus</param>
        /// <param name="expands">Expands collection</param>
        /// <returns>Returns RequestStatus details.</returns>
        RequestStatusModel GetRequestStatus(int requestStatusId, ExpandCollection expands);

        /// <summary>
        /// Method Creates the RequestStatus.
        /// </summary>
        /// <param name="model">Model of RequestStatus</param>
        /// <returns>Return the RequestStatus.</returns>
        RequestStatusModel CreateRequestStatus(RequestStatusModel model);

        /// <summary>
        /// Method Updates the RequestStatus on the basis of RequestStatus Id.
        /// </summary>
        /// <param name="requestStatusId">RequestStatus Id</param>
        /// <param name="model">Model of RequestStatus</param>
        /// <returns>Returns Updated RequestStatus.</returns>
        RequestStatusModel UpdateRequestStatus(int requestStatusId, RequestStatusModel model);

        /// <summary>
        /// Deletes an existing RequestStatus
        /// </summary>
        /// <param name="requestStatusId">RequestStatusId by which RequestStatus will delete</param>
        /// <returns>boolean value true/false</returns>
        bool DeleteRequestStatus(int requestStatusId);
    }
}
