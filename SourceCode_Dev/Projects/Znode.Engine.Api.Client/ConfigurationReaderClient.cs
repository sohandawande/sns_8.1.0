﻿using System.Collections.ObjectModel;
using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    public class ConfigurationReaderClient : BaseClient, IConfigurationReaderClient
    {

        public string GetFilterConfigurationXML(string itemName)
        {
            var endpoint = ConfigurationReaderEndpoint.GetFilterConfigurationXML(itemName);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ConfigurationReaderResponce>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (Equals(response, null)) ? null : response.FilterXML;
        }

    }
}
