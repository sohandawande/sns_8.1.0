﻿using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IDashboardClient :IBaseClient
    {
        /// <summary>
        /// Get Dashboard Items By Portal Id.
        /// </summary>
        /// <param name="portalId">Id of the portal.</param>
        /// <returns>DashboardModel</returns>
        DashboardModel GetDashboardItemsByPortalId(int portalId);

        /// <summary>
        /// Get Dashboard Items By Portal Id.
        /// </summary>
        /// <param name="portalId">Id of the portal.</param>
        /// <param name="expands">Expand collection</param>
        /// <returns>DashboardModel</returns>
        DashboardModel GetDashboardItemsByPortalId(int portalId, ExpandCollection expands);
    }
}
