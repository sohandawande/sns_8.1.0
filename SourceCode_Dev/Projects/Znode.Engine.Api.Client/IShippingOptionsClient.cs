﻿using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IShippingOptionsClient : IBaseClient
	{
        /// <summary>
        /// Get Shipping Option by ShippingOptionId
        /// </summary>
        /// <param name="shippingOptionId">shippingOptionId</param>
        /// <returns>Returns ShippingOptionModel</returns>
		ShippingOptionModel GetShippingOption(int shippingOptionId);

        /// <summary>
        ///  Get Shipping Option by ShippingOptionId
        /// </summary>
        /// <param name="shippingOptionId">shippingOptionId</param>
        /// <param name="expands">expands</param>
        /// <returns>Returns ShippingOptionModel</returns>  
		ShippingOptionModel GetShippingOption(int shippingOptionId, ExpandCollection expands);

        /// <summary>
        /// Get Shipping Options
        /// </summary>
        /// <param name="expands">expands</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <returns>Returns ShippingOptionListModel</returns>
		ShippingOptionListModel GetShippingOptions(ExpandCollection expands, FilterCollection filters, SortCollection sorts);

        /// <summary>
        /// Get Shipping Options
        /// </summary>
        /// <param name="expands">expands</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <param name="pageIndex">Index of page</param>
        /// <param name="pageSize">Size of page</param>
        /// <returns>Returns ShippingOptionListModel</returns>
		ShippingOptionListModel GetShippingOptions(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
		
        /// <summary>
        /// Create Shipping Options
        /// </summary>
        /// <param name="model">ShippingOptionModel</param>
        /// <returns>Returns ShippingOptionModel</returns>
        ShippingOptionModel CreateShippingOption(ShippingOptionModel model);

        /// <summary>
        /// Update Shipping Options
        /// </summary>
        /// <param name="shippingOptionId">shippingOptionId</param>
        /// <param name="model">ShippingOptionModel</param>
        /// <returns>Returns ShippingOptionModel</returns>
		ShippingOptionModel UpdateShippingOption(int shippingOptionId, ShippingOptionModel model);

        /// <summary>
        /// Delete Shipping Options
        /// </summary>
        /// <param name="shippingOptionId">shippingOptionId</param>
        /// <returns>Returns true/false</returns>
		bool DeleteShippingOption(int shippingOptionId);

        /// <summary>
        /// Get the list of shipping option list for Franchise
        /// </summary>
        /// <param name="expands">expands</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <param name="pageIndex">Index of page</param>
        /// <param name="pageSize">Size of page</param>
        /// <returns>Returns ShippingOptionListModel</returns>
        ShippingOptionListModel GetFranchiseShippingOptionList(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
	}
}
