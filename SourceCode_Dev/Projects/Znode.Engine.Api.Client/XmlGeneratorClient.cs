﻿using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.Data;
using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;
namespace Znode.Engine.Api.Client
{
    public class XmlGeneratorClient : BaseClient, IXmlGeneratorClient
    {
        public ApplicationSettingListModel GetApplicationSettings(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = ApplicationSettingEndpoint.List();
            endpoint += BuildEndpointQueryString(null, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ApplicationSettingListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new ApplicationSettingListModel { ApplicationSettingList = (Equals(response, null)) ? null : response.List };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public DataSet GetColumnList(string entityType, string entityName)
        {
            var endpoint = ApplicationSettingEndpoint.ColumnList(entityType, entityName);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ApplicationSettingListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var dataset = Equals(response, null) ? null : response.ColumnList;

            return dataset;
        }

        public bool SaveXmlConfiguration(ApplicationSettingDataModel model)
        {
            var endpoint = ApplicationSettingEndpoint.Create();

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<ApplicationSettingListResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return (Equals(response, null)) ? false : response.CreateStatus;
        }


    }
}
