﻿using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IWishListsClient : IBaseClient
    {
        WishListModel GetWishList(int wishlistId);
		WishListListModel GetWishLists(FilterCollection filters, SortCollection sorts);
        WishListListModel GetWishLists(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
        WishListModel CreateWishList(WishListModel model);
        WishListModel UpdateWishList(int wishlistId, WishListModel model);
        bool DeleteWishList(int wishlistId);
    }
}