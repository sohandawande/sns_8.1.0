﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IFranchiseAccountClient
    {
        /// <summary>
        /// Create franchise Account
        /// </summary>
        /// <param name="model">AccountModel model</param>
        /// <returns></returns>
        AccountModel CreateFranchiseAccount(AccountModel model);
    }
}
