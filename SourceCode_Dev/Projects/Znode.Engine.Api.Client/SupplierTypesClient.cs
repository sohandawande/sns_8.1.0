﻿using System.Collections.ObjectModel;
using System.Net;
using Newtonsoft.Json;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
	public class SupplierTypesClient : BaseClient, ISupplierTypesClient
	{
		public SupplierTypeModel GetSupplierType(int supplierTypeId)
		{
			var endpoint = SupplierTypesEndpoint.Get(supplierTypeId);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<SupplierTypeResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

			return (response == null) ? null : response.SupplierType;
		}

		public SupplierTypeListModel GetSupplierTypes(FilterCollection filters, SortCollection sorts)
		{
			return GetSupplierTypes(filters, sorts, null, null);
		}

		public SupplierTypeListModel GetSupplierTypes(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
		{
			var endpoint = SupplierTypesEndpoint.List();
			endpoint += BuildEndpointQueryString(null, filters, sorts, pageIndex, pageSize);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<SupplierTypeListResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new SupplierTypeListModel { SupplierTypes = (response == null) ? null : response.SupplierTypes };
			list.MapPagingDataFromResponse(response);

			return list;
		}

		public SupplierTypeModel CreateSupplierType(SupplierTypeModel model)
		{
			var endpoint = SupplierTypesEndpoint.Create();

			var status = new ApiStatus();
			var response = PostResourceToEndpoint<SupplierTypeResponse>(endpoint, JsonConvert.SerializeObject(model), status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

			return (response == null) ? null : response.SupplierType;
		}

		public SupplierTypeModel UpdateSupplierType(int supplierTypeId, SupplierTypeModel model)
		{
			var endpoint = SupplierTypesEndpoint.Update(supplierTypeId);

			var status = new ApiStatus();
			var response = PutResourceToEndpoint<SupplierTypeResponse>(endpoint, JsonConvert.SerializeObject(model), status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

			return (response == null) ? null : response.SupplierType;
		}

		public bool DeleteSupplierType(int supplierTypeId)
		{
			var endpoint = SupplierTypesEndpoint.Delete(supplierTypeId);

			var status = new ApiStatus();
			var deleted = DeleteResourceFromEndpoint<SupplierTypeResponse>(endpoint, status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

			return deleted;
        }

        #region Znode Version 8.0
        public SupplierTypeListModel GetAllSupplierTypesNotInDatabase(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = SupplierTypesEndpoint.GetAllSupplierTypesNotInDatabase();
            endpoint += BuildEndpointQueryString(null, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<SupplierTypeListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new SupplierTypeListModel { SupplierTypes = (response == null) ? null : response.SupplierTypes };
            list.MapPagingDataFromResponse(response);

            return list;
        }
        #endregion

    }
}
