﻿using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface ISuppliersClient : IBaseClient
	{
		SupplierModel GetSupplier(int supplierId);
		SupplierModel GetSupplier(int supplierId, ExpandCollection expands);
		SupplierListModel GetSuppliers(ExpandCollection expands, FilterCollection filters, SortCollection sorts);
		SupplierListModel GetSuppliers(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
		SupplierModel CreateSupplier(SupplierModel model);
		SupplierModel UpdateSupplier(int supplierId, SupplierModel model);

        /// <summary>
        /// Deletes the Existing supplier if it is not associated with any product or sku.
        /// </summary>
        /// <param name="supplierId">Id of the supplier</param>
        /// <returns>Returns true or false.</returns>
        bool DeleteSupplier(int supplierId);
	}
}
