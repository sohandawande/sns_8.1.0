﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface ICustomerUserMappingClient
    {
        /// <summary>
        /// Delete Associated customer mapping with user.
        /// </summary>
        /// <param name="customerUserMappingId">customerUserMappingId</param>
        /// <returns>True / False</returns>
        bool DeleteUserAssociatedCustomer(int customerUserMappingId);

        /// <summary>
        /// Save multiple Customer Id's with single accountId.
        /// </summary>
        /// <param name="accountId">accountId</param>
        /// <param name="customerAccountIds">profileIds</param>
        /// <returns>True / False</returns>
        bool UserAssociatedCustomers(int accountId, string customerAccountIds);

        AccountListModel GetSubUserList(int accountId, ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        bool DeleteAllUserMapping(int userAccountId);

    }
}
