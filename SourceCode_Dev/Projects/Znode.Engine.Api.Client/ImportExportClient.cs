﻿using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    public class ImportExportClient : BaseClient, IImportExportClient
    {
        public ExportModel GetExportData(ExportModel model)
        {
            var endpoint = ImportExportEndpoint.Exports();
            var status = new ApiStatus();
            var response = PostResourceToEndpoint<ImportExportResponse>(endpoint, JsonConvert.SerializeObject(model), status);
            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);
            return Equals(response,null) ? null : response.Exports;
        }

        public ImportModel ImportData(ImportModel model)
        {
            var endpoint = ImportExportEndpoint.Imports();
            var status = new ApiStatus();
            var response = PostResourceToEndpoint<ImportExportResponse>(endpoint, JsonConvert.SerializeObject(model), status);
            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);
            return Equals(response, null) ? null : response.Imports;
        }

        public ImportModel GetImportDefaultData(string type)
        {
            var endpoint = ImportExportEndpoint.GetImportDetails(type);
            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ImportExportResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (Equals(response, null)) ? null : response.Imports;
        }
    }
}
