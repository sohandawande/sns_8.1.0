﻿using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    public class StoreLocatorClient : BaseClient, IStoreLocatorClient
    {
        #region public methods
        #region create Store
        public StoreModel CreateStoreLocator(StoreModel model)
        {
            var endpoint = StoreLocatorEndpoint.Create();

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<StoreLocatorResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return Equals(response, null) ? null : response.StoreLocator;
        }
        #endregion

        #region Get Store
        public StoreModel GetStoreLocator(int storeId)
        {
            var endpoint = StoreLocatorEndpoint.Get(storeId);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<StoreLocatorResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (response == null) ? null : response.StoreLocator;
        }

        public StoreListModel GetStoreLocators(ExpandCollection expands, FilterCollection filters, SortCollection sorts)
        {
            return GetStoreLocators(expands, filters, sorts, null, null);
        }

        public StoreListModel GetStoreLocators(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = StoreLocatorEndpoint.List();
            endpoint += BuildEndpointQueryString(null, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<StoreLocatorListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new StoreListModel { Stores = Equals(response, null) ? null : response.StoreLocators };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public StoreListModel GetStoresList(StoreModel model)
        {
            string endpoint = StoreLocatorEndpoint.GetStoresList();

            ApiStatus status = new ApiStatus();
            var response = PostResourceToEndpoint<StoreLocatorListResponse>(endpoint, JsonConvert.SerializeObject(model),status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            StoreListModel list = new StoreListModel { Stores = Equals(response, null) ? null : response.StoreLocators };
            list.MapPagingDataFromResponse(response);

            return list;
        }
        #endregion

        #region Update Store
        public StoreModel UpdateStoreLocator(int storeId, StoreModel model)
        {
            var endpoint = StoreLocatorEndpoint.Update(storeId);

            var status = new ApiStatus();
            var response = PutResourceToEndpoint<StoreLocatorResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return Equals(response, null) ? null : response.StoreLocator;
        }
        #endregion

        #region Delete Store
        public bool DeleteStoreLocator(int storeId)
        {
            var endpoint = StoreLocatorEndpoint.Delete(storeId);

            var status = new ApiStatus();
            var deleted = DeleteResourceFromEndpoint<StoreLocatorResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

            return deleted;
        }
        #endregion
        #endregion

    }
}
