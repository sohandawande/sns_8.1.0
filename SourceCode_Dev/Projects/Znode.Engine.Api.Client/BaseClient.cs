﻿using System;
using System.Collections.ObjectModel;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    public abstract class BaseClient : IBaseClient
    {
        private string _domainName;
        private string _domainKey;

        public int AccountId { get; set; }
        public bool RefreshCache { get; set; }

        public string AccountHeader
        {
            get { return AccountId > 0 ? "Znode-AccountId: " + AccountId : String.Empty; }
        }

        public string DomainName
        {
            get
            {
                if (!String.IsNullOrEmpty(_domainName))
                {
                    return _domainName;
                }

                return HttpContext.Current.Request.Url.Authority;
            }

            set { _domainName = value; }
        }

        public string DomainKey
        {
            get
            {
                if (!String.IsNullOrEmpty(_domainKey))
                {
                    return _domainKey;
                }

                return ConfigurationManager.AppSettings[HttpContext.Current.Request.Url.Authority];
            }

            set { _domainKey = value; }
        }

        public string UriItemSeparator
        {
            get { return ConfigurationManager.AppSettings["ZnodeApiUriItemSeparator"]; }
        }

        public string UriKeyValueSeparator
        {
            get { return ConfigurationManager.AppSettings["ZnodeApiUriKeyValueSeparator"]; }
        }

        public string GetAuthorizationHeader(string domainName, string domainKey)
        {
            var value = String.Format("{0}|{1}", domainName, domainKey);
            return "Authorization: Basic " + EncodeBase64(value);
        }

        public void CheckStatusAndThrow<T>(ApiStatus status, HttpStatusCode expectedStatusCode) where T : ZnodeException, new()
        {
            CheckStatusAndThrow<T>(status, new Collection<HttpStatusCode> { expectedStatusCode });
        }

        public void CheckStatusAndThrow<T>(ApiStatus status, Collection<HttpStatusCode> expectedStatusCodes) where T : ZnodeException, new()
        {
            var ex = (T)Activator.CreateInstance(typeof(T), status.ErrorCode, status.ErrorMessage, status.StatusCode);

            // If status has an error, throw exception and get out early
            if (status.HasError) throw ex;

            // Check if the status code is in the list of ones we expect

            var found = expectedStatusCodes != null && expectedStatusCodes.Any(statusCode => status.StatusCode == statusCode);
            // If we didn't find our status code, throw the exception
            if (!found) throw ex;

        }

        /// <summary>
        /// Gets a resource from an endpoint.
        /// </summary>
        /// <typeparam name="T">The type of resource to retrieve.</typeparam>
        /// <param name="endpoint">The endpoint where the resource resides.</param>
        /// <param name="status">The status of the API call; treat this as an out parameter.</param>
        /// <returns>The resource.</returns>
        public T GetResourceFromEndpoint<T>(string endpoint, ApiStatus status) where T : BaseResponse
        {
            if (RefreshCache)
            {
                endpoint = BuildCacheRefreshQueryString(endpoint);
            }

            var req = (HttpWebRequest)WebRequest.Create(endpoint);
            req.KeepAlive = false; // Prevents "server committed a protocol violation" error
            req.Method = "GET";
            req.Headers.Add(GetAuthorizationHeader(DomainName, DomainKey));

            if (!String.IsNullOrEmpty(AccountHeader))
            {
                req.Headers.Add(AccountHeader);
            }

            var result = GetResultFromResponse<T>(req, status);
            return result;
        }

        /// <summary>
        /// Gets a resource from an endpoint.
        /// </summary>
        /// <typeparam name="T">The type of resource to retrieve.</typeparam>
        /// <param name="endpoint">The endpoint where the resource resides.</param>
        /// <param name="status">The status of the API call; treat this as an out parameter.</param>
        /// <returns>The resource.</returns>
        public async Task<T> GetResourceFromEndpointAsync<T>(string endpoint, ApiStatus status) where T : BaseResponse
        {
            if (RefreshCache)
            {
               endpoint= BuildCacheRefreshQueryString(endpoint);
            }

            var req = (HttpWebRequest)WebRequest.Create(endpoint);
            req.KeepAlive = false; // Prevents "server committed a protocol violation" error
            req.Method = "GET";
            req.Headers.Add(GetAuthorizationHeader(DomainName, DomainKey));

            if (!String.IsNullOrEmpty(AccountHeader))
            {
                req.Headers.Add(AccountHeader);
            }

            var result = await GetResultFromResponseAsync<T>(req, status);

            return result;
        }

        /// <summary>
        /// Posts resource data to an endpoint, usually for creating a new resource.
        /// </summary>
        /// <typeparam name="T">The type of resource being created.</typeparam>
        /// <param name="endpoint">The endpoint that accepts posting resource data.</param>
        /// <param name="data">The data for the resource.</param>
        /// <param name="status">The status of the API call; treat this as an out parameter.</param>
        /// <returns>The newly created resource.</returns>
        public T PostResourceToEndpoint<T>(string endpoint, string data, ApiStatus status) where T : BaseResponse
        {
            var dataBytes = Encoding.UTF8.GetBytes(data);

            var req = (HttpWebRequest)WebRequest.Create(endpoint);
            req.KeepAlive = false; // Prevents "server committed a protocol violation" error
            req.Method = "POST";
            req.ContentType = "application/json";
            req.ContentLength = dataBytes.Length;
            req.Headers.Add(GetAuthorizationHeader(DomainName, DomainKey));

            if (!String.IsNullOrEmpty(AccountHeader))
            {
                req.Headers.Add(AccountHeader);
            }

            using (var reqStream = req.GetRequestStream())
            {
                reqStream.Write(dataBytes, 0, dataBytes.Length);
            }

            var result = GetResultFromResponse<T>(req, status);
            return result;
        }

        /// <summary>
        /// Puts resource data to an endpoint, usually for updating an existing resource.
        /// </summary>
        /// <typeparam name="T">The type of resource being updated.</typeparam>
        /// <param name="endpoint">The endpoint where the resource resides.</param>
        /// <param name="data">The data for the resource.</param>
        /// <param name="status">The status of the API call; treat this as an out parameter.</param>
        /// <returns>The updated resource.</returns>
        public T PutResourceToEndpoint<T>(string endpoint, string data, ApiStatus status) where T : BaseResponse
        {
            var dataBytes = Encoding.UTF8.GetBytes(data);

            var req = (HttpWebRequest)WebRequest.Create(endpoint);
            req.KeepAlive = false; // Prevents "server committed a protocol violation" error
            req.Method = "PUT";
            req.ContentType = "application/json";
            req.ContentLength = dataBytes.Length;
            req.Headers.Add(GetAuthorizationHeader(DomainName, DomainKey));

            if (!String.IsNullOrEmpty(AccountHeader))
            {
                req.Headers.Add(AccountHeader);
            }

            using (var reqStream = req.GetRequestStream())
            {
                reqStream.Write(dataBytes, 0, dataBytes.Length);
            }

            var result = GetResultFromResponse<T>(req, status);
            return result;
        }

        /// <summary>
        /// Deletes a resource from an endpoint.
        /// </summary>
        /// <typeparam name="T">The type of resource being deleted.</typeparam>
        /// <param name="endpoint">The endpoint where the resource resides.</param>
        /// <param name="status">The status of the API call; treat this as an out parameter.</param>
        /// <returns>True if the resource was deleted; otherwise, false.</returns>
        public bool DeleteResourceFromEndpoint<T>(string endpoint, ApiStatus status) where T : BaseResponse
        {
            var req = (HttpWebRequest)WebRequest.Create(endpoint);
            req.KeepAlive = false; // Prevents "server committed a protocol violation" error
            req.Method = "DELETE";
            req.Headers.Add(GetAuthorizationHeader(DomainName, DomainKey));

            if (!String.IsNullOrEmpty(AccountHeader))
            {
                req.Headers.Add(AccountHeader);
            }

            try
            {
                using (var rsp = (HttpWebResponse)req.GetResponse())
                {
                    // Be sure to set the status code
                    status.StatusCode = rsp.StatusCode;

                    if (rsp.StatusCode == HttpStatusCode.NoContent || rsp.StatusCode.Equals(HttpStatusCode.OK))
                    {
                        return true;
                    }
                }
            }
            catch (WebException ex)
            {
                using (var rsp = (HttpWebResponse)ex.Response)
                {
                    // This deserialization is used to get the error information
                    var result = DeserializeResponseStream<T>(rsp);
                    UpdateApiStatus(result, rsp, status);
                }
            }

            return false;
        }

        /// <summary>
        /// Get Boolean a resource from an endpoint.
        /// </summary>
        /// <typeparam name="T">The type of resource being deleted.</typeparam>
        /// <param name="endpoint">The endpoint where the resource resides.</param>
        /// <param name="status">The status of the API call; treat this as an out parameter.</param>
        /// <returns>True if the get resource; otherwise, false.</returns>
        public bool GetBooleanResourceFromEndpoint<T>(string endpoint, ApiStatus status) where T : BaseResponse
        {
            var req = (HttpWebRequest)WebRequest.Create(endpoint);
            req.KeepAlive = false; // Prevents "server committed a protocol violation" error
            req.Method = "GET";
            req.Headers.Add(GetAuthorizationHeader(DomainName, DomainKey));

            if (!String.IsNullOrEmpty(AccountHeader))
            {
                req.Headers.Add(AccountHeader);
            }

            try
            {
                using (var rsp = (HttpWebResponse)req.GetResponse())
                {
                    // Be sure to set the status code
                    status.StatusCode = rsp.StatusCode;

                    if (rsp.StatusCode == HttpStatusCode.NoContent)
                    {
                        return true;
                    }
                }
            }
            catch (WebException ex)
            {
                using (var rsp = (HttpWebResponse)ex.Response)
                {
                    // This deserialization is used to get the error information
                    var result = DeserializeResponseStream<T>(rsp);
                    UpdateApiStatus(result, rsp, status);
                }
            }

            return false;
        }

        private T GetResultFromResponse<T>(HttpWebRequest request, ApiStatus status) where T : BaseResponse
        {
            T result;

            try
            {
                using (var rsp = (HttpWebResponse)request.GetResponse())
                {
                    // This deserialization gives back the populated resource
                    result = DeserializeResponseStream<T>(rsp);
                    UpdateApiStatus(result, rsp, status);
                }
            }
            catch (WebException ex)
            {
                using (var rsp = (HttpWebResponse)ex.Response)
                {
                    // This deserialization is used to get the error information
                    result = DeserializeResponseStream<T>(rsp);
                    UpdateApiStatus(result, rsp, status);
                }
            }

            return result;
        }

        private async Task<T> GetResultFromResponseAsync<T>(HttpWebRequest request, ApiStatus status) where T : BaseResponse
        {
            T result;

            try
            {
                using (var rsp = (HttpWebResponse)await request.GetResponseAsync())
                {
                    // This deserialization gives back the populated resource
                    result = DeserializeResponseStream<T>(rsp);
                    UpdateApiStatus(result, rsp, status);
                }
            }
            catch (WebException ex)
            {
                using (var rsp = (HttpWebResponse)ex.Response)
                {
                    // This deserialization is used to get the error information
                    result = DeserializeResponseStream<T>(rsp);
                    UpdateApiStatus(result, rsp, status);
                }
            }

            return result;
        }

        private T DeserializeResponseStream<T>(WebResponse response) where T : BaseResponse
        {
            if (response != null)
            {
                using (var body = response.GetResponseStream())
                {
                    if (body != null)
                    {
                        using (var stream = new StreamReader(body))
                        {
                            using (var jsonReader = new JsonTextReader(stream))
                            {
                                var jsonSerializer = new JsonSerializer();
                                try
                                {
                                    return jsonSerializer.Deserialize<T>(jsonReader);
                                }
                                catch (JsonReaderException ex)
                                {
                                    throw new ZnodeException(null, ex.Message);
                                }
                            }
                        }
                    }
                }
            }

            return default(T);
        }

        private void UpdateApiStatus<T>(T result, HttpWebResponse response, ApiStatus status) where T : BaseResponse
        {
            if (status == null)
            {
                status = new ApiStatus();
            }

            if (result != null)
            {
                status.HasError = result.HasError;
                status.ErrorCode = result.ErrorCode;
                status.ErrorMessage = result.ErrorMessage;
            }

            if (response != null) status.StatusCode = response.StatusCode;
        }

        public string BuildEndpointQueryString(ExpandCollection expands)
        {
            return BuildEndpointQueryString(expands, null, null, null, null);
        }

        public string BuildEndpointQueryString(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            // IMPORTANT: Expand always starts with ? while all the others start with &, which
            // means the expands must be added first when building the querystring parameters.

            var queryString = BuildExpandQueryString(expands);
            queryString += BuildFilterQueryString(filters);
            queryString += BuildSortQueryString(sorts);
            queryString += BuildPageQueryString(pageIndex, pageSize);

            return queryString;
        }

        private string BuildExpandQueryString(ExpandCollection expands)
        {
            var queryString = "?expand=";

            if (expands != null)
            {
                foreach (var e in expands)
                {
                    queryString += e + UriItemSeparator;
                }

                queryString = queryString.TrimEnd(UriItemSeparator.ToCharArray());
            }

            return queryString;
        }

        private string BuildFilterQueryString(FilterCollection filters)
        {
            var queryString = "&filter=";

            if (filters != null)
            {
                foreach (var f in filters)
                {
                    queryString += String.Format("{0}{1}{2}{3}{4}{5}", f.FilterName, UriKeyValueSeparator, f.FilterOperator, UriKeyValueSeparator, HttpUtility.UrlEncode(f.FilterValue), UriItemSeparator);
                }

                queryString = queryString.TrimEnd(UriItemSeparator.ToCharArray());
            }

            return queryString;
        }

        private string BuildSortQueryString(SortCollection sorts)
        {
            var queryString = "&sort=";

            if (sorts != null)
            {
                foreach (var s in sorts)
                {
                    queryString += String.Format("{0}{1}{2}{3}", s.Key, UriKeyValueSeparator, s.Value, UriItemSeparator);
                }

                queryString = queryString.TrimEnd(UriItemSeparator.ToCharArray());
            }

            return queryString;
        }

        private string BuildPageQueryString(int? pageIndex, int? pageSize)
        {
            var queryString = "&page=";

            if (pageIndex.HasValue && pageSize.HasValue)
            {
                queryString += String.Format("index{0}{1}", UriKeyValueSeparator, pageIndex.Value);
                queryString += UriItemSeparator;
                queryString += String.Format("size{0}{1}", UriKeyValueSeparator, pageSize.Value);
            }

            return queryString;
        }

        private string BuildCacheRefreshQueryString(string endpoint)
        {            
            if (endpoint.Contains('?'))
            {
                return (endpoint + "&cache=refresh");
            }
            return (endpoint + "?cache=refresh");
        }

        private string EncodeBase64(string value)
        {
            var valueAsBytes = Encoding.UTF8.GetBytes(value);
            return Convert.ToBase64String(valueAsBytes);
        }
    }
}
