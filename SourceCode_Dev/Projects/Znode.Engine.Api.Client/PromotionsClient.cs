﻿using System.Collections.ObjectModel;
using System.Net;
using Newtonsoft.Json;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
	public class PromotionsClient : BaseClient, IPromotionsClient
	{
		public PromotionModel GetPromotion(int promotionId)
		{
			return GetPromotion(promotionId, null);
		}

		public PromotionModel GetPromotion(int promotionId, ExpandCollection expands)
		{
			var endpoint = PromotionsEndpoint.Get(promotionId);
			endpoint += BuildEndpointQueryString(expands);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<PromotionResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

			return (response == null) ? null : response.Promotion;
		}

		public PromotionListModel GetPromotions(ExpandCollection expands, FilterCollection filters, SortCollection sorts)
		{
			return GetPromotions(expands, filters, sorts, null, null);
		}

		public PromotionListModel GetPromotions(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
		{
			var endpoint = PromotionsEndpoint.List();
			endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<PromotionListResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new PromotionListModel { Promotions = (response == null) ? null : response.Promotions };
			list.MapPagingDataFromResponse(response);

			return list;
		}

        public ProductListModel GetProductList(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = PromotionsEndpoint.GetProductsList();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ProductListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new ProductListModel { Products = (Equals(response,null)) ? null : response.Products };
            list.MapPagingDataFromResponse(response);

            return list;
        }

		public PromotionModel CreatePromotion(PromotionModel model)
		{
			var endpoint = PromotionsEndpoint.Create();

			var status = new ApiStatus();
			var response = PostResourceToEndpoint<PromotionResponse>(endpoint, JsonConvert.SerializeObject(model), status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

			return (response == null) ? null : response.Promotion;
		}

		public PromotionModel UpdatePromotion(int promotionId, PromotionModel model)
		{
			var endpoint = PromotionsEndpoint.Update(promotionId);

			var status = new ApiStatus();
			var response = PutResourceToEndpoint<PromotionResponse>(endpoint, JsonConvert.SerializeObject(model), status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

			return (response == null) ? null : response.Promotion;
		}

		public bool DeletePromotion(int promotionId)
		{
			var endpoint = PromotionsEndpoint.Delete(promotionId);

			var status = new ApiStatus();
			var deleted = DeleteResourceFromEndpoint<PromotionResponse>(endpoint, status);
			
			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

			return deleted;
		}
	}
}
