﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public partial interface ICustomerClient : IBaseClient
    {
        /// <summary>
        /// Get Customer List
        /// </summary>
        /// <param name="expands">Exapnd collection.</param>
        /// <param name="filters">Filter collection.</param>
        /// <param name="sorts">Sort collection.</param>
        /// <param name="pageIndex">page index</param>
        /// <param name="pageSize">page size</param>
        /// <returns>CustomerListModel</returns>
        CustomerListModel GetAssociatedCustomer(int accountId, ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        CustomerListModel GetNotAssociatedCustomer(int accountId, ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);        

    }
}
