﻿using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IProductTypeAttributeClient : IBaseClient
    {
        /// <summary>
        /// Get Attribute Types by Attribute Id.
        /// </summary>
        /// <param name="attributeTypeId">Attribute Id.</param>
        /// <returns>Returns ProductTypeAttributeModel</returns>
        ProductTypeAttributeModel GetAttributeType(int attributeTypeId);

        /// <summary>
        /// Get List of Attribute Types by Product Type Attribute Id.
        /// </summary>
        /// <param name="ProductTypeAttributeId">Product Type Attribute Id</param>
        /// <param name="expands">expand collection for Product Type Attribute</param>
        /// <returns>Returns ProductTypeAttributeModel</returns>
        ProductTypeAttributeModel GetAttributeType(int ProductTypeAttributeId, ExpandCollection expands);

        /// <summary>
        /// Get List of Attribute Types .
        /// </summary>
        /// <param name="expands">expand collection for attribute type</param>
        /// <param name="filters">filtering for attribute type</param>
        /// <param name="sorts">sorting for attribute type </param>
        /// <returns>Returns ProductTypeAttributeListModel</returns>
        ProductTypeAttributeListModel GetAttributeTypes(ExpandCollection expands, FilterCollection filters, SortCollection sorts);

        /// <summary>
        /// Get List of Attribute Types .
        /// </summary>
        /// <param name="expands">expand collection for attribute type</param>
        /// <param name="filters">filtering for attribute type</param>
        /// <param name="sorts">sorting for attribute type </param>
        /// <returns>Returns ProductTypeAttributeListModel</returns>
        ProductTypeAttributeListModel GetAttributeTypes(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Create Attribute Type Associated With Product Type.
        /// </summary>
        /// <param name="model">ProductTypeAssociatedAttributeTypesModel</param>
        /// <returns>Returns ProductTypeAttributeModel</returns>
        ProductTypeAttributeModel CreateAttributeType(ProductTypeAssociatedAttributeTypesModel model);

        /// <summary>
        /// Delete Existing Attribute Type.
        /// </summary>
        /// <param name="attributeTypeId">Attribute Type Id</param>
        /// <returns>Returns bool.</returns>
        bool DeleteAttributeType(int attributeTypeId);
 
        /// <summary>
        /// Gets Attribute Types associated with specific Product Type.
        /// </summary>
        /// <param name="productId">product id</param>
        /// <param name="expands">expans collection for product type</param>
        /// <param name="filters">filtering for product type</param>
        /// <param name="sorts">sorting for product type</param>
        /// <param name="pageIndex">paging for for product type</param>
        /// <param name="pageSize">product type record per page</param>
        /// <returns>Returns list of Attributes associated with a Product Type.</returns>
        ProductTypeAssociatedAttributeTypesListModel GetAttributeTypesByProductTypeIdService(int productId,ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
        
        
    }
}
