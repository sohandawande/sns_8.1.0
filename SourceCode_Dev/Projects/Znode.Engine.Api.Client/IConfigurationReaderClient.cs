﻿
namespace Znode.Engine.Api.Client
{
    public interface IConfigurationReaderClient : IBaseClient
    {
        /// <summary>
        /// Get dynamic grid configuration XML 
        /// </summary>
        /// <param name="itemName">string item Name</param>
        /// <returns>return string XML</returns>
        string GetFilterConfigurationXML(string itemName);
    }
}
