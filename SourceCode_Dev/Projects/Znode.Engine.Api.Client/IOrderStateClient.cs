﻿using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IOrderStateClient : IBaseClient
    {
        /// <summary>
        /// Gets Order Sates List
        /// </summary>
        /// <param name="expands">Expands for Order Sate List.</param>
        /// <param name="filters">Filters for Order Sate List.</param>
        /// <param name="sorts">Sort for Order Sate List.</param>
        /// <returns>Order State List Model.</returns>
        OrderStateListModel GetOrderStates(ExpandCollection expands, FilterCollection filters, SortCollection sorts);

        /// <summary>
        /// Gets Order Sates List
        /// </summary>
        /// <param name="expands">Expands for Order Sate List.</param>
        /// <param name="filters">Filters for Order Sate List.</param>
        /// <param name="sorts">Sort for Order Sate List.</param>
        /// <param name="pageIndex">Page index for Order Sate list.</param>
        /// <param name="pageSize">Page Size for Order State List.</param>
        /// <returns>Order State List Model.</returns>
        OrderStateListModel GetOrderStates(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
    }
}
