﻿using System.Data;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IRMARequestClient : IBaseClient
    {
        /// <summary>
        /// Gets the list of RMA Request.
        /// </summary>
        /// <param name="expands">Expand collection for RMA List.</param>
        /// <param name="filters">Filters for RMA request list.</param>
        /// <param name="sorts">Sorting of RMA List</param>
        /// <returns>List of RMA requests.</returns>
        RMARequestListModel GetRMARequestList(ExpandCollection expands, FilterCollection filters, SortCollection sorts);

        /// <summary>
        /// Gets the list of RMA Request.
        /// </summary>
        /// <param name="expands">Expand collection for RMA List.</param>
        /// <param name="filters">Filters for RMA request list.</param>
        /// <param name="sorts">Sorting of RMA List</param>
        /// <param name="pageIndex">Page index for RMA request list.</param>
        /// <param name="pageSize">Page size for RMA request list.</param>
        /// <returns>List of RMA requests.</returns>
        RMARequestListModel GetRMARequestList(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Updates an RMA Request response.
        /// </summary>
        /// <param name="rmaRequestId">RMA Request ID.</param>
        /// <param name="model">RMA Request Model.</param>
        /// <returns>RMA Request model.</returns>
        RMARequestModel UpdateRMARequest(int rmaRequestId, RMARequestModel model);

        /// <summary>
        /// Gets RMA Request
        /// </summary>
        /// <param name="rmaRequestId">RMA Request ID.</param>
        /// <returns>RMA Request model.</returns>
        RMARequestModel GetRMARequest(int rmaRequestId);

        /// <summary>
        /// To Get Order RMA Display Flag by Order Id.
        /// </summary>
        /// <param name="orderId">int orderId</param>
        ///  <returns>Returns true if Order RMA  is 1 else returns false.</returns>
        bool GetOrderRMAFlag(int orderId);

        /// <summary>
        /// Deletes an RMA Request.
        /// </summary>
        /// <param name="rmaRequestId">RMA Request ID</param>
        /// <returns>Returns true or false.</returns>
        bool DeleteRMARequest(int rmaRequestId);

        /// <summary>
        /// Gets RMA request gift card details.
        /// </summary>
        /// <param name="rmaRequestId">RMA Request ID.</param>
        /// <returns>Issued gift card model.</returns>
        IssuedGiftCardListModel GetRMARequestGiftCardDetails(int rmaRequestId);

        /// <summary>
        /// Creates an RMA request reqponse.
        /// </summary>
        /// <param name="model">RMA Request model.</param>
        /// <returns>RMA Request model.</returns>
        RMARequestModel CreateRMARequest(RMARequestModel model);

        /// <summary>
        /// Sends email sent status.
        /// </summary>
        /// <param name="rmaRequestId">RMA Request ID.</param>
        /// <returns>Boolean value whethere the mail is sent or not.</returns>
        bool IsStatusEmailSent(int rmaRequestId);

        /// <summary>
        /// Sends Gift Card mail.
        /// </summary>
        /// <param name="orderId">Order id of the gift RMA Request for which gift card is being sent.</param>
        /// <param name="model">Gift card model from where email data will be retrieved.</param>
        /// <returns>Bool value consisting of status if the gift card mail is sent or not.</returns>
        bool IsGiftCardMailSent(GiftCardModel model);
    }
}
