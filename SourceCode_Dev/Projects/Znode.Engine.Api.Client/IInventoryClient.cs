﻿using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IInventoryClient : IBaseClient
	{
		InventoryModel GetInventory(int inventoryId);
		InventoryModel GetInventory(int inventoryId, ExpandCollection expands);
		InventoryListModel GetInventories(ExpandCollection expands, FilterCollection filters, SortCollection sorts);
		InventoryListModel GetInventories(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
		InventoryModel CreateInventory(InventoryModel model);
		InventoryModel UpdateInventory(int inventoryId, InventoryModel model);
		bool DeleteInventory(int inventoryId);
	}
}
