﻿using System.Collections.ObjectModel;
using System.Net;
using Newtonsoft.Json;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    public class SkusClient : BaseClient, ISkusClient
    {
        public SkuModel GetSku(int skuId)
        {
            return GetSku(skuId, null);
        }

        public SkuModel GetSku(int skuId, ExpandCollection expands)
        {
            var endpoint = SkusEndpoint.Get(skuId);
            endpoint += BuildEndpointQueryString(expands);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<SkuResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (response == null) ? null : response.Sku;
        }

        public SkuListModel GetSkus(ExpandCollection expands, FilterCollection filters, SortCollection sorts)
        {
            return GetSkus(expands, filters, sorts, null, null);
        }

        public SkuListModel GetSkus(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = SkusEndpoint.List();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<SkuListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new SkuListModel { Skus = (response == null) ? null : response.Skus };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public SkuModel CreateSku(SkuModel model)
        {
            var endpoint = SkusEndpoint.Create();

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<SkuResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return (response == null) ? null : response.Sku;
        }

        public SkuModel UpdateSku(int skuId, SkuModel model)
        {
            var endpoint = SkusEndpoint.Update(skuId);

            var status = new ApiStatus();
            var response = PutResourceToEndpoint<SkuResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.InternalServerError };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (response == null) ? null : response.Sku;
        }

        public bool DeleteSku(int skuId)
        {
            var endpoint = SkusEndpoint.Delete(skuId);

            var status = new ApiStatus();
            var deleted = DeleteResourceFromEndpoint<SkuResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.NoContent, HttpStatusCode.InternalServerError };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return deleted;
        }

        /// <summary>
        /// Znode Version 8.0
        /// To Save Sku Attribute in ZnodeSkuAttribute
        /// </summary>
        /// <param name="skuId">int skuId</param>
        /// <param name="attributeIds">string comma seperated attributeId</param>
        /// <returns>return true/false</returns>
        public bool AddSkuAttribute(int skuId, string attributeIds)
        {
            var endpoint = SkusEndpoint.AddSkuAttribute(skuId, attributeIds);
            var status = new ApiStatus();

            var response = PostResourceToEndpoint<SkuResponse>(endpoint, JsonConvert.SerializeObject(attributeIds), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

            return (response == null) ? false : true;
        }

        public SkuListModel GetSKUListByProductId(int productId)
        {
            var endpoint = SkusEndpoint.GetSKUListByProductId(productId);
           
            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<SkuListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new SkuListModel { Skus = (Equals(response,null)) ? null : response.Skus };
            list.MapPagingDataFromResponse(response);

            return list;
        }
    }
}
