﻿using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    /// <summary>
    /// Tax Rule Types Client
    /// </summary>
	public class TaxRuleTypesClient : BaseClient, ITaxRuleTypesClient
	{
        #region Public Methods

        public TaxRuleTypeModel GetTaxRuleType(int taxRuleTypeId)
        {
            var endpoint = TaxRuleTypesEndpoint.Get(taxRuleTypeId);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<TaxRuleTypeResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (Equals(response, null)) ? null : response.TaxRuleType;
        }

        public TaxRuleTypeListModel GetTaxRuleTypes(FilterCollection filters, SortCollection sorts)
        {
            return GetTaxRuleTypes(filters, sorts, null, null);
        }

        public TaxRuleTypeListModel GetTaxRuleTypes(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = TaxRuleTypesEndpoint.List();
            endpoint += BuildEndpointQueryString(null, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<TaxRuleTypeListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new TaxRuleTypeListModel { TaxRuleTypes = (Equals(response, null)) ? null : response.TaxRuleTypes };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public TaxRuleTypeModel CreateTaxRuleType(TaxRuleTypeModel model)
        {
            var endpoint = TaxRuleTypesEndpoint.Create();

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<TaxRuleTypeResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return (Equals(response, null)) ? null : response.TaxRuleType;
        }

        public TaxRuleTypeModel UpdateTaxRuleType(int taxRuleTypeId, TaxRuleTypeModel model)
        {
            var endpoint = TaxRuleTypesEndpoint.Update(taxRuleTypeId);

            var status = new ApiStatus();
            var response = PutResourceToEndpoint<TaxRuleTypeResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return (Equals(response, null)) ? null : response.TaxRuleType;
        }

        public bool DeleteTaxRuleType(int taxRuleTypeId)
        {
            var endpoint = TaxRuleTypesEndpoint.Delete(taxRuleTypeId);

            var status = new ApiStatus();
            var deleted = DeleteResourceFromEndpoint<TaxRuleTypeResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

            return deleted;
        }

        #region Znode Version 8.0

        public TaxRuleTypeListModel GetAllTaxRuleTypesNotInDatabase(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = TaxRuleTypesEndpoint.GetAllTaxRuleTypesNotInDatabase();
            endpoint += BuildEndpointQueryString(null, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<TaxRuleTypeListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new TaxRuleTypeListModel { TaxRuleTypes = (Equals(response, null)) ? null : response.TaxRuleTypes };
            list.MapPagingDataFromResponse(response);

            return list;
        }
        #endregion 

        #endregion
    }
}
