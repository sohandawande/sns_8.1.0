﻿using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    public class RotateKeyClient : BaseClient, IRotateKeyClient
    {
        public bool GenerateRotateKey()
        {
            var endpoint = RotateEndpoint.GenerateRotateKey();

            var status = new ApiStatus();
            var response = GetBooleanResourceFromEndpoint<RotateKeyResponse>(endpoint, status); 

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

            return response;
        }
    }
}
