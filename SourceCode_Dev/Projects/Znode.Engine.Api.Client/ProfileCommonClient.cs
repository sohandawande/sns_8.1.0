﻿using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    public class ProfileCommonClient : BaseClient, IProfileCommonClient
    {
        public ProfileCommonListModel GetProfileStoreAccess(string userName)
        {
            var endpoint = PermissionsEndPoint.GetProfileStoreAccess();           
            var status = new ApiStatus();
            AccountModel model = new AccountModel();
            model.UserName = userName;
            var response = PostResourceToEndpoint<ProfileCommonListResponse>(endpoint, JsonConvert.SerializeObject(model), status);
            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new ProfileCommonListModel { Profiles = Equals(response,null) ? null : response.Profiles };
            return list;
        }
    }
}
