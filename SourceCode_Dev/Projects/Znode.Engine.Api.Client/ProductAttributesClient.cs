﻿using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    public class ProductAttributesClient : BaseClient, IProductAttributesClient
    {
        public AttributeModel GetAttributes(int attributesId)
        {
            var endpoint = ProductAttributesEndpoint.Get(attributesId);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ProductAttributesResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (response == null) ? null : response.Attributes;
        }

        public ProductAttributeListModel GetAttributes(FilterCollection filters, SortCollection sorts)
        {
            var endpoint = ProductAttributesEndpoint.List();
            endpoint += BuildEndpointQueryString(null, filters, sorts, null, null);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ProductAttributesListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new ProductAttributeListModel { Attributes = (response == null) ? null : response.Attributes };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public AttributeModel CreateAttributes(AttributeModel model)
        {
            var endpoint = ProductAttributesEndpoint.Create();

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<ProductAttributesResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return (response == null) ? null : response.Attributes;
        }

        public AttributeModel UpdateAttributes(int attributesId, AttributeModel model)
        {
            var endpoint = ProductAttributesEndpoint.Update(attributesId);

            var status = new ApiStatus();
            var response = PutResourceToEndpoint<ProductAttributesResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return (response == null) ? null : response.Attributes;
        }

        public bool DeleteAttributes(int attributeId)
        {
            var endpoint = ProductAttributesEndpoint.Delete(attributeId);

            var status = new ApiStatus();
            var deleted = DeleteResourceFromEndpoint<ProductAttributesResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

            return deleted;
        }

        public ProductAttributeListModel GetAttributesByAttributeTypeId(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = ProductAttributesEndpoint.GetAttributesByAttributeTypeId();
            endpoint += BuildEndpointQueryString(null, filters, sorts, pageIndex, pageSize);
            
            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ProductAttributesListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new ProductAttributeListModel { Attributes = (Equals(response, null)) ? null : response.Attributes };
            list.MapPagingDataFromResponse(response);

            return list;

        }
    }
}
