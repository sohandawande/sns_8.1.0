﻿using Newtonsoft.Json;
using System;
using System.Collections.ObjectModel;
using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    public class VendorProductClient : BaseClient, IVendorProductClient
    {
        public VendorProductListModel GetVendorProductList(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = VendorProductEndpoint.List();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<VendorProductListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new VendorProductListModel { Products = (Equals(response, null)) ? null : response.Products };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public VendorProductListModel GetMallAdminProductList(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = VendorProductEndpoint.MallAdminProductList();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<VendorProductListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new VendorProductListModel { Products = (Equals(response, null)) ? null : response.Products };
            list.MapPagingDataFromResponse(response);

            return list;
        }
        public bool ChangeProductStatus(RejectProductModel model, string state)
        {
            var endpoint = VendorProductEndpoint.ChangeProductStatus(state);
            var status = new ApiStatus();
            var success = PostResourceToEndpoint<VendorProductListResponse>(endpoint, JsonConvert.SerializeObject(model), status);
            return !Equals(success, null);
        }

        public VendorProductListModel BindRejectProduct(string productIds)
        {
            var endpoint = VendorProductEndpoint.BindRejectProduct(productIds);
            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<RejectProductModelResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            var list = new VendorProductListModel { RejectProduct = (Equals(response, null)) ? null : response.Product };
            return list;
        }

        public ProductAlternateImageListModel GetReviewImageList(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = VendorProductEndpoint.ImageList();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ProductAlternateImageListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new ProductAlternateImageListModel { ProductImages = (Equals(response, null)) ? null : response.ProductImages };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public bool UpdateProductImageStatus(string productIds, string state)
        {
            var endpoint = VendorProductEndpoint.UpdateProductImageStatus(productIds, state);
            var status = new ApiStatus();
            var success = GetBooleanResourceFromEndpoint<VendorProductListResponse>(endpoint, status);
            return success;
        }

        public ProductModel UpdateVendorProduct(int productId, ProductModel model)
        {
            var endpoint = VendorProductEndpoint.Update(productId);

            var status = new ApiStatus();
            var response = PutResourceToEndpoint<ProductResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return (Equals(response, null)) ? null : response.Product;
        }

        public ProductModel UpdateMarketingDetails(ProductModel model)
        {
            var endpoint = VendorProductEndpoint.UpdateMarketingDetails(model.ProductId);
            var status = new ApiStatus();
            var response = PutResourceToEndpoint<ProductResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return (Equals(response, null)) ? null : response.Product;
        }

        public CategoryNodeListModel GetCategoryNode(int productId)
        {
            var endpoint = VendorProductEndpoint.GetCategoryNode(productId);
            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<CategoryNodeListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new CategoryNodeListModel { CategoryNodes = (Equals(response, null)) ? null : response.CategoryNodes };
            return list;
        }

        public bool DeleteProduct(int productId)
        {
            var endpoint = VendorProductEndpoint.Delete(productId);

            var status = new ApiStatus();
            var deleted = DeleteResourceFromEndpoint<ProductResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

            return deleted;
        }
    }
}
