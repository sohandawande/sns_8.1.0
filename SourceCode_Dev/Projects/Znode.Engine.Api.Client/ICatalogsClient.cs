﻿using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface ICatalogsClient : IBaseClient
    {
        CatalogModel GetCatalog(int catalogId);
        CatalogListModel GetCatalogs(FilterCollection filters, SortCollection sorts);
        CatalogListModel GetCatalogs(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
        CatalogListModel GetCatalogsByCatalogIds(string catalogIds, SortCollection sorts);
        CatalogModel CreateCatalog(CatalogModel model);
        CatalogModel UpdateCatalog(int catalogId, CatalogModel model);
        bool DeleteCatalog(int catalogId);

        /// <summary>
        /// Znode Version 8.0
        /// Copies an existing catalog.
        /// </summary>
        /// <param name="model">The model of type Catalog Model.</param>
        /// <returns>Returns true or false.</returns>
        bool CopyCatalog(CatalogModel model);

        /// <summary>
        /// Znode Version 8.0
        /// Deletes an existing catalog.
        /// </summary>
        /// <param name="catalogId">The Id of catalog</param>
        /// <param name="preserveCategories">boolean value for preserve categories.</param>
        /// <returns>Returns true or false.</returns>
        bool DeleteCatalog(int catalogId, bool preserveCategories);

        /// <summary>
        /// Gets list of catalogs by portal id.
        /// </summary>
        /// <param name="portalId">portalId by which retrieve catalogList</param>
        /// <returns>CatalogListModel</returns>
        CatalogListModel GetCatalogListByPortalId(int portalId);
    }
}
