﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IPRFTERPOrdersClient: IBaseClient
    {
        PRFTOEHeaderHistoryModel GetInvoiceDetailsFromERP(string ExternalId);
    }
}
