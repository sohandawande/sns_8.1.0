﻿using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IRejectionMessageClient : IBaseClient
    {
        /// <summary>
        /// Get the existing Rejection Message.
        /// </summary>
        /// <param name="rejectionMessageId">Id of the Rejection Message.</param>
        /// <returns>RejectionMessageModel</returns>
        RejectionMessageModel GetRejectionMessage(int rejectionMessageId);

        /// <summary>
        /// Get the existing Rejection Message.
        /// </summary>
        /// <param name="rejectionMessageId">Id of the Rejection Message.</param>
        /// <param name="expands">Expand for rejection message.</param>
        /// <returns>RejectionMessageModel</returns>
        RejectionMessageModel GetRejectionMessage(int rejectionMessageId, ExpandCollection expands);

        /// <summary>
        /// Get the rejection message list.
        /// </summary>
        /// <param name="expands">expand collection</param>
        /// <param name="filters">Filter Collection</param>
        /// <param name="sorts">Sort Collection</param>
        /// <returns>RejectionMessageListModel</returns>
        RejectionMessageListModel GetRejectionMessages(ExpandCollection expands, FilterCollection filters, SortCollection sorts);

        /// <summary>
        /// Get the rejection message list.
        /// </summary>
        /// <param name="expands">Expand Collection</param>
        /// <param name="filters">Filter Collection</param>
        /// <param name="sorts">Sort Collection</param>
        /// <param name="pageIndex">Page Index</param>
        /// <param name="pageSize">Page Size</param>
        /// <returns>RejectionMessageListModel</returns>
        RejectionMessageListModel GetRejectionMessages(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Create the Rejection Message.
        /// </summary>
        /// <param name="model">model to create.</param>
        /// <returns>RejectionMessageModel</returns>
        RejectionMessageModel CreateRejectionMessage(RejectionMessageModel model);

        /// <summary>
        /// Update the existing Rejection Message.
        /// </summary>
        /// <param name="rejectionMessageId">Id of the Rejection Message.</param>
        /// <param name="model">model to create.</param>
        /// <returns>RejectionMessageModel</returns>
        RejectionMessageModel UpdateRejectionMessage(int rejectionMessageId, RejectionMessageModel model);

        /// <summary>
        /// Delete the existing Rejection Message.
        /// </summary>
        /// <param name="rejectionMessageId">Id of the Rejection Message.</param>
        /// <returns>Returns True or False.</returns>
        bool DeleteRejectionMessage(int rejectionMessageId);
    }
}
