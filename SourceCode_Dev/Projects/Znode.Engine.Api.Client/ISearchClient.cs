﻿using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface ISearchClient : IBaseClient
	{
		SuggestedSearchListModel GetTypeAheadResponse(SuggestedSearchModel model);
		KeywordSearchModel Search(KeywordSearchModel model, ExpandCollection expands, SortCollection sort);

        /// <summary>
        /// To Get Seo Url Details.
        /// </summary>
        /// <param name="model">SEOUrlModel model</param>
        /// <returns>Returns Seo Url Details having SEOUrlModel format.</returns>
        SEOUrlModel GetSeoUrlDetail(SEOUrlModel model);

        /// <summary>
        /// To Check whether provided Seo Url is restricted or not.
        /// </summary>
        /// <param name="seoUrl">string seoUrl</param>
        /// <returns>Return true or false.</returns>
        bool IsRestrictedSeoUrl(string seoUrl);
	}
}
