﻿using System.Collections.ObjectModel;
using System.Net;
using Newtonsoft.Json;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
	public class PaymentTypesClient : BaseClient, IPaymentTypesClient
	{
		public PaymentTypeModel GetPaymentType(int paymentTypeId)
		{
			var endpoint = PaymentTypesEndpoint.Get(paymentTypeId);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<PaymentTypeResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

			return (response == null) ? null : response.PaymentType;
		}

		public PaymentTypeListModel GetPaymentTypes(FilterCollection filters, SortCollection sorts)
		{
			return GetPaymentTypes(filters, sorts, null, null);
		}

		public PaymentTypeListModel GetPaymentTypes(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
		{
			var endpoint = PaymentTypesEndpoint.List();
			endpoint += BuildEndpointQueryString(null, filters, sorts, pageIndex, pageSize);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<PaymentTypeListResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

			var list = new PaymentTypeListModel { PaymentTypes = (response == null) ? null: response.PaymentTypes };
			list.MapPagingDataFromResponse(response);

			return list;
		}

		public PaymentTypeModel CreatePaymentType(PaymentTypeModel model)
		{
			var endpoint = PaymentTypesEndpoint.Create();

			var status = new ApiStatus();
			var response = PostResourceToEndpoint<PaymentTypeResponse>(endpoint, JsonConvert.SerializeObject(model), status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

			return (response == null) ? null : response.PaymentType;
		}

		public PaymentTypeModel UpdatePaymentType(int paymentTypeId, PaymentTypeModel model)
		{
			var endpoint = PaymentTypesEndpoint.Update(paymentTypeId);

			var status = new ApiStatus();
			var response = PutResourceToEndpoint<PaymentTypeResponse>(endpoint, JsonConvert.SerializeObject(model), status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

			return (response == null) ? null : response.PaymentType;
		}

		public bool DeletePaymentType(int paymentTypeId)
		{
			var endpoint = PaymentTypesEndpoint.Delete(paymentTypeId);

			var status = new ApiStatus();
			var deleted = DeleteResourceFromEndpoint<PaymentTypeResponse>(endpoint, status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

			return deleted;
		}
	}
}
