﻿using System.Collections.ObjectModel;
using System.Net;
using Newtonsoft.Json;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;
using System.Data;
using System;

namespace Znode.Engine.Api.Client
{
    public class PortalProfileClient : BaseClient, IPortalProfileClient
    {
        #region Public Methods
        public PortalProfileModel CreatePortalProfile(PortalProfileModel model)
        {
            var endpoint = PortalProfileEndpoint.Create();

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<PortalProfileResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return (Equals(response, null)) ? null : response.PortalProfile;
        }

        public bool DeletePortalProfile(int portalProfileId)
        {
            var endpoint = PortalProfileEndpoint.Delete(portalProfileId);

            var status = new ApiStatus();
            var deleted = DeleteResourceFromEndpoint<PortalProfileResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

            return deleted;
        }

        public PortalProfileModel GetPortalProfile(int portalProfileId)
        {
            return GetPortalProfile(portalProfileId, null);
        }

        public PortalProfileModel GetPortalProfile(int portalProfileId, ExpandCollection expands)
        {
            var endpoint = PortalProfileEndpoint.Get(portalProfileId);
            endpoint += BuildEndpointQueryString(expands);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<PortalProfileResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (Equals(response, null)) ? null : response.PortalProfile;
        }

        public PortalProfileListModel GetPortalProfiles(ExpandCollection expands, FilterCollection filters, SortCollection sorts)
        {
            return GetPortalProfiles(expands, filters, sorts, null, null);
        }

        public PortalProfileListModel GetPortalProfiles(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = PortalProfileEndpoint.List();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<PortalProfileListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new PortalProfileListModel { PortalProfiles = (Equals(response, null)) ? null : response.PortalProfiles };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public PortalProfileModel UpdatePortalProfile(int portalProfileId, PortalProfileModel model)
        {
            var endpoint = PortalProfileEndpoint.Update(portalProfileId);

            var status = new ApiStatus();
            var response = PutResourceToEndpoint<PortalProfileResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return (Equals(response, null)) ? null : response.PortalProfile;
        }

        public PortalProfileListModel GetPortalProfilesDetails(ExpandCollection expands, FilterCollection filters, SortCollection sorts)
        {
            return GetPortalProfilesDetails(expands, filters, sorts, null, null);
        }

        public PortalProfileListModel GetPortalProfilesDetails(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = PortalProfileEndpoint.GetportalProfileDetails();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<PortalProfileListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new PortalProfileListModel { PortalProfiles = (Equals(response, null)) ? null : response.PortalProfiles };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public PortalProfileListModel GetPortalProfilesByPortalId(int portalId)
        {
            var endpoint = PortalProfileEndpoint.GetPortalProfilesByPortalId(portalId);            

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<PortalProfileListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new PortalProfileListModel { PortalProfiles = (Equals(response, null)) ? null : response.PortalProfiles };
            list.MapPagingDataFromResponse(response);

            return list;
        }
        #endregion
    }
}
