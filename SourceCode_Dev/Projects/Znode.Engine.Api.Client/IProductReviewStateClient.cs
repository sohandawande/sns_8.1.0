﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IProductReviewStateClient
    {
        /// <summary>
        /// Product Review States List.
        /// </summary>
        /// <param name="expands">Expands for Product Review List.</param>
        /// <param name="filters">Filters for Product Review List.</param>
        /// <param name="sorts">Sort for Product Review List.</param>
        /// <returns>Product Review Model.</returns>
        ProductReviewStateListModel GetProductReviewStates(ExpandCollection expands, FilterCollection filters, SortCollection sorts);

        /// <summary>
        /// Product Review States List.
        /// </summary>
        /// <param name="expands">Expands for Product Review List.</param>
        /// <param name="filters">Filters for Product Review List.</param>
        /// <param name="sorts">Sort for Product Review List.</param>
        /// <param name="pageIndex">Page index for Product Review list.</param>
        /// <param name="pageSize">Page Size for Product Review List.</param>
        /// <returns>Product Review Model.</returns>
        ProductReviewStateListModel GetProductReviewStates(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
    }
}
