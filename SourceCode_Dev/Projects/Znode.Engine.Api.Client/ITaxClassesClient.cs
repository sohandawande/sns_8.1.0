﻿using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    /// <summary>
    /// Interafce for Tax class client
    /// </summary>
    public interface ITaxClassesClient : IBaseClient
	{
        /// <summary>
        /// To Get Tax Class by tax class id
        /// </summary>
        /// <param name="taxClassId">int taxClassId</param>
        /// <returns>Returns TaxClassModel</returns>
		TaxClassModel GetTaxClass(int taxClassId);

        /// <summary>
        /// To get List of Tax Classes
        /// </summary>
        /// <param name="filters">Filter Collection</param>
        /// <param name="sorts">Sort Collection</param>
        /// <returns>Returns List of Tax Classes</returns>
		TaxClassListModel GetTaxClasses(FilterCollection filters, SortCollection sorts);

        /// <summary>
        /// To get List of Tax Classes
        /// </summary>
        /// <param name="filters">Filter Collection</param>
        /// <param name="sorts">Sort Collection</param>
        /// <param name="pageIndex">Index of page</param>
        /// <param name="pageSize">Size of page</param>
        /// <returns>Returns List of Tax Classes</returns>
		TaxClassListModel GetTaxClasses(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Create Tax Class
        /// </summary>
        /// <param name="model">TaxClassModel model</param>
        /// <returns>Returns TaxClassModel</returns>
		TaxClassModel CreateTaxClass(TaxClassModel model);

        /// <summary>
        /// Update Tax Class by tax class id
        /// </summary>
        /// <param name="taxClassId">int taxClassId</param>
        /// <param name="model">TaxClassModel model</param>
        /// <returns>Returns TaxClassModel</returns>
		TaxClassModel UpdateTaxClass(int taxClassId, TaxClassModel model);

        /// <summary>
        /// Delete Tax Class by tax class id
        /// </summary>
        /// <param name="taxClassId">int taxClassId</param>
        /// <returns>Returns true/false</returns>
		bool DeleteTaxClass(int taxClassId);

        /// <summary>
        /// Znode Version 8.0
        /// To  Get Active Tax Class By PortalId
        /// </summary>
        /// <param name="portalId">int portalId</param>
        /// <returns>Returns List of Tax Classes</returns>
        TaxClassListModel GetActiveTaxClassByPortalId(int portalId);

        /// <summary>
        /// To Get Tax Class Details
        /// </summary>
        /// <param name="filters">Filter Collection</param>
        /// <param name="sorts">Sort Collection</param>
        /// <param name="pageIndex">Index of page</param>
        /// <param name="pageSize">Size of page</param>
        /// <returns>Returns TaxClassModel</returns>
        TaxClassModel GetTaxClassDetails(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Gets list of tax classes
        /// </summary>
        /// <param name="filters">FilterCollection filters</param>
        /// <param name="sorts">SortCollection sorts</param>
        /// <param name="pageIndex">Index of page</param>
        /// <param name="pageSize">Size of page</param>
        /// <returns>Returns list of tax classes</returns>
        TaxClassListModel GetTaxClassList(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
	}
}
