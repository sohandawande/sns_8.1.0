﻿using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    /// <summary>
    /// Tax Rules Client Interface
    /// </summary>
    public interface ITaxRulesClient : IBaseClient
	{
        /// <summary>
        /// Get tax rule 
        /// </summary>
        /// <param name="taxRuleId">taxRuleId</param>
        /// <returns>Returns TaxRuleModel</returns>
		TaxRuleModel GetTaxRule(int taxRuleId);

        /// <summary>
        /// Get tax rule 
        /// </summary>
        /// <param name="taxRuleId">taxRuleId</param>
        /// <param name="expands">expands for ExpandCollection</param>
        /// <returns>Returns TaxRuleModel</returns>
		TaxRuleModel GetTaxRule(int taxRuleId, ExpandCollection expands);

        /// <summary>
        /// Get tax rules
        /// </summary>
        /// <param name="expands">expands for ExpandCollection </param>
        /// <param name="filters">filters for FilterCollection</param>
        /// <param name="sorts">sorts for SortCollection</param>
        /// <returns>Returns TaxRuleListModel</returns>
		TaxRuleListModel GetTaxRules(ExpandCollection expands, FilterCollection filters, SortCollection sorts);

        /// <summary>
        /// Get tax rules
        /// </summary>
        /// <param name="expands">expands for ExpandCollection</param>
        /// <param name="filters">filters for FilterCollection</param>
        /// <param name="sorts">sorts for SortCollection</param>
        /// <param name="pageIndex">pageIndex</param>
        /// <param name="pageSize">pageSize</param>
        /// <returns>Returns TaxRuleListModel</returns>
		TaxRuleListModel GetTaxRules(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Create Tax Rule
        /// </summary>
        /// <param name="model">TaxRuleModel</param>
        /// <returns>Returns TaxRuleModel</returns>
		TaxRuleModel CreateTaxRule(TaxRuleModel model);

        /// <summary>
        /// Update Tax Rule
        /// </summary>
        /// <param name="taxRuleId">taxRuleId</param>
        /// <param name="model">TaxRuleModel</param>
        /// <returns>Returns TaxRuleModel</returns>
		TaxRuleModel UpdateTaxRule(int taxRuleId, TaxRuleModel model);

        /// <summary>
        /// Delete Tax Rule
        /// </summary>
        /// <param name="taxRuleId">taxRuleId</param>
        /// <returns>Returns true/false</returns>
		bool DeleteTaxRule(int taxRuleId);
	}
}
