﻿using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;


namespace Znode.Engine.Api.Client
{
    /// <summary>
    /// Clent of Content Page Revisions.
    /// </summary>
    public class ContentPageRevisionClient : BaseClient, IContentPageRevisionClient
    {
        #region Public Methods
        public ContentPageRevisionListModel GetContentPageRevisions(ExpandCollection expands, FilterCollection filters, SortCollection sorts)
        {
            return GetContentPageRevisions(expands, filters, sorts, null, null);
        }

        public ContentPageRevisionListModel GetContentPageRevisions(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = ContentPageRevisionEndpoint.List();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ContentPageRevisionListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new ContentPageRevisionListModel { ContentPageRevisions = (response == null) ? null : response.ContentPageRevisions };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public ContentPageRevisionListModel GetContentPageRevisionsById(int contentPageId)
        {
            var endpoint = ContentPageRevisionEndpoint.GetContentPageRevisionsById(contentPageId);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ContentPageRevisionListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new ContentPageRevisionListModel { ContentPageRevisions = Equals(response, null) ? null : response.ContentPageRevisions };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public bool RevertRevision(int revisionId, ContentPageRevisionModel model)
        {
            var endpoint = ContentPageRevisionEndpoint.RevertRevision(revisionId);

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<ContentPageRevisionResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return response.IsRevertRevision;
        }

        public ContentPageRevisionListModel GetContentPageRevisionOnId(int contentPageId, FilterCollection filters = null, SortCollection sorts = null, int? pageIndex = 1, int? pageSize = 10)
        {
            return contentPageId > 0 ? GetContentPageRevisionListByIds(contentPageId, filters, sorts, pageIndex, pageSize) : null;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Get Content Page Revision List By content page Id
        /// </summary>
        /// <param name="contentPageId">Id of the content page</param>
        /// <param name="filters">FilterCollection filters</param>
        /// <param name="sorts">SortCollection sorts</param>
        /// <param name="pageIndex">int? pageIndex</param>
        /// <param name="pageSize">int? pageSize</param>
        /// <returns>ContentPageRevisionListModel</returns>
        private ContentPageRevisionListModel GetContentPageRevisionListByIds(int contentPageId, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = ContentPageRevisionEndpoint.List(contentPageId);
            endpoint += BuildEndpointQueryString(null, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ContentPageRevisionListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new ContentPageRevisionListModel { ContentPageRevisions = Equals(response, null) ? null : response.ContentPageRevisions };
            list.MapPagingDataFromResponse(response);

            return list;
        } 

        #endregion

    }
}
