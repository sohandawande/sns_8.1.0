﻿using System.Collections.ObjectModel;
using System.Net;
using Newtonsoft.Json;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    public class PortalCatalogsClient : BaseClient, IPortalCatalogsClient
    {
        #region Public Methods
        public PortalCatalogModel GetPortalCatalog(int portalCatalogId)
        {
            return GetPortalCatalog(portalCatalogId, null);
        }

        public PortalCatalogModel GetPortalCatalog(int portalCatalogId, ExpandCollection expands)
        {
            var endpoint = PortalCatalogsEndpoint.Get(portalCatalogId);
            endpoint += BuildEndpointQueryString(expands);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<PortalCatalogResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (Equals(response, null)) ? null : response.PortalCatalog;
        }

        public PortalCatalogListModel GetPortalCatalogs(ExpandCollection expands, FilterCollection filters, SortCollection sorts)
        {
            return GetPortalCatalogs(expands, filters, sorts, null, null);
        }

        public PortalCatalogListModel GetPortalCatalogs(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = PortalCatalogsEndpoint.List();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<PortalCatalogListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new PortalCatalogListModel { PortalCatalogs = (Equals(response, null)) ? null : response.PortalCatalogs };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public PortalCatalogListModel GetPortalCatalogsByPortalId(int portalId)
        {
            var endpoint = PortalCatalogsEndpoint.GetPortalCatalogsByPortalId(portalId);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<PortalCatalogListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new PortalCatalogListModel { PortalCatalogs = Equals(response, null) ? null : response.PortalCatalogs };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public PortalCatalogModel CreatePortalCatalog(PortalCatalogModel model)
        {
            var endpoint = PortalCatalogsEndpoint.Create();

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<PortalCatalogResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return Equals(response, null) ? null : response.PortalCatalog;
        }

        public PortalCatalogModel UpdatePortalCatalog(int portalCatalogId, PortalCatalogModel model)
        {
            var endpoint = PortalCatalogsEndpoint.Update(portalCatalogId);

            var status = new ApiStatus();
            var response = PutResourceToEndpoint<PortalCatalogResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return Equals(response,null) ? null : response.PortalCatalog;
        }

        public bool DeletePortalCatalog(int portalCatalogId)
        {
            var endpoint = PortalCatalogsEndpoint.Delete(portalCatalogId);

            var status = new ApiStatus();
            var deleted = DeleteResourceFromEndpoint<PortalCatalogResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

            return deleted;
        }
        #endregion
    }
}
