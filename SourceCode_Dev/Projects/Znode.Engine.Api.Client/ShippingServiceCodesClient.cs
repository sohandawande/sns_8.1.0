﻿using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    /// <summary>
    /// Shipping Service Codes Client 
    /// </summary>
	public class ShippingServiceCodesClient : BaseClient, IShippingServiceCodesClient
	{
        #region Public Methods

        public ShippingServiceCodeModel GetShippingServiceCode(int shippingServiceCodeId)
        {
            return GetShippingServiceCode(shippingServiceCodeId, null);
        }

        public ShippingServiceCodeModel GetShippingServiceCode(int shippingServiceCodeId, ExpandCollection expands)
        {
            var endpoint = ShippingServiceCodesEndpoint.Get(shippingServiceCodeId);
            endpoint += BuildEndpointQueryString(expands);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ShippingServiceCodeResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (Equals(response, null)) ? null : response.ShippingServiceCode;
        }

        public ShippingServiceCodeListModel GetShippingServiceCodes(ExpandCollection expands, FilterCollection filters, SortCollection sorts)
        {
            return GetShippingServiceCodes(expands, filters, sorts, null, null);
        }

        public ShippingServiceCodeListModel GetShippingServiceCodes(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = ShippingServiceCodesEndpoint.List();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ShippingServiceCodeListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new ShippingServiceCodeListModel { ShippingServiceCodes = (Equals(response, null)) ? null : response.ShippingServiceCodes };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public ShippingServiceCodeModel CreateShippingServiceCode(ShippingServiceCodeModel model)
        {
            var endpoint = ShippingServiceCodesEndpoint.Create();

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<ShippingServiceCodeResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return (Equals(response, null)) ? null : response.ShippingServiceCode;
        }

        public ShippingServiceCodeModel UpdateShippingServiceCode(int shippingServiceCodeId, ShippingServiceCodeModel model)
        {
            var endpoint = ShippingServiceCodesEndpoint.Update(shippingServiceCodeId);

            var status = new ApiStatus();
            var response = PutResourceToEndpoint<ShippingServiceCodeResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return (Equals(response, null)) ? null : response.ShippingServiceCode;
        }

        public bool DeleteShippingServiceCode(int shippingServiceCodeId)
        {
            var endpoint = ShippingServiceCodesEndpoint.Delete(shippingServiceCodeId);

            var status = new ApiStatus();
            var deleted = DeleteResourceFromEndpoint<ShippingServiceCodeResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

            return deleted;
        } 

        #endregion
	}
}
