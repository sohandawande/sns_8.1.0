﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface ICurrencyTypeClient:IBaseClient
    {
        /// <summary>
        /// Gets Currency Types.
        /// </summary>
        /// <param name="expands"></param>
        /// <param name="filters"></param>
        /// <param name="sorts"></param>
        /// <returns></returns>
        CurrencyTypeListModel GetCurrencyTypes(ExpandCollection expands, FilterCollection filters, SortCollection sorts);
        /// <summary>
        /// Gets Currency Types.
        /// </summary>
        /// <param name="expands"></param>
        /// <param name="filters"></param>
        /// <param name="sorts"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        CurrencyTypeListModel GetCurrencyTypes(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
        /// <summary>
        /// Gets Currency Type.
        /// </summary>
        /// <param name="currencyTypeId"></param>
        /// <returns></returns>
        CurrencyTypeModel GetCurrencyType(int currencyTypeId);

        /// <summary>
        /// Update currency type.
        /// </summary>
        /// <param name="currencyTypeId">Currency type id.</param>
        /// <param name="model">Currency Type Model.</param>
        /// <returns>Currency Type Model.</returns>
        CurrencyTypeModel UpdateCurrencyType(int currencyTypeId, CurrencyTypeModel model);
    }
}
