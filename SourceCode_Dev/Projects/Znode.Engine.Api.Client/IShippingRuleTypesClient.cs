﻿using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    /// <summary>
    /// Shipping Rule Type Client Interface
    /// </summary>
    public interface IShippingRuleTypesClient : IBaseClient
	{
        /// <summary>
        /// Get Shipping Rule Type on the basis of shippingRuleTypeId
        /// </summary>
        /// <param name="shippingRuleTypeId">shippingRuleTypeId</param>
        /// <returns>Returns Shipping Rule Type</returns>
		ShippingRuleTypeModel GetShippingRuleType(int shippingRuleTypeId);

        /// <summary>
        /// Get all Shipping Rule Types
        /// </summary>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <returns>Returns list of Shipping Rule Type</returns>
		ShippingRuleTypeListModel GetShippingRuleTypes(FilterCollection filters, SortCollection sorts);

        /// <summary>
        /// Get all Shipping Rule Types
        /// </summary>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <param name="pageIndex">Index of page</param>
        /// <param name="pageSize">Size of page</param>
        /// <returns>Returns list of Shipping Rule Type</returns>
		ShippingRuleTypeListModel GetShippingRuleTypes(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Create Shipping Rule Type 
        /// </summary>
        /// <param name="model">ShippingRuleTypeModel model</param>
        /// <returns>Returns created model</returns>
		ShippingRuleTypeModel CreateShippingRuleType(ShippingRuleTypeModel model);

        /// <summary>
        /// Update Shipping Rule Type on the basis of shipping rule type id
        /// </summary>
        /// <param name="shippingRuleTypeId">shippingRuleTypeId</param>
        /// <param name="model">ShippingRuleTypeModel model</param>
        /// <returns>Returns updated Shipping Rule Type</returns>
		ShippingRuleTypeModel UpdateShippingRuleType(int shippingRuleTypeId, ShippingRuleTypeModel model);

        /// <summary>
        /// Delete Shipping Rule Type on the basis of shipping rule type id
        /// </summary>
        /// <param name="shippingRuleTypeId">shippingRuleTypeId</param>
        /// <returns>Returns true/false</returns>
		bool DeleteShippingRuleType(int shippingRuleTypeId);
	}
}
