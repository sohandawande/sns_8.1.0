﻿using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IPaymentGatewaysClient : IBaseClient
	{
		PaymentGatewayModel GetPaymentGateway(int paymentGatewayId);
		PaymentGatewayListModel GetPaymentGateways(FilterCollection filters, SortCollection sorts);
		PaymentGatewayListModel GetPaymentGateways(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
		PaymentGatewayModel CreatePaymentGateway(PaymentGatewayModel model);
		PaymentGatewayModel UpdatePaymentGateway(int paymentGatewayId, PaymentGatewayModel model);
		bool DeletePaymentGateway(int paymentGatewayId);
	}
}
