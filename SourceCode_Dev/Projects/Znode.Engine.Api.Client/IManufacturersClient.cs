﻿using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IManufacturersClient : IBaseClient
	{
        /// <summary>
        /// This method will fetch the manufacturer with relevant Manufacturer id
        /// </summary>
        /// <param name="manufacturerId">int Manufacturer Id</param>
        /// <returns>caomplete data of Manufacturer</returns>
		ManufacturerModel GetManufacturer(int manufacturerId);

        /// <summary>
        /// This method will return the manufacturer based on manufacturer id
        /// </summary>
        /// <param name="filters">FilterCollection filter</param>
        /// <param name="sorts">SortCollection sorts</param>
        /// <returns></returns>
        ManufacturerListModel GetManufacturers(FilterCollection filters, SortCollection sorts);
		
        /// <summary>
        /// This method will return the manufacturer based on manufacturer id
        /// </summary>
        /// <param name="filters">FilterCollection filters collection</param>
        /// <param name="sorts">SortCollection sort collection</param>
        /// <param name="pageIndex">int page index</param>
        /// <param name="pageSize">int page size</param>
        /// <returns></returns>
        ManufacturerListModel GetManufacturers(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
		
        /// <summary>
        /// This method will save the data of Manufacturer
        /// </summary>
        /// <param name="model">ManufacturerModel model</param>
        /// <returns></returns>
        ManufacturerModel CreateManufacturer(ManufacturerModel model);
		
        /// <summary>
        /// This method will update the manufacture details
        /// </summary>
        /// <param name="manufacturerId">int Manufacturer id</param>
        /// <param name="model">ManufacturerModel model</param>
        /// <returns>Updated data of the Manufacturer</returns>
        ManufacturerModel UpdateManufacturer(int manufacturerId, ManufacturerModel model);
		
        /// <summary>
        /// This method will delete the manufacturer by manufacturer id
        /// </summary>
        /// <param name="manufacturerId">int manufacturer id</param>
        /// <returns>True if data deleted</returns>
        bool DeleteManufacturer(int manufacturerId);
	}
}
