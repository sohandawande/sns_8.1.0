﻿using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    /// <summary>
    /// Shipping Types Client Interface
    /// </summary>
    public interface IShippingTypesClient : IBaseClient
	{
        /// <summary>
        /// Get shipping Type
        /// </summary>
        /// <param name="shippingTypeId">shippingTypeId</param>
        /// <returns>Returns Shipping Type</returns>
		ShippingTypeModel GetShippingType(int shippingTypeId);

        /// <summary>
        /// Get List of Shipping Types
        /// </summary>
        /// <param name="filters">FilterCollection filters</param>
        /// <param name="sorts">SortCollection sorts</param>
        /// <returns>Returns list of  Shipping Type</returns>
		ShippingTypeListModel GetShippingTypes(FilterCollection filters, SortCollection sorts);

        /// <summary>
        /// Get List of Shipping Types
        /// </summary>
        /// <param name="filters">FilterCollection filters</param>
        /// <param name="sorts"> SortCollection sorts</param>
        /// <param name="pageIndex">Index of page</param>
        /// <param name="pageSize">Size of page</param>
        /// <returns>Returns list of  Shipping Type</returns>
		ShippingTypeListModel GetShippingTypes(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Create Shipping Type
        /// </summary>
        /// <param name="model">ShippingTypeModel model</param>
        /// <returns>Returns created Shipping Type Model</returns>
		ShippingTypeModel CreateShippingType(ShippingTypeModel model);

        /// <summary>
        /// Update Shipping Type
        /// </summary>
        /// <param name="shippingTypeId">shippingTypeId</param>
        /// <param name="model">ShippingTypeModel model</param>
        /// <returns>Returns updated Shipping Type Model</returns>
		ShippingTypeModel UpdateShippingType(int shippingTypeId, ShippingTypeModel model);

        /// <summary>
        /// Delete Shipping Type
        /// </summary>
        /// <param name="shippingTypeId">shippingTypeId</param>
        /// <returns>Returns true/false</returns>
		bool DeleteShippingType(int shippingTypeId);

        /// <summary>
        /// ZNode Version 8.0
        /// Get all Shipping Types not present in database.
        /// </summary>
        /// <param name="filters">FilterCollection filters</param>
        /// <param name="sorts">SortCollection sorts</param>
        /// <param name="pageIndex">Nullable int pageIndex</param>
        /// <param name="pageSize">Nullable int pageSize</param>
        /// <returns>Returns shipping types list</returns>
        ShippingTypeListModel GetAllShippingTypesNotInDatabase(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
	}
}
