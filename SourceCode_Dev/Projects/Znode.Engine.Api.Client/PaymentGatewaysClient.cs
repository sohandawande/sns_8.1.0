﻿using System.Collections.ObjectModel;
using System.Net;
using Newtonsoft.Json;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
	public class PaymentGatewaysClient : BaseClient, IPaymentGatewaysClient
	{
		public PaymentGatewayModel GetPaymentGateway(int paymentGatewayId)
		{
			var endpoint = PaymentGatewaysEndpoint.Get(paymentGatewayId);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<PaymentGatewayResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

			return (response == null) ? null : response.PaymentGateway;
		}

		public PaymentGatewayListModel GetPaymentGateways(FilterCollection filters, SortCollection sorts)
		{
			return GetPaymentGateways(filters, sorts, null, null);
		}

		public PaymentGatewayListModel GetPaymentGateways(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
		{
			var endpoint = PaymentGatewaysEndpoint.List();
			endpoint += BuildEndpointQueryString(null, filters, sorts, pageIndex, pageSize);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<PaymentGatewayListResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new PaymentGatewayListModel { PaymentGateways = (response == null) ? null : response.PaymentGateways };
			list.MapPagingDataFromResponse(response);

			return list;
		}

		public PaymentGatewayModel CreatePaymentGateway(PaymentGatewayModel model)
		{
			var endpoint = PaymentGatewaysEndpoint.Create();

			var status = new ApiStatus();
			var response = PostResourceToEndpoint<PaymentGatewayResponse>(endpoint, JsonConvert.SerializeObject(model), status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

			return (response == null) ? null : response.PaymentGateway;
		}

		public PaymentGatewayModel UpdatePaymentGateway(int paymentGatewayId, PaymentGatewayModel model)
		{
			var endpoint = PaymentGatewaysEndpoint.Update(paymentGatewayId);

			var status = new ApiStatus();
			var response = PutResourceToEndpoint<PaymentGatewayResponse>(endpoint, JsonConvert.SerializeObject(model), status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

			return (response == null) ? null : response.PaymentGateway;
		}

		public bool DeletePaymentGateway(int paymentGatewayId)
		{
			var endpoint = PaymentGatewaysEndpoint.Delete(paymentGatewayId);

			var status = new ApiStatus();
			var deleted = DeleteResourceFromEndpoint<PaymentGatewayResponse>(endpoint, status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

			return deleted;
		}
	}
}
