﻿using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IReasonForReturnClient : IBaseClient
    {
        /// <summary>
        /// Method gets the ReasonForReturn details.
        /// </summary>
        /// <param name="reasonForReturnId">The Id of ReasonForReturn</param>
        /// <returns>Returns ReasonForReturn details.</returns>
        ReasonForReturnModel GetReasonForReturn(int reasonForReturnId);

        /// <summary>
        /// Method gets the ReasonForReturn details.
        /// </summary>
        /// <param name="reasonForReturnId">The Id of ReasonForReturn</param>
        /// <param name="expands">Expands collection</param>
        /// <returns>Returns ReasonForReturn details.</returns>
        ReasonForReturnModel GetReasonForReturn(int reasonForReturnId, ExpandCollection expands);

        /// <summary>
        /// Method Returns the ReasonForReturn List.
        /// </summary>
        /// <param name="expands">Expands collection</param>
        /// <param name="filters">Filter collection</param>
        /// <param name="sorts">Sort collection</param>
        /// <returns>Returns the ReasonForReturn List.</returns>
        ReasonForReturnListModel GetListOfReasonForReturn(ExpandCollection expands, FilterCollection filters, SortCollection sorts);

        /// <summary>
        /// Method Returns the ReasonForReturn List.
        /// </summary>
        /// <param name="expands">Expands collection</param>
        /// <param name="filters">Filter collection</param>
        /// <param name="sorts">Sort collection</param>
        /// <param name="pageIndex">Index of page</param>
        /// <param name="pageSize">Size of page</param>
        /// <returns>Returns the ReasonForReturn List.</returns>
        ReasonForReturnListModel GetListOfReasonForReturn(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Method Creates the ReasonForReturn.
        /// </summary>
        /// <param name="model">Model of ReasonForReturn</param>
        /// <returns>Return the ReasonForReturn.</returns>
        ReasonForReturnModel CreateReasonForReturn(ReasonForReturnModel model);

        /// <summary>
        /// Method Updates the ReasonForReturn on the basis of ReasonForReturn Id.
        /// </summary>
        /// <param name="reasonForReturnId">ReasonForReturn</param>
        /// <param name="model">Model of ReasonForReturn</param>
        /// <returns>Returns Updated ReasonForReturn.</returns>
        ReasonForReturnModel UpdateReasonForReturn(int reasonForReturnId, ReasonForReturnModel model);

        /// <summary>
        /// Deletes an existing ReasonForReturn
        /// </summary>
        /// <param name="reasonForReturnId">ReasonForReturnId by which ReasonForReturn will delete</param>
        /// <returns>boolean value true/false</returns>
        bool DeleteReasonForReturn(int reasonForReturnId);
    }
}
