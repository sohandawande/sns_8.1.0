﻿
namespace Znode.Engine.Api.Client
{
    /// <summary>
    /// This is the interface which will contact to the controller/agent.
    /// </summary>
    public interface IClearCacheClient : IBaseClient
    {
        bool ClearAPICache();
    }
}
