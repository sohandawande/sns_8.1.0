﻿using System.Collections.ObjectModel;
using System.Net;
using Newtonsoft.Json;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    public class PortalCountriesClient : BaseClient, IPortalCountriesClient
    {
        public PortalCountryModel GetPortalCountry(int portalCountryId)
        {
            return GetPortalCountry(portalCountryId, null);
        }

        public PortalCountryModel GetPortalCountry(int portalCountryId, ExpandCollection expands)
        {
            var endpoint = PortalCountriesEndpoint.Get(portalCountryId);
            endpoint += BuildEndpointQueryString(expands);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<PortalCountryResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (Equals(response, null)) ? null : response.PortalCountry;
        }

        public PortalCountryListModel GetPortalCountries(ExpandCollection expands, FilterCollection filters, SortCollection sorts)
        {
            return GetPortalCountries(expands, filters, sorts, null, null);
        }

        public PortalCountryListModel GetPortalCountries(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
		{
			var endpoint = PortalCountriesEndpoint.List();
			endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<PortalCountryListResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new PortalCountryListModel { PortalCountries = (Equals(response, null)) ? null : response.PortalCountries };
			list.MapPagingDataFromResponse(response);

			return list;
		}

        public PortalCountryModel CreatePortalCountry(PortalCountryModel model)
        {
            var endpoint = PortalCountriesEndpoint.Create();

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<PortalCountryResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return (Equals(response, null)) ? null : response.PortalCountry;
        }

        public PortalCountryModel UpdatePortalCountry(int portalCountryId, PortalCountryModel model)
        {
            var endpoint = PortalCountriesEndpoint.Update(portalCountryId);

            var status = new ApiStatus();
            var response = PutResourceToEndpoint<PortalCountryResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return Equals(response, null) ? null : response.PortalCountry;
        }

        public bool DeletePortalCountry(int portalCountryId)
        {
            var endpoint = PortalCountriesEndpoint.Delete(portalCountryId);

            var status = new ApiStatus();
            var deleted = DeleteResourceFromEndpoint<PortalCountryResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

            return deleted;
        }

        #region ZNode Version 8.0

        public PortalCountryListModel GetAllPortalCountries(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = PortalCountriesEndpoint.GetAllPortalCountries();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<PortalCountryListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new PortalCountryListModel { PortalCountries = (Equals(response, null)) ? null : response.PortalCountries };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public bool DeletePortalCountryIfNotAssociated(int portalCountryId)
        {
            var endpoint = PortalCountriesEndpoint.DeleteIfNotAssociated(portalCountryId);

            var status = new ApiStatus();
            var deleted = DeleteResourceFromEndpoint<PortalCountryResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

            return deleted;
        }

        public bool CreatePortalCountries(int portalId, string billableCountryCode, string shippableCountryCode)
        {
            var endpoint = PortalCountriesEndpoint.CreatePortalCountries(portalId, billableCountryCode, shippableCountryCode);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<PortalCountryResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return response.IsPortalCountryAdded;
        }
        #endregion


    }
}
