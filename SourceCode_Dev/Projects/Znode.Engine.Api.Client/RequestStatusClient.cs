﻿using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    public class RequestStatusClient:BaseClient,IRequestStatusClient
    {
        #region Public Methods
        public RequestStatusListModel GetRequestStatus(ExpandCollection expands, FilterCollection filters, SortCollection sorts)
        {
            return GetRequestStatus(expands, filters, sorts, null, null);
        }

        public RequestStatusListModel GetRequestStatus(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = RequestStatusEndPoint.List();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<RequestStatusListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new RequestStatusListModel { RequestStatusList = (Equals(response, null)) ? null : response.RequestStatusList };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public RequestStatusModel GetRequestStatus(int requestStatusId)
        {
            return GetRequestStatus(requestStatusId, null);
        }

        public RequestStatusModel GetRequestStatus(int requestStatusId, ExpandCollection expands)
        {
            var endpoint = RequestStatusEndPoint.Get(requestStatusId);
            endpoint += BuildEndpointQueryString(expands);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<RequestStatusResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return Equals(response, null) ? null : response.RequestStatus;
        }

        public RequestStatusModel CreateRequestStatus(RequestStatusModel model)
        {
            var endpoint = RequestStatusEndPoint.Create();

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<RequestStatusResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return (Equals(response, null)) ? null : response.RequestStatus;
        }

        public RequestStatusModel UpdateRequestStatus(int requestStatusId, RequestStatusModel model)
        {
            var endpoint = RequestStatusEndPoint.Update(requestStatusId);

            var status = new ApiStatus();
            var response = PutResourceToEndpoint<RequestStatusResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return (Equals(response, null)) ? null : response.RequestStatus;
        }

        public bool DeleteRequestStatus(int requestStatusId)
        {
            var endpoint = RequestStatusEndPoint.Delete(requestStatusId);

            var status = new ApiStatus();
            var deleted = DeleteResourceFromEndpoint<RequestStatusResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

            return deleted;
        }
        #endregion
    }
}
