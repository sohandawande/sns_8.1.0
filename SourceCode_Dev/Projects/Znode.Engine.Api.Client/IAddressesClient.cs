﻿using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
	public interface IAddressesClient : IBaseClient
	{
		AddressModel GetAddress(int addressId, bool refreshCache = false);

        /// <summary>
        /// Get Address List.
        /// </summary>
        /// <param name="filters">FilterCollection for filtering Vebdor account data</param>
        /// <param name="sorts">sorting address record.</param>
        /// <param name="refreshCache">refresh address cache </param>
        /// <returns></returns>
		AddressListModel GetAddresses(FilterCollection filters, SortCollection sorts, bool refreshCache = false);

        /// <summary>
        /// Get Address List.
        /// </summary>
        /// <param name="filters">FilterCollection for filtering Vebdor account data</param>
        /// <param name="sorts">sorting address record.</param>
        /// <param name="pageIndex">page index for address data</param>
        /// <param name="pageSize">total record page size</param>
        /// <param name="refreshCache">refresh address cache</param>
        /// <returns></returns>
		AddressListModel GetAddresses(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize, bool refreshCache = false);

        /// <summary>
        /// Create New Address.
        /// </summary>
        /// <param name="model">AddressModel</param>
        /// <returns>AddressModel</returns>
		AddressModel CreateAddress(AddressModel model);

        /// <summary>
        /// Update Existing Address.
        /// </summary>
        /// <param name="addressId">addressId</param>
        /// <param name="model">AddressModel</param>
        /// <returns>AddressModel</returns>
		AddressModel UpdateAddress(int addressId, AddressModel model);

        /// <summary>
        /// Delete Existing Address.
        /// </summary>
        /// <param name="addressId">addressId</param>
        /// <returns>returns true / false</returns>
		bool DeleteAddress(int addressId);

        /// <summary>
        /// Set address as default address.
        /// </summary>
        /// <param name="addressId">address id</param>
        /// <param name="model">AddressModel</param>
        /// <returns>AddressModel</returns>
        AddressModel UpdateDefaultAddress(int addressId, AddressModel model);
	}
}
