﻿using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IDomainsClient : IBaseClient
	{
		DomainModel GetDomain(int domainId);
		DomainListModel GetDomains(FilterCollection filters, SortCollection sorts);
		DomainListModel GetDomains(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
		DomainModel CreateDomain(DomainModel model);
		DomainModel UpdateDomain(int domainId, DomainModel model);
		bool DeleteDomain(int domainId);
	}
}
