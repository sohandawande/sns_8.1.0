﻿using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    /// <summary>
    /// Tax Classes Client 
    /// </summary>
	public class TaxClassesClient : BaseClient, ITaxClassesClient
	{
        #region Public Methods

		public TaxClassModel GetTaxClass(int taxClassId)
		{
			var endpoint = TaxClassesEndpoint.Get(taxClassId);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<TaxClassResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (Equals(response, null)) ? null : response.TaxClass;
		}

		public TaxClassListModel GetTaxClasses(FilterCollection filters, SortCollection sorts)
		{
			return GetTaxClasses(filters, sorts, null, null);
		}

		public TaxClassListModel GetTaxClasses(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
		{
			var endpoint = TaxClassesEndpoint.List();
			endpoint += BuildEndpointQueryString(null, filters, sorts, pageIndex, pageSize);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<TaxClassListResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new TaxClassListModel { TaxClasses = (Equals(response, null)) ? null : response.TaxClasses };
			list.MapPagingDataFromResponse(response);

			return list;
		}

        public TaxClassListModel GetTaxClassList(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = TaxClassesEndpoint.TaxClassList();
            endpoint += BuildEndpointQueryString(null, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<TaxClassListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new TaxClassListModel { TaxClasses = (Equals(response, null)) ? null : response.TaxClasses };
			list.MapPagingDataFromResponse(response);

			return list;
		}
       

		public TaxClassModel CreateTaxClass(TaxClassModel model)
		{
			var endpoint = TaxClassesEndpoint.Create();

			var status = new ApiStatus();
			var response = PostResourceToEndpoint<TaxClassResponse>(endpoint, JsonConvert.SerializeObject(model), status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return (Equals(response, null)) ? null : response.TaxClass;
		}

		public TaxClassModel UpdateTaxClass(int taxClassId, TaxClassModel model)
		{
			var endpoint = TaxClassesEndpoint.Update(taxClassId);

			var status = new ApiStatus();
			var response = PutResourceToEndpoint<TaxClassResponse>(endpoint, JsonConvert.SerializeObject(model), status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return (Equals(response, null)) ? null : response.TaxClass;
		}

		public bool DeleteTaxClass(int taxClassId)
		{
			var endpoint = TaxClassesEndpoint.Delete(taxClassId);

			var status = new ApiStatus();
			var deleted = DeleteResourceFromEndpoint<TaxClassResponse>(endpoint, status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

			return deleted;
		}

        public TaxClassListModel GetActiveTaxClassByPortalId(int portalId)
        {
            var endpoint = TaxClassesEndpoint.GetActiveCountryByPortalId(portalId);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<TaxClassListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new TaxClassListModel { TaxClasses = (Equals(response, null)) ? null : response.TaxClasses };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public TaxClassModel GetTaxClassDetails(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = TaxClassesEndpoint.GetTaxClassDetails();
            endpoint += BuildEndpointQueryString(null, filters, sorts, pageIndex, pageSize);
            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<TaxClassResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (Equals(response, null)) ? null : response.TaxClass;
        }

        #endregion
	}
}
