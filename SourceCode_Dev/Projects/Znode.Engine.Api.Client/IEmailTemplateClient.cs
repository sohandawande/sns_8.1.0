﻿using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IEmailTemplateClient : IBaseClient
    {
        /// <summary>
        /// Get list of Templates.
        /// </summary>
        /// <param name="filters">FilterCollection</param>
        /// <param name="sorts">SortCollection</param>
        /// <param name="pageIndex">index of page</param>
        /// <param name="pageSize">size of page</param>
        /// <returns>EmailTemplateListModel</returns>
        EmailTemplateListModel GetTemplates(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Create new template page.
        /// </summary>
        /// <param name="model">EmailTemplateModel</param>
        /// <returns>EmailTemplateModel</returns>
        EmailTemplateModel CreateTemplatePage(EmailTemplateModel model);

        /// <summary>
        /// Get template details by template name
        /// </summary>
        /// <param name="templateName">name of template page</param>
        /// <param name="extension">extension of template page</param>
        /// <returns>EmailTemplateModel</returns>
        EmailTemplateModel GetTemplatePage(string templateName, string extension);

        /// <summary>
        /// Update existing template.
        /// </summary>
        /// <param name="model">EmailTemplateModel</param>
        /// <returns>true / false</returns>
        bool UpdateTemplatePage(EmailTemplateModel model);

        /// <summary>
        /// Delete the existing template page.
        /// </summary>
        /// <param name="templateName">template page name</param>
        /// <param name="extension">extension of template page name</param>
        /// <returns>true / false</returns>
        bool DeleteTemplatePage(string templateName, string extension);

        /// <summary>
        /// Get all Deleted template files.
        /// </summary>
        /// <param name="expands">ExpandCollection</param>
        /// <param name="filters">FilterCollection</param>
        /// <param name="sorts">SortCollection</param>
        /// <returns>EmailTemplateListModel</returns>
        EmailTemplateListModel GetDeletedTemplates(ExpandCollection expands, FilterCollection filters, SortCollection sorts);

    }
}
