﻿
namespace Znode.Engine.Api.Client
{
    public interface ILoadPluginsClient : IBaseClient
    {
        /// <summary>
        /// Load Plugins
        /// </summary>
        /// <returns>Return boolean result</returns>
        bool IsPluginsLoaded();
    }
}
