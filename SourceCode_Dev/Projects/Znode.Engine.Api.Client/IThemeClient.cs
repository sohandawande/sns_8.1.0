﻿using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
namespace Znode.Engine.Api.Client
{
    public interface IThemeClient : IBaseClient
    {
        /// <summary>
        /// Gets Theme list.
        /// </summary>
        /// <param name="filters">Filter Collection for theme List.</param>
        /// <param name="sorts">Sort collection of Theme List.</param>
        /// <returns>Theme List model.</returns>
        ThemeListModel GetThemes(FilterCollection filters, SortCollection sorts);

        /// <summary>
        /// Gets Theme list.
        /// </summary>
        /// <param name="filters">Filter Collection for theme List.</param>
        /// <param name="sorts">Sort collection of Theme List.</param>
        /// <param name="pageIndex">Index of requesting page</param>
        /// <param name="pageSize">Record per page</param>
        /// <returns>List of themes</returns>
        ThemeListModel GetThemes(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Create new theme
        /// </summary>
        /// <param name="model">Theme Model</param>
        /// <returns>Theme Model</returns>
        ThemeModel CreateTheme(ThemeModel model);

        /// <summary>
        /// Get theme by themeId
        /// </summary>
        /// <param name="themeId">themeId to get theme</param>
        /// <returns>Theme Model</returns>
        ThemeModel GetTheme(int themeId);

        /// <summary>
        /// Update the theme
        /// </summary>
        /// <param name="themeId">themeId to get theme</param>
        /// <param name="model">Theme Model</param>
        /// <returns>Theme Model</returns>
        ThemeModel UpdateTheme(int themeId, ThemeModel model);

        /// <summary>
        /// Delete Theme
        /// </summary>
        /// <param name="themeId">Theme ID to delete Theme</param>
        /// <returns>boolean value true/false</returns>
        bool DeleteTheme(int themeId);
    }
}
