﻿using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IPortalCatalogsClient : IBaseClient
	{
		PortalCatalogModel GetPortalCatalog(int portalCatalogId);
		PortalCatalogModel GetPortalCatalog(int portalCatalogId, ExpandCollection expands);
		PortalCatalogListModel GetPortalCatalogs(ExpandCollection expands, FilterCollection filters, SortCollection sorts);
		PortalCatalogListModel GetPortalCatalogs(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
		PortalCatalogModel CreatePortalCatalog(PortalCatalogModel model);
		PortalCatalogModel UpdatePortalCatalog(int portalCatalogId, PortalCatalogModel model);
		bool DeletePortalCatalog(int portalCatalogId);
        /// <summary>
        /// Znode Version 8.0
        /// Get portal catalogs according to portal catalog id.
        /// </summary>
        /// <param name="portalId">Portal id of the portal according to which portal catalog list is to be returned.</param>
        /// <param name="expands">Expands</param>
        /// <returns></returns>
        PortalCatalogListModel GetPortalCatalogsByPortalId(int portalId);
	}
}
