﻿using System.Collections.ObjectModel;
using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    /// <summary>
    /// This is the client which has all methods regarding clear cache functionality
    /// </summary>
    public class ClearCacheClient : BaseClient, IClearCacheClient
    {
        public bool ClearAPICache()
        {
            var endpoint = ClearCacheEndpoint.ClearCache();

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<TrueFalseResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return response.booleanModel.disabled;
        }
    }
}
