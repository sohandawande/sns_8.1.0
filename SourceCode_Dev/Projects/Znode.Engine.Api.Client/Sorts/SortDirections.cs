﻿namespace Znode.Engine.Api.Client.Sorts
{
	public static class SortDirections
	{
		public static string Ascending = "asc";
		public static string Descending = "desc";
	}
}
