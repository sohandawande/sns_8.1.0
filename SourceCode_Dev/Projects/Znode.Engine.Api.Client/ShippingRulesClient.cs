﻿using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    /// <summary>
    /// Client For Shipping Rules
    /// </summary>
    public class ShippingRulesClient : BaseClient, IShippingRulesClient
    {
        #region Public Methods

        public ShippingRuleModel GetShippingRule(int shippingRuleId)
        {
            return GetShippingRule(shippingRuleId, null);
        }

        public ShippingRuleModel GetShippingRule(int shippingRuleId, ExpandCollection expands)
        {
            var endpoint = ShippingRulesEndpoint.Get(shippingRuleId);
            endpoint += BuildEndpointQueryString(expands);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ShippingRuleResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (Equals(response, null)) ? null : response.ShippingRule;
        }

        public Collection<ShippingRuleModel> GetShippingRules(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = ShippingRulesEndpoint.List();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ShippingRuleListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (Equals(response, null)) ? null : response.ShippingRules;
        }

        public ShippingRuleModel CreateShippingRule(ShippingRuleModel model)
        {
            var endpoint = ShippingRulesEndpoint.Create();

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<ShippingRuleResponse>(endpoint, JsonConvert.SerializeObject(model), status);
            if (!Equals(status.ErrorMessage, null))
            {
                model.errorMessage = status.ErrorMessage;
            }

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);
            return (Equals(response, null)) ? null : response.ShippingRule;
        }

        public ShippingRuleModel UpdateShippingRule(int shippingRuleId, ShippingRuleModel model)
        {
            var endpoint = ShippingRulesEndpoint.Update(shippingRuleId);

            var status = new ApiStatus();
            var response = PutResourceToEndpoint<ShippingRuleResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return (Equals(response, null)) ? null : response.ShippingRule;
        }

        public bool DeleteShippingRule(int shippingRuleId)
        {
            var endpoint = ShippingRulesEndpoint.Delete(shippingRuleId);

            var status = new ApiStatus();
            var deleted = DeleteResourceFromEndpoint<ShippingRuleResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

            return deleted;
        }
 
        #endregion
    }
}
