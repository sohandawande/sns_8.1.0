﻿using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IPortalCountriesClient : IBaseClient
	{

		PortalCountryModel GetPortalCountry(int portalCountryId);
		PortalCountryModel GetPortalCountry(int portalCountryId, ExpandCollection expands);

        /// <summary>
        /// Gets Portal Countries.
        /// </summary>
        /// <param name="expands">Expands</param>
        /// <param name="filters">Filters</param>
        /// <param name="sorts">Sorts.</param>
        /// <returns>PortalCountryListModel</returns>
		PortalCountryListModel GetPortalCountries(ExpandCollection expands, FilterCollection filters, SortCollection sorts);

        /// <summary>
        /// Gets Portal Countries.
        /// </summary>
        /// <param name="expands">Expands</param>
        /// <param name="filters">Filters</param>
        /// <param name="sorts">Sorts.</param>
        /// <param name="pageIndex">Page index value.</param>
        /// <param name="pageSize">Page size value.</param>
        /// <returns>Portal Country List Model</returns>
		PortalCountryListModel GetPortalCountries(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Creates a portal Country.
        /// </summary>
        /// <param name="model">Portal country model.</param>
        /// <returns>Portal country model.</returns>
		PortalCountryModel CreatePortalCountry(PortalCountryModel model);

          /// <summary>
        /// Updates aportal country.
        /// </summary>
        /// <param name="portalCountryId">Portal country id</param>
        /// <param name="model">Portal country model.</param>
        /// <returns>Portal country model.</returns>
		PortalCountryModel UpdatePortalCountry(int portalCountryId, PortalCountryModel model);

        /// <summary>
        /// Deletes a Portal Country.
        /// </summary>
        /// <param name="portalCountryId">Portal Country Id.</param>
        /// <returns>Returns ture or false.</returns>
		bool DeletePortalCountry(int portalCountryId);

        /// <summary>
        /// ZNode Version 8.0
        /// Get all Portal Countries 
        /// </summary>
        /// <param name="expands">ExpandCollection expands</param>
        /// <param name="filters">FilterCollection filters</param>
        /// <param name="sorts">SortCollection sorts</param>
        /// <param name="pageIndex">Nullable int pageIndex</param>
        /// <param name="pageSize">Nullable int pageSize</param>
        /// <returns>List of Portal Country</returns>
        PortalCountryListModel GetAllPortalCountries(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Znode ver sion 8.0
        /// Deletes a portal country if not associated.
        /// </summary>
        /// <param name="portalCountryId">Portal Country id.</param>
        /// <returns>Returns ture or false.</returns>
        bool DeletePortalCountryIfNotAssociated(int portalCountryId);

        /// <summary>
        /// Znode Version 8.0
        /// Create a portal country if not exists.
        /// </summary>
        /// <param name="portalId">Portal country model</param>
        /// <param name="billableCountryCode">Billable Country codes.</param>
        /// <param name="shippableCountryCode">Shipable country codes.</param>
        /// <returns>Returns ture or false.</returns>
        bool CreatePortalCountries(int portalId, string billableCountryCode, string shippableCountryCode);
	}
}
