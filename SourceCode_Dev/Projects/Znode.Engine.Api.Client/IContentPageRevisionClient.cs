﻿using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IContentPageRevisionClient : IBaseClient
    {
        /// <summary>
        /// List of Content Page revisions.
        /// </summary>
        /// <param name="expands">Expand values for content page revisions.</param>
        /// <param name="filters">Filter values for content page revisions.</param>
        /// <param name="sorts">Sort parameters for content page revisions.</param>
        /// <returns>List of Content page models.</returns>
        ContentPageRevisionListModel GetContentPageRevisions(ExpandCollection expands, FilterCollection filters, SortCollection sorts);

        /// <summary>
        /// List of content page revisions.
        /// </summary>
        /// <param name="expands">Expand values for content page revisions.</param>
        /// <param name="filters">Filter values for content page revisions.</param>
        /// <param name="sorts">Sort parameters for content page revisions.</param>
        /// <param name="pageIndex">Start page index of content page revisions.</param>
        /// <param name="pageSize">Page size of content page revisions list.</param>
        /// <returns></returns>
        ContentPageRevisionListModel GetContentPageRevisions(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Get the content page revisions by content page Id.
        /// </summary>
        /// <param name="contentPageId">Id of the content page.</param>
        /// <returns>List of Content page models.</returns>
        ContentPageRevisionListModel GetContentPageRevisionsById(int contentPageId);

        /// <summary>
        /// Revert the content page revision.
        /// </summary>
        /// <param name="revisionId">Id of the revision.</param>
        /// <param name="model">ContentPageRevisionModel model</param>
        /// <returns>Returns True or False.</returns>
        bool RevertRevision(int revisionId, ContentPageRevisionModel model);

        /// <summary>
        /// Get content page revision by id.
        /// </summary>
        /// <param name="contentPageId">Id of the content page.</param>
        /// <param name="filters">Filter values for content page revisions</param>
        /// <param name="sorts">Sort parameters for content page revisions.</param>
        /// <param name="pageIndex">Start page index of content page revisions.</param>
        /// <param name="pageSize">Page size of content page revisions list.</param>
        /// <returns>ContentPageRevisionListModel</returns>
        ContentPageRevisionListModel GetContentPageRevisionOnId(int contentPageId, FilterCollection filters = null, SortCollection sorts = null, int? pageIndex = 1, int? pageSize = 10);
    }
}
