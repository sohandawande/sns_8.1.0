﻿using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public partial interface ICustomerClient : IBaseClient
    {
        /// <summary>
        /// Get Customer List
        /// </summary>
        /// <param name="expands">Exapnd collection.</param>
        /// <param name="filters">Filter collection.</param>
        /// <param name="sorts">Sort collection.</param>
        /// <param name="pageIndex">page index</param>
        /// <param name="pageSize">page size</param>
        /// <returns>CustomerListModel</returns>
        CustomerListModel GetCustomerList(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Get details about Customer Affiliate
        /// </summary>
        /// <param name="accountId">Account Id by which get Affiliate details</param>
        /// <param name="expands">Expand Collection</param>
        /// <returns>CustomerAffiliateModel</returns>
        CustomerAffiliateModel GetCustomerAffiliate(int accountId, ExpandCollection expands);

        /// <summary>
        /// Get ReferralCommissionType List
        /// </summary>
        /// <param name="expands">Exapnd collection.</param>
        /// <param name="filters">Filter collection.</param>
        /// <param name="sorts">Sort collection.</param>
        /// <param name="pageIndex">page index</param>
        /// <param name="pageSize">page size</param>
        /// <returns>ReferralCommissionTypeListModel</returns>
        ReferralCommissionTypeListModel GetReferralCommissionTypeList(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Get ReferralCommission List
        /// </summary>
        /// <param name="expands">Exapnd collection.</param>
        /// <param name="filters">Filter collection.</param>
        /// <param name="sorts">Sort collection.</param>
        /// <param name="pageIndex">page index</param>
        /// <param name="pageSize">page size</param>
        /// <returns>ReferralCommissionListModel</returns>
        ReferralCommissionListModel GetReferralCommissionList(int accountId,ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        CustomerBasedPricingListModel GetCustomerBasedPricing(int accountId, ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
    }
}
