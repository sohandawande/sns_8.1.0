﻿using System.Collections.ObjectModel;
using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    public class ReportsClient : BaseClient, IReportsClient
    {
        public ReportsDataModel GetReportData(string reportName, ExpandCollection expands, FilterCollection filters, SortCollection sorts)
        {
            return GetReportData(reportName, expands, filters, sorts, null, null);
        }

        public ReportsDataModel GetReportData(string reportName, ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = ReportsEndpoint.Reports(reportName);
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ReportsResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new ReportsDataModel { RecordSet = (Equals(response, null)) ? null : response.ReportData.RecordSet };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public ReportsDataModel GetOrderDetails(int orderId, string reportName)
        {
            var endpoint = ReportsEndpoint.GetOrderDetails(orderId,reportName);
            
            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ReportsResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new ReportsDataModel { RecordSet = (Equals(response, null)) ? null : response.ReportData.RecordSet };
            list.MapPagingDataFromResponse(response);

            return list;
        }
    }
}
