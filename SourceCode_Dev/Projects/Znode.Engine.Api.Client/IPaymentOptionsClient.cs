﻿using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IPaymentOptionsClient : IBaseClient
	{
		PaymentOptionModel GetPaymentOption(int paymentOptionId);
		PaymentOptionModel GetPaymentOption(int paymentOptionId, ExpandCollection expands);
		PaymentOptionListModel GetPaymentOptions(ExpandCollection expands, FilterCollection filters, SortCollection sorts);
		PaymentOptionListModel GetPaymentOptions(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
		PaymentOptionModel CreatePaymentOption(PaymentOptionModel model);
		PaymentOptionModel UpdatePaymentOption(int paymentOptionId, PaymentOptionModel model);
		bool DeletePaymentOption(int paymentOptionId);
	}
}
