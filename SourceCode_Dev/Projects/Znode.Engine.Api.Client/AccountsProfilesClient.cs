﻿using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    public class AccountsProfilesClient : BaseClient, IAccountsProfilesClient
    {

        public bool AccountAssociatedProfiles(int accountId, string profileIds)
        {
            var endpoint = AccountProfileEndpoint.AccountAssociatedProfiles(accountId, profileIds);
            var status = new ApiStatus();
            bool isExist = GetBooleanResourceFromEndpoint<AccountProfileResponse>(endpoint, status);
            return isExist;
        }

        public bool DeleteAccountProfile(int accountProfileId)
        {
            var endpoint = AccountProfileEndpoint.Delete(accountProfileId);

            var status = new ApiStatus();
            var deleted = DeleteResourceFromEndpoint<AccountProfileResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

            return deleted;
        }
    }
}
