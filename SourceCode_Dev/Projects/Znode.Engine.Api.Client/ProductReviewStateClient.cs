﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;
using Znode.Engine.Api.Models.Extensions;

namespace Znode.Engine.Api.Client
{
    public class ProductReviewStateClient : BaseClient, IProductReviewStateClient
    {
        #region Public Methods
        public ProductReviewStateListModel GetProductReviewStates(ExpandCollection expands, FilterCollection filters, SortCollection sorts)
        {
            return GetProductReviewStates(expands, filters, sorts, null, null);
        }
        public ProductReviewStateListModel GetProductReviewStates(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = ProductReviewStateEndpoint.List();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ProductReviewStateListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new ProductReviewStateListModel { ProductReviewStates = (Equals(response, null)) ? null : response.ProductReviewStates };
            list.MapPagingDataFromResponse(response);

            return list;
        }
        #endregion
    }
}
