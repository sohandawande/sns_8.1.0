﻿using System;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IProfileCommonClient : IBaseClient
    {
        /// <summary>
        /// To Get Store Access list based on user name.
        /// </summary>
        /// <param name="userName">User name of the user.</param>
        /// <returns>Return Stored Access List.</returns>
        ProfileCommonListModel GetProfileStoreAccess(string userName);
    }
}
