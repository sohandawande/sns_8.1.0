﻿using System.Collections.ObjectModel;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IShippingRulesClient : IBaseClient
    {
        /// <summary>
        /// Get Shipping Rule by shippingRuleId
        /// </summary>
        /// <param name="shippingRuleId">shippingRuleId</param>
        /// <returns>Returns ShippingRuleModel</returns>
        ShippingRuleModel GetShippingRule(int shippingRuleId);

        /// <summary>
        /// Get Shipping Rule by shippingRuleId
        /// </summary>
        /// <param name="shippingRuleId">shippingRuleId</param>
        /// <param name="expands">expands</param>
        /// <returns>Returns ShippingRuleModel</returns>
        ShippingRuleModel GetShippingRule(int shippingRuleId, ExpandCollection expands);

        /// <summary>
        /// Get Shipping Rules
        /// </summary>
        /// <param name="expands">Expand Collection</param>
        /// <param name="filters">Filter Collection</param>
        /// <param name="sorts">Sort Collection</param>
        /// <param name="pageIndex">Index of Page</param>
        /// <param name="pageSize">Size of Page</param>
        /// <returns>Returns collection of Shipping Rules</returns>
        Collection<ShippingRuleModel> GetShippingRules(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Create Shipping Rule
        /// </summary>
        /// <param name="model">ShippingRuleModel</param>
        /// <returns>Returns ShippingRuleModel</returns>
        ShippingRuleModel CreateShippingRule(ShippingRuleModel model);

        /// <summary>
        /// Update shipping Rule
        /// </summary>
        /// <param name="shippingRuleId">shippingRuleId</param>
        /// <param name="model">ShippingRuleModel</param>
        /// <returns>Returns ShippingRuleModel</returns>
        ShippingRuleModel UpdateShippingRule(int shippingRuleId, ShippingRuleModel model);       

        /// <summary>
        /// Delete Shipping Rule
        /// </summary>
        /// <param name="shippingRuleId">shippingRuleId</param>
        /// <returns>Returns true/false</returns>
        bool DeleteShippingRule(int shippingRuleId);
    }
}
