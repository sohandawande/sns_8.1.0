﻿using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IPersonalizationClient : IBaseClient
    {
        /// <summary>
        /// Gets Product list with Frequently bought products.
        /// </summary>
        /// <param name="expands">Expands for product list along with frequently bought products.</param>
        /// <param name="filters">Filters for product list along with frequently bought products.</param>
        /// <param name="sorts">Sorting of product list along with frequently bought products.</param>
        /// <returns>List of products with Frequently bought products.</returns>
        CrossSellListModel GetFrequentlyBoughtProducts(ExpandCollection expands, FilterCollection filters, SortCollection sorts);


        /// <summary>
        /// <summary>
        /// Gets Product list with Frequently bought products.
        /// </summary>
        /// <param name="expands">Expands for product list along with frequently bought products.</param>
        /// <param name="filters">Filters for product list along with frequently bought products.</param>
        /// <param name="sorts">Sorting of product list along with frequently bought products.</param>
        /// <param name="pageIndex">Page index of frequently bought product list.</param>
        /// <param name="pageSize">Page size of frequently bought product list.</param>
        /// <returns>Frequently bought product list.</returns>
        CrossSellListModel GetFrequentlyBoughtProducts(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
        
        /// <summary>
        /// Gets Product cross sells by product id.
        /// </summary>
        /// <param name="filters">Filters</param>
        /// <param name="sorts"> Sorting</param>
        /// <returns>CrossSellListModel</returns>
        CrossSellListModel GetProductCrossSellByProductId(FilterCollection filters, SortCollection sorts);

        /// <summary>
        /// Create new cross sell products.
        /// </summary>
        /// <param name="model">List of Cross sell model</param>
        /// <returns>Boolean value if the cross sells are created or not.</returns>
        bool CreateCrossSellProducts(CrossSellListModel model);

        /// <summary>
        /// Delete Cross Sell Products
        /// </summary>
        /// <param name="listModel">Cross Sell List Model</param>
        /// <returns>boolean value true/false</returns>
        bool DeleteCrossSellProduct(int productId, int relationTypeId);
    }
}
