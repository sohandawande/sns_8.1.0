﻿using System.Collections.ObjectModel;
using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    /// <summary>
    /// Client for Dashboard.
    /// </summary>
    public class DashboardClient : BaseClient, IDashboardClient
    {
        public DashboardModel GetDashboardItemsByPortalId(int portalId)
        {
            return GetDashboardItemsByPortalId(portalId, null);
        }

        public DashboardModel GetDashboardItemsByPortalId(int portalId, ExpandCollection expands)
        {
            var endpoint = DashboardEndpoint.GetDashboardItems(portalId);
            endpoint += BuildEndpointQueryString(expands);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<DashboardResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return Equals(response, null) ? null : response.Dashboard;
        }
    }
}
