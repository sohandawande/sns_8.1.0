﻿using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface ISupplierTypesClient : IBaseClient
	{
		SupplierTypeModel GetSupplierType(int supplierTypeId);
		SupplierTypeListModel GetSupplierTypes(FilterCollection filters, SortCollection sorts);
		SupplierTypeListModel GetSupplierTypes(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
		SupplierTypeModel CreateSupplierType(SupplierTypeModel model);
		SupplierTypeModel UpdateSupplierType(int supplierTypeId, SupplierTypeModel model);
		bool DeleteSupplierType(int supplierTypeId);

        /// <summary>
        /// ZNode Version 8.0
        /// Get all Supplier Types not present in database.
        /// </summary>
        /// <param name="filters">FilterCollection filters</param>
        /// <param name="sorts">SortCollection sorts</param>
        /// <param name="pageIndex">Nullable int pageIndex</param>
        /// <param name="pageSize">Nullable int pageSize</param>
        /// <returns>Returns supplier types list</returns>
        SupplierTypeListModel GetAllSupplierTypesNotInDatabase(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
	}
}
