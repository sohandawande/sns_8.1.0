﻿using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IAttributeTypesClient : IBaseClient
	{
        /// <summary>
        /// To Get Attribute Type details based on Attribute Type Id.
        /// </summary>
        /// <param name="attributeTypeId">Id of the Attribute Type</param>
        /// <returns>Return Attribute Type Details in AttributeTypeModel format.</returns>
		AttributeTypeModel GetAttributeType(int attributeTypeId);

        /// <summary>
        /// To Get Attribute Types detail.
        /// </summary>
        /// <param name="filters">FilterCollection filters</param>
        /// <param name="sorts">SortCollection sorts</param>
        /// <param name="pageIndex">Nullable int pageIndex</param>
        /// <param name="pageSize">Nullable int pageSize</param>
        /// <returns>Return Attribute Type Details in AttributeTypeListModel format.</returns>
        AttributeTypeListModel GetAttributeTypes(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// To Create Attribute Type
        /// </summary>
        /// <param name="model">Model of the AttributeTypeModel</param>
        /// <returns>Return Attribute Type Details in AttributeTypeModel format</returns>
		AttributeTypeModel CreateAttributeType(AttributeTypeModel model);

        /// <summary>
        /// To Update Attribute Type
        /// </summary>
        /// <param name="attributeTypeId">Id of the Attribute Type</param>
        /// <param name="model"></param>
        /// <returns>Return Attribute Type Details in AttributeTypeModel format</returns>
		AttributeTypeModel UpdateAttributeType(int attributeTypeId, AttributeTypeModel model);

        /// <summary>
        /// To Delete the Attribute Type
        /// </summary>
        /// <param name="attributeTypeId"></param>
        /// <returns>Return True or false</returns>
		bool DeleteAttributeType(int attributeTypeId);

        /// <summary>
        /// To Get Attribute Types detail based on catalog Id.
        /// </summary>
        /// <param name="catalogId">Id of the Catalog</param>
        /// <returns>Return Attribute Type Details in AttributeTypeListModel format</returns>
        AttributeTypeListModel GetAttributesByCatalogID(int catalogId);
	}
}
