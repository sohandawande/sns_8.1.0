﻿using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    /// <summary>
    /// This is the interface for the Permission client
    /// </summary>
    public interface IPermissionsClient : IBaseClient
    {
        /// <summary>
        /// This method will fetch the roles and permissions based on Account Id
        /// </summary>
        /// <param name="accountId">int Account Id</param>
        /// <returns>All the data of Roles and Permission for the account Id</returns>
        PermissionsModel GetAllRolesAndPermissionsByAccountId(int accountId);

        /// <summary>
        /// This method will update role and permissions for Account Id
        /// </summary>
        /// <param name="model">PermissionsModel  model</param>
        /// <returns>True if changes updated in DB</returns>
        bool UpdateRolesAndPermissions(PermissionsModel model);
    }
}
