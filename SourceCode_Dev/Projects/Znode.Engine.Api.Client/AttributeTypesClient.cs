﻿using System.Collections.ObjectModel;
using System.Net;
using Newtonsoft.Json;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
	public class AttributeTypesClient : BaseClient, IAttributeTypesClient
	{
		public AttributeTypeModel GetAttributeType(int attributeTypeId)
		{
			var endpoint = AttributeTypesEndpoint.Get(attributeTypeId);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<AttributeTypeResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

			return (response == null) ? null : response.AttributeType;
		}

        public AttributeTypeListModel GetAttributeTypes(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
		{
			var endpoint = AttributeTypesEndpoint.List();
            endpoint += BuildEndpointQueryString(null, filters, sorts, pageIndex, pageSize);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<AttributeTypeListResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new AttributeTypeListModel { AttributeTypes = (response == null) ? null : response.AttributeTypes };
			list.MapPagingDataFromResponse(response);

			return list;
		}
        
		public AttributeTypeModel CreateAttributeType(AttributeTypeModel model)
		{
			var endpoint = AttributeTypesEndpoint.Create();

			var status = new ApiStatus();
			var response = PostResourceToEndpoint<AttributeTypeResponse>(endpoint, JsonConvert.SerializeObject(model), status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

			return (response == null) ? null : response.AttributeType;
		}

		public AttributeTypeModel UpdateAttributeType(int attributeTypeId, AttributeTypeModel model)
		{
			var endpoint = AttributeTypesEndpoint.Update(attributeTypeId);

			var status = new ApiStatus();
			var response = PutResourceToEndpoint<AttributeTypeResponse>(endpoint, JsonConvert.SerializeObject(model), status);
			
			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

			return (response == null) ? null : response.AttributeType;
		}

		public bool DeleteAttributeType(int attributeTypeId)
		{
			var endpoint = AttributeTypesEndpoint.Delete(attributeTypeId);

			var status = new ApiStatus();
			var deleted = DeleteResourceFromEndpoint<AttributeTypeResponse>(endpoint, status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

			return deleted;
        }

        #region Znode Version 8.0
        public AttributeTypeListModel GetAttributesByCatalogID(int catalogId)
        {
            var endpoint = AttributeTypesEndpoint.GetAttributesByCatalogID(catalogId);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<AttributeTypeListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new AttributeTypeListModel { AttributeTypes = (Equals(response, null)) ? null : response.AttributeTypes };
            list.MapPagingDataFromResponse(response);

            return list;
        }
        #endregion
    }
}
