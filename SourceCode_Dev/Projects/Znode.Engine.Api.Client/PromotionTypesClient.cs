﻿using System.Collections.ObjectModel;
using System.Net;
using Newtonsoft.Json;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    public class PromotionTypesClient : BaseClient, IPromotionTypesClient
    {
        public PromotionTypeModel GetPromotionType(int promotionTypeId)
        {
            var endpoint = PromotionTypesEndpoint.Get(promotionTypeId);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<PromotionTypeResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return Equals(response, null) ? null : response.PromotionType;
        }

        public PromotionTypeListModel GetPromotionTypes(FilterCollection filters, SortCollection sorts)
        {
            return GetPromotionTypes(filters, sorts, null, null);
        }

        public PromotionTypeListModel GetPromotionTypes(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = PromotionTypesEndpoint.List();
            endpoint += BuildEndpointQueryString(null, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<PromotionTypeListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new PromotionTypeListModel { PromotionTypes = Equals(response, null) ? null : response.PromotionTypes };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public PromotionTypeModel CreatePromotionType(PromotionTypeModel model)
        {
            var endpoint = PromotionTypesEndpoint.Create();

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<PromotionTypeResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return Equals(response, null) ? null : response.PromotionType;
        }

        public PromotionTypeModel UpdatePromotionType(int promotionTypeId, PromotionTypeModel model)
        {
            var endpoint = PromotionTypesEndpoint.Update(promotionTypeId);

            var status = new ApiStatus();
            var response = PutResourceToEndpoint<PromotionTypeResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return Equals(response, null) ? null : response.PromotionType;
        }

        public bool DeletePromotionType(int promotionTypeId)
        {
            var endpoint = PromotionTypesEndpoint.Delete(promotionTypeId);

            var status = new ApiStatus();
            var deleted = DeleteResourceFromEndpoint<PromotionTypeResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

            return deleted;
        }

        #region Znode Version 8.0
        public PromotionTypeListModel GetAllPromotionTypesNotInDatabase(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = PromotionTypesEndpoint.GetAllPromotionTypesNotInDatabase();
            endpoint += BuildEndpointQueryString(null, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<PromotionTypeListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new PromotionTypeListModel { PromotionTypes = Equals(response, null) ? null : response.PromotionTypes };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public PromotionTypeListModel GetFranchisePromotionTypes(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = PromotionTypesEndpoint.GetFranchisePromotionTypes();
            endpoint += BuildEndpointQueryString(null, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<PromotionTypeListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new PromotionTypeListModel { PromotionTypes = Equals(response, null) ? null : response.PromotionTypes };
            list.MapPagingDataFromResponse(response);

            return list;
        }
        #endregion
    }
}
