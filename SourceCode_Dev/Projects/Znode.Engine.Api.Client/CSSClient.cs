﻿using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    public class CSSClient : BaseClient, ICSSClient
    {
        public CSSListModel GetCSSs(ExpandCollection expands, FilterCollection filters, SortCollection sorts)
        {
            return GetCSSs(expands, filters, sorts, null, null);
        }
        public CSSListModel GetCSSs(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = CSSEndpoint.List();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<CSSListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new CSSListModel { CSSs = (response == null) ? null : response.CSSs };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public CSSModel CreateCSS(CSSModel model)
        {
            string endpoint = CSSEndpoint.Create();

            ApiStatus status = new ApiStatus();
            var response = PostResourceToEndpoint<CSSResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return (Equals(response, null)) ? null : response.CSS;
        }

        public CSSModel GetCSS(int cssId)
        {
            var endpoint = CSSEndpoint.Get(cssId);

            ApiStatus status = new ApiStatus();
            var response = GetResourceFromEndpoint<CSSResponse>(endpoint, status);

            Collection<HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (Equals(response, null)) ? null : response.CSS;
        }

        public CSSModel UpdateCSS(int cssId, CSSModel model)
        {
            var endpoint = CSSEndpoint.Update(cssId);

            ApiStatus status = new ApiStatus();
            var response = PutResourceToEndpoint<CSSResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return (Equals(response, null)) ? null : response.CSS;
        }

        public bool DeleteCSS(int cssId)
        {
            var endpoint = CSSEndpoint.Delete(cssId);

            ApiStatus status = new ApiStatus();
            var deleted = DeleteResourceFromEndpoint<CSSResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

            return deleted;
        }
    }
}
