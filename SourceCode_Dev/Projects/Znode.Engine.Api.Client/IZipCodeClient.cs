﻿using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    /// <summary>
    /// Interface for ZipCodeClient
    /// </summary>
    public interface IZipCodeClient : IBaseClient
    {
        /// <summary>
        /// Get Zip Codes
        /// </summary>
        /// <param name="filters">Filter collection</param>
        /// <param name="sorts">Sort collection</param>
        /// <param name="pageIndex">Index of page</param>
        /// <param name="pageSize">Size of page</param>
        /// <returns>Returns ZipCodeListModel</returns>
        ZipCodeListModel GetZipCodes(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
    }
}
