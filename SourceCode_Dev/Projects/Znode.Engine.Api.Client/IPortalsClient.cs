﻿using System.Data;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IPortalsClient : IBaseClient
	{
        /// <summary>
        /// Get Portals
        /// </summary>
        /// <param name="portalId">portalId</param>
        /// <returns>Returns PortalModel</returns>
		PortalModel GetPortal(int portalId);

        /// <summary>
        /// Get Portals
        /// </summary>
        /// <param name="portalId">portalId</param>
        /// <param name="expands">expands</param>
        /// <returns>Returns PortalModel</returns>
		PortalModel GetPortal(int portalId, ExpandCollection expands);

        /// <summary>
        /// Get Portals
        /// </summary>
        /// <param name="expands">expands</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <returns>Returns PortalListModel</returns>
		PortalListModel GetPortals(ExpandCollection expands, FilterCollection filters, SortCollection sorts);

        /// <summary>
        /// Get Portals
        /// </summary>
        /// <param name="expands">expands</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <param name="pageIndex">pageIndex</param>
        /// <param name="pageSize">pageSize</param>
        /// <returns>Returns PortalListModel</returns>
		PortalListModel GetPortals(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Get Portals By Portal Ids
        /// </summary>
        /// <param name="portalIds">portalIds</param>
        /// <param name="expands">expands</param>
        /// <returns>Returns PortalListModel</returns>
        PortalListModel GetPortalsByPortalIds(string portalIds, ExpandCollection expands);

        /// <summary>
        /// Create Portal
        /// </summary>
        /// <param name="model">PortalModel</param>
        /// <returns>Returns PortalModel</returns>
		PortalModel CreatePortal(PortalModel model);

        /// <summary>
        /// Update Portal
        /// </summary>
        /// <param name="portalId">portalId</param>
        /// <param name="model">PortalModel</param>
        /// <returns>Returns PortalModel</returns>
		PortalModel UpdatePortal(int portalId, PortalModel model);

        /// <summary>
        /// Delete Portal
        /// </summary>
        /// <param name="portalId">portalId</param>
        /// <returns>Returns True/False</returns>
		bool DeletePortal(int portalId);

        /// <summary>
        /// Gets default fedex keys.
        /// </summary>
        /// <returns>Dataset of fedex keys.</returns>
        DataSet GetFedexKeys();

        /// <summary>
        /// Creates default messages for new portal.
        /// </summary>
        /// <param name="portalId">Portal id of the portal for which messages are to be created.</param>
        /// <param name="localeId">Locale id of the portal.</param>
        void CreateMessage(int portalId, int localeId);

        /// <summary>
        /// Copies a store.
        /// </summary>
        /// <param name="portalId">Portal ID of the portal which is to be copied.</param>
        /// <returns>Bool value if the store is copied or not.</returns>
        bool CopyStore(int portalId);       

        /// <summary>
        /// Deletes portal by portal id.
        /// </summary>
        /// <param name="portalId">Portal id of the portal to be deleted.</param>
        /// <returns>Bool value if the Portal is deleted or not.</returns>
        bool DeletePortalByPortalId(int portalId);

        /// <summary>
        /// Gets the portal information according to portal id.
        /// </summary>
        /// <param name="portalId">Portal id for which portal information is to be retrieved.</param>
        /// <returns>Portal model.</returns>
        PortalModel GetPortalInformationByPortalId(int portalId);

        /// <summary>
        /// Gets portal list according to profile access.
        /// </summary>
        /// <param name="expands">Expand collection of the portal list.</param>
        /// <param name="filters">Filter collection of the Portal list.</param>
        /// <param name="sorts">Sort collection of the Portal list.</param>
        /// <returns>Portal list according to profile access to store.</returns>
        PortalListModel GetPortalsByProfileAccess(ExpandCollection expands, FilterCollection filters, SortCollection sorts);

        /// <summary>
        /// Gets portal list according to profile access.
        /// </summary>
        /// <param name="expands">Expand collection of the portal list.</param>
        /// <param name="filters">Filter collection of the Portal list.</param>
        /// <param name="sorts">Sort collection of the Portal list.</param>
        /// <param name="pageIndex">Page index of the portal list.</param>
        /// <param name="pageSize">Page size of the portal list.</param>
        /// <returns>Portal list according to profile access to store.</returns>
        PortalListModel GetPortalsByProfileAccess(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
	}
}
