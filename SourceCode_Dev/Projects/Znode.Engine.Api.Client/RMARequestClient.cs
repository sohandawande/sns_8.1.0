﻿using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.Data;
using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;
namespace Znode.Engine.Api.Client
{
    public class RMARequestClient : BaseClient, IRMARequestClient
    {
        #region Public Region
        public RMARequestListModel GetRMARequestList(ExpandCollection expands, FilterCollection filters, SortCollection sorts)
        {
            return GetRMARequestList(expands, filters, sorts, null, null);
        }

        public RMARequestListModel GetRMARequestList(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = RMARequestEndpoint.List();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<RMARequestListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new RMARequestListModel { RMARequests = (Equals(response, null)) ? null : response.RMARequestList };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public RMARequestModel UpdateRMARequest(int rmaRequestId, RMARequestModel model)
        {
            var endpoint = RMARequestEndpoint.Update(rmaRequestId);

            var status = new ApiStatus();
            var response = PutResourceToEndpoint<RMARequestResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return (Equals(response, null)) ? null : response.RMARequest;
        }

        public RMARequestModel GetRMARequest(int rmaRequestId)
        {
            var endpoint = RMARequestEndpoint.Get(rmaRequestId);


            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<RMARequestResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);
            
            return (Equals(response, null)) ? null : response.RMARequest;
        }

        public bool GetOrderRMAFlag(int orderId)
        {
            var endpoint = RMARequestEndpoint.GetOrderRMAFlag(orderId);
            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<TrueFalseResponse>(endpoint, status);
            return response.booleanModel.disabled;
        }

        public bool DeleteRMARequest(int rmaRequestId)
        {
            var endpoint = RMARequestEndpoint.Delete(rmaRequestId);

            var status = new ApiStatus();
            var deleted = DeleteResourceFromEndpoint<RMARequestItemResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

            return deleted;
        }

        public IssuedGiftCardListModel GetRMARequestGiftCardDetails(int rmaRequestId)
        {
            string endpoint = RMARequestEndpoint.GetRMAGiftCardDetails(rmaRequestId);

            ApiStatus status = new ApiStatus();
            IssuedGiftCardListResponse response = GetResourceFromEndpoint<IssuedGiftCardListResponse>(endpoint, status);

            Collection<HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return new IssuedGiftCardListModel { IssuedGiftCardModels = (Equals(response, null)) ? null : response.IssuedGiftCards };
        }

        public RMARequestModel CreateRMARequest(RMARequestModel model)
        {
            var endpoint = RMARequestEndpoint.Create();

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<RMARequestResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return (Equals(response, null)) ? null : response.RMARequest;
        }

        public bool IsStatusEmailSent(int rmaRequestId)
        {
            var endpoint = RMARequestEndpoint.SendRMAStatusMail(rmaRequestId);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<TrueFalseResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return response.booleanModel.disabled;
        }

        public bool IsGiftCardMailSent(GiftCardModel model)
        {
            string endpoint = RMARequestEndpoint.SendGiftCardMail();

            ApiStatus status = new ApiStatus();
            TrueFalseResponse response = PostResourceToEndpoint<TrueFalseResponse>(endpoint,JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return response.booleanModel.disabled;
        }
        #endregion
    }
}
