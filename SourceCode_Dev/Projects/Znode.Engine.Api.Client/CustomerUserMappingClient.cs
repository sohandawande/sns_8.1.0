﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    public class CustomerUserMappingClient : BaseClient, ICustomerUserMappingClient
    {
        public bool DeleteUserAssociatedCustomer(int customerUserMappingId)
        {
            var endpoint = CustomerUserMappingEndPoint.Delete(customerUserMappingId);

            var status = new ApiStatus();
            var deleted = DeleteResourceFromEndpoint<CustomerUserMappingResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

            return deleted;
        }

        public bool UserAssociatedCustomers(int accountId, string customerAccountIds)
        {
            var endpoint = CustomerUserMappingEndPoint.UserAssociatedCustomers(accountId, customerAccountIds);
            //var endpoint = AccountProfileEndpoint.AccountAssociatedProfiles(accountId, customerAccountIds);
            var status = new ApiStatus();
            bool isExist = GetBooleanResourceFromEndpoint<CustomerUserMappingResponse>(endpoint, status);
            return isExist;
        }

        public bool DeleteAllUserMapping(int userAccountId)
        {
            var endpoint = CustomerUserMappingEndPoint.DeleteAllUserMapping(userAccountId);            
            var status = new ApiStatus();
            bool deleted = DeleteResourceFromEndpoint<CustomerUserMappingResponse>(endpoint, status);
            return deleted;
        }

        public AccountListModel GetSubUserList(int parentAccountId, ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = CustomerEndpoint.GetSubUser(parentAccountId);
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<AccountListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new AccountListModel { Accounts = (response == null) ? null : response.Accounts };
            list.MapPagingDataFromResponse(response);

            return list;
        }
    }
}
