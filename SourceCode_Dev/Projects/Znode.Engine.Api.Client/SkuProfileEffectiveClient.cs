﻿using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    /// <summary>
    /// Client for Sku Profile Effective.
    /// </summary>
    public class SkuProfileEffectiveClient : BaseClient, ISkuProfileEffectiveClient
    {
        #region Public Methods

        public SkuProfileEffectiveModel GetSkuProfileEffective(int skuProfileEffectiveId)
        {
            return GetSkuProfileEffective(skuProfileEffectiveId, null);
        }

        public SkuProfileEffectiveModel GetSkuProfileEffective(int skuProfileEffectiveId, ExpandCollection expands)
        {
            var endpoint = SkuProfileEffectiveEndpoint.Get(skuProfileEffectiveId);
            endpoint += BuildEndpointQueryString(expands);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<SkuProfileEffectiveResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return Equals(response, null) ? null : response.SkuProfileEffective;
        }

        public SkuProfileEffectiveListModel GetSkuProfileEffectives(ExpandCollection expands, FilterCollection filters, SortCollection sorts)
        {
            return GetSkuProfileEffectives(expands, filters, sorts, null, null);
        }

        public SkuProfileEffectiveListModel GetSkuProfileEffectiveBySkuId(int skuId, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = SkuProfileEffectiveEndpoint.GetSkuProfileEffectiveBySkuId(skuId);
            endpoint += BuildEndpointQueryString(null, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<SkuProfileEffectiveListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new SkuProfileEffectiveListModel { SkuProfileEffectives = Equals(response, null) ? null : response.SkuProfileEffectives };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public SkuProfileEffectiveListModel GetSkuProfileEffectives(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = SkuProfileEffectiveEndpoint.List();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<SkuProfileEffectiveListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new SkuProfileEffectiveListModel { SkuProfileEffectives = Equals(response, null) ? null : response.SkuProfileEffectives };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public SkuProfileEffectiveModel CreateSkuProfileEffective(SkuProfileEffectiveModel model)
        {
            var endpoint = SkuProfileEffectiveEndpoint.Create();

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<SkuProfileEffectiveResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return Equals(response, null) ? null : response.SkuProfileEffective;
        }

        public SkuProfileEffectiveModel UpdateSkuProfileEffective(int skuProfileEffectiveId, SkuProfileEffectiveModel model)
        {
            var endpoint = SkuProfileEffectiveEndpoint.Update(skuProfileEffectiveId);

            var status = new ApiStatus();
            var response = PutResourceToEndpoint<SkuProfileEffectiveResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return Equals(response, null) ? null : response.SkuProfileEffective;
        }

        public bool DeleteSkuProfileEffective(int skuProfileEffectiveId)
        {
            var endpoint = SkuProfileEffectiveEndpoint.Delete(skuProfileEffectiveId);

            var status = new ApiStatus();
            var deleted = DeleteResourceFromEndpoint<SkuProfileEffectiveResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

            return deleted;
        } 

        #endregion
    }
}
