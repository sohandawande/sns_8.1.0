﻿using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IProfilesClient : IBaseClient
    {
        /// <summary>
        /// Method gets the Profile details.
        /// </summary>
        /// <param name="profileId">The id of profile</param>
        /// <returns>Returns profile details.</returns>
        ProfileModel GetProfile(int profileId);

        /// <summary>
        /// Method gets the Profile details.
        /// </summary>
        /// <param name="profileId">The id of profile</param>
        /// <param name="expands">Expands collection</param>
        /// <returns>Returns profile details.</returns>
        ProfileModel GetProfile(int profileId, ExpandCollection expands);

        /// <summary>
        /// Method Returns the Profiles.
        /// </summary>
        /// <param name="expands">Expands collection</param>
        /// <param name="filters">Filter collection</param>
        /// <param name="sorts">Sort collection</param>
        /// <returns>Returns the profiles</returns>
        ProfileListModel GetProfiles(ExpandCollection expands, FilterCollection filters, SortCollection sorts);

        /// <summary>
        /// Method Returns the Profiles.
        /// </summary>
        /// <param name="expands">Expands collection</param>
        /// <param name="filters">Filter collection</param>
        /// <param name="sorts">Sort collection</param>
        /// <param name="pageIndex">Index of page</param>
        /// <param name="pageSize">Size of page</param>
        /// <returns>Returns the profiles</returns>
        ProfileListModel GetProfiles(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Method Creates the Profile.
        /// </summary>
        /// <param name="model">Model of type ProfileModel</param>
        /// <returns>Return the profile.</returns>
        ProfileModel CreateProfile(ProfileModel model);

        /// <summary>
        /// Method Updates the Profile.
        /// </summary>
        /// <param name="profileId">The id of profile</param>
        /// <param name="model">Model of type ProfileModel</param>
        /// <returns>Returns Updated profile.</returns>
        ProfileModel UpdateProfile(int profileId, ProfileModel model);

        /// <summary>
        /// Method Deletes the profile based on Id.
        /// </summary>
        /// <param name="profileId">The id of profile</param>
        /// <returns>Returns true or false.</returns>
        bool DeleteProfile(int profileId);

        /// <summary>
        /// Gets list of profiles.
        /// </summary>
        /// <returns>Returns model of type ProfileListModel.</returns>
        ProfileListModel GetProfiles();

        /// <summary>
        /// Gets list of profiles by portal id.
        /// </summary>
        /// <returns>Returns model of type ProfileListModel.</returns>
        ProfileListModel GetProfilesByProfileId(int portalId);

        /// <summary>
        /// Get Customer Profile Account
        /// </summary>
        /// <param name="accountId">account Id</param>
        /// <param name="expands">expand collection</param>
        /// <param name="filters">filter collection</param>
        /// <param name="sorts">sorting for Customer Profile Account</param>
        /// <param name="pageIndex">Paging index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>ProfileListModel</returns>
        ProfileListModel GetCustomerProfile(int accountId, ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Get profiles list that not associated with selected account
        /// </summary>
        /// <param name="accountId">Account id</param>
        /// <param name="expands">ExpandCollection for profile list</param>
        /// <param name="filters">Filtering for profile list </param>
        /// <param name="sorts">Sorting for profile list</param>
        /// <param name="pageIndex">Page indexing for profile list</param>
        /// <param name="pageSize">Total page size</param>
        /// <returns>ProfileListModel</returns>
        ProfileListModel GetCustomerNotAssociatedProfiles(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Gets list of profiles.
        /// </summary>
        /// <param name="portalId">Portal Id to retrieve ProfileList</param>
        /// <returns>ProfileListModel</returns>
        ProfileListModel GetProfileListByPortalId(int portalId);

        /// <summary>
        /// Gets available profiles by sku id.
        /// </summary>
        /// <param name="model">Model to get the profiles.</param>
        /// <returns>List of profiles.</returns>
        ProfileListModel GetAvailableProfilesBySkuIdCategoryId(AssociatedSkuCategoryProfileModel model);

        /// <summary>
        /// Get Znode Profile Details
        /// </summary>
        /// <returns>Return profile model</returns>
        ProfileModel GetZnodeProfile();

        /// <summary>
        /// Get Profiles List by userName
        /// </summary>
        /// <param name="userName">string userName</param>
        /// <returns>Return list of profiles</returns>
        ProfileListModel GetProfilesAssociatedWithUsers(string userName);
    }
}
