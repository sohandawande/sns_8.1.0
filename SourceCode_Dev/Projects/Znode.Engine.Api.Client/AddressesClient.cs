﻿using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
	public class AddressesClient : BaseClient, IAddressesClient
	{
		public AddressModel GetAddress(int addressId, bool refreshCache = false)
		{
			RefreshCache = refreshCache;

			var endpoint = AddressesEndpoint.Get(addressId);
			endpoint += BuildEndpointQueryString(null);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<AddressResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return Equals(response , null) ? null : response.Address;
		}

		public AddressListModel GetAddresses(FilterCollection filters, SortCollection sorts, bool refreshCache = false)
		{
			return GetAddresses(filters, sorts, null, null, refreshCache);
		}

		public AddressListModel GetAddresses(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize, bool refreshCache = false)
		{

			var endpoint = AddressesEndpoint.List();
			endpoint += BuildEndpointQueryString(null, filters, sorts, pageIndex, pageSize);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<AddressListResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

			var list = new AddressListModel { Addresses = (response == null) ? null : response.Addresses };
			list.MapPagingDataFromResponse(response);

			return list;
		}

		public AddressModel CreateAddress(AddressModel model)
		{
			var endpoint = AddressesEndpoint.Create();

			var status = new ApiStatus();
			var response = PostResourceToEndpoint<AddressResponse>(endpoint, JsonConvert.SerializeObject(model), status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return Equals(response , null) ? null : response.Address;
		}

		public AddressModel UpdateAddress(int addressId, AddressModel model)
		{
			var endpoint = AddressesEndpoint.Update(addressId);

			var status = new ApiStatus();
			var response = PutResourceToEndpoint<AddressResponse>(endpoint, JsonConvert.SerializeObject(model), status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return Equals(response , null) ? null : response.Address;
		}

        
        public AddressModel UpdateDefaultAddress(int addressId, AddressModel model)
        {
            var endpoint = AddressesEndpoint.UpdateDefaultAddress(addressId);
            var status = new ApiStatus();
            var response = PutResourceToEndpoint<AddressResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return Equals(response , null) ? null : response.Address;
        }

		public bool DeleteAddress(int addressId)
		{
			var endpoint = AddressesEndpoint.Delete(addressId);

			var status = new ApiStatus();
			var deleted = DeleteResourceFromEndpoint<AddressResponse>(endpoint, status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

			return deleted;
		}
	}
}
