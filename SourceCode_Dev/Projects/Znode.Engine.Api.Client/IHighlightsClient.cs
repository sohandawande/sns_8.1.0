﻿using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IHighlightsClient : IBaseClient
	{
        /// <summary>
        /// Get Highlight
        /// </summary>
        /// <param name="highlightId">highlightId</param>
        /// <returns>HighlightModel</returns>
		HighlightModel GetHighlight(int highlightId);

        /// <summary>
        /// GetHighlight
        /// </summary>
        /// <param name="highlightId">highlightId</param>
        /// <param name="expands">expands</param>
        /// <returns>HighlightModel</returns>
		HighlightModel GetHighlight(int highlightId, ExpandCollection expands);

        /// <summary>
        /// Get Highlights list
        /// </summary>
        /// <param name="expands">expands</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <returns>Highlight List Model</returns>
		HighlightListModel GetHighlights(ExpandCollection expands, FilterCollection filters, SortCollection sorts);

        /// <summary>
        /// Get Highlights list
        /// </summary>
        /// <param name="expands">expands</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <param name="pageIndex">page index</param>
        /// <param name="pageSize">page size</param>
        /// <returns>Highlight List Model</returns>
		HighlightListModel GetHighlights(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Create Highlight
        /// </summary>
        /// <param name="model">model</param>
        /// <returns>HighlightModel</returns>
		HighlightModel CreateHighlight(HighlightModel model);

        /// <summary>
        /// Update Highlight
        /// </summary>
        /// <param name="highlightId">highlightId</param>
        /// <param name="model">model</param>
        /// <returns>updated model</returns>
		HighlightModel UpdateHighlight(int highlightId, HighlightModel model);

        /// <summary>
        /// Delete Highlight
        /// </summary>
        /// <param name="highlightId">highlightId</param>
        /// <returns>true or false</returns>
		bool DeleteHighlight(int highlightId);

        /// <summary>
        ///  Znode Version 8.0
        ///  Check the Associated product with highlight and sets the property of it.
        /// </summary>
        /// <returns>HighlightModel</returns>
        HighlightModel CheckAssociatedProduct(int highlightId);
	}
}
