﻿using Newtonsoft.Json;
using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    /// <summary>
    /// Google Site Map Client
    /// </summary>
    public class GoogleSiteMapClient : BaseClient, IGoogleSiteMapClient
    {
        #region Public Methods

        public GoogleSiteMapModel CreateGoogleSiteMap(GoogleSiteMapModel model)
        {          
            var endpoint = GoogleSiteMapEndpoint.Create();

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<GoogleSiteMapResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return (Equals(response, null)) ? null : response.GoogleSiteMap;
        }

        #endregion
    }
}
