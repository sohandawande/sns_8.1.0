﻿using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    public class ProfilesClient : BaseClient, IProfilesClient
    {
        #region Public Methods
        public ProfileModel GetProfile(int profileId)
        {
            return GetProfile(profileId, null);
        }

        public ProfileModel GetProfile(int profileId, ExpandCollection expands)
        {
            var endpoint = ProfilesEndpoint.Get(profileId);
            endpoint += BuildEndpointQueryString(expands);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ProfileResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return Equals(response, null) ? null : response.Profile;
        }

        public Models.ProfileListModel GetProfiles(Expands.ExpandCollection expands, Filters.FilterCollection filters, Sorts.SortCollection sorts)
        {
            return GetProfiles(expands, filters, sorts, null, null);
        }

        public ProfileListModel GetProfiles()
        {
            var endpoint = ProfilesEndpoint.Get();
            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ProfileListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new ProfileListModel { Profiles = (Equals(response, null)) ? null : response.Profiles };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public ProfileListModel GetProfilesByProfileId(int portalId)
        {
            var endpoint = ProfilesEndpoint.ProfileListByPortalId(portalId);
            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ProfileListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new ProfileListModel { Profiles = (Equals(response, null)) ? null : response.Profiles };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public ProfileListModel GetProfiles(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = ProfilesEndpoint.List();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ProfileListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new ProfileListModel { Profiles = (Equals(response, null)) ? null : response.Profiles };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public ProfileModel CreateProfile(ProfileModel model)
        {
            var endpoint = ProfilesEndpoint.Create();

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<ProfileResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return (Equals(response, null)) ? null : response.Profile;
        }

        public ProfileModel UpdateProfile(int profileId, ProfileModel model)
        {
            var endpoint = ProfilesEndpoint.Update(profileId);

            var status = new ApiStatus();
            var response = PutResourceToEndpoint<ProfileResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return (Equals(response, null)) ? null : response.Profile;
        }

        public bool DeleteProfile(int profileId)
        {
            var endpoint = ProfilesEndpoint.Delete(profileId);

            var status = new ApiStatus();
            var deleted = DeleteResourceFromEndpoint<ProfileResponse>(endpoint, status);

            return deleted;
        }

        public ProfileListModel GetCustomerProfile(int accountId, ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = ProfilesEndpoint.AccountProfileList(accountId);
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ProfileListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new ProfileListModel { Profiles = (Equals(response, null)) ? null : response.Profiles };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public ProfileListModel GetCustomerNotAssociatedProfiles( ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = ProfilesEndpoint.CustomerNotAssociatedProfileList();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ProfileListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new ProfileListModel { Profiles = (Equals(response, null)) ? null : response.Profiles };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public ProfileListModel GetProfileListByPortalId(int portalId)
        {
            var endpoint = ProfilesEndpoint.GetProfileListByPortalId(portalId);
            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ProfileListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new ProfileListModel { Profiles = (Equals(response, null)) ? null : response.Profiles };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public ProfileListModel GetAvailableProfilesBySkuIdCategoryId(AssociatedSkuCategoryProfileModel model)
        {
            var endpoint = ProfilesEndpoint.GetAvailableProfilesBySkuId();
            var status = new ApiStatus();
            var response = PostResourceToEndpoint<ProfileListResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new ProfileListModel { Profiles = (Equals(response, null)) ? null : response.Profiles };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public ProfileModel GetZnodeProfile()
        {
            var endpoint = ProfilesEndpoint.GetZnodeProfile();

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ProfileResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return Equals(response, null) ? null : response.Profile;
        }

        public ProfileListModel GetProfilesAssociatedWithUsers(string userName)
        {
            var endpoint = ProfilesEndpoint.GetProfilesAssociatedWithUsers();
            var status = new ApiStatus();
            AccountModel model = new AccountModel();
            model.UserName = userName;
            var response = PostResourceToEndpoint<ProfileListResponse>(endpoint, JsonConvert.SerializeObject(model), status);
            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new ProfileListModel { Profiles = (Equals(response, null)) ? null : response.Profiles };
            list.MapPagingDataFromResponse(response);

            return list;
        }
        #endregion
    }
}
