﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;
using Znode.Engine.Api.Models.Extensions;

namespace Znode.Engine.Api.Client
{
    public class CategoryProfileClient : BaseClient, ICategoryProfileClient
    {
        #region Public Methods
        public CategoryProfileModel GetCategoryProfile(int categoryProfileId)
        {
            var endpoint = CategoryProfileEndpoint.GetCategoryProfileById(categoryProfileId);       

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<CategoryProfileResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (Equals(response, null)) ? null : response.CategoryProfile;
        }

        public CategoryProfileModel CreateCategoryProfile(CategoryProfileModel model)
        {
            var endpoint = CategoryProfileEndpoint.Create();

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<CategoryProfileResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return (Equals(response, null)) ? null : response.CategoryProfile;
        }

        public CategoryProfileListModel GetCategoryProfileByCategoryId(int categoryId)
        {
            var endpoint = CategoryProfileEndpoint.GetCategoryProfileByCategoryId(categoryId);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<CategoryProfileListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);
            var list = new CategoryProfileListModel { CategoryProfiles = (Equals(response, null)) ? null : response.CategoryProfiles };
            list.MapPagingDataFromResponse(response);

            return list;
            
        }

        public bool UpdateCategoryProfile(int categoryProfileId, CategoryProfileModel model)
        {
            var endpoint = CategoryProfileEndpoint.Update(categoryProfileId);

            var status = new ApiStatus();
            var response = PutResourceToEndpoint<TrueFalseResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return (response.booleanModel.disabled) ? true : false;
        }

        public bool DeleteCategoryProfile(int categoryProfileId)
        {
            var endpoint = CategoryProfileEndpoint.Delete(categoryProfileId);

            var status = new ApiStatus();
            var deleted = DeleteResourceFromEndpoint<TrueFalseResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return deleted;
        } 
        #endregion
    }
}
