﻿using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Engine.Api.Client
{
    public interface ICategoryNodesClient : IBaseClient
    {
        /// <summary>
        /// Gets category node by category node id.
        /// </summary>
        /// <param name="categoryNodeId">The id of category node</param>
        /// <returns>Returns model of type CategoryNodeModel.</returns>
        CategoryNodeModel GetCategoryNode(int categoryNodeId);

        /// <summary>
        ///  Gets category node by category node id.
        /// </summary>
        /// <param name="categoryNodeId">The id of category node</param>
        /// <param name="expands">Expands Collection</param>
        /// <returns>Returns model of type CategoryNodeModel.</returns>
        CategoryNodeModel GetCategoryNode(int categoryNodeId, ExpandCollection expands);

        /// <summary>
        /// Gets list of CategoryNodes.
        /// </summary>
        /// <param name="expands">Expands Collection</param>
        /// <param name="filters">Filter Collection</param>
        /// <param name="sorts">Sort Collection</param>
        /// <returns>Returns model of type CategoryNodeListModel.</returns>
        CategoryNodeListModel GetCategoryNodes(ExpandCollection expands, FilterCollection filters, SortCollection sorts);

        /// <summary>
        /// Gets list of CategoryNodes.
        /// </summary>
        /// <param name="expands">Expands Collection</param>
        /// <param name="filters">Filter Collection</param>
        /// <param name="sorts">Sort Collection</param>
        /// <param name="pageIndex">Index of page</param>
        /// <param name="pageSize">Size of page</param>
        /// <returns>Returns model of type CategoryNodeListModel.</returns>
        CategoryNodeListModel GetCategoryNodes(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Creates category node.
        /// </summary>
        /// <param name="model">Model of type CategoryNodeModel.</param>
        /// <returns>Returns model of type CategoryNodeModel.</returns>
        CategoryNodeModel CreateCategoryNode(CategoryNodeModel model);

        /// <summary>
        /// Updates category node.
        /// </summary>
        /// <param name="categoryNodeId">The id of category node</param>
        /// <param name="model">Model of type CategoryNodeModel</param>
        /// <returns>Returns model of type CategoryNodeModel.</returns>
        CategoryNodeResponse UpdateCategoryNode(int categoryNodeId, CategoryNodeModel model);

        /// <summary>
        /// Deletes the category node.
        /// </summary>
        /// <param name="categoryNodeId">The id of category node</param>
        /// <returns>Returns true or false.</returns>
        bool DeleteCategoryNode(int categoryNodeId);

        /// <summary>
        /// Deletes associated category node.
        /// </summary>
        /// <param name="categoryNodeId">The id of category node</param>
        /// <returns>Returns model of type ZnodeDeleteModel.</returns>
        ZnodeDeleteModel DeleteAssociatedCategoryNode(int categoryNodeId);

        /// <summary>
        /// Gets parent category nodes for specific catalog.
        /// </summary>
        /// <param name="catalogId">The id of catalog</param>
        /// <param name="filters">Filter Collection</param>
        /// <param name="sorts">Sort Collection</param>
        /// <param name="pageIndex">Index of page</param>
        /// <param name="pageSize">Size of page</param>
        /// <returns>Returns model of type CategoryNodeListModel.</returns>
        CategoryNodeListModel GetParentCategoryNodes(int catalogId, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Get BreadCrumb
        /// </summary>
        /// <param name="model">BreadCrumbModel model</param>
        /// <returns>Returns model of type BreadCrumbModel.</returns>
        BreadCrumbModel GetBreadCrumb(BreadCrumbModel model);
    }
}
