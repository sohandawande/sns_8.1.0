﻿using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface ICountriesClient : IBaseClient
	{
        /// <summary>
        /// Get Country on the basis of country code
        /// </summary>
        /// <param name="countryCode">Country Code</param>
        /// <returns>Returns country on the basis of country code</returns>
		CountryModel GetCountry(string countryCode);

        /// <summary>
        /// Get Country on the basis of country code
        /// </summary>
        /// <param name="countryCode">Country Code</param>
        /// <param name="expands">expands</param>
        /// <returns>Returns country on the basis of country code</returns>
		CountryModel GetCountry(string countryCode, ExpandCollection expands);

        /// <summary>
        /// Get list of Countries
        /// </summary>
        /// <param name="expands">expands</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <returns>Returns list of countries</returns>
		CountryListModel GetCountries(ExpandCollection expands, FilterCollection filters, SortCollection sorts);

        /// <summary>
        /// Get list of Countries
        /// </summary>
        /// <param name="expands">expands</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <param name="pageIndex">Index of page</param>
        /// <param name="pageSize">Size of page</param>
        /// <returns>Returns list of countries</returns>
		CountryListModel GetCountries(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Create Country
        /// </summary>
        /// <param name="model">Country Model</param>
        /// <returns>Returns created country</returns>
		CountryModel CreateCountry(CountryModel model);

        /// <summary>
        /// Updates country on the basis of country code
        /// </summary>
        /// <param name="countryCode">Country Code</param>
        /// <param name="model">Country Model</param>
        /// <returns>Returns updated country</returns>
		CountryModel UpdateCountry(string countryCode, CountryModel model);

        /// <summary>
        /// Delete Country on the basis of country code
        /// </summary>
        /// <param name="countryCode">Country Code</param>
        /// <returns>Returns true/false</returns>
		bool DeleteCountry(string countryCode);

        /// <summary>
        /// Znode Version 7.2.2
        /// This function returns list of all active billing and shipping countries on the basis of portal id.
        /// </summary>
        /// <param name="model">int portalId</param>
        /// <param name="billingShippingFlag">int billingShippingFlag</param>
        /// <returns>Returns list of all active billing and shipping countries on the basis of portal id.</returns>
        CountryListModel GetActiveCountryByPortalId(int portalId, int billingShippingFlag=0);

        /// <summary>
        /// This function Get all countries for portalId.
        /// </summary>
        /// <param name="portalId">int portalId to get countries related to perticular portal</param>
        /// <returns>Returns CountryListModel</returns>
        CountryListModel GetCountryByPortalId(int portalId);

        /// <summary>
        /// This function Get Shipping Active countries for portalId.
        /// </summary>
        /// <param name="portalId">int portalId to get countries related to perticular portal</param>
        /// <returns>Returns CountryListModel</returns>
        CountryListModel GetShippingActiveCountryByPortalId(int portalId);
	}
}
