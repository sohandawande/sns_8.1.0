﻿using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IUrlRedirectClient : IBaseClient
    {
        /// <summary>
        /// Gets the URL Redirect List.
        /// </summary>
        /// <param name="filters">Filters for URL Redirect.</param>
        /// <param name="sorts">Sorts for URL Redirect.</param>
        /// <param name="pageIndex">Page Index.</param>
        /// <param name="pageSize">Page Size.</param>
        /// <returns></returns>
        UrlRedirectListModel Get301Redirects(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Get the Url Redirect By url redirect ID.
        /// </summary>
        /// <param name="urlRedirectId">Id of the Url Redirect.</param>
        /// <returns>Returns the url redirect model.</returns>
        UrlRedirectModel GetUrlRedirect(int urlRedirectId);

        /// <summary>
        /// Get the Url Redirect by url redirect ID.
        /// </summary>
        /// <param name="urlRedirectId">Id of the Url Redirect.</param>
        /// <param name="expands">Expand Collection for Url Redirect.</param>
        /// <returns>Returns the url redirect model.</returns>
        UrlRedirectModel GetUrlRedirect(int urlRedirectId, ExpandCollection expands);

        /// <summary>
        /// Create the Url Redirect Model.
        /// </summary>
        /// <param name="model">Model to create.</param>
        /// <returns>Returns the url redirect model.</returns>
        UrlRedirectModel CreateUrlRedirect(UrlRedirectModel model);

        /// <summary>
        /// Update the existing Redirect Url.
        /// </summary>
        /// <param name="urlRedirectId">Id of the Url Redirect.</param>
        /// <param name="model">model to update.</param>
        /// <returns>Returns the updated url redirect model.</returns>
        UrlRedirectModel UpdateUrlRedirect(int urlRedirectId, UrlRedirectModel model);

        /// <summary>
        /// Delete existing url redirect.
        /// </summary>
        /// <param name="urlRedirectId">Id of the Url Redirect.</param>
        /// <returns>Returns True or false.</returns>
        bool DeleteUrlRedirect(int urlRedirectId);
    }
}
