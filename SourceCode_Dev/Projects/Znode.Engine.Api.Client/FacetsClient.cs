﻿using System;
using System.Collections.ObjectModel;
using System.Net;
using Newtonsoft.Json;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;
namespace Znode.Engine.Api.Client
{
    public class FacetsClient : BaseClient, IFacetsClient
    {
        public FacetGroupModel GetFacetGroup(int facetGroupId)
        {
            return GetFacetGroup(facetGroupId, null);
        }

        public FacetGroupModel GetFacetGroup(int facetGroupId, ExpandCollection expands)
        {
            var endpoint = FacetGroupEndpoint.Get(facetGroupId);
            endpoint += BuildEndpointQueryString(expands);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<FacetGroupResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return Equals(response, null) ? null : response.FacetGroup;
        }

        public FacetGroupListModel GetFacetGroups(ExpandCollection expands, FilterCollection filters, SortCollection sorts)
        {
            return GetFacetGroups(expands, filters, sorts, null, null);
        }

        public FacetGroupListModel GetFacetGroups(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = FacetGroupEndpoint.List();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<FacetGroupListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new FacetGroupListModel { FacetGroups = (Equals(response,null)) ? null : response.FacetGroups };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public FacetControlTypeListModel GetFacetControlTypes(ExpandCollection expands, FilterCollection filters, SortCollection sorts)
        {
            var endpoint = FacetGroupEndpoint.FacetControlTypeList();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, null, null);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<FacetControlTypeListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new FacetControlTypeListModel { FacetControlTypes = (Equals(response, null)) ? null : response.FacetControlTypes };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public FacetGroupModel CreateFacetGroup(FacetGroupModel model)
        {
            var endpoint = FacetGroupEndpoint.Create();

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<FacetGroupResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return (Equals(response, null)) ? null : response.FacetGroup;
        }

        public FacetGroupModel UpdateFacetGroup(int facetGroupId, FacetGroupModel model)
        {
            var endpoint = FacetGroupEndpoint.Update(facetGroupId);

            var status = new ApiStatus();
            var response = PutResourceToEndpoint<FacetGroupResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return (Equals(response, null)) ? null : response.FacetGroup;
        }

        public bool DeleteFacetGroup(int facetGroupId)
        {
            var endpoint = FacetGroupEndpoint.Delete(facetGroupId);

            var status = new ApiStatus();
            var deleted = DeleteResourceFromEndpoint<FacetGroupResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

            return deleted;
        }

        public bool InsertFacetGroupCategory(FacetGroupCategoryListModel model)
        {
            var endpoint = FacetGroupEndpoint.InsertFacetGroupCategory();

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<FacetGroupResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return !Equals(response, null);
        }

        public bool DeleteFacetGroupCategoryByFacetGroupId(int facetGroupId)
        {
            var endpoint = FacetGroupEndpoint.DeleteFacetGroupCategory(facetGroupId);

            var status = new ApiStatus();
            var deleted = DeleteResourceFromEndpoint<FacetGroupResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

            return deleted;
        }

        public FacetGroupModel ManageFacetGroup(int facetGroupId, FacetGroupModel model)
        {
            var endpoint = FacetGroupEndpoint.Update(facetGroupId);

            var status = new ApiStatus();
            var response = PutResourceToEndpoint<FacetGroupResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return Equals(response, null) ? null : response.FacetGroup;
        }

        public FacetModel CreateFacet(FacetModel model)
        {
            var endpoint = FacetGroupEndpoint.CreateFacet();

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<FacetResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return (Equals(response, null)) ? null : response.Facet;
        }

        public FacetModel GetFacet(int facetId)
        {
            return GetFacet(facetId, null);
        }

        public FacetModel GetFacet(int facetId, ExpandCollection expands)
        {
            var endpoint = FacetGroupEndpoint.GetFacet(facetId);
            endpoint += BuildEndpointQueryString(expands);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<FacetResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return Equals(response, null) ? null : response.Facet;
        }

        public FacetModel EditFacet(int facetId, FacetModel model)
        {
            var endpoint = FacetGroupEndpoint.UpdateFacet(facetId);

            var status = new ApiStatus();
            var response = PutResourceToEndpoint<FacetResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return Equals(response, null) ? null : response.Facet;
        }

        public bool DeleteFacet(int facetId)
        {
            var endpoint = FacetGroupEndpoint.DeleteFacet(facetId);

            var status = new ApiStatus();
            var deleted = DeleteResourceFromEndpoint<FacetResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

            return deleted;
        }

        public FacetListModel GetFacetList(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = FacetGroupEndpoint.FacetList();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<FacetListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new FacetListModel {Facets = (Equals(response, null)) ? null : response.Facets };
            list.MapPagingDataFromResponse(response);

            return list;
        }


    }
}
