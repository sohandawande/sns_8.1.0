﻿using System.Data;
using Znode.Engine.Api.Client.Filters;

namespace Znode.Engine.Api.Client
{
    public interface IAffiliateTrackingClient : IBaseClient
    {
        /// <summary>
        /// Gets the tracking data on basis of dates provided in filters.
        /// </summary>
        /// <param name="filters">FilterCollection</param>
        /// <returns>Returns DataSet</returns>
        DataSet GetTrackingData(FilterCollection filters = null);

    }
}
