﻿using System.Collections.ObjectModel;
using System.Net;
using Newtonsoft.Json;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
	public class HighlightTypesClient : BaseClient, IHighlightTypesClient
	{
		public HighlightTypeModel GetHighlightType(int highlightTypeId)
		{
			var endpoint = HighlightTypesEndpoint.Get(highlightTypeId);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<HighlightTypeResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

			return (response == null) ? null : response.HighlightType;
		}

		public HighlightTypeListModel GetHighlightTypes(FilterCollection filters, SortCollection sorts)
		{
			return GetHighlightTypes(filters, sorts, null, null);
		}

		public HighlightTypeListModel GetHighlightTypes(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
		{
			var endpoint = HighlightTypesEndpoint.List();
			endpoint += BuildEndpointQueryString(null, filters, sorts, pageIndex, pageSize);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<HighlightTypeListResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new HighlightTypeListModel { HighlightTypes = (response == null) ? null : response.HighlightTypes };
			list.MapPagingDataFromResponse(response);

			return list;
		}

		public HighlightTypeModel CreateHighlightType(HighlightTypeModel model)
		{
			var endpoint = HighlightTypesEndpoint.Create();

			var status = new ApiStatus();
			var response = PostResourceToEndpoint<HighlightTypeResponse>(endpoint, JsonConvert.SerializeObject(model), status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

			return (response == null) ? null : response.HighlightType;
		}

		public HighlightTypeModel UpdateHighlightType(int highlightTypeId, HighlightTypeModel model)
		{
			var endpoint = HighlightTypesEndpoint.Update(highlightTypeId);

			var status = new ApiStatus();
			var response = PutResourceToEndpoint<HighlightTypeResponse>(endpoint, JsonConvert.SerializeObject(model), status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

			return (response == null) ? null : response.HighlightType;
		}

		public bool DeleteHighlightType(int highlightTypeId)
		{
			var endpoint = HighlightTypesEndpoint.Delete(highlightTypeId);

			var status = new ApiStatus();
			var deleted = DeleteResourceFromEndpoint<HighlightTypeResponse>(endpoint, status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

			return deleted;
		}
	}
}
