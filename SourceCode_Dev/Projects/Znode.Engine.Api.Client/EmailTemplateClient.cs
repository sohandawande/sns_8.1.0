﻿using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    public class EmailTemplateClient : BaseClient, IEmailTemplateClient
    {

        public EmailTemplateListModel GetTemplate(FilterCollection filters, SortCollection sorts)
        {
            return GetTemplates(filters, sorts, null, null);
        }

        public EmailTemplateListModel GetTemplates(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            string endpoint = EmailTemplateEndpoint.List();
            endpoint += BuildEndpointQueryString(null, filters, sorts, pageIndex, pageSize);

            ApiStatus status = new ApiStatus();
            EmailTemplateListResponse response = GetResourceFromEndpoint<EmailTemplateListResponse>(endpoint, status);

            Collection<HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            EmailTemplateListModel list = new EmailTemplateListModel { EmailTemplates = Equals(response, null) ? null : response.EmailTemplates };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public EmailTemplateModel CreateTemplatePage(EmailTemplateModel model)
        {
            string endpoint = EmailTemplateEndpoint.Create();

            ApiStatus status = new ApiStatus();
            EmailTemplateResponse response = PostResourceToEndpoint<EmailTemplateResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            if (!Equals(status.ErrorMessage, null))
            {
                model.CustomErrorMessage = status.ErrorMessage;
            }

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return (Equals(response, null)) ? null : response.EmailTemplate;
        }

        public EmailTemplateModel GetHtmlContent(EmailTemplateModel model)
        {
            string endpoint = EmailTemplateEndpoint.Create();

            ApiStatus status = new ApiStatus();
            EmailTemplateResponse response = PostResourceToEndpoint<EmailTemplateResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            if (!Equals(status.ErrorMessage, null))
            {
                model.CustomErrorMessage = status.ErrorMessage;
            }

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return (Equals(response, null)) ? null : response.EmailTemplate;
        }

        public EmailTemplateModel GetTemplatePage(string templateName, string extension)
        {
            return GetTemplatePage(templateName, extension, null);
        }

        public EmailTemplateModel GetTemplatePage(string templateName, string extension, ExpandCollection expands)
        {
            string endpoint = EmailTemplateEndpoint.Get(templateName, extension);
            endpoint += BuildEndpointQueryString(expands);

            ApiStatus status = new ApiStatus();
            EmailTemplateResponse response = GetResourceFromEndpoint<EmailTemplateResponse>(endpoint, status);

            Collection<HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return Equals(response, null) ? null : response.EmailTemplate;
        }

        public bool UpdateTemplatePage(EmailTemplateModel model)
        {
            string endpoint = EmailTemplateEndpoint.Update();

            ApiStatus status = new ApiStatus();
            TrueFalseResponse response = PutResourceToEndpoint<TrueFalseResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return response.booleanModel.disabled;
        }

        public bool DeleteTemplatePage(string templateName, string extension)
        {
            string endpoint = EmailTemplateEndpoint.Delete(templateName,extension);

            ApiStatus status = new ApiStatus();
            bool deleted = DeleteResourceFromEndpoint<TrueFalseResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

            return deleted;
        }

        public EmailTemplateListModel GetDeletedTemplates(ExpandCollection expands, FilterCollection filters, SortCollection sorts)
        {
            return GetDeletedTemplates(expands, filters, sorts, null, null);
        }

        public EmailTemplateListModel GetDeletedTemplates(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            string endpoint = EmailTemplateEndpoint.GetKeys();
            endpoint += BuildEndpointQueryString(expands, null, sorts, null, null);

            ApiStatus status = new ApiStatus();
            EmailTemplateListResponse response = GetResourceFromEndpoint<EmailTemplateListResponse>(endpoint, status);

            Collection<HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            EmailTemplateListModel list = new EmailTemplateListModel { EmailTemplates = (Equals(response, null)) ? null : response.EmailTemplates };
            list.MapPagingDataFromResponse(response);

            return list;
        }

    }
}
