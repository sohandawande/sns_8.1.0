﻿using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.Net;
using System.Threading.Tasks;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    /// <summary>
    /// Message Configs Client
    /// </summary>
	public class MessageConfigsClient : BaseClient, IMessageConfigsClient
	{
        #region Public Methods

        public MessageConfigModel GetMessageConfig(int messageConfigId)
        {
            return GetMessageConfig(messageConfigId, null);
        }

        public MessageConfigModel GetMessageConfig(int messageConfigId, ExpandCollection expands)
        {
            var endpoint = MessageConfigsEndpoint.Get(messageConfigId);
            endpoint += BuildEndpointQueryString(expands);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<MessageConfigResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (Equals(response, null)) ? null : response.MessageConfig;
        }

        public MessageConfigModel GetMessageConfigByKey(string key, ExpandCollection expands)
        {
            var endpoint = MessageConfigsEndpoint.GetByKey(key);
            endpoint += BuildEndpointQueryString(expands);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<MessageConfigResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (response == null) ? null : response.MessageConfig;
        }

        public async Task<MessageConfigModel> GetMessageConfigByKeyAsync(string key, ExpandCollection expands)
        {
            var endpoint = MessageConfigsEndpoint.GetByKey(key);
            endpoint += BuildEndpointQueryString(expands);

            var status = new ApiStatus();
            var response = await GetResourceFromEndpointAsync<MessageConfigResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (response == null) ? null : response.MessageConfig;
        }

        public MessageConfigListModel GetMessageConfigs(ExpandCollection expands, FilterCollection filters, SortCollection sorts)
        {
            return GetMessageConfigs(expands, filters, sorts, null, null);
        }

        public MessageConfigListModel GetMessageConfigs(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = MessageConfigsEndpoint.List();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<MessageConfigListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new MessageConfigListModel { MessageConfigs = (Equals(response, null)) ? null : response.MessageConfigs };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public MessageConfigListModel GetMessageConfigsByKeys(string keys, ExpandCollection expands, SortCollection sorts)
        {
            var endpoint = MessageConfigsEndpoint.List();
            endpoint += BuildEndpointQueryString(expands, null, sorts, null, null);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<MessageConfigListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new MessageConfigListModel { MessageConfigs = (Equals(response, null)) ? null : response.MessageConfigs };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public MessageConfigModel CreateMessageConfig(MessageConfigModel model)
        {
            var endpoint = MessageConfigsEndpoint.Create();

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<MessageConfigResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return (Equals(response, null)) ? null : response.MessageConfig;
        }

        public MessageConfigModel UpdateMessageConfig(int messageConfigId, MessageConfigModel model)
        {
            var endpoint = MessageConfigsEndpoint.Update(messageConfigId);

            var status = new ApiStatus();
            var response = PutResourceToEndpoint<MessageConfigResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return (Equals(response, null)) ? null : response.MessageConfig;
        }

        public bool DeleteMessageConfig(int messageConfigId)
        {
            var endpoint = MessageConfigsEndpoint.Delete(messageConfigId);

            var status = new ApiStatus();
            var deleted = DeleteResourceFromEndpoint<MessageConfigResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

            return deleted;
        } 

        #endregion
	}
}
