﻿using System.Collections.ObjectModel;
using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    /// <summary>
    /// Client of master page.
    /// </summary>
    public class MasterPageClient : BaseClient, IMasterPageClient
    {
        #region Public Methods
        public MasterPageListModel GetMasterPages(ExpandCollection expands, FilterCollection filters, SortCollection sorts)
        {
            return GetMasterPages(expands, filters, sorts, null, null);
        }

        public MasterPageListModel GetMasterPages(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = MasterPageEndpoint.List();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<MasterPageListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new MasterPageListModel { MasterPages = (Equals(response, null)) ? null : response.MasterPages };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public MasterPageListModel GetMasterPageByThemeId(int themeId, string pageType)
        {
            var endpoint = MasterPageEndpoint.GetMasterPageByThemeId(themeId, pageType);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<MasterPageListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new MasterPageListModel { MasterPages = (Equals(response, null)) ? null : response.MasterPages };
            list.MapPagingDataFromResponse(response);

            return list;
        } 
        #endregion
    }
}
