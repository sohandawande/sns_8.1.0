﻿using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;
namespace Znode.Engine.Api.Client
{
    public class ReasonForReturnsClient :  BaseClient, IReasonForReturnClient
    {
        #region Public Methods
        public ReasonForReturnModel GetReasonForReturn(int reasonForReturnId)
        {
            return GetReasonForReturn(reasonForReturnId, null);
        }

        public ReasonForReturnModel GetReasonForReturn(int reasonForReturnId, ExpandCollection expands)
        {
            var endpoint = ReasonForReturnEndpoint.Get(reasonForReturnId);
            endpoint += BuildEndpointQueryString(expands);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ReasonForReturnResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return Equals(response, null) ? null : response.ReasonForReturn;
        }

        public ReasonForReturnListModel GetListOfReasonForReturn(Expands.ExpandCollection expands, FilterCollection filters, SortCollection sorts)
        {
            return GetListOfReasonForReturn(expands, filters, sorts, null, null);
        }

        public ReasonForReturnListModel GetListOfReasonForReturn(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = ReasonForReturnEndpoint.List();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ReasonForReturnListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new ReasonForReturnListModel { ReasonsForReturn = (Equals(response, null)) ? null : response.ReasonForReturnList };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public ReasonForReturnModel CreateReasonForReturn(ReasonForReturnModel model)
        {
            var endpoint = ReasonForReturnEndpoint.Create();

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<ReasonForReturnResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return (Equals(response, null)) ? null : response.ReasonForReturn;
        }

        public ReasonForReturnModel UpdateReasonForReturn(int reasonForReturnId, ReasonForReturnModel model)
        {
            var endpoint = ReasonForReturnEndpoint.Update(reasonForReturnId);

            var status = new ApiStatus();
            var response = PutResourceToEndpoint<ReasonForReturnResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return (Equals(response, null)) ? null : response.ReasonForReturn;
        }

        public bool DeleteReasonForReturn(int reasonForReturnId)
        {
            var endpoint = ReasonForReturnEndpoint.Delete(reasonForReturnId);

            var status = new ApiStatus();
            var deleted = DeleteResourceFromEndpoint<ReasonForReturnResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

            return deleted;
        }
        #endregion
    }
}
