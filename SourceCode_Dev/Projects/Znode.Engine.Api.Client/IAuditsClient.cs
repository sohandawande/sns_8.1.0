﻿using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
	public interface IAuditsClient :  IBaseClient
	{
		AuditModel GetAudit(int auditId);
		AuditModel GetAudit(int auditId, ExpandCollection expands);
		AuditListModel GetAudits(ExpandCollection expands, FilterCollection filters, SortCollection sorts);
		AuditListModel GetAudits(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
		AuditModel UpdateAudit(int auditId, AuditModel model);
		bool DeleteAudit(int auditId);
	}
}
