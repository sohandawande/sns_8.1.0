﻿using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    public class CurrencyTypeClient : BaseClient, ICurrencyTypeClient
    {
        public CurrencyTypeListModel GetCurrencyTypes(ExpandCollection expands, FilterCollection filters, SortCollection sorts)
        {
            return GetCurrencyTypes(expands, filters, sorts, null, null);
        }
        public CurrencyTypeListModel GetCurrencyTypes(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = CurrencyTypeEndpoint.List();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<CurrencyTypeListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new CurrencyTypeListModel { Currency = (response == null) ? null : response.CurrencyTypes };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public CurrencyTypeModel GetCurrencyType(int currencyTypeId)
        {
            var endpoint = CurrencyTypeEndpoint.Get(currencyTypeId);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<CurrencyTypeResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return ((Equals(response, null) ? null : response.CurrencyType));
        }

        public CurrencyTypeModel UpdateCurrencyType(int currencyTypeId, CurrencyTypeModel model)
        {
            var endpoint = CurrencyTypeEndpoint.Update(currencyTypeId);

            var status = new ApiStatus();
            var response = PutResourceToEndpoint<CurrencyTypeResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return (Equals(response, null)) ? null : response.CurrencyType;
        }
    }
}
