﻿using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    public class ProductTypeAttributeClient : BaseClient , IProductTypeAttributeClient
    {

        #region Get Product Type Attribute
        public ProductTypeAttributeModel GetAttributeType(int attributeTypeId)
        {
            return GetAttributeType(attributeTypeId, null);
        }

        public ProductTypeAttributeListModel GetAttributeTypes(ExpandCollection expands, FilterCollection filters, SortCollection sorts)
        {
            return GetAttributeTypes(expands, filters, sorts, null, null);
        }

        public ProductTypeAttributeListModel GetAttributeTypes(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = ProductTypeAttributeEndpoint.List();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ProductTypeAttributeListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new ProductTypeAttributeListModel { ProductTypeAttribute = (response == null) ? null : response.ProductTypeAttributes };
            list.MapPagingDataFromResponse(response);

            return list;
        }
        public ProductTypeAttributeModel GetAttributeType(int ProductTypeAttributeId, ExpandCollection expands)
        {
            var endpoint = ProductTypeAttributeEndpoint.Get(ProductTypeAttributeId);
            endpoint += BuildEndpointQueryString(expands);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ProductTypeAttributeResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (Equals(response, null)) ? null : response.ProductTypeAttribute;
        } 
        #endregion

        #region Create Product Type Attribute
        public ProductTypeAttributeModel CreateAttributeType(ProductTypeAssociatedAttributeTypesModel model)
        {
            var endpoint = ProductTypeAttributeEndpoint.Create();

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<ProductTypeAttributeResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return (Equals(response, null)) ? null : response.ProductTypeAttribute;
        } 
        #endregion

        #region Delete Attribute Type
        public bool DeleteAttributeType(int attributeTypeId)
        {
            var endpoint = ProductTypeAttributeEndpoint.Delete(attributeTypeId);

            var status = new ApiStatus();
            var deleted = DeleteResourceFromEndpoint<ProductTypeAttributeResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

            return deleted;
        } 
        #endregion

        #region Get Attribute Types with filters
        public ProductTypeAssociatedAttributeTypesListModel GetAttributeTypesByProductTypeIdService(int productId, ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = ProductTypeAttributeEndpoint.GetAttributeTypesByProductTypeId(productId);
            endpoint += BuildEndpointQueryString(null, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ProductTypeAttributeListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (Equals(response, null)) ? null : response.ProductTypeAssociatedAttributeTypes;
        } 
        #endregion
    }
}
