﻿using System.Collections.ObjectModel;
using System.Net;
using Newtonsoft.Json;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
	public class HighlightsClient : BaseClient, IHighlightsClient
	{
		public HighlightModel GetHighlight(int highlightId)
		{
			return GetHighlight(highlightId, null);
		}

		public HighlightModel GetHighlight(int highlightId, ExpandCollection expands)
		{
			var endpoint = HighlightsEndpoint.Get(highlightId);
			endpoint += BuildEndpointQueryString(expands);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<HighlightResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

			return (response == null) ? null : response.Highlight;
		}

		public HighlightListModel GetHighlights(ExpandCollection expands, FilterCollection filters, SortCollection sorts)
		{
			return GetHighlights(expands, filters, sorts, null, null);
		}

		public HighlightListModel GetHighlights(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
		{
			var endpoint = HighlightsEndpoint.List();
			endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<HighlightListResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new HighlightListModel { Highlights = (response == null) ? null : response.Highlights };
			list.MapPagingDataFromResponse(response);

			return list;
		}

		public HighlightModel CreateHighlight(HighlightModel model)
		{
			var endpoint = HighlightsEndpoint.Create();

			var status = new ApiStatus();
			var response = PostResourceToEndpoint<HighlightResponse>(endpoint, JsonConvert.SerializeObject(model), status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

			return (response == null) ? null : response.Highlight;
		}

		public HighlightModel UpdateHighlight(int highlightId, HighlightModel model)
		{
			var endpoint = HighlightsEndpoint.Update(highlightId);

			var status = new ApiStatus();
			var response = PutResourceToEndpoint<HighlightResponse>(endpoint, JsonConvert.SerializeObject(model), status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

			return (response == null) ? null : response.Highlight;
		}

		public bool DeleteHighlight(int highlightId)
		{
			var endpoint = HighlightsEndpoint.Delete(highlightId);

			var status = new ApiStatus();
			var deleted = DeleteResourceFromEndpoint<HighlightResponse>(endpoint, status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

			return deleted;
		}

        public HighlightModel CheckAssociatedProduct(int highlightId)
        {
            var endpoint = HighlightsEndpoint.CheckAssociatedProduct(highlightId);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<HighlightResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (response == null) ? null : response.Highlight;
        }
	}
}
