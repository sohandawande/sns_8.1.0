﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public partial interface IAccountsClient : IBaseClient
    {
        Collection<PRFTSalesRepresentative>GetSalesRepInfoByRoleName(string roleName);
    }
}
