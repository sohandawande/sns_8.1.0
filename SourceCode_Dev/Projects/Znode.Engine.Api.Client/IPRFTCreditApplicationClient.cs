﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IPRFTCreditApplicationClient:IBaseClient
    {
        PRFTCreditApplicationModel GetCreditApplication(int creditApplicationId);
        PRFTCreditApplicationListModel GetCreditApplications(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
        PRFTCreditApplicationModel CreateCreditApplication(PRFTCreditApplicationModel model);
        PRFTCreditApplicationModel UpdateCreditApplication(int creditApplicationId, PRFTCreditApplicationModel model);
    }
}
