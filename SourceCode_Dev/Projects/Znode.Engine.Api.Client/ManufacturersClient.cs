﻿using System.Collections.ObjectModel;
using System.Net;
using Newtonsoft.Json;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
	public class ManufacturersClient : BaseClient, IManufacturersClient
	{
		public ManufacturerModel GetManufacturer(int manufacturerId)
		{
			var endpoint = ManufacturersEndpoint.Get(manufacturerId);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<ManufacturerResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

			return (response == null) ? null : response.Manufacturer;
		}

		public ManufacturerListModel GetManufacturers(FilterCollection filters, SortCollection sorts)
		{
			return GetManufacturers(filters, sorts, null, null);
		}

		public ManufacturerListModel GetManufacturers(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
		{
			var endpoint = ManufacturersEndpoint.List();
			endpoint += BuildEndpointQueryString(null, filters, sorts, pageIndex, pageSize);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<ManufacturerListResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new ManufacturerListModel { Manufacturers = (response == null) ? null : response.Manufacturers };
			list.MapPagingDataFromResponse(response);

			return list;
		}

		public ManufacturerModel CreateManufacturer(ManufacturerModel model)
		{
			var endpoint = ManufacturersEndpoint.Create();

			var status = new ApiStatus();
			var response = PostResourceToEndpoint<ManufacturerResponse>(endpoint, JsonConvert.SerializeObject(model), status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

			return (response == null) ? null : response.Manufacturer;
		}

		public ManufacturerModel UpdateManufacturer(int manufacturerId, ManufacturerModel model)
		{
			var endpoint = ManufacturersEndpoint.Update(manufacturerId);

			var status = new ApiStatus();
			var response = PutResourceToEndpoint<ManufacturerResponse>(endpoint, JsonConvert.SerializeObject(model), status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

			return (response == null) ? null : response.Manufacturer;
		}

		public bool DeleteManufacturer(int manufacturerId)
		{
			var endpoint = ManufacturersEndpoint.Delete(manufacturerId);

			var status = new ApiStatus();
			var deleted = DeleteResourceFromEndpoint<TrueFalseResponse>(endpoint, status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

			return deleted;
		}
	}
}
