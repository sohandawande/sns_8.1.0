﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    public class PRFTERPOrdersClient: BaseClient, IPRFTERPOrdersClient
    {
        public PRFTOEHeaderHistoryModel GetInvoiceDetailsFromERP(string ExternalId)
        {
            return GetInvoiceDetailsFromERP(ExternalId, null);
        }
        public PRFTOEHeaderHistoryModel GetInvoiceDetailsFromERP(string ExternalId, ExpandCollection expands)
        {
            var endpoint = OrdersEndpoint.InvoiceDetails(ExternalId);
            endpoint += BuildEndpointQueryString(expands);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<PRFTERPOrderResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (response == null) ? null : response.OEHeaderHistoryModel;
        }
    }
}
