﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
    public class CurrencyTypeEndpoint:BaseEndpoint
    {
        public static string List()
        {
            return String.Format("{0}/currencytypes", ApiRoot);
        }

        public static string Get(int currencyTypeId)
        {
            return String.Format("{0}/currencytype/{1}", ApiRoot, currencyTypeId);
        }

        public static string Update(int currencyTypeId)
        {
            return String.Format("{0}/currencytype/{1}", ApiRoot, currencyTypeId);
        }
    }
}
