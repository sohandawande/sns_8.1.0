﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
    public class CSSEndpoint : BaseEndpoint
    {
        public static string List()
        {
            return String.Format("{0}/csslist", ApiRoot);
        }

        public static string Create()
        {
            return String.Format("{0}/createcss", ApiRoot);
        }

        public static string Get(int cssId)
        {
            return String.Format("{0}/getcss/{1}", ApiRoot, cssId);
        }

        public static string Update(int cssId)
        {
            return String.Format("{0}/updatecss/{1}", ApiRoot, cssId);
        }

        public static string Delete(int cssId)
        {
            return String.Format("{0}/deletecss/{1}", ApiRoot, cssId);
        }
    }
}
