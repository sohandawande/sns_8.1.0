﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
	public class InventoryEndpoint : BaseEndpoint
	{
		public static string Create()
		{
			return String.Format("{0}/inventory", ApiRoot);
		}

		public static string Delete(int inventoryId)
		{
			return String.Format("{0}/inventory/{1}", ApiRoot, inventoryId);
		}

		public static string Get(int inventoryId)
		{
			return String.Format("{0}/inventory/{1}", ApiRoot, inventoryId);
		}

		public static string List()
		{
			return String.Format("{0}/inventory", ApiRoot);
		}

		public static string Update(int inventoryId)
		{
			return String.Format("{0}/inventory/{1}", ApiRoot, inventoryId);
		}
	}
}
