﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
    public class DiagnosticsEndpoint : BaseEndpoint
    {
        public static string CheckEmailAccount()
        {
            return String.Format("{0}/Diagnostics/CheckEmailAccount", ApiRoot);
        }

        public static string GetProductVersionDetails()
        {
            return String.Format("{0}/Diagnostics/GetProductVersionDetails", ApiRoot);
        }

        public static string EmailDiagnostics()
        {
            return String.Format("{0}/Diagnostics/EmailDiagnostics", ApiRoot);
        }
    }
}
