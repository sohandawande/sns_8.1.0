﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
     public class CustomerEndpoint : BaseEndpoint
    {
         public static string List()
         {
             return String.Format("{0}/customer", ApiRoot);
         }
         public static string GetCustomerPricingProduct(int accountId)
         {
             return String.Format("{0}/customers/getcustomerpricingproduct/{1}", ApiRoot, accountId);
         }

         public static string GetCustomerAffiliate(int accountId)
         {
             return String.Format("{0}/customer/{1}", ApiRoot, accountId);
         }

         public static string GetReferralCommissionTypeList()
         {
             return String.Format("{0}/getreferralcommissiontypelist", ApiRoot);
         }

         public static string GetReferralCommissionList(int accountId)
         {
             return String.Format("{0}/getreferralcommissionlist/{1}", ApiRoot, accountId);
         }

        //PRFT Custom Code : Start
        public static string associatedSuperUserList(int accountId)
        {
            return String.Format("{0}/getAssociatedSuperUser/{1}", ApiRoot, accountId);
        }

        public static string notAssociatedSuperUserList(int accountId)
        {
            return String.Format("{0}/getNotAssociatedSuperUser/{1}", ApiRoot, accountId);
        }
        public static string GetSubUser(int parentAccountId)
        {
            return String.Format("{0}/getSubUser/{1}", ApiRoot, parentAccountId);
        }
        //PRFT Custom Code : End
    }
}
