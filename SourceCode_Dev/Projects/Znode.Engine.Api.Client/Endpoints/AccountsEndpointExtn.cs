﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Client.Endpoints
{
    public partial class AccountsEndpoint : BaseEndpoint
    {
        public static string GetSalesRepInfoByRoleName(string roleName)
        {
            return String.Format("{0}/accounts/getsalesrepinfobyrolename/{1}", ApiRoot, roleName);
        }

        public static string GetAccountDetailsFromERP(string ExternalId)
        {
            return String.Format("{0}/accounts/getaccountdetailsfromerp/{1}", ApiRoot, ExternalId);
        }
    }
}
