﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
    /// <summary>
    /// Endpoint for Google Site Map
    /// </summary>
    public class GoogleSiteMapEndpoint : BaseEndpoint
    {
        public static string Create()
        {
            return String.Format("{0}/googlesitemap", ApiRoot);
        }
    }
}
