﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
    public class VendorProductEndpoint : BaseEndpoint
    {
        public static string List()
        {
            return String.Format("{0}/vendorproducts", ApiRoot);
        }
        public static string ChangeProductStatus(string state)
        {
            return String.Format("{0}/vendorproducts/changeproductstatus/{1}", ApiRoot, state);
        }
        public static string BindRejectProduct(string productIds)
        {
            return String.Format("{0}/vendorproducts/bindrejectproduct/{1}/", ApiRoot, productIds);
        }
        public static string ImageList()
        {
            return String.Format("{0}/vendorproducts/reviewimagelist", ApiRoot);
        }
        public static string UpdateProductImageStatus(string productIds, string state)
        {
            return String.Format("{0}/vendorproducts/updateproductimagestatus/{1}/{2}", ApiRoot,productIds, state);
        }

        public static string GetProductReviewHistory()
        {
            return String.Format("{0}/productshistory", ApiRoot);
        }
        public static string GetReviewHistoryById(int productReviewHistoryID)
        {
            return String.Format("{0}/productshistory/{1}", ApiRoot, productReviewHistoryID);
        }
        public static string Update(int productId)
        {
            return String.Format("{0}/vendorproducts/{1}", ApiRoot, productId);
        }
        public static string UpdateMarketingDetails(int productId)
        {
            return String.Format("{0}/vendorproducts/updatemarketingdetails/{1}", ApiRoot, productId);
        }
        public static string GetCategoryNode(int productId)
        {
            return String.Format("{0}/vendorproducts/getcategorynode/{1}", ApiRoot, productId);
        }
        public static string Delete(int productId)
        {
            return String.Format("{0}/vendorproducts/{1}", ApiRoot, productId);
        }
        public static string MallAdminProductList()
        {
            return String.Format("{0}/malladminproducts", ApiRoot);
        }
    }
}
