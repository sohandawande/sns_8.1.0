﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
	public class TaxRulesEndpoint : BaseEndpoint
	{
		public static string Create()
		{
			return String.Format("{0}/taxrules", ApiRoot);
		}

		public static string Delete(int taxRuleId)
		{
			return String.Format("{0}/taxrules/{1}", ApiRoot, taxRuleId);
		}

		public static string Get(int taxRuleId)
		{
			return String.Format("{0}/taxrules/{1}", ApiRoot, taxRuleId);
		}

		public static string List()
		{
			return String.Format("{0}/taxrules", ApiRoot);
		}

		public static string Update(int taxRuleId)
		{
			return String.Format("{0}/taxrules/{1}", ApiRoot, taxRuleId);
		}
	}
}
