﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
    /// <summary>
    /// Endpoint for dashboard.
    /// </summary>
    public class DashboardEndpoint : BaseEndpoint
    {
        public static string GetDashboardItems(int portalId)
        {
            return String.Format("{0}/dashboard/getdashboarditemsbyportalid/{1}", ApiRoot, portalId);
        }
    }
}
