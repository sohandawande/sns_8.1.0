﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
	public class ReviewsEndpoint : BaseEndpoint
	{
		public static string Create()
		{
			return String.Format("{0}/reviews", ApiRoot);
		}

		public static string Delete(int reviewId)
		{
			return String.Format("{0}/reviews/{1}", ApiRoot, reviewId);
		}

		public static string Get(int reviewId)
		{
			return String.Format("{0}/reviews/{1}", ApiRoot, reviewId);
		}

		public static string List()
		{
			return String.Format("{0}/reviews", ApiRoot);
		}

		public static string Update(int reviewId)
		{
			return String.Format("{0}/reviews/{1}", ApiRoot, reviewId);
		}
	}
}
