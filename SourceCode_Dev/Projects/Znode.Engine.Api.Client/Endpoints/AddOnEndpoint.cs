﻿    using System;

namespace Znode.Engine.Api.Client.Endpoints
{
    public class AddOnEndpoint : BaseEndpoint
    {
        public static string Create()
        {
            return String.Format("{0}/addons", ApiRoot);
        }

        public static string Delete(int addOnId)
        {
            return String.Format("{0}/addons/{1}", ApiRoot, addOnId);
        }

        public static string Get(int addOnId)
        {
            return String.Format("{0}/addons/{1}", ApiRoot, addOnId);
        }

        public static string List()
        {
            return String.Format("{0}/addons", ApiRoot);
        }

        public static string Update(int addOnId)
        {
            return String.Format("{0}/addons/{1}", ApiRoot, addOnId);
        }
    }
}
