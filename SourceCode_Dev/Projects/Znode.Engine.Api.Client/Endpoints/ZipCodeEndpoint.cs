﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
    public class ZipCodeEndpoint : BaseEndpoint
    {
        public static string List()
        {
            return String.Format("{0}/zipcodes", ApiRoot);
        }
    }
}
