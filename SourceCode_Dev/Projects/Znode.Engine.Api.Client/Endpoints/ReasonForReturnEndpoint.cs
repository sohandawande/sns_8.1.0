﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
    public class ReasonForReturnEndpoint : BaseEndpoint
    {
        public static string Get(int reasonForReturnId)
        {
            return String.Format("{0}/reasonforreturn/{1}", ApiRoot, reasonForReturnId);
        }
       
        public static string List()
        {
            return String.Format("{0}/reasonforreturn", ApiRoot);
        }

        public static string Create()
        {
            return String.Format("{0}/reasonforreturn", ApiRoot);
        }

        public static string Delete(int reasonForReturnId)
        {
            return String.Format("{0}/reasonforreturn/{1}", ApiRoot, reasonForReturnId);
        }

        public static string Update(int reasonForReturnId)
        {
            return String.Format("{0}/reasonforreturn/{1}", ApiRoot, reasonForReturnId);
        }
    }
}
