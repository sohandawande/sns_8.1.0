﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
    /// <summary>
    /// Endpoints of Product Categories
    /// </summary>
	public class ProductCategoriesEndpoint : BaseEndpoint
	{
		public static string Create()
		{
			return String.Format("{0}/productcategories", ApiRoot);
		}

		public static string Delete(int productCategoryId)
		{
			return String.Format("{0}/productcategories/{1}", ApiRoot, productCategoryId);
		}

		public static string Get(int productCategoryId)
		{
			return String.Format("{0}/productcategories/{1}", ApiRoot, productCategoryId);
		}

		public static string List()
		{
			return String.Format("{0}/productcategories", ApiRoot);
		}

		public static string Update(int productCategoryId)
		{
			return String.Format("{0}/productcategories/{1}", ApiRoot, productCategoryId);
		}

        public static string GetProductCategory(int productId, int categoryId)
        {
            return String.Format("{0}/productcategories/{1}/{2}", ApiRoot, productId, categoryId);
        }
	}
}
