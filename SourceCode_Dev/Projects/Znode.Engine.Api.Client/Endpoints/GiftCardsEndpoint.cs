﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
	public class GiftCardsEndpoint : BaseEndpoint
	{
		public static string Create()
		{
			return String.Format("{0}/giftcards", ApiRoot);
		}

		public static string Delete(int giftCardId)
		{
			return String.Format("{0}/giftcards/{1}", ApiRoot, giftCardId);
		}

		public static string Get(int giftCardId)
		{
			return String.Format("{0}/giftcards/{1}", ApiRoot, giftCardId);
		}

		public static string List()
		{
			return String.Format("{0}/giftcards", ApiRoot);
		}

		public static string Update(int giftCardId)
		{
			return String.Format("{0}/giftcards/{1}", ApiRoot, giftCardId);
		}

        public static string GetNextGiftCardNumber()
        {
            return string.Format("{0}/giftcardnumber", ApiRoot);
        }

        public static string IsGiftCardValid(string giftCard,int accountId)
        {
            return String.Format("{0}/giftcardnumber/{1}/{2}", ApiRoot, giftCard, accountId);
        }
	}
}
