﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Client.Endpoints
{
    public class PRFTCreditApplicationEndpoint : BaseEndpoint
    {
        public static string Create()
        {
            return String.Format("{0}/creditapplication", ApiRoot);
        }

        public static string Get(int creditApplicationId)
        {
            return String.Format("{0}/creditapplication/{1}", ApiRoot, creditApplicationId);
        }


        public static string List()
        {
            return String.Format("{0}/creditapplication", ApiRoot);
        }

        public static string Update(int creditApplicationId)
        {
            return String.Format("{0}/creditapplication/{1}", ApiRoot, creditApplicationId);
        }
    }
}
