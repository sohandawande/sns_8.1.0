﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
    /// <summary>
    /// Endpoints of Categories
    /// </summary>
    public class CategoriesEndpoint : BaseEndpoint
    {
        public static string Create()
        {
            return String.Format("{0}/categories", ApiRoot);
        }

        public static string Delete(int categoryId)
        {
            return String.Format("{0}/categories/{1}", ApiRoot, categoryId);
        }

        public static string Get(int categoryId)
        {
            return String.Format("{0}/categories/{1}", ApiRoot, categoryId);
        }

        public static string List()
        {
            return String.Format("{0}/categories", ApiRoot);
        }

        public static string CategoryTree()
        {
            return String.Format("{0}/categories/getcategorytree", ApiRoot);
        }

        public static string ListByCatalog(int catalogId)
        {
            return String.Format("{0}/categories/catalog/{1}", ApiRoot, catalogId);
        }

        public static string ListByCatalogIds(string catalogIds)
        {
            return String.Format("{0}/categories/catalog/{1}", ApiRoot, catalogIds);
        }

        public static string ListByCategoryIds(string categoryIds)
        {
            return String.Format("{0}/categories/{1}", ApiRoot, categoryIds);
        }

        public static string Update(int categoryId)
        {
            return String.Format("{0}/categories/{1}", ApiRoot, categoryId);
        }

        public static string UpdateCategorySEODetails(int categoryId)
        {
            return String.Format("{0}/categories/updatecategoryseodetails/{1}", ApiRoot, categoryId);
        }

        public static string GetCategoriesByCatalogId(int catalogId)
        {
            return String.Format("{0}/categories/getcategoriesbycatalogid/{1}", ApiRoot, catalogId);
        }

        public static string GetAllCategories()
        {
            return String.Format("{0}/categories/getallcategories", ApiRoot);           
        }

        public static string CheckAssociatedCategory(int categoryId)
        {
            return String.Format("{0}/categories/checkassociatecategory/{1}", ApiRoot, categoryId);
        }

        public static string CategoryAssociatedProducts(int categoryId, string productIds)
        {
            return String.Format("{0}/categories/categoryassociatedproducts/{1}/{2}", ApiRoot, categoryId, productIds);
        }

        public static string GetCategoryAssociatedProducts()
        {
            return String.Format("{0}/getcategoryassociatedproducts", ApiRoot);
        }
    }
}
