﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
     public class ProductSearchSettingEndPoint: BaseEndpoint
    {
         public static string GetProductLevelSettings()
        {
            return String.Format("{0}/getproductlevelsetting", ApiRoot);
        }

        public static string GetCategoryLevelSettings()
        {
            return String.Format("{0}/getcategorylevelsettings", ApiRoot);
        }

        public static string GetFieldLevelSettings()
        {
            return String.Format("{0}/getfieldlevelsettings", ApiRoot);
        }

        public static string SaveBoostValues()
        {
            return String.Format("{0}/saveboostvalues", ApiRoot);
        }
    }
}
