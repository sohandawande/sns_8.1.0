﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
    public class ReorderEndpoint : BaseEndpoint
    {
        public static string GetItems(int orderId)
        {
            return String.Format("{0}/reorder/{1}", ApiRoot, orderId);
        }
        public static string GetReorderLineItem(string orderLineItemIds)
        {
            return String.Format("{0}/reorderorderlineitem/{1}", ApiRoot, orderLineItemIds);
        }

        //Endpoint for Multiparameter reorder function TODO
        public static string GetReorderItemsList(int orderId, int orderLineItemId, bool isOrder)
        {
            return String.Format("{0}/reorder/getreorderlist?orderId={1}&orderLineItemId={2}&isOrder={3}", ApiRoot, orderId, orderLineItemId, isOrder);
        }

    }
}
