﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
	public class PromotionTypesEndpoint : BaseEndpoint
	{
		public static string Create()
		{
			return String.Format("{0}/promotiontypes", ApiRoot);
		}

		public static string Delete(int promotionTypeId)
		{
			return String.Format("{0}/promotiontypes/{1}", ApiRoot, promotionTypeId);
		}

		public static string Get(int promotionTypeId)
		{
			return String.Format("{0}/promotiontypes/{1}", ApiRoot, promotionTypeId);
		}

		public static string List()
		{
			return String.Format("{0}/promotiontypes", ApiRoot);
		}

		public static string Update(int promotionTypeId)
		{
			return String.Format("{0}/promotiontypes/{1}", ApiRoot, promotionTypeId);
		}

        public static string GetAllPromotionTypesNotInDatabase()
		{
            return String.Format("{0}/getallpromotiontypesnotindatabase", ApiRoot);
		}

        public static string GetFranchisePromotionTypes()
        {
            return String.Format("{0}/getfranchisepromotiontypes", ApiRoot);
        }
	}
}
