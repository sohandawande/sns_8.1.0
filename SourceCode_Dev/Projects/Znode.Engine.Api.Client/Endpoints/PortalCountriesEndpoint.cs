﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
	public class PortalCountriesEndpoint : BaseEndpoint
	{
		public static string Create()
		{
			return String.Format("{0}/portalcountries", ApiRoot);
		}

		public static string Delete(int portalCountryId)
		{
			return String.Format("{0}/portalcountries/{1}", ApiRoot, portalCountryId);
		}

		public static string Get(int portalCountryId)
		{
			return String.Format("{0}/portalcountries/{1}", ApiRoot, portalCountryId);
		}

		public static string List()
		{
			return String.Format("{0}/portalcountries", ApiRoot);
		}

		public static string Update(int portalCountryId)
		{
			return String.Format("{0}/portalcountries/{1}", ApiRoot, portalCountryId);
		}

        #region ZNode version 8.0
        public static string GetAllPortalCountries()
        {
            return String.Format("{0}/getallportalcountries", ApiRoot);
        }
        #endregion

        public static string DeleteIfNotAssociated(int portalCountryId)
		{
			return String.Format("{0}/deleteportalcountryifnotassociated/{1}", ApiRoot, portalCountryId);
		}

        public static string CreatePortalCountries(int portalId,string shipableCountryCodes,string billableCountryCodes)
        {
            return String.Format("{0}/createsportalcountries/{1}/{2}/{3}", ApiRoot,portalId,billableCountryCodes,shipableCountryCodes);
        }
        
	}
}
