﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
	public class AttributeTypesEndpoint : BaseEndpoint
	{
		public static string Create()
		{
			return String.Format("{0}/attributetypes", ApiRoot);
		}

		public static string Delete(int attributeTypeId)
		{
			return String.Format("{0}/attributetypes/{1}", ApiRoot, attributeTypeId);
		}

		public static string Get(int attributeTypeId)
		{
			return String.Format("{0}/attributetypes/{1}", ApiRoot, attributeTypeId);
		}

		public static string List()
		{
			return String.Format("{0}/attributetypes", ApiRoot);
		}

		public static string Update(int attributeTypeId)
		{
			return String.Format("{0}/attributetypes/{1}", ApiRoot, attributeTypeId);
		}

        public static string GetAttributesByCatalogID(int catalogId)
        {
            return String.Format("{0}/attributetypesbycatalogid/{1}", ApiRoot, catalogId);
        }
	}
}
