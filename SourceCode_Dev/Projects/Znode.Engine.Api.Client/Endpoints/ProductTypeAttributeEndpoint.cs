﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
    public class ProductTypeAttributeEndpoint: BaseEndpoint
    {
        public static string List()
        {
            return String.Format("{0}/producttypeattribute", ApiRoot);
        }
        
        public static string Create()
        {
            return String.Format("{0}/producttypeattributs", ApiRoot);
        }

        public static string Delete(int productTypeAttributId)
        {
            return String.Format("{0}/producttypeattribute/{1}", ApiRoot, productTypeAttributId);
        }

        public static string Get(int productTypeAttributId)
        {
            return String.Format("{0}/producttypeattributs/{1}", ApiRoot, productTypeAttributId);
        }

        public static string GetAttributeTypesByProductTypeId(int productTypeId)
        {
            return String.Format("{0}/producttypeattribute/getattributetypesbyproducttypeid/{1}", ApiRoot, productTypeId);
        }

        public static string IsProductAssociatedProductType(int productTypeId)
        {
            return String.Format("{0}/producttypeattribute/{1}", ApiRoot, productTypeId);
        }
    }
}
