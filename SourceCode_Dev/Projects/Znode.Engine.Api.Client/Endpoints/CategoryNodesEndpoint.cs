﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
    public class CategoryNodesEndpoint : BaseEndpoint
    {
        public static string Create()
        {
            return String.Format("{0}/categorynodes", ApiRoot);
        }

        public static string Delete(int categoryNodeId)
        {
            return String.Format("{0}/categorynodes/{1}", ApiRoot, categoryNodeId);
        }

        public static string DeleteAssociatedCategoryNode(int categoryNodeId)
        {
            return String.Format("{0}/categorynodes/deleteassociatedcategorynode/{1}", ApiRoot, categoryNodeId);
        }

        public static string Get(int categoryNodeId)
        {
            return String.Format("{0}/categorynodes/{1}", ApiRoot, categoryNodeId);
        }

        public static string List()
        {
            return String.Format("{0}/categorynodes", ApiRoot);
        }

        public static string Update(int categoryNodeId)
        {
            return String.Format("{0}/categorynodes/{1}", ApiRoot, categoryNodeId);
        }

        public static string GetParentCategoryNodes(int catalogId)
        {
            return String.Format("{0}/getparentcategorylist/{1}", ApiRoot, catalogId);
        }

        public static string GetBreadCrumb()
        {
            return String.Format("{0}/breadcrumb", ApiRoot);
        }
    }
}
