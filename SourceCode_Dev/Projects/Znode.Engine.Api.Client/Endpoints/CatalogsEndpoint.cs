﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
	public class CatalogsEndpoint : BaseEndpoint
	{
		public static string Create()
		{
			return String.Format("{0}/catalogs", ApiRoot);
		}

		public static string Delete(int catalogId)
		{
			return String.Format("{0}/catalogs/{1}", ApiRoot, catalogId);
		}

		public static string Get(int catalogId)
		{
			return String.Format("{0}/catalogs/{1}", ApiRoot, catalogId);
		}

		public static string List()
		{
			return String.Format("{0}/catalogs", ApiRoot);
		}

        public static string ListByCatalogIds(string catalogIds)
        {
            return String.Format("{0}/catalogs/{1}", ApiRoot, catalogIds);
        }

		public static string Update(int catalogId)
		{
			return String.Format("{0}/catalogs/{1}", ApiRoot, catalogId);
		}

        public static string CopyCatalog()
        {
            return String.Format("{0}/copycatalog", ApiRoot);
        }

        public static string DeleteCatalog(int catalogId,bool preserveCategories)
        {
            return String.Format("{0}/catalogs/{1}/{2}", ApiRoot,catalogId,preserveCategories);
        }

        public static string GetCatalogsByPortalId(int portalId)
        {
            return String.Format("{0}/getcatalogsbyportalid/{1}", ApiRoot, portalId);
        }
	}
}
