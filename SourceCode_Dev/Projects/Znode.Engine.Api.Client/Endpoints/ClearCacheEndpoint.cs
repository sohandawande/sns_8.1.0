﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
    /// <summary>
    /// This is the endpoint method for clear cache
    /// </summary>
    public class ClearCacheEndpoint : BaseEndpoint
    {
        public static string ClearCache()
        {
            return String.Format("{0}/clearcache/clearapicache", ApiRoot);
        }
    }
}
