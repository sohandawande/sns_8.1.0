﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
    /// <summary>
    /// Endpoint for Content Page.
    /// </summary>
    public class ContentPageEndpoint : BaseEndpoint
    {
        public static string List()
        {
            return String.Format("{0}/contentpages", ApiRoot);
        }

        public static string Create()
        {
            return String.Format("{0}/contentpages", ApiRoot);
        }

        public static string Get(int contentPageId)
        {
            return String.Format("{0}/contentpages/{1}", ApiRoot, contentPageId);
        }

        public static string Update(int contentPageId)
        {
            return String.Format("{0}/contentpages/{1}", ApiRoot, contentPageId);
        }

        public static string CopyContentPage()
        {
            return String.Format("{0}/copycontentpage", ApiRoot);
        }

        public static string AddPage()
        {
            return String.Format("{0}/addpage", ApiRoot);
        }

        public static string Delete(int contentPageId)
        {
            return String.Format("{0}/contentpages/{1}", ApiRoot, contentPageId);
        }

        public static string GetByName(string contentPageName, string extension)
        {
            return String.Format("{0}/contentpage/getcontentpagebyname/{1}/{2}", ApiRoot, contentPageName, extension);
        }

    }
}
