﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
	public class SkusEndpoint : BaseEndpoint
	{
		public static string Create()
		{
			return String.Format("{0}/skus", ApiRoot);
		}

		public static string Delete(int skuId)
		{
			return String.Format("{0}/skus/{1}", ApiRoot, skuId);
		}

		public static string Get(int skuId)
		{
			return String.Format("{0}/skus/{1}", ApiRoot, skuId);
		}

		public static string List()
		{
			return String.Format("{0}/skus", ApiRoot);
		}

		public static string Update(int skuId)
		{
			return String.Format("{0}/skus/{1}", ApiRoot, skuId);
        }

        public static string AddSkuAttribute(int skuId, string attributeIds)
        {
            return String.Format("{0}/skus/addskuattribute/{1}/{2}", ApiRoot, skuId, attributeIds);
        }

        public static string GetSKUListByProductId(int productId)
        {
            return String.Format("{0}/getskulistbyproductid/{1}", ApiRoot, productId);
        }

    }
}
