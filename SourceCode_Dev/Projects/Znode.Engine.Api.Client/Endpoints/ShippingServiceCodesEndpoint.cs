﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
	public class ShippingServiceCodesEndpoint : BaseEndpoint
	{
		public static string Create()
		{
			return String.Format("{0}/shippingservicecodes", ApiRoot);
		}

		public static string Delete(int shippingServiceCodeId)
		{
			return String.Format("{0}/shippingservicecodes/{1}", ApiRoot, shippingServiceCodeId);
		}

		public static string Get(int shippingServiceCodeId)
		{
			return String.Format("{0}/shippingservicecodes/{1}", ApiRoot, shippingServiceCodeId);
		}

		public static string List()
		{
			return String.Format("{0}/shippingservicecodes", ApiRoot);
		}

		public static string Update(int shippingServiceCodeId)
		{
			return String.Format("{0}/shippingservicecodes/{1}", ApiRoot, shippingServiceCodeId);
		}
	}
}
