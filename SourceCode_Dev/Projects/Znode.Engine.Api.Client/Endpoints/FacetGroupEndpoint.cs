﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Client.Endpoints
{
    public class FacetGroupEndpoint : BaseEndpoint
    {
        public static string Get(int facetGroupId)
        {
            return String.Format("{0}/facetgroups/{1}", ApiRoot, facetGroupId);
        }

        public static string List()
        {
            return String.Format("{0}/facetgroups", ApiRoot);
        }

        public static string FacetControlTypeList()
        {
            return String.Format("{0}/facetcontroltypes", ApiRoot);
        }

        public static string Create()
        {
            return String.Format("{0}/facetgroups", ApiRoot);
        }

        public static string Delete(int facetGroupId)
        {
            return String.Format("{0}/facetgroups/{1}", ApiRoot, facetGroupId);
        }

        public static string Update(int facetGroupId)
        {
            return String.Format("{0}/facetgroups/{1}", ApiRoot, facetGroupId);
        }

        public static string InsertFacetGroupCategory()
        {
            return String.Format("{0}/facetgroupcategory", ApiRoot);
        }

        public static string DeleteFacetGroupCategory(int facetGroupId)
        {
            return String.Format("{0}/facetgroupcategory/{1}", ApiRoot, facetGroupId);
        }

        public static string CreateFacet()
        {
            return String.Format("{0}/facet", ApiRoot);
        }

        public static string DeleteFacet(int facetId)
        {
            return String.Format("{0}/facet/{1}", ApiRoot, facetId);
        }

        public static string UpdateFacet(int facetId)
        {
            return String.Format("{0}/facet/{1}", ApiRoot, facetId);
        }

        public static string GetFacet(int facetId)
        {
            return String.Format("{0}/facet/{1}", ApiRoot, facetId);
        }
        public static string FacetList()
        {
            return String.Format("{0}/facet", ApiRoot);
        }
    }
}
