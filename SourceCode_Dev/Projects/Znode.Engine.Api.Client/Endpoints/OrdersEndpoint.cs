﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
    public partial class OrdersEndpoint : BaseEndpoint
    {
        public static string Create()
        {
            return String.Format("{0}/orders", ApiRoot);
        }

        public static string Get(int orderId)
        {
            return String.Format("{0}/orders/{1}", ApiRoot, orderId);
        }

        public static string List()
        {
            return String.Format("{0}/orders", ApiRoot);
        }

        public static string Update(int orderId)
        {
            return String.Format("{0}/orders/{1}", ApiRoot, orderId);
        }

        public static string OrderList()
        {
            return String.Format("{0}/order/getorderlist", ApiRoot);
        }

        public static string OrderDetails()
        {
            return String.Format("{0}/getorderdetails", ApiRoot);
        }

        public static string UpdateOrderStatus(int orderId)
        {
            return String.Format("{0}/updateorderstatus/{1}", ApiRoot, orderId);
        }

        public static string VoidPayment(int orderId)
        {
            return String.Format("{0}/voidpayment/{1}", ApiRoot, orderId);
        }

        public static string RefundPayment(int orderId)
        {
            return String.Format("{0}/refundpayment/{1}", ApiRoot, orderId);
        }

        public static string OrderDownload()
        {
            return String.Format("{0}/downloadorder/", ApiRoot);
        }

        public static string OrderLineItemDownload()
        {
            return String.Format("{0}/downloadorderlineitemdata/", ApiRoot);
        }

        public static string AddNewCustomer()
        {
            return String.Format("{0}/addnewcustomer", ApiRoot);
        }

        public static string UpdateCustomerAddress(int accountId)
        {
            return String.Format("{0}/updatecustomeraddress/{1}", ApiRoot, accountId);
        }

        public static string GetOrderLineItem(int orderLineItemId)
        {
            return String.Format("{0}/getorderlineitems/{1}", ApiRoot, orderLineItemId);
        }

        public static string UpdateOrderLineItemStatus(int orderLineItemId)
        {
            return String.Format("{0}/updateorderlineitemstatus/{1}", ApiRoot, orderLineItemId);
        }

        public static string UpdateOrderPaymentStatus(int orderId, string paymentStatus)
        {
            return String.Format("{0}/updateorderpaymentstatus/{1}/{2}", ApiRoot, orderId, paymentStatus);
        }

        public static string SendEmail()
        {
            return String.Format("{0}/sendemail/", ApiRoot);
        }

    }
}
