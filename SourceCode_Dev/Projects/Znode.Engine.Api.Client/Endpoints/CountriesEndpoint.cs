﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
    public class CountriesEndpoint : BaseEndpoint
    {
        public static string Create()
        {
            return String.Format("{0}/countries", ApiRoot);
        }

        public static string Delete(string countryCode)
        {
            return String.Format("{0}/countries/{1}", ApiRoot, countryCode);
        }

        public static string Get(string countryCode)
        {
            return String.Format("{0}/countries/{1}", ApiRoot, countryCode);
        }

        public static string List()
        {
            return String.Format("{0}/countries", ApiRoot);
        }

        public static string Update(string countryCode)
        {
            return String.Format("{0}/countries/{1}", ApiRoot, countryCode);
        }

        /// <summary>
        /// Znode Version 7.2.2
        /// This function returns list of all active billing and shipping countries on the basis of portal id.
        /// </summary>
        /// <param name="portalId">int portalId</param>
        /// <param name="billingShippingFlag">int billingShippingFlag</param>
        /// <returns>Returns list of all active billing and shipping countries on the basis of portal id.</returns>
        public static string GetActiveCountryByPortalId(int portalId, int billingShippingFlag = 0)
        {
            return String.Format("{0}/countries/getactivecountrybyportalid/{1}/{2}", ApiRoot, portalId, billingShippingFlag);
        }

        /// <summary>
        /// This function Get all countries for portalId.
        /// </summary>
        /// <param name="portalId">int portalId </param>
        /// <returns>returns end point url</returns>
        public static string GetCountryByPortalId(int portalId)
        {
            return String.Format("{0}/countries/getcountrybyportalid/{1}", ApiRoot, portalId);
        }

        public static string GetShippingActiveCountryByPortalId(int portalId)
        {
            return String.Format("{0}/countries/GetShippingActiveCountryByPortalId/{1}", ApiRoot, portalId);
        }
    }
}
