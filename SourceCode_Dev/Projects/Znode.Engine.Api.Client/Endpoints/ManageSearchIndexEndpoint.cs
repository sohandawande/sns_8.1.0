﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Client.Endpoints
{
    public class ManageSearchIndexEndpoint:BaseEndpoint
    {
        public static string List()
        {
            return String.Format("{0}/managesearchindex", ApiRoot);
        }

        public static string Create()
        {
            return String.Format("{0}/managesearchindex", ApiRoot);
        }

        public static string DisableTriggers(int flag)
        {
            return String.Format("{0}/managesearchindex/{1}", ApiRoot, flag);
        }

        public static string DisableWinservice(int flag)
        {
            return String.Format("{0}/managesearchindex/serviceflag/{1}", ApiRoot, flag);
            
        }

        public static string SearchIndexStatus(int indexId)
        {
            return String.Format("{0}/managesearchindex/{1}", ApiRoot, indexId);
        }

    }
}
