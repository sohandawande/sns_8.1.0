﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
    public class ProductTypeEndpoint : BaseEndpoint
    {
        public static string Create()
        {
            return String.Format("{0}/producttypes", ApiRoot);
        }

        public static string Delete(int productTypeId)
        {
            return String.Format("{0}/producttypes/{1}", ApiRoot, productTypeId);
        }

        public static string Get(int productTypeId)
        {
            return String.Format("{0}/producttype/{1}", ApiRoot, productTypeId);
        }

        public static string List()
        {
            return String.Format("{0}/producttype", ApiRoot);
        }

        public static string Update(int productTypeId)
        {
            return String.Format("{0}/producttype/{1}", ApiRoot, productTypeId);
        }

        public static string GetProductAttributesByProductTypeId()
        {
            return String.Format("{0}/getproductattributesbyproducttypeid", ApiRoot);
        }
    }
}
