﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
	public class PromotionsEndpoint : BaseEndpoint
	{
		public static string Create()
		{
			return String.Format("{0}/promotions", ApiRoot);
		}

		public static string Delete(int promotionId)
		{
			return String.Format("{0}/promotions/{1}", ApiRoot, promotionId);
		}

		public static string Get(int promotionId)
		{
			return String.Format("{0}/promotions/{1}", ApiRoot, promotionId);
		}

		public static string List()
		{
			return String.Format("{0}/promotions", ApiRoot);
		}

		public static string Update(int promotionId)
		{
			return String.Format("{0}/promotions/{1}", ApiRoot, promotionId);
		}

        public static string GetProductsList()
        {
            return String.Format("{0}/getproductslist", ApiRoot);
        }
	}
}
