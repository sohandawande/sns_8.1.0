﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
    public class MasterPageEndpoint : BaseEndpoint
    {
        public static string List()
        {
            return String.Format("{0}/masterpages", ApiRoot);
        }

        public static string GetMasterPageByThemeId(int themeId, string pageType)
        {
            return String.Format("{0}/masterpages/getmasterpagebythemeid/{1}/{2}", ApiRoot, themeId, pageType);
        }
    }
}
