﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
    public class VendorsAccountEndpoint : BaseEndpoint
    {
        public static string List()
        {
            return String.Format("{0}/vendoraccounts", ApiRoot);
        }
        public static string Create()
        {
            return String.Format("{0}/vendoraccounts", ApiRoot);
        }
        public static string Update(int accountId)
        {
            return String.Format("{0}/vendoraccount/{1}", ApiRoot, accountId);
        }
        public static string GetVendorAccountById(int accountId)
        {
            return String.Format("{0}/vendoraccount/getvendoraccountbyid/{1}", ApiRoot, accountId);
        }
    }
}
