﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
    public class ProductsEndpoint : BaseEndpoint
    {
        public static string Create()
        {
            return String.Format("{0}/products", ApiRoot);
        }

        public static string Delete(int productId)
        {
            return String.Format("{0}/products/{1}", ApiRoot, productId);
        }

        public static string Get(int productId)
        {
            return String.Format("{0}/products/{1}", ApiRoot, productId);
        }

        public static string GetProductDetailsByProductId(int productId)
        {
            return String.Format("{0}/products/getproductdetailsbyproductid/{1}", ApiRoot, productId);
        }

        public static string GetProductCategoryByProductId()
        {
            return String.Format("{0}/products/getproductcategorybyproductid", ApiRoot);
        }

        public static string GetProductUnAssociatedCategoryByProductId(int productId)
        {
            return String.Format("{0}/products/getproductunassociatedcategorybyproductid/{1}", ApiRoot, productId);
        }

   
        public static string AssociateProductCategory()
        {
            return String.Format("{0}/products/associateproductcategory", ApiRoot);
        }

        public static string UnAssociateProductCategory()
        {
            return String.Format("{0}/products/unassociateproductcategory", ApiRoot);
        }

        public static string AssociateSkuFacets(int? skuId, string associateFacetIds, string unassociateFacetIds)
        {
            return String.Format("{0}/products/associateskufacets/{1}/{2}/{3}", ApiRoot, skuId, associateFacetIds, unassociateFacetIds);
        }

        public static string GetWithSku(int productId, int skuId)
        {
            return String.Format("{0}/products/{1}/{2}", ApiRoot, productId, skuId);
        }

        public static string IsSeoUrlExist(int productId, string seoUrl)
        {
            return String.Format("{0}/products/isseourlexist/{1}/{2}", ApiRoot, productId, seoUrl);
        }

        public static string List()
        {
            return String.Format("{0}/products", ApiRoot);
        }

        public static string ListByCatalog(int catalogId)
        {
            return String.Format("{0}/products/catalog/{1}", ApiRoot, catalogId);
        }

        public static string ListByCatalogIds(string catalogIds)
        {
            return String.Format("{0}/products/catalog/{1}", ApiRoot, catalogIds);
        }

        public static string ListByCategory(int categoryId)
        {
            return String.Format("{0}/products/category/{1}", ApiRoot, categoryId);
        }

        public static string ListByCategoryByPromotionType(int categoryId, int promotionTypeId)
        {
            return String.Format("{0}/products/category/{1}/promotiontype/{2}", ApiRoot, categoryId, promotionTypeId);
        }

        public static string ListByExternalIds(string externalIds)
        {
            return String.Format("{0}/products/externalids/{1}", ApiRoot, externalIds);
        }

        public static string ListByHomeSpecials()
        {
            return String.Format("{0}/products/homespecials", ApiRoot);
        }

        public static string ListByProductIds()
        {
            return String.Format("{0}/products/listbyproductids", ApiRoot);
        }

        public static string ListByPromotionType(int promotionTypeId)
        {
            return String.Format("{0}/products/promotiontype/{1}", ApiRoot, promotionTypeId);
        }

        public static string Update(int productId)
        {
            return String.Format("{0}/products/{1}", ApiRoot, productId);
        }

        public static string UpdateProductSettings(int productId)
        {
            return String.Format("{0}/products/updateproductsettings/{1}", ApiRoot, productId);
        }

        public static string UpdateProductSEOInformation(int productId)
        {
            return String.Format("{0}/products/updateproductseoinformation/{1}", ApiRoot, productId);
        }

        public static string UpdateImage()
        {
            return String.Format("{0}/image", ApiRoot);
        }

        public static string GetProductTags(int productId)
        {
            return String.Format("{0}/producttags/{1}", ApiRoot, productId);
        }

        public static string CreateProductTag()
        {
            return String.Format("{0}/producttags", ApiRoot);
        }
        public static string UpdateProductTag(int tagId)
        {
            return String.Format("{0}/producttags/{1}", ApiRoot, tagId);
        }
        public static string DeleteProductTag(int tagId)
        {
            return String.Format("{0}/producttags/{1}", ApiRoot, tagId);
        }
        public static string GetProductAssociatedFacets(int productId, int facetGroupId)
        {
            return String.Format("{0}/productfacets/{1}/{2}", ApiRoot, productId, facetGroupId);
        }

        public static string GetSkuAssociatedFacets(int productId, int skuId, int facetGroupId)
        {
            return String.Format("{0}/products/getskuassociatedfacets/{1}/{2}/{3}", ApiRoot, productId, skuId, facetGroupId);
        }

        public static string UpdateProductFacets(int productId)
        {
            return String.Format("{0}/productfacets/{1}", ApiRoot, productId);
        }
        public static string DeleteProductAssociatedFacets(int productId, int facetGroupId)
        {
            return String.Format("{0}/productfacets/{1}/{2}", ApiRoot, productId, facetGroupId);
        }
        public static string DeleteSkuAssociatedFacets(int skuId, int facetGroupId)
        {
            return String.Format("{0}/products/deleteskuassociatedfacets/{1}/{2}", ApiRoot, skuId, facetGroupId);
        }

        public static string ProductImageTypeList()
        {
            return String.Format("{0}/productimagetypes", ApiRoot);
        }
        public static string InsertProductAlternateImage()
        {
            return String.Format("{0}/productalternateimage", ApiRoot);
        }
        public static string UpdateProductAlternateImage(int productImageId)
        {
            return String.Format("{0}/productalternateimage/{1}", ApiRoot, productImageId);
        }
        public static string DeleteProductAlternateImage(int productImageId)
        {
            return String.Format("{0}/productalternateimage/{1}", ApiRoot, productImageId);
        }
        public static string ProductAlternateImageList()
        {
            return String.Format("{0}/productalternateimage", ApiRoot);
        }
        public static string GetProductImage(int productImageId)
        {
            return String.Format("{0}/productalternateimage/{1}", ApiRoot, productImageId);
        }
        public static string RemoveProductAddOn(int productAddOnId)
        {
            return String.Format("{0}/RemoveProductAddOn/{1}", ApiRoot, productAddOnId);
        }
        public static string AssociateAddOns()
        {
            return String.Format("{0}/associateaddon/", ApiRoot);
        }
        public static string GetProductAddOns(int productId)
        {
            return String.Format("{0}/productaddons/{1}", ApiRoot, productId);
        }
        public static string GetUnassociatedAddOns(int productId, int portalId)
        {
            return String.Format("{0}/unassociatedaddons/{1}/{2}", ApiRoot, productId, portalId);
        }
        public static string RemoveProductHighlight(int productHighlightId)
        {
            return String.Format("{0}/removeproducthighlight/{1}", ApiRoot, productHighlightId);
        }
        public static string AssociateHighlight()
        {
            return String.Format("{0}/associatehighlight", ApiRoot);
        }
        public static string GetProductHighlights(int productId)
        {
            return String.Format("{0}/producthighlights/{1}", ApiRoot, productId);
        }
        public static string GetUnassociatedHighlights(int productId, int portalId)
        {
            return String.Format("{0}/unassociatedhighlights/{1}/{2}", ApiRoot, productId, portalId);
        }
        public static string ProductBundlesList()
        {
            return String.Format("{0}/productbundles", ApiRoot);
        }
        public static string DeleteBundleProduct(int parentChildProductID)
        {
            return String.Format("{0}/productbundles/{1}", ApiRoot, parentChildProductID);
        }
        public static string GetProductFacetsList()
        {
            return String.Format("{0}/productfacets", ApiRoot);
        }
        public static string GetSkuFacets()
        {
            return String.Format("{0}/products/getskufacets", ApiRoot);
        }
        public static string GetProductList()
        {
            return String.Format("{0}/customproductlist", ApiRoot);
        }

        public static string GetAllProducts()
        {
            return String.Format("{0}/products/getallproducts", ApiRoot);
        }
        public static string GetProductSkuDetails()
        {
            return String.Format("{0}/products/getproductskudetails", ApiRoot);
        }
        public static string CreateProductTier()
        {
            return String.Format("{0}/createproducttier", ApiRoot);
        }
        public static string UpdateProductTier()
        {
            return String.Format("{0}/updateproducttier", ApiRoot);
        }
        public static string DeleteProductTier(int productTierId)
        {
            return String.Format("{0}/deleteproducttier/{1}", ApiRoot, productTierId);
        }
        public static string GetProductTiers(int productId)
        {
            return String.Format("{0}/producttiers/{1}", ApiRoot, productId);
        }
        public static string CreateDigitalAsset()
        {
            return String.Format("{0}/createdigitalasset", ApiRoot);
        }

        public static string AssociateBundleProduct()
        {
            return String.Format("{0}/productbundles/associatebundleproduct", ApiRoot);
        }

        public static string GetDigitalAssets(int productId)
        {
            return String.Format("{0}/digitalassets/{1}", ApiRoot, productId);
        }

        public static string SearchProducts()
        {
            return String.Format("{0}/searchproducts", ApiRoot);
        }

        public static string CreateOrderProduct(int productId)
        {
            return String.Format("{0}/createorderproduct/{1}", ApiRoot, productId);
        }

        public static string CopyProduct(int productId)
        {
            return String.Format("{0}/copyproduct/{1}", ApiRoot, productId);
        }

        public static string DeleteProductByProductId(int productId)
        {
            return String.Format("{0}/products/deletesbyproductid/{1}", ApiRoot, productId);
        }

        public static string DeleteDigitlAsset(int digitalAssetId)
        {
            return String.Format("{0}/deletedigitalasset/{1}", ApiRoot, digitalAssetId);
        }

        public static string GetProductListBySku()
        {
            return String.Format("{0}/products/getskuproductlistbysku", ApiRoot);
        }

        public static string SendMailToFriend()
        {
            return String.Format("{0}/products/sendmail", ApiRoot);
        }

        public static string GetInventoriesFromErp()
        {
            return String.Format("{0}/products/getinventoryfromerp", ApiRoot);
        }
    }
}
