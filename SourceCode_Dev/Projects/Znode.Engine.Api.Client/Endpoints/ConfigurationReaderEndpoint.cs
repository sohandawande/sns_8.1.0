﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
    public class ConfigurationReaderEndpoint : BaseEndpoint
    {
        public static string GetFilterConfigurationXML(string itemName)
        {
            return String.Format("{0}/configurationreader/{1}", ApiRoot, itemName);
        }
    }
}
