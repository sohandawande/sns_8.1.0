﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
    public class LoadPluginsEndpoint : BaseEndpoint
    {
        public static string IsPluginsLoaded()
        {
            return String.Format("{0}/loadplugins", ApiRoot);
        }
    }
}
