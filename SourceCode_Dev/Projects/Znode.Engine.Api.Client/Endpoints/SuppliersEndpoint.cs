﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
	public class SuppliersEndpoint : BaseEndpoint
	{
		public static string Create()
		{
			return String.Format("{0}/suppliers", ApiRoot);
		}

		public static string Delete(int supplierId)
		{
			return String.Format("{0}/suppliers/delete/{1}", ApiRoot, supplierId);
		}

		public static string Get(int supplierId)
		{
			return String.Format("{0}/suppliers/{1}", ApiRoot, supplierId);
		}

		public static string List()
		{
			return String.Format("{0}/suppliers", ApiRoot);
		}

        public static string Update(int supplierId)
		{
            return String.Format("{0}/suppliers/{1}", ApiRoot, supplierId);
		}
	}
}
