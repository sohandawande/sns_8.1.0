﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Client.Endpoints
{
    public class CustomerBasedPricingEndpoint : BaseEndpoint
    {
        public static string GetCustomerPricingId(int productId)
        {
            return String.Format("{0}/products/getCustomerpricing/{1}", ApiRoot, productId);
        }
    }
}
