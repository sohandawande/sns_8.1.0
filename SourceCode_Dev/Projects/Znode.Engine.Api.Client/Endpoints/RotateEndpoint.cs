﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
    public class RotateEndpoint : BaseEndpoint
    {
        public static string GenerateRotateKey()
        {
            return String.Format("{0}/generaterotatekey", ApiRoot);
        }
    }
}
