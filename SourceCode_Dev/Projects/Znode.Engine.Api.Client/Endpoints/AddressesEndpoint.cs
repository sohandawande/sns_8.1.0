﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
	public class AddressesEndpoint : BaseEndpoint
	{
		public static string Create()
		{
			return String.Format("{0}/addresses", ApiRoot);
		}

		public static string Delete(int addressId)
		{
			return String.Format("{0}/addresses/{1}", ApiRoot, addressId);
		}

		public static string Get(int addressId)
		{
			return String.Format("{0}/addresses/{1}", ApiRoot, addressId);
		}

		public static string List()
		{
			return String.Format("{0}/addresses", ApiRoot);
		}

		public static string Update(int addressId)
		{
			return String.Format("{0}/addresses/{1}", ApiRoot, addressId);
		}

        //new method
        public static string UpdateDefaultAddress(int addressId)
        {
            return String.Format("{0}/updatedefaultaddress/{1}", ApiRoot, addressId);
        }
	}
}
