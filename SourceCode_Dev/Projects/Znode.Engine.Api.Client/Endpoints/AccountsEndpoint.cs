﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
	public partial class AccountsEndpoint : BaseEndpoint
	{
		public static string ChangePassword()
		{
			return String.Format("{0}/accounts/changepassword", ApiRoot);
		}

		public static string Create()
		{
			return String.Format("{0}/accounts", ApiRoot);
		}

		public static string Delete(int accountId)
		{
			return String.Format("{0}/accounts/{1}", ApiRoot, accountId);
		}

		public static string Get(int accountId)
		{
			return String.Format("{0}/accounts/{1}", ApiRoot, accountId);
		}

		public static string GetByUserId(string userId)
		{
			return String.Format("{0}/accounts/{1}", ApiRoot, userId);
		}

        public static string GetByUsername()
        {
            return String.Format("{0}/accounts/getbyusername", ApiRoot);
        }

		public static string List()
		{
			return String.Format("{0}/accounts", ApiRoot);
		}

		public static string Login()
		{
			return String.Format("{0}/accounts/login", ApiRoot);
		}

		public static string ResetPassword()
		{
			return String.Format("{0}/accounts/resetpassword", ApiRoot);
		}

		public static string Update(int accountId)
		{
			return String.Format("{0}/accounts/{1}", ApiRoot, accountId);
		}

        public static string CheckResetPasswordLinkStatus()
        {
            return String.Format("{0}/accounts/checkresetpasswordlinkstatus", ApiRoot);
        }

        public static string CheckUserRole()
        {
            return String.Format("{0}/accounts/checkuserrole", ApiRoot);
        }

        public static string IsAddressValid()
        {
            return String.Format("{0}/accounts/isaddressvalid", ApiRoot);
        }

        public static string ResetAdminDetails()
        {
            return String.Format("{0}/accounts/resetadmindetails", ApiRoot);
        }

        public static string CreateAdminAccount()
        {
            return String.Format("{0}/accounts/createadminaccount", ApiRoot);
        }

        public static string GetByRoleName(string roleName)
        {
            return String.Format("{0}/accounts/getaccountdetailsbyrolename/{1}", ApiRoot, roleName);
        }

        public static string EnableDisable(int accountId)
        {
            return String.Format("{0}/accounts/enabledisable/{1}", ApiRoot, accountId);
        }
        public static string UpdateCustomerAccount(int accountId)
        {
            return String.Format("{0}/accounts/updatecustomeraccount/{1}", ApiRoot, accountId);
        }
        public static string CreateCustomerAccount()
        {
            return String.Format("{0}/accounts/createcustomeraccount", ApiRoot);
        }

        public static string SendEmail()
        {
            return String.Format("{0}/accounts/sendemail", ApiRoot);
        }

        public static string GetAccountPaymentList(int accountId)
        {
            return String.Format("{0}/getaccountpaymentlist/{1}", ApiRoot,accountId);
        }

        public static string CreateAccountPayment()
        {
            return String.Format("{0}/createaccountpayment", ApiRoot);
        }
        public static string GetRolePermission()
        {
            return String.Format("{0}/accounts/getrolepermissionbyusername", ApiRoot );
        }
        public static string GetRoleMenuList()
        {
            return String.Format("{0}/accounts/getrolemenulistbyusername", ApiRoot);
        }
        public static string IsRoleExistForProfile(int porfileId, string roleName)
        {
            return String.Format("{0}/isroleexistforprofile/{1}/{2}", ApiRoot, porfileId, roleName);
        }
        public static string SignUpForNewsLetter()
        {
            return String.Format("{0}/accounts/signupfornewsletter", ApiRoot);
        }
        public static string SocialUserLogin()
        {
            return String.Format("{0}/accounts/socialuserlogin", ApiRoot);
        }
        public static string SocialUserCreateOrUpdateAccount()
        {
            return String.Format("{0}/accounts/socialusercreateorupdateaccount", ApiRoot);
        }
        public static string GetRegisteredSocialClientDetailsList()
        {
            return String.Format("{0}/accounts/getregisteredsocialclientdetailslist", ApiRoot);
        }
	}
}
