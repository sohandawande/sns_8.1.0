﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
    public class ProductAttributesEndpoint : BaseEndpoint
    {
        public static string Create()
        {
            return String.Format("{0}/productattributes", ApiRoot);
        }

        public static string Delete(int attributeId)
        {
            return String.Format("{0}/productattributes/{1}", ApiRoot, attributeId);
        }

        public static string Get(int attributeId)
        {
            return String.Format("{0}/productattributes/{1}", ApiRoot, attributeId);
        }

        public static string List()
        {
            return String.Format("{0}/productattributes", ApiRoot);
        }

        public static string Update(int attributeId)
        {
            return String.Format("{0}/productattributes/{1}", ApiRoot, attributeId);
        }

        public static string GetAttributesByAttributeTypeId()
        {
            return String.Format("{0}/productattributesbyattributetypeid", ApiRoot);
        }
    }
}
