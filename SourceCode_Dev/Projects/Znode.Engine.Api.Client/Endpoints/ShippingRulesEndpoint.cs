﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
	public class ShippingRulesEndpoint : BaseEndpoint
	{
		public static string Create()
		{
			return String.Format("{0}/shippingrules", ApiRoot);
		}

		public static string Delete(int shippingRuleId)
		{
			return String.Format("{0}/shippingrules/{1}", ApiRoot, shippingRuleId);
		}

		public static string Get(int shippingRuleId)
		{
			return String.Format("{0}/shippingrules/{1}", ApiRoot, shippingRuleId);
		}

		public static string List()
		{
			return String.Format("{0}/shippingrules", ApiRoot);
		}

		public static string Update(int shippingRuleId)
		{
			return String.Format("{0}/shippingrules/{1}", ApiRoot, shippingRuleId);
		}
	}
}
