﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
    public class RMAConfigurationsEndpoint : BaseEndpoint
    {
        public static string Get(int rmaConfigId)
        {
            return String.Format("{0}/rmaconfigurations/{1}", ApiRoot, rmaConfigId);
        }

        public static string List()
        {
            return String.Format("{0}/rmaconfigurations", ApiRoot);
        }

        public static string Create()
        {
            return String.Format("{0}/rmaconfigurations", ApiRoot);
        }

        public static string Update(int rmaConfigId)
        {
            return String.Format("{0}/rmaconfigurations/{1}", ApiRoot, rmaConfigId);
        }

        public static string GetAllRMAConfigurations()
        {
            return String.Format("{0}/allrmaconfigurations", ApiRoot);
        }

    }
}
