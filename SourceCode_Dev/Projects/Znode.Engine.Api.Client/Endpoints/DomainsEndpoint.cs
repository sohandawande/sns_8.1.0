﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
	public class DomainsEndpoint : BaseEndpoint
	{
		public static string Create()
		{
			return String.Format("{0}/domains", ApiRoot);
		}

		public static string Delete(int domainId)
		{
			return String.Format("{0}/domains/{1}", ApiRoot, domainId);
		}

		public static string Get(int domainId)
		{
			return String.Format("{0}/domains/{1}", ApiRoot, domainId);
		}

		public static string List()
		{
			return String.Format("{0}/domains", ApiRoot);
		}

		public static string Update(int domainId)
		{
			return String.Format("{0}/domains/{1}", ApiRoot, domainId);
		}

        public static string GetUrl(int domainId)
        {
            return String.Format("{0}/domains/url/{1}", ApiRoot, domainId);
        }
	}
}
