﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Client.Endpoints
{
    public class AccountProfileEndpoint : BaseEndpoint
    {

        public static string AccountAssociatedProfiles(int accountId, string profileIds)
        {
            return String.Format("{0}/accountprofile/accountassociatedprofiles/{1}/{2}", ApiRoot, accountId, profileIds);
        }

        public static string Delete(int accountProfileId)
        {
            return String.Format("{0}/accountprofile/{1}", ApiRoot, accountProfileId);
        }
    }
}
