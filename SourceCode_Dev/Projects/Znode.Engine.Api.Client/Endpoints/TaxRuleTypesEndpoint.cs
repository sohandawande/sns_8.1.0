﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
	public class TaxRuleTypesEndpoint : BaseEndpoint
	{
		public static string Create()
		{
			return String.Format("{0}/taxruletypes", ApiRoot);
		}

		public static string Delete(int taxRuleTypeId)
		{
			return String.Format("{0}/taxruletypes/{1}", ApiRoot, taxRuleTypeId);
		}

		public static string Get(int taxRuleTypeId)
		{
			return String.Format("{0}/taxruletypes/{1}", ApiRoot, taxRuleTypeId);
		}

		public static string List()
		{
			return String.Format("{0}/taxruletypes", ApiRoot);
		}

		public static string Update(int taxRuleTypeId)
		{
			return String.Format("{0}/taxruletypes/{1}", ApiRoot, taxRuleTypeId);
		}

        public static string GetAllTaxRuleTypesNotInDatabase()
		{
            return String.Format("{0}/getalltaxruletypesnotindatabase", ApiRoot);
		}
	}
}
