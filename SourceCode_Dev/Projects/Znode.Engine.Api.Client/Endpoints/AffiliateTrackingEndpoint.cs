﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
    public class AffiliateTrackingEndpoint : BaseEndpoint
    {
        public static string GetTrackingData()
        {
            return String.Format("{0}/affiliatetracking/", ApiRoot);
        }
    }
}
