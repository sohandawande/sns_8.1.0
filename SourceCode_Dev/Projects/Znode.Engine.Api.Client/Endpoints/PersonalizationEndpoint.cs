﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
    public class PersonalizationEndpoint : BaseEndpoint
    {
        public static string GetFrequentlyBoughtProductsList()
        {
            return String.Format("{0}/frequentlyboughtproducts", ApiRoot);
        }

        public static string ListByProductId()
        {
            return String.Format("{0}/crosssells", ApiRoot);
        }

        public static string CreateCrossSellProduct()
        {
            return String.Format("{0}/crosssellproducts", ApiRoot);
        }

        public static string DeleteCrossSellProduct(int productId, int relationTypeId)
        {
            return String.Format("{0}/crosssellproducts/{1}/{2}", ApiRoot, productId, relationTypeId);
        }
    }
}
