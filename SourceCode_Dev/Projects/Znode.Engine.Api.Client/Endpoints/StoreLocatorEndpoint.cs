﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Client.Endpoints
{
    public class StoreLocatorEndpoint : BaseEndpoint
    {
        public static string Create()
        {
            return String.Format("{0}/storelocator", ApiRoot);
        }

        public static string List()
        {
            return String.Format("{0}/storelocators", ApiRoot);
        }

        public static string Get(int storeId)
        {
            return String.Format("{0}/storelocators/{1}", ApiRoot, storeId);
        }

        public static string Update(int StoreId)
        {
            return String.Format("{0}/storelocator/{1}", ApiRoot, StoreId);
        }


        public static string Delete(int storeId)
        {
            return String.Format("{0}/storelocator/{1}", ApiRoot, storeId);
        }

        public static string GetStoresList()
        {
            return String.Format("{0}/storelocators", ApiRoot);
        }

    }
}
