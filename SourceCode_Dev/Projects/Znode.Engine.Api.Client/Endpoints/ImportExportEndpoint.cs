﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Client.Endpoints
{
    public class ImportExportEndpoint : BaseEndpoint
    {
        public static string Exports()
        {
            return String.Format("{0}/exports", ApiRoot);
        }
        public static string Imports()
        {
            return String.Format("{0}/imports", ApiRoot);
        }
        public static string GetImportDetails(string type)
        {
            return String.Format("{0}/imports/{1}", ApiRoot, type);
        }
    }
}
