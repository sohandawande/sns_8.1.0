﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
    public class PortalsEndpoint : BaseEndpoint
    {
        public static string Create()
        {
            return String.Format("{0}/portals", ApiRoot);
        }

        public static string Delete(int portalId)
        {
            return String.Format("{0}/portals/{1}", ApiRoot, portalId);
        }

        public static string Get(int portalId)
        {
            return String.Format("{0}/portals/{1}", ApiRoot, portalId);
        }

        public static string List()
        {
            return String.Format("{0}/portals", ApiRoot);
        }

        public static string ListByPortalIds(string portalIds)
        {
            return String.Format("{0}/portals/{1}", ApiRoot, portalIds);
        }


        public static string Update(int portalId)
        {
            return String.Format("{0}/portals/{1}", ApiRoot, portalId);
        }

        public static string GetFedexKeys()
        {
            return String.Format("{0}/getfedexkey", ApiRoot);
        }

        public static string CreateMessage(int portalId, int localeId)
        {
            return String.Format("{0}/createmessages/{1}/{2}", ApiRoot, portalId, localeId);
        }

        public static string CopyStore(int portalId)
        {
            return String.Format("{0}/copystore/{1}", ApiRoot, portalId);
        }

        public static string DeletePortalByPortalId(int portalId)
        {
            return String.Format("{0}/deleteportalbyportalid/{1}", ApiRoot, portalId);
        }

        public static string GetPortalInformationByPortalId(int portalId)
        {
            return String.Format("{0}/portalinformationbyportalid/{1}", ApiRoot, portalId);
        }

        public static string GetPortalListByProfileAccess()
        {
            return String.Format("{0}/portalsbyprofileaccess", ApiRoot);
        }
    }
}
