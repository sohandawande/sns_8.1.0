﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Client.Endpoints
{
    public class ThemeEndPoint:BaseEndpoint
    {
        public static string List()
        {
            return String.Format("{0}/themes", ApiRoot);
        }

        public static string Create()
        {
            return String.Format("{0}/themes", ApiRoot);
        }

        public static string Get(int themeId)
        {
            return String.Format("{0}/themes/{1}", ApiRoot, themeId);
        }

        public static string Update(int themeId)
        {
            return String.Format("{0}/themes/{1}", ApiRoot, themeId);
        }

        public static string Delete(int themeId)
        {
            return String.Format("{0}/themes/{1}", ApiRoot, themeId);
        }
    }
}
