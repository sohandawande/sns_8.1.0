﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Client.Endpoints
{
    public class ReportsEndpoint : BaseEndpoint
    {
        //GetReportDataSet
        public static string Reports(string reportName)
        {
            return String.Format("{0}/myreports/{1}", ApiRoot, reportName);
        }

        public static string GetOrderDetails(int orderId, string reportName)
        {
            return String.Format("{0}/myreports/{1}/{2}", ApiRoot, orderId, reportName);
        }
    }
}
