﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
	public class ShippingTypesEndpoint : BaseEndpoint
	{
		public static string Create()
		{
			return String.Format("{0}/shippingtypes", ApiRoot);
		}

		public static string Delete(int shippingTypeId)
		{
			return String.Format("{0}/shippingtypes/{1}", ApiRoot, shippingTypeId);
		}

		public static string Get(int shippingTypeId)
		{
			return String.Format("{0}/shippingtypes/{1}", ApiRoot, shippingTypeId);
		}

		public static string List()
		{
			return String.Format("{0}/shippingtypes", ApiRoot);
		}

		public static string Update(int shippingTypeId)
		{
			return String.Format("{0}/shippingtypes/{1}", ApiRoot, shippingTypeId);
		}

        public static string GetAllShippingTypesNotInDatabase()
        {
            return String.Format("{0}/getallshippingtypesnotindatabase", ApiRoot);
        }
	}
}
