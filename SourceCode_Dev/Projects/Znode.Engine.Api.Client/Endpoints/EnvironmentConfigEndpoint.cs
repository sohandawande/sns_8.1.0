﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
    public class EnvironmentConfigEndpoint:BaseEndpoint
    {
        public static string GetEnvironmentConfig()
        {
            return String.Format("{0}/environmentconfig", ApiRoot);
        }
    }
}
