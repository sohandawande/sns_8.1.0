﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
    /// <summary>
    /// AddOn Value End Point
    /// </summary>
    class AddOnValueEndpoint : BaseEndpoint
    {
     	public static string Create()
		{
			return String.Format("{0}/addonValues", ApiRoot);
		}

        public static string Delete(int addOnValueId)
		{
            return String.Format("{0}/addonValues/{1}", ApiRoot, addOnValueId);
		}

        public static string Get(int addOnValueId)
		{
            return String.Format("{0}/addonValues/{1}", ApiRoot, addOnValueId);
		}

		public static string List()
		{
            return String.Format("{0}/addonValues", ApiRoot);
		}

        public static string Update(int addOnValueId)
		{
            return String.Format("{0}/addonValues/{1}", ApiRoot, addOnValueId);
		}
	}
}
