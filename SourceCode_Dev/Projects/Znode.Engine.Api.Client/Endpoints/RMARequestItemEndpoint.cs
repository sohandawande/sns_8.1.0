﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Client.Endpoints
{
    public class RMARequestItemEndpoint : BaseEndpoint
    {
        public static string List()
        {
            return String.Format("{0}/rmarequestitems", ApiRoot);
        }
        public static string ListForGiftCard(string orderLineItems)
        {
            return String.Format("{0}/rmarequestitem/{1}", ApiRoot, orderLineItems);
        }
        public static string Create()
        {
            return String.Format("{0}/rmarequestitem", ApiRoot);
        }        
    }
}
