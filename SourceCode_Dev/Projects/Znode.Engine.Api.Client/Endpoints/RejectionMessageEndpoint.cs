﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
    public class RejectionMessageEndpoint : BaseEndpoint
    {
        public static string List()
        {
            return String.Format("{0}/rejectionmessage", ApiRoot);
        }

        public static string Create()
        {
            return String.Format("{0}/rejectionmessage", ApiRoot);
        }

        public static string Get(int rejectionMessageId)
        {
            return String.Format("{0}/rejectionmessage/{1}", ApiRoot, rejectionMessageId);
        }

        public static string Update(int rejectionMessageId)
        {
            return String.Format("{0}/rejectionmessage/{1}", ApiRoot, rejectionMessageId);
        }

        public static string Delete(int rejectionMessageId)
        {
            return String.Format("{0}/rejectionmessage/{1}", ApiRoot, rejectionMessageId);
        }
    }
}
