﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Client.Endpoints
{
    public class ApplicationSettingEndpoint : BaseEndpoint
    {
        public static string Create()
        {
            return String.Format("{0}/applicationsetting", ApiRoot);
        }

        public static string Delete(int addOnId)
        {
            return String.Format("{0}/addons/{1}", ApiRoot, addOnId);
        }

        public static string ColumnList(string entityType, string entityName)
        {
            return String.Format("{0}/applicationsetting/{1}/{2}", ApiRoot, entityType, entityName);
        }

        public static string List()
        {
            return String.Format("{0}/applicationsetting", ApiRoot);
        }

        public static string Update(int addOnId)
        {
            return String.Format("{0}/addons/{1}", ApiRoot, addOnId);
        }
    }
}
