﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Client.Endpoints
{
    public class UrlRedirectEndPoint : BaseEndpoint
    {
        public static string Create()
        {
            return String.Format("{0}/urlredirects", ApiRoot);
        }

        public static string Delete(int urlRedirectId)
        {
            return String.Format("{0}/urlredirects/{1}", ApiRoot, urlRedirectId);
        }

        public static string Get(int urlRedirectId)
        {
            return String.Format("{0}/urlredirects/{1}", ApiRoot, urlRedirectId);
        }

        public static string List()
        {
            return String.Format("{0}/urlredirects", ApiRoot);
        }

        public static string Update(int urlRedirectId)
        {
            return String.Format("{0}/urlredirects/{1}", ApiRoot, urlRedirectId);
        }
    }
}
