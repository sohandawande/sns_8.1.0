﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Client.Endpoints
{
    public class CustomerUserMappingEndPoint : BaseEndpoint
    {
        public static string Delete(int customerUserMappingId)
        {
            return String.Format("{0}/customerusermapping/{1}", ApiRoot, customerUserMappingId);
        }

        public static string UserAssociatedCustomers(int accountId, string customerAccountIds)
        {
            return String.Format("{0}/CustomerUserMapping/InsertUserAssociatedCustomer/{1}/{2}", ApiRoot, accountId, customerAccountIds);
        }

        public static string DeleteAllUserMapping(int userAccountId)
        {
            return String.Format("{0}/CustomerUserMapping/DeleteAllUserMapping/{1}", ApiRoot, userAccountId);
        }
        
    }
}
