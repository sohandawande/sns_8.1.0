﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Client.Endpoints
{
    public class FranchiseAccountEndpoint : BaseEndpoint
    {
        public static string Create()
        {
            return String.Format("{0}/franchiseadmin", ApiRoot);
        }
    }
}
