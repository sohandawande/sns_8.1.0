﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
	public class SupplierTypesEndpoint : BaseEndpoint
	{
		public static string Create()
		{
			return String.Format("{0}/suppliertypes", ApiRoot);
		}

		public static string Delete(int supplierTypeId)
		{
			return String.Format("{0}/suppliertypes/{1}", ApiRoot, supplierTypeId);
		}

		public static string Get(int supplierTypeId)
		{
			return String.Format("{0}/suppliertypes/{1}", ApiRoot, supplierTypeId);
		}

		public static string List()
		{
			return String.Format("{0}/suppliertypes", ApiRoot);
		}

		public static string Update(int supplierTypeId)
		{
			return String.Format("{0}/suppliertypes/{1}", ApiRoot, supplierTypeId);
		}

        public static string GetAllSupplierTypesNotInDatabase()
		{
            return String.Format("{0}/getallsuppliertypesnotindatabase", ApiRoot);
		}
       
	}
}
