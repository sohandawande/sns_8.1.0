﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
    /// <summary>
    /// This class is used to specify the endpoints for the Roles and Permission
    /// </summary>
    public class PermissionsEndPoint : BaseEndpoint
    {
        public static string GetRolesAndPermissions(int accountId)
        {
            return String.Format("{0}/permissions/getrolesandperminssions/{1}", ApiRoot, accountId);
        }

        public static string UpdateRolesAndPermissions()
        {
            return String.Format("{0}/permissions/updaterolesandpermissions", ApiRoot);
        }
        public static string GetProfileStoreAccess()
        {
            return String.Format("{0}/profilecommon/getprofilestoreaccess", ApiRoot);
        }
    }
}
