﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
	public class TaxClassesEndpoint : BaseEndpoint
	{
		public static string Create()
		{
			return String.Format("{0}/taxclasses", ApiRoot);
		}

		public static string Delete(int taxClassId)
		{
			return String.Format("{0}/taxclasses/{1}", ApiRoot, taxClassId);
		}

		public static string Get(int taxClassId)
		{
			return String.Format("{0}/taxclasses/{1}", ApiRoot, taxClassId);
		}

		public static string List()
		{
			return String.Format("{0}/taxclasses", ApiRoot);
		}

        public static string TaxClassList()
        {
            return String.Format("{0}/taxclasslist", ApiRoot);
        }
        
		public static string Update(int taxClassId)
		{
			return String.Format("{0}/taxclasses/{1}", ApiRoot, taxClassId);
		}

        /// <summary>
        /// Znode Version 8.0
        /// This endpoint used to  Get all active countries for portalId.
        /// </summary>
        /// <param name="portalId"></param>
        /// <returns></returns>
        public static string GetActiveCountryByPortalId(int portalId)
        {
            return String.Format("{0}/taxclasses/getactivetaxclassbyportalid/{1}", ApiRoot, portalId);
        }

        public static string GetTaxClassDetails()
        {
            return String.Format("{0}/taxclassdetails", ApiRoot);
        }
	}
}
