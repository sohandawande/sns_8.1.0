﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Client.Endpoints
{
    public class PortalProfileEndpoint : BaseEndpoint
    {
        public static string Create()
        {
            return String.Format("{0}/portalprofile", ApiRoot);
        }

        public static string Delete(int portalProfileId)
        {
            return String.Format("{0}/portalprofile/{1}", ApiRoot, portalProfileId);
        }

        public static string Get(int portalProfileId)
        {
            return String.Format("{0}/portalprofiles/{1}", ApiRoot, portalProfileId);
        }

        public static string List()
        {
            return String.Format("{0}/portalprofiles", ApiRoot);
        }

        public static string Update(int portalProfileId)
        {
            return String.Format("{0}/portalprofiles/{0}", ApiRoot, portalProfileId);
        }

        public static string GetportalProfileDetails()
        {
            return String.Format("{0}/portalprofilesdetails", ApiRoot);
        }

        public static string GetPortalProfilesByPortalId(int portalId)
        {
            return String.Format("{0}/getportalprofilesbyportalid/{1}", ApiRoot, portalId);
        }

    }
}
