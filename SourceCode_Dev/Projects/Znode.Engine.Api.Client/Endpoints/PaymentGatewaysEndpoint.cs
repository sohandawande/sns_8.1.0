﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
	public class PaymentGatewaysEndpoint : BaseEndpoint
	{
		public static string Create()
		{
			return String.Format("{0}/paymentgateways", ApiRoot);
		}

		public static string Delete(int paymentGatewayId)
		{
			return String.Format("{0}/paymentgateways/{1}", ApiRoot, paymentGatewayId);
		}

		public static string Get(int paymentGatewayId)
		{
			return String.Format("{0}/paymentgateways/{1}", ApiRoot, paymentGatewayId);
		}

		public static string List()
		{
			return String.Format("{0}/paymentgateways", ApiRoot);
		}

		public static string Update(int paymentGatewayId)
		{
			return String.Format("{0}/paymentgateways/{1}", ApiRoot, paymentGatewayId);
		}
	}
}
