﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
	public class PaymentOptionsEndpoint : BaseEndpoint
	{
		public static string Create()
		{
			return String.Format("{0}/paymentoptions", ApiRoot);
		}

		public static string Delete(int paymentOptionId)
		{
			return String.Format("{0}/paymentoptions/{1}", ApiRoot, paymentOptionId);
		}

		public static string Get(int paymentOptionId)
		{
			return String.Format("{0}/paymentoptions/{1}", ApiRoot, paymentOptionId);
		}

		public static string List()
		{
			return String.Format("{0}/paymentoptions", ApiRoot);
		}

		public static string Update(int paymentOptionId)
		{
			return String.Format("{0}/paymentoptions/{1}", ApiRoot, paymentOptionId);
		}
	}
}
