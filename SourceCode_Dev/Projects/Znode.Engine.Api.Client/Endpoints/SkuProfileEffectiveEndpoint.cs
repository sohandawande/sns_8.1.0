﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
    /// <summary>
    /// Endpoint of the Sku Profile Effective.
    /// </summary>
    public class SkuProfileEffectiveEndpoint : BaseEndpoint
    {
        public static string Create()
        {
            return String.Format("{0}/skuprofileeffective", ApiRoot);
        }

        public static string Delete(int skuProfileEffectiveId)
        {
            return String.Format("{0}/skuprofileeffective/{1}", ApiRoot, skuProfileEffectiveId);
        }

        public static string Get(int skuProfileEffectiveId)
        {
            return String.Format("{0}/skuprofileeffective/{1}", ApiRoot, skuProfileEffectiveId);
        }

        public static string List()
        {
            return String.Format("{0}/skuprofileeffective", ApiRoot);
        }

        public static string GetSkuProfileEffectiveBySkuId(int skuId)
        {
            return String.Format("{0}/skuprofileeffective/getskuprofileeffectivebyskuid/{1}", ApiRoot, skuId);
        }

        public static string Update(int skuProfileEffectiveId)
        {
            return String.Format("{0}/skuprofileeffective/{1}", ApiRoot, skuProfileEffectiveId);
        }
    }
}
