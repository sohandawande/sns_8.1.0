﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Client.Endpoints
{
    public partial class OrdersEndpoint
    {
        public static string GetOrderReceiptJavascript()
        {
            return String.Format("{0}/orders/getorderreceiptjavascript", ApiRoot);
        }

        public static string InvoiceDetails(string externalId)
        {
            return String.Format("{0}/orders/getinvoicedetails/{1}", ApiRoot, externalId);
        }
        public static string ReSubmitOrderToERP(int orderId)
        {
            return String.Format("{0}/orders/resubmitordertoerp/{1}", ApiRoot, orderId);
        }
    }
}
