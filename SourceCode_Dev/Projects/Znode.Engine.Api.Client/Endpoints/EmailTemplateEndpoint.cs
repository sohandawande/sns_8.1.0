﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
    public class EmailTemplateEndpoint : BaseEndpoint
    {
        public static string List()
        {
            return String.Format("{0}/emailtemplates", ApiRoot);
        }

        public static string Create()
        {
            return String.Format("{0}/emailtemplates", ApiRoot);
        }

        public static string Get(string templateName , string extension)
        {
            return String.Format("{0}/emailtemplates/{1}/{2}", ApiRoot, templateName, extension);
        }

        public static string Update()
        {
            return String.Format("{0}/updateemailtemplates", ApiRoot);
        }

        public static string Delete(string templateName,string extension)
        {
            return String.Format("{0}/emailtemplate/{1}/{2}", ApiRoot, templateName, extension);
        }

        public static string GetKeys()
        {
            return String.Format("{0}/deletedemailtemplates", ApiRoot);
        }

        public static string GetHtmlContent()
        {
            return String.Format("{0}/emailhtmlcontent", ApiRoot);
        }
    }
}
