﻿using System;
using System.Configuration;

namespace Znode.Engine.Api.Client.Endpoints
{
	public abstract class BaseEndpoint
	{
		public static string ApiRoot = ConfigurationManager.AppSettings["ZnodeApiRootUri"];

		public static string ListByQuery(string query)
		{
			return String.Format("{0}/{1}", ApiRoot, query);
		}
	}
}
