﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
	public class SearchEndpoint : BaseEndpoint
	{
		public static string Keyword()
		{
			return String.Format("{0}/search/keyword", ApiRoot);
		}

		public static string Suggested()
		{
			return String.Format("{0}/search/suggested", ApiRoot);
		}
        public static string SeoUrl(string seoUrl)
        {
            return String.Format("{0}/search/seourl/{1}", ApiRoot, seoUrl);
        }
        public static string IsRestrictedSeoUrl(string seoUrl)
        {
            return String.Format("{0}/search/isrestrictedseourl/{1}", ApiRoot, seoUrl);
        }
	}
}
