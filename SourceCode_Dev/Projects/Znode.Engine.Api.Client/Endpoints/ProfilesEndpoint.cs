﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
    public class ProfilesEndpoint : BaseEndpoint
    {
        public static string Get(int profileId)
        {
            return String.Format("{0}/profiles/{1}", ApiRoot, profileId);
        }
        public static string Get()
        {
            return String.Format("{0}/profile/getprofilelist", ApiRoot);
        }
        public static string List()
        {
            return String.Format("{0}/profiles", ApiRoot);
        }

        public static string Create()
        {
            return String.Format("{0}/profiles", ApiRoot);
        }

        public static string Delete(int profileId)
        {
            return String.Format("{0}/profiles/{1}", ApiRoot, profileId);
        }

        public static string Update(int profileId)
        {
            return String.Format("{0}/profiles/{1}", ApiRoot, profileId);
        }

        public static string ProfileListByPortalId(int portalId)
        {
            return String.Format("{0}/getprofilelistbyid/{1}", ApiRoot, portalId);
        }

        public static string AccountProfileList(int accountId)
        {
            return String.Format("{0}/customerprofiles/{1}", ApiRoot, accountId);
        }

        public static string CustomerNotAssociatedProfileList()
        {
            return String.Format("{0}/customernotassociatedprofiles", ApiRoot);
        }

        public static string GetProfileListByPortalId(int portalId)
        {
            return String.Format("{0}/getprofilelistbyportalid/{1}", ApiRoot, portalId);
        }

        public static string GetAvailableProfilesBySkuId()
        {
            return String.Format("{0}/GetAvailableProfileListBySkuIdCategoryId", ApiRoot);
        }
        public static string GetZnodeProfile()
        {
            return String.Format("{0}/profiles/getznodeprofile", ApiRoot);
        }

        public static string GetProfilesAssociatedWithUsers()
        {
            return String.Format("{0}/getprofilesassociatedwithusers", ApiRoot);
        }
    }
}
