﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
    public class CategoryProfileEndpoint : BaseEndpoint
    {
        public static string GetCategoryProfileById(int categoryProfileId)
        {
            return String.Format("{0}/categoryprofile/getcategoryprofilesbycategoryprofileid/{1}", ApiRoot, categoryProfileId);
        }

        public static string GetCategoryProfileByCategoryId(int categoryId)
        {
            return String.Format("{0}/categoryprofile/getcategoryprofilesbycategoryid/{1}", ApiRoot, categoryId);
        }

        public static string Create()
        {
            return String.Format("{0}/categoryprofile", ApiRoot);
        }

        public static string Update(int categoryProfileId)
        {
            return String.Format("{0}/categoryprofile/{1}", ApiRoot, categoryProfileId);
        }

        public static string Delete(int categoryProfileId)
        {
            return String.Format("{0}/categoryprofile/{1}", ApiRoot, categoryProfileId);
        }
    }
}
