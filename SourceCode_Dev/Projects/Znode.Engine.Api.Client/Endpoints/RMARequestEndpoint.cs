﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Client.Endpoints
{
    public class RMARequestEndpoint : BaseEndpoint
    {
        public static string List()
        {
            return String.Format("{0}/rmarequests", ApiRoot);
        }

        public static string Update(int rmaRequestId)
        {
            return String.Format("{0}/rmarequestupdate/{1}", ApiRoot, rmaRequestId);
        }
        public static string Get(int rmaRequestId)
        {
            return String.Format("{0}/rmarequest/{1}", ApiRoot, rmaRequestId);
        }
        public static string GetRMAGiftCardDetails(int rmaRequestId)
        {
            return String.Format("{0}/rmarequestgiftcarddetails/{1}", ApiRoot, rmaRequestId);
        }
        public static string Delete(int rmaRequestId)
        {
            return String.Format("{0}/deletermarequest/{1}", ApiRoot, rmaRequestId);
        }
        public static string Create()
        {
            return String.Format("{0}/creatermarequest", ApiRoot);
        }
        public static string GetOrderRMAFlag(int orderId)
        {
            return String.Format("{0}/getorderrmaflag/{1}", ApiRoot, orderId);
        }
        public static string SendRMAStatusMail(int rmaRequestId)
        {
            return String.Format("{0}/sendstatusmail/{1}", ApiRoot, rmaRequestId);
        }

        public static string SendGiftCardMail()
        {
            return String.Format("{0}/sendgiftcardmail",ApiRoot);
        }
    }
}
