﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
    public class RequestStatusEndPoint : BaseEndpoint
    {
        public static string Get(int requestStatusId)
        {
            return String.Format("{0}/requeststatus/{1}", ApiRoot, requestStatusId);
        }

        public static string List()
        {
            return String.Format("{0}/requeststatuslist", ApiRoot);
        }

        public static string Create()
        {
            return String.Format("{0}/requeststatus", ApiRoot);
        }

        public static string Delete(int requestStatusId)
        {
            return String.Format("{0}/requeststatus/{1}", ApiRoot, requestStatusId);
        }

        public static string Update(int requestStatusId)
        {
            return String.Format("{0}/requeststatus/{1}", ApiRoot, requestStatusId);
        }
    }
}
