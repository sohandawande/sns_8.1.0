﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
	public class AuditsEndpoint : BaseEndpoint
	{
		public static string Delete(int auditId)
		{
			return String.Format("{0}/audits/{1}", ApiRoot, auditId);
		}

		public static string Get(int auditId)
		{
			return String.Format("{0}/audits/{1}", ApiRoot, auditId);
		}

		public static string List()
		{
			return String.Format("{0}/audits", ApiRoot);
		}

		public static string Update(int auditId)
		{
			return String.Format("{0}/audits/{1}", ApiRoot, auditId);
		}
	}
}
