﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
	public class CaseRequestsEndpoint : BaseEndpoint
	{
		public static string Create()
		{
			return String.Format("{0}/caserequests", ApiRoot);
		}

		public static string Delete(int caseRequestId)
		{
			return String.Format("{0}/caserequests/{1}", ApiRoot, caseRequestId);
		}

		public static string Get(int caseRequestId)
		{
			return String.Format("{0}/caserequests/{1}", ApiRoot, caseRequestId);
		}       


		public static string List()
		{
			return String.Format("{0}/caserequests", ApiRoot);
		}

		public static string Update(int caseRequestId)
		{
			return String.Format("{0}/caserequests/{1}", ApiRoot, caseRequestId);
		}

        public static string CreateCaseNote()
        {
            return String.Format("{0}/caserequests/CreateCaseNote", ApiRoot);
        }

        public static string CaseStatusList()
        {
            return String.Format("{0}/getcasestatusrequests", ApiRoot);
        }

        public static string CasePriorityList()
        {
            return String.Format("{0}/getcaseprioritylist", ApiRoot);
        }

        public static string CaseNoteList()
        {
            return String.Format("{0}/getcasenotelist", ApiRoot);
        }
	}
}
