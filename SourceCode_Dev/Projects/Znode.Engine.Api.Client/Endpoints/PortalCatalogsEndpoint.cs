﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
	public class PortalCatalogsEndpoint : BaseEndpoint
	{
		public static string Create()
		{
			return String.Format("{0}/portalcatalogs", ApiRoot);
		}

		public static string Delete(int portalCatalogId)
		{
			return String.Format("{0}/portalcatalogs/{1}", ApiRoot, portalCatalogId);
		}

		public static string Get(int portalCatalogId)
		{
			return String.Format("{0}/portalcatalogs/{1}", ApiRoot, portalCatalogId);
		}

		public static string List()
		{
			return String.Format("{0}/portalcatalogs", ApiRoot);
		}

		public static string Update(int portalCatalogId)
		{
			return String.Format("{0}/portalcatalogs/{1}", ApiRoot, portalCatalogId);
		}

        public static string GetPortalCatalogsByPortalId(int portalId)
		{
            return String.Format("{0}/portalcatalogsbyportalid/{1}", ApiRoot, portalId);
		}
	}
}
