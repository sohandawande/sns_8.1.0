﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Client.Endpoints
{
    public class PRFTERPServicesEndpoint:BaseEndpoint
    {
        public static string GetERPItemDetails(string productNum, string associatedCustomerExternalId, string customerType)
        {
            return String.Format("{0}/geterpitemdetails/{1}/{2}/{3}", ApiRoot, productNum, associatedCustomerExternalId, customerType);
        }

        public static string GetInventoryDetails(string productNum)
        {
            return String.Format("{0}/getinventorydetails/{1}", ApiRoot, productNum);
        }
    }
}
