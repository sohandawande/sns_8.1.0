﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
    /// <summary>
    /// Endpoint for Content Page Revision.
    /// </summary>
    public class ContentPageRevisionEndpoint : BaseEndpoint
    {
        public static string List()
        {
            return String.Format("{0}/contentpagerevisions", ApiRoot);
        }

        public static string RevertRevision(int revisionId)
        {
            return String.Format("{0}/contentpagerevisions/{1}", ApiRoot, revisionId);
        }

        public static string GetContentPageRevisionsById(int contentPageId)
        {
            return String.Format("{0}/contentpagerevisions/{1}", ApiRoot, contentPageId);
        }

        public static string List(int contentPageId)
        {
            return String.Format("{0}/contentpagerevisions/getcontentpagerevisionbyid/{1}", ApiRoot, contentPageId);
        }

    }
}
