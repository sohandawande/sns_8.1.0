﻿using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    public class PersonalizationClient : BaseClient, IPersonalizationClient
    {
        #region Public Methods
        public CrossSellListModel GetFrequentlyBoughtProducts(ExpandCollection expands, FilterCollection filters, SortCollection sorts)
        {
            return GetFrequentlyBoughtProducts(expands, filters, sorts, null, null);
        }

        public CrossSellListModel GetFrequentlyBoughtProducts(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = PersonalizationEndpoint.GetFrequentlyBoughtProductsList();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<CrossSellListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new CrossSellListModel { CrossSellProducts = (Equals(response, null)) ? null : response.ProductCrossSells };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public CrossSellListModel GetProductCrossSellByProductId(FilterCollection filters, SortCollection sorts)
        {
            string endpoint = PersonalizationEndpoint.ListByProductId();
            endpoint += BuildEndpointQueryString(null, filters, sorts, null, null);

            ApiStatus status = new ApiStatus();
            CrossSellListResponse response = GetResourceFromEndpoint<CrossSellListResponse>(endpoint, status);

            Collection<HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            CrossSellListModel list = new CrossSellListModel { CrossSellProducts = (Equals(response, null)) ? null : response.ProductCrossSells };
            list.MapPagingDataFromResponse(response);
            return list;
        }

        public bool CreateCrossSellProducts(CrossSellListModel model)
        {
            string endpoint = PersonalizationEndpoint.CreateCrossSellProduct();

            ApiStatus status = new ApiStatus();
            TrueFalseResponse response = PostResourceToEndpoint<TrueFalseResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return response.booleanModel.disabled;
        }

        public bool DeleteCrossSellProduct(int productId, int relationTypeId)
        {
            string endpoint = PersonalizationEndpoint.DeleteCrossSellProduct(productId, relationTypeId);

            ApiStatus status = new ApiStatus();
            bool deleted = DeleteResourceFromEndpoint<CrossSellResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

            return deleted;
        }
        #endregion
    }
}
