﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    public partial class OrdersClient
    {
        public string GetOrderReceiptJavascript(OrderModel model)
        {
            var endpoint = OrdersEndpoint.GetOrderReceiptJavascript();

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<PRFTOrderReceiptJavascriptResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return Equals(response, null) ? null : response.EcommerceTrackingJavascript.ToString();
        }

        public PRFTOrderSubmissionModel ResubmitOrderToERP(int orderID)
        {
            var endpoint = OrdersEndpoint.ReSubmitOrderToERP(orderID);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<PRFTERPOrderSubmissionResponse>(endpoint, status);
            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return Equals(response, null) ? null : response.OrderSubmissionResponse;
        }
    }
}
