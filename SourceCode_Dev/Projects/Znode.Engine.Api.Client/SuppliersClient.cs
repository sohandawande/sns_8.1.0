﻿using System.Collections.ObjectModel;
using System.Net;
using Newtonsoft.Json;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
	public class SuppliersClient : BaseClient, ISuppliersClient
	{
		public SupplierModel GetSupplier(int supplierId)
		{
			return GetSupplier(supplierId, null);
		}

		public SupplierModel GetSupplier(int supplierId, ExpandCollection expands)
		{
			var endpoint = SuppliersEndpoint.Get(supplierId);
			endpoint += BuildEndpointQueryString(expands);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<SupplierResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

			return (response == null) ? null : response.Supplier;
		}

		public SupplierListModel GetSuppliers(ExpandCollection expands, FilterCollection filters, SortCollection sorts)
		{
			return GetSuppliers(expands, filters, sorts, null, null);
		}

		public SupplierListModel GetSuppliers(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
		{
			var endpoint = SuppliersEndpoint.List();
			endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<SupplierListResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new SupplierListModel { Suppliers = (response == null) ? null : response.Suppliers };
			list.MapPagingDataFromResponse(response);

			return list;
		}

		public SupplierModel CreateSupplier(SupplierModel model)
		{
			var endpoint = SuppliersEndpoint.Create();

			var status = new ApiStatus();
			var response = PostResourceToEndpoint<SupplierResponse>(endpoint, JsonConvert.SerializeObject(model), status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

			return (response == null) ? null : response.Supplier;
		}

		public SupplierModel UpdateSupplier(int supplierId, SupplierModel model)
		{
			var endpoint = SuppliersEndpoint.Update(supplierId);

			var status = new ApiStatus();
			var response = PutResourceToEndpoint<SupplierResponse>(endpoint, JsonConvert.SerializeObject(model), status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

			return (response == null) ? null : response.Supplier;
		}

        public bool DeleteSupplier(int supplierId)
		{
            var endpoint = SuppliersEndpoint.Delete(supplierId);

            var status = new ApiStatus();
            var deleted = DeleteResourceFromEndpoint<SupplierResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

            return deleted;
		}
	}
}
