﻿using System.Collections.ObjectModel;
using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    public partial class CustomerClient : BaseClient, ICustomerClient
    {
        #region Public Methods
        public CustomerListModel GetCustomerList(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = CustomerEndpoint.List();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<CustomerListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new CustomerListModel { CustomerList = (Equals(response, null)) ? null : response.CustomerAccount };
            list.MapPagingDataFromResponse(response);

            return list;
        } 
        #endregion

        public CustomerBasedPricingListModel GetCustomerBasedPricing(int accountId, ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = CustomerEndpoint.GetCustomerPricingProduct(accountId);
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<CustomerBasedPricingListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new CustomerBasedPricingListModel { CustomerBasedPricing = (Equals(response, null)) ? null : response.CustomerBasedPricingList };
            list.MapPagingDataFromResponse(response);

            return list;
        }
        #region Customer Affiliate
        public CustomerAffiliateModel GetCustomerAffiliate(int accountId, ExpandCollection expands)
        {
            var endpoint = CustomerEndpoint.GetCustomerAffiliate(accountId);
            endpoint += BuildEndpointQueryString(expands);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<CustomerAffiliateResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (Equals(response, null)) ? null : response.CustomerAffiliate;
        }

        public ReferralCommissionTypeListModel GetReferralCommissionTypeList(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = CustomerEndpoint.GetReferralCommissionTypeList();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ReferralCommissionTypeListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new ReferralCommissionTypeListModel { ReferralCommissionTypes = (Equals(response, null)) ? null : response.ReferralCommissionTypes };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public ReferralCommissionListModel GetReferralCommissionList(int accountId,ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = CustomerEndpoint.GetReferralCommissionList(accountId);
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ReferralCommissionListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new ReferralCommissionListModel { ReferralCommissions = (Equals(response, null)) ? null : response.ReferralCommissions };
            list.MapPagingDataFromResponse(response);

            return list;
        } 
        #endregion
    }
}
