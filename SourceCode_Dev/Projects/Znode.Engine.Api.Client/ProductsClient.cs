﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net;
using System.Threading.Tasks;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    public class ProductsClient : BaseClient, IProductsClient
    {
        #region Constructor
        public ProductsClient() { }

        public ProductsClient(int accountId)
        {
            AccountId = accountId;
        }
        #endregion

        #region Public Methods
        public ProductModel GetProduct(int productId)
        {
            return GetProduct(productId, null);
        }

        public ProductModel GetProduct(int productId, ExpandCollection expands)
        {
            var endpoint = ProductsEndpoint.Get(productId);
            endpoint += BuildEndpointQueryString(expands);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ProductResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (Equals(response, null)) ? null : response.Product;
        }

        public ProductModel GetProductWithSku(int productId, int skuId)
        {
            return GetProductWithSku(productId, skuId, null);
        }

        public ProductModel GetProductWithSku(int productId, int skuId, ExpandCollection expands)
        {
            var endpoint = ProductsEndpoint.GetWithSku(productId, skuId);
            endpoint += BuildEndpointQueryString(expands);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ProductResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (Equals(response, null)) ? null : response.Product;
        }

        public ProductListModel GetProducts(ExpandCollection expands, FilterCollection filters, SortCollection sorts)
        {
            return GetProducts(expands, filters, sorts, null, null);
        }

        public ProductListModel GetProducts(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = ProductsEndpoint.List();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ProductListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new ProductListModel { Products = (Equals(response, null)) ? null : response.Products };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public ProductListModel GetProductsByCatalog(int catalogId, ExpandCollection expands, FilterCollection filters, SortCollection sorts)
        {
            return GetProductsByCatalog(catalogId, expands, filters, sorts, null, null);
        }

        public ProductListModel GetProductsByCatalog(int catalogId, ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = ProductsEndpoint.ListByCatalog(catalogId);
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ProductListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new ProductListModel { Products = (Equals(response, null)) ? null : response.Products };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public ProductListModel GetProductsByCatalogIds(string catalogIds, ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = ProductsEndpoint.ListByCatalogIds(catalogIds);
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ProductListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new ProductListModel { Products = (Equals(response, null)) ? null : response.Products };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public ProductListModel GetProductsByCategory(int categoryId, ExpandCollection expands, FilterCollection filters, SortCollection sorts)
        {
            return GetProductsByCategory(categoryId, expands, filters, sorts, null, null);
        }

        public ProductListModel GetProductsByCategory(int categoryId, ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = ProductsEndpoint.ListByCategory(categoryId);
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ProductListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new ProductListModel { Products = (Equals(response, null)) ? null : response.Products };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public ProductListModel GetProductsByCategoryByPromotionType(int categoryId, int promotionTypeId, ExpandCollection expands, FilterCollection filters, SortCollection sorts)
        {
            return GetProductsByCategoryByPromotionType(categoryId, promotionTypeId, expands, filters, sorts, null, null);
        }

        public ProductListModel GetProductsByCategoryByPromotionType(int categoryId, int promotionTypeId, ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = ProductsEndpoint.ListByCategoryByPromotionType(categoryId, promotionTypeId);
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ProductListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new ProductListModel { Products = (Equals(response, null)) ? null : response.Products };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public ProductListModel GetProductsByExternalIds(string externalIds, ExpandCollection expands, SortCollection sorts)
        {
            var endpoint = ProductsEndpoint.ListByExternalIds(externalIds);
            endpoint += BuildEndpointQueryString(expands, null, sorts, null, null);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ProductListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new ProductListModel { Products = (Equals(response, null)) ? null : response.Products };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public ProductListModel GetProductsByHomeSpecials(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = ProductsEndpoint.ListByHomeSpecials();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ProductListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new ProductListModel { Products = (Equals(response, null)) ? null : response.Products };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public async Task<ProductListModel> GetProductsByHomeSpecialsAsync(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = ProductsEndpoint.ListByHomeSpecials();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = await GetResourceFromEndpointAsync<ProductListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new ProductListModel { Products = (Equals(response, null)) ? null : response.Products };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public ProductListModel GetProductsByProductIds(string productIds, ExpandCollection expands, SortCollection sorts)
        {
            var endpoint = ProductsEndpoint.ListByProductIds();
            endpoint += BuildEndpointQueryString(expands, null, sorts, null, null);

            var status = new ApiStatus();
            EntityIdsModel model = new EntityIdsModel();
            model.Ids = productIds;
            var response = PostResourceToEndpoint<ProductListResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new ProductListModel { Products = (Equals(response, null)) ? null : response.Products };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public async Task<ProductListModel> GetProductsByProductIdsAsync(string productIds, int accountId, ExpandCollection expands, SortCollection sorts)
        {
            AccountId = accountId;
            var endpoint = ProductsEndpoint.ListByProductIds();
            endpoint += BuildEndpointQueryString(expands, null, sorts, null, null);

            var status = new ApiStatus();
            EntityIdsModel model = new EntityIdsModel();
            model.Ids = productIds;
            var response = PostResourceToEndpoint<ProductListResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new ProductListModel { Products = (Equals(response, null)) ? null : response.Products };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public ProductListModel GetProductsByProductIds(string productIds, ExpandCollection expands, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = ProductsEndpoint.ListByProductIds();
            endpoint += BuildEndpointQueryString(expands, null, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            EntityIdsModel model = new EntityIdsModel();
            model.Ids = productIds;
            var response = PostResourceToEndpoint<ProductListResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new ProductListModel { Products = (Equals(response, null)) ? null : response.Products };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public ProductListModel GetProductsByPromotionType(int promotionTypeId, ExpandCollection expands, FilterCollection filters, SortCollection sorts)
        {
            return GetProductsByPromotionType(promotionTypeId, expands, filters, sorts, null, null);
        }

        public ProductListModel GetProductsByPromotionType(int promotionTypeId, ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = ProductsEndpoint.ListByPromotionType(promotionTypeId);
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ProductListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new ProductListModel { Products = (Equals(response, null)) ? null : response.Products };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public ProductListModel GetProductsByQuery(string query)
        {
            var endpoint = ProductsEndpoint.ListByQuery(query);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ProductListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new ProductListModel { Products = (Equals(response, null)) ? null : response.Products };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public ProductModel CreateProduct(ProductModel model)
        {
            var endpoint = ProductsEndpoint.Create();

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<ProductResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return (Equals(response, null)) ? null : response.Product;
        }

        public ProductModel UpdateProduct(int productId, ProductModel model)
        {
            var endpoint = ProductsEndpoint.Update(productId);

            var status = new ApiStatus();
            var response = PutResourceToEndpoint<ProductResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return (Equals(response, null)) ? null : response.Product;
        }

        public bool DeleteProduct(int productId)
        {
            var endpoint = ProductsEndpoint.Delete(productId);

            var status = new ApiStatus();
            var deleted = DeleteResourceFromEndpoint<ProductResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

            return deleted;
        }

        public SkuProductListModel GetSkuProductListBySku(FilterCollection filters)
        {
            var endpoint = ProductsEndpoint.GetProductListBySku();
            endpoint += BuildEndpointQueryString(null, filters, null, null, null);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<SkuProductListResponse>(endpoint, status);
            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new SkuProductListModel { SkuProductList = (Equals(response, null)) ? null : response.SkuProducts };
            list.MapPagingDataFromResponse(response);

            return list;
        }
        #endregion

        #region Znode Version 8.0

        #region Product Category
        public ProductModel UpdateProductSettings(int productId, ProductModel model)
        {
            var endpoint = ProductsEndpoint.UpdateProductSettings(productId);

            var status = new ApiStatus();
            var response = PutResourceToEndpoint<ProductResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return (Equals(response, null)) ? null : response.Product;
        }

        public bool IsSeoUrlExist(int productId, string seoUrl)
        {
            var endpoint = ProductsEndpoint.IsSeoUrlExist(productId, seoUrl);
            var status = new ApiStatus();
            var isExist = GetBooleanResourceFromEndpoint<ProductResponse>(endpoint, status);
            return isExist;
        }

        public ProductModel GetProductDetailsByProductId(int productId)
        {
            var endpoint = ProductsEndpoint.GetProductDetailsByProductId(productId);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ProductResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (Equals(response, null)) ? null : response.Product;
        }

        #endregion

        #region Product Sku

        public ProductSkuListModel GetProductSkuDetails(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = ProductsEndpoint.GetProductSkuDetails();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ProductSkuListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new ProductSkuListModel { ProductSkus = (Equals(response, null)) ? null : response.Skus };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public bool AssociateSkuFacets(int? skuId, string associateFacetIds, string unassociateFacetIds)
        {
            var endpoint = ProductsEndpoint.AssociateSkuFacets(skuId, associateFacetIds, unassociateFacetIds);
            var status = new ApiStatus();
            var isExist = GetBooleanResourceFromEndpoint<ProductResponse>(endpoint, status);
            return isExist;
        }

        public bool DeleteSkuAssociatedFacets(int skuId, int facetGroupId)
        {
            var endpoint = ProductsEndpoint.DeleteSkuAssociatedFacets(skuId, facetGroupId);

            var status = new ApiStatus();
            var deleted = DeleteResourceFromEndpoint<ProductFacetsResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

            return deleted;
        }

        public ProductFacetModel GetSkuAssociatedFacets(int productId, int skuId, int facetGroupId)
        {
            return GetSkuAssociatedFacets(productId, skuId, facetGroupId, null);
        }

        public ProductFacetModel GetSkuAssociatedFacets(int productId, int skuId, int facetGroupId, ExpandCollection expands)
        {
            var endpoint = ProductsEndpoint.GetSkuAssociatedFacets(productId, skuId, facetGroupId);
            endpoint += BuildEndpointQueryString(expands);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ProductFacetsResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (Equals(response, null)) ? null : response.ProductFacets;
        }

        public ProductFacetListModel GetSkuFacets(ExpandCollection expands, FilterCollection filters, SortCollection sorts)
        {
            return GetSkuFacets(expands, filters, sorts, null, null);
        }

        public ProductFacetListModel GetSkuFacets(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = ProductsEndpoint.GetSkuFacets();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ProductFacetListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new ProductFacetListModel { FacetGroups = (Equals(response, null)) ? null : response.FacetGroups };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        #endregion

        #region Product Tags

        public ProductTagsModel GetProductTags(int productId)
        {
            return GetProductTags(productId, null);
        }

        public ProductTagsModel GetProductTags(int productId, ExpandCollection expands)
        {
            var endpoint = ProductsEndpoint.GetProductTags(productId);
            endpoint += BuildEndpointQueryString(expands);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ProductTagResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (Equals(response, null)) ? null : response.ProductTag;
        }

        public ProductTagsModel CreateProductTags(ProductTagsModel model)
        {
            var endpoint = ProductsEndpoint.CreateProductTag();
            var status = new ApiStatus();
            var response = PostResourceToEndpoint<ProductTagResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return (Equals(response, null)) ? null : response.ProductTag;
        }

        public ProductTagsModel UpdateProductTags(int tagId, ProductTagsModel model)
        {
            var endpoint = ProductsEndpoint.UpdateProductTag(tagId);

            var status = new ApiStatus();
            var response = PutResourceToEndpoint<ProductTagResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return (Equals(response, null)) ? null : response.ProductTag;
        }

        public bool DeleteProductTags(int tagId)
        {
            var endpoint = ProductsEndpoint.DeleteProductTag(tagId);

            var status = new ApiStatus();
            var deleted = DeleteResourceFromEndpoint<ProductTagResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

            return deleted;
        }

        #endregion

        #region Product Facets

        public ProductFacetModel GetAssociatedFacets(int productId, int facetGroupId)
        {
            return GetAssociatedFacets(productId, facetGroupId, null);
        }

        public ProductFacetModel GetAssociatedFacets(int productId, int facetGroupId, ExpandCollection expands)
        {
            var endpoint = ProductsEndpoint.GetProductAssociatedFacets(productId, facetGroupId);
            endpoint += BuildEndpointQueryString(expands);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ProductFacetsResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (Equals(response, null)) ? null : response.ProductFacets;
        }

        public ProductFacetListModel GetProductFacets(ExpandCollection expands, FilterCollection filters, SortCollection sorts)
        {
            return GetProductFacets(expands, filters, sorts, null, null);
        }

        public ProductFacetListModel GetProductFacets(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = ProductsEndpoint.GetProductFacetsList();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ProductFacetListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new ProductFacetListModel { FacetGroups = (Equals(response, null)) ? null : response.FacetGroups };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public bool BindAssociatedFacets(ProductFacetModel model)
        {
            var endpoint = ProductsEndpoint.UpdateProductFacets(model.ProductId);

            var status = new ApiStatus();
            var response = PutResourceToEndpoint<ProductFacetsResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return (!Equals(response, null));
        }

        public bool DeleteProductAssociatedFacets(int productId, int facetGroupId)
        {
            var endpoint = ProductsEndpoint.DeleteProductAssociatedFacets(productId, facetGroupId);

            var status = new ApiStatus();
            var deleted = DeleteResourceFromEndpoint<ProductFacetsResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

            return deleted;
        }

        #endregion

        #region Product Image

        public ProductImageTypeListModel GetProductImageTypes()
        {
            var endpoint = ProductsEndpoint.ProductImageTypeList();
            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ProductImageTypeListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new ProductImageTypeListModel { ProductImageTypes = (Equals(response, null)) ? null : response.ImageTypes };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public ProductAlternateImageModel InsertProductAlternateImage(ProductAlternateImageModel model)
        {
            var endpoint = ProductsEndpoint.InsertProductAlternateImage();
            var status = new ApiStatus();
            var response = PostResourceToEndpoint<ProductAlternateImageResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return (Equals(response, null)) ? null : response.ProductImage;
        }

        public ProductAlternateImageModel UpdateProductAlternateImage(int productImageId, ProductAlternateImageModel model)
        {
            var endpoint = ProductsEndpoint.UpdateProductAlternateImage(productImageId);

            var status = new ApiStatus();
            var response = PutResourceToEndpoint<ProductAlternateImageResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return (Equals(response, null)) ? null : response.ProductImage;
        }

        public bool DeleteProductAlternateImage(int productImageId)
        {
            var endpoint = ProductsEndpoint.DeleteProductAlternateImage(productImageId);

            var status = new ApiStatus();
            var deleted = DeleteResourceFromEndpoint<ProductAlternateImageResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

            return deleted;
        }

        public ProductAlternateImageListModel GetProductAlternateImages(ExpandCollection expands, FilterCollection filters, SortCollection sorts)
        {
            return GetProductAlternateImages(expands, filters, sorts, null, null);
        }

        public ProductAlternateImageListModel GetProductAlternateImages(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = ProductsEndpoint.ProductAlternateImageList();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ProductAlternateImageListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new ProductAlternateImageListModel { ProductImages = (Equals(response, null)) ? null : response.ProductImages };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public ProductAlternateImageModel GetProductAlternateImageByProductImageId(int productImageId)
        {
            var endpoint = ProductsEndpoint.GetProductImage(productImageId);
            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ProductAlternateImageResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (Equals(response, null)) ? null : response.ProductImage;
        }

        #endregion

        #region Product Bundle

        public ProductBundlesListModel GetProductBundleList(ExpandCollection expands, FilterCollection filters, SortCollection sorts)
        {
            return GetProductBundleList(expands, filters, sorts, null, null);
        }

        public ProductBundlesListModel GetProductBundleList(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = ProductsEndpoint.ProductBundlesList();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ProductBundlesListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new ProductBundlesListModel { ProductBundles = (Equals(response, null)) ? null : response.ProductBundles };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public bool DeleteBundleProduct(int parentChildProductID)
        {
            var endpoint = ProductsEndpoint.DeleteBundleProduct(parentChildProductID);

            var status = new ApiStatus();
            var deleted = DeleteResourceFromEndpoint<ProductBundlesListResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

            return deleted;
        }

        public ProductListModel GetProductList(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = ProductsEndpoint.GetProductList();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ProductListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new ProductListModel { Products = (Equals(response, null)) ? null : response.Products };
            list.MapPagingDataFromResponse(response);

            return list;
        }



        public bool AssociateBundleProduct(ProductModel model)
        {
            var endpoint = ProductsEndpoint.AssociateBundleProduct();
            var status = new ApiStatus();

            var response = PostResourceToEndpoint<ProductResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            return (Equals(response, null)) ? false : response.IsSuccess;
        }

        #endregion

        #region Product Category

        public CategoryListModel GetProductCategoryByProductId(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = ProductsEndpoint.GetProductCategoryByProductId();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<CategoryListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new CategoryListModel { Categories = (Equals(response, null)) ? null : response.Categories };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public CategoryListModel GetProductUnAssociatedCategoryByProductId(int productId, ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = ProductsEndpoint.GetProductUnAssociatedCategoryByProductId(productId);
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<CategoryListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new CategoryListModel { Categories = (Equals(response, null)) ? null : response.Categories };
            list.MapPagingDataFromResponse(response);

            return list;
        }


        public bool AssociateProductCategory(CategoryModel model)
        {
            var endpoint = ProductsEndpoint.AssociateProductCategory();
            var status = new ApiStatus();

            var response = PostResourceToEndpoint<ProductResponse>(endpoint, JsonConvert.SerializeObject(model), status);
            return (Equals(response, null)) ? false : response.IsSuccess;
        }

        public bool UnAssociateProductCategory(CategoryModel model)
        {
            var endpoint = ProductsEndpoint.UnAssociateProductCategory();
            var status = new ApiStatus();

            var response = PostResourceToEndpoint<ProductResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            return (Equals(response, null)) ? false : response.IsSuccess;
        }

        #endregion

        #region AddOns

        public bool RemoveProductAddOn(int productAddOnId)
        {
            var endpoint = ProductsEndpoint.RemoveProductAddOn(productAddOnId);

            var status = new ApiStatus();
            var deleted = DeleteResourceFromEndpoint<ProductAddOnResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

            return deleted;
        }

        public bool AssociateAddOns(List<AddOnModel> models)
        {
            var endpoint = ProductsEndpoint.AssociateAddOns();

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<ProductAddOnResponse>(endpoint, JsonConvert.SerializeObject(models), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

            return true;
        }

        public AddOnListModel GetProductAddOns(int productId, ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = ProductsEndpoint.GetProductAddOns(productId);
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus(); var response = GetResourceFromEndpoint<AddOnListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new AddOnListModel { AddOns = (Equals(response, null)) ? null : response.AddOns };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public AddOnListModel GetUnassociatedAddOns(int productId, int portalId, ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = ProductsEndpoint.GetUnassociatedAddOns(productId, portalId);
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<AddOnListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new AddOnListModel { AddOns = (Equals(response, null)) ? null : response.AddOns };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        #endregion

        #region Highlights

        public bool RemoveProductHighlight(int productHighlightId)
        {
            var endpoint = ProductsEndpoint.RemoveProductHighlight(productHighlightId);

            var status = new ApiStatus();
            var deleted = DeleteResourceFromEndpoint<HighlightResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

            return deleted;
        }

        public bool AssociateHighlights(List<HighlightModel> models)
        {
            var endpoint = ProductsEndpoint.AssociateHighlight();

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<HighlightListResponse>(endpoint, JsonConvert.SerializeObject(models), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

            return true;
        }

        public HighlightListModel GetProductHighlights(int productId, ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = ProductsEndpoint.GetProductHighlights(productId);
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<HighlightListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new HighlightListModel { Highlights = (Equals(response, null)) ? null : response.Highlights };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public HighlightListModel GetUnassociatedHighlights(int productId, int portalId, ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = ProductsEndpoint.GetUnassociatedHighlights(productId, portalId);
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<HighlightListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new HighlightListModel { Highlights = (Equals(response, null)) ? null : response.Highlights, HighlightTypes = response.HighlightTypes };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        #endregion

        #region Product Tier Pricing

        public TierListModel GetProductTiers(int productId, ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = ProductsEndpoint.GetProductTiers(productId);
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<TierListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new TierListModel { Tiers = (Equals(response, null)) ? null : response.Tiers };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public ProductTierPricingModel CreateProductTier(ProductTierPricingModel model)
        {
            var endpoint = ProductsEndpoint.CreateProductTier();

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<ProductTierResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return (Equals(response, null)) ? null : response.ProductTier;
        }

        public ProductTierPricingModel UpdateProductTier(ProductTierPricingModel model)
        {
            var endpoint = ProductsEndpoint.UpdateProductTier();

            var status = new ApiStatus();
            var response = PutResourceToEndpoint<ProductTierResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return (Equals(response, null)) ? null : response.ProductTier;
        }

        public bool DeleteProductTier(int productTierId)
        {
            var endpoint = ProductsEndpoint.DeleteProductTier(productTierId);

            var status = new ApiStatus();
            var deleted = DeleteResourceFromEndpoint<ProductTierResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

            return deleted;
        }
        #endregion

        #region Digital Asset

        public DigitalAssetLisModel GetProductDigitalAssets(int productId, ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = ProductsEndpoint.GetDigitalAssets(productId);
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<DigitalAssetListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new DigitalAssetLisModel { DigitalAssets = (Equals(response, null)) ? null : response.DigitalAssets };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public DigitalAssetModel CreateDigitalAsset(DigitalAssetModel model)
        {
            var endpoint = ProductsEndpoint.CreateDigitalAsset();

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<DigitalAssetResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return (Equals(response, null)) ? null : response.DigitalAsset;
        }


        public bool DeleteDigitlAsset(int digitalAssetId)
        {
            var endpoint = ProductsEndpoint.DeleteDigitlAsset(digitalAssetId);

            var status = new ApiStatus();
            var deleted = DeleteResourceFromEndpoint<DigitalAssetResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

            return deleted;
        }
        #endregion

        #region Category Associated Products

        public CategoryAssociatedProductsListModel GetAllProducts(ExpandCollection expands, FilterCollection filters,
                                                            SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = ProductsEndpoint.GetAllProducts();
            endpoint += BuildEndpointQueryString(null, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ProductListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (Equals(response, null)) ? null : response.CategoryAssociatedProducts;
        }

        #endregion

        #region Product SEO Information

        public ProductModel UpdateProductSEOInformation(int productId, ProductModel model)
        {
            var endpoint = ProductsEndpoint.UpdateProductSEOInformation(productId);

            var status = new ApiStatus();
            var response = PutResourceToEndpoint<ProductResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return (Equals(response, null)) ? null : response.Product;
        }
        #endregion

        #region Update Image

        public UpdateImageModel UpdateImage(UpdateImageModel model)
        {
            var endpoint = ProductsEndpoint.UpdateImage();

            var status = new ApiStatus();
            var response = PutResourceToEndpoint<UpdateImageResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return (Equals(response, null)) ? null : response.Image;
        }

        #endregion

        public ProductModel CopyProduct(int productId)
        {
            var endpoint = ProductsEndpoint.CopyProduct(productId);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ProductResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (Equals(response, null)) ? null : response.Product;
        }

        public bool DeleteProductByProductId(int productId)
        {
            var endpoint = ProductsEndpoint.DeleteProductByProductId(productId);

            var status = new ApiStatus();
            var deleted = DeleteResourceFromEndpoint<ProductResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

            return deleted;
        }

        public ProductListModel SearchProduct(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = ProductsEndpoint.SearchProducts();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ProductListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new ProductListModel { Products = (Equals(response, null)) ? null : response.Products };
            list.MapPagingDataFromResponse(response);
            return list;

        }

        public ProductModel GetOrderProduct(int productId)
        {
            var endpoint = ProductsEndpoint.CreateOrderProduct(productId);
            endpoint += BuildEndpointQueryString(null);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ProductResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (Equals(response, null)) ? null : response.Product;
        }

        public bool SendMailToFriend(CompareProductModel model)
        {
            string endpoint = ProductsEndpoint.SendMailToFriend();
            ApiStatus status = new ApiStatus();

            ProductResponse response = PostResourceToEndpoint<ProductResponse>(endpoint, JsonConvert.SerializeObject(model), status);
            return (Equals(response, null)) ? false : response.IsSuccess;
        }

        #endregion

        //PRFT Custom Code:start
        /// <summary>
        /// Get the inventory lists from ERP
        /// </summary>
        /// <param name="expands"></param>
        /// <param name="filters"></param>
        /// <param name="sorts"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public PRFTInventoryListModel GetInventoryFromERP(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = ProductsEndpoint.GetInventoriesFromErp();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<PRFTInventoryListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new PRFTInventoryListModel { Inventories = (response == null) ? null : response.Inventories };
            list.MapPagingDataFromResponse(response);
            return list;
        }

        /// <summary>
        /// Get the product details from ERP
        /// </summary>
        /// <param name="productNum">Product Number</param>
        /// <param name="associatedCustomerExternalId">Associated Customer External ID</param>
        /// <param name="customerType">Customer Type</param>
        /// <returns></returns>
        public PRFTERPItemDetailsModel GetItemDetailsFromERP(string productNum, string associatedCustomerExternalId, string customerType)
        {
            
            var endpoint = PRFTERPServicesEndpoint.GetERPItemDetails(productNum, associatedCustomerExternalId, customerType);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<PRFTERPItemDetailsResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (response == null) ? null : response.ERPItemDetails;
        }

        /// <summary>
        /// Get Inventory Details from ERP based on product number
        /// </summary>
        /// <param name="productNum">Product Number</param>
        /// <returns></returns>
        public PRFTInventoryModel GetInventoryDetailsFromERP(string productNum)
        {
            var endpoint = PRFTERPServicesEndpoint.GetInventoryDetails(productNum);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<PRFTInventoryResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (response == null) ? null : response.Inventory;
        }
        //PRFT Custom Code:end
    }
}
