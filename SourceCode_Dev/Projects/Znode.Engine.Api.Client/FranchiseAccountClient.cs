﻿using Newtonsoft.Json;
using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    public class FranchiseAccountClient : BaseClient, IFranchiseAccountClient
    {
        public AccountModel CreateFranchiseAccount(AccountModel model)
        {
            var endpoint = FranchiseAccountEndpoint.Create();
            var status = new ApiStatus();
            var response = PostResourceToEndpoint<AccountResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return Equals(response, null) ? null : response.Account;
        }
    }
}
