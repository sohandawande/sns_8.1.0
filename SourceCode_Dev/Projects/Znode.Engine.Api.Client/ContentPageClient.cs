﻿using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    /// <summary>
    /// Client of Content Page.
    /// </summary>
    public class ContentPageClient : BaseClient, IContentPageClient
    {
        #region Public Methods

        public ContentPageListModel GetContentPages(ExpandCollection expands, FilterCollection filters, SortCollection sorts)
        {
            return GetContentPages(expands, filters, sorts, null, null);
        }

        public ContentPageListModel GetContentPages(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = ContentPageEndpoint.List();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ContentPageListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new ContentPageListModel { ContentPages = (Equals(response,null)) ? null : response.ContentPages };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public ContentPageModel CreateContentPage(ContentPageModel model)
        {
            var endpoint = ContentPageEndpoint.Create();

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<ContentPageResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            if (!Equals(status.ErrorMessage, null))
            {
                model.CustomErrorMessage = status.ErrorMessage;
            }

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return (Equals(response, null)) ? null : response.ContentPage;
        }

        public object CopyContentPage(ContentPageModel model)
        {
            var endpoint = ContentPageEndpoint.CopyContentPage();

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<ContentPageResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return (Equals(response, null)) ? null : response.ContentPageCopy;
        }

        public ContentPageModel GetContentPage(int contentPageId)
        {
            return GetContentPage(contentPageId, null);
        }

        public ContentPageModel GetContentPage(int contentPageId, ExpandCollection expands)
        {
            var endpoint = ContentPageEndpoint.Get(contentPageId);
            endpoint += BuildEndpointQueryString(expands);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ContentPageResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return Equals(response, null) ? null : response.ContentPage;
        }

        public bool AddPage(ContentPageModel model)
        {
            var endpoint = ContentPageEndpoint.AddPage();

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<TrueFalseResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return response.booleanModel.disabled;
        }

        public bool UpdateContentPage(int contentPageId, ContentPageModel model)
        {
            var endpoint = ContentPageEndpoint.Update(contentPageId);

            var status = new ApiStatus();
            var response = PutResourceToEndpoint<TrueFalseResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return response.booleanModel.disabled;
        }

        public bool DeleteContentPage(int contentPageId)
        {
            var endpoint = ContentPageEndpoint.Delete(contentPageId);

            var status = new ApiStatus();
            var deleted = DeleteResourceFromEndpoint<TrueFalseResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

            return deleted;
        }

        public string GetContentPageByName(string contentPageName, string extension)
        {
            return GetContentPageByName(contentPageName, extension, null);
        }

        public string GetContentPageByName(string contentPageName, string extension, ExpandCollection expands)
        {
            var endpoint = ContentPageEndpoint.GetByName(contentPageName, extension);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ContentPageResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);
            return Equals(response, null) ? null : response.Html;
        }

        #endregion
    }
}
