﻿using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    public class CountriesClient : BaseClient, ICountriesClient
    {
        public CountryModel GetCountry(string countryCode)
        {
            return GetCountry(countryCode, null);
        }

        public CountryModel GetCountry(string countryCode, ExpandCollection expands)
        {
            var endpoint = CountriesEndpoint.Get(countryCode);
            endpoint += BuildEndpointQueryString(expands);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<CountryResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return Equals(response, null) ? null : response.Country;
        }

        public CountryListModel GetCountries(ExpandCollection expands, FilterCollection filters, SortCollection sorts)
        {
            return GetCountries(expands, filters, sorts, null, null);
        }

        public CountryListModel GetCountries(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = CountriesEndpoint.List();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<CountryListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new CountryListModel { Countries = Equals(response, null) ? null : response.Countries };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public CountryModel CreateCountry(CountryModel model)
        {
            var endpoint = CountriesEndpoint.Create();

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<CountryResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return Equals(response, null) ? null : response.Country;
        }

        public CountryModel UpdateCountry(string countryCode, CountryModel model)
        {
            var endpoint = CountriesEndpoint.Update(countryCode);

            var status = new ApiStatus();
            var response = PutResourceToEndpoint<CountryResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return Equals(response, null) ? null : response.Country;
        }

        public bool DeleteCountry(string countryCode)
        {
            var endpoint = CountriesEndpoint.Delete(countryCode);

            var status = new ApiStatus();
            var deleted = DeleteResourceFromEndpoint<CountryResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

            return deleted;
        }

        /// <summary>
        /// Znode Version 7.2.2
        /// This function Get all active countries for portalId.
        /// </summary>
        /// <param name="portalId">int portalId</param>
        ///  <param name="billingShippingFlag">int billingShippingFlag</param>
        /// <returns>Returns the country list</returns>
        public CountryListModel GetActiveCountryByPortalId(int portalId, int billingShippingFlag = 0)
        {
            var endpoint = CountriesEndpoint.GetActiveCountryByPortalId(portalId, billingShippingFlag);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<CountryListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new CountryListModel { Countries = Equals(response, null) ? null : response.Countries };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public CountryListModel GetCountryByPortalId(int portalId)
        {
            var endpoint = CountriesEndpoint.GetCountryByPortalId(portalId);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<CountryListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new CountryListModel { Countries = Equals(response, null) ? null : response.Countries };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public CountryListModel GetShippingActiveCountryByPortalId(int portalId)
        {
            var endpoint = CountriesEndpoint.GetShippingActiveCountryByPortalId(portalId);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<CountryListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new CountryListModel { Countries = Equals(response, null) ? null : response.Countries };
            list.MapPagingDataFromResponse(response);

            return list;
        }
    }
}
