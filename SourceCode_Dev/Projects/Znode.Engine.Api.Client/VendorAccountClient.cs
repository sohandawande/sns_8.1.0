﻿using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    public class VendorAccountClient : BaseClient , IVendorAccountClient
    {
        public VendorAccountListModel GetVendorAccountList(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = VendorsAccountEndpoint.List();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<VendorAccountListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new VendorAccountListModel { VendorAccount = (Equals(response, null)) ? null : response.VendorAccounts };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public AccountModel CreateVendorAccount(AccountModel model)
        {
            var endpoint = VendorsAccountEndpoint.Create();

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<AccountResponse>(endpoint, JsonConvert.SerializeObject(model), status);
            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.Created };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (response == null) ? null : response.Account;
        }

        public AccountModel UpdateVendorAccount(int accountId, AccountModel model)
        {
            var endpoint = VendorsAccountEndpoint.Update(accountId);

            var status = new ApiStatus();
            var response = PutResourceToEndpoint<AccountResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return (Equals(response, null)) ? null : response.Account;
        }

        public VendorAccountModel GetVendorAccountById(int accountid)
        {
            var endpoint = VendorsAccountEndpoint.GetVendorAccountById(accountid);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<VendorAccountResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (Equals(response, null)) ? null : response.VendorAccount;
        }
    }
}
