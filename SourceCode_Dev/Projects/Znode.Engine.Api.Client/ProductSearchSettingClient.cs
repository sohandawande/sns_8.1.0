﻿using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client 
{
    public class ProductSearchSettingClient : BaseClient, IProductSearchSettingClient
    {
        #region Public Methods
        
        public ProductLevelSettingListModel GetProductLevelSettings(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = ProductSearchSettingEndPoint.GetProductLevelSettings();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ProductSearchSettingResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new ProductLevelSettingListModel { Products = (Equals(response, null)) ? null : response.Products };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public CategoryLevelSettingListModel GetCategoryLevelSettings(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = ProductSearchSettingEndPoint.GetCategoryLevelSettings();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ProductSearchSettingResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new CategoryLevelSettingListModel { Categories = (Equals(response, null)) ? null : response.Categories };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public FieldLevelSettingListModel GetFieldLevelSettings(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = ProductSearchSettingEndPoint.GetFieldLevelSettings();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ProductSearchSettingResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new FieldLevelSettingListModel { FieldLevelSettings = (Equals(response, null)) ? null : response.Fields };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public bool SaveBoostValues(BoostSearchSettingModel model)
        {
            var endpoint = ProductSearchSettingEndPoint.SaveBoostValues();

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<ProductSearchSettingResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return (response == null) ? false : response.SaveFlag;
        }

        #endregion
    }
}
