﻿using System.Data;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Engine.Api.Client
{
    public partial interface IOrdersClient : IBaseClient
    {
        /// <summary>
        /// To Get Order Details based on OrderId
        /// </summary>
        /// <param name="orderId">Id for the Order</param>
        /// <returns>Return the Order Details in OrderModel format.</returns>
        OrderModel GetOrder(int orderId);

        /// <summary>
        /// To Get Order Details based on OrderId
        /// </summary>
        /// <param name="orderId">Id for the Order</param>
        /// <param name="expands">Type of Expand Collection.</param>
        /// <returns>Return the Order Details in OrderModel format.</returns>
        OrderModel GetOrder(int orderId, ExpandCollection expands);

        /// <summary>
        /// To Get Order List.
        /// </summary>
        /// <param name="expands">Type of Expand Collection</param>
        /// <param name="filters">Type of Filter Collection</param>
        /// <param name="sorts">Type of Sort Collection</param>
        /// <returns>Return the Orders List in OrderListModel format. </returns>
        OrderListModel GetOrders(ExpandCollection expands, FilterCollection filters, SortCollection sorts);

        /// <summary>
        /// To Get Order List.
        /// </summary>
        /// <param name="expands">Type of Expand Collection</param>
        /// <param name="filters">Type of Filter Collection</param>
        /// <param name="sorts">Type of Sort Collection</param>
        /// <param name="pageIndex">Page Start Index</param>
        /// <param name="pageSize">Page Size</param>
        /// <returns>Return the Orders List in OrderListModel format. </returns>
        OrderListModel GetOrders(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// To Create the New Order.
        /// </summary>
        /// <param name="model">Model of type ShoppingCartModel</param>
        /// <returns>Return the Order details in OrderModel format.</returns>
        OrderModel CreateOrder(ShoppingCartModel model);

        /// <summary>
        /// To Update the Order Details
        /// </summary>
        /// <param name="orderId">Id of the Order</param>
        /// <param name="model">Model of type ShoppingCartModel</param>
        /// <returns>Return the Order details in OrderModel format.</returns>
        OrderModel UpdateOrder(int orderId, OrderModel model);

        /// <summary>
        /// Gets Order list from database using stored procedure.
        /// ZNode Version 8.0
        /// </summary>
        /// <param name="expands">Expands collection</param>
        /// <param name="filters">Filters collection</param>
        /// <param name="sorts">Sort collection</param>
        /// <param name="pageIndex">Index of page</param>
        /// <param name="pageSize">Size of page(Records per page)</param>
        /// <returns>Returns model of type AdminOrderList model.</returns>
        AdminOrderListModel GetOrderList(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Gets Order deatils from database using stored procedure.
        /// ZNode Version 8.0
        /// </summary>
        /// <param name="expands">Expands collection</param>
        /// <param name="filters">Filters collection</param>
        /// <param name="sorts">Sort collection</param>
        /// <param name="pageIndex">Index of page</param>
        /// <param name="pageSize">Size of page(Records per page)</param>
        /// <returns>Returns model of type Order model.</returns>
        OrderModel GetOrderDetails(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Updates existing order status.
        /// </summary>
        /// <param name="orderId">Id of order</param>
        /// <param name="model">Model of type Order model</param>
        /// <returns>Returns model of type Order model.</returns>
        OrderModel UpdateOrderStatus(int orderId, OrderModel model);

        /// <summary>
        /// Updates existing order with void payment.
        /// </summary>
        /// <param name="orderId">Id of order</param>
        /// <param name="model">Model of type Order model</param>
        /// <returns>Returns response of type Order response.</returns>
        OrderResponse VoidPayment(int orderId, OrderModel model);

        /// <summary>
        /// Updates existing order with refund payment.
        /// </summary>
        /// <param name="orderId">Id of order</param>
        /// <param name="model">Model of type Order model</param>
        /// <returns>Returns response of type Order response.</returns>
        OrderResponse RefundPayment(int orderId, OrderModel model);

        /// <summary>
        /// Downloads order data from db using sp.
        /// ZNode Version 8.0
        /// </summary>
        /// <param name="filters">Filters collection</param>
        /// <returns>Returns Dataset.</returns>
        DataSet DownloadOrderData(FilterCollection filters = null);

        /// <summary>
        /// Downloads order line items data from db using sp.
        /// ZNode Version 8.0
        /// </summary>
        /// <param name="filters">Filters collection</param>
        /// <returns>Returns Dataset.</returns>
        DataSet DownloadOrderLineItemData(FilterCollection filters = null);

        /// <summary>
        /// Creates a new customer.
        /// </summary>
        /// <param name="listModel">Model of type AddressListModel</param>
        /// <returns>Returns response of type AddressListResponse.</returns>
        AddressListResponse AddNewCustomer(AddressListModel listModel);

        /// <summary>
        /// Get order line item on the basis of order line item id.
        /// </summary>
        /// <param name="orderLineItemId">int id of order line item</param>
        /// <returns>Returns order line item on the basis of order line item id.</returns>
        OrderLineItemModel GetOrderLineItems(int orderLineItemId);

        /// <summary>
        /// Updates order line item status on the basis of order line item id.
        /// </summary>
        /// <param name="orderLineItemId">int id of order line item</param>
        /// <param name="model">Model of type OrderLineItemModel</param>
        /// <returns>Returns updated order line item status.</returns>
        OrderLineItemModel UpdateOrderLineItemStatus(int orderLineItemId, OrderLineItemModel model);

        /// <summary>
        /// This method will update the payment status for that perticualr order
        /// </summary>
        /// <param name="orderId">int order id</param>
        /// <param name="paymentStatus">string payment status</param>
        /// <returns>True if updated</returns>
        bool UpdateOrderPaymentStatus(int orderId, string paymentStatus);

        /// <summary>
        /// To update customer addresses.
        /// </summary>
        /// <param name="accountId">customer account id</param>
        /// <param name="listModel">AddressListModel with address data of customer</param>
        /// <returns>AddressListResponse</returns>
        AddressListResponse UpdateCustomerAddress(int accountId, AddressListModel listModel);

        /// <summary>
        /// Sends email for the shipped status change and also tracking number of order line item.
        /// ZNode Version 8.0
        /// </summary>
        /// <param name="filters">Filter collection which will contain order related data.</param>
        /// <returns>Returns string message whether the email was sent or not.</returns>
        string SendEmail(FilterCollection filters = null);
    }
}
