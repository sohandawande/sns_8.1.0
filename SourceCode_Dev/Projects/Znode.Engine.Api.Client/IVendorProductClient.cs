﻿using System;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;


namespace Znode.Engine.Api.Client
{
    public interface IVendorProductClient : IBaseClient
    {
        
        /// <summary>
        /// This method will get the vendor product list
        /// </summary>
        /// <param name="expands">Expand Collection expands</param>
        /// <param name="filters">Filter criteria</param>
        /// <param name="sorts">sort criteria</param>
        /// <param name="page">page collection</param>
        /// <returns>Returns all vendor product details in AccountListModel format</returns>
        VendorProductListModel GetVendorProductList(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// This method will get the mall admin product list
        /// </summary>
        /// <param name="expands">Expand Collection expands</param>
        /// <param name="filters">Filter criteria</param>
        /// <param name="sorts">sort criteria</param>
        /// <param name="page">page collection</param>
        /// <returns>Returns all vendor product details in AccountListModel format</returns>
        VendorProductListModel GetMallAdminProductList(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// To Change the Product review state.
        /// </summary>
        /// <param name="model">RejectProductModel model</param>
        /// <param name="state">string state</param>
        /// <returns>Return true or false</returns>
        bool ChangeProductStatus(RejectProductModel model, string state);

        /// <summary>
        /// To Get Rejected Product details
        /// </summary>
        /// <param name="productIds">string productIds</param>
        /// <returns>Return rejected product details</returns>
        VendorProductListModel BindRejectProduct(string productIds);

        /// <summary>
        /// To Get Vendor Product image List
        /// </summary>
        /// <param name="filters">FilterCollection filters</param>
        /// <param name="sortCollection">SortCollection sortCollection</param>
        /// <param name="pageIndex">int? pageIndex</param>
        /// <param name="recordPerPage">int? recordPerPage</param>
        /// <returns>Return Product image List.</returns>
        ProductAlternateImageListModel GetReviewImageList(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// To Change the Product images review state.
        /// </summary>
        /// <param name="productIds">string productIds</param>
        /// <param name="state">string state</param>
        /// <returns>Return true or false</returns>
        bool UpdateProductImageStatus(string productIds, string state);

        /// <summary>
        /// To Update the Vendor Product Details.
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="model">ProductModel model</param>
        /// <returns>Return the Updated Product Model.</returns>
        ProductModel UpdateVendorProduct(int productId, ProductModel model);

        /// <summary>
        /// To Update the Vendor Product Marketing Details.
        /// </summary>
        /// <param name="model">ProductModel model</param>
        /// <returns>Return updated Product Model</returns>
        ProductModel UpdateMarketingDetails(ProductModel model);

        /// <summary>
        /// To Get the Product Category Node based on Product Id.
        /// </summary>
        /// <param name="productId">Id for the Product</param>
        /// <returns>Return Product Category Node List in CategoryNodeListModel model</returns>
        CategoryNodeListModel GetCategoryNode(int productId);

        /// <summary>
        /// To Delete the product details based on productId.
        /// </summary>
        /// <param name="productId">Id for the product</param>
        /// <returns>Return true or false</returns>
        bool DeleteProduct(int productId);

    }
}
