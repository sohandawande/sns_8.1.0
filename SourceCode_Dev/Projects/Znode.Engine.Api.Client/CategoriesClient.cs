﻿using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    public class CategoriesClient : BaseClient, ICategoriesClient
    {
        #region Public Methods
        public CategoryModel GetCategory(int categoryId)
        {
            return GetCategory(categoryId, null);
        }

        public CategoryModel GetCategory(int categoryId, ExpandCollection expands)
        {
            var endpoint = CategoriesEndpoint.Get(categoryId);
            endpoint += BuildEndpointQueryString(expands);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<CategoryResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (Equals(response, null)) ? null : response.Category;
        }

        public CategoryListModel GetCategories(ExpandCollection expands, FilterCollection filters, SortCollection sorts)
        {
            return GetCategories(expands, filters, sorts, null, null);
        }

        public CategoryListModel GetCategories(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = CategoriesEndpoint.List();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<CategoryListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new CategoryListModel { Categories = (Equals(response, null)) ? null : response.Categories };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public CategoryListModel GetCategoryTree(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = CategoriesEndpoint.CategoryTree();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<CategoryListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new CategoryListModel { Categories = (Equals(response, null)) ? null : response.Categories };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public CategoryListModel GetCategoriesByCatalog(int catalogId, ExpandCollection expands, FilterCollection filters, SortCollection sorts)
        {
            return GetCategoriesByCatalog(catalogId, expands, filters, sorts, null, null);
        }

        public CategoryListModel GetCategoriesByCatalog(int catalogId, ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = CategoriesEndpoint.ListByCatalog(catalogId);
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<CategoryListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new CategoryListModel { Categories = (Equals(response, null)) ? null : response.Categories };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public CategoryListModel GetCategoriesByCatalogIds(string catalogIds, ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = CategoriesEndpoint.ListByCatalogIds(catalogIds);
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<CategoryListResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            var list = new CategoryListModel { Categories = (Equals(response, null)) ? null : response.Categories };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public CategoryListModel GetCategoriesByCategoryIds(string categoryIds, ExpandCollection expands, FilterCollection filters,
                                                            SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = CategoriesEndpoint.ListByCategoryIds(categoryIds);
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<CategoryListResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            var list = new CategoryListModel { Categories = (Equals(response, null)) ? null : response.Categories };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public CategoryModel CreateCategory(CategoryModel model)
        {
            var endpoint = CategoriesEndpoint.Create();

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<CategoryResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return (Equals(response, null)) ? null : response.Category;
        }

        public CategoryModel UpdateCategory(int categoryId, CategoryModel model)
        {
            var endpoint = CategoriesEndpoint.Update(categoryId);

            var status = new ApiStatus();
            var response = PutResourceToEndpoint<CategoryResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return (Equals(response, null)) ? null : response.Category;
        }

        public bool DeleteCategory(int categoryId)
        {
            var endpoint = CategoriesEndpoint.Delete(categoryId);

            var status = new ApiStatus();
            var deleted = DeleteResourceFromEndpoint<CategoryResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

            return deleted;
        }
        #endregion

        #region Znode Version 8.0

        //Gets the categories associated with a specific catalog.
        public CatalogAssociatedCategoriesListModel GetCategoryByCatalogIdFromCustomService(int catalogId = 0, FilterCollection filters = null,
                                                            SortCollection sorts = null, int? pageIndex = 1, int? pageSize = 10)
        {
            return catalogId > 0 ? GetCategoryByIdCustomService(catalogId, filters, sorts, pageIndex, pageSize) : null;
        }

        private CatalogAssociatedCategoriesListModel GetCategoryByIdCustomService(int catalogId, FilterCollection filters,
                                                            SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = CategoriesEndpoint.GetCategoriesByCatalogId(catalogId);
            endpoint += BuildEndpointQueryString(null, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<CategoryListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (Equals(response, null)) ? null : response.CatalogAssociatedCategories;
        }

        public CatalogAssociatedCategoriesListModel GetAllCategories(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {

            var endpoint = CategoriesEndpoint.GetAllCategories();
            endpoint += BuildEndpointQueryString(null, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<CategoryListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (Equals(response, null)) ? null : response.CatalogAssociatedCategories;
        }

        public bool CategoryAssociatedProducts(int categoryId, string productIds)
        {
            var endpoint = CategoriesEndpoint.CategoryAssociatedProducts(categoryId, productIds);
            var status = new ApiStatus();
            bool isExist = GetBooleanResourceFromEndpoint<CategoryResponse>(endpoint, status);
            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent); 
            return isExist;
        }

        public CategoryModel UpdateCategorySEODetails(int categoryId, CategoryModel model)
        {
            var endpoint = CategoriesEndpoint.UpdateCategorySEODetails(categoryId);

            var status = new ApiStatus();
            var response = PutResourceToEndpoint<CategoryResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return (Equals(response, null)) ? null : response.Category;
        }

        public ProductListModel GetCategoryAssociatedProducts(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = CategoriesEndpoint.GetCategoryAssociatedProducts();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ProductListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new ProductListModel { Products = (Equals(response, null)) ? null : response.Products };
            list.MapPagingDataFromResponse(response);

            return list;
        }
        #endregion
    }
}
