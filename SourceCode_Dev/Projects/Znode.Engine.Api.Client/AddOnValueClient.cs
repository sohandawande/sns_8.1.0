﻿using System.Collections.ObjectModel;
using System.Net;
using Newtonsoft.Json;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    /// <summary>
    /// AddOn Value Client
    /// </summary>
    public class AddOnValueClient : BaseClient, IAddOnValueClient
    {
        #region Public Methods
        public AddOnValueModel GetAddOnValue(int addOnValueId)
        {
            return GetAddOnValue(addOnValueId, null);
        }

        public AddOnValueModel GetAddOnValue(int addOnValueId, ExpandCollection expands)
        {
            var endpoint = AddOnValueEndpoint.Get(addOnValueId);
            endpoint += BuildEndpointQueryString(expands);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<AddOnValueResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (Equals(response, null)) ? null : response.AddOnValue;
        }



        public AddOnValueModel CreateAddOnValue(AddOnValueModel model)
        {
            var endpoint = AddOnValueEndpoint.Create();

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<AddOnValueResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return (Equals(response, null)) ? null : response.AddOnValue;
        }

        public AddOnValueModel UpdateAddOnValue(int addOnValueId, AddOnValueModel model)
        {
            var endpoint = AddOnValueEndpoint.Update(addOnValueId);

            var status = new ApiStatus();
            var response = PutResourceToEndpoint<AddOnValueResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return (Equals(response, null)) ? null : response.AddOnValue;
        }

        public bool DeleteAddOnValue(int addOnValueId)
        {
            var endpoint = AddOnValueEndpoint.Delete(addOnValueId);

            var status = new ApiStatus();
            var deleted = DeleteResourceFromEndpoint<AddOnValueResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

            return deleted;
        }

        public AddOnValueListModel GetAddOnValueByAddOnId(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = AddOnValueEndpoint.List();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<AddOnValueListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new AddOnValueListModel { AddOnValues = (Equals(response, null)) ? null : response.AddOnValuesList.AddOnValues };
            list.MapPagingDataFromResponse(response);

            return list;
        }
       
        #endregion


       
    }
}