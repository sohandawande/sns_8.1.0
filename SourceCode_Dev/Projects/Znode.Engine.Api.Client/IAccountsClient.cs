﻿using System;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
	public partial interface IAccountsClient : IBaseClient
	{
        /// <summary>
        /// This method will get the account by Account ID
        /// </summary>
        /// <param name="accountId">integer Account ID</param>
        /// <returns>Returns the account details in AccountModel format</returns>
		AccountModel GetAccount(int accountId);

        /// <summary>
        /// This method will get the account by Account ID
        /// </summary>
        /// <param name="accountId">integer Account ID</param>
        /// <param name="expands">Expand collection</param>
        /// <returns>Returns the account details in AccountModel format</returns>
		AccountModel GetAccount(int accountId, ExpandCollection expands);

        /// <summary>
        /// This method will get the Account details by user id
        /// </summary>
        /// <param name="userId">Guid User ID</param>
        /// <returns>Returns the account details in AccountModel format</returns>
		AccountModel GetAccountByUser(Guid userId);

        /// <summary>
        /// This method will get the Account details by user id
        /// </summary>
        /// <param name="userId">Guid User ID</param>
        /// <param name="expands">Expad Collection</param>
        /// <returns>Returns the account details in AccountModel format</returns>
		AccountModel GetAccountByUser(Guid userId, ExpandCollection expands);

        /// <summary>
        /// This method will get the Account details by user name
        /// </summary>
        /// <param name="userId">Guid User ID</param>
        /// <returns>Returns the account details in AccountModel format</returns>
		AccountModel GetAccountByUser(string username);

        /// <summary>
        /// This method will get the Account details by user name
        /// </summary>
        /// <param name="userId">Guid User ID</param>
        /// <param name="expands">Expand collection expands</param>
        /// <returns>Returns the account details in AccountModel format</returns>
		AccountModel GetAccountByUser(string username, ExpandCollection expands);

        /// <summary>
        /// This method will get the details of the Accounts
        /// </summary>
        /// <param name="expands">Expand Collection expands</param>
        /// <param name="filters">Filter criteria</param>
        /// <param name="sorts">sort criteria</param>
        /// <param name="page">page collection</param>
        /// <returns>Returns all account detaisl in AccountListModel format</returns>
        AccountListModel GetAccounts(ExpandCollection expands, FilterCollection filters, SortCollection sorts);

        /// <summary>
        /// This method will get the details of the Accounts
        /// </summary>
        /// <param name="expands">Expand Collection expands</param>
        /// <param name="filters">Filter criteria</param>
        /// <param name="sorts">sort criteria</param>
        /// <param name="page">page collection</param>
        /// <returns>Returns all account detaisl in AccountListModel format</returns>
        AccountListModel GetAccounts(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// This method will create the Account for the user
        /// </summary>
        /// <param name="model">AccountModel model</param>
        /// <returns>Returns the new details in AccountModel format</returns>
        AccountModel CreateAccount(AccountModel model);

        /// <summary>
        /// This method will update the account details based on account id
        /// </summary>
        /// <param name="model">AccountModel model</param>
        /// <returns>Returns the updated details</returns>
		AccountModel UpdateAccount(int accountId, AccountModel models);

        /// <summary>
        /// This method is used to delete the account
        /// </summary>
        /// <param name="accountId">integer Account ID</param>
        /// <returns>Returns true if Account deleted</returns>
		bool DeleteAccount(int accountId);

        /// <summary>
        /// This method is used to logged in to the site
        /// </summary>
        /// <param name="model">AccountModel model</param>
        /// <returns>Returns the data for the logged in</returns>
		AccountModel Login(AccountModel model);

        /// <summary>
        /// This method is used to logged in to the site
        /// </summary>
        /// <param name="model">AccountModel model</param>
        /// <param name="expand">Expand collection</param>
        /// <returns>Returns the data for the logged in</returns>
		AccountModel Login(AccountModel model, ExpandCollection expandss);

        /// <summary>
        /// This method will reset the password
        /// </summary>
        /// <param name="model">AccountModel model</param>
        /// <returns>returns all the account model data</returns>
		AccountModel ResetPassword(AccountModel models);

        /// <summary>
        /// This method is used to change the password
        /// </summary>
        /// <param name="model">AccountModel model</param>
        /// <returns>Returns changed data in AccountModel format</returns>
		AccountModel ChangePassword(AccountModel model);

        /// <summary>
        /// Znode Version 7.2.2
        /// This function will check the Reset Password Link current status.
        /// </summary>
        /// <param name="model">AccountModel</param>
        /// <returns>Returns Status of Reset Password Link.</returns>
        AccountModel CheckResetPasswordLinkStatus(AccountModel model);

        /// <summary>
        /// Znode Version 8.0
        /// Method Check for the user role based on role name.
        /// </summary>
        /// <param name="model">AccountModel</param>
        /// <returns>Returns status for the user based on role name</returns>
        AccountModel CheckUserRole(AccountModel model);

        /// <summary>
        /// To check  Address is Valid
        /// </summary>
        /// <param name="model">AddressModel model</param>
        /// <returns>returns AddressModel</returns>
        AddressModel IsAddressValid(AddressModel model);

        /// <summary>
        /// Znode Version 8.0
        /// Function used for Reset admin details for the first default login.
        /// </summary>
        /// <param name="model">AccountModel</param>
        /// <returns>Returns reset admin details status.</returns>
        AccountModel ResetAdminDetails(AccountModel model);

        /// <summary>
        /// Znode Version 8.0
        /// This is the method to create the SiteAdmin user
        /// </summary>
        /// <param name="model">AccountModel</param>
        /// <returns>Returns admin details in AccountModel format.</returns>
        AccountModel CreateAdminAccount(AccountModel model);

        /// <summary>
        /// This method will fetch the account details by role name
        /// </summary>
        /// <param name="roleName">page collection</param>
        /// <param name="expands">Expand Collection expands</param>
        /// <param name="filters">Filter criteria</param>
        /// <param name="sorts">sort criteria</param>
        /// <param name="page">page collection</param>        
        /// <returns>Returns all account detaisl in AccountListModel format</returns>
        AccountListModel GetAccountDetailsByRoleName(string roleName, ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// This method will disable the admin account
        /// </summary>
        /// <param name="accountId">int Account Id</param>
        /// <returns>True if account disabled</returns>
        bool EnableDisableAdminAccount(int accountId);

        /// <summary>
        /// Znode Version 8.0
        /// To Update Customer Account Details.
        /// </summary>
        /// <param name="model">AccountModel model</param>
        /// <returns>Returns updated customer account details in AccountModel format.</returns>
        AccountModel UpdateCustomerAccount(AccountModel model);

        /// <summary>
        /// Create New Customer Account.
        /// </summary>
        /// <param name="model">AccountModel</param>
        /// <returns>AccountModel</returns>
        AccountModel CreateCustomerAccount(AccountModel model);

        /// <summary>
        /// To send mail
        /// </summary>
        /// <param name="model">SendMailModel model</param>
        /// <returns>returns SendMailModel</returns>
        SendMailModel SendEmail(SendMailModel model);

        /// <summary>
        /// Get list of Account Payments
        /// </summary>
        /// <param name="expands">Expand Collection expands</param>
        /// <param name="filters">Filter criteria</param>
        /// <param name="sorts">sort criteria</param>
        /// <param name="page">page collection</param>   
        /// <returns>Returns list of All Account Payments</returns>
        AccountPaymentListModel GetAccountPaymentList(int accountId,ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Create new Customer Account Payment 
        /// </summary>
        /// <param name="model">AccountPaymentModel</param>
        /// <returns>Returns AccountPaymentModel</returns>
        AccountPaymentModel CreateAccountPayment(AccountPaymentModel model);

        /// <summary>
        /// Get list of Role Permissions based on userName.
        /// </summary>
        /// <param name="userName">UserName for the user</param>
        /// <returns>Return the Role Permission list in RolePermissionListModel format</returns>
        RolePermissionListModel GetRolePermission(string userName);

        /// <summary>
        /// Get list of Role Menu based on userName.
        /// </summary>
        /// <param name="userName">UserName for the user</param>
        /// <returns>Return the Role Menu list in RoleMenuListModel format</returns>
        RoleMenuListModel GetRoleMenuList(string userName);

        /// <summary>
        /// This function is used to check Role Exist For Profile
        /// </summary>
        /// <param name="porfileId">int porfileId - default porfileId</param>
        /// <param name="roleName">string roleName</param>
        /// <returns>returns true/false</returns>     
        bool IsRoleExistForProfile(int porfileId, string roleName);

        /// <summary>
        /// Sign Up the User for NewsLetters
        /// </summary>
        /// <param name="model">NewsLetterSignUpModel</param>
        /// <returns>returns true/false</returns>
        bool SignUpForNewsLetter(NewsLetterSignUpModel model);

        /// <summary>
        /// Validate the Social User Login
        /// </summary>
        /// <param name="model">SocialLoginModel</param>
        /// <param name="expands">Expand collection</param>
        /// <returns>return account details in AccountModel Format</returns>
        AccountModel SocialUserLogin(SocialLoginModel model, ExpandCollection expands);

        /// <summary>
        /// Creates/Updated the Social User Account
        /// </summary>
        /// <param name="model">SocialLoginModel</param>
        /// <returns>Return true or false</returns>
        bool SocialUserCreateOrUpdateAccount(SocialLoginModel model);

        /// <summary>
        /// To Get the Registered Social Client Details.
        /// </summary>
        /// <returns>Return details in RegisteredSocialClientListModel format</returns>
        RegisteredSocialClientListModel GetRegisteredSocialClientDetails();
	}
}