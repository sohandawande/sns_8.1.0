﻿using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface ICustomerBasedPricingClient
    {
        /// <summary>
        /// Get Customer  Based Pricing.
        /// </summary>
        /// <param name="productId">product id</param>
        /// <param name="expands">expand collection for customer based pricing</param>
        /// <param name="filters">filtering for customer based pricing</param>
        /// <param name="sorts">sorting for customer based pricing</param>
        /// <returns>CustomerBasedPricingListModel</returns>
        CustomerBasedPricingListModel GetCustomerBasedPricing(int productId, ExpandCollection expands, FilterCollection filters, SortCollection sorts);

        /// <summary>
        ///  Get Customer  Based Pricing.
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="expands">expand collection for customer based pricing</param>
        /// <param name="filters">filtering for customer based pricing</param>
        /// <param name="sorts">sorting for customer based pricing</param>
        /// <param name="pageIndex">paging for customer pricing</param>
        /// <param name="pageSize">customer pricing record per page</param>
        /// <returns>CustomerBasedPricingListModel</returns>
        CustomerBasedPricingListModel GetCustomerBasedPricing(int productId, ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
    }
}
