﻿using System.Threading.Tasks;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    /// <summary>
    /// Message Configs Client Interface
    /// </summary>
    public interface IMessageConfigsClient : IBaseClient
	{
        /// <summary>
        /// Get Message Config on the basis of messageConfigId
        /// </summary>
        /// <param name="messageConfigId">messageConfigId</param>
        /// <returns>Returns MessageConfigModel</returns>
		MessageConfigModel GetMessageConfig(int messageConfigId);

        /// <summary>
        /// Get Message Config on the basis of messageConfigId
        /// </summary>
        /// <param name="messageConfigId">messageConfigId</param>
        /// <param name="expands">ExpandCollection expands</param>
        /// <returns>Returns MessageConfigModel</returns>
		MessageConfigModel GetMessageConfig(int messageConfigId, ExpandCollection expands);

        /// <summary>
        /// Get Message Config By Key
        /// </summary>
        /// <param name="key">key</param>
        /// <param name="expands">ExpandCollection expands</param>
        /// <returns>Returns MessageConfigModel</returns>
        MessageConfigModel GetMessageConfigByKey(string key, ExpandCollection expands);

        /// <summary>
        /// Get list of Message Configs
        /// </summary>
        /// <param name="expands">ExpandCollection expands</param>
        /// <param name="filters">FilterCollection filters</param>
        /// <param name="sorts">SortCollection sorts</param>
        /// <returns>Returns list of message configs</returns>
		MessageConfigListModel GetMessageConfigs(ExpandCollection expands, FilterCollection filters, SortCollection sorts);

        /// <summary>
        /// Get list of Message Configs
        /// </summary>
        /// <param name="expands">ExpandCollection expands</param>
        /// <param name="filters">FilterCollection filters</param>
        /// <param name="sorts">SortCollection sorts</param>
        /// <param name="pageIndex">Index of page</param>
        /// <param name="pageSize">Size of page</param>
        /// <returns>Returns list of message configs</returns>
		MessageConfigListModel GetMessageConfigs(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Get Message Configs By Keys
        /// </summary>
        /// <param name="keys">keys</param>
        /// <param name="expands">ExpandCollection expands</param>
        /// <param name="sorts">SortCollection sorts</param>
        /// <returns>Returns list of message configs</returns>
		MessageConfigListModel GetMessageConfigsByKeys(string keys, ExpandCollection expands, SortCollection sorts);

        /// <summary>
        /// Create Message Config
        /// </summary>
        /// <param name="model">MessageConfigModel model</param>
        /// <returns>Returns MessageConfigModel</returns>
		MessageConfigModel CreateMessageConfig(MessageConfigModel model);

        /// <summary>
        /// Update Message Config
        /// </summary>
        /// <param name="messageConfigId">messageConfigId</param>
        /// <param name="model">MessageConfigModel model</param>
        /// <returns>Returns MessageConfigModel</returns>
		MessageConfigModel UpdateMessageConfig(int messageConfigId, MessageConfigModel model);

        /// <summary>
        /// Delete Message Config on the basis of messageConfigId
        /// </summary>
        /// <param name="messageConfigId">messageConfigId</param>
        /// <returns>Returns true/false</returns>
		bool DeleteMessageConfig(int messageConfigId);

        /// <summary>
        /// Get Message Config By Key Async
        /// </summary>
        /// <param name="key">key</param>
        /// <param name="expands">ExpandCollection expands</param>
        /// <returns>Returns MessageConfigModel</returns>
        Task<MessageConfigModel> GetMessageConfigByKeyAsync(string key, ExpandCollection expands);
	}
}
