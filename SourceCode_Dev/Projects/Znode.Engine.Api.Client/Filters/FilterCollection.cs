﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Client.Filters
{
	public class FilterCollection : Collection<FilterTuple>
	{
		public void Add(string filterName, string filterOperator, string filterValue)
		{
			Add(new FilterTuple(filterName, filterOperator, filterValue));
		}
	}
}
