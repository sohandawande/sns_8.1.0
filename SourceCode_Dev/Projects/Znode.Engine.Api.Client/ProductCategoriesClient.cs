﻿using System.Collections.ObjectModel;
using System.Net;
using Newtonsoft.Json;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
	public class ProductCategoriesClient : BaseClient, IProductCategoriesClient
	{
		public ProductCategoryModel GetProductCategory(int productCategoryId)
		{
			return GetProductCategory(productCategoryId, null);
		}

		public ProductCategoryModel GetProductCategory(int productCategoryId, ExpandCollection expands)
		{
			var endpoint = ProductCategoriesEndpoint.Get(productCategoryId);
			endpoint += BuildEndpointQueryString(expands);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<ProductCategoryResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

			return (response == null) ? null : response.ProductCategory;
		}

        public ProductCategoryModel GetProductCategory(int productId, int categoryId)
        {
            return GetProductCategory(productId, categoryId,null);
        }

        public ProductCategoryModel GetProductCategory(int productId, int categoryId, ExpandCollection expands)
        {
            var endpoint = ProductCategoriesEndpoint.GetProductCategory(productId, categoryId);
            endpoint += BuildEndpointQueryString(expands);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ProductCategoryResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (Equals(response,null)) ? null : response.ProductCategory;
        }

		public ProductCategoryListModel GetProductCategories(ExpandCollection expands, FilterCollection filters, SortCollection sorts)
		{
			return GetProductCategories(expands, filters, sorts, null, null);
		}

		public ProductCategoryListModel GetProductCategories(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
		{
			var endpoint = ProductCategoriesEndpoint.List();
			endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<ProductCategoryListResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

			var list = new ProductCategoryListModel { ProductCategories = (response == null) ? null : response.ProductCategories };
			list.MapPagingDataFromResponse(response);

			return list;
		}

		public ProductCategoryListModel GetProductCategoriesByQuery(string query)
		{
			var endpoint = ProductCategoriesEndpoint.ListByQuery(query);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<ProductCategoryListResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

			var list = new ProductCategoryListModel { ProductCategories = (response == null) ? null : response.ProductCategories };
			list.MapPagingDataFromResponse(response);

			return list;
		}

		public ProductCategoryModel CreateProductCategory(ProductCategoryModel model)
		{
			var endpoint = ProductCategoriesEndpoint.Create();

			var status = new ApiStatus();
			var response = PostResourceToEndpoint<ProductCategoryResponse>(endpoint, JsonConvert.SerializeObject(model), status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

			return (response == null) ? null : response.ProductCategory;
		}

		public ProductCategoryModel UpdateProductCategory(int productCategoryId, ProductCategoryModel model)
		{
			var endpoint = ProductCategoriesEndpoint.Update(productCategoryId);

			var status = new ApiStatus();
			var response = PutResourceToEndpoint<ProductCategoryResponse>(endpoint, JsonConvert.SerializeObject(model), status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

			return (response == null) ? null : response.ProductCategory;
		}

		public bool DeleteProductCategory(int productCategoryId)
		{
			var endpoint = ProductCategoriesEndpoint.Delete(productCategoryId);

			var status = new ApiStatus();
			var deleted = DeleteResourceFromEndpoint<ProductCategoryResponse>(endpoint, status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

			return deleted;
		}
	}
}
