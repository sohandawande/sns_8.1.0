﻿using System.Collections.ObjectModel;
using System.Net;
using Newtonsoft.Json;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
	public class AuditsClient : BaseClient, IAuditsClient
	{
		public AuditModel GetAudit(int auditId)
		{
			return GetAudit(auditId, null);
		}

		public AuditModel GetAudit(int auditId, ExpandCollection expands)
		{
			var endpoint = AuditsEndpoint.Get(auditId);
			endpoint += BuildEndpointQueryString(expands);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<AuditResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

			return (response == null) ? null : response.Audit;
		}

		public AuditListModel GetAudits(ExpandCollection expands, FilterCollection filters, SortCollection sorts)
		{
			return GetAudits(expands, filters, sorts, null, null);
		}

		public AuditListModel GetAudits(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
		{
			var endpoint = AuditsEndpoint.List();
			endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<AuditListResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new AuditListModel { Audits = (response == null) ? null : response.Audits };
			list.MapPagingDataFromResponse(response);

			return list;
		}

		public AuditModel UpdateAudit(int auditId, AuditModel model)
		{
			var endpoint = AuditsEndpoint.Update(auditId);

			var status = new ApiStatus();
			var response = PutResourceToEndpoint<AuditResponse>(endpoint, JsonConvert.SerializeObject(model), status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

			return (response == null) ? null : response.Audit;
		}

		public bool DeleteAudit(int auditId)
		{
			var endpoint = AuditsEndpoint.Delete(auditId);

			var status = new ApiStatus();
			var deleted = DeleteResourceFromEndpoint<AuditResponse>(endpoint, status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

			return deleted;
		}
	}
}
