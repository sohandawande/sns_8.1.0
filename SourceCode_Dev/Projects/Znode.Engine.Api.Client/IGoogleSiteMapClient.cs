﻿using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    /// <summary>
    /// Interface for Google Site Map Client
    /// </summary> 
    public interface IGoogleSiteMapClient
    {
        /// <summary>
        /// Creates Google Site Map for generating XML file
        /// </summary>
        /// <param name="model">GoogleSiteMapModel model</param>
        /// <returns>Returns Created Model</returns>
        GoogleSiteMapModel CreateGoogleSiteMap(GoogleSiteMapModel model);
    }
}
