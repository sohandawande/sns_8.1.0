﻿using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    /// <summary>
    /// Client for Rejection Message.
    /// </summary>
    public class RejectionMessageClient : BaseClient, IRejectionMessageClient
    {
        public RejectionMessageModel GetRejectionMessage(int rejectionMessageId)
        {
            return GetRejectionMessage(rejectionMessageId, null);
        }

        public RejectionMessageModel GetRejectionMessage(int rejectionMessageId, ExpandCollection expands)
        {
            var endpoint = RejectionMessageEndpoint.Get(rejectionMessageId);
            endpoint += BuildEndpointQueryString(expands);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<RejectionMessageResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return Equals(response, null) ? null : response.RejectionMessage;
        }

        public RejectionMessageListModel GetRejectionMessages(ExpandCollection expands, FilterCollection filters, SortCollection sorts)
        {
            return GetRejectionMessages(expands, filters, sorts, null, null);
        }

        public RejectionMessageListModel GetRejectionMessages(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = RejectionMessageEndpoint.List();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<RejectionMessageListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new RejectionMessageListModel { RejectionMessages = Equals(response, null) ? null : response.RejectionMessages };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public RejectionMessageModel CreateRejectionMessage(RejectionMessageModel model)
        {
            var endpoint = RejectionMessageEndpoint.Create();

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<RejectionMessageResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return Equals(response, null) ? null : response.RejectionMessage;
        }

        public RejectionMessageModel UpdateRejectionMessage(int rejectionMessageId, RejectionMessageModel model)
        {
            var endpoint = RejectionMessageEndpoint.Update(rejectionMessageId);

            var status = new ApiStatus();
            var response = PutResourceToEndpoint<RejectionMessageResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return Equals(response, null) ? null : response.RejectionMessage;
        }

        public bool DeleteRejectionMessage(int rejectionMessageId)
        {
            var endpoint = RejectionMessageEndpoint.Delete(rejectionMessageId);

            var status = new ApiStatus();
            var deleted = DeleteResourceFromEndpoint<RejectionMessageResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

            return deleted;
        }
    }
}
