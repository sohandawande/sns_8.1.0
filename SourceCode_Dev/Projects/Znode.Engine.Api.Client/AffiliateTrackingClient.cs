﻿using System.Collections.ObjectModel;
using System.Data;
using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    public class AffiliateTrackingClient : BaseClient, IAffiliateTrackingClient
    {
        #region Public Methods

        public DataSet GetTrackingData(FilterCollection filters = null)
        {
            var endpoint = AffiliateTrackingEndpoint.GetTrackingData();
            endpoint += BuildEndpointQueryString(null, filters, null, null, null);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<AffiliateTrackingResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (Equals(response, null)) ? null : response.TrackingData;
        }

        #endregion
    }
}
