﻿using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface ICaseRequestsClient : IBaseClient
    {
        CaseRequestModel GetCaseRequest(int caseRequestId);
        CaseRequestModel GetCaseRequest(int caseRequestId, ExpandCollection expands);
        CaseRequestListModel GetCaseRequests(ExpandCollection expands, FilterCollection filters, SortCollection sorts);
        CaseRequestListModel GetCaseRequests(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
        CaseRequestModel CreateCaseRequest(CaseRequestModel model);
        CaseRequestModel UpdateCaseRequest(int caseRequestId, CaseRequestModel model);
        bool DeleteCaseRequest(int caseRequestId);

        /// <summary>
        /// Znode Version 8.0
        /// To create case note
        /// </summary>
        /// <param name="model">NoteModel model</param>
        /// <returns>returns NoteModel</returns>
        NoteModel CreateCaseNote(NoteModel model);

        /// <summary>
        /// To Get Case Status
        /// </summary>
        /// <param name="expands"></param>
        /// <param name="filters"></param>
        /// <param name="sorts"></param>
        /// <returns></returns>
        CaseStatusListModel GetCaseStatus(ExpandCollection expands, FilterCollection filters, SortCollection sorts);

        /// <summary>
        /// To Get Case Status
        /// </summary>
        /// <param name="expands"></param>
        /// <param name="filters"></param>
        /// <param name="sorts"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        CaseStatusListModel GetCaseStatus(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// To GetCasePriority
        /// </summary>
        /// <param name="expands"></param>
        /// <param name="filters"></param>
        /// <param name="sorts"></param>
        /// <returns></returns>
        CasePriorityListModel GetCasePriority(ExpandCollection expands, FilterCollection filters, SortCollection sorts);

        /// <summary>
        /// To GetCasePriority
        /// </summary>
        /// <param name="expands"></param>
        /// <param name="filters"></param>
        /// <param name="sorts"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        CasePriorityListModel GetCasePriority(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// To GetCaseNote
        /// </summary>
        /// <param name="expands"></param>
        /// <param name="filters"></param>
        /// <param name="sorts"></param>
        /// <returns></returns> 
        NoteListModel GetCaseNote(ExpandCollection expands, FilterCollection filters, SortCollection sorts);

        /// <summary>
        /// To GetCaseNote
        /// </summary>
        /// <param name="expands"></param>
        /// <param name="filters"></param>
        /// <param name="sorts"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        NoteListModel GetCaseNote(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Contact Us and Customer Request in Service Request
        /// </summary>
        /// <param name="model">Case Request Model</param>
        /// <returns>Returns CaseRequestModel</returns>
        CaseRequestModel SaveContactUsCusotmerFeedbackInfo(CaseRequestModel model);
    }
}
