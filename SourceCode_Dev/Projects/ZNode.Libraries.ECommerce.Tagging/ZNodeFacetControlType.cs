namespace ZNode.Libraries.ECommerce.Tagging
{
    /// <summary>
    /// Specifies the control type of TagGroup
    /// </summary>
    public enum ZNodeFacetControlType
    {
        /// <summary>
        /// Represents the Dropdownlist
        /// </summary>
        DROPDOWNLIST = 1,

        /// <summary>
        /// Represents the label
        /// </summary>
        LABEL = 2,

        /// <summary>
        /// Represents the icons
        /// </summary>
        ICONS = 3,

        /// <summary>
        /// Represents the radio button
        /// </summary>
        RADIOBUTTON = 4 
    }
}
