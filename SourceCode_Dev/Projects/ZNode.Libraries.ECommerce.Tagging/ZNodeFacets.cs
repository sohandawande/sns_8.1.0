using System.Data;
using System.Data.SqlClient;

namespace ZNode.Libraries.ECommerce.Tagging
{
    public class ZNodeFacets
    {
        /// <summary>
        /// Returns Products based on category
        /// </summary>
        /// <param name="FacetIds">Facet Ids for the products</param>
        /// <param name="CategoryId">Category Id</param>
        /// <returns>Returns the products based on the facet ids</returns>
        public DataTable RetrieveProductsByFacetIds(string FacetIds, int CategoryId)
        {
            // Create Instance of Connection and Command Object
            SqlConnection myConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString);
            SqlCommand myCommand = new SqlCommand("ZNode_GetProductsByFacetIDs", myConnection);

            // Mark the Command as a SPROC
            myCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterFacetId = new SqlParameter("@FacetIds", SqlDbType.VarChar);
            parameterFacetId.Value = FacetIds;
            myCommand.Parameters.Add(parameterFacetId);

            // Add Parameters to SPROC
            SqlParameter parameterCategoryId = new SqlParameter("@CategoryId", SqlDbType.Int);
            parameterCategoryId.Value = CategoryId;
            myCommand.Parameters.Add(parameterCategoryId);

            // Execute the command
            myConnection.Open();
            
            DataTable dt = new DataTable();

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = myCommand;

            dataAdapter.Fill(dt);
            
            myConnection.Close();

            return dt;
        }

        /// <summary>
        /// Returns Facets based on category
        /// </summary>
        /// <param name="CategoryId">Categgory ID</param>
        /// <param name="_facetIds">Facet Ids for the category</param>
        /// <returns>Returns the facets by category id</returns>
        public DataSet RetrieveFacetsByCategoryID(int CategoryId, string _facetIds, int _catalogId)
        {
            SqlConnection myConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString);

            SqlDataAdapter myCommand = new SqlDataAdapter("ZNode_GetFacetsByCategoryId", myConnection);

            // Mark the Command as a SPROC
            myCommand.SelectCommand.CommandType = CommandType.StoredProcedure;

            // Add Parameters to SPROC
            SqlParameter parameterCategoryId = new SqlParameter("@CategoryId", SqlDbType.Int, 4);
            parameterCategoryId.Value = CategoryId;
            myCommand.SelectCommand.Parameters.Add(parameterCategoryId);

            // Add Parameters to SPROC
            SqlParameter parameterFacetId = new SqlParameter("@FacetIds", SqlDbType.VarChar);
            parameterFacetId.Value = _facetIds;
            myCommand.SelectCommand.Parameters.Add(parameterFacetId);

            // Add Parameters to SPROC
            SqlParameter parameterCatalogId = new SqlParameter("@CatalogId", SqlDbType.Int, 4);
            parameterCatalogId.Value = _catalogId;
            myCommand.SelectCommand.Parameters.Add(parameterCatalogId);

            // Create and Fill the DataSet
            DataSet myDataSet = new DataSet();
            myCommand.Fill(myDataSet);

            // close connection
            myConnection.Close();

            // Return DataSet
            return myDataSet;
        }

        /// <summary>
        /// Returns Facets for BreadCrumbs
        /// </summary>
        /// <param name="FacetIds">Facet Ids to get breadcrumbs</param>
        /// <returns>Returns the Facets for BreadCrumbs</returns>
        public DataSet GetBreadCrumbsByFacetIds(string FacetIds)
        {
            SqlConnection myConn = null;
            SqlDataAdapter MyAdapter = null;

            // Create Instance of Connection Object
            string ConnectionStr = System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ToString();
            myConn = new SqlConnection(ConnectionStr);

            // CREATE INSTANCE OF SQLDATA ADAPTER
            MyAdapter = new SqlDataAdapter("ZNode_GetBreadCrumbsByFacetIds", myConn);

            // Add Parameters to SPROC
            MyAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            SqlParameter myparam1 = new SqlParameter("@FacetIds", SqlDbType.VarChar);
            myparam1.Value = FacetIds;

            MyAdapter.SelectCommand.Parameters.Add(myparam1);

            // Fill DataSet
            DataSet MyDataset = new DataSet();

            MyAdapter.Fill(MyDataset);

            // Release Resources
            MyAdapter.Dispose();

            myConn.Close();

            // return dataset
            return MyDataset;
        }
    }
}
