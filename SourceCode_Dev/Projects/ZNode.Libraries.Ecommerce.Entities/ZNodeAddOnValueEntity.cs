using System;
using System.Xml.Serialization;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Entities
{
    /// <summary>
    /// Represents the properties and methods of a product AddOn values.
    /// </summary>
    [Serializable()]
    [XmlRoot()]
    public class ZNodeAddOnValueEntity : ZNodeBusinessBase
    {
        #region protected Member Variables
        private int _AddOnId;
        private int _AddOnValueId;
        private string _Sku = String.Empty;
        private string _Name = String.Empty;
        private string _Description = String.Empty;
        private int _QuantityOnHand;
        private decimal _Weight;
        private int _DisplayOrder;
        private decimal _RetailPrice;
        private decimal? _SalePrice;
        private decimal? _WholeSalePrice;
        private decimal _Price;
        private bool _IsDefault;
        private decimal _Height = 0;
        private decimal _Width = 0;
        private decimal _Length = 0;
        private int? _ShippingRuleTypeId;
        private decimal _ShippingCost = 0;
        private bool _FreeShippingInd = false;
        private decimal _DiscountAmount = 0;
        private int _SupplierId;
        private int _TaxClassId = 0;
        private decimal _GST = 0;
        private decimal _HST = 0;
        private decimal _PST = 0;
        private decimal _VAT = 0;
        private decimal _SalesTax = 0;
        private decimal? _InclusiveTaxRate=0;
        private bool _RecurringBillingInd = false;
        private bool _RecurringBillingInstallmentInd = false;
        private string _RecurringBillingPeriod = string.Empty;
        private string _RecurringBillingFrequency = string.Empty;
        private int _RecurringBillingTotalCycles = 0;
        private decimal _RecurringBillingInitialAmount = 0;
        private string _CustomText = string.Empty;
        private int? _ExternalProductID = null;
        private bool _IsTaxCalculated = false;
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the AddOnValue id
        /// </summary>
        [XmlElement()]
        public int AddOnValueID
        {
            get { return this._AddOnValueId; }
            set { this._AddOnValueId = value; }
        }

        /// <summary>
        /// Gets or sets the AddOnid for this AddOn Value
        /// </summary>
        [XmlElement()]
        public int AddOnId
        {
            get { return this._AddOnId; }
            set { this._AddOnId = value; }
        }

        /// <summary>
        /// Gets or sets the Add-on value Name
        /// </summary>
        [XmlElement()]
        public string Name
        {
            get { return this._Name; }
            set { this._Name = value; }
        }

        /// <summary>
        /// Gets or sets the Add-on value description
        /// </summary>
        [XmlElement()]
        public string Description
        {
            get { return this._Description; }
            set { this._Description = value; }
        }

        /// <summary>
        /// Gets or sets the SKU Name
        /// </summary>
        [XmlElement()]
        public string SKU
        {
            get { return this._Sku; }
            set { this._Sku = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the add-on value is default.
        /// </summary>
        [XmlElement()]
        public bool IsDefault
        {
            get { return this._IsDefault; }
            set { this._IsDefault = value; }
        }

        /// <summary>
        /// Gets or sets the qantity on stock for this addonValue
        /// </summary>
        [XmlElement()]
        public int QuantityOnHand
        {
            get { return this._QuantityOnHand; }
            set { this._QuantityOnHand = value; }
        }

        /// <summary>
        /// Gets or sets the order of the display
        /// </summary>
        [XmlElement()]
        public int DisplayOrder
        {
            get { return this._DisplayOrder; }
            set { this._DisplayOrder = value; }
        }

        /// <summary>
        /// Gets or sets the retail price value for this addon value
        /// </summary>
        [XmlElement()]
        public decimal RetailPrice
        {
            get
            {
                return this._RetailPrice;
            }

            set
            {
                this._RetailPrice = value;
            }
        }

        /// <summary>
        /// Gets or sets the sale price value for this addon value
        /// </summary>
        [XmlElement()]
        public decimal? SalePrice
        {
            get
            {
                return this._SalePrice;
            }

            set
            {
                this._SalePrice = value;
            }
        }

        /// <summary>
        /// Gets or sets the wholesale price value for this addon value
        /// </summary>
        [XmlElement()]
        public decimal? WholesalePrice
        {
            get
            {
                return this._WholeSalePrice;
            }

            set
            {
                this._WholeSalePrice = value;
            }
        }

        /// <summary>
        /// Gets a value indicating whether to use wholesale price.
        /// </summary>
        [XmlIgnore()]
        public bool UseWholeSalePrice
        {
            get
            {
                if (System.Web.HttpContext.Current.Session["ProfileCache"] != null)
                {
                    ZNode.Libraries.DataAccess.Entities.Profile profile = (ZNode.Libraries.DataAccess.Entities.Profile)System.Web.HttpContext.Current.Session["ProfileCache"];

                    if (profile.UseWholesalePricing.HasValue)
                    {
                        return profile.UseWholesalePricing.Value;
                    }
                }

                return false;
            }
        }

        /// <summary>
        /// Gets the final price value for this addon value
        /// </summary>
        [XmlIgnore()]
        public decimal FinalPrice
        {
            get
            {
                bool useWholeSalePrice = this.UseWholeSalePrice;
                decimal finalPrice = this.RetailPrice;
                decimal? finalAddOnPrice = this.RetailPrice;
                decimal? decSalePrice = this.SalePrice;
                decimal? decWholeSalePrice = this.WholesalePrice;

                if (decSalePrice.HasValue && !useWholeSalePrice)
                {
                    finalPrice = decSalePrice.Value;
                }

                else if (useWholeSalePrice)
                {
                    // If Wholesaleprice has null ,then ignore it
                    if (decWholeSalePrice.HasValue)
                    {
                        finalPrice = decWholeSalePrice.Value;
                    }
                    else if (decSalePrice.HasValue)
                    {
                        finalPrice = decSalePrice.Value;
                    }
                }

                return finalPrice;
            }
        }

        /// <summary>
        /// Gets the final price value for this addon value
        /// </summary>
        [XmlIgnore()]
        public decimal Price
        {
            get
            {
                bool useWholeSalePrice = this.UseWholeSalePrice;
                this._Price = this._RetailPrice;
                decimal? salePrice = this._SalePrice;
                decimal? wholeSalePrice = this._WholeSalePrice;

                // If sale price has value
                if (salePrice.HasValue && !useWholeSalePrice)
                {
                    // Then set sale price value
                    this._Price = salePrice.Value;
                }
                else if (useWholeSalePrice)
                {
                    // If Wholesaleprice has null ,then ignore it
                    if (wholeSalePrice.HasValue)
                    {
                        this._Price = wholeSalePrice.Value;
                    }
                    else if (salePrice.HasValue)
                    {
                        this._Price = salePrice.Value;
                    }
                }

                return this._Price;
            }
        }

        /// <summary>
        /// Gets or sets the weight of this addon value 
        /// </summary>
        [XmlElement()]
        public decimal Weight
        {
            get
            {
                return this._Weight;
            }

            set
            {
                this._Weight = value;
            }
        }

        /// <summary>
        /// Gets or sets the add-on value package height  
        /// </summary>
        [XmlElement()]
        public decimal Height
        {
            get { return this._Height; }
            set { this._Height = value; }
        }

        /// <summary>
        /// Gets or sets the add-on value package width 
        /// </summary>
        [XmlElement()]
        public decimal Width
        {
            get { return this._Width; }
            set { this._Width = value; }
        }

        /// <summary>
        /// Gets or sets the add-on value package length 
        /// </summary>
        [XmlElement()]
        public decimal Length
        {
            get { return this._Length; }
            set { this._Length = value; }
        }

        /// <summary>
        ///  Gets or sets the add-on value shippingRuleTypeId
        /// </summary>
        public int? ShippingRuleTypeID
        {
            get
            {
                return this._ShippingRuleTypeId;
            }

            set
            {
                this._ShippingRuleTypeId = value;
            }
        }

        /// <summary>
        /// Gets or sets the calculated shipping cost for this addonvalue
        /// </summary>
        [XmlElement()]
        public decimal ShippingCost
        {
            get { return this._ShippingCost; }
            set { this._ShippingCost = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the add-on is free shipping
        /// </summary>
        [XmlElement()]
        public bool FreeShippingInd
        {
            get { return this._FreeShippingInd; }
            set { this._FreeShippingInd = value; }
        }

        /// <summary>
        /// Gets or sets the supplierId for this AddOn Value
        /// </summary>
        [XmlElement()]
        public int SupplierID
        {
            get { return this._SupplierId; }
            set { this._SupplierId = value; }
        }

        /// <summary>
        /// Gets or sets the TaxClassId for this AddOn Value
        /// </summary>
        [XmlElement()]
        public int TaxClassID
        {
            get { return this._TaxClassId; }
            set { this._TaxClassId = value; }
        }

        /// <summary>
        /// Gets or sets the discount amount.
        /// </summary>
        [XmlIgnore()]
        public decimal DiscountAmount
        {
            get
            {
                return this._DiscountAmount;
            }

            set
            {
                this._DiscountAmount = value;
            }
        }

        /// <summary>
        /// Gets or sets of HST
        /// </summary>
        [XmlIgnore()]
        public decimal HST
        {
            get
            {
                return this._HST;
            }

            set
            {
                this._HST = value;
            }
        }

        /// <summary>
        /// Gets or sets of GST
        /// </summary>
        [XmlIgnore()]
        public decimal GST
        {
            get
            {
                return this._GST;
            }

            set
            {
                this._GST = value;
            }
        }

        /// <summary>
        /// Gets or sets of PST
        /// </summary>
        [XmlIgnore()]
        public decimal PST
        {
            get
            {
                return this._PST;
            }

            set
            {
                this._PST = value;
            }
        }

        /// <summary>
        /// Gets or sets the VAT
        /// </summary>
        [XmlIgnore()]
        public decimal VAT
        {
            get
            {
                return this._VAT;
            }

            set
            {
                this._VAT = value;
            }
        }

        /// <summary>
        /// Gets or sets sales tax
        /// </summary>
        [XmlIgnore()]
        public decimal SalesTax
        {
            get
            {
                return this._SalesTax;
            }

            set
            {
                this._SalesTax = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the recurring billing is enabled
        /// </summary>
        [XmlElement()]
        public bool RecurringBillingInd
        {
            get
            {
                return this._RecurringBillingInd;
            }

            set
            {
                this._RecurringBillingInd = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether installmentInd  is enabled.
        /// </summary>
        [XmlElement()]
        public bool RecurringBillingInstallmentInd
        {
            get
            {
                return this._RecurringBillingInstallmentInd;
            }

            set
            {
                this._RecurringBillingInstallmentInd = value;
            }
        }

        /// <summary>
        /// Gets or sets the billing period (days or months), in association with the frequency.
        /// </summary>
        [XmlElement()]
        public string RecurringBillingPeriod
        {
            get
            {
                return this._RecurringBillingPeriod;
            }

            set
            {
                this._RecurringBillingPeriod = value;
            }
        }

        /// <summary>
        /// Gets or sets the billing frequency, in association with the Period.
        /// </summary>
        [XmlElement()]
        public string RecurringBillingFrequency
        {
            get
            {
                return this._RecurringBillingFrequency;
            }

            set
            {
                this._RecurringBillingFrequency = value;
            }
        }

        /// <summary>
        /// Gets or sets the number of billing occurrences or payments for the subscription
        /// </summary>
        [XmlElement()]
        public int RecurringBillingTotalCycles
        {
            get
            {
                return this._RecurringBillingTotalCycles;
            }

            set
            {
                this._RecurringBillingTotalCycles = value;
            }
        }

        /// <summary>
        /// Gets or sets the initial amount to be charged during subscription creation,
        /// </summary>
        [XmlElement()]
        public decimal RecurringBillingInitialAmount
        {
            get
            {
                return this._RecurringBillingInitialAmount;
            }

            set
            {
                this._RecurringBillingInitialAmount = value;
            }
        }

        /// <summary>
        /// Gets or sets the Add-on value description
        /// </summary>
       [XmlElement()]
        public string CustomText
        {
            get { return this._CustomText; }
            set { this._CustomText = value; }
        }

        /// <summary>
        /// Gets or sets the initial amount to be charged during subscription creation,
        /// </summary>
        [XmlElement()]
        public int? ExternalProductID
        {
            get
            {
                return this._ExternalProductID;
            }

            set
            {
                this._ExternalProductID = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether is tax already calculated for the current shopping cart.
        /// </summary>
        public bool IsTaxCalculated
        {
            get { return this._IsTaxCalculated; }
            set { this._IsTaxCalculated = value; }
        }
        #endregion

        #region Protected Properties
        /// <summary>
        /// Gets inclusive tax rate.
        /// </summary>
        [XmlIgnore()]
        protected decimal InclusiveTaxRate
        {
            get
            {
                return this._InclusiveTaxRate.GetValueOrDefault(0);
            }
        }
        #endregion

		#region Clone 
		
		public ZNodeAddOnValueEntity Clone()
		{
			return this.MemberwiseClone() as ZNodeAddOnValueEntity;
		}

		#endregion 
	}
}
