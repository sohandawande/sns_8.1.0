using System;
using System.Xml.Serialization;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Entities
{
    /// <summary>
    /// Represents a product tier price
    /// </summary>    
    [Serializable()]
    [XmlRoot("ZNodeProductTier")]
    public class ZNodeProductTierEntity : ZNodeBusinessBase
    {
        private int _ProductTierId;
        private int _ProductId;
        private int _ProfileId;
        private int _TierStart;
        private int _TierEnd;
        private decimal _Price;

        /// <summary>
        /// Gets or sets the product tier Id.
        /// </summary>
        [XmlElement()]
        public int ProductTierID
        {
            get 
            {
                return this._ProductTierId;
            }

            set 
            {
                this._ProductTierId = value;
            }
        }

        /// <summary>
        /// Gets or sets the product Id.
        /// </summary>
        [XmlElement()]
        public int ProductID
        {
            get
            {
                return this._ProductId;
            }
            
            set
            {
                this._ProductId = value;
            }
        }

        /// <summary>
        /// Gets or sets the profile Id.
        /// </summary>
        [XmlElement()]
        public int ProfileID
        {
            get
            {
                return this._ProfileId;
            }

            set
            {
                this._ProfileId = value;
            }
        }

        /// <summary>
        /// Gets or sets the tier start.
        /// </summary>
        [XmlElement()]
        public int TierStart
        {
            get
            {
                return this._TierStart;
            }

            set
            {
                this._TierStart = value;
            }
        }
        
        /// <summary>
        /// Gets or sets the tier end.
        /// </summary>
        [XmlElement()]
        public int TierEnd
        {
            get
            {
                return this._TierEnd;
            }

            set
            {
                this._TierEnd = value;
            }
        }
        
        /// <summary>
        /// Gets or sets the price.
        /// </summary>
        [XmlElement()]
        public decimal Price
        {
            get
            {
                return this._Price;
            }

            set
            {
                this._Price = value;
            }
        }
    }
}
