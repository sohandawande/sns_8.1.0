using System;
using System.Text;
using System.Linq;
using System.Xml.Serialization;
using ZNode.Libraries.Framework.Business;
using System.Collections.Generic;

namespace ZNode.Libraries.ECommerce.Entities
{
    /// <summary>
    /// Represents the properties and methods of a product AddOn.
    /// </summary>
    [Serializable()]
    [XmlRoot()]
    public class ZNodeAddOnListEntity : ZNodeBusinessBase
    {
        #region Private Memeber Variables
        private string _selectedAddOnValueIds = string.Empty;
        private Dictionary<int, string> _selectedAddOnValueCustomText = null;
        private ZNodeGenericCollection<ZNodeAddOnEntity> _addOnCollection = new ZNodeGenericCollection<ZNodeAddOnEntity>();
        #endregion

        /// <summary>
        /// Initializes a new instance of the ZNodeAddOnListEntity class
        /// </summary>
        public ZNodeAddOnListEntity()
        {
        }

		public ZNodeAddOnListEntity Clone()
		{
			var copiedItem = this.MemberwiseClone() as ZNodeAddOnListEntity;

			var collection = new ZNodeGenericCollection<ZNodeAddOnEntity>();
			foreach (ZNodeAddOnEntity item in AddOnCollection)
				collection.Add(item.Clone());
			copiedItem.AddOnCollection = collection;
			
			return copiedItem;
		}

        /// <summary>
        /// Gets or sets the products contained within this category
        /// </summary>
        [XmlElement("ZNodeAddOn")]
        public ZNodeGenericCollection<ZNodeAddOnEntity> AddOnCollection
        {
            get
            {
                return this._addOnCollection;
            }

            set
            {
                this._addOnCollection = value;
            }
        }

        /// <summary>
        /// Gets the total shipping cost of all the selected addon values
        /// </summary>
        [XmlIgnore()]
        public decimal TotalShippingCost
        {
            get
            {
                decimal shippingCost = 0;

                foreach (ZNodeAddOnEntity addOn in this._addOnCollection)
                {
                    foreach (ZNodeAddOnValueEntity value in addOn.AddOnValueCollection)
                    {
                        shippingCost += value.ShippingCost;
                    }
                }

                return shippingCost;
            }
        }

        /// <summary>
        /// Gets the total Add-on cost of all the selected addon values
        /// </summary>
        [XmlIgnore()]
        public decimal TotalAddOnCost
        {
            get
            {
                decimal addOnCost = 0;

                foreach (ZNodeAddOnEntity addOn in this._addOnCollection)
                {
                    foreach (ZNodeAddOnValueEntity value in addOn.AddOnValueCollection)
                    {
                        addOnCost += value.FinalPrice;
                    }
                }

                return addOnCost;
            }
        }

		/// <summary>
		/// Gets the total Add-on cost of all the selected addon values
		/// </summary>
		[XmlIgnore()]
		public decimal TotalAddOnRetailCost
		{
			get
			{
				decimal addOnCost = 0;

				foreach (ZNodeAddOnEntity addOn in this._addOnCollection)
				{
					foreach (ZNodeAddOnValueEntity value in addOn.AddOnValueCollection)
					{
						addOnCost += value.RetailPrice;
					}
				}

				return addOnCost;
			}
		}

        /// <summary>
        /// Gets or sets the addonIds for all the selected addon values
        /// </summary>
        [XmlIgnore()]
        public string SelectedAddOnValueIds
        {
            get
            {
                return this._selectedAddOnValueIds;
            }

            set
            {
                this._selectedAddOnValueIds = value;
            }
        }

        /// <summary>
        /// Gets or sets the addonIds for all the selected addon values custom text
        /// </summary>
        [XmlIgnore()]
        public Dictionary<int, string> SelectedAddOnValueCustomText
        {
            get
            {
                return this._selectedAddOnValueCustomText;
            }

            set
            {
                this._selectedAddOnValueCustomText = value;
            }
        }

        /// <summary>
        /// Gets the shopping cart selected addons description        
        /// This metadata is displayed on the shopping cart list
        /// </summary>
        [XmlIgnore()]
        public string ShoppingCartAddOnsDescription
        {
            get
            {
                StringBuilder addOnDescription = new StringBuilder();

                foreach (ZNodeAddOnEntity addOn in this._addOnCollection)
                {
                    foreach (ZNodeAddOnValueEntity addOnValue in addOn.AddOnValueCollection)
                    {
                        decimal decPrice = addOnValue.FinalPrice;

                        if (string.Compare(addOn.DisplayType, "TextBox", true) == 0)
                        {
                            addOnDescription.Append(addOnValue.Name);
                            addOnDescription.Append(" - ");
							addOnDescription.Append(string.IsNullOrEmpty(addOnValue.CustomText)
														? addOnValue.Description
														: addOnValue.CustomText);
                        }
                        else
                        {
                            addOnDescription.Append(addOn.Name);
                            addOnDescription.Append(" - ");
                            addOnDescription.Append(addOnValue.Name);
                        }

                        addOnDescription.Append("<br />");
                    }
                }

                return addOnDescription.ToString();
            }
        }
    }
}
