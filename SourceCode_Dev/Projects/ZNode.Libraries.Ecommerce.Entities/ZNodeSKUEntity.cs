using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Entities
{
    /// <summary>
    /// Represents the properties and methods of a ZNodeSKU.
    /// </summary>
    [Serializable()]
    [XmlRoot()]
    public class ZNodeSKUEntity : ZNodeBusinessBase
    {
        #region Private Member Variables
        private int _SkuId;
        private int _ProductId;
        private string _Sku = String.Empty;
        private int _WarehouseNo;
        private string _Note = String.Empty;
        private decimal _Surcharge;
        private int _QuantityOnHand;
        private int _QuantityBuffer;
        private int _ReorderLevel;
        private decimal _WeightAdditional;
        private string _SkuPicturePath = String.Empty;
        private int _DisplayOrder;
        private decimal? _RetailPrice;
        private decimal? _SalePrice;
        private decimal? _WholesalePrice;
        private bool _IsActive;
        private string _AttributesDescription;
        private string _AttributesValue = string.Empty;
        private string _Custom1 = string.Empty;
        private string _Custom2 = string.Empty;
        private string _Custom3 = string.Empty;
        private string _ImageAltTag = string.Empty;
        private int _SupplierId;
        private string _RecurringBillingPeriod = string.Empty;
        private string _RecurringBillingFrequency = string.Empty;
        private int _RecurringBillingTotalCycles = 0;
        private decimal _RecurringBillingInitialAmount = 0;
        private string _ExternalID = string.Empty;
       
        private ZNodeGenericCollection<ZNodeSKUProfile> _skuProfile = new ZNodeGenericCollection<ZNodeSKUProfile>();

        #endregion

        #region Constructor
        /// <summary>
        /// Initializes a new instance of the ZNodeSKUEntity class
        /// </summary>
        public ZNodeSKUEntity()
        {
        }
        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the product attributetypes for this product
        /// </summary>
        [XmlElement("ZNodeSKUProfile")]
        public ZNodeGenericCollection<ZNodeSKUProfile> SkuProfileCollection
        {
            get
            {
                return this._skuProfile;
            }

            set
            {
                this._skuProfile = value;
            }
        }

        /// <summary>
        /// Gets or sets the SKU id
        /// </summary>
        [XmlElement()]
        public int SKUID
        {
            get { return this._SkuId; }
            set { this._SkuId = value; }
        }

        /// <summary>
        /// Gets or sets the product Id for this sku.
        /// </summary>
        [XmlElement()]
        public int ProductID
        {
            get { return this._ProductId; }
            set { this._ProductId = value; }
        }

        /// <summary>
        /// Gets or sets the SKU Name
        /// </summary>
        [XmlElement()]
        public string SKU
        {
            get { return this._Sku; }
            set { this._Sku = value; }
        }

        /// <summary>
        /// Gets or sets the Warehouse number for this SKU
        /// </summary>
        [XmlElement()]
        public int WarehouseNo
        {
            get { return this._WarehouseNo; }
            set { this._WarehouseNo = value; }
        }

        /// <summary>
        /// Gets or sets the text notes for this SKU
        /// </summary>
        [XmlElement()]
        public string Note
        {
            get { return this._Note; }
            set { this._Note = value; }
        }

        /// <summary>
        /// Gets or sets the sur-charge for this SKU
        /// </summary>
        [XmlElement()]
        public decimal Surcharge
        {
            get { return this._Surcharge; }
            set { this._Surcharge = value; }
        }

        /// <summary>
        /// Gets or sets the qantity on stock for this SKU
        /// </summary>
        [XmlElement()]
        public int QuantityOnHand
        {
            get { return this._QuantityOnHand; }
            set { this._QuantityOnHand = value; }
        }

        /// <summary>
        /// Gets or sets the quantity buffer value for this SKU
        /// </summary>
        [XmlElement()]
        public int QuantityBuffer
        {
            get { return this._QuantityBuffer; }
            set { this._QuantityBuffer = value; }
        }

        /// <summary>
        /// Gets or sets the reorder level for this SKU. 
        /// If the quantity on stock reaches this reorder level, then it will send email to sales department
        /// </summary>
        [XmlElement()]
        public int ReorderLevel
        {
            get { return this._ReorderLevel; }
            set { this._ReorderLevel = value; }
        }

        /// <summary>
        /// Gets or sets the additional weight value for this SKU
        /// </summary>
        [XmlElement()]
        public decimal WeightAdditional
        {
            get { return this._WeightAdditional; }
            set { this._WeightAdditional = value; }
        }

        /// <summary>
        /// Gets or sets fully qualified image file path
        /// </summary>
        [XmlElement()]
        public string SKUPicturePath
        {
            get { return this._SkuPicturePath; }
            set { this._SkuPicturePath = value; }
        }

        /// <summary>
        /// Gets or sets the order of the display
        /// </summary>
        [XmlElement()]
        public int DisplayOrder
        {
            get { return this._DisplayOrder; }
            set { this._DisplayOrder = value; }
        }

        /// <summary>
        /// Gets or sets the override retail price value for this SKU
        /// </summary>
        [XmlElement()]
        public decimal? RetailPriceOverride
        {
            get { return this._RetailPrice; }
            set { this._RetailPrice = value; }
        }

        /// <summary>
        /// Gets or sets the override sale price value for this SKU
        /// </summary>
        [XmlElement()]
        public decimal? SalePriceOverride
        {
            get { return this._SalePrice; }
            set { this._SalePrice = value; }
        }

        /// <summary>
        /// Gets or sets the wholesale price value for this SKU
        /// </summary>
        [XmlElement()]
        public decimal? WholesalePriceOverride
        {
            get { return this._WholesalePrice; }
            set { this._WholesalePrice = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the sku is active.
        /// </summary>
        [XmlElement()]
        public bool IsActive
        {
            get { return this._IsActive; }
            set { this._IsActive = value; }
        }

        /// <summary>
        /// Gets or sets the attributes description. Stores metadata on this sku (attribute selection, etc)
        /// This metadata is displayed on the shopping cart list
        /// This property is set explicily by the UI and is not retrieved automatically
        /// </summary>
        [XmlElement()]
        public string AttributesDescription
        {
            get { return this._AttributesDescription; }
            set { this._AttributesDescription = value; }
        }

        /// <summary>
        /// Gets or sets the attribute ids for this SKU
        /// </summary>
        [XmlIgnore()]
        public string AttributesValue
        {
            get { return this._AttributesValue; }
            set { this._AttributesValue = value; }
        }

        /// <summary>
        /// Gets or sets the SKU Custom1
        /// </summary>
        [XmlElement()]
        public string Custom1
        {
            get { return this._Custom1; }
            set { this._Custom1 = value; }
        }

        /// <summary>
        /// Gets or sets the SKU Custom2
        /// </summary>
        [XmlElement()]
        public string Custom2
        {
            get { return this._Custom2; }
            set { this._Custom2 = value; }
        }

        /// <summary>
        /// Gets or sets the SKU Custom3
        /// </summary>
        [XmlElement()]
        public string Custom3
        {
            get { return this._Custom3; }
            set { this._Custom3 = value; }
        }

        /// <summary>
        /// Gets or sets the image alternative tag text
        /// </summary>
        [XmlElement()]
        public string ImageAltTag
        {
            get
            {
                return this._ImageAltTag;
            }
            
            set
            {
                this._ImageAltTag = value;
            }
        }

        /// <summary>
        /// Gets or sets the supplierid for this SKU
        /// </summary>
        [XmlElement()]
        public int SupplierID
        {
            get { return this._SupplierId; }
            set { this._SupplierId = value; }
        }

        /// <summary>
        /// Gets or sets the billing period (days or months), in association with the frequency.
        /// </summary>
        [XmlElement()]
        public string RecurringBillingPeriod
        {
            get
            {
                return this._RecurringBillingPeriod;
            }

            set
            {
                this._RecurringBillingPeriod = value;
            }
        }

        /// <summary>
        /// Gets or sets the billing frequency, in association with the Period.
        /// </summary>
        [XmlElement()]
        public string RecurringBillingFrequency
        {
            get
            {
                return this._RecurringBillingFrequency;
            }

            set
            {
                this._RecurringBillingFrequency = value;
            }
        }

        /// <summary>
        /// Gets or sets the number of billing occurrences or payments for the subscription
        /// </summary>
        [XmlElement()]
        public int RecurringBillingTotalCycles
        {
            get
            {
                return this._RecurringBillingTotalCycles;
            }

            set
            {
                this._RecurringBillingTotalCycles = value;
            }
        }

        /// <summary>
        /// Gets or sets the number of billing occurrences or payments for the subscription
        /// </summary>
        [XmlElement()]
        public string ExternalID
        {
            get
            {
                return this._ExternalID;
            }

            set
            {
                this._ExternalID = value;
            }
        }
        
        /// <summary>
        /// Gets or sets the initial amount to be charged during subscription creation,
        /// </summary>
        [XmlElement()]
        public decimal RecurringBillingInitialAmount
        {
            get { return this._RecurringBillingInitialAmount; }
            set { this._RecurringBillingInitialAmount = value; }
        }

        /// <summary>
        /// Gets or sets the initial amount to be charged during subscription creation,
        /// </summary>
        [XmlElement()]
        public decimal? NegotiatedPrice { get; set; }
        #endregion
    }
}
