
using System;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Entities
{
    /// <summary>
    /// Provides an instance properties for Credit card information
    /// </summary>
    [Serializable()]
    public class GatewayToken : ZNodePaymentBase
    {
        #region Private Member Variables
        private string _AuthCode = string.Empty;
        private string _CardExp = string.Empty;
        private string _CardSecurityCode = string.Empty;
        private string _TransactionId = string.Empty;        
        #endregion

        #region Credit Card instance Properties
        /// <summary>
        /// Gets or sets the Credit card expiration date property
        /// </summary>
        public string CreditCardExp
        {
            get
            {
                return this._CardExp;
            }

            set
            {
                this._CardExp = value;
            }
        }

        /// <summary>
        /// Gets or sets the Credit card Security code property
        /// </summary>
        public string CardSecurityCode
        {
            get
            { 
                return this._CardSecurityCode; 
            }

            set
            {
                this._CardSecurityCode = value;
            }
        }

        /// <summary>
        /// Gets or sets the transaction Id
        /// </summary>
        public string TransactionID
        {
            get { return this._TransactionId; }
            set { this._TransactionId = value; }
        }

        /// <summary>
        /// Gets or sets the authorization code
        /// </summary>
        public string AuthCode
        {
            get { return this._AuthCode; }
            set { this._AuthCode = value; }
        }
        #endregion
    }
}

