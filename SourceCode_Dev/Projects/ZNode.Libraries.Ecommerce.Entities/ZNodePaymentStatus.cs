
namespace ZNode.Libraries.ECommerce.Entities
{
    /// <summary>
    /// Specifies the status of the Order
    /// </summary>
    public enum ZNodePaymentStatus
    {
        /// <summary>
        /// Credit card Authorized
        /// </summary>
        CREDIT_AUTHORIZED = 0,

        /// <summary>
        /// Credir Card Captured.
        /// </summary>
        CREDIT_CAPTURED = 1,

        /// <summary>
        /// Card Card Declined
        /// </summary>
        CREDIT_DECLINED = 2,

        /// <summary>
        /// Card Card Refunded
        /// </summary>
        CREDIT_REFUNDED = 3,

        /// <summary>
        /// Credit Card Payment was Voided
        /// </summary>
        CREDIT_VOIDED = 4,

        /// <summary>
        /// Credit Card Payment Pending
        /// </summary>
        CREDIT_PENDING = 5,

        /// <summary>
        /// Purchase Order Pending
        /// </summary>
        PO_PENDING = 10,

        /// <summary>
        /// Purchase Order Received
        /// </summary>
        PO_RECEIVED = 11,

        /// <summary>
        /// COD Pending
        /// </summary>
        COD_PENDING = 20,

        /// <summary>
        /// COD Received
        /// </summary>
        COD_RECEIVED = 21,
    }
}
