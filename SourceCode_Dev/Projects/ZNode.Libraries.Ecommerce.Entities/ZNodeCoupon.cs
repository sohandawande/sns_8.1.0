﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZNode.Libraries.ECommerce.Entities
{
    /// <summary>
    /// Represents Coupon Information applied on shopping cart.
    /// </summary>
    public class ZNodeCoupon
    {
        private string _CouponMessage = string.Empty;
        private bool _CouponApplied = false;

        /// <summary>
        /// Gets or sets the Znode coupon applied by the user.
        /// </summary>
        public string Coupon { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the coupon applied
        /// </summary>
        public bool CouponApplied
        {
            get
            {
                return this._CouponApplied;
            }
            set
            {
                this._CouponApplied = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the coupon valid.
        /// </summary>
        public bool CouponValid { get; set; }


        /// <summary>
        /// Gets or sets the Coupon Message
        /// </summary>
        public string CouponMessage
        {
            get
            {
                return this._CouponMessage;
            }

            set
            {
                this._CouponMessage = value;
            }
        }

        public int DisplayOrder { get; set; }

        public bool AllowsMultipleCoupon { get; set; }
    }
}
