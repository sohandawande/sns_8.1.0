using System;
using System.Xml.Serialization;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Entities
{
    #region ZNodeCategoryBase
    /// <summary>
    /// Represents the properties and methods of a ZNodeCategory.
    /// </summary>
    [Serializable()]
    [XmlRoot()]
    public class ZNodeCategoryBase : ZNodeBusinessBase
    {
        #region Private Member Variables
        private int _CategoryId;
        private int _PortalId;
        private int _CategoryTemplateId;
        private string _Name = String.Empty;
        private string _Description = String.Empty;
        private string _ShortDescription = String.Empty;
        private int _ParentCategoryId;        
        private int _DisplayOrder;
        private string _ImageFile = String.Empty;
        private bool _VisibleInd;
        private string _Title = String.Empty;
        private bool _SubCategoryGridVisibleInd;
        private string _SeoTitle = String.Empty;
        private string _SeoKeywords = String.Empty;
        private string _SeoDescription = String.Empty;
        private string _SeoUrl = String.Empty;
        private string _Custom1 = String.Empty;
        private string _Custom2 = String.Empty;
        private string _Custom3 = String.Empty;
        private string _MasterPage = string.Empty;
        private string _Theme = string.Empty;
        private string _Css = string.Empty;
        private string _ImageAltTag = string.Empty;        
        private string _AlternateDescription = string.Empty;
        //Znode version 7.2.2 To set Category Banner value
        private string _CategoryBanner = string.Empty;
        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the category UniqueID
        /// </summary>
        [XmlElement()]
        public int CategoryID
        {
            get { return this._CategoryId; }
            set { this._CategoryId = value; }
        }        

        /// <summary>
        /// Gets or sets the site portalID
        /// </summary>
        [XmlElement()]
        public int PortalID
        {
            get { return this._PortalId; }
            set { this._PortalId = value; }
        }

        /// <summary>
        /// Gets or sets the category templateID
        /// </summary>
        [XmlElement()]
        public int CategoryTemplateID
        {
            get { return this._CategoryTemplateId; }
            set { this._CategoryTemplateId = value; }
        }

        /// <summary>
        /// Gets or sets the category Name
        /// </summary>
        [XmlElement()]
        public string Name
        {
            get { return this._Name; }
            set { this._Name = value; }
        }

        /// <summary>
        /// Gets or sets the category description
        /// </summary>
        [XmlElement()]
        public string Description
        {
            get { return this._Description; }
            set { this._Description = value; }
        }

        /// <summary>
        /// Gets or sets the category short description
        /// </summary>
        [XmlElement()]
        public string ShortDescription
        {
            get { return this._ShortDescription; }
            set { this._ShortDescription = value; }
        }        

        /// <summary>
        /// Gets or sets the category Alternate description
        /// </summary>
        [XmlElement()]
        public string AlternateDescription
        {
            get { return this._AlternateDescription; }
            set { this._AlternateDescription = value; }
        }

        /// <summary>
        /// Gets or sets the category Banner
        /// </summary>
        [XmlElement()]
        public string CategoryBanner
        {
            get { return this._CategoryBanner; }
            set { this._CategoryBanner = value; }
        }

        /// <summary>
        /// Gets or sets the parent categoryId for this category
        /// </summary>
        [XmlElement()]
        public int ParentCategoryID
        {
            get { return this._ParentCategoryId; }
            set { this._ParentCategoryId = value; }
        }

        /// <summary>
        /// Gets or sets the display order number
        /// </summary>
        [XmlElement()]
        public int DisplayOrder
        {
            get { return this._DisplayOrder; }
            set { this._DisplayOrder = value; }
        }

        /// <summary>
        /// Gets or sets the image file name
        /// </summary>
        [XmlElement()]
        public string ImageFile
        {
            get { return this._ImageFile; }
            set { this._ImageFile = value; }
        }

        /// <summary>
        /// Gets the fully qualified image file path
        /// </summary>
        [XmlIgnore()]
        public string ImageFilePath
        {
            get
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeImage znodeImage = new ZNode.Libraries.ECommerce.Utilities.ZNodeImage();
                return znodeImage.GetImageHttpPathLarge(this.ImageFile);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the category visible Ind
        /// </summary>
        [XmlElement()]
        public bool VisibleInd
        {
            get { return this._VisibleInd; }
            set { this._VisibleInd = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the visibility is enabled
        /// </summary>
        [XmlElement()]
        public bool SubCategoryGridVisibleInd
        {
            get { return this._SubCategoryGridVisibleInd; }
            set { this._SubCategoryGridVisibleInd = value; }
        }

        /// <summary>
        /// Gets or sets the Category Title
        /// </summary>
        [XmlElement()]
        public string Title
        {
            get { return this._Title; }
            set { this._Title = value; }
        }

        /// <summary>
        /// Gets or sets the SEO Description
        /// </summary>
        [XmlElement()]
        public string SEODescription
        {
            get { return this._SeoDescription; }
            set { this._SeoDescription = value; }
        }

        /// <summary>
        /// Gets or sets the SEO Keywords
        /// </summary>
        [XmlElement()]
        public string SEOKeywords
        {
            get { return this._SeoKeywords; }
            set { this._SeoKeywords = value; }
        }

        /// <summary>
        /// Gets or sets the SEO Title
        /// </summary>
        [XmlElement()]
        public string SEOTitle
        {
            get { return this._SeoTitle; }
            set { this._SeoTitle = value; }
        }

        /// <summary>
        /// Gets or sets the SEO firendly URL
        /// </summary>
        [XmlElement()]
        public string SEOURL
        {
            get { return this._SeoUrl; }
            set { this._SeoUrl = value; }
        }

        /// <summary>
        /// Gets or sets the Category Custom1
        /// </summary>
        [XmlElement()]
        public string Custom1
        {
            get { return this._Custom1; }
            set { this._Custom1 = value; }
        }

        /// <summary>
        /// Gets or sets the Category Custom2
        /// </summary>
        [XmlElement()]
        public string Custom2
        {
            get { return this._Custom2; }
            set { this._Custom2 = value; }
        }

        /// <summary>
        /// Gets or sets the Category Custom3
        /// </summary>
        [XmlElement()]
        public string Custom3
        {
            get { return this._Custom3; }
            set { this._Custom3 = value; }
        }

        /// <summary>
        /// Gets or sets the master page template for this category
        /// </summary>
        [XmlElement()]
        public string MasterPage
        {
            get { return this._MasterPage; }
            set { this._MasterPage = value; }
        }

        /// <summary>
        ///  Gets or sets the Theme for this category
        /// </summary>
        [XmlElement()]
        public string Theme
        {
            get { return this._Theme; }
            set { this._Theme = value; }
        }

        /// <summary>
        ///  Gets or sets the CSS for this category
        /// </summary>
        [XmlElement()]
        public string Css
        {
            get { return this._Css; }
            set { this._Css = value; }
        }

        /// <summary>
        /// Gets or sets the image alternative tag text for this category
        /// </summary>
        [XmlElement()]
        public string ImageAltTag
        {
            get
            {
                // If image Alt tag value not exists then use category title
                if (string.IsNullOrEmpty(this._ImageAltTag))
                {
                    return this._Title;
                }

                // Otherwise, return Alternative text
                return this._ImageAltTag;
            }

            set
            {
                this._ImageAltTag = value;
            }
        }      
        #endregion
    }
    #endregion
}
