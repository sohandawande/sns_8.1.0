using System;
using System.Linq;
using System.Collections.Generic;
using System.Xml.Serialization;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Entities
{
    /// <summary>
    /// Represents a product items in the shopping cart
    /// </summary>
	[Serializable()]
	public class ZNodeShoppingCartItem : ZNodeBusinessBase
	{
		#region Private Member Variables
		private string _Guid = string.Empty;		
		protected decimal _ExtendedPrice = 0;
		private decimal _ShippingCost = 0;
		private string _PromoDescription = string.Empty;

        //PRFT Custom Code : Start
        protected decimal _erpExtendedPrice = 0;
        //PRFT Custom Code : End

        #endregion

        #region Constructors

        public List<ZNodeOrderShipment> OrderShipments { get; set; }

		/// <summary>
		/// Initializes a new instance of the ZNodeShoppingCartItem class. Create GUID value for the shopping cart item
		/// </summary>
		public ZNodeShoppingCartItem()
		{
			// Create GUID
			this._Guid = System.Guid.NewGuid().ToString();
			OrderShipments = new List<ZNodeOrderShipment>();
			IsTaxCalculated = false;
		}
		#endregion

		#region Public Properties

		/// <summary>
		/// Gets the database shopping cart line item ID for this cart item.
		/// </summary>
		public int LineItemId { get; set; }

		/// <summary>
		/// Gets the GUID (unique value) for this Shopping cart item. 
		/// </summary>
		[XmlElement()]
		public string GUID
		{
			get
			{
				return this._Guid;
			}
		}

		/// <summary>
		/// Gets or sets the product object for this cart item
		/// </summary>        
		public ZNodeProductBaseEntity Product { get; set; }

		/// <summary>
		/// Gets the unit price of this line item.
		/// </summary>        
		public decimal UnitPrice
		{
			get
			{
				decimal basePrice = this.TieredPricing;

				decimal unitPrice = basePrice + this.Product.AddOnPrice;

				return unitPrice;
			}
		}

		/// <summary>
		/// Gets or sets the product extended price
		/// </summary>       
		public decimal ExtendedPrice
		{
			get
			{
				this._ExtendedPrice = this.UnitPrice * this.Quantity;

				return this._ExtendedPrice;
			}

			set
			{
				this._ExtendedPrice = value;
			}
		}

		/// <summary>
		/// Gets the product tiered pricing. Returns the tiered Price for this product based on the quantity.
		/// If no tiered pricing applied, it will return the product base price.
		/// </summary>
		public decimal TieredPricing
		{
			get
			{
				int profileId = 0;
				ZNodeGenericCollection<ZNodeProductTierEntity> productTiers = this.Product.ZNodeTieredPriceCollection;
				int quantity = this.Quantity;
				decimal finalPrice = this.Product.FinalPrice;

				// Check product tiers list
				if (productTiers.Count > 0)
				{
					// Get user profile from ProfileCache Session object
					if (System.Web.HttpContext.Current.Session["ProfileCache"] != null)
					{
						ZNode.Libraries.DataAccess.Entities.Profile profile = (ZNode.Libraries.DataAccess.Entities.Profile)System.Web.HttpContext.Current.Session["ProfileCache"];

						if (profile != null)
						{
							profileId = profile.ProfileID;
						}
					}

					foreach (ZNodeProductTierEntity productTieredPrice in productTiers)
					{
						if (quantity >= productTieredPrice.TierStart && productTieredPrice.TierEnd >= quantity)
						{
							if (productTieredPrice.ProfileID == 0)
							{
								finalPrice = productTieredPrice.Price;
								break;
							}
							else
							{
								if (profileId == productTieredPrice.ProfileID)
								{
									finalPrice = productTieredPrice.Price;
									break;
								}
							}
						}
					}
				}				

				// Get Product tiered price if one exists, otherwise unit price
				return finalPrice;
			}
		}

		/// <summary>
		/// Gets or sets the qunatity of products for this line item.
		/// </summary>        
		public int Quantity { get; set; }

		/// <summary>
		/// Gets or sets the shipping cost for this line item.
		/// </summary>        
		public decimal ShippingCost
		{
			get
			{
				decimal shipCost = 0;

				foreach (ZNodeAddOnEntity addOn in this.Product.SelectedAddOns.AddOnCollection)
				{
					foreach (ZNodeAddOnValueEntity addOnvalue in addOn.AddOnValueCollection)
					{
						shipCost += addOnvalue.ShippingCost;
					}
				}

				return this._ShippingCost + shipCost + this.Product.ShippingCost;
			}

			set
			{
				this._ShippingCost = value;
			}
		}

		/// <summary>
		///  Gets the tax cost for this line item.
		/// </summary>        
		public decimal TaxCost
		{
			get
			{
				decimal taxGST = 0;
				decimal taxPST = 0;
				decimal taxHST = 0;
				decimal taxVAT = 0;
				decimal taxSalesTax = 0;
				decimal taxCost = this.Product.VAT + this.Product.GST + this.Product.HST + this.Product.PST + this.Product.SalesTax;

				foreach (ZNodeAddOnEntity addOn in this.Product.SelectedAddOns.AddOnCollection)
				{
					foreach (ZNodeAddOnValueEntity addOnValue in addOn.AddOnValueCollection)
					{
						taxGST += addOnValue.GST;
						taxPST += addOnValue.PST;
						taxHST += addOnValue.HST;
						taxVAT += addOnValue.VAT;
						taxSalesTax += addOnValue.SalesTax;
					}
				}

				return taxCost + taxGST + taxHST + taxPST + taxVAT + taxSalesTax;
			}
		}

		/// <summary>
		/// Gets the discount amount applied to this line item.
		/// </summary>        
		public decimal DiscountAmount
		{
			get
			{
				decimal discountAmount = 0;

				discountAmount += this.Product.DiscountAmount * this.Quantity;

				// Loop through the selected addOns for this product
				foreach (ZNodeAddOnEntity AddOn in this.Product.SelectedAddOns.AddOnCollection)
				{
					foreach (ZNodeAddOnValueEntity AddOnValue in AddOn.AddOnValueCollection)
					{
						discountAmount += AddOnValue.DiscountAmount * this.Quantity;
					}
				}

				return discountAmount;
			}
		}

		/// <summary>
		/// Gets or sets the price of product after applying promotions on tiered price
		/// </summary>        
		public decimal PromotionalPrice { get; set; }

		/// <summary>
		/// Gets or sets the discount amount applied to the extended price of this line item.
		/// </summary>        
		public decimal ExtendedPriceDiscount { get; set; }

		/// <summary>
		/// Gets the promotion description.
		/// </summary>        
		public string PromoDescription
		{
			get
			{
				return this._PromoDescription;
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether is tax already calculated for the current shopping cart.
		/// </summary>
		public bool IsTaxCalculated { get; set; }

		#endregion		
	
		public virtual ZNodeShoppingCartItem Clone()
		{
			var copiedItem = this.MemberwiseClone() as ZNodeShoppingCartItem;
			copiedItem.Product = this.Product.Clone();
			return copiedItem;
		}

        #region PRFT Custom Properties
        /// <summary>
        /// Gets or sets the ERP unit price of this line item.
        /// </summary>        
        public decimal ERPUnitPrice { get; set; }

        /// <summary>
        /// Retrieves or sets the product extended price based on ERP unit price
        /// </summary>       
        public decimal ERPExtendedPrice
        {
            get
            {

                _erpExtendedPrice = ERPUnitPrice * this.Quantity;
                return _erpExtendedPrice;
            }
            set
            {
                _erpExtendedPrice = value;
            }
        }

        public bool AllowBackOrder { get; set; }
        public string BackOrderMessage { get; set;}
        
        //public ERPItemDetail ERPItemDetail { get; set; }

        #endregion
    }
}
