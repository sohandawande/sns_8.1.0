using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Entities
{
    /// <summary>
    /// Represents Shopping cart and shopping cart items
    /// </summary>
    [Serializable()]
    public class ZNodeShoppingCart : ZNodeBusinessBase
    {
        #region Member Variables
		private StringBuilder _PromoDescription = new StringBuilder();
		protected StringBuilder _ErrorMessage = new StringBuilder();
		private decimal _GiftCardAmount = 0;
        private decimal _GiftCardBalance = 0;
		private decimal _OrderLevelShipping = 0;
		private decimal _OrderLevelTaxes = 0;
		private decimal _TaxCost = 0;
		//private string _CouponMessage = string.Empty;
		
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the ZNodeShoppingCart class
        /// </summary>
        public ZNodeShoppingCart()
        {
			ShoppingCartItems = new ZNodeGenericCollection<ZNodeShoppingCartItem>();
			Shipping = new ZNodeShipping();
			Payment = new ZNodePayment();
            Coupons = new List<ZNodeCoupon>();
        }
        #endregion

        #region Public Properties       

        /// <summary>
        /// Gets the error messages that have been created in this cart.
        /// </summary>
        public string ErrorMessage
        {
            get { return this._ErrorMessage.ToString(); }
        }

        /// <summary>
        /// Gets or sets a collection of shopping cart items
        /// </summary>
        public ZNodeGenericCollection<ZNodeShoppingCartItem> ShoppingCartItems { get; set; }

        ///// <summary>
        ///// Gets or sets the Znode coupon applied by the user.
        ///// </summary>
        //public string Coupon { get; set; }

        ///// <summary>
        ///// Gets or sets a value indicating whether the coupon applied
        ///// </summary>
        //public bool CouponApplied { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether any promotion applied
        /// </summary>
        public bool IsAnyPromotionApplied { get; set; }

        ///// <summary>
        ///// Gets or sets a value indicating whether the coupon valid.
        ///// </summary>
        //public bool CouponValid { get; set; }

        /// <summary>
        /// Gets or sets the Shipping object
        /// </summary>
		public ZNodeShipping Shipping { get; set; }

        /// <summary>
        /// Gets or sets the payment object
        /// </summary>
        public ZNodePayment Payment { get; set; }

        /// <summary>
        /// Gets the total shipping cost of line items in the shopping cart.
        /// </summary>    
		public virtual decimal ShippingCost
        {
            get
            {
				decimal totalShippingCost = this.ShoppingCartItems.Cast<ZNodeShoppingCartItem>().Sum(x => x.ShippingCost) + this.OrderLevelShipping;                

                decimal shippingDiscount = this.Shipping.ShippingDiscount;

                return totalShippingCost < shippingDiscount ? 0 : Math.Round((totalShippingCost - shippingDiscount), 2);
            }
        }

        /// <summary>
        /// Gets or sets the discounted applied to this order.
        /// </summary>    
        public decimal OrderLevelDiscount { get; set; }

        /// <summary>
        /// Gets or sets the Gift Card Amount.
        /// </summary>
        public decimal GiftCardAmount
        {
            get
            {
                return Math.Round(this._GiftCardAmount, 2);
            }

            set
            {
                this._GiftCardAmount = value;
            }
        }

        /// <summary>
        /// Gets or sets the Gift Card Balance.
        /// </summary>
        public decimal GiftCardBalance
        {
            get
            {
                return Math.Round(this._GiftCardBalance, 2);
            }

            set
            {
                this._GiftCardBalance = value;
            }
        }

        /// <summary>
        /// Gets or sets the gift card number.
        /// </summary>
        public string GiftCardNumber { get; set; }
        /// <summary>
        /// Gets or sets the Gift Card Message.
        /// </summary>
        public string GiftCardMessage { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the gift card number is valid.
        /// </summary>
        public bool IsGiftCardValid { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the gift card is applied to the shopping cart.
        /// </summary>
        public bool IsGiftCardApplied { get; set; }

        /// <summary>
        /// Gets or sets the shipping cost for this order.
        /// </summary>     
        public decimal OrderLevelShipping
        {
            get
            {
                return this._OrderLevelShipping + this.Shipping.ShippingHandlingCharge;
            }

            set
            {
                this._OrderLevelShipping = value;
            }
        }

        /// <summary>
        /// Gets or sets the tax cost for this order.
        /// </summary>     
		public decimal OrderLevelTaxes
        {
            get
            {
                return Math.Round(this._OrderLevelTaxes + this.SalesTax + this.VAT + this.HST + this.PST + this.GST, 2);
            }

            set
            {
                this._OrderLevelTaxes = value;
            }
        }

        /// <summary>
        /// Gets or sets the total tax cost of items in the shopping cart.
        /// </summary>
		public virtual decimal TaxCost
        {
			get
			{
				decimal totalTaxCost = this.ShoppingCartItems.Cast<ZNodeShoppingCartItem>().Sum(x => x.TaxCost);

				return Math.Round(totalTaxCost, 2);
			}
        }

        /// <summary>
        /// Gets or sets the total sales tax cost of items in the shopping cart.
        /// </summary>
        public decimal SalesTax { get; set; }

        /// <summary>
        /// Gets or sets the total tax cost of items in the shopping cart.
        /// </summary>
        public decimal VAT { get; set; }

        /// <summary>
        /// Gets or sets the total tax cost of items in the shopping cart.
        /// </summary>
        public decimal PST { get; set; }

        /// <summary>
        /// Gets or sets the total tax cost of items in the shopping cart.
        /// </summary>
        public decimal HST { get; set; }

        /// <summary>
        /// Gets or sets the total tax cost of items in the shopping cart.
        /// </summary>
        public decimal GST { get; set; }

        /// <summary>
        /// Gets or sets the sales tax rate (%)
        /// </summary>
        public decimal TaxRate { get; set; }

        //PRFT Custom code: Start
        /// <summary>
        /// Multipage checkout additional instructions
        /// </summary>
        public string AdditionalInstructions { get; set; }

        public string CustomerExternalId { get; set; }
        //PRFT Custom code: End
        /// <summary>
        /// Gets the count of items in the shopping cart
        /// </summary>
        public int Count
        {
            get
            {
                return this.ShoppingCartItems.Count;
            }
        }

        /// <summary>
        /// Gets the total cost of items in the shopping cart before shipping and taxes
        /// </summary>
		public virtual decimal SubTotal
        {
            get
            {
				decimal subTotal = this.ShoppingCartItems.Cast<ZNodeShoppingCartItem>().Sum(x => x.ExtendedPrice);              

                return subTotal;
            }
        }

        /// <summary>
        /// Gets total discount of applied to the items in the shopping cart.
        /// </summary>
		public virtual decimal Discount
        {
            get
            {
                decimal subTotal = this.SubTotal;

				decimal totalDiscount = this.OrderLevelDiscount + this.ShoppingCartItems.Cast<ZNodeShoppingCartItem>().Sum(x => x.ExtendedPriceDiscount + x.DiscountAmount);

                if (totalDiscount > subTotal)
                {
                    return subTotal;
                }

                return totalDiscount;
            }
        }
		
        /// <summary>
        /// Gets the total cost after shipping, taxes and promotions
        /// </summary>        
        public virtual decimal Total
        {
            get
            {
                decimal tempCalc = 0;
                tempCalc = (this.SubTotal - this.Discount) + this.ShippingCost + this._TaxCost + this.OrderLevelTaxes - this.GiftCardAmount;
                return tempCalc;
            }
        }

        /// <summary>
        /// Gets all of the promotion descriptions that have been added to the cart.
        /// </summary>        
        public string PromoDescription
        {
            get
            {
                StringBuilder promoDesc = new StringBuilder(this._PromoDescription.ToString());

                foreach (ZNodeShoppingCartItem item in this.ShoppingCartItems)
                {
                    if (promoDesc.Length > 0)
                    {
                        this._PromoDescription.Append("<br/>");
                    }

                    promoDesc.Append(item.PromoDescription.ToString());
                }

                return promoDesc.ToString();
            }
        }

        ///// <summary>
        ///// Gets or sets the Coupon Message
        ///// </summary>
        //public string CouponMessage
        //{
        //    get
        //    {
        //        return this._CouponMessage;
        //    }

        //    set
        //    {
        //        this._CouponMessage = value;
        //    }
        //}

        /// <summary>
        /// Gets or sets the promotion description.
        /// </summary>
        public string AddPromoDescription
        {
            get
            {
                return this._PromoDescription.ToString();
            }

            set
            {
                this._PromoDescription.Append(value);
            }
        }

        /// <summary>
        /// Gets or sets a message to the shopping cart main error message.
        /// </summary>
        public string AddErrorMessage
        {
            get
            {
                if (this._ErrorMessage.Length > 0)
                {
                    this._ErrorMessage.Append("<br/>");
                }

                this._ErrorMessage.Append(this.ErrorMessage);

                return this._ErrorMessage.ToString();
            }

            set
            {
                this._ErrorMessage.Append(value);
            }
        }

        // for paypal express checkout for Api
        public string Token { get; set; }
        public string Payerid { get; set; }

        //for setting PortalId in case of franchise admin
        public int? PortalId { get; set; }

        //depends on the first coupon applied.
        public bool CartAllowsMultiCoupon { get; set; }
        #endregion

        /// <summary>
        /// Get the current shopping cart.
        /// </summary>
        /// <returns>Returns the current ZNodeShoppingCart object.</returns>
        public static ZNodeShoppingCart CurrentShoppingCart()
        {
            ZNodeShoppingCart shoppingCart;

            // Get the user account from session
            shoppingCart = (ZNodeShoppingCart)HttpContext.Current.Session[ZNodeSessionKeyType.ShoppingCart.ToString()];

            if (shoppingCart == null)
            {
                return null;
            }
            else
            {
                return shoppingCart;
            }
        }

        /// <summary>
        /// Gets or sets Znode Coupons for applied in shopping cart.
        /// </summary>
        public List<ZNodeCoupon> Coupons { get; set; }

        /// <summary>
        /// Clear the previous error messages.
        /// </summary>
        public void ClearPreviousErrorMessges()
        {
            _ErrorMessage = new StringBuilder();
            _PromoDescription = new StringBuilder();
        }		
    }
}
