using System;
using System.Xml.Serialization;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Entities
{
    /// <summary>
    /// Represents the properties and methods of a product AddOn.
    /// </summary>
    [Serializable()]
    [XmlRoot()]
    public class ZNodeAddOnEntity : ZNodeBusinessBase
    {
        #region Private Member Variables
        private int _AddOnId;
        private int _ProductId;
        private string _Name = String.Empty;
        private string _Title = String.Empty;
        private string _Description = string.Empty;
        private bool _AllowbackOrder = false;
        private bool _TrackInventory = false;
        private bool _IsOptional;
        private int _DisplayOrder;
        private string _DisplayType = string.Empty;
        private string _OutOfStockMsg = string.Empty;
        private string _InStockMsg = string.Empty;
        private string _BackOrderMsg = string.Empty;
        protected ZNodeGenericCollection<ZNodeAddOnValueEntity> _AddonValueCollection = new ZNodeGenericCollection<ZNodeAddOnValueEntity>();
        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the ZNodeAddOnEntity class
        /// </summary>
        public ZNodeAddOnEntity()
        {
        }

		
		public ZNodeAddOnEntity Clone()
		{
			var copiedItem = this.MemberwiseClone() as ZNodeAddOnEntity;
			var collection = new ZNodeGenericCollection<ZNodeAddOnValueEntity>();
			foreach (ZNodeAddOnValueEntity item in AddOnValueCollection)
				collection.Add(item.Clone());
			copiedItem.AddOnValueCollection = collection;
			return copiedItem;
		}

        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the product addon values
        /// </summary>
        [XmlElement("ZNodeAddOnValue")]
        public ZNodeGenericCollection<ZNodeAddOnValueEntity> AddOnValueCollection
        {
            get
            {
                return this._AddonValueCollection;
            }

            set
            {
                this._AddonValueCollection = value;
            }
        }

        /// <summary>
        /// Gets or sets the Product Add-onId
        /// </summary>
        [XmlElement()]
        public int AddOnID
        {
            get { return this._AddOnId; }
            set { this._AddOnId = value; }
        }

        /// <summary>
        /// Gets or sets the product Id
        /// </summary>
        [XmlElement()]
        public int ProductId
        {
            get { return this._ProductId; }
            set { this._ProductId = value; }
        }

        /// <summary>
        /// Gets or sets the product add-on name
        /// </summary>
        [XmlElement()]
        public string Name
        {
            get { return this._Name; }
            set { this._Name = value; }
        }

        /// <summary>
        /// Gets or sets the product add-on title
        /// </summary>
        [XmlElement()]
        public string Title
        {
            get { return this._Title; }
            set { this._Title = value; }
        }

        /// <summary>
        /// Gets or sets the product add-on description
        /// </summary>
        [XmlElement()]
        public string Description
        {
            get { return this._Description; }
            set { this._Description = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the allow back order is true for this add-on.
        /// Items can always be added to the cart and Inventory is reduced
        /// </summary>
        [XmlElement()]
        public bool AllowBackOrder
        {
            get { return this._AllowbackOrder; }
            set { this._AllowbackOrder = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the track inventory is enabled for this add-on.
        /// </summary>
        [XmlElement()]
        public bool TrackInventoryInd
        {
            get { return this._TrackInventory; }
            set { this._TrackInventory = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the add-on is optional.
        /// </summary>
        [XmlElement()]
        public bool OptionalInd
        {
            get { return this._IsOptional; }
            set { this._IsOptional = value; }
        }

        /// <summary>
        /// Gets or sets the Product Add-on display order
        /// </summary>
        [XmlElement()]
        public int DisplayOrder
        {
            get { return this._DisplayOrder; }
            set { this._DisplayOrder = value; }
        }

        /// <summary>
        /// Gets or sets the Out of Stock message
        /// </summary>
        [XmlElement()]
        public string OutOfStockMsg
        {
            get { return this._OutOfStockMsg; }
            set { this._OutOfStockMsg = value; }
        }

        /// <summary>
        /// Gets or sets the DisplayType 
        /// </summary>
        [XmlElement()]
        public string DisplayType
        {
            get { return this._DisplayType; }
            set { this._DisplayType = value; }
        }

        /// <summary>
        /// Gets or sets the Out of In-Stock message
        /// </summary>
        [XmlElement()]
        public string InStockMsg
        {
            get { return this._InStockMsg; }
            set { this._InStockMsg = value; }
        }

        /// <summary>
        /// Gets or sets the Out of Back Order message
        /// </summary>
        [XmlElement()]
        public string BackOrderMsg
        {
            get { return this._BackOrderMsg; }
            set { this._BackOrderMsg = value; }
        }

        #endregion
    }
}
