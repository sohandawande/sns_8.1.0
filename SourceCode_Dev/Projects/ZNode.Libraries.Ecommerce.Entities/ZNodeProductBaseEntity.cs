using System;
using System.Linq;
using System.Xml.Serialization;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Entities
{
    /// <summary>
    /// Represents a product in the catalog
    /// </summary>
    /// <remarks>
    /// 
    /// </remarks>
    [Serializable()]
    [XmlRoot("ZNodeProduct")]
    public class ZNodeProductBaseEntity : ZNodeBusinessBase
    {
        #region Private Variables
        protected int _ProductID = 0;
        protected int _PortalID = 0;
        protected string _Name = string.Empty;
        protected string _ProductNum = string.Empty;
        protected int _ProductTypeID;
        protected int _ExpirationPeriod;
        protected int _ExpirationFrequency;
        protected decimal _RetailPrice;
        protected decimal? _WholesalePrice;
        protected string _ImageFile;
        protected decimal _Weight;
        protected bool _IsActive;
        protected int _MaxQty;
        protected int _MinQty;
        protected int _DisplayOrder;
        protected string _Custom1 = string.Empty;
        protected string _Custom2 = string.Empty;
        protected string _Custom3 = string.Empty;
        protected string _CallMessage = string.Empty;
        protected int _ManufacturerID;
        protected string _ManufacturerPartNum = string.Empty;
        protected int? _ShippingRuleTypeID;
        protected decimal? _SalePrice;
        protected decimal _DiscountAmount;
        protected string _downloadLink = string.Empty;
        protected string _addOnDescription = string.Empty;
        protected bool _TrackInventoryInd;
        protected bool _AllowBackOrder;
        protected int _QuantityOnHand;
        protected string _sku = String.Empty;
        protected decimal _shippingCost;
        protected decimal _height;
        protected decimal _width;
        protected decimal _length;
        protected string _shortDescription = string.Empty;
        protected string _GiftCardDescription = string.Empty;
        private string _GiftCardNumber = string.Empty;
        protected bool _freeShippingInd = false;
        protected int _TaxClassID = 0;
        protected bool _shipSeparately = false;
        protected string _Guid = string.Empty;
        protected string _BackOrderMsg = string.Empty;
        protected string _seoURL = string.Empty;
        protected int _SupplierID;
        protected bool _newProductInd = false;
        protected bool _featuredInd = false;
        protected bool _CategorySpecial;
        protected int _InventoryDisplay;
        protected bool _HomepageSpecial;
        protected bool _CallForPricing;
        protected string _AffiliateUrl;
        protected int _reviewRating = 0;
        protected int _totalReviews = 0;
        protected string _ImageAltTag = "";
        protected string _ManufacturerName = string.Empty;
        protected string _Description = string.Empty;
        protected int _AccountID;

        protected bool _RecurringBillingInd = false;
        protected bool _RecurringBillingInstallmentInd = false;
        protected string _RecurringBillingPeriod = "";
        protected string _RecurringBillingFrequency = "";
        protected int _RecurringBillingTotalCycles = 0;
        protected decimal _RecurringBillingInitialAmount = 0;


        protected decimal _GST = 0;
        protected decimal _HST = 0;
        protected decimal _PST = 0;
        protected decimal _VAT = 0;
        protected decimal _SalesTax = 0;

        protected decimal _ShippingRate;
        protected int _RowIndex = 0;

        protected string _SEOTitle = string.Empty;
        protected string _SEOKeywords = string.Empty;
        protected string _SEODescription = string.Empty;
        protected int? _ExternalProductID = null;

        protected bool isPromotionApplied = false;
        #endregion

        #region protected MemberObjects
        protected ZNodeAddOnListEntity _selectedAddOnCollection = new ZNodeAddOnListEntity();
        protected ZNodeSKUEntity _selectedSKU = new ZNodeSKUEntity();
        protected ZNodeAddOnValueEntity _selectedAddOn = new ZNodeAddOnValueEntity();
        protected ZNodeGenericCollection<ZNodeProductTierEntity> _tieredPriceCollection = new ZNodeGenericCollection<ZNodeProductTierEntity>();
        private ZNodeGenericCollection<ZNodeBundleProductEntity> _bundleproductCollection = new ZNodeGenericCollection<ZNodeBundleProductEntity>();
        protected ZNodeImage _znodeImage = new ZNodeImage();
        protected ZNodeSKUProfile _skuProfile = null;
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the product description
        /// </summary>
        [XmlElement()]
        public string Description
        {
            get
            {
                return _Description;
            }
            set
            {
                _Description = value;
            }
        }

        /// <summary>
        /// Retrieves the GUID(unique value) for this product. 
        /// </summary>
        [XmlIgnore()]
        public string GUID
        {
            get
            {
                return _Guid;
            }
            set
            {
                _Guid = value;
            }
        }


        /// <summary>
        /// Gets or sets the product Add-on selected by the user for this product
        /// </summary>
        [XmlIgnore()]
        public ZNodeAddOnListEntity SelectedAddOns
        {
            get
            {
                return _selectedAddOnCollection;
            }
            set
            {
                _selectedAddOnCollection = value;
            }
        }

        /// <summary>
        /// Collection of bundle product association 
        /// </summary>
        [XmlElement("ZNodeBundleProduct")]
        public ZNodeGenericCollection<ZNodeBundleProductEntity> ZNodeBundleProductCollection
        {
            get
            {
                return _bundleproductCollection;
            }
            set
            {
                _bundleproductCollection = value;
            }
        }


        /// <summary>
        /// Gets or sets the product SKU selected by the user
        /// </summary>
        [XmlElement("ZNodeSKU")]
        public ZNodeSKUEntity SelectedSKUvalue
        {
            get
            {
                return _selectedSKU;
            }
            set
            {
                _selectedSKU = value;
            }
        }

        /// <summary>
        /// Gets or sets the product Addon selected by the user
        /// </summary>
        [XmlElement("ZNodeAddOnValue")]
        [Obsolete("This property is obsolete.  Use 'SelectedAddOns' property to get a collection of selected add-on values for each selected add-on.")]
        public ZNodeAddOnValueEntity SelectedAddonValues
        {
            get
            {
                return _selectedAddOn;
            }
            set
            {
                _selectedAddOn = value;
            }
        }

        /// <summary>
        /// Gets or sets the ProductID
        /// </summary>
        [XmlElement()]
        public int ProductID
        {
            get
            {
                return _ProductID;
            }
            set
            {
                _ProductID = value;
            }
        }

        /// <summary>
        /// Gets or sets the site portal id
        /// </summary>
        [XmlElement()]
        public int PortalID
        {
            get
            {
                if (_PortalID == 0)
                {

                    // Get product category Id                    
                    TList<ProductCategory> productCategoryList = DataRepository.ProductCategoryProvider.GetByProductID(ProductID);
                    DataRepository.ProductCategoryProvider.DeepLoad(productCategoryList, true, DeepLoadType.IncludeChildren, typeof(Category));
                    var portalCatalog =
                        productCategoryList.Where(x => x.CategoryIDSource.PortalID != ZNodeConfigManager.SiteConfig.PortalID)
                                           .SelectMany(
                                               x =>
                                               DataRepository.CategoryNodeProvider.GetByCategoryID(x.CategoryID)
                                                             .SelectMany(
                                                                 y =>
                                                                 DataRepository.PortalCatalogProvider.GetByCatalogID(
                                                                     y.CatalogID.GetValueOrDefault(0))))
                                           .OrderBy(x => x.PortalID).FirstOrDefault();

                    if (portalCatalog != null)
                        _PortalID = portalCatalog.PortalID;

                    if (_PortalID == 0)
                    {
                        _PortalID = ZNodeConfigManager.SiteConfig.PortalID;
                    }
                }

                return _PortalID;
            }
            set
            {
                _PortalID = value;
            }
        }

        /// <summary>
        /// Gets or sets the product Name
        /// </summary>
        [XmlElement()]
        public string Name
        {
            get
            {
                return _Name;
            }
            set
            {
                _Name = value;
            }
        }

        /// <summary>
        /// Gets or sets the product shortdescription
        /// </summary>
        [XmlElement()]
        public string ShortDescription
        {
            get
            {
                return _shortDescription;
            }
            set
            {
                _shortDescription = value;
            }
        }


        /// <summary>
        /// Gets or sets the product code
        /// </summary>
        [XmlElement()]
        public string ProductNum
        {
            get
            {
                return _ProductNum;
            }
            set
            {
                _ProductNum = value;
            }
        }

        /// <summary>
        /// Gets or sets the product type id
        /// </summary>
        [XmlElement()]
        public int ProductTypeID
        {
            get
            {
                return _ProductTypeID;
            }
            set
            {
                _ProductTypeID = value;
            }
        }

        /// <summary>
        /// Gets or sets the gift card expiration period.
        /// </summary>
        [XmlElement()]
        public int ExpirationPeriod
        {
            get
            {
                return _ExpirationPeriod;
            }

            set
            {
                _ExpirationPeriod = value;
            }
        }

        /// <summary>
        /// Gets or sets the gift card expiration frequency.
        /// </summary>
        [XmlElement()]
        public int ExpirationFrequency
        {
            get
            {
                return _ExpirationFrequency;
            }

            set
            {
                _ExpirationFrequency = value;
            }
        }

        /// <summary>
        /// Gets or sets the retail price. Will return the SKU override retail price if one exists.        
        /// </summary>
        [XmlElement()]
        public decimal RetailPrice
        {
            get
            {
                decimal retailPrice = _RetailPrice;
                decimal? _skuOverrideRetailPrice = _selectedSKU.RetailPriceOverride;

                if (_skuOverrideRetailPrice.HasValue)
                {
                    // if a sku retail price override exists then use that price
                    if (_skuOverrideRetailPrice.Value >= 0)
                        retailPrice = _skuOverrideRetailPrice.Value;
                }

                // 
                return retailPrice;
            }
            set
            {
                _RetailPrice = value;
            }
        }

        /// <summary>
        /// Gets or sets the wholesale price. Will return the SKU override wholesale price if one exists.
        /// </summary>
        [XmlElement()]
        public decimal? WholesalePrice
        {
            get
            {
                decimal? wholesalePrice = _WholesalePrice;
                decimal? skuWholeSalePrice = _selectedSKU.WholesalePriceOverride;

                if (skuWholeSalePrice.HasValue)
                {
                    // if sku wholesale override exists then use that price                
                    if (skuWholeSalePrice.Value >= 0)
                        wholesalePrice = skuWholeSalePrice.Value;
                }

                if (wholesalePrice.HasValue)
                    return wholesalePrice;

                return wholesalePrice;
            }
            set
            {
                _WholesalePrice = value;
            }
        }

        /// <summary>
        /// Gets or sets the discount amount applied to this line item.
        /// </summary>        
        public decimal DiscountAmount
        {
            get
            {
                return _DiscountAmount;
            }
            set
            {
                _DiscountAmount = value;
            }
        }

        /// <summary>
        /// Gets or sets the tiered prices related with this product
        /// </summary>
        [XmlElement("ZNodeProductTier")]
        public ZNodeGenericCollection<ZNodeProductTierEntity> ZNodeTieredPriceCollection
        {
            get
            {
                return _tieredPriceCollection;
            }
            set
            {
                _tieredPriceCollection = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to use wholesale price.
        /// </summary>
        [XmlIgnore()]
        public bool UseWholeSalePrice
        {
            get
            {
                // Get user profile from ProfileCache Session object
                if (System.Web.HttpContext.Current.Session["ProfileCache"] != null)
                {
                    ZNode.Libraries.DataAccess.Entities.Profile profile = (ZNode.Libraries.DataAccess.Entities.Profile)System.Web.HttpContext.Current.Session["ProfileCache"];

                    if (profile != null)
                    {
                        return profile.UseWholesalePricing.GetValueOrDefault(false);
                    }
                }

                return false;
            }
        }

        /// <summary>
        /// Gets or sets the product sale price as a decimal value
        /// </summary>
        [XmlElement()]
        public virtual decimal? SalePrice
        {
            get
            {
                decimal? salePrice = _SalePrice;

                // Get Selected SKU override sale price
                decimal? skuSalePrice = _selectedSKU.SalePriceOverride;

                if (skuSalePrice.HasValue)
                {
                    // if sku override exists then use that price with Sale price
                    salePrice = skuSalePrice.Value;
                }

                if (!salePrice.HasValue)
                {
                    salePrice = _selectedSKU.NegotiatedPrice;
                }

                if (salePrice.HasValue)
                {
                    return salePrice;
                }

                return _SalePrice;
            }
            set
            {
                _SalePrice = value;
            }
        }


        /// <summary>
        /// Gets the tiered Price for this product for the quantity 1.
        /// If no tiered pricing applied, it will return 0
        /// </summary>
        public decimal TieredPrice
        {
            get
            {
                int profileID = 0;
                decimal tieredPrice = 0;

                // Check product tiers list
                if (_tieredPriceCollection.Count > 0)
                {
                    // Get user profile from ProfileCache Session object
                    if (System.Web.HttpContext.Current.Session["ProfileCache"] != null)
                    {
                        ZNode.Libraries.DataAccess.Entities.Profile _profile = (ZNode.Libraries.DataAccess.Entities.Profile)System.Web.HttpContext.Current.Session["ProfileCache"];

                        if (_profile != null)
                        {
                            profileID = _profile.ProfileID;
                        }
                    }

                    foreach (ZNodeProductTierEntity productTieredPrice in _tieredPriceCollection)
                    {
                        if (1 >= productTieredPrice.TierStart && productTieredPrice.TierEnd >= 1)
                        {
                            if (productTieredPrice.ProfileID == 0)
                            {
                                tieredPrice = productTieredPrice.Price;
                                break;
                            }
                            else
                            {
                                if (profileID == productTieredPrice.ProfileID)
                                {
                                    tieredPrice = productTieredPrice.Price;
                                    break;
                                }
                            }
                        }
                    }
                }

                // Get Product tiered price if one exists, otherwise 0
                return tieredPrice;
            }
        }

        /// <summary>
        /// Gets the final calculated price for a product which includes inclusive tax
        /// </summary>
        [XmlIgnore()]
        public decimal FinalPrice
        {
            get
            {
                return ProductPrice;
            }
        }

        /// <summary>
        /// Gets the final calculated price for a product without inclusive tax
        /// </summary>
        [XmlIgnore()]
        public decimal ProductPrice
        {
            get
            {
                bool useWholeSalePrice = UseWholeSalePrice;
                decimal basePrice = _RetailPrice;
                decimal? _skuOverrideRetailPrice = _selectedSKU.RetailPriceOverride;

                if (_skuOverrideRetailPrice.HasValue)
                {
                    // If a sku retail price override exists then use that price
                    if (_skuOverrideRetailPrice.Value >= 0)
                    {
                        basePrice = _skuOverrideRetailPrice.Value;
                    }
                }

                decimal? salePrice = _SalePrice;

                // Get Selected SKU override sale price
                decimal? skuSalePrice = _selectedSKU.SalePriceOverride;

                if (skuSalePrice.HasValue)
                {
                    // If sku override exists then use that price with Sale price
                    salePrice = skuSalePrice.Value;
                }


                decimal? wholesalePrice = _WholesalePrice;
                decimal? skuWholeSalePrice = _selectedSKU.WholesalePriceOverride;

                if (skuWholeSalePrice.HasValue)
                {
                    // If sku wholesale override exists then use that price                
                    if (skuWholeSalePrice.Value >= 0)
                    {
                        wholesalePrice = skuWholeSalePrice.Value;
                    }
                }

                // If sale price has value
                if (!salePrice.HasValue && !useWholeSalePrice && _selectedSKU.NegotiatedPrice.HasValue)
                {
                    salePrice = _selectedSKU.NegotiatedPrice.GetValueOrDefault();
                    // Then set sale price value
                    basePrice = salePrice.Value;
                }
                else if (salePrice.HasValue && !useWholeSalePrice)
                {
                    basePrice = salePrice.Value;
                }
                else
                {
                    // If Wholesaleprice has null ,then ignore it
                    if (useWholeSalePrice && wholesalePrice.HasValue)
                    {
                        if (_selectedSKU.NegotiatedPrice.HasValue)
                        {
                            salePrice = _selectedSKU.NegotiatedPrice.GetValueOrDefault();
                            basePrice = salePrice.GetValueOrDefault();
                        }
                        else
                        {
                            basePrice = wholesalePrice.Value;
                        }

                    }
                    else if (salePrice.HasValue)
                    {
                        basePrice = salePrice.Value;
                    }
                }

                decimal tieredPrice = TieredPrice;

                return tieredPrice > 0 ? tieredPrice : basePrice;
            }
        }

        /// <summary>
        /// Gets or sets the image file name for this product. Will return the SKU picture override if one exists.
        /// </summary>
        [XmlElement()]
        public string ImageFile
        {
            get
            {
                string imageFilePath = _ImageFile;

                // If a sku image override exists then use that image
                if (_selectedSKU.SKUPicturePath.Trim().Length > 0)
                {
                    imageFilePath = _selectedSKU.SKUPicturePath;
                }

                if (string.IsNullOrEmpty(imageFilePath))
                {
                    return string.Empty;
                }

                return imageFilePath;
            }
            set
            {
                _ImageFile = value;
            }
        }

        /// <summary>
        /// Gets or sets the large image file path.
        /// </summary>
        [XmlIgnore()]
        public string LargeImageFilePath
        {
            get
            {
                return _znodeImage.GetImageHttpPathLarge(ImageFile);
            }
        }

        /// <summary>
        /// Gets or sets the medium image file path.
        /// </summary>
        [XmlIgnore()]
        public string MediumImageFilePath
        {
            get
            {
                return _znodeImage.GetImageHttpPathMedium(ImageFile);
            }
        }

        /// <summary>
        /// Gets or sets the small image file path.
        /// </summary>
        [XmlIgnore()]
        public string SmallImageFilePath
        {
            get
            {
                return _znodeImage.GetImageHttpPathSmall(ImageFile);
            }
        }

        /// <summary>
        /// Gets or sets the thumbnail image file path.
        /// </summary>
        [XmlIgnore()]
        public string ThumbnailImageFilePath
        {
            get
            {
                return _znodeImage.GetImageHttpPathThumbnail(ImageFile);
            }
        }

        /// <summary>
        /// Gets or sets the Cross sell image file path.
        /// </summary>
        [XmlIgnore()]
        public string CrossSellImagePath
        {
            get
            {
                return _znodeImage.GetImageHttpPathCrossSell(ImageFile);
            }
        }

        /// <summary>
        /// Gets or sets the small thumbnail image file path.
        /// </summary>
        [XmlIgnore()]
        public string SmallThumbnailImagePath
        {
            get
            {
                return _znodeImage.GetImageHttpPathSmallThumbnail(ImageFile);
            }
        }

        /// <summary>
        /// Gets or sets the weight of this product 
        /// </summary>
        [XmlElement()]
        public decimal Weight
        {
            get
            {
                decimal skuWeightAdditional = _selectedSKU.WeightAdditional;

                // If a sku additional weight exists then use that weight with product weight
                if (skuWeightAdditional > 0)
                {
                    return skuWeightAdditional + _Weight;
                }

                return _Weight;
            }
            set
            {
                _Weight = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the product is active.
        /// </summary>
        [XmlElement(ElementName = "ActiveInd")]
        public bool IsActive
        {
            get
            {
                return _IsActive;
            }
            set
            {
                _IsActive = value;
            }
        }

        /// <summary>
        /// Gets or sets the display order for this product
        /// </summary>
        [XmlElement()]
        public int DisplayOrder
        {
            get
            {
                return _DisplayOrder;
            }
            set
            {
                _DisplayOrder = value;
            }
        }

        /// <summary>
        /// Gets or sets the custom comments for this product
        /// </summary>
        [XmlElement()]
        public string Custom1
        {
            get
            {
                return _Custom1;
            }
            set
            {
                _Custom1 = value;
            }
        }

        /// <summary>
        /// Gets or sets the custom comments for this product
        /// </summary>
        [XmlElement()]
        public string Custom2
        {
            get
            {
                return _Custom2;
            }

            set
            {
                _Custom2 = value;
            }
        }

        /// <summary>
        /// Gets or sets the custom comments for this product
        /// </summary>
        [XmlElement()]
        public string Custom3
        {
            get
            {
                return _Custom3;
            }

            set
            {
                _Custom3 = value;
            }
        }

        /// <summary>
        /// Gets or sets the call for pricing text message
        /// </summary>
        public string CallMessage
        {
            get
            {
                return _CallMessage;
            }

            set
            {
                _CallMessage = value;
            }
        }

        /// <summary>        
        /// Sets or retrieves the manufacturerid for this product        
        /// </summary>
        [XmlElement()]
        public int ManufacturerID
        {
            get
            {
                return _ManufacturerID;
            }

            set
            {
                _ManufacturerID = value;
            }
        }

        /// <summary>
        /// Sets or retrieves the manufacturer part number for this product
        /// </summary>
        [XmlElement()]
        public string ManufacturerPartNum
        {
            get
            {
                return _ManufacturerPartNum;
            }

            set
            {
                _ManufacturerPartNum = value;
            }
        }

        /// <summary>
        /// Sets or retrieves the ShippingRuleTypeID for this product
        /// </summary>
        [XmlElement()]
        public int? ShippingRuleTypeID
        {
            get
            {
                return _ShippingRuleTypeID;
            }

            set
            {
                _ShippingRuleTypeID = value;
            }
        }

        /// <summary>
        /// Gets or sets the TrackInventoryInd property for this product
        /// </summary>
        [XmlElement()]
        public bool TrackInventoryInd
        {
            get
            {
                return _TrackInventoryInd;
            }

            set
            {
                _TrackInventoryInd = value;
            }
        }

        /// <summary>
        /// Gets or sets the AllowBackOrder property for this product
        /// </summary>
        [XmlElement()]
        public bool AllowBackOrder
        {
            get
            {
                return _AllowBackOrder;
            }

            set
            {
                _AllowBackOrder = value;
            }
        }

        /// <summary>
        /// Gets or sets the SKU for this product
        /// It will return the SKU override if one exists.
        /// </summary>
        [XmlElement()]
        public string SKU
        {
            get
            {
                if (_selectedSKU.SKUID > 0)
                {
                    return _selectedSKU.SKU;
                }
                else
                {
                    return _sku;
                }
            }

            set
            {
                _sku = value;
            }
        }

        /// <summary>
        /// Gets or sets the QuantityOnHand for this product
        /// It will return the SKU Quantity Available if one exists
        /// </summary>
        [XmlElement()]
        public int QuantityOnHand
        {
            get
            {
                if (_selectedSKU.SKUID > 0)
                {
                    return _selectedSKU.QuantityOnHand;
                }
                else
                {
                    return _QuantityOnHand;
                }
            }

            set
            {
                _QuantityOnHand = value;
            }
        }

        /// <summary>
        /// Gets or sets the MaxQuantity       
        /// </summary>
        [XmlElement()]
        public int MinQty
        {
            get
            {
                return _MinQty;
            }

            set
            {
                _MinQty = value;
            }
        }

        /// <summary>
        /// Gets or sets the MaxQuantity       
        /// </summary>
        [XmlElement()]
        public virtual int MaxQty
        {
            get
            {
                if (_skuProfile != null)
                    return _skuProfile.ProfileLimit;

                return _MaxQty;
            }

            set
            {
                _MaxQty = value;
            }
        }

        /// <summary>
        /// Gets or sets the product height         
        /// </summary>
        [XmlElement()]
        public decimal Height
        {
            get { return _height; }
            set { _height = value; }
        }

        /// <summary>
        /// Gets or sets the product width         
        /// </summary>
        [XmlElement()]
        public decimal Width
        {
            get { return _width; }
            set { _width = value; }
        }

        /// <summary>
        /// Gets or sets the product length         
        /// </summary>
        [XmlElement()]
        public decimal Length
        {
            get { return _length; }
            set { _length = value; }
        }

        /// <summary>
        ///Gets or sets the download Link
        /// </summary>
        [XmlElement()]
        public string DownloadLink
        {

            get { return _downloadLink; }
            set { _downloadLink = value; }
        }

        /// <summary>
        ///Gets or sets the free shipping property
        /// </summary>
        [XmlElement()]
        public bool FreeShippingInd
        {
            get { return _freeShippingInd; }
            set { _freeShippingInd = value; }
        }

        /// <summary>
        ///Gets or sets the ship separately boolean property
        /// </summary>
        [XmlElement()]
        public bool ShipSeparately
        {
            get { return _shipSeparately; }
            set { _shipSeparately = value; }
        }

        /// <summary>
        ///Gets or sets the tax exempt property
        /// </summary>
        [XmlElement()]
        public int TaxClassID
        {
            get { return _TaxClassID; }
            set { _TaxClassID = value; }
        }

        /// <summary>
        /// Gets or sets the SEO url.
        /// </summary>
        [XmlElement()]
        public string SEOURL
        {
            get
            {
                return _seoURL;
            }

            set
            {
                _seoURL = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this is new item. 
        /// </summary>
        [XmlElement()]
        public bool NewProductInd
        {
            get
            {
                return _newProductInd;
            }
            set
            {
                _newProductInd = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this is featured Item.
        /// </summary>
        [XmlElement()]
        public bool FeaturedInd
        {
            get
            {
                return _featuredInd;
            }
            set
            {
                _featuredInd = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the CallForPricing is enabled.
        /// </summary>
        [XmlElement()]
        public bool CallForPricing
        {
            get
            {
                return _CallForPricing;
            }
            set
            {
                _CallForPricing = value;
            }
        }

        /// <summary>
        /// Gets or sets to HomepageSpecial property
        /// </summary>
        [XmlElement()]
        public bool HomepageSpecial
        {
            get
            {
                return _HomepageSpecial;
            }
            set
            {
                _HomepageSpecial = value;
            }
        }

        /// <summary>
        /// Sets or retrieves the CategorySpecial property
        /// </summary>
        [XmlElement()]
        public bool CategorySpecial
        {
            get
            {
                return _CategorySpecial;
            }
            set
            {
                _CategorySpecial = value;
            }
        }

        /// <summary>
        /// Sets or retrieves the inventory display value for this product
        /// </summary>
        [XmlElement()]
        public int InventoryDisplay
        {
            get
            {
                return _InventoryDisplay;
            }
            set
            {
                _InventoryDisplay = value;
            }
        }

        /// <summary>
        /// Gets or Sets the average review rating for this product
        /// </summary>
        [XmlElement()]
        public int ReviewRating
        {
            get
            {
                return _reviewRating;
            }
            set
            {
                _reviewRating = value;
            }
        }

        /// <summary>
        /// Gets or Sets the total number of reviews for this product
        /// </summary>
        [XmlElement()]
        public int TotalReviews
        {
            get
            {
                return _totalReviews;
            }
            set
            {
                _totalReviews = value;
            }
        }

        /// <summary>
        /// Gets or Sets the image alternative tag text
        /// </summary>
        [XmlElement()]
        public string ImageAltTag
        {
            get
            {
                string skuImageAlternativeText = _selectedSKU.ImageAltTag;

                // if selected Sku has image alt tag text, then use that alternative text
                if (_selectedSKU.SKUID > 0 && !string.IsNullOrEmpty(skuImageAlternativeText))
                {
                    return skuImageAlternativeText;
                }

                // if image Alt tag value not exists then use product name
                if (string.IsNullOrEmpty(_ImageAltTag))
                    return _Name;

                // otherwise, return Alternative text
                return _ImageAltTag;
            }
            set
            {
                _ImageAltTag = value;
            }
        }

        /// <summary>
        /// Sets or retrieves the SupplierID
        /// </summary>
        [XmlElement()]
        public int SupplierID
        {
            get
            {
                if (_selectedSKU.SKUID > 0 && !string.IsNullOrEmpty(_selectedSKU.AttributesDescription))
                    return _selectedSKU.SupplierID;

                return _SupplierID;
            }
            set { _SupplierID = value; }
        }

        /// <summary>
        /// Gets or Sets the billing enable property
        /// </summary>
        [XmlElement()]
        public bool RecurringBillingInd
        {
            get { return _RecurringBillingInd; }
            set { _RecurringBillingInd = value; }
        }

        /// <summary>
        /// Gets or Sets the installmentInd  property
        /// </summary>
        [XmlElement()]
        public bool RecurringBillingInstallmentInd
        {
            get { return _RecurringBillingInstallmentInd; }
            set { _RecurringBillingInstallmentInd = value; }
        }

        /// <summary>
        /// Gets or Sets the billing period (days or months), in association with the frequency.
        /// </summary>
        [XmlElement()]
        public string RecurringBillingPeriod
        {
            get
            {
                //
                //if (_selectedSKU.SKUID > 0)
                //    return _selectedSKU.RecurringBillingPeriod;

                return _RecurringBillingPeriod;
            }
            set { _RecurringBillingPeriod = value; }
        }

        /// <summary>
        /// Gets or Sets the billing frequency, in association with the Period.
        /// </summary>
        [XmlElement()]
        public string RecurringBillingFrequency
        {
            get
            {
                // 
                //if (_selectedSKU.SKUID > 0)
                //    return _selectedSKU.RecurringBillingFrequency;

                return _RecurringBillingFrequency;
            }
            set { _RecurringBillingFrequency = value; }
        }

        /// <summary>
        /// Gets or sets the number of billing occurrences or payments for the subscription
        /// </summary>
        [XmlElement()]
        public int RecurringBillingTotalCycles
        {
            get
            {
                // Override SKu recurring billing total cycle
                //if (_selectedSKU.SKUID > 0)
                //    return _selectedSKU.RecurringBillingTotalCycles;

                return _RecurringBillingTotalCycles;
            }
            set { _RecurringBillingTotalCycles = value; }
        }

        /// <summary>
        /// Gets or sets the initial amount to be charged during subscription creation,
        /// </summary>
        [XmlElement()]
        public decimal RecurringBillingInitialAmount
        {
            get { return _RecurringBillingInitialAmount; }
            set { _RecurringBillingInitialAmount = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [XmlElement()]
        public decimal ShippingRate
        {
            get { return _ShippingRate; }
            set { _ShippingRate = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [XmlElement()]
        public int RowIndex
        {
            get { return _RowIndex; }
            set { _RowIndex = value; }
        }


        /// <summary>
        /// Sets or retrieves the SEO Keywords
        /// </summary>
        [XmlElement()]
        public string SEOKeywords
        {
            get
            {
                return _SEOKeywords;
            }
            set
            {
                _SEOKeywords = value;
            }
        }

        /// <summary>
        /// Sets or retrieves the SEO Title
        /// </summary>
        [XmlElement()]
        public string SEOTitle
        {
            get
            {
                return _SEOTitle;
            }
            set
            {
                _SEOTitle = value;
            }
        }

        /// <summary>
        /// Sets or retrieves the SEO Description
        /// </summary>
        [XmlElement()]
        public string SEODescription
        {
            get
            {
                return _SEODescription;
            }
            set
            {
                _SEODescription = value;
            }
        }


        /// <summary>
        /// Sets or retrieves the Manufacturer Name
        /// </summary>
        [XmlElement()]
        public string ManufacturerName
        {
            get
            {
                return _ManufacturerName;
            }
            set
            {
                _ManufacturerName = value;
            }
        }

        /// <summary>
        /// Sets or retreives the ProductID for the external database / service, like 2Checkout ProductID, etc.
        /// </summary>
        [XmlElement()]
        public int? ExternalProductID
        {
            get
            {
                return _ExternalProductID;
            }

            set
            {
                _ExternalProductID = value;
            }
        }

        /// <summary>
        /// Sets or retrieves the AccountID
        /// </summary>
        [XmlElement()]
        public int AccountID
        {
            get
            {
                return _AccountID;
            }

            set
            {
                _AccountID = value;
            }
        }


        /// <summary>
        /// Gets or sets the Product Affiliate Url
        /// </summary>
        [XmlElement()]
        public string AffiliateUrl
        {
            get
            {
                return _AffiliateUrl;
            }

            set
            {
                _AffiliateUrl = value;
            }
        }

        /// <summary>
        /// Gets or sets Promotion Applied or not.
        /// </summary>
        [XmlIgnore()]
        public bool IsPromotionApplied
        {
            get
            {
                return this.isPromotionApplied;
            }

            set
            {
                this.isPromotionApplied = value;
            }
        }

        /// <summary>
        /// Gets SKU Profile for the current user profile.
        /// </summary>
        [XmlIgnore()]
        public ZNodeSKUProfile SkuProfile
        {
            get
            {
                return this._skuProfile;
            }
        }
        #endregion

        # region Public Instance Properties - Related to Shopping Cart

        /// <summary>
        /// Returns the discounted final price of the product
        /// This method calculates the addon price, product price which includes promotions discount.
        /// If Tiered price exists, then it will override the product price
        /// </summary>
        public decimal AddOnPrice
        {
            get
            {
                decimal addOnRetailPriceTotal = 0;

                # region Calculate AddOn additional price without promotions discount
                // Loop through the selected addOns for this product
                foreach (ZNodeAddOnEntity AddOn in SelectedAddOns.AddOnCollection)
                {
                    foreach (ZNodeAddOnValueEntity AddOnValue in AddOn.AddOnValueCollection)
                    {
                        // calculate additional price
                        addOnRetailPriceTotal += AddOnValue.FinalPrice;
                    }
                }
                #endregion

                return addOnRetailPriceTotal;
            }
        }

        /// <summary>
        /// Returns the total shopping cart description for this item
        /// </summary>
        public string ShoppingCartDescription
        {
            get
            {
                string stockMessage = string.Empty;
                string bundlestockMessage = string.Empty;
                string msg = string.Empty;

                // Allow Back Order
                if (AllowBackOrder && QuantityOnHand <= 0 && !string.IsNullOrEmpty(_BackOrderMsg))
                {
                    //Set back order message
                    stockMessage = "Note :  " + _BackOrderMsg;
                }
                msg = (string.IsNullOrEmpty(SelectedSKUvalue.AttributesDescription) ? string.Empty : SelectedSKUvalue.AttributesDescription + " <br/>") + SelectedAddOns.ShoppingCartAddOnsDescription + " " + stockMessage + " " + (string.IsNullOrEmpty(GiftCardDescription) ? string.Empty : GiftCardDescription + "<br/>");

                foreach (ZNodeProductBaseEntity bundleProduct in ZNodeBundleProductCollection)
                {
                    if (bundleProduct.AllowBackOrder && bundleProduct.QuantityOnHand <= 0 && !string.IsNullOrEmpty(bundleProduct.BackOrderMsg))
                    {
                        //Set back order message
                        bundlestockMessage = "Note :  " + bundleProduct.BackOrderMsg;
                    }

                    msg += "<b>" + bundleProduct.Name + "</b> <br/>" + bundleProduct.SelectedSKUvalue.AttributesDescription + " " + bundleProduct.SelectedAddOns.ShoppingCartAddOnsDescription + " " + bundlestockMessage + ((!string.IsNullOrEmpty(bundleProduct.SelectedSKUvalue.AttributesDescription) || !string.IsNullOrEmpty(bundleProduct.SelectedAddOns.ShoppingCartAddOnsDescription) || !string.IsNullOrEmpty(bundlestockMessage)) ? "<br />" : string.Empty);
                }

                return msg.Trim();
            }
        }

        /// <summary>
        /// Stores metadata on the gift card.(attribute selection, etc)
        /// This metadata is displayed on the shopping cart list
        /// This property is set explicitly by the UI and is not retrieved automatically
        /// </summary>
        [XmlElement()]
        public string GiftCardDescription
        {
            get { return _GiftCardDescription; }
            set { _GiftCardDescription = value; }
        }

        /// <summary>
        /// Gets or Sets the gift card number.
        /// </summary>
        [XmlElement()]
        public string GiftCardNumber
        {
            get { return _GiftCardNumber; }
            set { _GiftCardNumber = value; }
        }

        /// <summary>
        /// Gets or sets the BackOrder Message
        /// </summary>
        [XmlElement()]
        public string BackOrderMsg
        {
            get
            {
                return _BackOrderMsg;
            }
            set
            {
                _BackOrderMsg = value;
            }
        }

        /// <summary>
        /// Gets or Sets of shipping cost
        /// </summary>
        [XmlIgnore()]
        public decimal ShippingCost
        {
            get
            {
                return _shippingCost;
            }
            set
            {
                _shippingCost = value;
            }
        }

        /// <summary>
        /// Gets or Sets of HST
        /// </summary>
        [XmlIgnore()]
        public decimal HST
        {
            get
            {
                return _HST;
            }
            set
            {
                _HST = value;
            }
        }

        /// <summary>
        /// Gets or Sets of GST
        /// </summary>
        [XmlIgnore()]
        public decimal GST
        {
            get
            {
                return _GST;
            }
            set
            {
                _GST = value;
            }
        }

        /// <summary>
        /// Gets or Sets of PST
        /// </summary>
        [XmlIgnore()]
        public decimal PST
        {
            get
            {
                return _PST;
            }
            set
            {
                _PST = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [XmlIgnore()]
        public decimal VAT
        {
            get
            {
                return _VAT;
            }
            set
            {
                _VAT = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [XmlIgnore()]
        public decimal SalesTax
        {
            get
            {
                return _SalesTax;
            }
            set
            {
                _SalesTax = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [XmlIgnore()]
        public ZNode.Libraries.DataAccess.Entities.Address AddressToShip { get; set; }

        #endregion

        #region Constructors

        public ZNodeProductBaseEntity()
        {

        }

        public virtual ZNodeProductBaseEntity Clone()
        {
            var copiedItem = this.MemberwiseClone() as ZNodeProductBaseEntity;
            copiedItem.SelectedAddOns = this.SelectedAddOns.Clone();
            return copiedItem;
        }
        #endregion
    }

    [Serializable()]
    public class ZNodeSKUProfile
    {
        [XmlElement]
        public int ProfileLimit
        {
            get;
            set;
        }

        [XmlElement]
        public DateTime ExpirationDate
        {
            get;
            set;
        }

        [XmlElement]
        public int ProfileID
        {
            get;
            set;
        }
    }
}
