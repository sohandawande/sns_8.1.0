﻿using System;
using System.Linq;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Entities
{
    [Serializable()]
    public class ZNodeOrder
    {
        #region Private Variables
        private string _CreditCardNumber = string.Empty;
        private string _CardSecurityCode = string.Empty;
        private string _Email = string.Empty;
        private ZNode.Libraries.DataAccess.Entities.Order _Order = new ZNode.Libraries.DataAccess.Entities.Order();
        private string _AdditionalInstructions = string.Empty;
        protected ZNodeShoppingCart shoppingCart = (ZNodeShoppingCart)ZNodeShoppingCart.CreateFromSession(ZNodeSessionKeyType.ShoppingCart);
        private TList<OrderLineItem> _OrderLineItems = new TList<OrderLineItem>();
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the collection of orderline items for this order
        /// </summary>
        public TList<OrderLineItem> OrderLineItems
        {
            get
            {
                return this._OrderLineItems;
            }
            set
            {
                this._OrderLineItems = value;
            }
        }

        /// <summary>
        /// Gets or sets the Order object.
        /// </summary>
        public ZNode.Libraries.DataAccess.Entities.Order Order
        {
            get { return this._Order; }
            set { this._Order = value; }
        }

        /// <summary>
        /// Gets or sets the transaction id for this order.
        /// </summary>
        public string CardAuthCode
        {
            get { return this._Order.CardAuthCode; }
            set { this._Order.CardAuthCode = value; }
        }

        /// <summary>
        /// Gets the gift card amount.
        /// </summary>
        public decimal GiftCardAmount
        {
            get
            {
                return this.shoppingCart.GiftCardAmount;
            }
        }

        /// <summary>
        /// Gets the gift card number.
        /// </summary>
        public string GiftCardNumber
        {
            get 
            {
                return this.shoppingCart.GiftCardNumber; 
            }
        }

        /// <summary>
        /// Gets or sets the account mail Id.
        /// </summary>
        public string Email
        {
            get
            {
                return this._Email;
            }

            set
            {
                this._Email = value;
            }
        }
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the Billing Address object.
        /// </summary>
        public ZNode.Libraries.DataAccess.Entities.Address BillingAddress
        {
            get
            {
                ZNode.Libraries.DataAccess.Entities.Address address = new ZNode.Libraries.DataAccess.Entities.Address();
                address.FirstName = this._Order.BillingFirstName;
                address.LastName = this._Order.BillingLastName;
                address.CompanyName = this._Order.BillingCompanyName;
                address.Street = this._Order.BillingStreet;
                address.Street1 = this._Order.BillingStreet1;
                address.City = this._Order.BillingCity;
                address.StateCode = this._Order.BillingStateCode;
                address.PostalCode = this._Order.BillingPostalCode;
                address.CountryCode = this._Order.BillingCountry;
                address.PhoneNumber = this._Order.BillingPhoneNumber;
                return address;
            }

            set
            {
                ZNode.Libraries.DataAccess.Entities.Address address = value;

                this._Order.BillingFirstName = address.FirstName;
                this._Order.BillingLastName = address.LastName;
                this._Order.BillingCompanyName = address.CompanyName;
                this._Order.BillingStreet = address.Street;
                this._Order.BillingStreet1 = address.Street1;
                this._Order.BillingCity = address.City;
                this._Order.BillingStateCode = address.StateCode;
                this._Order.BillingPostalCode = address.PostalCode;
                this._Order.BillingCountry = address.CountryCode;
                this._Order.BillingPhoneNumber = address.PhoneNumber;
            }
        }

        /// <summary>
        /// Gets or sets the Shipping Address object. 
        /// </summary>
        public ZNode.Libraries.DataAccess.Entities.Address ShippingAddress
        {
            get
            {
				var orderShipmentList = this.OrderLineItems.Where(x => x.OrderShipmentIDSource != null).Select(x => x.OrderShipmentIDSource).Distinct();
				
				ZNode.Libraries.DataAccess.Entities.Address address = new ZNode.Libraries.DataAccess.Entities.Address();

				foreach (var orderShipment in orderShipmentList)
				{
					address.FirstName = orderShipment.ShipToFirstName;
					address.LastName = orderShipment.ShipToLastName;
					address.Street = orderShipment.ShipToStreet;
					address.Street1 = orderShipment.ShipToStreet1;
					address.City = orderShipment.ShipToCity;
					address.StateCode = orderShipment.ShipToStateCode;
					address.PostalCode = orderShipment.ShipToPostalCode;
					address.CountryCode = orderShipment.ShipToCountry;
					address.CompanyName = orderShipment.ShipToCompanyName;
					address.PhoneNumber = orderShipment.ShipToPhoneNumber;
				}
	            return address;
            }

            set
            {
                ZNode.Libraries.DataAccess.Entities.Address address = value;

				// Commenting this code as we need to rewrite the code for #16485
				//this._Order.ShipFirstName = address.FirstName;
				//this._Order.ShipLastName = address.LastName;
				//this._Order.ShipCompanyName = address.CompanyName;
				//this._Order.ShipStreet = address.Street;
				//this._Order.ShipStreet1 = address.Street1;
				//this._Order.ShipCity = address.City;
				//this._Order.ShipStateCode = address.StateCode;
				//this._Order.ShipPostalCode = address.PostalCode;
				//this._Order.ShipCountry = address.CountryCode;
				//this._Order.ShipPhoneNumber = address.PhoneNumber;
            }
        }

        /// <summary>
        /// Gets or sets the _Order id for this case
        /// </summary>
        public int OrderID
        {
            get { return this._Order.OrderID; }
            set { this._Order.OrderID = value; }
        }

        /// <summary>
        /// Gets or sets the site portal id
        /// </summary>
        public int PortalId
        {
            get { return this._Order.PortalId.GetValueOrDefault(0); }
            set { this._Order.PortalId = value; }
        }

        /// <summary>
        /// Gets or sets the account Id
        /// </summary>   
        public int AccountID
        {
            get { return this._Order.AccountID.GetValueOrDefault(0); }
            set { this._Order.AccountID = value; }
        }

        /// <summary>
        /// Gets or sets the status for this order
        /// </summary>
        public int OrderStateID
        {
            get { return this._Order.OrderStateID.GetValueOrDefault(0); }
            set { this._Order.OrderStateID = value; }
        }

        /// <summary>
        /// Gets or sets the shipping type for this order
        /// </summary>
        public int? ShippingID
        {
            get { return this._Order.ShippingID.GetValueOrDefault(0); }
            set { this._Order.ShippingID = value; }
        }

        /// <summary>
        /// Gets or sets the payment type for this order
        /// </summary>
        public int? PaymentTypeId
        {
            get { return this._Order.PaymentTypeId; }
            set { this._Order.PaymentTypeId = value; }
        }

        /// <summary>
        /// Gets or sets the payment setting for this order
        /// </summary>
        public int? PaymentSettingID
        {
            get { return this._Order.PaymentSettingID; }
            set { this._Order.PaymentSettingID = value; }
        }

        /// <summary>
        /// Gets or sets the transaction id for this order
        /// </summary>
        public string CardTransactionID
        {
            get { return this._Order.CardTransactionID; }
            set { this._Order.CardTransactionID = value; }
        }

        /// <summary>
        /// Gets or sets the purchase _Order number applied by the customer,
        /// if Purchase _Order selected for this order
        /// </summary>
        public string PurchaseOrderNumber
        {
            get { return this._Order.PurchaseOrderNumber; }
            set { this._Order.PurchaseOrderNumber = value; }
        }

        /// <summary>
        /// Gets or sets the type of the card
        /// </summary>
        public int? PaymentStatusID
        {
            get { return this._Order.PaymentStatusID; }
            set { this._Order.PaymentStatusID = value; }
        }

        /// <summary>
        /// Gets or sets the tax cost for this order
        /// </summary>
        public decimal TaxCost
        {
            get { return this._Order.TaxCost.GetValueOrDefault(0); }
            set { this._Order.TaxCost = value; }
        }

        /// <summary>
        /// Gets or sets the tax cost for this order
        /// </summary>
        public decimal VAT
        {
            get { return this._Order.VAT.GetValueOrDefault(0); }
            set { this._Order.VAT = value; }
        }

        /// <summary>
        /// Gets or sets the tax cost for this order
        /// </summary>
        public decimal SalesTax
        {
            get { return this._Order.SalesTax.GetValueOrDefault(0); }
            set { this._Order.SalesTax = value; }
        }

        /// <summary>
        /// Gets or sets the tax cost for this order
        /// </summary>
        public decimal HST
        {
            get { return this._Order.HST.GetValueOrDefault(0); }
            set { this._Order.HST = value; }
        }

        /// <summary>
        /// Gets or sets the tax cost for this order
        /// </summary>
        public decimal GST
        {
            get { return this._Order.GST.GetValueOrDefault(0); }
            set { this._Order.GST = value; }
        }

        /// <summary>
        /// Gets or sets the tax cost for this order
        /// </summary>
        public decimal PST
        {
            get { return this._Order.PST.GetValueOrDefault(0); }
            set { this._Order.PST = value; }
        }

        /// <summary>
        /// Gets or sets the shipping cost for this order
        /// </summary>
        public decimal ShippingCost
        {
            get { return this._Order.ShippingCost.GetValueOrDefault(0); }
            set { this._Order.ShippingCost = value; }
        }

        /// <summary>
        /// Gets or sets the sub-total amount for this order
        /// </summary>
        public decimal SubTotal
        {
            get { return this._Order.SubTotal.GetValueOrDefault(0); }
            set { this._Order.SubTotal = value; }
        }

        /// <summary>
        /// Gets or sets the total amount for this order
        /// </summary>
        public decimal Total
        {
            get { return this._Order.Total.GetValueOrDefault(0); }
            set { this._Order.Total = value; }
        }

        /// <summary>
        /// Gets or sets the ordered date
        /// </summary>
        public DateTime OrderDate
        {
            get { return this._Order.OrderDate.GetValueOrDefault(DateTime.Today); }
            set { this._Order.OrderDate = value; }
        }
       
        /// <summary>
        /// Gets or sets the Credit card Number  
        /// </summary>
        public string CreditCardNumber
        {
            get { return this._CreditCardNumber; }
            set { this._CreditCardNumber = value; }
        }
        
        /// <summary>
        /// Gets or sets the Credit card expiration date
        /// </summary>
        public string CreditCardExp
        {
            get { return this._Order.CardExp; }
            set { this._Order.CardExp = value; }
        }

        /// <summary>
        /// Gets or sets the Credit card Security code
        /// </summary>
        public string CreditCardCVV
        {
            get { return this._CardSecurityCode; }
            set { this._CardSecurityCode = value; }
        }
       
        /// <summary>
        ///  Gets or sets the coupon discount amount for this order
        /// </summary>
        public decimal DiscountAmount
        {
            get { return this.Discount; }
            set { this._Order.DiscountAmount = value; }
        }
        
        /// <summary>
        ///  Gets the coupon discount for this order
        /// </summary>
        public decimal Discount
        {
            get
            {
                decimal disc = 0;
                if (this.shoppingCart != null)
                {
                    disc = this.shoppingCart.Discount;
                }

                return disc;
            }
        }

        /// <summary>
        ///  Gets or sets the Additional customer Instructions for this order
        /// </summary>
        public string AdditionalInstructions
        {
            get { return this._Order.AdditionalInstructions; }
            set { this._Order.AdditionalInstructions = value; }
        }

        /// <summary>
        ///  Gets or sets the coupon codes applied by the customer for this _Order
        /// </summary>
        public string CouponCode
        {
            get { return this._Order.CouponCode; }
            set { this._Order.CouponCode = value; }
        }

        /// <summary>
        ///  Gets or sets the referral account Id for this _Order
        /// </summary>
        public int? ReferralAccountId
        {
            get { return this._Order.ReferralAccountID; }
            set { this._Order.ReferralAccountID = value; }
        }

        /// <summary>
        ///  Gets or sets the custom1 data for this _Order
        /// </summary>
        public string Custom1
        {
            get { return this._Order.Custom1; }
            set { this._Order.Custom1 = value; }
        }

        /// <summary>
        ///  Gets or sets the custom2 data for this _Order
        /// </summary>
        public string Custom2
        {
            get { return this._Order.Custom2; }
            set { this._Order.Custom2 = value; }
        }

        /// <summary>
        ///  Gets or sets the custom3 data for this _Order
        /// </summary>
        public string Custom3
        {
            get { return this._Order.Custom3; }
            set { this._Order.Custom3 = value; }
        }
        #endregion

        #region PRFT Custom Code
        public string ExternalId
        {
            get { return this._Order.ExternalID; }
            set { this._Order.ExternalID = value; }
        }
        #endregion
    }
}
