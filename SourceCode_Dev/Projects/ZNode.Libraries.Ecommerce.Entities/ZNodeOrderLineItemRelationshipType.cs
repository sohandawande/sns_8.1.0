﻿
namespace ZNode.Libraries.ECommerce.Entities
{
    /// <summary>
    /// Specifies the status of the Order
    /// </summary>    
    public enum ZNodeOrderLineItemRelationshipType
    {
        /// <summary>
        /// Represents the order line item relationship is bundles.
        /// </summary>
        ProductBundles = 1,

        /// <summary>
        /// Represents the order line item relationship is addons.
        /// </summary>
        Addons = 2
    }
}
