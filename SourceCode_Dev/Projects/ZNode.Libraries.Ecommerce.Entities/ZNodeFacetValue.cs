﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZNode.Libraries.Ecommerce.Entities
{
   public class ZNodeFacetValue
   {
       public string AttributeValue { get; set; }
       public long FacetCount { set; get; }
       public bool Selected { get; set; }
       public string DisplayText
       {
           get { return string.Format("{0} ({1})", AttributeValue, FacetCount); }
       }
    }
}
