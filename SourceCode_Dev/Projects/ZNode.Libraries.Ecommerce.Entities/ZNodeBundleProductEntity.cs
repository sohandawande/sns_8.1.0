﻿using System;
using System.Xml.Serialization;

namespace ZNode.Libraries.ECommerce.Entities
{
    /// <summary>
    /// Represents a bundle product item
    /// </summary>    
    [Serializable()]
    [XmlRoot("ZNodeBundleProduct")]
    public class ZNodeBundleProductEntity : ZNodeProductBaseEntity
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the ZNodeBundleProductEntity class
        /// </summary>
        public ZNodeBundleProductEntity()
        {
        }

		/// <summary>
		/// Initializes a new instance of the ZNodeBundleProductEntity class
		/// </summary>
		public ZNodeBundleProductEntity(ZNodeProductBaseEntity product)
		{
			// General Settings
			this._Name = product.Name;
			this._PortalID = product.PortalID;
			this._ProductID = product.ProductID;
			this._ProductNum = product.ProductNum;
			this._ProductTypeID = product.ProductTypeID;
			this._ExpirationPeriod = product.ExpirationPeriod;
			this._ExpirationFrequency = product.ExpirationFrequency;
			this._ImageFile = product.ImageFile;
			this._IsActive = product.IsActive;
			this._ManufacturerID = product.ManufacturerID;
			this._ManufacturerPartNum = product.ManufacturerPartNum;
			this._MaxQty = product.MaxQty;
			this._MinQty = product.MinQty;
			this._shortDescription = product.ShortDescription;
			this._sku = product.SKU;			
			this._DisplayOrder = product.DisplayOrder;
			this._downloadLink = product.DownloadLink;

			this._Guid = product.GUID;
			this._selectedSKU = product.SelectedSKUvalue;			
			this._seoURL = product.SEOURL;

			// Collection Properties			
			this._tieredPriceCollection = product.ZNodeTieredPriceCollection;
			this._selectedAddOnCollection = product.SelectedAddOns;
			this._selectedAddOn = product.SelectedAddonValues;			

			// Inventory Settings
			this._QuantityOnHand = product.QuantityOnHand;
			this._AllowBackOrder = product.AllowBackOrder;
			this._TrackInventoryInd = product.TrackInventoryInd;
			this._BackOrderMsg = product.BackOrderMsg;
			this._CallMessage = product.CallMessage;
			this._AffiliateUrl = product.AffiliateUrl;

			// Shipping & tax
			this._shippingCost = product.ShippingCost;
			this._ShippingRuleTypeID = product.ShippingRuleTypeID;
			this._shipSeparately = product.ShipSeparately;
			
			this._freeShippingInd = product.FreeShippingInd;
			this._TaxClassID = product.TaxClassID;
			this._ShippingRate = product.ShippingRate;

			// Product pricing
			this._RetailPrice = product.RetailPrice;
			this._SalePrice = product.SalePrice;
			this._WholesalePrice = product.WholesalePrice;

			// Product Dimensions
			this._height = product.Height;
			this._length = product.Length;
			this._width = product.Width;
			this._Weight = product.Weight;

			// Custom Properties
			this._Custom1 = product.Custom1;
			this._Custom2 = product.Custom2;
			this._Custom3 = product.Custom3;
			this._ImageAltTag = product.ImageAltTag;

			// Customer Review details
			this._reviewRating = product.ReviewRating;
			this._totalReviews = product.TotalReviews;

			// Recurring Billing
			this._RecurringBillingInd = product.RecurringBillingInd;
			this._RecurringBillingTotalCycles = product.RecurringBillingTotalCycles;
			this._RecurringBillingPeriod = product.RecurringBillingPeriod;
			this._RecurringBillingFrequency = product.RecurringBillingFrequency;
			this._RecurringBillingInstallmentInd = product.RecurringBillingInstallmentInd;
			this._RecurringBillingInitialAmount = product.RecurringBillingInitialAmount;

			// Supplier Detail
			this._SupplierID = product.SupplierID;

			// Bundle Product Collection
			this.ZNodeBundleProductCollection = product.ZNodeBundleProductCollection;

			this.isPromotionApplied = product.IsPromotionApplied;

			this._skuProfile = product.SkuProfile;
		}
        #endregion
    }
}
