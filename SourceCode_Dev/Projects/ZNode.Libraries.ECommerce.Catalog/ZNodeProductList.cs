using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;
using System.Web;

namespace ZNode.Libraries.ECommerce.Catalog
{
    /// <summary>
    /// Provides methods for list of home specials products and collection of products for a category
    /// </summary>
    [Serializable()]
    [XmlRoot()]
    public class ZNodeProductList : ZNodeBusinessBase
    {
        #region Private Member Variables
        private int totalPageCount = 0;
        private int totalRecordCount = 0;

        /// <summary>
        /// Represents Collection of products
        /// </summary>
        private ZNodeGenericCollection<ZNodeProductBase> productCollection = new ZNodeGenericCollection<ZNodeProductBase>();
        #endregion

        /// <summary>
        /// Initializes a new instance of the ZNodeProductList class.
        /// </summary>
        public ZNodeProductList() 
        { 
        }

        #region Public Properties

        /// <summary>
        /// Gets or sets Collection of products contained within this category
        /// </summary>
        [XmlElement("ZNodeProduct")]
        public ZNodeGenericCollection<ZNodeProductBase> ZNodeProductCollection
        {
            get
            {
                return this.productCollection;
            }

            set
            {
                this.productCollection = value;
            }
        }

        /// <summary>
        /// Gets or sets Collection of products contained within this category
        /// </summary>
        [XmlElement("TotalPageCount")]
        public int TotalPageCount
        {
            get
            {
                return this.totalPageCount;
            }

            set
            {
                this.totalPageCount = value;
            }
        }

        /// <summary>
        /// Gets or sets Collection of products contained within this category
        /// </summary>
        [XmlElement("TotalRecordCount")]
        public int TotalRecordCount
        {
            get
            {
                return this.totalRecordCount;
            }

            set
            {
                this.totalRecordCount = value;
            }
        }

        #endregion

        /// <summary>
        /// Returns a product list of home page specials
        /// </summary>
        /// <param name="portalId">Portal Id of the requested products</param>
        /// <param name="localeId">Locale Id of the requested products</param>
        /// <returns>ZNodeProductList Object</returns>
        public static ZNodeProductList GetHomePageSpecials(int portalId, int localeId)
        {
            ZNodeProductList productList = (ZNodeProductList)System.Web.HttpRuntime.Cache["SpecialsNavigation" + ZNodeConfigManager.SiteConfig.PortalID + ZNodeCatalogManager.LocaleId + ZNodeProfile.CurrentUserProfileId];

            if (productList == null)
            {
                int displayItem = 0;

                if (System.Configuration.ConfigurationManager.AppSettings["FeaturedProductsDisplayItem"] != null)
                {
                    int.TryParse(System.Configuration.ConfigurationManager.AppSettings["FeaturedProductsDisplayItem"].ToString(), out displayItem);
                }

                var externalAccountNo = string.Empty;
                if (ZNode.Libraries.ECommerce.UserAccount.ZNodeUserAccount.CurrentAccount() != null && ZNodeConfigManager.SiteConfig.EnableCustomerPricing.GetValueOrDefault() && ZNode.Libraries.ECommerce.UserAccount.ZNodeUserAccount.CurrentAccount().EnableCustomerPricing)
                    externalAccountNo = ZNode.Libraries.ECommerce.UserAccount.ZNodeUserAccount.CurrentAccount().ExternalAccountNo;

	            productList = ZnodeCatalogFactory.GetHomePageSpecials(portalId, localeId, displayItem,
                                                                      ZNodeProfile.CurrentUserProfileId, externalAccountNo);            

                productList.ApplyPromotion();

                ZNodeCacheDependencyManager.Insert("SpecialsNavigation" + ZNodeConfigManager.SiteConfig.PortalID + ZNodeCatalogManager.LocaleId 
                                            + ZNodeProfile.CurrentUserProfileId, 
                                            productList,
                                            DateTime.Now.AddHours(1),
                                            System.Web.Caching.Cache.NoSlidingExpiration,
                                            "ZNodePromotion", "ZNodeProduct", "ZNodeProductCategory", 
                                            "ZNodeCategoryNode", "ZNodeSKU", "ZNodeSKUProfile", "ZNodeSKUProfileEffective");                
            }

            return productList;
        }
       
        /// <summary>
        /// Returns a product list for a brand
        /// </summary>
        /// <param name="portalId">Portal Id of the request products</param>
        /// <param name="manufacturerId">Manufacturer (Brand) Id of the request products</param>
        /// <param name="localeId">Locale Id of the request products</param>
        /// <returns>ZNodeProductList object</returns>
        public static ZNodeProductList GetProductsByBrand(int portalId, int manufacturerId, int localeId)
        {
            ZNode.Libraries.DataAccess.Custom.ProductHelper productHelper = new ZNode.Libraries.DataAccess.Custom.ProductHelper();
            StringBuilder xmlOut = new StringBuilder();
            xmlOut.Append("<ZNodeProductList>");
            xmlOut.Append(productHelper.GetProductsByBrandXML(portalId, manufacturerId, localeId));
            xmlOut.Append("</ZNodeProductList>");

            // serialize the object
            ZNodeSerializer ser = new ZNodeSerializer();
            ZNodeProductList productList = (ZNodeProductList)ser.GetContentFromString(xmlOut.ToString(), typeof(ZNodeProductList));
            productList.ApplyPromotion();

            return productList;
        }

        /// <summary>
        /// Returns a product list for a price filter
        /// </summary>
        /// <param name="portalId">Portal id of the requested products</param>
        /// <param name="localeId">Locale id of the requested products</param>
        /// <param name="startValue">Price Start Value of the requested products</param>
        /// <param name="endValue">Price End value of the requested products</param>
        /// <returns>ZNodeProductList object</returns>
        public static ZNodeProductList GetProductsByPrice(int portalId, int localeId, decimal startValue, decimal endValue)
        {
            ZNode.Libraries.DataAccess.Custom.ProductHelper productHelper = new ZNode.Libraries.DataAccess.Custom.ProductHelper();
            StringBuilder xmlOut = new StringBuilder();
            xmlOut.Append("<ZNodeProductList>");
            xmlOut.Append(productHelper.GetProductsByPriceXML(portalId, localeId, startValue, endValue));
            xmlOut.Append("</ZNodeProductList>");

            // serialize the object
            ZNodeSerializer ser = new ZNodeSerializer();
            ZNodeProductList productList = (ZNodeProductList)ser.GetContentFromString(xmlOut.ToString(), typeof(ZNodeProductList));
            productList.ApplyPromotion();

            return productList;
        } 
                       
        /// <summary>
        /// Gets a page of products from the DataSource.
        /// </summary>
        /// <param name="portalId">The portal Id to search for.</param>
        /// <param name="name">The product name to search for.</param>
        /// <param name="productNum">The product number to search for.</param>
        /// <param name="sku">The product Sku to search for.</param>
        /// <param name="brand">The product brand to search for.</param>
        /// <param name="category">The product category to search for.</param>
        /// <param name="pageIndex">Row number at which to start reading.</param>
        /// <param name="pageSize">Number of rows to return.</param>
        /// <param name="catalogId">Catalog Id to return.</param>
        /// <param name="filterPortalID"></param>
        /// <param name="totalPages">Out Parameter, Number of rows in the DataSource.</param>
        /// <param name="externalAccountNo"></param>
        /// <returns>Returns a product collection as datatable.</returns>
        public static System.Data.DataTable GetPagedProductListByPortalID(int portalId, string name, string productNum, string sku, string brand, string category, int pageIndex, int pageSize, int catalogId, int? filterPortalID, out int totalPages, string externalAccountNo = "")
        {
            ZNode.Libraries.DataAccess.Custom.ProductHelper productHelper = new ZNode.Libraries.DataAccess.Custom.ProductHelper();

            return productHelper.GetPagedProductListByPortalID(portalId, name, productNum, sku, brand, category, pageIndex, pageSize, catalogId, filterPortalID, out totalPages,externalAccountNo);
        }

        /// <summary>
        /// Gets a page of products from the DataSource.
        /// </summary>
        /// <param name="portalId">The portal Id to search for.</param>
        /// <param name="name">The product name to search for.</param>
        /// <param name="productNum">The product number to search for.</param>
        /// <param name="sku">The product Sku to search for.</param>
        /// <param name="brand">The product brand to search for.</param>
        /// <param name="category">The product category to search for.</param>
        /// <param name="pageIndex">Row number at which to start reading.</param>
        /// <param name="pageSize">Number of rows to return.</param>
        /// <param name="catalogId">Catalog Id to return.</param>
        /// <param name="filterPortalID"></param>
        /// <param name="totalPages">Out Parameter, Number of rows in the DataSource.</param>
        /// <param name="totalRowCount">Out Parameter, total count in the DataSource.</param>
        /// <param name="externalAccountNo"></param>
        /// <returns>Returns a product collection as datatable.</returns>
        public static System.Data.DataTable GetPagedProductListByPortalID(int portalId, string name, string productNum, string sku, string brand, string category, int pageIndex, int pageSize, int catalogId, int? filterPortalID, out int totalPages, out int totalRowCount, string externalAccountNo = "")
        {
            ZNode.Libraries.DataAccess.Custom.ProductHelper productHelper = new ZNode.Libraries.DataAccess.Custom.ProductHelper();

            return productHelper.GetPagedProductListByPortalID(portalId, name, productNum, sku, brand, category, pageIndex, pageSize, catalogId, filterPortalID, out totalPages, externalAccountNo,out totalRowCount);
        }

        /// <summary>
        /// Gets products list from the DataSource.
        /// </summary>
        /// <param name="portalId">The portal Id to search for.</param>       
        /// <returns>Returns a product collection as dataset.</returns>
        public static System.Data.DataSet GetProductListByPortalID(int portalId)
        {
            ZNode.Libraries.DataAccess.Custom.ProductHelper productHelper = new ZNode.Libraries.DataAccess.Custom.ProductHelper();

            return productHelper.GetProductListByPortalId(portalId);
        }

        /// <summary>
        /// Returns a product list of best seller items
        /// </summary>
        /// <param name="catalogID">Catalog Id of the Reqeusted Products</param>
        /// <param name="displayItem">Display Item of the Reqeusted Products</param>
        /// <param name="categoryID">Category Id of the Reqeusted Products</param>
        /// <param name="portalID">Portal Id of the Reqeusted Products</param>
        /// <returns>ZNodeProductList object</returns>
        public static ZNodeProductList GetBestSellers(int catalogID, int displayItem, int categoryID, int portalID)
        {
             ZNodeProductList productList = (ZNodeProductList)System.Web.HttpContext.Current.Cache["BestSellers" + ZNodeConfigManager.SiteConfig.PortalID + ZNodeCatalogManager.LocaleId];

             if (productList == null)
             {
                 ZNode.Libraries.DataAccess.Custom.ProductHelper productHelper = new ZNode.Libraries.DataAccess.Custom.ProductHelper();
                 StringBuilder xmlOut = new StringBuilder();
                 xmlOut.Append("<ZNodeProductList>");
                 xmlOut.Append(productHelper.GetBestSellerItems(catalogID, displayItem, categoryID, portalID));
                 xmlOut.Append("</ZNodeProductList>");

                 // serialize the object
                 ZNodeSerializer ser = new ZNodeSerializer();
                 productList = (ZNodeProductList)ser.GetContentFromString(xmlOut.ToString(), typeof(ZNodeProductList));
                 productList.ApplyPromotion();
                 
                 ZNodeCacheDependencyManager.Insert("BestSellers" + ZNodeConfigManager.SiteConfig.PortalID + ZNodeCatalogManager.LocaleId, productList, System.DateTime.UtcNow.AddHours(1), System.Web.Caching.Cache.NoSlidingExpiration, null);
             }

             return productList;
        }

        /// <summary>
        /// Returns a product list of best seller items
        /// </summary>
        /// <param name="accountID">Account ID of Wishlist</param>
        /// <returns>ZNodeProductList object</returns>
        public static ZNodeProductList GetWishListByAccountID(int accountID)
        {
            ZNode.Libraries.DataAccess.Custom.ProductHelper productHelper = new ZNode.Libraries.DataAccess.Custom.ProductHelper();
            StringBuilder xmlOut = new StringBuilder();
            xmlOut.Append("<ZNodeProductList>");
            xmlOut.Append(productHelper.GetWishListItemsByAccountID(accountID, ZNodeConfigManager.SiteConfig.PortalID));
            xmlOut.Append("</ZNodeProductList>");

            // serialize the object
            ZNodeSerializer ser = new ZNodeSerializer();
            ZNodeProductList productList = (ZNodeProductList)ser.GetContentFromString(xmlOut.ToString(), typeof(ZNodeProductList));
            productList.ApplyPromotion();

            return productList;
        }

        /// <summary>
        /// Apply Promotion Product if any.
        /// </summary>
        public void ApplyPromotion()
        {
            foreach (ZNodeProductBase product in this.ZNodeProductCollection)
            {               
                product.ApplyPromotion();
            }
        }
    }
}
