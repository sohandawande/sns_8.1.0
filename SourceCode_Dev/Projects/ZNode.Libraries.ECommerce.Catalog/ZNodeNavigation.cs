using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.SEO;
using ZNode.Libraries.ECommerce.Utilities;
using System.Linq;

namespace ZNode.Libraries.ECommerce.Catalog
{
    /// <summary>
    /// Provides methods to create Bread crumb,populate Store menu and tree view with Store categories
    /// </summary>
    public class ZNodeNavigation : ZNodeBusinessBase
    {
        #region Local variables
        private string _breadcrumbCid = string.Empty;
        #endregion

        #region Properties

        public string BreadCrumbCid
        {
            get { return _breadcrumbCid; }
            set { _breadcrumbCid = value; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Creates a bread crumb path using the category and product ids passed in
        /// </summary>
        /// <param name="CategoryId"></param>
        /// <param name="ProductId"></param>
        /// <param name="Separator"></param>
        /// <returns></returns>
        public string GetBreadCrumbPath(int CategoryId, int ProductId, string Separator, int PortalId)
        {
            StringBuilder sbBreadCrumbPath = new StringBuilder();

            if (CategoryId > 0)
            {
                CategoryHelper categoryHelper = new CategoryHelper();
                string delimitedString = categoryHelper.GetCategoryPathByCategoryId(CategoryId, PortalId);
                sbBreadCrumbPath.Append(ParsePath(delimitedString, Separator));
            }
            else if (ProductId > 0)
            {
                CategoryHelper categoryHelper = new CategoryHelper();
                string delimitedString = categoryHelper.GetCategoryPathByProductId(ProductId, PortalId);
                sbBreadCrumbPath.Append(ParsePath(delimitedString, Separator));
            }

            return sbBreadCrumbPath.ToString();
        }

        public DataSet GetNavigationMenu()
        {
            // check if the product already exists in the cache
            System.Data.DataSet ds = (System.Data.DataSet)HttpContext.Current.Cache["CategoryNavigation" + ZNodeConfigManager.SiteConfig.PortalID + ZNodeCatalogManager.LocaleId + ZNodeProfile.CurrentUserProfileId];

            if (ds == null)
            {
                CategoryHelper categoryHelper = new CategoryHelper();
                ds = categoryHelper.GetNavigationItems(ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId, ZNodeProfile.CurrentUserProfileId);

                // add the hierarchical relationship to the dataset
                ds.Relations.Add("NodeRelation", ds.Tables[0].Columns["CategoryNodeId"], ds.Tables[0].Columns["ParentCategoryNodeId"]);

                // Create two sql Cache Dependency objects for categorynode and product table
                ZNodeCacheDependencyManager.Insert("CategoryNavigation" + ZNodeConfigManager.SiteConfig.PortalID + ZNodeCatalogManager.LocaleId + ZNodeProfile.CurrentUserProfileId, ds, "ZNodeCategoryNode", "ZNodePortalCatalog", "ZNodeProductCategory", "ZNodeCategory", "ZNodeCategoryProfile");
            }

            return ds;
        }

        public string GetCategoryUrl(string categoryName)
        {
            // check if the product already exists in the cache
            System.Data.DataSet ds = GetNavigationMenu();

            DataRow dbRow = ds.Tables[0].Select("Name='" + categoryName + "'" ).FirstOrDefault();

            if (dbRow == null)
            {
                dbRow = ds.Tables[0].Rows.Cast<DataRow>().SelectMany(x => x.GetChildRows("NodeRelation")).Where(x => x["Name"].ToString() == categoryName).FirstOrDefault();                
            }

            if (dbRow != null)
            {
                return ZNodeSEOUrl.MakeURL(dbRow["CategoryId"].ToString(), SEOUrlType.Category, dbRow["SEOURL"].ToString());
            }

            return string.Empty;
        }


        public string GetParentCategoryTitle(string categoryTitle)
        {
            // check if the product already exists in the cache
            System.Data.DataSet ds = GetNavigationMenu();

            DataRow dbRow = ds.Tables[0].Rows.Cast<DataRow>().Where(x => x["ParentCategoryNodeID"] == System.DBNull.Value && x["Title"].ToString() == categoryTitle).FirstOrDefault();

            if (dbRow == null)
            {
                dbRow = ds.Tables[0].Rows.Cast<DataRow>().Where(x => x["ParentCategoryNodeID"] == System.DBNull.Value && x.GetChildRows("NodeRelation").Any(y => y["Title"].ToString() == categoryTitle)).FirstOrDefault();
            }

            if (dbRow != null)
            {
                return dbRow["Title"].ToString();
            }

            return string.Empty;
        }

        public string GetParentCategoryName(string categoryName)
        {
            // check if the product already exists in the cache
            System.Data.DataSet ds = GetNavigationMenu();

            DataRow dbRow = ds.Tables[0].Rows.Cast<DataRow>().Where(x => x["ParentCategoryNodeID"] == System.DBNull.Value && x["Name"].ToString() == categoryName).FirstOrDefault();

            if (dbRow == null)
            {
                dbRow = ds.Tables[0].Rows.Cast<DataRow>().Where(x => x["ParentCategoryNodeID"] == System.DBNull.Value && x.GetChildRows("NodeRelation").Any(y => x["Name"].ToString() == categoryName)).FirstOrDefault();
            }

            if (dbRow != null)
            {
                return dbRow["Name"].ToString();
            }

            return string.Empty;
        }



        /// <summary>
        /// Populate the store menu in the control reference passed in
        /// </summary>
        /// <param name="ctrlMenu"></param>
        public void PopulateStoreMenu(Menu ctrlMenu, string SelectedCategoryID, int MaximumDynamicDisplayLevel)
        {
            // check if the product already exists in the cache
            System.Data.DataSet ds = GetNavigationMenu();

            string theme = ZNodeCatalogManager.Theme;
            // If category theme wise theme selected then apply the current category theme.
            if (HttpContext.Current.Session["CurrentCategoryTheme"] != null)
            {
                theme = HttpContext.Current.Session["CurrentCategoryTheme"].ToString();
            }

            foreach (DataRow dbRow in ds.Tables[0].Rows)
            {
                if (dbRow.IsNull("ParentCategoryNodeID"))
                {
                    if ((bool)dbRow["VisibleInd"])
                    {
                        //create new menu item
                        MenuItem mi = new MenuItem();
                        mi.Text = dbRow["Name"].ToString();

                        mi.SeparatorImageUrl = "~/themes/" + theme + "/Images/menu_seperator.gif";

                        string categoryId = dbRow["CategoryId"].ToString();
                        string seoURL = dbRow["SEOURL"].ToString();

                        seoURL = ZNodeSEOUrl.MakeURL(categoryId, SEOUrlType.Category, seoURL);

                        mi.NavigateUrl = seoURL;

                        if (categoryId == SelectedCategoryID)
                        {
                            mi.Selected = true;
                        }

                        // add to tree control
                        ctrlMenu.Items.Add(mi);

                        
                        // recursively populate node
                        RecursivelyPopulateMenu(dbRow, mi, MaximumDynamicDisplayLevel);
                    }
                }
            }
        }

        /// <summary>
        /// Populate the store menu in the control reference passed in
        /// </summary>
        /// <param name="ctrlMenu"></param>
        public void PopulateStoreMenu(Menu ctrlMenu, string selectedCategoryID)
        {
            PopulateStoreMenu(ctrlMenu, selectedCategoryID, ctrlMenu.MaximumDynamicDisplayLevels);
        }

        /// <summary>
        /// Populate the store menu in the control reference passed in
        /// </summary>
        /// <param name="ctrlMenu"></param>
        public void PopulateStoreMenu(Menu ctrlMenu)
        {
            PopulateStoreMenu(ctrlMenu, "0", ctrlMenu.MaximumDynamicDisplayLevels);
        }

        /// <summary>
        /// Populates a treeview with store categories
        /// </summary>
        /// <param name="ctrlTreeView"></param>
        public void PopulateStoreTreeView(TreeView ctrlTreeView, string selectedCategoryId, bool showCategorySiblings)
        {
            // check if the product already exists in the cache
            System.Data.DataSet ds = (System.Data.DataSet)HttpRuntime.Cache["CategoryNavigation" + ZNodeConfigManager.SiteConfig.PortalID + ZNodeCatalogManager.LocaleId + ZNodeProfile.CurrentUserProfileId];

            if (ds == null)
            {
                CategoryHelper categoryHelper = new CategoryHelper();
                ds = categoryHelper.GetNavigationItems(ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId, ZNodeProfile.CurrentUserProfileId + ZNodeProfile.CurrentUserProfileId);

                // add the hierarchical relationship to the dataset
                ds.Relations.Add("NodeRelation", ds.Tables[0].Columns["CategoryNodeId"], ds.Tables[0].Columns["ParentCategoryNodeId"]);

                // Create two sql Cache Dependency objects for categorynode and product table
                ZNodeCacheDependencyManager.Insert("CategoryNavigation" + ZNodeConfigManager.SiteConfig.PortalID + ZNodeCatalogManager.LocaleId, ds, "ZNodeCategoryNode", "ZNodePortalCatalog", "ZNodeProductCategory", "ZNodeCategory", "ZNodeCategoryProfile");
            }

            if (selectedCategoryId.Length > 0 && !showCategorySiblings)
            {
                ds = RecursivelyBindCategories(ds, selectedCategoryId);
            }

            foreach (DataRow dbRow in ds.Tables[0].Rows)
            {
                if (dbRow.IsNull("ParentCategoryNodeId"))
                {
                    if ((bool)dbRow["VisibleInd"])
                    {
                        //create new tree node
                        TreeNode tn = new TreeNode();
                        tn.Text = "<span class='Rightarrow'>&raquo; </span>" + dbRow["Name"].ToString() + " (" + dbRow["ProductCount"].ToString() + ")";

                        string categoryId = dbRow["CategoryId"].ToString();
                        string seoURL = dbRow["SEOURL"].ToString();

                        seoURL = ZNodeSEOUrl.MakeURL(categoryId, SEOUrlType.Category, seoURL);

                        tn.NavigateUrl = seoURL;

                        tn.SelectAction = TreeNodeSelectAction.Expand;                        

                        ctrlTreeView.Nodes.Add(tn);

                        if (selectedCategoryId.Equals(categoryId))
                        {
                            tn.Selected = true;
                            RecursivelyExpandTreeView(tn);
                        }

                        if (selectedCategoryId.Trim().Length > 0)
                            RecursivelyPopulateTreeView(dbRow, tn, selectedCategoryId);
                    }
                }
            }
        }


        /// <summary>
        /// Populates a treeview with store specials
        /// </summary>
        /// <param name="ctrlTreeView"></param>
        public void PopulateSpecialsTreeView(TreeView ctrlTreeView)
        {
            ZNodeProductList prodList = (ZNodeProductList)HttpContext.Current.Cache["SpecialsNavigation" + ZNodeConfigManager.SiteConfig.PortalID + ZNodeCatalogManager.LocaleId];

            if (prodList == null)
            {

                prodList = ZNodeProductList.GetHomePageSpecials(ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId);

                // Create two sql Cache Dependency objects for categorynode and product table
                ZNodeCacheDependencyManager.Insert("SpecialsNavigation" + ZNodeConfigManager.SiteConfig.PortalID + ZNodeCatalogManager.LocaleId, prodList, "ZNodeCategoryNode", "ZNodePortalCatalog", "ZNodeProduct");
            }

            foreach (ZNodeProductBase prodItem in prodList.ZNodeProductCollection)
            {

                //create new tree node
                TreeNode tn = new TreeNode();
                tn.Text = prodItem.Name;

                string prodId = prodItem.ProductID.ToString();
                string seoURL = "";

                if (prodItem.SEOURL != null)
                    seoURL = prodItem.SEOURL;

                seoURL = ZNodeSEOUrl.MakeURL(prodId, SEOUrlType.Product, seoURL);

                tn.NavigateUrl = seoURL;

                tn.SelectAction = TreeNodeSelectAction.Expand;

                ctrlTreeView.Nodes.Add(tn);
            }
        }

        /// <summary>
        /// Populates a dropdown list control with store brands
        /// </summary>
        /// <param name="ctrlTreeView"></param>
        public void PopulateBrandDropDownList(DropDownList ctrlList)
        {
            // check if the brand navigation list already exists in the cache
            System.Data.DataTable manList = (System.Data.DataTable)HttpContext.Current.Cache["BrandDropDownNavigation" + ZNodeConfigManager.SiteConfig.PortalID + ZNodeCatalogManager.LocaleId];

            if (manList == null)
            {
                ManufacturerHelper helper = new ManufacturerHelper();
                manList = helper.GetBrandNavigationItems(ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId);

                ZNodeCacheDependencyManager.Insert("BrandDropDownNavigation" + ZNodeConfigManager.SiteConfig.PortalID + ZNodeCatalogManager.LocaleId, manList, "ZNodeCategoryNode", "ZNodePortalCatalog", "ZNodeProductCategory");
            }

            ListItem selectlabel = new ListItem("Shop by Brand", "0");
            ctrlList.Items.Insert(0, selectlabel);
            ctrlList.SelectedValue = "0";


            foreach (DataRow manItem in manList.Rows)
            {
                // Create new list item
                ListItem li = new ListItem();
                li.Text = manItem["Name"].ToString() + " (" + manItem["ProductsCount"].ToString() + ")";

                string manId = manItem["ManufacturerID"].ToString();

                li.Value = manId;
                ctrlList.Items.Add(li);
            }
        }

        /// <summary>
        /// Populates a treeview with pricing tiers
        /// </summary>
        /// <param name="ctrlTreeView"></param>
        public void PopulatePricedTreeView(TreeView ctrlTreeView)
        {
            Portal portal = (Portal)HttpContext.Current.Cache["PriceNavigation" + ZNodeConfigManager.SiteConfig.PortalID + ZNodeCatalogManager.LocaleId];

            if (portal == null)
            {
                PortalService portalServ = new PortalService();
                portal = portalServ.GetByPortalID(ZNodeConfigManager.SiteConfig.PortalID);

                ZNodeCacheDependencyManager.Insert("PriceNavigation" + ZNodeConfigManager.SiteConfig.PortalID + ZNodeCatalogManager.LocaleId, portal, "ZNodePortal");
            }

            //retrieve the maximum priced product
            int minLimit = (int)portal.ShopByPriceMin;

            //retrieve the maximum priced product
            int maxLimit = (int)portal.ShopByPriceMax;

            //get increment
            int increment = (int)portal.ShopByPriceIncrement;

            //set running tier
            int tier = minLimit;

            while (tier < maxLimit)
            {
                int minAmt = tier;
                int maxAmt = tier + increment;
                string currencySymbol = ZNodeCurrencyManager.GetCurrencyPrefix();
                string currencySuffix = ZNodeCurrencyManager.GetCurrencySuffix();

                System.Text.StringBuilder sbTier = new System.Text.StringBuilder();
                sbTier.Append(currencySymbol);
                sbTier.Append(minAmt.ToString() + currencySuffix);
                sbTier.Append(" - " + currencySymbol);
                sbTier.Append(maxAmt.ToString() + currencySuffix);

                //create new tree node
                TreeNode tn = new TreeNode();
                tn.Text = sbTier.ToString();

                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append("~/price.aspx");
                sb.Append("?min=");
                sb.Append(minAmt.ToString());
                sb.Append("&max=");
                sb.Append(maxAmt.ToString());

                tn.NavigateUrl = sb.ToString();
                tn.SelectAction = TreeNodeSelectAction.Expand;
                ctrlTreeView.Nodes.Add(tn);

                tier = tier + increment;
            }
        }

        #endregion

        #region Helper Functions

        /// <summary>
        /// Returns a bread crumb path from a delimited string
        /// </summary>
        /// <param name="DelimitedString"></param>
        /// <param name="Separator"></param>
        /// <returns></returns>
        public string ParsePath(string DelimitedString, string Separator)
        {
            StringBuilder sbPath = new StringBuilder();

            string[] delim1 = { "||" };
            string[] pathitems = DelimitedString.Split(delim1, StringSplitOptions.None);

            string categoryId = string.Empty;

            foreach (string pathitem in pathitems)
            {
                string[] delim2 = { "%%" };
                string[] items = pathitem.Split(delim2, StringSplitOptions.None);

                categoryId = items.GetValue(0).ToString();
                string categoryName = items.GetValue(1).ToString();
                string seoURL = items.GetValue(2).ToString();

                seoURL = ZNodeSEOUrl.MakeURL(categoryId, SEOUrlType.Category, seoURL);
                seoURL = seoURL.Replace("~/", "");

                sbPath.Append(Separator);
                sbPath.Append(CreateLink(seoURL, categoryName));
            }

            _breadcrumbCid = categoryId.ToString();

            return sbPath.ToString();
        }

        /// <summary>
        /// Creates a category page link
        /// </summary>
        /// <param name="CategoryName"></param>
        /// <param name="CategoryId"></param>
        /// <returns></returns>
        private string CreateCategoryLink(string CategoryName, string CategoryId)
        {
            StringBuilder link = new StringBuilder();
            link.Append("category.aspx?zcid=");
            link.Append(CategoryId);

            return CreateLink(link.ToString(), CategoryName);
        }

        /// <summary>
        /// Creates a generic <![CDATA[ <a>]]> link
        /// </summary>
        /// <param name="LinkURL"></param>
        /// <param name="LinkText"></param>
        /// <returns></returns>
        public string CreateLink(string LinkURL, string LinkText)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<a href='");
            sb.Append(LinkURL);
            sb.Append("'>");
            sb.Append(LinkText);
            sb.Append("</a>");

            return sb.ToString();
        }

        /// <summary>
        /// Recursively populate a particular node with it's children
        /// </summary>
        /// <param name="dbRow"></param>
        /// <param name="parentMenuItem"></param>
        /// <param name="MaximumDynamicDisplayLevel"></param>
        private void RecursivelyPopulateMenu(DataRow dbRow, MenuItem parentMenuItem, int maximumDynamicDisplayLevel)
        {
            if (parentMenuItem.Depth <= maximumDynamicDisplayLevel || maximumDynamicDisplayLevel == -1)
            {
                foreach (DataRow childRow in dbRow.GetChildRows("NodeRelation"))
                {
                    if ((bool)childRow["VisibleInd"])
                    {
                        MenuItem mi = new MenuItem();
                        mi.Text = childRow["Name"].ToString();

                        string categoryId = childRow["CategoryId"].ToString();
                        string seoURL = childRow["SEOURL"].ToString();

                        seoURL = ZNodeSEOUrl.MakeURL(categoryId, SEOUrlType.Category, seoURL);
                        if (HttpContext.Current.Session["TaggedValues"] != null)
                            mi.NavigateUrl = ConstructQueryStringFromSessionValues(seoURL);
                        else
                        mi.NavigateUrl = seoURL;

                        parentMenuItem.ChildItems.Add(mi);

                        RecursivelyPopulateMenu(childRow, mi, maximumDynamicDisplayLevel);
                    }
                }
            }
        }
        private string QKeyName(string squeryname)
        {
            return squeryname.Replace(" ", "_") + "=";
        }
          private string ConstructQueryStringFromSessionValues(string url)
        {
            string qstring = string.Empty;

			var facetService = new ZNode.Libraries.DataAccess.Service.FacetService();
			var facetGroupService = new ZNode.Libraries.DataAccess.Service.FacetGroupService();

            if (HttpContext.Current.Session["TaggedValues"] != null)
            {
                string[] keyValues = HttpContext.Current.Session["TaggedValues"].ToString().Split(',');

                foreach (string sessionkey in keyValues)
                {
                    if (!string.IsNullOrEmpty(sessionkey))
                    {
                        //if (sessionkey == tagId.ToString() || tagId == 0)
                        //{
                        //    continue;
                        //}

						Facet facet = facetService.GetByFacetID(Convert.ToInt32(sessionkey));

                        if (facet != null)
                        {
							qstring = qstring + (qstring.Trim().Length == 0 ? string.Empty : "&") + this.QKeyName(facetGroupService.GetByFacetGroupID(facet.FacetGroupID.Value).FacetGroupLabel) + HttpContext.Current.Server.UrlEncode(facet.FacetName);
                        }
                    }
                }
            }
         

            string qurl = url;

            if (qstring.Trim().Length > 0)
            {
                qurl = url + ((url.IndexOf('?') > 0) ? "&" : "?") + qstring.Trim();
            }

            return qurl;
        }

     
        //Recursively populate a particular node with it's children
        private void RecursivelyPopulateTreeView(DataRow dbRow, TreeNode parentNode, string selectedCategoryId)
        {
            ZNodeUrl url = new ZNodeUrl();

            foreach (DataRow childRow in dbRow.GetChildRows("NodeRelation"))
            {
                if ((bool)childRow["VisibleInd"])
                {
                    //create new tree node
                    TreeNode tn = new TreeNode();
                    tn.Text = "<span class='Rightarrow'>&raquo; </span>" + childRow["Name"].ToString() + " (" + childRow["ProductCount"].ToString() + ")";

                    string categoryId = childRow["CategoryId"].ToString();
                    string seoURL = childRow["SEOURL"].ToString();

                    seoURL = ZNodeSEOUrl.MakeURL(categoryId, SEOUrlType.Category, seoURL);


                    if (HttpContext.Current.Session["TaggedValues"] != null)
                        tn.NavigateUrl = ConstructQueryStringFromSessionValues(seoURL);
                    else
                    tn.NavigateUrl = seoURL;                    

                    parentNode.ChildNodes.Add(tn);

                    if (selectedCategoryId.Equals(categoryId))
                    {
                        tn.Selected = true;
                        tn.Expand();
                        RecursivelyExpandTreeView(tn);
                    }

                    RecursivelyPopulateTreeView(childRow, tn, selectedCategoryId);
                }
            }
        }

        /// <summary>
        /// Recursively expands parent nodes of a selected tree node
        /// </summary>
        /// <param name="nodeItem"></param>
        private void RecursivelyExpandTreeView(TreeNode nodeItem)
        {
            if (nodeItem.Parent != null)
            {
                nodeItem.Parent.Expand();
                RecursivelyExpandTreeView(nodeItem.Parent);
            }
            else
            {
                nodeItem.Expand();
                return;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="CategoryList"></param>
        /// <param name="SelectedCategoryId"></param>
        /// <returns></returns>
        protected DataSet RecursivelyBindCategories(DataSet CategoryList, string SelectedCategoryId)
        {
            CategoryList.Tables[0].PrimaryKey = new DataColumn[] { CategoryList.Tables[0].Columns["CategoryId"] };

            // Create new table with existing dataset table structure
            DataTable tempTable = CategoryList.Tables[0].Clone();

            // Get Selected category row from dataset
            DataRow selectedRow = CategoryList.Tables[0].Rows.Find(SelectedCategoryId);

            // Add it to temporary table
            tempTable.Rows.Add(selectedRow.ItemArray);


            if (!selectedRow.IsNull("ParentCategoryNodeId"))
            {
                RecursivelyBindChildCategories(selectedRow, tempTable, CategoryList);
            }

            // Get child rows for a selected row
            DataRow[] childRows = selectedRow.GetChildRows("NodeRelation");

            foreach (DataRow dr in childRows)
            {
                tempTable.Rows.Add(dr.ItemArray);
            }

            DataSet categoryDataSet = new DataSet();
            categoryDataSet.Tables.Add(tempTable);

            tempTable.Dispose();

            // add the hierarchical relationship to the dataset
            categoryDataSet.Relations.Add("NodeRelation", categoryDataSet.Tables[0].Columns["CategoryNodeId"], categoryDataSet.Tables[0].Columns["ParentCategoryNodeId"]);

            return categoryDataSet;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="SelectedRow"></param>
        /// <param name="TempTable"></param>
        /// <param name="CategoryList"></param>
        protected void RecursivelyBindChildCategories(DataRow SelectedRow, DataTable TempTable, DataSet CategoryList)
        {
            foreach (DataRow dbRow in CategoryList.Tables[0].Rows)
            {
                if (dbRow["CategoryNodeId"].ToString() == SelectedRow["ParentCategoryNodeId"].ToString())
                {
                    TempTable.Rows.Add(dbRow.ItemArray);

                    if (dbRow.IsNull("ParentCategoryNodeId"))
                    {
                        break;
                    }

                    RecursivelyBindChildCategories(dbRow, TempTable, CategoryList);
                }
            }
        }
        #endregion

    }
}