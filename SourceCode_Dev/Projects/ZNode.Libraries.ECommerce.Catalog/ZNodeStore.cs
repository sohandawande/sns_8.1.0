using System;
using System.Text;
using System.Xml.Serialization;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Catalog
{
    #region ZNodeStore
    /// <summary>
    /// Represents the properties and methods of a ZNodeStore.
    /// </summary>
    [Serializable()]
    [XmlRoot()]
    public class ZNodeStore : ZNodeBusinessBase
    {
        #region Protected Member Variables
        private int _StoreId;
        private string _Name;
        private string _Address1;
        private string _Address2;
        private string _Address3;
        private string _City;
        private string _State;
        private string _Zipcode;
        private string _Phone;
        private string _Fax;
        private string _ImageFile;
        private string _Custom1 = string.Empty;
        private string _Custom2 = string.Empty;
        private string _Custom3 = string.Empty;
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the ZNodeStore class
        /// </summary>
        public ZNodeStore() 
        {
        }
        #endregion

        #region Public Instance Properties

        /// <summary>
        /// Gets or sets the StoreId
        /// </summary>
        [XmlElement()]
        public int StoreID
        {
            get { return this._StoreId; }
            set { this._StoreId = value; }
        }

        /// <summary>
        /// Gets or sets name of the store
        /// </summary>
        [XmlElement()]
        public string Name
        {
            get
            {
                return this._Name;
            }
          
            set 
            {
                this._Name = value;
            }
        }

        /// <summary>
        /// Gets or sets address line1 of the Store
        /// </summary>
        [XmlElement()]
        public string Address1
        {
            get
            {
                return this._Address1;
            }
           
            set 
            { 
                this._Address1 = value; 
            }
        }

        /// <summary>
        /// Gets or sets address line2 of the Store
        /// </summary>
        [XmlElement()]
        public string Address2
        {
            get
            {
                return this._Address2;
            }
           
            set 
            { 
                this._Address2 = value; 
            }
        }

        /// <summary>
        /// Gets or sets address line3 of the Store
        /// </summary>
        [XmlElement()]
        public string Address3
        {
            get
            {
                return this._Address3;
            }
            
            set 
            { 
                this._Address3 = value; 
            }
        }

        /// <summary>
        /// Gets or sets city of the Store
        /// </summary>
        [XmlElement()]
        public string City
        {
            get
            {
                return this._City;
            }
            
            set 
            { 
                this._City = value; 
            }
        }

        /// <summary>
        /// Gets or sets the store state code
        /// </summary>
        [XmlElement()]
        public string State
        {
            get
            {
                return this._State;
            }
            
            set 
            { 
                this._State = value; 
            }
        }

        /// <summary>
        /// Gets or sets the store zip code(postal code)
        /// </summary>
        [XmlElement()]
        public string Zip
        {
            get
            {
                return this._Zipcode;
            }
            
            set 
            { 
                this._Zipcode = value; 
            }
        }

        /// <summary>
        /// Gets or sets the Store phone number
        /// </summary>
        [XmlElement()]
        public string Phone
        {
            get
            {
                return this._Phone;
            }
            
            set 
            {
                this._Phone = value; 
            }
        }

        /// <summary>
        /// Gets or sets the store fax number
        /// </summary>
        [XmlElement()]
        public string Fax
        {
            get
            {
                return this._Fax;
            }
            
            set 
            {
                this._Fax = value; 
            }
        }

        /// <summary>
        /// Gets or sets Image file property
        /// </summary>
        [XmlElement()]
        public string ImageFile
        {
            get
            {
                return this._ImageFile;
            }

            set 
            { 
                this._ImageFile = value; 
            }
        }

        /// <summary>
        /// Gets a mapquest address string
        /// </summary>
        [XmlIgnore()]
        public string MapQuestURL
        {
            get 
            {
                System.Text.StringBuilder sb = new StringBuilder();
                sb.Append("http://www.mapquest.com/maps/map.adp?country=US&countryid=US&addtohistory=&searchtab=address&searchtype=address");
                sb.Append("&address=");
                sb.Append(this.Address1);
                sb.Append("&city=");
                sb.Append(this.City);
                sb.Append("&state=");
                sb.Append(this.State);
                sb.Append("&zipcode=");
                sb.Append(this.Zip);

                return sb.ToString();
            }
        }

        /// <summary>
        /// Gets or sets the store Custom1
        /// </summary>
        [XmlElement()]
        public string Custom1
        {
            get { return this._Custom1; }
            set { this._Custom1 = value; }
        }

        /// <summary>
        /// Gets or sets the store Custom2
        /// </summary>
        [XmlElement()]
        public string Custom2
        {
            get { return this._Custom2; }
            set { this._Custom2 = value; }
        }

        /// <summary>
        /// Gets or sets the store Custom3
        /// </summary>
        [XmlElement()]
        public string Custom3
        {
            get { return this._Custom3; }
            set { this._Custom3 = value; }
        }
        
        #endregion
    }
    #endregion
}
