using System;
using System.Xml.Serialization;
using ZNode.Libraries.ECommerce.SEO;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Catalog
{
    /// <summary>
    /// Represents a cross-sell product item
    /// </summary>    
    [Serializable()]
    [XmlRoot("ZNodeCrossSellItem")]
    public class ZNodeCrossSellItem : ZNodeBusinessBase
    {
        #region Private Variables
        private int _ProductId = 0;
        private int _PortalId = 0;
        private string _Name = string.Empty;
        private string _ShortDescription = string.Empty;
        private string _ImageFile;
        private string _SeoURL = string.Empty;
        private string _ImageAltTag = string.Empty;
        private decimal _RetailPrice = 0;
        private decimal? _SalePrice;
        private bool _CallForPricing = false;
        private int _ReviewRating = 0;
        private int _TotalReviews = 0;
        private int _RelationTypeId = 0;
		private decimal? _wholeSalePrice = 0;

        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the ZNodeCrossSellItem class
        /// </summary>
        public ZNodeCrossSellItem()
        {
        }
        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the productid for this cross sell product
        /// </summary>
        [XmlElement()]
        public int ProductId
        {
            get
            {
                return this._ProductId;
            }

            set
            {
                this._ProductId = value;
            }
        }

        /// <summary>
        /// Gets or sets the site portal id
        /// </summary>
        [XmlElement()]
        public int PortalId
        {
            get
            {
                return this._PortalId;
            }

            set
            {
                this._PortalId = value;
            }
        }

        /// <summary>
        /// Gets or sets the cross sell product Name
        /// </summary>
        [XmlElement()]
        public string Name
        {
            get
            {
                return this._Name;
            }

            set
            {
                this._Name = value;
            }
        }

        /// <summary>
        /// Gets or sets the short description for this cross sell product.
        /// </summary>
        [XmlElement()]
        public string ShortDescription
        {
            get
            {
                return this._ShortDescription;
            }

            set
            {
                this._ShortDescription = value;
            }
        }

        /// <summary>
        /// Gets or sets the image file name for this cross sell product.
        /// </summary>
        [XmlElement()]
        public string ImageFile
        {
            get
            {
                return this._ImageFile;
            }

            set
            {
                this._ImageFile = value;
            }
        }

        /// <summary>
        /// Gets or sets the image file name for this cross sell product.
        /// </summary>
        [XmlElement()]
        public string SeoURL
        {
            get
            {
                return this._SeoURL;
            }

            set
            {
                this._SeoURL = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the call for pricing is enabled or not.
        /// </summary>
        [XmlElement()]
        public bool CallForPricing
        {
            get
            {
                return this._CallForPricing;
            }

            set
            {
                this._CallForPricing = value;
            }
        }

        /// <summary>
        /// Gets or sets the image file name for this cross sell product.
        /// </summary>
        [XmlElement()]
        public decimal RetailPrice
        {
            get
            {
                return this._RetailPrice;
            }

            set
            {
                this._RetailPrice = value;
            }
        }

        /// <summary>
        /// Gets or sets the sale price.
        /// </summary>
        [XmlElement()]
        public decimal? SalePrice
        {
            get
            {
                return this._SalePrice;
            }

            set
            {
                this._SalePrice = value;
            }
        }

        /// <summary>
        /// Gets the formatted price. Reconfigures the product base price and returns it as string
        /// Check the user's Login status and display the price based on Showpricing Status.
        /// </summary>
        [XmlIgnore()]
        public string FormattedPrice
        {
            get
            {
				if (this._SalePrice.HasValue || this._wholeSalePrice.HasValue)
				{
					decimal price = 0;
					price = this._SalePrice.HasValue ? this._SalePrice.Value : this._wholeSalePrice.Value;
					if (price == 0)
						return ZNodePricingFormatter.ConfigureProductPricing(this._RetailPrice);
					else
						return ZNodePricingFormatter.ConfigureProductPricing(this._RetailPrice, price);
				}
				else
				{
					return ZNodePricingFormatter.ConfigureProductPricing(this._RetailPrice);
				}
            }
        }

        /// <summary>
        /// Gets the fully qualified link to product details page
        /// </summary>
        [XmlIgnore()]
        public string ViewProductLink
        {
            get
            {
                return ZNodeSEOUrl.MakeURL(this._ProductId.ToString(), SEOUrlType.Product, this._SeoURL);
            }
        }

        /// <summary>
        /// Gets or sets the image alternative tag text for this product
        /// </summary>
        [XmlElement()]
        public string ImageAltTag
        {
            get
            {
                // if image Alt tag value not exists then use product name
                if (string.IsNullOrEmpty(this._ImageAltTag))
                {
                    return this._Name;
                }

                // Otherwise, return Alternative text
                return this._ImageAltTag;
            }

            set
            {
                this._ImageAltTag = value;
            }
        }

        /// <summary>
        /// Gets or sets the average review rating for this product
        /// </summary>
        [XmlElement()]
        public int ReviewRating
        {
            get
            {
                return this._ReviewRating;
            }

            set
            {
                this._ReviewRating = value;
            }
        }

        /// <summary>
        /// Gets or sets the total number of reviews for this product
        /// </summary>
        [XmlElement()]
        public int TotalReviews
        {
            get
            {
                return this._TotalReviews;
            }

            set
            {
                this._TotalReviews = value;
            }
        }

        /// <summary>
        /// Gets or sets the productid for this cross sell product
        /// </summary>
        [XmlElement()]
        public int RelationTypeId
        {
            get
            {
                return this._RelationTypeId;
            }

            set
            {
                this._RelationTypeId = value;
            }
        }

        /// <summary>
        /// Gets or sets the Back Order Message for this cross sell product.
        /// </summary>
        [XmlElement()]
        public string BackOrderMsg { get; set; }

        /// <summary>
        /// Gets or sets the Allow Back Order for this cross sell product.
        /// </summary>
        [XmlElement()]
        public bool AllowBackOrder { get; set; }

        /// <summary>
        /// Gets or sets the Allow Back Order for this cross sell product.
        /// </summary>
        [XmlElement()]
        public int QuantityOnHand { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the product is active.
        /// </summary>
        [XmlElement()]
        public bool IsActive { get; set; }

		[XmlElement()]
		public decimal? WholeSalePrice
		{
			get
			{
				return this._wholeSalePrice;
			}
			set
			{
				this._wholeSalePrice = value;
			}
		}
        #endregion
    }
}
