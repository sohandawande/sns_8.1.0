﻿
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using StructureMap;
using ZNode.Libraries.Search;
using ZNode.Libraries.Search.LuceneSearchProvider.Indexer;
using ZNode.Libraries.Search.LuceneSearchProvider.Search;
using ZNode.Libraries.Search.Interfaces;
using System.Data;
using ZNode.Libraries.Ecommerce.Entities;

namespace ZNode.Libraries.ECommerce.Catalog
{
    public class ZNodeSearchEngine
    {
        
        

        public ZNodeSearchEngineResult Search(string category, string searchText,string searchResultText, List<KeyValuePair<string, IEnumerable<string>>> refineBy , int sort)
        {
            IZNodeSearchRequest searchRequest = new LuceneSearchRequest();
            ZNodeSearchEngineResult result = new ZNodeSearchEngineResult();
    
            searchRequest.SearchText = searchText;
            searchRequest.InnerSearchKeywords = GetSearchKeywords(searchResultText);
            searchRequest.Category = category;
            searchRequest.PortalCatalogID = ZNodeCatalogManager.CatalogConfig.PortalCatalogID;
            searchRequest.SortOrder = sort;
            searchRequest.Facets = refineBy;
            var searchProvider = ObjectFactory.GetInstance<IZnodeSearchProvider>();
            // Default making true for Facets and Categories 
            searchRequest.GetFacets = true;
            searchRequest.GetCategoriesHeirarchy = true;
            IZNodeSearchResponse response = searchProvider.Search(searchRequest);

            if (response != null)
            {
                if (response.CategoryItems != null)
                {
                    Func<IEnumerable<IZNodeSearchCategoryItem>, IEnumerable<ZNodeCategoryNavigation>> SelectNavigation = null;
                    SelectNavigation = x => x.Select(y => new ZNodeCategoryNavigation() { Name = y.Name, Title = y.Title, NumberOfProducts = y.Count, CategoryHierarchy = SelectNavigation(y.Hierarchy) });

                    result.CategoryNavigation = SelectNavigation(response.CategoryItems);
                }
                if (response.Products != null)
                {
                    result.Ids = response.Products.Select(x => x.Id).ToList();
                }
                if (response.Facets != null)
                {
                    result.Facets = response.Facets.Select(x => new ZNodeFacet() { AttributeName = x.AttributeName, ControlTypeID = x.ControlTypeID, AttributeValues = x.AttributeValues.Select(y => new ZNodeFacetValue() { AttributeValue = y.AttributeValue, FacetCount = y.FacetCount }).ToList() }).ToList();
                }
                return result;
            }
            return null;
        }


        public List<ZnodeTypeAheadAutoComplete> SuggestTermsFor(string term, string category)
        {
            var searchProvider = ObjectFactory.GetInstance<IZnodeSearchProvider>();
            var response= searchProvider.SuggestTermsFor(term, category, ZNodeCatalogManager.CatalogConfig.PortalCatalogID,false);

            if (response != null && response.Count > 0)
            {
                return response.Select(x => new ZnodeTypeAheadAutoComplete() 
                {
                    label = string.IsNullOrEmpty( x.Category) ? x.Name : string.Format("{0} in {1}" ,x.Name, x.Category)  ,
                    name = x.Name,
                    value = x.Category
                }).ToList();

            }

            return new List<ZnodeTypeAheadAutoComplete>();
        }


        public void UpdateReader()
        {
            LuceneSearchProvider searchProvider = new LuceneSearchProvider();
            searchProvider.ReloadIndex();
        }

        private List<string> GetSearchKeywords(string searchResultText)
        {
            string[] stringSeparators = new[] {"||"};
            if (!string.IsNullOrEmpty(searchResultText))
            {
                return searchResultText.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries).ToList();

            }
           return null;
        }


        
    }
}

