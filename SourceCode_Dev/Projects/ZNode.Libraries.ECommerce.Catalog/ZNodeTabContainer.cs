﻿
namespace ZNode.Libraries.ECommerce.Catalog
{
    /// <summary>
    /// Represents the ZNodeTabContainer class
    /// </summary>
    public class ZNodeTabContainer : System.Web.UI.ITemplate
    {
        private System.Web.UI.Control childControls = null;

        public ZNodeTabContainer(System.Web.UI.Control ChildControls)
        {
            this.childControls = ChildControls;
        }

        #region ITemplate Members
        /// <summary>
        /// Add the container
        /// </summary>
        /// <param name="container">Container control</param>
        public void InstantiateIn(System.Web.UI.Control container)
        {
            container.Controls.Add(this.childControls);
        }

        #endregion
    }
}
