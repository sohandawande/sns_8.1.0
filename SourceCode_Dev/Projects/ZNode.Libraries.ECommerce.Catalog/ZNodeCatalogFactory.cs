using ZNode.Libraries.ECommerce.Utilities.Extensions;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Catalog
{
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using Entities;

    public static class ZnodeCatalogFactory
    {

        #region DataSet Methods

        public static ZNodeProduct CreateZnodeProduct(DataRow dr)
        {

            var product = new ZNodeProduct
                {
                    ProductID = dr.InferInt("ProductID"),
                    Name = dr.InferString("Name"),
                    RetailPrice = dr.InferDecimal("RetailPrice"),
                    SalePrice = dr.InferDecimal("SalePrice", null),
                    WholesalePrice = dr.InferDecimal("WholesalePrice", null),
                    ImageFile = dr.InferString("ImageFile"),
                    DisplayOrder = dr.InferInt("DisplayOrder"),
                    CallForPricing = dr.InferBool("CallForPricing"),
                    ShortDescription = dr.InferString("ShortDescription"),
                    Description = dr.InferString("Description"),
                    ProductNum = dr.InferString("ProductNum"),
                    ImageAltTag = dr.InferString("ImageAltTag"),
                    TaxClassID = dr.InferInt("TaxClassID"),
                    ProductTypeID = dr.InferInt("ProductTypeID"),
                    IsActive = dr.InferBool("ActiveInd"),
                    HomepageSpecial = dr.InferBool("HomepageSpecial"),
                    FeaturedInd = dr.InferBool("FeaturedInd"),
                    RowIndex = dr.InferInt("RowIndex"),
                    AlternateProductImageCount = dr.InferInt("AlternateProductImageCount"),
                    QuantityOnHand = dr.InferInt("QuantityOnHand"),
                    Weight = dr.InferDecimal("Weight"),
                    SEOURL = dr.InferString("SEOURL"),
                    SEOTitle = dr.InferString("SEOTitle"),
                    SEODescription = dr.InferString("SEODescription"),
                    SEOKeywords = dr.InferString("SEOKeywords"),
                    ShipSeparately = dr.InferBool("ShipSeparately"),
                    MinQty = dr.InferInt("MinQty"),
                    MaxQty = dr.InferInt("MaxQty"),
                    AllowBackOrder = dr.InferBool("AllowBackOrder"),
                    BackOrderMsg = dr.InferString("BackOrderMsg"),
                    TrackInventoryInd = dr.InferBool("TrackInventoryInd"),
                    ReviewRating = (int)dr.InferDecimal("ReviewRating"),
                    TotalReviews = dr.InferInt("TotalReviews"),
                    SupplierID = dr.InferInt("SupplierID"),
                    ShippingRate = dr.InferDecimal("ShippingRate"),
                    Franchisable = dr.InferBool("Franchisable")

                };

            return product;
        }

        public static ZNodeProductTier CreateZnodeProductTier(DataRow dr)
        {
            var productTier = new ZNodeProductTier
                {
                    ProductID = dr.InferInt("ProfileID"),
                    TierStart = dr.InferInt("TierStart"),
                    TierEnd = dr.InferInt("TierEnd"),
                    Price = dr.InferDecimal("Price")
                };

            return productTier;
        }

        public static ZnodeProductCategory CreateZnodeProductCategory(DataRow dr)
        {
            var productCategory = new ZnodeProductCategory
                {
                    Theme = dr.InferString("Theme"),
                    MasterPage = dr.InferString("MasterPage"),
                    CSS = dr.InferString("CSS"),
                    CategoryID = dr.InferInt("CategoryID")
                };

            return productCategory;
        }

        public static ZNodeHighlight CreateZnodeHighlight(DataRow dr)
        {
            var highlight = new ZNodeHighlight
                {
                    HighlightID = dr.InferInt("HighlightID"),
                    ImageFile = dr.InferString("Highlight_ImageFile"),
                    Name = dr.InferString("Highlight_Name")
                };

            return highlight;
        }

        public static ZNodeReview CreateZnodeReview(DataRow dr)
        {
            var review = new ZNodeReview
                {
                    Subject = dr.InferString("Subject"),
                    Comments = dr.InferString("Comments"),
                    CreateUser = dr.InferString("CreateUser"),
                    UserLocation = dr.InferString("UserLocation"),
                    Rating = dr.InferInt("Rating"),
                    CreateDate = dr.InferDate("CreateDate")
                };

            return review;
        }

        public static ZNodeCrossSellItem CreateZnodeCrossSellItem(DataRow dr)
        {
            var crossSellItem = new ZNodeCrossSellItem
                {
                    ProductId = dr.InferInt("RelatedProductId"),
                    Name = dr.InferString("CrossSell_Name"),
                    ImageFile = dr.InferString("CrossSell_ImageFile"),
                    ImageAltTag = dr.InferString("CrossSell_ImageAltTag"),
                    SeoURL = dr.InferString("CrossSell_SEOUrl")
                };

            return crossSellItem;
        }

        public static ZNodeAttributeType CreateZnodeAttributeType(DataRow dr)
        {
            var attributeType = new ZNodeAttributeType
                {
                    AttributeTypeId = dr.InferInt("AttributeTypeId"),
                    Name = dr.InferString("AttributeType_Name"),
                    DisplayOrder = dr.InferInt("DisplayOrder"),
                    Description = dr.InferString("Description"),
                    IsPrivate = dr.InferBool("IsPrivate")
                };

            return attributeType;
        }

        public static ZNodeAddOn CreateZnodeAddOn(DataRow dr)
        {
            var addOn = new ZNodeAddOn
                {
                    AddOnID = dr.InferInt("AddOnID"),
                    ProductId = dr.InferInt("ProductId"),
                    Title = dr.InferString("Title"),
                    Name = dr.InferString("Name"),
                    Description = dr.InferString("Description"),
                    DisplayType = dr.InferString("DisplayType"),
                    OptionalInd = dr.InferBool("OptionalInd"),
                    AllowBackOrder = dr.InferBool("AllowBackOrder"),
                    InStockMsg = dr.InferString("InStockMsg"),
                    OutOfStockMsg = dr.InferString("OutOfStockMsg"),
                    BackOrderMsg = dr.InferString("BackOrderMsg"),
                    TrackInventoryInd = dr.InferBool("TrackInventoryInd")
                };

            return addOn;
        }

        public static ZNodeAddOnValue CreateZnodeAddOnValue(DataRow dr)
        {
            var addonValue = new ZNodeAddOnValue
                {
                    AddOnId = dr.InferInt("AddOnValueId"),
                    Name = dr.InferString("AddOnValue_Name"),
                    Description = dr.InferString("AddOnValue_Description"),
                    DisplayOrder = dr.InferInt("AddOnValue_DisplayOrder"),
                    RetailPrice = dr.InferDecimal("RetailPrice"),
                    SalePrice = dr.InferDecimal("SalePrice", null),
                    WholesalePrice = dr.InferDecimal("WholesalePrice", null),
                    IsDefault = dr.InferBool("IsDefault"),
                    QuantityOnHand = dr.InferInt("QuantityOnHand"),
                    SKU = dr.InferString("SKU"),
                    Weight = dr.InferDecimal("Weight"),
                    Height = dr.InferDecimal("Height"),
                    Length = dr.InferDecimal("Length"),
                    ShippingRuleTypeID = dr.InferInt("ShippingRuleTypeID"),
                    FreeShippingInd = dr.InferBool("FreeShippingInd"),
                    SupplierID = dr.InferInt("SupplierID"),
                    RecurringBillingInd = dr.InferBool("RecurringBillingInd"),
                    RecurringBillingInstallmentInd = dr.InferBool("RecurringBillingInstallmentInd"),
                    RecurringBillingPeriod = dr.InferString("RecurringBillingPeriod"),
                    RecurringBillingFrequency = dr.InferString("RecurringBillingFrequency"),
                    RecurringBillingTotalCycles = dr.InferInt("RecurringBillingTotalCycles"),
                    RecurringBillingInitialAmount = dr.InferDecimal("RecurringBillingInitialAmount"),
                    TaxClassID = dr.InferInt("TaxClassID")
                };

            return addonValue;
        }

        public static ZNodeDigitalAsset CreateZnodeDigitalAsset(DataRow dr)
        {
            var digitalAsset = new ZNodeDigitalAsset
                {
                    DigitalAssetID = dr.InferInt("DigitalAssetID"),
                    DigitalAsset = dr.InferString("DigitalAsset"),
                    OrderLineItemID = dr.InferInt("OrderLineItemID", null)
                };

            return digitalAsset;
        }

        public static ZNodeSKU CreateZnodeSKU(DataRow dr)
        {
            var sku = new ZNodeSKU
                {
                    SKUID = dr.InferInt("SKUID"),
                    ProductID = dr.InferInt("ProductID"),
                    SupplierID = dr.InferInt("SupplierID"),
                    Note = dr.InferString("Note"),
                    WeightAdditional = dr.InferDecimal("WeightAdditional"),
                    SKUPicturePath = dr.InferString("SKUPicturePath"),
                    ImageAltTag = dr.InferString("ImageAltTag"),
                    DisplayOrder = dr.InferInt("DisplayOrder"),
                    RetailPriceOverride = dr.InferDecimal("RetailPriceOverride", null),
                    SalePriceOverride = dr.InferDecimal("SalePriceOverride", null),
                    WholesalePriceOverride = dr.InferDecimal("WholesalePriceOverride"),
                    RecurringBillingPeriod = dr.InferString("RecurringBillingPeriod"),
                    RecurringBillingFrequency = dr.InferString("RecurringBillingFrequency"),
                    RecurringBillingTotalCycles = dr.InferInt("RecurringBillingTotalCycles"),
                    RecurringBillingInitialAmount = dr.InferDecimal("RecurringBillingInitialAmount"),
                    IsActive = dr.InferBool("ActiveInd"),
                    Custom1 = dr.InferString("Custom1"),
                    Custom2 = dr.InferString("Custom2"),
                    Custom3 = dr.InferString("Custom3"),
                    ExternalID = dr.InferString("ExternalID"),
                    SKU = dr.InferString("SKU"),
                    QuantityOnHand = dr.InferInt("QuantityOnHand"),
                    ReorderLevel = dr.InferInt("ReOrderLevel"),
                    NegotiatedPrice = dr.InferDecimal("NegotiatedPrice", null)
                };

            return sku;
        }

        public static ZNodeCategory CreateZnodeCategory(DataRow dr)
        {
            var category = new ZNodeCategory
                {
                    CategoryID = dr.InferInt("CategoryID"),
                    Name = dr.InferString("Name"),
                    Title = dr.InferString("Title"),
                    Description = dr.InferString("Description"),
                    ShortDescription = dr.InferString("ShortDescription"),
                    ImageFile = dr.InferString("ImageFile"),
                    ImageAltTag = dr.InferString("ImageAltTag"),
                    VisibleInd = dr.InferBool("VisibleInd"),
                    SubCategoryGridVisibleInd = dr.InferBool("SubCategoryGridVisibleInd"),
                    SEOTitle = dr.InferString("SEOTitle"),
                    SEOKeywords = dr.InferString("SEOKeywords"),
                    SEODescription = dr.InferString("SEODescription"),
                    AlternateDescription = dr.InferString("AlternateDescription"),
                    DisplayOrder = dr.InferInt("DisplayOrder"),
                    Custom1 = dr.InferString("Custom1"),
                    Custom2 = dr.InferString("Custom2"),
                    Custom3 = dr.InferString("Custom3"),
                    SEOURL = dr.InferString("SEOURL"),
                    CategoryPath = dr.InferString("CategoryPath"),
                    Theme = dr.InferString("Theme"),
                    MasterPage = dr.InferString("MasterPage"),
                    Css = dr.InferString("CSS")
                };

            return category;
        }

        public static ZNodeManufacturer CreateZnodeManufacturer(DataRow dr)
        {
            var manufacturer = new ZNodeManufacturer
                {
                    ManufacturerID = dr.InferInt("ManufacturerID"),
                    Name = dr.InferString("Name"),
                    Description = dr.InferString("Description"),
                    ActiveInd = dr.InferBool("ActiveInd")
                };

            return manufacturer;
        }

        public static ZNodeAttribute CreateZnodeAttribute(DataRow dr)
        {
            return new ZNodeAttribute
            {
                AttributeId = dr.InferInt("AttributeId"),
                AttributeTypeId = dr.InferInt("AttributeTypeId"),
                Name = dr.InferString("Name"),
                DisplayOrder = dr.InferInt("DisplayOrder"),
                IsActive = dr.InferBool("ActiveInd"),
                //SkuAttributes = dr.InferBool("ActiveInd"),
            };
        }

        public static ZNodeSkuAttribute CreateZnodeSkuAttribute(DataRow dr)
        {
            return new ZNodeSkuAttribute
            {
                SKUID = dr.InferInt("SKUID"),
            };
        }

        #endregion

        #region SqlDataReader

        public static ZNodeProduct CreateZnodeProduct(SqlDataReader dr)
        {

            var product = new ZNodeProduct
            {
                ProductID = dr.InferInt("ProductID"),
                Name = dr.InferString("Name"),
                RetailPrice = dr.InferDecimal("RetailPrice"),
                SalePrice = dr.InferDecimal("SalePrice", null),
                WholesalePrice = dr.InferDecimal("WholesalePrice", null),
                ImageFile = dr.InferString("ImageFile"),
                DisplayOrder = dr.InferInt("DisplayOrder"),
                CallForPricing = dr.InferBool("CallForPricing"),
                ShortDescription = dr.InferString("ShortDescription"),
                Description = dr.InferString("Description"),
                ProductNum = dr.InferString("ProductNum"),
                ImageAltTag = dr.InferString("ImageAltTag"),
                TaxClassID = dr.InferInt("TaxClassID"),
                ProductTypeID = dr.InferInt("ProductTypeID"),
                IsActive = dr.InferBool("ActiveInd"),
                HomepageSpecial = dr.InferBool("HomepageSpecial"),
                FeaturedInd = dr.InferBool("FeaturedInd"),
                RowIndex = dr.InferInt("RowIndex"),
                AlternateProductImageCount = dr.InferInt("AlternateProductImageCount"),
                QuantityOnHand = dr.InferInt("QuantityOnHand"),
                Weight = dr.InferDecimal("Weight"),
                SEOURL = dr.InferString("SEOURL"),
                SEOTitle = dr.InferString("SEOTitle"),
                SEODescription = dr.InferString("SEODescription"),
                SEOKeywords = dr.InferString("SEOKeywords"),
                ShipSeparately = dr.InferBool("ShipEachItemSeparately"),
                MinQty = dr.InferInt("MinQty"),
                MaxQty = dr.InferInt("MaxQty", 10).Value,
                AllowBackOrder = dr.InferBool("AllowBackOrder"),
                BackOrderMsg = dr.InferString("BackOrderMsg"),
                TrackInventoryInd = dr.InferBool("TrackInventoryInd"),
                ReviewRating = (int)dr.InferDecimal("ReviewRating"),
                TotalReviews = dr.InferInt("TotalReviews"),
                PortalID = dr.InferInt("PortalID"),
                AccountID = dr.InferInt("AccountID"),
                OutOfStockMsg = dr.InferString("OutOfStockMsg"),
                InStockMsg = dr.InferString("InStockMsg"),
                ManufacturerID = dr.InferInt("ManufacturerID"),
                ManufacturerName = dr.InferString("Manufacturer_Name"),
                NewProductInd = dr.InferBool("NewProductInd"),
                AdditionalInformation = dr.InferString("AdditionalInformation"),
                FeaturesDesc = dr.InferString("FeaturesDesc"),
                ShippingRuleTypeID = dr.InferInt("ShippingRuleTypeID"),
                FreeShippingInd = dr.InferBool("FreeShippingInd"),
                RecurringBillingInd = dr.InferBool("RecurringBillingInd"),
                RecurringBillingInstallmentInd = dr.InferBool("RecurringBillingInstallmentInd"),
                RecurringBillingPeriod = dr.InferString("RecurringBillingPeriod"),
                RecurringBillingFrequency = dr.InferString("RecurringBillingFrequency"),
                RecurringBillingTotalCycles = dr.InferInt("RecurringBillingTotalCycles"),
                RecurringBillingInitialAmount = dr.InferDecimal("RecurringBillingInitialAmount"),
                Height = dr.InferDecimal("Height"),
                Length = dr.InferDecimal("Length"),
                Width = dr.InferDecimal("Width"),
                ExpirationPeriod = dr.InferInt("ExpirationPeriod"),
                ExpirationFrequency = dr.InferInt("ExpirationFrequency"),
                ShippingRate = dr.InferDecimal("ShippingRate"),
                SupplierID = dr.InferInt("SupplierID"),
                Franchisable = dr.InferBool("Franchisable"),
                InventoryDisplay = dr.InferInt("InventoryDisplay"),
                AffiliateUrl = dr.InferString("AffiliateUrl"),
                Custom1 = dr.InferString("Custom1"),
                Custom2 = dr.InferString("Custom2"),
                Custom3 = dr.InferString("Custom3")
            };

            return product;
        }

        public static ZNodeProductTier CreateZnodeProductTier(SqlDataReader dr)
        {
            var productTier = new ZNodeProductTier
            {
                ProfileID = dr.InferInt("ProfileID"),
                TierStart = dr.InferInt("TierStart"),
                TierEnd = dr.InferInt("TierEnd"),
                Price = dr.InferDecimal("Price")
            };

            return productTier;
        }

        public static ZnodeProductCategory CreateZnodeProductCategory(SqlDataReader dr)
        {
            var productCategory = new ZnodeProductCategory
            {
                Theme = dr.InferString("Theme"),
                MasterPage = dr.InferString("MasterPage"),
                CSS = dr.InferString("CSS"),
                CategoryID = dr.InferInt("CategoryID")
            };

            return productCategory;
        }

        public static ZNodeHighlight CreateZnodeHighlight(SqlDataReader dr)
        {
            var highlight = new ZNodeHighlight
            {
                HighlightID = dr.InferInt("HighlightID"),
                ImageFile = dr.InferString("Highlight_ImageFile"),
                Name = dr.InferString("Highlight_Name"),
                IsActive = dr.InferBool("Highlight_IsActive"),
                HyperlinkNewWinInd = dr.InferBool("Highlight_HyperlinkNewWindow"),
                Hyperlink = dr.InferString("Highlight_Hyperlink"),
                HighlightTypeID = dr.InferInt("Highlight_HighlightTypeId"),
                DisplayPopup = dr.InferBool("Highlight_DisplayPopup"),
                ImageAltTag = dr.InferString("Highlight_ImageAltTag")
            };

            return highlight;
        }

        public static ZNodeReview CreateZnodeReview(SqlDataReader dr)
        {
            var review = new ZNodeReview
            {
                Subject = dr.InferString("Subject"),
                Comments = dr.InferString("Comments"),
                CreateUser = dr.InferString("CreateUser"),
                UserLocation = dr.InferString("UserLocation"),
                Rating = dr.InferInt("Rating"),
                CreateDate = dr.InferDate("CreateDate")
            };

            return review;
        }

        public static ZNodeCrossSellItem CreateZnodeCrossSellItem(SqlDataReader dr)
        {
            var crossSellItem = new ZNodeCrossSellItem
            {
                ProductId = dr.InferInt("RelatedProductId"),
                Name = dr.InferString("CrossSell_Name"),
                ImageFile = dr.InferString("CrossSell_ImageFile"),
                ImageAltTag = dr.InferString("CrossSell_ImageAltTag"),
                SeoURL = dr.InferString("CrossSell_SEOUrl"),
                RelationTypeId = dr.InferInt("RelationTypeId"),
                BackOrderMsg = dr.InferString("BackOrderMsg"),
                AllowBackOrder = dr.InferBool("AllowBackOrder"),
                QuantityOnHand = dr.InferInt("QuantityOnHand"),
                IsActive = dr.InferBool("ActiveInd")
            };

            return crossSellItem;
        }

        public static ZNodeAttributeType CreateZnodeAttributeType(SqlDataReader dr)
        {
            var attributeType = new ZNodeAttributeType
            {
                AttributeTypeId = dr.InferInt("AttributeTypeId"),
                Name = dr.InferString("AttributeType_Name"),
                DisplayOrder = dr.InferInt("DisplayOrder"),
                Description = dr.InferString("Description"),
                IsPrivate = dr.InferBool("IsPrivate")
            };

            return attributeType;
        }

        public static ZNodeAddOn CreateZnodeAddOn(SqlDataReader dr)
        {
            var addOn = new ZNodeAddOn
            {
                AddOnID = dr.InferInt("AddOnID"),
                ProductId = dr.InferInt("ProductId"),
                Title = dr.InferString("Title"),
                Name = dr.InferString("AddOn_Name"),
                Description = dr.InferString("Description"),
                DisplayType = dr.InferString("DisplayType"),
                OptionalInd = dr.InferBool("OptionalInd"),
                AllowBackOrder = dr.InferBool("AllowBackOrder"),
                InStockMsg = dr.InferString("InStockMsg"),
                OutOfStockMsg = dr.InferString("OutOfStockMsg"),
                BackOrderMsg = dr.InferString("BackOrderMsg"),
                TrackInventoryInd = dr.InferBool("TrackInventoryInd")
            };

            return addOn;
        }

        public static ZNodeAddOnValue CreateZnodeAddOnValue(SqlDataReader dr)
        {
            var addonValue = new ZNodeAddOnValue
            {
                AddOnValueID = dr.InferInt("AddOnValueID"),
                AddOnId = dr.InferInt("AddOnID"),
                Name = dr.InferString("AddOnValue_Name"),
                Description = dr.InferString("AddOnValue_Description"),
                DisplayOrder = dr.InferInt("AddOnValue_DisplayOrder"),
                RetailPrice = dr.InferDecimal("RetailPrice"),
                SalePrice = dr.InferDecimal("SalePrice", null),
                WholesalePrice = dr.InferDecimal("WholesalePrice", null),
                IsDefault = dr.InferBool("IsDefault"),
                QuantityOnHand = dr.InferInt("QuantityOnHand"),
                SKU = dr.InferString("SKU"),
                Weight = dr.InferDecimal("Weight"),
                Height = dr.InferDecimal("Height"),
                Length = dr.InferDecimal("Length"),
                ShippingRuleTypeID = dr.InferInt("ShippingRuleTypeID"),
                FreeShippingInd = dr.InferBool("FreeShippingInd"),
                SupplierID = dr.InferInt("SupplierID"),
                RecurringBillingInd = dr.InferBool("RecurringBillingInd"),
                RecurringBillingInstallmentInd = dr.InferBool("RecurringBillingInstallmentInd"),
                RecurringBillingPeriod = dr.InferString("RecurringBillingPeriod"),
                RecurringBillingFrequency = dr.InferString("RecurringBillingFrequency"),
                RecurringBillingTotalCycles = dr.InferInt("RecurringBillingTotalCycles"),
                RecurringBillingInitialAmount = dr.InferDecimal("RecurringBillingInitialAmount"),
                TaxClassID = dr.InferInt("TaxClassID")
            };

            return addonValue;
        }

        public static ZNodeDigitalAsset CreateZnodeDigitalAsset(SqlDataReader dr)
        {
            var digitalAsset = new ZNodeDigitalAsset
            {
                DigitalAssetID = dr.InferInt("DigitalAssetID"),
                DigitalAsset = dr.InferString("DigitalAsset"),
                OrderLineItemID = dr.InferInt("OrderLineItemID", null)
            };

            return digitalAsset;
        }

        public static ZNodeSKU CreateZnodeSKU(SqlDataReader dr)
        {
            var sku = new ZNodeSKU
            {
                SKUID = dr.InferInt("SKUID"),
                ProductID = dr.InferInt("ProductID"),
                SupplierID = dr.InferInt("SupplierID"),
                Note = dr.InferString("Note"),
                WeightAdditional = dr.InferDecimal("WeightAdditional"),
                SKUPicturePath = dr.InferString("SKUPicturePath"),
                ImageAltTag = dr.InferString("ImageAltTag"),
                DisplayOrder = dr.InferInt("DisplayOrder"),
                RetailPriceOverride = dr.InferDecimal("RetailPriceOverride", null),
                SalePriceOverride = dr.InferDecimal("SalePriceOverride", null),
                WholesalePriceOverride = dr.InferDecimal("WholesalePriceOverride", null),
                RecurringBillingPeriod = dr.InferString("RecurringBillingPeriod"),
                RecurringBillingFrequency = dr.InferString("RecurringBillingFrequency"),
                RecurringBillingTotalCycles = dr.InferInt("RecurringBillingTotalCycles"),
                RecurringBillingInitialAmount = dr.InferDecimal("RecurringBillingInitialAmount"),
                IsActive = dr.InferBool("ActiveInd"),
                Custom1 = dr.InferString("Custom1"),
                Custom2 = dr.InferString("Custom2"),
                Custom3 = dr.InferString("Custom3"),
                ExternalID = dr.InferString("ExternalID"),
                SKU = dr.InferString("SKU"),
                QuantityOnHand = dr.InferInt("QuantityOnHand"),
                ReorderLevel = dr.InferInt("ReOrderLevel"),
                AttributesDescription = dr.InferString("AttributesDescription"),
                NegotiatedPrice = dr.InferDecimal("NegotiatedPrice", null),
            };

            return sku;
        }

        public static ZNodeCategory CreateZnodeCategory(SqlDataReader dr)
        {
            var category = new ZNodeCategory
            {
                CategoryID = dr.InferInt("CategoryID"),
                Name = dr.InferString("Name"),
                Title = dr.InferString("Title"),
                Description = dr.InferString("Description"),
                ShortDescription = dr.InferString("ShortDescription"),
                ImageFile = dr.InferString("ImageFile"),
                ImageAltTag = dr.InferString("ImageAltTag"),
                VisibleInd = dr.InferBool("VisibleInd"),
                SubCategoryGridVisibleInd = dr.InferBool("SubCategoryGridVisibleInd"),
                SEOTitle = dr.InferString("SEOTitle"),
                SEOKeywords = dr.InferString("SEOKeywords"),
                SEODescription = dr.InferString("SEODescription"),
                AlternateDescription = dr.InferString("AlternateDescription"),
                DisplayOrder = dr.InferInt("DisplayOrder"),
                Custom1 = dr.InferString("Custom1"),
                Custom2 = dr.InferString("Custom2"),
                Custom3 = dr.InferString("Custom3"),
                SEOURL = dr.InferString("SEOURL"),
                CategoryPath = dr.InferString("CategoryPath"),
                Theme = dr.InferString("Theme"),
                MasterPage = dr.InferString("MasterPage"),
                Css = dr.InferString("CSS")
            };

            return category;
        }

        public static ZNodeManufacturer CreateZnodeManufacturer(SqlDataReader dr)
        {
            var manufacturer = new ZNodeManufacturer
            {
                ManufacturerID = dr.InferInt("ManufacturerID"),
                Name = dr.InferString("Name"),
                Description = dr.InferString("Description"),
                ActiveInd = dr.InferBool("ActiveInd")
            };

            return manufacturer;
        }

        public static ZNodeAttribute CreateZnodeAttribute(SqlDataReader dr)
        {
            return new ZNodeAttribute
            {
                AttributeId = dr.InferInt("AttributeId"),
                AttributeTypeId = dr.InferInt("AttributeTypeId"),
                Name = dr.InferString("ProductAttribute_Name"),
                DisplayOrder = dr.InferInt("DisplayOrder"),
                IsActive = dr.InferBool("ActiveInd"),

            };
        }

        public static ZNodeSkuAttribute CreateZnodeSkuAttribute(SqlDataReader dr)
        {
            return new ZNodeSkuAttribute
            {
                SKUID = dr.InferInt("SKUID"),
            };
        }

        #endregion

        #region Non-XML based Methods

        /// <summary>
        /// Get Product Details in table instead of XML.
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="portalId"></param>
        /// <param name="localeId"></param>
        /// <param name="profileId"></param>
        /// <param name="externalAccountNo"></param>
        /// <returns></returns>
        public static ZNodeProduct GetProductDetailsByProductId(int productId, int portalId, int localeId, int profileId, string externalAccountNo, bool isAdmin)
		{
			// Create instance of connection  Object   
			using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
			{
				SqlDataReader reader = null;
				
				try
				{
					var command = new SqlCommand("ZNode_GetProductByProductID_Details", connection);

					// Mark the command as store procedure
					command.CommandType = CommandType.StoredProcedure;
					command.Parameters.AddWithValue("@ProductID", productId);

                    //commented below code block for MVC admin while creating order we dont get profileId,portalId,localeId,externalAccountNo

                    //command.Parameters.AddWithValue("@profileId", profileId);

                    //if (portalId > 0)
                    //    command.Parameters.AddWithValue("@PortalID", portalId);

                    //if (localeId > 0)
                    //    command.Parameters.AddWithValue("@LocaleId", localeId);

                    //if (!string.IsNullOrEmpty(externalAccountNo))
						
                    //    command.Parameters.AddWithValue("@externalAccountNo", externalAccountNo);

                    command.Parameters.AddWithValue("@isAdmin", isAdmin);
					
					// Execute the command
					connection.Open();

                    var product = new ZNodeProduct();

					reader = command.ExecuteReader();

					// Read Product Info
					while (reader.Read())
					{
						product = ZnodeCatalogFactory.CreateZnodeProduct(reader);
					}

					if (product.ProductID == 0) return null;

					// Product Category Info
					reader.NextResult();
					while (reader.Read())
					{
						product.ZNodeProductCategoryCollection.Add(ZnodeCatalogFactory.CreateZnodeProductCategory(reader));
					}

					// Read the product highlights
					reader.NextResult();
					while (reader.Read())
					{
						product.ZNodeHighlightCollection.Add(ZnodeCatalogFactory.CreateZnodeHighlight(reader));
					}

					// Read the product Review
					reader.NextResult();
					while (reader.Read())
					{
						product.ZNodeReviewCollection.Add(ZnodeCatalogFactory.CreateZnodeReview(reader));
					}

					// Read the product CrossSellItem
					reader.NextResult();
					while (reader.Read())
					{
						product.ZNodeCrossSellItemCollection.Add(ZnodeCatalogFactory.CreateZnodeCrossSellItem(reader));
					}

					// Read the product AttributeType
					reader.NextResult();
					while (reader.Read())
					{
						var attributeType =
							product.ZNodeAttributeTypeCollection.Cast<ZNodeAttributeType>()
							       .FirstOrDefault(x => x.AttributeTypeId == reader.InferInt("AttributeTypeId"));
						if (attributeType == null)
						{
							attributeType = ZnodeCatalogFactory.CreateZnodeAttributeType(reader);
							product.ZNodeAttributeTypeCollection.Add(attributeType);
						}

						var attribute =
							attributeType.ZNodeAttributeCollection.Cast<ZNodeAttribute>()
							             .FirstOrDefault(x => x.AttributeId == reader.InferInt("AttributeId"));

						if (attribute == null)
						{
							attribute = ZnodeCatalogFactory.CreateZnodeAttribute(reader);
							attributeType.ZNodeAttributeCollection.Add(attribute);
						}

						attribute.SkuAttributes.Add(ZnodeCatalogFactory.CreateZnodeSkuAttribute(reader));
					}

					// Read the product AddOn
					reader.NextResult();
					while (reader.Read())
					{
						var addOn =
							product.ZNodeAddOnCollection.Cast<ZNodeAddOn>().FirstOrDefault(x => x.AddOnID == reader.InferInt("AddOnID"));
						if (addOn == null)
						{
							addOn = ZnodeCatalogFactory.CreateZnodeAddOn(reader);
							product.ZNodeAddOnCollection.Add(addOn);
						}

						addOn.AddOnValueCollection.Add(ZnodeCatalogFactory.CreateZnodeAddOnValue(reader));						
					}

					// Read the product tier
					reader.NextResult();
					while (reader.Read())
					{
						product.ZNodeTieredPriceCollection.Add(ZnodeCatalogFactory.CreateZnodeProductTier(reader));
					}

					// Read the product default sku
					reader.NextResult();
					if (reader.Read())
					{
						product.SelectedSKU = ZnodeCatalogFactory.CreateZnodeSKU(reader);
					}

					// Read BundleProduct
					reader.NextResult();
					while (reader.Read())
					{
                        var bundleProduct = GetProductDetailsByProductId(reader.InferInt("ProductID"), 0, 0, profileId, string.Empty, isAdmin);
						if (bundleProduct != null)
							product.ZNodeBundleProductCollection.Add(new ZNodeBundleProductEntity(bundleProduct));
						else
						{
						    return null;
						}
					}

                    // Read Digital Assets
                    reader.NextResult();
                    while (reader.Read())
                    {
                        product.ZNodeDigitalAssetCollection.Add(ZnodeCatalogFactory.CreateZnodeDigitalAsset(reader));
                    }

					return product;
				}
				catch (SqlException ex)
				{
					ZNodeLoggingBase.LogMessage(ex.ToString());
				}
				finally
				{
					if (reader != null)
					{
						// Close the reader
						reader.Close();
					}

					if (connection != null)
					{
						// Close the connection
						connection.Close();
					}
				}

				return null;
			}
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="productIds"></param>
        /// <param name="externalAccountNo"></param>
        /// <returns></returns>
        public static ZNodeProductList GetProductDetailsByProductIds(string productIds, string externalAccountNo, int profileId)
        {
            // Create instance of connection  Object   
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlDataReader reader = null;

                try
                {

                    var command = new SqlCommand("ZNode_GetProductsByIds_Details", connection) { CommandType = CommandType.StoredProcedure };

                    // Mark the command as store procedure
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@ProductIds", productIds);
                    command.Parameters.AddWithValue("@ProfileId", profileId);
                    command.Parameters.AddWithValue("@externalAccountNo", externalAccountNo);
                    // Execute the command
                    connection.Open();

                    reader = command.ExecuteReader();

                    var productList = new ZNodeProductList();

                    // Read Product Info
                    while (reader.Read())
                    {
                        productList.ZNodeProductCollection.Add(ZnodeCatalogFactory.CreateZnodeProduct(reader));
                    }


                    // Read the product tier
                    reader.NextResult();
                    while (reader.Read())
                    {
                        var product =
                            productList.ZNodeProductCollection.Cast<ZNodeProduct>()
                                       .FirstOrDefault(x => x.ProductID == reader.InferInt("ProductID"));
                        if (product != null) product.ZNodeTieredPriceCollection.Add(ZnodeCatalogFactory.CreateZnodeProductTier(reader));
                    }

                    // Read the product default sku
                    reader.NextResult();
                    while (reader.Read())
                    {
                        var product =
                            productList.ZNodeProductCollection.Cast<ZNodeProduct>()
                                       .FirstOrDefault(x => x.ProductID == reader.InferInt("ProductID"));
                        if (product != null &&
                            (product.SelectedSKU == null || (product.SelectedSKU != null && product.SelectedSKU.ProductID == 0)))
                            product.SelectedSKU = ZnodeCatalogFactory.CreateZnodeSKU(reader);
                    }

                    productList.ApplyPromotion();

                    return productList;
                }
                catch (SqlException ex)
                {
                    ZNodeLoggingBase.LogMessage(ex.ToString());
                }
                finally
                {
                    if (reader != null)
                    {
                        // Close the reader
                        reader.Close();
                    }

                    if (connection != null)
                    {
                        // Close the connection
                        connection.Close();
                    }
                }

                return null;
            }
        }

        public static ZNodeProductList GetHomePageSpecials(int portalId, int localeId, int displayItem, int profileId, string externalAccountNo = "")
        {
            // Create instance of connection  Object   
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlDataReader reader = null;

                try
                {

                    var command = new SqlCommand("ZNode_GetHomePageSpecials_Details", connection);

                    // Mark the command as store procedure
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@PortalID", portalId);
                    command.Parameters.AddWithValue("@LocaleId", localeId);
                    command.Parameters.AddWithValue("@DisplayItem", displayItem);
                    command.Parameters.AddWithValue("@ProfileId", profileId);
                    command.Parameters.AddWithValue("@externalAccountNo", externalAccountNo);
                    // Execute the command
                    connection.Open();

                    reader = command.ExecuteReader();

                    var productList = new ZNodeProductList();

                    // Read Product Info
                    while (reader.Read())
                    {
                        productList.ZNodeProductCollection.Add(ZnodeCatalogFactory.CreateZnodeProduct(reader));
                    }

                    // Read the product default sku
                    reader.NextResult();
                    while (reader.Read())
                    {
                        var product =
                            productList.ZNodeProductCollection.Cast<ZNodeProduct>()
                                       .FirstOrDefault(x => x.ProductID == reader.InferInt("ProductID"));
                        if (product != null &&
                            (product.SelectedSKU == null || (product.SelectedSKU != null && product.SelectedSKU.ProductID == 0)))
                            product.SelectedSKU = ZnodeCatalogFactory.CreateZnodeSKU(reader);
                    }

                    productList.ApplyPromotion();

                    return productList;
                }
                catch (SqlException ex)
                {
                    ZNodeLoggingBase.LogMessage(ex.ToString());
                }
                finally
                {
                    if (reader != null)
                    {
                        // Close the reader
                        reader.Close();
                    }

                    if (connection != null)
                    {
                        // Close the connection
                        connection.Close();
                    }
                }

                return null;
            }
        }

        public static ZNodeSKU GetDefaultSku(int productId, string externalAccountNo = "")
        {
            // Create instance of connection  Object   
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlDataReader reader = null;

                try
                {

                    var command = new SqlCommand("ZNODE_GetDefaultSKUByProduct_Details", connection);

                    // Mark the command as store procedure
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@ProductID", productId);
                    command.Parameters.AddWithValue("@externalAccountNo", externalAccountNo);
                    // Execute the command
                    connection.Open();

                    reader = command.ExecuteReader();

                    var sku = new ZNodeSKU();

                    // Read Product Info
                    if (reader.Read())
                    {
                        sku = ZnodeCatalogFactory.CreateZnodeSKU(reader);
                    }

                    return sku;
                }
                catch (SqlException ex)
                {
                    ZNodeLoggingBase.LogMessage(ex.ToString());
                }
                finally
                {
                    if (reader != null)
                    {
                        // Close the reader
                        reader.Close();
                    }

                    if (connection != null)
                    {
                        // Close the connection
                        connection.Close();
                    }
                }

                return null;
            }
        }

        public static ZNodeSKU GetSkuByAttributes(int productId, string attributes, string externalAccountNo = "")
        {
            // Create instance of connection  Object   
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlDataReader reader = null;

                try
                {

                    var command = new SqlCommand("ZNODE_GetSKUByAttributes_Details", connection);

                    // Mark the command as store procedure
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@ProductID", productId);
                    command.Parameters.AddWithValue("@Attributes", attributes);
                    command.Parameters.AddWithValue("@externalAccountNo", externalAccountNo);
                    // Execute the command
                    connection.Open();

                    reader = command.ExecuteReader();

                    var sku = new ZNodeSKU();

                    // Read Product Info
                    if (reader.Read())
                    {
                        sku = ZnodeCatalogFactory.CreateZnodeSKU(reader);
                    }

                    return sku;
                }
                catch (SqlException ex)
                {
                    ZNodeLoggingBase.LogMessage(ex.ToString());
                }
                finally
                {
                    if (reader != null)
                    {
                        // Close the reader
                        reader.Close();
                    }

                    if (connection != null)
                    {
                        // Close the connection
                        connection.Close();
                    }
                }

                return null;
            }
        }

        public static ZNodeSKU GetBySku(string sku, int profileID, string externalAccountNo = "")
        {
            // Create instance of connection  Object   
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlDataReader reader = null;

                try
                {

                    // Mark the command as store procedure
                    var command = new SqlCommand("ZNODE_GetSKUBySKU_Details", connection) { CommandType = CommandType.StoredProcedure };
                    command.Parameters.AddWithValue("@SKU", sku);
                    //command.Parameters.AddWithValue("@ProfileID", profileID);
                    command.Parameters.AddWithValue("@ExternalAccountNo", externalAccountNo);
                    connection.Open();
                    // Execute the command
                    reader = command.ExecuteReader();

                    var skuObj = new ZNodeSKU();

                    // Read Product Info
                    if (reader.Read())
                    {
                        skuObj = ZnodeCatalogFactory.CreateZnodeSKU(reader);
                    }

                    return skuObj;
                }
                catch (SqlException ex)
                {
                    ZNodeLoggingBase.LogMessage(ex.ToString());
                }
                finally
                {
                    if (reader != null)
                    {
                        // Close the reader
                        reader.Close();
                    }

                    if (connection.State != ConnectionState.Closed)
                    {
                        // Close the connection
                        connection.Close();
                    }
                }

                return null;
            }
        }

        public static ZNodeAddOnList GetAddOnByValues(int productId, string selectedValues)
        {
            // Create instance of connection  Object   
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlDataReader reader = null;

                try
                {

                    var command = new SqlCommand("ZNode_GetAddOnsByAddOnValues_Details", connection);

                    // Mark the command as store procedure
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@ProductID", productId);
                    command.Parameters.AddWithValue("@AddOnValues", selectedValues);

                    // Execute the command
                    connection.Open();

                    reader = command.ExecuteReader();

                    var addOnList = new ZNodeAddOnList();

                    while (reader.Read())
                    {
                        var addOn =
                            addOnList.ZNodeAddOnCollection.Cast<ZNodeAddOn>().FirstOrDefault(x => x.AddOnID == reader.InferInt("AddOnID"));
                        if (addOn == null)
                        {
                            addOn = ZnodeCatalogFactory.CreateZnodeAddOn(reader);
                            addOnList.ZNodeAddOnCollection.Add(addOn);
                        }

                        addOn.AddOnValueCollection.Add(ZnodeCatalogFactory.CreateZnodeAddOnValue(reader));
                    }

                    return addOnList;
                }
                catch (SqlException ex)
                {
                    ZNodeLoggingBase.LogMessage(ex.ToString());
                }
                finally
                {
                    if (reader != null)
                    {
                        // Close the reader
                        reader.Close();
                    }

                    if (connection != null)
                    {
                        // Close the connection
                        connection.Close();
                    }
                }

                return null;
            }
        }

        public static ZNodeCategory GetCategoryById(int categoryId, int portalId, int localeId)
        {
            // Create instance of connection and Command Object
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlDataReader reader = null;

                try
                {
                    SqlCommand command = new SqlCommand("ZNODE_GetCategoryById_Details", connection);

                    // Mark the command as store procedure
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.AddWithValue("@PortalID", portalId);
                    command.Parameters.AddWithValue("@LocaleId", localeId);
                    command.Parameters.AddWithValue("@categoryId", categoryId);

                    // Execute the command
                    connection.Open();

                    reader = command.ExecuteReader();

                    ZNodeCategory category = null;

                    while (reader.Read())
                    {
                        category = ZnodeCatalogFactory.CreateZnodeCategory(reader);
                    }

                    if (category != null)
                    {
                        reader.NextResult();
                        while (reader.Read())
                        {
                            category.ZNodeCategoryCollection.Add(ZnodeCatalogFactory.CreateZnodeCategory(reader));
                        }
                    }

                    return category;
                }
                finally
                {
                    if (reader != null)
                    {
                        // Close the reader
                        reader.Close();
                    }

                    if (connection != null)
                    {
                        // Close the connection
                        connection.Close();
                    }
                }
            }
        }

        #endregion
    }
}