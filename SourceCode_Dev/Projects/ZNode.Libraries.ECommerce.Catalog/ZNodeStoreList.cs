using System;
using System.Xml.Serialization;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Catalog
{
    /// <summary>
    /// Represents the collection of ZNodeStore objects.
    /// </summary>
    [Serializable()]
    [XmlRoot()]
    public class ZNodeStoreList : ZNodeBusinessBase
    {
        #region Protected Member variables
        private ZNodeGenericCollection<ZNodeStore> _StoreCollection = new ZNodeGenericCollection<ZNodeStore>();
        #endregion

        #region Public properties
        /// <summary>
        /// Gets or sets the ZNode store collection object
        /// Collection of Znodestore objects
        /// </summary>
        [XmlElement("ZNodeStore")]
        public ZNodeGenericCollection<ZNodeStore> StoreCollection
        {
            get { return this._StoreCollection; }

            set { this._StoreCollection = value; }
        }
        #endregion

        #region Public Static Methods
        /// <summary>
        /// Search storeList based on the search fields(Zipcode and radius)
        /// </summary>
        /// <param name="zipcode">Zip code to search</param>
        /// <param name="radius">Radion to search within.</param>
        /// <param name="city">City name to search</param>
        /// <param name="stateAbbr">State code to search.</param>
        /// <param name="areaCode">Area code to search.</param>
        /// <param name="portalId">Portal Id to search.</param>
        /// <returns>Returns the store locator search result in XML format.</returns>
        public static ZNodeStoreList SearchByZipCodeAndRadius(string zipcode, int radius, string city, string stateAbbr, string areaCode, int portalId)
        {
            ZNode.Libraries.DataAccess.Custom.StoreLocatorHelper storeLocatorHelper = new ZNode.Libraries.DataAccess.Custom.StoreLocatorHelper();
            string xmlOut = storeLocatorHelper.GetStoresByZipcode(zipcode, radius, city, stateAbbr, areaCode, portalId);

            ZNodeStoreList storeList = new ZNodeStoreList();

            // Serialize the object
            if (xmlOut.Trim().Length > 0)
            {
                ZNodeSerializer serializer = new ZNodeSerializer();
                storeList = (ZNodeStoreList)serializer.GetContentFromString(xmlOut, typeof(ZNodeStoreList));
            }

            return storeList;
        }

        /// <summary>
        /// Returns the ZipCode Count
        /// </summary>
        /// <returns>Gets the zip code count.</returns>
        public int GetZipCodeCount()
        {
            ZNode.Libraries.DataAccess.Custom.ProductHelper productHelper = new ZNode.Libraries.DataAccess.Custom.ProductHelper();
            return productHelper.GetZipCodeCount();
        }
        #endregion
    }
}
