using System;
using System.Xml.Serialization;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Catalog
{
    /// <summary>
    /// Represents the properties and methods of a product AddOn.
    /// </summary>
    [Serializable()]
    [XmlRoot()]
    public class ZNodeAddOnList : ZNode.Libraries.ECommerce.Entities.ZNodeAddOnListEntity
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the ZNodeAddOnList class
        /// </summary>
        public ZNodeAddOnList()
        {
        }

        /// <summary>
        /// Initializes a new instance of the ZNodeAddOnList class
        /// </summary>
        /// <param name="addOnList">ZNodeAddOnList object</param>
        public ZNodeAddOnList(ZNodeAddOnList addOnList)
        {
            this.AddOnCollection= addOnList.AddOnCollection;
            this.SelectedAddOnValueIds = addOnList.SelectedAddOnValueIds;
        }
        #endregion

        #region Public properties
        /// <summary>
        /// Gets or sets the products add on collection
        /// </summary>
        [XmlIgnore()]
        public ZNodeGenericCollection<ZNodeAddOn> ZNodeAddOnCollection
        {
            get
            {
                ZNodeGenericCollection<ZNodeAddOn> AddOnCollectionValues = new ZNodeGenericCollection<ZNodeAddOn>();

                foreach (ZNodeAddOnEntity addOn in this.AddOnCollection)
                {
                    AddOnCollectionValues.Add(new ZNodeAddOn(addOn));
                }

                return AddOnCollectionValues;
            }

            set
            {
                foreach (ZNodeAddOn addOn in value)
                {
                    this.AddOnCollection.Add((ZNodeAddOnEntity)addOn);
                }
            }
        }
        #endregion        

        #region Public Methods
        /// <summary>
        /// Returns a AddOn based on the productID and by the given addon value ids.
        /// </summary>
        /// <param name="productId">Product Id to create the AddOn list.</param>
        /// <param name="addOnvalues">AddOn value id's to create the addon list.</param>
        /// <returns>Returns the ZNodeAddOnList object.</returns>
        public static ZNodeAddOnList CreateByProductAndAddOns(int productId, string addOnvalues, int savedCartLineItemId = 0)
        {
            ZNode.Libraries.DataAccess.Custom.ProductHelper productHelper = new ZNode.Libraries.DataAccess.Custom.ProductHelper();
            string xmlOut = productHelper.GetAddOnByValues(productId, addOnvalues, savedCartLineItemId);

            ZNodeAddOnList addOnList = new ZNodeAddOnList();
            
            // Serialize the object
            if (xmlOut.Trim().Length > 0)
            {
                ZNodeSerializer ser = new ZNodeSerializer();
                addOnList = (ZNodeAddOnList)ser.GetContentFromString(xmlOut, typeof(ZNodeAddOnList));
            }

            return addOnList;
        }
        #endregion
    }
}
