using System;
using System.Xml.Serialization;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Catalog
{
    /// <summary>
    /// Represents the properties and methods of a ZNodeAttributeType
    /// </summary>
    [Serializable()]
    [XmlRoot()]
    public class ZNodeAttributeType : ZNodeBusinessBase
    {
        #region Private Member Variables
        private int _AttributeTypeId;
        private int _PortalId;
        private string _Name = string.Empty;
        private string _Description = string.Empty;
        private int _DisplayOrder;
        private bool _IsPrivate;
        private int _SelectedAttributeId;
        private ZNodeGenericCollection<ZNodeAttribute> _AttributeCollection = new ZNodeGenericCollection<ZNodeAttribute>();
        #endregion

        #region Constructor
        /// <summary>
        /// Initializes a new instance of the ZNodeAttributeType class
        /// </summary>
        public ZNodeAttributeType()
        {
        }
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the attribute collection
        /// </summary>
        [XmlElement("ZNodeAttribute")]
        public ZNodeGenericCollection<ZNodeAttribute> ZNodeAttributeCollection
        {
            get
            {
                return this._AttributeCollection;
            }
            
            set
            {
                this._AttributeCollection = value;
            }
        }
        
        /// <summary>
        /// Gets or sets the product AttributeTypeId
        /// </summary>
        [XmlElement()]
        public int AttributeTypeId
        {
            get { return this._AttributeTypeId; }
            set { this._AttributeTypeId = value; }
        }

        /// <summary>
        /// Gets or sets the site portalId
        /// </summary>
        [XmlElement()]
        public int PortalId
        {
            get { return this._PortalId; }
            set { this._PortalId = value; }
        }

        /// <summary>
        /// Gets or sets the AttributeType Name
        /// </summary>
        [XmlElement()]
        public string Name
        {
            get { return this._Name; }
            set { this._Name = value; }
        }

        /// <summary>
        /// Gets or sets the attribute type description
        /// </summary>
        [XmlElement()]
        public string Description
        {
            get { return this._Description; }
            set { this._Description = value; }
        }

        /// <summary>
        /// Gets or sets the attribute type display order.
        /// </summary>
        [XmlElement()]
        public int DisplayOrder
        {
            get { return this._DisplayOrder; }
            set { this._DisplayOrder = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the attribute type is private or not.
        /// </summary>
        [XmlElement()]
        public bool IsPrivate
        {
            get { return this._IsPrivate; }
            set { this._IsPrivate = value; }
        }

        /// <summary>
        /// Gets or sets the attribute selected by the user. 
        /// </summary>
        [XmlIgnore()]
        public int SelectedAttributeId
        {
            get { return this._SelectedAttributeId; }
            set { this._SelectedAttributeId = value; }
        }

        #endregion
    }
}
