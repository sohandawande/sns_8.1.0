using System.IO;
using System.Web;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Catalog
{
    /// <summary>
    /// Represents the ZNodeContentManager class.
    /// </summary>
    public class ZNodeContentManager : ZNodeBusinessBase
    {
        /// <summary>
        /// Gets the locale code.
        /// </summary>
        public static string LocalCode
        {
            get
            {
                return ZNodeCatalogManager.LocaleCode;
            }
        }

        /// <summary>
        /// Returns the HTML content of a page
        /// </summary>
        /// <param name="pageName">Page name to get the content page</param>
        /// <param name="portalId">Portal Id to get the content page.</param>
        /// <returns>Returns the content page text.</returns>
        public static string GetPageHTMLByName(string pageName, int portalId)
        {
            try
            {
                string filePath = ZNodeConfigManager.EnvironmentConfig.ContentPath + pageName + "_" + portalId + "_" + ZNodeContentManager.LocalCode + ".htm";

                string htmlText = ZNodeStorageManager.ReadTextStorage(filePath);

                return htmlText;
            }
            catch
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Get the content page by page name, portal Id and locale Id.
        /// </summary>
        /// <param name="pageName">Page name to get the content page</param>
        /// <param name="portalId">Portal Id of the content page.</param>
        /// <param name="localeId">Locale Id of the content page.</param>
        /// <returns>Returns the ContentPage object.</returns>      
        public static ContentPage GetPageByName(string pageName, int portalId, int localeId)
        {
            ContentPageService contentPageService = new ContentPageService();
			ContentPage contentPage = contentPageService.DeepLoadByNamePortalIDLocaleId(pageName, portalId, localeId, true, DeepLoadType.IncludeChildren, typeof(Theme), typeof(MasterPage), typeof(CSS));

            if (contentPage != null)
            {
                return contentPage;
            }

            return null;
        }

        /// <summary>
        /// Get the content page by Id.
        /// </summary>
        /// <param name="contentPageId">Content page Id to get the content page object.</param>
        /// <returns>Returns the ContentPage object.</returns>      
        public static ContentPage GetPageById(int contentPageId)
        {
            ContentPageService contentPageService = new ContentPageService();
            ContentPage contentPage = contentPageService.GetByContentPageID(contentPageId);

            if (contentPage != null)
            {
                return contentPage;
            }

            return null;
        }
    }
}
