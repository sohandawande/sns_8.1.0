using System;
using System.Xml.Serialization;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.ECommerce.Entities;

namespace ZNode.Libraries.ECommerce.Catalog
{
    /// <summary>
    /// Represents a product in the catalog
    /// </summary>    
    [Serializable()]
    public class ZNodeProduct : ZNodeProductBase
    {
        #region Private Variables
        private string _MasterPage = string.Empty;        
        private string _FeaturesDesc = string.Empty;
        private bool _Hide;
        private string _Keywords = string.Empty;
        private string _AdditionalInfoLink = string.Empty;
        private string _AdditionalInfoLinkLabel = string.Empty;
        private string _Specifications = string.Empty;
        private string _AdditionalInformation = string.Empty;
        private string _InStockMsg = string.Empty;
        private string _OutOfStockMsg = string.Empty;
        private bool _Franchisable = false;
        #endregion

        #region Private MemberObjects
        private ZNodeGenericCollection<ZNodeReview> _ReviewCollection = new ZNodeGenericCollection<ZNodeReview>();
        private ZNodeGenericCollection<ZNodeHighlight> _HighlightCollection = new ZNodeGenericCollection<ZNodeHighlight>();
        private ZNodeGenericCollection<ZNodeAttributeType> _AttributeTypeCollection = new ZNodeGenericCollection<ZNodeAttributeType>();
        private ZNodeGenericCollection<ZNodeAddOn> _AddOnCollection = new ZNodeGenericCollection<ZNodeAddOn>();
        private ZNodeGenericCollection<ZnodeProductCategory> _ProductCategoryCollection = new ZNodeGenericCollection<ZnodeProductCategory>();
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the ZNodeProduct class
        /// </summary>
        public ZNodeProduct()
        {
        }
        #endregion

        #region Public Properties        
       
        /// <summary>
        /// Gets or sets the MaxQuantity       
        /// </summary>
        public override int MaxQty
        {
            get
            {
                if (CheckSKUProfile() != null || this._skuProfile != null)
                {
                    return this._skuProfile.ProfileLimit;
                }

                return _MaxQty;
            }

            set
            {
                _MaxQty = value;
            }
        }


        /// <summary>
        /// Gets or sets the category association related with this product
        /// </summary>
        [XmlElement("ZNodeProductCategory")]
        public ZNodeGenericCollection<ZnodeProductCategory> ZNodeProductCategoryCollection
        {
            get
            {
                return this._ProductCategoryCollection;
            }
            
            set
            {
                this._ProductCategoryCollection = value;
            }
        }

        /// <summary>
        /// Gets or sets the customer reviews related with this product
        /// </summary>
        [XmlElement("ZNodeReview")]
        public ZNodeGenericCollection<ZNodeReview> ZNodeReviewCollection
        {
            get
            {
                return this._ReviewCollection;
            }
          
            set
            {
                this._ReviewCollection = value;
            }
        }

        /// <summary>
        /// Gets or sets the product highlights related with this product
        /// </summary>
        [XmlElement("ZNodeHighlight")]
        public ZNodeGenericCollection<ZNodeHighlight> ZNodeHighlightCollection
        {
            get
            {
                return this._HighlightCollection;
            }
          
            set
            {
                this._HighlightCollection = value;
            }
        }

        /// <summary>
        /// Gets or sets the product addons for this product
        /// </summary>
        [XmlElement("ZNodeAddOn")]
        public ZNodeGenericCollection<ZNodeAddOn> ZNodeAddOnCollection
        {
            get
            {
                return this._AddOnCollection;
            }
         
            set
            {
                this._AddOnCollection = value;
            }
        }

        /// <summary>
        /// Gets or sets the product attributetypes for this product
        /// </summary>
        [XmlElement("ZNodeAttributeType")]
        public ZNodeGenericCollection<ZNodeAttributeType> ZNodeAttributeTypeCollection
        {
            get
            {
                return this._AttributeTypeCollection;
            }
            
            set
            {
                this._AttributeTypeCollection = value;
            }
        }        

        /// <summary>
        /// Gets or sets the product features
        /// </summary>
        [XmlElement()]
        public string FeaturesDesc
        {
            get
            {
                return this._FeaturesDesc;
            }
            
            set
            {
                this._FeaturesDesc = value;
            }
        }

        /// <summary>
        /// Gets or sets the additional information link for this product
        /// </summary>
        [XmlElement()]
        public string AdditionalInfoLink
        {
            get
            {
                return this._AdditionalInfoLink;
            }
            
            set
            {
                this._AdditionalInfoLink = value;
            }
        }

        /// <summary>
        /// Gets or sets the additional link labale text for this product
        /// </summary>
        [XmlElement()]
        public string AdditionalInfoLinkLabel
        {
            get
            {
                return this._AdditionalInfoLinkLabel;
            }
            
            set
            {
                this._AdditionalInfoLinkLabel = value;
            }
        }        
        
        /// <summary>
        /// Gets or sets the Specification
        /// </summary>
        [XmlElement()]
        public string Specifications
        {
            get
            {
                return this._Specifications;
            }
            
            set
            {
                this._Specifications = value;
            }
        }
        
        /// <summary>
        /// Gets or sets the Additional Information
        /// </summary>
        [XmlElement()]
        public string AdditionalInformation
        {
            get
            {
                return this._AdditionalInformation;
            }
            
            set
            {
                this._AdditionalInformation = value;
            }
        }

        /// <summary>
        /// Gets or sets the InStock Message
        /// </summary>
        [XmlElement()]
        public string InStockMsg
        {
            get
            {
                return this._InStockMsg;
            }
            
            set
            {
                this._InStockMsg = value;
            }
        }

        /// <summary>
        /// Gets or sets the OutOfStock Message
        /// </summary>
        [XmlElement()]
        public string OutOfStockMsg
        {
            get
            {
                return this._OutOfStockMsg;
            }

            set
            {
                this._OutOfStockMsg = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether ti show or hide the option.
        /// </summary>
        [XmlElement()]
        public bool Hide
        {
            get
            {
                return this._Hide;
            }

            set
            {
                this._Hide = value;
            }
        }

        /// <summary>
        /// Gets or sets the product features
        /// </summary>
        [XmlElement()]
        public bool Franchisable
        {
            get
            {
                return this._Franchisable;
            }

            set
            {
                this._Franchisable = value;
            }
        }
        #endregion        

        /// <summary>
        /// Get the next random number for gift card.
        /// </summary>
        /// <returns>Returns the unique gift card number.</returns>
        public string GetNextGiftCardNumber()
        {
            System.Security.Cryptography.RandomNumberGenerator numberGenerator = System.Security.Cryptography.RandomNumberGenerator.Create();
            int numberLength = 10;
            char[] chars = new char[numberLength];

            string validChars = "ABCEDFGHIJKLMNOPQRSTUVWXYZ1234567890";

            for (int i = 0; i < numberLength; i++)
            {
                byte[] bytes = new byte[1];
                numberGenerator.GetBytes(bytes);
                Random rnd = new Random(bytes[0]);
                chars[i] = validChars[rnd.Next(0, 35)];
            }

            return new string(chars);
        }

        /// <summary>
        /// Get the expiration date string 
        /// </summary>
        /// <param name="expirationPeriod">Gift card expiration period.</param>
        /// <param name="expirationFrequency">Gift card expiration frequency.</param>
        /// <returns>Returns the product expiration date (MM/dd/yyyy) string.</returns>
        public DateTime GetExpirationDate(int expirationPeriod, int expirationFrequency)
        {
            DateTime expirationDate = DateTime.Today;            
            switch (expirationFrequency)
            {
                case 7:
                    // Week
                    expirationDate = DateTime.Today.Date.AddDays(expirationPeriod * 7);
                    break;

                case 30:
                    // Month
                    expirationDate = DateTime.Today.Date.AddMonths(expirationPeriod);
                    break;

                case 365:
                    // Year
                    expirationDate = DateTime.Today.Date.AddYears(expirationPeriod);
                    break;

                default:
                    // Days (default case)
                    expirationDate = DateTime.Today.Date.AddDays(expirationPeriod);
                    break;
            }

            return expirationDate;
        }
    }
}
