using System.Text;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Catalog
{
    /// <summary>
    /// Provides method to reconfigure the pricing format for a product
    /// </summary>
    public class ZNodePricingFormatter : ZNodeBusinessBase
    {
        /// <summary>
        /// Initializes a new instance of the ZNodePricingFormatter class
        /// </summary>
        public ZNodePricingFormatter() 
        {
        }
        
        /// <summary>
        /// Reconfigures the pricing format for a product
        /// </summary>
        /// <param name="actualPrice">Product actual price.</param>
        /// <param name="discountedPrice">Product discounted price.</param>
        /// <returns>Returns the formatted actual price string.</returns>
        public static string ConfigureProductPricing(decimal actualPrice, decimal discountedPrice)
        {
            string currencySuffix = ZNodeCurrencyManager.GetCurrencySuffix();

            StringBuilder formattedPrice = new StringBuilder();
            formattedPrice.Append("<span class=RegularPrice>");
            formattedPrice.Append(actualPrice.ToString("c"));

            if (!string.IsNullOrEmpty(currencySuffix))
            {
                formattedPrice.Append(" " + currencySuffix);
            }

            formattedPrice.Append("</span> &nbsp;");

            formattedPrice.Append("<span class=SalePrice>");
            formattedPrice.Append(discountedPrice.ToString("C"));

            if (!string.IsNullOrEmpty(currencySuffix))
            {
                formattedPrice.Append(" " + currencySuffix);
            }

            formattedPrice.Append("</span>");

            // Return Price
            return formattedPrice.ToString();            
        }

        /// <summary>
        /// Reconfigures the pricing format for a product
        /// </summary>
        /// <param name="actualPrice">Actual price value to format.</param>
        /// <returns>Returns the formatted actual price string.</returns>
        public static string ConfigureProductPricing(decimal actualPrice)
        {
            string currencySuffix = ZNodeCurrencyManager.GetCurrencySuffix();

            StringBuilder formattedPrice = new StringBuilder();
            formattedPrice.Append("<span class=Price>");
            formattedPrice.Append(actualPrice.ToString("C"));

            if (!string.IsNullOrEmpty(currencySuffix))
            {
                formattedPrice.Append(" " + currencySuffix);
            }

            formattedPrice.Append("</span>");

            return formattedPrice.ToString();
        }
    }
}
