using System;
using System.Xml.Serialization;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.Framework.Business;
using System.Data;

namespace ZNode.Libraries.ECommerce.Catalog
{
    #region ZNodeSKU
    /// <summary>
    /// Represents the properties and methods of a ZNodeSKU.
    /// </summary>
    [Serializable()]
    [XmlRoot()]
    public class ZNodeSKU : ZNodeSKUEntity
    {

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the ZNodeSKU class
        /// </summary>
        public ZNodeSKU()
        {
        }

        public ZNodeSKU(ZNodeSKUEntity skuEntity)
        {
            if (skuEntity == null) return;
            this.AttributesDescription = skuEntity.AttributesDescription;
            this.AttributesValue = skuEntity.AttributesValue;
            this.Custom1 = skuEntity.Custom1;
            this.Custom2 = skuEntity.Custom2;
            this.Custom3 = skuEntity.Custom3;
            this.DisplayOrder = skuEntity.DisplayOrder;
            this.ImageAltTag = skuEntity.ImageAltTag;
            this.IsActive = skuEntity.IsActive;
            this.Note = skuEntity.Note;
            this.ProductID = skuEntity.ProductID;
            this.QuantityBuffer = skuEntity.QuantityBuffer;
            this.QuantityOnHand = skuEntity.QuantityOnHand;
            this.RecurringBillingFrequency = skuEntity.RecurringBillingFrequency;
            this.RecurringBillingInitialAmount = skuEntity.RecurringBillingInitialAmount;
            this.RecurringBillingPeriod = skuEntity.RecurringBillingPeriod;
            this.RecurringBillingTotalCycles = skuEntity.RecurringBillingTotalCycles;
            this.ReorderLevel = skuEntity.ReorderLevel;
            this.RetailPriceOverride = skuEntity.RetailPriceOverride;
            this.SalePriceOverride = skuEntity.SalePriceOverride;
            this.NegotiatedPrice = skuEntity.NegotiatedPrice;
            this.SKU = skuEntity.SKU;
            this.SKUID = skuEntity.SKUID;
            this.SKUPicturePath = skuEntity.SKUPicturePath;
            this.SupplierID = skuEntity.SupplierID;
            this.Surcharge = skuEntity.Surcharge;
            this.WarehouseNo = skuEntity.WarehouseNo;
            this.WeightAdditional = skuEntity.WeightAdditional;
            this.WholesalePriceOverride = skuEntity.WholesalePriceOverride;
            this.SkuProfileCollection = skuEntity.SkuProfileCollection;
        }
        #endregion

        #region Static Methods

        /// <summary>
        /// Gets sku by product attribute.
        /// </summary>
        /// <param name="productId">Product Id to get the Sku attribute</param>
        /// <param name="attributes">Attributes to get the product sku.</param>
        /// <returns>Returns the sku by product attributes.</returns>
        public static ZNodeSKU CreateByProductAndAttributes(int productId, string attributes)
        {
            var externalAccountNo = string.Empty;
            if (ZNode.Libraries.ECommerce.UserAccount.ZNodeUserAccount.CurrentAccount() != null && ZNodeConfigManager.SiteConfig.EnableCustomerPricing.GetValueOrDefault() && ZNode.Libraries.ECommerce.UserAccount.ZNodeUserAccount.CurrentAccount().EnableCustomerPricing)
                externalAccountNo = ZNode.Libraries.ECommerce.UserAccount.ZNodeUserAccount.CurrentAccount().ExternalAccountNo;

            return ZnodeCatalogFactory.GetSkuByAttributes(productId, attributes, externalAccountNo);
        }

        /// <summary>
        /// Get a sku based on the SKUID
        /// </summary>
        /// <param name="productSku">Product SKU string</param>
        /// <returns>Returns the ZNodeSKU object</returns>
        public static ZNodeSKU CreateBySKU(string productSku, string externalAccountNo = null)
        {
            if (ZNode.Libraries.ECommerce.UserAccount.ZNodeUserAccount.CurrentAccount() != null && ZNodeConfigManager.SiteConfig.EnableCustomerPricing.GetValueOrDefault() && ZNode.Libraries.ECommerce.UserAccount.ZNodeUserAccount.CurrentAccount().EnableCustomerPricing)
                externalAccountNo = ZNode.Libraries.ECommerce.UserAccount.ZNodeUserAccount.CurrentAccount().ExternalAccountNo;

            return ZnodeCatalogFactory.GetBySku(productSku, ZNodeProfile.CurrentUserProfileId, externalAccountNo);
        }



        /// <summary>
        /// Returns the default sku for a product with NO attributes
        /// </summary>
        /// <param name="productId">Product Id to get the default sku.</param>
        ///  <param name="externalAccountNo"></param>
        /// <returns>Returns the default ZNodeSKU object.</returns>
        public static ZNodeSKU CreateByProductDefault(int productId, string externalAccountNo = null)
        {

            if (ZNode.Libraries.ECommerce.UserAccount.ZNodeUserAccount.CurrentAccount() != null && ZNodeConfigManager.SiteConfig.EnableCustomerPricing.GetValueOrDefault() && ZNode.Libraries.ECommerce.UserAccount.ZNodeUserAccount.CurrentAccount().EnableCustomerPricing && externalAccountNo == null)
                externalAccountNo = ZNode.Libraries.ECommerce.UserAccount.ZNodeUserAccount.CurrentAccount().ExternalAccountNo;

            return ZnodeCatalogFactory.GetDefaultSku(productId, externalAccountNo);
        }



        #endregion
    }
    #endregion
}
