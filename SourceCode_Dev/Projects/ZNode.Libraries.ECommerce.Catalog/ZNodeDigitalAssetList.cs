using System;
using System.Xml.Serialization;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Catalog
{
    /// <summary>
    /// Represents a digital asset collection 
    /// </summary>    
    [Serializable()]
    [XmlRoot("ZNodeDigitalAssetList")]
    public class ZNodeDigitalAssetList : ZNodeBusinessBase
    {
        private ZNodeGenericCollection<ZNodeDigitalAsset> _DigitalAssetCollection = new ZNodeGenericCollection<ZNodeDigitalAsset>();

        /// <summary>
        /// Gets or sets the digital asset collection.
        /// </summary>
        [XmlElement("ZNodeDigitalAsset")]
        public ZNodeGenericCollection<ZNodeDigitalAsset> DigitalAssetCollection
        {
            get
            {
                return this._DigitalAssetCollection;            
            }

            set
            {
                this._DigitalAssetCollection = value;
            }
        }

        #region Public Methods
        /// <summary>
        /// Returns a AddOn based on the productID and Attributes
        /// </summary>
        /// <param name="productId">Product Id to create disgital asset.</param>
        /// <param name="quantity">Quantity to get the digital asset.</param>
        /// <returns>Returns the ZNodeDigitalAssetList object.</returns>
        public static ZNodeDigitalAssetList CreateByProductIdAndQuantity(int productId, int quantity)
        {
            ZNode.Libraries.DataAccess.Custom.DigitalAssetHelper digitalAssetHelper = new ZNode.Libraries.DataAccess.Custom.DigitalAssetHelper();
            string xmlOut = digitalAssetHelper.GetDigitalAssetsByProductAndQuantityXML(productId, quantity);

            ZNodeDigitalAssetList digitalAssetList = new ZNodeDigitalAssetList();

            // serialize the object
            if (xmlOut.Trim().Length > 0)
            {
                ZNodeSerializer serializer = new ZNodeSerializer();
                digitalAssetList = (ZNodeDigitalAssetList)serializer.GetContentFromString(xmlOut, typeof(ZNodeDigitalAssetList));
            }

            return digitalAssetList;
        }
        #endregion
    }
}
