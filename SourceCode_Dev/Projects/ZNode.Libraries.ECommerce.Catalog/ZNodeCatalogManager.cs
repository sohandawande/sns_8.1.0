using System;
using System.Web;
using System.Xml.Serialization;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Catalog
{
    /// <summary>
    /// Represents the properties and methods of a ZNodeCategory.
    /// </summary>
    [Serializable()]
    [XmlRoot()]
    public class ZNodeCatalogManager : ZNodeBusinessBase
    {
        #region Public Properties
        /// <summary>
        /// Gets the theme name. If CatalogConfig null then default theme will be returned.
        /// </summary>
        public static string Theme
        {
            get
            {
                if (ZNodeCatalogManager.CatalogConfig != null)
                {
                    if (CatalogConfig.ThemeIDSource!=null && !string.IsNullOrEmpty(CatalogConfig.ThemeIDSource.Name))
                    {
                        return CatalogConfig.ThemeIDSource.Name;
                    }
                }

                return "Default";
            }
        }

        /// <summary>
        /// Gets the locale Id.
        /// </summary>
        public static int LocaleId
        {
            get
            {
                if (HttpContext.Current.Session != null && CatalogConfig != null)
                {
                    return CatalogConfig.LocaleID;
                }

                if (ZNodeConfigManager.SiteConfig.LocaleID > 0)
                {
                    return ZNodeConfigManager.SiteConfig.LocaleID;
                }

                return 43;
            }
        }

        /// <summary>
        /// Gets the locale code.
        /// </summary>
        public static string LocaleCode
        {
            get
            {
                if (HttpContext.Current.Session["CurrentUICulture"] != null)
                {
                    return HttpContext.Current.Session["CurrentUICulture"].ToString();
                }

                return "en";
            }
        }

        /// <summary>
        /// Gets the Css value
        /// </summary>
        public static string CSS
        {
            get
            {
                if (CatalogConfig != null)
                {
                    if (CatalogConfig.CSSIDSource != null && !string.IsNullOrEmpty(CatalogConfig.CSSIDSource.Name))
                    {
                        return CatalogConfig.CSSIDSource.Name;
                    }
                }

                return "Default";
            }
        }

        /// <summary>
        /// Gets the culture info.
        /// </summary>
        public static string CultureInfo
        {
            get
            {
                if (HttpContext.Current.Session["CurrentUICulture"] != null)
                {
                    return HttpContext.Current.Session["CurrentUICulture"].ToString();
                }

                if (CatalogConfig != null)
                {
                    if (CatalogConfig.LocaleID > 0)
                    {
                        LocaleService localservice = new LocaleService();
                        Locale locale = localservice.GetByLocaleID(CatalogConfig.LocaleID);

                        return locale.LocaleCode;
                    }
                }

                return "en";
            }
        }

        /// <summary>
        /// Gets the portal catalog.
        /// </summary>
        public static PortalCatalog CatalogConfig
        {
            get
            {
                return ZNodeCatalogManager.GetCatalog();
            }
        }
        #endregion

        #region Portal Catalog Config Methods
        /// <summary>
        /// Get Catalog by locale preference
        /// </summary>
        /// <returns>Returns the PortalCatalog object.</returns>
        public static PortalCatalog GetCatalog()
        {
            ZNode.Libraries.DataAccess.Entities.PortalCatalog portalCatalog = null;

            if (System.Web.HttpContext.Current.Session["CatalogConfig"] == null)
            {
                Locale locale = GetLocaleByLocaleCode(LocaleCode);

                if (locale != null)
                {
                    portalCatalog = (ZNode.Libraries.DataAccess.Entities.PortalCatalog)HttpContext.Current.Session["CatalogConfig"];
                }
            }
            else
            {
                portalCatalog = (ZNode.Libraries.DataAccess.Entities.PortalCatalog)HttpContext.Current.Session["CatalogConfig"];
            }

            return portalCatalog;
        }

        #endregion

        #region Locale Methods

        /// <summary>
        /// Get Locale by locale code
        /// </summary>
        /// <param name="localeCode">Locale code to get the Locale object.</param>
        /// <returns>Returns locale entity object if one exists, 
        /// otherwise default locale in the portal table</returns>
        public static Locale GetLocaleByLocaleCode(string localeCode)
        {
            PortalCatalog portalCatalog = ZNodeCatalogManager.GetDefaultLocaleCode(localeCode);

            Locale locale = null;

            if (portalCatalog != null)
            {
                locale = portalCatalog.LocaleIDSource;
                System.Web.HttpContext.Current.Session["CatalogConfig"] = portalCatalog;
            }

            return locale;
        }

        /// <summary>
        /// Get CSS path by locale
        /// Using Theme and CSS as Parameters
        /// </summary>
        /// <param name="theme">Theme name.</param>
        /// <param name="cssFileName">CSS file name.</param>
        /// <returns>Returns the locale specific css file name.</returns>
        public static string GetCssPathByLocale(string theme, string cssFileName)
        {
            string localeCode = ZNodeCatalogManager.CultureInfo;
            string cssFilePath = string.Format("~/Themes/{0}/{1}/{2}.css", theme, localeCode, cssFileName);

            bool isCssFileExists = System.IO.File.Exists(HttpContext.Current.Server.MapPath(cssFilePath));

            if (isCssFileExists)
            {
                return "Themes/" + theme + "/" + localeCode + "/Css/" + cssFileName + ".css";
            }
            else
            {
                // If category theme wise theme selected then apply the current category theme.
                if (HttpContext.Current.Session["CurrentCategoryTheme"] != null)
                {
                    theme = HttpContext.Current.Session["CurrentCategoryTheme"].ToString();
                }

                return string.Format("Themes/{0}/Css/{1}.css", theme, cssFileName);
            }
        }

        /// <summary>
        /// Get Receipt CSS path by locale
        /// Using theme and CSS as Parameters
        /// </summary>
        /// <param name="theme">Theme name.</param>
        /// <param name="cssFileName">CSS file name.</param>
        /// <returns>Returns the locale specific css file name for order receipt..</returns>
        public static string GetReceiptCssPathByLocale(string theme, string cssFileName)
        {
            string localeCode = ZNodeCatalogManager.CultureInfo;

            string cssFilePath = "~\\Themes\\" + theme + "\\" + localeCode + "\\receipt\\" + cssFileName + ".css";

            bool isCssFileExists = System.IO.File.Exists(HttpContext.Current.Server.MapPath(cssFilePath));

            if (isCssFileExists)
            {
                return "Themes/" + theme + "/" + localeCode + "/receipt/" + cssFileName + ".css";
            }
            else
            {
                return "Themes/" + theme + "/receipt/" + cssFileName + ".css";
            }
        }

        /// <summary>
        ///  Get Image path by locale
        ///  Using Image Name as Parameters
        /// </summary>
        /// <param name="imageFileName">Locale specific file name</param>
        /// <returns>Returns the locale specific image path. </returns>
        public static string GetImagePathByLocale(string imageFileName)
        {
            string localeCode = CultureInfo;

            string imageFilePath = "~\\Themes\\" + ZNodeCatalogManager.Theme + "\\" + localeCode + "\\Images\\" + imageFileName;

            bool isImageFileExists = System.IO.File.Exists(HttpContext.Current.Server.MapPath(imageFilePath));

            if (isImageFileExists)
            {
                return "~/Themes/" + ZNodeCatalogManager.Theme + "/" + localeCode + "/Images/" + imageFileName;
            }
            else
            {
                return "~/Themes/" + ZNodeCatalogManager.Theme + "/Images/" + imageFileName;
            }
        }

        /// <summary>
        /// Set the locale code.
        /// </summary>
        public static void SetLocaleCode()
        {
            string cookieCulture = string.Empty;
            bool isCookieCode = false;
            bool isUpdated = false;

            if (HttpContext.Current.Request.Cookies != null)
            {
                // Set Culture From Cookie
                if (HttpContext.Current.Request.Cookies["LocaleID"] != null)
                {
                    ZNodeSeoUrlBase.LocaleID = int.Parse(HttpContext.Current.Request.Cookies["LocaleID"].Value);
                    isUpdated = true;
                }
                else
                {
                    if (HttpContext.Current.Request.Cookies["CultureLanguage"] != null)
                    {
                        cookieCulture = HttpContext.Current.Request.Cookies["CultureLanguage"].Value;
                        isCookieCode = true;
                    }
                }
            }

            if (isCookieCode)
            {
                PortalCatalog validLocale = GetDefaultLocaleCode(cookieCulture);

                if (validLocale != null)
                {
                    // Set the localeId
                    ZNodeSeoUrlBase.LocaleID = validLocale.LocaleID;
                    isUpdated = true;
                }
            }

            if (!isUpdated)
            {
                ZNodeSeoUrlBase.LocaleID = ZNodeCatalogManager.LocaleId;
            }
        }

        /// <summary>
        /// Get Locale by locale code
        /// </summary>
        /// <param name="localeCode">Locale code to get the portal catalog.</param>
        /// <returns>Returns locale entity object if one exists, 
        /// otherwise default locale in the portal table</returns>
        private static PortalCatalog GetDefaultLocaleCode(string localeCode)
        {
            PortalCatalog portalCatalog = null;

            PortalCatalogService portalCatalogService = new PortalCatalogService();
            TList<PortalCatalog> portalCatalogList = portalCatalogService.GetByPortalID(ZNodeConfigManager.SiteConfig.PortalID);

            if (portalCatalogList.Count > 0)
            {
                portalCatalogService.DeepLoad(portalCatalogList, true, DeepLoadType.IncludeChildren, typeof(Locale),typeof(Theme),typeof(CSS));

                TList<PortalCatalog> tempPortalCatalogList = portalCatalogList.Copy();

                TList<PortalCatalog> filteredPortalCatalogList = portalCatalogList.FindAll(
                delegate(PortalCatalog pc)
                {
                    return string.Compare(pc.LocaleIDSource.LocaleCode, localeCode, true) == 0;
                });

                if (filteredPortalCatalogList.Count > 0)
                {
                    portalCatalog = filteredPortalCatalogList[0];
                }
                else
                {
                    filteredPortalCatalogList = tempPortalCatalogList.FindAll(
                    delegate(PortalCatalog pc)
                    {
                        return pc.LocaleID == ZNodeConfigManager.SiteConfig.LocaleID;
                    });

                    if (filteredPortalCatalogList.Count > 0)
                    {
                        portalCatalog = filteredPortalCatalogList[0];
                    }
                }
            }

            return portalCatalog;
        }
        
        #endregion

        #region MessageConfig
        /// <summary>
        /// Class to to get the Message Key values from Database instead of (ZNodeConfigManager.MessageConfig)  XML file for Catalog side.
        /// </summary>
        public static class MessageConfig
        {
            public static string GetMessage(string MessageKey, int PortalId, int LocaleId)
            {
                ZNode.Libraries.DataAccess.Service.MessageConfigService messageConfigServ = new ZNode.Libraries.DataAccess.Service.MessageConfigService();
                TList<ZNode.Libraries.DataAccess.Entities.MessageConfig> messageConfig = messageConfigServ.GetByKeyPortalIDLocaleIDMessageTypeID(MessageKey, PortalId, LocaleId, 1);  // 1 = ZNodeMessageType.Message
                if (messageConfig != null && messageConfig.Count > 0)
                {
                    return messageConfig[0].Value;
                }
                else
                {
                    return string.Empty;
                }
            }

            /// <summary>
            ///  Get the message text by message key.
            /// </summary>
            /// <param name="messageKey">Message key to get the message text.</param>
            /// <returns>Returns the message text.</returns>
            public static string GetMessage(string messageKey)
            {
                return MessageConfig.GetMessage(messageKey, CatalogConfig.PortalID, CatalogConfig.LocaleID);
            }
        }
        #endregion
    }
}
