using System;
using System.Xml.Serialization;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.ECommerce.SEO;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Catalog
{
    #region ZNodeCategory
    /// <summary>
    /// Represents the properties and methods of a ZNodeCategory.
    /// </summary>
    [Serializable()]
    [XmlRoot()]
    public class ZNodeCategory : ZNodeCategoryBase
    {
        #region Member Variables
        private ZNodeGenericCollection<ZNodeCategory> _CategoryCollection = new ZNodeGenericCollection<ZNodeCategory>();
        private string _CategoryPath = string.Empty;
        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the category collection.
        /// </summary>
        [XmlElement("ZNodeSubCategory")]
        public ZNodeGenericCollection<ZNodeCategory> ZNodeCategoryCollection
        {
            get
            {
                return this._CategoryCollection;
            }

            set
            {
                this._CategoryCollection = value;
            }
        }

        /// <summary>
        /// Gets the fully qualified link to category details page.
        /// </summary>
        [XmlIgnore()]
        public string ViewCategoryLink
        {
            get
            {
                return ZNodeSEOUrl.MakeURL(this.CategoryID.ToString(), SEOUrlType.Category, this.SEOURL);
            }
        }

        /// <summary>
        ///  Gets or sets the CategoryPath for this category
        /// </summary>      
        [XmlElement()]
        public string CategoryPath
        {
            get
            {
                if (!string.IsNullOrEmpty(_CategoryPath))
                {
                    ZNodeNavigation navigation = new ZNodeNavigation();
                    return navigation.ParsePath(this._CategoryPath, " &raquo; ");
                }

                return string.Empty;
            }
            
            set
            {
                _CategoryPath = value;
            }
        }
        #endregion

        #region Static Create
        /// <summary>
        /// This static method creates category using XMLSerializer
        /// </summary>
        /// <param name="categoryId">Category Id to get the category object.</param>
        /// <param name="portalId">Portal Id to get the categroy.</param>
        /// <param name="localeId">Locale Id of the category.</param>
        /// <returns>Returns the ZNodeCategory object.</returns>
        public static ZNodeCategory Create(int categoryId, int portalId, int localeId)
        {            
            return ZnodeCatalogFactory.GetCategoryById(categoryId, portalId, localeId);
        }

        /// <summary>
        /// Get a product list by portal Id, category Id and locale Id
        /// </summary>
        /// <param name="portalId">Portal Id to get the product list.</param>
        /// <param name="categoryId">Category Id to get the product list.</param>
        /// <param name="localeId">Locale Id to get the product list.</param>
        /// <returns>Returns the product list datatable.</returns>
        public static System.Data.DataTable GetProductsByCategory(int portalId, int categoryId, int localeId)
        {
            ZNode.Libraries.DataAccess.Custom.ProductHelper productHelper = new ZNode.Libraries.DataAccess.Custom.ProductHelper();
            return productHelper.GetProductsByCategoryID(categoryId, portalId, localeId, ZNodeProfile.CurrentUserProfileId);
        }
        #endregion       
    }
    #endregion
}
