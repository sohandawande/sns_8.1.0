using System.Globalization;
using System.Web;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Catalog
{
    /// <summary>
    /// Provides methods to format currencies (Retail Price,sale price).
    /// </summary>
    public class ZNodeCurrencyManager : ZNodeBusinessBase
    {        
        /// <summary>
        /// Creates and initializes the CultureInfo which uses the international sort (Ex: en-US).
        /// </summary>
        public static void CacheCurrencySetting()
        {
            string cultureType = "en-US";
            CurrencyType currencyType = null;
           
            if (!Equals(ZNodeConfigManager.SiteConfig , null))
            {
                if (HttpRuntime.Cache["CurrencyTypeCache" + ZNodeConfigManager.SiteConfig.PortalID] == null)
                {
                
                    if (ZNodeConfigManager.SiteConfig.CurrencyTypeID.HasValue)
                    {
                        int currencyTypeId = ZNodeConfigManager.SiteConfig.CurrencyTypeID.Value;

                        // Get Culture Info
                        ZNode.Libraries.DataAccess.Service.CurrencyTypeService currencyTypeService = new CurrencyTypeService();
                        currencyType = currencyTypeService.GetByCurrencyTypeID(currencyTypeId);

                        ZNodeCacheDependencyManager.Insert("CurrencyTypeCache" + ZNodeConfigManager.SiteConfig.PortalID, currencyType, "ZNodePortal");

                        if (currencyType != null)
                        {
                            cultureType = currencyType.Name;
                        }
                    }
                }

                //Set new culture to current thread
                System.Globalization.CultureInfo newcultureInfo = new System.Globalization.CultureInfo(cultureType);

                //This line is to retain the US datetime format instead of selected culture datetime format
                newcultureInfo.DateTimeFormat = System.Globalization.CultureInfo.CreateSpecificCulture("en-US").DateTimeFormat;

                // set new culture to use
                System.Threading.Thread.CurrentThread.CurrentCulture = newcultureInfo;
            }
        }

        /// <summary>
        /// Retreieves the culture type value from cache object, if one exists
        /// oterwise, calls the cache method to hold the culture type
        /// </summary>
        public static void SetPageCulture()
        {
            string cultureType = "en-US";
            CurrencyType currencyType = null;

            if (HttpRuntime.Cache["CurrencyTypeCache" + ZNodeConfigManager.SiteConfig.PortalID] == null)
            {
                CacheCurrencySetting();
            }
            else
            {
                // Retrieve Culture type from cache object
                currencyType = HttpRuntime.Cache["CurrencyTypeCache" + ZNodeConfigManager.SiteConfig.PortalID] as CurrencyType;

                if (currencyType != null)
                {
                    cultureType = currencyType.Name;
                }

                //Set new culture to current thread
                System.Globalization.CultureInfo newcultureInfo = new System.Globalization.CultureInfo(cultureType);

                //This is to retain the US datetime format instead of selected culture datetime format
                newcultureInfo.DateTimeFormat = System.Globalization.CultureInfo.CreateSpecificCulture("en-US").DateTimeFormat;

                // set new culture to use
                System.Threading.Thread.CurrentThread.CurrentCulture = newcultureInfo;
            }
        }

        /// <summary>
        /// Returns the ISO currency Symbol like AUD,INR,SGD
        /// </summary>
        /// <returns></returns>
        public static string GetCurrencySuffix()
        {
            CurrencyType currencyType = null;

            // Check cache object for null value
            if (HttpRuntime.Cache["CurrencyTypeCache" + ZNodeConfigManager.SiteConfig.PortalID] != null)
            {
                currencyType = HttpRuntime.Cache["CurrencyTypeCache" + ZNodeConfigManager.SiteConfig.PortalID] as CurrencyType;
            }

            if (currencyType != null && !string.IsNullOrEmpty(currencyType.CurrencySuffix))
            {
                if (currencyType.CurrencySuffix.Length > 0)
                    return " (" + currencyType.CurrencySuffix + ")";
                else
                    return string.Empty;
            }

            return string.Empty;
        }

        /// <summary>
        /// Returns the Currency Symbol like $,Y,
        /// </summary>
        /// <returns></returns>
        public static string GetCurrencyPrefix()
        {
            // Create RegionInfo using CultureInfo (Current Culture)
            System.Globalization.RegionInfo regionInfo = new System.Globalization.RegionInfo(System.Threading.Thread.CurrentThread.CurrentCulture.LCID);

            return regionInfo.CurrencySymbol;
        }

        /// <summary>
        /// Returns the Currency code for the current culture
        /// </summary>
        /// <returns></returns>
        public static string CurrencyCode()
        {            
            // Create RegionInfo using CultureInfo (Current Culture)
            System.Globalization.RegionInfo regionInfo = new System.Globalization.RegionInfo(System.Threading.Thread.CurrentThread.CurrentCulture.LCID);

            return regionInfo.ISOCurrencySymbol;         
        }

        /// <summary>
        /// Get Currency Unit for a price.
        /// </summary>
        /// <param name="priceValue">Price value.</param>
        /// <returns>Price with currency symbol and currency suffix.</returns>
        public static string GetCurrencyUnit(decimal priceValue)
        {
            CurrencyType currencyType = HttpRuntime.Cache["CurrencyTypeCache" + ZNodeConfigManager.SiteConfig.PortalID] as CurrencyType;

            string currencySymbol = "en-US";
            if (!Equals(currencyType , null))
            {
                currencySymbol = currencyType.Name;
            }

            //Set new culture to current thread
            System.Globalization.CultureInfo newcultureInfo = new System.Globalization.CultureInfo(currencySymbol);
            string currencyValue = string.Empty;

            CultureInfo info = new CultureInfo(currencySymbol);

            decimal price = priceValue;

            currencyValue = price.ToString("c", info.NumberFormat);

            return currencyValue;
        }
    }
}
