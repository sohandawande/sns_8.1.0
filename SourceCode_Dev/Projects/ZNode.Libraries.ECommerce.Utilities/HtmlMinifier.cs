﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace ZNode.Libraries.ECommerce.Utilities
{
    public class HtmlMinifier : IHttpModule
    {
        public void Dispose()
        {
        }

        public void Init(HttpApplication context)
        {
            context.ReleaseRequestState += new EventHandler(context_ReleaseRequestState);
        }

        void context_ReleaseRequestState(object sender, EventArgs e)
        {
            try
            {
                HttpApplication app = (HttpApplication)sender;

                if (app == null || app.Context == null || app.Context.Request == null || app.Context.Request.RawUrl.StartsWith("/console") || app.Context.Request.RawUrl.StartsWith("/Integration") || app.Context.Response == null)//|| (HttpContext.Current.Handler is System.Web.UI.Page) == false
                {
                    return;
                }

                HttpResponse Response = app.Context.Response;
                HttpRequest Request = app.Context.Request;
                string url = Request.RawUrl.ToLower();
                bool isAync = Request.QueryString["ajax"] != null ? true : false;
                if (Response.ContentType == "text/html")//&& Request.HttpMethod.ToUpper() == "GET"
                {

                    PageFilterStream filter = new PageFilterStream(Response.Filter);
                    filter.TransformString += filter_TransformString;
                    Response.Filter = filter;
                }

            }
            catch (Exception ex)
            {

            }
        }

        string filter_TransformString(string output)
        {
            string filteredOutput = string.Empty;
            try
            {
                HttpRequest Request = HttpContext.Current.Request;
                filteredOutput = GetSpaceCleanedHtml(output);
                return filteredOutput;
            }
            catch (Exception ex)
            {
                return output;
            }

        }

        public string GetSpaceCleanedHtml(string html)
        {
            //Regex RegexBetweenTags = new Regex(@">(?! )\s+", RegexOptions.Compiled);
            //Regex RegexLineBreaks = new Regex(@"([\n\s])+?(?<= {2,})<", RegexOptions.Compiled);
            //if ((HttpContext.Current.Handler is Page && HttpContext.Current.Handler.GetType().Name != "SyncSessionlessHandler")
            //        && HttpContext.Current.Request["HTTP_X_MICROSOFTAJAX"] == null)
            if (HttpContext.Current.Response.ContentType == "text/html")
            {
                //html = RegexBetweenTags.Replace(html, ">");
                //html = RegexLineBreaks.Replace(html, "<");
                html = Regex.Replace(html, @">(?! )\s+", ">");
                html = Regex.Replace(html, @"([\n\s])+?(?<= {2,})<", "<");
                html = Regex.Replace(html, @"\s*\n\s*", "\n");
                html = Regex.Replace(html, @"\s*\>\s*\<\s*", "><");
                html = Regex.Replace(html, @"<!--(.*?)-->", "");
                html = html.Trim();
            }

            return html;
        }
    }
}
