﻿using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Utilities
{
    /// <summary>
    /// Znode Logging Class
    /// </summary>
    public class ZNodeLogging : ZNodeLoggingBase
    {
        /// <summary>
        /// Represents the Error Number for logging.
        /// </summary>
        public new enum ErrorNum
        {
            /// <summary>
            /// Represents the general error
            /// </summary>
            GeneralError = 1,

            /// <summary>
            /// Represents the General Warning
            /// </summary>
            GeneralWarning = 2,

            /// <summary>
            /// Represents the General Message
            /// </summary>
            GeneralMessage = 3,

            /// <summary>
            /// Represents the Upgrade schema
            /// </summary>
            UpgradeSchema = 100,

            /// <summary>
            /// Represents the Diagnostics page
            /// </summary>
            DiagnosticsPage = 101,

            /// <summary>
            /// Represents the Diagnostics sent
            /// </summary>
            DiagnosticsSent = 102,

            /// <summary>
            /// Represents the Activation Page
            /// </summary>
            ActivationPage = 103,

            /// <summary>
            /// Represents the Activation success
            /// </summary>
            ActivationSuccess = 104,

            /// <summary>
            /// Represents the Activation Failed
            /// </summary>
            ActivationFailed = 105,

            /// <summary>
            /// Represents the Login Success
            /// </summary>
            LoginSuccess = 1000,

            /// <summary>
            /// Represents the Login Failed
            /// </summary>
            LoginFailed = 1001,

            /// <summary>
            /// Represents the Login Create Success
            /// </summary>
            LoginCreateSuccess = 1002,

            /// <summary>
            /// Represents the Login Crate Failed
            /// </summary>
            LoginCreateFailed = 1003,

            /// <summary>
            /// Represents the Account Unlock
            /// </summary>
            AccountUnLock = 1104,

            /// <summary>
            /// Represents the Account Lock
            /// </summary>
            AccountLock = 1105,

            /// <summary>
            /// Represents the Password Change Success
            /// </summary>
            PasswordChangeSuccess = 1106,

            /// <summary>
            /// Represents the Password Change Failed
            /// </summary>
            PasswordChangeFailed = 1107,

            /// <summary>
            /// Represents the Password Reset Success
            /// </summary>
            PasswordResetSuccess = 1108,

            /// <summary>
            /// Represents the Password Reset Failed
            /// </summary>
            PasswordResetFailed = 1109,

            /// <summary>
            /// Represents the User Log Out
            /// </summary>
            UserLogout = 1110,

            /// <summary>
            /// Represents the Application start
            /// </summary>
            ApplicationStart = 2000,

            /// <summary>
            /// Represents the Application End
            /// </summary>
            ApplicationEnd = 2001,

            /// <summary>
            /// Represents the Create Object
            /// </summary>
            CreateObject = 3001,

            /// <summary>
            /// Represents the Edit Object
            /// </summary>
            EditObject = 3002,

            /// <summary>
            /// Represents the Delete Object
            /// </summary>
            DeleteObject = 3003,

            /// <summary>
            /// Represents the Payment Settings Change
            /// </summary>
            PaymentSettingsChange = 3004,

            /// <summary>
            /// Represents the Imported
            /// </summary>
            Imported = 3005,

            /// <summary>
            /// Represents the Exported
            /// </summary>
            Exported = 3006,

            /// <summary>
            /// Represents the View Object
            /// </summary>
            ViewObject = 3007,

            /// <summary>
            /// Represents the activity Log Report
            /// </summary>
            ActivityLogReport = 4001,

            /// <summary>
            /// Represents the Submit payment success
            /// </summary>
            SubmitPaymentSuccess = 5000,

            /// <summary>
            /// Represents the Submit Payment Failed
            /// </summary>
            SubmitPaymentFailed = 5001,

            /// <summary>
            /// Represents the Order Submission Success
            /// </summary>
            OrderSubmissionSuccess = 5002,

            /// <summary>
            /// Represents the Order Submission Failed
            /// </summary>
            OrderSubmissionFailed = 5003,

            /// <summary>
            /// Represents the Store Key Rotated
            /// </summary>
            StoreKeyRotated = 9000,

            /// <summary>
            /// Represents the Store Setting Change Success
            /// </summary>
            StoreSettingsChangeSuccess = 9001,

            /// <summary>
            /// Represents the Store settings change failed
            /// </summary>
            StoreSettingsChangeFailed = 9002,

            /// <summary>
            /// Represents the Keyword Search
            /// </summary>
            KeywordSearch = 9500,

            /// <summary>
            /// Represents the Product Search
            /// </summary>
            Productsearch = 9501,

            /// <summary>
            /// Represents the SKU Search
            /// </summary>
            SKUsearch = 9502
        }
    }
}
