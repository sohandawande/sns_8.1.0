﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Web;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Utilities
{
    /// <summary>
    /// Represents the ImageHelper class.
    /// </summary>
    [Serializable]
    public class ZNodeImage
    {
        #region Private Variables
        /// <summary>
        /// No Image File Name
        /// </summary>
        private string noImageFileName = string.Empty;

        /// <summary>
        /// Is Already checked
        /// </summary>
        private bool isAlreadyChecked = false;

        /// <summary>
        /// Use Site config or not
        /// </summary>
        private bool useSiteConfig = false;

        #endregion

        #region Public Constructor
        /// <summary>
        /// Initializes a new instance of the ZNodeImage class.
        /// </summary>
        public ZNodeImage()
        {
            // Get No image from store settings
            if (!string.IsNullOrEmpty(ZNodeConfigManager.SiteConfig.ImageNotAvailablePath))
            {
                this.noImageFileName = Path.GetFileName(ZNodeConfigManager.SiteConfig.ImageNotAvailablePath).Trim();
            }

            // Get No Image file name from web.config
            if ((System.Configuration.ConfigurationManager.AppSettings["ImageNotAvailableFileName"] != null) && string.IsNullOrEmpty(this.noImageFileName))
            {
                this.noImageFileName = System.Configuration.ConfigurationManager.AppSettings["ImageNotAvailableFileName"].ToString();
            }

            // Hard  code the No image file name.
            if (string.IsNullOrEmpty(this.noImageFileName))
            {
                this.noImageFileName = "noimage.gif";
            }
        }
        #endregion

        /// <summary>
        /// Get the large size image relative path.
        /// </summary>
        /// <param name="imageFileName">Image file name.</param>
        /// <returns>Returns the large size image relative path.</returns>
        public string GetImageHttpPathLarge(string imageFileName)
        {
            return this.GetRelativeImageUrl(ZNodeConfigManager.SiteConfig.MaxCatalogItemLargeWidth, imageFileName, false);
        }

        /// <summary>
        /// Get the small medium size image relative path.
        /// </summary>
        /// <param name="imageFileName">Image file name.</param>
        /// <returns>Returns the medium size image relative path.</returns>
        public string GetImageHttpPathMedium(string imageFileName)
        {
            return this.GetRelativeImageUrl(ZNodeConfigManager.SiteConfig.MaxCatalogItemMediumWidth, imageFileName, false);
        }

        /// <summary>
        /// Get the small image relative path.
        /// </summary>
        /// <param name="imageFileName">Image file name.</param>
        /// <returns>Returns the small image relative path.</returns>
        public string GetImageHttpPathSmall(string imageFileName)
        {
            return this.GetRelativeImageUrl(ZNodeConfigManager.SiteConfig.MaxCatalogItemSmallWidth, imageFileName, false);
        }

        /// <summary>
        /// Get the thumbnail image relative path.
        /// </summary>
        /// <param name="imageFileName">Image file name.</param>
        /// <returns>Returns the thumbnail image relative path.</returns>
        public string GetImageHttpPathThumbnail(string imageFileName)
        {
            return this.GetRelativeImageUrl(ZNodeConfigManager.SiteConfig.MaxCatalogItemThumbnailWidth, imageFileName, false);
        }

        /// <summary>
        /// Get the cross sell image relative path.
        /// </summary>
        /// <param name="imageFileName">Image file name.</param>
        /// <returns>Returns the cross sell image relative path.</returns>
        public string GetImageHttpPathCrossSell(string imageFileName)
        {
            return this.GetRelativeImageUrl(ZNodeConfigManager.SiteConfig.MaxCatalogItemCrossSellWidth, imageFileName, false);
        }

        /// <summary>
        /// Get the small thumbnail image relative path.
        /// </summary>
        /// <param name="imageFileName">Image file name.</param>
        /// <returns>Returns the small thumbnail image relative path.</returns>
        public string GetImageHttpPathSmallThumbnail(string imageFileName)
        {
            return this.GetRelativeImageUrl(ZNodeConfigManager.SiteConfig.MaxCatalogItemSmallThumbnailWidth, imageFileName, true);
        }

        /// <summary>
        /// Get Image By size
        /// </summary>
        /// <param name="imageSize">Image file size</param>
        /// <param name="imageFileName">Image file name.</param>
        /// <returns>Returns the image by specified size</returns>
        public string GetImageBySize(int imageSize, string imageFileName)
        {
            return this.GetRelativeImageUrl(imageSize, imageFileName, false);
        }

        public string GetStoreLogoBySize(System.Drawing.Size imageSize, string imageFileName)
        {
            string orginalImageDirectory = string.Format("{0}images/catalog/original/turnkey/{1}/", ZNodeConfigManager.EnvironmentConfig.DataPath, ZNodeConfigManager.SiteConfig.PortalID);
            string returnUrl = string.Empty;
            string imageFileFullName = string.Empty;
            if (imageFileName == null)
            {
                imageFileName = string.Empty;
            }
            else
            {
                imageFileFullName = Path.Combine(orginalImageDirectory, imageFileName);
            }
                if (!string.IsNullOrEmpty(imageFileName))
                {
                    // If file exists and resized image folder and no new image uploaded in original folder then use already resized image.
                    if (!ZNodeStorageManager.Exists(imageFileFullName))
                    {
                        string originalImageDirectory = string.Format("{0}images/catalog/{1}/", ZNodeConfigManager.EnvironmentConfig.DataPath, imageSize.Width);
                        string originalImageFileFullName = Path.Combine(originalImageDirectory, imageFileName);
                        string LogoOrignalImagePath = Path.Combine(ZNodeConfigManager.EnvironmentConfig.OriginalImagePath, imageFileName);

                        if (ZNodeStorageManager.Exists(originalImageFileFullName))
                        {
                            returnUrl = originalImageFileFullName;
                        }
                        else
                        {
                            returnUrl = this.ResizeImage(originalImageFileFullName, imageSize.Height, imageSize.Width, originalImageDirectory);
                        }
                    }
                    else
                    {
                        string resizedImageDirectory = string.Format("{0}images/catalog/{2}/turnkey/{1}/", ZNodeConfigManager.EnvironmentConfig.DataPath, ZNodeConfigManager.SiteConfig.PortalID, imageSize.Width);
                        string resizedImageFullName = Path.Combine(resizedImageDirectory, imageFileName);
                        if (ZNodeStorageManager.Exists(resizedImageFullName))
                        {
                            returnUrl = resizedImageFullName;
                        }
                        else
                        {
                            returnUrl = this.ResizeImage(imageFileFullName, imageSize.Height, imageSize.Width, resizedImageDirectory);
                        }
                    }
                }
                else
                {
                    returnUrl = GetImageHttpPathSmall(string.Format("Turnkey/{0}/{1}", ZNodeConfigManager.SiteConfig.PortalID, Path.GetFileName(imageFileName)));
                }
            
                return returnUrl;

        }
        #region Helper Methods

        /// <summary>
        /// Get the relative image path to a resized image. If the resized image does not exist then it will be created. If the image does not exist.
        /// then the "No Image" image will be returned.
        /// </summary>
        /// <param name="imageSize">Image Size you want to get.</param>
        /// <param name="imageFileName">The name of the image you want to get.</param>
        /// <param name="cropImage">Indicates whether to crop the image.</param>
        /// <param name="imageFileExists">Returns true if image file exists else false.</param>
        /// <returns>Returns the resized image file path.</returns>
        public string GetImageHttpPath(int imageSize, string imageFileName, bool cropImage, out bool imageFileExists)
        {
            string imageFileFullName = string.Empty;
            string imageFilePath = string.Empty;
            string fileName = string.Empty;

            // Build up a path for our resized image.
            imageFilePath = Path.Combine(ZNodeConfigManager.EnvironmentConfig.ImagePath, imageSize.ToString()) + "/";
            imageFileFullName = Path.Combine(imageFilePath, imageFileName);

            // If file exists and resized image folder and no new image uploaded in original folder then use already resized image.
            if (ZNodeStorageManager.Exists(imageFileFullName))
            {
                imageFileExists = true;
                return imageFileFullName;
            }
            else
            {
                string originalFileName = cropImage ? imageFileName.ToLower().Replace("-swatch.", ".") : imageFileName;

                string orignalImagePath = string.Empty;
                if (this.useSiteConfig)
                {
                    orignalImagePath = Path.Combine(Path.GetDirectoryName(ZNodeConfigManager.SiteConfig.ImageNotAvailablePath), imageFileName);
                }
                else
                {
                    orignalImagePath = Path.Combine(ZNodeConfigManager.EnvironmentConfig.OriginalImagePath, originalFileName);
                }

                imageFileExists = false;

                // Check is file exist in Original folder.
                if (ZNodeStorageManager.Exists(orignalImagePath))
                {
                    // Resize the image for the current request image size.
                    fileName = this.ResizeImage(orignalImagePath, imageSize, imageFilePath);

                    // Crop the image only for swatch.
                    if (cropImage)
                    {
                        int maxSmallThumnailWidthHeight = ZNodeConfigManager.SiteConfig.MaxCatalogItemSmallThumbnailWidth;

                        string croppedImageFileName = Path.GetFileName(this.CropImage(orignalImagePath, maxSmallThumnailWidthHeight, maxSmallThumnailWidthHeight, fileName.ToLower().Replace("-swatch.", ".")));
                        fileName = Path.Combine(imageFilePath, croppedImageFileName);
                    }
                }
                else
                {
                    // If source file doesn't exist in Original folder then return the Image Not Available file.
                    string noImageRelativePath = string.Empty;

                    // Get the no image file from store settings.
                    if (ZNodeConfigManager.SiteConfig.ImageNotAvailablePath.Trim().Length > 0)
                    {
                        noImageRelativePath = ZNodeConfigManager.SiteConfig.ImageNotAvailablePath;
                        if (ZNodeStorageManager.Exists(noImageRelativePath))
                        {
                            // Set to use the SiteConfig for source file lookup.
                            this.useSiteConfig = true;

                            // Call the current method recursively with the site "Not Available" image name.
                            fileName = this.GetImageHttpPath(imageSize, Path.GetFileName(noImageRelativePath), cropImage, out imageFileExists);

                            if (cropImage)
                            {
                                fileName = Path.Combine(imageFilePath, Path.GetFileNameWithoutExtension(fileName) + "-swatch" + Path.GetExtension(fileName));
                            }
                        }
                        else
                        {
                            fileName = imageFileFullName;
                        }
                    }
                    else
                    {
                        noImageRelativePath = Path.Combine(ZNodeConfigManager.EnvironmentConfig.OriginalImagePath, this.noImageFileName);
                        if (ZNodeStorageManager.Exists(noImageRelativePath))
                        {
                            // Call the current method recursively with the "Original" folder this.noImageFileName.
                            fileName = this.GetImageHttpPath(imageSize, this.noImageFileName, cropImage, out imageFileExists);

                            if (cropImage)
                            {
                                fileName = Path.Combine(imageFilePath, Path.GetFileNameWithoutExtension(fileName) + "-swatch" + Path.GetExtension(fileName));
                            }
                        }
                        else
                        {
                            // Image not available in Data/Default/Images and Data/Default/Images/Catalog/Original folders.
                            fileName = imageFileFullName;
                        }
                    }
                }

                // Reset to use EnvironmentConfig for Original file folder.
                this.useSiteConfig = false;

                return fileName;
            }
        }

        /// <summary>
        /// Resizing the image size and storing it in the respective folder.
        /// </summary>
        /// <param name="relativeImageFilePath">Relative Image file path.</param>
        /// <param name="maxHeight">Maximum Height of the image.</param>
        /// <param name="maxWidth">Maximum width of the image.</param>
        /// <param name="saveToFullPath">Physical path</param>
        /// <returns>Returns the resised image relative path.</returns>
        public string ResizeImage(string relativeImageFilePath, int maxHeight, int maxWidth, string saveToFullPath)
        {
            string returnUrl = string.Empty;
            string fullName = string.Empty;
            string fileName = Path.GetFileName(relativeImageFilePath);
            string[] str = relativeImageFilePath.Split(new char[] { '/' });
            if (this.useSiteConfig)
            {
                fullName = Path.Combine(Path.GetDirectoryName(ZNodeConfigManager.SiteConfig.ImageNotAvailablePath), fileName);
            }
            else
            {
                if (str.Length == 9)
                {
                    fullName = ZNodeConfigManager.EnvironmentConfig.OriginalImagePath + str[6] + "/" + str[7] + "/" + fileName;
                    //saveToFullPath = saveToFullPath + str[6] + "/" + str[7] + "/";
                }
                else
                {
                    fullName = ZNodeConfigManager.EnvironmentConfig.OriginalImagePath + fileName;
                }
            }

            byte[] fileData = ZNodeStorageManager.ReadBinaryStorage(fullName);
            MemoryStream ms = new MemoryStream(fileData);
            Image sourceImage = Image.FromStream(ms);
            decimal scaleFactor;
            decimal originalProportion = sourceImage.Width / sourceImage.Height;
            decimal resizeProportion = maxWidth / maxHeight;
            if (originalProportion > resizeProportion)
            {
                scaleFactor = Convert.ToDecimal(maxWidth) / Convert.ToDecimal(sourceImage.Width);
            }
            else
            {
                scaleFactor = Convert.ToDecimal(maxHeight) / Convert.ToDecimal(sourceImage.Height);
            }

            int newWidth = (int)Math.Round((sourceImage.Width * scaleFactor));
            int newHeight = (int)Math.Round((sourceImage.Height * scaleFactor));
            Bitmap thumbnailBitmap = new Bitmap(newWidth, newHeight);

            Graphics thumbnailGraph = Graphics.FromImage(thumbnailBitmap);
            thumbnailGraph.CompositingQuality = CompositingQuality.HighQuality;
            thumbnailGraph.SmoothingMode = SmoothingMode.HighQuality;
            thumbnailGraph.InterpolationMode = InterpolationMode.HighQualityBicubic;
            thumbnailGraph.Clear(Color.White);

            Rectangle imageRectangle = new Rectangle(0, 0, newWidth, newHeight);
            thumbnailGraph.DrawImage(sourceImage, imageRectangle);

            MemoryStream stream = new MemoryStream();
            Image imageToSave = thumbnailBitmap;
            imageToSave.Save(stream, ImageFormat.Jpeg);
            imageToSave.Dispose();
            stream.Seek(0, SeekOrigin.Begin);
            fileData = new byte[stream.Length];
            stream.Read(fileData, 0, fileData.Length);

            ZNodeStorageManager.WriteBinaryStorage(fileData, Path.Combine(saveToFullPath, fileName));

            thumbnailGraph.Dispose();
            thumbnailBitmap.Dispose();
            sourceImage.Dispose();

            ms.Close();

            return Path.Combine(saveToFullPath, fileName);
        }

        /// <summary>
        /// Resize the image without losing quality
        /// </summary>
        /// <param name="orignalImagePath">Original Image Path</param>
        /// <param name="maxSize">Maximum file resize size.</param>
        /// <param name="saveToFullPath">Save to physical path.</param>
        /// <returns>Returns the image physical path with file name.</returns>
        public string ResizeImage(string orignalImagePath, int maxSize, string saveToFullPath)
        {
            string returnUrl = string.Empty;
            string fullName = string.Empty;
            string fileName = Path.GetFileName(orignalImagePath);
            string[] str = orignalImagePath.Split(new char[] { '/' });
            if (this.useSiteConfig)
            {
                fullName = Path.Combine(Path.GetDirectoryName(ZNodeConfigManager.SiteConfig.ImageNotAvailablePath), fileName);
            }
            else
            {
                if (str.Length == 9)
                {
                    fullName = ZNodeConfigManager.EnvironmentConfig.OriginalImagePath + str[6] + "/" + str[7] + "/" + fileName;
                    saveToFullPath = saveToFullPath + str[6] + "/" + str[7] + "/";
                }
                else
                {
                    fullName = ZNodeConfigManager.EnvironmentConfig.OriginalImagePath + fileName;
                }
            }

            byte[] bytes = ZNodeStorageManager.ReadBinaryStorage(fullName);

            if (bytes == null || bytes.Length == 0)
            {
                return string.Empty;
            }

            MemoryStream ms = new MemoryStream(bytes);
            Image sourceImage = Image.FromStream(ms);
            decimal scaleFactor;
            if (sourceImage.Width >= sourceImage.Height)
            {
                scaleFactor = Convert.ToDecimal(maxSize) / Convert.ToDecimal(sourceImage.Width);
            }
            else
            {
                scaleFactor = Convert.ToDecimal(maxSize) / Convert.ToDecimal(sourceImage.Height);
            }

            int newWidth = (int)Math.Round((sourceImage.Width * scaleFactor));
            int newHeight = (int)Math.Round((sourceImage.Height * scaleFactor));
            Bitmap thumbnailBitmap = new Bitmap(newWidth, newHeight);

            Graphics thumbnailGraph = Graphics.FromImage(thumbnailBitmap);
            thumbnailGraph.CompositingQuality = CompositingQuality.HighQuality;
            thumbnailGraph.SmoothingMode = SmoothingMode.HighQuality;
            thumbnailGraph.InterpolationMode = InterpolationMode.HighQualityBicubic;
            thumbnailGraph.Clear(Color.White);

            Rectangle imageRectangle = new Rectangle(0, 0, newWidth, newHeight);
            thumbnailGraph.DrawImage(sourceImage, imageRectangle);

            MemoryStream stream = new MemoryStream();
            Image imageToSave = thumbnailBitmap;
            imageToSave.Save(stream, ImageFormat.Jpeg);
            imageToSave.Dispose();
            stream.Seek(0, SeekOrigin.Begin);
            bytes = new byte[stream.Length];
            stream.Read(bytes, 0, bytes.Length);

            ZNodeStorageManager.WriteBinaryStorage(bytes, Path.Combine(saveToFullPath, fileName));

            thumbnailGraph.Dispose();
            thumbnailBitmap.Dispose();
            sourceImage.Dispose();

            ms.Close();
           
            return Path.Combine(saveToFullPath, fileName);
        }

        /// <summary>
        /// Method for cropping an image.
        /// </summary>
        /// <param name="orignalImagePath">Original Image path</param>
        /// <param name="width">New width of the image.</param>
        /// <param name="height">New height of the image</param>
        /// <param name="saveToFullPath">Save to full path.</param>        
        /// <returns>Returns the image relative path.</returns>
        public string CropImage(string orignalImagePath, int width, int height, string saveToFullPath)
        {
            string fileName = orignalImagePath;

            try
            {
                // Original image
                byte[] fileData = ZNodeStorageManager.ReadBinaryStorage(fileName);
                MemoryStream stream = new MemoryStream(fileData);
                Image imgPhoto = Image.FromStream(stream);

                int targetW = width;
                int targetH = height;
                int targetX = 0;
                int targetY = 0;

                int pointX = imgPhoto.Width / 2;
                int pointY = imgPhoto.Height / 2;

                targetX = pointX - (targetW / 2);
                targetY = pointY - (targetH / 2) - 2;

                Bitmap bmpPhoto = new Bitmap(targetW, targetH, PixelFormat.Format24bppRgb);
                bmpPhoto.SetResolution(80, 60);

                Graphics gfxPhoto = Graphics.FromImage(bmpPhoto);
                gfxPhoto.CompositingQuality = CompositingQuality.HighQuality;
                gfxPhoto.SmoothingMode = SmoothingMode.HighQuality;
                gfxPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;
                gfxPhoto.PixelOffsetMode = PixelOffsetMode.HighQuality;

                gfxPhoto.DrawImage(imgPhoto, new Rectangle(0, 0, targetW, targetH), targetX, targetY, targetW, targetH, GraphicsUnit.Pixel);

                fileName = Path.Combine(Path.GetDirectoryName(saveToFullPath), Path.GetFileNameWithoutExtension(saveToFullPath) + "-Swatch" + Path.GetExtension(fileName));

                stream = new MemoryStream();
                Image imageToSave = bmpPhoto;
                imageToSave.Save(stream, ImageFormat.Jpeg);
                imageToSave.Dispose();
                stream.Seek(0, SeekOrigin.Begin);
                fileData = new byte[stream.Length];
                stream.Read(fileData, 0, fileData.Length);

                ZNodeStorageManager.WriteBinaryStorage(fileData, fileName);

                // Dispose of all the objects to prevent memory leaks
                imgPhoto.Dispose();
                bmpPhoto.Dispose();
                gfxPhoto.Dispose();
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.StackTrace);
            }

            return fileName;
        }

        /// <summary>
        /// Resize the mobile splash images. Splash image width hard-coded as 320 and 640.
        /// </summary>
        /// <param name="relativeImageFilePath">Relative Image File Path.</param>
        public void ResizeMobileSplashImage(string relativeImageFilePath)
        {
            string splashImageWidthSmallDir = Path.Combine(ZNodeConfigManager.EnvironmentConfig.DataPath, "images/splash/320");
            string splashImageWidthLargeDir = Path.Combine(ZNodeConfigManager.EnvironmentConfig.DataPath, "images/splash/640");

            this.ResizeImage(relativeImageFilePath, 320, splashImageWidthSmallDir + "/");
            this.ResizeImage(relativeImageFilePath, 640, splashImageWidthLargeDir + "/");
        }

        /// <summary>
        /// Get a resized image relative path.
        /// </summary>
        /// <param name="imageSize">Image Size</param>
        /// <param name="imageFileName">Image file name.</param>
        /// <param name="cropImage">Crop Image</param>
        /// <returns>Returns the resized relative image path.</returns>
        public string GetRelativeImageUrl(int imageSize, string imageFileName, bool cropImage)
        {
            string returnFileName = string.Empty;
            bool isImageFileExists = true;

            if (string.IsNullOrEmpty(imageFileName))
            {
                imageFileName = this.noImageFileName;
            }

            returnFileName = this.GetImageHttpPath(imageSize, imageFileName, cropImage, out isImageFileExists);

            if (!isImageFileExists && !this.isAlreadyChecked)
            {
                isImageFileExists = this.isAlreadyChecked = true;

                // Call the current function recursively to load image on first time.
                returnFileName = this.GetRelativeImageUrl(imageSize, imageFileName, cropImage);
            }

            // Reset the recursion check to default value.
            this.isAlreadyChecked = false;

            return ZNodeStorageManager.HttpPath(returnFileName);
        }

        /// <summary>
        /// Create a Directory here.
        /// </summary>
        /// <param name="path">Directory name to create.</param>
        private void CreateDirectory(string path)
        {
            if (!System.IO.Directory.Exists(path))
            {
                System.IO.Directory.CreateDirectory(path);
            }
        }
        #endregion
    }
}
