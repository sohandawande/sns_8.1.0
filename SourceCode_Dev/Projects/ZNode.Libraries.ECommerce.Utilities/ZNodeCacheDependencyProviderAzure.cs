using System;
using System.Configuration;
using System.Configuration.Provider;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.StorageClient;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Utilities
{
    public class ZNodeCacheDependencyProviderAzure :ZNodeCacheDependencyProvider
    {
        #region Public Methods

        /// <summary>
        /// Initialize the Provider base.
        /// </summary>
        /// <param name="name">The friendly name of the provider.</param>
        /// <param name="config">A collection of the name/value pairs representing the provider-specific attributes specified in the configuration for this provider.</param>
        public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config)
        {
            if ((config == null) || (config.Count == 0))
            {
                throw new ArgumentNullException("You must supply a valid configuration dictionary.");
            }

            if (string.IsNullOrEmpty(config["description"]))
            {
                config.Remove("description");
                config.Add("description", "Default File storage provider ");
            }

            // Let ProviderBase perform the basic initialization
            base.Initialize(name, config);

            // Check to see if any attributes were set in configuration that does not need for file writing.
            if (config.Count > 0)
            {
                string extraAttribute = config.GetKey(0);
                if (!String.IsNullOrEmpty(extraAttribute))
                {
                    throw new ProviderException("The following unrecognized attribute was found in " + Name + "'s configuration: '" +
                                                extraAttribute + "'");
                }
                else
                {
                    throw new ProviderException("An unrecognized attribute was found in the provider's configuration.");
                }
            }

        }


        /// <summary>
        /// Inserts the cache for the given key with the object value
        /// </summary>
        /// <param name="key">Cache key value</param>
        /// <param name="objValue">Cache object value</param>
        /// <param name="tableNames">Table Name</param>
        public override void Insert(string key, object objValue, params string[] tableNames)
        {
            try
            {
                Insert(key, objValue, DateTime.Now.AddMinutes(1), TimeSpan.Zero, tableNames);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Inserts the cache for the given key with the object value
        /// </summary>
        /// <param name="key">Cache key value</param>
        /// <param name="objValue">Cache object value</param>
        /// <param name="absoluteExpiration">Absoloute Date time of the expiration time.</param>
        /// <param name="timeSpan">Amount of time before cache expires.</param>
        /// <param name="tableNames">Table Name</param>
        public override void Insert(string key, object objValue, DateTime absoluteExpiration, TimeSpan timeSpan, params string[] tableNames)
        {
            try
            {
                System.Web.HttpRuntime.Cache.Insert(key, objValue, null, absoluteExpiration, timeSpan);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Clears the cache
        /// </summary>
        public override void Remove(string key)
        {
            try
            {
                System.Web.HttpContext.Current.Cache.Remove(key);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion
    }
}
