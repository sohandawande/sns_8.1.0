using System;
using System.Configuration;
using System.Configuration.Provider;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Utilities
{
    public class ZNodeStorageProviderFile : ZNodeStorageProviderBase
    {
        #region Public Methods

        /// <summary>
        /// Initialize the Provider base.
        /// </summary>
        /// <param name="name">The friendly name of the provider.</param>
        /// <param name="config">A collection of the name/value pairs representing the provider-specific attributes specified in the configuration for this provider.</param>
        public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config)
        {
            if ((config == null) || (config.Count == 0))
            {
                throw new ArgumentNullException("You must supply a valid configuration dictionary.");
            }

            if (string.IsNullOrEmpty(config["description"]))
            {
                config.Remove("description");
                config.Add("description", "Default File storage provider ");
            }

            // Let ProviderBase perform the basic initialization
            base.Initialize(name, config);            

            // Check to see if any attributes were set in configuration that does not need for file writing.
            if (config.Count > 0)
            {
                string extraAttribute = config.GetKey(0);
                if (!String.IsNullOrEmpty(extraAttribute))
                {
                    throw new ProviderException("The following unrecognized attribute was found in " + Name + "'s configuration: '" +
                                                extraAttribute + "'");
                }
                else
                {
                    throw new ProviderException("An unrecognized attribute was found in the provider's configuration.");
                }
            }
        }

        /// <summary>
        /// Reads binary file (such as Images) from persistant storage.
        /// </summary>
        /// <param name="filePath">Specify the relative path of the file.</param>
        /// <returns>Returns the binary content of the file.</returns>
        public override byte[] ReadBinaryStorage(string filePath)
        {
            try
            {
                return System.IO.File.ReadAllBytes(System.Web.HttpContext.Current.Server.MapPath(filePath));
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Writes binary file (such as Images) to persistant storage.
        /// </summary>
        /// <param name="fileData">Specify the binrary file content.</param>
        /// <param name="filePath">Specify the relative file path.</param>
        public override void WriteBinaryStorage(byte[] fileData, string filePath)
        {
            try
            {
                // Create directory if not exists.
                System.IO.FileInfo fileInfo = new System.IO.FileInfo(System.Web.HttpContext.Current.Server.MapPath(filePath));
                if (!fileInfo.Directory.Exists)
                {
                    fileInfo.Directory.Create();
                }

                // Write the binary content.
                System.IO.File.WriteAllBytes(System.Web.HttpContext.Current.Server.MapPath(filePath), fileData);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Reads text file from persistant storage.
        /// </summary>
        /// <param name="filePath">Specify the relative file path.</param>
        /// <returns>returns the text content of the file.</returns>
        public override string ReadTextStorage(string filePath)
        {
            try
            { 
                return System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath(filePath));
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Writes text file to persistant storage.
        /// </summary>
        /// <param name="fileData">Specify the string that has the file content.</param>
        /// <param name="filePath">Specify the relative file path.</param>
        /// <param name="fileMode">Specify the file write mode operatation. </param>
        public override void WriteTextStorage(string fileData, string filePath, Mode fileMode)
        {
            try
            {
                // Create directory if not exists.
                System.IO.FileInfo fileInfo = new System.IO.FileInfo(System.Web.HttpContext.Current.Server.MapPath(filePath));
                if (!fileInfo.Directory.Exists)
                {
                    fileInfo.Directory.Create();
                }

                // Check file write mode and write content.
                if (fileMode == Mode.Append)
                {
                    System.IO.File.AppendAllText(System.Web.HttpContext.Current.Server.MapPath(filePath), fileData);
                }
                else
                {
                    System.IO.File.WriteAllText(System.Web.HttpContext.Current.Server.MapPath(filePath), fileData);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Deletes the file from persistant storage.
        /// </summary>
        /// <param name="filePath">Specify the relative file path.</param>
        public override bool DeleteStorage(string filePath)
        {
            try
            {
                if (System.IO.File.Exists(System.Web.HttpContext.Current.Server.MapPath(filePath)))
                {
                    System.IO.File.Delete(System.Web.HttpContext.Current.Server.MapPath(filePath));
                    return true;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Check file exists in persistant storage.
        /// </summary>
        /// <param name="filePath">Specify the relative file path.</param>
        /// <returns>Returns boolean indicating the file exists (TRUE) or not (FALSE).</returns>
        public override bool Exists(string filePath)
        {
            try
            {
                return System.IO.File.Exists(System.Web.HttpContext.Current.Server.MapPath(filePath));
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get HttpPath of file from file storage.
        /// </summary>
        /// <param name="filePath">Specify the relative file path.</param>
        /// <returns>Returns HttpPath of file.</returns>
        public override string HttpPath(string filePath)
        {
            return filePath;
        }

        #endregion
    }
}
