using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Mail;
using System.Net;

namespace ZNode.Libraries.Framework.Business
{
    /// <summary>
    /// This class inherits from the ZNodeEmailBase class. 
    /// </summary>
    public class ZNodeEmail : ZNodeEmailBase
    {
        // If you wish to implement your own email functionality uncomment the code below to 
        // override the base function.

        // Note that you may find the following properties in the base class usefull.
        // ZNodeEmailBase.SMTPPassword;
        // ZNodeEmailBase.SMTPUserName;
        // ZNodeEmailBase.SMTPServer;
        // ZNodeEmailBase.SMTPPort;

        ///// <summary>
        ///// Sends email using SMTP, Uses default network credentials
        ///// </summary>
        ///// <param name="To">Email address of the recipient.</param>
        ///// <param name="From">Email address of the sender.</param>
        ///// <param name="BCC">Blind carbon copy email address.</param>
        ///// <param name="Subject">The subject line of the email.</param>
        ///// <param name="Body">The body of the email.</param>
        ///// <param name="IsBodyHtml">Set to True to send this email in HTML format.</param>
        //public static new void SendEmail(string To, string From, string BCC, string Subject, string Body, bool IsBodyHtml)
        //{
        //}

        /// <summary>
        /// Sends email using SMTP, Uses default network credentials(Embed inline image)
        /// </summary>
        /// <param name="To">Email address of the recipient.</param>
        /// <param name="From">Email address of the sender.</param>
        /// <param name="BCC">Blind carbon copy email address.</param>
        /// <param name="Subject">The subject line of the email.</param>
        /// <param name="Body">The body of the email.</param>
        /// <param name="IsBodyHtml">Set to True to send this email in HTML format.</param>
        public static void SendEmail(string To, string From, string BCC, string Subject, AlternateView av, bool IsBodyHtml)
        {
            //create mail message
            MailMessage message = new MailMessage(From, To);
            if (BCC.Length > 0)
            {
                message.Bcc.Add(BCC);
            }

            message.IsBodyHtml = IsBodyHtml;
            message.Subject = Subject;
            message.AlternateViews.Add(av);

            //create mail client and send email
            SmtpClient emailClient = new SmtpClient();

            emailClient.Host = SMTPServer;
            emailClient.Port = SMTPPort;
            emailClient.Credentials = new NetworkCredential(SMTPUserName, SMTPPassword);

            //emailClient.UseDefaultCredentials = true;
            emailClient.Send(message);
        }

        public static void SendEmail(string To, string From, string BCC, string Subject, string Body, AlternateView av, bool IsBodyHtml)
        {
            //create mail message
            MailMessage message = new MailMessage(From, To);
            if (BCC.Length > 0)
            {
                message.Bcc.Add(BCC);
            }

            message.IsBodyHtml = IsBodyHtml;
            message.Subject = Subject;
            message.Body = Body;
            message.AlternateViews.Add(av);

            //create mail client and send email
            SmtpClient emailClient = new SmtpClient();

            emailClient.Host = SMTPServer;
            emailClient.Port = SMTPPort;
            emailClient.Credentials = new NetworkCredential(SMTPUserName, SMTPPassword);

            //emailClient.UseDefaultCredentials = true;
            emailClient.Send(message);
        }

    }

}
