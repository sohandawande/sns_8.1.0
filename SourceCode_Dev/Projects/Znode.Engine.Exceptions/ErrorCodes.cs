﻿using System;

namespace Znode.Engine.Exceptions
{
	public static class ErrorCodes
	{
		public const Int32 ProfileNotPresent = 1000;
		public const Int32 MembershipError = 1001;
		public const Int32 UserNameUnavailable = 1002;
	    public const Int32 LoginFailed = 1003;
        #region Znode 7.2.2 Reset Password Link
        //Error Code for Account gets Locked
        public const Int32 AccountLocked = 1004;
       
        //Error Codes for Reset Password Links
        public const Int32 ResetPasswordContinue = 2001;
        public const Int32 ResetPasswordLinkExpired = 2002;
        public const Int32 ResetPasswordTokenMismatch = 2003;
        public const Int32 ResetPasswordNoRecord = 2004;

        public const Int32 SEOUrlAlreadExists = 2005;

        public const Int32 DefaultBillingAddressNoSet = 3001;
        public const Int32 DefaultShippingNoSet = 3002;
        public const Int32 NoShippingFacility = 3003;

        public const Int32 IsAssociatedSupplier = 4001;
        #endregion

        public const Int32 CustomerAccountError = 1005;
        public const Int32 ExportError = 1006;
        public const Int32 ImportError = 1007;
        public const Int32 ZnodeEncryptionError = 5001;
    }
}
