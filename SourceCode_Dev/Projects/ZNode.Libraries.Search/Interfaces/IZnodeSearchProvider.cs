﻿using System;
using System.Collections.Generic;
using ZNode.Libraries.Search.LuceneSearchProvider.Search;
 

namespace ZNode.Libraries.Search.Interfaces
{
    public interface IZnodeSearchProvider
    {
        IZNodeSearchResponse Search(IZNodeSearchRequest request);
        List<TypeAheadResponse> SuggestTermsFor(string term, string category, int portalId, bool externalIdNullCheck);
    }
}
