﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZNode.Libraries.Search.LuceneSearchProvider.LuceneExtensionMethods
{
    public static class LuceneExtensions
    {
        public static string ToLuceneFieldString(this object obj)
        {
            return obj.ToString().Replace(" ", string.Empty);
        }

       public static string ToLuceneValueString(this object obj)
        {
            return obj.ToString().Replace("!", string.Empty).Replace("$", string.Empty);
        }

        public static string ToLuceneCategoryString(this object obj)
        {
            return obj.ToString().ToLower().Replace(" ", "_");
        }

        public static string ToCategoryNameString(this string obj)
        {
           var temp = obj.Split(new char[] {'/','|'});

            return temp[temp.Length - 1];
           
        }

        public static T FirstOrDefaultFromMany<T>(
            this IEnumerable<T> source, Func<T, IEnumerable<T>> childrenSelector,
            Predicate<T> condition)
        {
            // return default if no items
            if (source == null || !source.Any()) return default(T);

            // return result if found and stop traversing hierarchy
            var attempt = source.FirstOrDefault(t => condition(t));
            if (!Equals(attempt, default(T))) return attempt;

            // recursively call this function on lower levels of the
            // hierarchy until a match is found or the hierarchy is exhausted
            return source.SelectMany(childrenSelector)
                .FirstOrDefaultFromMany(childrenSelector, condition);
        }
    }
}
