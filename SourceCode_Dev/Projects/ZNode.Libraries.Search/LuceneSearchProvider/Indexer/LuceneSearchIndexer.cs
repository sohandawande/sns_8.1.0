﻿using System;
using System.Collections;
using System.Collections.Generic;

using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using Lucene.Net.Analysis;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Documents;
using Lucene.Net.Index;
using Lucene.Net.Store;
using ZNode.Libraries.Search.LuceneSearchProvider.Analyzers;
using ZNode.Libraries.Search.LuceneSearchProvider.LuceneExtensionMethods;
using ZNode.Libraries.Search.LuceneSearchProvider.Indexer.Model;
using ZNode.Libraries.Search.LuceneSearchProvider.Search;
using Version = Lucene.Net.Util.Version;
using ZNode.Libraries.Search.LuceneSearchProvider.Indexer.Helper;
using ZNode.Libraries.Search.LuceneSearchProvider.Indexer.Services;

namespace ZNode.Libraries.Search.LuceneSearchProvider.Indexer
{
    public class LuceneSearchIndexer
    {
        public ILuceneCategoryService CategoryService { get; set; }
        public ILuceneDocMappingService DocumentMappingService { get; set; }
        public string IndexLocation { get; set; }
        public int PageLength { get; set; }
        public ILuceneProductService ProductService { get; set; }

        public LuceneSearchIndexer(ILuceneProductService productService, ILuceneDocMappingService documentMappingService, ILuceneCategoryService categoreyService, string indexLocation, int pageLength = 1000)
        {
            CategoryService = categoreyService;
            DocumentMappingService = documentMappingService;
            IndexLocation = indexLocation;
            PageLength = pageLength;
            ProductService = productService;
        }

        public void AddCategoryFacetDocuments(List<LuceneCategoryFacet> categoryFacets, IndexWriter indexWriter)
        {

             foreach (var categoryFacet in categoryFacets)
                {
                    var document = new Document();
                    document.Add(new Field("CategoryName", categoryFacet.CategoryName, Field.Store.YES, Field.Index.NOT_ANALYZED));
                    document.Add(new Field("CategoryID", categoryFacet.CategoryId.ToString(), Field.Store.YES, Field.Index.ANALYZED));
                    document.Add(new Field("PortalCatalogID", categoryFacet.PortalCatalogID.ToString(), Field.Store.YES, Field.Index.NOT_ANALYZED));
                    document.Add(new Field("CategoryFacets", string.Join(",", categoryFacet.FacetNames), Field.Store.YES, Field.Index.NO));

                    if (categoryFacet.ParentCategory != null && categoryFacet.ParentCategory.Any() && categoryFacet.ParentCategory[0] != null)
                    {
                        categoryFacet.ParentCategory.ForEach(x =>
                            document.Add(new Field("ParentCategory", string.Join("/", x.CategoryName.Split('/').Skip(1)), Field.Store.YES, Field.Index.NOT_ANALYZED)));
                    }

                    if (categoryFacet.SubCategory.Any() && categoryFacet.SubCategory[0] != null)
                    {
                        categoryFacet.SubCategory.ForEach(x =>
                            document.Add(new Field("SubCategory", string.Format("{0}/{1}|{2}", categoryFacet.CategoryName, x.CategoryName, x.Title), Field.Store.YES, Field.Index.NOT_ANALYZED))
                        );
                    }

                    indexWriter.AddDocument(document);
                }
          
        }

        public void AddProductDocuments(List<LuceneProduct> products,IndexWriter indexWriter)
        {
                var documentMapping = DocumentMappingService.GetDocumentMappings();

                foreach (var product in products)
                {
                    var document = new Document();

                    foreach (var mapping in documentMapping)
                    {
                        var productPropertyValue = ReflectionHelper.GetPropertyValueByString(product, mapping.DocumentName, mapping.PropertyName, mapping.IsBoosted, mapping.IsFaceted);
                        //Prevent duplicate values since that will change scoring
                        var insertedFieldValueList = new Dictionary<string, string>();

                        if (productPropertyValue != null)
                        {
                            if (productPropertyValue is ArrayList)
                            {
                                var productPropertyList = (ArrayList)productPropertyValue;

                                MapDocumentArrayList(document, mapping, product, productPropertyList, mapping.IsBoosted, insertedFieldValueList);
                            }
                            else
                            {
                                MapDocumentField(document, mapping, product, productPropertyValue, mapping.IsBoosted, insertedFieldValueList);
                            }
                        }

                    }
                    document.Boost = (float)product.Boost;

                    indexWriter.AddDocument(document);
                }
        }

        public void UpdateCategoryFacetDocuments(List<LuceneCategoryFacet> categoryFacets, IndexWriter indexWriter)
        {
            
               // var indexWriter = LuceneSearchManager.GetIndexWriter(IndexLocation, false);

                foreach (var categoryFacet in categoryFacets)
                {
                    var document = new Document();

                    document.Add(new Field("CategoryName", categoryFacet.CategoryName, Field.Store.YES, Field.Index.NOT_ANALYZED));
                    document.Add(new Field("CategoryID", categoryFacet.CategoryId.ToString(), Field.Store.YES, Field.Index.ANALYZED));
                    document.Add(new Field("PortalCatalogID", categoryFacet.PortalCatalogID.ToString(), Field.Store.YES, Field.Index.NOT_ANALYZED));
                    document.Add(new Field("CategoryFacets", string.Join(",", categoryFacet.FacetNames), Field.Store.YES, Field.Index.NO));

                    if (categoryFacet.ParentCategory != null &&  categoryFacet.ParentCategory.Any())
                    {
                        categoryFacet.ParentCategory.ForEach(x =>
                            document.Add(new Field("ParentCategory", string.Join("/", x.CategoryName.Split('/').Skip(1)), Field.Store.YES, Field.Index.NOT_ANALYZED)));
                    }

                    if (categoryFacet.SubCategory != null && categoryFacet.SubCategory.Any())
                    {
                        categoryFacet.SubCategory.ForEach(x =>
                            document.Add(new Field("SubCategory", string.Format("{0}/{1}|{2}", categoryFacet.CategoryName, x.CategoryName, x.Title), Field.Store.YES, Field.Index.NOT_ANALYZED))
                        );
                    }                    

                    Term updateTerm = new Term("CategoryID", document.GetFieldable("CategoryID").StringValue);

                    indexWriter.UpdateDocument(updateTerm, document);

                }

                //indexWriter.Commit();
                //indexWriter.Dispose();
            
            
           // return true;
        }

        public void UpdateProductDocuments(List<LuceneProduct> products, IndexWriter indexWriter)
        {
                var documentMapping = DocumentMappingService.GetDocumentMappings();

                foreach (var product in products)
                {
                    var document = new Document();

                    foreach (var mapping in documentMapping)
                    {
                        var productPropertyValue = ReflectionHelper.GetPropertyValueByString(product, mapping.DocumentName, mapping.PropertyName, mapping.IsBoosted, mapping.IsFaceted);
                        //Prevent duplicate values since that will change scoring
                        var insertedFieldValueList = new Dictionary<string, string>();

                        if (productPropertyValue != null)
                        {
                            if (productPropertyValue is ArrayList)
                            {
                                var productPropertyList = (ArrayList)productPropertyValue;

                                MapDocumentArrayList(document, mapping, product, productPropertyList, mapping.IsBoosted, insertedFieldValueList);
                            }
                            else
                            {
                                MapDocumentField(document, mapping, product, productPropertyValue, mapping.IsBoosted, insertedFieldValueList);
                            }
                        }

                    }
                    document.Boost = (float)product.Boost;

                    Term updateTerm = new Term("ID", document.GetFieldable("ID").StringValue);

                    indexWriter.UpdateDocument(updateTerm, document);

                }
        }

        public void DeleteProductDocuments(List<LuceneProduct> products, IndexWriter indexWriter)
        {
            foreach (var product in products)
            {
                Term deleteTerm = new Term("ID", product.ProductId.ToString());
                indexWriter.DeleteDocuments(deleteTerm);
            }

        }

        public void DeleteCategoryFacetDocuments(List<LuceneCategoryFacet> categoryFacets, IndexWriter indexWriter)
        {
            foreach (var item in categoryFacets)
            {
                Term deleteTerm = new Term("CategoryID", item.CategoryId.ToString());
                indexWriter.DeleteDocuments(deleteTerm);
            }
        }

        public void CreateIndex(IndexWriter indexWriter)
        {
            int start = 0;
            int totalCount = 0;
            var products = new List<LuceneProduct>();
            // var result = true;
            // Get the Index writer 
            
            do
            {
                var pagedProducts = ProductService.GetAllProducts(start, PageLength, out totalCount);
                products = products.Union(pagedProducts).ToList();
                start ++;
            } while (start * PageLength < totalCount);

            // Get the parent child table
            products = ProductService.FilterParentChildProducts(products);
            
            AddProductDocuments(products, indexWriter);
       
            start = 0;
            totalCount = 0;
            do
            {
                var categoryFacets = CategoryService.GetAllCategoryFacets(start,PageLength, out totalCount);
                AddCategoryFacetDocuments(categoryFacets, indexWriter);
                start++;
            } while (start * PageLength < totalCount);

            //indexWriter.Optimize();
            //indexWriter.Dispose();

            //// Reopen the Index file once update to Index file is completed 
            //LuceneSearchManager.ReopenIndexReader();

        }

        public void MapDocumentArrayList(Document document, LuceneDocMapping documentMapping,
                                    LuceneProduct product, ArrayList arrayList, bool isBoosted, Dictionary<string, string> insertedFieldValueList)
        {
            foreach (var arrayItem in arrayList)
            {
                if (arrayItem is ArrayList)
                {
                    MapDocumentArrayList(document, documentMapping, product, (ArrayList)arrayItem, isBoosted, insertedFieldValueList);
                }
                else
                {
                    if (arrayItem != null)
                    {
                        MapDocumentField(document, documentMapping, product, arrayItem, isBoosted, insertedFieldValueList);
                    }
                }
            }
        }

        public void MapDocumentField(Document document, LuceneDocMapping documentMapping,
                                            LuceneProduct product, object productPropertyValue, bool isBoosted, Dictionary<string, string> insertedFieldValueList)
        {
            var reflectionObject = (LuceneReflectionObject)productPropertyValue;
            if (reflectionObject != null && reflectionObject.PropertyValue != null)
            {
                Field field = null;
                if (documentMapping.IsFaceted)
                {
                    field = new Field(reflectionObject.PropertyName.ToString().Replace(" ", "_"), reflectionObject.PropertyValue.ToLuceneValueString(),
                        LuceneHelper.GetMappingStore(documentMapping.IsStored),
                        Field.Index.NOT_ANALYZED);
                }
                else if (reflectionObject.PropertyName.ToLuceneFieldString().ToLower() == "productname")
                {
					field = new Field(reflectionObject.PropertyName.ToLuceneFieldString(), reflectionObject.PropertyValue.ToLuceneFieldString(),
                    LuceneHelper.GetMappingStore(documentMapping.IsStored),
                    LuceneHelper.GetMappingIndex(documentMapping.IsIndexed));
                }
                else if (reflectionObject.PropertyName.ToLuceneFieldString().ToLower() == "category")
                {
                    field = new Field(string.Format(reflectionObject.PropertyName.ToLuceneFieldString() + "_" + reflectionObject.PropertyValue.ToLuceneCategoryString()), reflectionObject.PropertyValue.ToLuceneValueString(),
                    LuceneHelper.GetMappingStore(documentMapping.IsStored),
                    LuceneHelper.GetMappingIndex(documentMapping.IsIndexed));
                }

                else if (reflectionObject.PropertyName.ToLuceneFieldString().ToLower() == "externalid")
                {
                    var temp = reflectionObject.PropertyValue.ToLuceneValueString();
                    var value = (string.IsNullOrEmpty(temp)) ? "_null_" : temp;

                    field = new Field(reflectionObject.PropertyName.ToLuceneFieldString(), value,
                    LuceneHelper.GetMappingStore(documentMapping.IsStored),
                     Field.Index.NOT_ANALYZED);
                }
                else
                {
                    var propertyName = documentMapping.IsBoosted
                                           ? string.Format("{0}_{1}",
                                                           reflectionObject.PropertyName.ToLuceneFieldString(),
                                                           reflectionObject.PropertyValue.ToLuceneValueString()
                                                                           .Replace(" ", "_"))
                                           : reflectionObject.PropertyName.ToLuceneFieldString();

                    field = new Field(propertyName, reflectionObject.PropertyValue.ToLuceneValueString(),
                    LuceneHelper.GetMappingStore(documentMapping.IsStored),
                    LuceneHelper.GetMappingIndex(documentMapping.IsIndexed));
                }

                var boost = (float)Convert.ToDouble(reflectionObject.Boost);

                field.Boost = boost * (float)documentMapping.Boost;

                try
                {

                    insertedFieldValueList.Add(field.Name + field.StringValue, field.Name);
                    document.Add(field);
                }
                //Duplicate field ignore
                catch (ArgumentException)
                {

                }
            }
        }


      

        
    }
}