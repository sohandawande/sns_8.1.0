﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZNode.Libraries.Search.LuceneSearchProvider.Indexer.Model
{
    public class LuceneDocMapping
    {
        public int SearchDocumentMappingId { get; set; }
        public double Boost { get; set; }
        public string PropertyName { get; set; }
        public string DocumentName { get; set; }
        public bool IsBoosted { get; set; }
        public bool IsFaceted { get; set; }
        public bool IsIndexed { get; set; }
        public bool IsStored { get; set; }
    }
}
