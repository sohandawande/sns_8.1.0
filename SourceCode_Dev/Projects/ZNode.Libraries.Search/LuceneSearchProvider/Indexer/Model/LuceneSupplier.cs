﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZNode.Libraries.Search.LuceneSearchProvider.Indexer.Model
{
    public class LuceneSupplier
    {
        public int? SupplierId { get; set; }
        public string SupplierName { get; set; }
    }
}
