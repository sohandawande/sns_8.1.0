﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZNode.Libraries.Search.LuceneSearchProvider.Indexer.Model
{
    public class LuceneIndexChange
    {
        public int CatalogMonitorId { get; set; }
        public int SourceId { get; set; }
        public string SourceType { get; set; }
        public string SourceTransactionType { get; set; }
        public DateTime TransactionDateTime { get; set; }

        public LuceneIndexChange()
        {
            
        }
    }
}
