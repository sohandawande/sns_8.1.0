﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZNode.Libraries.Search.LuceneSearchProvider.Indexer.Model
{
    public class LuceneCategory
    {
        public int CategoryId { get; set; }
        public double Boost { get; set; }
        public string CategoryName { get; set; }
        public string Title { get; set; }
        public List<LuceneCategoryNode> CategoryNodes { get; set; }

        public LuceneCategory()
        {
            Boost = 1.0;
            CategoryNodes = new List<LuceneCategoryNode>();
        }

        public List<LuceneCategory> Hierarchy
        {
            get
            {
                if (CategoryNodes.Any())
                {
                    return GetHierarchy(CategoryNodes).ToList();
                }
                return null;
            }
        }

        private IEnumerable<LuceneCategory> GetHierarchy(List<LuceneCategoryNode> categoryNodes)
        {
            if (categoryNodes.Any())
            {
                foreach (var node in categoryNodes)
                {
                    foreach (var portalCatalog in node.Catalog.PortalCatalogs)
                    {
                        yield return new LuceneCategory() { CategoryName = GetHierarchy(node, 0, portalCatalog.PortalCatalogId) };
                    }
                }
            }
        }

        private string GetHierarchy(LuceneCategoryNode categoryNode, int hierarchy, int portalCatalogId)
        {
            if (!Equals(categoryNode, null) && categoryNode.ParentCategoryNodeId.HasValue)
            {
                if (!Equals(categoryNode.ParentCategory, null) && !Equals(categoryNode.Category, null))
                {
                    return string.Format("{0}/{1}|{2}", GetHierarchy(categoryNode.ParentCategory, hierarchy + 1, portalCatalogId), categoryNode.Category.CategoryName, categoryNode.Category.Title);
                }
                else
                {
                    return string.Empty;
                }
            }
            else
            {
                return string.Format("{0}/{1}/{2}|{3}", portalCatalogId, hierarchy, categoryNode.Category.CategoryName, categoryNode.Category.Title);
            }
        }
    }
}
