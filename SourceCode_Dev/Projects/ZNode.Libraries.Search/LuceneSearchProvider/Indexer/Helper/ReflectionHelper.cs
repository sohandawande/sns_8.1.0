﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

using ZNode.Libraries.Search.LuceneSearchProvider.Indexer.Model;

namespace ZNode.Libraries.Search.LuceneSearchProvider.Indexer.Helper
{
    public static class ReflectionHelper
    {
        public static object GetPropertyValueByString(object objectToSearch, string propertyDocumentName,string propertyName, bool isBoosted, bool isFaceted)
        {
            if (propertyName.Contains("."))
            {
                var splitProperties = propertyName.Split('.');
                var objectChildToSearch = objectToSearch.GetType().GetProperty(splitProperties[0]).GetValue(objectToSearch,null);
                if (objectChildToSearch is IList)
                {
                    var objectToSearchList = (IList)objectChildToSearch;
                    if (objectToSearchList.Count > 0)
                    {
                        IList newObjectToSearch = new ArrayList();
                        foreach (var objectItem in objectToSearchList)
                        {
                            newObjectToSearch.Add(GetPropertyValueByString(objectItem, propertyDocumentName, propertyName.Substring(propertyName.IndexOf('.') + 1), isBoosted, isFaceted));
                        }
                        return newObjectToSearch;
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    if (objectChildToSearch == null)
                    {
                        // Do we want to log this? Could help when there are problems
                        //throw new NullReferenceException(string.Format("Unable to find object property by property name: {0}",propertyName));
                        return null;
                    }
                    else
                    {
                        return GetPropertyValueByString(objectChildToSearch, propertyDocumentName, propertyName.Substring(propertyName.IndexOf('.') + 1), isBoosted, isFaceted);
                    }
                }

            }
            else
            {
                if (objectToSearch is IList)
                {
                    var objectToSearchList = (IList)objectToSearch;
                    var objectValueList = new ArrayList();

                    foreach (var objectValue in objectToSearchList)
                    {
                        if (isBoosted)
                        {
                            objectValueList.Add(new LuceneReflectionObject
                                {
                                    Boost = objectToSearch.GetType().GetProperty("Boost").GetValue(objectToSearch,null),
                                    PropertyName = (isFaceted) ? objectToSearch.GetType().GetProperty("FacetName").GetValue(objectToSearch,null) : propertyDocumentName,
                                    PropertyValue =
                                        objectToSearch.GetType().GetProperty(propertyName).GetValue(objectToSearch,null)
                                });
                        }
                        else
                        {
                            objectValueList.Add(objectValue.GetType().GetProperty(propertyName).GetValue(objectValue,null));
                        }

                    }
                    return objectValueList;
                }
                else
                {
                    if ((isFaceted) && objectToSearch.GetType().GetProperty("FacetName") == null)
                    {
                        return new LuceneReflectionObject
                        {
                            Boost = (isBoosted) ? objectToSearch.GetType().GetProperty("Boost").GetValue(objectToSearch, null) : 1.0,
                            PropertyName = propertyDocumentName,
                            PropertyValue = objectToSearch.GetType().GetProperty(propertyName).GetValue(objectToSearch, null)
                        };
                    }

                    if (isBoosted)
                    {
                        return new LuceneReflectionObject
                            {
                                Boost = objectToSearch.GetType().GetProperty("Boost").GetValue(objectToSearch,null),
                                PropertyName = (isFaceted) ? objectToSearch.GetType().GetProperty("FacetName").GetValue(objectToSearch,null) : propertyDocumentName,
                                PropertyValue =
                                    objectToSearch.GetType().GetProperty(propertyName).GetValue(objectToSearch,null)
                            };
                    }
                    else
                    {
                        return new LuceneReflectionObject
                            {
                               Boost = 1.0,
                               PropertyName = (isFaceted) ? objectToSearch.GetType().GetProperty("FacetName").GetValue(objectToSearch,null) : propertyDocumentName,
                               PropertyValue = objectToSearch.GetType().GetProperty(propertyName).GetValue(objectToSearch,null)
                            };
                    }
                }
            }
        }
    }
}
