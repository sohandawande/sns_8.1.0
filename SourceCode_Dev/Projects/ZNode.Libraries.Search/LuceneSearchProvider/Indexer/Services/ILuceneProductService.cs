﻿using System.Collections.Generic;
using System.Linq;


using ZNode.Libraries.Search.LuceneSearchProvider.Indexer.Model;

namespace ZNode.Libraries.Search.LuceneSearchProvider.Indexer.Services
{
    public interface ILuceneProductService
    {
        List<LuceneProduct> GetAllProducts(int start, int pageLength, out int totalCount);
        LuceneProduct GetProductByProductId(int productId);
        List<LuceneProduct> FilterParentChildProducts(List<LuceneProduct> products);
    }
}