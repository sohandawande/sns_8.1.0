﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;

using ZNode.Libraries.Search.LuceneSearchProvider.Indexer.Mapping;
using ZNode.Libraries.Search.LuceneSearchProvider.Indexer.Model;

namespace ZNode.Libraries.Search.LuceneSearchProvider.Indexer.Services
{
    public class LuceneProductService : ILuceneProductService
    {
        public void FillProductChildren(LuceneProduct product)
        {
            product.Boost = GetProductBoost(product.ProductId);
			product.SKUs = GetSkus(product.ProductId);
            product.Facets = GetProductFacets(product.ProductId, product.SKUs);
            product.ProductCategories = GetProductCategories(product.ProductId);
			product.Tags = GetProductTags(product.ProductId);

            foreach (var productCategorey in product.ProductCategories)
            {
                if (productCategorey.Category != null)
                {
                    productCategorey.Category.Boost = GetProductCategoryBoost(productCategorey.ProductCategoryId);
                }
            }
            product.ProductSubCategories = (from productCategory in product.ProductCategories
                                            where
                                                productCategory.Category != null && productCategory.Category.CategoryNodes.Any(
                                                    cn => cn.ParentCategoryNodeId != null)
                                            select productCategory.Category).ToList();
            product.Reviews = GetProductReviews(product.ProductId);
            
        }

        public List<LuceneProduct> GetAllProducts(int start, int pageLength, out int totalCount)
        {
            ProductService productService = new ProductService();

            var ntProducts = productService.GetPaged("[ActiveInd] = 1 and [ReviewStateID] = 20", string.Empty, start, pageLength, out totalCount);
			productService.DeepLoad(ntProducts, true, DeepLoadType.IncludeChildren, new Type[] { typeof(Manufacturer), typeof(Supplier), typeof(LuceneGlobalProductBoost)});

            var products = new List<LuceneProduct>(); 
            foreach (var ntProduct in ntProducts)
            {   
                products.Add(NetTiersToLuceneMapper.LuceneProduct(ntProduct));
            }

            products.ForEach(FillProductChildren);

            products = products.Where(x => x.ProductCategories.Any(y => y.Category != null && y.Category.CategoryNodes.Any(z => z.Catalog != null && z.Catalog.PortalCatalogs.Any(v => v.Portal != null)))).ToList();

            return products;
        }

        public List<LuceneProduct> FilterParentChildProducts(List<LuceneProduct> products)
        {
            var parentChildProductService = new ParentChildProductService();
            
            var parentChildProduct = parentChildProductService.GetAll();

            foreach (var ntparentChildProduct in parentChildProduct)
            {   
                if (products != null && !products.Any(x => x.ProductId == ntparentChildProduct.ChildProductID))
                {
                    var parentProductId =
                        products.FirstOrDefault(x => x.ProductId == ntparentChildProduct.ParentProductID);

                    if (parentProductId != null)
                    {
                        products.Remove(parentProductId);    
                    }
                }
            }

            return products;
        }

        public double GetProductBoost(int productId)
        {
            LuceneGlobalProductBoostService luceneGlobalProductBoostService = new LuceneGlobalProductBoostService();
            var productBoost = luceneGlobalProductBoostService.GetByProductID(productId);

            return productBoost != null ? productBoost.Boost : 1.00;
        }

        public LuceneProduct GetProductByProductId(int productId)
        {
			 var productService = new ProductService();

			 var ntProduct = productService.GetByProductID(productId);
            if (ntProduct != null)
            {
                productService.DeepLoad(ntProduct, true, DeepLoadType.IncludeChildren,
                                        new Type[]
                                            {
                                                typeof (Manufacturer), typeof (Supplier),
                                                typeof (LuceneGlobalProductBoost)
                                            });

                var product = NetTiersToLuceneMapper.LuceneProduct(ntProduct);

                FillProductChildren(product);

                return product;
            }
            return null;
        }


        public double GetProductCategoryBoost(int productCategoryId)
        {
            LuceneGlobalProductCategoryBoostService luceneProductCategoryBoostService = new LuceneGlobalProductCategoryBoostService();
            var productCategoryBoost = luceneProductCategoryBoostService.GetByProductCategoryID(productCategoryId);

            return productCategoryBoost != null ? productCategoryBoost.Boost : 1.00;
        }

        public List<LuceneProductCategory> GetProductCategories(int productId)
        {
            ProductCategoryService productCategoryService = new ProductCategoryService();
            var ntProductCategories = productCategoryService.DeepLoadByProductID(productId, true,
                                                DeepLoadType.IncludeChildren,
                                                new Type[]
                                                    {
                                                        typeof(Category)
                                                    });

            CategoryService categoryService = new CategoryService();
            ntProductCategories.ForEach(
                pc =>
                categoryService.DeepLoad(pc.CategoryIDSource, true, DeepLoadType.IncludeChildren,
                                         new Type[] { typeof(TList<CategoryNode>) }));

            CategoryNodeService categoryNodeService = new CategoryNodeService();
            CatalogService catalogService = new CatalogService();
            PortalCatalogService portalCatalogService = new PortalCatalogService();

            foreach (CategoryNode ntCategoryNode in ntProductCategories.Select(pc => pc.CategoryIDSource).SelectMany(c => c.CategoryNodeCollection))
            {
                categoryNodeService.DeepLoad(ntCategoryNode, true, DeepLoadType.IncludeChildren, new Type[] { typeof(Catalog), typeof(CategoryNode), typeof(Category) });
                catalogService.DeepLoad(ntCategoryNode.CatalogIDSource, true, DeepLoadType.IncludeChildren, new Type[] { typeof(TList<PortalCatalog>) });                
                portalCatalogService.DeepLoad(ntCategoryNode.CatalogIDSource.PortalCatalogCollection, true, DeepLoadType.IncludeChildren, new Type[] { typeof(Portal) });
            }

            return ntProductCategories.Select(pc => NetTiersToLuceneMapper.LuceneProductCategory(pc)).Where(x => (x != null) && (x.ActiveInd == true)).ToList();
        }

        public List<LuceneProductFacet> GetProductFacets(int productId, List<LuceneSKU> skus)
        {
			FacetProductSKUService tagProductSkuService = new FacetProductSKUService();

	        var skuList = skus.SelectMany(x => tagProductSkuService.DeepLoadBySKUID(x.SKUId, true, DeepLoadType.IncludeChildren,
										new Type[] { typeof(Facet), typeof(FacetGroup) }).OrderBy(y => y.FacetIDSource.FacetGroupIDSource.DisplayOrder).ThenBy(z => z.FacetIDSource.FacetDisplayOrder)
	                                                            .Select(NetTiersToLuceneMapper.LuceneProductFacet));

			return tagProductSkuService.DeepLoadByProductID(productId, true, DeepLoadType.IncludeChildren,
										new Type[] { typeof(Facet), typeof(FacetGroup) }).OrderBy(y => y.FacetIDSource.FacetGroupIDSource.DisplayOrder).ThenBy(z => z.FacetIDSource.FacetDisplayOrder).Select(NetTiersToLuceneMapper.LuceneProductFacet)
										.Union(skuList).ToList();
        }

        public List<LuceneProductReview> GetProductReviews(int productId)
        {
            ReviewService reviewService = new ReviewService();
            List<LuceneProductReview> reviews = new List<LuceneProductReview>();

            var reviewslist = reviewService.GetByProductID(productId)
            .Select(r => NetTiersToLuceneMapper.LuceneProductReview(r))
            .ToList();
            if (reviewslist.Any())
            {
                reviews.Insert(0, new LuceneProductReview() { ReviewRating = reviewslist.Average(x => x.ReviewRating), ReviewId = reviewslist.Max(y => y.ReviewId) });
            }
            return reviews;
        }

        public List<LuceneSKU> GetSkus(int productId)
        {
            SKUService skuService = new SKUService();
            return skuService.GetByProductID(productId)
                             .Select(s => NetTiersToLuceneMapper.LuceneSku(s))
                             .ToList();
        }

		public string GetProductTags(int productId)
		{ 
			TagsService tagsService = new TagsService(); 
			var tags = tagsService.GetByProductID(productId).FirstOrDefault();

			if (tags != null)
			{ 
				return tags.Tags.ToString();
			}
			else
			{
				return string.Empty;
			}

		}
    }
}
