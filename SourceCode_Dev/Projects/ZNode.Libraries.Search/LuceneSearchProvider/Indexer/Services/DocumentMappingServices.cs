﻿using System.Collections.Generic;
using System.Linq;
using ZNode.Libraries.DataAccess.EntityFramework.POCO;
using ZNode.Libraries.DataAccess.Repository;

using ZNode.Libraries.Search.LuceneSearchProvider.Indexer.Model;

namespace ZNode.Libraries.Search.LuceneSearchProvider.Indexer.Services
{
    public class DocumentMappingService : IDocumentMappingService
    {
        public List<ZNodeSearchDocMapping> GetDocumentMappings()
        {
            using (var repo = new ZNodeRepository(new ZNodeContext()))
            {
                return (from documentMapping in repo.Get<ZNodeLuceneDocumentMapping>()
                        select new ZNodeSearchDocMapping
                            {
                                SearchDocumentMappingId = documentMapping.LuceneDocumentMappingID,
                                Boost = documentMapping.Boost ?? 1.0F,
                                DocumentName = documentMapping.DocumentName,
                                IsBoosted = documentMapping.IsBoosted,
                                IsFaceted = documentMapping.IsFaceted,
                                IsIndexed = documentMapping.IsIndexed,
                                IsStored = documentMapping.IsStored,
                                PropertyName = documentMapping.PropertyName
                            }).ToList();
            }
        }
    }
}