﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;

using System.Configuration;

using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;

using ZNode.Libraries.Search.LuceneSearchProvider.Indexer.Model;
using ZNode.Libraries.Search.LuceneSearchProvider.Indexer.Mapping;

   

namespace ZNode.Libraries.Search.LuceneSearchProvider.Indexer.Services
{

    /// <summary>
    /// Represents the znode lucene index server status names
    /// </summary>
    public enum ZNodeStatusNames
    {
        /// <summary>
        /// In Process
        /// </summary>
        InProcess = 1,

        /// <summary>
        /// Completed
        /// </summary>
        Complete = 2,

        /// <summary>
        /// Failed
        /// </summary>
        Failed = 3,

        /// <summary>
        /// Ignored
        /// </summary>
        Ignored = 4
    }

    public class LuceneIndexService: ILuceneIndexService
    {
        public List<LuceneIndexMonitor> GetIndexChanges()
        {
            try
            {
                var luceneIndexerStatusService = new LuceneIndexMonitorService();

                var ntIndexChange =
                    luceneIndexerStatusService.GetAll().Where(p=>p.SourceType == "CreateIndex").OrderBy(p => p.TransactionDateTime).Take(10).Select(p => p).ToList();

                return ntIndexChange;
            }
            catch (Exception)
            {

                return null;
            }
           
        }

        public List<LuceneIndexServerStatus> GetIndexServerStatus(int luceneIndexMonitorID)
        {
            try
            {
                LuceneIndexServerStatusService luceneIndexerServerService = new LuceneIndexServerStatusService();

                var ntIndexChange = luceneIndexerServerService.GetAll().Where(p => p.LuceneIndexMonitorID == luceneIndexMonitorID).ToList();

                return ntIndexChange;
            }
            catch (Exception)
            {

                return null;
            }

        }


        public List<LuceneIndexMonitor> GetWinServiceIndexChanges()
        {
            try
            {

                var luceneIndexMonitorService = new LuceneIndexMonitorService();

                var ntWinserviceIndexChange = luceneIndexMonitorService.GetAll().OrderBy(c => c.TransactionDateTime).Take(50).Select(c => c).ToList();

                return ntWinserviceIndexChange;
            }
            catch (Exception)
            {

                return null;
            }
        
        }

       public int IsLuceneTriggersDisabled()
       {

           try
           {
               var luceneSearchHelper = new LuceneSearchHelper();

               var returnInt = luceneSearchHelper.IsLuceneTriggersDisabled();

               return returnInt;
               
           }
           catch (Exception ex)
           {
               
               throw ex;
           }
           
       }
        
       public void SwitchLuceneTrigger(int intFlag)
       {
           var luceneSearchHelper = new LuceneSearchHelper();

           luceneSearchHelper.SwitchForLuceneTriggers(intFlag);
       }

       public void CreateLuceneIndexButton(int intAccountID)
       {
           
           var luceneSearchHelper = new LuceneSearchHelper();
           luceneSearchHelper.CreateLuceneIndex(intAccountID);

       }

        public int IsCreateIndexButtonDisabled()
        {
            try
            {
                var luceneIndexMonitorService = new LuceneIndexMonitorService();
                var ntIndexMonitor = luceneIndexMonitorService.GetAll().OrderByDescending(b => b.TransactionDateTime).Take(1).Select(b => b);
                var cutOffDate = ntIndexMonitor.FirstOrDefault().TransactionDateTime;
                cutOffDate = cutOffDate.AddMinutes(5);
                var result = DateTime.Compare(cutOffDate, System.DateTime.Now);
                if (result > 0)
                    return 1;
                else
                    return 0;
            }
            catch (Exception)
            {

                return 0;
            }
        }

       public void SwitchWindowsService(int intFlag)
       {
           try
           {
               var luceneServerConfigurationStatusService = new LuceneServerConfigurationStatusService();
               var listall = luceneServerConfigurationStatusService.GetAll().Select(c => c).ToList();

               foreach (var luceneIndex in listall)
               {

                   if (intFlag == 1)
                   {
                       luceneIndex.Status = true;

                   }
                   else
                   {
                       luceneIndex.Status = false;
                   }

                   luceneServerConfigurationStatusService.Update(luceneIndex);

                   
                  
               }
               


               
              
           }
           catch (Exception ex)
           {
               Console.WriteLine("Exception Occurred :{0},{1}",
                         ex.Message, ex.StackTrace.ToString());
           }
		   // System.Diagnostics.Process.Start(ConfigurationManager.AppSettings["LuceneServiceLocation"], intFlag.ToString());
       }

        public int IsLucceneWinserviceDisable()
        {
            try
            {
                var luceneServerConfigurationStatusService = new LuceneServerConfigurationStatusService();
                var listall = luceneServerConfigurationStatusService.GetAll().Select(c => c).ToList();
                var test = 0;
                foreach (var luceneIndex in listall)
                {
                    if (luceneIndex.Status == true)
                        test = 1;
                }
             
                //var sc = new ServiceController("Znode Background Service");
                //if (ServiceControllerStatus.Running == sc.Status)
                if (test==1)
                    return 0;
                else
                    return 1;
            }
            catch (Exception)
            {
                return 2;
            }
           
        }


    }
}
