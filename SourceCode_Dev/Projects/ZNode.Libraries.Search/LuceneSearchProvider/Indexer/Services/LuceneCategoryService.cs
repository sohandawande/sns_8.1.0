﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;

using ZNode.Libraries.Search.LuceneSearchProvider.Indexer.Mapping;
using ZNode.Libraries.Search.LuceneSearchProvider.Indexer.Model;

namespace ZNode.Libraries.Search.LuceneSearchProvider.Indexer.Services
{
    public class LuceneCategoryService : ILuceneCategoryService
    {

        public LuceneCategoryFacet GetCategoryFacet(int categoryId)
        {
            CategoryService categoryService = new CategoryService();

            var category = categoryService.GetByCategoryID(categoryId);

            if (category != null)
            {
                CategoryNodeService categoryNodeService = new CategoryNodeService();
                CatalogService catalogService = new CatalogService();
                PortalCatalogService portalCatalogService = new PortalCatalogService();

                categoryService.DeepLoad(category, true, DeepLoadType.IncludeChildren,
                                         new Type[] {typeof (TList<CategoryNode>)});

                foreach (CategoryNode ntCategoryNode in category.CategoryNodeCollection)
                {
                    categoryNodeService.DeepLoad(ntCategoryNode, true, DeepLoadType.IncludeChildren,
                                                 new Type[] {typeof (Catalog), typeof (CategoryNode), typeof (Category)});
                    catalogService.DeepLoad(ntCategoryNode.CatalogIDSource, true, DeepLoadType.IncludeChildren,
                                            new Type[] {typeof (TList<PortalCatalog>)});
                    portalCatalogService.DeepLoad(ntCategoryNode.CatalogIDSource.PortalCatalogCollection, true,
                                                  DeepLoadType.IncludeChildren, new Type[] {typeof (Portal)});
                }

                var portalCatalog =
                    category.CategoryNodeCollection.SelectMany(x => x.CatalogIDSource.PortalCatalogCollection)
                            .FirstOrDefault();

                if (portalCatalog != null)
                {
                    var categoryFacet = NetTiersToLuceneMapper.LuceneCategoryFacet(category,
                                                                                   portalCatalog.PortalCatalogID);

                    categoryFacet.FacetNames = GetCategoryFacetNames(categoryId);

                    return categoryFacet;
                }
            }

            return new LuceneCategoryFacet();
        }

        public List<LuceneCategoryFacet> GetAllCategoryFacets(int start, int pageLength, out int totalCount)
        {
            CategoryService categoryService = new CategoryService();

            CategoryNodeService categoryNodeService = new CategoryNodeService();
            CatalogService catalogService = new CatalogService();
            PortalCatalogService portalCatalogService = new PortalCatalogService();

            var categories = categoryService.GetPaged(start, pageLength, out totalCount);
            categoryService.DeepLoad(categories, true, DeepLoadType.IncludeChildren,
                                         new Type[] { typeof(TList<CategoryNode>) });

            foreach (CategoryNode ntCategoryNode in categories.SelectMany(c => c.CategoryNodeCollection))
            {
                categoryNodeService.DeepLoad(ntCategoryNode, true, DeepLoadType.IncludeChildren, new Type[] { typeof(Catalog), typeof(CategoryNode), typeof(Category) });
                categoryNodeService.DeepLoad(ntCategoryNode.CategoryNodeCollection, true, DeepLoadType.IncludeChildren, new Type[] { typeof(Catalog), typeof(CategoryNode), typeof(Category) });
	            foreach (CategoryNode ntCategoryNode1 in ntCategoryNode.CategoryNodeCollection)
	            {
					catalogService.DeepLoad(ntCategoryNode1.CatalogIDSource, true, DeepLoadType.IncludeChildren, new Type[] { typeof(TList<PortalCatalog>) });
					portalCatalogService.DeepLoad(ntCategoryNode1.CatalogIDSource.PortalCatalogCollection, true, DeepLoadType.IncludeChildren, new Type[] { typeof(Portal) });
	            }

	            catalogService.DeepLoad(ntCategoryNode.CatalogIDSource, true, DeepLoadType.IncludeChildren, new Type[] { typeof(TList<PortalCatalog>) });
                portalCatalogService.DeepLoad(ntCategoryNode.CatalogIDSource.PortalCatalogCollection, true, DeepLoadType.IncludeChildren, new Type[] { typeof(Portal) });
            }

            var portalCategories = categories.Select(x => new { Category = x, PortalCatalogIDs = x.CategoryNodeCollection.Select(y => y.CatalogIDSource.PortalCatalogCollection).SelectMany(z => z.Select(v => v.PortalCatalogID)) });

            var categoryFacets = portalCategories.SelectMany(c => c.PortalCatalogIDs.Select(p => NetTiersToLuceneMapper.LuceneCategoryFacet(c.Category, p))).ToList();

            foreach (var luceneCategoryFacet in categoryFacets)
            {
                luceneCategoryFacet.FacetNames = GetCategoryFacetNames(luceneCategoryFacet.CategoryId);
            }

            return categoryFacets;
        }

        public List<string> GetCategoryFacetNames(int categoryId)
        {
			FacetGroupCategoryService tagGroupCategoryService = new FacetGroupCategoryService();

            return tagGroupCategoryService.DeepLoadByCategoryID(categoryId, true, DeepLoadType.IncludeChildren,
                                                             new Type[] {typeof (FacetGroup)})
                                       .Select(
                                           tgc =>
                                           System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(tgc.FacetGroupIDSource.FacetGroupLabel.ToLower().Replace(" ", "_")) + "|" + tgc.FacetGroupIDSource.ControlTypeID)
                                       .ToList();
        }
    }
}
