﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lucene.Net.Documents;

namespace ZNode.Libraries.Search.LuceneSearchProvider.Search
{
    public class SearchResultDocument
    {
        public Document Doc { get; set; }
    }
}
