﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lucene.Net.Search;

namespace ZNode.Libraries.Search.LuceneSearchProvider.Search
{
   public static class LuceneUtility
    {

       public static string GetQuery(string fieldName,string value)
       {
           return String.Concat(fieldName, ":\"", value, "\"");
       }

       public static  List<SortField> GetSortFields(List<SortCriteria> sortCriterias, int sortOrder)
       {
           var sortFields = new List<SortField>();

           // Add default sorting if none was provided
           if (sortCriterias == null || sortCriterias.Count.Equals(0))
               sortCriterias = new List<SortCriteria>
                                                      {
                                                          new SortCriteria
                                                              {
                                                                  SortDirection = SortCriteria.SortDirectionEnum.ASC,
                                                              }
                                                      };

           foreach (SortCriteria sortCriteria in sortCriterias)
           {
               switch (sortOrder)
               {
                   case 4:
                       sortFields.Add(new SortField("ProductName", SortField.STRING, false));
                       break;

                   case 5:
                       sortFields.Add(new SortField("ProductName", SortField.STRING, true));
                       break;

                   case 6:
                       sortFields.Add(new SortField("ReviewRating", SortField.DOUBLE, true));
                       break;
               }
           }


           return sortFields;

       }

       public static string BuildPrefixQuery(string query)
       {
           var queryText = new StringBuilder();

           if (!string.IsNullOrWhiteSpace(query))
           {
               var temp = query.Trim();
               string[] tokens = temp.Split(' ');
               foreach (var token in tokens)
               {
                   if (!string.IsNullOrEmpty(token))
                   queryText.AppendFormat("{0}* ", token);
               }
           }

           return queryText.ToString().Trim();
       }
    }
}
