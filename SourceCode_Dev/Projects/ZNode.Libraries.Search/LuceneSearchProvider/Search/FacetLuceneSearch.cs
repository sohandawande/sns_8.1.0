﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lucene.Net.Analysis;
using Lucene.Net.Documents;
using Lucene.Net.QueryParsers;
using Lucene.Net.Search;
using ZNode.Libraries.Search.Interfaces;
using ZNode.Libraries.Search.LuceneSearchProvider.LuceneExtensionMethods;


namespace ZNode.Libraries.Search.LuceneSearchProvider.Search
{
    /// <summary>
    /// Class provides the Facet Names , Its values and count 
    /// </summary>
    public class FacetLuceneSearch : LuceneSearchBase
    {
        public FacetLuceneSearch() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parser"></param>
        public FacetLuceneSearch(QueryParser parser) : base(parser) { }

        public List<IZNodeSearchFacet> FacetSearch(IZNodeSearchRequest request)
        {
            // Get the Facet results
            var facetNameResults = GetFacetNamesFromIndex(request.Category, request.PortalCatalogID);

            var indexSearcher = LuceneSearchManager.GetIndexSearcher;

            // Create the query 
            if (facetNameResults.Any())
            {
                // Get facetname for selected category
                var facetNameList = facetNameResults.ToArray();

                // Get the common Query
                BooleanQuery query = GetQuery(request.SearchText, request.Category, request.PortalCatalogID, request.ExternalIdNotNull);

                // update facetquery to get filtered result while fetching facet value/count.
                UpdateFacetQuery(request, query);

                UpdateSearchResultQuery(request,query);

                return DoSimpleFacetSearch(facetNameList, query);
            }
            return new List<IZNodeSearchFacet>();
        }


        public List<IZNodeSearchCategoryItem> CategoryFacet(IZNodeSearchRequest request)
        {

            List<SearchFacet> factes = new List<SearchFacet>();
            var parserFields = LuceneSearchManager.SearchFields.ToList();
            QueryParser = new MultiFieldQueryParser(LuceneSearchManager.GetVersion, parserFields.ToArray(), LuceneAnalyzer);
            
            if (!string.IsNullOrEmpty(request.Category) && string.IsNullOrEmpty(request.SearchText))
            {
                var ChildCategories = GetCategoryInfoFromIndex(request.Category, request.PortalCatalogID);

                foreach (var zNodeSearchResponse in ChildCategories)
                {
                    CategorySearch(request, zNodeSearchResponse, parserFields);                   
                }

                return ChildCategories;
            }
            
        
            // update query parser with facet fields
            if (request.Facets != null && request.Facets.Count > 0)
            {
                parserFields.AddRange(request.Facets.Select(x => x.Key.ToString()));
            }
            var searchQuery = GetQuery(request.SearchText, request.Category, request.PortalCatalogID,request.ExternalIdNotNull);

            UpdateSearchResultQuery(request, searchQuery);
           
            return DoSimpleCategoryFacetSearch(new string[] { "CategoryHierarchy" },searchQuery, request.PortalCatalogID);
        }

        private void CategorySearch(IZNodeSearchRequest request, IZNodeSearchCategoryItem item, List<string> parserFields)
        {
            BooleanQuery searchQuery = GetQuery(request.SearchText, item.Name.ToCategoryNameString(), request.PortalCatalogID,request.ExternalIdNotNull);
            // update query parser with facet fields
            if (request.Facets != null && request.Facets.Count > 0)
            {
                parserFields.AddRange(request.Facets.Select(x => x.Key.ToString()));
            }
            UpdateFacetQuery(request, searchQuery);
            UpdateSearchResultQuery(request,searchQuery);
            var indexSearcher = LuceneSearchManager.GetIndexSearcher;
            var result = indexSearcher.Search(searchQuery, LuceneSearchManager.MaxSearchLimit);

            item.Count = result.TotalHits;

            foreach (var categoryFacets in item.Hierarchy)
            {
                CategorySearch(request, categoryFacets, parserFields);
            }

        }

        private IEnumerable<string> GetFacetNamesFromIndex(string category, int portalCatalogID)
        {
            var indexSearcher = LuceneSearchManager.GetIndexSearcher;
            var booleanQuery = new BooleanQuery();

            var term = GetTerm("CategoryName", category);
            booleanQuery.Add(new BooleanClause(term, Occur.MUST));

            booleanQuery.Add(GetMustClause("PortalCatalogID", portalCatalogID.ToString()));
            var Hits = indexSearcher.Search(booleanQuery, null, LuceneSearchManager.MaxSearchLimit, new Sort(new SortField(null, SortField.SCORE, false))).ScoreDocs;

            return MapDocumentToFacetList(Hits, indexSearcher);
        }

        private List<IZNodeSearchCategoryItem> GetCategoryInfoFromIndex(string category, int portalCatalogID, bool isChildOnly = false)
        {
            var indexSearcher = LuceneSearchManager.GetIndexSearcher;
            var booleanQuery = new BooleanQuery();

            var term = GetTerm("CategoryName", category);
            booleanQuery.Add(new BooleanClause(term, Occur.MUST));
            booleanQuery.Add(GetMustClause("PortalCatalogID", portalCatalogID.ToString()));
            var Hits = indexSearcher.Search(booleanQuery, null, LuceneSearchManager.MaxSearchLimit, new Sort(new SortField(null, SortField.SCORE, false))).ScoreDocs;

            return MapDocumentToCatogoryDataList(Hits, indexSearcher, portalCatalogID, isChildOnly);
        }

        /// <summary>
        /// Map document to datalist with hits and searcher
        /// </summary>
        /// <param name="hits"></param>
        /// <param name="searcher"></param>
        /// <returns>Returns the Product list</returns>
        protected List<IZNodeSearchCategoryItem> MapDocumentToCatogoryDataList(IEnumerable<ScoreDoc> hits, IndexSearcher searcher, int portalCatalogID, bool isChildOnly = false)
        {
            var categoryList = new List<IZNodeSearchCategoryItem>();

            //searcher.Doc(hits.Doc))
            foreach (var scoreDoc in hits)
            {
                var doc = searcher.Doc(scoreDoc.Doc);

                var values = doc.GetValues("ParentCategory").Distinct();

                if (values.Any())
                {
                    List<IZNodeSearchCategoryItem> childHierarchy = categoryList;
                    LuceneSearchCategoryItem parentCategory = null;

                    if (!isChildOnly)
                    {                        
                        var parentTemp = values.FirstOrDefault().Split('/').Skip(1);

                        foreach (var parent in parentTemp)
                        {
                            if (parentCategory == null)
                            {
                                parentCategory = new LuceneSearchCategoryItem()
                                {
                                    Name = parent.Split('|')[0],
                                    Title = parent.Split('|')[1],
                                    Hierarchy = new List<IZNodeSearchCategoryItem>()
                                };
                            }
                            else
                            {
                                parentCategory.Hierarchy.Add(new LuceneSearchCategoryItem()
                                {
                                    Name = parent.Split('|')[0],
                                    Title = parent.Split('|')[1],
                                    Hierarchy = new List<IZNodeSearchCategoryItem>()
                                }
                                );
                            }
                        }

                        childHierarchy = parentCategory.Hierarchy;

                        categoryList.Add(parentCategory);
                    }                                   

                    var childs = doc.GetValues("SubCategory");

                    foreach (var child in childs)
                    {
                        var childtemp = child.Split('/').Skip(1).FirstOrDefault();

                        childHierarchy.Add(new LuceneSearchCategoryItem()
                                {
                                    Name = childtemp.Split('|')[0],
                                    Title = childtemp.Split('|')[1],
                                    Hierarchy = GetCategoryInfoFromIndex(childtemp.Split('|')[0], portalCatalogID, true)
                                }
                            );
                    }                    
                }
            }

            return categoryList;
        }

        /// <summary>
        /// Map Document To Data with document
        /// </summary>
        /// <param name="doc"></param>
        /// <returns>Returns the Search Product</returns>
        private IZNodeSearchCategoryItem MapDocumentToCategoryData(Document doc)
        {

            IZNodeSearchCategoryItem response = new LuceneSearchCategoryItem();

            response.CategoryID = Convert.ToInt32(doc.Get("CategoryID"));
            var parents = doc.GetValues("ParentCategory");
            var childs = doc.GetValues("SubCategory");

            response.Hierarchy = new List<IZNodeSearchCategoryItem>(); 
                       
            return response;
        }

        private List<IZNodeSearchFacet> DoSimpleFacetSearch(string[] facetNameList, BooleanQuery booleanQuery)
        {
            List<IZNodeSearchFacet> facetList = new List<IZNodeSearchFacet>();
            var indexreader = LuceneSearchManager.GetIndexSearcher.IndexReader;
            string facetArray = string.Empty;

            foreach (var item in facetNameList[0].Split(','))
            {
                facetArray = item.Split('|')[0];
                var sfs = new SimpleFacetedSearch(LuceneSearchManager.GetIndexSearcher.IndexReader, facetArray);

                SimpleFacetedSearch.Hits hits = sfs.Search(booleanQuery, LuceneSearchManager.MaxFacetSearchLimit);

                long totalHits = hits.TotalHitCount;
                foreach (SimpleFacetedSearch.HitsPerFacet hpg in hits.HitsPerFacet)
                {
                    long hitCountPerGroup = hpg.HitCount;
                    SimpleFacetedSearch.FacetName facetName = hpg.Name;

                    AddFacetResult(facetList, item, facetName.ToString(), Convert.ToInt32(item.Split('|')[1]), hitCountPerGroup);
                }
            }

            return facetList;
        }

        private List<IZNodeSearchCategoryItem> DoSimpleCategoryFacetSearch(string[] categoryList, BooleanQuery booleanQuery, int portalCatalogID)
        {
            List<IZNodeSearchCategoryItem> facetList = new List<IZNodeSearchCategoryItem>();
            var indexreader = LuceneSearchManager.GetIndexSearcher.IndexReader;
            string facetArray = string.Empty;

            if (categoryList != null)
            {
                var sfs = new SimpleFacetedSearch(LuceneSearchManager.GetIndexSearcher.IndexReader, categoryList);

                SimpleFacetedSearch.Hits hits = sfs.Search(booleanQuery, LuceneSearchManager.MaxFacetSearchLimit);

                long totalHits = hits.TotalHitCount;

                foreach (SimpleFacetedSearch.HitsPerFacet hpg in hits.HitsPerFacet)
                {
                    long hitCountPerGroup = hpg.HitCount;
                    if (hitCountPerGroup > 0 && hpg.Name.ToString().StartsWith(string.Format("{0}/", portalCatalogID)))
                    {
                        SimpleFacetedSearch.FacetName facetName = hpg.Name;
                        AddCategoryFacetResult(facetList, facetName.ToString(), facetName.ToString(), Convert.ToInt32(0), hitCountPerGroup);
                    }
                }
            }

            return facetList;
        }

        private void AddCategoryFacetResult(List<IZNodeSearchCategoryItem> facetList, string facetName, string facetValue, int controlType, long hitCountPerGroup)
        {
            if (hitCountPerGroup > 0)
            {
                var parentCategoryName = string.Empty;
                facetName.Split('/').Skip(2).Select((categoryName, idx) => new { categoryName, idx }).ToList().ForEach(y =>
                {
                    var z = y.categoryName.Split('|');
                    var existing = facetList.FirstOrDefaultFromMany(sr => sr.Hierarchy, sr => sr.Name == z[0]);
                    if (existing == null)
                    {
                        var path = facetName.Split('/').Take(y.idx + 3).ToArray();
                        path[1] = y.idx.ToString();
                        var category = new LuceneSearchCategoryItem()
                        {
                            Title = z[1],
                            Name = z[0],
                            Count = (int)hitCountPerGroup,
                            Hierarchy = new List<IZNodeSearchCategoryItem>()
                        };

                        var parent = facetList.FirstOrDefaultFromMany(sr => sr.Hierarchy, sr => sr.Name == parentCategoryName);
                        if (parent != null)
                            parent.Hierarchy.Add(category);
                        else
                            facetList.Add(category);
                    }

                    parentCategoryName = z[0];
                });
            }
        }

        private void AddFacetResult(List<IZNodeSearchFacet> facetList, string facetName, string facetValue, int controlType, long hitCountPerGroup)
        {
            if (hitCountPerGroup > 0)
            {
                IZNodeSearchFacet searchFacet = facetList.FirstOrDefault(x => x.AttributeName == facetName);
                if (searchFacet == null)
                {
                    searchFacet = new LuceneSearchFacet();
                    searchFacet.AttributeName = facetName;
                    searchFacet.ControlTypeID = controlType;
                    searchFacet.AttributeValues = new List<IZNodeSearchFacetValue>();
                    facetList.Add(searchFacet);
                }

                //foreach (string value in facetValue.ToString().Split('/'))
                {
                    IZNodeSearchFacetValue fValue = searchFacet.AttributeValues.FirstOrDefault(x => x.AttributeValue == facetValue);
                    if (fValue == null)
                    {
                        fValue = new LuceneSearchFacetValue();
                        fValue.AttributeValue = facetValue;
                        searchFacet.AttributeValues.Add(fValue);
                    }

                    fValue.FacetCount += hitCountPerGroup;
                }
            }
        }
    }
}
