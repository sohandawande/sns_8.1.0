﻿using System;
using System.ComponentModel;
using System.IO;
using Lucene.Net.Analysis;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Index;
using Lucene.Net.Search;
using Lucene.Net.Store;
using ZNode.Libraries.Search.LuceneSearchProvider.Search.Constants;
using Directory = Lucene.Net.Store.Directory;
using Version = Lucene.Net.Util.Version;
using System.Collections.Generic;
using ZNode.Libraries.Search.LuceneSearchProvider.Analyzers;
using System.Linq;
namespace ZNode.Libraries.Search.LuceneSearchProvider.Search
{
    /// <summary>
    /// Class responsible for Managing the Lucene Search and IndexWriter
    /// </summary>
    public class LuceneSearchManager
    {

        #region public static  properties
        public const string IndexDirectory = "LuceneIndexLocation";
        // Fields to search 
        public static string[] SearchFields = new[] { "ProductName", "Supplier", "Description", "ShortDescription","Features", "Specifications" };
        public static string[] PartialDefaultFields = new[] { "Name", "ProductName", "ProductNum", "Brand", "SKU", "Tags" };
        public const int TypeAheadCategories = 3;
        public const int MaxSearchLimit = 100000;
        public const int MaxFacetSearchLimit = 1000;
        public const string LuceneIndexReaderKey = "LuceneIndexReaderKey";
        #endregion


        #region Private variables
       private static readonly string ZnodeIndexDirectory = System.Configuration.ConfigurationManager.AppSettings[LuceneLibraryConstants.indexDirectory];
       
        /// <summary>
        /// holds the singleton IndexSearcher 
        /// </summary>
        private static volatile IndexSearcher _indexSearcher;
        private static object syncRoot = new Object();
        /// holds the Lucene index path
        /// </summary>
        private static readonly Directory Dir = new SimpleFSDirectory(new DirectoryInfo(ZnodeIndexDirectory));

        #endregion

        private LuceneSearchManager()
        {

        }

        #region public methods
        /// <summary>
        /// Singleton implementation for IndexSearhcer
        /// </summary>
        public static IndexSearcher GetIndexSearcher
        {
            get
            {
                lock (syncRoot)
                {
                    _indexSearcher = new IndexSearcher(Dir, true);
                }

                return _indexSearcher;
            }
        }


        /// <summary>
        /// Get the Index writer 
        /// </summary>
        /// <param name="indexLocation">Location of Index file</param>
        /// <param name="create">true for new file/false for updating existing file</param>
        /// <returns>IndexWrtiter</returns>
        public static IndexWriter GetIndexWriter(string indexLocation, bool? create=null)
        {
            Directory directory = FSDirectory.Open(indexLocation);
            IndexWriter writer = null;

            try
            {
                if (!create.HasValue)
                {
                    writer = new IndexWriter(directory, GetAnalyzer(), IndexWriter.MaxFieldLength.UNLIMITED);
                    return writer;
                }
                writer = new IndexWriter(directory, GetAnalyzer(), create.Value, IndexWriter.MaxFieldLength.UNLIMITED);
            
            }
            catch (LockObtainFailedException)
            {
                // Unlock the index file by removing the lock and call Index Writer again 
                DirectoryInfo indexDirInfo = new DirectoryInfo(indexLocation);
                FSDirectory indexFSDir = FSDirectory.Open(indexDirInfo, new Lucene.Net.Store.SimpleFSLockFactory(indexDirInfo));
                IndexWriter.Unlock(indexFSDir);

                writer = new IndexWriter(directory, GetAnalyzer(), create.Value, IndexWriter.MaxFieldLength.UNLIMITED);
             }
            catch (Exception)
            {   
                throw;
            }

           
            return writer;

        }

        /// <summary>
        /// Gets the PerFieldAnalyzerWrapper for Indexing and searching
        /// </summary>
        /// <returns></returns>

        public static Analyzer GetAnalyzer()
        {
            return new PerFieldAnalyzerWrapper(new StandardAnalyzer(GetVersion), GetAnalyzers());
        }


        /// <summary>
        /// Call this method whenver there is update in the Index file and Indexreader needs to get those changes in the memory 
        /// </summary>
        public static void ReopenIndexReader()
        {
            if (_indexSearcher != null)
            {
                var newReader = _indexSearcher.IndexReader.Reopen(true);

                if (newReader != _indexSearcher.IndexReader)
                {
                    _indexSearcher.Dispose();
                    _indexSearcher = new IndexSearcher(newReader);

                }

            }

        }

        // Het the version we are going to use 
        public static Version GetVersion
        {
            get { return Version.LUCENE_29; }
        }
        #endregion


        #region private methods
        /// <summary>
        /// Method to get Analyzers update this method when you want to use different lind of analyzers while differen fields  
        /// </summary>
        /// <returns></returns>

        private static IEnumerable<KeyValuePair<string, Analyzer>> GetAnalyzers()
        {
            var analyzers = new List<KeyValuePair<string, Analyzer>>();

            analyzers.Add(new KeyValuePair<string, Analyzer>("ProductName", new NewWhitespaceAnalyzer()));
            analyzers.Add(new KeyValuePair<string, Analyzer>("CategoryHierarchy", new NewWhitespaceAnalyzer()));
            analyzers.Add(new KeyValuePair<string, Analyzer>("ParentCategory", new NewWhitespaceAnalyzer()));
            analyzers.Add(new KeyValuePair<string, Analyzer>("SubCategory", new NewWhitespaceAnalyzer()));
            return analyzers;
        }
        #endregion






    }
}
