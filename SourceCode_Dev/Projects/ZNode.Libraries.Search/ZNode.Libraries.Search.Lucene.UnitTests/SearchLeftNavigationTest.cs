﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZNode.Libraries.Search.Interfaces;
using ZLSLS = ZNode.Libraries.Search.LuceneSearchProvider.Search;

namespace ZNode.Libraries.Search.Lucene.UnitTests
{

    # region Positive Test cases

    [TestClass]
    public class LeftNavigationUnitTesting
    {
        //Search with Single Category
        [TestMethod]
        public void SearchWithSingleCategory()
        {
            string searchtext = string.Empty;
            string category = "Fruit";

            IZNodeSearchRequest request = new ZLSLS.LuceneSearchRequest();
            request.searchText = searchtext;
            request.category = category;
            request.PortalCatalogID = 3;
            request.Facets = new List<KeyValuePair<string, IEnumerable<string>>>();
            request.SortCriteria = new List<ZLSLS.SortCriteria>();

            var provider = new ZLSLS.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);

            Assert.AreEqual(true, response.CategoryItems.Any(x => x.Name == category));
        }


        //Search with Single Category and its Count
        [TestMethod]
        public void SearchWithSingleCategoryCount()
        {
            string searchtext = string.Empty;
            string category = "Fruit";

            IZNodeSearchRequest request = new ZLSLS.LuceneSearchRequest();
            request.searchText = searchtext;
            request.category = category;
            request.PortalCatalogID = 3;
            request.Facets = new List<KeyValuePair<string, IEnumerable<string>>>();
            request.SortCriteria = new List<ZLSLS.SortCriteria>();

            var provider = new ZLSLS.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);

            Assert.AreEqual(true, response.CategoryItems.Any(x => x.Name == category && x.Count > 0));
        }

        //Search with Sub Category and its Count
        [TestMethod]
        public void SearchCategoryCount()
        {
            string searchtext = string.Empty;
            string category = "Fruit";

            IZNodeSearchRequest request = new ZLSLS.LuceneSearchRequest();
            request.searchText = searchtext;
            request.category = category;
            request.PortalCatalogID = 3;
            request.Facets = new List<KeyValuePair<string, IEnumerable<string>>>();
            request.SortCriteria = new List<ZLSLS.SortCriteria>();

            var provider = new ZLSLS.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);

            Assert.AreEqual(true, response.CategoryItems.Any(x => x.Name == "Organic Produce" && x.Count > 0));
        }

        //Search for Different Category
        [TestMethod]
        public void SearchDifferentCategory()
        {
            string searchtext = string.Empty;
            string category = "Vegetables";

            IZNodeSearchRequest request = new ZLSLS.LuceneSearchRequest();
            request.searchText = searchtext;
            request.category = category;
            request.PortalCatalogID = 3;
            request.Facets = new List<KeyValuePair<string, IEnumerable<string>>>();
            request.SortCriteria = new List<ZLSLS.SortCriteria>();

            var provider = new ZLSLS.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);

            Assert.AreEqual(true, response.CategoryItems.Any(x => x.Name == category));
        }

        //Search for Different Category and Search Text
        [TestMethod]
        public void SearchCategoryWithSearchText()
        {
            string searchtext = "Carrots";
            string category = "Vegetables";

            IZNodeSearchRequest request = new ZLSLS.LuceneSearchRequest();
            request.searchText = searchtext;
            request.category = category;
            request.PortalCatalogID = 3;
            request.Facets = new List<KeyValuePair<string, IEnumerable<string>>>();
            request.SortCriteria = new List<ZLSLS.SortCriteria>();

            var provider = new ZLSLS.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);

            Assert.AreEqual(true, response.CategoryItems.Any(x => x.Name == category && x.Count == 1));
        }

        //Search With Empty Category and Search Text 
        [TestMethod]
        public void SearchEmptyCategory()
        {
            string searchtext = "Vegetables";
            string category = string.Empty;

            IZNodeSearchRequest request = new ZLSLS.LuceneSearchRequest();
            request.searchText = searchtext;
            request.category = category;
            request.PortalCatalogID = 3;
            request.Facets = new List<KeyValuePair<string, IEnumerable<string>>>();
            request.SortCriteria = new List<ZLSLS.SortCriteria>();

            var provider = new ZLSLS.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);

            Assert.AreEqual(true, response.CategoryItems.Any(x => x.Name == searchtext));
        }

        //Search With Empty Category and Search Text with its Count
        [TestMethod]
        public void SearchEmptyCategoryCount()
        {
            string searchtext = "Vegetables";
            string category = string.Empty;

            IZNodeSearchRequest request = new ZLSLS.LuceneSearchRequest();
            request.searchText = searchtext;
            request.category = category;
            request.PortalCatalogID = 3;
            request.Facets = new List<KeyValuePair<string, IEnumerable<string>>>();
            request.SortCriteria = new List<ZLSLS.SortCriteria>();

            var provider = new ZLSLS.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);

            Assert.AreEqual(true, response.CategoryItems.Any(x => x.Name == searchtext && x.Count > 0));
        }

        //Search for single Category with Empty Category and Empty Search Text 
        [TestMethod]
        public void SearchAllDepartmentWithSingleCategory()
        {
            string searchtext = string.Empty;
            string category = string.Empty;

            IZNodeSearchRequest request = new ZLSLS.LuceneSearchRequest();
            request.searchText = searchtext;
            request.category = category;
            request.PortalCatalogID = 3;
            request.Facets = new List<KeyValuePair<string, IEnumerable<string>>>();
            request.SortCriteria = new List<ZLSLS.SortCriteria>();

            var provider = new ZLSLS.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);
            Assert.AreEqual(true, response.CategoryItems.Any(x => x.Name == "Wine"));
        }

        //Search for single Category with Empty Category and Empty Search Text and its Count
        [TestMethod]
        public void SearchAllDepartmentWithSingleCategoryCount()
        {
            string searchtext = string.Empty;
            string category = string.Empty;

            IZNodeSearchRequest request = new ZLSLS.LuceneSearchRequest();
            request.searchText = searchtext;
            request.category = category;
            request.PortalCatalogID = 3;
            request.Facets = new List<KeyValuePair<string, IEnumerable<string>>>();
            request.SortCriteria = new List<ZLSLS.SortCriteria>();

            var provider = new ZLSLS.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);
            Assert.AreEqual(true, response.CategoryItems.Any(x => x.Name == "Wine" && x.Count > 0));
        }
    }

    #endregion
}
