﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZNode.Libraries.Search.Interfaces;
using ZLSLS = ZNode.Libraries.Search.LuceneSearchProvider.Search;

namespace ZNode.Libraries.Search.Lucene.UnitTests
{

    #region Positive Test cases

    [TestClass]
    public class SearchCategoryTest
    {
        [TestMethod]
        public void SearchCategory()
        {
            string categoryName = "Fruit";
            string expectedResult = "Fruit";
            string searchtext = string.Empty;

            IZNodeSearchRequest request = new ZLSLS.LuceneSearchRequest();
            request.searchText = searchtext;
            request.category = categoryName;
            request.PortalCatalogID = 3;
            request.Facets = new List<KeyValuePair<string, IEnumerable<string>>>();
            request.SortCriteria = new List<ZLSLS.SortCriteria>();

            var provider = new ZLSLS.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);

            Assert.AreEqual(true, response.CategoryItems.Any(x => x.Name == expectedResult));

            Assert.AreEqual(true, response.Products.Count ==11);

            Assert.AreEqual(true,response.CategoryItems[0].Count==11);
        }

        [TestMethod]
        public void CategoryProductCount()
        {
            string categoryName = "Organic Produce";
            string searchtext = string.Empty;

            IZNodeSearchRequest request = new ZLSLS.LuceneSearchRequest();
            request.searchText = searchtext;
            request.category = categoryName;
            request.PortalCatalogID = 3;
            request.Facets = new List<KeyValuePair<string, IEnumerable<string>>>();
            request.SortCriteria = new List<ZLSLS.SortCriteria>();

            var provider = new ZLSLS.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);
            Assert.AreEqual(true, response.Products.Count == 8);
            Assert.AreEqual(true, response.CategoryItems.Any(x => x.Name == categoryName));
            Assert.AreEqual(true, response.CategoryItems.Any(x => x.Count == 8));
        }


        [TestMethod]
        public void GetProductByRelevance()
        {
            string categoryName = "Nuts";
            int productIdExpected = 365;
            string searchtext = string.Empty;

            IZNodeSearchRequest request = new ZLSLS.LuceneSearchRequest();
            request.searchText = searchtext;
            request.category = categoryName;
            request.PortalCatalogID = 3;
            request.Facets = new List<KeyValuePair<string, IEnumerable<string>>>();
            request.SortCriteria = null;
            request.sortOrder = 0;

            var provider = new ZLSLS.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);

            Assert.AreEqual(true, response.Products.Select(x => x.ProductID == productIdExpected).First());
        }

        [TestMethod]
        public void GetProductASC()
        {
            string categoryName = "Gifts";
            int productIdExpected = 306;
            string searchtext = string.Empty;

            IZNodeSearchRequest request = new ZLSLS.LuceneSearchRequest();
            request.searchText = searchtext;
            request.category = categoryName;
            request.PortalCatalogID = 3;
            request.Facets = new List<KeyValuePair<string, IEnumerable<string>>>();
            request.SortCriteria = null;
            request.sortOrder = 4;

            var provider = new ZLSLS.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);

            Assert.AreEqual(true, response.Products.Select(x => x.ProductID == productIdExpected).First());
        }

        [TestMethod]
        public void GetProductDESC()
        {
            string categoryName = "Cheese";
            int productIdExpected = 385;
            string searchtext = string.Empty;

            IZNodeSearchRequest request = new ZLSLS.LuceneSearchRequest();
            request.searchText = searchtext;
            request.category = categoryName;
            request.PortalCatalogID = 3;
            request.Facets = new List<KeyValuePair<string, IEnumerable<string>>>();
            request.SortCriteria = null;
            request.sortOrder = 5;

            var provider = new ZLSLS.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);

            Assert.AreEqual(true, response.Products.Select(x => x.ProductID == productIdExpected).Last());
        }

        [TestMethod]
        public void GetProductByRating()
        {
            string categoryName = "Fruit";
            int productIdExpected = 302;
            string searchtext = string.Empty;

            IZNodeSearchRequest request = new ZLSLS.LuceneSearchRequest();
            request.searchText = searchtext;
            request.category = categoryName;
            request.PortalCatalogID = 3;
            request.Facets = new List<KeyValuePair<string, IEnumerable<string>>>();
            request.SortCriteria = null;
            request.sortOrder = 6;

            var provider = new ZLSLS.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);

            Assert.AreEqual(true, response.Products.Select(x => x.ProductID == productIdExpected).First());
        }

        [TestMethod]
        public void GetFacetGroupCount()
        {
            string categoryName = "Organic Produce";
            int expectedResult = 1;
            string searchtext = string.Empty;

            IZNodeSearchRequest request = new ZLSLS.LuceneSearchRequest();
            request.searchText = searchtext;
            request.category = categoryName;
            request.PortalCatalogID = 3;
            request.Facets = new List<KeyValuePair<string, IEnumerable<string>>>();
            request.SortCriteria = null;

            var provider = new ZLSLS.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);
            Assert.AreEqual(true, response.Facets.Count >= expectedResult);
        }

        [TestMethod]
        public void FacetName()
        {
            string categoryName = "Fruit";
            string facetName = "Calories";
            string searchtext = string.Empty;

            IZNodeSearchRequest request = new ZLSLS.LuceneSearchRequest();
            request.searchText = searchtext;
            request.category = categoryName;
            request.PortalCatalogID = 3;
            request.Facets = new List<KeyValuePair<string, IEnumerable<string>>>();
            request.SortCriteria = null;

            var provider = new ZLSLS.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);

            Assert.AreEqual(true,
                            response.Facets[0].AttributeName.Split('|')[0] == facetName &&
                            response.Facets[0].AttributeValues.Count > 0);
        }

        [TestMethod]
        public void FacetValues()
        {
            string categoryName = "Fruit";
            string attributeValue = "> 50";
            string searchtext = string.Empty;

            IZNodeSearchRequest request = new ZLSLS.LuceneSearchRequest();
            request.searchText = searchtext;
            request.category = categoryName;
            request.PortalCatalogID = 3;
            request.Facets = new List<KeyValuePair<string, IEnumerable<string>>>();
            request.SortCriteria = null;

            var provider = new ZLSLS.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);

            Assert.AreEqual(true, response.Facets[0].AttributeValues[0].AttributeValue == attributeValue);
        }

        [TestMethod]
        public void FacetsControlType()
        {
            string categoryName = "Flowers";
            int controlType = 2;
            string result = string.Empty;

            IZNodeSearchRequest request = new ZLSLS.LuceneSearchRequest();
            request.searchText = string.Empty;
            request.category = categoryName;
            request.PortalCatalogID = 3;
            request.Facets = new List<KeyValuePair<string, IEnumerable<string>>>();
            request.SortCriteria = null;

            var provider = new ZLSLS.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);
            Assert.AreEqual(true, response.Facets[0].ControlTypeID == controlType);
        }

        [TestMethod]
        public void FacetsCount()
        {
            string categoryName = "Nuts";
            bool selected = false;
            string result = string.Empty;

            IZNodeSearchRequest request = new ZLSLS.LuceneSearchRequest();
            request.searchText = string.Empty;
            request.category = categoryName;
            request.PortalCatalogID = 3;
            request.Facets = new List<KeyValuePair<string, IEnumerable<string>>>();
            request.SortCriteria = null;

            var provider = new ZLSLS.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);

            Assert.AreEqual(true,
                            response.Facets[1].AttributeValues[0].FacetCount > 0 &&
                            response.Facets[1].AttributeValues[0].Selected == selected);
        }

        #endregion

        #region Negative Test cases

        [TestMethod]
        public void NegSearchCategory()
        {
            string categoryName = "Fruit";
            string expectedResult = "Wine";
            string searchtext = string.Empty;

            IZNodeSearchRequest request = new ZLSLS.LuceneSearchRequest();
            request.searchText = searchtext;
            request.category = categoryName;
            request.PortalCatalogID = 3;
            request.Facets = new List<KeyValuePair<string, IEnumerable<string>>>();
            request.SortCriteria = new List<ZLSLS.SortCriteria>();

            var provider = new ZLSLS.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);

            Assert.AreNotEqual(true, response.CategoryItems[0].Name == expectedResult);
        }

        [TestMethod]
        public void NegCategoryProductCount()
        {
            string categoryName = "Organic Produce";
            string searchtext = string.Empty;

            IZNodeSearchRequest request = new ZLSLS.LuceneSearchRequest();
            request.searchText = searchtext;
            request.category = categoryName;
            request.PortalCatalogID = 3;
            request.Facets = new List<KeyValuePair<string, IEnumerable<string>>>();
            request.SortCriteria = new List<ZLSLS.SortCriteria>();

            var provider = new ZLSLS.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);

            Assert.AreNotEqual(true, response.CategoryItems[0].Count == 0);
        }

        [TestMethod]
        public void NegGetProductByRelevance()
        {
            string categoryName = "Fruit";
            int productIdExpected = 306;
            string searchtext = string.Empty;

            IZNodeSearchRequest request = new ZLSLS.LuceneSearchRequest();
            request.searchText = searchtext;
            request.category = categoryName;
            request.PortalCatalogID = 3;
            request.Facets = new List<KeyValuePair<string, IEnumerable<string>>>();
            request.SortCriteria = null;
            request.sortOrder = 0;

            var provider = new ZLSLS.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);

            Assert.AreEqual(false, response.Products.Select(x => x.ProductID == productIdExpected).First());
        }

        [TestMethod]
        public void NegGetProductASC()
        {
            string categoryName = "Gifts";
            int productIdExpected = 332;
            string searchtext = string.Empty;

            IZNodeSearchRequest request = new ZLSLS.LuceneSearchRequest();
            request.searchText = searchtext;
            request.category = categoryName;
            request.PortalCatalogID = 3;
            request.Facets = new List<KeyValuePair<string, IEnumerable<string>>>();
            request.SortCriteria = null;
            request.sortOrder = 4;

            var provider = new ZLSLS.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);

            Assert.AreEqual(false, response.Products.Select(x => x.ProductID == productIdExpected).First());
        }

        [TestMethod]
        public void NegGetProductDESC()
        {
            string categoryName = "Cheese";
            int productIdExpected = 394;
            string searchtext = string.Empty;

            IZNodeSearchRequest request = new ZLSLS.LuceneSearchRequest();
            request.searchText = searchtext;
            request.category = categoryName;
            request.PortalCatalogID = 3;
            request.Facets = new List<KeyValuePair<string, IEnumerable<string>>>();
            request.SortCriteria = null;
            request.sortOrder = 5;

            var provider = new ZLSLS.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);

            Assert.AreEqual(false, response.Products.Select(x => x.ProductID == productIdExpected).Last());
        }

        [TestMethod]
        public void NegGetProductByRating()
        {
            string categoryName = "Fruit";
            int productIdExpected = 402;
            string searchtext = string.Empty;

            IZNodeSearchRequest request = new ZLSLS.LuceneSearchRequest();
            request.searchText = searchtext;
            request.category = categoryName;
            request.PortalCatalogID = 3;
            request.Facets = new List<KeyValuePair<string, IEnumerable<string>>>();
            request.SortCriteria = null;
            request.sortOrder = 6;

            var provider = new ZLSLS.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);

            Assert.AreEqual(false, response.Products.Select(x => x.ProductID == productIdExpected).First());
        }

        [TestMethod]
        public void NegGetFacetGroupCount()
        {
            string categoryName = "Organic Produce";
            int expectedResult = 2;
            string searchtext = string.Empty;

            IZNodeSearchRequest request = new ZLSLS.LuceneSearchRequest();
            request.searchText = searchtext;
            request.category = categoryName;
            request.PortalCatalogID = 3;
            request.Facets = new List<KeyValuePair<string, IEnumerable<string>>>();
            request.SortCriteria = null;

            var provider = new ZLSLS.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);

            Assert.AreEqual(false, response.Facets.Count >= expectedResult);
        }

        [TestMethod]
        public void NegFacetName()
        {
            string categoryName = "Cheese";
            string facetName = "Calories";
            int facetCount = 3;
            string searchtext = string.Empty;

            IZNodeSearchRequest request = new ZLSLS.LuceneSearchRequest();
            request.searchText = searchtext;
            request.category = categoryName;
            request.PortalCatalogID = 3;
            request.Facets = new List<KeyValuePair<string, IEnumerable<string>>>();
            request.SortCriteria = null;

            var provider = new ZLSLS.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);

            Assert.AreEqual(false,
                            response.Facets[0].AttributeName.Split('|')[0] == facetName &&
                            response.Facets[0].AttributeValues.Count == facetCount);
        }

        [TestMethod]
        public void NegFacetsControlType()
        {
            string categoryName = "Cheese";
            int controlType = 5;
            string result = string.Empty;

            IZNodeSearchRequest request = new ZLSLS.LuceneSearchRequest();
            request.searchText = string.Empty;
            request.category = categoryName;
            request.PortalCatalogID = 3;
            request.Facets = new List<KeyValuePair<string, IEnumerable<string>>>();
            request.SortCriteria = null;

            var provider = new ZLSLS.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);
            Assert.AreEqual(false, (response.Facets[0].ControlTypeID == controlType));
        }

        [TestMethod]
        public void NegFacetValues()
        {
            string categoryName = "Gifts";
            string attributeValue = "USDA";
            string searchtext = string.Empty;

            IZNodeSearchRequest request = new ZLSLS.LuceneSearchRequest();
            request.searchText = searchtext;
            request.category = categoryName;
            request.PortalCatalogID = 3;
            request.Facets = new List<KeyValuePair<string, IEnumerable<string>>>();
            request.SortCriteria = null;

            var provider = new ZLSLS.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);

            Assert.AreEqual(false, response.Facets[0].AttributeValues[0].AttributeValue == attributeValue);
        }

        [TestMethod]
        public void NegFacetsCount()
        {
            string categoryName = "Wine";
            int facetCount = 9;
            bool selected = true;
            string result = string.Empty;
            IZNodeSearchRequest request = new ZLSLS.LuceneSearchRequest();
            request.searchText = string.Empty;
            request.category = categoryName;
            request.PortalCatalogID = 3;
            request.Facets = new List<KeyValuePair<string, IEnumerable<string>>>();
            request.SortCriteria = null;

            var provider = new ZLSLS.LuceneSearchProvider();
            IZNodeSearchResponse response = provider.Search(request);

            Assert.AreEqual(false,
                            response.Facets[0].AttributeValues[0].FacetCount == facetCount &&
                            response.Facets[0].AttributeValues[0].Selected == selected);
        }

        #endregion
    }
}
