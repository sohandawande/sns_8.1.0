﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using ZNode.Libraries.ECommerce.Utilities;

namespace PRFT.Engine.ERP
{
    public class ServiceHelper : HttpWebBase
    {
        
        /// <summary>
        /// Get Inventory List from ERP
        /// </summary>
        /// <param name="itemNumbers"></param>
        public DataTable GetInventory(string itemNumbers)
        {
            try
            {
                string getQueryString = GetInventoryQueryString(itemNumbers);
                string uriEndpoint = BaseUrl + string.Format(InventoryEndPoint, getQueryString);
                DataSet inventoryResponse = GetResourceFromEndpoint(uriEndpoint);    
                if(HasError)
                {
                   //Log the message 
                    ZNodeLogging.LogMessage("ServiceHelper:GetInventory: " + StatusDescription);
                }
                else
                {
                    if(inventoryResponse!=null && inventoryResponse.Tables!=null && inventoryResponse.Tables["properties"]!=null)
                    {
                        return inventoryResponse.Tables["properties"];
                    }
                }
            }
            catch (Exception ex)
            {
                ZNodeLogging.LogMessage("ServiceHelper:GetInventory: " + ex.Message);
            }
            return null;
        }

        public DataSet GetShippingAddressList(string companyCodeDebtorAccountID)
        {
            try
            {
                string uriEndpoint = BaseUrl + string.Format(ShippingAddressListEndPoint, companyCodeDebtorAccountID);
                DataSet shippingAddressesResponse = GetResourceFromEndpoint(uriEndpoint);
                if (HasError)
                {
                    //Log the message 
                    ZNodeLogging.LogMessage("ServiceHelper:GetShippingAddressList: " + StatusDescription);
                }
                else
                {
                    if (shippingAddressesResponse != null && shippingAddressesResponse.Tables != null && shippingAddressesResponse.Tables.Count>0)
                    {
                        return shippingAddressesResponse;
                    }
                    else
                    {
                        ZNodeLogging.LogMessage("ServiceHelper:GetShippingAddressList-No record found for Company Code Debtor AccountID= "+companyCodeDebtorAccountID);
                    }
                }
            }
            catch (Exception ex)
            {
                ZNodeLogging.LogMessage("ServiceHelper:GetShippingAddressList: " + ex.Message);
            }
            return null;
        }

        public DataSet GetAccountAndBillingDetails(string externalAccountNumber)
        {
            try
            {
                string uriEndpoint = BaseUrl + string.Format(AccountAndBillingDetailsEndPoint, externalAccountNumber);
                DataSet accountAndBillingDetailResponse = GetResourceFromEndpoint(uriEndpoint);
                if (HasError)
                {
                    //Log the message 
                    ZNodeLogging.LogMessage("ServiceHelper:GetAccountAndBillingDetails: External Account Number: " +externalAccountNumber +" Error Description:"+ StatusDescription);
                }
                else
                {
                    if (accountAndBillingDetailResponse != null && accountAndBillingDetailResponse.Tables != null && accountAndBillingDetailResponse.Tables.Count>0)
                    {
                        return accountAndBillingDetailResponse;
                    }
                }
            }
            catch (Exception ex)
            {
                ZNodeLogging.LogMessage("ServiceHelper:GetAccountAndBillingDetails: " + externalAccountNumber + " Error Description:" + ex.Message);
            }
            return null;
        }

        public DataSet GetProductPriceOfCodeType1FromERP(string associatedCustomerExternalId, string productNum, out bool isAnyERPError)
        {
            string uriEndpoint = string.Empty;
            isAnyERPError = false;
            try
            {
                //Condition 1 with code type1
                uriEndpoint = BaseUrl + string.Format(PricingWithPriceCode1EndPoint, productNum,associatedCustomerExternalId,Convert.ToString(DateTime.Now.Date),Convert.ToString(DateTime.Now.Date));
                DataSet priceResponse = GetResourceFromEndpoint(uriEndpoint);

                if (HasError)
                {
                    isAnyERPError = true;
                    //Log the message 
                    ZNodeLogging.LogMessage("ServiceHelper:GetProductPriceOfCodeType1FromERP: URL: " + uriEndpoint + ",  Error Description:" + StatusDescription);
                    
                }
                else
                {
                    if (priceResponse != null && priceResponse.Tables != null && priceResponse.Tables["properties"]!=null)
                    {
                        return priceResponse;
                    }
                }
            }
            catch (Exception ex)
            {
                isAnyERPError = true;
                ZNodeLogging.LogMessage("ServiceHelper:GetProductPriceOfCodeType1FromERP: URL: " + uriEndpoint + ",  Error Description:" + ex.Message);
            }
            return null;
        }

        public DataSet GetProductPriceOfCodeType3FromERP(string productNum, string customerType, out bool isAnyERPError)
        {
            string uriEndpoint = string.Empty;
            isAnyERPError = false;

            try
            {
                uriEndpoint = BaseUrl + string.Format(PricingWithPriceCode3EndPoint, productNum, customerType, Convert.ToString(DateTime.Now.Date), Convert.ToString(DateTime.Now.Date));
                DataSet priceResponse = GetResourceFromEndpoint(uriEndpoint);

                if (HasError)
                {
                    isAnyERPError = true;
                    //Log the message 
                    ZNodeLogging.LogMessage("ServiceHelper:GetProductPriceOfCodeType3FromERP: URL: " + uriEndpoint + ",  Error Description:" + StatusDescription);
                }
                else
                {
                    if (priceResponse != null && priceResponse.Tables != null && priceResponse.Tables["properties"] != null)
                    {
                        return priceResponse;
                    }
                }
            }
            catch (Exception ex)
            {
                isAnyERPError = true;
                ZNodeLogging.LogMessage("ServiceHelper:GetProductPriceOfCodeType3FromERP: URL: " + uriEndpoint + ",  Error Description:" + ex.Message);
            }
            return null;
        }

        public DataSet GetProductPriceOfCodeType6FromERP(string productNum, out bool isAnyERPError)
        {
            string uriEndpoint = string.Empty;
            isAnyERPError = false;
            try
            {
                uriEndpoint = BaseUrl + string.Format(PricingWithPriceCode6EndPoint, productNum, Convert.ToString(DateTime.Now.Date), Convert.ToString(DateTime.Now.Date));
                DataSet priceResponse = GetResourceFromEndpoint(uriEndpoint);

                if (HasError)
                {
                    isAnyERPError = true;
                    //Log the message 
                    ZNodeLogging.LogMessage("ServiceHelper:GetProductPriceOfCodeType6FromERP: URL: " + uriEndpoint + ",  Error Description:" + StatusDescription);
                }
                else
                {
                    if (priceResponse != null && priceResponse.Tables != null && priceResponse.Tables["properties"] != null)
                    {
                        return priceResponse;
                    }
                }
            }
            catch (Exception ex)
            {
                isAnyERPError = true;
                ZNodeLogging.LogMessage("ServiceHelper:GetProductPriceOfCodeType6FromERP: URL: " + uriEndpoint + ",  Error Description:" + ex.Message);
            }
            return null;
        }
    }
    
}
