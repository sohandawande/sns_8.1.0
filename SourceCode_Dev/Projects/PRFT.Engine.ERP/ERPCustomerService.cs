﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using ZNode.Libraries.ECommerce.Utilities;

namespace PRFT.Engine.ERP
{
    public class ERPCustomerService
    {

        public PRFTERPAccountDetailsModel GetAccountDetailsFromERP(string externalAccountNumber)
        {
            PRFTERPAccountDetailsModel accountDetails = null;
            accountDetails = GetAccountAndBillingDetailsFromERP(externalAccountNumber);
            if (accountDetails != null && !string.IsNullOrEmpty(accountDetails.CompanyCodeDebtorAccountID)) 
            {
                accountDetails.ShippingAddresses = GetShippingAddressListFromERP(accountDetails.CompanyCodeDebtorAccountID);
            }

            return accountDetails;
        }

        public PRFTERPAccountDetailsModel GetAccountAndBillingDetailsFromERP(string externalAccountNumber)
        {
            PRFTERPAccountDetailsModel accountDetails = null;
            DataSet ds = new DataSet();
            ds = new ServiceHelper().GetAccountAndBillingDetails(externalAccountNumber);

            if (ds != null)
            {
                try
                {
                    if (ds.Tables["properties"] != null && ds.Tables["properties"].Rows.Count > 0)
                    {
                        string invoiceAddress1 = string.Empty;
                        string invoiceAddress2 = string.Empty;
                        string invoiceAddress3 = string.Empty;

                        accountDetails = new PRFTERPAccountDetailsModel();
                        DataRow dr = ds.Tables["properties"].Rows[0]; //Get the top row only
                        accountDetails.DebtorCode = externalAccountNumber;
                        accountDetails.CustomerStatus = Convert.ToString(dr["CustomerStatus"]);
                        accountDetails.CustomerStatusDescription = Convert.ToString(dr["CustomerStatusDescription"]);
                        accountDetails.CustomerType = Convert.ToString(dr["TextFreeField30"]);
                        //accountDetails.CustomerType = Convert.ToString(dr["CustomerType"]);
                        //accountDetails.CustomerTypeDescription = Convert.ToString(dr["CustomerTypeDescription"]);

                        invoiceAddress2 = string.Empty;
                        if (dr.Table.Columns.Contains("InvoiceAddress2") && dr["InvoiceAddress2"] != null)
                        {
                            invoiceAddress2 = Convert.ToString(dr["InvoiceAddress2"]);
                        }
                        accountDetails.BillingAddress.City = Convert.ToString(dr["InvoiceCity"]);
                        accountDetails.BillingAddress.CountryCode = Convert.ToString(dr["InvoiceCountry"]);
                        accountDetails.BillingAddress.PostalCode = Convert.ToString(dr["InvoiceAddressPostCode"]);
                        accountDetails.BillingAddress.StateCode = Convert.ToString(dr["InvoiceState"]);
                        accountDetails.BillingAddress.CompanyName = Convert.ToString(dr["AccountName"]);
                        accountDetails.PaymentCondition = Convert.ToString(dr["PaymentCondition"]);
                        accountDetails.PaymentConditionDescription = Convert.ToString(dr["PaymentConditionDescription"]);
                        accountDetails.BillingAddress.PhoneNumber = Convert.ToString(dr["PhoneNumber"]);
                        accountDetails.Terms = Convert.ToString(dr["Terms"]);

                        //Get Modified Date
                        DateTime tmpDate = DateTime.Now;
                        if (ds.Tables["ModifiedDate"] != null && ds.Tables["ModifiedDate"].Rows.Count > 0)
                        {
                            DateTime.TryParse(Convert.ToString(ds.Tables["ModifiedDate"].Rows[0]["ModifiedDate_Text"]), out tmpDate);
                            accountDetails.ModifiedDate = tmpDate;
                        }

                        //Get CompanyCodeDebtorAccountID
                        if (ds.Tables["CompanyCodeDebtorAccountID"] != null && ds.Tables["CompanyCodeDebtorAccountID"].Rows.Count > 0)
                        {
                            accountDetails.CompanyCodeDebtorAccountID = Convert.ToString(ds.Tables["CompanyCodeDebtorAccountID"].Rows[0]["CompanyCodeDebtorAccountID_Text"]);
                        }

                        //Get Address Line1
                        if (ds.Tables["InvoiceAddress"] != null && ds.Tables["InvoiceAddress"].Rows.Count > 0)
                        {
                            bool isNullValue = false;
                            bool.TryParse(Convert.ToString(ds.Tables["InvoiceAddress"].Rows[0][0]), out isNullValue);

                            //first column tells is value null, true means null else value present
                            if (isNullValue)
                            {
                                invoiceAddress1 = string.Empty;
                            }
                            else
                            {
                                invoiceAddress1 = Convert.ToString(ds.Tables["InvoiceAddress"].Rows[0]["InvoiceAddress_Text"]);
                            }
                        }

                        //Get Address Line1
                        if (ds.Tables["InvoiceAddress2"] != null && ds.Tables["InvoiceAddress2"].Rows.Count > 0 && string.IsNullOrEmpty(invoiceAddress2))
                        {
                            bool isNullValue = false;
                            bool.TryParse(Convert.ToString(ds.Tables["InvoiceAddress2"].Rows[0][0]), out isNullValue);

                            //first column tells is value null, true means null else value present
                            if (isNullValue)
                            {
                                invoiceAddress2 = string.Empty;
                            }
                            else
                            {
                                invoiceAddress2 = Convert.ToString(ds.Tables["InvoiceAddress2"].Rows[0]["InvoiceAddress2_Text"]);
                            }
                        }

                        //Get Address Line3
                        if (ds.Tables["InvoiceAddress3"] != null && ds.Tables["InvoiceAddress3"].Rows.Count > 0)
                        {
                            bool isNullValue = false;
                            bool.TryParse(Convert.ToString(ds.Tables["InvoiceAddress3"].Rows[0][0]), out isNullValue);
                            //first column tells is value null, true means null else value present
                            if (isNullValue)
                            {
                                invoiceAddress3 = string.Empty;
                            }
                            else
                            {
                                invoiceAddress3 = Convert.ToString(ds.Tables["InvoiceAddress3"].Rows[0]["InvoiceAddress3_Text"]);
                            }
                        }

                        //Get proper address 
                        //As per the logic suggested by client if addressline1 contain numeric value then it will be part of street address
                        accountDetails.BillingAddress.StreetAddress1 = invoiceAddress2;
                        if (!string.IsNullOrEmpty(invoiceAddress1) && Regex.IsMatch(invoiceAddress1, @"^\d+$"))
                        {
                            accountDetails.BillingAddress.StreetAddress1 = invoiceAddress1 + " " + accountDetails.BillingAddress.StreetAddress1;
                        }
                        accountDetails.BillingAddress.StreetAddress2 = invoiceAddress3;
                    }
                }
                catch (Exception ex)
                {
                    ZNodeLogging.LogMessage("ERPCustomerService:GetAccountAndBillingDetailsFromERP-> " + ex.Message);
                }
            }
            return accountDetails;
        }

        public Collection<AddressModel> GetShippingAddressListFromERP(string companyCodeDebtorAccountID)
        {
            DataSet ds = new DataSet();
            ds = new ServiceHelper().GetShippingAddressList(companyCodeDebtorAccountID);

            List<ERPAddress> erpAddressList = new List<ERPAddress>();
            if (ds != null)
            {
                try
                {
                    if(ds.Tables["properties"]!=null && ds.Tables["properties"].Rows.Count>0)
                    {
                        int rowNum = 0;
                        foreach (DataRow dr in ds.Tables["properties"].Rows)
                        {
                            ERPAddress addr = new ERPAddress();
                            addr.Country = Convert.ToString(dr["Country"]);
                            addr.State = Convert.ToString(dr["State"]);
                            addr.TextFreeField1 = Convert.ToString(dr["TextFreeField1"]);
                            addr.Account = companyCodeDebtorAccountID;

                            //Get Address Line1
                            if(ds.Tables["AddressLine1"]!=null && ds.Tables["AddressLine1"].Rows.Count>rowNum)
                            {
                                bool isNullValue = false;
                                bool.TryParse(Convert.ToString(ds.Tables["AddressLine1"].Rows[rowNum][0]),out isNullValue);

                                //first column tells is value null, true means null else value present
                                if(isNullValue)
                                {
                                    addr.AddressLine1 = string.Empty;
                                }
                                else
                                {
                                    addr.AddressLine1 = Convert.ToString(ds.Tables["AddressLine1"].Rows[rowNum]["AddressLine1_Text"]);
                                }
                            }

                            //Get Address Line2
                            if (ds.Tables["AddressLine2"] != null && ds.Tables["AddressLine2"].Rows.Count > rowNum)
                            {
                                bool isNullValue = false;
                                bool.TryParse(Convert.ToString(ds.Tables["AddressLine2"].Rows[rowNum][0]), out isNullValue);
                                //first column tells is value null, true means null else value present
                                if (isNullValue)
                                {
                                    addr.AddressLine2 = string.Empty;
                                }
                                else
                                {
                                    addr.AddressLine2 = Convert.ToString(ds.Tables["AddressLine2"].Rows[rowNum]["AddressLine2_Text"]);
                                }
                            }

                            //Get Address Line3
                            if (ds.Tables["AddressLine3"] != null && ds.Tables["AddressLine3"].Rows.Count > rowNum)
                            {
                                bool isNullValue = false;
                                bool.TryParse(Convert.ToString(ds.Tables["AddressLine3"].Rows[rowNum][0]), out isNullValue);
                                //first column tells is value null, true means null else value present
                                if (isNullValue)
                                {
                                    addr.AddressLine3 = string.Empty;
                                }
                                else
                                {
                                    addr.AddressLine3 = Convert.ToString(ds.Tables["AddressLine3"].Rows[rowNum]["AddressLine3_Text"]);
                                }
                            }

                            //Get City
                            if (ds.Tables["City"] != null && ds.Tables["City"].Rows.Count > rowNum)
                            {
                                bool isNullValue = false;
                                bool.TryParse(Convert.ToString(ds.Tables["City"].Rows[rowNum][0]), out isNullValue);
                                //first column tells is value null, true means null else value present
                                if (isNullValue)
                                {
                                    addr.City = string.Empty;
                                }
                                else
                                {
                                    addr.City = Convert.ToString(ds.Tables["City"].Rows[rowNum]["City_Text"]);
                                }
                            }

                            //Get Created Date value
                            DateTime tmpDate = DateTime.Now;
                            if (ds.Tables["CreatedDate"] != null && ds.Tables["CreatedDate"].Rows.Count > rowNum)
                            {
                                DateTime.TryParse(Convert.ToString(ds.Tables["CreatedDate"].Rows[rowNum]["CreatedDate_Text"]), out tmpDate);
                                addr.CreatedDate = tmpDate;
                            }

                            //Get Modified Date
                            tmpDate = DateTime.Now;
                            if (ds.Tables["ModifiedDate"] != null && ds.Tables["ModifiedDate"].Rows.Count > rowNum)
                            {
                                DateTime.TryParse(Convert.ToString(ds.Tables["ModifiedDate"].Rows[rowNum]["ModifiedDate_Text"]), out tmpDate);
                                addr.ModifiedDate = tmpDate;
                            }

                            //Get ERP Address ID
                            if (ds.Tables["ID"] != null && ds.Tables["ID"].Rows.Count > rowNum)
                            {
                                addr.ID = Convert.ToString(ds.Tables["ID"].Rows[rowNum]["ID_Text"]);
                            }

                            //Get Phone
                            if (ds.Tables["Phone"] != null && ds.Tables["Phone"].Rows.Count > rowNum)
                            {
                                bool isNullValue = false;
                                bool.TryParse(Convert.ToString(ds.Tables["Phone"].Rows[rowNum][0]), out isNullValue);
                                //first column tells is value null, true means null else value present
                                if (isNullValue)
                                {
                                    addr.Phone = string.Empty;
                                }
                                else
                                {
                                    addr.Phone = Convert.ToString(ds.Tables["Phone"].Rows[rowNum]["Phone_Text"]);
                                }
                            }

                            //Get Postal Code
                            if (ds.Tables["PostCode"] != null && ds.Tables["PostCode"].Rows.Count > rowNum)
                            {
                                bool isNullValue = false;
                                bool.TryParse(Convert.ToString(ds.Tables["PostCode"].Rows[rowNum][0]), out isNullValue);
                                //first column tells is value null, true means null else value present
                                if (isNullValue)
                                {
                                    addr.PostCode = string.Empty;
                                }
                                else
                                {
                                    addr.PostCode = Convert.ToString(ds.Tables["PostCode"].Rows[rowNum]["PostCode_Text"]);
                                }
                            }

                            //Add the current address to the list.
                            erpAddressList.Add(addr);
                            rowNum++;
                        }
                    }
                }
                catch (Exception ex)
                {
                    ZNodeLogging.LogMessage("ERPCustomerService:GetShippingAddressListFromERP-> " + ex.Message);
                }
            }

            //Filter the valid address i.e. atleast having city and postal code. Will call address do not have these values
            Collection<AddressModel> shippingAdddresses = new Collection<AddressModel>();
            if (erpAddressList.Count>0)
            {
                shippingAdddresses = GetValidShippingAddress(erpAddressList);
            }
            return shippingAdddresses;
        }

        private Collection<AddressModel> GetValidShippingAddress(List<ERPAddress> erpAddressList)
        {
            Collection<AddressModel> tmpAddressList = new Collection<AddressModel>();

            foreach (var addr in erpAddressList)
            {
                
                if(!string.IsNullOrEmpty(addr.City) && !string.IsNullOrEmpty(addr.PostCode))
                {
                    AddressModel tmpAddr = new AddressModel();
                    tmpAddr.CompanyName = !string.IsNullOrEmpty(addr.AddressLine1) && !Regex.IsMatch(addr.AddressLine1, @"^\d+$") ? addr.AddressLine1:addr.TextFreeField1;
                    tmpAddr.City = addr.City;
                    tmpAddr.CountryCode = addr.Country;
                    tmpAddr.IsDefaultShipping = true;
                    tmpAddr.PhoneNumber = addr.Phone;
                    tmpAddr.PostalCode = addr.PostCode;
                    tmpAddr.StateCode = addr.State;
                    tmpAddr.StreetAddress1 = addr.AddressLine2;
                    //As per the logic suggested by client if addressline1 contain numeric value then it will be part of street address
                    if(!string.IsNullOrEmpty(addr.AddressLine1) && Regex.IsMatch(addr.AddressLine1, @"^\d+$"))
                    {
                        tmpAddr.StreetAddress1 = addr.AddressLine1 + " "+ tmpAddr.StreetAddress1;
                    }

                    tmpAddr.StreetAddress2 = addr.AddressLine3;

                    tmpAddr.AddressExtn.CreatedDate = addr.CreatedDate;
                    tmpAddr.AddressExtn.ModifiedDate = addr.ModifiedDate;
                    tmpAddr.AddressExtn.ERPAddressID = addr.ID;
                    
                    tmpAddressList.Add(tmpAddr);
                }
            }
            return tmpAddressList;
        }

        private class ERPAddress
        {
            public string Account { get; set; }
            public string AddressLine1 { get; set; }
            public string AddressLine2 { get; set; }
            public string AddressLine3 { get; set; }
            public string City { get; set; }
            public string State { get; set; }
            public string Country { get; set; }
            public string CompanyName { get; set; }
            public DateTime CreatedDate { get; set; }
            public DateTime ModifiedDate { get; set; }
            public string ID { get; set; }
            public string Phone { get; set; }
            public string PostCode { get; set; }
            public string TextFreeField1 { get; set; }
        }
    }

    
}
