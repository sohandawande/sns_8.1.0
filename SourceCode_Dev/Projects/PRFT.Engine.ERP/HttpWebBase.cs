﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data;
using Znode.Engine.Api.Models;
using System.Net;

namespace PRFT.Engine.ERP
{
    public class HttpWebBase
    {
        #region Public Properties
        public string BaseUrl
        {
            get { return ConfigurationManager.AppSettings["BaseUrl"]; }
        }

        public string UserName
        {
            get { return ConfigurationManager.AppSettings["UserName"]; }
        }

        public string Password
        {
            get { return ConfigurationManager.AppSettings["Password"]; }
        }

        public string DomainName
        {
            get { return ConfigurationManager.AppSettings["DomainName"]; }
        }

        public bool HasError { get; set; }
        public string StatusCode { get; set; }
        public string StatusDescription { get; set; }

        public string InventoryEndPoint
        {
            get
            {
                return ConfigurationManager.AppSettings["InventoryEndPoint"];
            }
        }

        public string ShippingAddressListEndPoint
        {
            get
            {
                return ConfigurationManager.AppSettings["ShippingAddressListEndPoint"];
            }
        }

        public string AccountAndBillingDetailsEndPoint
        {
            get
            {
                return ConfigurationManager.AppSettings["AccountAndBillingDetailsEndPoint"];
            }
        }

        public string PricingWithPriceCode1EndPoint
        {
            get
            {
                return ConfigurationManager.AppSettings["PricingWithPriceCode1EndPoint"];
            }
        }

        public string PricingWithPriceCode3EndPoint
        {
            get
            {
                return ConfigurationManager.AppSettings["PricingWithPriceCode3EndPoint"];
            }
        }

        public string PricingWithPriceCode6EndPoint
        {
            get
            {
                return ConfigurationManager.AppSettings["PricingWithPriceCode6EndPoint"];
            }
        }

        public string OrderHeaderEndPoint
        {
            get
            {
                return ConfigurationManager.AppSettings["OrderHeaderEndPoint"];
            }
        }

        public string OrderLinesEndPoint
        {
            get
            {
                return ConfigurationManager.AppSettings["OrderLinesEndPoint"];
            }
        }
        public string OrderHeaderByOrderIDEndPoint
        {
            get
            {
                return ConfigurationManager.AppSettings["OrderHeaderByOrderIDEndPoint"];
            }
        }

        public string InvoiceHeaderEndPoint
        {
            get
            {
                return ConfigurationManager.AppSettings["InvoiceHeaderEndPoint"];
            }
        }

        public string InvoiceItemsEndPoint
        {
            get
            {
                return ConfigurationManager.AppSettings["InvoiceItemsEndPoint"];
            }
        }



        public DataSet GetResourceFromEndpoint(string endpoint)
        {
            DataSet dsResult = null;
            
            try
            {
                // Create the Request
                HttpWebRequest request = WebRequest.Create(endpoint) as HttpWebRequest;
                request.KeepAlive = false;

                // Set the Windows Authentication credentials
                request.Credentials = new NetworkCredential(UserName, Password, DomainName);
                int requestTimeout = 30000;
                int.TryParse(ConfigurationManager.AppSettings["ERPRequestTimeout"],out requestTimeout);
                request.Timeout = requestTimeout;

                // Get the Response
                using (var webResponse = request.GetResponse() as HttpWebResponse)
                {
                    if (webResponse.StatusCode.ToString().ToUpper().Equals("OK"))
                    {
                        //Load data into a dataset  
                        dsResult = new DataSet();
                        dsResult.ReadXml(webResponse.GetResponseStream());
                    }
                    else
                    {
                        HasError = true;
                        StatusCode = webResponse.StatusCode.ToString();
                        StatusDescription = webResponse.StatusDescription;
                    }
                }
            }
            catch (Exception ex)
            {
                HasError = true;
                StatusDescription = ex.ToString();
            }

            return dsResult;
        }

        /// <summary>
        /// Creates filter query strings for the Inventory.
        /// </summary>
        /// <param name="itemNumber"></param>
        /// <returns></returns>
        public string GetInventoryQueryString(string itemNumber)
        {
            string queryString = string.Empty;
            string[] items = itemNumber.Split(',');
            int itemCount=items.Length;
            for(int index=0;index<itemCount;index++)
            {
                if (index < itemCount - 1)
                {
                    queryString = queryString + "ItemNumber eq '" + items[index] + "' or ";
                }
                else
                {
                    queryString = queryString + "ItemNumber eq '" + items[index] + "'";
                }
            }

            return queryString;
        }

        #endregion
    }
}
