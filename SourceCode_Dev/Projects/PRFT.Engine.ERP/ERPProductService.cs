﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using ZNode.Libraries.ECommerce.Utilities;

namespace PRFT.Engine.ERP
{
    public class ERPProductService
    {
        public PRFTERPItemDetailsModel GetProductPriceFromERP(string associatedCustomerExternalId, string productNum, string customerType)
        {
            PRFTERPItemDetailsModel itemDetails = new PRFTERPItemDetailsModel();
            itemDetails.IsCallForPricing = false;
            bool isAnyERPError = false;
            ServiceHelper service = new ServiceHelper();
            DataSet ds = new DataSet();

            //Try with the code type 1 option
            ds = service.GetProductPriceOfCodeType1FromERP(associatedCustomerExternalId, productNum,out isAnyERPError);

            if (ds == null)
            {
                //Try with the code type 3 option
                ds = service.GetProductPriceOfCodeType3FromERP(productNum, customerType, out isAnyERPError);
            }

            if (ds == null)
            {
                ds = service.GetProductPriceOfCodeType6FromERP(productNum, out isAnyERPError);
            }

            if (ds != null)
            {
                try
                {
                    if (ds.Tables["properties"] != null && ds.Tables["properties"].Rows.Count > 0)
                    {
                        itemDetails.UnitPrice = Convert.ToDecimal(ds.Tables["properties"].Rows[0]["PriceOrDiscount1"]);
                    }
                }
                catch (Exception ex)
                {
                    isAnyERPError = true;
                    ZNodeLogging.LogMessage("ERPProductService:GetProductPriceFromERP-> " + ex.Message);
                }
            }

            itemDetails.HasError = isAnyERPError; //Has Error is true in case ERP is down or wrong request sent then we need to use the generic price and allow submit order.

            if (isAnyERPError == false && itemDetails.UnitPrice <= 0)
            {
                itemDetails.IsCallForPricing = true;
            }
            
            return itemDetails;
        }
    }
}
