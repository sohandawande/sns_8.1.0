﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Enum;
using ZNode.Libraries.ECommerce.Utilities;

namespace PRFT.Engine.ERP
{
    public class ERPInventoryService
    {
        public void UpdateInventoryDetailsFromERP(PRFTInventoryListModel list)
        {
            string queryString = string.Join(",", list.Inventories.Select(item => item.ProductNum));
            DataTable dt = new DataTable();
            dt = new ServiceHelper().GetInventory(queryString);

            if (dt != null && dt.Rows.Count > 0)
            {
                try
                {
                    foreach (PRFTInventoryModel item in list.Inventories)
                    {
                        item.Milwaukee = 0;
                        item.Bloomington = 0;
                        item.Nebraska = 0;
                        item.DesMoines = 0;
                        List<DataRow> milwaukeeInventory = dt.Rows.Cast<DataRow>().Where(x => Convert.ToString(x["ItemNumber"]).Trim() == item.ProductNum.Trim() && Convert.ToString(x["Location"]).Trim() == "01").ToList<DataRow>();
                        List<DataRow> bloomingtonInventory = dt.Rows.Cast<DataRow>().Where(x => Convert.ToString(x["ItemNumber"]).Trim() == item.ProductNum.Trim() && Convert.ToString(x["Location"]).Trim() == "02").ToList<DataRow>();
                        List<DataRow> nebraskaInventory = dt.Rows.Cast<DataRow>().Where(x => Convert.ToString(x["ItemNumber"]).Trim() == item.ProductNum.Trim() && Convert.ToString(x["Location"]).Trim() == "04").ToList<DataRow>();
                        List<DataRow> desmoinesInventory = dt.Rows.Cast<DataRow>().Where(x => Convert.ToString(x["ItemNumber"]).Trim() == item.ProductNum.Trim() && Convert.ToString(x["Location"]).Trim() == "05").ToList<DataRow>();
                        if (milwaukeeInventory.Count > 0)
                        {
                            int qohAtMilwaukee = 0;
                            int qAllocatedAtMilwaukee = 0;
                            int.TryParse(Convert.ToString(milwaukeeInventory[0]["QuantityOnHand"]), out qohAtMilwaukee);
                            int.TryParse(Convert.ToString(milwaukeeInventory[0]["QuantityAllocated"]), out qAllocatedAtMilwaukee);
                            item.Milwaukee = ((qohAtMilwaukee-qAllocatedAtMilwaukee)<0)?0: qohAtMilwaukee - qAllocatedAtMilwaukee;
                        }

                        if (bloomingtonInventory.Count > 0)
                        {
                            int qohAtBloomington = 0;
                            int qAllocatedAtBloomington = 0;
                            int.TryParse(Convert.ToString(bloomingtonInventory[0]["QuantityOnHand"]), out qohAtBloomington);
                            int.TryParse(Convert.ToString(bloomingtonInventory[0]["QuantityAllocated"]), out qAllocatedAtBloomington);
                            item.Bloomington = ((qohAtBloomington - qAllocatedAtBloomington) < 0) ? 0 : qohAtBloomington - qAllocatedAtBloomington;
                        }

                        if (nebraskaInventory.Count > 0)
                        {
                            int qohAtNebraska = 0;
                            int qAllocatedAtNebraska = 0;
                            int.TryParse(Convert.ToString(nebraskaInventory[0]["QuantityOnHand"]), out qohAtNebraska);
                            int.TryParse(Convert.ToString(nebraskaInventory[0]["QuantityAllocated"]), out qAllocatedAtNebraska);
                            item.Nebraska = ((qohAtNebraska - qAllocatedAtNebraska) < 0) ? 0 : qohAtNebraska - qAllocatedAtNebraska;
                        }

                        if (desmoinesInventory.Count > 0)
                        {
                            int qohAtDesMoines = 0;
                            int qAllocatedAtDesMoines = 0;
                            int.TryParse(Convert.ToString(desmoinesInventory[0]["QuantityOnHand"]), out qohAtDesMoines);
                            int.TryParse(Convert.ToString(desmoinesInventory[0]["QuantityAllocated"]), out qAllocatedAtDesMoines);
                            item.DesMoines = ((qohAtDesMoines - qAllocatedAtDesMoines) < 0) ? 0 : qohAtDesMoines - qAllocatedAtDesMoines;
                        }
                    }
                }
                catch (Exception ex)
                {
                    ZNodeLogging.LogMessage("ERPInventoryService=>UpdateInventoryDetailsFromERP: " + ex.Message);
                }
            }
        }

        public PRFTInventoryModel GetInventoryDetails(string productNum)
        {

            PRFTInventoryModel inventoryDetails = null;
            DataTable dt = new ServiceHelper().GetInventory(productNum);

            if (dt != null && dt.Rows.Count > 0)
            {
                try
                {
                    inventoryDetails = new PRFTInventoryModel();
                    inventoryDetails.ProductNum = productNum;
                    inventoryDetails.Milwaukee = 0;
                    inventoryDetails.Bloomington = 0;
                    inventoryDetails.Nebraska = 0;
                    inventoryDetails.DesMoines = 0;
                    List<DataRow> milwaukeeInventory = dt.Rows.Cast<DataRow>().Where(x => Convert.ToString(x["ItemNumber"]).Trim() == productNum.Trim() && Convert.ToString(x["Location"]).Trim() == "01").ToList<DataRow>();
                    List<DataRow> bloomingtonInventory = dt.Rows.Cast<DataRow>().Where(x => Convert.ToString(x["ItemNumber"]).Trim() == productNum.Trim() && Convert.ToString(x["Location"]).Trim() == "02").ToList<DataRow>();
                    List<DataRow> nebraskaInventory = dt.Rows.Cast<DataRow>().Where(x => Convert.ToString(x["ItemNumber"]).Trim() == productNum.Trim() && Convert.ToString(x["Location"]).Trim() == "04").ToList<DataRow>();
                    List<DataRow> desmoinesInventory = dt.Rows.Cast<DataRow>().Where(x => Convert.ToString(x["ItemNumber"]).Trim() == productNum.Trim() && Convert.ToString(x["Location"]).Trim() == "05").ToList<DataRow>();
                    if (milwaukeeInventory.Count > 0)
                    {
                        int qohAtMilwaukee = 0;
                        int qAllocatedAtMilwaukee = 0;
                        int.TryParse(Convert.ToString(milwaukeeInventory[0]["QuantityOnHand"]), out qohAtMilwaukee);
                        int.TryParse(Convert.ToString(milwaukeeInventory[0]["QuantityAllocated"]), out qAllocatedAtMilwaukee);
                        inventoryDetails.Milwaukee = ((qohAtMilwaukee - qAllocatedAtMilwaukee) < 0) ? 0 : qohAtMilwaukee - qAllocatedAtMilwaukee;
                    }

                    if (bloomingtonInventory.Count > 0)
                    {
                        int qohAtBloomington = 0;
                        int qAllocatedAtBloomington = 0;
                        int.TryParse(Convert.ToString(bloomingtonInventory[0]["QuantityOnHand"]), out qohAtBloomington);
                        int.TryParse(Convert.ToString(bloomingtonInventory[0]["QuantityAllocated"]), out qAllocatedAtBloomington);
                        inventoryDetails.Bloomington = ((qohAtBloomington - qAllocatedAtBloomington) < 0) ? 0 : qohAtBloomington - qAllocatedAtBloomington;
                    }

                    if (nebraskaInventory.Count > 0)
                    {
                        int qohAtNebraska = 0;
                        int qAllocatedAtNebraska = 0;
                        int.TryParse(Convert.ToString(nebraskaInventory[0]["QuantityOnHand"]), out qohAtNebraska);
                        int.TryParse(Convert.ToString(nebraskaInventory[0]["QuantityAllocated"]), out qAllocatedAtNebraska);
                        inventoryDetails.Nebraska = ((qohAtNebraska - qAllocatedAtNebraska) < 0) ? 0 : qohAtNebraska - qAllocatedAtNebraska;
                    }

                    if (desmoinesInventory.Count > 0)
                    {
                        int qohAtDesMoines = 0;
                        int qAllocatedAtDesMoines = 0;
                        int.TryParse(Convert.ToString(desmoinesInventory[0]["QuantityOnHand"]), out qohAtDesMoines);
                        int.TryParse(Convert.ToString(desmoinesInventory[0]["QuantityAllocated"]), out qAllocatedAtDesMoines);
                        inventoryDetails.DesMoines = ((qohAtDesMoines - qAllocatedAtDesMoines) < 0) ? 0 : qohAtDesMoines - qAllocatedAtDesMoines;
                    }
                }
                catch (Exception ex)
                {
                    ZNodeLogging.LogMessage("ERPInventoryService=>GetInventoryDetails: " + ex.Message);
                }
            }
            return inventoryDetails;
        }
    }
}
