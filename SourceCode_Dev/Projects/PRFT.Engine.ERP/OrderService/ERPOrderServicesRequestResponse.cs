﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRFT.Engine.ERP.OrderService
{
    public class ERPOrderServicesRequestResponse
    {
    }

    public class ERPOrderServicesResponse
    {
        public string NewOrderID { get; set; }
        public bool HasError { get; set; }
        public string StatusCode { get; set; }
        public string StatusDescription { get; set; }
    }
    public class ERPOrderServicesRequest
    {
        public OEOrderHeader OrderHeader { get; set; }
        public OEOrderLines LineItems{ get; set; }

        public ERPOrderServicesRequest()
        {
            OrderHeader = new OEOrderHeader();
            LineItems = new OEOrderLines();
        }
    }
    public class OEOrderHeader
    {
        /// <summary>
        /// Macola Account's Entity Debtor Code
        /// </summary>
        public string CustomerNumber { get; set; }
        public string BillToAddressLine1 { get; set; }
        /// <summary>
        /// Znode BillTo Address1 and Address2
        /// </summary>
        public string BillToAddressLine2 { get; set; }
        public string BillToAddressLine3 { get; set; }
        /// <summary>
        /// City, State ZipCode
        /// </summary>
        public string BillToAddressLine4 { get; set; }
        public string BillToCountry { get; set; }
        public string BillToCustomerName { get; set; }
        public string OrderDate { get; set; }
        /// <summary>
        /// Should be O
        /// </summary>
        public string OrderType { get; set; }
        public string PurchaseOrderNumber { get; set; }
        
        /// <summary>
        /// Order Level Notes - Character limit of 30 characters
        /// </summary>
        public string ShipInstruction1 { get; set; }
        public string ShipToAddressLine1 { get; set; }
        /// <summary>
        /// Znode ShipTo Address1 and Address2
        /// </summary>
        public string ShipToAddressLine2 { get; set; }
        public string ShipToAddressLine3 { get; set; }
        public string ShipToAddressLine4 { get; set; }
        public string ShipToCountry { get; set; }
        public string ShipToName { get; set; }
        /// <summary>
        /// ZnodeOrderNumber - Prefix with "W"
        /// </summary>
        public string UserDefinedField1 { get; set; }
        /// <summary>
        /// Freight Charges
        /// </summary>
        public string UserDefinedField2 { get; set; }
        /// <summary>
        /// Tax
        /// </summary>
        public string UserDefinedField3 { get; set; }
    }
    public class OEOrderLines
    {
        public List<OEOrderLine> OrderLineItems { get; set; }

        public OEOrderLines()
        {
            OrderLineItems = new List<OEOrderLine>();
        }
    }

    public class OEOrderLine
    {
        /// <summary>
        /// Macola OrderID
        /// </summary>
        public string _HeaderID { get; set; }
        /// <summary>
        /// True for last item
        /// </summary>
        public string _OrderComplete { get; set; }
        /// <summary>
        /// Product Num
        /// </summary>
        public string ItemNo { get; set; }
        public string QuantityOrdered { get; set; }
        /// <summary>
        /// ZnodeOrderNumber - Prefix with "W"
        /// </summary>
        public string UserDefinedField1 { get; set; }
        public string UnitPrice { get; set; }

        public string ItemDescription1 { get; set; }
    }

    public class Order
    {
        public ERPOrderServicesResponse ServiceResponse { get; set; }
        public string ID { get; set; }
        public string OrderNumber { get; set; }

        public Order()
        {
            ServiceResponse = new ERPOrderServicesResponse();
        }

    }
    public class OrderContainer
    {
        public List<Order> d { get; set; }

        public OrderContainer()
        {
            d = new List<Order>();
        }
    }

    public class OrderDetailResponseConatiner
    {
        public OrderList d { get; set; }

        public OrderDetailResponseConatiner()
        {
            d = new OrderList();
        }
    }

    public class OrderList
    {
        public List<Order> results { get; set; }

        public OrderList()
        {
            results = new List<Order>();
        }
    }

    #region Invoice classes

    public class OrderInvoiceResponseConatiner
    {
        public OrderInvoiceList d { get; set; }

        public OrderInvoiceResponseConatiner()
        {
            d = new OrderInvoiceList();
        }
    }

    public class OrderInvoiceList
    {
        public List<ERPHeaderHistory> results { get; set; }

        public OrderInvoiceList()
        {
            results = new List<ERPHeaderHistory>();
        }
    }

    public class OrderInvoice: ERPOrderServicesResponse
    {
        public ERPHeaderHistory InvoiceHeaderDetails { get; set; }
        public List<ERPLineHistory> InvoiceItems { get; set; } 

        public OrderInvoice()
        {
            InvoiceHeaderDetails = new ERPHeaderHistory();
            InvoiceItems = new List<ERPLineHistory>();
        }
    }

    public class ERPHeaderHistory
    {
        public string InvoiceNumber { get; set; }
        public string InvoiceDate { get; set; }
        public string BillToCustomerName { get; set; }
        public string BillToAddressLine1 { get; set; }
        public string BillToAddressLine2 { get; set; }
        public string BillToAddressLine3 { get; set; }
        public string BillToAddressLine4 { get; set; }
        public string BillToCity { get; set; }
        public string BillToState { get; set; }
        public string BillToZip { get; set; }
        public string BillToCountry { get; set; }
        public string ShipToName { get; set; }
        public string ShipToAddressLine1 { get; set; }
        public string ShipToAddressLine2 { get; set; }
        public string ShipToAddressLine3 { get; set; }
        public string ShipToAddressLine4 { get; set; }
        public string ShipToCity { get; set; }
        public string ShipToState { get; set; }
        public string ShipToZip { get; set; }
        public string ShipToCountry { get; set; }
        public string OrderNumber { get; set; }
        public string OrderDate { get; set; }
        public string CustomerNumber { get; set; }
        public string ManufacturingLocation { get; set; }
        public string SalespersonNumber { get; set; }
        public string PurchaseOrderNumber { get; set; }
        public string JobNumber { get; set; }
        public string ShipViaCode { get; set; }
        public string FreightPaymentCode { get; set; }
        public string TotalSaleAmt { get; set; }
        public string MiscellaneousAmount { get; set; }
        public string FreightAmount { get; set; }
        public string SalesTaxAmount1 { get; set; }
        public string SalesTaxAmount2 { get; set; }
        public string SalesTaxAmount3 { get; set; }
        public string PaymentAmount { get; set; }
        public string PaymentDiscountAmount { get; set; }
        public string AccountsReceivableTermsCode { get; set; }
        public string CommentLine1 { get; set; }
        public string CommentLine2 { get; set; }
        public string CommentLine3 { get; set; }
    }

    public class ERPLineHistory
    {
        public string ItemNo { get; set; }
        public string ItemDescription1 { get; set; }
        public string ItemDescription2 { get; set; }
        public string QuantityOrdered { get; set; }
        public string QuantityToShip { get; set; }
        public string QuantityReturnedtoStock { get; set; }
        public string QuantityBackordered { get; set; }
        public string UnitPrice { get; set; }
        public string DiscountPercent { get; set; }
        public string UnitOfMeasure { get; set; }
        public string SalesAmount { get; set; }
    }
    
    public class OrderInvoiceLineItems:ERPOrderServicesResponse
    {
        public List<ERPLineHistory> InvoiceItems { get; set; }

        public OrderInvoiceLineItems()
        {
            InvoiceItems = new List<ERPLineHistory>();
        }
    }

    public class OrderInvoiceItemsResponseConatiner
    {
        public OrderInvoiceItemList d { get; set; }

        public OrderInvoiceItemsResponseConatiner()
        {
            d = new OrderInvoiceItemList();
        }
    }

    public class OrderInvoiceItemList
    {
        public List<ERPLineHistory> results { get; set; }

        public OrderInvoiceItemList()
        {
            results = new List<ERPLineHistory>();
        }
    }

    #endregion
}
