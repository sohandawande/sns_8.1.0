﻿using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRFT.Engine.ERP.OrderService
{
    public class ERPOrderService:IERPOrderService
    {
        public ERPOrderServicesResponse SubmitOrderToERP(ERPOrderServicesRequest request)
        {
            ERPOrderServicesResponse response = new ERPOrderServicesResponse();
            try
            {
                HttpWebBase baseObject = new HttpWebBase();
                // Set up the REST connection
                var client = new RestClient(baseObject.BaseUrl);
                client.Authenticator = new NtlmAuthenticator(baseObject.UserName, baseObject.Password);
                // set up the order header
                var restRequest = new RestRequest(baseObject.OrderHeaderEndPoint, Method.POST);
                restRequest.RequestFormat = DataFormat.Json;
                restRequest.AddBody(request.OrderHeader);
                
                // Insert the order header
                IRestResponse<OrderContainer> restResponse = client.Execute<OrderContainer>(restRequest);
                response.StatusCode = Convert.ToString(restResponse.StatusCode);

                response.StatusDescription = restResponse.StatusDescription;

                if (restResponse.StatusCode == System.Net.HttpStatusCode.Created)
                {
                    response.NewOrderID=restResponse.Data.d[0].ID;
                    response.HasError = false;

                    foreach (OEOrderLine lineItem in request.LineItems.OrderLineItems)
                    {
                        //Add an item to the order
                        var restRequest2 = new RestRequest("OEOrderLine", Method.POST);
                        restRequest2.RequestFormat = DataFormat.Json;
                        lineItem._HeaderID = restResponse.Data.d[0].ID;
                        restRequest2.AddBody(lineItem);
                        
                        // Insert the order Line
                        IRestResponse restResponse2 = client.Execute(restRequest2);
                    }
                }
                else
                {
                    response.HasError = true;
                    response.StatusDescription += Environment.NewLine+"<br/>Response Content: " + restResponse.Content;
                }
            }
            catch(Exception ex)
            {
                response.StatusCode = "Error";
                response.StatusDescription = ex.ToString();
                response.HasError = true;
            }

            return response;
        }

        public Order GetOrderDetailsByID(string OrderID)
        {
            Order response = new Order(); 
            ERPOrderServicesResponse serviceResponse = new ERPOrderServicesResponse();
            try
            {
                HttpWebBase baseObject = new HttpWebBase();
                // Set up the REST connection
                var client = new RestClient(baseObject.BaseUrl);
                client.Authenticator = new NtlmAuthenticator(baseObject.UserName, baseObject.Password);
                // set up the order header
                string endPoint = string.Format(baseObject.OrderHeaderByOrderIDEndPoint, OrderID);
                var restRequest = new RestRequest(endPoint, Method.GET);
                restRequest.RequestFormat = DataFormat.Json;

                // Insert the order header
                IRestResponse<OrderDetailResponseConatiner> restResponse = client.Execute<OrderDetailResponseConatiner>(restRequest);

                serviceResponse.StatusCode = Convert.ToString(restResponse.StatusCode);
                serviceResponse.StatusDescription = restResponse.StatusDescription;

                if (restResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    if (restResponse.Data.d.results.Count > 0)
                    {
                        response.OrderNumber = restResponse.Data.d.results[0].OrderNumber;
                        serviceResponse.HasError = false;
                    }
                }
                else
                {
                    serviceResponse.HasError = true;
                    serviceResponse.StatusDescription += Environment.NewLine + "<br/>Response Content: " + restResponse.Content;
                }
            }
            catch (Exception ex)
            {
                serviceResponse.StatusCode = "Error";
                serviceResponse.StatusDescription = ex.ToString();
                serviceResponse.HasError = true;
            }

            response.ServiceResponse = serviceResponse;
            return response;
        }

        #region Order Invoice
        public OrderInvoice GetOrderHistory(string erpOrderNumber)
        {
            OrderInvoice response = new OrderInvoice();
            
            try
            {
                HttpWebBase baseObject = new HttpWebBase();
                // Set up the REST connection
                var client = new RestClient(baseObject.BaseUrl);
                client.Authenticator = new NtlmAuthenticator(baseObject.UserName, baseObject.Password);

                int invoiceAndOrderNumberLength = 8;
                int.TryParse(ConfigurationManager.AppSettings["InvoiceAndOrderNumberLength"], out invoiceAndOrderNumberLength);
                string endPoint = string.Format(baseObject.InvoiceHeaderEndPoint, erpOrderNumber.PadLeft(invoiceAndOrderNumberLength));
                var restRequest = new RestRequest(endPoint, Method.GET);
                restRequest.RequestFormat = DataFormat.Json;

                // Insert the order header
                IRestResponse<OrderInvoiceResponseConatiner> restResponse = client.Execute<OrderInvoiceResponseConatiner>(restRequest);

                response.StatusCode = Convert.ToString(restResponse.StatusCode);
                response.StatusDescription = restResponse.StatusDescription;

                if (restResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    if (restResponse.Data.d.results.Count > 0)
                    {
                        response.HasError = false;
                        var restResponseOrderInvoiceObject = restResponse.Data.d.results[0];
                        response.InvoiceHeaderDetails.InvoiceNumber = string.IsNullOrEmpty(restResponseOrderInvoiceObject.InvoiceNumber) ? string.Empty : restResponseOrderInvoiceObject.InvoiceNumber.Trim(); 
                        response.InvoiceHeaderDetails.InvoiceDate = GetStringDate(restResponseOrderInvoiceObject.InvoiceDate);
                        response.InvoiceHeaderDetails.BillToCustomerName = string.IsNullOrEmpty(restResponseOrderInvoiceObject.BillToCustomerName) ? string.Empty : restResponseOrderInvoiceObject.BillToCustomerName.Trim();
                        response.InvoiceHeaderDetails.BillToAddressLine1 = string.IsNullOrEmpty(restResponseOrderInvoiceObject.BillToAddressLine1) ? string.Empty : restResponseOrderInvoiceObject.BillToAddressLine1.Trim();
                        response.InvoiceHeaderDetails.BillToAddressLine2 = string.IsNullOrEmpty(restResponseOrderInvoiceObject.BillToAddressLine2) ? string.Empty : restResponseOrderInvoiceObject.BillToAddressLine2.Trim();
                        response.InvoiceHeaderDetails.BillToAddressLine3 = string.IsNullOrEmpty(restResponseOrderInvoiceObject.BillToAddressLine3) ? string.Empty : restResponseOrderInvoiceObject.BillToAddressLine3.Trim();
                        response.InvoiceHeaderDetails.BillToAddressLine4 = string.IsNullOrEmpty(restResponseOrderInvoiceObject.BillToAddressLine4) ? string.Empty : restResponseOrderInvoiceObject.BillToAddressLine4.Trim();
                        response.InvoiceHeaderDetails.BillToCity = string.IsNullOrEmpty(restResponseOrderInvoiceObject.BillToCity) ? string.Empty : restResponseOrderInvoiceObject.BillToCity.Trim();
                        response.InvoiceHeaderDetails.BillToState = string.IsNullOrEmpty(restResponseOrderInvoiceObject.BillToState) ? string.Empty : restResponseOrderInvoiceObject.BillToState.Trim();
                        response.InvoiceHeaderDetails.BillToZip = string.IsNullOrEmpty(restResponseOrderInvoiceObject.BillToZip) ? string.Empty : restResponseOrderInvoiceObject.BillToZip.Trim();
                        response.InvoiceHeaderDetails.BillToCountry = string.IsNullOrEmpty(restResponseOrderInvoiceObject.BillToCountry) ? string.Empty : restResponseOrderInvoiceObject.BillToCountry.Trim();
                        response.InvoiceHeaderDetails.ShipToName = string.IsNullOrEmpty(restResponseOrderInvoiceObject.ShipToName) ? string.Empty : restResponseOrderInvoiceObject.ShipToName.Trim();
                        response.InvoiceHeaderDetails.ShipToAddressLine1 = string.IsNullOrEmpty(restResponseOrderInvoiceObject.ShipToAddressLine1) ? string.Empty : restResponseOrderInvoiceObject.ShipToAddressLine1.Trim();
                        response.InvoiceHeaderDetails.ShipToAddressLine2 = string.IsNullOrEmpty(restResponseOrderInvoiceObject.ShipToAddressLine2) ? string.Empty : restResponseOrderInvoiceObject.ShipToAddressLine2.Trim();
                        response.InvoiceHeaderDetails.ShipToAddressLine3 = string.IsNullOrEmpty(restResponseOrderInvoiceObject.ShipToAddressLine3) ? string.Empty : restResponseOrderInvoiceObject.ShipToAddressLine3.Trim();
                        response.InvoiceHeaderDetails.ShipToAddressLine4 = string.IsNullOrEmpty(restResponseOrderInvoiceObject.ShipToAddressLine4) ? string.Empty : restResponseOrderInvoiceObject.ShipToAddressLine4.Trim();
                        response.InvoiceHeaderDetails.ShipToCity = string.IsNullOrEmpty(restResponseOrderInvoiceObject.ShipToCity) ? string.Empty : restResponseOrderInvoiceObject.ShipToCity.Trim();
                        response.InvoiceHeaderDetails.ShipToState = string.IsNullOrEmpty(restResponseOrderInvoiceObject.ShipToState) ? string.Empty : restResponseOrderInvoiceObject.ShipToState.Trim();
                        response.InvoiceHeaderDetails.ShipToZip = string.IsNullOrEmpty(restResponseOrderInvoiceObject.ShipToZip) ? string.Empty : restResponseOrderInvoiceObject.ShipToZip.Trim();
                        response.InvoiceHeaderDetails.ShipToCountry = string.IsNullOrEmpty(restResponseOrderInvoiceObject.ShipToCountry) ? string.Empty : restResponseOrderInvoiceObject.ShipToCountry.Trim();
                        response.InvoiceHeaderDetails.OrderNumber = string.IsNullOrEmpty(restResponseOrderInvoiceObject.OrderNumber) ? string.Empty : restResponseOrderInvoiceObject.OrderNumber.Trim();
                        response.InvoiceHeaderDetails.OrderDate = GetStringDate(restResponseOrderInvoiceObject.OrderDate);
                        response.InvoiceHeaderDetails.CustomerNumber = string.IsNullOrEmpty(restResponseOrderInvoiceObject.CustomerNumber) ? string.Empty : restResponseOrderInvoiceObject.CustomerNumber.Trim();
                        response.InvoiceHeaderDetails.ManufacturingLocation = string.IsNullOrEmpty(restResponseOrderInvoiceObject.ManufacturingLocation) ? string.Empty : restResponseOrderInvoiceObject.ManufacturingLocation.Trim();
                        response.InvoiceHeaderDetails.SalespersonNumber = string.IsNullOrEmpty(restResponseOrderInvoiceObject.SalespersonNumber) ? string.Empty : restResponseOrderInvoiceObject.SalespersonNumber.Trim();
                        response.InvoiceHeaderDetails.PurchaseOrderNumber = string.IsNullOrEmpty(restResponseOrderInvoiceObject.PurchaseOrderNumber) ? string.Empty : restResponseOrderInvoiceObject.PurchaseOrderNumber.Trim();
                        response.InvoiceHeaderDetails.JobNumber = string.IsNullOrEmpty(restResponseOrderInvoiceObject.JobNumber) ? string.Empty : restResponseOrderInvoiceObject.JobNumber.Trim();
                        response.InvoiceHeaderDetails.ShipViaCode = string.IsNullOrEmpty(restResponseOrderInvoiceObject.ShipViaCode) ? string.Empty : restResponseOrderInvoiceObject.ShipViaCode.Trim();
                        response.InvoiceHeaderDetails.FreightPaymentCode = string.IsNullOrEmpty(restResponseOrderInvoiceObject.FreightPaymentCode) ? string.Empty : restResponseOrderInvoiceObject.FreightPaymentCode.Trim();
                        response.InvoiceHeaderDetails.TotalSaleAmt = string.IsNullOrEmpty(restResponseOrderInvoiceObject.TotalSaleAmt) ?"0.0" : restResponseOrderInvoiceObject.TotalSaleAmt.Trim();
                        response.InvoiceHeaderDetails.MiscellaneousAmount = string.IsNullOrEmpty(restResponseOrderInvoiceObject.MiscellaneousAmount) ? "0.0" : restResponseOrderInvoiceObject.MiscellaneousAmount.Trim();
                        response.InvoiceHeaderDetails.FreightAmount = string.IsNullOrEmpty(restResponseOrderInvoiceObject.FreightAmount) ?"0.0" : restResponseOrderInvoiceObject.FreightAmount.Trim();

                        response.InvoiceHeaderDetails.SalesTaxAmount1 = string.IsNullOrEmpty(restResponseOrderInvoiceObject.SalesTaxAmount1) ? "0.0" : restResponseOrderInvoiceObject.SalesTaxAmount1.Trim();
                        response.InvoiceHeaderDetails.SalesTaxAmount2 = string.IsNullOrEmpty(restResponseOrderInvoiceObject.SalesTaxAmount2) ? "0.0" : restResponseOrderInvoiceObject.SalesTaxAmount2.Trim();
                        response.InvoiceHeaderDetails.SalesTaxAmount3 = string.IsNullOrEmpty(restResponseOrderInvoiceObject.SalesTaxAmount3) ? "0.0" : restResponseOrderInvoiceObject.SalesTaxAmount3.Trim();
                        response.InvoiceHeaderDetails.PaymentAmount = string.IsNullOrEmpty(restResponseOrderInvoiceObject.PaymentAmount) ? "0.0" : restResponseOrderInvoiceObject.PaymentAmount.Trim();
                        response.InvoiceHeaderDetails.PaymentDiscountAmount = string.IsNullOrEmpty(restResponseOrderInvoiceObject.PaymentDiscountAmount) ? "0.0" : restResponseOrderInvoiceObject.PaymentDiscountAmount.Trim();
                        response.InvoiceHeaderDetails.AccountsReceivableTermsCode = string.IsNullOrEmpty(restResponseOrderInvoiceObject.AccountsReceivableTermsCode) ? string.Empty : restResponseOrderInvoiceObject.AccountsReceivableTermsCode.Trim();
                        response.InvoiceHeaderDetails.CommentLine1 = string.IsNullOrEmpty(restResponseOrderInvoiceObject.CommentLine1) ? string.Empty : restResponseOrderInvoiceObject.CommentLine1.Trim();
                        response.InvoiceHeaderDetails.CommentLine2 = string.IsNullOrEmpty(restResponseOrderInvoiceObject.CommentLine2) ? string.Empty : restResponseOrderInvoiceObject.CommentLine2.Trim();
                        response.InvoiceHeaderDetails.CommentLine3 = string.IsNullOrEmpty(restResponseOrderInvoiceObject.CommentLine3) ? string.Empty : restResponseOrderInvoiceObject.CommentLine3.Trim();
                    }
                    else
                    {
                        response.HasError = true;
                        response.StatusDescription += Environment.NewLine + "<br/>Response Content: No Invoice details found for the ERP Order No:"+ erpOrderNumber;
                    }
                }
                else
                {
                    response.HasError = true;
                    response.StatusDescription += Environment.NewLine + "<br/>Response Content: " + restResponse.Content;
                }
            }
            catch (Exception ex)
            {
                response.StatusCode = "Error";
                response.StatusDescription = ex.ToString();
                response.HasError = true;
            }
            
            return response;
        }
        
        public OrderInvoiceLineItems GetOrderLineHistory(string invoiceNumber)
        {
            OrderInvoiceLineItems response = new OrderInvoiceLineItems();

            try
            {
                HttpWebBase baseObject = new HttpWebBase();
                // Set up the REST connection
                var client = new RestClient(baseObject.BaseUrl);
                client.Authenticator = new NtlmAuthenticator(baseObject.UserName, baseObject.Password);

                int invoiceAndOrderNumberLength = 8;
                int.TryParse(ConfigurationManager.AppSettings["InvoiceAndOrderNumberLength"], out invoiceAndOrderNumberLength);
                string endPoint = string.Format(baseObject.InvoiceItemsEndPoint, invoiceNumber.PadLeft(invoiceAndOrderNumberLength));
                var restRequest = new RestRequest(endPoint, Method.GET);
                restRequest.RequestFormat = DataFormat.Json;

                // Insert the order header
                IRestResponse<OrderInvoiceItemsResponseConatiner> restResponse = client.Execute<OrderInvoiceItemsResponseConatiner>(restRequest);

                response.StatusCode = Convert.ToString(restResponse.StatusCode);
                response.StatusDescription = restResponse.StatusDescription;

                if (restResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    if (restResponse.Data.d.results.Count > 0)
                    {
                        response.HasError = false;
                        ERPLineHistory lineItem = new ERPLineHistory();
                        foreach (var restResponseOrderInvoiceItem in restResponse.Data.d.results)
                        {
                            lineItem = new ERPLineHistory();
                            lineItem.ItemNo = string.IsNullOrEmpty(restResponseOrderInvoiceItem.ItemNo) ? string.Empty : restResponseOrderInvoiceItem.ItemNo.Trim();
                            lineItem.ItemDescription1 = string.IsNullOrEmpty(restResponseOrderInvoiceItem.ItemDescription1) ? string.Empty : restResponseOrderInvoiceItem.ItemDescription1.Trim();
                            lineItem.ItemDescription2 = string.IsNullOrEmpty(restResponseOrderInvoiceItem.ItemDescription2) ? string.Empty : restResponseOrderInvoiceItem.ItemDescription2.Trim();
                            lineItem.QuantityOrdered = string.IsNullOrEmpty(restResponseOrderInvoiceItem.QuantityOrdered) ? "0.0" : restResponseOrderInvoiceItem.QuantityOrdered.Trim();
                            lineItem.QuantityToShip = string.IsNullOrEmpty(restResponseOrderInvoiceItem.QuantityToShip) ? "0.0" : restResponseOrderInvoiceItem.QuantityToShip.Trim();
                            lineItem.QuantityReturnedtoStock = string.IsNullOrEmpty(restResponseOrderInvoiceItem.QuantityReturnedtoStock) ? "0.0" : restResponseOrderInvoiceItem.QuantityReturnedtoStock.Trim();
                            lineItem.QuantityBackordered = string.IsNullOrEmpty(restResponseOrderInvoiceItem.QuantityBackordered) ? "0.0" : restResponseOrderInvoiceItem.QuantityBackordered.Trim();
                            lineItem.UnitPrice = string.IsNullOrEmpty(restResponseOrderInvoiceItem.UnitPrice) ? "0.0" : restResponseOrderInvoiceItem.UnitPrice.Trim();
                            lineItem.DiscountPercent = string.IsNullOrEmpty(restResponseOrderInvoiceItem.DiscountPercent) ? "0.0" : restResponseOrderInvoiceItem.DiscountPercent.Trim();
                            lineItem.UnitOfMeasure = string.IsNullOrEmpty(restResponseOrderInvoiceItem.UnitOfMeasure) ? string.Empty : restResponseOrderInvoiceItem.UnitOfMeasure.Trim();
                            lineItem.SalesAmount = string.IsNullOrEmpty(restResponseOrderInvoiceItem.SalesAmount) ? "0.0" : restResponseOrderInvoiceItem.SalesAmount.Trim();
                            response.InvoiceItems.Add(lineItem);
                        }
                    }
                    else
                    {
                        response.HasError = true;
                        response.StatusDescription += Environment.NewLine + "<br/>Response Content: No line items found within invoice for the invoice number:"+ invoiceNumber;
                    }
                }
                else
                {
                    response.HasError = true;
                    response.StatusDescription += Environment.NewLine + "<br/>Response Content: " + restResponse.Content;
                }
            }
            catch (Exception ex)
            {
                response.StatusCode = "Error";
                response.StatusDescription = ex.ToString();
                response.HasError = true;
            }

            return response;
        }

        private string GetStringDate(string inputValue)
        {
            DateTime tmpDate = new DateTime();
            string dateString = string.IsNullOrEmpty(inputValue) ? string.Empty : inputValue.Trim();
            if(dateString.Length>0)
            {
                DateTime.TryParse(dateString, out tmpDate);
                dateString = tmpDate.ToShortDateString();
            }

            return dateString;
        }
        #endregion
    }
}
