﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRFT.Engine.ERP.OrderService
{
    public interface IERPOrderService
    {
        ERPOrderServicesResponse SubmitOrderToERP(ERPOrderServicesRequest request);
        Order GetOrderDetailsByID(string OrderID);

        #region Order Invoice
        OrderInvoice GetOrderHistory(string erpOrderNumber);

        OrderInvoiceLineItems GetOrderLineHistory(string invoiceNumber);
        #endregion
    }
}
