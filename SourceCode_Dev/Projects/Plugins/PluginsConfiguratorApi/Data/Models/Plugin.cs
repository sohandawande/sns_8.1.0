﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PluginsConfiguratorApi.Data.Models
{
    public class Plugin : IEntity
    {
        public Plugin()
        {
            PluginSettings = new List<PluginSetting>();
        }

        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }
        public bool Installed { get; set; }

        public List<PluginSetting> PluginSettings { get; set; }
    }
}
