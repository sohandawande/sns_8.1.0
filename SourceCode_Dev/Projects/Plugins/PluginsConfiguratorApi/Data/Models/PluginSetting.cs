﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace PluginsConfiguratorApi.Data.Models
{
    public class PluginSetting : IEntity
    {
        public int Id { get; set; }
        public Plugin Plugin { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
