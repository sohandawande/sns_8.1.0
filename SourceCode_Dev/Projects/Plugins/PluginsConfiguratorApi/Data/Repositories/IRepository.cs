﻿using System.Linq;

namespace PluginsConfiguratorApi.Data.Repositories
{
    public interface IRepository<T>
    {
        IQueryable<T> List();
        void Save(T entity);
    }
}
