﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using PluginsConfiguratorApi.Data.Models;

namespace PluginsConfiguratorApi.Data.Repositories
{
    public class PluginConfiguratorDbContext : DbContext
    {
        public PluginConfiguratorDbContext()
            : base("DefaultConnection")
        {

        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Plugin> Plugins { get; set; }
        public DbSet<PluginSetting> PluginSettings { get; set; }
    }
}