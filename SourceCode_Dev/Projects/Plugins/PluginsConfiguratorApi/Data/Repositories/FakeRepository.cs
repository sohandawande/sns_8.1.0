﻿using System;
using System.Collections.Generic;
using System.Linq;
using PluginsConfiguratorApi.Data.Models;

namespace PluginsConfiguratorApi.Data.Repositories
{
    public class FakeRepository<T> : IRepository<T> where T : IEntity
    {
        private Dictionary<int, T> data = new Dictionary<int, T>();

        public T Get(int id)
        {
            var entity = default(T);
            data.TryGetValue(id, out entity);
            return entity;
        }

        public void Save(T entity)
        {
            if (entity.Id == 0)
            {
                entity.Id = data.Count + 1;
                data[entity.Id] = entity;
            }
            else
            {
                data[entity.Id] = entity;
            }
        }

        public void Delete(T entity)
        {
            data.Remove(entity.Id);
        }

        public IQueryable<T> List()
        {
            return data.Values.AsQueryable();
        }
    }
}
