﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Web.Http;
using Newtonsoft.Json;
using PluginsConfiguratorApi.Api.Responses;

namespace PluginsConfiguratorApi.Controllers
{
    public class BaseController : ApiController
    {
        protected HttpResponseMessage CreateOKResponse<T>(string data)
        {
            if (Indent)
            {
                // Indentation should only ever be used by humans when viewing a response in a browser,
                // so taking the performance hit (albeit a small one) with the deserialization is fine.
                var dataDeserialized = JsonConvert.DeserializeObject<T>(data);
                return Request.CreateResponse(HttpStatusCode.OK, dataDeserialized, MediaTypeFormatter);
            }

            // We use StringContent to skip the content negotiation and serializaton step (performance improvement).
            var response = new HttpResponseMessage { StatusCode = HttpStatusCode.OK, Content = new StringContent(data) };
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json"); ;
            return response;
        }
        protected HttpResponseMessage CreateNotFoundResponse()
        {
            return Request.CreateResponse(HttpStatusCode.NotFound);
        }
        protected HttpResponseMessage CreateInternalServerErrorResponse<T>(T data)
        {
            var basedata = data as BaseResponse;

            if (basedata != null)
            {
                var newEx = new Exception(basedata.ErrorCode + ":" + basedata.ErrorMessage);
                //Elmah.ErrorSignal.FromCurrentContext().Raise(newEx);
            }

            return Request.CreateResponse(HttpStatusCode.InternalServerError, data, MediaTypeFormatter);
        }

        protected static bool Indent
        {
            get { return true; }
        }

        protected static MediaTypeFormatter MediaTypeFormatter
        {
            get { return new JsonMediaTypeFormatter() { Indent = Indent }; }
        }

        protected MediaTypeHeaderValue MediaTypeHeaderValue
        {
            get { return new MediaTypeHeaderValue("application/json"); }
        }
    }
}