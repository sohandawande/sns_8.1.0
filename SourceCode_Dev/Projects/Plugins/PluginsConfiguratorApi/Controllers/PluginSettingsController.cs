﻿using System;
using System.Net.Http;
using System.Web.Http;
using PluginsConfiguratorApi.Api;
using PluginsConfiguratorApi.Api.Cache;
using PluginsConfiguratorApi.Api.Responses;
using PluginsConfiguratorApi.Api.Services;

namespace PluginsConfiguratorApi.Controllers
{
    public class PluginSettingsController : BaseController
    {
        private readonly IPluginSettingCache _cache;

        public PluginSettingsController()
        {
            _cache = new PluginSettingCache();
        }

        [HttpGet]
        public HttpResponseMessage List()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetPluginSettings();
                response = data != null ? CreateOKResponse<PluginSettingListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new PluginSettingListResponse() { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
    }
}
