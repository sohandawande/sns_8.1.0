﻿using System;
using System.Data.Entity.Migrations;
using System.IO;
using System.Reflection;
using System.Web.Hosting;
using PluginsConfiguratorApi.Data.Migrations;
using Znode.Plugin;

namespace PluginsConfiguratorApi
{
    public class PluginConfiguratorPlugin : IPlugin
    {
        public string Name
        {
            get { return Assembly.GetAssembly(GetType()).GetName().Name; }
        }

        public Version Version
        {
            get
            {
                return Assembly.GetAssembly(GetType()).GetName().Version;
            }
        }

        public string Title
        {
            get { return "Plugin Configurator"; }
        }

        public string EntryControllerName
        {
            get { return "PluginConfigurator"; }
        }

        public void Install()
        {
            var fileName = string.Format(@"{0}\{1}.json", HostingEnvironment.MapPath("~/plugins"), this.GetType().Name);

            File.Create(fileName);

            // install the database schema
            var migrationConfig = new Configuration() { AutomaticMigrationsEnabled = true };
            var migrator = new DbMigrator(migrationConfig);
            migrator.Update();
        }

        public void Uninstall()
        {
            // uninstall the database schema
            var migrationConfig = new Configuration() { AutomaticMigrationDataLossAllowed = true };
            var migrator = new DbMigrator(migrationConfig);
            migrator.Update(DbMigrator.InitialDatabase);
        }

        public int Order
        {
            get { return -100; }
        }

        public bool IsInstalled { get; set; }
    }
}
