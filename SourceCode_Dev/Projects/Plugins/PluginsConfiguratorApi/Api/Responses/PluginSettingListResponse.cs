﻿using System.Collections.ObjectModel;
using PluginsConfiguratorApi.Api.Models;

namespace PluginsConfiguratorApi.Api.Responses
{
    public class PluginSettingListResponse : BaseResponse
    {
        public Collection<PluginSettingModel> PluginSettings { get; set; }
    }
}
