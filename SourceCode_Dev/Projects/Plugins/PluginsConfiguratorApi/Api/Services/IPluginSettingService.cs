﻿using PluginsConfiguratorApi.Api.Models;

namespace PluginsConfiguratorApi.Api.Services
{
    public interface IPluginSettingService
    {
        PluginListModel GetPluginSettings();
    }
}