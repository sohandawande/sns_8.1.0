﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Newtonsoft.Json;
using PluginsConfiguratorApi.Api.Models;
using PluginsConfiguratorApi.Data.Models;
using PluginsConfiguratorApi.Data.Repositories;

namespace PluginsConfiguratorApi.Api.Services
{
    public class PluginSettingService : IPluginSettingService
    {
        private readonly IRepository<PluginSetting> _pluginRepository;

        public PluginSettingService()
        {
            _pluginRepository = new FakeRepository<PluginSetting>();

            _pluginRepository.Save(new PluginSetting()
            {
                Plugin = new Plugin() { Name = "PluginConfigurator" },
                Name = "Timeout",
                Value = "30"
            });
        }

        public PluginListModel GetPluginSettings()
        {
            var plugins = _pluginRepository.List().ToList();

            // this is really mapping code
            var list = new PluginListModel();

            plugins.ForEach(x => { list.PluginSettings.Add(new PluginSettingModel() { Id = x.Id, Name = x.Name, Value = x.Value }); });

            return list;
        }
    }
}
