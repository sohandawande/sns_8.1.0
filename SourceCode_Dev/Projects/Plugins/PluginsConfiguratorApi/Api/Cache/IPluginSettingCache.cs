namespace PluginsConfiguratorApi.Api.Cache
{
    public interface IPluginSettingCache
    {
        string GetPluginSettings();
    }
}