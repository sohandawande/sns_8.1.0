﻿using Newtonsoft.Json;
using PluginsConfiguratorApi.Api.Responses;
using PluginsConfiguratorApi.Api.Services;

namespace PluginsConfiguratorApi.Api.Cache
{
    public class PluginSettingCache : IPluginSettingCache
    {
        private IPluginSettingService _service;
        public PluginSettingCache()
        {
            _service = new PluginSettingService();
        }

        public string GetPluginSettings()
        {
            string data = null;
            // var data = GetFromCache(routeUri); 

            if (data == null)
            {
                var list = _service.GetPluginSettings();
                if (list.PluginSettings.Count > 0)
                {
                    var response = new PluginSettingListResponse() { PluginSettings = list.PluginSettings };
                    data = JsonConvert.SerializeObject(response);
                }
            }

            return data;
        }
    }
}
