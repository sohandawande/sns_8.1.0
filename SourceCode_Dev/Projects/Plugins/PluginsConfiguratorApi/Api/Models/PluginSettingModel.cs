﻿using System;

namespace PluginsConfiguratorApi.Api.Models
{
    public class PluginSettingModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
