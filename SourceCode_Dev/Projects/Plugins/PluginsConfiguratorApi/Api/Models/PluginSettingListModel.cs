﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PluginsConfiguratorApi.Api.Models
{
    public class PluginListModel
    {
        public Collection<PluginSettingModel> PluginSettings { get; set; }

        public PluginListModel()
        {
            PluginSettings = new Collection<PluginSettingModel>();
        }
    }
}
