namespace PluginsConfiguratorApi.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Plugin",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Active = c.Boolean(nullable: false),
                        Installed = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PluginSetting",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Value = c.String(),
                        Plugin_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Plugin", t => t.Plugin_Id)
                .Index(t => t.Plugin_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PluginSetting", "Plugin_Id", "dbo.Plugin");
            DropIndex("dbo.PluginSetting", new[] { "Plugin_Id" });
            DropTable("dbo.PluginSetting");
            DropTable("dbo.Plugin");
        }
    }
}
