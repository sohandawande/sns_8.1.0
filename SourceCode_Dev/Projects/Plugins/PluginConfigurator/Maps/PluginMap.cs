﻿using PluginConfigurator.Models;
using Znode.Plugin;

namespace PluginConfigurator.Maps
{
    public class PluginVmMap
    {
        public PluginModel ToVm(IPlugin entity)
        {
            return new PluginModel()
            {
                Name = entity.Name,
                Title = entity.Title,
                Version = entity.Version.ToString(),
                IsInstalled = entity.IsInstalled,
                ConfigurationEndPoint = entity.EntryControllerName
            };
        }

    }
}
