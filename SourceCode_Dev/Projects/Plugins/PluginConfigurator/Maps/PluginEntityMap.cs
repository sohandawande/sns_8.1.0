﻿using PluginConfigurator.Domain.Models;
using Znode.Plugin;

namespace PluginConfigurator.Maps
{
    public class PluginEntityMap
    {
        public PluginEntity ToEntity(IPlugin source)
        {
            return new PluginEntity()
            {
                Name = source.Name,
                Version = source.Version.ToString(),
                Installed = source.IsInstalled
            };

        }
    }
}