﻿using System.Linq;
using PluginConfigurator.Domain.Models;
using PluginConfigurator.Domain.Repositories;
using PluginConfigurator.Maps;
using PluginConfigurator.Models;
using Znode.Plugin;
using Znode.Plugin.Framework;

namespace PluginConfigurator.Domain.Services
{
    public class PluginService
    {
        private IRepository<PluginEntity> _repository;
        private readonly PluginVmMap _pluginMap;
        private readonly PluginEntityMap _entityMap;

        public PluginService()
        {
            _repository = new JsonRepository<PluginEntity>();
            _pluginMap = new PluginVmMap();
            _entityMap = new PluginEntityMap();
        }

        public ConfigurationModel GetPlugins()
        {
            var installedPlugins = _repository.List();

            foreach (var plugin in PluginManager.Current.GetPlugins())
            {
                var persistedPlugin = installedPlugins.SingleOrDefault(x => x.Name.ToLower().Equals(plugin.Name.ToLower()));
                plugin.IsInstalled = persistedPlugin != null && persistedPlugin.Installed;
            }

            var model = new ConfigurationModel()
            {
                Plugins = PluginManager.Current
                            .GetPlugins()
                            //.Where(x => x.Name != Assembly.GetAssembly(GetType()).GetName().Name)
                            .Select(x => _pluginMap.ToVm(x)).ToList()
            };

            return model;
        }

        public void Install(string name)
        {
            var persistedPlugin = _repository.List().SingleOrDefault(x => x.Name == name);
            var inMemoryPlugin = PluginManager.Current.GetPlugins().SingleOrDefault(x => x.Name == name);

            if (inMemoryPlugin == null) return;

            if (persistedPlugin != null && persistedPlugin.Installed)
            {
                // already installed
                return;
            }

            // continue only if the plugin has not already been installed
            // and its currently loaded
            inMemoryPlugin.Install();
            inMemoryPlugin.IsInstalled = true;

            var pluginsToBePersisted = PluginManager.Current
                .GetPlugins()
                .Select(x => _entityMap.ToEntity(x))
                .ToList();

            _repository.Save(pluginsToBePersisted);
        }

        public void Uninstall(string name)
        {
            var persistedPlugin = _repository.List().SingleOrDefault(x => x.Name == name);
            var inMemoryPlugin = PluginManager.Current.GetPlugins().SingleOrDefault(x => x.Name == name);

            if (inMemoryPlugin == null)
            {
                // the plugin is not loaded, nothing to do
                return;
            }

            if (persistedPlugin != null && persistedPlugin.Installed)
            {
                // continue only if the plugin has not already been installed
                // and its currently loaded
                inMemoryPlugin.Uninstall();
                inMemoryPlugin.IsInstalled = false;

                var pluginsToBePersisted = PluginManager.Current
                    .GetPlugins()
                    .Select(x => _entityMap.ToEntity(x))
                    .ToList();

                _repository.Save(pluginsToBePersisted);
            }
        }



        private IPlugin FindPlugin(string name)
        {
            return PluginManager.Current.GetPlugins().SingleOrDefault(x => x.Name == name);
        }
    }
}
