﻿using System.Collections.Generic;
using System.Linq;
using PluginConfigurator.Domain.Models;

namespace PluginConfigurator.Domain.Repositories
{
    public interface IRepository<T> where T : IEntity
    {
        IQueryable<T> List();
        void Save(List<T> entities);
    }
}
