﻿using System;

namespace PluginConfigurator.Domain.Models
{
    public class PluginEntity : IEntity
    {
        public string Name { get; set; }
        public string Version { get; set; }
        public bool Installed { get; set; }
    }
}
