﻿using System.Web.Mvc;
using Znode.Plugin;
using Znode.Plugin.Framework.Domain.Services;
using Znode.Plugin.Framework.Models;

namespace PluginConfigurator.Controllers
{
    public class PluginConfiguratorController : Controller
    {
        private readonly PluginService<IAdminPlugin> _pluginService;

        public PluginConfiguratorController()
        {
            _pluginService = new PluginService<IAdminPlugin>();
        }

        public ActionResult Index()
        {
            var model = _pluginService.GetPlugins();

            return View("~/Plugins/PluginConfigurator/Views/PluginConfigurator/Index.cshtml", model);
        }

        [HttpPost]
        public ActionResult Index(ConfigurationModel model)
        {
            if (!ModelState.IsValid)
            {
                return Index();
            }

            // save the settings 

            return Index();
        }

        public ActionResult Uninstall(string name)
        {
            _pluginService.Uninstall(name);

            return Index();
        }

        public ActionResult Install(string name)
        {
            _pluginService.Install(name);

            return Index();
        }
    }
}
