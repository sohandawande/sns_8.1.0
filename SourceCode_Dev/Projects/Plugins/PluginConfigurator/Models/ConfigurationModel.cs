﻿using System.Collections.Generic;

namespace PluginConfigurator.Models
{
    public class ConfigurationModel
    {
        public List<PluginModel> Plugins { get; set; }
    }
}
