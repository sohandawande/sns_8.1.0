﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Compilation;

namespace PluginConfigurator.Models
{
    public class PluginModel
    {
        [DisplayName("Title")]
        public string Title { get; set; }
        [DisplayName("Plugin Name")]
        public string Name { get; set; }
        [DisplayName("Version")]
        public string Version { get; set; }
        [DisplayName("Installed?")]
        public bool IsInstalled { get; set; }
        [DisplayName("Configuration End Point")]
        public string ConfigurationEndPoint { get; set; }
    }
}
