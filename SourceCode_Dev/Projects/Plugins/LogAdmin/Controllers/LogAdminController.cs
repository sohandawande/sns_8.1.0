﻿using System.Web.Mvc;

namespace LogAdmin.Controllers
{
    public class LogAdminController : Controller
    {

        public LogAdminController()
        { }

        public ActionResult Index()
        {
            return View("~/Plugins/LogAdmin/Views/LogAdmin/Index.cshtml");
        }
    }
}
