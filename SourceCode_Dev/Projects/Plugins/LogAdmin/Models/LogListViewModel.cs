﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LogAdmin.Models
{
    public class LogListViewModel
    {
        public LogListViewModel()
        {
            Logs = new List<Log>();
        }

        public List<Log> Logs { get; set; }
       
    }
}
