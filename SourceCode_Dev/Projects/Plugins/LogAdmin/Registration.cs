﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;
using Znode.Plugin;

namespace LogAdmin
{
    public class Registration : IAdminPlugin
    {
        public string Name
        {
            get { return Assembly.GetAssembly(GetType()).GetName().Name; }
        }

        public Version Version
        {
            get
            {
                return Assembly.GetAssembly(GetType()).GetName().Version;
            }
        }

        public string Title
        {
            get { return "Log Admin"; }
        }

        public string EntryControllerName
        {
            get { return "LogAdmin"; }
        }

        public void Install()
        {
            var fileName = string.Format(@"{0}\{1}.json", HostingEnvironment.MapPath("~/plugins"), this.GetType().Name);

            File.Create(fileName);
        }

        public void Uninstall()
        {
        }

        public int Order
        {
            get { return -100; }
        }

        public bool IsInstalled { get; set; }
    }
}
