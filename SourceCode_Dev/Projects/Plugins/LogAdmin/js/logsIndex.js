﻿(function ($) {
    function init() {
        $(document).ready(function () {
            $('#logDataDynamic').dataTable({
                dataSrc: "Logs",
                ajax: {
                    url: 'http://api.multifront.localhost.com/logs',
                    dataSrc: "Logs"
                },
                //data: dataSet,
                "columns": [
                    { "data": "Id" },
                    { "data": "Message" }
                ]
            });
        });
    };

    init();
})(jQuery);