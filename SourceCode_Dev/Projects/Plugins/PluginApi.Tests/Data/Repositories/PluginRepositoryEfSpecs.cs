﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using PluginSettingsApi.Data.Models;
using PluginSettingsApi.Data.Repositories;
using StructureMap.Query;

namespace PluginApi.Tests.Data.Repositories
{
    [TestFixture]
    public class PluginRepositoryEfSpecs
    {
        private PluginConfiguratorDbContext _context;

        [SetUp]
        public void Setup()
        {
            _context = new PluginConfiguratorDbContext();
        }

        [TearDown]
        public void TearDown()
        {
            _context = null;
        }


        [Test]
        public void can_create_plugin()
        {
            var plugin = GetNewPlugin();
            CreatePluginSimple(plugin);
            Assert.AreNotEqual(0, plugin.Id);
        }

        [Test]
        public void can_create_then_delete()
        {
            var plugin = GetNewPlugin();
            CreatePluginSimple(plugin);
            var retrievedPlugin = GetPlugin(plugin.Id);
            Assert.AreNotEqual(0, plugin.Id);
            Assert.AreEqual(plugin.PluginSettings.Count, retrievedPlugin.PluginSettings.Count);


            // now attempt to delete a plugin 
            var pluginId = plugin.Id;
            DeletePluginSimple(plugin);

            retrievedPlugin = GetPlugin(pluginId);

            Assert.IsNull(retrievedPlugin);
        }

        [Test]
        public void can_update_plugin()
        {
            var plugin = GetNewPlugin();
            CreatePluginSimple(plugin);
            Assert.AreNotEqual(0, plugin.Id);
            Assert.AreEqual(1, plugin.PluginSettings.Count);

            var retrievedPlugin = GetPlugin(plugin.Id);
            Assert.IsNotNull(retrievedPlugin);
            Assert.AreEqual(plugin.PluginSettings.Count, retrievedPlugin.PluginSettings.Count);

            plugin.PluginSettings.Add(new PluginSetting() { Name = "second setting", Value = "second value", PluginId = plugin.Id, Plugin = plugin });

            // should have added a new plugin setting
            UpdateComplex(plugin);

            var updatedPlugin = GetPlugin(plugin.Id);
            Assert.AreEqual(plugin.PluginSettings.Count, updatedPlugin.PluginSettings.Count);

            plugin.PluginSettings.Clear();
            UpdateComplex(plugin);
            updatedPlugin = GetPlugin(plugin.Id);
            Assert.AreEqual(plugin.PluginSettings.Count, updatedPlugin.PluginSettings.Count);
        }



        private Plugin GetPlugin(int id)
        {
            using (var db = new PluginConfiguratorDbContext())
            {
                return db.Plugins.Include("PluginSettings").SingleOrDefault(x => x.Id == id);
            }
        }

        private void CreatePluginSimple(Plugin plugin)
        {
            using (var newContext = new PluginConfiguratorDbContext())
            {
                newContext.Plugins.Add(plugin);
                newContext.SaveChanges();
            }
        }

        private void UpdatePluginSimple(Plugin plugin)
        {
            using (var newContext = new PluginConfiguratorDbContext())
            {
                //newContext.Plugins.AddOrUpdate(plugin);
                newContext.Plugins.Attach(plugin);
                newContext.Entry(plugin).State = EntityState.Modified;
                newContext.SaveChanges();
            }
        }

        private void DeletePluginSimple(Plugin plugin)
        {
            using (var newContext = new PluginConfiguratorDbContext())
            {
                newContext.Plugins.Attach(plugin);
                newContext.Plugins.Remove(plugin);
                newContext.SaveChanges();
            }
        }

        private Plugin GetNewPlugin()
        {
            return new Plugin
            {
                Active = false,
                Installed = false,
                Name = Guid.NewGuid().ToString(),
                PluginSettings = new List<PluginSetting>() { new PluginSetting() { Name = "test", Value = "test" } }
            };
        }

        private void UpdateComplex(Plugin plugin)
        {
            //Save Plugin and tearchers Updated above in different context just to create disconnected scenario
            using (var newCtx = new PluginConfiguratorDbContext())
            {
                //fetch existing Plugin info
                var existingPlugin = (from p in newCtx.Plugins
                                      where p.Id == plugin.Id
                                      select p).FirstOrDefault<Plugin>();

                var newPluginSettings = plugin.PluginSettings.ToList<PluginSetting>();
                var existingPluginSettings = existingPlugin == null ? new List<PluginSetting>() : existingPlugin.PluginSettings.ToList<PluginSetting>();

                // find added PluginSettings from newPluginSettings whose PluginSettingId doesn't match with Existing PluginSettings
                var addedPluginSettings = newPluginSettings.Except(existingPluginSettings, new PluginSettingComparer());

                // find deleted PluginSettings from existing (from db) PluginSettings whose PluginSettingId is not in newPluginSettings list
                var deletedPluginSettings = existingPlugin.PluginSettings.Except(newPluginSettings, new PluginSettingComparer());

                //find Updated PluginSettings from existing PluginSettings which is either not deleted or Added
                var updatedPluginSettings = existingPlugin.PluginSettings.Except(deletedPluginSettings, new PluginSettingComparer());

                //Add new PluginSettings to context
                addedPluginSettings.ToList<PluginSetting>().ForEach(t => newCtx.PluginSettings.Add(t));

                //Attacher Updated PluginSettings to context and mark it's state as Modified
                foreach (PluginSetting t in updatedPluginSettings)
                {
                    newCtx.PluginSettings.Attach(t);
                    newCtx.Entry(t).State = EntityState.Modified;
                    //newCtx.ObjectStateManager.ChangeObjectState(t, System.Data.EntityState.Modified);
                }

                // delete the deleted PluginSettings in the context
                deletedPluginSettings.ToList<PluginSetting>().ForEach(t => newCtx.PluginSettings.Remove(t));

                // save all above changes
                newCtx.SaveChanges();
            }

        }
    }
}
