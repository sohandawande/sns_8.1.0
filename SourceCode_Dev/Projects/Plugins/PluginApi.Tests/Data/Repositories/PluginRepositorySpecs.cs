﻿using NUnit.Framework;
using PluginSettingsApi.Data.Models;
using PluginSettingsApi.Data.Repositories;
using RefactorThis.GraphDiff;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;

namespace PluginApi.Tests.Data.Repositories
{
    [TestFixture]
    public class PluginRepositorySpecs
    {
        private PluginConfiguratorDbContext _context;

        [SetUp]
        public void Setup()
        {
            _context = new PluginConfiguratorDbContext();
        }

        [TearDown]
        public void TearDown()
        {
            _context = null;
        }

        [Test]
        public void can_save_using_attach()
        {
            var plugin = GetNewPlugin();

            CreatePluginSimple(plugin);
            Assert.AreNotEqual(0, plugin.Id);
        }

        [Test]
        public void can_save_using_update_graph()
        {
            //var plugin = new Plugin()
            //{
            //    Active = false,
            //    Installed = false,
            //    Name = "TestPlugin",
            //    PluginSettings = new List<PluginSetting>() { new PluginSetting() { Name = "test", Value = "test" } }
            //};

            var plugin = GetNewPlugin();

            _context.UpdateGraph(plugin, map => map.OwnedCollection(p => p.PluginSettings));

            _context.SaveChanges();

            Assert.AreNotEqual(0, plugin.Id);
        }

        [Test]
        public void can_save_plugin_manually()
        {
            var plugin = GetNewPlugin();
            UpdatePlugin(plugin);
            Assert.AreNotEqual(0, plugin.Id);
        }


        [Test]
        public void can_add_pluginsetting_to_plugin()
        {
            var plugin = GetNewPlugin();

            CreatePluginSimple(plugin);
            Assert.AreNotEqual(0, plugin.Id);

            // now update the plugin 
            var beforeName = plugin.Name;
            var afterName = Guid.NewGuid().ToString();

            plugin.PluginSettings.Add(new PluginSetting() { Name = Guid.NewGuid().ToString(), Value = "testvalue", PluginId = plugin.Id });

            //plugin.Name = afterName;

            UpdatePlugin(plugin);

            Assert.AreEqual(afterName, plugin.Name);
        }

        [Test]
        public void can_create_plugin()
        {
            var plugin = GetNewPlugin(); 
            CreatePluginSimple(plugin);
            Assert.AreNotEqual(0, plugin.Id);
        }

        [Test]
        public void can_update_plugin()
        {
            var plugin = GetNewPlugin();
            CreatePluginSimple(plugin);
            Assert.AreNotEqual(0, plugin.Id);
            Assert.AreEqual(1, plugin.PluginSettings.Count);

            
            
        }

        private void CreatePluginSimple(Plugin plugin)
        {
            using (var newContext = new PluginConfiguratorDbContext())
            {
                newContext.Plugins.Add(plugin);
                newContext.SaveChanges();
            }
        }

        private void UpdatePluginSimple(Plugin plugin)
        {
            using (var newContext = new PluginConfiguratorDbContext())
            {
                newContext.Plugins.AddOrUpdate(plugin);
                newContext.SaveChanges();
            }
        }
        

        private void UpdatePlugin(Plugin plugin)
        {
            //Save Plugin and tearchers Updated above in different context just to create disconnected scenario
            using (var newCtx = new PluginConfiguratorDbContext())
            {
                //fetch existing Plugin info
                var existingPlugin = (from p in newCtx.Plugins
                                      where p.Id == plugin.Id
                                      select p).FirstOrDefault<Plugin>();

                var newPluginSettings = plugin.PluginSettings.ToList<PluginSetting>();
                var existingPluginSettings = existingPlugin == null ? new List<PluginSetting>() : existingPlugin.PluginSettings.ToList<PluginSetting>();

                // find added PluginSettings from newPluginSettings whose PluginSettingId doesn't match with Existing PluginSettings
                var addedPluginSettings = newPluginSettings.Except(existingPluginSettings, new PluginSettingComparer());

                // find deleted PluginSettings from existing (from db) PluginSettings whose PluginSettingId is not in newPluginSettings list
                var deletedPluginSettings = existingPlugin.PluginSettings.Except(newPluginSettings, new PluginSettingComparer());

                //find Updated PluginSettings from existing PluginSettings which is either not deleted or Added
                var updatedPluginSettings = existingPlugin.PluginSettings.Except(deletedPluginSettings, new PluginSettingComparer());

                //Add new PluginSettings to context
                addedPluginSettings.ToList<PluginSetting>().ForEach(t => newCtx.PluginSettings.Add(t));

                //Attacher Updated PluginSettings to context and mark it's state as Modified
                foreach (PluginSetting t in updatedPluginSettings)
                {
                    newCtx.PluginSettings.Attach(t);
                    newCtx.Entry(t).State = EntityState.Modified;
                    //newCtx.ObjectStateManager.ChangeObjectState(t, System.Data.EntityState.Modified);
                }

                // delete the deleted PluginSettings in the context
                deletedPluginSettings.ToList<PluginSetting>().ForEach(t => newCtx.PluginSettings.Remove(t));

                // save all above changes
                newCtx.SaveChanges();
            }

        }

        private Plugin GetNewPlugin()
        {
            return new Plugin
            {
                Active = false,
                Installed = false,
                Name = Guid.NewGuid().ToString(),
                PluginSettings = new List<PluginSetting>() { new PluginSetting() { Name = "test", Value = "test" } }
            };
        }

    }
}
