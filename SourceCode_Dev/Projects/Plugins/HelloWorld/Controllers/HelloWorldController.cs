﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using HelloWorld.Model;

namespace HelloWorld.Controllers
{
    public class HelloWorldController : Controller
    {
        public ActionResult Index()
        {
            var model = new PingModel() {Message = "Pong"};
            return View("~/Plugins/HelloWorld/Views/HelloWorld/Index.cshtml", model);
        }
    }
}
