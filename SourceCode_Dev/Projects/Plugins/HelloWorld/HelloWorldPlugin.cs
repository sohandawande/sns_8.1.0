﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Znode.Plugin;

namespace HelloWorld
{
    public class HelloWorldPlugin : IPlugin
    {
        public string Name
        {
            get { return Assembly.GetAssembly(GetType()).GetName().Name; }
        }

        public Version Version
        {
            get
            {
                return Assembly.GetAssembly(GetType()).GetName().Version;
            }
        }

        public string Title
        {
            get { return "Hello World"; }
        }

        public string EntryControllerName
        {
            get { return "HelloWorld"; }
        }

        public void Install()
        {
            // do nothing
        }

        public void Uninstall()
        {
            // do nothing
        }

        public int Order
        {
            get { return 100; }
        }
        public bool IsInstalled { get; set; }
    }
}
