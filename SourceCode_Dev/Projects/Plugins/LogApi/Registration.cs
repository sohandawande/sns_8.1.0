﻿using System;
using System.Collections.ObjectModel;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Web.Http;
using LogApi.Migrations;
using Znode.Framework.Api.HelpPage.Extensions;
using Znode.Plugin;
using System.Web.Http.Routing;
using LogApi.Model;
using LogApi.Model.Responses;

namespace LogApi
{
    public class Registration : IApiPlugin
    {
        public string Name
        {
            get { return Assembly.GetAssembly(GetType()).GetName().Name; }
        }

        public Version Version
        {
            get
            {
                return Assembly.GetAssembly(GetType()).GetName().Version;
            }
        }

        public string Title
        {
            get { return "Plugin Api"; }
        }

        public void Install()
        {
            // install the database schema
            var migrationConfig = new Configuration()
            {
                AutomaticMigrationsEnabled = true,
                TargetDatabase = new DbConnectionInfo(Constants.ConnectionStringName)
            };

            var migrator = new DbMigrator(migrationConfig);
            migrator.Update();

            IsInstalled = true;
        }

        public void Uninstall()
        {
            // uninstall the database schema
            var migrationConfig = new Configuration()
            {
                AutomaticMigrationDataLossAllowed = true,
                TargetDatabase = new DbConnectionInfo(Constants.ConnectionStringName)
            };

            var migrator = new DbMigrator(migrationConfig);
            migrator.Update(DbMigrator.InitialDatabase);

            IsInstalled = false;
        }

        public int Order
        {
            get { return -100; }
        }

        public bool IsInstalled { get; set; }

        public void RegisterRoutes(HttpConfiguration config)
        {
            const string controllerName = "logs";

            config.Routes.MapHttpRoute("logs-get", "logs/{id}", new { controller = controllerName, action = "get" }, new { httpMethod = new HttpMethodConstraint(new HttpMethod("GET")) });
            config.Routes.MapHttpRoute("logs-list", "logs", new { controller = controllerName, action = "list" }, new { httpMethod = new HttpMethodConstraint(new HttpMethod("GET")) });
            config.Routes.MapHttpRoute("logs-create", "logs", new { controller = controllerName, action = "create" }, new { httpMethod = new HttpMethodConstraint(new HttpMethod("POST")) });
            config.Routes.MapHttpRoute("logs-update", "logs/{pluginId}", new { controller = controllerName, action = "update" }, new { httpMethod = new HttpMethodConstraint(new HttpMethod("PUT")) });
            config.Routes.MapHttpRoute("logs-delete", "logs/{pluginId}", new { controller = controllerName, action = "delete" }, new { httpMethod = new HttpMethodConstraint(new HttpMethod("DELETE")) });
        }

        public void SetSampleResponses(HttpConfiguration config)
        {
            var LogResponse = new LogResponse { Log = new LogModel().Sample() };
            var LogListResponse = new LogListResponse() { Logs = new Collection<LogModel>() };
            LogListResponse.Logs.Add(LogResponse.Log);

            var jsonMediaType = new MediaTypeHeaderValue("application/json");

            config.SetSampleResponse(LogResponse.ToJson(), jsonMediaType, "Logs", "Get", "LogName");
            config.SetSampleResponse(LogListResponse.ToJson(), jsonMediaType, "Logs", "List");
        }
    }
}
