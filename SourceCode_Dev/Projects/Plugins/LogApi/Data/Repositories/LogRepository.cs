﻿using System.Linq;
using LogApi.Data.Model;

namespace LogApi.Data.Repositories
{
    public class LogRepository : ILogRepository
    {
        private readonly LogDbContext _context;

        public LogRepository(LogDbContext context)
        {
            _context = context;
        }

        public IQueryable<LogMessage> Query()
        {
            return _context.Logs;
        }

        public IQueryable<LogMessage> List()
        {
            return Query();
        }

        public LogMessage GetById(int id)
        {
            return Query().SingleOrDefault(x => x.Id == id);
        }

        public LogMessage Save(LogMessage entity)
        {
            if (entity.Id <= 0)
            {
                // do an insert
                _context.Logs.Add(entity);
                _context.SaveChanges();

                return GetById(entity.Id);
            }
            else
            {
                _context.SaveChanges();

                return GetById(entity.Id);
            }
        }

        public bool Delete(int id)
        {
            var plugin = GetById(id);
            return Delete(plugin);
        }

        public bool Delete(LogMessage entity)
        {
            _context.Logs.Attach(entity);
            _context.Logs.Remove(entity);
            var count = _context.SaveChanges();

            return count > 1;
        }
    }
}
