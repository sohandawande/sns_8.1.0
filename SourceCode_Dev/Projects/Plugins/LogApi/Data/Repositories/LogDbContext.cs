﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogApi.Data.Model;

namespace LogApi.Data.Repositories
{
    public class LogDbContext : DbContext
    {
        public LogDbContext()
            : base(Constants.ConnectionStringName)
        {

        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            base.OnModelCreating(modelBuilder);
        }

        public DbSet<LogMessage> Logs { get; set; }
    }
}
