﻿using System.Linq;

namespace LogApi.Data.Repositories
{
    public interface IRepository<T>
    {
        IQueryable<T> List();
        void Save(T entity);
    }
}
