using System.Linq;
using LogApi.Data.Model;

namespace LogApi.Data.Repositories
{
    public interface ILogRepository
    {
        IQueryable<LogMessage> List();
        LogMessage Save(LogMessage entity);
        bool Delete(int id);
        bool Delete(LogMessage entity);
    }
}