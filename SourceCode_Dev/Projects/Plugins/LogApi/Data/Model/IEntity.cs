namespace LogApi.Data.Model
{
    public interface IEntity
    {
        int Id { get; set; }
    }
}