﻿using System.Collections.ObjectModel;
using Znode.Engine.Api.Models;

namespace LogApi.Model
{
    public class LogListModel : BaseListModel
    {
        public Collection<LogModel> Plugins { get; set; }

        public LogListModel()
        {
            Plugins = new Collection<LogModel>();
        }
    }
}