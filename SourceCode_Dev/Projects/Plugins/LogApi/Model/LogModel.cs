﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;

namespace LogApi.Model
{
    public class LogModel : BaseModel
    {
        public int Id { get; set; }
        public string Message { get; set; }

        public LogModel Sample()
        {
            return new LogModel()
            {
                Id = 1,
                Message = "Sample Message"
            };
        }
    }
}
