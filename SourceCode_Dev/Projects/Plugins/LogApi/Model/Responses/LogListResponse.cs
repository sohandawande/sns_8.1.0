﻿using System.Collections.ObjectModel;
using Znode.Engine.Api.Models.Responses;

namespace LogApi.Model.Responses
{
    public class LogListResponse : BaseListResponse
    {
        public Collection<LogModel> Logs { get; set; }
    }
}