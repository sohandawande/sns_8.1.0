﻿using Znode.Engine.Api.Models.Responses;

namespace LogApi.Model.Responses
{
    public class LogResponse : BaseResponse
    {
        public LogModel Log { get; set; }
    }
}