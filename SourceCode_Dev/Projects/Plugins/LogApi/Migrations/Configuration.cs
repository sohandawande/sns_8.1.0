using System.Data.Entity.Migrations;
using LogApi.Data.Model;
using LogApi.Data.Repositories;

namespace LogApi.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<LogDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(LogDbContext context)
        {
            //  This method will be called after migrating to the latest version.
            var message = new LogMessage() { Id = 1, Message = "first message" };
            context.Logs.AddOrUpdate(p => p.Id, message);
        }
    }
}
