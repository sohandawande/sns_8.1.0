namespace LogApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Commence : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LogMessage",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Message = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.LogMessage");
        }
    }
}
