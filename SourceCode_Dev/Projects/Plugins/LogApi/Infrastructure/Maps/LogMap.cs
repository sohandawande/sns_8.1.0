﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogApi.Data.Model;
using LogApi.Model;

namespace LogApi.Infrastructure.Maps
{
    public static class LogMap
    {
        public static LogModel ToModel(LogMessage entity)
        {
            if (entity == null)
            {
                return null;
            }

            return new LogModel()
            {
                Id = entity.Id,
                Message = entity.Message
            };
        }


        public static LogMessage ToEntity(LogModel model)
        {
            if (model == null)
            {
                return null;
            }

            return new LogMessage()
            {
                Id = model.Id,
                Message = model.Message
            };
        }
    }
}
