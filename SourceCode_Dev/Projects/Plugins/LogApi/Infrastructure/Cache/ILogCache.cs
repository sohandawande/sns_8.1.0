﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogApi.Infrastructure.Cache
{
    public interface ILogCache
    {
        string GetLog(int id, string routeUri, string routeTemplate);
        string GetLogs();
    }
}
