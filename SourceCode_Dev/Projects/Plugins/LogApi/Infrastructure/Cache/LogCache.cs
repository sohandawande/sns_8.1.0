﻿using System;
using System.Collections.ObjectModel;
using LogApi.Infrastructure.Services;
using LogApi.Model;
using LogApi.Model.Responses;
using Newtonsoft.Json;

namespace LogApi.Infrastructure.Cache
{
    public class LogCache : ILogCache
    {
        private readonly ILogService _service;

        public LogCache()
        {
            _service = new LogService();
        }


        public string GetLog(int id, string routeUri, string routeTemplate)
        {
            //TODO: GetFromCache is a method on BaseCache, but currently this has some dependencie that can not be refactored
            // var data = GetFromCache(routeUri);
            string data = null;

            if (data == null)
            {
                var model = _service.GetLog(id);
                if (model != null)
                {
                    var response = new LogResponse() { Log = model };
                    //TODO: InsertIntoCache is a method on BaseCache, but currently this has some dependencie that can not be refactored
                    //data = InsertIntoCache(routeUri, routeTemplate, response);
                    data = JsonConvert.SerializeObject(response);
                }
            }

            return data;
        }

        public string GetLogs()
        {
            string data = null;
            // var data = GetFromCache(routeUri); 

            if (data == null)
            {
                var list = _service.GetLogs();
                if (list.Count > 0)
                {
                    var response = new LogListResponse() { Logs = new Collection<LogModel>(list) };
                    data = JsonConvert.SerializeObject(response);
                }
            }

            return data;
        }
    }
}