﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogApi.Model;

namespace LogApi.Infrastructure.Services
{
    public interface ILogService
    {
        LogModel GetLog(int id);
        List<LogModel> GetLogs();
        LogModel CreateLog(LogModel model);
        LogModel UpdateLog(int id, LogModel model);
        bool DeleteLog(int id);
    }
}
