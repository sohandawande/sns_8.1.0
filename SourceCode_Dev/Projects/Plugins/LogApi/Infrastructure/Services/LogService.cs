﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogApi.Data.Repositories;
using LogApi.Infrastructure.Maps;
using LogApi.Model;

namespace LogApi.Infrastructure.Services
{
    public class LogService : ILogService
    {
        private readonly ILogRepository _logRepository;
        public LogService()
        {
            _logRepository = new LogRepository(new LogDbContext());
        }

        public LogModel GetLog(int id)
        {
            return
                _logRepository.List()
                    .Where(x => x.Id == id)
                    .Select(l => new LogModel() { Id = l.Id, Message = l.Message })
                    .SingleOrDefault();
        }

        public List<LogModel> GetLogs()
        {
            return _logRepository.List()
                .Select(l => new LogModel() { Id = l.Id, Message = l.Message })
                .ToList();
        }

        public LogModel CreateLog(LogModel model)
        {
            if (model == null)
            {
                throw new Exception("Plugin model cannot be null.");
            }

            var entity = LogMap.ToEntity(model);
            var inserted = _logRepository.Save(entity);
            return LogMap.ToModel(inserted);
        }

        public LogModel UpdateLog(int id, LogModel model)
        {
            throw new NotImplementedException();
        }

        public bool DeleteLog(int id)
        {
            throw new NotImplementedException();
        }
    }
}