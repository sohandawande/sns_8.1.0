﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using LogApi.Infrastructure.Cache;
using LogApi.Infrastructure.Services;
using LogApi.Model;
using LogApi.Model.Responses;
using Znode.Framework.Api.Attributes;
using Znode.Framework.Api.Controllers;

namespace LogApi.Controllers
{
    public class LogsController : BaseController
    {
           private readonly ILogCache _cache;
        private readonly ILogService _service;

        public LogsController()
        {
            _cache = new LogCache();
            _service = new LogService();
        }

        /// <summary>
        /// gets a list of Logs
        /// </summary>
        /// <param name="id">log id associated with logs</param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetLog(id, RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<LogResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new LogListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage List()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetLogs();
                response = data != null ? CreateOKResponse<LogListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new LogListResponse() { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Creates a new highlight type.
        /// </summary>
        /// <param name="model">The model of the highlight type.</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Create([FromBody] LogModel model)
        {
            HttpResponseMessage response;

            try
            {
                var Log = _service.CreateLog(model);
                if (Log != null)
                {
                    var uri = Request.RequestUri;
                    var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + Log.Id;

                    response = CreateCreatedResponse(new LogResponse { Log = Log });
                    response.Headers.Add("Location", location);
                }
                else
                {
                    response = CreateInternalServerErrorResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new LogResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Updates an existing highlight type.
        /// </summary>
        /// <param name="LogId">The ID of the highlight type.</param>
        /// <param name="model">The model of the highlight type.</param>
        /// <returns></returns>
        [HttpPut]
        public HttpResponseMessage Update(int LogId, [FromBody] LogModel model)
        {
            HttpResponseMessage response;

            try
            {
                var Log = _service.UpdateLog(LogId, model);
                response = Log != null ? CreateOKResponse(new LogResponse { Log = Log }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new LogResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Deletes an existing Log.
        /// </summary>
        /// <param name="LogId">The ID of the Log.</param>
        /// <returns></returns>
        [HttpDelete]
        public HttpResponseMessage Delete(int LogId)
        {
            HttpResponseMessage response;

            try
            {
                var deleted = _service.DeleteLog(LogId);
                response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new LogResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
    }
}
