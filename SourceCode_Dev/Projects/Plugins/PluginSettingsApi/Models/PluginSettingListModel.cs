﻿using System.Collections.ObjectModel;
using Znode.Engine.Api.Models;

namespace PluginApi.Models
{
    public class PluginListModel : BaseListModel
    {
        public Collection<PluginModel> Plugins { get; set; }

        public PluginListModel()
        {
            Plugins = new Collection<PluginModel>();
        }
    }
}
