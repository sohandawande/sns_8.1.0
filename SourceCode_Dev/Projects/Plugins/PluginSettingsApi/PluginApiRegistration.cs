﻿using System;
using System.Collections.ObjectModel;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using System.IO;
using System.Net.Http.Headers;
using System.Reflection;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.Routing;
using PluginApi.Models;
using PluginApi.Models.Responses;
using PluginSettingsApi.Migrations;
using Znode.Framework.Api.HelpPage.Extensions;
using Znode.Plugin;

namespace PluginSettingsApi
{
    public class PluginApiRegistration : IApiPlugin
    {
        public string Name
        {
            get { return Assembly.GetAssembly(GetType()).GetName().Name; }
        }

        public Version Version
        {
            get
            {
                return Assembly.GetAssembly(GetType()).GetName().Version;
            }
        }

        public string Title
        {
            get { return "Plugin Api"; }
        }

        public void Install()
        {
            // install the database schema
            var migrationConfig = new Configuration()
            {
                AutomaticMigrationsEnabled = true,
                TargetDatabase = new DbConnectionInfo(Constants.ConnectionStringName)
            };

            var migrator = new DbMigrator(migrationConfig);
            migrator.Update();

            IsInstalled = true;
        }

        public void Uninstall()
        {
            // uninstall the database schema
            var migrationConfig = new Configuration()
            {
                AutomaticMigrationDataLossAllowed = true,
                TargetDatabase = new DbConnectionInfo(Constants.ConnectionStringName)
            };

            var migrator = new DbMigrator(migrationConfig);
            migrator.Update(DbMigrator.InitialDatabase);

            IsInstalled = false;
        }

        public int Order
        {
            get { return -100; }
        }

        public bool IsInstalled { get; set; }

        public void RegisterRoutes(HttpConfiguration config)
        {
            //config.Routes.MapHttpRoute("review-get", "reviews/{reviewId}", new { controller = "reviews", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            //config.Routes.MapHttpRoute("review-list", "reviews", new { controller = "reviews", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            //config.Routes.MapHttpRoute("review-create", "reviews", new { controller = "reviews", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            //config.Routes.MapHttpRoute("review-update", "reviews/{reviewId}", new { controller = "reviews", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            //config.Routes.MapHttpRoute("review-delete", "reviews/{reviewId}", new { controller = "reviews", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            config.Routes.MapHttpRoute("pluginsettings-get", "pluginlist/{pluginname}", new { controller = "plugins", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("pluginsettings-list", "pluginlist", new { controller = "plugins", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("pluginsettings-create", "pluginlist", new { controller = "plugins", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("pluginsettings-update", "pluginlist/{pluginId}", new { controller = "plugins", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("pluginsettings-delete", "pluginlist/{pluginId}", new { controller = "plugins", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });
        }

        public void SetSampleResponses(HttpConfiguration config)
        {

            // Sample promotion type
            //var promotionTypeResponse = new PromotionTypeResponse { PromotionType = new PromotionTypeModel().Sample() };
            //var promotionTypeListResponse = new PromotionTypeListResponse { PromotionTypes = new Collection<PromotionTypeModel>() };
            //promotionTypeListResponse.PromotionTypes.Add(promotionTypeResponse.PromotionType);

            //config.SetSampleResponse(promotionTypeResponse.ToJson(), jsonMediaType, "PromotionTypes", "Get", "promotionTypeId");
            //config.SetSampleResponse(promotionTypeListResponse.ToJson(), jsonMediaType, "PromotionTypes", "List");

            var pluginResponse = new PluginResponse { Plugin = new PluginModel().Sample() };
            var pluginListResponse = new PluginListResponse() { Plugins = new Collection<PluginModel>() };
            pluginListResponse.Plugins.Add(pluginResponse.Plugin);

            var jsonMediaType = new MediaTypeHeaderValue("application/json");

            config.SetSampleResponse(pluginResponse.ToJson(), jsonMediaType, "Plugins", "Get", "pluginName");
            config.SetSampleResponse(pluginResponse.ToJson(), jsonMediaType, "Plugins", "List");
        }
    }
}
