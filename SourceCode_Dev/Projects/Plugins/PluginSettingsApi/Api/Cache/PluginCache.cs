﻿using System.Collections.ObjectModel;
using System.Linq;
using Newtonsoft.Json;
using PluginApi.Models;
using PluginApi.Models.Responses;
using PluginSettingsApi.Api.Services;

namespace PluginSettingsApi.Api.Cache
{
    public class PluginCache : IPluginCache
    {
        private IPluginService _service;

        public PluginCache()
        {
            _service = new PluginService();
        }

        public string GetPluginSettings(string pluginName, string routeUri, string routeTemplate)
        {
            //TODO: GetFromCache is a method on BaseCache, but currently this has some dependencie that can not be refactored
            // var data = GetFromCache(routeUri);
            string data = null;

            if (data == null)
            {
                var model = _service.GetPlugin(pluginName);
                if (model != null)
                {
                    var response = new PluginResponse() { Plugin = model };
                    //TODO: InsertIntoCache is a method on BaseCache, but currently this has some dependencie that can not be refactored
                    //data = InsertIntoCache(routeUri, routeTemplate, response);
                    data = JsonConvert.SerializeObject(response);
                }
            }

            return data;
        }

        public string GetPluginSettings()
        {
            string data = null;
            // var data = GetFromCache(routeUri); 

            if (data == null)
            {
                var list = _service.GetPlugins();
                if (list.Count > 0)
                {
                    var response = new PluginListResponse() { Plugins = new Collection<PluginModel>(list) };
                    data = JsonConvert.SerializeObject(response);
                }
            }

            return data;
        }
    }
}
