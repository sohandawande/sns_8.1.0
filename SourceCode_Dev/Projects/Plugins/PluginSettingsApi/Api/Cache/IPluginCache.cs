namespace PluginSettingsApi.Api.Cache
{
    
    public interface IPluginCache
    {
        string GetPluginSettings(string pluginName, string routeUri, string routeTemplate);
        string GetPluginSettings();
    }
}