﻿using System;
using System.Net.Http;
using System.Security.Policy;
using System.Web.Http;
using PluginApi.Models;
using PluginApi.Models.Responses;
using PluginSettingsApi.Api.Cache;
using PluginSettingsApi.Api.Services;
using Znode.Framework.Api.Attributes;
using Znode.Framework.Api.Controllers;

namespace PluginSettingsApi.Controllers
{
    public class PluginsController : BaseController
    {
        private readonly IPluginCache _cache;
        private readonly IPluginService _service;

        public PluginsController()
        {
            _cache = new PluginCache();
            _service = new PluginService();
        }

        /// <summary>
        /// gets a list of plugin settings
        /// </summary>
        /// <param name="pluginName">plugin name associated with settings</param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage Get(string pluginName)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetPluginSettings(pluginName, RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<PluginResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new PluginListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage List()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetPluginSettings();
                response = data != null ? CreateOKResponse<PluginListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new PluginListResponse() { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Creates a new highlight type.
        /// </summary>
        /// <param name="model">The model of the highlight type.</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Create([FromBody] PluginModel model)
        {
            HttpResponseMessage response;

            try
            {
                var Plugin = _service.CreatePlugin(model);
                if (Plugin != null)
                {
                    var uri = Request.RequestUri;
                    var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + Plugin.Name;

                    response = CreateCreatedResponse(new PluginResponse { Plugin = Plugin });
                    response.Headers.Add("Location", location);
                }
                else
                {
                    response = CreateInternalServerErrorResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new PluginResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Updates an existing highlight type.
        /// </summary>
        /// <param name="PluginId">The ID of the highlight type.</param>
        /// <param name="model">The model of the highlight type.</param>
        /// <returns></returns>
        [HttpPut]
        public HttpResponseMessage Update(int PluginId, [FromBody] PluginModel model)
        {
            HttpResponseMessage response;

            try
            {
                var Plugin = _service.UpdatePlugin(PluginId, model);
                response = Plugin != null ? CreateOKResponse(new PluginResponse { Plugin = Plugin }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new PluginResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Deletes an existing plugin.
        /// </summary>
        /// <param name="PluginId">The ID of the plugin.</param>
        /// <returns></returns>
        [HttpDelete]
        public HttpResponseMessage Delete(int PluginId)
        {
            HttpResponseMessage response;

            try
            {
                var deleted = _service.DeletePlugin(PluginId);
                response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new PluginResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
    }
}
