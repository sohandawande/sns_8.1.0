﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using PluginSettingsApi.Data.Models;

namespace PluginSettingsApi.Data.Repositories
{
    public class PluginConfiguratorDbContext : DbContext
    {
        public PluginConfiguratorDbContext()
            : base(Constants.ConnectionStringName)
        {

        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            
            //one-to-many 
            modelBuilder.Entity<PluginSetting>()
                        .HasRequired<Plugin>(s => s.Plugin)
                        .WithMany(s => s.PluginSettings)
                        .HasForeignKey(s => s.PluginId); 
            
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Plugin> Plugins { get; set; }
        public DbSet<PluginSetting> PluginSettings { get; set; }
    }
}