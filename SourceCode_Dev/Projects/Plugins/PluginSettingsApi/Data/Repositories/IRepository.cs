﻿using System.Linq;

namespace PluginSettingsApi.Data.Repositories
{
    public interface IRepository<T>
    {
        IQueryable<T> List();
        void Save(T entity);
    }
}
