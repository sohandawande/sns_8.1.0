﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PluginSettingsApi.Data.Models;
using PluginSettingsApi.Data.Repositories;
using RefactorThis.GraphDiff;

namespace PluginApi.Data.Repositories
{
    public class PluginRepository : IPluginRepository
    {
        private readonly PluginConfiguratorDbContext _context;

        public PluginRepository(PluginConfiguratorDbContext context)
        {
            _context = context;
        }

        public IQueryable<Plugin> Query()
        {
            return _context.Plugins.Include(x => x.PluginSettings);
        }

        public IQueryable<Plugin> List()
        {
            return Query();
        }

        public Plugin GetById(int id)
        {
            return Query().SingleOrDefault(x => x.Id == id);
        }

        public Plugin Save(Plugin plugin)
        {
            if (plugin.Id <= 0)
            {
                // do an insert
                _context.Plugins.Add(plugin);
                _context.SaveChanges();

                return GetById(plugin.Id); 
            }
            else
            {
                // do an update on the graph
                // UpdatePlugin(plugin);
                _context.UpdateGraph(plugin, map => map.OwnedCollection(p => p.PluginSettings));
                _context.SaveChanges();

                return GetById(plugin.Id);
            }
        }

        public bool Delete(int id)
        {
            var plugin = GetById(id);
            return Delete(plugin);
        }

        public bool Delete(Plugin plugin)
        {
            _context.Plugins.Attach(plugin);
            _context.Plugins.Remove(plugin);
            var count = _context.SaveChanges();

            return count > 1;
        }
    }
}
