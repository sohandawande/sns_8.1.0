﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PluginSettingsApi.Data.Models
{
    public class PluginSetting : IEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual int Id { get; set; }
        public virtual Plugin Plugin { get; set; }
        public virtual int PluginId { get; set; }
        public virtual string Name { get; set; }
        public virtual string Value { get; set; }
    }

    public class PluginSettingComparer : IEqualityComparer<PluginSetting>
    {
        public bool Equals(PluginSetting x, PluginSetting y)
        {
            return x.Id == y.Id;
        }

        public int GetHashCode(PluginSetting obj)
        {
            return obj.Id.GetHashCode();
        }
    }
}
