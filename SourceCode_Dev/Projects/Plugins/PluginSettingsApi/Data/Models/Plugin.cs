﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PluginSettingsApi.Data.Models
{
    public class Plugin : IEntity
    {
        public Plugin()
        {
            PluginSettings = new Collection<PluginSetting>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual bool Active { get; set; }
        public virtual bool Installed { get; set; }

        [ForeignKey("PluginId")]
        public ICollection<PluginSetting> PluginSettings { get; set; }
    }
}
