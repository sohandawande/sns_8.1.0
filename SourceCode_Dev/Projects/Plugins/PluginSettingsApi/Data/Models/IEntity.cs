namespace PluginSettingsApi.Data.Models
{
    public interface IEntity
    {
        int Id { get; set; }
    }
}