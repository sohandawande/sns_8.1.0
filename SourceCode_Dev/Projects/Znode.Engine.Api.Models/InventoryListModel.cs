﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
	public class InventoryListModel : BaseListModel
	{
		public Collection<InventoryModel> Inventories { get; set; }

		public InventoryListModel()
		{
			Inventories = new Collection<InventoryModel>();
		}
	}
}