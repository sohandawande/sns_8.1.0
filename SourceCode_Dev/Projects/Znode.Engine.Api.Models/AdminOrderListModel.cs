﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class AdminOrderListModel : BaseListModel
    {

        public Collection<AdminOrderModel> OrderList { get; set; }

        public AdminOrderListModel()
        {
            OrderList = new Collection<AdminOrderModel>();
        }
    }
}
