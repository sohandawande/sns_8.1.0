﻿namespace Znode.Engine.Api.Models
{
	public class PortalCountryModel : BaseModel
	{
		public CountryModel Country { get; set; }
		public string CountryCode { get; set; }
		public bool IsBillingActive { get; set; }
		public bool IsShippingActive { get; set; }
		public int PortalCountryId { get; set; }
		public int PortalId { get; set; }
        public string CountryName { get; set; }

		public PortalCountryModel Sample()
		{
			return new PortalCountryModel
			{
				CountryCode = "US",
				IsBillingActive = true,
				IsShippingActive = true,
				PortalCountryId = 1,
				PortalId = 1,
                CountryName ="United States"
			};
		}
	}
}