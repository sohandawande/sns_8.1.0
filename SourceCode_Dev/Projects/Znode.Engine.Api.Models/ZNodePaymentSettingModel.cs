﻿using System;

namespace Znode.Engine.Api.Models
{
    public class ZNodePaymentSettingModel
    {
        public ZNodePaymentSettingModel()
        {
        }
    
        public int PaymentSettingID { get; set; }
        public int PaymentTypeID { get; set; }
        public Nullable<int> ProfileID { get; set; }
        public Nullable<int> GatewayTypeID { get; set; }
        public string GatewayUsername { get; set; }
        public string GatewayPassword { get; set; }
        public Nullable<bool> EnableVisa { get; set; }
        public Nullable<bool> EnableMasterCard { get; set; }
        public Nullable<bool> EnableAmex { get; set; }
        public Nullable<bool> EnableDiscover { get; set; }
        public Nullable<bool> EnableRecurringPayments { get; set; }
        public Nullable<bool> EnableVault { get; set; }
        public string TransactionKey { get; set; }
        public bool ActiveInd { get; set; }
        public int DisplayOrder { get; set; }
        public bool TestMode { get; set; }
        public string Partner { get; set; }
        public string Vendor { get; set; }
        public bool PreAuthorize { get; set; }
        public Nullable<bool> IsRMACompatible { get; set; }    
    }
}
