﻿
namespace Znode.Engine.Api.Models
{
    /// <summary>
    /// Model for Rejection Message.
    /// </summary>
    public class RejectionMessageModel : BaseModel
    {
        public string MessageKey { get; set; }

        public string MessageValue { get; set; }

        public string StoreName { get; set; }

        public int PortalId { get; set; }

        public int LocaleId { get; set; }

        public int RejectionMessagesId { get; set; }
    }
}
