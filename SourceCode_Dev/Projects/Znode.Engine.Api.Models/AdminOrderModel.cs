﻿using System;

namespace Znode.Engine.Api.Models
{
    public class AdminOrderModel : BaseModel
    {
        public int OrderID { get; set; }
        public int PortalId { get; set; }
        public int AccountID { get; set; }
        public int OrderStateID { get; set; }
        public int ShippingID { get; set; }
        public int PaymentTypeId { get; set; }
        public int PaymentStatusID { get; set; }
        public string OrderStatus { get; set; }
        public string ShippingTypeName { get; set; }
        public string PaymentTypeName { get; set; }
        public string PaymentStatusName { get; set; }
        public string BillingFirstName { get; set; }
        public string BillingLastName { get; set; }
        public decimal Total { get; set; }
        public DateTime OrderDate { get; set; }
        public DateTime ShipDate { get; set; }
        public string TrackingNumber { get; set; }
        public int PaymentSettingId { get; set; }
        public string StoreName { get; set; }
        public string BillingCompanyName { get; set; }
        public string CardTransactionId { get; set; }
    }
}
