﻿
namespace Znode.Engine.Api.Models
{
    public class CSSModel : BaseModel
    {
        public int CSSID { get; set; }
        public int ThemeID { get; set; }
        public string Name { get; set; }
        public ThemeModel Theme { get; set; }

        public CSSModel Sample()
        {
            return new CSSModel
            {
                CSSID = 1,
                ThemeID = 1,
                Name = "Default"
            };
        }
    }
}
