﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class LocaleModel : BaseModel
    {
        public int LocaleID { get; set; }
        public string LocaleCode { get; set; }
        public string LocaleDescription { get; set; }
    }
}
