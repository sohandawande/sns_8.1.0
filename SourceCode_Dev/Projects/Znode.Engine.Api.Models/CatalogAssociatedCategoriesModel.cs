﻿
namespace Znode.Engine.Api.Models
{
    public class CatalogAssociatedCategoriesModel : BaseModel
    {
        public int CatalogId { get; set; }
        public int CategoryId { get; set; }
        public int CategoryNodeId { get; set; }  
        public string Name { get; set; }
        public int? ParentCategoryNodeId { get; set; }
        public string SeoUrl { get; set; }
        public bool ActiveInd { get; set; }
        public bool VisibleInd { get; set; }
        public int? DisplayOrder { get; set; }
        public string CatalogName { get; set; }
        //PRFT Custom Code: Start
        //Property for Manufacturer ID if it is Manufacturer as Category
        public string Custom1 { get; set; }
        //PRFT Custom Code: End
    }
}
