﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class PRFTOrderSubmissionModel : BaseModel
    {
        public bool IsSubmitted { get; set; }
        public string ErrorMessage { get; set; }
    }
}
