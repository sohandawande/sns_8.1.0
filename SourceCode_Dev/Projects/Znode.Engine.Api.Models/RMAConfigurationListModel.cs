﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class RMAConfigurationListModel : BaseListModel
    {
        public Collection<RMAConfigurationModel> RMAConfigurations { get; set; }

        /// <summary>
        /// Constructor for RMAConfigurationListModel
        /// </summary>
        public RMAConfigurationListModel()
		{
            RMAConfigurations = new Collection<RMAConfigurationModel>();
		}
    }
}
