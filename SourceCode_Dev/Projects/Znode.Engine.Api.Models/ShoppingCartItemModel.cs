﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace Znode.Engine.Api.Models
{
	public class ShoppingCartItemModel : BaseModel
	{
		public string Description { get; set; }
		public decimal ExtendedPrice { get; set; }
		public string ExternalId { get; set; }
		public int ProductId { get; set; }
		public int Quantity { get; set; }
		public decimal ShippingCost { get; set; }
        public decimal ProductDiscountAmount { get; set; }
		public int ShippingOptionId { get; set; }
		public string Sku { get; set; }
		public int SkuId { get; set; }
		public int[] AttributeIds { get; set; }
		public decimal UnitPrice { get; set; }
		public bool InsufficientQuantity { get; set; }
        public string CartDescription { get; set; }
		public int[] AddOnValueIds { get; set; }
		public Dictionary<int,string> AddOnValuesCustomText { get; set; }
        //Znode Version 7.2.2 - Add ImagePath
        public string ImagePath { get; set; }
        public string ProductName { get; set; }
        public string ImageMediumPath { get; set; }
        public int? MaxQuantity { get; set; }

        public bool RecurringBillingInd { get; set; }
        public decimal? RecurringBillingInitialAmount { get; set; }
        public string RecurringBillingFrequency { get; set; }

        public Collection<BundleItemModel> BundleItems { get; set; }

        [IgnoreDataMember]
        public ProductModel Product { get; set; }

	    [IgnoreDataMember]
		public AddressModel ShippingAddress { set; get; }

        /// <summary>
        /// To have Ordershipment details: Dictionary&lt;AddressID, Quantity&gt;
        /// </summary>
        public List<OrderShipmentModel> MultipleShipToAddress { get; set; }

        public decimal TaxCost { get; set; }

		public ShoppingCartItemModel()
		{
			ExternalId = Guid.NewGuid().ToString();
			MultipleShipToAddress = new List<OrderShipmentModel>();
		}

		public ShoppingCartItemModel Sample()
		{
			return new ShoppingCartItemModel
			{

			};
		}

        //PRFT Custom Code: Start
        public decimal ERPUnitPrice { get; set; }
        public decimal ERPExtendedPrice { get; set; }
        public string BackOrderMessage { get; set; }
        public bool AllowBackOrder { get; set; }
        //PRFT Custom Code: End
    }
}
