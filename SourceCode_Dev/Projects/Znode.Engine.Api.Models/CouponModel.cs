﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class CouponModel:BaseModel
    {
        public string Coupon { get; set; }
        public string CouponMessage { get; set; }
        public bool CouponApplied { get; set; }
        public bool CouponValid { get; set; }
    }
}
