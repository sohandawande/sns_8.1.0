﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class ZipCodeModel : BaseModel
    {
        public int ZipCodeId { get; set; }

        public string ZIP { get; set; }
        public string ZIPType { get; set; }
        public string CityName { get; set; }
        public string CityType { get; set; }
        public string StateName { get; set; }
        public string StateAbbr { get; set; }
        public string AreaCode { get; set; }
        public string CountyName { get; set; }
        public string CountyFips { get; set; }
        public string StateFips { get; set; }
        public string MSACode { get; set; }
        public string TimeZone { get; set; }

        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public decimal UTC { get; set; }

        public char DST { get; set; }
    }
}
