﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
	public class PromotionTypeListModel : BaseListModel
	{
		public Collection<PromotionTypeModel> PromotionTypes { get; set; }

		public PromotionTypeListModel()
		{
			PromotionTypes = new Collection<PromotionTypeModel>();
		}
	}
}