﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class RMARequestModel : BaseModel
    {

        public RMARequestModel()
        {
            RMARequestItems = new List<RMARequestItemModel>();
        }

        public int RMARequestID { get; set; }
        public int OrderId { get; set; }
        public string StoreName { get; set; }
        public string BillingFirstName { get; set; }
        public string BillingLastName { get; set; }
        public DateTime RequestDate { get; set; }
        public string RequestStatus { get; set; }
        public int? RequestStatusId { get; set; }
        public decimal? TaxCost { get; set; }
        public decimal? Discount { get; set; }
        public decimal? SubTotal { get; set; }
        public decimal? Total { get; set; }
        public string Comments { get; set; }
        public string RequestNumber { get; set; }
        public int PortalId { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public List<RMARequestItemModel> RMARequestItems { get; set; }
        public string CustomerName { get; set; }
    }
}
