﻿namespace Znode.Engine.Api.Models
{
	public class ShippingRuleTypeModel : BaseModel
	{
		public string ClassName { get; set; }
		public string Description { get; set; }
		public bool IsActive { get; set; }
		public string Name { get; set; }
		public int ShippingRuleTypeId { get; set; }

		public ShippingRuleTypeModel Sample()
		{
			return new ShippingRuleTypeModel
			{
				ClassName = null,
				Description = "Quantity-Based Rate",
				IsActive = true,
				Name = "Quantity-Based Rate",
				ShippingRuleTypeId = 1
			};
		}
	}
}
