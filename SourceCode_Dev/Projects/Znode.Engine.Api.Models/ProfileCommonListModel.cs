﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
    public class ProfileCommonListModel : BaseListModel
    {
        public Collection<ProfileCommonModel> Profiles { get; set; }

        public ProfileCommonListModel()
		{
            Profiles = new Collection<ProfileCommonModel>();
		}
    }
}
