﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
    public class CategoryLevelSettingListModel : BaseListModel
    {
        public CategoryLevelSettingListModel()
        {
            Categories = new Collection<CategoryLevelSettingModel>();
        }
        public Collection<CategoryLevelSettingModel> Categories { get; set; }
    }
}
