﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
    /// <summary>
    /// List model for content page revision.
    /// </summary>
    public class ContentPageRevisionListModel : BaseListModel
    {
        public Collection<ContentPageRevisionModel> ContentPageRevisions { get; set; }

        public ContentPageRevisionListModel()
        {
            ContentPageRevisions = new Collection<ContentPageRevisionModel>();
        }
    }
}
