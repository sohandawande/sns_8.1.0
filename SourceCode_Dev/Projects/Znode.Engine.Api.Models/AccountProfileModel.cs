﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class AccountProfileModel : BaseModel
    {
        public int AccountProfileID { get; set; }
        public int? ProfileID { get; set; }
        public int AccountID { get; set; }
    }
}
