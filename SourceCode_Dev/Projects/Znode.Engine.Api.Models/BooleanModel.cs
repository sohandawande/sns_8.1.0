﻿
namespace Znode.Engine.Api.Models
{
    /// <summary>
    /// This the boolean mode class which will be returned in case of boolean values need to sent as response
    /// </summary>
    public class BooleanModel : BaseModel
    {
        public BooleanModel()
        {
        }

        public bool disabled { get; set; }
    }
}
