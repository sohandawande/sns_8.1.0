﻿namespace Znode.Engine.Api.Models
{
	public class SupplierModel : BaseModel
	{
		public string ContactEmail { get; set; }
		public string ContactFirstName { get; set; }
		public string ContactLastName { get; set; }
		public string ContactPhone { get; set; }
		public string Custom1 { get; set; }
		public string Custom2 { get; set; }
		public string Custom3 { get; set; }
		public string Custom4 { get; set; }
		public string Custom5 { get; set; }
		public string Description { get; set; }
		public int? DisplayOrder { get; set; }
		public string EmailNotificationTemplate { get; set; }
		public bool EnableEmailNotification { get; set; }
		public string ExternalId { get; set; }
		public string ExternalSupplierNumber { get; set; }
		public bool IsActive { get; set; }
		public string Name { get; set; }
		public string NotificationEmail { get; set; }
		public int? PortalId { get; set; }
		public int SupplierId { get; set; }
		public SupplierTypeModel SupplierType { get; set; }
		public int? SupplierTypeId { get; set; }
        public AddressModel Address { get; set; }
        public AccountModel Account { get; set; }
        public string ClassName { get; set; }

		public SupplierModel Sample()
		{
			return new SupplierModel
			{
				ContactEmail = "john@smith.com",
				ContactFirstName = "John",
				ContactLastName = "Smith",
				ContactPhone = "111-222-3333",
				Custom1 = "",
				Custom2 = "",
				Custom3 = "",
				Custom4 = "",
				Custom5 = "",
				Description = "This is a supplier",
				DisplayOrder = 1,
				EmailNotificationTemplate = "",
				EnableEmailNotification = true,
				ExternalId = "",
				ExternalSupplierNumber = "ABC123",
				IsActive = true,
				Name = "Supplier",
				NotificationEmail = "supplier@supplier.com",
				PortalId = null,
				SupplierId = 1,
				SupplierTypeId = 1
			};
		}
	}
}
