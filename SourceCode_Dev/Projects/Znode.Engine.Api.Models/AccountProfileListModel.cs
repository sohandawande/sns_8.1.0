﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class AccountProfileListModel : BaseListModel
    {
        public Collection<AccountProfileModel> AccountProfile { get; set; }

        public AccountProfileListModel()
        {
            AccountProfile = new Collection<AccountProfileModel>();
        }
    }
}
