﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
	public class ShippingTypeListModel : BaseListModel
	{
		public Collection<ShippingTypeModel> ShippingTypes { get; set; }

		public ShippingTypeListModel()
		{
			ShippingTypes = new Collection<ShippingTypeModel>();
		}
	}
}