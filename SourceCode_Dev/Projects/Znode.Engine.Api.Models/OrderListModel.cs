﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
	public class OrderListModel : BaseListModel
	{
		public Collection<OrderModel> Orders { get; set; }

		public OrderListModel()
		{
			Orders = new Collection<OrderModel>();
		}
	}
}
