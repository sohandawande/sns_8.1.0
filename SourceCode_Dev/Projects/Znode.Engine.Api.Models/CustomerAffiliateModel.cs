﻿
namespace Znode.Engine.Api.Models
{
    public class CustomerAffiliateModel : BaseModel
    {
        public CustomerAffiliateModel()
        {
            AccountInformation = new AccountModel();
        }

        public AccountModel AccountInformation { get; set; }        
        public string AffiliateLink { get; set; }
        public decimal? AmountOwed { get; set; }
               
    }
}
