﻿using System;
using System.Collections.Generic;

namespace Znode.Engine.Api.Models
{
    /// <summary>
    /// This is the model used for setting up permissions on store
    /// </summary>
    public class RolesModel : BaseModel
    {
        public RolesModel()
        { }

        public List<Tuple<string, int, bool>> Permissions { get; set; }
    }
}
