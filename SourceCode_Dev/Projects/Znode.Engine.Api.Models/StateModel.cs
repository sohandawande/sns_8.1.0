﻿namespace Znode.Engine.Api.Models
{
	public class StateModel : BaseModel
	{
		public string Code { get; set; }
		public string CountryCode { get; set; }
		public string Name { get; set; }

		public StateModel Sample()
		{
			return new StateModel
			{
				Code = "OH",
				CountryCode = "US",
				Name = "OHIO"
			};
		}
	}
}