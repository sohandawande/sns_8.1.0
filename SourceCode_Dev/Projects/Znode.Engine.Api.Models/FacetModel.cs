﻿
namespace Znode.Engine.Api.Models
{
    public class FacetModel : BaseModel
    {
        public int FacetID { get; set; }
        public int? FacetGroupID { get; set; }
        public string FacetName { get; set; }
        public int? FacetDisplayOrder { get; set; }
        public string IconPath { get; set; }
    }
}
