﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    //List Model for Store 
    public class StoreListModel : BaseListModel
    {
        public Collection<StoreModel> Stores { get; set; }

        public StoreListModel()
		{
            Stores = new Collection<StoreModel>();
		}
    }
}
