﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
    public class ProductAttributeListModel : BaseListModel
    {
        public Collection<AttributeModel> Attributes { get; set; }
       
        public ProductAttributeListModel()
		{
            Attributes = new Collection<AttributeModel>();
		}
    }
}
