﻿namespace Znode.Engine.Api.Models
{
	public class CaseTypeModel : BaseModel
	{
		public int CaseTypeId { get; set; }
		public string Name { get; set; }

		public CaseTypeModel Sample()
		{
			return new CaseTypeModel
			{
				CaseTypeId = 1,
				Name = "Case associated with Account"
			};
		}
	}
}
