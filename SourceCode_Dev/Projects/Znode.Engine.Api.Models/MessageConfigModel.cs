﻿
namespace Znode.Engine.Api.Models
{
	public class MessageConfigModel : BaseModel
	{
		public string Description { get; set; }
		public string Key { get; set; }
		public int? LocaleId { get; set; }
		public int MessageConfigId { get; set; }
		public MessageTypeModel MessageType { get; set; }
		public int MessageTypeId { get; set; }
		public string PageSeoName { get; set; }
		public int PortalId { get; set; }
		public string Value { get; set; }
        public string PortalName { get; set; }
        public string CustomErrorMessage { get; set; }

		public MessageConfigModel Sample()
		{
			return new MessageConfigModel
			{
				Description = "",
				Key = "CurrentStoreSpecial",
				LocaleId = 43,
				MessageConfigId = 1,
				MessageTypeId = 1,
				PageSeoName = "",
				PortalId = 1,
				Value = "Current Store Special"
			};
		}
	}
}
