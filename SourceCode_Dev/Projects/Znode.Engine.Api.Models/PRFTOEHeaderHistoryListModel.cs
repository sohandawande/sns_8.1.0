﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class PRFTOEHeaderHistoryListModel
    {
        public Collection<PRFTOEHeaderHistoryModel> OEHeaderHistory { get; set; }

        public PRFTOEHeaderHistoryListModel()
        {
            OEHeaderHistory = new Collection<PRFTOEHeaderHistoryModel>();
        }
    }
}
