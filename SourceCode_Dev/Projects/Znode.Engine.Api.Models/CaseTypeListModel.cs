﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
	public class CaseTypeListModel : BaseListModel
	{
		public Collection<CaseTypeModel> CaseTypes { get; set; }

		public CaseTypeListModel()
		{
			CaseTypes = new Collection<CaseTypeModel>();
		}
	}
}
