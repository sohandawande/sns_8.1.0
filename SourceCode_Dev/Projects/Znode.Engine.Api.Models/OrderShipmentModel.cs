﻿namespace Znode.Engine.Api.Models
{
    public class OrderShipmentModel : BaseModel
    {
        public int OrderShipmentId { get; set; }
        public string ShipName { get; set; }
        public int? ShippingOptionId { get; set; }
        public string ShipToCity { get; set; }
        public string ShipToCompanyName { get; set; }
        public string ShipToCountryCode { get; set; }
        public string ShipToEmail { get; set; }
        public string ShipToFirstName { get; set; }
        public string ShipToLastName { get; set; }
        public string ShipToPhoneNumber { get; set; }
        public string ShipToPostalCode { get; set; }
        public string ShipToStateCode { get; set; }
        public string ShipToStreetAddress1 { get; set; }
        public string ShipToStreetAddress2 { get; set; }

        public int AddressId { get; set; }
        public int Quantity { get; set; }
        public decimal TaxCost { get; set; }
        public decimal ShippingCost { get; set; }
        public string ShippingName { get; set; }

        public OrderShipmentModel Sample()
        {
            return new OrderShipmentModel
                {
                    OrderShipmentId = 1,
                    ShipName = "Default Billing & Shipping",
                    ShippingOptionId = 9,
                    ShipToCity = "Columbus",
                    ShipToCompanyName = "Your Company",
                    ShipToCountryCode = "US",
                    ShipToEmail = "test@test.com",
                    ShipToFirstName = "John",
                    ShipToLastName = "Smith",
                    ShipToPhoneNumber = "555-111-2222",
                    ShipToPostalCode = "43210",
                    ShipToStateCode = "OH",
                    ShipToStreetAddress1 = "123 Any Street",
                    ShipToStreetAddress2 = null
                };
        }
    }
}
