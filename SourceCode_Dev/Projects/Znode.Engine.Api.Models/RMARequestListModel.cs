﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class RMARequestListModel:BaseListModel
    {
        public Collection<RMARequestModel> RMARequests { get; set; }

        public RMARequestListModel()
		{
            RMARequests = new Collection<RMARequestModel>();
		}
    }
}
