﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
   public class PortalProfileListModel:BaseListModel
    {
         public Collection<PortalProfileModel> PortalProfiles { get; set; }

         public PortalProfileListModel()
		{
            PortalProfiles = new Collection<PortalProfileModel>();
		}
    }
}
