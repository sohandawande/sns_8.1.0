﻿using System;

namespace Znode.Engine.Api.Models
{
	public class UserModel : BaseModel
	{
		public string Comment { get; set; }
		public DateTime CreateDate { get; set; }
		public string Email { get; set; }
		public bool IsApproved { get; set; }
		public bool IsLockedOut { get; set; }
		public bool IsOnline { get; set; }
		public DateTime LastActivityDate { get; set; }
		public DateTime LastLockoutDate { get; set; }
		public DateTime LastLoginDate { get; set; }
		public DateTime LastPasswordChangedDate { get; set; }
		public string NewPassword { get; set; }
		public string Password { get; set; }
		public string PasswordAnswer { get; set; }
		public string PasswordQuestion { get; set; }
		public string ProviderName { get; set; }
		public Guid UserId { get; set; }
		public string Username { get; set; }
        //Znode Version 7.2.2
        public string PasswordToken { get; set; }
        public bool IsConfirmed { get; set; }
        public int? ParentAccountId { get; set; }   //PRFT Custom Code
		public UserModel Sample()
		{
			return new UserModel
			{
				Comment = null,
				CreateDate = Convert.ToDateTime("2013-07-25 17:30:17.000"),
				Email = "znodeadmin1@test.com",
				IsApproved = true,
				IsLockedOut = false,
				IsOnline = false,
				LastActivityDate = Convert.ToDateTime("2013-09-24 14:09:29.090"),
				LastLockoutDate = Convert.ToDateTime("1754-01-01 00:00:00.000"),
				LastLoginDate = Convert.ToDateTime("2013-09-24 14:06:26.307"),
				LastPasswordChangedDate = Convert.ToDateTime("2013-07-25 17:30:17.000"),
				Password = "oOL4PhVkIbDiz2Aucn6n617aj76T6ihQ6wgo+87Tr4HbwvIjJ5FRr8IvRuWZJYnF",
				PasswordAnswer = "oOL4PhVkIbDiz2Aucn6n6/dJiH4deOwjRjJq+6UQd88=",
				PasswordQuestion = "What is the name of your favorite pet?",
				ProviderName = "ZNodeMembershipProvider",
				UserId = new Guid("8086A820-D3F6-4763-AE07-CE84AB85E2E7"),
				Username = "znodeadmin1"
			};
		}
	}
}