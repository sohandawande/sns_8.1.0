﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class ProductReviewStateModel : BaseModel
    {
        public int ReviewStateID { get; set; }
        public string ReviewStateName { get; set; }
        public string Description { get; set; }
    }
}
