﻿using System;
using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
	public class SkuModel : BaseModel
	{
		public Collection<AttributeModel> Attributes { get; set; }
        public Collection<CustomerPricingModel> CustomerPricingSku { get; set; }
		public string Custom1 { get; set; }
		public string Custom2 { get; set; }
		public string Custom3 { get; set; }
		public int? DisplayOrder { get; set; }
		public string ExternalId { get; set; }
		public decimal? FinalPrice { get; set; }
		public string ImageAltTag { get; set; }
		public string ImageFile { get; set; }
		public string ImageLargePath { get; set; }
		public string ImageMediumPath { get; set; }
		public string ImageSmallPath { get; set; }
		public string ImageSmallThumbnailPath { get; set; }
		public string ImageThumbnailPath { get; set; }
		public InventoryModel Inventory { get; set; }
		public bool IsActive { get; set; }
		public string Note { get; set; }
		public int ProductId { get; set; }
		public decimal? ProductPrice { get; set; }
		public decimal? PromotionPrice { get; set; }
		public string RecurringBillingFrequency { get; set; }
		public decimal? RecurringBillingInitialAmount { get; set; }
		public string RecurringBillingPeriod { get; set; }
		public int? RecurringBillingTotalCycles { get; set; }
		public decimal? RetailPriceOverride { get; set; }
		public decimal? SalePriceOverride { get; set; }
		public string Sku { get; set; }
		public int SkuId { get; set; }
		public SupplierModel Supplier { get; set; }
		public int? SupplierId { get; set; }
		public DateTime? UpdateDate { get; set; }
		public DateTime? WebServiceDownloadDate { get; set; }
		public decimal? WeightAdditional { get; set; }
		public decimal? WholesalePriceOverride { get; set; }

        public string AttributesIds { get; set; }
        public string Error { get; set; }
        public string UserType { get; set; }
        public string UserName { get; set; }
		public SkuModel()
		{
			Attributes = new Collection<AttributeModel>();
            Inventory = new InventoryModel();
            CustomerPricingSku = new Collection<CustomerPricingModel>();
		}

		public SkuModel Sample()
		{
			return new SkuModel
			{
				Custom1 = "",
				Custom2 = "",
				Custom3 = "",
				DisplayOrder = null,
				ExternalId = "",
				FinalPrice = null,
				ImageAltTag = "Green Apple",
				ImageFile = "apple-green.jpg",
				ImageLargePath = "~/data/default/images/catalog/450/apple-green.jpg",
				ImageMediumPath = "~/data/default/images/catalog/250/apple-green.jpg",
				ImageSmallPath = "~/data/default/images/catalog/100/apple-green.jpg",
				ImageSmallThumbnailPath = "~/data/default/images/catalog/37/apple-green.jpg",
				ImageThumbnailPath = "~/data/default/images/catalog/50/apple-green.jpg",
				IsActive = true,
				Note = "",
				ProductId = 302,
				ProductPrice = null,
				PromotionPrice = 1.23m,
				RecurringBillingFrequency = "1",
				RecurringBillingPeriod = "",
				RecurringBillingInitialAmount = 0.00m,
				RecurringBillingTotalCycles = 1,
				RetailPriceOverride = null,
				SalePriceOverride = 2.99m,
				Sku = "apg234",
				SkuId = 2,
				SupplierId = 1,
				UpdateDate = Convert.ToDateTime("2013-12-06 02:04:16.850"),
				WebServiceDownloadDate = null,
				WeightAdditional = 0.70m,
				WholesalePriceOverride = null,
			};
		}
	}
}
