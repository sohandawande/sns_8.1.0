﻿using System;

namespace Znode.Engine.Api.Models
{
    public class SkuProfileEffectiveModel : BaseModel
    {
        public int SkuProfileEffectiveID { get; set; }
        public int SkuId { get; set; }
        public int ProfileId { get; set; }
        public string Name { get; set; }

        public DateTime EffectiveDate { get; set; }
    }
}
