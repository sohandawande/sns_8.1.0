﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
    public class IssuedGiftCardListModel : BaseListModel
    {
        public IssuedGiftCardListModel()
        {
            IssuedGiftCardModels = new Collection<IssuedGiftCardModel>();
        }
        public Collection<IssuedGiftCardModel> IssuedGiftCardModels { get; set; }
    }
}
