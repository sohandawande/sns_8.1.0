﻿using System;
using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
    public class CategoryModel : BaseModel
    {
        public string AlternateDescription { get; set; }
        public int CategoryId { get; set; }
        public Collection<CategoryNodeModel> CategoryNodes { get; set; }
        public Collection<CategoryProfileModel> CategoryProfiles { get; set; }
        public string CategoryUrl { get; set; }
        public DateTime? CreateDate { get; set; }
        public string Custom1 { get; set; }
        public string Custom2 { get; set; }
        public string Custom3 { get; set; }
        public string Description { get; set; }
        public int? DisplayOrder { get; set; }
        public string ExternalId { get; set; }
        public string ImageAltTag { get; set; }
        public string ImageFile { get; set; }
        public string ImageLargePath { get; set; }
        public string ImageMediumPath { get; set; }
        public string ImageSmallPath { get; set; }
        public string ImageSmallThumbnailPath { get; set; }
        public string ImageThumbnailPath { get; set; }
        public bool IsVisible { get; set; }
        public string Name { get; set; }
        public string ProductIds { get; set; }
        public string SeoDescription { get; set; }
        public string SeoKeywords { get; set; }
        public string SeoTitle { get; set; }
        public string SeoUrl { get; set; }
        public string ShortDescription { get; set; }
        public bool RedirectUrlInd { get; set; }
        public Collection<CategoryModel> Subcategories { get; set; }
        public bool SubcategoryGridIsVisible { get; set; }
        public bool IsAssociate { get; set; }
        public string Title { get; set; }
        public DateTime? UpdateDate { get; set; }
        //Znode version 7.2.2 To get Category Banner
        public string CategoryBanner { get; set; }
        public int? PortalID { get; set; }

        public int ProductId { get; set; }
        public string CategoryIds { get; set; }
        public string UserName { get; set; }

        public bool IsFromSitecore { get; set; }
        public int ProductCount { get; set; }
        public int ParentCategoryNodeId { get; set; }
        public bool IsActive  { get; set; }

        public int ActiveProductCount { get; set; }

        public CategoryModel()
        {
            CategoryNodes = new Collection<CategoryNodeModel>();
            CategoryProfiles = new Collection<CategoryProfileModel>();
            Subcategories = new Collection<CategoryModel>();
        }

        public CategoryModel Sample()
        {
            return new CategoryModel
            {
                AlternateDescription = "",
                CategoryId = 85,
                CategoryUrl = "~/category/Fruits/85",
                CreateDate = Convert.ToDateTime("2013-07-01 00:00:00.000"),
                Custom1 = "",
                Custom2 = "",
                Custom3 = "",
                Description = "",
                DisplayOrder = 5,
                ExternalId = null,
                ImageAltTag = "Flowers",
                ImageFile = "Poppy.jpg",
                ImageLargePath = "~/data/default/images/catalog/450/Poppy.jpg",
                ImageMediumPath = "~/data/default/images/catalog/250/Poppy.jpg",
                ImageSmallPath = "~/data/default/images/catalog/100/Poppy.jpg",
                ImageSmallThumbnailPath = "~/data/default/images/catalog/37/Poppy.jpg",
                ImageThumbnailPath = "~/data/default/images/catalog/50/Poppy.jpg",
                IsVisible = true,
                Name = "Fruit",
                ProductIds = "302,303,304,305,306,307",
                SeoDescription = "Fruits",
                SeoKeywords = "Fruits",
                SeoTitle = "Fruits",
                SeoUrl = "Fruits",
                ShortDescription = "",
                SubcategoryGridIsVisible = true,
                Title = "Fruit",
                UpdateDate = Convert.ToDateTime("2013-07-31 23:59:59.999"),
                CategoryBanner = ""
            };
        }
    }
}