﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class PRFTSalesRepresentative:BaseModel
    {
        public int SalesRepID { get; set; }
        public string SaleRepNameWithLocation { get; set; }
        public string Email { get; set; }
    }
}
