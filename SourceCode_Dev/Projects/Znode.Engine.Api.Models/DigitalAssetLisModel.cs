﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class DigitalAssetLisModel : BaseListModel
    {
        public Collection<DigitalAssetModel> DigitalAssets { get; set; }
    }
}
