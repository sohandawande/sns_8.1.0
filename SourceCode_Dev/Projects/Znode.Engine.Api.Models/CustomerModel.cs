﻿using System;

namespace Znode.Engine.Api.Models
{
    public class CustomerModel : BaseModel
    {
        /// <summary>
        /// Constructor for CustomerModel
        /// </summary>
        public CustomerModel()
        { 
            
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string CompanyName { get; set; }
        public string ExternalAccountNo { get; set; }
        public string PhoneNumber { get; set; }
        public string FullName { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public string StateCode { get; set; }
        public string UserName { get; set; }

        public int AccountId { get; set; }
        public bool IsConfirmed { get; set; }
        public DateTime? CreateDte { get; set; }
        public DateTime? UpdateDte { get; set; }

        public bool? EnableCustomerPricing { get; set; }
        public Guid? UserId { get; set; }

        //PRFT Custom Code: Start
        public string Custom1 { get; set; }
        public string Custom2 { get; set; }
        public string Custom3 { get; set; }
        public int? AccountTypeId { get; set; }
        public int CustomerUserMappingId { get; set; }
        //PRFT Custom Code: End
    }
}
