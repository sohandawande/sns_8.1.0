﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class RMARequestItemListModel : BaseListModel
    {
        public RMARequestItemListModel()
		{
			RMARequestItemList = new Collection<RMARequestItemModel>();
		}
        public Collection<RMARequestItemModel> RMARequestItemList { get; set; }
    }
}
