﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
  public   class CartItemsModel
    {
        //Znode Version 7.2.2 - Start

        public string BundleItemsIds { get; set; }
        public string Description { get; set; }
        public decimal ExtendedPrice { get; set; }
        public string ExternalId { get; set; }
        public int ProductId { get; set; }
        public int Quantity { get; set; }
        public decimal ShippingCost { get; set; }
        public int ShippingOptionId { get; set; }
        public string Sku { get; set; }
        public int SkuId { get; set; }
        public int[] AttributeIds { get; set; }
        public decimal UnitPrice { get; set; }
        public bool InsufficientQuantity { get; set; }
        public string AddOnValueIds { get; set; }
        public string ProductName { get; set; }
        public string AddOnValuesCustomText { get; set; }
        public ProductsModel Product { get; set; }

        public Collection<BundleViewModel> SelectedBundles { get; set; }

        public CartItemsModel()
        {
            ExternalId = Guid.NewGuid().ToString();
        }
        //Znode Version 7.2.2 - End
    }
}
