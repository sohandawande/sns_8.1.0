﻿using System;

namespace Znode.Engine.Api.Models
{
    public class AccountPaymentModel : BaseModel
    {
        public string TransactionId { get; set; }
        public string Description { get; set; }
        public int AccountPaymentId { get; set; }
        public int AccountId { get; set; }

        public int? OrderId { get; set; }

        public decimal? Amount { get; set; }
        public decimal? AmountOwed { get; set; }

        public DateTime? PaymentDate { get; set; }
    }
}
