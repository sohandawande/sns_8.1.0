﻿namespace Znode.Engine.Api.Models
{
	public class CreditCardModel : BaseModel
	{
		public decimal Amount { get; set; }
		public string AuthorizationCode { get; set; }
		public string CardDataToken { get; set; }
		public string CardExpiration { get; set; }
		public string CardHolderName { get; set; }
		public string CardNumber { get; set; }
		public string CardSecurityCode { get; set; }
		public CreditCardTypeModel? CardType { get; set; }
		public string Description { get; set; }
		public int OrderId { get; set; }
		public decimal ShippingCharge { get; set; }
		public decimal SubTotal { get; set; }
		public decimal TaxCost { get; set; }
		public string TransactionId { get; set; }
        public string CardTransactionId { get; set; }

		public CreditCardModel Sample()
		{
			return new CreditCardModel
			{

			};
		}
	}
}
