﻿namespace Znode.Engine.Api.Models
{
	public class CatalogModel : BaseModel
	{
		public int CatalogId { get; set; }
		public string ExternalId { get; set; }
		public bool IsActive { get; set; }
		public string Name { get; set; }
		public int? PortalId { get; set; }        
        public bool PreserveCategories { get; set; }
        public bool IsDefault { get; set; }
        public bool IsFromSitecore { get; set; }

		public CatalogModel Sample()
		{
			return new CatalogModel
			{
				CatalogId = 1,
				ExternalId = null,
				IsActive = true,
				Name = "Fine Foods Catalog",
				PortalId = null
			};
		}
	}
}