﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class PRFTOELineHistoryModel : BaseModel
    {
        public string ItemNo { get; set; }
        public string ItemDescription1 { get; set; }
        public string ItemDescription2 { get; set; }
        public string QuantityOrdered { get; set; }
        public string QuantityToShip { get; set; }
        public string QuantityReturnedtoStock { get; set; }
        public string QuantityBackordered { get; set; }
        public string UnitPrice { get; set; }
        public string DiscountPercent { get; set; }
        public string UnitOfMeasure { get; set; }
        public string SalesAmount { get; set; }
        
    }
}
