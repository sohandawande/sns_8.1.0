﻿namespace Znode.Engine.Api.Models
{
	public class ProfileModel : BaseModel
	{
		public string EmailList { get; set; }
		public string ExternalId { get; set; }
		public string Name { get; set; }
		public int ProfileId { get; set; }
		public bool ShowOnPartnerSignup { get; set; }
		public bool ShowPricing { get; set; }
		public int? TaxClassId { get; set; }
		public bool TaxExempt { get; set; }
		public bool? UseWholesalePricing { get; set; }
		public decimal? Weighting { get; set; }
        public int AccountProfileID { get; set; }

        public int AccountId { get; set; }

		public ProfileModel Sample()
		{
			return new ProfileModel
			{
				EmailList = null,
				ExternalId = null,
				Name = "Anonymous",
				ProfileId = 1,
				ShowOnPartnerSignup = false,
				ShowPricing = true,
				TaxClassId = null,
				TaxExempt = false,
				UseWholesalePricing = false,
				Weighting = 1.00m
			};
		}
	}
}
