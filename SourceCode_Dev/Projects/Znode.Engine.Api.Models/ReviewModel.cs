﻿using System;

namespace Znode.Engine.Api.Models
{
	public class ReviewModel : BaseModel
	{
		public int? AccountId { get; set; }
		public string Comments { get; set; }
		public string Cons { get; set; }
		public DateTime? CreateDate { get; set; }
		public string CreateUser { get; set; }
		public string Custom1 { get; set; }
		public string Custom2 { get; set; }
		public string Custom3 { get; set; }
		public ProductModel Product { get; set; }
		public int? ProductId { get; set; }
		public string Pros { get; set; }
		public int Rating { get; set; }
		public int ReviewId { get; set; }
		public string Status { get; set; }
		public string Subject { get; set; }
		public string UserLocation { get; set; }
        public string ProductName { get; set; }

		public ReviewModel Sample()
		{
			return new ReviewModel
			{
				AccountId = null,
				Comments = "These apples are very good, highly recommended.",
				Cons = "",
				CreateDate = Convert.ToDateTime("2013-06-14 19:24:51.450"),
				CreateUser = "John Smith",
				Custom1 = "",
				Custom2 = "",
				Custom3 = "",
				ProductId = 302,
				Pros = "",
				Rating = 5,
				ReviewId = 1,
				Status = "A",
				Subject = "Very delicious apples!",
				UserLocation = "Columbus, OH"
			};
		}
	}
}
