﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
	public class CountryListModel : BaseListModel
	{
		public Collection<CountryModel> Countries { get; set; }

		public CountryListModel()
		{
			Countries = new Collection<CountryModel>();
		}
	}
}
