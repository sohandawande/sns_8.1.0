﻿
using System.Runtime.Serialization;
namespace Znode.Engine.Api.Models.Enum
{
    /// <summary>
    /// Enum for List Type
    /// </summary>
    public enum ListType
    {
        TestGrid = 0,
        OperatorXML = 1,
        vw_ZnodeGetWebpagesProfile = 2,
        StoreLocatorList = 3,
        vw_ZNodeGetProductCategoryByProductID = 4,
        vw_ZNodeGetProductCategoryNonAssociated = 5,
        ProductTypeList = 19,
        vw_GetAttributeTypesBy = 8,
        vw_ZNodeSearchAddons = 26,
        vw_ZNodeGetQtyReorderLverl = 31,
        GiftCardList = 34,
        vw_ZNodeGetDistinctProductsByCategory = 25,
        ProductImage = 23,
        ManageMessageList = 18,
        ShippingOptionList = 27,
        ShippingRuleList = 30,
        vw_ZnodeGetChildProduct = 29,
        vw_Znode_TaxClass = 33,
        AssociateBundleProductList = 37,
        ProductFacetList = 41,
        HighlightList = 17,
        ContentPageList = 21,
        ContentPageRevisionList = 24,
        vw_ZNodeGetAssociatedAddOn = 38,
        PaymentOptions = 32,
        PortalCatalogList = 44,
        CatalogList = 43,
        ProductsList = 36,
        CatalogAssociatedCategoryList = 45,
        vw_ZNodeProductSKUDetails = 46,
        vw_ZnodeGetProfilewithSerialNumber = 47,
        CountryList = 53,
        vw_ZNodeSearchCategories = 48,
        SkuProfileList = 52,
        ManufacturerList = 49,
        vw_ZNodeGetCustomerBasedPricing = 51,
        StoreManageUrlList = 50,
        CategoryProductList = 42,
        vw_ZNodeGetPortalCountry = 54,
        vw_ZNodeAdminAccounts = 40,
        BannerList = 55,
        FacetGroupList = 56,
        FacetList = 57,
        PromotionList = 58,
        ProfilesList = 60,
        vw_ZNodeGetProductWithProductBoost = 66,
        ProductLevelSettingList = 73,
        vw_ZNodeGetcustReview = 62,
        vw_ZNodeGetDistinctProductsByCriteria = 63,
        vw_ZNodeGetOrderDetails = 69,
        ContentPageSEOList = 61,
        TaxRuleList = 77,
        vw_ZNodeGetVendorProduct = 64,
        FieldLevelSettingList = 80,
        VendorProductImageList = 75,
        UrlRedirectSEOList = 74,
        vw_ZNodeAccountDetails = 76,
        FranchiesAccountList = 85,
        YouMayLikeProductList = 78,
        FrequentlyBoughtProducts = 83,
        vw_ZNodeGetAvailableAddOn = 59,
        vw_ZnodeProductTier = 68,
        vw_ZNodeGetAssocitedHighLights = 71,
        vw_ZNodeGetAvailableHighlights = 72,
        vw_ZNodeGetDigitalAsset = 81,
        vw_ZNodeSearchCategoriesByPortalCatalog = 70,
        SkuFacetsList = 82,
        ProductReviewHistoryList = 84,
        OrderLineItemList = 79,
        vw_ZNodeGetRejectionMessages = 86,
        VendorImageList = 87,
        AccountAddressList = 88,
        vw_ZNodeGetProductFacetGroup = 89,
        VendorProductAddOnsList = 90,
        VendorProductUnAssociatedAddOnsList = 91,
        SuppliersList = 93,
        FranchiseAdmin_Payment = 167,
        vw_ZnodeTaxClass = 169,
        FranchiseAdmin_TaxRuleList = 170,
        FranchiseAdmin_AddOnList = 176,
        FranchiseAdmin_AddOnValueList = 178,
        FranchiseAdmin_ProductReviewHistoryList = 187,


        EmailTemplateList = 203,
        /// <summary>
        /// Lucene Indexer Status List
        /// </summary>
        LuceneIndexStatus = 127,

        vw_ZNodeSearchRMA = 96,
        FranchiseAccountAddressList = 92,
        ReasonForReturnList = 97,
        RequestStatusList = 118,
        PromotionTypeList = 119,
        TaxRuleTypeList = 120,
        SupplierTypeList = 121,
        ShippingTypeList = 122,
        CustomerAddressList = 124,
        vw_ZNodeFullAccountDetails = 125,

        OrdersList = 126,
        vw_ZNodeSearchCase = 95,
        OrderCustomerSearchList = 128,
        CustomerPricingProduct = 129,
        vw_ZNodeGetAssociatedProfile = 130,
        NotAssociatedProfileWithCustomer = 131,
        AccountPaymentList = 132,
        ReferralCommissionList = 133,
        PromotionProductList = 134,
        vw_ZNodeCategory = 190,
        vw_ZNodeApplicationSetting = 191,

        //PRFT Custom Code:Start
        PRFTCreditApplication = 208,
        InventoryListFromErp = 209,
        PRFTCustomerUserMappingList = 210,
        PRFTUserNotAssociatedToCustomer = 211,
        //PRFT Custom Code:End

        #region Reports
        /// <summary>
        /// Test report
        /// </summary>
        ZNodeGetOrderDetails = 94,

        /// <summary>
        /// Orders and details report.
        /// </summary>
        [EnumMember(Value = "Orders")]
        ZNode_ReportsCustom = 98,

        /// <summary>
        /// Recurring Billing details report.
        /// </summary>
        [EnumMember(Value = "Recurring Billing")]
        ZNode_ReportsRecurring = 99,

        /// <summary>
        /// Account list report.
        /// </summary>
        [EnumMember(Value = "Accounts")]
        ZNode_ReportsAccounts = 100,

        /// <summary>
        /// Best seller report
        /// </summary>
        [EnumMember(Value = "Best Sellers")]
        ZNode_ReportsPopularFiltered = 101,

        /// <summary>
        /// Top Earning Product report
        /// </summary>
        [EnumMember(Value = "Top Earning Product")]
        ZNode_ReportsTopEarningProduct = 107,


        /// <summary>
        /// Service request report.
        /// </summary>
        [EnumMember(Value = "Service Requests")]
        ZNode_ReportsFeedback = 102,

        /// <summary>
        /// Email opt-in customer report.
        /// </summary>
        [EnumMember(Value = "Email Opt-in Customers")]
        ZNode_ReportsOptIn = 103,

        /// <summary>
        /// Reorder level report.
        /// </summary>
        [EnumMember(Value = "Inventory Re-order")]
        ZNode_ReportsReOrder = 104,

        /// <summary>
        /// Frequent customer report.
        /// </summary>
        [EnumMember(Value = "Most Frequent Customers")]
        ZNode_ReportsFrequentFiltered = 105,

        /// <summary>
        /// Top spending customer report
        /// </summary>
        [EnumMember(Value = "Top Spending Customers")]
        ZNode_ReportsVolumeFiltered = 106,

        /// <summary>
        /// Order pick list report.
        /// </summary>
        [EnumMember(Value = "Order Pick List")]
        ZNode_ReportsPicklist = 108,

        /// <summary>
        /// Activity log report.
        /// </summary>
        [EnumMember(Value = "Activity Log")]
        ZNode_ReportsActivityLog = 109,

        /// <summary>
        /// Coupon usage report.
        /// </summary>
        [EnumMember(Value = "Coupon Usage")]
        ZNodeReportsCouponFiltered = 110,

        /// <summary>
        /// Sales tax report.
        /// </summary>
        [EnumMember(Value = "Sales Tax")]
        ZNode_ReportsTaxFiltered_All = 111,

        /// <summary>
        /// Affiliate order report.
        /// </summary>
        [EnumMember(Value = "Affiliate Orders")]
        ZNode_ReportsAffiliateFiltered = 112,

        /// <summary>
        /// Supplier list report.
        /// </summary>
        [EnumMember(Value = "Supplier List")]
        ZNode_ReportsSupplierList = 113,

        /// <summary>
        /// Franchise Order report.
        /// </summary>
        [EnumMember(Value = "Franchise Orders")]
        ZNode_ReportVendorRevenue = 114,

        /// <summary>
        /// Franchise sale by product report.
        /// </summary>
        [EnumMember(Value = "Franchise Sale By Product")]
        ZNode_ReportVendorProductRevenue = 115,

        /// <summary>
        /// Product sold on vendor sites report.
        /// </summary>
        [EnumMember(Value = "Products Sold On Vendor Sites")]
        ZNode_ReportsProductSoldOnVendorSites = 116,

        /// <summary>
        /// Popular search report.
        /// </summary>
        [EnumMember(Value = "Popular Search")]
        ZNode_ReportsSEOFiltered = 117,

        #endregion


        AttributeTypeList = 136,
        vw_AttributeTypeWiseAttributes = 138,
        SearchCategoryList = 139,
        SearchProducts_CreateOrders = 123,
        AssociateCategoryProfiles = 135,
        VendorViewProductList = 140,
        FranchiseAdmin_CustomerReviewList = 144,
        FranchiseAdmin_StoreLocator = 145,
        FranchiseAdmin_YouMayLikeProductList = 147,
        FranchiseAdmin_FrequentlyBoughtProducts = 148,
        FranchiseAdmin_StoreManageUrlList = 141,
        FranchiseAdmin_CatalogList = 142,
        FranchiseAdmin_CatalogAssociatedCategoryList = 143,
        vw_ZnodeGetShippingList = 166,
        FranchiseAdmin_ServiceRequestList = 181,
        FranchiseAdmin_PromotionList = 174,

        #region MallAdmin
        vw_ZNodeGetVendorProductforMallAdmin = 149,
        MallAdmin_ProductReviewHistoryList = 151,
        MallAdmin_ProductSkuList = 152,
        MallAdmin_ProductBundlesList = 153,
        MallAdmin_ImageList = 156,
        MallAdmin_ProductFacetList = 157,
        MallAdmin_ProductAddOnsList = 158,
        MallAdmin_ProductUnAssociatedAddOnsList = 159,
        MallAdmin_SkuProfileList = 160,
        MallAdmin_SkuFacetsList = 161,
        MallAdmin_OrdersList = 180,
        MallAdmin_OrderLineItemList = 182,
        MallAdmin_AddOnValueList = 189,
        #endregion
        FranchiseAdmin_ProductList = 150,
        FranchiseAdmin_CategoryList = 146,
        FranchiseAdmin_ProductCategoryList = 154,
        FranchiseAdmin_ProductSkuList = 155,
        FranchiseAdmin_CategoryAssociateProductList = 162,
        FranchiseAdmin_CategoryProductList = 163,
        FranchiseAdmin_ProductImageList = 164,
        FranchiseAdmin_ContentPageList = 172,
        FranchiseAdmin_ContentPageRevisionList = 173,
        FranchiseAdmin_CustomersList = 175,
        FranchiseCustomerAddressList = 177,
        FranchiseAdmin_ManageMessageList = 171,
        FranchiseAdmin_CustomersOrderList = 179,
        FranchiseAdmin_OrderList = 183,
        RmaRequestList_ManageOrder = 186,

        FranchiseAdmin_ProductAddonsList = 165,
        FranchiseAdmin_ProductTierPricingList = 168,
        FranchiseAdmin_OrderCustomerSearchList = 184,
        FranchiseAdmin_OrderProductSearchList = 185,
        FranchiseAdmin_ShippingRuleList = 188,
        FranchiseAdmin_ReferralCommissionList = 204,
        ThemeList = 205,
        CSSList = 206
    }


    /// <summary>
    /// Enum for Frequeny change in Google Site Map
    /// </summary>
    public enum Frequency
    {
        Daily = 0,
        Always = 1,
        Hourly = 2,
        Weekly = 3,
        Monthly = 4,
        Yearly = 5,
        Never = 6
    }

    /// <summary>
    /// Enum for Priority in Google Site Map
    /// </summary>
    public enum Priority
    {
        First = 100,
        Second = 101,
        Third = 102,
        Fourth = 103,
        Fifth = 104,
        Sixth = 105,
        Seventh = 106,
        Eighth = 107,
        Ninth = 108,
        Tenth = 109,
        Eleventh = 200
    }

    /// <summary>
    /// Enum for XML Site Map in Google Site Map
    /// </summary>
    public enum XMLSiteMap
    {
        [EnumMember(Value = "Xml Site Map")]
        XmlSiteMap = 1,
        [EnumMember(Value = "Google Product Feed")]
        GoogleProductFeed = 2,

        [EnumMember(Value = "Bing Product Feed")]
        BingProductFeed = 3
    }

    /// <summary>
    /// Enum for Last Modification Date in Google Site Map
    /// </summary>
    public enum LastModification
    {
        [EnumMember(Value = "None")]
        None = 1,
        [EnumMember(Value = "Use the database update date")]
        DatabaseDate = 2,
        [EnumMember(Value = "  Use date / time of this update")]
        CustomDate = 3
    }

    /// <summary>
    /// Enum for XML Site Map Type in Google Site Map
    /// </summary>
    public enum XMLSiteMapType
    {
        [EnumMember(Value = "Category")]
        Category = 1,
        [EnumMember(Value = "Content Pages")]
        ContentPages = 2,
        #region PRFT Custom Code
        [EnumMember(Value = "Product")]
        Product = 3
        #endregion
    }

    /// <summary>
    /// Represents the znode lucene index server status names
    /// </summary>
    public enum ZNodeStatusNames
    {
        /// <summary>
        /// In Process
        /// </summary>
        InProcess = 1,

        /// <summary>
        /// Completed
        /// </summary>
        Complete = 2,

        /// <summary>
        /// Failed
        /// </summary>
        Failed = 3,

        /// <summary>
        /// Ignored
        /// </summary>
        Ignored = 4
    }
    public enum EntityType
    {
        Table = 1,
        StoredProcedure = 2,
        View = 3
    }
    public enum ViewOptions
    {
        Grid = 1,
        Tile = 2,
        Graph = 3,
        Report = 4
    }
    public enum EntityName
    {
        R_ApplicationSettingEntity = 1
    }

    public enum GetObjectColumnListParameter
    {
        V = 1,//for View
        U = 2, //for Table
        P = 3 //for Procedure
    }
    public enum ViewMode
    {
        Create = 1,
        Edit = 2,
        Delete = 3
    }

    public enum StoreSection
    {
        General,
        Catalog,
        Url,
        Profile,
        Display,
        Units,
        Countries,
        Shipping,
        Javascript,
        Smtp
    }
}

