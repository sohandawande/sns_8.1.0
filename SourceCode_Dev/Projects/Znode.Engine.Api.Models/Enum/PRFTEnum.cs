﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
namespace Znode.Engine.Api.Models.Enum
{
    public enum Location
    {
        [EnumMember(Value = "Milwaukee")]
        Milwaukee = 01,

        [EnumMember(Value = "Bloomington")]
        Bloomington = 02,
        
        [EnumMember(Value = "IN TRANSIT")]
        InTransit = 03
    }
}
