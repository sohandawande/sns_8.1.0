﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
    public class ReferralCommissionTypeListModel : BaseListModel
    {
        public Collection<ReferralCommissionTypeModel> ReferralCommissionTypes { get; set; }

        /// <summary>
        /// Constructor for ReferralCommissionTypeListModel
        /// </summary>
        public ReferralCommissionTypeListModel()
		{
            ReferralCommissionTypes = new Collection<ReferralCommissionTypeModel>();
		}
    }
}
