﻿using System.Collections.ObjectModel;
namespace Znode.Engine.Api.Models
{
    public class ProductReviewHistoryListModel: BaseListModel
    {
         public Collection<ProductReviewHistoryModel> ProductReviewHistory { get; set; }

         public ProductReviewHistoryListModel()
        {
            ProductReviewHistory = new Collection<ProductReviewHistoryModel>();
        }
    }
}
