﻿
namespace Znode.Engine.Api.Models
{
    public class FieldLevelSettingModel : BaseModel
    {
        public int Id { get; set; }
        public string DocumentName { get; set; }
        public double? Boost { get; set; }
    }
}
