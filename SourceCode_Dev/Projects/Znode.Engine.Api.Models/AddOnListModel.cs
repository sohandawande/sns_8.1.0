﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
	public class AddOnListModel :BaseListModel
	{
		public Collection<AddOnModel> AddOns { get; set; }

		public AddOnListModel()
		{
			AddOns = new Collection<AddOnModel>();
		}
	}
}
