﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
    public class UrlRedirectListModel : BaseListModel
    {
        public Collection<UrlRedirectModel> UrlRedirects { get; set; }

        public UrlRedirectListModel()
        {
            UrlRedirects = new Collection<UrlRedirectModel>();
        }
    }
}
