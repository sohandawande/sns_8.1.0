﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class PRFTERPOrderResponseModel
    {
        public string NewOrderID { get; set; }
        public bool HasError { get; set; }
        public string StatusCode { get; set; }
        public string StatusDescription { get; set; }
    }
}
