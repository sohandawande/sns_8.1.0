﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
    public class FieldLevelSettingListModel : BaseListModel
    {
        public FieldLevelSettingListModel()
        {
            FieldLevelSettings = new Collection<FieldLevelSettingModel>();
        }
        public Collection<FieldLevelSettingModel> FieldLevelSettings { get; set; }
    }
}
