﻿namespace Znode.Engine.Api.Models
{
	public class TierModel : BaseModel
	{
		public decimal Price { get; set; }
		public int ProductId { get; set; }
		public int ProductTierId { get; set; }
		public int? ProfileId { get; set; }
		public int TierEnd { get; set; }
		public int TierStart { get; set; }
        public string ProfileName { get; set; }

		public TierModel Sample()
		{
			return new TierModel
			{
				Price = 1.25m,
				ProductId = 302,
				ProductTierId = 1,
				ProfileId = 3,
				TierEnd = 5,
				TierStart = 1
			};
		}
	}
}