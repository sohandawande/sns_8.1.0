﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
   public class BundleItemModel : BaseModel
    {
       public string ExternalId { get; set; }
       public int[] AddOnValueIds { get; set; }
	   public Dictionary<int, string> AddOnValuesCustomText { get; set; }
       public int ProductId { set; get; }
       public string  Sku { set; get; }
    }
}
