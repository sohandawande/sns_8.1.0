﻿namespace Znode.Engine.Api.Models
{
	public class PaymentOptionModel : BaseModel
	{
		public int DisplayOrder { get; set; }
		public bool? EnableAmericanExpress { get; set; }
		public bool? EnableDiscover { get; set; }
		public bool? EnableMasterCard { get; set; }
		public bool? EnableRecurringPayments { get; set; }
		public bool? EnableVault { get; set; }
		public bool? EnableVisa { get; set; }
		public bool IsActive { get; set; }
		public bool? IsRmaCompatible { get; set; }
		public string Partner { get; set; }
		public PaymentGatewayModel PaymentGateway { get; set; }
		public int? PaymentGatewayId { get; set; }
		public string PaymentGatewayPassword { get; set; }
		public string PaymentGatewayUsername { get; set; }
		public int PaymentOptionId { get; set; }
		public PaymentTypeModel PaymentType { get; set; }
		public int PaymentTypeId { get; set; }
		public bool PreAuthorize { get; set; }
		public int? ProfileId { get; set; }
        public string ProfileName { get; set; } 
		public bool TestMode { get; set; }
		public string TransactionKey { get; set; }
		public string Vendor { get; set; }
        public bool? IsExists { get; set; }
        public string AdditionalFee { get; set; }
		public PaymentOptionModel Sample()
		{
			return new PaymentOptionModel
			{
				DisplayOrder = 1,
				EnableAmericanExpress = true,
				EnableDiscover = true,
				EnableMasterCard = true,
				EnableRecurringPayments = null,
				EnableVault = null,
				EnableVisa = true,
				IsActive = true,
				IsRmaCompatible = null,
				Partner = null,
				PaymentGatewayId = 1,
				PaymentGatewayPassword = "",
				PaymentGatewayUsername = "73BPkIVFnu65hHKjxRDZ0lTxmyNsV9hT",
				PaymentOptionId = 24,
				PaymentTypeId = 2,
				PreAuthorize = false,
				ProfileId = null,
				TestMode = true,
				TransactionKey = "FguRet0ahGXzM5kICiVYqxQWLHiW+1T+",
				Vendor = null
			};
		}
	}
}