﻿namespace Znode.Engine.Api.Models
{
    public class RolePermissionModel:BaseModel
    {
        public int RoleId { get; set; }
        public int PermissionId { get; set; }
        public string EntityAction { get; set; }
    }
}
