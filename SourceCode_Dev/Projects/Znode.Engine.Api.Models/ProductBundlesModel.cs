﻿
namespace Znode.Engine.Api.Models
{
    public class ProductBundlesModel : BaseModel
    {
        public int ParentChildProductID { get; set; }
        public int? ParentProductID { get; set; }
        public int? ChildProductID { get; set; }
        public string ProductName { get; set; }
        public bool? IsActive { get; set; }
    }
}
