﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
    public class ReferralCommissionListModel: BaseListModel
    {
        public Collection<ReferralCommissionModel> ReferralCommissions { get; set; }

        /// <summary>
        /// Constructor For ReferralCommissionListModel
        /// </summary>
        public ReferralCommissionListModel()
		{
            ReferralCommissions = new Collection<ReferralCommissionModel>();
		}
    }
}
