﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class ProductsModel
    {
        //Znode Version 7.2.2 - Start
        public ProductsModel()
        {
            ShowQuantity = true;
        }

        public bool IsCallForPricing { get; set; }
        public string BundleItemsIds { get; set; }
        public string ShortDescription { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public string ImageAltTag { get; set; }
        public string ImageFile { get; set; }
        public string ImageLargePath { get; set; }
        public string ImageMediumPath { get; set; }
        public string InStockMessage { get; set; }
        public string BackOrderMessage { get; set; }
        public string OutOfStockMessage { get; set; }
        public int ProductId { get; set; }
        public decimal Price { get; set; }
        public decimal ProductPrice { get; set; }
        public decimal OriginalPrice { get; set; }
        public string ProductNumber { get; set; }
        public string Sku { get; set; }
        public int MinQuantity { get; set; }
        public int MaxQuantity { get; set; }
        public bool? TrackInventory { get; set; }
        public bool? AllowBackOrder { get; set; }
        public bool IsActive { get; set; }
        public string InventoryMessage { get; set; }
        public string SeoDescription { get; set; }
        public string SeoKeywords { get; set; }
        public string SeoTitle { get; set; }
        public string SeoPageName { get; set; }
        public bool ShowAddToCart { get; set; }
        public bool ShowWishlist { get; set; }
        public bool ShowQuantity { get; set; }
        public int SelectedQuantity { get; set; }
        public string CategoryName { get; set; }
        public int Rating { get; set; }
        public int WishListID { get; set; }
        public string FBTProductsIds { get; set; }
        public string YMALProductsIds { get; set; }
        //Znode Version 7.2.2 - Start

    }
}
