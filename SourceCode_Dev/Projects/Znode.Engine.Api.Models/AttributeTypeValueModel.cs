﻿
namespace Znode.Engine.Api.Models
{
    public class AttributeTypeValueModel : BaseModel
    {
        public int ProductTypeId { get; set; }
        public int AttributeTypeId { get; set; }
        public int AttributeId { get; set; }
        public string AttributeType { get; set; }
        public string AttributeValue { get; set; }
        public int DisplayOrder { get; set; }
        public bool IsGiftCard { get; set; }
    }
}
