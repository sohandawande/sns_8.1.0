﻿using System;
using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
    public class OrderModel : BaseModel
    {
        public AccountModel Account { get; set; }
        public int? AccountId { get; set; }
        public string AdditionalInstructions { get; set; }
        public string BillingCity { get; set; }
        public string BillingCompanyName { get; set; }
        public string BillingCountryCode { get; set; }
        public string BillingEmail { get; set; }
        public string BillingEmailId { get; set; }
        public string BillingFirstName { get; set; }
        public string BillingLastName { get; set; }
        public string BillingPhoneNumber { get; set; }
        public string BillingPostalCode { get; set; }
        public string BillingStateCode { get; set; }
        public string BillingStreetAddress1 { get; set; }
        public string BillingStreet { get; set; }
        public string BillingStreet1 { get; set; }
        public string BillingStreetAddress2 { get; set; }
        public string CardAuthorizationCode { get; set; }
        public string CardExpiration { get; set; }
        public string CardTransactionId { get; set; }
        public int? CardTypeId { get; set; }
        public string CouponCode { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreatedBy { get; set; }
        public string Custom1 { get; set; }
        public string Custom2 { get; set; }
        public string Custom3 { get; set; }
        public decimal? DiscountAmount { get; set; }
        public string ExternalId { get; set; }
        public decimal? Gst { get; set; }
        public decimal? Hst { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public DateTime? OrderDate { get; set; }
        public int OrderId { get; set; }
        public Collection<OrderLineItemModel> OrderLineItems { get; set; }
        public int? OrderStateId { get; set; }
        public PaymentOptionModel PaymentOption { get; set; }
        public int? PaymentOptionId { get; set; }
        public int? PaymentStatusId { get; set; }
        public PaymentTypeModel PaymentType { get; set; }
        public int? PaymentTypeId { get; set; }
        public int? PortalId { get; set; }
        public string PromotionDescription { get; set; }
        public decimal? Pst { get; set; }
        public string PurchaseOrderNumber { get; set; }
        public string ReceiptHtml { get; set; }
        public bool IsOffsitePayment { get; set; }
        public int? ReferralAccountId { get; set; }
        public DateTime? ReturnDate { get; set; }
        public decimal? SalesTax { get; set; }
        public DateTime? ShipDate { get; set; }
        public decimal? ShippingCost { get; set; }
        public ShippingOptionModel ShippingOption { get; set; }
        public int? ShippingOptionId { get; set; }
        public decimal? SubTotal { get; set; }
        public decimal? TaxCost { get; set; }
        public decimal? Total { get; set; }
        public string TrackingNumber { get; set; }
        public decimal? Vat { get; set; }
        public DateTime? WebServiceDownloadDate { get; set; }
        public OrderShipmentModel OrderShipment { get; set; }
        public int? ShippingID { get; set; }
        public string OrderStatus { get; set; }
        public string ShippingTypeName { get; set; }
        public string PaymentStatusName { get; set; }
        public int? PaymentSettingId { get; set; }
        public decimal GiftAmount { get; set; }
        public string PaymentMethod { get; set; }
        public string PromoCode { get; set; }
        public OrderLineItemListModel OrderLineItemList { get; set; }

        public int OrderShipmentId { get; set; }
        public string ShipName { get; set; }
        public string ShipToCity { get; set; }
        public string ShipToCompanyName { get; set; }
        public string ShipToCountryCode { get; set; }
        public string ShipToEmailId { get; set; }
        public string ShipToFirstName { get; set; }
        public string ShipToLastName { get; set; }
        public string ShipToPhoneNumber { get; set; }
        public string ShipToPostalCode { get; set; }
        public string ShipToStateCode { get; set; }
        public string ShipToStreet { get; set; }
        public string ShipToStreet1 { get; set; }

        public int AddressId { get; set; }
        public int Quantity { get; set; }
        public string ShippingName { get; set; }


        public string RefundCardNumber { get; set; }
        public string ExpirationDate { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public string SecurityCode { get; set; }
        public decimal RefundAmount { get; set; }
        public string ErrorMessage { get; set; }
        public string TrackingText { get; set; }
        public RMARequestListModel rmaRequestList { get; set; }
        public string PaymentTypeName { get; set; }


        public OrderModel()
        {
            OrderLineItems = new Collection<OrderLineItemModel>();
            rmaRequestList = new RMARequestListModel();
            Account = new AccountModel();
            PaymentOption = new PaymentOptionModel();
            PaymentType = new PaymentTypeModel();
            ShippingOption = new ShippingOptionModel();
            OrderShipment = new OrderShipmentModel();
            OrderLineItemList = new OrderLineItemListModel();
        }

        public OrderModel Sample()
        {
            return new OrderModel
            {
                AccountId = 11521,
                AdditionalInstructions = "",
                BillingCity = "Fake City",
                BillingCompanyName = "Your Company",
                BillingCountryCode = "US",
                BillingEmail = "znodeadmin1@test.com",
                BillingFirstName = "Site",
                BillingLastName = "Admin",
                BillingPhoneNumber = "1-888-Your-Store",
                BillingPostalCode = "12345",
                BillingStateCode = "SD",
                BillingStreetAddress1 = "123 Fake Street",
                BillingStreetAddress2 = "",
                CardAuthorizationCode = "",
                CardExpiration = "",
                CardTransactionId = "",
                CardTypeId = null,
                CouponCode = null,
                CreateDate = null,
                CreatedBy = null,
                Custom1 = null,
                Custom2 = null,
                Custom3 = "XXXX",
                DiscountAmount = 6.00m,
                ExternalId = null,
                Gst = 0.00m,
                Hst = 0.00m,
                ModifiedBy = null,
                ModifiedDate = null,
                OrderDate = Convert.ToDateTime("2013-09-18 12:18:56.94"),
                OrderId = 2,
                OrderStateId = 50,
                PaymentOptionId = 24,
                PaymentStatusId = 10,
                PaymentTypeId = 2,
                PortalId = 1,
                PromotionDescription = null,
                Pst = 0.00m,
                PurchaseOrderNumber = "123",
                ReceiptHtml = null,
                ReferralAccountId = null,
                ReturnDate = null,
                SalesTax = 0.00m,
                ShipDate = null,
                ShippingCost = 4.00m,
                ShippingOptionId = null,
                SubTotal = 15.90m,
                TaxCost = 0.00m,
                Total = 13.90m,
                TrackingNumber = null,
                Vat = 0.00m,
                WebServiceDownloadDate = null
            };
        }
    }
}
