﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
	public class InventoryModel : BaseModel
	{
		public int InventoryId { get; set; }
		public int? QuantityOnHand { get; set; }
		public int? ReorderLevel { get; set; }
		public string Sku { get; set; }
		public Collection<SkuModel> Skus { get; set; }

		public InventoryModel()
		{
			Skus = new Collection<SkuModel>();
		}

		public InventoryModel Sample()
		{
			return new InventoryModel
			{
				InventoryId = 1,
				QuantityOnHand = 1000,
				ReorderLevel = 10,
				Sku = "apg234"
			};
		}
	}
}
