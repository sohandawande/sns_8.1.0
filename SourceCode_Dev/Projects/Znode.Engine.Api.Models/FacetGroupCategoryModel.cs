﻿
namespace Znode.Engine.Api.Models
{
    public class FacetGroupCategoryModel : BaseModel
    {
        public int FacetGroupID { get; set; }
        public int CategoryID { get; set; }
        public int? CategoryDisplayOrder { get; set; }
    }
}
