﻿
namespace Znode.Engine.Api.Models
{
    public class SocialLoginModel : BaseModel
    {
        public string Provider { get; set; }
        public string ProviderUserId { get; set; }
        public bool CreatePersistentCookie { get; set; }
        public string UserName { get; set; }
    }
}
