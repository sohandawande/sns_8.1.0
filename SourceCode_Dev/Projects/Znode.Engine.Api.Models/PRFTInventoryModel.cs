﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class PRFTInventoryModel : BaseModel
    {
        public int ProductId { get; set; }
        public string Name { get; set; }
        public string ProductNum { get; set; }
        public int Milwaukee { get; set; }
        public int Bloomington { get; set; }
        public int Nebraska { get; set; }
        public int DesMoines { get; set; }
        public int? PortalID { get; set; }

        public PRFTInventoryModel()
        {
            PortalID = 1;
            Milwaukee = 0;
            Bloomington = 0;
            Nebraska = 0;
            DesMoines = 0;
        }

        public PRFTInventoryModel Sample()
        {
            return new PRFTInventoryModel
            {
                ProductId = 1,
                Name = "Dymonic® 100 Cartridges",
                ProductNum = "DY1T",
                Milwaukee = 999,
                Bloomington = 999,
                Nebraska = 999,
                DesMoines=999,
                PortalID = 1
            };
        }
    }
}
