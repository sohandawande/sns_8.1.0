﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
    public class ProductModel : BaseModel
    {
        public int? AccountId { get; set; }
        public string AdditionalInfo { get; set; }
        public string AdditionalInfoLink { get; set; }
        public string AdditionalInfoLinkLabel { get; set; }
        public Collection<AddOnModel> AddOns { get; set; }
        public string AffiliateUrl { get; set; }
        public bool? AllowBackOrder { get; set; }
        public bool? AllowDropShip { get; set; }
        public bool? AllowFreeShipping { get; set; }
        public bool AllowRecurringBilling { get; set; }
        public bool AllowRecurringBillingInstallment { get; set; }
        public Collection<AttributeModel> Attributes { get; set; }
        public string BackOrderMessage { get; set; }
        public DateTime? BeginActiveDate { get; set; }
        public string BundledProductIds { get; set; }
        public string BundledProductExternalIds { get; set; }
        public bool CallForPricing { get; set; }
        public Collection<CategoryModel> Categories { get; set; }
        public Collection<ProductCategoryModel> ProductCategories { get; set; }
        public DateTime? CreateDate { get; set; }
        public Collection<CrossSellModel> CrossSells { get; set; }
        public string Custom1 { get; set; }
        public string Custom2 { get; set; }
        public string Custom3 { get; set; }
        public string Description { get; set; }
        public int? DisplayOrder { get; set; }
        public string DownloadLink { get; set; }
        public string DropShipEmailAddress { get; set; }
        public DateTime? EndActiveDate { get; set; }
        public int? ExpirationFrequency { get; set; }
        public int? ExpirationPeriod { get; set; }
        public string ExternalId { get; set; }
        public string FeaturesDescription { get; set; }
        public Collection<CrossSellModel> FrequentlyBoughtTogether { get; set; }
        public decimal? Height { get; set; }
        public Collection<HighlightModel> Highlights { get; set; }
        public string ImageAltTag { get; set; }
        public string ImageFile { get; set; }
        public string ImageLargePath { get; set; }
        public string ImageMediumPath { get; set; }
        public string ImageSmallPath { get; set; }
        public string ImageSmallThumbnailPath { get; set; }
        public string ImageThumbnailPath { get; set; }
        public Collection<ImageModel> Images { get; set; }
        public string InStockMessage { get; set; }
        public byte InventoryDisplay { get; set; }
        public bool IsActive { get; set; }
        public bool IsCategorySpecial { get; set; }
        public bool IsFeatured { get; set; }
        public bool IsFranchisable { get; set; }
        public bool IsHomepageSpecial { get; set; }
        public bool? IsNewProduct { get; set; }
        public bool? IsShippable { get; set; }
        public string Keywords { get; set; }
        public decimal? Length { get; set; }
        public ManufacturerModel Manufacturer { get; set; }
        public int? ManufacturerId { get; set; }
        public int? MaxSelectableQuantity { get; set; }
        public int? MinSelectableQuantity { get; set; }
        public string Name { get; set; }
        public string OutOfStockMessage { get; set; }
        public int? PortalId { get; set; }
        public int ProductId { get; set; }
        public string ProductNumber { get; set; }
        public ProductTypeModel ProductType { get; set; }
        public int ProductTypeId { get; set; }
        public string ProductUrl { get; set; }
        public string PromotionMessage { get; set; }
        public decimal? PromotionPrice { get; set; }
        public Collection<PromotionModel> Promotions { get; set; }
        public string RecurringBillingFrequency { get; set; }
        public decimal? RecurringBillingInitialAmount { get; set; }
        public string RecurringBillingPeriod { get; set; }
        public int? RecurringBillingTotalCycles { get; set; }
        public decimal? RetailPrice { get; set; }
        public Collection<ReviewModel> Reviews { get; set; }
        public int? ReviewStateId { get; set; }
        public decimal? SalePrice { get; set; }
        public SkuModel SelectedSku { get; set; }
        public string SeoDescription { get; set; }
        public string SeoKeywords { get; set; }
        public string SeoTitle { get; set; }
        public string SeoUrl { get; set; }
        public bool? ShipEachItemSeparately { get; set; }
        public decimal? ShippingRate { get; set; }
        public int? ShippingRuleTypeId { get; set; }
        public bool ShipSeparately { get; set; }
        public string ShortDescription { get; set; }
        public Collection<SkuModel> Skus { get; set; }
        public string Specifications { get; set; }
        public int? SupplierId { get; set; }
        public int? TaxClassId { get; set; }
        public decimal? TieredPrice { get; set; }
        public Collection<TierModel> Tiers { get; set; }
        public bool? TrackInventory { get; set; }
        public DateTime? UpdateDate { get; set; }
        public DateTime? WebServiceDownloadDate { get; set; }
        public decimal? Weight { get; set; }
        public decimal? Width { get; set; }
        public decimal? WholesalePrice { get; set; }
        public Collection<CrossSellModel> YouMayAlsoLike { get; set; }
        public bool? IsChildProduct { get; set; }
        public bool? IsParentProductId { get; set; }
        public decimal? NegotiatedPrice { get; set; }
        public string ProductNum { get; set; }
        public string Vendor { get; set; }
        public bool IsGiftCard { get; set; }
        public int AttributeCount { get; set; }
        public Dictionary<string, Dictionary<string, string>> ProductAttributes { get; set; }
        public Dictionary<string, Dictionary<string, string>> ProductAddOns { get; set; }
        public Dictionary<string, Dictionary<string, Dictionary<string, string>>> BundleProductDict { get; set; }
        public string UserType { get; set; }
        public string UserName { get; set; }
        public string ReviewHistoryDescription { get; set; }
        public string ProductIds { get; set; }

        public bool IsFromSitecore { get; set; }

        #region Znode Version 8.0
        public string SupplierName { get; set; }
        public string SupplierTypeName { get; set; }
        public string ManufacturerName { get; set; }
        public string ProductTypeName { get; set; }
        public string SkuName { get; set; }
        public string TaxClassName { get; set; }
        public int SkuId { get; set; }
        public string ShippingRule { get; set; }
        public string AttributeIds { get; set; }
        public int? QuantityOnHand { get; set; }
        public int? ReorderLevel { get; set; }
        public bool RedirectUrlInd { get; set; }
        public string CategoryIds { get; set; }
        public string ReviewStateString { get; set; }
        #endregion
        public ProductModel()
        {
            AddOns = new Collection<AddOnModel>();
            Attributes = new Collection<AttributeModel>();
            Categories = new Collection<CategoryModel>();
            CrossSells = new Collection<CrossSellModel>();
            FrequentlyBoughtTogether = new Collection<CrossSellModel>();
            Highlights = new Collection<HighlightModel>();
            Images = new Collection<ImageModel>();
            Promotions = new Collection<PromotionModel>();
            Reviews = new Collection<ReviewModel>();
            Skus = new Collection<SkuModel>();
            Tiers = new Collection<TierModel>();
            YouMayAlsoLike = new Collection<CrossSellModel>();
            ProductCategories = new Collection<ProductCategoryModel>();
        }

        public ProductModel Sample()
        {
            return new ProductModel
            {
                AccountId = 33,
                AdditionalInfo = "",
                AdditionalInfoLink = "",
                AdditionalInfoLinkLabel = "",
                AffiliateUrl = "",
                AllowBackOrder = true,
                AllowFreeShipping = true,
                AllowDropShip = true,
                AllowRecurringBilling = false,
                AllowRecurringBillingInstallment = false,
                BackOrderMessage = "",
                BeginActiveDate = null,
                CallForPricing = false,
                CreateDate = Convert.ToDateTime("2013-04-29 10:49:32.657"),
                Custom1 = "",
                Custom2 = "",
                Custom3 = "",
                Description = "Product description",
                DisplayOrder = 10,
                DownloadLink = "",
                DropShipEmailAddress = "",
                EndActiveDate = null,
                ExpirationFrequency = null,
                ExpirationPeriod = null,
                ExternalId = "",
                FeaturesDescription = "Features description",
                Height = 1.0m,
                ImageAltTag = "Apple",
                ImageFile = "apple.jpg",
                ImageLargePath = "~/data/default/images/catalog/450/apple.jpg",
                ImageMediumPath = "~/data/default/images/catalog/250/apple.jpg",
                ImageSmallPath = "~/data/default/images/catalog/100/apple.jpg",
                ImageSmallThumbnailPath = "~/data/default/images/catalog/37/apple.jpg",
                ImageThumbnailPath = "~/data/default/images/catalog/50/apple.jpg",
                InStockMessage = "This item is in stock",
                InventoryDisplay = 0,
                IsActive = true,
                IsCategorySpecial = false,
                IsFeatured = false,
                IsFranchisable = true,
                IsHomepageSpecial = false,
                IsNewProduct = false,
                IsShippable = true,
                Keywords = "",
                Length = 1.0m,
                ManufacturerId = 3,
                MaxSelectableQuantity = 10,
                MinSelectableQuantity = 0,
                Name = "Apple",
                OutOfStockMessage = "This item is out of stock",
                PortalId = 1,
                ProductId = 302,
                ProductNumber = "ap231",
                ProductTypeId = 8,
                ProductUrl = "~/product/Apple/302",
                PromotionMessage = "",
                PromotionPrice = 0.25m,
                RecurringBillingFrequency = "",
                RecurringBillingInitialAmount = 0.0m,
                RecurringBillingPeriod = "",
                RecurringBillingTotalCycles = 0,
                RetailPrice = 4.23m,
                ReviewStateId = 20,
                SalePrice = 3.50m,
                SeoDescription = "Delicious Apple",
                SeoKeywords = "apple",
                SeoTitle = "Delicious Apple",
                SeoUrl = "Apple",
                ShipEachItemSeparately = false,
                ShippingRate = 0.56m,
                ShippingRuleTypeId = 0,
                ShipSeparately = false,
                ShortDescription = "Short description",
                Specifications = "Product specifications",
                SupplierId = 12,
                TaxClassId = 1,
                TieredPrice = 1.50m,
                TrackInventory = true,
                UpdateDate = Convert.ToDateTime("2013-08-12 07:12:34.407"),
                WebServiceDownloadDate = null,
                Weight = 1.0m,
                WholesalePrice = 2.10m
            };
        }
    }
}