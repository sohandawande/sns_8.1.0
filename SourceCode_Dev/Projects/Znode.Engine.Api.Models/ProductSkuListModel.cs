﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
    public class ProductSkuListModel : BaseListModel
    {
        public Collection<ProductSkuModel> ProductSkus { get; set; }

        public ProductSkuListModel()
		{
            ProductSkus = new Collection<ProductSkuModel>();
		}
       
    }
}
