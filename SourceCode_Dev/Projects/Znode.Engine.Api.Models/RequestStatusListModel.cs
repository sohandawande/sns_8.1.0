﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
    public class RequestStatusListModel : BaseListModel
    {
        public Collection<RequestStatusModel> RequestStatusList { get; set; }

        public RequestStatusListModel()
        {
            RequestStatusList = new Collection<RequestStatusModel>();
        }
    }
}
