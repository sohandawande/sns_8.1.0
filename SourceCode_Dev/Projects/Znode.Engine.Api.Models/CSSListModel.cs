﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class CSSListModel : BaseListModel
    {
         public Collection<CSSModel> CSSs { get; set; }

         public CSSListModel()
        {
            CSSs = new Collection<CSSModel>();
        }
    }
}
