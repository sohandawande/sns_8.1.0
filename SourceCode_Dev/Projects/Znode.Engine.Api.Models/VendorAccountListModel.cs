﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
    /// <summary>
    /// List Model for Vendor Account.
    /// </summary>
    public class VendorAccountListModel : BaseListModel
    {
        public Collection<VendorAccountModel> VendorAccount { get; set; }
        
        /// <summary>
        /// Constructor for VendorAccountListModel
        /// </summary>
        public VendorAccountListModel()
        {
            VendorAccount = new Collection<VendorAccountModel>();
        }
    }
}
