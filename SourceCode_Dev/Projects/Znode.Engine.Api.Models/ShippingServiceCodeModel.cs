﻿namespace Znode.Engine.Api.Models
{
	public class ShippingServiceCodeModel : BaseModel
	{
		public string Code { get; set; }
		public string Description { get; set; }
		public int? DisplayOrder { get; set; }
		public bool IsActive { get; set; }
		public int ShippingServiceCodeId { get; set; }
		public ShippingTypeModel ShippingType { get; set; }
		public int ShippingTypeId { get; set; }

		public ShippingServiceCodeModel Sample()
		{
			return new ShippingServiceCodeModel
			{
				Code = "FEDEX_GROUND",
				Description = "FedEx Ground",
				DisplayOrder = 9,
				IsActive = true,
				ShippingServiceCodeId = 19,
				ShippingTypeId = 2
			};
		}
	}
}
