﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
    public class ProductAlternateImageListModel: BaseListModel
	{
		public Collection<ProductAlternateImageModel> ProductImages { get; set; }

        public ProductAlternateImageListModel()
		{
			ProductImages = new Collection<ProductAlternateImageModel>();
		}
    }
}
