﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
	public class PortalListModel : BaseListModel
	{
		public Collection<PortalModel> Portals { get; set; }

		public PortalListModel()
		{
			Portals = new Collection<PortalModel>();
		}
	}
}