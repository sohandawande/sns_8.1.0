﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class LuceneIndexModel : BaseModel
    {
        public long LuceneIndexMonitorId { get; set; }
        public int SourceId { get; set; }
        public string SourceType { get; set; }
        public string SourceTransationType { get; set; }
        public DateTime TransationDateTime { get; set; }
        public bool IsDuplicate { get; set; }
        public string AffectedType { get; set; }
        public string IndexerStatusChangedBy { get; set; }

        public int TriggerFlag { get; set; }
        public int ServiceFlag { get; set; }

        public int? PageIndex { get; set; }
        public int? PageSize { get; set; }
        public int? TotalResults { get; set; }

        public int? TotalPages
        {
            get
            {
                if (PageSize > 0)
                {
                    if (TotalResults % PageSize == 0)
                    {
                        return TotalResults / PageSize;
                    }
                    else
                    {
                        return (TotalResults / PageSize) + 1;
                    }
                }

                return null;
            }
        }
    }
}
