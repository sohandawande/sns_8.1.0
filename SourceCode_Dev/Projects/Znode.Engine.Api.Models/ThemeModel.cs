﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class ThemeModel
    {
        public int ThemeID { get; set; }
        public string Name { get; set; }

        public ThemeModel Sample()
        {
            return new ThemeModel
            {
                ThemeID = 1,
                Name = "Defaul"
            };
        }
    }
}
