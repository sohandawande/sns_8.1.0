﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class ReportsDataModel : BaseListModel
    {
       public List<dynamic> RecordSet { get; set; }       
    }
}
