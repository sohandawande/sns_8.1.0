﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
	public class ProductCategoryListModel : BaseListModel
	{
		public Collection<ProductCategoryModel> ProductCategories { get; set; }

		public ProductCategoryListModel()
		{
			ProductCategories = new Collection<ProductCategoryModel>();
		}
	}
}
