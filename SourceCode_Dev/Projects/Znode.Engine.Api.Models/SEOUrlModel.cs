﻿
namespace Znode.Engine.Api.Models
{
    public class SEOUrlModel : BaseModel
    {
        public string SeoUrl { get; set; }
        public int? ProductId { get; set; }
        public string CategoryName { get; set; }
        public string ContentPageName { get; set; }
    }
}
