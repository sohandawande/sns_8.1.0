﻿using System;
using System.Collections.Generic;

namespace Znode.Engine.Api.Models
{
    /// <summary>
    /// This class is the model for Permissions and Roles
    /// </summary>
    public class PermissionsModel : BaseModel
    {
        public PermissionsModel()
        { }

        public int AccountId { get; set; }
        public string UserName { get; set; }
        public Guid UserId { get; set; }

        public RolesModel roleModel { get; set; }
        public StoresModel storeModel { get; set; }

        public string SelectedStoreIds { get; set; }
        public string SelectedRoleIds { get; set; }
    }
}