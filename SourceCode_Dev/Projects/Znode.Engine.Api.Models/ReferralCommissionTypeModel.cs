﻿
namespace Znode.Engine.Api.Models
{
    public class ReferralCommissionTypeModel : BaseModel
    {
        public string ReferralCommissionTypeName { get; set; }
        public int ReferralCommissionTypeId { get; set; }
    }
}
