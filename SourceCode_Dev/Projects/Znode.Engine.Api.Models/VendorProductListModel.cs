﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
    public class VendorProductListModel:BaseListModel
    {
        public Collection<ProductModel> Products { get; set; }
        public RejectProductModel RejectProduct { get; set; }
        public VendorProductListModel()
		{
			Products = new Collection<ProductModel>();
            RejectProduct = new RejectProductModel();
		}
    }
}
