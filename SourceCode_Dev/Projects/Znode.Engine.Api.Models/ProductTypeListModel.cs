﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class ProductTypeListModel : BaseListModel
	{
		public Collection<ProductTypeModel> ProductTypes { get; set; }

        public ProductTypeListModel()
		{
			ProductTypes = new Collection<ProductTypeModel>();
		}
	}
}