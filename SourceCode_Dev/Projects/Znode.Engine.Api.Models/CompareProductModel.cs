﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class CompareProductModel: BaseModel
    {
        public string SenderEmailAddress { get; set; }
        public string RecieverEmailAddress { get; set; }
        public Collection<ProductModel> ProductList { get; set; }
        public string ProductIds { get; set; }
        public string BaseUrl { get; set; }
        public int PortalId { get; set; }
        public int ProductId { get; set; }
        public bool IsProductDetails { get; set; }
    }
}
