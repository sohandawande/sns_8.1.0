﻿using System;

namespace Znode.Engine.Api.Models
{
    public class ProfileCommonModel : BaseModel
    {
        public int WebPageProfileId { get; set; }
        public Guid UserId { get; set; }
        public int PortalId { get; set; }
    }
}
