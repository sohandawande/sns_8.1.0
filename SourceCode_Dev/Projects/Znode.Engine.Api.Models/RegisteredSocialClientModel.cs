﻿
namespace Znode.Engine.Api.Models
{
    public class RegisteredSocialClientModel : BaseModel
    {
        public string ProviderName { get; set; }
        public string ProviderClientId { get; set; }
        public string ProviderClientSecret { get; set; }
    }
}
