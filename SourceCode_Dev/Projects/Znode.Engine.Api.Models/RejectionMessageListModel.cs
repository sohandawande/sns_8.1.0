﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
    /// <summary>
    /// List Model for Rejection Message
    /// </summary>
    public class RejectionMessageListModel : BaseListModel
    {
        public Collection<RejectionMessageModel> RejectionMessages { get; set; }

        #region Consructor
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public RejectionMessageListModel()
        {
            RejectionMessages = new Collection<RejectionMessageModel>();
        } 
        #endregion
    }
}
