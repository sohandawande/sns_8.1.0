﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class YouMayLikeAlsoListModel :  BaseListModel
    {
        public Collection<YouMayLikeAlsoModel> AssociatedProductItems { get; set; }

        public YouMayLikeAlsoListModel()
        {
            AssociatedProductItems = new Collection<YouMayLikeAlsoModel>();
        }
    }
}
