﻿
namespace Znode.Engine.Api.Models
{
    /// <summary>
    /// Model for attributes by Product Type.
    /// </summary>
    public class ProductTypeAssociatedAttributeTypesModel : BaseModel
    {
        public int ProductAttributeTypeID { get; set; }
        public int ProductTypeId { get; set; }
        public int AttributeTypeId { get; set; }
        public int LocaleId { get; set; }
        public int DisplayOrder { get; set; }

        public int? PortalId { get; set; }

        public string Name { get; set; }
        
        public bool IsPrivate { get; set; }
        
        
    }
}
