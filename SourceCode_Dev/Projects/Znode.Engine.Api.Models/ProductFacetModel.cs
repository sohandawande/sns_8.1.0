﻿using System;
using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
    public class ProductFacetModel : BaseModel
    {
        public ProductFacetModel()
		{
            FacetGroups = new Collection<FacetGroupModel>();
            AssociatedFacets = new Collection<FacetModel>();
            AssociatedFacetsSkus = new Collection<FacetProductSKUModel>();
		}
        public int ProductId { get; set; }
        public string[] FacetIds { get; set; }
        public int FacetGroupId { get; set; }
        public Collection<FacetGroupModel> FacetGroups { get; set; }
        public Collection<FacetModel> AssociatedFacets { get; set; }
        public Collection<FacetProductSKUModel> AssociatedFacetsSkus { get; set; }
    }

    public class FacetProductSKUModel : BaseModel
    {
        public int FacetProductSKUID { get; set; }
        public int? FacetID { get; set; }
        public int? ProductID { get; set; }
        public int? SKUID { get; set; }
    }
}
