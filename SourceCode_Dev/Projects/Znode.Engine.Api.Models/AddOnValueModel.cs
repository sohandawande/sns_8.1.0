﻿using System;
using System.Xml.Serialization;

namespace Znode.Engine.Api.Models
{
	public class AddOnValueModel :BaseModel	
	{
		//private decimal _price;
		
		public AddOnValueModel()
		{
		}

		/// <summary>
		/// Gets or sets the AddOnValue id
		/// </summary>
		public int AddOnValueId { get; set; }

		/// <summary>
		/// Gets or sets the AddOnid for this AddOn Value
		/// </summary>
		public int AddOnId { get; set; }

		/// <summary>
		/// Gets or sets the Add-on value Name
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// Gets or sets the Add-on value description
		/// </summary>
		public string Description { get; set; }

		/// <summary>
		/// Gets or sets the SKU Name
		/// </summary>
		public string SKU { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether the add-on value is default.
		/// </summary>
		public bool IsDefault { get; set; }

		/// <summary>
		/// Gets or sets the qantity on stock for this addonValue
		/// </summary>
		public int QuantityOnHand { get; set; }

		/// <summary>
		/// Gets or sets the order of the display
		/// </summary>
		public int DisplayOrder { get; set; }

		/// <summary>
		/// Gets or sets the retail price value for this addon value
		/// </summary>
		public decimal RetailPrice { get; set; }

		/// <summary>
		/// Gets or sets the sale price value for this addon value
		/// </summary>
		public decimal? SalePrice { get; set; }

		/// <summary>
		/// Gets or sets the wholesale price value for this addon value
		/// </summary>
		public decimal? WholesalePrice { get; set; }

		/// <summary>
		/// Gets or sets the weight of this addon value 
		/// </summary>
		public decimal Weight { get; set; }

		/// <summary>
		/// Gets or sets the add-on value package height  
		/// </summary>
		public decimal? Height { get; set; }

		/// <summary>
		/// Gets or sets the add-on value package width 
		/// </summary>
		public decimal? Width { get; set; }

		/// <summary>
		/// Gets or sets the add-on value package length 
		/// </summary>
		public decimal? Length { get; set; }

		/// <summary>
		///  Gets or sets the add-on value shippingRuleTypeId
		/// </summary>
		public int? ShippingRuleTypeId { get; set; }

		///// <summary>
		///// Gets or sets the calculated shipping cost for this addonvalue
		///// </summary>
		//
		//public decimal ShippingCost { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether the add-on is free shipping
		/// </summary>
		public bool FreeShippingInd { get; set; }

		/// <summary>
		/// Gets or sets the supplierId for this AddOn Value
		/// </summary>
		public int? SupplierId { get; set; }

		/// <summary>
		/// Gets or sets the TaxClassId for this AddOn Value
		/// </summary>
		public int TaxClassId { get; set; }

		/// <summary>
		/// Gets or sets the Add-on value description
		/// </summary>
		[XmlIgnore()]
		public string CustomText { get; set; }
		
		/// <summary>
		/// Gets or sets a value indicating whether the recurring billing is enabled
		/// </summary>
		public bool RecurringBillingInd { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether installmentInd  is enabled.
		/// </summary>

		public bool RecurringBillingInstallmentInd { get; set; }

		/// <summary>
		/// Gets or sets the billing period (days or months), in association with the frequency.
		/// </summary>

		public string RecurringBillingPeriod { get; set; }

		/// <summary>
		/// Gets or sets the billing frequency, in association with the Period.
		/// </summary>

		public string RecurringBillingFrequency { get; set; }

		/// <summary>
		/// Gets or sets the number of billing occurrences or payments for the subscription
		/// </summary>

		public int RecurringBillingTotalCycles { get; set; }

		/// <summary>
		/// Gets or sets the initial amount to be charged during subscription creation,
		/// </summary>

		public decimal? RecurringBillingInitialAmount { get; set; }

        /// <summary>
        /// Gets or sets the Reorder Level for this addonValue
        /// </summary>
        public int? ReOrderLevel { get; set; }

        /// <summary>
        /// Date of update
        /// </summary>
        public DateTime? UpdateDate { get; set; }

		public AddOnValueModel Sample()
		{
			return new AddOnValueModel
			{
				
				AddOnValueId =  1,
				AddOnId = 1,
				CustomText = string.Empty,
				Description = "",
				DisplayOrder = 1,
				FreeShippingInd = false,
				Height =10,
				Length = 20,
				IsDefault = false,
				Name = "Vase",
				QuantityOnHand = 1000,
				RecurringBillingInd = false,
				RecurringBillingInstallmentInd = false,
				RecurringBillingPeriod = null,
				RecurringBillingFrequency = string.Empty,
				RecurringBillingTotalCycles = 1,
				RecurringBillingInitialAmount = 0,
				RetailPrice = (decimal) 10.00,
				SalePrice = (decimal?) 7.25,
				ShippingRuleTypeId = 0,
				SKU = "gb441",
				SupplierId =0,
				TaxClassId = 0,
				Weight = 12,
				WholesalePrice = 0,
				Width = 5,
                ReOrderLevel=10,
                UpdateDate=null
			};
		}
	}
}
