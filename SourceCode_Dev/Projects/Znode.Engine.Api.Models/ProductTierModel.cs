﻿
namespace Znode.Engine.Api.Models
{
    public class ProductTierPricingModel : BaseModel
    {
        public decimal Price { get; set; }
        public int? ProfileID { get; set; }
        public int TierEnd { get; set; }
        public int TierStart { get; set; }
        public int ProductId { get; set; }
        public int ProductTierID { get; set; }
        public string UserType { get; set; }
        public string UserName { get; set; }
    }
}
