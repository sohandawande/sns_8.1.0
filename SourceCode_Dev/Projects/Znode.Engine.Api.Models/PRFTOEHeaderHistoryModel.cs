﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class PRFTOEHeaderHistoryModel : BaseModel
    {
        public string InvoiceNumber { get; set; }
        public string InvoiceDate { get; set; }
        public string BillToCustomerName { get; set; }
        public string BillToAddressLine1 { get; set; }
        public string BillToAddressLine2 { get; set; }
        public string BillToAddressLine3 { get; set; }
        public string BillToAddressLine4 { get; set; }
        public string BillToCity { get; set; }
        public string BillToState { get; set; }
        public string BillToZip { get; set; }
        public string BillToCountry { get; set; }
        public string ShipToName { get; set; }
        public string ShipToAddressLine1 { get; set; }
        public string ShipToAddressLine2 { get; set; }
        public string ShipToAddressLine3 { get; set; }
        public string ShipToAddressLine4 { get; set; }    
        public string ShipToCity { get; set; }
        public string ShipToState { get; set; }
        public string ShipToZip { get; set; }
        public string ShipToCountry { get; set; }
        public string OrderNumber { get; set; }
        public string OrderDate { get; set; }
        public string CustomerNumber { get; set; }
        public string ManufacturingLocation { get; set; }
        public string SalesPersonNumber { get; set; }
        public string PurchaseOrderNumber { get; set; }
        public string JobNumber { get; set; }
        public string ShipViaCode { get; set; }
        public string FreightPaymentCode { get; set; }
        public string TotalSaleAmt { get; set; }
        public string MiscellaneousAmount { get; set; }
        public string FreightAmount { get; set; }
        public string SalesTaxAmount1{ get; set; }
        public string SalesTaxAmount2 { get; set; }
        public string SalesTaxAmount3 { get; set; }        
        public string PaymentAmount { get; set; }
        public string PaymentDiscountAmount { get; set; }  
        public string PaymentTerms { get; set; }
        public string Comments { get; set; }

        public Collection<PRFTOELineHistoryModel> OELineHistoryLists { get; set; }

        public PRFTOEHeaderHistoryModel()
        {
            OELineHistoryLists = new Collection<PRFTOELineHistoryModel>();
        }
    }
}
