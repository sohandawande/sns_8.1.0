﻿using System;
using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
    /// <summary>
    /// Model Vendor Account
    /// </summary>
    public class VendorAccountModel : BaseModel
    {
        #region Default Constructor
        /// <summary>
        /// Constructor for VendorAccountModel
        /// </summary>
        public VendorAccountModel()
        {

        }
        #endregion

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string RoleName { get; set; }
        public string CompanyName { get; set; }
        public string ExternalAccountNo { get; set; }
        public string Description { get; set; }
        public string Website { get; set; }
        public string CreateUser { get; set; }
        public string UpdateUser { get; set; }
        public string Custom1 { get; set; }
        public string Custom2 { get; set; }
        public string Custom3 { get; set; }
        public string Source { get; set; }
        public string PhoneNumber { get; set; }
        public string Street { get; set; }
        public string Street1 { get; set; }
        public string City { get; set; }
        public string StateCode { get; set; }
        public string PostalCode { get; set; }
        public string UserName { get; set; }
        public string AddressCompanyName { get; set; }

        public int AccountId { get; set; }

        public bool CheckRole { get; set; }

        public int? ProfileId { get; set; }

        public DateTime? CreateDte { get; set; }
        public DateTime? UpdateDte { get; set; }

        public bool? EnableCustomerPricing { get; set; }
        public bool IsConfirmed { get; set; }
        public bool EmailOptIn { get; set; }

        public Guid? UserId { get; set; }

        public AccountModel AccountModel { get; set; }
        public Collection<AddressModel> Addresses { get; set; }
    }
}
