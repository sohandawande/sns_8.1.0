﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
    public class RolePermissionListModel : BaseListModel
    {
        public Collection<RolePermissionModel> UserPermissionList { get; set; }

        public RolePermissionListModel()
		{
            UserPermissionList = new Collection<RolePermissionModel>();
		}
    }
}
