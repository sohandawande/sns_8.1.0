﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models.Comparers
{
	public class ShoppingCartItemModelComparer : IEqualityComparer<ShoppingCartItemModel>
	{
		public bool Equals(ShoppingCartItemModel x, ShoppingCartItemModel y)
		{
			if (x != null && y != null)
			{
				if (x.SkuId == y.SkuId && (x.AddOnValueIds == null && y.AddOnValueIds == null))
					return true;

				if (x.AddOnValueIds != null && y.AddOnValueIds != null)
					return (x.SkuId == y.SkuId && x.AddOnValueIds.SequenceEqual(y.AddOnValueIds));

				return false;
			}

			return false;
		}

		public int GetHashCode(ShoppingCartItemModel obj)
		{
			var cartItemIdStruct = new ShoppingCartItemModelIdStruct {SkuId = obj.SkuId, 
																AddOnValueIds = obj.AddOnValueIds};

			return cartItemIdStruct.GetHashCode();
			
		}
		

		public struct ShoppingCartItemModelIdStruct
		{
			public int SkuId;
			public int[] AddOnValueIds;
		}

	}
}
