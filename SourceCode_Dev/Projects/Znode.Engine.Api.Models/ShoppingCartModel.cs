﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
    public class ShoppingCartModel : BaseModel
    {
        public AccountModel Account { get; set; }
        public string AdditionalInstructions { get; set; }
        public string BillingEmail { get; set; }
        public string CancelUrl { get; set; }
        public int CookieId { get; set; }
        public decimal Discount { get; set; }
        public string FeedbackUrl { get; set; }
        public bool GiftCardApplied { get; set; }
        public decimal GiftCardAmount { get; set; }
        public string GiftCardMessage { get; set; }
        public string GiftCardNumber { get; set; }
        public bool GiftCardValid { get; set; }
        public decimal GiftCardBalance { get; set; }
        public decimal? Gst { get; set; }
        public decimal? Hst { get; set; }
        public bool MultipleShipToEnabled { set; get; }
        public decimal OrderLevelDiscount { get; set; }
        public decimal OrderLevelShipping { get; set; }
        public decimal OrderLevelTaxes { get; set; }
        public PaymentModel Payment { get; set; }
        public int? ProfileId { get; set; }
        public decimal? Pst { get; set; }
        public string PurchaseOrderNumber { get; set; }
        public string RedirectUrl { get; set; }
        public string ReturnUrl { get; set; }
        public decimal SalesTax { get; set; }
        public ShippingModel Shipping { get; set; }
        public decimal ShippingCost { get; set; }
        public AddressModel ShippingAddress { get; set; }
        public AddressModel BillingAddress { get; set; }
        public Collection<ShoppingCartItemModel> ShoppingCartItems { get; set; }
        public decimal? SubTotal { get; set; }
        public decimal TaxCost { get; set; }
        public decimal TaxRate { get; set; }
        public decimal? Total { get; set; }
        public decimal? Vat { get; set; }
        public int? PortalId { get; set; }
        // for paypal express checkout for Api
        public string Token { get; set; }
        public string Payerid { get; set; }
        //Znode Version 7.2.2 Multiple shipping collection
        public List<OrderShipmentDataModel> OrderShipment { get; set; }
        public List<CouponModel> Coupons { get; set; }
        public string CurrentURL { get; set; }

        public string CustomerExternalAccountId { get; set; }//PRFT Custom Code

        public ShoppingCartModel()
        {
            Account = new AccountModel();
            Payment = new PaymentModel();
            Shipping = new ShippingModel();
            ShippingAddress = new AddressModel();
            ShoppingCartItems = new Collection<ShoppingCartItemModel>();
            Coupons = new List<CouponModel>();
        }

        public ShoppingCartModel Sample()
        {
            return new ShoppingCartModel
            {

            };
        }
    }
}
