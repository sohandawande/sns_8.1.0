﻿using System;

namespace Znode.Engine.Api.Models
{
    public class ProductReviewHistoryModel : BaseModel
    {
        public int ProductReviewHistoryID { get; set; }
        public int ProductID { get; set; }
        public int VendorID { get; set; }
        public string Status { get; set; }
        public string Reason { get; set; }
        public string Description { get; set; }
        public string Username { get; set; }
        public DateTime LogDate { get; set; }
        public DateTime? NotificationDate { get; set; }
        public string ProductName { get; set; }
        public string VendorName { get; set; }
    }
}
