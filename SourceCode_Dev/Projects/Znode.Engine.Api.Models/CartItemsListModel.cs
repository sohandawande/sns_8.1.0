﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
    public class CartItemsListModel : BaseListModel
    {
        //Znode Version 7.2.2 - Start

        public Collection<CartItemsModel> ReorderItems { get; set; }

        public CartItemsListModel()
		{
            ReorderItems = new Collection<CartItemsModel>();
		}
        //Znode Version 7.2.2 - End
    }
}
