﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
	public class SupplierListModel : BaseListModel
	{
		public Collection<SupplierModel> Suppliers { get; set; }

		public SupplierListModel()
		{
			Suppliers = new Collection<SupplierModel>();
		}
	}
}