﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
    public class PRFTCreditApplicationListModel:BaseListModel
    {
        public Collection<PRFTCreditApplicationModel> CreditApplications { get; set; }

        public PRFTCreditApplicationListModel()
        {
            CreditApplications = new Collection<PRFTCreditApplicationModel>();
            foreach(var creditApplication in CreditApplications)
            {
                creditApplication.TradeReferences = new Collection<PRFTTradeReferencesModel>();
            }
        }
    }
}
