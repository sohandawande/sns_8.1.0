﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
	public class ShippingServiceCodeListModel : BaseListModel
	{
		public Collection<ShippingServiceCodeModel> ShippingServiceCodes { get; set; }

		public ShippingServiceCodeListModel()
		{
			ShippingServiceCodes = new Collection<ShippingServiceCodeModel>();
		}
	}
}