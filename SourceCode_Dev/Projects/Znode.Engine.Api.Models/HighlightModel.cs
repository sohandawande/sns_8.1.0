﻿namespace Znode.Engine.Api.Models
{
	public class HighlightModel : BaseModel
	{
		public string Description { get; set; }
		public int? DisplayOrder { get; set; }
		public string ImageAltTag { get; set; }
		public string ImageFile { get; set; }
		public string ImageLargePath { get; set; }
		public string ImageMediumPath { get; set; }
		public string ImageSmallPath { get; set; }
		public string ImageSmallThumbnailPath { get; set; }
		public string ImageThumbnailPath { get; set; }
		public int HighlightId { get; set; }
		public HighlightTypeModel HighlightType { get; set; }
        public string HighlightTypeName { get; set; }
		public int? HighlightTypeId { get; set; }
		public string Hyperlink { get; set; }
		public bool? HyperlinkNewWindow { get; set; }
		public bool? IsActive { get; set; }
		public int LocaleId { get; set; }
		public string Name { get; set; }
		public int? PortalId { get; set; }
        public int ProductId { get; set; }
		public string ShortDescription { get; set; }
		public bool DisplayPopup { get; set; }
        public int ProductHighlightID { get; set; }
        public bool IsAssociatedProduct { get; set; }
        public string UserName { get; set; }
        public string UserType { get; set; }
		public HighlightModel Sample()
		{
			return new HighlightModel
			{
				Description = "We offer free shipping on all items for purchases of $50 or more.",
				DisplayOrder = 10,
				HighlightId = 2,
				HighlightTypeId = 1,
				Hyperlink = "",
				HyperlinkNewWindow = false,
				ImageAltTag = "Free Shipping",
				ImageFile = "FreeShipping.jpg",
				ImageLargePath = "~/data/default/images/catalog/450/FreeShipping.png",
				ImageMediumPath = "~/data/default/images/catalog/250/FreeShipping.png",
				ImageSmallPath = "~/data/default/images/catalog/100/FreeShipping.png",
				ImageSmallThumbnailPath = "~/data/default/images/catalog/37/FreeShipping.png",
				ImageThumbnailPath = "~/data/default/images/catalog/50/FreeShipping.png",
				IsActive = true,
				LocaleId = 43,
				Name = "Free Shipping",
				PortalId = null,
				ShortDescription = "",
                DisplayPopup = true
			};
		}
	}
}
