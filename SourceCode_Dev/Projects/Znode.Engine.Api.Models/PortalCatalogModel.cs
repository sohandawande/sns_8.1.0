﻿namespace Znode.Engine.Api.Models
{
	public class PortalCatalogModel : BaseModel
	{
		public CatalogModel Catalog { get; set; }
		public int CatalogId { get; set; }
		public int? CssId { get; set; }
		public int LocaleId { get; set; }
		public int PortalCatalogId { get; set; }
		public PortalModel Portal { get; set; }
		public int PortalId { get; set; }
		public int? ThemeId { get; set; }
        public string CSSName { get; set; }
        public string ThemeName { get; set; }
        public string CatalogName { get; set; }

		public PortalCatalogModel Sample()
		{
			return new PortalCatalogModel
			{
				CatalogId = 4,
				CssId = 3,
				LocaleId = 43,
				PortalCatalogId = 1,
				PortalId = 5,
				ThemeId = 3
			};
		}
	}
}