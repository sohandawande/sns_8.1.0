﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
    public class RoleMenuListModel : BaseListModel
    {
        public Collection<RoleMenuModel> RoleMenuList { get; set; }
        public RoleMenuListModel()
		{
            RoleMenuList = new Collection<RoleMenuModel>();
		}
    }
}
