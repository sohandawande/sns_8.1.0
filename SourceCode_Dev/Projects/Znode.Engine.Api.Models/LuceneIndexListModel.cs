﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class LuceneIndexListModel : BaseListModel
    {
        public List<LuceneIndexModel> LuceneIndexList { get; set; }
       
        public LuceneIndexListModel()
        {
            LuceneIndexList = new List<LuceneIndexModel>();
        }
    }
}
