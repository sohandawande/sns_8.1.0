﻿namespace Znode.Engine.Api.Models
{
    public class UpdateImageModel : BaseModel
    {
        public int Id { get; set; }
        public string EntityName { get; set; }
        public string ImagePath { get; set; }
    }
}
