﻿
namespace Znode.Engine.Api.Models
{
    /// <summary>
    /// Model of Content Page
    /// </summary>
    public class ContentPageModel : BaseModel
    {
        public int ContentPageID { get; set; }
        public string Name { get; set; }
        public int PortalID { get; set; }
        public string Title { get; set; }
        public string SEOMetaKeywords { get; set; }
        public string SEOMetaDescription { get; set; }
        public bool AllowDelete { get; set; }
        public string TemplateName { get; set; }
        public bool ActiveInd { get; set; }        
        public string AnalyticsCode { get; set; }
        public string SEOURL { get; set; }
        public int LocaleId { get; set; }
        public string MetaTagAdditional { get; set; }
        public int? ThemeID { get; set; }
        public int? CSSID { get; set; }
        public int? MasterPageID { get; set; }
        public string SEOTitle { get; set; }
        public string OldHtml { get; set; }
        public string CustomErrorMessage { get; set; }
        public string StoreName { get; set; }
        public string MasterPageName { get; set; }

        public string Html { get; set; }
        public string UpdatedUser { get; set; }
        public string MappedSeoUrl { get; set; }
        public bool IsUrlRedirectEnabled { get; set; }
        public bool IsNameAvailable { get; set; }
    }
}
