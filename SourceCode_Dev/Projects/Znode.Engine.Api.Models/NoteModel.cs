﻿using System;
namespace Znode.Engine.Api.Models
{
    public class NoteModel : BaseModel
    {
        public int NoteId { get; set; }
        public int? CaseId { get; set; }
        public int? AccountId { get; set; }
        public string NoteTitle { get; set; }
        public string NoteBody { get; set; }
        public DateTime CreateDte { get; set; }
        public string CreateUser { get; set; }

        public NoteModel Sample()
        {
            return new NoteModel
            {
                NoteId = 1,
                CaseId = 1,
                AccountId = 1,
                NoteTitle = "",
                NoteBody = "",
                CreateDte = Convert.ToDateTime("2013-04-29 10:49:32.657"),
                CreateUser = ""
            };
        }
    }
}
