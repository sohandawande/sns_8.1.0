﻿namespace Znode.Engine.Api.Models
{
	public class AttributeTypeModel : BaseModel
	{
		public int AttributeTypeId { get; set; }
		public string Description { get; set; }
		public int DisplayOrder { get; set; }
		public bool IsPrivate { get; set; }
		public int LocaleId { get; set; }
		public string Name { get; set; }
		public int? PortalId { get; set; }

		public AttributeTypeModel Sample()
		{
			return new AttributeTypeModel
			{
				AttributeTypeId = 1,
				Description = null,
				DisplayOrder = 5,
				IsPrivate = false,
				LocaleId = 43,
				Name = "Color",
				PortalId = null
			};
		}
	}
}
