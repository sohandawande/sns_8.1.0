﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
	public class HighlightTypeListModel : BaseListModel
	{
		public Collection<HighlightTypeModel> HighlightTypes { get; set; }

		public HighlightTypeListModel()
		{
			HighlightTypes = new Collection<HighlightTypeModel>();
		}
	}
}
