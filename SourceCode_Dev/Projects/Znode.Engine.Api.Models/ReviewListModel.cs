﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
	public class ReviewListModel : BaseListModel
	{
		public Collection<ReviewModel> Reviews { get; set; }

		public ReviewListModel()
		{
			Reviews = new Collection<ReviewModel>();
		}
	}
}
