﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class PRFTERPServicesModel
    {
        
    }

    public class PRFTERPItemDetailsModel
    {
        public decimal UnitPrice { get; set; }
        public bool HasError { get; set; }
        public bool IsCallForPricing { get; set; }
    }
}
