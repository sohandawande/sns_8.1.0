﻿
namespace Znode.Engine.Api.Models
{
    public class PortalProfileModel : BaseModel
    {
        public int PortalProfileID { get; set; }
        public int PortalID { get; set; }
        public int ProfileID { get; set; }
        public string Name { get; set; }      
        public int SerialNumber { get; set; }
    }
}
