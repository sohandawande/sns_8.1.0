﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
    /// <summary>
    /// List Model for Zip Code
    /// </summary>
    public class ZipCodeListModel: BaseListModel
	{
		public Collection<ZipCodeModel> ZipCode { get; set; }

        /// <summary>
        /// Constructor for Zip Code List Model
        /// </summary>
        public ZipCodeListModel()
		{
            ZipCode = new Collection<ZipCodeModel>();
		}
	}
}
