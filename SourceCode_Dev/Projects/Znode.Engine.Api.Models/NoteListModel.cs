﻿using System.Collections.ObjectModel;
namespace Znode.Engine.Api.Models
{
    public class NoteListModel : BaseListModel
    {
        public Collection<NoteModel> Notes { get; set; }

        public NoteListModel()
        {
            Notes = new Collection<NoteModel>();
        }
    }
}
