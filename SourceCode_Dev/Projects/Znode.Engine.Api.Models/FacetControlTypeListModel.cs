﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
    public class FacetControlTypeListModel : BaseListModel
    {
         public Collection<FacetControlTypeModel> FacetControlTypes { get; set; }

         public FacetControlTypeListModel()
        {
            FacetControlTypes = new Collection<FacetControlTypeModel>();
        }
    }
}
