﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
	public class CaseStatusListModel : BaseListModel
	{
		public Collection<CaseStatusModel> CaseStatuses { get; set; }

		public CaseStatusListModel()
		{
			CaseStatuses = new Collection<CaseStatusModel>();
		}
	}
}
