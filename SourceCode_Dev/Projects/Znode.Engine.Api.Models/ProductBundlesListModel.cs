﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
    public class ProductBundlesListModel: BaseListModel
	{
        public Collection<ProductBundlesModel> ProductBundles { get; set; }

        public ProductBundlesListModel()
		{
            ProductBundles = new Collection<ProductBundlesModel>();
		}
    }
}
