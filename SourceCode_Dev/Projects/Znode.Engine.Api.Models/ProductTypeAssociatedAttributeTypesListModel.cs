﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
    /// <summary>
    /// List Model for Associate Attributes To Product Types.
    /// </summary>
    public class ProductTypeAssociatedAttributeTypesListModel : BaseListModel
    {
        public Collection<ProductTypeAssociatedAttributeTypesModel> AttributeList { get; set; }

        public ProductTypeAssociatedAttributeTypesListModel()
        {
            AttributeList = new Collection<ProductTypeAssociatedAttributeTypesModel>();
        }
    }
}
