﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace Znode.Engine.Api.Models
{
	public abstract class BaseModel
	{
		public string ToJson()
		{
			return JsonConvert.SerializeObject(this, Newtonsoft.Json.Formatting.Indented);
		}

		public string ToXml()
		{
			var xml = String.Empty;

			var serializer = new XmlSerializer(GetType());
			var ms = new MemoryStream();

			using (var tw = new XmlTextWriter(ms, Encoding.UTF8) { Formatting = System.Xml.Formatting.Indented })
			{
				serializer.Serialize(tw, this);
				ms = tw.BaseStream as MemoryStream;

				if (ms != null)
				{
					xml = new UTF8Encoding().GetString(ms.ToArray());
					ms.Dispose();
				}
			}

			return xml;
		}
	}
}
