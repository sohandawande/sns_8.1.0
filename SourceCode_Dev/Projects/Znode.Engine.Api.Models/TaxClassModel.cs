﻿namespace Znode.Engine.Api.Models
{
	public class TaxClassModel : BaseModel
	{
		public int? DisplayOrder { get; set; }
		public string ExternalId { get; set; }
        public bool ActiveInd { get; set; }
		public string Name { get; set; }
		public int? PortalId { get; set; }
		public int TaxClassId { get; set; }
        public string StoreName { get; set; }
        public string CustomErrorMessage { get; set; }
        public TaxRuleListModel TaxRuleList { get; set; }

		public TaxClassModel Sample()
		{
			return new TaxClassModel
			{
				DisplayOrder = 1,
				ExternalId = null,
                ActiveInd = true,
				Name = "Default",
				PortalId = 7,
				TaxClassId = 2
			};
		}
	}
}
