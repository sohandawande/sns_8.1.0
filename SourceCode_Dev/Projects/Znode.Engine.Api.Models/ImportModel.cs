﻿using System.Collections.Generic;
using System.IO;
using System.Web;
namespace Znode.Engine.Api.Models
{
    public class ImportModel : BaseModel
    {
        public ImportModel()
        {
            ImportConfigData = new ImportConfigUploadHeadModel();
        }
        public string ImportFile { get; set; }
        public string Type { get; set; }
        public string UserName { get; set; }
        public bool Status { get; set; }
        public List<dynamic> ImportData { get; set; }
        public List<dynamic> ErrorData { get; set; }
        public ImportConfigUploadHeadModel ImportConfigData { get; set; }
    }

    public class ImportConfigUploadHeadModel : BaseModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string TemplateUniqueName { get; set; }
        public string DownloadLink { get; set; }
        public int? Status { get; set; }
        public string Description { get; set; }
    }
}
