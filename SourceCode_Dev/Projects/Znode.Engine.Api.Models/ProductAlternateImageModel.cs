﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class ProductAlternateImageModel : BaseModel
    {
        public int ProductImageID { get; set; }
        public int ProductID { get; set; }
        public string Name { get; set; }
        public string ImageFile { get; set; }
        public string ImageAltTag { get; set; }
        public string AlternateThumbnailImageFile { get; set; }
        public bool ActiveInd { get; set; }
        public bool ShowOnCategoryPage { get; set; }
        public int? ProductImageTypeID { get; set; }
        public int? DisplayOrder { get; set; }
        public int? ReviewStateID { get; set; }
        public string ImagePath { get; set; }
        public string ReviewStateString { get; set; }
        public string VendorName { get; set; }
        public int? VendorId { get; set; }
        public string ProductSku { get; set; }
        public string ImageTypeName { get; set; }
        public string UserName { get; set; }
    }
}
