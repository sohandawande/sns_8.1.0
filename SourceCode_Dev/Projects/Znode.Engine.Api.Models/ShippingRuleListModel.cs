﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
	public class ShippingRuleListModel : BaseListModel
	{
		public Collection<ShippingRuleModel> ShippingRules { get; set; }

		public ShippingRuleListModel()
		{
			ShippingRules = new Collection<ShippingRuleModel>();
		}
	}
}