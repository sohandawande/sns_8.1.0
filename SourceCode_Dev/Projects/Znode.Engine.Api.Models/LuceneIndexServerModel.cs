﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
   public class LuceneIndexServerModel:BaseModel
    {
       public string ServerName { get; set; }
       public int Status { get; set; }
       public DateTime StartTime { get; set; }
       public DateTime? EndTime { get; set; }
    }
}
