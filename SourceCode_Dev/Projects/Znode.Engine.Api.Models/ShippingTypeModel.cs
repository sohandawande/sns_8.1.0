﻿namespace Znode.Engine.Api.Models
{
	public class ShippingTypeModel : BaseModel
	{
		public string ClassName { get; set; }
		public string Description { get; set; }
		public bool IsActive { get; set; }
		public string Name { get; set; }
		public int ShippingTypeId { get; set; }

		public ShippingTypeModel Sample()
		{
			return new ShippingTypeModel
			{
				ClassName = "ZnodeShippingFedEx",
				Description = "Calculates shipping rates when using FedEx.",
				IsActive = true,
				Name = "FedEx",
				ShippingTypeId = 1
			};
		}
	}
}
