﻿using System;

namespace Znode.Engine.Api.Models
{
    public partial class CaseRequestModel : BaseModel
    {
        //Used to send proper email to Sales Representative and Customer Service.
        public string City { get; set; }
        public string CountryCode { get; set; }
        public string StateCode { get; set; }
        public int SalesRepId { get; set; }
        public DateTime DueDate { get; set; }
        public string FaxNumber { get; set; }
        public string ContactChoiceSelected { get; set; }
        public bool IsRequestAQuoteForm { get; set; }   
        public string SalesRepName { get; set; }
        public string SalesRepEmail { get; set; }
    }
}
