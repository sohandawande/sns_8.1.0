﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class ZnodeDeleteModel : BaseModel
    {
        public bool IsDeleted { get; set; }
        public string ErrorMessage { get; set; }
    }
}
