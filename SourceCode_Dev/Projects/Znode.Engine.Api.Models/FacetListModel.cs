﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
    public class FacetListModel : BaseListModel
    {
         public Collection<FacetModel> Facets{ get; set; }

         public FacetListModel()
        {
            Facets = new Collection<FacetModel>();
        }
    }
}
