﻿
namespace Znode.Engine.Api.Models
{
    public class DigitalAssetModel : BaseModel
    {
        public int DigitalAssetID { get; set; }
        public int ProductID { get; set; }
        public string DigitalAsset { get; set; }
        public int? OrderLineItemID { get; set; }
        public string UserName { get; set; }
        public string UserType { get; set; }
    }
}
