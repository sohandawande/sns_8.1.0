﻿
namespace Znode.Engine.Api.Models
{
    public class CategoryAssociatedProductsModel : BaseModel
    {
        public string CategoryName { get; set; }
        public string ManufacturerName { get; set; }
        public string Name { get; set; }
        public string ProductNumber { get; set; }
        public string SKU { get; set; }
        public string ProductType { get; set; }
        public string ImageFile { get; set; }
        public string ImageLargePath { get; set; }
        public string ImageSmallPath { get; set; }
        public string ImageThumbnailPath { get; set; }
        public string ImageSmallThumbnailPath { get; set; }
        public string ImageMediumPath { get; set; }

        public int CategoryId { get; set; }
        public int ProductId { get; set; }
        public int SKUId { get; set; }
        public int ManufacturerId { get; set; }
        public int ProductTypeId { get; set; }
        public bool IsActive { get; set; }  
    }
}
