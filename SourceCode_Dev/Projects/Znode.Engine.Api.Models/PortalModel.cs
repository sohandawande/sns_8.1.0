﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
    public class PortalModel : BaseModel
    {
        public string AdminEmail { get; set; }
        public string CompanyName { get; set; }
        public Collection<CatalogModel> Catalogs { get; set; }
        public int? CurrencyTypeId { get; set; }
        public string CustomerServiceEmail { get; set; }
        public string CustomerServicePhoneNumber { get; set; }
        public int? DefaultAnonymousProfileId { get; set; }
        public int? DefaultOrderStateId { get; set; }
        public int? DefaultProductReviewStateId { get; set; }
        public int? DefaultRegisteredProfileId { get; set; }
        public string DefaultReviewStatus { get; set; }
        public string DimensionUnit { get; set; }
        public string EmailListDefaultList { get; set; }
        public string EmailListPassword { get; set; }
        public string EmailListUsername { get; set; }
        public bool? EnableAddressValidation { get; set; }
        public bool? EnableCustomerPricing { get; set; }
        public bool? EnablePims { get; set; }
        public string ExternalId { get; set; }
        public string FedExAccountNumber { get; set; }
        public bool? FedExAddInsurance { get; set; }
        public string FedExClientProductId { get; set; }
        public string FedExClientProductVersion { get; set; }
        public string FedExCspKey { get; set; }
        public string FedExCspPassword { get; set; }
        public string FedExDropoffType { get; set; }
        public string FedExMeterNumber { get; set; }
        public string FedExPackagingType { get; set; }
        public string FedExProductionKey { get; set; }
        public string FedExSecurityCode { get; set; }
        public bool? FedExUseDiscountRate { get; set; }
        public string GoogleAnalyticsCode { get; set; }
        public string ImageNotAvailablePath { get; set; }
        public bool InclusiveTax { get; set; }
        public bool IsActive { get; set; }
        public bool IsShippingTaxable { get; set; }
        public int LocaleId { get; set; }
        public string LogoPath { get; set; }
        public string MasterPage { get; set; }
        public int MaxCatalogCategoryDisplayThumbnails { get; set; }
        public byte MaxCatalogDisplayColumns { get; set; }
        public int MaxCatalogDisplayItems { get; set; }
        public int MaxCatalogItemCrossSellWidth { get; set; }
        public int MaxCatalogItemLargeWidth { get; set; }
        public int MaxCatalogItemMediumWidth { get; set; }
        public int MaxCatalogItemSmallThumbnailWidth { get; set; }
        public int MaxCatalogItemSmallWidth { get; set; }
        public int MaxCatalogItemThumbnailWidth { get; set; }
        public string MobileTheme { get; set; }
        public string OrderReceiptAffiliateJavascript { get; set; }
        public bool? PersistentCartEnabled { get; set; }
        public Collection<PortalCatalogModel> PortalCatalogs { get; set; }
        public Collection<PortalCountryModel> PortalCountries { get; set; }
        public int PortalId { get; set; }
        public bool? RequireValidatedAddress { get; set; }
        public string SalesEmail { get; set; }
        public string SalesPhoneNumber { get; set; }
        public string SeoDefaultCategoryDescription { get; set; }
        public string SeoDefaultCategoryKeyword { get; set; }
        public string SeoDefaultCategoryTitle { get; set; }
        public string SeoDefaultContentDescription { get; set; }
        public string SeoDefaultContentKeyword { get; set; }
        public string SeoDefaultContentTitle { get; set; }
        public string SeoDefaultProductDescription { get; set; }
        public string SeoDefaultProductKeyword { get; set; }
        public string SeoDefaultProductTitle { get; set; }
        public string ShippingOriginAddress1 { get; set; }
        public string ShippingOriginAddress2 { get; set; }
        public string ShippingOriginCity { get; set; }
        public string ShippingOriginCountryCode { get; set; }
        public string ShippingOriginPhone { get; set; }
        public string ShippingOriginStateCode { get; set; }
        public string ShippingOriginZipCode { get; set; }
        public int ShopByPriceIncrement { get; set; }
        public int ShopByPriceMax { get; set; }
        public int ShopByPriceMin { get; set; }
        public bool ShowAlternateImageInCategory { get; set; }
        public bool ShowSwatchInCategory { get; set; }
        public string SiteWideAnalyticsJavascript { get; set; }
        public string SiteWideBottomJavascript { get; set; }
        public string SiteWideTopJavascript { get; set; }
        public string SmtpPassword { get; set; }
        public int? SmtpPort { get; set; }
        public string SmtpServer { get; set; }
        public string SmtpUsername { get; set; }
        public int? SplashCategoryId { get; set; }
        public string SplashImageFile { get; set; }
        public string StoreName { get; set; }
        public string TimeZoneOffset { get; set; }
        public string UpsKey { get; set; }
        public string UpsPassword { get; set; }
        public string UpsUsername { get; set; }
        public bool? UseDynamicDisplayOrder { get; set; }
        public bool UseSsl { get; set; }
        public string WeightUnit { get; set; }

        public string OrderStatus { get; set; }
        public string DefaultProductReviewStateName { get; set; }
        public string CurrencySuffix { get; set; }
        public string CurrencyName { get; set; }
        public string CatalogName { get; set; }
        public string CssName { get; set; }
        public string ThemeName { get; set; }
        public string LocaleDescription { get; set; }
        public bool? IsEnableCompare { get; set; }
        public string CompareType { get; set; }
        public int? TabMode { get; set; }
        public bool IsMultipleCouponAllowed { get; set; }
        public bool? IsEnableSinglePageCheckout { get; set; }
        public string MobileLogoPath { get; set; }
        public CurrencyTypeModel CurrencyTypeModel { get; set; }
        public bool EnableSslForSmtp { get; set; }
        public PortalModel()
        {
            Catalogs = new Collection<CatalogModel>();
            PortalCountries = new Collection<PortalCountryModel>();
            PortalCatalogs = new Collection<PortalCatalogModel>();
            CurrencyTypeModel = new CurrencyTypeModel();
        }

        public PortalModel Sample()
        {
            return new PortalModel
            {
                AdminEmail = "admin@test.com",
                CompanyName = "MaxWell's",
                CurrencyTypeId = 107,
                CustomerServiceEmail = "customerservice@test.com",
                CustomerServicePhoneNumber = "555-111-2222",
                DefaultAnonymousProfileId = 1,
                DefaultOrderStateId = 50,
                DefaultProductReviewStateId = 10,
                DefaultRegisteredProfileId = 2,
                DefaultReviewStatus = "N",
                DimensionUnit = "IN",
                EmailListDefaultList = null,
                EmailListPassword = null,
                EmailListUsername = null,
                EnableAddressValidation = true,
                EnableCustomerPricing = false,
                IsEnableCompare = false,
                EnablePims = false,
                ExternalId = null,
                FedExAccountNumber = "icZ1z0zfF54=",
                FedExAddInsurance = true,
                FedExClientProductId = "KHHG",
                FedExClientProductVersion = "3203",
                FedExCspKey = "vuB2Ui3CCpHkCs53",
                FedExCspPassword = "TAt234utxOFiJuJM2Yw0oL7sD",
                FedExDropoffType = "REGULAR_PICKUP",
                FedExMeterNumber = "icZ1z0zfF54=",
                FedExPackagingType = "YOUR_PACKAGING",
                FedExProductionKey = "icZ1z0zfF54=",
                FedExSecurityCode = "icZ1z0zfF54=",
                FedExUseDiscountRate = false,
                GoogleAnalyticsCode = null,
                ImageNotAvailablePath = "~/data/default/images/noimage.gif",
                InclusiveTax = true,
                IsActive = true,
                IsShippingTaxable = false,
                LocaleId = 43,
                LogoPath = "~/data/default/content/maxwell.gif",
                MasterPage = null,
                MaxCatalogCategoryDisplayThumbnails = 5,
                MaxCatalogDisplayColumns = 4,
                MaxCatalogDisplayItems = 12,
                MaxCatalogItemCrossSellWidth = 97,
                MaxCatalogItemLargeWidth = 450,
                MaxCatalogItemMediumWidth = 250,
                MaxCatalogItemSmallThumbnailWidth = 37,
                MaxCatalogItemSmallWidth = 100,
                MaxCatalogItemThumbnailWidth = 50,
                MobileTheme = "B2B",
                OrderReceiptAffiliateJavascript = null,
                PersistentCartEnabled = true,
                PortalId = 1,
                RequireValidatedAddress = true,
                SalesEmail = "sales@test.com",
                SalesPhoneNumber = "555-111-2222",
                SeoDefaultCategoryDescription = null,
                SeoDefaultCategoryKeyword = null,
                SeoDefaultCategoryTitle = null,
                SeoDefaultContentDescription = null,
                SeoDefaultContentKeyword = null,
                SeoDefaultContentTitle = null,
                SeoDefaultProductDescription = null,
                SeoDefaultProductKeyword = null,
                SeoDefaultProductTitle = null,
                ShippingOriginAddress1 = "123 Anywhere Street",
                ShippingOriginAddress2 = "",
                ShippingOriginCity = "Columbus",
                ShippingOriginCountryCode = "US",
                ShippingOriginPhone = "555-111-2222",
                ShippingOriginStateCode = "OH",
                ShippingOriginZipCode = "43210",
                ShopByPriceIncrement = 20,
                ShopByPriceMax = 100,
                ShopByPriceMin = 0,
                ShowAlternateImageInCategory = false,
                ShowSwatchInCategory = false,
                SiteWideAnalyticsJavascript = null,
                SiteWideBottomJavascript = null,
                SiteWideTopJavascript = null,
                SmtpPassword = "icZ1z0zfF54=",
                SmtpPort = 25,
                SmtpServer = "",
                SmtpUsername = "icZ1z0zfF54=",
                SplashCategoryId = 81,
                SplashImageFile = "images_231110124101_061210031622.gif",
                StoreName = "Fine Foods",
                TimeZoneOffset = null,
                UpsKey = "icZ1z0zfF54=",
                UpsPassword = "icZ1z0zfF54=",
                UpsUsername = "icZ1z0zfF54=",
                UseDynamicDisplayOrder = false,
                UseSsl = true,
                WeightUnit = "LBS",
                IsMultipleCouponAllowed = false,
                IsEnableSinglePageCheckout = false,
                MobileLogoPath = "images_231110124101_061210031622.gif"
            };
        }
    }
}