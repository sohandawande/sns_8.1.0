﻿
using System.Web;
namespace Znode.Engine.Api.Models
{
    public class EmailTemplateModel : BaseModel
    {
        public string TemplateName { get; set; }
        public string DeleteTemplate { get; set; }
        public string Key { get; set; }
        public string Html { get; set; }
        public string CustomErrorMessage { get; set; }
        public string Extension { get; set; }

        public bool IsNewAdd { get; set; }
    }
}
