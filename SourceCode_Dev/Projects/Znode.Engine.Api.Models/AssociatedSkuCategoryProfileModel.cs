﻿
namespace Znode.Engine.Api.Models
{
    /// <summary>
    /// Model for available profile.
    /// </summary>
    public class AssociatedSkuCategoryProfileModel
    {
        public string UserName { get; set; }

        public int SkuId { get; set; }
        public int SkuEffectiveId { get; set; }
        public int CategoryId { get; set; }
        public int CategoryProfileId { get; set; }
    }
}
