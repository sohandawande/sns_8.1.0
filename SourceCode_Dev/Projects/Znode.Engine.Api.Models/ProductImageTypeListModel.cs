﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
    public class ProductImageTypeListModel : BaseListModel
    {
        public Collection<ProductImageTypeModel> ProductImageTypes { get; set; }

        public ProductImageTypeListModel()
		{
            ProductImageTypes = new Collection<ProductImageTypeModel>();
		}
    }
}
