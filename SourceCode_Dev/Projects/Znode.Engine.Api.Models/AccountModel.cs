﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace Znode.Engine.Api.Models
{
    public class AccountModel : BaseModel
    {
        public int AccountId { get; set; }
        public string AccountProfileCode { get; set; }
        public AccountTypeModel AccountType { get; set; }
        public int? AccountTypeId { get; set; }
        public Collection<AddressModel> Addresses { get; set; }
        public string CompanyName { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreateUser { get; set; }
        public string ReferralCommissionType { get; set; }
        public string Custom1 { get; set; }
        public string Custom2 { get; set; }
        public string Custom3 { get; set; }
        public string Description { get; set; }
        public string Email { get; set; }
        public bool EmailOptIn { get; set; }
        public bool? EnableCustomerPricing { get; set; }
        public string ExternalId { get; set; }
        public Collection<GiftCardHistoryModel> GiftCardHistoryList { get; set; }
        public bool? IsActive { get; set; }
        public Collection<OrderModel> Orders { get; set; }
        public int? ParentAccountId { get; set; }
        public int? ProfileId { get; set; }
        public Collection<ProfileModel> Profiles { get; set; }
        public int? ReferralAccountId { get; set; }
        public decimal? ReferralCommission { get; set; }
        public int? ReferralCommissionTypeId { get; set; }
        public string ReferralStatus { get; set; }
        public string Source { get; set; }
        public int? SubAccountLimit { get; set; }
        public string TaxId { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string UpdateUser { get; set; }
        public UserModel User { get; set; }
        public AddressModel AddressModel { get; set; }
        public Guid? UserId { get; set; }
        public DateTime? WebServiceDownloadDate { get; set; }
        public string Website { get; set; }
        public Collection<WishListModel> WishList { get; set; }
        //Znode Version 7.2.2
        public string BaseUrl { get; set; }
        //Znode Version 8.0
        public bool IsUserInRole { get; set; }
        public string UserName { get; set; }
        public string RoleName { get; set; }
        public string ErrorCode { get; set; }

        public string FullName { get; set; }
        public string PhoneNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        
        //PRFT Custom Code : Start 
        public string MiddleName { get; set; } // Used For Sales Rep Location
        //PRFT Custom Code : End
        public string LoginName { get; set; }
        public string ExternalAccountNum { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int? PortalId { get; set; }
        public string UserType { get; set; }
        public bool IsEmailSentFailed { get; set; }
        public bool IsSinglePageCheckoutUser { get; set; }
        public string TrackingLink { get; set; }
        
        public AccountModel()
        {
            Addresses = new Collection<AddressModel>();
            Orders = new Collection<OrderModel>();
            Profiles = new Collection<ProfileModel>();
            WishList = new Collection<WishListModel>();
            GiftCardHistoryList = new Collection<GiftCardHistoryModel>();
        }

        public AccountModel Sample()
        {
            return new AccountModel
            {
                AccountId = 11521,
                AccountProfileCode = null,
                AccountTypeId = 0,
                CompanyName = "Your Company",
                CreateDate = Convert.ToDateTime("2013-04-29 10:49:32.657"),
                CreateUser = null,
                Custom1 = null,
                Custom2 = null,
                Custom3 = null,
                Description = "Site Administrator Account",
                Email = "znodeadmin1@test.com",
                EmailOptIn = true,
                EnableCustomerPricing = false,
                ExternalId = null,
                GiftCardHistoryList = null,
                IsActive = true,
                ParentAccountId = null,
                ProfileId = 1,
                ReferralAccountId = null,
                ReferralCommission = null,
                ReferralCommissionTypeId = null,
                ReferralStatus = null,
                Source = null,
                SubAccountLimit = null,
                TaxId = null,
                UpdateDate = Convert.ToDateTime("2013-09-21 11:59:19.740"),
                UpdateUser = null,
                UserId = new Guid("8086A820-D3F6-4763-AE07-CE84AB85E2E7"),
                WebServiceDownloadDate = null,
                Website = null,
                BaseUrl = null,
                IsSinglePageCheckoutUser = false,
            };
        }
    }
}
