﻿
namespace Znode.Engine.Api.Models
{
    public class ProductImageTypeModel : BaseModel
    {
        public int ProductImageTypeID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
