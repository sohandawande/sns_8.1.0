﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class LuceneIndexServerListModel : BaseListModel
    {
        public List<LuceneIndexServerModel> IndexServerStatusList { get; set; }
        public LuceneIndexServerListModel ()
        {
            IndexServerStatusList = new List<LuceneIndexServerModel>();
        }
    }
}
