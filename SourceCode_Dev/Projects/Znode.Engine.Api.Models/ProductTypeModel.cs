﻿namespace Znode.Engine.Api.Models
{
	public class ProductTypeModel : BaseModel
	{
		public string Description { get; set; }
        public string OldProductTypeName { get; set; }
        public string Name { get; set; }

		public int DisplayOrder { get; set; }
        public int ProductTypeId { get; set; }

        public int? PortalId { get; set; }

		public bool IsFranchiseable { get; set; }
		public bool? IsGiftCard { get; set; }

		public ProductTypeModel Sample()
		{
			return new ProductTypeModel
			{
				Description = "Default Product",
				DisplayOrder = 1,
				IsFranchiseable = true,
				IsGiftCard = false,
				Name = "Default",
				PortalId = null,
				ProductTypeId = 7
			};
		}
	}
}