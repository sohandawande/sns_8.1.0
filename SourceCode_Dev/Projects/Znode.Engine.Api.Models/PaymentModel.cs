﻿namespace Znode.Engine.Api.Models
{
    public class PaymentModel : BaseModel
    {
        public string AuthorizationCode { get; set; }
        public AddressModel BillingAddress { get; set; }
        public AddressModel ShippingAddress { get; set; }
        public CreditCardModel CreditCard { get; set; }
        public GatewayTokenModel GatewayToken { get; set; }
        public decimal NonRecurringItemsTotalAmount { get; set; }
        public string PaymentName { get; set; }
        public PaymentOptionModel PaymentOption { get; set; }
        public bool RecurringBillingExists { get; set; }
        public bool SaveCardData { get; set; }
        public bool Secure3D { get; set; }
        public string SessionId { get; set; }

        public string SubscriptionId { get; set; }
        public int TokenId { get; set; }
        public string TransactionId { get; set; }
        public bool UseToken { get; set; }
        public string WorldPayEchoData { get; set; }
        public string WorldPayHeaderCookie { get; set; }
        public string WorldPayPostData { get; set; }

        public PaymentModel Sample()
        {
            return new PaymentModel
            {

            };
        }
    }
}
