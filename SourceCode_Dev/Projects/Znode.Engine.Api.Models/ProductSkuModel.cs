﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class ProductSkuModel : BaseModel
    {
        public ProductSkuModel()
        {

        }
        public string SKU { get; set; }
        public string SKUPicturePath { get; set; }
        public string SKUImage { get; set; }
        public string ImageAltTag { get; set; }
        public string RecurringBillingPeriod { get; set; }
        public string RecurringBillingFrequency { get; set; }
        public string AttributeIds { get; set; }
        public string ProductTypeName { get; set; }
        public string Name { get; set; } 

        public int SKUId { get; set; }
        public int ProductId { get; set; }
        public int? SupplierId { get; set; }
        public int ProductTypeId { get; set; }
        public int? RecurringBillingTotalCycles { get; set; }

        public decimal? WeightAdditional { get; set; }
        public decimal? RetailPriceOverride { get; set; }
        public decimal? SalePriceOverride { get; set; }
        public decimal? WholesalePriceOverride { get; set; }
        public decimal? RecurringBillingInitialAmount { get; set; }

        public bool IsActive { get; set; }
        public bool IsGiftCard { get; set; }
        public bool ParentChildProduct { get; set; }
        public int AttributeCount { get; set; }
        public int InventoryId { get; set; }
        public int? QuantityOnHand { get; set; }
        public int? ReorderLevel { get; set; }

        //PRFT Custom Properties:Start
        public string AttributeDescription { get; set; } 

        public string Custom1 { get; set; }

        public string Custom2 { get; set; }

        public string Custom3 { get; set; }

        //PRFT Custom Properties:Start
    }
}
