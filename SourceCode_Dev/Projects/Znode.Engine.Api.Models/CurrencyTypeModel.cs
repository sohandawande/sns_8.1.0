﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class CurrencyTypeModel : BaseModel
    {
        public int CurrencyTypeID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string CurrencySuffix { get; set; }
        public string Symbol { get; set; }
    }
}
