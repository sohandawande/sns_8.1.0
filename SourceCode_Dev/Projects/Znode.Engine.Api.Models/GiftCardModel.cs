﻿using System;
using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
	public class GiftCardModel : BaseModel
	{
		public int? AccountId { get; set; }
		public decimal? Amount { get; set; }
		public string CardNumber { get; set; }
		public int CreatedBy { get; set; }
		public DateTime CreateDate { get; set; }
		public DateTime? ExpirationDate { get; set; }
		public int GiftCardId { get; set; }
		public Collection<GiftCardHistoryModel> History { get; set; } 
		public string Name { get; set; }
		public OrderLineItemModel OrderLineItem { get; set; }
		public int? OrderLineItemId { get; set; }
		public int PortalId { get; set; }
        public string DisplayAmount { get; set; }
        public int OrderId { get; set; }
		public GiftCardModel()
		{
			History = new Collection<GiftCardHistoryModel>();
		}

		public GiftCardModel Sample()
		{
			return new GiftCardModel
			{
				AccountId = null,
				Amount = 10.00m,
				CardNumber = "W4KO51LQ7G",
				CreatedBy = 1,
				CreateDate = Convert.ToDateTime("2013-09-01 00:00:00.000"),
				ExpirationDate = Convert.ToDateTime("2013-09-01 23:59:59.999"),
				GiftCardId = 1,
				Name = "My Gift Card",
				OrderLineItemId = null,
				PortalId = 1
			};
		}
	}
}
