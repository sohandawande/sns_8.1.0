﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
    public class ProductLevelSettingListModel : BaseListModel
    {
        public Collection<ProductLevelSettingModel> Products { get; set; }

        public ProductLevelSettingListModel()
        {
            Products = new Collection<ProductLevelSettingModel>();
        }
    }
}