﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
	public class AddOnValueListModel :BaseListModel
	{
		public Collection<AddOnValueModel> AddOnValues { get; set; }

		public AddOnValueListModel()
		{
            AddOnValues = new Collection<AddOnValueModel>();
		}
	}
}