﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
    /// <summary>
    /// List View Model for CategoryAssociatedProducts
    /// </summary>
    public class CategoryAssociatedProductsListModel : BaseListModel
    {
        public Collection<CategoryAssociatedProductsModel> ProductList { get; set; }

        /// <summary>
        /// Constructor for CategoryAssociatedProductsListModel
        /// </summary>
        public CategoryAssociatedProductsListModel()
		{
            ProductList = new Collection<CategoryAssociatedProductsModel>();
		}
    }
}
