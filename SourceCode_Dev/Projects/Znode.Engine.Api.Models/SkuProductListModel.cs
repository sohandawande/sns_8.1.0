﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
    /// <summary>
    /// List model for sku product
    /// </summary>
    public class SkuProductListModel : BaseListModel
    {
        public Collection<SkuProductModel> SkuProductList { get; set; }

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public SkuProductListModel()
        {
            SkuProductList = new Collection<SkuProductModel>();
        }
        #endregion
    }
}
