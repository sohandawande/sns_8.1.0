﻿namespace Znode.Engine.Api.Models
{
	public class ShippingOptionModel : BaseModel
	{
		public string CountryCode { get; set; }
		public string Description { get; set; }
        public string CustomErrorMessage { get; set; }
		public int DisplayOrder { get; set; }
		public string ExternalId { get; set; }
		public decimal HandlingCharge { get; set; }
        public bool ActiveInd { get; set; }
		public int? ProfileId { get; set; }
		public string ShippingCode { get; set; }
		public int ShippingId { get; set; }
        public int PortalId { get; set; }
		public ShippingTypeModel ShippingType { get; set; }
		public int ShippingTypeId { get; set; }
        public ShippingRuleModel ShippingRule { get; set; }
        public int ShippingServiceCodeId { get; set; }
        public ShippingServiceCodeModel ShippingServiceCode { get; set; }
        public string ShippingtypeName { get; set; }
        public string ProfileName { get; set; }
        public string ClassName { get; set; }
        public string UserType { get; set; }
		public ShippingOptionModel Sample()
		{
			return new ShippingOptionModel
			{
				CountryCode = "US",
				Description = "Custom Flat Rate",
				DisplayOrder = 1,
				ExternalId = null,
				HandlingCharge = 2.50m,
                ActiveInd = true,
				ProfileId = 7,
				ShippingCode = "FLAT",
                ShippingId = 9,
				ShippingTypeId = 1
			};
		}
	}
}
