﻿namespace Znode.Engine.Api.Models
{
	public class CrossSellModel : BaseModel
	{
		public int? DisplayOrder { get; set; }
		public int ProductCrossSellTypeId { get; set; }
		public int ProductId { get; set; }
		public int RelatedProductId { get; set; }
		public int RelationTypeId { get; set; }
        public string ProductName { get; set; }
        public string CrossSellProducts { get; set; }


        public string FrequentlyBoughtProduct1 { get; set; }
        public string FrequentlyBoughtProduct2 { get; set; }

        public ProductRelationCode ProductRelation { get; set; }

	    public CrossSellModel Sample()
	    {
		    return new CrossSellModel
			    {
				    DisplayOrder = 10,
				    ProductCrossSellTypeId = 1,
				    ProductId = 302,
				    RelatedProductId = 329,
				    RelationTypeId = 4,
				    ProductRelation = ProductRelationCode.FrequentlyBoughtTogether,
			    };
	    }
	}

    public enum ProductRelationCode
    {
        AddOn = 1,
        Bundle = 2,
        Upgrade = 3,
        FrequentlyBoughtTogether= 4,
        YouMayAlsoLike = 5
    }
}