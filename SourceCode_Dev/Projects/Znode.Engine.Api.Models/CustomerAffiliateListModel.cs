﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class CustomerAffiliateListModel : BaseListModel
    {
        public Collection< CustomerAffiliateModel> CustomerAffiliates { get; set; }

        /// <summary>
        /// Constructor For CustomerAffiliateListModel
        /// </summary>
        public CustomerAffiliateListModel()
		{
			CustomerAffiliates = new Collection<CustomerAffiliateModel>();
		}
    }
}
