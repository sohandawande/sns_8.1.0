﻿namespace Znode.Engine.Api.Models
{
	public class PaymentTypeModel : BaseModel
	{
		public string Description { get; set; }
		public bool IsActive { get; set; }
		public string Name { get; set; }
		public int PaymentTypeId { get; set; }

		public PaymentTypeModel Sample()
		{
			return new PaymentTypeModel
			{
				Description = "Google Checkout",
				IsActive = true,
				Name = "Google Checkout",
				PaymentTypeId = 3
			};
		}
	}
}