﻿
namespace Znode.Engine.Api.Models
{
    public class BreadCrumbModel : BaseModel
    {
        public string BreadCrumb { get; set; }
        public int CategoryID { get; set; }
        public string Name { get; set; }
        public string SEOUrl { get; set; }
        public bool IsProductPresent { get; set; }
        public int CatalogId { get; set; }
    }
}
