﻿namespace Znode.Engine.Api.Models
{
    public class PRFTTradeReferencesModel : BaseModel
    {
        public int TradeRefID { get; set; }
        public int CreditApplicationID { get; set; }
        public string ReferenceName { get; set; }
        public string BusinessName { get; set; }
        public string Street { get; set; }
        public string Street1 { get; set; }
        public string City { get; set; }
        public string StateCode { get; set; }
        public string CountryCode { get; set; }
        public string PostalCode { get; set; }
        public string PhoneNumber { get; set; }
        public string FaxNumber { get; set; }
        public string Custom1 { get; set; }
        public string Custom2 { get; set; }
        public string Custom3 { get; set; }
        public string Custom4 { get; set; }
        public string Custom5 { get; set; }

        public PRFTTradeReferencesModel Sample()
        {
            return new PRFTTradeReferencesModel
            {
                TradeRefID = 1,
                CreditApplicationID = 1,
                ReferenceName = "Reference Person",
                BusinessName = "Your Company",
                Street = "123 Fake Street",
                Street1 = "",
                City = "Fake City",
                StateCode = "SD",
                CountryCode = "US",
                PostalCode = "12345",
                PhoneNumber = "1-888-Your-Store",
                FaxNumber = "1-888-Your-Store",
                Custom1 = null,
                Custom2 = null,
                Custom3 = null,
                Custom4=null,
                Custom5=null
            };
        }
    }
}
