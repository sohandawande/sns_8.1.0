﻿namespace Znode.Engine.Api.Models
{
	public class AttributeModel : BaseModel
	{
		public int AttributeId { get; set; }
		public AttributeTypeModel AttributeType { get; set; }
		public int AttributeTypeId { get; set; }
		public int DisplayOrder { get; set; }
		public string ExternalId { get; set; }
		public bool IsActive { get; set; }
		public string Name { get; set; }
		public int? OldAttributeId { get; set; }
        public string AttributeTypeName { get; set; }

		public AttributeModel Sample()
		{
			return new AttributeModel
			{
				AttributeId = 2,
				AttributeTypeId = 1,
				DisplayOrder = 1,
				ExternalId = "",
				IsActive = true,
				Name = "Red",
				OldAttributeId = null,
                AttributeTypeName = ""
			};
		}
	}
}
