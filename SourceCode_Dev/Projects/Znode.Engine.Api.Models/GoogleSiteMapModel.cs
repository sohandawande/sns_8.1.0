﻿
namespace Znode.Engine.Api.Models
{
    /// <summary>
    /// Google Site Map Model
    /// </summary>
    public class GoogleSiteMapModel : BaseModel
    {
        public int GoogleSiteMapId { get; set; }
        public int[] PortalId { get; set; }

        public string ChangeFreq { get; set; }        
        public string RootTag { get; set; } 
        public string RootTagValue { get; set; } 
        public string XmlFileName { get; set; }
        public string LastModified { get; set; }
        public string Date { get; set; }
        public string SuccessXMLGenerationMessage { get; set; }

        public decimal Priority { get; set; }
        public StoresModel Stores { get; set; }
        public string ErrorMessage { get; set; }

        public string GoogleSiteMapFeedTitle { get; set; }
        public string GoogleSiteMapFeedLink { get; set; }
        public string GoogleSiteMapFeedDescription { get; set; }
    }
}
