﻿using System;

namespace Znode.Engine.Api.Models
{
	public class CategoryNodeModel : BaseModel
	{
		public CatalogModel Catalog { get; set; }
		public int? CatalogId { get; set; }
		public CategoryModel Category { get; set; }
		public int CategoryId { get; set; }
		public int CategoryNodeId { get; set; }
		public DateTime? BeginDate { get; set; }
		public int? CssId { get; set; }
		public int? DisplayOrder { get; set; }
		public DateTime? EndDate { get; set; }
		public bool IsActive { get; set; }
		public int? MasterPageId { get; set; }
		public int? ParentCategoryNodeId { get; set; }
		public int? ThemeId { get; set; }
        public string ErrorMessage { get; set; }

        public string Name { get; set; }
        public string Title { get; set; }
        public bool? VisibleInd { get; set; }
        public bool IsCategoryAssociated { get; set; }
		public CategoryNodeModel Sample()
		{
			return new CategoryNodeModel
			{
				CatalogId = 1,
				CategoryId = 89,
				CategoryNodeId = 7,
				BeginDate = null,
				CssId = null,
				DisplayOrder = 5,
				EndDate = null,
				IsActive = true,
				MasterPageId = null,
				ParentCategoryNodeId = 1,
				ThemeId = null
			};
		}
	}
}
