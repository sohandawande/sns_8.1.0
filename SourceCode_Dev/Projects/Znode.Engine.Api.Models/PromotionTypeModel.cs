﻿namespace Znode.Engine.Api.Models
{
	public class PromotionTypeModel : BaseModel
	{
		public string ClassName { get; set; }
		public string ClassType { get; set; }
		public string Description { get; set; }
		public bool IsActive { get; set; }
		public string Name { get; set; }
		public int PromotionTypeId { get; set; }
        public string UserType { get; set; }

		public PromotionTypeModel Sample()
		{
			return new PromotionTypeModel
			{
				ClassName = "ZnodeCartPromotionAmountOffProduct",
				ClassType = "CART",
				Description = "Applies an amount off a product for an order; affects the shopping cart.",
				IsActive = true,
				Name = "Amount Off Product",
				PromotionTypeId = 6
			};
		}
	}
}
