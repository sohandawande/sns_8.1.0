﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
	public class CategoryNodeListModel : BaseListModel
	{
		public Collection<CategoryNodeModel> CategoryNodes { get; set; }

		public CategoryNodeListModel()
		{
			CategoryNodes = new Collection<CategoryNodeModel>();
		}
	}
}
