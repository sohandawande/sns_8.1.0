﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class ApplicationSettingListModel : BaseListModel
    {
        public List<ApplicationSettingDataModel> ApplicationSettingList { get; set; }

        public ApplicationSettingListModel()
        {
            ApplicationSettingList = new List<ApplicationSettingDataModel>();
        }

    }
}
