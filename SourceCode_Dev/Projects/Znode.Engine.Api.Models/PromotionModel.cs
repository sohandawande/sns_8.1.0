﻿using System;

namespace Znode.Engine.Api.Models
{
	public class PromotionModel : BaseModel
	{
		public int? AccountId { get; set; }
		public int? AddOnValueId { get; set; }
		public int? CatalogId { get; set; }
		public int? CategoryId { get; set; }
		public string CouponCode { get; set; }
		public int? CouponQuantityAvailable { get; set; }
		public string Custom1 { get; set; }
		public string Custom2 { get; set; }
		public string Custom3 { get; set; }
		public string Description { get; set; }
		public decimal Discount { get; set; }      
		public int? DiscountedProductId { get; set; }
		public int? DiscountedProductQuantity { get; set; }
		public int DisplayOrder { get; set; }
		public bool? EnableCouponUrl { get; set; }
		public DateTime EndDate { get; set; }
		public string ExternalId { get; set; }
		public bool HasCoupon { get; set; }
		public int? ManufacturerId { get; set; }
		public decimal? MinimumOrderAmount { get; set; }
		public string Name { get; set; }
		public int? PortalId { get; set; }
		public int? ProfileId { get; set; }
		public string PromoCode { get; set; }
		public int PromotionId { get; set; }
		public string PromotionMessage { get; set; }
		public PromotionTypeModel PromotionType { get; set; }
        public string StoreName { get; set; }
        public string Profile { get; set; }
		public int PromotionTypeId { get; set; }
		public int? RequiredProductId { get; set; }
		public int? RequiredProductMinimumQuantity { get; set; }
		public int? SkuId { get; set; }
		public DateTime StartDate { get; set; }
        public bool IsCouponAllowedWithOtherCoupons { get; set; }


		public PromotionModel Sample()
		{
			return new PromotionModel
			{
				AccountId = null,
				AddOnValueId = null,
				CatalogId = 4,
				CategoryId = 86,
				CouponCode = "",
				CouponQuantityAvailable = 0,
				Custom1 = "",
				Custom2 = "",
				Custom3 = "",
				Description = "Applies a percent off order",
				Discount = 10.00m,
				DiscountedProductId = null,
				DiscountedProductQuantity = 0,
				DisplayOrder = 1,
				EnableCouponUrl = false,
				EndDate = Convert.ToDateTime("2013-08-31 00:00:00.000"),
				ExternalId = "",
				HasCoupon = false,
				ManufacturerId = 3,
				MinimumOrderAmount = 0.00m,
				Name = "Percent Off Order",
				PortalId = 7,
				ProfileId = 5,
				PromoCode = "AUG2013",
				PromotionId = 13,
				PromotionMessage = "Good through August 31, 2013",
				PromotionTypeId = 6,
				RequiredProductId = 552,
				RequiredProductMinimumQuantity = 1,
				SkuId = null,
				StartDate = Convert.ToDateTime("2013-08-01 00:00:00.000"),
                IsCouponAllowedWithOtherCoupons=false
			};
		}
	}
}
