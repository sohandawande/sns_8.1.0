﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
	public class PaymentTypeListModel : BaseListModel
	{
		public Collection<PaymentTypeModel> PaymentTypes { get; set; }

		public PaymentTypeListModel()
		{
			PaymentTypes = new Collection<PaymentTypeModel>();
		}
	}
}
