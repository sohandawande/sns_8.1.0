﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class ThemeListModel : BaseListModel
    {
        public Collection<ThemeModel> Themes { get; set; }
        public ThemeListModel()
        {
            Themes = new Collection<ThemeModel>();
        }
    }
}
