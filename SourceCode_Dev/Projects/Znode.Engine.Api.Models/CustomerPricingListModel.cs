﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
    public class CustomerPricingListModel : BaseListModel
    {
        public Collection<CustomerPricingModel> CustomerPricing { get; set; }

        public CustomerPricingListModel()
		{
            CustomerPricing = new Collection<CustomerPricingModel>();
		}
    }
}
