﻿namespace Znode.Engine.Api.Models
{
	public class TaxRuleModel : BaseModel
	{
		public string CountryCode { get; set; }
		public string CountyFips { get; set; }
        public string CountyName { get; set; }
		public string Custom1 { get; set; }
		public string Custom2 { get; set; }
		public string Custom3 { get; set; }
		public string ExternalId { get; set; }
		public decimal? Gst { get; set; }
		public decimal? Hst { get; set; }
		public bool IsInclusive { get; set; }
		public int PortalId { get; set; }
		public int Precedence { get; set; }
		public decimal? Pst { get; set; }
		public decimal? SalesTax { get; set; }
		public string StateCode { get; set; }
		public TaxClassModel TaxClass { get; set; }
		public int? TaxClassId { get; set; }
		public int TaxRuleId { get; set; }
		public TaxRuleTypeModel TaxRuleType { get; set; }
		public int? TaxRuleTypeId { get; set; }
		public bool TaxShipping { get; set; }
		public decimal? Vat { get; set; }

		public TaxRuleModel Sample()
		{
			return new TaxRuleModel
			{
				CountryCode = "US",
				CountyFips = "",
				Custom1 = "",
				Custom2 = "",
				ExternalId = null,
				Gst = 0.00m,
				Hst = 0.00m,
				IsInclusive = false,
				PortalId = 1,
				Precedence = 1,
				Pst = 0.00m,
				SalesTax = 5.00m,
				StateCode = "OH",
				TaxClassId = 1,
				TaxRuleId = 18,
				TaxRuleTypeId = 1,
				TaxShipping = false,
				Vat = 0.00m
			};
		}
	}
}
