﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
	public class StateListModel : BaseListModel
	{
		public Collection<StateModel> States { get; set; }

		public StateListModel()
		{
			States = new Collection<StateModel>();
		}
	}
}
