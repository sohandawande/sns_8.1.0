﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
    public class OrderLineItemListModel : BaseListModel
    {
        public Collection<OrderLineItemModel> OrdersLineItems { get; set; }

        public OrderLineItemListModel()
        {
            OrdersLineItems = new Collection<OrderLineItemModel>();
        }

    }
}
