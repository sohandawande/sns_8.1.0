﻿using System;

namespace Znode.Engine.Api.Models
{
	public class AuditModel : BaseModel
	{
		public long AuditId { get; set; }
		public AuditSourceTypeModel AuditSourceType { get; set; }
		public int AuditSourceTypeId { get; set; }
		public AuditStatusModel AuditStatus { get; set; }
		public int AuditStatusId { get; set; }
		public AuditTypeModel AuditType { get; set; }
		public int AuditTypeId { get; set; }
		public DateTime CreateDate { get; set; }
		public string CreatedBy { get; set; }
		public string ExternalId { get; set; }
		public DateTime? ProcessedDate { get; set; }
		public long SourceId { get; set; }
	    public ProductModel Product { get; set; }
	    public CategoryModel Category { get; set; }
	    public CatalogModel Catalog { get; set; }
	    public PortalModel Portal { get; set; }

		public AuditModel Sample()
		{
			return new AuditModel
			{
				AuditId = 1,
				AuditSourceTypeId = 1,
				AuditStatusId = 1,
				AuditTypeId = 1,
				CreateDate = Convert.ToDateTime("2013-04-29 10:49:32.657"),
				CreatedBy = "Username",
				ExternalId = null,
				ProcessedDate = null,
				SourceId = 1
			};
		}
	}
}
