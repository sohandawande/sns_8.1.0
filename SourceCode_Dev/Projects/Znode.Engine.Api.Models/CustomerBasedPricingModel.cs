﻿
namespace Znode.Engine.Api.Models
{
    public class CustomerBasedPricingModel : BaseModel
    {
        public int ProductId { get; set; }
        public int CustomerPricingId { get; set; }
        public int AccountId { get; set; }
        public string ExternalAccountNo { get; set; }
        public decimal? NegotiatedPrice { get; set; }
        public string CompanyName { get; set; }
        public decimal BasePrice { get; set; }
        public string FullName { get; set; }
        public decimal? Discount { get; set; }

        //Order customer based pricing product
        public int ExternalID { get; set; }
        public string SKU { get; set; }
        public string Name { get; set; }
    }
}
