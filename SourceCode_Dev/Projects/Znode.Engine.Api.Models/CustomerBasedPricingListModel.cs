﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
    public class CustomerBasedPricingListModel : BaseListModel
    {
        public Collection<CustomerBasedPricingModel> CustomerBasedPricing { get; set; }

        public CustomerBasedPricingListModel()
		{
            CustomerBasedPricing = new Collection<CustomerBasedPricingModel>();
		}

        public int ProductId { get; set; }
        public int CustomerId { get; set; }
        public string ExternalAccountNo { get; set; }
    }
}
