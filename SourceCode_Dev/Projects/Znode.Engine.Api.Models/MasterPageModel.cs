﻿
namespace Znode.Engine.Api.Models
{
    public class MasterPageModel : BaseModel
    {
        public int MasterPageId { get; set; }

        public int ThemeId { get; set; }

        public string Name { get; set; }
    }
}
