﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
    public class ProfileCommonListResponse : BaseListResponse
    {
        public Collection<ProfileCommonModel> Profiles { get; set; }
    }
}
