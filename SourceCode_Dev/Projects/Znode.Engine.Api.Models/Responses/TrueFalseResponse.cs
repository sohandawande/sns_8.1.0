﻿
namespace Znode.Engine.Api.Models.Responses
{
    /// <summary>
    /// This is the True false response class
    /// </summary>
    public class TrueFalseResponse : BaseResponse
    {
        public BooleanModel booleanModel { get; set; }
    }
}
