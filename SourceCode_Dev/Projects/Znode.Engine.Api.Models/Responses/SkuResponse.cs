﻿namespace Znode.Engine.Api.Models.Responses
{
	public class SkuResponse : BaseResponse
	{
		public SkuModel Sku { get; set; }
	}
}
