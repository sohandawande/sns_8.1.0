﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
	public class OrderListResponse : BaseListResponse
	{
		public Collection<OrderModel> Orders { get; set; }
	}
}
