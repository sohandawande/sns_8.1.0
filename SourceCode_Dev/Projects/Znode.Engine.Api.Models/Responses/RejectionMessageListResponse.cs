﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
    /// <summary>
    /// List Response for Rejection Message.
    /// </summary>
    public class RejectionMessageListResponse : BaseListResponse
    {
        public Collection<RejectionMessageModel> RejectionMessages { get; set; }
    }
}
