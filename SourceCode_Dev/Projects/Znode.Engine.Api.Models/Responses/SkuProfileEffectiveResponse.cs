﻿
namespace Znode.Engine.Api.Models.Responses
{
    /// <summary>
    /// Responce of the Sku Profile Effective.
    /// </summary>
    public class SkuProfileEffectiveResponse : BaseResponse
    {
        public SkuProfileEffectiveModel SkuProfileEffective { get; set; }
    }
}
