﻿namespace Znode.Engine.Api.Models.Responses
{
    public class UpdateImageResponse : BaseResponse
    {
        public UpdateImageModel Image { get; set; }
    }
}
