﻿using System.Collections.ObjectModel;
namespace Znode.Engine.Api.Models.Responses
{
    public class CaseStatusListResponse : BaseListResponse
    {
        public Collection<CaseStatusModel> CaseStatus { get; set; }
    }
}
