﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
    public class FacetControlTypeListResponse : BaseListResponse
    {
        public Collection<FacetControlTypeModel> FacetControlTypes { get; set; }
    }
}
