﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
    public class ProductAlternateImageListResponse : BaseListResponse
    {
        public Collection<ProductAlternateImageModel> ProductImages { get; set; }
    }
}
