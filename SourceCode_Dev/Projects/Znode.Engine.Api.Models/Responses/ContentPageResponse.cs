﻿
namespace Znode.Engine.Api.Models.Responses
{
    /// <summary>
    /// Responce of Content Page
    /// </summary>
    public class ContentPageResponse : BaseResponse
    {
        public ContentPageModel ContentPage { get; set; }
        public ContentPageModel ContentPageCopy { get; set; }
        public bool IsContentPageAdded { get; set; }
        public bool IsContentPageUpdated { get; set; }
        public string Html { get; set; }
    }
}
