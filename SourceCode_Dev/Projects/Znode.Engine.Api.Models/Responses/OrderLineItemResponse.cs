﻿
namespace Znode.Engine.Api.Models.Responses
{
    /// <summary>
    /// Response for order line item.
    /// </summary>
   public class OrderLineItemResponse : BaseResponse
    {
       public OrderLineItemModel OrderLineItem { get; set; }
    }
}
