﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
    public class CustomerBasedPricingListResponse : BaseListResponse
    {
        public Collection<CustomerBasedPricingModel> CustomerBasedPricingList { get; set; }
    }
}
