﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models.Responses
{
    public class ManageSearchIndexResponse : BaseListResponse
    {
        public LuceneIndexListModel LuceneIndexList { get; set; }
        public List<LuceneIndexServerModel> IndexServerList { get; set; }
        public bool CreateIndex { get; set; }
        public int DisableFlag { get; set; }
    }
}
