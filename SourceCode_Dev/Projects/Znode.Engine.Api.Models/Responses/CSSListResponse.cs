﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models.Responses
{
    public class CSSListResponse : BaseListResponse
    {
        public Collection<CSSModel> CSSs { get; set; }
    }
}
