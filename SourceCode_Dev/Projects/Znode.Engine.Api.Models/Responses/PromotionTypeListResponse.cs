﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
	public class PromotionTypeListResponse : BaseListResponse
	{
		public Collection<PromotionTypeModel> PromotionTypes { get; set; }
	}
}
