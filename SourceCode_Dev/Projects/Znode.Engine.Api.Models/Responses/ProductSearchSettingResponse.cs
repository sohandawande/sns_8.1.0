﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
    public class ProductSearchSettingResponse : BaseListResponse
    {
        public Collection<ProductLevelSettingModel> Products { get; set; }
        public Collection<CategoryLevelSettingModel> Categories { get; set; }
        public Collection<FieldLevelSettingModel> Fields { get; set; }

        public bool SaveFlag { get; set; }
    }
}
