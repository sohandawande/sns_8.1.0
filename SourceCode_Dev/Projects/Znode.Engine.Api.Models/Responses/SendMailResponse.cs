﻿namespace Znode.Engine.Api.Models.Responses
{
    public class SendMailResponse : BaseListResponse
    {
        public SendMailModel Email { get; set; }
    }
}
