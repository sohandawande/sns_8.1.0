﻿namespace Znode.Engine.Api.Models.Responses
{
    public class UrlRedirectResponse : BaseResponse
    {
        public UrlRedirectModel UrlRedirect { get; set; }
    }
}
