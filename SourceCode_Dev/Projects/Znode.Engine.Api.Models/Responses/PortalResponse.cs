﻿namespace Znode.Engine.Api.Models.Responses
{
	public class PortalResponse : BaseResponse
	{
		public PortalModel Portal { get; set; }
        public bool IsMessagesCreated { get; set; }
        public bool IsStoreCopied { get; set; }
	}
}
