﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
	public class StateListResponse : BaseListResponse
	{
		public Collection<StateModel> States { get; set; }
	}
}
