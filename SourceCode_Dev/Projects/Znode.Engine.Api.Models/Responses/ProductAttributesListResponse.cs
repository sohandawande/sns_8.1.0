﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
    public class ProductAttributesListResponse : BaseListResponse
    {
        public Collection<AttributeModel> Attributes { get; set; }

        public ProductAttributeListModel AttributeList { get; set; }
    }
}
