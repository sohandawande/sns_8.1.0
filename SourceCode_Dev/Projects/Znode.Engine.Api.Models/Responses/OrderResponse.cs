﻿using System.Data;
namespace Znode.Engine.Api.Models.Responses
{
    public class OrderResponse : BaseResponse
    {
        public OrderResponse()
        {
            Order = new OrderModel();
            OrderDownloadData = new DataSet();
        }
        public OrderModel Order { get; set; }
        public DataSet OrderDownloadData { get; set; }
        public string EmailSentMessage { get; set; }
    }

}
