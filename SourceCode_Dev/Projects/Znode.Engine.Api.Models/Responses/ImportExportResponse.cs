﻿
namespace Znode.Engine.Api.Models.Responses
{
    public class ImportExportResponse: BaseResponse
	{
		public ExportModel Exports { get; set; }
        public ImportModel Imports { get; set; }
    }
}
