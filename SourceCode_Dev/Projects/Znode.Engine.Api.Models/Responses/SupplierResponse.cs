﻿namespace Znode.Engine.Api.Models.Responses
{
	public class SupplierResponse : BaseResponse
	{
		public SupplierModel Supplier { get; set; }
	}
}
