﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
    public class AddOnValueListResponse : BaseListResponse
    {
        public Collection<AddOnValueModel> AddOnValues { get; set; }

        public AddOnValueListModel AddOnValuesList { get; set; }

    }
}