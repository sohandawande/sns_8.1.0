﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
    public class MasterPageListResponse : BaseListResponse
    {
        public Collection<MasterPageModel> MasterPages { get; set; }
    }
}
