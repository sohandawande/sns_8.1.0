﻿
namespace Znode.Engine.Api.Models.Responses
{
    /// <summary>
    /// Response For Attributes as per product Types.
    /// </summary>
    public class ProductTypeAttributeResponse : BaseResponse
    {
        public ProductTypeAttributeModel ProductTypeAttribute { get; set; }

        public bool productTypeAssociatedProduct { get; set; }
    }
}
