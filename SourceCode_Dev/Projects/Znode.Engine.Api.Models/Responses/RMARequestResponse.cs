﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models.Responses
{
    public class RMARequestResponse : BaseResponse
    {
        public RMARequestResponse()
        {
            RMAGiftCardDetails = new DataSet();
        }

        public RMARequestModel RMARequest { get; set; }
        public DataSet RMAGiftCardDetails { get; set; }

    }
}
