﻿namespace Znode.Engine.Api.Models.Responses
{
    public class CaseRequestResponse : BaseResponse
    {
        public CaseRequestModel CaseRequest { get; set; }
    }
}
