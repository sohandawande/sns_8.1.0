﻿namespace Znode.Engine.Api.Models.Responses
{
    public class EnvironmentConfigResponse : BaseResponse
    {
        public EnvironmentConfigModel EnvironmentConfig { get; set; }
    }
}
