﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
    public class AccountPaymentListResponse : BaseListResponse
    {
        public Collection<AccountPaymentModel> AccountPayments { get; set; }
    }
}
