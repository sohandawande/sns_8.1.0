﻿
namespace Znode.Engine.Api.Models.Responses
{
    public class ProductReviewHistoryResponse: BaseResponse
	{
        public ProductReviewHistoryModel ProductReview { get; set; }
    }
}
