﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
	public class SuggestedSearchListResponse : BaseListResponse
	{
		public Collection<SuggestedSearchModel> SuggestedSearchResults { get; set; }

		public SuggestedSearchListResponse()
		{
			SuggestedSearchResults = new Collection<SuggestedSearchModel>();
		}
	}
}
