﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
	public class AddressListResponse : BaseListResponse
	{
		public Collection<AddressModel> Addresses { get; set; }

        public AddressListModel AddressListModel { get; set; }
	}
}
