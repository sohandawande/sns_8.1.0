﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
    public class VendorProductListResponse : BaseListResponse
    {
        public Collection<ProductModel> Products { get; set; }

        public RejectProductModel RejectProduct { get; set; }
    }
}
