﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
	public class SkuListResponse : BaseListResponse
	{
		public Collection<SkuModel> Skus { get; set; }
	}
}
