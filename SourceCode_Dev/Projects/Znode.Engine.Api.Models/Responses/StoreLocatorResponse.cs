﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models.Responses
{
    /// <summary>
    /// Response for Store Locator.
    /// </summary>
    public class StoreLocatorResponse : BaseResponse
    {
        public StoreModel StoreLocator { get; set; }
    }
}
