﻿
namespace Znode.Engine.Api.Models.Responses
{
    public class AddOnValueResponse : BaseResponse
    {
        public AddOnValueModel AddOnValue { get; set; }
    }
}
