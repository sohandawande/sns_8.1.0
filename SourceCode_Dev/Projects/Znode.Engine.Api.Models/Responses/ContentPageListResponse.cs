﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
    public class ContentPageListResponse:BaseListResponse
    {
        public Collection<ContentPageModel> ContentPages { get; set; }
    }
}
