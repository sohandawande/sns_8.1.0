﻿
namespace Znode.Engine.Api.Models.Responses
{
    public class AdminOrderResponse : BaseResponse
    {
        public AdminOrderModel Order { get; set; }
    }
}
