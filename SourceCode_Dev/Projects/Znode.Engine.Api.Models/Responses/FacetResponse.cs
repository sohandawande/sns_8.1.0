﻿
namespace Znode.Engine.Api.Models.Responses
{
    public class FacetResponse : BaseResponse
    {
        public FacetModel Facet { get; set; }
    }
}
