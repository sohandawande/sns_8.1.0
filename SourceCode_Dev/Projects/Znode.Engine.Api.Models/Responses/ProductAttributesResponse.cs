﻿
namespace Znode.Engine.Api.Models.Responses
{
    public class ProductAttributesResponse : BaseResponse
    {
        public AttributeModel Attributes { get; set; }
    }
}
