﻿namespace Znode.Engine.Api.Models.Responses
{
	public class ShippingOptionResponse : BaseResponse
	{
		public ShippingOptionModel ShippingOption { get; set; }
	}
}
