﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
    public class ReferralCommissionListResponse : BaseListResponse
    {
        public Collection<ReferralCommissionModel> ReferralCommissions { get; set; }
    }
}
