﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
    public class CrossSellListResponse : BaseListResponse
    {
        public Collection<CrossSellModel> ProductCrossSells { get; set; }
    }
}
