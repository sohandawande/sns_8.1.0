﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
    public class RegisteredSocialClientListResponse : BaseListResponse
    {
        public Collection<RegisteredSocialClientModel> SocialClients { get; set; }
    }
}
