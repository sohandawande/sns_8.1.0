﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
	public class PaymentGatewayListResponse : BaseListResponse
	{
		public Collection<PaymentGatewayModel> PaymentGateways { get; set; }
	}
}
