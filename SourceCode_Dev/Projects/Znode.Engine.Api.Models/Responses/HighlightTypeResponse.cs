﻿namespace Znode.Engine.Api.Models.Responses
{
	public class HighlightTypeResponse : BaseResponse
	{
		public HighlightTypeModel HighlightType { get; set; }
	}
}
