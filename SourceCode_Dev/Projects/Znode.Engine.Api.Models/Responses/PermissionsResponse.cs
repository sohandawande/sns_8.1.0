﻿
namespace Znode.Engine.Api.Models.Responses
{
    /// <summary>
    /// This class will act as a response for the roles and permissions
    /// </summary>
    public class PermissionsResponse : BaseResponse
    {
        public PermissionsModel Permissions { get; set; }
    }
}
