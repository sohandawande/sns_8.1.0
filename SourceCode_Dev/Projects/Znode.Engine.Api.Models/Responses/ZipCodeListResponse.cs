﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
    /// <summary>
    /// List Response for Zip Code
    /// </summary>
   public class ZipCodeListResponse : BaseListResponse
	{
		public Collection<ZipCodeModel> ZipCode { get; set; }
	}
}

 