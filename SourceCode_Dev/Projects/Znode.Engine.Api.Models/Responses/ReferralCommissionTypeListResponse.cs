﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
    public class ReferralCommissionTypeListResponse : BaseListResponse
    {
        public Collection<ReferralCommissionTypeModel> ReferralCommissionTypes { get; set; }
    }
}
