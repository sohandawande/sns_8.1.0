﻿
namespace Znode.Engine.Api.Models.Responses
{
    /// <summary>
    /// Response for Google Site Map
    /// </summary>
    public class GoogleSiteMapResponse : BaseResponse
    {
        public GoogleSiteMapModel GoogleSiteMap { get; set; }
    }
}
