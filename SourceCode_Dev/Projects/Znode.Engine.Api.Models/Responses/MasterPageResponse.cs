﻿
namespace Znode.Engine.Api.Models.Responses
{
    class MasterPageResponse : BaseResponse
    {
        public MasterPageModel MasterPage { get; set; }
    }
}
