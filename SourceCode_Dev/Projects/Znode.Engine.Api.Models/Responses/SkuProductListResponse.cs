﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
    /// <summary>
    /// List response for sku product model
    /// </summary>
    public class SkuProductListResponse : BaseListResponse
    {
        public Collection<SkuProductModel> SkuProducts { get; set; }
    }
}
