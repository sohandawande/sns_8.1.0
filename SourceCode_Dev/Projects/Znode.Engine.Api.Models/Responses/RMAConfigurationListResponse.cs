﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
    public class RMAConfigurationListResponse : BaseListResponse
    {
        public Collection<RMAConfigurationModel> RMAConfiguratons { get; set; }
    }
}
