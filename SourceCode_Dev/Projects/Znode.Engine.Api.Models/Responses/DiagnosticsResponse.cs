﻿
namespace Znode.Engine.Api.Models.Responses
{
    public class DiagnosticsResponse : BaseResponse
    {
        public string ProductVersion { get; set; }
    }
}
