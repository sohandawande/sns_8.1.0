﻿using System.Collections.Generic;
using System.Data;

namespace Znode.Engine.Api.Models.Responses
{
    public class ApplicationSettingListResponse : BaseListResponse
    {
        public List<ApplicationSettingDataModel> List { get; set; }
        public DataSet ColumnList { get; set; }
        public bool CreateStatus { get; set; }

        public ApplicationSettingListResponse()
        {
            ColumnList = new DataSet();
        }
    }
}
