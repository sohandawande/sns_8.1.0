﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
    public class CasePrioritiesListResponse : BaseListResponse
    {
        public Collection<CasePriorityModel> CasePriorities { get; set; }
    }
}
