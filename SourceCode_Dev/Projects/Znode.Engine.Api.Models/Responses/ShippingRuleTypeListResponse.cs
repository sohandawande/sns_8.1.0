﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
	public class ShippingRuleTypeListResponse : BaseListResponse
	{
		public Collection<ShippingRuleTypeModel> ShippingRuleTypes { get; set; }
	}
}
