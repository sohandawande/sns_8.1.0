﻿namespace Znode.Engine.Api.Models.Responses
{
	public class PaymentGatewayResponse : BaseResponse
	{
		public PaymentGatewayModel PaymentGateway { get; set; }
	}
}
