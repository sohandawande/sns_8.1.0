﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
	public class PaymentOptionListResponse : BaseListResponse
	{
		public Collection<PaymentOptionModel> PaymentOptions { get; set; }
	}
}
