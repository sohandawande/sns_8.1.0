﻿
namespace Znode.Engine.Api.Models.Responses
{
    /// <summary>
    /// Response for Zip Code
    /// </summary>
   public class ZipCodeResponse : BaseResponse
	{
		public ZipCodeModel ZipCode { get; set; }
	}
}

   