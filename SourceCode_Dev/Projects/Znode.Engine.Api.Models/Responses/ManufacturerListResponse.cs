﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
	public class ManufacturerListResponse : BaseListResponse
	{
		public Collection<ManufacturerModel> Manufacturers { get; set; }
	}
}
