﻿using System.Collections.ObjectModel;
namespace Znode.Engine.Api.Models.Responses
{
    public class CaseNoteListResponse : BaseListResponse
    {
        public Collection<NoteModel> CaseNotes { get; set; }
    }
}
