﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
    public class RequestStatusListResponse:BaseListResponse
    {
        public Collection<RequestStatusModel> RequestStatusList { get; set; }
    }
}
