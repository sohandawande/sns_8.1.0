﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
	public class RolePermissionListResponse : BaseListResponse
	{
		public Collection<RolePermissionModel> UserPermissionList { get; set; }
	}
}
