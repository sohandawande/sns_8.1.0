﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
	public class CaseRequestListResponse : BaseListResponse
	{
		public Collection<CaseRequestModel> CaseRequests { get; set; }
	}
}
