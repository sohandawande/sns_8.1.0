﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
    public class ProductReviewHistoryListResponse : BaseListResponse
    {
        public Collection<ProductReviewHistoryModel> ProductsHistory { get; set; }
    }
}
