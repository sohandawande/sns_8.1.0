﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
	public class ShoppingCartListResponse : BaseListResponse
	{
		public Collection<ShoppingCartModel> ShoppingCarts { get; set; }
	}
}
