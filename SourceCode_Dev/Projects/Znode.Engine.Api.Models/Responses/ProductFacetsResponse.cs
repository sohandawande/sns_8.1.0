﻿
namespace Znode.Engine.Api.Models.Responses
{
    public class ProductFacetsResponse: BaseResponse
	{
        public ProductFacetModel ProductFacets { get; set; }
    }
}
