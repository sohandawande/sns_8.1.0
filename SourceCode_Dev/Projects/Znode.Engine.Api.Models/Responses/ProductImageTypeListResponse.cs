﻿using System.Collections.ObjectModel;
namespace Znode.Engine.Api.Models.Responses
{
    public class ProductImageTypeListResponse : BaseListResponse
    {
        public Collection<ProductImageTypeModel> ImageTypes { get; set; }
    }
}
