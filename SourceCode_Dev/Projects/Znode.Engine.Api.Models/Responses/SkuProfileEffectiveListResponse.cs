﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
    /// <summary>
    /// Responce of the Sku Profile Effective List.
    /// </summary>
    public class SkuProfileEffectiveListResponse : BaseListResponse
    {
        public Collection<SkuProfileEffectiveModel> SkuProfileEffectives { get; set; }
    }
}
