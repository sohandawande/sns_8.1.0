﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
    public class RoleMenuListResponse : BaseListResponse
    {
        public Collection<RoleMenuModel> RoleMenuList { get; set; }
    }
}
