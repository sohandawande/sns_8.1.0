﻿namespace Znode.Engine.Api.Models.Responses
{
    public class CrossSellResponse : BaseResponse
    {
        public CrossSellModel CrossSell { get; set; }
    }
}
