﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
    public class EmailTemplateListResponse : BaseListResponse
    {
        public Collection<EmailTemplateModel> EmailTemplates { get; set; }
    }
}
