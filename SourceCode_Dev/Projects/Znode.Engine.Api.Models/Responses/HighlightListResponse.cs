﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
	public class HighlightListResponse : BaseListResponse
	{
		public Collection<HighlightModel> Highlights { get; set; }

        public Collection<HighlightTypeModel> HighlightTypes { get; set; }
	}
}
