﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
	public class TaxRuleListResponse : BaseListResponse
	{
		public Collection<TaxRuleModel> TaxRules { get; set; }
	}
}
