﻿namespace Znode.Engine.Api.Models.Responses
{
	public class PortalCountryResponse : BaseResponse
	{
		public PortalCountryModel PortalCountry { get; set; }
        public bool IsPortalCountryAdded { get; set; }
	}
}
