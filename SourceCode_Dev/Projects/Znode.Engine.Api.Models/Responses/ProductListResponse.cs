﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
	public class ProductListResponse : BaseListResponse
	{
		public Collection<ProductModel> Products { get; set; }

        public ProductListModel ProductList { get; set; }

        public CategoryAssociatedProductsListModel CategoryAssociatedProducts { get; set; }
	}
}
