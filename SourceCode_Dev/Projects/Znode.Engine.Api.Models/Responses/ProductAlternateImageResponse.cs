﻿
namespace Znode.Engine.Api.Models.Responses
{
    public class ProductAlternateImageResponse:BaseResponse
    {
        public ProductAlternateImageModel ProductImage { get; set; }
    }
}
