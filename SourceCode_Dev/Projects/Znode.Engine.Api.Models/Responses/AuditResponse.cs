﻿namespace Znode.Engine.Api.Models.Responses
{
	public class AuditResponse : BaseResponse
	{
		public AuditModel Audit { get; set; }
	}
}
