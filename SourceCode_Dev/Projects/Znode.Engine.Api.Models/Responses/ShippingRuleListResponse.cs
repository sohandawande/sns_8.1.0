﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
	public class ShippingRuleListResponse : BaseListResponse
	{
		public Collection<ShippingRuleModel> ShippingRules { get; set; }
	}
}
