﻿
namespace Znode.Engine.Api.Models.Responses
{
    public class FacetGroupResponse : BaseResponse
    {
        public FacetGroupModel FacetGroup { get; set; }
    }
}
