﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
    public class PortalProfileListResponse:BaseListResponse
    {
          public Collection<PortalProfileModel> PortalProfiles { get; set; }        
    }
}
