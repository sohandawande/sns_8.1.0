﻿using System.Collections.ObjectModel;


namespace Znode.Engine.Api.Models.Responses
{
    public class FacetListResponse : BaseListResponse
    {
        public Collection<FacetModel> Facets { get; set; }
    }
}
