﻿
namespace Znode.Engine.Api.Models.Responses
{
    /// <summary>
    /// Response for sku product.
    /// </summary>
    public class SkuProductResponse : BaseResponse
    {
        public SkuProductModel SkuProduct { get; set; }
    }
}
