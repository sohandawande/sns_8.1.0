﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
	public class ProductCategoryListResponse : BaseListResponse
	{
		public Collection<ProductCategoryModel> ProductCategories { get; set; }
	}
}
