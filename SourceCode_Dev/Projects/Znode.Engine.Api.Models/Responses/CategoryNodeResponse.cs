﻿namespace Znode.Engine.Api.Models.Responses
{
	public class CategoryNodeResponse : BaseResponse
	{
		public CategoryNodeModel CategoryNode { get; set; }
	}
}
