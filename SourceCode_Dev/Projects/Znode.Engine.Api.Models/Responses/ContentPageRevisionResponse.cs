﻿
namespace Znode.Engine.Api.Models.Responses
{
    /// <summary>
    /// Responce for content page revision.
    /// </summary>
    public class ContentPageRevisionResponse : BaseResponse
    {
        public ContentPageRevisionModel ContentPageRevision { get; set; }

        public bool IsRevertRevision { get; set; }
    }
}
