﻿
namespace Znode.Engine.Api.Models.Responses
{
    public class SEOUrlResponse : BaseResponse
    {
        public SEOUrlModel SeoUrl { get; set; }
    }
}
