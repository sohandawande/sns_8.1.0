﻿using System.Data;

namespace Znode.Engine.Api.Models.Responses
{
    public class AffiliateTrackingResponse : BaseResponse
    {
        public DataSet TrackingData { get; set; }
    }
}
