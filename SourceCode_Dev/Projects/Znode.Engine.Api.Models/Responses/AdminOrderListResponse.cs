﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
    public class AdminOrderListResponse : BaseListResponse
    {
        public Collection<AdminOrderModel> OrderList { get; set; }
    
    }
}
