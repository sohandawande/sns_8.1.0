﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
    public class ProductBundlesListResponse : BaseListResponse
    {
        public Collection<ProductBundlesModel> ProductBundles { get; set; }
    }
}
