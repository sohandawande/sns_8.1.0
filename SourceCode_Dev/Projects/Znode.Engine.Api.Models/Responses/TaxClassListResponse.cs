﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
	public class TaxClassListResponse : BaseListResponse
	{
		public Collection<TaxClassModel> TaxClasses { get; set; }
	}
}
