﻿
namespace Znode.Engine.Api.Models.Responses
{
    public class ReferralCommissionTypeResponse : BaseResponse
    {
        public ReferralCommissionTypeModel ReferralCommissionType { get; set; }
    }
}
