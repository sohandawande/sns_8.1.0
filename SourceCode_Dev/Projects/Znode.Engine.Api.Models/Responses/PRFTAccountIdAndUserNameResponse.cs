﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
    public class PRFTAccountIdAndUserNameResponse : BaseResponse
    {
        public Collection<PRFTSalesRepresentative> AccountIdAndUserNameList { get; set; }
    }
}
