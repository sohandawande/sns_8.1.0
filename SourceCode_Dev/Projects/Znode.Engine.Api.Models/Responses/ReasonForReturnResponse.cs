﻿
namespace Znode.Engine.Api.Models.Responses
{
    public class ReasonForReturnResponse : BaseResponse
    {
        public ReasonForReturnModel ReasonForReturn { get; set; }
    }
}
