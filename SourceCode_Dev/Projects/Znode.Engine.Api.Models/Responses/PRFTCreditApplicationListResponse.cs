﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
    public class PRFTCreditApplicationListResponse : BaseListResponse
    {
        public Collection<PRFTCreditApplicationModel> CreditApplications { get; set; }
    }
}
