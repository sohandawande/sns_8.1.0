﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models.Responses
{
    public class ProductTypeListResponse : BaseListResponse
    {
        public Collection<ProductTypeModel> ProductTypes { get; set; }

        public AttributeTypeValueListModel AttributeTypeValues { get; set; }

        public Collection<AttributeTypeValueModel> AttributeTypeValuesList { get; set; }
    }
}
