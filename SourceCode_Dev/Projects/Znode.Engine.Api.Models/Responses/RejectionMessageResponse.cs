﻿
namespace Znode.Engine.Api.Models.Responses
{
    /// <summary>
    /// Responce for Rejection Message.
    /// </summary>
    public class RejectionMessageResponse : BaseResponse
    {
        public RejectionMessageModel RejectionMessage { get; set; }
    }
}
