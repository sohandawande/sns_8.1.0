﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
	public class CategoryListResponse : BaseListResponse
	{
		public Collection<CategoryModel> Categories { get; set; }

        public CatalogAssociatedCategoriesListModel CatalogAssociatedCategories { get; set; }

        public CategoryListModel CategoryList { get; set; }
	}
}
