﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
	public class CategoryNodeListResponse : BaseListResponse
	{
		public Collection<CategoryNodeModel> CategoryNodes { get; set; }
	}
}
