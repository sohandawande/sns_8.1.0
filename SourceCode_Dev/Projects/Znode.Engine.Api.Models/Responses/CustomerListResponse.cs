﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{ 
    public class CustomerListResponse : BaseListResponse
    {
        public Collection<CustomerModel>  CustomerAccount { get;  set;  }
    }
}
