﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
	public class AuditListResponse : BaseListResponse
	{
		public Collection<AuditModel> Audits { get; set; }
	}
}
