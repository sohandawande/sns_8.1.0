﻿
namespace Znode.Engine.Api.Models.Responses
{
    public class BreadCrumbResponse : BaseResponse
    {
        public BreadCrumbModel BreadCrumb { get; set; }
    }
}
