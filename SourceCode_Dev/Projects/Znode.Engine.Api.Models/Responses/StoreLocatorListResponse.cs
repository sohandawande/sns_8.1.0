﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models.Responses
{
    /// <summary>
    /// List Response for Store Locator.
    /// </summary>
    public class StoreLocatorListResponse : BaseListResponse
    {
        public Collection<StoreModel> StoreLocators { get; set; }
    }
}
