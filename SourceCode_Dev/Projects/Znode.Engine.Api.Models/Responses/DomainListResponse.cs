﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
	public class DomainListResponse : BaseListResponse
	{
		public Collection<DomainModel> Domains { get; set; }
	}
}
