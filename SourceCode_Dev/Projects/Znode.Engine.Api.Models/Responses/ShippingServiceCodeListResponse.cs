﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
	public class ShippingServiceCodeListResponse : BaseListResponse
	{
		public Collection<ShippingServiceCodeModel> ShippingServiceCodes { get; set; }
	}
}
