﻿
namespace Znode.Engine.Api.Models.Responses
{
    public class RejectProductModelResponse : BaseResponse
    {
        public RejectProductModel Product { get; set; }
    }
}
