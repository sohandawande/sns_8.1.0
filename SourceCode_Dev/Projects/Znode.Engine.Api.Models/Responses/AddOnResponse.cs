﻿namespace Znode.Engine.Api.Models.Responses
{
    public class AddOnResponse : BaseResponse
    {
        public AddOnModel AddOn { get; set; }
    }
}
