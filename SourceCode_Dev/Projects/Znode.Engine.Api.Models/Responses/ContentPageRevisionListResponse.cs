﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
    public class ContentPageRevisionListResponse : BaseListResponse
    {
        public Collection<ContentPageRevisionModel> ContentPageRevisions { get; set; }
    }
}
