﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
	public class PaymentTypeListResponse : BaseListResponse
	{
		public Collection<PaymentTypeModel> PaymentTypes { get; set; }
	}
}
