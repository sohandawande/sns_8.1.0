﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
	public class ShippingOptionListResponse : BaseListResponse
	{
		public Collection<ShippingOptionModel> ShippingOptions { get; set; }
	}
}
