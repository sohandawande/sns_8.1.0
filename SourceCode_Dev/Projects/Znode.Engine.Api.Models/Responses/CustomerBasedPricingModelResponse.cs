﻿
namespace Znode.Engine.Api.Models.Responses
{
    public class CustomerBasedPricingModelResponse : BaseResponse
    {
        public CustomerBasedPricingModel CustomerPricing { get; set; }
    }
}
