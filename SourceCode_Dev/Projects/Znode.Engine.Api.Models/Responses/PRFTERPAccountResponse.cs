﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models.Responses
{
    public class PRFTERPAccountResponse: BaseResponse
    {
        public PRFTERPAccountDetailsModel ERPAccount { get; set; }
    }
}
