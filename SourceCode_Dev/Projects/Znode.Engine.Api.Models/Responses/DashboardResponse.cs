﻿
namespace Znode.Engine.Api.Models.Responses
{
    public class DashboardResponse : BaseResponse
    {
        public DashboardModel Dashboard { get; set; }
    }
}
