﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
    public class IssuedGiftCardListResponse : BaseResponse
    {
        public Collection<IssuedGiftCardModel> IssuedGiftCards { get; set; }
    }
}
