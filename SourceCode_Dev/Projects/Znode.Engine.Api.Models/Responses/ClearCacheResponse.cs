﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models.Responses
{
    /// <summary>
    /// This method retunrs the response of the Clear Cache method
    /// </summary>
    public class ClearCacheResponse : BaseResponse
    {
        public ClearCacheModel ClearCache { get; set; }
    }
}
