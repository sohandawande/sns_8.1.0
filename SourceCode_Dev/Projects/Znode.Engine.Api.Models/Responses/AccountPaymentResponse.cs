﻿
namespace Znode.Engine.Api.Models.Responses
{
    public class AccountPaymentResponse : BaseResponse
    {
        public AccountPaymentModel AccountPayment { get; set; }
    }
}
