﻿
namespace Znode.Engine.Api.Models.Responses
{
    public class CaseNoteResponse : BaseResponse
    {
        public NoteModel CaseNote { get; set; }
    }
}
