﻿
namespace Znode.Engine.Api.Models.Responses
{
    public class ConfigurationReaderResponce : BaseListResponse
    {
        public string FilterXML { get; set; }
    }
}
