﻿
namespace Znode.Engine.Api.Models.Responses
{
    public class RotateKeyResponse : BaseResponse
    {
        public string RotateKey { get; set; }
    }
}
