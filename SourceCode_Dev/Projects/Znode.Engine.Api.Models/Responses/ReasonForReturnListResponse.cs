﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
    public class ReasonForReturnListResponse : BaseListResponse
    {
        public Collection<ReasonForReturnModel> ReasonForReturnList { get; set; }
    }
}
