﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
    public class ProductSkuListResponse : BaseListResponse
    {
        public Collection<ProductSkuModel> Skus { get; set; }
    }
}
