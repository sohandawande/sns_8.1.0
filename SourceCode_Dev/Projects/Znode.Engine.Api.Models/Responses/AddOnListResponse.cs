﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models.Responses
{
    public class AddOnListResponse : BaseListResponse
    {
        public Collection<AddOnModel> AddOns { get; set; }
    }
}
