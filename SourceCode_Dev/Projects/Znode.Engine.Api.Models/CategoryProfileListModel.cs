﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class CategoryProfileListModel : BaseListModel
    {
        public Collection<CategoryProfileModel> CategoryProfiles { get; set; }

        public CategoryProfileListModel()
        {
            CategoryProfiles = new Collection<CategoryProfileModel>();
        }
    }
}
