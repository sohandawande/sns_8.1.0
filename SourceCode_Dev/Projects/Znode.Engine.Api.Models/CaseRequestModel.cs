﻿using System;

namespace Znode.Engine.Api.Models
{
    public partial class CaseRequestModel : BaseModel
    {
        public int? AccountId { get; set; }
        public string CaseOrigin { get; set; }
        public CasePriorityModel CasePriority { get; set; }
        public int CasePriorityId { get; set; }
        public int CaseRequestId { get; set; }
        public CaseStatusModel CaseStatus { get; set; }
        public int CaseStatusId { get; set; }
        public CaseTypeModel CaseType { get; set; }
        public int CaseTypeId { get; set; }
        public string CompanyName { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateUser { get; set; }
        public string Description { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int? OwnerAccountId { get; set; }
        public string PhoneNumber { get; set; }
        public int PortalId { get; set; }
        public string Title { get; set; }

        public string StoreName { get; set; }
        public string CaseStatusName { get; set; }
        public string CasePriorityName { get; set; }
        public NoteModel CaseNote { get; set; }
        public string CaseOldStatusName { get; set; }
        public string CaseNewStatusName { get; set; }

        public CaseRequestModel Sample()
        {
            return new CaseRequestModel
            {
                AccountId = 11522,
                CaseOrigin = "Contact Us Form",
                CasePriorityId = 3,
                CaseRequestId = 1,
                CaseStatusId = 1,
                CaseTypeId = 0,
                CompanyName = "Test",
                CreateDate = DateTime.Now,
                CreateUser = "",
                Description = "This is a test message.",
                Email = "test@znode.com",
                FirstName = "John",
                LastName = "Smith",
                PhoneNumber = "13453453453",
                PortalId = 1,
                OwnerAccountId = 11522,
                Title = "Contact Us Form"
            };
        }
    }
}
