﻿using System.Collections.Generic;

namespace Znode.Engine.Api.Models
{
    public class ExportModel : BaseListModel
    {
        public List<dynamic> RecordSet { get; set; }
        public string Type { get; set; }
        public string FileType { get; set; }
        public string UserName { get; set; }
        public int CatalogId { get; set; }
        public int AttributeTypeId { get; set; }
        public int CategoryId { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string AttributeIds { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string CountryCode { get; set; }
    }
}
