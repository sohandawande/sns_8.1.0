﻿
namespace Znode.Engine.Api.Models
{
    public class CustomerPricingModel : BaseModel
    {
        public int CustomerPricingId { get; set; }
        public string ExternalAccountNo { get; set; }
        public decimal? NegotiatedPrice { get; set; }
        public string SKUExternalId { get; set; }
    }
}
