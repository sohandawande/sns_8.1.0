﻿
namespace Znode.Engine.Api.Models
{
    public class RMARequestItemModel:BaseModel
    {
        public int? OrderLineItemID { get; set; }
        public int? OrderID { get; set; }
        public string ProductNum { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? MaxQuantity { get; set; }
        public decimal? Price { get; set; }
        public string SKU { get; set; }
        public decimal? DiscountAmount { get; set; }
        public decimal? ShippingCost { get; set; }
        public string PromoDescription { get; set; }
        public decimal? SalesTax { get; set; }
        public int? RMAMaxQuantity { get; set; }
        public int? RMAQuantity { get; set; }
        public bool? IsReturnable { get; set; }
        public bool? IsReceived { get; set; }
        public int? ReasonForReturnId { get; set; }
        public decimal? TaxCost { get; set; }
        public decimal? SubTotal { get; set; }
        public decimal? Total { get; set; }
        public string ReasonforReturn { get; set; }
        public int? RMARequestId { get; set; }
        public int RMARequestItemID { get; set; }
        public int AccountID { get; set; }
        public int? GCExpirationPeriod { get; set; }
        public int? Quantity { get; set; }
        public int? GiftCardId { get; set; }
        public string TransactionId { get; set; }
    }
}
