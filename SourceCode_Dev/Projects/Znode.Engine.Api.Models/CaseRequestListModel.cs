﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
	public class CaseRequestListModel : BaseListModel
	{
		public Collection<CaseRequestModel> CaseRequests { get; set; }

		public CaseRequestListModel()
		{
			CaseRequests = new Collection<CaseRequestModel>();
		}
	}
}
