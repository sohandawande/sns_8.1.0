﻿
namespace Znode.Engine.Api.Models
{
    public class FacetControlTypeModel : BaseModel
    {
        public int ControlTypeID { get; set; }
        public string ControlName { get; set; }
        public string ControlDescription { get; set; }
    }
}
