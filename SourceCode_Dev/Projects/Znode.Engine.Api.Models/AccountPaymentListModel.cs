﻿using System;
using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
    public class AccountPaymentListModel :BaseListModel
    {
        public Collection<AccountPaymentModel> AccountPayments { get; set; }

        /// <summary>
        /// Constructor for AccountPaymentListModel
        /// </summary>
        public AccountPaymentListModel()
		{
            AccountPayments = new Collection<AccountPaymentModel>();
		}
    }
}
