﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class StoreModel : BaseModel
    {
        public int StoreID { get; set; }
        public int PortalID { get; set; }

        public int? AccountID { get; set; }
        public int? DisplayOrder { get; set; }

        public string Name { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string ContactName { get; set; }
        public string ImageFile { get; set; }
        public string Custom1 { get; set; }
        public string Custom2 { get; set; }
        public string Custom3 { get; set; }

        public string ImageLargePath { get; set; }
        public string ImageMediumPath { get; set; }
        public string ImageSmallPath { get; set; }
        public string ImageSmallThumbnailPath { get; set; }
        public string ImageThumbnailPath { get; set; }

        public bool ActiveInd { get; set; }

        public string AreaCode { get; set; }
        public int Radius { get; set; }
        public string MapQuestURL { get; set; }
    }
}
