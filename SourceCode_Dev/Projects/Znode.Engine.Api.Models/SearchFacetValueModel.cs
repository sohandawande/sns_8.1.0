﻿namespace Znode.Engine.Api.Models
{
	public class SearchFacetValueModel : BaseModel
	{
		public string AttributeValue { get; set; }
		public long FacetCount { set; get; }
	}
}