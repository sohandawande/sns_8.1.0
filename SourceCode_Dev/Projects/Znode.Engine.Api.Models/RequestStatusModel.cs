﻿
namespace Znode.Engine.Api.Models
{
    public class RequestStatusModel:BaseModel
    {
        public int RequestStatusID { get; set; }
        public string Name { get; set; }
        public string CustomerNotification { get; set; }
        public string AdminNotification { get; set; }
        public bool? IsEnabled { get; set; }

        public enum RequestStatus
        {
            Authorized = 2,
            Returned_Refundable = 3,
            Void = 4
        }
    }
}
