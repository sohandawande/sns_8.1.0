﻿using System;

namespace Znode.Engine.Api.Models
{
	public class AuditStatusModel : BaseModel
	{
		public int AuditStatusId { get; set; }
		public DateTime CreateDate { get; set; }
		public string CreatedBy { get; set; }
		public string Description { get; set; }
		public DateTime? UpdateDate { get; set; }
		public string UpdatedBy { get; set; }

		public AuditStatusModel Sample()
		{
			return new AuditStatusModel
			{
				AuditStatusId = 2,
				CreateDate = Convert.ToDateTime("2013-04-29 10:49:32.657"),
				CreatedBy = "Username",
				Description = "New",
				UpdateDate = null,
				UpdatedBy = null
			};
		}
	}
}
