﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
	public class AuditListModel : BaseListModel
	{
		public Collection<AuditModel> Audits { get; set; }

		public AuditListModel()
		{
			Audits = new Collection<AuditModel>();
		}
	}
}
