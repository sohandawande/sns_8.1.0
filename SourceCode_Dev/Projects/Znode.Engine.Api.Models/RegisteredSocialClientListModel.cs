﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
    public class RegisteredSocialClientListModel : BaseListModel
    {
        public Collection<RegisteredSocialClientModel> SocialClients { get; set; }

        public RegisteredSocialClientListModel()
		{
            SocialClients = new Collection<RegisteredSocialClientModel>();
		}
    }
}
