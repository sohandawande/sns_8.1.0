﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class YouMayLikeAlsoModel :BaseModel
    {
        public string ProductName { get; set; }
        public string CrossSellProducts { get; set; }

        public int ProductId { get; set; }
    }
}
