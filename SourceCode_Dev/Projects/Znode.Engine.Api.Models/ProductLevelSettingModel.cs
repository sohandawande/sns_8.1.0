﻿
namespace Znode.Engine.Api.Models
{
    public class ProductLevelSettingModel : BaseModel
    {
        
        public ProductLevelSettingModel()
        {

        }

        public string Name { get; set; }
        public string SKU { get; set; }
        public string ProductNumber { get; set; }

        public int ProductId { get; set; }
        public int CatalogId { get; set; }
        public int categoryId { get; set; }
        public int ProductTypeId { get; set; }
        public int ManufacturerId { get; set; }

        public double? Boost { get; set; }
    }
}
