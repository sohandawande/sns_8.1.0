﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class PRFTERPAccountDetailsModel:BaseModel
    {
        public string DebtorCode { get; set; }
        public string CustomerType { get; set; }
        public string CustomerTypeDescription { get; set; }
        public string CompanyCodeDebtorAccountID { get; set; }
        public string CustomerStatus { get; set; }
        public string CustomerStatusDescription { get; set; }
        public string PaymentCondition { get; set; }
        public string PaymentConditionDescription { get; set; }
        public string Terms { get; set; }
        public DateTime ModifiedDate { get; set; }
        public AddressModel BillingAddress { get; set; }
        public Collection<AddressModel> ShippingAddresses { get; set; }

        public PRFTERPAccountDetailsModel()
        {
            ModifiedDate = DateTime.Now;
            BillingAddress = new AddressModel();
            ShippingAddresses = new Collection<AddressModel>();
        }

        public PRFTERPAccountDetailsModel Sample()
        {
            return new PRFTERPAccountDetailsModel
            {
                DebtorCode = "                1001",
                CustomerType = "Dymonic® 100 Cartridges",
                CustomerTypeDescription = "DY1T",
                CompanyCodeDebtorAccountID = "",
                CustomerStatus = "A",
                CustomerStatusDescription = "Active",
                PaymentCondition = "30",
                PaymentConditionDescription = "NET 30 DAYS",
                Terms = "30",
                ModifiedDate = new DateTime(),
                BillingAddress = new AddressModel().Sample()
            };
        }
    }
}
