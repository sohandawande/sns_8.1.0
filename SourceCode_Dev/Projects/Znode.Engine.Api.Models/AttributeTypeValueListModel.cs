﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
    public class AttributeTypeValueListModel : BaseListModel
    {
        public Collection<AttributeTypeValueModel> AttributeTypeValueList { get; set; }

        public AttributeTypeValueListModel()
		{
            AttributeTypeValueList = new Collection<AttributeTypeValueModel>();
		}
    }
}
