﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
	public class AddOnModel : BaseModel
	{
		public int? AccountId { get; set; }
		public int AddOnId { get; set; }
		public bool AllowBackOrder { get; set; }
		public string BackOrderMessage { get; set; }
		public string Description { get; set; }
		public int DisplayOrder { get; set; }
		public string DisplayType { get; set; } 
		public string ExternalApiId { get; set; }
		public int? ExternalId { get; set; }
		public string InStockMessage { get; set; }
		public bool IsOptional { get; set; }
		public int LocaleId { get; set; }
		public string Name { get; set; }
		public string OutOfStockMessage { get; set; }
		public int? PortalId { get; set; }
		public int ProductId { get; set; }
		public string PromptMessage { get; set; }
		public string Title { get; set; }
		public bool TrackInventory { get; set; }
        public int ProductAddOnId { get; set; }
		public Collection<AddOnValueModel> AddOnValues { get; set; }
        public bool IsAssociatedAddOnValue { get; set; }
        public string AddOnValueName { get; set; }
        public string UserName { get; set; }
		public AddOnModel()
		{
			AddOnValues = new Collection<AddOnValueModel>();
		}

		public AddOnModel Sample()
		{
			return new AddOnModel
			{
				AccountId = null,
				AddOnId = 1,
				AllowBackOrder = true,
				BackOrderMessage = "On back order",
				Description = "",
				DisplayOrder = 1,
				DisplayType = "DropDownList",
				ExternalId = null,
				ExternalApiId = "",
				InStockMessage = "In stock",
				IsOptional = false,
				LocaleId = 43,
				Name = "Vase",
				OutOfStockMessage = "Out of stock",
				PortalId = null,
				ProductId = 0,
				PromptMessage = "",
				Title = "Vase",
				TrackInventory = true
			};
		}
	}
}
