﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
    /// <summary>
    /// List View Model for Tax Rule
    /// </summary>
	public class TaxRuleListModel : BaseListModel
	{
		public Collection<TaxRuleModel> TaxRules { get; set; }

        /// <summary>
        /// Constructor for Tax Rule List Model
        /// </summary>
		public TaxRuleListModel()
		{
			TaxRules = new Collection<TaxRuleModel>();
		}
	}
}