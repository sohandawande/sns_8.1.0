﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
	public class CountryModel : BaseModel
	{
		public string Code { get; set; }
		public int DisplayOrder { get; set; }
		public bool IsActive { get; set; }
		public string Name { get; set; }
		public Collection<StateModel> States { get; set; }

		public CountryModel()
		{
			States = new Collection<StateModel>();
		}

		public CountryModel Sample()
		{
			return new CountryModel
			{
				Code = "US",
				DisplayOrder = 0,
				IsActive = true,
				Name = "UNITED STATES"
			};
		}
	}
}