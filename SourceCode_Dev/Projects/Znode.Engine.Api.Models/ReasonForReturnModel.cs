﻿
namespace Znode.Engine.Api.Models
{
    public class ReasonForReturnModel : BaseModel
    {    
        public ReasonForReturnModel()
        {
            ReasonForReturnId = 1;
            Name = "";
            IsEnabled = true;
        }

        public string Name { get; set; }

        public int ReasonForReturnId { get; set; }

        public bool IsEnabled { get; set; }
    }
}
