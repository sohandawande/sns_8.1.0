﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class PRFTOELineHistoryListModel
    {
        public Collection<PRFTOELineHistoryModel> OELineHistory  { get;set;}

        public PRFTOELineHistoryListModel()
        {
            OELineHistory = new Collection<PRFTOELineHistoryModel>();
        }
    }
}
