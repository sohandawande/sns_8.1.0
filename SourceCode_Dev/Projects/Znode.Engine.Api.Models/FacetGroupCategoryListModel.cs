﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
    public class FacetGroupCategoryListModel : BaseModel
    {
        public Collection<FacetGroupCategoryModel> FacetGroupCategoryList { get; set; }

         public FacetGroupCategoryListModel()
        {
            FacetGroupCategoryList = new Collection<FacetGroupCategoryModel>();
        }
    }
}
