﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class ReasonForReturnListModel : BaseListModel
    {
        public Collection<ReasonForReturnModel> ReasonsForReturn { get; set; }

        /// <summary>
        /// Constructor for RMAConfigurationListModel
        /// </summary>
        public ReasonForReturnListModel()
		{
            ReasonsForReturn = new Collection<ReasonForReturnModel>();
		}
    }
}
