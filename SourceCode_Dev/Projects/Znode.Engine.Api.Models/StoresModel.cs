﻿using System;
using System.Collections.Generic;

namespace Znode.Engine.Api.Models
{
    /// <summary>
    /// This model is used for setting up the Stores
    /// </summary>
    public class StoresModel : BaseModel
    {
        public StoresModel()
        { }

        public List<Tuple<string, int, bool>> Stores { get; set; }
    }
}
