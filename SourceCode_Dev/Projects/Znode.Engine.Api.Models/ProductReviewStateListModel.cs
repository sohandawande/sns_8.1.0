﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class ProductReviewStateListModel:BaseListModel
    {
        public Collection<ProductReviewStateModel> ProductReviewStates { get; set; }

        public ProductReviewStateListModel()
        {
            ProductReviewStates = new Collection<ProductReviewStateModel>();
        }
    }
}
