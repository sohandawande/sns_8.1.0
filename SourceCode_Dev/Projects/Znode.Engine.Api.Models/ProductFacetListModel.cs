﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
    public class ProductFacetListModel: BaseListModel
	{
        public ProductFacetListModel()
		{
            FacetGroups = new Collection<FacetGroupModel>();
		}
        public Collection<FacetGroupModel> FacetGroups { get; set; }
    }
}
