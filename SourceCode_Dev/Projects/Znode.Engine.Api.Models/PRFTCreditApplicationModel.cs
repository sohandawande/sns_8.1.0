﻿using System;
using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
    public class PRFTCreditApplicationModel : BaseModel
    {
        public int CreditApplicationID { get; set; }
        public string BusinessName { get; set; }
        public string BusinessStreet { get; set; }
        public string BusinessStreet1 { get; set; }
        public string BusinessCity { get; set; }
        public string BusinessStateCode { get; set; }
        public string BusinessPostalCode { get; set; }
        public string BusinessCountryCode { get; set; }
        public string BusinessPhoneNumber { get; set; }
        public string BusinessFaxNumber { get; set; }
        public string BusinessEmail { get; set; }
        public int? BusinessYearEstablished { get; set; }
        public string BusinessType { get; set; }
        public bool? IsTaxExempt { get; set; }
        public string TaxExemptCertificate { get; set; }
        public string BillingStreet { get; set; }
        public string BillingStreet1 { get; set; }
        public string BillingCity { get; set; }
        public string BillingStateCode { get; set; }
        public string BillingPostalCode { get; set; }
        public string BillingCountryCode { get; set; }
        public string BankName { get; set; }
        public string BankAddress { get; set; }
        public string BankPhoneNumber { get; set; }
        public string BankAccountNumber { get; set; }
        public DateTime? RequestDate { get; set; }
        public bool? IsCreditApproved { get; set; }
        public int? CreditDays { get; set; }
        public DateTime? CreditApprovedDate { get; set; }
        public string Custom1 { get; set; }
        public string Custom2 { get; set; }
        public string Custom3 { get; set; }
        public string Custom4 { get; set; }
        public string Custom5 { get; set; }
        public Collection<PRFTTradeReferencesModel> TradeReferences { get; set; }

        public PRFTCreditApplicationModel()
        {
            IsTaxExempt = false;
            RequestDate = DateTime.Now;
            IsCreditApproved = false;
            CreditDays = 0;
            CreditApprovedDate = DateTime.Now;
            TradeReferences = new Collection<PRFTTradeReferencesModel>();
        }

        public PRFTCreditApplicationModel Sample()
        {
            return new PRFTCreditApplicationModel
            {
                CreditApplicationID = 1,
                BusinessName = "Your Company",
                BusinessStreet = "123 Fake Street",
                BusinessStreet1 = "",
                BusinessCity = "Fake City",
                BusinessStateCode = "SD",
                BusinessPostalCode = "12345",
                BusinessCountryCode = "US",
                BusinessPhoneNumber = "1-888-Your-PhoneN",
                BusinessFaxNumber="1-888-Your-FaxNu",
                BusinessEmail = "znodeadmin1@test.com",
                
                BusinessYearEstablished = 1800, 
                BusinessType  = "Online Store",
                IsTaxExempt =false,
                TaxExemptCertificate =string.Empty,
                BillingStreet = "123 Fake Street",
                BillingStreet1 ="",
                BillingCity ="Fake City",
                BillingStateCode = "SD",
                BillingPostalCode="12345",
                BillingCountryCode="US",
                BankName = "",
                BankAddress = "Bank Address",
                BankPhoneNumber = "1-888-Bank-PhoneN",
                BankAccountNumber = "Account Number",
                RequestDate = DateTime.Now,
                IsCreditApproved =false,
                CreditDays=0,
                CreditApprovedDate =null,
                Custom1 = null,
                Custom2 = null,
                Custom3 = null,
                Custom4 = null,
                Custom5 = null
            };
        }
    }
}
