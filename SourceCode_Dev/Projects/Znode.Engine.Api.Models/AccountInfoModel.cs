﻿
namespace Znode.Engine.Api.Models
{
    public class AccountInfoModel : BaseModel
    {
        public int AccountId { get; set; }
        public string RoleName { get; set; }
        public int PortalId { get; set; }
    }
}
