﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
    public class EmailTemplateListModel: BaseListModel
	{
		public Collection<EmailTemplateModel> EmailTemplates { get; set; }

        public EmailTemplateListModel()
		{
            EmailTemplates = new Collection<EmailTemplateModel>();
		}
	}
}
