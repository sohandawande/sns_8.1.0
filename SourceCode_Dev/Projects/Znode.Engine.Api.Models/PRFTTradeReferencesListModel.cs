﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
    public class PRFTTradeReferencesListModel : BaseListModel
    {
        public Collection<PRFTTradeReferencesModel> TradeReferences { get; set; }

        public PRFTTradeReferencesListModel()
        {
            TradeReferences = new Collection<PRFTTradeReferencesModel>();
        }
    }
}
