﻿namespace Znode.Engine.Api.Models
{
	public class ShippingRuleModel : BaseModel
	{
		public decimal BaseCost { get; set; }
		public string ClassName { get; set; }
		public string Custom1 { get; set; }
		public string Custom2 { get; set; }
		public string Custom3 { get; set; }
		public string ExternalId { get; set; }
        public string errorMessage { get; set; }
		public decimal? LowerLimit { get; set; }
		public decimal PerItemCost { get; set; }
		public ShippingOptionModel ShippingOption { get; set; }
		public int ShippingOptionId { get; set; }
		public int ShippingRuleId { get; set; }
		public ShippingRuleTypeModel ShippingRuleType { get; set; }
		public int ShippingRuleTypeId { get; set; }
		public decimal? UpperLimit { get; set; }
		
		public ShippingRuleModel Sample()
		{
			return new ShippingRuleModel
			{
				BaseCost = 5.00m,
				ClassName = null,
				Custom1 = "",
				Custom2 = "",
				Custom3 = "",
				ExternalId = null,
				LowerLimit = 1.00m,
				PerItemCost = 2.25m,
				ShippingOptionId = 9,
				ShippingRuleId = 1,
				ShippingRuleTypeId = 0,
				UpperLimit = 2.00m
			};
		}
	}
}
