﻿namespace Znode.Engine.Api.Models
{
	public class CaseStatusModel : BaseModel
	{
		public int CaseStatusId { get; set; }
		public int DisplayOrder { get; set; }
		public string Name { get; set; }

		public CaseStatusModel Sample()
		{
			return new CaseStatusModel
			{
				CaseStatusId = 1,
				DisplayOrder = 1,
				Name = "Pending"
			};
		}
	}
}
