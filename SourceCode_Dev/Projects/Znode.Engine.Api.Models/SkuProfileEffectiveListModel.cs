﻿using System.Collections.ObjectModel;

namespace Znode.Engine.Api.Models
{
    /// <summary>
    /// Model of Sku Profile Effective List.
    /// </summary>
    public class SkuProfileEffectiveListModel : BaseListModel
    {
        public Collection<SkuProfileEffectiveModel> SkuProfileEffectives { get; set; }

        #region Constructor
        public SkuProfileEffectiveListModel()
        {
            SkuProfileEffectives = new Collection<SkuProfileEffectiveModel>();
        } 
        #endregion
    }
}
