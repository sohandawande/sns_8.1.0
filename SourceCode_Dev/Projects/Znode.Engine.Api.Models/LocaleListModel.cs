﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class LocaleListModel : BaseListModel
    {
        public Collection<LocaleModel> Locales { get; set; }

        public LocaleListModel()
        {
            Locales = new Collection<LocaleModel>();
        }
    }
}
