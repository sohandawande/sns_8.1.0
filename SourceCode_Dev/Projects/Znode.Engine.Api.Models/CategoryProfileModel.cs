﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class CategoryProfileModel : BaseModel
    {
        public int CategoryProfileID { get; set; }
        public int CategoryID { get; set; }
        public int ProfileID { get; set; }
        public DateTime EffectiveDate { get; set; }
        public string ProfileName { get; set; }

        public CategoryProfileModel Sample()
        {
            return new CategoryProfileModel
            {
                CategoryProfileID = 1,
                CategoryID = 91,
                ProfileID = 1,
                EffectiveDate = Convert.ToDateTime("2013-04-29 10:49:32.657"),
            };
        }
    }
}
