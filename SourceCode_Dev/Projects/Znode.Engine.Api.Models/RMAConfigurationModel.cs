﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    public class RMAConfigurationModel : BaseModel
    {
        /// <summary>
        /// Constructor for RMAConfigurationModel
        /// </summary>
        public RMAConfigurationModel()
        {
            DisplayName = null;
            EmailId = null;
            Address = null;
            ShippingDirections = null;
            GiftCardNotification = null;

            RMAConfigId = 1;
            MaxDays = null;
            GiftCardExpirationPeriod = null;

            EnableEmailNotification = true;
        }

        public string DisplayName { get; set; }
        public string EmailId { get; set; }
        public string Address { get; set; }
        public string ShippingDirections { get; set; }
        public string GiftCardNotification { get; set; }

        public int RMAConfigId { get; set; }
        public int? MaxDays { get; set; }
        public int? GiftCardExpirationPeriod { get; set; }

        public bool? EnableEmailNotification { get; set; }
    }
}
