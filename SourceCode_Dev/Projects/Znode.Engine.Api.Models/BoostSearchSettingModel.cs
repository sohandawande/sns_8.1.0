﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
   public class BoostSearchSettingModel:BaseModel
    {
        public Dictionary<int, double> BoostCollection { get; set; }
        public string Name { get; set; }

        public BoostSearchSettingModel()
        {
            BoostCollection = new Dictionary<int, double>();
        }
    }
}
