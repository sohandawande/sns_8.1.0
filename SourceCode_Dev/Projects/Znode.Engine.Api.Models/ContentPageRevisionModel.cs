﻿using System;

namespace Znode.Engine.Api.Models
{
    /// <summary>
    /// Model for content page revision.
    /// </summary>
    public class ContentPageRevisionModel : BaseModel
    {
        public int RevisionID { get; set; }
        public int ContentPageID { get; set; }

        public string UpdatedUser { get; set; }
        public string Description { get; set; }
        public string HtmlText { get; set; }
        public string OldHtml { get; set; }
        public string ContentPageName { get; set; }

        public DateTime UpdateDate { get; set; }
    }
}
