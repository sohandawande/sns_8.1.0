﻿namespace Znode.Engine.Api.Models
{
	public class DomainModel : BaseModel
	{
		public string ApiKey { get; set; }
		public int DomainId { get; set; }
		public string DomainName { get; set; }
		public bool IsActive { get; set; }
		public int PortalId { get; set; }
        public string OldDomainName { get; set; }
		public DomainModel Sample()
		{
			return new DomainModel
			{
				ApiKey = "5D2B4C5E-D8B3-4488-904D-64094762E136",
				DomainId = 1,
				DomainName = "localhost",
				IsActive = true,
				PortalId = 1
			};
		}
	}
}
