﻿namespace Znode.Engine.Api.Models
{
	public class GatewayTokenModel
	{
		public string AuthorizationCode { get; set; }
		public string CardExpiration { get; set; }
		public string CardSecurityCode { get; set; }
		public string TransactionId { get; set; }

		public GatewayTokenModel Sample()
		{
			return new GatewayTokenModel
			{

			};
		}
	}
}
