﻿using System.Collections.Generic;

namespace Znode.Engine.Api.Models
{
    public class RejectProductModel : BaseModel
    {
        public string VendorName { get; set; }
        public Dictionary<string,string> RejectionResons { get; set; }
        public string ProductName { get; set; }
        public string DetailReason { get; set; }
        public string SelectedProductIds { get; set; }
        public string SelectedRejectionReason { get; set; }
        public string UserName { get; set; }
    }
}
