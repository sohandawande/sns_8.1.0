﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Models
{
    /// <summary>
    /// List Model For Customer
    /// </summary>
    public class CustomerListModel : BaseListModel
    {
        public Collection<CustomerModel> CustomerList { get; set; }

        /// <summary>
        /// Constructor for CustomerListModel
        /// </summary>
        public CustomerListModel()
        {
            CustomerList = new Collection<CustomerModel>();
        }
    }
}
