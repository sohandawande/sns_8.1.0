﻿
using System.Collections.ObjectModel;
namespace Znode.Engine.Api.Models
{
    public class FacetGroupModel : BaseModel
    {
        public FacetGroupModel()
        {
            FacetGroupCategories = new Collection<FacetGroupCategoryModel>();
            FacetGroupFacets = new Collection<FacetModel>();
        }
        public int FacetGroupID { get; set; }
        public string FacetGroupLabel { get; set; }
        public int? ControlTypeID { get; set; }
        public int? CatalogID { get; set; }
        public int? DisplayOrder { get; set; }
        public Collection<FacetGroupCategoryModel> FacetGroupCategories { get; set; }
        public Collection<FacetModel> FacetGroupFacets { get; set; }
        public string FacetName { get; set; }
    }
}
