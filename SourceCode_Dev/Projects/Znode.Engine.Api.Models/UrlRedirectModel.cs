﻿using System;

namespace Znode.Engine.Api.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class UrlRedirectModel : BaseModel
    {
        public bool IsActive { get; set; }
        public string NewUrl { get; set; }
        public string OldUrl { get; set; }
        public int UrlRedirectId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public UrlRedirectModel Sample()
        {
            return new UrlRedirectModel
            {
              UrlRedirectId = 1,
              OldUrl = "",
              NewUrl = "",
              IsActive = true
            };
        }
    }
}
