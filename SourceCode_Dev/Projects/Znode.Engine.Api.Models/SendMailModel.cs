﻿using System.Net.Mail;
namespace Znode.Engine.Api.Models
{
    public class SendMailModel
    {
        public string EmailId { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public AlternateView Attachment { get; set; }
    }
}
