﻿#region Using directives

using System;

#endregion

namespace ZNode.Libraries.DataAccess.Entities
{	
	///<summary>
	/// An object representation of the 'ZNodeTrackingOutbound' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class TrackingOutbound : TrackingOutboundBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="TrackingOutbound"/> instance.
		///</summary>
		public TrackingOutbound():base(){}	
		
		#endregion
	}
}
