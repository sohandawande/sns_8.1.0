﻿#region Using directives

using System;

#endregion

namespace ZNode.Libraries.DataAccess.Entities
{	
	///<summary>
	/// An object representation of the 'ZNodeLuceneGlobalProductCatalogBoost' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class LuceneGlobalProductCatalogBoost : LuceneGlobalProductCatalogBoostBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="LuceneGlobalProductCatalogBoost"/> instance.
		///</summary>
		public LuceneGlobalProductCatalogBoost():base(){}	
		
		#endregion
	}
}
