﻿#region Using directives

using System;

#endregion

namespace ZNode.Libraries.DataAccess.Entities
{	
	///<summary>
	/// An object representation of the 'ZNodeSupplierType' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class SupplierType : SupplierTypeBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="SupplierType"/> instance.
		///</summary>
		public SupplierType():base(){}	
		
		#endregion
	}
}
