﻿using System;
using System.ComponentModel;

namespace ZNode.Libraries.DataAccess.Entities
{
	/// <summary>
	///		The data structure representation of the 'ZNodeSourceModificationAudit' table via interface.
	/// </summary>
	/// <remarks>
	/// 	This struct is generated by a tool and should never be modified.
	/// </remarks>
	public interface ISourceModificationAudit 
	{
		/// <summary>			
		/// Id : 
		/// </summary>
		/// <remarks>Member of the primary key of the underlying table "ZNodeSourceModificationAudit"</remarks>
		System.Int64 Id { get; set; }
				
		
		
		/// <summary>
		/// SourceTypeId : 
		/// </summary>
		System.Int32  SourceTypeId  { get; set; }
		
		/// <summary>
		/// SourceId : 
		/// </summary>
		System.Int64  SourceId  { get; set; }
		
		/// <summary>
		/// ExternalId : 
		/// </summary>
		System.String  ExternalId  { get; set; }
		
		/// <summary>
		/// ModificationTypeId : 
		/// </summary>
		System.Int32  ModificationTypeId  { get; set; }
		
		/// <summary>
		/// StatusId : 
		/// </summary>
		System.Int32  StatusId  { get; set; }
		
		/// <summary>
		/// ProcessedDate : 
		/// </summary>
		System.DateTime?  ProcessedDate  { get; set; }
		
		/// <summary>
		/// CreatedDate : 
		/// </summary>
		System.DateTime  CreatedDate  { get; set; }
		
		/// <summary>
		/// CreatedBy : 
		/// </summary>
		System.String  CreatedBy  { get; set; }
			
		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		System.Object Clone();
		
		#region Data Properties

		#endregion Data Properties

	}
}


