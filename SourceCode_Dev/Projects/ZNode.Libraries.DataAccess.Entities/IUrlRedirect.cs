﻿using System;
using System.ComponentModel;

namespace ZNode.Libraries.DataAccess.Entities
{
	/// <summary>
	///		The data structure representation of the 'ZNodeUrlRedirect' table via interface.
	/// </summary>
	/// <remarks>
	/// 	This struct is generated by a tool and should never be modified.
	/// </remarks>
	public interface IUrlRedirect 
	{
		/// <summary>			
		/// UrlRedirectID : 
		/// </summary>
		/// <remarks>Member of the primary key of the underlying table "ZNodeUrlRedirect"</remarks>
		System.Int32 UrlRedirectID { get; set; }
				
		
		
		/// <summary>
		/// OldUrl : 
		/// </summary>
		System.String  OldUrl  { get; set; }
		
		/// <summary>
		/// NewUrl : 
		/// </summary>
		System.String  NewUrl  { get; set; }
		
		/// <summary>
		/// IsActive : 
		/// </summary>
		System.Boolean  IsActive  { get; set; }
			
		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		System.Object Clone();
		
		#region Data Properties

		#endregion Data Properties

	}
}


