﻿using System;
using System.ComponentModel;

namespace ZNode.Libraries.DataAccess.Entities
{
	/// <summary>
	///		The data structure representation of the 'ZNodeTheme' table via interface.
	/// </summary>
	/// <remarks>
	/// 	This struct is generated by a tool and should never be modified.
	/// </remarks>
	public interface ITheme 
	{
		/// <summary>			
		/// ThemeID : 
		/// </summary>
		/// <remarks>Member of the primary key of the underlying table "ZNodeTheme"</remarks>
		System.Int32 ThemeID { get; set; }
				
		
		
		/// <summary>
		/// Name : 
		/// </summary>
		System.String  Name  { get; set; }
			
		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		System.Object Clone();
		
		#region Data Properties


		/// <summary>
		///	Holds a collection of entity objects
		///	which are related to this object through the relation _contentPageThemeID
		/// </summary>	
		TList<ContentPage> ContentPageCollection {  get;  set;}	


		/// <summary>
		///	Holds a collection of entity objects
		///	which are related to this object through the relation _portalCatalogThemeID
		/// </summary>	
		TList<PortalCatalog> PortalCatalogCollection {  get;  set;}	


		/// <summary>
		///	Holds a collection of entity objects
		///	which are related to this object through the relation _productCategoryThemeID
		/// </summary>	
		TList<ProductCategory> ProductCategoryCollection {  get;  set;}	


		/// <summary>
		///	Holds a collection of entity objects
		///	which are related to this object through the relation _cSSThemeID
		/// </summary>	
		TList<CSS> CSSCollection {  get;  set;}	


		/// <summary>
		///	Holds a collection of entity objects
		///	which are related to this object through the relation _categoryNodeThemeID
		/// </summary>	
		TList<CategoryNode> CategoryNodeCollection {  get;  set;}	

		#endregion Data Properties

	}
}


