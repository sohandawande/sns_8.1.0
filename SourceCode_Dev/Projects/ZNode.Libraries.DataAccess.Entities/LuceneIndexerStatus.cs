﻿#region Using directives

using System;

#endregion

namespace ZNode.Libraries.DataAccess.Entities
{	
	///<summary>
	/// An object representation of the 'ZNodeLuceneIndexerStatus' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class LuceneIndexerStatus : LuceneIndexerStatusBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="LuceneIndexerStatus"/> instance.
		///</summary>
		public LuceneIndexerStatus():base(){}	
		
		#endregion
	}
}
