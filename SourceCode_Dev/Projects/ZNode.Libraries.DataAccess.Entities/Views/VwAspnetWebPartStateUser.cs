﻿#region Using directives

using System;

#endregion

namespace ZNode.Libraries.DataAccess.Entities
{	
	///<summary>
	/// An object representation of the 'vw_aspnet_WebPartState_User' view. [No description found in the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class VwAspnetWebPartStateUser : VwAspnetWebPartStateUserBase
	{
		#region Constructors

		///<summary>
		/// Creates a new <see cref="VwAspnetWebPartStateUser"/> instance.
		///</summary>
		public VwAspnetWebPartStateUser():base(){}	
		
		#endregion
	}
}
