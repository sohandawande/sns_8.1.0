﻿#region Using directives

using System;

#endregion

namespace ZNode.Libraries.DataAccess.Entities
{	
	///<summary>
	/// An object representation of the 'vw_aspnet_WebPartState_Paths' view. [No description found in the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class VwAspnetWebPartStatePaths : VwAspnetWebPartStatePathsBase
	{
		#region Constructors

		///<summary>
		/// Creates a new <see cref="VwAspnetWebPartStatePaths"/> instance.
		///</summary>
		public VwAspnetWebPartStatePaths():base(){}	
		
		#endregion
	}
}
