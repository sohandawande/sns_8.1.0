﻿#region Using directives

using System;

#endregion

namespace ZNode.Libraries.DataAccess.Entities
{	
	///<summary>
	/// An object representation of the 'vw_aspnet_MembershipUsers' view. [No description found in the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class VwAspnetMembershipUsers : VwAspnetMembershipUsersBase
	{
		#region Constructors

		///<summary>
		/// Creates a new <see cref="VwAspnetMembershipUsers"/> instance.
		///</summary>
		public VwAspnetMembershipUsers():base(){}	
		
		#endregion
	}
}
