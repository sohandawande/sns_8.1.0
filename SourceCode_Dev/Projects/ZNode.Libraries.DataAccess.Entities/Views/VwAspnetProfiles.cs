﻿#region Using directives

using System;

#endregion

namespace ZNode.Libraries.DataAccess.Entities
{	
	///<summary>
	/// An object representation of the 'vw_aspnet_Profiles' view. [No description found in the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class VwAspnetProfiles : VwAspnetProfilesBase
	{
		#region Constructors

		///<summary>
		/// Creates a new <see cref="VwAspnetProfiles"/> instance.
		///</summary>
		public VwAspnetProfiles():base(){}	
		
		#endregion
	}
}
