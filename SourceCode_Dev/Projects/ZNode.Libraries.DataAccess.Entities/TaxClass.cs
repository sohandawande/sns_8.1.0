﻿#region Using directives

using System;

#endregion

namespace ZNode.Libraries.DataAccess.Entities
{	
	///<summary>
	/// An object representation of the 'ZNodeTaxClass' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class TaxClass : TaxClassBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="TaxClass"/> instance.
		///</summary>
		public TaxClass():base(){}	
		
		#endregion
	}
}
