﻿#region Using directives

using System;

#endregion

namespace ZNode.Libraries.DataAccess.Entities
{	
	///<summary>
	/// An object representation of the 'ZNodeReferralCommission' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class ReferralCommission : ReferralCommissionBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="ReferralCommission"/> instance.
		///</summary>
		public ReferralCommission():base(){}	
		
		#endregion
	}
}
