﻿#region Using directives

using System;

#endregion

namespace ZNode.Libraries.DataAccess.Entities
{	
	///<summary>
	/// An object representation of the 'ZNodeMasterPage' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class MasterPage : MasterPageBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="MasterPage"/> instance.
		///</summary>
		public MasterPage():base(){}	
		
		#endregion
	}
}
