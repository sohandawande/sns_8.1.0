﻿using System;
using System.ComponentModel;

namespace ZNode.Libraries.DataAccess.Entities
{
	/// <summary>
	///		The data structure representation of the 'ZNodeOrderProcessingType' table via interface.
	/// </summary>
	/// <remarks>
	/// 	This struct is generated by a tool and should never be modified.
	/// </remarks>
	public interface IOrderProcessingType 
	{
		/// <summary>			
		/// OrderProcessingTypeID : 
		/// </summary>
		/// <remarks>Member of the primary key of the underlying table "ZNodeOrderProcessingType"</remarks>
		System.Int32 OrderProcessingTypeID { get; set; }
				
		
		
		/// <summary>
		/// Description : 
		/// </summary>
		System.String  Description  { get; set; }
		
		/// <summary>
		/// ClassID : 
		/// </summary>
		System.String  ClassID  { get; set; }
			
		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		System.Object Clone();
		
		#region Data Properties

		#endregion Data Properties

	}
}


