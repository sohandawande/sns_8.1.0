﻿#region Using directives

using System;

#endregion

namespace ZNode.Libraries.DataAccess.Entities
{	
	///<summary>
	/// An object representation of the 'ZNodeProduct' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class Product : ProductBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="Product"/> instance.
		///</summary>
        public Product()
            : base()
        {
            Attributes = new TList<ProductAttribute>();
            ProductFrquentlyBoughtTogetherCollection = new TList<ProductCrossSell>();
            ProductYouMayAlsoLikeCollection = new TList<ProductCrossSell>();
        }	
		
		#endregion

        public TList<ProductAttribute> Attributes { get; set; }
        public decimal? PromotionPrice { get; set; }
        public SKU SelectedSku { get; set; }
        public decimal? TieredPrice { get; set; }

        public TList<ProductCrossSell> ProductFrquentlyBoughtTogetherCollection { get; set; }

        public TList<ProductCrossSell> ProductYouMayAlsoLikeCollection { get; set; }
	}
}
