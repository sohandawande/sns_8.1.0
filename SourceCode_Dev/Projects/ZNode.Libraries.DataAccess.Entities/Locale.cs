﻿#region Using directives

using System;

#endregion

namespace ZNode.Libraries.DataAccess.Entities
{	
	///<summary>
	/// An object representation of the 'ZNodeLocale' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class Locale : LocaleBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="Locale"/> instance.
		///</summary>
		public Locale():base(){}	
		
		#endregion
	}
}
