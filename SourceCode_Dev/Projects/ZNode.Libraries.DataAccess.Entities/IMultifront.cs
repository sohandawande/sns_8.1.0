﻿using System;
using System.ComponentModel;

namespace ZNode.Libraries.DataAccess.Entities
{
	/// <summary>
	///		The data structure representation of the 'ZNodeMultifront' table via interface.
	/// </summary>
	/// <remarks>
	/// 	This struct is generated by a tool and should never be modified.
	/// </remarks>
	public interface IMultifront 
	{
		/// <summary>			
		/// ID : 
		/// </summary>
		/// <remarks>Member of the primary key of the underlying table "ZNodeMultifront"</remarks>
		System.Int32 ID { get; set; }
				
		/// <summary>
		/// keep a copy of the original so it can be used for editable primary keys.
		/// </summary>
		System.Int32 OriginalID { get; set; }
			
		
		
		/// <summary>
		/// MajorVersion : 
		/// </summary>
		System.Int32  MajorVersion  { get; set; }
		
		/// <summary>
		/// MinorVersion : 
		/// </summary>
		System.Int32  MinorVersion  { get; set; }
		
		/// <summary>
		/// Build : 
		/// </summary>
		System.Int32  Build  { get; set; }
		
		/// <summary>
		/// LVK : 
		/// </summary>
		System.String  LVK  { get; set; }
			
		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		System.Object Clone();
		
		#region Data Properties

		#endregion Data Properties

	}
}


