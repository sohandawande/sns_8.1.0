﻿#region Using directives

using System;

#endregion

namespace ZNode.Libraries.DataAccess.Entities
{	
	///<summary>
	/// An object representation of the 'ZNodePaymentTokenAuthorize' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class PaymentTokenAuthorize : PaymentTokenAuthorizeBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="PaymentTokenAuthorize"/> instance.
		///</summary>
		public PaymentTokenAuthorize():base(){}	
		
		#endregion
	}
}
