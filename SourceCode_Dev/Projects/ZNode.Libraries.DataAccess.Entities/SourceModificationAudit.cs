﻿#region Using directives

using System;

#endregion

namespace ZNode.Libraries.DataAccess.Entities
{	
	///<summary>
	/// An object representation of the 'ZNodeSourceModificationAudit' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class SourceModificationAudit : SourceModificationAuditBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="SourceModificationAudit"/> instance.
		///</summary>
		public SourceModificationAudit():base(){}	
		
		#endregion

        public Product Product { get; set; }
        public Category Category { get; set; }
        public Catalog Catalog { get; set; }
        public Portal Portal { get; set; }
	}
}
