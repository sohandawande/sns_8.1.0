﻿#region Using directives

using System;

#endregion

namespace ZNode.Libraries.DataAccess.Entities
{	
	///<summary>
	/// An object representation of the 'ZNodeDigitalAsset' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class DigitalAsset : DigitalAssetBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="DigitalAsset"/> instance.
		///</summary>
		public DigitalAsset():base(){}	
		
		#endregion
	}
}
