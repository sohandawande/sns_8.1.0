﻿#region Using directives

using System;

#endregion

namespace ZNode.Libraries.DataAccess.Entities
{	
	///<summary>
	/// An object representation of the 'ZNodeAddress' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class Address : AddressBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="Address"/> instance.
		///</summary>
		public Address():base(){}	
		
		#endregion

        ///<summary>
        /// Returns a String that represents the current object.
        ///</summary>
        public override string ToString()
        {
            return string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "{0} {1}{2}{3}{4}{5}, {6} {7} {8}<BR>{9}",
                (string.IsNullOrEmpty(this.FirstName) ? string.Empty : this.FirstName.ToString()),
                (string.IsNullOrEmpty(this.LastName) ? string.Empty : this.LastName.ToString()),
                (string.IsNullOrEmpty(this.CompanyName) ? string.Empty : "<BR>" + this.CompanyName.ToString()),
                (string.IsNullOrEmpty(this.Street) ? string.Empty : "<BR>" + this.Street.ToString()),
                (string.IsNullOrEmpty(this.Street1) ? string.Empty : "<BR>" + this.Street1.ToString()),
                (string.IsNullOrEmpty(this.City) ? string.Empty : "<BR>" + this.City.ToString()),
                (string.IsNullOrEmpty(this.StateCode) ? string.Empty : this.StateCode.ToString()),
                this.PostalCode,
                (string.IsNullOrEmpty(this.CountryCode) ? string.Empty : "<BR>" + this.CountryCode.ToString()),
                (string.IsNullOrEmpty(this.PhoneNumber) ? string.Empty : "Ph: " + this.PhoneNumber.ToString()));
        }
	}
}
