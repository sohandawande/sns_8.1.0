﻿#region Using directives

using System;

#endregion

namespace ZNode.Libraries.DataAccess.Entities
{	
	///<summary>
	/// An object representation of the 'ZNodeMessageType' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class MessageType : MessageTypeBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="MessageType"/> instance.
		///</summary>
		public MessageType():base(){}	
		
		#endregion
	}
}
