﻿#region Using directives

using System;
using System.Collections.ObjectModel;

#endregion

namespace ZNode.Libraries.DataAccess.Entities
{	
	///<summary>
	/// An object representation of the 'ZNodeCountry' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class Country : CountryBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="Country"/> instance.
		///</summary>
		public Country():base(){}	
		
		#endregion

        private Collection<State> _states;

        public Collection<State> States
        {
            get { return _states ?? (_states = new Collection<State>()); }
        }
	}
}
