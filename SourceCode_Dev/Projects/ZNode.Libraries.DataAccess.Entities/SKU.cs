﻿#region Using directives

using System;

#endregion

namespace ZNode.Libraries.DataAccess.Entities
{	
	///<summary>
	/// An object representation of the 'ZNodeSKU' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class SKU : SKUBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="SKU"/> instance.
		///</summary>
        public SKU()
            : base()
        {
            CustomerPricing = new TList<CustomerPricing>();
        }	
		
		#endregion

        public decimal? FinalPrice { get; set; }
        public SKUInventory Inventory { get; set; }
        public decimal? NegotiatedPrice { get; set; }
        public decimal? ProductPrice { get; set; }
        public decimal? PromotionPrice { get; set; }
        public TList<CustomerPricing> CustomerPricing { get; set; }
	}
}
