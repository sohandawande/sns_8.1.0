﻿#region Using directives

using System;

#endregion

namespace ZNode.Libraries.DataAccess.Entities
{	
	///<summary>
	/// An object representation of the 'ZNodeApplicationSetting' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class ApplicationSetting : ApplicationSettingBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="ApplicationSetting"/> instance.
		///</summary>
		public ApplicationSetting():base(){}	
		
		#endregion
	}
}
