﻿#region Using directives

using System;

#endregion

namespace ZNode.Libraries.DataAccess.Entities
{	
	///<summary>
	/// An object representation of the 'ZnodeProductRelationType' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class ProductRelationType : ProductRelationTypeBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="ProductRelationType"/> instance.
		///</summary>
		public ProductRelationType():base(){}	
		
		#endregion
	}
}
