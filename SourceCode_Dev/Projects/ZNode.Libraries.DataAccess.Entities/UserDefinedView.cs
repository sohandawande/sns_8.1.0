﻿#region Using directives

using System;

#endregion

namespace ZNode.Libraries.DataAccess.Entities
{	
	///<summary>
	/// An object representation of the 'ZNodeUserDefinedView' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class UserDefinedView : UserDefinedViewBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="UserDefinedView"/> instance.
		///</summary>
		public UserDefinedView():base(){}	
		
		#endregion
	}
}
