﻿using System;
using System.ComponentModel;

namespace ZNode.Libraries.DataAccess.Entities
{
	/// <summary>
	///		The data structure representation of the 'ZNodeOrder' table via interface.
	/// </summary>
	/// <remarks>
	/// 	This struct is generated by a tool and should never be modified.
	/// </remarks>
	public interface IOrder 
	{
		/// <summary>			
		/// OrderID : 
		/// </summary>
		/// <remarks>Member of the primary key of the underlying table "ZNodeOrder"</remarks>
		System.Int32 OrderID { get; set; }
				
		
		
		/// <summary>
		/// PortalId : 
		/// </summary>
		System.Int32?  PortalId  { get; set; }
		
		/// <summary>
		/// AccountID : 
		/// </summary>
		System.Int32?  AccountID  { get; set; }
		
		/// <summary>
		/// OrderStateID : 
		/// </summary>
		System.Int32?  OrderStateID  { get; set; }
		
		/// <summary>
		/// ShippingID : 
		/// </summary>
		System.Int32?  ShippingID  { get; set; }
		
		/// <summary>
		/// PaymentTypeId : 
		/// </summary>
		System.Int32?  PaymentTypeId  { get; set; }
		
		/// <summary>
		/// BillingFirstName : 
		/// </summary>
		System.String  BillingFirstName  { get; set; }
		
		/// <summary>
		/// BillingLastName : 
		/// </summary>
		System.String  BillingLastName  { get; set; }
		
		/// <summary>
		/// BillingCompanyName : 
		/// </summary>
		System.String  BillingCompanyName  { get; set; }
		
		/// <summary>
		/// BillingStreet : 
		/// </summary>
		System.String  BillingStreet  { get; set; }
		
		/// <summary>
		/// BillingStreet1 : 
		/// </summary>
		System.String  BillingStreet1  { get; set; }
		
		/// <summary>
		/// BillingCity : 
		/// </summary>
		System.String  BillingCity  { get; set; }
		
		/// <summary>
		/// BillingStateCode : 
		/// </summary>
		System.String  BillingStateCode  { get; set; }
		
		/// <summary>
		/// BillingPostalCode : 
		/// </summary>
		System.String  BillingPostalCode  { get; set; }
		
		/// <summary>
		/// BillingCountry : 
		/// </summary>
		System.String  BillingCountry  { get; set; }
		
		/// <summary>
		/// BillingPhoneNumber : 
		/// </summary>
		System.String  BillingPhoneNumber  { get; set; }
		
		/// <summary>
		/// BillingEmailId : 
		/// </summary>
		System.String  BillingEmailId  { get; set; }
		
		/// <summary>
		/// CardTransactionID : 
		/// </summary>
		System.String  CardTransactionID  { get; set; }
		
		/// <summary>
		/// CardAuthCode : 
		/// </summary>
		System.String  CardAuthCode  { get; set; }
		
		/// <summary>
		/// CardTypeId : 
		/// </summary>
		System.Int32?  CardTypeId  { get; set; }
		
		/// <summary>
		/// CardExp : 
		/// </summary>
		System.String  CardExp  { get; set; }
		
		/// <summary>
		/// TaxCost : 
		/// </summary>
		System.Decimal?  TaxCost  { get; set; }
		
		/// <summary>
		/// ShippingCost : 
		/// </summary>
		System.Decimal?  ShippingCost  { get; set; }
		
		/// <summary>
		/// SubTotal : 
		/// </summary>
		System.Decimal?  SubTotal  { get; set; }
		
		/// <summary>
		/// DiscountAmount : 
		/// </summary>
		System.Decimal?  DiscountAmount  { get; set; }
		
		/// <summary>
		/// Total : 
		/// </summary>
		System.Decimal?  Total  { get; set; }
		
		/// <summary>
		/// OrderDate : 
		/// </summary>
		System.DateTime?  OrderDate  { get; set; }
		
		/// <summary>
		/// Custom1 : 
		/// </summary>
		System.String  Custom1  { get; set; }
		
		/// <summary>
		/// Custom2 : 
		/// </summary>
		System.String  Custom2  { get; set; }
		
		/// <summary>
		/// AdditionalInstructions : 
		/// </summary>
		System.String  AdditionalInstructions  { get; set; }
		
		/// <summary>
		/// Custom3 : 
		/// </summary>
		System.String  Custom3  { get; set; }
		
		/// <summary>
		/// TrackingNumber : 
		/// </summary>
		System.String  TrackingNumber  { get; set; }
		
		/// <summary>
		/// CouponCode : 
		/// </summary>
		System.String  CouponCode  { get; set; }
		
		/// <summary>
		/// PromoDescription : 
		/// </summary>
		System.String  PromoDescription  { get; set; }
		
		/// <summary>
		/// ReferralAccountID : 
		/// </summary>
		System.Int32?  ReferralAccountID  { get; set; }
		
		/// <summary>
		/// PurchaseOrderNumber : 
		/// </summary>
		System.String  PurchaseOrderNumber  { get; set; }
		
		/// <summary>
		/// PaymentStatusID : 
		/// </summary>
		System.Int32?  PaymentStatusID  { get; set; }
		
		/// <summary>
		/// WebServiceDownloadDate : 
		/// </summary>
		System.DateTime?  WebServiceDownloadDate  { get; set; }
		
		/// <summary>
		/// PaymentSettingID : 
		/// </summary>
		System.Int32?  PaymentSettingID  { get; set; }
		
		/// <summary>
		/// ShipDate : 
		/// </summary>
		System.DateTime?  ShipDate  { get; set; }
		
		/// <summary>
		/// ReturnDate : 
		/// </summary>
		System.DateTime?  ReturnDate  { get; set; }
		
		/// <summary>
		/// SalesTax : 
		/// </summary>
		System.Decimal?  SalesTax  { get; set; }
		
		/// <summary>
		/// VAT : 
		/// </summary>
		System.Decimal?  VAT  { get; set; }
		
		/// <summary>
		/// GST : 
		/// </summary>
		System.Decimal?  GST  { get; set; }
		
		/// <summary>
		/// PST : 
		/// </summary>
		System.Decimal?  PST  { get; set; }
		
		/// <summary>
		/// HST : 
		/// </summary>
		System.Decimal?  HST  { get; set; }
		
		/// <summary>
		/// Created : 
		/// </summary>
		System.DateTime?  Created  { get; set; }
		
		/// <summary>
		/// CreatedBy : 
		/// </summary>
		System.String  CreatedBy  { get; set; }
		
		/// <summary>
		/// Modified : 
		/// </summary>
		System.DateTime?  Modified  { get; set; }
		
		/// <summary>
		/// ModifiedBy : 
		/// </summary>
		System.String  ModifiedBy  { get; set; }
		
		/// <summary>
		/// ExternalID : 
		/// </summary>
		System.String  ExternalID  { get; set; }
			
		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		System.Object Clone();
		
		#region Data Properties


		/// <summary>
		///	Holds a collection of entity objects
		///	which are related to this object through the relation _accountPaymentOrderID
		/// </summary>	
		TList<AccountPayment> AccountPaymentCollection {  get;  set;}	


		/// <summary>
		///	Holds a collection of entity objects
		///	which are related to this object through the relation _referralCommissionOrderID
		/// </summary>	
		TList<ReferralCommission> ReferralCommissionCollection {  get;  set;}	


		/// <summary>
		///	Holds a collection of entity objects
		///	which are related to this object through the relation _orderLineItemOrderID
		/// </summary>	
		TList<OrderLineItem> OrderLineItemCollection {  get;  set;}	


		/// <summary>
		///	Holds a collection of entity objects
		///	which are related to this object through the relation _giftCardHistoryOrderID
		/// </summary>	
		TList<GiftCardHistory> GiftCardHistoryCollection {  get;  set;}	
	

		/// <summary>
		///	Holds a  Order entity object
		///	which is related to this object through the relation _orderOrderID
		/// </summary>
		Order Order { get; set; }

		#endregion Data Properties

	}
}


