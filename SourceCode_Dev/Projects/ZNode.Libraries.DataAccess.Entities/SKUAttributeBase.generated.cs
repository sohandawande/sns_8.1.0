﻿
/*
	File generated by NetTiers templates [www.nettiers.net]
	Important: Do not modify this file. Edit the file SKUAttribute.cs instead.
*/

#region using directives
using System;
using System.ComponentModel;
using System.Collections;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using ZNode.Libraries.DataAccess.Entities.Validation;
#endregion

namespace ZNode.Libraries.DataAccess.Entities
{
	///<summary>
	/// An object representation of the 'ZNodeSKUAttribute' table. [No description found the database]	
	///</summary>
	[Serializable]
	[DataObject, CLSCompliant(true)]
	public abstract partial class SKUAttributeBase : EntityBase, ISKUAttribute, IEntityId<SKUAttributeKey>, System.IComparable, System.ICloneable, ICloneableEx, IEditableObject, IComponent, INotifyPropertyChanged
	{		
		#region Variable Declarations
		
		/// <summary>
		///  Hold the inner data of the entity.
		/// </summary>
		private SKUAttributeEntityData entityData;
		
		/// <summary>
		/// 	Hold the original data of the entity, as loaded from the repository.
		/// </summary>
		private SKUAttributeEntityData _originalData;
		
		/// <summary>
		/// 	Hold a backup of the inner data of the entity.
		/// </summary>
		private SKUAttributeEntityData backupData; 
		
		/// <summary>
		/// 	Key used if Tracking is Enabled for the <see cref="EntityLocator" />.
		/// </summary>
		private string entityTrackingKey;
		
		/// <summary>
		/// 	Hold the parent TList&lt;entity&gt; in which this entity maybe contained.
		/// </summary>
		/// <remark>Mostly used for databinding</remark>
		[NonSerialized]
		private TList<SKUAttribute> parentCollection;
		
		private bool inTxn = false;
		
		/// <summary>
		/// Occurs when a value is being changed for the specified column.
		/// </summary>
		[field:NonSerialized]
		public event SKUAttributeEventHandler ColumnChanging;		
		
		/// <summary>
		/// Occurs after a value has been changed for the specified column.
		/// </summary>
		[field:NonSerialized]
		public event SKUAttributeEventHandler ColumnChanged;
		
		#endregion Variable Declarations
		
		#region Constructors
		///<summary>
		/// Creates a new <see cref="SKUAttributeBase"/> instance.
		///</summary>
		public SKUAttributeBase()
		{
			this.entityData = new SKUAttributeEntityData();
			this.backupData = null;
		}		
		
		///<summary>
		/// Creates a new <see cref="SKUAttributeBase"/> instance.
		///</summary>
		///<param name="_sKUID"></param>
		///<param name="_attributeId"></param>
		public SKUAttributeBase(System.Int32 _sKUID, System.Int32 _attributeId)
		{
			this.entityData = new SKUAttributeEntityData();
			this.backupData = null;

			this.SKUID = _sKUID;
			this.AttributeId = _attributeId;
		}
		
		///<summary>
		/// A simple factory method to create a new <see cref="SKUAttribute"/> instance.
		///</summary>
		///<param name="_sKUID"></param>
		///<param name="_attributeId"></param>
		public static SKUAttribute CreateSKUAttribute(System.Int32 _sKUID, System.Int32 _attributeId)
		{
			SKUAttribute newSKUAttribute = new SKUAttribute();
			newSKUAttribute.SKUID = _sKUID;
			newSKUAttribute.AttributeId = _attributeId;
			return newSKUAttribute;
		}
				
		#endregion Constructors
			
		#region Properties	
		
		#region Data Properties		
		/// <summary>
		/// 	Gets or sets the SKUAttributeID property. 
		///		
		/// </summary>
		/// <value>This type is int.</value>
		/// <remarks>
		/// This property can not be set to null. 
		/// </remarks>
		
		[Required(ErrorMessage = "SKUAttributeID is required")]




		[ReadOnlyAttribute(false)/*, XmlIgnoreAttribute()*/, DescriptionAttribute(@""), System.ComponentModel.Bindable( System.ComponentModel.BindableSupport.Yes)]
		[DataObjectField(true, true, false)]
		public virtual System.Int32 SKUAttributeID
		{
			get
			{
				return this.entityData.SKUAttributeID; 
			}
			
			set
			{
				if (this.entityData.SKUAttributeID == value)
					return;
				
                OnPropertyChanging("SKUAttributeID");                    
				OnColumnChanging(SKUAttributeColumn.SKUAttributeID, this.entityData.SKUAttributeID);
				this.entityData.SKUAttributeID = value;
				this.EntityId.SKUAttributeID = value;
				if (this.EntityState == EntityState.Unchanged)
					this.EntityState = EntityState.Changed;
				OnColumnChanged(SKUAttributeColumn.SKUAttributeID, this.entityData.SKUAttributeID);
				OnPropertyChanged("SKUAttributeID");
			}
		}
		
		/// <summary>
		/// 	Gets or sets the SKUID property. 
		///		
		/// </summary>
		/// <value>This type is int.</value>
		/// <remarks>
		/// This property can not be set to null. 
		/// </remarks>
		
		[Required(ErrorMessage = "SKUID is required")]




		[DescriptionAttribute(@""), System.ComponentModel.Bindable( System.ComponentModel.BindableSupport.Yes)]
		[DataObjectField(false, false, false)]
		public virtual System.Int32 SKUID
		{
			get
			{
				return this.entityData.SKUID; 
			}
			
			set
			{
				if (this.entityData.SKUID == value)
					return;
				
                OnPropertyChanging("SKUID");                    
				OnColumnChanging(SKUAttributeColumn.SKUID, this.entityData.SKUID);
				this.entityData.SKUID = value;
				if (this.EntityState == EntityState.Unchanged)
					this.EntityState = EntityState.Changed;
				OnColumnChanged(SKUAttributeColumn.SKUID, this.entityData.SKUID);
				OnPropertyChanged("SKUID");
			}
		}
		
		/// <summary>
		/// 	Gets or sets the AttributeId property. 
		///		
		/// </summary>
		/// <value>This type is int.</value>
		/// <remarks>
		/// This property can not be set to null. 
		/// </remarks>
		
		[Required(ErrorMessage = "AttributeId is required")]




		[DescriptionAttribute(@""), System.ComponentModel.Bindable( System.ComponentModel.BindableSupport.Yes)]
		[DataObjectField(false, false, false)]
		public virtual System.Int32 AttributeId
		{
			get
			{
				return this.entityData.AttributeId; 
			}
			
			set
			{
				if (this.entityData.AttributeId == value)
					return;
				
                OnPropertyChanging("AttributeId");                    
				OnColumnChanging(SKUAttributeColumn.AttributeId, this.entityData.AttributeId);
				this.entityData.AttributeId = value;
				if (this.EntityState == EntityState.Unchanged)
					this.EntityState = EntityState.Changed;
				OnColumnChanged(SKUAttributeColumn.AttributeId, this.entityData.AttributeId);
				OnPropertyChanged("AttributeId");
			}
		}
		
		#endregion Data Properties		

		#region Source Foreign Key Property
				
		/// <summary>
		/// Gets or sets the source <see cref="ProductAttribute"/>.
		/// </summary>
		/// <value>The source ProductAttribute for AttributeId.</value>
        [XmlIgnore()]
		[Browsable(false), System.ComponentModel.Bindable(System.ComponentModel.BindableSupport.Yes)]
		public virtual ProductAttribute AttributeIdSource
      	{
            get { return entityData.AttributeIdSource; }
            set { entityData.AttributeIdSource = value; }
      	}
		/// <summary>
		/// Gets or sets the source <see cref="SKU"/>.
		/// </summary>
		/// <value>The source SKU for SKUID.</value>
        [XmlIgnore()]
		[Browsable(false), System.ComponentModel.Bindable(System.ComponentModel.BindableSupport.Yes)]
		public virtual SKU SKUIDSource
      	{
            get { return entityData.SKUIDSource; }
            set { entityData.SKUIDSource = value; }
      	}
		#endregion
		
		#region Children Collections
		#endregion Children Collections
		
		#endregion
		#region Validation
		
		/// <summary>
		/// Assigns validation rules to this object based on model definition.
		/// </summary>
		/// <remarks>This method overrides the base class to add schema related validation.</remarks>
		protected override void AddValidationRules()
		{
			//Validation rules based on database schema.
		}
   		#endregion
		
		#region Table Meta Data
		/// <summary>
		///		The name of the underlying database table.
		/// </summary>
		[BrowsableAttribute(false), XmlIgnoreAttribute()]
		public override string TableName
		{
			get { return "ZNodeSKUAttribute"; }
		}
		
		/// <summary>
		///		The name of the underlying database table's columns.
		/// </summary>
		[BrowsableAttribute(false), XmlIgnoreAttribute()]
		public override string[] TableColumns
		{
			get
			{
				return new string[] {"SKUAttributeID", "SKUID", "AttributeId"};
			}
		}
		#endregion 
		
		#region IEditableObject
		
		#region  CancelAddNew Event
		/// <summary>
        /// The delegate for the CancelAddNew event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
		public delegate void CancelAddNewEventHandler(object sender, EventArgs e);
    
    	/// <summary>
		/// The CancelAddNew event.
		/// </summary>
		[field:NonSerialized]
		public event CancelAddNewEventHandler CancelAddNew ;

		/// <summary>
        /// Called when [cancel add new].
        /// </summary>
        public void OnCancelAddNew()
        {    
			if (!SuppressEntityEvents)
			{
	            CancelAddNewEventHandler handler = CancelAddNew ;
            	if (handler != null)
	            {    
    	            handler(this, EventArgs.Empty) ;
        	    }
	        }
        }
		#endregion 
		
		/// <summary>
		/// Begins an edit on an object.
		/// </summary>
		void IEditableObject.BeginEdit() 
	    {
	        //Console.WriteLine("Start BeginEdit");
	        if (!inTxn) 
	        {
	            this.backupData = this.entityData.Clone() as SKUAttributeEntityData;
	            inTxn = true;
	            //Console.WriteLine("BeginEdit");
	        }
	        //Console.WriteLine("End BeginEdit");
	    }
	
		/// <summary>
		/// Discards changes since the last <c>BeginEdit</c> call.
		/// </summary>
	    void IEditableObject.CancelEdit() 
	    {
	        //Console.WriteLine("Start CancelEdit");
	        if (this.inTxn) 
	        {
	            this.entityData = this.backupData;
	            this.backupData = null;
				this.inTxn = false;

				if (this.bindingIsNew)
	        	//if (this.EntityState == EntityState.Added)
	        	{
					if (this.parentCollection != null)
						this.parentCollection.Remove( (SKUAttribute) this ) ;
				}	            
	        }
	        //Console.WriteLine("End CancelEdit");
	    }
	
		/// <summary>
		/// Pushes changes since the last <c>BeginEdit</c> or <c>IBindingList.AddNew</c> call into the underlying object.
		/// </summary>
	    void IEditableObject.EndEdit() 
	    {
	        //Console.WriteLine("Start EndEdit" + this.custData.id + this.custData.lastName);
	        if (this.inTxn) 
	        {
	            this.backupData = null;
				if (this.IsDirty) 
				{
					if (this.bindingIsNew) {
						this.EntityState = EntityState.Added;
						this.bindingIsNew = false ;
					}
					else
						if (this.EntityState == EntityState.Unchanged) 
							this.EntityState = EntityState.Changed ;
				}

				this.bindingIsNew = false ;
	            this.inTxn = false;	            
	        }
	        //Console.WriteLine("End EndEdit");
	    }
	    
	    /// <summary>
        /// Gets or sets the parent collection of this current entity, if available.
        /// </summary>
        /// <value>The parent collection.</value>
	    [XmlIgnore]
		[Browsable(false)]
	    public override object ParentCollection
	    {
	        get 
	        {
	            return this.parentCollection;
	        }
	        set 
	        {
	            this.parentCollection = value as TList<SKUAttribute>;
	        }
	    }
	    
	    /// <summary>
        /// Called when the entity is changed.
        /// </summary>
	    private void OnEntityChanged() 
	    {
	        if (!SuppressEntityEvents && !inTxn && this.parentCollection != null) 
	        {
	            this.parentCollection.EntityChanged(this as SKUAttribute);
	        }
	    }


		#endregion
		
		#region ICloneable Members
		///<summary>
		///  Returns a Typed SKUAttribute Entity 
		///</summary>
		protected virtual SKUAttribute Copy(IDictionary existingCopies)
		{
			if (existingCopies == null)
			{
				// This is the root of the tree to be copied!
				existingCopies = new Hashtable();
			}

			//shallow copy entity
			SKUAttribute copy = new SKUAttribute();
			existingCopies.Add(this, copy);
			copy.SuppressEntityEvents = true;
				copy.SKUAttributeID = this.SKUAttributeID;
				copy.SKUID = this.SKUID;
				copy.AttributeId = this.AttributeId;
			
			if (this.AttributeIdSource != null && existingCopies.Contains(this.AttributeIdSource))
				copy.AttributeIdSource = existingCopies[this.AttributeIdSource] as ProductAttribute;
			else
				copy.AttributeIdSource = MakeCopyOf(this.AttributeIdSource, existingCopies) as ProductAttribute;
			if (this.SKUIDSource != null && existingCopies.Contains(this.SKUIDSource))
				copy.SKUIDSource = existingCopies[this.SKUIDSource] as SKU;
			else
				copy.SKUIDSource = MakeCopyOf(this.SKUIDSource, existingCopies) as SKU;
		
			copy.EntityState = this.EntityState;
			copy.SuppressEntityEvents = false;
			return copy;
		}		
		
		
		
		///<summary>
		///  Returns a Typed SKUAttribute Entity 
		///</summary>
		public virtual SKUAttribute Copy()
		{
			return this.Copy(null);	
		}
		
		///<summary>
		/// ICloneable.Clone() Member, returns the Shallow Copy of this entity.
		///</summary>
		public object Clone()
		{
			return this.Copy(null);
		}
		
		///<summary>
		/// ICloneableEx.Clone() Member, returns the Shallow Copy of this entity.
		///</summary>
		public object Clone(IDictionary existingCopies)
		{
			return this.Copy(existingCopies);
		}
		
		///<summary>
		/// Returns a deep copy of the child collection object passed in.
		///</summary>
		public static object MakeCopyOf(object x)
		{
			if (x == null)
				return null;
				
			if (x is ICloneable)
			{
				// Return a deep copy of the object
				return ((ICloneable)x).Clone();
			}
			else
				throw new System.NotSupportedException("Object Does Not Implement the ICloneable Interface.");
		}
		
		///<summary>
		/// Returns a deep copy of the child collection object passed in.
		///</summary>
		public static object MakeCopyOf(object x, IDictionary existingCopies)
		{
			if (x == null)
				return null;
			
			if (x is ICloneableEx)
			{
				// Return a deep copy of the object
				return ((ICloneableEx)x).Clone(existingCopies);
			}
			else if (x is ICloneable)
			{
				// Return a deep copy of the object
				return ((ICloneable)x).Clone();
			}
			else
				throw new System.NotSupportedException("Object Does Not Implement the ICloneable or IClonableEx Interface.");
		}
		
		
		///<summary>
		///  Returns a Typed SKUAttribute Entity which is a deep copy of the current entity.
		///</summary>
		public virtual SKUAttribute DeepCopy()
		{
			return EntityHelper.Clone<SKUAttribute>(this as SKUAttribute);	
		}
		#endregion
		
		#region Methods	
			
		///<summary>
		/// Revert all changes and restore original values.
		///</summary>
		public override void CancelChanges()
		{
			IEditableObject obj = (IEditableObject) this;
			obj.CancelEdit();

			this.entityData = null;
			if (this._originalData != null)
			{
				this.entityData = this._originalData.Clone() as SKUAttributeEntityData;
			}
			else
			{
				//Since this had no _originalData, then just reset the entityData with a new one.  entityData cannot be null.
				this.entityData = new SKUAttributeEntityData();
			}
		}	
		
		/// <summary>
		/// Accepts the changes made to this object.
		/// </summary>
		/// <remarks>
		/// After calling this method, properties: IsDirty, IsNew are false. IsDeleted flag remains unchanged as it is handled by the parent List.
		/// </remarks>
		public override void AcceptChanges()
		{
			base.AcceptChanges();

			// we keep of the original version of the data
			this._originalData = null;
			this._originalData = this.entityData.Clone() as SKUAttributeEntityData;
		}
		
		#region Comparision with original data
		
		/// <summary>
		/// Determines whether the property value has changed from the original data.
		/// </summary>
		/// <param name="column">The column.</param>
		/// <returns>
		/// 	<c>true</c> if the property value has changed; otherwise, <c>false</c>.
		/// </returns>
		public bool IsPropertyChanged(SKUAttributeColumn column)
		{
			switch(column)
			{
					case SKUAttributeColumn.SKUAttributeID:
					return entityData.SKUAttributeID != _originalData.SKUAttributeID;
					case SKUAttributeColumn.SKUID:
					return entityData.SKUID != _originalData.SKUID;
					case SKUAttributeColumn.AttributeId:
					return entityData.AttributeId != _originalData.AttributeId;
			
				default:
					return false;
			}
		}
		
		/// <summary>
		/// Determines whether the property value has changed from the original data.
		/// </summary>
		/// <param name="columnName">The column name.</param>
		/// <returns>
		/// 	<c>true</c> if the property value has changed; otherwise, <c>false</c>.
		/// </returns>
		public override bool IsPropertyChanged(string columnName)
		{
			return 	IsPropertyChanged(EntityHelper.GetEnumValue< SKUAttributeColumn >(columnName));
		}
		
		/// <summary>
		/// Determines whether the data has changed from original.
		/// </summary>
		/// <returns>
		/// 	<c>true</c> if data has changed; otherwise, <c>false</c>.
		/// </returns>
		public bool HasDataChanged()
		{
			bool result = false;
			result = result || entityData.SKUAttributeID != _originalData.SKUAttributeID;
			result = result || entityData.SKUID != _originalData.SKUID;
			result = result || entityData.AttributeId != _originalData.AttributeId;
			return result;
		}	
		
		///<summary>
		///  Returns a SKUAttribute Entity with the original data.
		///</summary>
		public SKUAttribute GetOriginalEntity()
		{
			if (_originalData != null)
				return CreateSKUAttribute(
				_originalData.SKUID,
				_originalData.AttributeId
				);
				
			return (SKUAttribute)this.Clone();
		}
		#endregion
	
	#region Value Semantics Instance Equality
        ///<summary>
        /// Returns a value indicating whether this instance is equal to a specified object using value semantics.
        ///</summary>
        ///<param name="Object1">An object to compare to this instance.</param>
        ///<returns>true if Object1 is a <see cref="SKUAttributeBase"/> and has the same value as this instance; otherwise, false.</returns>
        public override bool Equals(object Object1)
        {
			// Cast exception if Object1 is null or DbNull
			if (Object1 != null && Object1 != DBNull.Value && Object1 is SKUAttributeBase)
				return ValueEquals(this, (SKUAttributeBase)Object1);
			else
				return false;
        }

        /// <summary>
		/// Serves as a hash function for a particular type, suitable for use in hashing algorithms and data structures like a hash table.
        /// Provides a hash function that is appropriate for <see cref="SKUAttributeBase"/> class 
        /// and that ensures a better distribution in the hash table
        /// </summary>
        /// <returns>number (hash code) that corresponds to the value of an object</returns>
        public override int GetHashCode()
        {
			return this.SKUAttributeID.GetHashCode() ^ 
					this.SKUID.GetHashCode() ^ 
					this.AttributeId.GetHashCode();
        }
		
		///<summary>
		/// Returns a value indicating whether this instance is equal to a specified object using value semantics.
		///</summary>
		///<param name="toObject">An object to compare to this instance.</param>
		///<returns>true if toObject is a <see cref="SKUAttributeBase"/> and has the same value as this instance; otherwise, false.</returns>
		public virtual bool Equals(SKUAttributeBase toObject)
		{
			if (toObject == null)
				return false;
			return ValueEquals(this, toObject);
		}
		#endregion
		
		///<summary>
		/// Determines whether the specified <see cref="SKUAttributeBase"/> instances are considered equal using value semantics.
		///</summary>
		///<param name="Object1">The first <see cref="SKUAttributeBase"/> to compare.</param>
		///<param name="Object2">The second <see cref="SKUAttributeBase"/> to compare. </param>
		///<returns>true if Object1 is the same instance as Object2 or if both are null references or if objA.Equals(objB) returns true; otherwise, false.</returns>
		public static bool ValueEquals(SKUAttributeBase Object1, SKUAttributeBase Object2)
		{
			// both are null
			if (Object1 == null && Object2 == null)
				return true;

			// one or the other is null, but not both
			if (Object1 == null ^ Object2 == null)
				return false;
				
			bool equal = true;
			if (Object1.SKUAttributeID != Object2.SKUAttributeID)
				equal = false;
			if (Object1.SKUID != Object2.SKUID)
				equal = false;
			if (Object1.AttributeId != Object2.AttributeId)
				equal = false;
					
			return equal;
		}
		
		#endregion
		
		#region IComparable Members
		///<summary>
		/// Compares this instance to a specified object and returns an indication of their relative values.
		///<param name="obj">An object to compare to this instance, or a null reference (Nothing in Visual Basic).</param>
		///</summary>
		///<returns>A signed integer that indicates the relative order of this instance and obj.</returns>
		public virtual int CompareTo(object obj)
		{
			throw new NotImplementedException();
			//return this. GetPropertyName(SourceTable.PrimaryKey.MemberColumns[0]) .CompareTo(((SKUAttributeBase)obj).GetPropertyName(SourceTable.PrimaryKey.MemberColumns[0]));
		}
		
		/*
		// static method to get a Comparer object
        public static SKUAttributeComparer GetComparer()
        {
            return new SKUAttributeComparer();
        }
        */

        // Comparer delegates back to SKUAttribute
        // Employee uses the integer's default
        // CompareTo method
        /*
        public int CompareTo(Item rhs)
        {
            return this.Id.CompareTo(rhs.Id);
        }
        */

/*
        // Special implementation to be called by custom comparer
        public int CompareTo(SKUAttribute rhs, SKUAttributeColumn which)
        {
            switch (which)
            {
            	
            	
            	case SKUAttributeColumn.SKUAttributeID:
            		return this.SKUAttributeID.CompareTo(rhs.SKUAttributeID);
            		
            		                 
            	
            	
            	case SKUAttributeColumn.SKUID:
            		return this.SKUID.CompareTo(rhs.SKUID);
            		
            		                 
            	
            	
            	case SKUAttributeColumn.AttributeId:
            		return this.AttributeId.CompareTo(rhs.AttributeId);
            		
            		                 
            }
            return 0;
        }
        */
	
		#endregion
		
		#region IComponent Members
		
		private ISite _site = null;

		/// <summary>
		/// Gets or Sets the site where this data is located.
		/// </summary>
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public ISite Site
		{
			get{ return this._site; }
			set{ this._site = value; }
		}

		#endregion

		#region IDisposable Members
		
		/// <summary>
		/// Notify those that care when we dispose.
		/// </summary>
		[field:NonSerialized]
		public event System.EventHandler Disposed;

		/// <summary>
		/// Clean up. Nothing here though.
		/// </summary>
		public virtual void Dispose()
		{
			this.parentCollection = null;
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}
		
		/// <summary>
		/// Clean up.
		/// </summary>
		protected virtual void Dispose(bool disposing)
		{
			if (disposing)
			{
				EventHandler handler = Disposed;
				if (handler != null)
					handler(this, EventArgs.Empty);
			}
		}
		
		#endregion
				
		#region IEntityKey<SKUAttributeKey> Members
		
		// member variable for the EntityId property
		private SKUAttributeKey _entityId;

		/// <summary>
		/// Gets or sets the EntityId property.
		/// </summary>
		[XmlIgnore]
		public virtual SKUAttributeKey EntityId
		{
			get
			{
				if ( _entityId == null )
				{
					_entityId = new SKUAttributeKey(this);
				}

				return _entityId;
			}
			set
			{
				if ( value != null )
				{
					value.Entity = this;
				}
				
				_entityId = value;
			}
		}
		
		#endregion
		
		#region EntityState
		/// <summary>
		///		Indicates state of object
		/// </summary>
		/// <remarks>0=Unchanged, 1=Added, 2=Changed</remarks>
		[BrowsableAttribute(false) , XmlIgnoreAttribute()]
		public override EntityState EntityState 
		{ 
			get{ return entityData.EntityState;	 } 
			set{ entityData.EntityState = value; } 
		}
		#endregion 
		
		#region EntityTrackingKey
		///<summary>
		/// Provides the tracking key for the <see cref="EntityLocator"/>
		///</summary>
		[XmlIgnore]
		public override string EntityTrackingKey
		{
			get
			{
				if(entityTrackingKey == null)
					entityTrackingKey = new System.Text.StringBuilder("SKUAttribute")
					.Append("|").Append( this.SKUAttributeID.ToString()).ToString();
				return entityTrackingKey;
			}
			set
		    {
		        if (value != null)
                    entityTrackingKey = value;
		    }
		}
		#endregion 
		
		#region ToString Method
		
		///<summary>
		/// Returns a String that represents the current object.
		///</summary>
		public override string ToString()
		{
			return string.Format(System.Globalization.CultureInfo.InvariantCulture,
				"{4}{3}- SKUAttributeID: {0}{3}- SKUID: {1}{3}- AttributeId: {2}{3}{5}", 
				this.SKUAttributeID,
				this.SKUID,
				this.AttributeId,
				System.Environment.NewLine, 
				this.GetType(),
				this.Error.Length == 0 ? string.Empty : string.Format("- Error: {0}\n",this.Error));
		}
		
		#endregion ToString Method
		
		#region Inner data class
		
	/// <summary>
	///		The data structure representation of the 'ZNodeSKUAttribute' table.
	/// </summary>
	/// <remarks>
	/// 	This struct is generated by a tool and should never be modified.
	/// </remarks>
	[EditorBrowsable(EditorBrowsableState.Never)]
	[Serializable]
	internal protected class SKUAttributeEntityData : ICloneable, ICloneableEx
	{
		#region Variable Declarations
		private EntityState currentEntityState = EntityState.Added;
		
		#region Primary key(s)
		/// <summary>			
		/// SKUAttributeID : 
		/// </summary>
		/// <remarks>Member of the primary key of the underlying table "ZNodeSKUAttribute"</remarks>
		public System.Int32 SKUAttributeID;
			
		#endregion
		
		#region Non Primary key(s)
		
		/// <summary>
		/// SKUID : 
		/// </summary>
		public System.Int32 SKUID = (int)0;
		
		/// <summary>
		/// AttributeId : 
		/// </summary>
		public System.Int32 AttributeId = (int)0;
		#endregion
			
		#region Source Foreign Key Property
				
		private ProductAttribute _attributeIdSource = null;
		
		/// <summary>
		/// Gets or sets the source <see cref="ProductAttribute"/>.
		/// </summary>
		/// <value>The source ProductAttribute for AttributeId.</value>
		[XmlIgnore()]
		[Browsable(false)]
		public virtual ProductAttribute AttributeIdSource
      	{
            get { return this._attributeIdSource; }
            set { this._attributeIdSource = value; }
      	}
		private SKU _sKUIDSource = null;
		
		/// <summary>
		/// Gets or sets the source <see cref="SKU"/>.
		/// </summary>
		/// <value>The source SKU for SKUID.</value>
		[XmlIgnore()]
		[Browsable(false)]
		public virtual SKU SKUIDSource
      	{
            get { return this._sKUIDSource; }
            set { this._sKUIDSource = value; }
      	}
		#endregion
        
		#endregion Variable Declarations

		#region Data Properties

		#endregion Data Properties
		#region Clone Method

		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		public Object Clone()
		{
			SKUAttributeEntityData _tmp = new SKUAttributeEntityData();
						
			_tmp.SKUAttributeID = this.SKUAttributeID;
			
			_tmp.SKUID = this.SKUID;
			_tmp.AttributeId = this.AttributeId;
			
			#region Source Parent Composite Entities
			if (this.AttributeIdSource != null)
				_tmp.AttributeIdSource = MakeCopyOf(this.AttributeIdSource) as ProductAttribute;
			if (this.SKUIDSource != null)
				_tmp.SKUIDSource = MakeCopyOf(this.SKUIDSource) as SKU;
			#endregion
		
			#region Child Collections
			#endregion Child Collections
			
			//EntityState
			_tmp.EntityState = this.EntityState;
			
			return _tmp;
		}
		
		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		public object Clone(IDictionary existingCopies)
		{
			if (existingCopies == null)
				existingCopies = new Hashtable();
				
			SKUAttributeEntityData _tmp = new SKUAttributeEntityData();
						
			_tmp.SKUAttributeID = this.SKUAttributeID;
			
			_tmp.SKUID = this.SKUID;
			_tmp.AttributeId = this.AttributeId;
			
			#region Source Parent Composite Entities
			if (this.AttributeIdSource != null && existingCopies.Contains(this.AttributeIdSource))
				_tmp.AttributeIdSource = existingCopies[this.AttributeIdSource] as ProductAttribute;
			else
				_tmp.AttributeIdSource = MakeCopyOf(this.AttributeIdSource, existingCopies) as ProductAttribute;
			if (this.SKUIDSource != null && existingCopies.Contains(this.SKUIDSource))
				_tmp.SKUIDSource = existingCopies[this.SKUIDSource] as SKU;
			else
				_tmp.SKUIDSource = MakeCopyOf(this.SKUIDSource, existingCopies) as SKU;
			#endregion
		
			#region Child Collections
			#endregion Child Collections
			
			//EntityState
			_tmp.EntityState = this.EntityState;
			
			return _tmp;
		}
		
		#endregion Clone Method
		
		/// <summary>
		///		Indicates state of object
		/// </summary>
		/// <remarks>0=Unchanged, 1=Added, 2=Changed</remarks>
		[BrowsableAttribute(false), XmlIgnoreAttribute()]
		public EntityState	EntityState
		{
			get { return currentEntityState;  }
			set { currentEntityState = value; }
		}
	
	}//End struct

		#endregion
		
				
		
		#region Events trigger
		/// <summary>
		/// Raises the <see cref="ColumnChanging" /> event.
		/// </summary>
		/// <param name="column">The <see cref="SKUAttributeColumn"/> which has raised the event.</param>
		public virtual void OnColumnChanging(SKUAttributeColumn column)
		{
			OnColumnChanging(column, null);
			return;
		}
		
		/// <summary>
		/// Raises the <see cref="ColumnChanged" /> event.
		/// </summary>
		/// <param name="column">The <see cref="SKUAttributeColumn"/> which has raised the event.</param>
		public virtual void OnColumnChanged(SKUAttributeColumn column)
		{
			OnColumnChanged(column, null);
			return;
		} 
		
		
		/// <summary>
		/// Raises the <see cref="ColumnChanging" /> event.
		/// </summary>
		/// <param name="column">The <see cref="SKUAttributeColumn"/> which has raised the event.</param>
		/// <param name="value">The changed value.</param>
		public virtual void OnColumnChanging(SKUAttributeColumn column, object value)
		{
			if(IsEntityTracked && EntityState != EntityState.Added && !EntityManager.TrackChangedEntities)
                EntityManager.StopTracking(entityTrackingKey);
                
			if (!SuppressEntityEvents)
			{
				SKUAttributeEventHandler handler = ColumnChanging;
				if(handler != null)
				{
					handler(this, new SKUAttributeEventArgs(column, value));
				}
			}
		}
		
		/// <summary>
		/// Raises the <see cref="ColumnChanged" /> event.
		/// </summary>
		/// <param name="column">The <see cref="SKUAttributeColumn"/> which has raised the event.</param>
		/// <param name="value">The changed value.</param>
		public virtual void OnColumnChanged(SKUAttributeColumn column, object value)
		{
			if (!SuppressEntityEvents)
			{
				SKUAttributeEventHandler handler = ColumnChanged;
				if(handler != null)
				{
					handler(this, new SKUAttributeEventArgs(column, value));
				}
			
				// warn the parent list that i have changed
				OnEntityChanged();
			}
		} 
		#endregion
			
	} // End Class
	
	
	#region SKUAttributeEventArgs class
	/// <summary>
	/// Provides data for the ColumnChanging and ColumnChanged events.
	/// </summary>
	/// <remarks>
	/// The ColumnChanging and ColumnChanged events occur when a change is made to the value 
	/// of a property of a <see cref="SKUAttribute"/> object.
	/// </remarks>
	public class SKUAttributeEventArgs : System.EventArgs
	{
		private SKUAttributeColumn column;
		private object value;
		
		///<summary>
		/// Initalizes a new Instance of the SKUAttributeEventArgs class.
		///</summary>
		public SKUAttributeEventArgs(SKUAttributeColumn column)
		{
			this.column = column;
		}
		
		///<summary>
		/// Initalizes a new Instance of the SKUAttributeEventArgs class.
		///</summary>
		public SKUAttributeEventArgs(SKUAttributeColumn column, object value)
		{
			this.column = column;
			this.value = value;
		}
		
		///<summary>
		/// The SKUAttributeColumn that was modified, which has raised the event.
		///</summary>
		///<value cref="SKUAttributeColumn" />
		public SKUAttributeColumn Column { get { return this.column; } }
		
		/// <summary>
        /// Gets the current value of the column.
        /// </summary>
        /// <value>The current value of the column.</value>
		public object Value{ get { return this.value; } }

	}
	#endregion
	
	///<summary>
	/// Define a delegate for all SKUAttribute related events.
	///</summary>
	public delegate void SKUAttributeEventHandler(object sender, SKUAttributeEventArgs e);
	
	#region SKUAttributeComparer
		
	/// <summary>
	///	Strongly Typed IComparer
	/// </summary>
	public class SKUAttributeComparer : System.Collections.Generic.IComparer<SKUAttribute>
	{
		SKUAttributeColumn whichComparison;
		
		/// <summary>
        /// Initializes a new instance of the <see cref="T:SKUAttributeComparer"/> class.
        /// </summary>
		public SKUAttributeComparer()
        {            
        }               
        
        /// <summary>
        /// Initializes a new instance of the <see cref="T:SKUAttributeComparer"/> class.
        /// </summary>
        /// <param name="column">The column to sort on.</param>
        public SKUAttributeComparer(SKUAttributeColumn column)
        {
            this.whichComparison = column;
        }

		/// <summary>
        /// Determines whether the specified <see cref="SKUAttribute"/> instances are considered equal.
        /// </summary>
        /// <param name="a">The first <see cref="SKUAttribute"/> to compare.</param>
        /// <param name="b">The second <c>SKUAttribute</c> to compare.</param>
        /// <returns>true if objA is the same instance as objB or if both are null references or if objA.Equals(objB) returns true; otherwise, false.</returns>
        public bool Equals(SKUAttribute a, SKUAttribute b)
        {
            return this.Compare(a, b) == 0;
        }

		/// <summary>
        /// Gets the hash code of the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        public int GetHashCode(SKUAttribute entity)
        {
            return entity.GetHashCode();
        }

        /// <summary>
        /// Performs a case-insensitive comparison of two objects of the same type and returns a value indicating whether one is less than, equal to, or greater than the other.
        /// </summary>
        /// <param name="a">The first object to compare.</param>
        /// <param name="b">The second object to compare.</param>
        /// <returns></returns>
        public int Compare(SKUAttribute a, SKUAttribute b)
        {
        	EntityPropertyComparer entityPropertyComparer = new EntityPropertyComparer(this.whichComparison.ToString());
        	return entityPropertyComparer.Compare(a, b);
        }

		/// <summary>
        /// Gets or sets the column that will be used for comparison.
        /// </summary>
        /// <value>The comparison column.</value>
        public SKUAttributeColumn WhichComparison
        {
            get { return this.whichComparison; }
            set { this.whichComparison = value; }
        }
	}
	
	#endregion
	
	#region SKUAttributeKey Class

	/// <summary>
	/// Wraps the unique identifier values for the <see cref="SKUAttribute"/> object.
	/// </summary>
	[Serializable]
	[CLSCompliant(true)]
	public class SKUAttributeKey : EntityKeyBase
	{
		#region Constructors
		
		/// <summary>
		/// Initializes a new instance of the SKUAttributeKey class.
		/// </summary>
		public SKUAttributeKey()
		{
		}
		
		/// <summary>
		/// Initializes a new instance of the SKUAttributeKey class.
		/// </summary>
		public SKUAttributeKey(SKUAttributeBase entity)
		{
			this.Entity = entity;

			#region Init Properties

			if ( entity != null )
			{
				this.SKUAttributeID = entity.SKUAttributeID;
			}

			#endregion
		}
		
		/// <summary>
		/// Initializes a new instance of the SKUAttributeKey class.
		/// </summary>
		public SKUAttributeKey(System.Int32 _sKUAttributeID)
		{
			#region Init Properties

			this.SKUAttributeID = _sKUAttributeID;

			#endregion
		}
		
		#endregion Constructors

		#region Properties
		
		// member variable for the Entity property
		private SKUAttributeBase _entity;
		
		/// <summary>
		/// Gets or sets the Entity property.
		/// </summary>
		public SKUAttributeBase Entity
		{
			get { return _entity; }
			set { _entity = value; }
		}
		
		// member variable for the SKUAttributeID property
		private System.Int32 _sKUAttributeID;
		
		/// <summary>
		/// Gets or sets the SKUAttributeID property.
		/// </summary>
		public System.Int32 SKUAttributeID
		{
			get { return _sKUAttributeID; }
			set
			{
				if ( this.Entity != null )
					this.Entity.SKUAttributeID = value;
				
				_sKUAttributeID = value;
			}
		}
		
		#endregion

		#region Methods
		
		/// <summary>
		/// Reads values from the supplied <see cref="IDictionary"/> object into
		/// properties of the current object.
		/// </summary>
		/// <param name="values">An <see cref="IDictionary"/> instance that contains
		/// the key/value pairs to be used as property values.</param>
		public override void Load(IDictionary values)
		{
			#region Init Properties

			if ( values != null )
			{
				SKUAttributeID = ( values["SKUAttributeID"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["SKUAttributeID"], typeof(System.Int32)) : (int)0;
			}

			#endregion
		}

		/// <summary>
		/// Creates a new <see cref="IDictionary"/> object and populates it
		/// with the property values of the current object.
		/// </summary>
		/// <returns>A collection of name/value pairs.</returns>
		public override IDictionary ToDictionary()
		{
			IDictionary values = new Hashtable();

			#region Init Dictionary

			values.Add("SKUAttributeID", SKUAttributeID);

			#endregion Init Dictionary

			return values;
		}
		
		///<summary>
		/// Returns a String that represents the current object.
		///</summary>
		public override string ToString()
		{
			return String.Format("SKUAttributeID: {0}{1}",
								SKUAttributeID,
								System.Environment.NewLine);
		}

		#endregion Methods
	}
	
	#endregion	

	#region SKUAttributeColumn Enum
	
	/// <summary>
	/// Enumerate the SKUAttribute columns.
	/// </summary>
	[Serializable]
	public enum SKUAttributeColumn : int
	{
		/// <summary>
		/// SKUAttributeID : 
		/// </summary>
		[EnumTextValue("SKU Attribute ID")]
		[ColumnEnum("SKUAttributeID", typeof(System.Int32), System.Data.DbType.Int32, true, true, false)]
		SKUAttributeID = 1,
		/// <summary>
		/// SKUID : 
		/// </summary>
		[EnumTextValue("SKUID")]
		[ColumnEnum("SKUID", typeof(System.Int32), System.Data.DbType.Int32, false, false, false)]
		SKUID = 2,
		/// <summary>
		/// AttributeId : 
		/// </summary>
		[EnumTextValue("Attribute Id")]
		[ColumnEnum("AttributeId", typeof(System.Int32), System.Data.DbType.Int32, false, false, false)]
		AttributeId = 3
	}//End enum

	#endregion SKUAttributeColumn Enum

} // end namespace
