﻿using System.IO;
using System.Web.Mvc;
using System.Web.Routing;
using Znode.Engine.MvcDemo.Agents;
using Znode.Engine.MvcDemo.Helpers;

namespace Znode.Engine.MvcDemo.Controllers
{
    public class 
        BaseController : Controller
    {
        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            ViewBag.Title = PortalAgent.CurrentPortal.Name;
            ViewBag.Theme = PortalAgent.CurrentPortal.Theme;
            ViewBag.CSS = PortalAgent.CurrentPortal.Css;
            ViewBag.PortalID = PortalAgent.CurrentPortal.PortalId;
            ViewBag.CustomerServicePhoneNumber = PortalAgent.CurrentPortal.CustomerServicePhoneNumber;
            ViewBag.CustomerServiceEmail = PortalAgent.CurrentPortal.CustomerServiceEmail;
            ViewBag.IsEnableSinglePageCheckout = PortalAgent.CurrentPortal.IsEnableSinglePageCheckout;
            ViewBag.StoreLogo = PortalAgent.CurrentPortal.LogoPath;

            this.ControllerContext = new ControllerContext(requestContext, this);
        }

        //Method converts the Partial view result to string.
        public string RenderRazorViewToString(string viewName, object model)
        {
            ViewData.Model = model;
            using (StringWriter stringWriter = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext,
                viewName);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View,
                ViewData, TempData, stringWriter);
                viewResult.View.Render(viewContext, stringWriter);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                return stringWriter.GetStringBuilder().ToString();
            }
        }
    }
}
