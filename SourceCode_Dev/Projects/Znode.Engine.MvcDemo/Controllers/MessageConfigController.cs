﻿using System.Web.Mvc;
using Znode.Engine.MvcDemo.Agents;

namespace Znode.Engine.MvcDemo.Controllers
{
    public class MessageConfigController : BaseController
    {
        #region Private Variables
        private readonly IMessageConfigsAgent _agent; 
        #endregion

        #region Constructor
        public MessageConfigController()
        {
            _agent = new MessageConfigsAgent();
        } 
        #endregion

        #region Public Methods
        public ActionResult GetMessage(string key)
        {
            string content = _agent.GetByKey(key);
            string filteredString = content.Replace("<p>", string.Empty).Replace("</p>", string.Empty);
            return Content(filteredString);
        } 
        #endregion
    }
}
