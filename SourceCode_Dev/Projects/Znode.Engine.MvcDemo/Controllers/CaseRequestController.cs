﻿using System.Web.Mvc;
using Znode.Engine.MvcDemo.Agents;
using Znode.Engine.MvcDemo.Helpers;
using Znode.Engine.MvcDemo.ViewModels;
using System.Collections.Generic;
using Znode.Libraries.Helpers.Extensions;
using System.Configuration;
using System.Net;
using Newtonsoft.Json;

namespace Znode.Engine.MvcDemo.Controllers
{
    public partial class CaseRequestController : BaseController
    {
        #region Private Readonly Variables
        private readonly ICaseRequestAgent _caseRequestAgent;
        private readonly IAccountAgent _accountAgent;
        #endregion

        #region Private String Variables
        private string caseOriginContactUs = "Contact Us Form";
        private string caseRequestContactUsTitle = "Contact Form Submission";
        private string caseOriginCustomerFeedback = "Feedback Form";
        private string caseRequestCustomerFeedbackTitle = "User Feedback";
        private string contactUsSuccessPage = "ContactUsSuccessPage";
        private string customerFeedbackSuccessPage = "CustomerFeedbackSuccessPage";
        private string contactUsSuccess = "ContactUsSuccess";
        private string customerFeedbackSuccess = "CustomerFeedbackSuccess";
        #endregion

        #region Constructor
        public CaseRequestController()
        {
            _caseRequestAgent = new CaseRequestAgent();
            _accountAgent = new AccountAgent();
        }
        #endregion

        #region Public Methods for Contact Us service

        /// <summary>
        /// Contact Us in Service Request.
        /// </summary>
        /// <returns>Returns Contact Us page.</returns>
        [HttpGet]
        public ActionResult ContactUs()
        {
            CaseRequestViewModel model = new CaseRequestViewModel();

            //PRFT Custom Code : Start
            
            model.IsRequestAQuoteForm = false;
            ContactUsExtnViewModel modelContactUsExtn = new ContactUsExtnViewModel();
            modelContactUsExtn.Countries = _caseRequestAgent.GetCountries(1);
            modelContactUsExtn.States = _caseRequestAgent.GetStates();
            model.ContactUsExtn = modelContactUsExtn;
            model.SalesReps = _caseRequestAgent.GetSalesRep();
            if (model.SalesReps != null)
            {
                model.SalesReps.Insert(0, new Api.Models.PRFTSalesRepresentative() { SalesRepID = 0, SaleRepNameWithLocation = Resources.ZnodeResource.PleaseSelectText, Email = string.Empty });
            }
            model.IsSalesRepFound = false;
            model.SalesRepId = 0;
            if (User.Identity.IsAuthenticated)
            {
                AccountViewModel accountModel = _accountAgent.GetAccountViewModel();
                if (!Equals(accountModel, null))
                {
                    if (!string.IsNullOrEmpty(accountModel.Custom1))
                    {
                        int salesRepID = 0;
                        int.TryParse(accountModel.Custom1, out salesRepID);
                        if (salesRepID > 0)
                        {
                            model.SalesRepId = salesRepID;
                            model.IsSalesRepFound = true;
                        }
                    }
                }
            }

            //PRFT Custom Code : End
            return View("ContactUs", model);
        }

        /// <summary>
        /// Contact Us in Service Request. 
        /// </summary>
        /// <param name="model">Case Request View Model</param>
        /// <returns>Returns success page on submiting contact us form.</returns>
        [HttpPost]
        public ActionResult ContactUs(CaseRequestViewModel model)
        {
            if (ModelState.IsValid && !Equals(model, null))
            {
                //PRFT Custom Code for Google reCaptcha : Start
                if (!RecaptchaValidation())
                {
                    ContactUsExtnViewModel modelContactUsExtn = new ContactUsExtnViewModel();
                    modelContactUsExtn.Countries = _caseRequestAgent.GetCountries(1);
                    modelContactUsExtn.States = _caseRequestAgent.GetStates();
                    model.ContactUsExtn = modelContactUsExtn;
                    model.SalesReps = _caseRequestAgent.GetSalesRep();
                    if (model.SalesReps != null)
                    {
                        model.SalesReps.Insert(0, new Api.Models.PRFTSalesRepresentative() { SalesRepID = 0, SaleRepNameWithLocation = Resources.ZnodeResource.PleaseSelectText, Email = string.Empty });
                    }

                    return View(model);
                }
                //PRFT Custom Code for Google reCaptcha : End

                model.PortalId = PortalAgent.CurrentPortal.PortalId;
                model.CasePriorityId = MvcDemoConstants.CasePriorityId;
                model.CaseStatusId = MvcDemoConstants.CaseStatusId;
                model.CreatedDate = System.DateTime.Now;
                model.CaseTypeId = MvcDemoConstants.CaseTypeId;
                model.CaseOrigin = caseOriginContactUs;
                model.CaseRequestTitle = caseRequestContactUsTitle;

                //PRFT Custom Code : Start
                model.SalesReps = _caseRequestAgent.GetSalesRep();
                var SalesRep = model.SalesReps.ToList<Api.Models.PRFTSalesRepresentative>().Find(x => x.SalesRepID == model.SalesRepId);
                if(SalesRep!=null)
                {
                    model.SaleRepNameWithLocation = SalesRep.SaleRepNameWithLocation;
                    model.SalesRepEmail = SalesRep.Email;
                }
                //PRFT Custom Code : Start
                
                _caseRequestAgent.SaveContactUsInfo(model);
                return RedirectToAction(contactUsSuccess);
            }
            
            return View(model);
            //return View();
            
        }        

        /// <summary>
        /// Contact Us Success Message
        /// </summary>
        /// <returns>Returns view contactUsSuccessPage.</returns>
        public ActionResult ContactUsSuccess()
        {
            return View(contactUsSuccessPage);
        }
        #endregion

        #region Public Methods for Customer Feedback Service.
        /// <summary>
        /// Customer Feedback in Service Request.
        /// </summary>
        /// <returns>Returns Customer Feedback Form.</returns>
        [HttpGet]
        public ActionResult CustomerFeedback()
        {
            CaseRequestViewModel model = new CaseRequestViewModel();
            return View(model);
        }

        /// <summary>
        /// Customer Feedback in Service Request.
        /// </summary>
        /// <param name="model">Case Request View Model</param>
        /// <returns>Returns success page on submiting customer feedback form.</returns>
        [HttpPost]
        public ActionResult CustomerFeedback(CaseRequestViewModel model)
        {
            if (ModelState.IsValid && !Equals(model, null))
            {
                //PRFT Custom Code for Google reCaptcha : Start
                if (!RecaptchaValidation())
                {
                    return View();
                }
                //PRFT Custom Code for Google reCaptcha : End
                model.PortalId = PortalAgent.CurrentPortal.PortalId;
                model.CasePriorityId = MvcDemoConstants.CasePriorityId;
                model.CaseStatusId = MvcDemoConstants.CaseStatusId;
                model.CreatedDate = System.DateTime.Now;
                model.CaseTypeId = MvcDemoConstants.CaseTypeId;
                model.CaseOrigin = caseOriginCustomerFeedback;
                model.CaseRequestTitle = caseRequestCustomerFeedbackTitle;
                _caseRequestAgent.SaveCustomerFeedbackInfo(model);
                return RedirectToAction(customerFeedbackSuccess);
            }
            return View();
        }

        /// <summary>
        /// Customer Feedback Success Message
        /// </summary>
        /// <returns>Returns view customerFeedbackSuccessPage.</returns>
        public ActionResult CustomerFeedbackSuccess()
        {
            return View(customerFeedbackSuccessPage);
        }
        #endregion

        #region PRFT Custom Code
        private bool RecaptchaValidation()
        {
            bool isValid = false;
            TempData["CaptchaErrorMessage"] = string.Empty;

            var response = Request["g-recaptcha-response"];

            if (string.IsNullOrEmpty(response))
            {
                TempData["CaptchaErrorMessage"] = "Please validate Captcha.";
                return isValid;
            }
            //secret that was generated in key value pair
            string secret = ConfigurationManager.AppSettings["recaptchaPrivateKey"];
            var client = new WebClient();
            var reply = client.DownloadString(string.Format(ConfigurationManager.AppSettings["recaptchaValidationUrl"], secret, response));

            var captchaResponse = JsonConvert.DeserializeObject<CaptchaResponse>(reply);

            //when response is false check for the error message
            if (!captchaResponse.Success)
            {
                if (captchaResponse.ErrorCodes.Count <= 0)
                {
                    isValid = true;
                }
                else
                {
                    isValid = false;
                }

                var error = captchaResponse.ErrorCodes[0].ToLower();
                switch (error)
                {
                    case ("missing-input-secret"):
                        TempData["CaptchaErrorMessage"] = "The secret parameter is missing.";
                        break;
                    case ("invalid-input-secret"):
                        TempData["CaptchaErrorMessage"] = "The secret parameter is invalid or malformed.";
                        break;

                    case ("missing-input-response"):
                        TempData["CaptchaErrorMessage"] = "Please re-validate Captcha.";
                        break;
                    case ("invalid-input-response"):
                        TempData["CaptchaErrorMessage"] = "Please re-validate Captcha.";
                        break;

                    default:
                        TempData["CaptchaErrorMessage"] = "The captcha value is invalid or malformed.";
                        break;
                }
            }
            else
            {
                TempData["CaptchaErrorMessage"] = string.Empty;
                isValid = true;
            }

            return isValid;
        }
        #endregion
    }
}