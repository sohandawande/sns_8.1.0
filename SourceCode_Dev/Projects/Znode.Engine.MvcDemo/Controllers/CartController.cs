﻿using System;
using System.Web.Mvc;
using Znode.Engine.MvcDemo.Agents;
using Znode.Engine.MvcDemo.Helpers;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Controllers
{
    public class CartController : BaseController
    {
        private readonly ICartAgent _cartAgent;

        public CartController()
        {
            _cartAgent = new CartAgent();
        }

        public ActionResult Index()
        {
            //PRFT Custom Code for Google reCaptcha : Start
            //if(TempData["CaptchaErrorMessage"]!=null && !string.IsNullOrEmpty(Convert.ToString(TempData["CaptchaErrorMessage"])))
            //{
            //    ModelState.AddModelError("CaptchaError", TempData["CaptchaErrorMessage"].ToString());
            //}
            
            //PRFT Custom Code for Google reCaptcha : End
            Session[MvcDemoConstants.MultiCartModelSessionKey] = null;
            Session[MvcDemoConstants.BillingAddressKey] = null;
            Session[MvcDemoConstants.ShippingAddressKey] = null;
            var cartViewModel = _cartAgent.GetCart();
            _cartAgent.SetMessages(cartViewModel);
            return View("Cart", cartViewModel);
        }


        public ActionResult CartCount()
        {
            var count = _cartAgent.GetCartCount();
            return Content(count.ToString());
        }

        [HttpPost]
        public ActionResult RemoveItem(string guid)
        {
            bool removeItem = _cartAgent.RemoveItem(guid);
            TempData[MvcDemoConstants.RefreshCartItem] = true;
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult UpdateQuantity(string guid, int quantity)
        {
            _cartAgent.UpdateItem(guid, quantity);
            return RedirectToAction("Index");
        }

        public ActionResult ApplyCoupon(string coupon)
        {
            CartViewModel cartViewModel = _cartAgent.ApplyCoupon(coupon);
            string errorMsg = string.IsNullOrEmpty(coupon) ? Resources.ZnodeResource.RequiredCouponCode : Resources.ZnodeResource.ErrorCouponCode;
            return Json(new
             {
                 success = !cartViewModel.HasError,
                 message = "not set now",
                 isMultipleCoupon = PortalAgent.CurrentPortal.IsMultipleCouponAllowed,
                 data = new
                 {
                     promoCode = cartViewModel.Coupons,
                     subtotal = !Equals(cartViewModel.SubTotal, null) ? HelperMethods.FormatPriceWithCurrency(cartViewModel.SubTotal) : HelperMethods.FormatPriceWithCurrency(cartViewModel.SubTotal),
                     discountamount = HelperMethods.FormatPriceWithCurrency(0 - cartViewModel.Discount),
                     taxamount = HelperMethods.FormatPriceWithCurrency(cartViewModel.TaxCost),
                     ordertotal = HelperMethods.FormatPriceWithCurrency(cartViewModel.Total),

                 }
             }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult RemoveAllCartItems()
        {
            bool removeItem = _cartAgent.RemoveAllCartItems();
            TempData[MvcDemoConstants.RefreshCartItem] = true;
            return RedirectToAction("Index");
        }

        public ActionResult IsCouponUrlEnabled(string coupon)
        {
            return Json(
                new
                {
                    couponUrlIsEnabled = _cartAgent.IsCouponUrlEnabled(coupon)
                }, JsonRequestBehavior.AllowGet);
        }
        #region PRFT Custom Code        
        public ActionResult CartTotal()
        {
            var total = _cartAgent.GetCartTotal();
            return Content(total.ToString());
        }       
        #endregion
    }
}
