﻿using DevTrends.MvcDonutCaching;
using System;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.MvcDemo.Agents;
using Znode.Engine.MvcDemo.Helpers;
using Znode.Engine.MvcDemo.Maps;
using Znode.Engine.MvcDemo.ViewModels;
using System.Linq;

namespace Znode.Engine.MvcDemo.Controllers
{
    public class CategoryController : BaseController
    {
        private readonly ISearchAgent _searchAgent;
        private readonly ICategoryAgent _categoryAgent;
        private readonly PortalAgent _portalAgent;
        //PRFT Custom Code: Start
        private readonly IProductAgent _productAgent;
        //PRFT Custom Code: End

        public CategoryController()
        {
            _searchAgent = new SearchAgent();
            _categoryAgent = new CategoryAgent();
            _portalAgent = new PortalAgent();
            //PRFT Custom Code: Start
            _productAgent = new ProductAgent();
            //PRFT Custom Code: End
        }

        [DonutOutputCache(CacheProfile = "PortalCacheProfile")]
        [ChildActionOnly]
        public ActionResult TopLevelList()
        {
            var categories = _categoryAgent.GetCategories();

            return PartialView("_HeaderNav", categories);
        }

        [DonutOutputCache(CacheProfile = "PortalCacheProfile")]
        [ChildActionOnly]
        public ActionResult GetCategories()
        {
            var categories = _categoryAgent.GetCategories();

            if (categories == null && categories.Categories.Count == 0)
            {
                return new EmptyResult();
            }

            return PartialView("../Home/Categories", categories);
        }

        // [DonutOutputCache(CacheProfile = "CatalogCacheProfile")]
        [HttpGet]
        public ActionResult Index(SearchRequestModel searchRequestModel, string category, string seo)
        {
            searchRequestModel.RefineBy = SearchHelper.GetRefineBy();
            searchRequestModel.Sort = SearchHelper.GetSortBy(searchRequestModel.Sort);
            searchRequestModel.Category = !string.IsNullOrEmpty(category) ? category : WebUtility.UrlDecode(searchRequestModel.Category);

            CategoryViewModel model = _categoryAgent.GetCategoryByName(searchRequestModel.Category, true, searchRequestModel.CategoryId.GetValueOrDefault());

            searchRequestModel.Category = model.Name;
            searchRequestModel.CategoryId = model.CategoryId;

            #region ZNode code commented by PRFT
            
            //if (Equals(searchRequestModel.PageSize, null))
            //{
            //    if (!Equals(Session[MvcDemoConstants.PageSizeValue], null))
            //    {
            //        searchRequestModel.PageSize = Convert.ToInt32(Session[MvcDemoConstants.PageSizeValue]);
            //    }
            //    else
            //    {
            //        Session[MvcDemoConstants.PageSizeValue] = MvcDemoConstants.DefaultPageSize;
            //        searchRequestModel.PageSize = MvcDemoConstants.DefaultPageSize;
            //    }
            //}
            //else
            //{
            //    Session[MvcDemoConstants.PageSizeValue] = searchRequestModel.PageSize;
            //}

            //var result = _searchAgent.Search(searchRequestModel);            
            #endregion

            //PRFT Custom Code for PageSize on PLP: START
            if (searchRequestModel.PageNumber == null) { searchRequestModel.PageNumber = searchRequestModel.PageNumber.GetValueOrDefault(1); }

            //Get page size.
            var result = new KeywordSearchViewModel();
            searchRequestModel.PageSizes = _productAgent.GetPageSizesItems();
            if (searchRequestModel.PageSize == null)
            {
                var pageSize = searchRequestModel.PageSizes.Where(x => x.Selected = true).First();
                int perPageValue = 0;
                int.TryParse(pageSize.Value, out perPageValue);
                searchRequestModel.PageSize = perPageValue;
            }
            else 
            { 
                result.PageSizes = new SelectList(searchRequestModel.PageSizes, "Value", "Text", searchRequestModel.PageSize.ToString()).ToList(); 
            }
            result = _searchAgent.Search(searchRequestModel);
            result.PageSize = searchRequestModel.PageSize;
            result.PageSizes = searchRequestModel.PageSizes;
            result.PageNumber = searchRequestModel.PageNumber;
            result.OldPageNumber = searchRequestModel.PageNumber ?? MvcDemoConstants.DefaultPageNumber;
            int previousPage = (result.OldPageNumber - 1) == 0 ? 1 : (result.OldPageNumber - 1);
            int nextPage = (result.OldPageNumber >= result.TotalPages) ? result.OldPageNumber : result.OldPageNumber + 1;
            if (result.TotalPages < 1)
            {
                result.TotalPages = 1;
            }
            result.NextPageUrl = SearchMap.UpdateUrlQueryString("pagenumber", nextPage.ToString());
            result.PreviousPageurl = SearchMap.UpdateUrlQueryString("pagenumber", previousPage.ToString());

            result.FirstPageNumber = 1;
            result.FirstPageUrl = SearchMap.UpdateUrlQueryString("pagenumber", result.FirstPageNumber.ToString());
            result.LastPageNumber = result.TotalPages;
            result.LastPageUrl = SearchMap.UpdateUrlQueryString("pagenumber", result.LastPageNumber.ToString());
            //PRFT Custom Code:END

            #region ZNode code commented by PRFT
            //PRFT Commented Code : Start

            //if (!Equals(result, null) && !Equals(result.Products, null) && result.Products.Count == 0)
            //{
            //    if (!Equals(model, null) && !Equals(model.Subcategories, null) && model.Subcategories.Count > 0)
            //    {
            //        SubcategoriesViewModel _subcategories = _categoryAgent.GetSubCategories(model, searchRequestModel);
            //        if (!Equals(_subcategories, null) && !Equals(_subcategories.Subcategories, null))
            //        {
            //            ViewBag.Title = model.Title;
            //            _subcategories.ParentCategoryName = model.CategoryTitle;
            //            return View("Subcategories", _subcategories);
            //        }
            //    }

            //}
            //else
            //{
            //    SubcategoriesViewModel _subcategories = _categoryAgent.GetSubCategories(model, searchRequestModel);
            //    result.Subcategories = new SubcategoriesViewModel();
            //    result.Subcategories = _subcategories;
            //}

            //PRFT Commented Code : End
            #endregion
            //PRFT Custom Code for Subcategory: Start
            var categoryData = _categoryAgent.GetExpandedCategoryByName(searchRequestModel.Category);
            if (categoryData != null)
            {
                result.ShortDescription = categoryData.ShortDescription;

                if (result.Categories != null && result.Categories[0] != null &&
                   categoryData.Subcategories != null && categoryData.Subcategories.Count != 0)
                {
                    foreach (var subCategory in result.Categories[0].Hierarchy)
                    {
                        result.SubCategories.Add(_categoryAgent.GetExpandedCategoryByName(subCategory.Name));
                    }
                }
            }
            //PRFT Custom Code: End
           
            if (!(result.Categories != null && result.Categories.Count > 0))
            {
                result.HasError = true;
                result.ErrorMessage = Resources.ZnodeResource.ErrorNoCategoryListFound;

                return View("Landing", result);
            }

            //result.OldPageNumber = searchRequestModel.PageNumber ?? MvcDemoConstants.DefaultPageNumber;

            var categoryDetails = _categoryAgent.GetCategoryByName(searchRequestModel.Category, false, searchRequestModel.CategoryId.GetValueOrDefault());

            _categoryAgent.GetProductCategory(result, category, categoryDetails);


            PortalViewModel portal = PortalAgent.CurrentPortal;
            ViewBag.IsEnableCompare = portal.IsEnableCompare;
            ViewBag.CompareType = portal.CompareType;

            #region ZNode code commented by PRFT
            //PRFT Commented code: Start
            //int previousPage = (result.OldPageNumber - 1) == 0 ? 1 : (result.OldPageNumber - 1);
            //int nextPage = (result.OldPageNumber >= result.TotalPages) ? result.OldPageNumber : result.OldPageNumber + 1;
            //if (Session[MvcDemoConstants.PageSizeValue].Equals(-1))
            //{
            //    result.TotalPages = 1;
            //}
            //else
            //{
            //    var totalPages = Math.Ceiling(((decimal)result.TotalProducts / (decimal)Convert.ToInt32(Session[MvcDemoConstants.PageSizeValue])));
            //    result.TotalPages = (int)totalPages;
            //}
            //result.NextPageUrl = SearchMap.UpdateUrlQueryString("pagenumber", nextPage.ToString());
            //result.PreviousPageurl = SearchMap.UpdateUrlQueryString("pagenumber", previousPage.ToString());

            //PRFT Commented code: End
            #endregion

            string title = string.Empty;
            if (!string.IsNullOrEmpty(seo))
            {
                if (categoryDetails.SeoUrl.ToLower() != seo.ToLower())
                {
                    throw new HttpException(404, "SEO does not match");
                }
            }

            //Znode version 7.2.2  Start-  To show category banner 
            //PRFT Custom Code: Removed Unneccessary call
            //var categoryBanner = _categoryAgent.GetCategoryByName(searchRequestModel.Category, false, searchRequestModel.CategoryId.GetValueOrDefault()).CategoryBanner;
            var categoryBanner = categoryDetails.CategoryBanner;
            if (!string.IsNullOrEmpty(categoryBanner))
            {
                result.CategoryBanner = categoryBanner;
            }
            //Znode version 7.2.2  End-To show category banner
            //PRFT Custom code to add | S&S Sales on title
            ViewBag.Title = categoryDetails.Title;
            string titleSuffix = System.Configuration.ConfigurationManager.AppSettings["StoreName"];

            if (categoryDetails != null && !string.IsNullOrEmpty(categoryDetails.Title) && !categoryDetails.Title.Contains(titleSuffix))
            {
                ViewBag.Title = categoryDetails.Title + titleSuffix;
            }
            return View("Landing", result);
        }

        // This is a copy of Index, used for alternate category landing page view 
        [HttpGet]
        public ActionResult AltLanding(SearchRequestModel searchRequestModel, string category, string seo)
        {
            searchRequestModel.RefineBy = SearchHelper.GetRefineBy();
            searchRequestModel.Sort = SearchHelper.GetSortBy(searchRequestModel.Sort);
            searchRequestModel.Category = !string.IsNullOrEmpty(category) ? category : WebUtility.UrlDecode(searchRequestModel.Category);

            var result = _searchAgent.Search(searchRequestModel);           

            if (!(result!=null && result.Categories != null && result.Categories.Count > 0))
            {
                result.HasError = true;
                result.ErrorMessage = Resources.ZnodeResource.ErrorNoCategoryListFound;
            }

            result.OldPageNumber = searchRequestModel.PageNumber ?? MvcDemoConstants.DefaultPageNumber;

            var categoryDetails = _categoryAgent.GetCategoryByName(searchRequestModel.Category);
            string title = string.Empty;

            if (!string.IsNullOrEmpty(seo))
            {
                if (categoryDetails.SeoUrl.ToLower() != seo.ToLower())
                {
                    return new HttpNotFoundResult("SEO name does not match");
                }
            }

            ViewBag.Title = categoryDetails.Title;

            return View("LandingAlt", result);
        }

        [HttpGet]
        public JsonResult GetCategory(int categoryId)
        {
            var result = _categoryAgent.GetCategory(categoryId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public string GetPageSize()
        {
            return Convert.ToString(Session[MvcDemoConstants.PageSizeValue]);
        }

        #region PRFT Custom Code
        /// <summary>
        /// Get the lists of top level Categories to show on Home page
        /// </summary>
        /// <returns></returns>
        [DonutOutputCache(CacheProfile = "PortalCacheProfile")]
        [ChildActionOnly]
        public ActionResult GetHomePageCategoryList()
        {
            var categories = _categoryAgent.GetCategories();
            if (categories != null && categories.Categories != null && categories.Categories.Count > 0)
            {
                //Get only the category list by checking custom1 value. If there is value then its Manufacturer
                categories.Categories = categories.Categories.FindAll(q => string.IsNullOrEmpty(q.Custom1));
                return PartialView("_PRFTHomePageCategoryGrid", categories);
            }
            return null;
        }
        #endregion

        [DonutOutputCache(CacheProfile = "PortalCacheProfile")]
        [ChildActionOnly]
        public ActionResult FooterLevelList()
        {
            var categories = _categoryAgent.GetCategories();
            return PartialView("_PRFTFooterLeftSection", categories);
        }
    }
}
