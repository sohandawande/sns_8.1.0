﻿using System;
using System.Web.Mvc;
using Znode.Engine.MvcDemo.Agents;
using Znode.Engine.MvcDemo.Helpers;
using Znode.Engine.MvcDemo.Maps;
using Znode.Engine.MvcDemo.ViewModels;
using System.Linq;

namespace Znode.Engine.MvcDemo.Controllers
{
    [OutputCache(CacheProfile = "CatalogCacheProfile")]
    public class SearchController : BaseController
    {
        private readonly ISearchAgent _searchAgent;
        private readonly PortalAgent _portalAgent;
        private readonly ICategoryAgent _categoryAgent;
        //PRFT Custom Code: Start
        private readonly IProductAgent _productAgent;
        //PRFT Custom Code: Start

        public SearchController()
        {
            _searchAgent = new SearchAgent();
            _portalAgent = new PortalAgent();
            _categoryAgent = new CategoryAgent();
            //PRFT Custom Code: Start
            _productAgent = new ProductAgent();
            //PRFT Custom Code: Start
        }

        [HttpGet]
        public JsonResult GetSuggestions(string searchTerm, string category)
        {

            var result = _searchAgent.GetSuggestions(searchTerm, category);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        ///  // searchTerm = Keyword
        // category = Cateogry name Selected
        // sortBy = Should be passed in format as sort=2 in the querystring. If nothing is passed it will sory as sort=1
        // pageSize = Exact page size or -1 for Show All option or if nothing is passed, it is considered as 10 as default page size.
        // pageNumber = valid page number in integer
        // refine By = this should be generated as below. This is an example. This should be a valid facets associated. 
        // The Key and value will come dynamically from the Querystring. To test it uncomment the below and comment the var refineBy = GetRefineBy().
        // var refineBy = new Collection<KeyValuePair<string, IEnumerable<string>>>
        //{
        //    new KeyValuePair<string, IEnumerable<string>>("Price_Range", new string[] {"1 - 10"}),
        //    new KeyValuePair<string, IEnumerable<string>>("Color", new string[] {"Black"})
        //};
        /// </summary>
        /// <param name="searchRequestModel"></param>
        /// <returns></returns>
        public ActionResult Index(SearchRequestModel searchRequestModel)
        {
            searchRequestModel.RefineBy = SearchHelper.GetRefineBy();
            searchRequestModel.Sort = SearchHelper.GetSortBy(searchRequestModel.Sort);


            #region ZNode code commented by PRFT
            //PRFT commented Code: Start

            //if (Equals(searchRequestModel.PageSize, null))
            //{
            //    if (!Equals(Session[MvcDemoConstants.PageSizeValue], null))
            //    {
            //        searchRequestModel.PageSize = Convert.ToInt32(Session[MvcDemoConstants.PageSizeValue]);
            //    }
            //    else
            //    {
            //        Session[MvcDemoConstants.PageSizeValue] = MvcDemoConstants.DefaultPageSize;
            //        searchRequestModel.PageSize = MvcDemoConstants.DefaultPageSize;
            //    }
            //}
            //else
            //{
            //    Session[MvcDemoConstants.PageSizeValue] = searchRequestModel.PageSize;
            //}

            //var result = _searchAgent.Search(searchRequestModel);

            //result.OldPageNumber = searchRequestModel.PageNumber ?? MvcDemoConstants.DefaultPageNumber;

            //var categoryDetails = _categoryAgent.GetCategoryByName(searchRequestModel.Category);
            //_categoryAgent.GetProductCategory(result, searchRequestModel.Category, categoryDetails);

            //PRFT commented Code: End
            #endregion

            PortalViewModel portal = PortalAgent.CurrentPortal;
            ViewBag.IsEnableCompare = portal.IsEnableCompare;
            ViewBag.CompareType = portal.CompareType;

            #region ZNode code commented by PRFT
            //PRFT commented Code for Pagesize: Start

            //int previousPage = (result.OldPageNumber - 1) == 0 ? 1 : (result.OldPageNumber - 1);
            //int nextPage = (result.OldPageNumber >= result.TotalPages) ? result.OldPageNumber : result.OldPageNumber + 1;
            //if (Session[MvcDemoConstants.PageSizeValue].Equals(-1))
            //{
            //    result.TotalPages = 1;
            //}
            //else
            //{
            //    var totalPages = Math.Ceiling(((decimal)result.TotalProducts / (decimal)Convert.ToInt32(Session[MvcDemoConstants.PageSizeValue])));
            //    result.TotalPages = (int)totalPages;
            //}
            //result.NextPageUrl = SearchMap.UpdateUrlQueryString("pagenumber", nextPage.ToString());
            //result.PreviousPageurl = SearchMap.UpdateUrlQueryString("pagenumber", previousPage.ToString());

            //return View("BrowseSearch", result);

            //PRFT commented Code: End
            #endregion
            //PRFT Custom Code for Pagesize: Start
            if (searchRequestModel.PageNumber == null) { searchRequestModel.PageNumber = searchRequestModel.PageNumber.GetValueOrDefault(1); }

            searchRequestModel.PageSizes = _productAgent.GetPageSizesItems();

            var viewModel = new KeywordSearchViewModel();
            if (searchRequestModel.PageSize == null)
            {
                var pageSize = searchRequestModel.PageSizes.Where(x => x.Selected = true).First();
                int perPageValue = 0;
                int.TryParse(pageSize.Value, out perPageValue);
                searchRequestModel.PageSize = perPageValue;
            }
            else
            {
                viewModel.PageSizes = new SelectList(searchRequestModel.PageSizes, "Value", "Text", searchRequestModel.PageSize.ToString()).ToList();
            }

            viewModel = _searchAgent.Search(searchRequestModel);
            viewModel.PageSize = searchRequestModel.PageSize;
            viewModel.PageSizes = searchRequestModel.PageSizes;
            viewModel.PageNumber = searchRequestModel.PageNumber;

            viewModel.OldPageNumber = searchRequestModel.PageNumber ?? MvcDemoConstants.DefaultPageNumber;
            int previousPage = (viewModel.OldPageNumber - 1) == 0 ? 1 : (viewModel.OldPageNumber - 1);
            int nextPage = (viewModel.OldPageNumber >= viewModel.TotalPages) ? viewModel.OldPageNumber : viewModel.OldPageNumber + 1;

            if (viewModel.TotalPages < 1)
            {
                viewModel.TotalPages = 1;
            }

            viewModel.NextPageUrl = SearchMap.UpdateUrlQueryString("pagenumber", nextPage.ToString());
            viewModel.PreviousPageurl = SearchMap.UpdateUrlQueryString("pagenumber", previousPage.ToString());

            viewModel.FirstPageNumber = 1;
            viewModel.FirstPageUrl = SearchMap.UpdateUrlQueryString("pagenumber", viewModel.FirstPageNumber.ToString());
            viewModel.LastPageNumber = viewModel.TotalPages;
            viewModel.LastPageUrl = SearchMap.UpdateUrlQueryString("pagenumber", viewModel.LastPageNumber.ToString());

            if (viewModel.ErrorMessage != null)
            {
                viewModel.ErrorMessage = "";
            }

            return View("../Search/BrowseSearch", viewModel);
            //PRFT Custom Code: End
        }

        public string GetPageSize()
        {
            return Convert.ToString(Session[MvcDemoConstants.PageSizeValue]);
        }
    }
}
