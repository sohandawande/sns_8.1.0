﻿using Newtonsoft.Json;
using Resources;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.Agents;
using Znode.Engine.MvcDemo.Helpers;
using Znode.Engine.MvcDemo.ViewModels;
using System.Configuration;
using System.Net;


namespace Znode.Engine.MvcDemo.Controllers
{
    public class CheckoutController : BaseController
    {
        private readonly IShippingOptionAgent _shippingOptionAgent;
        private readonly ICheckoutAgent _checkoutAgent;
        private readonly IAccountAgent _accountAgent;
        private readonly ICartAgent _cartAgent;
        private const string BillingAddressModel_Name = "ShippingBillingAddressViewModel.BillingAddressModel.Name";
        private const string ShippingAddressModel_Name = "ShippingBillingAddressViewModel.ShippingAddressModel.Name";
        private const string ShippingAddressModelFirstName = "ShippingBillingAddressViewModel.ShippingAddressModel.FirstName";
        private const string ShippingAddressModelLastName = "ShippingBillingAddressViewModel.ShippingAddressModel.LastName";
        private const string ShippingAddressModelStreetAddress1 = "ShippingBillingAddressViewModel.ShippingAddressModel.StreetAddress1";
        private const string ShippingAddressModelCity = "ShippingBillingAddressViewModel.ShippingAddressModel.City";
        private const string ShippingAddressModelPostalCode = "ShippingBillingAddressViewModel.ShippingAddressModel.PostalCode";
        private const string ShippingAddressModelStateCode = "ShippingBillingAddressViewModel.ShippingAddressModel.StateCode";
        private const string ShippingAddressModelEmail = "ShippingBillingAddressViewModel.EmailAddress";
        private const string Login_Name = "ShippingBillingAddressViewModel.LoginViewmodel.UserName";
        private const string Login_Password = "ShippingBillingAddressViewModel.LoginViewmodel.Password";
        private const string SingleCheckoutCartItemView = "_SingleCheckoutCartItem";
        private const string SingleCheckoutView = "SingleCheckout";
        private const string CheckoutReceiptView = "CheckoutReceipt";
        private const string CartControllerName = "Cart";
        private const string IndexActionName = "Index";
        private const string HomeControllerName = "Home";
        private const string PaymentsViewModel_PaymentTypeId = "PaymentsViewModel.PaymentTypeId";
        private const string CheckoutPaymentView = "CheckoutPayment";
        private const string LoginActionName = "Login";
        private const string AccountControllerName = "Account";
        private const string ShippingActionName = "Shipping";
        private const string CheckoutAddressbookViewName = "CheckoutAddressbook";
        private const string CheckoutGuestShippingViewName = "CheckoutGuestShipping";
        private const string MultipleAddressCartViewName = "MultipleAddressCart";
        private const string CheckoutControllerName = "checkout";
        private const string BillingAddressModel_PhoneNumber = "ShippingBillingAddressViewModel.BillingAddressModel.PhoneNumber";
        private const string ShippingAddressModel_PhoneNumber = "ShippingBillingAddressViewModel.ShippingAddressModel.PhoneNumber";
        private const string BillingPhoneNumber = "BillingAddressModel.PhoneNumber";

        public CheckoutController()
        {
            _shippingOptionAgent = new ShippingOptionAgent();
            _checkoutAgent = new CheckoutAgent();
            _accountAgent = new AccountAgent();
            _cartAgent = new CartAgent();

        }        
        public ActionResult Index(FormCollection form)
        {
            //PRFT Custom Code for Google reCaptcha : Start
            if (!RecaptchaValidation())
            {
                return RedirectToAction("Index", "Cart");
            }

            //check if minimum order restriction is satisfied or not
            var cartViewModel = _cartAgent.GetCart();
            var isB2BCustomer = new PRFTHelper().IsB2BUser();
            decimal minimumOrderValue = Convert.ToDecimal(ConfigurationManager.AppSettings["minimumOrderValue"]);
            if (cartViewModel!=null && cartViewModel.Total < minimumOrderValue && !isB2BCustomer)
            {
                return RedirectToAction(IndexActionName, CartControllerName);
            }
            

            //PRFT Custom Code for Google reCaptcha : End
            if (User.Identity.IsAuthenticated)
            {
                if (Convert.ToBoolean(ViewBag.IsEnableSinglePageCheckout))
                {
                    TempData[MvcDemoConstants.RefreshCartItem] = true;
                    Session[MvcDemoConstants.CheckoutViewModelSessionKey] = null;
                    return RedirectToAction(SingleCheckoutView);
                }
                else
                {
                    AccountViewModel model = _accountAgent.GetAccountViewModel();
                    if (Equals(model, null))
                    {
                        FormsAuthentication.SignOut();
                        return RedirectToAction(IndexActionName, CartControllerName);
                    }
                    return RedirectToAction(ShippingActionName);
                }
            }

            return RedirectToAction(LoginActionName, AccountControllerName, new { returnUrl = "~/checkout" });
        }


        #region Znode Version 7.2.2

        // Shipping also contains billing process.
        [HttpGet]
        public ActionResult Shipping(bool isAddressChange = false)
        {
            //PRFT Custom Code:Start
            var cartViewModel = _cartAgent.GetCart();
            var isB2BCustomer = new PRFTHelper().IsB2BUser();
            decimal minimumOrderValue = Convert.ToDecimal(ConfigurationManager.AppSettings["minimumOrderValue"]);
            if (cartViewModel != null && cartViewModel.Total < minimumOrderValue && !isB2BCustomer)
            {
                return RedirectToAction(IndexActionName, CartControllerName);
            }
            //PRFT Custom Code:End
            ShippingBillingAddressViewModel viewModel = new ShippingBillingAddressViewModel();
            Collection<StateModel> states = _accountAgent.GetStates();
            if (Convert.ToBoolean(ViewBag.IsEnableSinglePageCheckout))
            {
                TempData[MvcDemoConstants.RefreshCartItem] = true;
                Session[MvcDemoConstants.CheckoutViewModelSessionKey] = null;
                return RedirectToAction(SingleCheckoutView);
            }
            if (User.Identity.IsAuthenticated)
            {
                AccountViewModel accountViewModel = _accountAgent.GetAccountViewModel();
                //PRFT Custom Code : Start
                ViewBag.IsB2B = false;
                if (!string.IsNullOrEmpty(accountViewModel.Custom2) && accountViewModel.Custom2.Equals("1"))
                {
                    ViewBag.IsB2B = true;
                }
                //PRFT Custom Code : End
                if (Equals(accountViewModel, null))
                {
                    FormsAuthentication.SignOut();
                    return RedirectToAction(IndexActionName, CartControllerName);
                }

                if (!Equals(accountViewModel.BillingAddress, null))
                {
                    accountViewModel.BillingAddress.SourceLink = MvcDemoConstants.CheckoutShippingBilling;
                }
                if (!Equals(accountViewModel.ShippingAddress, null))
                {
                    accountViewModel.ShippingAddress.SourceLink = MvcDemoConstants.CheckoutShippingBilling;
                }                
                if (!Equals(accountViewModel.BillingAddress, null) && !Equals(accountViewModel.ShippingAddress, null) && !isAddressChange)
                {
                    ViewBag.DisplayAddress = true;
                    if (!Equals(TempData[MvcDemoConstants.AddressValidationError], null))
                    {
                        accountViewModel.ErrorMessage = TempData[MvcDemoConstants.AddressValidationError].ToString();
                        accountViewModel.HasError = true;
                    }
                    return View(CheckoutAddressbookViewName, accountViewModel);
                }
                else
                {
                    SetAddressesInViewModel(viewModel);
                    viewModel.ShippingAddressModel.AccountId = accountViewModel.AccountId;
                    viewModel.BillingAddressModel.AccountId = accountViewModel.AccountId;
                    viewModel.IsAddressChange = isAddressChange;
                    FillStatesAndCountries(viewModel);
                    return View(CheckoutGuestShippingViewName, viewModel);
                }
            }
            else
            {
                if (Equals(Session[MvcDemoConstants.CartModelSessionKey], null))
                {
                    return RedirectToAction(LoginActionName, AccountControllerName);
                }
                SetAddressesInViewModel(viewModel);
            }

            FillStatesAndCountries(viewModel);
            
            return View(CheckoutGuestShippingViewName, viewModel);
        }

        #region Muliple shipping

        public ActionResult Multipleshipto()
        {
            var cartViewModel = _cartAgent.GetCart();
            if (cartViewModel.Items.Count > 0)
            {
                var addressModel = _checkoutAgent.GetAllAddesses();

                foreach (var adr in addressModel)
                {
                    cartViewModel.Addresses.Add(adr);
                }
                return View(MultipleAddressCartViewName, cartViewModel);
            }
            return RedirectToAction(IndexActionName, CartControllerName);
        }

        [HttpPost]
        public ActionResult ViewMultipleshipto()
        {
            var cartViewModel = _cartAgent.GetCart();

            var shippingAddress = _checkoutAgent.GetAllAddesses();

            foreach (var adr in shippingAddress)
            {
                cartViewModel.Addresses.Add(adr);
            }

            return View(MultipleAddressCartViewName, cartViewModel);

        }

        [HttpPost]
        public ActionResult RemoveItem(string guid, int quantity)
        {
            quantity = quantity - 1;
            _cartAgent.UpdateItem(guid, quantity);
            return RedirectToAction("Multipleshipto");
        }

        [HttpPost]
        public ActionResult UpdateQuantity(string guid, int quantity)
        {
            _cartAgent.UpdateItem(guid, quantity);
            return RedirectToAction("Multipleshipto");
        }

        public string ContinueCheckout(string multipleAddress)
        {
            IEnumerable<OrderShipmentDataModel> data = JsonConvert.DeserializeObject<IEnumerable<OrderShipmentDataModel>>(multipleAddress);

            Session[MvcDemoConstants.MultiCartShipmentSessionKey] = data;

            //Sort Multiple Shipping Address
            List<OrderShipmentDataModel> sortedAddress = _checkoutAgent.SortMultipleShippingAddress(data);

            //Create multicart
            var model = _checkoutAgent.CreateMultipleShoppingCart(sortedAddress, data);

            if (Equals(model, null))
            {
                Session[MvcDemoConstants.MultiCartShipmentSessionKey] = null;
            }


            return "True";
        }

        public string SetSelectedIndex()
        {
            IEnumerable<OrderShipmentDataModel> data = (IEnumerable<OrderShipmentDataModel>)Session[MvcDemoConstants.MultiCartShipmentSessionKey];
            return JsonConvert.SerializeObject(data);
        }
        public ActionResult MultiAddressCartReview()
        {
            var model = _checkoutAgent.GetMultiCartOrderReview();

            return View("MultipleCheckoutReview", model);
        }

        [HttpPost]
        public ActionResult CalculateShipping(int cartIndex, string shipping)
        {
            _checkoutAgent.CalculateMultiCartShipping(cartIndex, int.Parse(shipping));
            var model = _checkoutAgent.GetMultiCartOrderReview();

            return View("MultipleCheckoutReview", model);

        }

        #endregion

        /// <summary>
        /// To remove Billing modal state if checked "Billing address is same as shipping".
        /// </summary>
        /// <param name="models">ShippingBillingAddressViewModel</param>
        private void RemoveBillingModelState(ShippingBillingAddressViewModel models)
        {
            ModelState.Remove("BillingAddressModel.FirstName");
            ModelState.Remove("BillingAddressModel.LastName");
            ModelState.Remove("BillingAddressModel.Name");
            ModelState.Remove("BillingAddressModel.PostalCode");
            ModelState.Remove("BillingAddressModel.StateCode");
            ModelState.Remove("BillingAddressModel.StreetAddress1");
            ModelState.Remove("BillingAddressModel.City");
            ModelState.Remove("BillingAddressModel.CountryCode");
            ModelState.Remove(BillingPhoneNumber);
        }

        [HttpPost]
        public ActionResult Shipping(ShippingBillingAddressViewModel models)
        {
            //PRFT Custom Code: Start
            if(string.IsNullOrEmpty(models.BillingAddressModel.Name))
            {
                models.BillingAddressModel.Name = ConfigurationManager.AppSettings["BillingDisplayName"];
            }
            if (string.IsNullOrEmpty(models.ShippingAddressModel.Name))
            {
                models.ShippingAddressModel.Name = ConfigurationManager.AppSettings["ShippingDisplayName"];
            }
            //if(string.IsNullOrEmpty(models.EmailAddress))
            //{
            //    models.EmailAddress = PortalAgent.CurrentPortal.CustomerServiceEmail;
            //}
            //PRFT Custom Code: End
            bool isSameAsShippingAddress = models.BillingAddressModel.UseSameAsShippingAddress;
            ModelState.Remove(BillingPhoneNumber);
            if (!User.Identity.IsAuthenticated)
            {
                ModelState.Remove("ShippingAddressModel.Name");
                ModelState.Remove("BillingAddressModel.Name");
            }
            if (isSameAsShippingAddress)
            {
                //PRFT Custom Code: Start                
                models.BillingAddressModel.Name = models.ShippingAddressModel.Name;                
                //PRFT Custom Code: End
                RemoveBillingModelState(models);
            }
            if (!ModelState.IsValid)
            {
                FillStatesAndCountries(models);
                return View(CheckoutGuestShippingViewName, models);
            }
            if (models.BillingAddressModel.UseSameAsShippingAddress)
            {
                ModelState.RemoveFor<ShippingBillingAddressViewModel>(ob => ob.BillingAddressModel);
            }

            if (User.Identity.IsAuthenticated)
            {
                if (AnyExistingAddresses(models))
                {
                    models.ShippingAddressModel.AddressId = models.ShippingAddressId;
                    models.BillingAddressModel.AddressId = models.BillingAddressId;
                    models.BillingAddressModel.Email = models.EmailAddress;

                    AddressViewModel shippingModel = Equals(models.ShippingAddressModel.AccountId, 0) ? _checkoutAgent.GetAddressByAddressID(models.ShippingAddressModel) : models.ShippingAddressModel;                    
                    if (models.IsAddressChange)
                    {
                        if (!models.BillingAddressModel.UseSameAsShippingAddress)
                        {
                            if (Equals(models.BillingAddressModel.AddressId, models.ShippingAddressModel.AddressId))
                            {
                                models.BillingAddressModel.AddressId = 0;
                            }
                            models.BillingAddressModel.IsDefaultShipping = false;
                            models.ShippingAddressModel.IsDefaultBilling = false;
                        }
                        else
                        {
                            if (!Equals(models.BillingAddressModel.AddressId, models.ShippingAddressModel.AddressId))
                            {
                                models.ShippingAddressModel.IsDefaultBilling = true;
                                models.BillingAddressModel.IsDefaultBilling = false;
                                models.BillingAddressModel.IsDefaultShipping = false;
                            }
                        }
                    }

                    shippingModel.Email = models.EmailAddress;
                    _checkoutAgent.SaveShippingAddess(shippingModel);

                    if (Equals(_checkoutAgent.IsValidAddressForCheckout(shippingModel), false))
                    {
                        TempData[MvcDemoConstants.AddressValidationError] = Resources.ZnodeResource.AddressValidationFailed;
                        if (models.IsAddressChange)
                        {
                            models.ErrorMessage = TempData[MvcDemoConstants.AddressValidationError].ToString();
                            models.HasError = true;
                            FillStatesAndCountries(models);
                            return View(CheckoutGuestShippingViewName, models);
                        }
                        return RedirectToAction(ShippingActionName);
                    }

                    if (models.BillingAddressModel.UseSameAsShippingAddress && models.ShippingAddressModel.AddressId == models.BillingAddressModel.AddressId)
                    {
                        models.BillingAddressModel = models.ShippingAddressModel;
                    }

                    AddressViewModel billingModel = Equals(models.BillingAddressModel.AccountId, 0) ? _checkoutAgent.GetAddressByAddressID(models.BillingAddressModel) : models.BillingAddressModel;
                    models.BillingAddressModel = Equals(billingModel, null) ? new AddressViewModel() : billingModel;
                    models.BillingAddressModel.UseSameAsShippingAddress = isSameAsShippingAddress;
                    models.BillingAddressModel.Email = models.EmailAddress;

                    if (!models.BillingAddressModel.UseSameAsShippingAddress)
                    {
                        SaveBillingAddress(models.BillingAddressModel, models.BillingAddressModel.UseSameAsShippingAddress);
                    }
                    else
                    {
                        _checkoutAgent.SaveEmailAddress(models.BillingAddressModel);
                    }

                    if (Equals(_checkoutAgent.IsAddressExist(shippingModel), false))
                    {
                        TempData[MvcDemoConstants.AddressValidationError] = Resources.ZnodeResource.ShippingAddressRequired;
                        return RedirectToAction(ShippingActionName);
                    }
                    else if (Equals(_checkoutAgent.IsAddressExist(billingModel), false))
                    {
                        TempData[MvcDemoConstants.AddressValidationError] = Resources.ZnodeResource.BillingAddressRequired;
                        return RedirectToAction(ShippingActionName);
                    }

                    return RedirectToAction("ShippingMethods");
                }
                else
                {
                    if (string.IsNullOrEmpty(models.ShippingAddressModel.Name))
                    {
                        models.ShippingAddressModel.Name = string.Format("{0} {1}", models.ShippingAddressModel.FirstName, models.ShippingAddressModel.LastName);
                    }
                    if (string.IsNullOrEmpty(models.BillingAddressModel.Name))
                    {
                        models.BillingAddressModel.Name = string.Format("{0} {1}", models.BillingAddressModel.FirstName, models.BillingAddressModel.LastName);
                    }

                    if (ModelState.IsValid)
                    {
                        AccountViewModel accountViewModel = _accountAgent.GetAccountViewModel();

                        if (accountViewModel == null)
                        {
                            FormsAuthentication.SignOut();
                            return RedirectToAction(IndexActionName, CartControllerName);
                        }
                        SaveShippingBillingAddresses(models);
                        if (Equals(_checkoutAgent.IsValidAddressForCheckout(models.ShippingAddressModel), false))
                        {
                            FillStatesAndCountries(models);
                            models.HasError = true;
                            models.ErrorMessage = Resources.ZnodeResource.AddressValidationFailed;
                            return View(CheckoutGuestShippingViewName, models);
                        }

                        return RedirectToAction("ShippingMethods");
                    }
                    else
                    {
                        FillStatesAndCountries(models);
                        return View(CheckoutGuestShippingViewName, models);
                    }
                }
            }
            else
            {
                ModelState.Remove("ShippingAddressModel.Name");
                ModelState.Remove("BillingAddressModel.Name");
                ModelState.Remove(BillingPhoneNumber);
                if (ModelState.IsValid)
                {
                    if (_checkoutAgent.IsValidAddressForCheckout(models.ShippingAddressModel))
                    {
                        if (models.BillingAddressModel.UseSameAsShippingAddress)
                        {
                            models.ShippingAddressModel.UseSameAsShippingAddress = true;
                            models.BillingAddressModel = models.ShippingAddressModel;
                        }
                        else
                        {
                            models.BillingAddressModel.PhoneNumber = models.ShippingAddressModel.PhoneNumber;
                        }

                        models.ShippingAddressModel.Name = MvcDemoConstants.DefaultAddressKey;
                        models.BillingAddressModel.Name = MvcDemoConstants.DefaultAddressKey;
                        _checkoutAgent.SaveShippingAddess(models.ShippingAddressModel);
                        models.BillingAddressModel.Email = models.EmailAddress;
                        SaveBillingAddress(models.BillingAddressModel, models.BillingAddressModel.UseSameAsShippingAddress);
                        return RedirectToAction("ShippingMethods");

                    }
                    else
                    {
                        models.HasError = true;
                        models.ErrorMessage = Resources.ZnodeResource.AddressValidationFailed;
                    }
                }
                FillStatesAndCountries(models);
                return View(CheckoutGuestShippingViewName, models);
            }
        }

        [HttpPost]
        public ActionResult AddMultipleAddress(string guid, int shipto, int product)
        {
            ReviewOrderViewModel model = new ReviewOrderViewModel();

            if (shipto > 0)
            {
                AddressViewModel ShippingAddress = new AddressViewModel();
                ShippingAddress.AddressId = shipto;

                ShippingAddress = _checkoutAgent.GetAddressByAddressID(ShippingAddress);

                _checkoutAgent.SaveShippingAddess(ShippingAddress);
            }

            model = _checkoutAgent.GetCheckoutReview(9);

            model.ShippingAddress.SourceLink = MvcDemoConstants.RouteGuestCheckoutShipping;
            return View("CheckoutReview", model);

        }


        [HttpPost]
        public ActionResult ContinueCheckout()
        {
            ReviewOrderViewModel model = new ReviewOrderViewModel();

            AddressViewModel ShippingAddress = new AddressViewModel();
            ShippingAddress.AddressId = 10;
            model.ShippingAddress = ShippingAddress;
            for (int i = 0; i < 2; i++)
            {
                model = _checkoutAgent.GetCheckoutReview(9);
            }
            model.ShippingAddress.SourceLink = MvcDemoConstants.RouteGuestCheckoutShipping;
            return View("CheckoutReview", model);
        }

        #endregion

        [HttpGet]
        public ActionResult ShippingMethods()
        {
            //PRFT Custom Code:Start
            var cartViewModel = _cartAgent.GetCart();
            var isB2BCustomer = new PRFTHelper().IsB2BUser();
            decimal minimumOrderValue = Convert.ToDecimal(ConfigurationManager.AppSettings["minimumOrderValue"]);
            if (cartViewModel != null && cartViewModel.Total < minimumOrderValue && !isB2BCustomer)
            {
                return RedirectToAction(IndexActionName, CartControllerName);
            }
            //PRFT Custom Code:End

            var model = _shippingOptionAgent.GetShippingList();

            if (!model.ShippingOptions.Any())
            {
                return RedirectToAction("Review", new { shippingOptions = 0 });
            }

            model.ShippingAddress = _checkoutAgent.GetShippingAddess();
            if (Equals(model.ShippingAddress, null))
            {
                return RedirectToAction(LoginActionName, AccountControllerName);
            }
            model.ShippingAddress.SourceLink = MvcDemoConstants.RouteGuestCheckoutShipping;

            if (!Equals(TempData[MvcDemoConstants.CartError], null))
            {
                model.HasError = true;
                model.ErrorMessage = TempData[MvcDemoConstants.CartError].ToString();
            }

            return View("CheckoutShippingMethod", model);
        }

        [HttpGet]
        public ActionResult Review(int shippingOptions, string isMultipleShip)
        {

            //PRFT Custom Code:Start
            var cartViewModel = _cartAgent.GetCart();
            var isB2BCustomer = new PRFTHelper().IsB2BUser();
            decimal minimumOrderValue = Convert.ToDecimal(ConfigurationManager.AppSettings["minimumOrderValue"]);
            if (cartViewModel != null && cartViewModel.Total < minimumOrderValue && !isB2BCustomer)
            {
                return RedirectToAction(IndexActionName, CartControllerName);
            }
            //PRFT Custom Code:End

            ShippingOptionListViewModel checkMultipleShipping = new ShippingOptionListViewModel();
            if (checkMultipleShipping.IsMultipleShipTo)
            {
                _checkoutAgent.CalculateMultiCartShipping(shippingOptions);
                return RedirectToAction("MultiAddressCartReview");
            }
            else
            {
                var model = _checkoutAgent.GetCheckoutReview(shippingOptions);
                if (Equals(model, null))
                {
                    return RedirectToAction(LoginActionName, AccountControllerName);
                }
                if (model.HasError)
                {
                    TempData[MvcDemoConstants.CartError] = model.ErrorMessage;
                    return RedirectToAction("ShippingMethods");
                }
                model.ShippingAddress.SourceLink = MvcDemoConstants.RouteGuestCheckoutShipping;
                return View("CheckoutReview", model);
            }
        }

        //Znode Version 7.2.2
        public void SaveBillingAddress(AddressViewModel billAddress, bool? useSameAsShippingAddress)
        {
            if (User.Identity.IsAuthenticated && billAddress.AddressId > 0)
            {
                if (useSameAsShippingAddress.GetValueOrDefault(false))
                {
                    billAddress = _checkoutAgent.GetAddressByAddressID(billAddress);
                }
                _checkoutAgent.SaveBillingAddess(billAddress, true, useSameAsShippingAddress.GetValueOrDefault(false));
            }
            else
            {
                _checkoutAgent.SaveBillingAddess(billAddress, ModelState.IsValid, useSameAsShippingAddress.GetValueOrDefault(false));
                if (string.IsNullOrEmpty(billAddress.Email))
                {
                    ModelState.AddModelError("Email", Resources.ZnodeResource.RequiredEmailID);
                }
            }
        }

        [HttpGet]
        public ActionResult AddNewAddress(string returnUrl)
        {
            var cart = (ShoppingCartModel)Session[MvcDemoConstants.CartModelSessionKey];
            if (Equals(cart, null))
            {
                return RedirectToAction(LoginActionName, AccountControllerName);
            }
            var model = new AddressViewModel();

            model.Countries = _accountAgent.GetCountries(1);
            model.States = new Collection<StateModel>();

            if (!string.IsNullOrEmpty(returnUrl))
            {
                ViewBag.IsShipping = returnUrl.ToLower().Contains(MvcDemoConstants.RouteGuestCheckoutShipping);
            }

            return View("AddNewAddress", model);
        }

        [HttpPost]
        public ActionResult AddNewAddress(AddressViewModel model, string returnUrl)
        {
            //PRFT Custom Code: Start
            if (string.IsNullOrEmpty(model.Name))
            {
                model.Name = ConfigurationManager.AppSettings["BillingDisplayName"];
            }
            
            if (string.IsNullOrEmpty(model.Email))
            {
                model.Email = PortalAgent.CurrentPortal.CustomerServiceEmail;
            }
            
            PRFTHelper prftHelper = new PRFTHelper();
            if(prftHelper.IsB2BUser())
            {
                model.AddressExtn.Custom1 = Convert.ToString(Convert.ToInt32(CustomValue.One));
            }
            //PRFT Custom Code: End
            if (!string.IsNullOrEmpty(returnUrl))
            {
                ViewBag.IsShipping = returnUrl.ToLower().Contains(MvcDemoConstants.RouteGuestCheckoutShipping);
            }

            model.States = Equals(model.CountryCode, null) ? new Collection<StateModel>() : _accountAgent.GetStates();
            model.Countries = _accountAgent.GetCountries(1);
            if (ModelState.IsValid)
            {
                model = _accountAgent.UpdateAddress(model);
            }
            else
            {
                return View("AddNewAddress", model);
            }

            if (!model.HasError && !string.IsNullOrEmpty(returnUrl))
            {
                Session.Add("NewAddressID", model.AddressId);
                return Redirect(returnUrl);
            }


            return View("AddNewAddress", model);
        }

        [HttpGet]
        public ActionResult Payment()
        {
            var paymentModel = _checkoutAgent.GetPaymentModel();
            if (Equals(paymentModel, null))
            {
                return RedirectToAction(LoginActionName, AccountControllerName);
            }
            if (!Equals(paymentModel, null) && !Equals(paymentModel.BillingAddress, null) && string.IsNullOrEmpty(paymentModel.BillingAddress.Email))
            {
                paymentModel.BillingAddress.Email = Convert.ToString(Session[MvcDemoConstants.BillingEmailAddress]);
            }

            if (!Equals(paymentModel, null) && !Equals(paymentModel.PaymentOptions, null) && Equals(paymentModel.PaymentOptions.Count, 0))
            {
                Session.Add("ErrorMessage", ZnodeResource.FranchisePaymentOptionsNotAvailableMessage);
                _checkoutAgent.SetMessages(paymentModel);
            }

            var model = _checkoutAgent.GetMultiCartOrderReview();

            if (model != null)
            {
                decimal cartTotal = 0;
                decimal shippingCost = 0;
                decimal discount = 0;
                decimal giftcardamount = 0;
                decimal subtotal = 0;
                decimal taxcost = 0;
                decimal taxrate = 0;

                if (model.ReviewOrderList.Count() > 0)
                {
                    paymentModel.IsMultipleShipping = true;

                    model.ReviewOrderList.ForEach(reviewOrder =>
                    {
                        paymentModel.MultipleShippingAddress.Add(reviewOrder.ShippingAddress);
                        shippingCost += (decimal)reviewOrder.ShoppingCart.ShippingCost;
                        discount += (decimal)reviewOrder.ShoppingCart.Discount;
                        giftcardamount += (decimal)reviewOrder.ShoppingCart.GiftCardAmount;
                        subtotal += (decimal)reviewOrder.ShoppingCart.SubTotal;
                        taxcost += (decimal)reviewOrder.ShoppingCart.TaxCost;
                        taxrate += (decimal)reviewOrder.ShoppingCart.TaxRate;
                        paymentModel.Cart.ShippingOption.ShippingDescription = reviewOrder.ShippingOption.ShippingDescription;
                    });

                    paymentModel.PaymanetTotal = (subtotal + shippingCost + taxcost) - paymentModel.Cart.ShoppingCart.Discount;
                    paymentModel.Cart.ShoppingCart.Total = (subtotal + shippingCost + taxcost) - paymentModel.Cart.ShoppingCart.Discount;
                    paymentModel.Cart.ShoppingCart.ShippingCost = shippingCost;
                    paymentModel.Cart.ShoppingCart.Discount = model.Discount;
                    paymentModel.Cart.ShoppingCart.GiftCardAmount = giftcardamount;
                    paymentModel.Cart.ShoppingCart.SubTotal = subtotal;
                    paymentModel.Cart.ShoppingCart.TaxCost = taxcost;
                    paymentModel.Cart.ShoppingCart.TaxRate = taxrate;

                }

                if (model.ReviewOrderList.Count().Equals(1))
                {
                    paymentModel.ShippingAddress = model.ReviewOrderList[0].ShippingAddress;
                    paymentModel.IsMultipleShipping = false;
                }

            }
            //Multiple Shipping Address - End

            if (!Equals(TempData[MvcDemoConstants.PaypalError], null))
            {
                ModelState.AddModelError("", TempData[MvcDemoConstants.PaypalError].ToString());
            }
            if (!Equals(TempData[MvcDemoConstants.PaymentType], null))
            {
                ViewBag.DefaultPayment = TempData[MvcDemoConstants.PaymentType];
            }
            if (!Equals(TempData[MvcDemoConstants.CustomError], null))
            {
                paymentModel.ErrorMessage = TempData[MvcDemoConstants.CustomError].ToString();
                paymentModel.HasError = true;
            }

            if (Convert.ToBoolean(ViewBag.IsEnableSinglePageCheckout))
            {
                string message = !Equals(TempData[MvcDemoConstants.CustomError], null) ? TempData[MvcDemoConstants.CustomError].ToString() : string.Empty;
                return RedirectToAction(SingleCheckoutView, CheckoutControllerName, new { message = message });
            }
            return View(CheckoutPaymentView, paymentModel);
        }

        //PRFT Custom Code: Start
        [HttpPost]
        public ActionResult Payment(FormCollection form)
        {
            //PRFT Custom Code: Start
            string additionalInstruction = Convert.ToString(Request.Form["hdnAdditionalInstructions"]);
            //PRFT Custom Code: End
            var paymentModel = _checkoutAgent.GetPaymentModel();
            if (Equals(paymentModel, null))
            {
                return RedirectToAction(LoginActionName, AccountControllerName);
            }
            if (!Equals(paymentModel, null) && !Equals(paymentModel.BillingAddress, null) && string.IsNullOrEmpty(paymentModel.BillingAddress.Email))
            {
                paymentModel.BillingAddress.Email = Convert.ToString(Session[MvcDemoConstants.BillingEmailAddress]);
            }

            if (!Equals(paymentModel, null) && !Equals(paymentModel.PaymentOptions, null) && Equals(paymentModel.PaymentOptions.Count, 0))
            {
                Session.Add("ErrorMessage", ZnodeResource.FranchisePaymentOptionsNotAvailableMessage);
                _checkoutAgent.SetMessages(paymentModel);
            }

            var model = _checkoutAgent.GetMultiCartOrderReview();

            if (model != null)
            {
                //decimal cartTotal = 0;
                decimal shippingCost = 0;
                decimal discount = 0;
                decimal giftcardamount = 0;
                decimal subtotal = 0;
                decimal taxcost = 0;
                decimal taxrate = 0;

                if (model.ReviewOrderList.Count() > 0)
                {
                    paymentModel.IsMultipleShipping = true;

                    model.ReviewOrderList.ForEach(reviewOrder =>
                    {
                        paymentModel.MultipleShippingAddress.Add(reviewOrder.ShippingAddress);
                        shippingCost += (decimal)reviewOrder.ShoppingCart.ShippingCost;
                        discount += (decimal)reviewOrder.ShoppingCart.Discount;
                        giftcardamount += (decimal)reviewOrder.ShoppingCart.GiftCardAmount;
                        subtotal += (decimal)reviewOrder.ShoppingCart.SubTotal;
                        taxcost += (decimal)reviewOrder.ShoppingCart.TaxCost;
                        taxrate += (decimal)reviewOrder.ShoppingCart.TaxRate;
                        paymentModel.Cart.ShippingOption.ShippingDescription = reviewOrder.ShippingOption.ShippingDescription;
                    });

                    paymentModel.PaymanetTotal = (subtotal + shippingCost + taxcost) - paymentModel.Cart.ShoppingCart.Discount;
                    paymentModel.Cart.ShoppingCart.Total = (subtotal + shippingCost + taxcost) - paymentModel.Cart.ShoppingCart.Discount;
                    paymentModel.Cart.ShoppingCart.ShippingCost = shippingCost;
                    paymentModel.Cart.ShoppingCart.Discount = model.Discount;
                    paymentModel.Cart.ShoppingCart.GiftCardAmount = giftcardamount;
                    paymentModel.Cart.ShoppingCart.SubTotal = subtotal;
                    paymentModel.Cart.ShoppingCart.TaxCost = taxcost;
                    paymentModel.Cart.ShoppingCart.TaxRate = taxrate;

                }

                if (model.ReviewOrderList.Count().Equals(1))
                {
                    paymentModel.ShippingAddress = model.ReviewOrderList[0].ShippingAddress;
                    paymentModel.IsMultipleShipping = false;
                }

            }
            //Multiple Shipping Address - End

            //PRFT Custom Code : Start
            _checkoutAgent.GetOrderDetailsForOrderReceipt(additionalInstruction);
            //PRFT Custom Code : End

            if (!Equals(TempData[MvcDemoConstants.PaypalError], null))
            {
                ModelState.AddModelError("", TempData[MvcDemoConstants.PaypalError].ToString());
            }
            if (!Equals(TempData[MvcDemoConstants.PaymentType], null))
            {
                ViewBag.DefaultPayment = TempData[MvcDemoConstants.PaymentType];
            }
            if (!Equals(TempData[MvcDemoConstants.CustomError], null))
            {
                paymentModel.ErrorMessage = TempData[MvcDemoConstants.CustomError].ToString();
                paymentModel.HasError = true;
            }

            if (Convert.ToBoolean(ViewBag.IsEnableSinglePageCheckout))
            {
                string message = !Equals(TempData[MvcDemoConstants.CustomError], null) ? TempData[MvcDemoConstants.CustomError].ToString() : string.Empty;
                return RedirectToAction(SingleCheckoutView, CheckoutControllerName, new { message = message });
            }
            return View(CheckoutPaymentView, paymentModel);
        }

        //PRFT Custom Code: End

        [HttpPost]
        public ActionResult CreditCard(PaymentOptionViewModel model)
        {
            if (ModelState.IsValid)
            {
                var orderViewModel = _checkoutAgent.SubmitOrder(model, string.Empty);

                if (orderViewModel == null)
                {
                    return RedirectToAction(IndexActionName, CheckoutControllerName);
                }

                if (!orderViewModel.HasError)
                {
                    SaveOrderIdCookie(Convert.ToString(orderViewModel.OrderId));
                    //PRFT Custom Code : Start
                    //return View(CheckoutReceiptView, orderViewModel);
                    TempData["orderViewModel"] = orderViewModel;
                    return RedirectToAction("OrderReceipt");
                    //PRFT Custom Code : End
                }

                ModelState.AddModelError("", Resources.ZnodeResource.ErrorUnablePlaceOrder);
            }

            return View(CheckoutPaymentView, _checkoutAgent.GetPaymentModel(model));
        }

        [HttpGet]
        public ActionResult Cod()
        {
            return RedirectToAction(IndexActionName, "Home");
        }

        [HttpPost]
        public ActionResult Cod(PaymentOptionViewModel model)
        {

            var orderViewModel = _checkoutAgent.SubmitOrder(model, string.Empty);

            if (orderViewModel == null)
            {
                return RedirectToAction(IndexActionName, CheckoutControllerName);
            }

            if (!orderViewModel.HasError)
            {
                SaveOrderIdCookie(Convert.ToString(orderViewModel.OrderId));
                //PRFT Custom Code : Start
                //return View(CheckoutReceiptView, orderViewModel);
                TempData["orderViewModel"] = orderViewModel;
                return RedirectToAction("OrderReceipt");
                //PRFT Custom Code : End               
            }

            SetCustomError(orderViewModel.ErrorMessage.ToString());
            return RedirectToAction("Payment");
        }

        [HttpGet]
        public ActionResult PurchaseOrder()
        {
            return RedirectToAction(IndexActionName, "Home");
        }

        [HttpPost]
        public ActionResult PurchaseOrder(PaymentOptionViewModel model)
        {
            string purchaseOrderNumber = string.IsNullOrEmpty(model.PurchaseOrderNumber) ? string.Empty : model.PurchaseOrderNumber;

            var orderViewModel = _checkoutAgent.SubmitOrder(model, purchaseOrderNumber);

            if (orderViewModel == null)
            {
                return RedirectToAction(IndexActionName, CheckoutControllerName);
            }

            if (!orderViewModel.HasError)
            {
                SaveOrderIdCookie(Convert.ToString(orderViewModel.OrderId));
                //PRFT Custom Code : Start
                //return View(CheckoutReceiptView, orderViewModel);
                TempData["orderViewModel"] = orderViewModel;
                return RedirectToAction("OrderReceipt");
                //PRFT Custom Code : End                
            }

            SetCustomError(orderViewModel.ErrorMessage.ToString());
            return RedirectToAction("Payment");
        }

        [HttpGet]
        public ActionResult Paypal(string token, string payerId, int paymentOptionId)
        {
            if (string.IsNullOrEmpty(token) || string.IsNullOrEmpty(payerId))
            {
                return RedirectToAction("Payment");
            }

            var orderViewModel = _checkoutAgent.SubmitPayPalOrder(token, payerId, paymentOptionId);

            if (!orderViewModel.HasError)
            {
                //PRFT Custom Code : Start
                //return View(CheckoutReceiptView, orderViewModel);
                TempData["orderViewModel"] = orderViewModel;
                return RedirectToAction("OrderReceipt");
                //PRFT Custom Code : End                
            }

            ModelState.AddModelError("", Resources.ZnodeResource.ErrorUnablePlaceOrder);

            return RedirectToAction("Payment");
        }

        [HttpPost]
        public ActionResult Paypal(PaymentOptionViewModel model)
        {
            var orderViewModel = _checkoutAgent.SubmitOrder(model, string.Empty);

            if (orderViewModel == null)
            {
                return RedirectToAction(IndexActionName, CheckoutControllerName);
            }
            TempData[MvcDemoConstants.PaypalError] = orderViewModel.ErrorMessage;
            TempData[MvcDemoConstants.PaymentType] = MvcDemoConstants.PaypalPaymentType;
            return RedirectToAction("Payment");
        }

        public ActionResult ApplyGiftCard(string number)
        {
            if (!_checkoutAgent.IsValidGiftCard(number))
            {
                return Json(new
                {
                    success = false,
                    message = "Invalid Gift Card",
                    data = new
                    {
                        style = "error",
                    }
                }, JsonRequestBehavior.AllowGet);
            }

            var cartViewModel = _checkoutAgent.ApplyGiftCard(number);

            return Json(new
            {
                success = !cartViewModel.HasError,
                message = !cartViewModel.HasError ? cartViewModel.SuccessMessage : cartViewModel.ErrorMessage,
                data = new
                {
                    style = !cartViewModel.HasError ? "success" : "error",
                    applied = HelperMethods.FormatPriceWithCurrency(cartViewModel.GiftCardAmount),
                    balance = HelperMethods.FormatPriceWithCurrency(cartViewModel.GiftCardBalance),
                    total = HelperMethods.FormatPriceWithCurrency(cartViewModel.Total.GetValueOrDefault()),
                    message = cartViewModel.GiftCardMessage,
                    paid = cartViewModel.Total == 0,
                    giftcardamount = cartViewModel.GiftCardAmount
                }
            }, JsonRequestBehavior.AllowGet);

        }

        //PRFT Custom Code : Start
        public ActionResult OrderReceipt(OrderViewModel model)
        {
            model = TempData["orderViewModel"] as OrderViewModel;
            return View(CheckoutReceiptView, model);
        }
        //PRFT Custom Code : End
        #region Private methods

        private void SetAddressesInViewModel(ShippingBillingAddressViewModel viewModel)
        {
            var addresses = _checkoutAgent.GetBillingAddress();
            viewModel.ShippingAddressModel = addresses.ShippingAddress ?? new AddressViewModel();
            viewModel.BillingAddressModel = addresses.BillingAddress ?? new AddressViewModel();
            viewModel.EmailAddress = viewModel.BillingAddressModel.Email;
            viewModel.ShippingAddressModel.IsDefaultShipping = true;
            viewModel.BillingAddressModel.IsDefaultBilling = true;

            viewModel.BillingAddressId = viewModel.BillingAddressModel.AddressId;
            viewModel.ShippingAddressId = viewModel.ShippingAddressModel.AddressId;
        }

        private void SaveShippingBillingAddresses(ShippingBillingAddressViewModel models)
        {
            _checkoutAgent.SaveShippingAddess(models.ShippingAddressModel);
            models.BillingAddressModel.Email = models.EmailAddress;
            SaveBillingAddress(models.BillingAddressModel, models.BillingAddressModel.UseSameAsShippingAddress);
        }

        private bool AnyExistingAddresses(ShippingBillingAddressViewModel models)
        {
            return models.ShippingAddressId > 0 && models.BillingAddressId > 0;
        }

        private void FillStatesAndCountries(ShippingBillingAddressViewModel models)
        {
            AddressViewModel _billingAddress = (AddressViewModel)new AddressViewModel().Clone();
            AddressViewModel _shippingAddress = (AddressViewModel)new AddressViewModel().Clone();

            _billingAddress = (AddressViewModel)models.BillingAddressModel.Clone();
            _shippingAddress = (AddressViewModel)models.ShippingAddressModel.Clone();

            _billingAddress.Countries = _accountAgent.GetCountries(1);

            _shippingAddress.Countries = _accountAgent.GetCountries(2);

            _shippingAddress.States = _accountAgent.GetStates();
            _billingAddress.States = _accountAgent.GetStates();

            models.BillingAddressModel = (AddressViewModel)new AddressViewModel().Clone();

            models.ShippingAddressModel = (AddressViewModel)new AddressViewModel().Clone();

            models.BillingAddressModel = _billingAddress;
            models.ShippingAddressModel = _shippingAddress;

            models.CreateLogin = true;
        }

        /// <summary>
        /// This method stores orderId in cookie
        /// </summary>
        /// <param name="userId">string orderId that is to be saved in cookie</param>
        private void SaveOrderIdCookie(string orderId)
        {
            //Check if the browser support cookies 
            if ((HttpContext.Request.Browser.Cookies))
            {
                HttpContext.Response.Cookies.Remove(MvcDemoConstants.OrderIdCookieKey);
                HttpCookie cookieOrderId = new HttpCookie(MvcDemoConstants.OrderIdCookieKey);
                cookieOrderId.Value = orderId;
                cookieOrderId.Expires = DateTime.Now.AddDays(1);
                cookieOrderId.Path = "/checkout";
                HttpContext.Response.Cookies.Add(cookieOrderId);
            }
        }


        private bool RecaptchaValidation()
        {
            bool isValid = false;
            TempData["CaptchaErrorMessage"] = string.Empty;

            var response = Request["g-recaptcha-response"];
            
            if(string.IsNullOrEmpty(response))
            {                
                TempData["CaptchaErrorMessage"] = Resources.ZnodeResource.ValidateCartPageCaptchaMsg;
                return isValid;
            }
            //secret that was generated in key value pair
            string secret = ConfigurationManager.AppSettings["recaptchaV3PrivateKey"];
            var client = new WebClient();
            var reply = client.DownloadString(string.Format(ConfigurationManager.AppSettings["recaptchaValidationUrl"],secret, response));

            var captchaResponse = JsonConvert.DeserializeObject<CaptchaResponse>(reply);

            //when response is false check for the error message
            if (!captchaResponse.Success)
            {
                if (captchaResponse.ErrorCodes.Count <= 0) {
                    isValid = true;
                }
                else
                {
                    isValid = false;
                }
                
                var error = captchaResponse.ErrorCodes[0].ToLower();
                switch (error)
                {
                    case ("missing-input-secret"):
                        TempData["CaptchaErrorMessage"] = "The secret parameter is missing.";
                        break;
                    case ("invalid-input-secret"):
                        TempData["CaptchaErrorMessage"] = "The secret parameter is invalid or malformed.";
                        break;

                    case ("missing-input-response"):
                        TempData["CaptchaErrorMessage"] = "Please re-validate Captcha.";
                        break;
                    case ("invalid-input-response"):
                        TempData["CaptchaErrorMessage"] = "Please re-validate Captcha.";
                        break;

                    default:
                        TempData["CaptchaErrorMessage"] = "The captcha value is invalid or malformed.";
                        break;
                }
            }
            else
            {
                TempData["CaptchaErrorMessage"] = string.Empty;
                isValid = true;
            }

            return isValid;
        }

        #endregion

        #region Payment

        /// <summary>
        /// To process payment on Payment API using customerProfileId & customerPaymentId
        /// </summary>
        /// <param name="paymentSettingId">paymentSettingId</param>
        /// <param name="customerProfileId">customerProfileId</param>
        /// <param name="customerPaymentId">customerPaymentId</param>
        /// <returns>returns payment response</returns>
        public JsonResult SubmitPayment(string paymentSettingId, string customerProfileId, string customerPaymentId)
        {
            string creditCardResponse = _checkoutAgent.SubmitPayment(paymentSettingId, customerProfileId, customerPaymentId);
            return Json(new { status = true, Data = creditCardResponse }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Custom(string token, int paymentOptionId)
        {
            if (string.IsNullOrEmpty(token))
            {
                return RedirectToAction("Payment");
            }

            var orderViewModel = _checkoutAgent.SubmitCustomOrder(token, paymentOptionId);

            if (!orderViewModel.HasError)
            {
                SaveOrderIdCookie(Convert.ToString(orderViewModel.OrderId));
                if (!Equals(Session[MvcDemoConstants.CheckoutViewModelSessionKey], null))
                {
                    Session[MvcDemoConstants.CheckoutViewModelSessionKey] = null;
                }
                //PRFT Custom Code : Start
                //return View(CheckoutReceiptView, orderViewModel);
                TempData["orderViewModel"] = orderViewModel;
                return RedirectToAction("OrderReceipt");
                //PRFT Custom Code : End                
            }

            if (!string.IsNullOrEmpty(orderViewModel.ErrorMessage))
            {
                SetCustomError(orderViewModel.ErrorMessage.ToString());
                return RedirectToAction("Payment");
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        [HttpPost]
        public ActionResult Custom(PaymentOptionViewModel model)
        {
            var orderViewModel = _checkoutAgent.SubmitOrder(model, string.Empty);

            if (Equals(orderViewModel, null))
            {
                return RedirectToAction(IndexActionName, CheckoutControllerName);
            }

            TempData[MvcDemoConstants.CustomError] = orderViewModel.ErrorMessage;
            return RedirectToAction("Payment");
        }

        public ActionResult PaymentCustom(PaymentOptionViewModel model)
        {
            return PartialView("_PaymentCustom", model);
        }

        [HttpGet]
        public ActionResult GetPaymentGatwayNameByPaymetOptionId(int? paymentOptionId)
        {
            int? profileId = null;
            string gatwayName = string.Empty;
            if (!Equals(paymentOptionId, null))
            {
                gatwayName = _checkoutAgent.GetPaymentGatwayNameByPaymetOptionId(paymentOptionId, out profileId);
            }
            return Json(new
            {
                success = string.IsNullOrEmpty(gatwayName) ? false : true,
                name = gatwayName,
                profileId = profileId,
            }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// This method will be called if the payment needs to be placed by TwoCheckout provider
        /// </summary>
        /// <param name="paymentOptionId"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult twoco(int? paymentOptionId, string token)
        {
            string actionUrl = _checkoutAgent.CallTwoCoPaymentMethod(paymentOptionId, token);
            return Json(new { status = true, Data = actionUrl }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Single Checkout

        /// <summary>
        /// To apply gift card number for the logged in user.
        /// </summary>
        /// <param name="number">Gift card number</param>
        /// <returns></returns>
        public ActionResult ApplyGiftCardNumber(string number)
        {
            var cartViewModel = _checkoutAgent.ApplyGiftCard(number);
            return PartialView(SingleCheckoutCartItemView, cartViewModel);
        }

        /// <summary>
        /// To apply discount to the user.
        /// </summary>
        /// <param name="coupon">Discount will apply by using this Coupon number</param>
        /// <returns>Json object containing partial view html and coupons in cart.</returns>
        public ActionResult ApplyDiscount(string couponCode)
        {
            CartViewModel cartViewModel = _cartAgent.ApplyCoupon(couponCode);
            string singleChekcoutCartItemView = RenderRazorViewToString(SingleCheckoutCartItemView, cartViewModel);
            return Json(new { html = singleChekcoutCartItemView, data = cartViewModel.Coupons }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Update existing cart.
        /// </summary>
        /// <param name="model">View model of Checkout </param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetUpdatedCart(CheckoutViewModel model, bool isCountryStateTax = false)
        {
            ModelState.Remove(PaymentsViewModel_PaymentTypeId);
            model = SaveCheckoutInSession(model, isCountryStateTax);

            if (!string.IsNullOrEmpty(model.CartViewModel.ErrorMessage))
            {
                return RedirectToAction(SingleCheckoutView, CheckoutControllerName, new { message = model.CartViewModel.ErrorMessage });
            }


            return PartialView(SingleCheckoutCartItemView, model.CartViewModel);
        }

        /// <summary>
        /// To save checkout data in session.
        /// </summary>
        /// <param name="model">View model of Checkout</param>
        /// <returns>View model of Checkout</returns>
        private CheckoutViewModel SaveCheckoutInSession(CheckoutViewModel model, bool isCountryStateTax = false)
        {
            if (model.ShippingBillingAddressViewModel.BillingAddressModel.UseSameAsShippingAddress)
            {
                ModelState.RemoveFor<CheckoutViewModel>(ob => ob.ShippingBillingAddressViewModel.BillingAddressModel);
            }
            if (!model.ShippingBillingAddressViewModel.CreateLogin)
            {
                ModelState.Remove(Login_Name);
                ModelState.Remove(Login_Password);
            }
            ModelState.Remove(BillingAddressModel_Name);
            ModelState.Remove(ShippingAddressModel_Name);
            ModelState.Remove(BillingAddressModel_PhoneNumber);
            if (User.Identity.IsAuthenticated)
            {
                var accountModel = _accountAgent.GetAccountViewModel();
                if (!Equals(accountModel, null))
                {
                    model.ShippingBillingAddressViewModel.ShippingAddressId = accountModel.Addresses.FirstOrDefault(x => x.IsDefaultShipping).AddressId;
                    model.ShippingBillingAddressViewModel.BillingAddressId = accountModel.Addresses.FirstOrDefault(x => x.IsDefaultBilling).AddressId;
                }

                if (AnyExistingAddresses(model.ShippingBillingAddressViewModel))
                {
                    model.ShippingBillingAddressViewModel.ShippingAddressModel.AddressId = model.ShippingBillingAddressViewModel.ShippingAddressId;
                    model.ShippingBillingAddressViewModel.BillingAddressModel.AddressId = model.ShippingBillingAddressViewModel.BillingAddressId;
                    model.ShippingBillingAddressViewModel.BillingAddressModel.Email = model.ShippingBillingAddressViewModel.EmailAddress;
                    if (string.IsNullOrEmpty(model.ShippingBillingAddressViewModel.ShippingAddressModel.Name))
                    {
                        model.ShippingBillingAddressViewModel.ShippingAddressModel.Name = MvcDemoConstants.DefaultAddressKey;
                    }
                    if (string.IsNullOrEmpty(model.ShippingBillingAddressViewModel.BillingAddressModel.Name))
                    {
                        model.ShippingBillingAddressViewModel.BillingAddressModel.Name = MvcDemoConstants.DefaultAddressKey;
                    }
                    _checkoutAgent.SaveShippingAddess(model.ShippingBillingAddressViewModel.ShippingAddressModel);

                    if (!model.ShippingBillingAddressViewModel.BillingAddressModel.UseSameAsShippingAddress)
                    {
                        if (Equals(model.ShippingBillingAddressViewModel.BillingAddressModel.AddressId, model.ShippingBillingAddressViewModel.ShippingAddressModel.AddressId))
                        {
                            model.ShippingBillingAddressViewModel.BillingAddressModel.AddressId = 0;
                        }
                        model.ShippingBillingAddressViewModel.BillingAddressModel.IsDefaultShipping = false;
                        model.ShippingBillingAddressViewModel.ShippingAddressModel.IsDefaultBilling = false;
                    }
                    SaveBillingAddress(model.ShippingBillingAddressViewModel.BillingAddressModel, model.ShippingBillingAddressViewModel.BillingAddressModel.UseSameAsShippingAddress);

                }
                else
                {
                    if (string.IsNullOrEmpty(model.ShippingBillingAddressViewModel.ShippingAddressModel.Name))
                    {
                        model.ShippingBillingAddressViewModel.ShippingAddressModel.Name = string.Format("{0} {1}", model.ShippingBillingAddressViewModel.ShippingAddressModel.FirstName, model.ShippingBillingAddressViewModel.ShippingAddressModel.LastName);
                    }
                    if (string.IsNullOrEmpty(model.ShippingBillingAddressViewModel.BillingAddressModel.Name))
                    {
                        model.ShippingBillingAddressViewModel.BillingAddressModel.Name = string.Format("{0} {1}", model.ShippingBillingAddressViewModel.BillingAddressModel.FirstName, model.ShippingBillingAddressViewModel.BillingAddressModel.LastName);
                    }

                    AccountViewModel accountViewModel = _accountAgent.GetAccountViewModel();
                    model.AccountViewModel = accountViewModel;
                    if (Equals(accountViewModel, null))
                    {
                        FormsAuthentication.SignOut();
                        model.HasError = true;
                        model.ErrorMessage = "Logout";
                    }
                }
            }
            else
            {
                if (isCountryStateTax)
                {
                    ModelState.Remove(ShippingAddressModelFirstName);
                    ModelState.Remove(ShippingAddressModelLastName);
                    ModelState.Remove(ShippingAddressModelStreetAddress1);
                    ModelState.Remove(Login_Name);
                    ModelState.Remove(Login_Password);
                    ModelState.Remove(ShippingAddressModelCity);
                    ModelState.Remove(ShippingAddressModelPostalCode);
                    ModelState.Remove(ShippingAddressModelStateCode);
                    ModelState.Remove(ShippingAddressModelEmail);
                    ModelState.Remove(ShippingAddressModel_PhoneNumber);
                }

                if (ModelState.IsValid)
                {
                    model.ShippingBillingAddressViewModel.ShippingAddressModel.Name = MvcDemoConstants.DefaultAddressKey;
                    model.ShippingBillingAddressViewModel.BillingAddressModel.Name = MvcDemoConstants.DefaultAddressKey;
                    _checkoutAgent.SaveShippingAddess(model.ShippingBillingAddressViewModel.ShippingAddressModel);
                    model.ShippingBillingAddressViewModel.BillingAddressModel.Email = model.ShippingBillingAddressViewModel.EmailAddress;
                    if (!model.ShippingBillingAddressViewModel.BillingAddressModel.UseSameAsShippingAddress)
                    {
                        model.ShippingBillingAddressViewModel.BillingAddressModel.PhoneNumber = model.ShippingBillingAddressViewModel.ShippingAddressModel.PhoneNumber;
                    }
                    SaveBillingAddress(model.ShippingBillingAddressViewModel.BillingAddressModel, model.ShippingBillingAddressViewModel.BillingAddressModel.UseSameAsShippingAddress);

                    _checkoutAgent.SaveLogin(model.ShippingBillingAddressViewModel.LoginViewModel, model.ShippingBillingAddressViewModel.CreateLogin);
                }
            }
            var reviewModel = _checkoutAgent.GetCheckoutReview(model.ShippingOptions);
            model.CartViewModel = reviewModel.ShoppingCart;

            return model;
        }

        /// <summary>
        /// To submit single page checkout.
        /// </summary>
        /// <param name="model">View model of Checkout</param>
        /// <returns>Json Result</returns>
        [HttpPost]
        public JsonResult SubmitSingleCheckout(CheckoutViewModel model)
        {
            model.ErrorMessage = string.Empty;
            model = SaveCheckoutInSession(model);
            ModelState.Remove(BillingAddressModel_PhoneNumber);
            if (ModelState.IsValid)
            {

                var paymentViewModel = _checkoutAgent.GetPaymentModel();
                model.PaymentsViewModel.PaymentOptions = paymentViewModel.PaymentOptions;
                var PaymentOptionViewModel = paymentViewModel.PaymentOptions.First();
                if (model.PaymentsViewModel.PaymentTypeId > 0)
                {
                    PaymentOptionViewModel = model.PaymentsViewModel.PaymentOptions.Where(x => Equals(x.PaymentTypeId, model.PaymentsViewModel.PaymentTypeId)).First();
                }

                string purchaseOrderNumber = string.IsNullOrEmpty(model.PaymentsViewModel.PurchaseOrderNumber) ? string.Empty : model.PaymentsViewModel.PurchaseOrderNumber;
                var orderViewModel = _checkoutAgent.SubmitOrder(PaymentOptionViewModel, purchaseOrderNumber, model.AdditionalInstruction);

                if (Equals(orderViewModel, null))
                {
                    FillCheckoutModel(model);
                    model.HasError = true;
                    model.ErrorMessage = Resources.ZnodeResource.ErrorUnablePlaceOrder;
                    return new JsonResult { Data = model };
                }

                if (!orderViewModel.HasError)
                {
                    if (model.ShippingBillingAddressViewModel.CreateLogin)
                    {
                        _checkoutAgent.LoginUser(model);
                    }
                    if (!Equals(Session[MvcDemoConstants.CheckoutViewModelSessionKey], null))
                    {
                        Session[MvcDemoConstants.CheckoutViewModelSessionKey] = null;
                    }
                    return new JsonResult { Data = orderViewModel, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
                }
                else
                {
                    if (model.ShippingBillingAddressViewModel.CreateLogin)
                    {
                        _checkoutAgent.LoginUser(model);
                    }
                    model = SaveCheckoutInSession(model);
                    model.HasError = true;
                    model.ErrorMessage = orderViewModel.ErrorMessage;
                    return new JsonResult { Data = model };
                }
            }
            if (User.Identity.IsAuthenticated)
            {
                SetAddressesInViewModel(model.ShippingBillingAddressViewModel);
                model.ShippingBillingAddressViewModel.ShippingAddressModel.AccountId = model.AccountViewModel.AccountId;
                model.ShippingBillingAddressViewModel.BillingAddressModel.AccountId = model.AccountViewModel.AccountId;
            }
            FillCheckoutModel(model);
            model.HasError = true;
            model.ErrorMessage = "";
            return new JsonResult { Data = model };
        }

        [HttpPost]
        public JsonResult CreateSingleCheckoutAccount(CheckoutViewModel model)
        {
            model = SaveCheckoutInSession(model);
            var createLoginModel = _checkoutAgent.CreateSingleCheckoutAccount();
            if (createLoginModel.HasError)
            {
                model.HasError = true;
                model.ErrorMessage = string.IsNullOrEmpty(createLoginModel.ErrorMessage) ? Resources.ZnodeResource.ErrorUnableToCreateAccount : createLoginModel.ErrorMessage;
            }
            else
            {
                TempData[MvcDemoConstants.RefreshCartItem] = true;
            }
            return new JsonResult { Data = model };
        }

        [HttpPost]
        public JsonResult CreateCreditCardUserAccount(CheckoutViewModel model)
        {
            model = SaveCheckoutInSession(model);

            AccountViewModel createLoginModel = _checkoutAgent.CreateSingleCheckoutAccount();
            if (createLoginModel.HasError)
            {
                model.HasError = true;
                model.ErrorMessage = string.IsNullOrEmpty(createLoginModel.ErrorMessage) ? Resources.ZnodeResource.ErrorUnableToCreateAccount : createLoginModel.ErrorMessage;
            }
            else
            {
                TempData[MvcDemoConstants.RefreshCartItem] = true;
                bool addressFlag = _checkoutAgent.UpdateSingleCheckoutAddress();
                if (model.ShippingBillingAddressViewModel.CreateLogin)
                {
                    _checkoutAgent.LoginUser(model);
                }
                if (!addressFlag)
                {
                    model.HasError = true;
                    model.ErrorMessage = Resources.ZnodeResource.AddressValidationFailed;
                }
            }
            return new JsonResult { Data = model };
        }

        [HttpPost]
        public JsonResult CreateCustomerPaypalAccount(CheckoutViewModel model)
        {
            model = SaveCheckoutInSession(model);

            if (model.ShippingBillingAddressViewModel.CreateLogin)
            {
                AccountViewModel createLoginModel = _checkoutAgent.CreateSingleCheckoutAccount();
                if (createLoginModel.HasError)
                {
                    model.HasError = true;
                    model.ErrorMessage = string.IsNullOrEmpty(createLoginModel.ErrorMessage) ? Resources.ZnodeResource.ErrorUnableToCreateAccount : createLoginModel.ErrorMessage;
                    return new JsonResult { Data = model };
                }
                else
                {
                    TempData[MvcDemoConstants.RefreshCartItem] = true;
                    bool addressFlag = _checkoutAgent.UpdateSingleCheckoutAddress();
                    if (model.ShippingBillingAddressViewModel.CreateLogin)
                    {
                        _checkoutAgent.LoginUser(model);
                    }
                    if (!addressFlag)
                    {
                        model.HasError = true;
                        model.ErrorMessage = Resources.ZnodeResource.AddressValidationFailed;
                    }
                }
            }
            else
            {
                TempData[MvcDemoConstants.RefreshCartItem] = true;
                bool addressFlag = _checkoutAgent.IsValidAddressForCheckout(model.ShippingBillingAddressViewModel.ShippingAddressModel);
                if (!addressFlag)
                {
                    model.HasError = true;
                    model.ErrorMessage = Resources.ZnodeResource.AddressValidationFailed;
                }
            }
            return new JsonResult { Data = model };
        }

        /// <summary>
        /// To validate single page checkout details while Credit card process.
        /// </summary>
        /// <param name="model">CheckoutViewModel</param>
        /// <returns>JsonResult</returns>
        [HttpPost]
        public JsonResult ValidateSinglePageCheckout(CheckoutViewModel model)
        {
            model.ErrorMessage = string.Empty;
            model = SaveCheckoutInSession(model);

            if (ModelState.IsValid)
            {
                return Json(model, JsonRequestBehavior.AllowGet);
            }
            FillCheckoutModel(model);
            if (!model.HasError)
            {
                model.HasError = true;
                model.ErrorMessage = Resources.ZnodeResource.ErrorUnablePlaceOrder;
            }

            return new JsonResult { Data = model };
        }

        /// <summary>
        /// Fill checkout details. 
        /// </summary>
        /// <param name="model">View model of Checkout</param>
        private void FillCheckoutModel(CheckoutViewModel model, bool refreshCartItems = false)
        {
            FillStatesAndCountries(model.ShippingBillingAddressViewModel);

            var shippingmodel = _shippingOptionAgent.GetShippingList();

            if (!shippingmodel.ShippingOptions.Any())
            {
                shippingmodel = _checkoutAgent.GetCheckoutReview(0).ShippingOptionList;
            }

            shippingmodel.ShippingAddress = _checkoutAgent.GetShippingAddess();
            model.ShippingOptionListViewModel = shippingmodel;
            model.ShippingOptions = shippingmodel.ShippingOptions.First().ShippingId;

            model.PaymentsViewModel = _checkoutAgent.GetSinglePageCheckoutPaymentModel();
            ReviewOrderViewModel cartReview = _checkoutAgent.GetCheckoutReview(model.ShippingOptions);
            model.CartViewModel = Equals(cartReview, null) ? new CartViewModel() : cartReview.ShoppingCart;
        }

        /// <summary>
        /// To make single page checkout.
        /// </summary>
        /// <param name="message">Success message</param>
        /// <returns>redirect to SingleCheckout view</returns>
        public ActionResult SingleCheckout(string message = "")
        {
            CheckoutViewModel viewModel = new CheckoutViewModel();

            bool refreshCartItems = false;
            if (!Equals(TempData[MvcDemoConstants.RefreshCartItem], null))
            {
                refreshCartItems = Convert.ToBoolean(TempData[MvcDemoConstants.RefreshCartItem]);
                TempData[MvcDemoConstants.RefreshCartItem] = false;
            }

            if (User.Identity.IsAuthenticated)
            {
                AccountViewModel accountViewModel = _accountAgent.GetAccountViewModel();
                if (Equals(accountViewModel, null))
                {
                    FormsAuthentication.SignOut();
                    return RedirectToAction(IndexActionName, CartControllerName);
                }

                if (!Equals(accountViewModel.BillingAddress, null))
                {
                    accountViewModel.BillingAddress.SourceLink = MvcDemoConstants.CheckoutShippingBilling;
                }
                if (!Equals(accountViewModel.ShippingAddress, null))
                {
                    accountViewModel.ShippingAddress.SourceLink = MvcDemoConstants.CheckoutShippingBilling;
                }

                viewModel.AccountViewModel = accountViewModel;
                SetAddressesInViewModel(viewModel.ShippingBillingAddressViewModel);
                viewModel.ShippingBillingAddressViewModel.ShippingAddressModel.AccountId = accountViewModel.AccountId;
                viewModel.ShippingBillingAddressViewModel.BillingAddressModel.AccountId = accountViewModel.AccountId;

                if (Equals(viewModel.ShippingBillingAddressViewModel.BillingAddressId, viewModel.ShippingBillingAddressViewModel.ShippingAddressId))
                {
                    viewModel.ShippingBillingAddressViewModel.UseSameAsShippingAddress = true;
                }
            }
            else
            {
                SetAddressesInViewModel(viewModel.ShippingBillingAddressViewModel);

                viewModel.ShippingBillingAddressViewModel.BillingAddressId = viewModel.ShippingBillingAddressViewModel.BillingAddressModel.AddressId;
                viewModel.ShippingBillingAddressViewModel.ShippingAddressId = viewModel.ShippingBillingAddressViewModel.ShippingAddressModel.AddressId;
                viewModel.ShippingBillingAddressViewModel.UseSameAsShippingAddress = true;
            }

            if (refreshCartItems)
            {
                FillCheckoutModel(viewModel, refreshCartItems);
                Session[MvcDemoConstants.CheckoutViewModelSessionKey] = viewModel;
            }
            else
            {
                if (!Equals(Session[MvcDemoConstants.CheckoutViewModelSessionKey], null))
                {
                    viewModel = (CheckoutViewModel)Session[MvcDemoConstants.CheckoutViewModelSessionKey];
                }
                else
                {
                    FillCheckoutModel(viewModel, refreshCartItems);
                    Session[MvcDemoConstants.CheckoutViewModelSessionKey] = viewModel;
                }
            }

            if (!string.IsNullOrEmpty(message))
            {
                viewModel.ErrorMessage = message;
                viewModel.HasError = true;
            }
            if (!(viewModel.PaymentsViewModel.PaymentOptions.Count > 0))
            {
                viewModel.ErrorMessage = ZnodeResource.FranchisePaymentOptionsNotAvailableMessage;
                viewModel.HasError = true;
            }

            if (!(viewModel.CartViewModel.Count > 0))
            {
                return RedirectToAction(IndexActionName, CartControllerName);
            }

            ViewBag.DisplayAddress = false;
            return View(SingleCheckoutView, viewModel);
        }

        /// <summary>
        /// Checkout receipt after successfull payment.
        /// </summary>
        /// <param name="model">View model of Order</param>
        /// <returns>CheckoutReceipt View</returns>
        public ActionResult CheckoutReceipt(OrderViewModel model)
        {
            if (Equals(model, null) || Equals(model.OrderId, 0))
            {
                return RedirectToAction(IndexActionName, HomeControllerName);
            }
            return View(CheckoutReceiptView, model);
        }

        public JsonResult ProcessPayPalPayment(int paymentSettingId, string returnUrl, string cancelUrl)
        {
            string paypalData = _checkoutAgent.ProcessPaypalPayment(paymentSettingId, returnUrl, cancelUrl);
            return Json(paypalData, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// To handle error message of submit order and show user friendly message to user.
        /// </summary>
        /// <param name="errorMessage">errorMessage from service</param>
        private void SetCustomError(string errorMessage)
        {
            if (errorMessage.ToLower() == Resources.ZnodeResource.InvalidShippingAddress.ToLower())
            {
                TempData[MvcDemoConstants.CustomError] = errorMessage;
            }
            else if (errorMessage.ToLower() == Resources.ZnodeResource.AddressValidationFailed.ToLower())
            {
                TempData[MvcDemoConstants.CustomError] = errorMessage;
            }
            else
            {
                TempData[MvcDemoConstants.CustomError] = Resources.ZnodeResource.ErrorUnablePlaceOrder;
            }
        }

        /// <summary>
        /// To check entered address is valid for guest user.
        /// </summary>
        /// <param name="model">CheckoutViewModel</param>
        /// <returns>succcess/failure status</returns>
        [HttpPost]
        public JsonResult IsValidGuestUser(CheckoutViewModel model)
        {
            if (!Equals(model, null))
            {
                if (!_checkoutAgent.IsValidAddressForCheckout(model.ShippingBillingAddressViewModel.ShippingAddressModel))
                {
                    model.HasError = true;
                    model.ErrorMessage = Resources.ZnodeResource.AddressValidationFailed;
                }
            }
            else
            {
                model = new CheckoutViewModel();
                model.HasError = true;
                model.ErrorMessage = Resources.ZnodeResource.AddressValidationFailed;
            }
            return new JsonResult { Data = model };
        }

        //public JsonResult IsValidUser(CheckoutViewModel model)
        //{ 

        //}
        #endregion
    }

    public class CaptchaResponse
    {
        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("error-codes")]
        public List<string> ErrorCodes { get; set; }
    }
}
