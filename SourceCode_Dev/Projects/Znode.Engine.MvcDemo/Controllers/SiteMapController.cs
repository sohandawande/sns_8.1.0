﻿using System.Web.Mvc;
using Znode.Engine.MvcDemo.Agents;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Controllers
{
    public class SiteMapController : BaseController
    {
        private readonly ICategoryAgent _categoryAgent;

        public SiteMapController()
        {
            _categoryAgent = new CategoryAgent();
        }
        
        public ActionResult Index()
        {
            CategoryListViewModel categoryList = _categoryAgent.GetSiteMapDetails();

            //PRFT Custom Code:Start
            //Get only the category list by checking custom1 value. If there is value then its Manufacturer
            categoryList.Categories = categoryList.Categories.FindAll(q => string.IsNullOrEmpty(q.Custom1));

            return View(categoryList);
        }
    }
}