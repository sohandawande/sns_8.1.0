﻿using System.Web.Mvc;
using Znode.Engine.MvcDemo.Agents;
using Znode.Engine.MvcDemo.Helpers;
using Znode.Engine.MvcDemo.ViewModels;
using System.Collections.Generic;
using Znode.Libraries.Helpers.Extensions;

namespace Znode.Engine.MvcDemo.Controllers
{
    public partial class CaseRequestController : BaseController
    {
        #region Private String Variables For Extn
        private string caseOriginRequestAQuote = "Request A Quote Form";
        private string caseRequestRequestAQuoteTitle = "Request A Quote Submission";
        private string requestAQuoteSuccess = "RequestAQuoteSuccess";
        private string requestAQuoteSuccessPage = "RequestAQuoteSuccessPage";
        #endregion  

        #region Public Methods for Contact Us service For Extn
        

        /// <summary>
        /// PRFT Custom Code : Start
        /// Request A Quote Service Request.
        /// </summary>
        /// <returns>Returns Request A Quote page.</returns>
        [HttpGet]
        public ActionResult RequestAQuote()
        {
            CaseRequestViewModel model = new CaseRequestViewModel();
            model.IsRequestAQuoteForm = true;
            model.SalesReps = _caseRequestAgent.GetSalesRep();
            if (model.SalesReps != null)
            {
                model.SalesReps.Insert(0, new Api.Models.PRFTSalesRepresentative() { SalesRepID = 0, SaleRepNameWithLocation = Resources.ZnodeResource.PleaseSelectText, Email = string.Empty });
            }

            model.IsSalesRepFound = false;

            if (User.Identity.IsAuthenticated)
            {
                AccountViewModel accountModel = _accountAgent.GetAccountViewModel();
                if (!Equals(accountModel, null))
                {
                    if (!string.IsNullOrEmpty(accountModel.Custom1))
                    {
                        int salesRepID = 0;
                        int.TryParse(accountModel.Custom1, out salesRepID);
                        if (salesRepID > 0)
                        {
                            model.SalesRepId = salesRepID;
                            model.IsSalesRepFound = true;
                        }
                    }
                }
            }

            RequestAQuoteViewModel requestAQuoteModel = new RequestAQuoteViewModel();
            requestAQuoteModel.ContactChoices = new System.Collections.Generic.List<ContactChoice>();
            requestAQuoteModel.ContactChoices.Add(new ContactChoice() { ChoiceName = "Phone", IsSelected = false });
            requestAQuoteModel.ContactChoices.Add(new ContactChoice() { ChoiceName = "Fax", IsSelected = false });
            requestAQuoteModel.ContactChoices.Add(new ContactChoice() { ChoiceName = "Email", IsSelected = false });

            model.RequestAQuote = requestAQuoteModel;
            return View("RequestAQuote", model);
        }

        /// <summary>
        /// Contact Us in Service Request. 
        /// </summary>
        /// <param name="model">Case Request View Model</param>
        /// <returns>Returns success page on submiting contact us form.</returns>
        [HttpPost]
        public ActionResult RequestAQuote(CaseRequestViewModel model)
        {
            if (ModelState.IsValid && !Equals(model, null))
            {

                //PRFT Custom Code for Google reCaptcha : Start
                if (!RecaptchaValidation())
                {
                    if (model.SalesReps == null)
                    {
                        model.SalesReps = _caseRequestAgent.GetSalesRep();
                        if (model.SalesReps != null)
                        {
                            model.SalesReps.Insert(0, new Api.Models.PRFTSalesRepresentative() { SalesRepID = 0, SaleRepNameWithLocation = Resources.ZnodeResource.PleaseSelectText, Email = string.Empty });
                        }
                    }
                    model.IsSalesRepFound = false;
                    return View("RequestAQuote", model);
                }
                //PRFT Custom Code for Google reCaptcha : End

                model.PortalId = PortalAgent.CurrentPortal.PortalId;
                model.CasePriorityId = MvcDemoConstants.CasePriorityId;
                model.CaseStatusId = MvcDemoConstants.CaseStatusId;
                model.CreatedDate = System.DateTime.Now;
                model.CaseTypeId = MvcDemoConstants.CaseTypeId;

                string choiceSelected = string.Empty;
                foreach (ContactChoice choice in model.RequestAQuote.ContactChoices)
                {
                    if (choice.IsSelected)
                    {
                        choiceSelected += choice.ChoiceName + ",";
                    }
                }
                model.RequestAQuote.ContactChoiceSelected = choiceSelected.TrimEnd(',');
                model.CaseOrigin = caseOriginRequestAQuote;
                model.CaseRequestTitle = caseRequestRequestAQuoteTitle;

                //PRFT Custom Code : Start
                model.SalesReps = _caseRequestAgent.GetSalesRep();
                var SalesRep = model.SalesReps.ToList<Api.Models.PRFTSalesRepresentative>().Find(x => x.SalesRepID == model.SalesRepId);
                if (SalesRep != null)
                {
                    model.SaleRepNameWithLocation = SalesRep.SaleRepNameWithLocation;
                    model.SalesRepEmail = SalesRep.Email;
                }
                //PRFT Custom Code : Start

                _caseRequestAgent.SaveContactUsInfo(model);

                return RedirectToAction(requestAQuoteSuccess);
            }
            return View("RequestAQuote", model);
        }        

        /// <summary>
        /// Contact Us Success Message
        /// </summary>
        /// <returns>Returns view contactUsSuccessPage.</returns>
        public ActionResult RequestAQuoteSuccess()
        {
            return View(requestAQuoteSuccessPage);
        }
        
        #endregion
    }
}