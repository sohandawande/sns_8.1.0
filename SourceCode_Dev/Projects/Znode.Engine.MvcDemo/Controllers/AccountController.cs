﻿using DotNetOpenAuth.AspNet;
using DotNetOpenAuth.GoogleOAuth2;
using Microsoft.Web.WebPages.OAuth;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.Agents;
using Znode.Engine.MvcDemo.Helpers;
using Znode.Engine.MvcDemo.Maps;
using Znode.Engine.MvcDemo.ViewModels;
using System.Linq;
using Newtonsoft.Json;

namespace Znode.Engine.MvcDemo.Controllers
{

    public class AccountController : BaseController
    {
        private readonly IAccountAgent _accountAgent;
        private readonly ICartAgent _cartAgent;
        private readonly IAuthenticationAgent _authenticationAgent;
        //PRFT Custom Code: Start
        private readonly IPRFTCustomerUserAssociationAgent _customerUserAssociationAgent;
        private readonly IPRFTERPCustomerAgent _erpCustomerAgent;
        private readonly IPRFTERPOrderAgent _erpOrderAgent;
        //PRFT Custom Code: End
        LoginViewModel model;
        PRFTCustomerListViewModel customerList;//PRFT Custom Code

        public AccountController()
        {
            _accountAgent = new AccountAgent();
            _cartAgent = new CartAgent();
            _authenticationAgent = new AuthenticationAgent();
            _customerUserAssociationAgent = new PRFTCustomerUserAssociationAgent();
            customerList = new PRFTCustomerListViewModel();
            _erpCustomerAgent = new PRFTERPCustomerAgent();
            _erpOrderAgent = new PRFTERPOrderAgent();
        }

        public ActionResult Redirect()
        {

            // your code

            return RedirectPermanent("~/Account/Login");
        }

        public PartialViewResult LoginStatus()
        {
            return PartialView("_LoginPartial");
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult Signup()
        {
            //PRFT Custom Code : Start
            if (User.Identity.IsAuthenticated && _accountAgent.GetAccountViewModel() != null)
            {
                return RedirectToAction("Dashboard");
            }
            //PRFT Custom Code : End
            var viewModel = _accountAgent.GetRegisterViewModel();
            return View("Register", viewModel);
        }

        [HttpPost]
        public ActionResult Signup(RegisterViewModel model, string returnUrl)
        {
            bool status = false;
            string customertAccountId;
            if (ModelState.IsValid)
            {
                //PRFT Custom Code : Start               

                //PRFT Custom Code for Google reCaptcha : Start
                if (!RecaptchaValidation())
                {
                    return View("Register", model);
                }
                //PRFT Custom Code for Google reCaptcha : End

                customertAccountId = Convert.ToString(ConfigurationManager.AppSettings["genericUser"]);               
                //PRFT Custom Code : End
                model = _accountAgent.SignUp(model);

                if (!model.HasError)
                {
                    //PRFT Custom Code : Start 
                    var accountModel = _accountAgent.GetDashboard();
                    status = _accountAgent.SaveAssociateCustomer(accountModel.AccountId, customertAccountId);
                    Session["AssociatedCustomerExternalId"] = ConfigurationManager.AppSettings["GenericUserExternalID"];
                    Session["ERPCustomerType"] = ConfigurationManager.AppSettings["GenericERPCustomerType"];
                    //PRFT Custom Code : End 
                    if (string.IsNullOrEmpty(returnUrl))
                    {
                        _authenticationAgent.SetAuthCookie(model.UserName, true);
                        return RedirectToAction("Dashboard");
                    }

                    _authenticationAgent.RedirectFromLoginPage(model.UserName, true);
                    return new EmptyResult();
                }
            }

            return View("Register", model);
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult Login(string returnUrl)
        {
            TempData["CaptchaErrorMessage"] = null;
            if (User.Identity.IsAuthenticated && _accountAgent.GetAccountViewModel() != null)
            {
                var account = _accountAgent.GetAccountViewModel();
                if (account != null && !string.IsNullOrEmpty(account.Custom2) && account.Custom2.Equals("1"))
                {
                    return RedirectToAction("UserAdministration", new PRFTSubUsersRequestModel { ParentAccountId = account.AccountId.ToString() });
                }
                return RedirectToAction("Dashboard");
            }

            if (!string.IsNullOrEmpty(returnUrl) && returnUrl.Contains("checkout"))
            {
                return View("CheckoutLogin");
            }

            //Get user name from cookies
            GetLoginRememberMeCookie();

            //Set the Password Reset Message.
            SetResetPasswordSuccessMessage();

            //Set the Social Login Error Message
            SetSocialLoginErrorMessage();

            return View("Login", model);
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Login(LoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                var loginviewModel = _accountAgent.Login(model);

                if (!loginviewModel.HasError)
                {
                    _authenticationAgent.SetAuthCookie(model.Username, model.RememberMe);

                    //Remember me
                    if (checked(model.RememberMe) == true)
                    {
                        SaveLoginRememberMeCookie(model.Username);
                    }

                    //PRFT Custom Code: Start 
                    var accountModel = _accountAgent.GetDashboard();
                    if (accountModel != null && !string.IsNullOrEmpty(accountModel.ExternalId))
                    {
                        UpdateAccountDetailsFromERP(accountModel);
                    }
                    

                    //Recall is required to update session properly.
                    accountModel = _accountAgent.GetDashboard();

                    if (accountModel != null && accountModel.AccountId > 0)
                    {
                        customerList = _customerUserAssociationAgent.GetAssociatedCustomerList(accountModel.AccountId, null, null, null, null);
                    }
                    

                    if (customerList.Customers != null && customerList.Customers.Count > 1)
                    {
                        return View("PRFTAssociatedCustomerList");
                    }
                    else
                    {
                        if (customerList.Customers != null && customerList.Customers.Count.Equals(1))
                        {
                            Session["AssociatedCustomerExternalId"] = new PRFTHelper().ExternalId(customerList.Customers[0].ExternalId);
                        }
                        else if (new PRFTHelper().IsB2BUser())
                        {
                            Session["AssociatedCustomerExternalId"] = new PRFTHelper().ExternalId(accountModel.ExternalId);
                        }
                        else
                        {
                            Session["AssociatedCustomerExternalId"] = ConfigurationManager.AppSettings["GenericUserExternalID"];
                        }

                        var isMerged = _cartAgent.Merge();

                        if (isMerged)
                        {
                            return RedirectToAction("Index", "Cart");
                        }

                        if (string.IsNullOrEmpty(returnUrl) || (isMerged == false && Session["CallForPriceItemRemoved"] != null && Convert.ToBoolean(Session["CallForPriceItemRemoved"]) == true))
                        {
                            Session.Remove("CallForPriceItemRemoved");
                            return RedirectToAction("Dashboard");
                        }

                        _authenticationAgent.RedirectFromLoginPage(model.Username, true);
                        return new EmptyResult();
                    }
                }
                    //PRFT Custom Code: End
                    
                

                if (loginviewModel != null && loginviewModel.IsResetPassword)
                {
                    if (!string.IsNullOrEmpty(loginviewModel.Username) && !string.IsNullOrEmpty(loginviewModel.PasswordResetToken))
                    {
                        return View("Login", loginviewModel);
                    }
                    return View("ResetPassword", new ChangePasswordViewModel());
                }

                return View("Login", loginviewModel);
            }

            return View("Login", model);
        }

        [Authorize]
        public ActionResult Dashboard()
        {
            var model = _accountAgent.GetDashboard();           
            if (model == null)
            {
                return RedirectToAction("Login");
            }

            return View("Dashboard", model);
        }

        [Authorize]
        public ActionResult Wishlist(int? wishid)
        {
            var wishlistModel = new WishListViewModel();

            if (wishid.HasValue)
            {
                var deleteWishList = _accountAgent.DeleteWishList(wishid.GetValueOrDefault(0));

                return Json(new
                {
                    success = deleteWishList,
                    message = deleteWishList ? Resources.ZnodeResource.SuccessProductDeleteWishlist : Resources.ZnodeResource.ErrorProductDeleteWishlist,
                    data = new { style = deleteWishList ? "success" : "error", total = _accountAgent.GetAccountViewModel().WishlistCount }
                }, JsonRequestBehavior.AllowGet);
            }

            wishlistModel = _accountAgent.GetWishList();

            return View("Wishlist", wishlistModel);
        }

        [Authorize]
        [HttpGet]
        public ActionResult OrderHistory()
        {
            var model = _accountAgent.GetAccountViewModel();

            return View("OrderHistory", model);
        }


        [Authorize]
        public ActionResult OrderReceipt(int id)
        {
            var model = _accountAgent.GetOrderReceiptDetails(id);
            return View("OrderReceipt", model);
        }


        #region Znode Version 7.2.2 Reorder

        /// <summary>
        /// Znode Version 7.2.2
        /// Reorder complite order.
        /// </summary>
        /// <param name="id">orderId</param>
        /// <returns>Redirect to Cart View</returns>
        [Authorize]
        public ActionResult ReorderProducts(int id)
        {
            List<CartItemViewModel> cartItemList = new List<CartItemViewModel>();

            cartItemList = _accountAgent.GetReorderItems(id);

            //Add items into cart 
            foreach (var crt in cartItemList)
            {
                var viewModel = _cartAgent.Create(crt);
            }

            return RedirectToAction("Index", "Cart");
        }

        /// <summary>
        /// Znode Version 7.2.2
        /// reorder single item of order.
        /// </summary>
        /// <param name="id">orderLineItemId</param>
        /// <returns>Redirect to Cart View</returns>
        [Authorize]
        public ActionResult ReorderOrderLineItem(int id)
        {
            CartItemViewModel cartItem = new CartItemViewModel();

            //Get order item for reorder
            cartItem = _accountAgent.GetReorderSingleItems(id);

            //Add item into cart
            var viewModel = _cartAgent.Create(cartItem);
            return RedirectToAction("Index", "Cart");
        }


        //TODO
        //Znode Version 7.2.2
        [Authorize]
        public ActionResult ReorderProductsList(int id, int orderLineItemId, bool isOrder)
        {
            List<CartItemViewModel> cartItemList = new List<CartItemViewModel>();
            cartItemList = _accountAgent.GetReorderItemsList(id, orderLineItemId, isOrder);
            foreach (var crt in cartItemList)
            {
                var viewModel = _cartAgent.Create(crt);
            }
            return RedirectToAction("Index", "Cart");
        }



        #endregion

        [Authorize]
        [HttpGet]
        public ActionResult AddressBook()
        {
            var model = _accountAgent.GetAccountViewModel();

            _accountAgent.SetMessages(model);

            _accountAgent.SetAddressLink(model);

            ViewBag.ShowHeader = true;
            //PRFT Custom Code : Start
            ViewBag.IsB2B = new PRFTHelper().IsB2BUser();
            //ViewBag.IsB2B = false;
            //if (!string.IsNullOrEmpty(model.Custom2) && model.Custom2.Equals("1"))
            //{
            //    ViewBag.IsB2B = true;                
            //}
            //PRFT Custom Code : End
            if (model.Addresses != null)
                return View("AddressBook", model);

            return View("AddressBook", new AccountViewModel());
        }

        [Authorize]
        [HttpPost]
        public ActionResult AddressBook(int id, bool isDefaultBillingAddress)
        {
            var addressModel = _accountAgent.UpdateAddress(id, isDefaultBillingAddress);

            var model = _accountAgent.GetAccountViewModel();

            if (addressModel.HasError)
            {
                model.HasError = addressModel.HasError;
                model.ErrorMessage = addressModel.ErrorMessage;
            }
            else
            {
                model.SuccessMessage = addressModel.SuccessMessage;
            }

            return View("AddressBook", model);
        }

        [Authorize]
        [HttpGet]
        public ActionResult Address(int? id)
        {
            var model = new AddressViewModel();

            if (id.HasValue)
                model = _accountAgent.GetAddress(id.GetValueOrDefault(0));

            if (Equals(model, null))
                return RedirectToAction("AddressBook");

            //1 is used to get all billing enable countries list 
            model.Countries = _accountAgent.GetCountries(1);
            model.States = id.HasValue ? _accountAgent.GetStates() : new Collection<StateModel>();

            if (!Equals(TempData[MvcDemoConstants.DeleteAddressValidationError], null))
            {
                model.ErrorMessage = TempData[MvcDemoConstants.DeleteAddressValidationError].ToString();
                model.HasError = true;
            }
            return View("EditAddress", model);
        }

        [HttpPost]
        public ActionResult Address(AddressViewModel model)
        {
            if (ModelState.IsValid)
            {
                model = _accountAgent.UpdateAddress(model);
            }
            else
            {
                model.States = Equals(model.CountryCode, null) ? new Collection<StateModel>() : _accountAgent.GetStates();
                model.Countries = _accountAgent.GetCountries(1);
                return View("EditAddress", model);
            }

            if (model.HasError)
            {
                model.States = _accountAgent.GetStates();
                model.Countries = _accountAgent.GetCountries(1);
                return View("EditAddress", model);
            }

            return RedirectToAction("AddressBook");
        }


        [Authorize]
        [HttpPost]
        public ActionResult DeleteAddress(int id)
        {
            var addressViewModel = _accountAgent.DeleteAddress(id);

            if (!addressViewModel.HasError)
            {
                var model = _accountAgent.GetAccountViewModel();
                Session.Add("SuccessMessage", addressViewModel.SuccessMessage);
                return RedirectToAction("AddressBook");
            }
            else
            {
                TempData[MvcDemoConstants.DeleteAddressValidationError] = addressViewModel.ErrorMessage;
            }
            return RedirectToAction("Address", new { id = id });
        }

        [Authorize]
        [HttpGet]
        public ActionResult Reviews()
        {
            var model = _accountAgent.GetReviews();
            return View("ReviewHistory", model);
        }

        [Authorize]
        [HttpGet]
        public ActionResult EditProfile()
        {
            var model = _accountAgent.GetAccountViewModel();
            return View("EditProfile", model);
        }

        [Authorize]
        [HttpPost]
        public ActionResult EditProfile(AccountViewModel model)
        {
            if (ModelState.IsValid)
            {
                model = _accountAgent.UpdateProfile(model);
            }

            return View("EditProfile", model);
        }

        [Authorize]
        [HttpGet]
        public ActionResult Changepassword()
        {
            return View("ChangePassword", new ChangePasswordViewModel());
        }

        [Authorize]
        [HttpPost]
        public ActionResult Changepassword(ChangePasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                model.UserName = User.Identity.Name;
                model = _accountAgent.ChangePassword(model);
            }

            return View("ChangePassword", model);
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult ForgotPassword()
        {
            if (TempData["ResetPasswordLinkErrorMessage"] != null)
            {
                return View("ForgotPassword", new AccountViewModel { SuccessMessage = TempData["ResetPasswordLinkErrorMessage"].ToString(), HasError = false });
            }
            else
            {
                return View("ForgotPassword", new AccountViewModel());
            }
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult ForgotPassword(AccountViewModel model)
        {
            if (ModelState.IsValid)
            {
                model = _accountAgent.ForgotPassword(model);

                if (!Equals(model, null) && !model.HasError)
                {
                    TempData[MvcDemoConstants.ResetPasswordSuccessMessage] = model.SuccessMessage;
                    return RedirectToAction("Login");
                }
                return View("ForgotPassword", model);
            }



            return View("ForgotPassword", model);
        }

        [Authorize]
        public ActionResult Logout()
        {
            if (!Equals(Session[MvcDemoConstants.SocialLoginDetails], null))
            {
                AuthenticationResult data = Session[MvcDemoConstants.SocialLoginDetails] as AuthenticationResult;
                string redirectUrl = _accountAgent.SetSocialUserLogoutUrl(data);
                return Redirect(redirectUrl);
            }
            _accountAgent.Logout();

            //PRFT Custom Code : Start
            if (Request.IsAjaxRequest())
            {
                return Json(new
                {
                    success = true,
                    link = ConfigurationManager.AppSettings["DemoWebsiteUrl"] + "/",
                    data = new
                    {
                        style = "success",
                        link = ConfigurationManager.AppSettings["DemoWebsiteUrl"] + "/",                        
                    }
                }, JsonRequestBehavior.AllowGet);
            }
            //PRFT Custom Code : End
            return RedirectToAction("Login");
        }

        #region Znode 7.2.2
        //Reset user password using link, passed in email. Below method reads the password token & user name.

        [AllowAnonymous]
        [HttpGet]
        public ActionResult ResetPassword(string passwordToken, string userName)
        {

            passwordToken = WebUtility.UrlDecode(passwordToken);
            userName = WebUtility.UrlDecode(userName);
            var resetPassword = new ChangePasswordViewModel();
            resetPassword.UserName = userName;
            resetPassword.PasswordToken = passwordToken;

            //Set ResetPasword flag, use to hide Old Password field in View.
            resetPassword.IsResetPassword = true;
            ResetPasswordStatusTypes enumstatus;

            enumstatus = _accountAgent.CheckResetPasswordLinkStatus(resetPassword);
            switch (enumstatus)
            {
                case ResetPasswordStatusTypes.Continue:
                    {
                        return View("ResetPassword", resetPassword);
                    }
                case ResetPasswordStatusTypes.LinkExpired:
                    {
                        TempData["ResetPasswordLinkErrorMessage"] = Resources.ZnodeResource.ResetPasswordLinkError;
                        return RedirectToAction("ForgotPassword");
                    }
                case ResetPasswordStatusTypes.TokenMismatch:
                    {
                        TempData["ResetPasswordLinkErrorMessage"] = Resources.ZnodeResource.ResetPasswordLinkError;
                        return RedirectToAction("ForgotPassword");
                    }
                case ResetPasswordStatusTypes.NoRecord:
                    {
                        TempData["ResetPasswordLinkErrorMessage"] = Resources.ZnodeResource.ResetPasswordLinkError;
                        return RedirectToAction("ForgotPassword");
                    }
                default:
                    {
                        TempData["ResetPasswordLinkErrorMessage"] = Resources.ZnodeResource.ResetPasswordLinkError;
                        return RedirectToAction("ForgotPassword");
                    }
            }
        }

        [HttpPost]
        public ActionResult ResetPassword(ChangePasswordViewModel model)
        {
            if (!string.IsNullOrEmpty(model.PasswordToken))
            {
                ModelState.Remove("OldPassword");
            }
            model.IsResetPassword = true;
            if (ModelState.IsValid)
            {
                var changePasswordModel = _accountAgent.ChangePassword(model);

                if (!changePasswordModel.HasError)
                {
                    var loginModel = new LoginViewModel()
                    {
                        Username = model.UserName,
                        Password = model.NewPassword,
                    };
                    var accountViewModel = _accountAgent.Login(loginModel);

                    if (!accountViewModel.HasError)
                    {
                        Session.Add("SuccessMessage", changePasswordModel.SuccessMessage);
                        _authenticationAgent.SetAuthCookie(loginModel.Username, true);

                        //PRFT Custom Code : Start
                        var accountModel = _accountAgent.GetDashboard();
                        if (accountModel != null && !string.IsNullOrEmpty(accountModel.ExternalId))
                        {
                            UpdateAccountDetailsFromERP(accountModel);
                        }

                        if (accountModel != null && accountModel.AccountId > 0)
                        {
                            customerList = _customerUserAssociationAgent.GetAssociatedCustomerList(accountModel.AccountId, null, null, null, null);
                        }

                        if (customerList.Customers != null && customerList.Customers.Count > 1)
                        {
                            return View("PRFTAssociatedCustomerList");
                        }
                        else
                        {
                            if (customerList.Customers != null && customerList.Customers.Count.Equals(1))
                            {
                                Session["AssociatedCustomerExternalId"] = new PRFTHelper().ExternalId(customerList.Customers[0].ExternalId); 
                            }
                            else if (new PRFTHelper().IsB2BUser())
                            {
                                Session["AssociatedCustomerExternalId"] = new PRFTHelper().ExternalId(accountModel.ExternalId);
                            }
                            else
                            {
                                Session["AssociatedCustomerExternalId"] = ConfigurationManager.AppSettings["GenericUserExternalID"];
                            }
                        }
                        //PRFT Custom Code : End

                        return RedirectToAction("Dashboard");
                    }
                }
                return View("ResetPassword", changePasswordModel);
            }
            return View("ResetPassword", (!string.IsNullOrEmpty(model.PasswordToken)) ? model : new ChangePasswordViewModel());

        }

        /// <summary>
        /// Remember me function for logged in user on remember me selection
        /// This function is used to save user name in cookies.
        /// </summary>
        /// <param name="userId">string userId</param>
        private void SaveLoginRememberMeCookie(string userId)
        {
            //Check if the browser support cookies 
            if ((HttpContext.Request.Browser.Cookies))
            {
                HttpCookie cookieLoginRememberMe = new HttpCookie("loginCookie");
                cookieLoginRememberMe.Values["loginCookie"] = userId;
                cookieLoginRememberMe.Expires = DateTime.Now.AddDays(Convert.ToDouble(ConfigurationManager.AppSettings["CookieExpiresValue"]));
                HttpContext.Response.Cookies.Add(cookieLoginRememberMe);
                cookieLoginRememberMe.HttpOnly = true;
            }
        }

        /// <summary>
        /// Remember me function for logged in user on remember me selection
        /// This method is used to get user name value from cookies.
        /// </summary>
        private void GetLoginRememberMeCookie()
        {
            if ((HttpContext.Request.Browser.Cookies))
            {
                if (HttpContext.Request.Cookies["loginCookie"] != null)
                {
                    HttpCookie cookieRememberMe = HttpContext.Request.Cookies["loginCookie"];
                    if (cookieRememberMe != null)
                    {
                        var loginName = HttpUtility.HtmlEncode(cookieRememberMe.Values["loginCookie"]);
                        model = new LoginViewModel();
                        model.Username = loginName;
                        model.RememberMe = true;
                    }
                }
            }
        }

        /// <summary>
        /// This function is used to get all state list by countryCode
        /// </summary>
        /// <param name="countryCode">string countryCode</param>
        /// <returns>returns state list</returns>
        [HttpGet]
        public JsonResult GetStateByCountryCode(string countryCode)
        {
            var result = _accountAgent.GetStateByCountryCode(countryCode);
            return Json(result, JsonRequestBehavior.AllowGet);

        }

        [AllowAnonymous]
        public JsonResult SignUpForNewsLetter(string emailId)
        {
            bool status = false;
            string message = string.Empty;
            if (!string.IsNullOrEmpty(emailId))
            {
                status = _accountAgent.SignUpForNewsLetter(new NewsLetterSignUpViewModel() { Email = emailId }, out message);

                message = (!string.IsNullOrEmpty(message))
                    ? status ? Resources.ZnodeResource.NewsLetterSignUpSuccess : message
                    : status ? Resources.ZnodeResource.NewsLetterSignUpSuccess : Resources.ZnodeResource.NewsLetterSignUpError;

            }
            return Json(new { sucess = status, message = message });
        }

        [Authorize]
        [HttpGet]
        public ActionResult GiftCardHistory()
        {
            var model = _accountAgent.GetAccountViewModel();

            return View("GiftCardHistory", model);
        }

        /// <summary>
        /// Set the Reset Password success message in Login View Model
        /// </summary>
        private void SetResetPasswordSuccessMessage()
        {
            if (!Equals(TempData[MvcDemoConstants.ResetPasswordSuccessMessage], null) && !string.IsNullOrEmpty(Convert.ToString(TempData[MvcDemoConstants.ResetPasswordSuccessMessage])))
            {
                if (Equals(model, null))
                {
                    model = new LoginViewModel();
                }
                model.SuccessMessage = Convert.ToString(TempData[MvcDemoConstants.ResetPasswordSuccessMessage]);
            }
        }

        /// <summary>
        /// Set the Social Login Error message in Login View Model
        /// </summary>
        private void SetSocialLoginErrorMessage()
        {
            if (!Equals(TempData[MvcDemoConstants.SocialLoginFailedMessage], null) && !string.IsNullOrEmpty(Convert.ToString(TempData[MvcDemoConstants.SocialLoginFailedMessage])))
            {
                if (Equals(model, null))
                {
                    model = new LoginViewModel();
                }
                model.HasError = true;
                model.ErrorMessage = Convert.ToString(TempData[MvcDemoConstants.SocialLoginFailedMessage]);
            }
        }
        #endregion


        [AllowAnonymous]
        [ChildActionOnly]
        public ActionResult ExternalLoginsList(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return PartialView("_ExternalLoginsListPartial", OAuthWebSecurity.RegisteredClientData);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            return new ExternalLoginResult(provider, Url.Action("ExternalLoginCallback", new { ReturnUrl = returnUrl }));
        }

        [AllowAnonymous]
        public ActionResult ExternalLoginCallback(string returnUrl)
        {
            GoogleOAuth2Client.RewriteRequest();
            AuthenticationResult result = OAuthWebSecurity.VerifyAuthentication(Url.Action("ExternalLoginCallback", new { ReturnUrl = returnUrl }));
            if (!result.IsSuccessful)
            {
                TempData[MvcDemoConstants.SocialLoginFailedMessage] = Resources.ZnodeResource.ErrorSocialLoginFailed;
                return RedirectToAction("Login");
            }
            Session[MvcDemoConstants.SocialLoginDetails] = result;
            AccountViewModel accountModel = _accountAgent.SocialLogin(result.Provider, result.ProviderUserId, createPersistentCookie: false);

            if (!accountModel.HasError)
            {
                _authenticationAgent.SetAuthCookie(accountModel.UserName, true);
                var isMerged = _cartAgent.Merge();
                if (isMerged)
                {
                    return RedirectToAction("Index", "Cart");
                }
                if (string.IsNullOrEmpty(returnUrl))
                {
                    return RedirectToAction("Dashboard");
                }
                _authenticationAgent.RedirectFromLoginPage(accountModel.UserName, true);
                return new EmptyResult();
            }
            else if (accountModel.IsLockedUser)
            {
                TempData[MvcDemoConstants.SocialLoginFailedMessage] = accountModel.ErrorMessage;
                return RedirectToAction("Login");
            }
            // User is new, register the user.
            RegisterViewModel model = _accountAgent.RegisertSocialUser(result);
            if (!model.HasError)
            {
                _accountAgent.SocialUserCreateOrUpdateAccount(result.Provider, result.ProviderUserId, model.UserName);
                _accountAgent.SocialLogin(result.Provider, result.ProviderUserId, createPersistentCookie: false);
                if (string.IsNullOrEmpty(returnUrl))
                {
                    _authenticationAgent.SetAuthCookie(model.UserName, true);
                    return RedirectToAction("Dashboard");
                }
                _authenticationAgent.RedirectFromLoginPage(model.UserName, true);
                return new EmptyResult();
            }
            TempData[MvcDemoConstants.SocialLoginFailedMessage] = model.ErrorMessage;
            return RedirectToAction("Login");
        }

        internal class ExternalLoginResult : ActionResult
        {
            public ExternalLoginResult(string provider, string returnUrl)
            {
                Provider = provider;
                ReturnUrl = returnUrl;
            }

            public string Provider { get; private set; }
            public string ReturnUrl { get; private set; }

            public override void ExecuteResult(ControllerContext context)
            {
                OAuthWebSecurity.RequestAuthentication(Provider, ReturnUrl);
            }
        }
        #region PRFT custom Code

        #region PRFT Custom code: Start
        /// <summary>
        /// Get the sub user list for user administration functionality
        /// </summary>
        /// <param name="searchTerm"></param>
        /// <returns></returns>
        [Authorize]
        public ActionResult UserAdministration(PRFTSubUsersRequestModel subUsersRequestModel, string parentAccountId)
        {
            //PRFT Custom Code: Start
            ViewBag.Title = "User Administration";
            //PRFT Custom Code: End

            subUsersRequestModel.RefineBy = PRFTAccountsHelper.GetRefineBy();
            subUsersRequestModel.Sort = PRFTAccountsHelper.GetSortBy(subUsersRequestModel.Sort);

            subUsersRequestModel.ParentAccountId = parentAccountId;

            //subUsersRequestModel.searchTerm = username;

            if (subUsersRequestModel.PageNumber == null) { subUsersRequestModel.PageNumber = subUsersRequestModel.PageNumber.GetValueOrDefault(1); }

            //subUsersRequestModel.PageSizes = _accountAgent.GetPageSizesItems();

            var viewModel = new PRFTSubUsersViewModel();
            if (subUsersRequestModel.PageSize == null)
            {
                //var pageSize = subUsersRequestModel.PageSizes.Where(x => x.Selected = true).First();
                int perPageValue = 10;
                int.TryParse(Resources.ZnodeResource.SubUsersListPageSize, out perPageValue);
                subUsersRequestModel.PageSize = perPageValue;
            }
            //else
            //{
            //    viewModel.PageSizes = new SelectList(subUsersRequestModel.PageSizes, "Value", "Text", subUsersRequestModel.PageSize.ToString()).ToList();
            //}

            if (!string.IsNullOrEmpty(subUsersRequestModel.searchTerm))
            {
                subUsersRequestModel.PageSize = int.MaxValue;//int.Parse(Resources.ZnodeResource.SubUsersListPageSize);
            }

            viewModel = AccountViewModelMap.ToSubUsersViewModel(_accountAgent.GetSubUserAccounts(Convert.ToInt32(parentAccountId), subUsersRequestModel));

            viewModel.PageSize = int.Parse(Resources.ZnodeResource.SubUsersListPageSize); //subUsersRequestModel.PageSize;
            //viewModel.PageSizes = subUsersRequestModel.PageSizes;
            viewModel.PageNumber = subUsersRequestModel.PageNumber;

            viewModel.OldPageNumber = subUsersRequestModel.PageNumber ?? MvcDemoConstants.DefaultPageNumber;
            int previousPage = (viewModel.OldPageNumber - 1) == 0 ? 1 : (viewModel.OldPageNumber - 1);
            int nextPage = (viewModel.OldPageNumber >= viewModel.TotalPages) ? viewModel.OldPageNumber : viewModel.OldPageNumber + 1;

            if (viewModel.TotalPages < 1)
            {
                viewModel.TotalPages = 1;
            }

            viewModel.NextPageUrl = SearchMap.UpdateUrlQueryString("pagenumber", nextPage.ToString());
            viewModel.PreviousPageurl = SearchMap.UpdateUrlQueryString("pagenumber", previousPage.ToString());

            viewModel.FirstPageNumber = 1;
            viewModel.FirstPageUrl = SearchMap.UpdateUrlQueryString("pagenumber", viewModel.FirstPageNumber.ToString());
            viewModel.LastPageNumber = viewModel.TotalPages;
            viewModel.LastPageUrl = SearchMap.UpdateUrlQueryString("pagenumber", viewModel.LastPageNumber.ToString());

            if (viewModel.ErrorMessage != null)
            {
                viewModel.ErrorMessage = "";
            }

            viewModel.searchKeyword = subUsersRequestModel.searchTerm;
            if (!string.IsNullOrEmpty(subUsersRequestModel.SuccessMessage))
            {
                viewModel.SuccessMessage = subUsersRequestModel.SuccessMessage;
            }

            return View("PRFTUserAdministration", viewModel);
        }

        [Authorize]
        [HttpGet]
        public ActionResult RegisterSubUser(int? id)
        {
            //PRFT Custom Code: Start
            ViewBag.Title = "Create New User";
            //PRFT Custom Code: End
            if (id.HasValue)
            {
                ViewBag.Title = "Edit User";

                var editAccountViewModel = _accountAgent.GetAccountByAccountId(id.Value);

                return View("PRFTEditUserAccount", editAccountViewModel);
                //return View("PRFTEditUserAccount", null);
            }

            var viewModel = new PRFTRegisterSubUserViewModel();
            return View("PRFTRegisterSubUser", viewModel);
        }

        [Authorize]
        [HttpPost]
        public ActionResult RegisterSubUser(PRFTRegisterSubUserViewModel model, string returnUrl)
        {
            bool status;
            //PRFT Custom Code: Start
            ViewBag.Title = "Create New User";
            //PRFT Custom Code: End

            if (ModelState.IsValid)
            {
                model = _accountAgent.RegisterSubUser(model);
                

                if (!model.HasError)
                {
                    var viewModel = _accountAgent.GetAccountViewModel();                    
                    status = _accountAgent.SaveAssociateCustomer(model.AccountId, viewModel.AccountId.ToString());//PRFT Custom Code

                    //Updating subuser billing address with super user billing address
                    var subUserAccountModel = _accountAgent.GetAccountByAccountId(model.AccountId);
                    model.BillingAddress = viewModel.BillingAddress;                    
                    model.BillingAddress.AccountId = subUserAccountModel.BillingAddress.AccountId;
                    model.BillingAddress.AddressId = subUserAccountModel.BillingAddress.AddressId;
                    _accountAgent.UpdateAddress(model.BillingAddress);

                    if (string.IsNullOrEmpty(returnUrl))
                    {
                        
                        if (viewModel != null && !string.IsNullOrEmpty(viewModel.Custom2))
                        {
                            //return RedirectToAction("UserAdministration", new PRFTSubUsersRequestModel { ParentAccountId = model.SuperUserAccount.AccountId.ToString(), HasError = false, SuccessMessage = "User created successfully.", ErrorMessage = "" });
                            return RedirectToAction("UserAdministration", new PRFTSubUsersRequestModel { ParentAccountId = model.SuperUserAccount.AccountId.ToString(), HasError = false, SuccessMessage = "User created successfully.", ErrorMessage = "" });
                        }
                        return RedirectToAction("Dashboard");
                    }

                    return new EmptyResult();
                }
            }

            return View("PRFTRegisterSubUser", model);
        }

        [Authorize]
        [HttpPost]
        public ActionResult EditUserAccount(AccountViewModel viewModel)
        {
            ViewBag.Title = "Edit User";

            if (ModelState.IsValid)
            {
                var upadatedViewModel = _accountAgent.UpdateUserAccount(viewModel);

                if (!upadatedViewModel.HasError)
                {
                    var model =  (AccountViewModel)Session[MvcDemoConstants.AccountKey];
                    if (model != null && model.AccountId >0)
                    {
                        //return RedirectToAction("UserAdministration", new PRFTSubUsersRequestModel { ParentAccountId = viewModel.ParentAccountId.ToString(), HasError = false, SuccessMessage = "User updated successfully.", ErrorMessage = "" });
                        return RedirectToAction("UserAdministration", new PRFTSubUsersRequestModel { ParentAccountId = model.AccountId.ToString(), HasError = false, SuccessMessage = "User updated successfully.", ErrorMessage = "" });
                    }
                }

                return View("PRFTEditUserAccount", upadatedViewModel);
            }

            return View("PRFTEditUserAccount", viewModel);
        }

        [Authorize]
        [HttpGet]
        public ActionResult SubUserOrderHistory(int? id)
        {
            var model = _accountAgent.GetAccountByAccountId(id.Value);

            AccountViewModel accountViewModel = (AccountViewModel)HttpContext.Session[MvcDemoConstants.AccountKey];
            
            for(int i=0; i<model.Orders.Count; i++)
            {
                if (i >= 0 && i < model.Orders.Count)
                {
                    if (string.IsNullOrEmpty(model.Orders[i].Custom1) || !model.Orders[i].Custom1.Equals(accountViewModel.ExternalId, StringComparison.InvariantCultureIgnoreCase))
                    {
                        model.Orders.RemoveAt(i);
                        i--;
                    }
                }
            }
            return View("PRFTSubUserOrderHistory", model);
        }


        [Authorize]
        public ActionResult SubUserOrderReceipt(int orderId)
        {
            int accountId = 0;
            int.TryParse(Request.QueryString["accountId"], out accountId);
            if (accountId > 0)
            {
                var model = _accountAgent.GetSubUserOrderReceiptDetails(orderId, accountId);
                return View("OrderReceipt", model);
            }
            return RedirectToAction("Dashboard");
        }
        #endregion

        #region PRFT Custom Code For CustomerUser-Mapping
        /// <summary>
        /// Shows the customer PopUp.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult AssociateCustomerListPopUp()
        {
            var model = _accountAgent.GetDashboard();

            PRFTCustomerListViewModel customerList = _customerUserAssociationAgent.GetAssociatedCustomerList(model.AccountId, null, null, null, null);

            return PartialView("_PRFTAssociatedCustomerList", customerList);
        }

        [Authorize]
        [ActionName("PRFTLoginWithAsssociatedCustomer")]
        public ActionResult LoginWithAsssociatedCustomer(string customerExternalAccountNo, string returnUrl)
        {
            string link = string.Empty;
            var model = _accountAgent.GetDashboard();
            
            if (model == null)
            {
                return RedirectToAction("Login");
            }

            //PRFT Custom Code:Start
            //model.AssociatedCustomerId = Convert.ToInt32(customerId);
            Session["AssociatedCustomerExternalId"] = new PRFTHelper().ExternalId(customerExternalAccountNo);
            //PRFT Custom Code:End

            if (Request.IsAjaxRequest())
            {
                var isMerged = _cartAgent.Merge();

                if(isMerged)
                {
                    link = ConfigurationManager.AppSettings["DemoWebsiteUrl"] + "/cart/index";
                }
                if(string.IsNullOrEmpty(returnUrl) || (isMerged==false && Session["CallForPriceItemRemoved"]!=null && Convert.ToBoolean(Session["CallForPriceItemRemoved"]) ==true))
                {
                    Session.Remove("CallForPriceItemRemoved");
                    link = ConfigurationManager.AppSettings["DemoWebsiteUrl"] + "/account/dashboard";
                }
                else
                {
                    link = ConfigurationManager.AppSettings["DemoWebsiteUrl"] + returnUrl.Replace("~","");
                }

                return Json(new
                {
                    success = model != null ? true : false,                                        
                    link = !string.IsNullOrEmpty(link) ? link : ConfigurationManager.AppSettings["DemoWebsiteUrl"] + "/",
                    data = new
                    {
                        style = "success",
                        link = !string.IsNullOrEmpty(link) ? link : ConfigurationManager.AppSettings["DemoWebsiteUrl"] + "/",                        
                    }
                }, JsonRequestBehavior.AllowGet);           

            }

            return View("Dashboard", model);
        }
        #endregion

        #region Updating Customer From ERP

        public void UpdateAccountDetailsFromERP(AccountViewModel model)
        {
            //Session["ERPCustomerType"] = HelperMethods.GetERPCustomerType();

            if (model!=null && new PRFTHelper().IsB2BUser())
            {
                //Get Details from ERP
                PRFTERPAccountDetailsViewModel erpCustomerModel = new PRFTERPAccountDetailsViewModel();
                erpCustomerModel = _erpCustomerAgent.GetAccountDetailsFromERP(new PRFTHelper().ExternalId(model.ExternalId));
                if (erpCustomerModel != null && erpCustomerModel.HasError == false && !string.IsNullOrEmpty(erpCustomerModel.CompanyCodeDebtorAccountID))
                {
                    //Update billing address
                    AddressViewModel billingAddress = new AddressViewModel();
                    billingAddress.City = (string.IsNullOrEmpty(erpCustomerModel.BillingAddress.City)) ? model.BillingAddress.City : erpCustomerModel.BillingAddress.City;
                    billingAddress.CompanyName = (string.IsNullOrEmpty(erpCustomerModel.BillingAddress.CompanyName)) ? model.BillingAddress.CompanyName : erpCustomerModel.BillingAddress.CompanyName;
                    billingAddress.CountryCode = (string.IsNullOrEmpty(erpCustomerModel.BillingAddress.CountryCode)) ? model.BillingAddress.CountryCode : erpCustomerModel.BillingAddress.CountryCode;
                    billingAddress.Email = (string.IsNullOrEmpty(erpCustomerModel.BillingAddress.Email)) ? model.BillingAddress.Email : erpCustomerModel.BillingAddress.Email;
                    billingAddress.FirstName = (string.IsNullOrEmpty(erpCustomerModel.BillingAddress.FirstName)) ? model.BillingAddress.FirstName : erpCustomerModel.BillingAddress.FirstName;
                    billingAddress.LastName = (string.IsNullOrEmpty(erpCustomerModel.BillingAddress.LastName)) ? model.BillingAddress.LastName : erpCustomerModel.BillingAddress.LastName;
                    billingAddress.MiddleName = (string.IsNullOrEmpty(erpCustomerModel.BillingAddress.MiddleName)) ? model.BillingAddress.MiddleName : erpCustomerModel.BillingAddress.MiddleName;
                    billingAddress.Name = (string.IsNullOrEmpty(erpCustomerModel.BillingAddress.Name)) ? string.IsNullOrEmpty(model.BillingAddress.Name)? ConfigurationManager.AppSettings["BillingDisplayName"]: model.BillingAddress.Name : erpCustomerModel.BillingAddress.Name;
                    billingAddress.PhoneNumber = (string.IsNullOrEmpty(erpCustomerModel.BillingAddress.PhoneNumber)) ? model.BillingAddress.PhoneNumber : erpCustomerModel.BillingAddress.PhoneNumber;
                    billingAddress.PostalCode = (string.IsNullOrEmpty(erpCustomerModel.BillingAddress.PostalCode)) ? model.BillingAddress.PostalCode : erpCustomerModel.BillingAddress.PostalCode;
                    billingAddress.StateCode = (string.IsNullOrEmpty(erpCustomerModel.BillingAddress.StateCode)) ? model.BillingAddress.StateCode : erpCustomerModel.BillingAddress.StateCode;
                    billingAddress.StreetAddress1 = (string.IsNullOrEmpty(erpCustomerModel.BillingAddress.StreetAddress1)) ? model.BillingAddress.StreetAddress1 : erpCustomerModel.BillingAddress.StreetAddress1;
                    billingAddress.StreetAddress2 = (string.IsNullOrEmpty(erpCustomerModel.BillingAddress.StreetAddress2)) ? model.BillingAddress.StreetAddress2 : erpCustomerModel.BillingAddress.StreetAddress2;

                    //billingAddress = erpCustomerModel.BillingAddress;
                    billingAddress.AccountId = model.AccountId;
                    billingAddress.AddressId = model.BillingAddress.AddressId;
                    billingAddress.IsDefaultBilling = true;
                    //billingAddress.CompanyName = model.BillingAddress.CompanyName;
                    //billingAddress.Name = ConfigurationManager.AppSettings["BillingDisplayName"];
                    Session["ERPCustomerType"] = erpCustomerModel.CustomerType;
                    _accountAgent.UpdateAddress(billingAddress);

                    //Add Update shipping Address
                    UpdateShippingAddresses(model, erpCustomerModel.ShippingAddresses);
                }
            }
        }
        
        private void UpdateShippingAddresses(AccountViewModel model,Collection<AddressViewModel> erpShippingAddressList)
        {
            foreach(var address in erpShippingAddressList)
            {
                AddressViewModel addressTobeUpdated = new AddressViewModel();

                addressTobeUpdated = address;
                addressTobeUpdated.IsDefaultBilling = false;
                addressTobeUpdated.AccountId = model.AccountId;

                var tmpAddress = model.Addresses.FirstOrDefault(addr => addr.AddressExtn.ERPAddressID == address.AddressExtn.ERPAddressID);
                bool isAddressExtensionDetailsExists = true;

                bool isUpdateRequired = false;
                if (tmpAddress == null)
                {
                    //written inside try catch block since imported data may have null values
                    try
                    {
                        isAddressExtensionDetailsExists = false;
                        tmpAddress = model.Addresses.FirstOrDefault(addr => addr.StreetAddress1.Trim().ToLower().Equals(address.StreetAddress1.Trim().ToLower()) &&
                                        addr.City.Trim().ToLower().Equals(address.City.Trim().ToLower()) && addr.StateCode.Trim().ToLower().Equals(address.StateCode.Trim().ToLower()) &&
                                        addr.CountryCode.Trim().ToLower().Equals(address.CountryCode.Trim().ToLower()) && addr.PostalCode.Trim().ToLower().Equals(address.PostalCode.Trim().ToLower()) && addr.IsDefaultShipping == true);
                    }
                    catch (Exception) { }
                }

                //Update only those data which is returned from erp rest use the existing values
                if(tmpAddress!=null)
                {
                    addressTobeUpdated.City = (string.IsNullOrEmpty(address.City)) ? tmpAddress.City : address.City;
                    addressTobeUpdated.CompanyName = (string.IsNullOrEmpty(address.CompanyName)) ? tmpAddress.CompanyName : address.CompanyName;
                    addressTobeUpdated.CountryCode = (string.IsNullOrEmpty(address.CountryCode)) ? tmpAddress.CountryCode : address.CountryCode;
                    addressTobeUpdated.Email = (string.IsNullOrEmpty(address.Email)) ? tmpAddress.Email : address.Email;
                    addressTobeUpdated.FirstName = (string.IsNullOrEmpty(address.FirstName)) ? tmpAddress.FirstName : address.FirstName;
                    addressTobeUpdated.LastName = (string.IsNullOrEmpty(address.LastName)) ? tmpAddress.LastName : address.LastName;
                    addressTobeUpdated.MiddleName = (string.IsNullOrEmpty(address.MiddleName)) ? tmpAddress.MiddleName : address.MiddleName;
                    addressTobeUpdated.Name = (string.IsNullOrEmpty(address.Name)) ? string.IsNullOrEmpty(tmpAddress.Name) ? ConfigurationManager.AppSettings["ShippingDisplayName"] : tmpAddress.Name : address.Name;
                    addressTobeUpdated.PhoneNumber = (string.IsNullOrEmpty(address.PhoneNumber)) ? tmpAddress.PhoneNumber : address.PhoneNumber;
                    addressTobeUpdated.PostalCode = (string.IsNullOrEmpty(address.PostalCode)) ? tmpAddress.PostalCode : address.PostalCode;
                    addressTobeUpdated.StateCode = (string.IsNullOrEmpty(address.StateCode)) ? tmpAddress.StateCode : address.StateCode;
                    addressTobeUpdated.StreetAddress1 = (string.IsNullOrEmpty(address.StreetAddress1)) ? tmpAddress.StreetAddress1 : address.StreetAddress1;
                    addressTobeUpdated.StreetAddress2 = (string.IsNullOrEmpty(address.StreetAddress2)) ? tmpAddress.StreetAddress2 : address.StreetAddress2;
                }



                if (tmpAddress!=null && isAddressExtensionDetailsExists==true)
                {
                    //Update address based on modified date
                    //if(tmpAddress.AddressExtn.ModifiedDate < address.AddressExtn.ModifiedDate) //Commented since the address also might changed from site.
                    //{
                        addressTobeUpdated.AddressId = tmpAddress.AddressId;
                        addressTobeUpdated.AddressExtn.AddressExtnID = tmpAddress.AddressExtn.AddressExtnID;
                        addressTobeUpdated.AddressExtn.AddressID = tmpAddress.AddressExtn.AddressID;
                        isUpdateRequired = true;
                    //}
                }
                else if(tmpAddress!=null)
                {
                    //No address extension details but address exists. so just update address as we don't know address is updated or not.
                    addressTobeUpdated.AddressId = tmpAddress.AddressId;
                    addressTobeUpdated.AddressExtn.AddressID = tmpAddress.AddressId;
                    isUpdateRequired = true;
                }
                else
                {
                    //insert address on both the table. But we don't have any value other than account ID which is already set
                    isUpdateRequired = true;
                }

                //This will not required when address details exists on both the table and znode modified date is > ERP modified date.
                if (isUpdateRequired)
                {
                    addressTobeUpdated.Name= ConfigurationManager.AppSettings["ShippingDisplayName"];
                    _accountAgent.UpdateAddress(addressTobeUpdated);
                }
            }
        }
        #endregion

        [AllowAnonymous]
        [HttpGet]
        public ActionResult AdminUserLogin(string aid)
        {
            
            int accountId = 0;
            int.TryParse(aid, out accountId);

            if (accountId > 0)
            {
                var accountModel = _accountAgent.GetAccountByAccountId(accountId);

                _authenticationAgent.SetAuthCookie(accountModel.UserName, true);
                _authenticationAgent.RedirectFromLoginPage(accountModel.UserName, true);
                HttpContext.Session[MvcDemoConstants.AccountKey] = accountModel;

                if (accountModel != null && accountModel.AccountId > 0)
                {
                    customerList = _customerUserAssociationAgent.GetAssociatedCustomerList(accountModel.AccountId, null, null, null, null);
                }

                if (customerList.Customers != null && customerList.Customers.Count > 1)
                {
                    return RedirectToAction("PRFTAssociatedCustomerList");
                }
                else
                {
                    if (customerList.Customers != null && customerList.Customers.Count.Equals(1))
                    {
                        Session["AssociatedCustomerExternalId"] = new PRFTHelper().ExternalId(customerList.Customers[0].ExternalId);
                    }
                    else if (new PRFTHelper().IsB2BUser())
                    {
                        Session["AssociatedCustomerExternalId"] = new PRFTHelper().ExternalId(accountModel.ExternalId);
                    }
                    else
                    {
                        Session["AssociatedCustomerExternalId"] = ConfigurationManager.AppSettings["GenericUserExternalID"];
                    }

                }                                  
                return RedirectToAction("Dashboard");
            }
            return View("Login");
        }


        [Authorize]
        public ActionResult PRFTAssociatedCustomerList()
        {           
            return View("PRFTAssociatedCustomerList");
        }

        [Authorize]
        public ActionResult Invoice(string externalId)
        {            
            var model = _erpOrderAgent.GetInvoiceDetailsFromERP(externalId);
            return View("PRFTInvoice",model);
        }

        private bool RecaptchaValidation()
        {
            bool isValid = false;
            TempData["CaptchaErrorMessage"] = string.Empty;

            var response = Request["g-recaptcha-response"];

            if (string.IsNullOrEmpty(response))
            {
                TempData["CaptchaErrorMessage"] = "Please validate Captcha.";
                return isValid;
            }
            //secret that was generated in key value pair
            string secret = ConfigurationManager.AppSettings["recaptchaPrivateKey"];
            var client = new WebClient();
            var reply = client.DownloadString(string.Format(ConfigurationManager.AppSettings["recaptchaValidationUrl"], secret, response));

            var captchaResponse = JsonConvert.DeserializeObject<CaptchaResponse>(reply);

            //when response is false check for the error message
            if (!captchaResponse.Success)
            {
                if (captchaResponse.ErrorCodes.Count <= 0)
                {
                    isValid = true;
                }
                else
                {
                    isValid = false;
                }

                var error = captchaResponse.ErrorCodes[0].ToLower();
                switch (error)
                {
                    case ("missing-input-secret"):
                        TempData["CaptchaErrorMessage"] = "The secret parameter is missing.";
                        break;
                    case ("invalid-input-secret"):
                        TempData["CaptchaErrorMessage"] = "The secret parameter is invalid or malformed.";
                        break;

                    case ("missing-input-response"):
                        TempData["CaptchaErrorMessage"] = "Please re-validate Captcha.";
                        break;
                    case ("invalid-input-response"):
                        TempData["CaptchaErrorMessage"] = "Please re-validate Captcha.";
                        break;

                    default:
                        TempData["CaptchaErrorMessage"] = "The captcha value is invalid or malformed.";
                        break;
                }
            }
            else
            {
                TempData["CaptchaErrorMessage"] = string.Empty;
                isValid = true;
            }

            return isValid;
        }
        #endregion

    }

}
