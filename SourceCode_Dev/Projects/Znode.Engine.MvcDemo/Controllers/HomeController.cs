﻿using Resources;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.Agents;
using Znode.Engine.MvcDemo.Helpers;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Controllers
{
    public class HomeController : BaseController
    {
        #region Private Agent variables
        private readonly IAccountAgent _accountAgent;
        private readonly IContentPageAgent _contentPageAgent;
        private readonly StoreLocatorAgent _storeLocatorAgent;
        #endregion

        #region Private Constants
        private const string contentPage = "ContentPage";
        private const string storeLocator = "StoreLocator";
        #endregion

        #region Constructor
        /// <summary>
        /// Default Constructor
        /// </summary>
        public HomeController()
        {
            _accountAgent = new AccountAgent();
            _contentPageAgent = new ContentPageAgent();
            _storeLocatorAgent = new StoreLocatorAgent();
        }
        #endregion

        public ActionResult Index()
        {
            string affiliate_Id = "affiliate_id";
            if (!string.IsNullOrEmpty(Request.QueryString[affiliate_Id]))
            {
                Session.Add(MvcDemoConstants.AffiliateIdSessionKey, Request.QueryString[affiliate_Id]);
            }
            ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";
            return View();
        }

        public ActionResult GiftCard(string giftcard)
        {
            if (ModelState.IsValid && Request.IsAjaxRequest())
            {
                if (string.IsNullOrEmpty(giftcard))
                {
                    return Json(new
                    {
                        success = false,
                        message = Resources.ZnodeResource.RequiredGiftCard,
                        data = new
                        {
                            style = "error",

                        }
                    }, JsonRequestBehavior.AllowGet);
                }

                var giftcardModel = _accountAgent.GetGiftCardBalance(giftcard);

                return Json(new
                    {
                        success = !giftcardModel.HasError,
                        message =
                                !giftcardModel.HasError ? giftcardModel.SuccessMessage : giftcardModel.ErrorMessage,
                        data = new
                            {
                                style = !giftcardModel.HasError ? "success" : "error",
                                amount = HelperMethods.FormatPriceWithCurrency(giftcardModel.Amount.GetValueOrDefault()),
                            }
                    }, JsonRequestBehavior.AllowGet);
            }

            return View("GiftCardBalance");
        }

        public ActionResult ContentPage(string contentPageName, string seoName = null)
        {
            contentPageName = contentPageName + '_' + PortalAgent.CurrentPortal.PortalId + '_' + "en";
            ContentPageListViewModel contentPageDetails = _contentPageAgent.GetContentPageDetails();              
            if (!string.IsNullOrEmpty(contentPageName) && contentPageName.IndexOf("_") >= 0)
            {
                string ContentPageData = _contentPageAgent.GetContentPageByName(contentPageName, "htm");
                string pageName = string.Empty;
                pageName = contentPageName.Split('_')[0];
                if (string.IsNullOrEmpty(ContentPageData))
                {
                    contentPageDetails.PageTitle = string.Empty;
                }
                else
                {
                    ViewData[MvcDemoConstants.ContentPageTitle] = contentPageDetails.ContentPages.Find(x => Equals(x.Name.ToLower(), pageName.ToLower())).PageTitle;
                }
                ViewData[MvcDemoConstants.ContentPageHtml] = ContentPageData;

                //PRFT custom code : Start
                var requiredPageObject = contentPageDetails.ContentPages.Find(x => Equals(x.Name.ToLower(), pageName.ToLower()));
                if (requiredPageObject != null && !string.IsNullOrEmpty(requiredPageObject.SEOTitle))
                {
                    ViewBag.Title = requiredPageObject.SEOTitle;
                    string titleSuffix = ConfigurationManager.AppSettings["StoreName"];
                    if (!requiredPageObject.SEOTitle.Contains(titleSuffix))
                    {
                        ViewBag.Title = requiredPageObject.SEOTitle + titleSuffix;
                    }
                }
                
                //if (!string.IsNullOrEmpty(seoName))
                //{
                //    ViewBag.Title = seoName;
                //}

                //PRFT custom code : End     


            }
            return View(contentPage, contentPageDetails.PageTitle);
        }

        /// <summary>
        /// Handles all application level errors.
        /// </summary>
        /// <param name="exception"></param>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult ErrorHandler(Exception exception)
        {
            HttpException httpexception = exception as HttpException;

            if (!Equals(httpexception, null))
            {
                int httpCode = httpexception.GetHttpCode();

                switch (httpCode)
                {
                    case 404:
                        {
                            //PRFT Custom Code :  Start
                            if (HelperMethods.IsDebugMode)
                            {
                                ViewBag.ErrorMessage = ZnodeResource.HttpCode_404_PageNotFoundMsg;
                            }
                            else
                            {
                                return RedirectToAction("Error404", "Error");
                            }
                            //PRFT Custom Code :  Start
                            break;
                        }
                    case 401:
                        {
                            //PRFT Custom Code :  Start
                            
                            if (HelperMethods.IsDebugMode)
                            {
                                ViewBag.ErrorMessage = ZnodeResource.HttpCode_401_AccessDeniedMsg;
                            }
                            else
                            {
                                return RedirectToAction("Error404", "Error");
                                
                            }
                            //PRFT Custom Code :  Start
                            break;
                        }
                    default:
                        {
                            if (HelperMethods.IsDebugMode)
                            {
                                if (exception is HttpRequestValidationException)
                                {
                                    ViewBag.ErrorMessage = ZnodeResource.HttpCode_500_RequestValidationErrorMsg;
                                }
                                else
                                {
                                    if (exception is HttpAntiForgeryException)
                                    {
                                        ViewBag.ErrorMessage = ZnodeResource.HttpAntiforgeryTokenExceptionMessage;
                                    }
                                    else
                                    {
                                        ViewBag.ErrorMessage = ZnodeResource.HttpCode_500_InternalServerErrorMsg;
                                    }
                                }
                            }
                            else
                            {
                                return RedirectToAction("UnhandledError", "Error");
                            }
                            break;
                        }
                }
            }
            else
            {
                if (HelperMethods.IsDebugMode)
                {
                    ViewBag.ErrorMessage = ZnodeResource.GenericErrorMessage;
                }
                else
                {
                    return RedirectToAction("UnhandledError", "Error");
                }


            }

            if(!HelperMethods.IsDebugMode)
            {
                return RedirectToAction("UnhandledError", "Error");
            }
            return View(MvcDemoConstants.ErrorView);
        }

       
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult StoreLocator()
        {
            StoreLocatorViewModel model = new StoreLocatorViewModel();
            model.RadiusList = _storeLocatorAgent.GetDistanceList();
            return View(storeLocator, model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult StoreLocator(StoreLocatorViewModel model)
        {
            if (ModelState.IsValid)
            {
                model.StoreList = _storeLocatorAgent.GetStoresList(model);
            }
            model.RadiusList = _storeLocatorAgent.GetDistanceList();
            return View(storeLocator, model);
        }

    }
}
