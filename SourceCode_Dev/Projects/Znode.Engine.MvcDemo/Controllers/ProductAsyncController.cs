﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.SessionState;
using Znode.Engine.MvcDemo.Agents;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Controllers
{
    [SessionState(SessionStateBehavior.Disabled)]
    public class ProductAsyncController : AsyncController 
    {
     

        [HttpGet]
        public async Task<ActionResult> GetProductsByIds(string ids, int accountId)
        {
            IProductAgent productAgent = new ProductAgent();
            var products = await productAgent.GetProductByIdsAsyn(ids, accountId);
             var viewModel = new Collection<ProductViewModel>(products.ToList());
             if (!viewModel.Any())
             {
                 return new EmptyResult();
             }
             return PartialView("../Product/_RelatedItems", viewModel);
        }

        [HttpGet]
        public async Task<ActionResult> GetConfigs(string key)
        {
            IMessageConfigsAgent productAgent = new MessageConfigsAgent();
            var content = await productAgent.GetByKeyAsync(key);
           
            return Content(content);
        }

        [HttpGet]
        public async Task<ActionResult> GetHomePageSpecials()
        {

            IProductAgent productAgent = new ProductAgent();
            var viewModel= await productAgent.GetHomeSpecialsAsync();
            if (viewModel == null || viewModel.Count < 1)
            {
                return new EmptyResult();
            }
            return PartialView("../Product/_ProductGrid", viewModel);
        }

    }
}
