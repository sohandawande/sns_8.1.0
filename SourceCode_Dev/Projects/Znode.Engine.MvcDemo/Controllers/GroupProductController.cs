﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Web.Mvc;
using DevTrends.MvcDonutCaching;
using Znode.Engine.MvcDemo.Agents;
using Znode.Engine.MvcDemo.Helpers;
using Znode.Engine.MvcDemo.Maps;
using Znode.Engine.MvcDemo.ViewModels;
using System.Linq;
using System.Web;
using System;
using System.Net.Http.Formatting;
using Newtonsoft.Json;
using Znode.Engine.Api.Models;

namespace Znode.Engine.MvcDemo.Controllers
{
    public class GroupProductController : BaseController
    {

        private readonly IProductAgent _productAgent;
        private readonly IAccountAgent _accountAgent;
        private readonly ISkuAgent _skuAgent;
        private readonly ICartAgent _cartAgent;

        public GroupProductController()
        {
            _productAgent = new ProductAgent();
            _skuAgent = new SkuAgent();
            _cartAgent = new CartAgent();
            _accountAgent = new AccountAgent();
        }
        //
        // GET: /GroupProduct/
        public ActionResult Index(int? id)
        {
            if (id.HasValue)
            {
                int productId = id.Value;
                PRFTProductSkuListViewModel model = new PRFTProductSkuListViewModel();
                if (!Equals(productId, 0))
                {
                   model.SkuList = _productAgent.GetProductSkuByProductId(productId, null, null, null, null);
                    if (!Equals(model, null))
                    {
                        ProductViewModel productModel = _productAgent.GetProduct(productId);
                        model.ProductId = productId;
                        model.ProductName = productModel.Name;
                        model.ProductPrice = productModel.ProductPrice;
                        model.MaxQuantity = productModel.MaxQuantity;
                        model.MinQuantity = productModel.MinQuantity;
                        model.HighLights = productModel.HighLights;
                        model.Price = productModel.Price;

                        model.IsCallForPricing = productModel.IsCallForPricing;
                        model.ShowAddToCart = productModel.IsCallForPricing?false:true;
                        //PRFT Custom Code:Start
                        //Geting Price form ERP
                        string associatedCustomerExternalId = HelperMethods.GetAssociatedCustomerExternalId();
                        if (!string.IsNullOrEmpty(associatedCustomerExternalId))
                        {
                            string customerType = HelperMethods.GetERPCustomerType();
                            PRFTERPItemDetailsModel prftERPItemDetailModel = new PRFTERPItemDetailsModel();
                            prftERPItemDetailModel = _productAgent.GetItemDetailsFromERP(productModel.ProductNumber, associatedCustomerExternalId, customerType);
                            if (prftERPItemDetailModel != null && prftERPItemDetailModel.HasError == false)
                            {
                                model.Price = model.ProductPrice = prftERPItemDetailModel.UnitPrice;//Required by JS to update total price.

                                if (prftERPItemDetailModel.IsCallForPricing == true || prftERPItemDetailModel.UnitPrice <= 0)
                                {
                                    model.IsCallForPricing = true;
                                    model.ShowAddToCart = false;
                                }
                            }
                        }

                        //Get Inventory From ERP
                        model.MaxQuantity = _productAgent.GetInventoryFromERP(productModel.ProductNumber);
                        if (model.MaxQuantity <= 0)
                        {
                            model.IsCallForPricing = true;
                            model.ShowAddToCart = false;
                        }
                        //PRFT Custom code: End
                        return View(model);
                    }
                }

            }
            return View();
        }

        [HttpPost]
        public ActionResult AddToCart(FormCollection formCollection)//CartItemViewModel cartItem)
        {

            string[] quantityValues;
            if (formCollection.Count > 0)
            {
                if (formCollection.GetValues("Quantity") != null)
                {
                    quantityValues = formCollection.GetValues("Quantity");
                    var skuValues = formCollection.GetValues("sku");
                    var productIdValues = formCollection.GetValues("productId");
                    var productNameValues = formCollection.GetValues("productName");
                    //var priceValues = formCollection.GetValues("price");
                    for (int count = 0; count < quantityValues.Count(); count++)
                    {
                        CartItemViewModel cartItem = new CartItemViewModel();
                        if (quantityValues[count] != null && quantityValues[count] != "" && Convert.ToInt32(quantityValues[count]) > 0)
                        {
                            int productid = 0;
                            int qty = 1;
                            decimal unitPrice = 0;
                            int.TryParse(productIdValues[0].ToString(), out productid);
                            cartItem.ProductId = productid;
                            cartItem.ProductName = productNameValues[0].ToString();
                            cartItem.Sku = skuValues[count].ToString();
                            int.TryParse(quantityValues[count].ToString(), out qty);
                            cartItem.Quantity = qty;

                            if (!string.IsNullOrEmpty(cartItem.BundleItemsIds))
                            {
                                var productsViewModel = _productAgent.GetBundleProductsDetails(cartItem);
                                return View("BundleSettings", productsViewModel);
                            }

                            var product = _cartAgent.WishlistCheckInventory(cartItem);

                            if (!string.IsNullOrEmpty(product.InventoryMessage) && !product.ShowAddToCart)
                            {
                                Session.Add("ErrorMessage", product.InventoryMessage);

                                return RedirectToAction("Index", "Cart");
                            }

                            _cartAgent.Create(cartItem);
                        }

                    }
                }

            }
            return RedirectToAction("Index", "Cart");
        }

        /// <summary>
        /// Add Multiple To Product List.
        /// </summary>
        /// <param name="wishListName"></param>
        /// <param name="formCollection"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddMultipleToProductList(string wishListName, string formCollection)
        {
            int productID = 0;


            if (formCollection.Count() > 0)
            {
                List<int> quantityValues = new List<int>();
                List<string> skuValues = new List<string>(); ;
                List<int> productIdValues = new List<int>(); ;
                List<string> productNameValues = new List<string>(); ;
                List<decimal> priceValues = new List<decimal>(); ;
                string[] formCollections = formCollection.Split('&');
                int quantity;
                string sku;
                int productId;
                string productName;
                decimal price;
                var wishListViewModel = new PRFTWishListListViewModel();
                foreach (var collection in formCollections)
                {
                    if (collection.Contains("Quantity"))
                    {

                        quantity = (collection.Split('=')[1].ToString() != "" ? Convert.ToInt32(collection.Split('=')[1].ToString()) : 0);
                        quantityValues.Add(quantity);
                    }
                    if (collection.Contains("sku"))
                    {
                        sku = collection.Split('=')[1].ToString();
                        skuValues.Add(sku);
                    }
                    if (collection.Contains("productId"))
                    {
                        productId = Convert.ToInt32(collection.Split('=')[1].ToString());
                        productIdValues.Add(productId);
                    }
                    if (collection.Contains("productName"))
                    {
                        productName = collection.Split('=')[1].ToString();
                        productNameValues.Add(productName);
                    }
                    if (collection.Contains("price"))
                    {
                        price = Convert.ToDecimal(collection.Split('=')[1].ToString());
                        priceValues.Add(price);
                    }
                }



                if (quantityValues != null && quantityValues.Count > 0 && skuValues != null && skuValues.Count > 0 && skuValues.Count == quantityValues.Count)
                {
                    int counter = skuValues.Count;
                    for (int count = 0; count < counter; count++)
                    {
                        if (quantityValues[count] != null && Convert.ToInt32(quantityValues[count]) > 0 && skuValues[count] != null)
                        {

                            int qty = 1;
                            decimal unitPrice = 0;
                            int.TryParse(productIdValues[0].ToString(), out productID);

                            string Sku = skuValues[count];
                            int.TryParse(quantityValues[count].ToString(), out qty);

                            decimal.TryParse(priceValues[0].ToString(), out unitPrice);

                            if (!String.IsNullOrEmpty(wishListName))
                            {
                               // wishListViewModel = _productAgent.CreateWishList(wishListName, productID, Sku, qty, unitPrice);

                            }
                        }
                    }
                    if (Request.IsAjaxRequest())
                    {
                        return Json(new
                        {
                            success = !wishListViewModel.HasError,
                            message = !wishListViewModel.HasError ? Resources.ZnodeResource.SuccessProductAddWishlist : Resources.ZnodeResource.ErrorProductAddWishlist,
                            data = new
                            {
                                style = !wishListViewModel.HasError ? "success" : "error",
                                link = "/account/Wishlist",
                                message = !wishListViewModel.HasError ? Resources.ZnodeResource.SuccessProductAddWishlist : Resources.ZnodeResource.ErrorProductAddWishlist
                            }
                        }, JsonRequestBehavior.AllowGet);
                    }

                    if (wishListViewModel.HasError)
                    {
                        Session.Add("ErrorMessage", Resources.ZnodeResource.ErrorProductAddWishlist);
                    }
                    else
                    {
                        Session.Add("SuccessMessage", Resources.ZnodeResource.SuccessProductAddWishlist);
                    }
                }
            }
            return Redirect(_productAgent.GetProductUrl(productID, Url));
        }
    }
}