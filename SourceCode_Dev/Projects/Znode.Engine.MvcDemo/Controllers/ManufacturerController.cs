﻿using DevTrends.MvcDonutCaching;
using System;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Linq;
using Znode.Engine.MvcDemo.Agents;
using Znode.Engine.MvcDemo.Helpers;
using Znode.Engine.MvcDemo.Maps;
using Znode.Engine.MvcDemo.ViewModels;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace Znode.Engine.MvcDemo.Controllers
{
    /// <summary>
    /// Replica of category controller since using manufacturer as a category and used for only showing all manufacturer and on home page widget.
    /// Rest of the functionality will be used using same category controllers
    /// </summary>
    public class ManufacturerController : BaseController
    {
        private readonly ISearchAgent _searchAgent;
        private readonly ICategoryAgent _categoryAgent;
        private readonly PortalAgent _portalAgent;

        public ManufacturerController()
        {
            _searchAgent = new SearchAgent();
            _categoryAgent = new CategoryAgent();
            _portalAgent = new PortalAgent();
        }

        // GET: /Manufacturer/
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Get the lists of top level Categories to show on Home page
        /// </summary>
        /// <returns></returns>
        [DonutOutputCache(CacheProfile = "PortalCacheProfile")]
        [ChildActionOnly]
        public ActionResult GetHomePageManufacturerList()
        {
            var categories = _categoryAgent.GetCategories();
            
            //Get only the category list by checking custom1 value. If there is value then its Manufacturer
            categories.Categories = categories.Categories.Where(q => !string.IsNullOrEmpty(q.Custom1)).Take(Convert.ToInt32(ConfigurationManager.AppSettings["BrandsToDisplay"])).ToList();
            return PartialView("_PRFTHomePageManufacturerGrid", categories);
        }

        /// <summary>
        /// Get List of all brands.
        /// </summary>
        /// <returns>List of Brands</returns>
        public List<CategoryHeaderViewModel> GetBrandList()
        {
            var categories = _categoryAgent.GetCategories();
            //Get only the category list by checking custom1 value. If there is value then its Manufacturer
            categories.Categories = categories.Categories.Where(q => !string.IsNullOrEmpty(q.Custom1)).ToList();
            return categories.Categories;
        }

        /// <summary>
        /// Binds the brand list.
        /// </summary>
        /// <param name="alpha"></param>
        /// <returns>String with the UI to be displayed.</returns>
        public string BindLiteralForBrandList(string alpha)
        {
            string brandLiteral = string.Empty;
            
            List<CategoryHeaderViewModel> brandList = GetBrandList();
            if (brandList != null && brandList.Count > 0)
            {
                if (!string.IsNullOrEmpty(alpha) && !alpha.Trim().Equals("All"))
                {
                    brandList = brandList.Where(x => x.CategoryName.StartsWith(alpha, StringComparison.OrdinalIgnoreCase)).ToList();
                }

                brandLiteral = GetBrandLiteral(brandList);
            }
            else
            {
                string ErrorMessage = Resources.ZnodeResource.ErrorNoBrandsFound.ToString();
                brandLiteral = string.Format(Resources.ZnodeResource.MftErrorMsg.ToString(), ErrorMessage);
            }
            return brandLiteral;
        }

        private string GetBrandLiteral(List<CategoryHeaderViewModel> brandList)
        {
            StringBuilder sbHtmlCode = new StringBuilder();
            int totalChildCount = brandList.Count;
            int liCount = Convert.ToInt32(Resources.ZnodeResource.MftMaxLiDisplay.ToString());

            for (int childCount = 0; childCount < totalChildCount; childCount++)
            {
                string SEOUrl = "~/" + brandList[childCount].SEOPageName;
                if (childCount == 0)
                {
                    sbHtmlCode.Append(Resources.ZnodeResource.MftOpeningHtml.ToString());
                }
                else
                {
                    if ((childCount % liCount) == 0)
                    {
                        sbHtmlCode.Append(Resources.ZnodeResource.MftOpeningHtml.ToString());
                    }
                }

                sbHtmlCode.Append(string.Format(Resources.ZnodeResource.MftBrandURL, SEOUrl.Replace("~", ""),
                    brandList[childCount].CategoryName,
                    brandList[childCount].CategoryName));

                //Start ul tag
                if (((childCount + 1) % liCount) == 0)
                {
                    sbHtmlCode.Append(Resources.ZnodeResource.MftClosingHtml.ToString());

                }
                else
                {
                    if ((childCount + 1) == totalChildCount)
                    {
                        sbHtmlCode.Append(Resources.ZnodeResource.MftClosingHtml.ToString());
                    }
                }
            }
            if (totalChildCount <= liCount)
            {
                sbHtmlCode.Append(Resources.ZnodeResource.MftClosingHtml.ToString());
            }

            return sbHtmlCode.ToString();
        }
        
    }
}
