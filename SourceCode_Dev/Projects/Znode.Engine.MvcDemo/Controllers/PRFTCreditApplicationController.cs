﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.Agents;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Controllers
{
    public class PRFTCreditApplicationController : BaseController
    {
        #region Private Readonly Variables
        private readonly IPRFTCreditApplicationAgent _creditApplicationRequestAgent;
        #endregion
        
        #region Constructor
        public PRFTCreditApplicationController()
        {
            _creditApplicationRequestAgent = new PRFTCreditApplicationAgent();
        }
        #endregion

        // GET: /CreditApplication/
        public ActionResult Index()
        {
            return View("CreditApplication");
        }

        /// <summary>
        /// Get Country Lists
        /// </summary>
        /// <returns>Country Lists as JSON Object</returns>
        public JsonResult GetCountries()
        {
            var countryList = _creditApplicationRequestAgent.GetCountries(1);
            return Json(countryList,JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get State Lists
        /// </summary>
        /// <returns>State Lists as JSON Object </returns>
        public JsonResult GetStates()
        {
            var stateList = _creditApplicationRequestAgent.GetStates();
            return Json(stateList, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Save Credit Application information
        /// </summary>
        /// <param name="prftCreditApplication"></param>
        /// <returns></returns>
        [HttpPost]
        public bool AddCreditApplication(PRFTCreditApplicationViewModel prftCreditApplication)
        {
            if (RecaptchaValidation(prftCreditApplication.Custom1))
            {
                return _creditApplicationRequestAgent.SaveCreditApplicationInfo(prftCreditApplication);
            }
            return false;
        }

        private bool RecaptchaValidation(string response)
        {
            bool isValid = false;
            //secret that was generated in key value pair
            string secret = ConfigurationManager.AppSettings["recaptchaPrivateKey"];
            var client = new WebClient();
            var reply = client.DownloadString(string.Format(ConfigurationManager.AppSettings["recaptchaValidationUrl"], secret, response));

            var captchaResponse = JsonConvert.DeserializeObject<CaptchaResponse>(reply);

            //when response is false check for the error message
            if (captchaResponse != null && captchaResponse.Success)
            {
                isValid = true;
            }
            return isValid;
        }
    }
}