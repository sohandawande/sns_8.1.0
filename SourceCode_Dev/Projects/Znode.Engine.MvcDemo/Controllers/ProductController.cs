﻿using DevTrends.MvcDonutCaching;
using Newtonsoft.Json;
using Resources;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.Agents;
using Znode.Engine.MvcDemo.Helpers;
using Znode.Engine.MvcDemo.Maps;
using Znode.Engine.MvcDemo.ViewModels;
namespace Znode.Engine.MvcDemo.Controllers
{
    public class ProductController : BaseController
    {
        #region Private Variables
        private readonly IProductAgent _productAgent;
        private readonly IAccountAgent _accountAgent;
        private readonly ISkuAgent _skuAgent;
        private readonly ICartAgent _cartAgent;
        private readonly PortalAgent _portalAgent;
        private const string QuickOrderPadView = "_QuickOrder";
        private const string ComparableProductsView = "_ComparableProducts";
        private const string ProductComparisonListView = "_ProductComparisonList";
        private const string ViewComparisonView = "ViewComparison";
        private const string IndexAction = "Index";
        private const string HomeController = "Home";
        private const string SendMailPartialView = "_SendMailView";
        private const string ViewComparisonAction = "ViewComparison";
        #endregion


        #region Znode Version 7.2.2 Private Variables
        private string _recentlyViewProductList = "RecentlyViewProductList";
        #endregion

        #region Private string
        private const string HighlightList = "HighlightList";
        private const string HighlightInfo = "HighlightInfo";
        private const string ProductHighlight = "_ProductHighlight";
        #endregion

        #region Constructor
        public ProductController()
        {
            _productAgent = new ProductAgent();
            _skuAgent = new SkuAgent();
            _cartAgent = new CartAgent();
            _accountAgent = new AccountAgent();
            _portalAgent = new PortalAgent();
        }
        #endregion
        [DonutOutputCache(CacheProfile = "CatalogCacheProfile")]
        [HttpGet]
        public ActionResult Details(int? id, string seo, bool isQuickView = false)
        {
            if (id.HasValue)
            {
                var product = _productAgent.GetProduct(id.Value);
                if (product == null)
                    throw new HttpException(404, "Couldn't find the product ");

                if (!string.IsNullOrEmpty(seo))
                {
                    if (product.SeoPageName.ToLower() != seo.ToLower())
                    {
                        throw new HttpException(404, "SEO does not match");
                    }
                }

                //PRFT Custom Code:Start
                //Geting Price form ERP
                string associatedCustomerExternalId = HelperMethods.GetAssociatedCustomerExternalId();
                if (!string.IsNullOrEmpty(associatedCustomerExternalId))
                {
                    string customerType = HelperMethods.GetERPCustomerType();
                    PRFTERPItemDetailsModel prftERPItemDetailModel = new PRFTERPItemDetailsModel();
                    prftERPItemDetailModel = _productAgent.GetItemDetailsFromERP(product.ProductNumber, associatedCustomerExternalId, customerType);
                    if (prftERPItemDetailModel != null && prftERPItemDetailModel.HasError == false)
                    {
                        product.OriginalPrice = product.Price = product.ProductPrice = prftERPItemDetailModel.UnitPrice;//Required by JS to update total price.

                        if (prftERPItemDetailModel.IsCallForPricing == true || prftERPItemDetailModel.UnitPrice <= 0)
                        {
                            product.IsCallForPricing = true;
                            product.ShowAddToCart = false;
                        }
                    }
                }

                //Get Inventory From ERP
                product.InventoryMessage = string.Empty;
                if (!product.IsCallForPricing)
                {
                    IEnumerable<int> attributeId = new List<int>();

                    if (product.ProductAttributes.Attributes.Any())
                    {
                        if (product.ProductAttributes.Attributes.First().ProductAttributes.Any())
                            attributeId = product.ProductAttributes.Attributes.Select(x => x.ProductAttributes.First(y => y.Available).AttributeId);
                    }

                    _productAgent.CheckInventoryAgainstERP(product, string.Join(",", attributeId), product.SelectedQuantity);
                }
                //PRFT Custom code: End

                _productAgent.SetMessages(product);

                var accountViewModel = _accountAgent.GetAccountViewModel();

                ViewBag.AccountId = accountViewModel != null ? accountViewModel.AccountId : 0;
                ViewBag.Title = product.Title;
                if (isQuickView)
                {
                    product.IsQuickView = true;
                    return PartialView("_QuickViewDetail", product);
                }
                return View(product);
            }
            return RedirectToAction(IndexAction, HomeController);
        }

        [HttpGet]
        public JsonResult GetAttributes(int? id, int? attributeId, string selectedIds, int? quantity, string selectedAddOnIds)
        {
            //PRFT Custom Code : Start
            PRFTHelper prfthelper = new PRFTHelper();//PRFT Custom Code
            string imageOriginalPath = string.Empty;
            //PRFT Custom Code : End
            string selectedSku = string.Empty;
            string imagePath = string.Empty;
            //ZNode Version 7.2.2 - Add imageMediumPath Out parameter
            string imageMediumPath = string.Empty;

            decimal productPrice;
            decimal finalProductPrice;
            string selectedAddOnValue = string.Empty;
            //ZNode Version 7.2.2 - Add imageMediumPath Out parameter
            var product = _productAgent.GetAttributes(id.Value, attributeId.GetValueOrDefault(0), selectedIds, quantity.GetValueOrDefault(1), out productPrice, out selectedSku, out imagePath, out imageMediumPath);
            if (string.IsNullOrEmpty(selectedAddOnIds) && !Equals(product.AddOns, null))
            {
                foreach (var addOn in product.AddOns)
                {
                    addOn.OutOfStockMessage = string.Empty;
                }
            }
            if (product.AddOns != null && !string.IsNullOrEmpty(selectedAddOnIds))
            {
                product.AddOns = _productAgent.GetAddOnDetails(product, selectedAddOnIds, quantity.GetValueOrDefault(1),
                                                               productPrice, out finalProductPrice, selectedSku);
            }
            else
            {
                finalProductPrice = productPrice;
            }
            //PRFT Custom Code : Start
            
            imageOriginalPath=  prfthelper.CheckImageExists(string.Format(System.Configuration.ConfigurationManager.AppSettings["OriginalImagePath"], System.Configuration.ConfigurationManager.AppSettings["ZnodeApiRootUri"]),product.ImageFile);
            //PRFT Custom Code : End


            return Json(new
            {
                success = product.ShowAddToCart,
                message = product.InventoryMessage,
                data = new
                {
                    style = product.ShowAddToCart ? "success" : "error",
                    price = HelperMethods.FormatPriceWithCurrency(finalProductPrice),
                    sku = selectedSku,
                    addOnId = !Equals(product.AddOns, null) ? product.AddOns.Select(x => x.AddOnId) : null,
                    addOnMessage = !Equals(product.AddOns, null) ? product.AddOns.Select(x => x.OutOfStockMessage) : null,
                    isOutOfStock = !Equals(product.AddOns, null) ? product.AddOns.Select(x => x.IsOutOfStock) : null,
                    html = RenderHelper.PartialView(this, "_attributes", product),
                    //Znode Version 7.2.2
                    //Change Main Image on Product Details page OnChange of Color Attributes - Start
                    imagePath = imagePath,
                    imageMediumPath = imageMediumPath,
                    imageOriginalPath = imageOriginalPath,
                    //Change Main Image on Product Details page OnChange of Color Attributes - End
                    //PRFT custom code: To show Backorder Message on PDP: Start
                    isbackorder = string.IsNullOrEmpty(product.BackOrderMessage) ? false : true,
                    backordermessage = product.BackOrderMessage,
                    //PRFT custom code: To show Backorder Message on PDP:End


                }
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddToCart(CartItemViewModel cartItem,FormCollection collection)
        {
            //PRFT custom code: Start
            int quantity = Convert.ToInt32(collection["MinQuantity"]);
            if (quantity != 0)
            {
                cartItem.Quantity = Convert.ToInt32(collection["MinQuantity"]);
            }
            //PRFT custom code: End
            if (!string.IsNullOrEmpty(cartItem.BundleItemsIds))
            {
                var productsViewModel = _productAgent.GetBundleProductsDetails(cartItem);
                Session.Add(MvcDemoConstants.BundleProductId, cartItem.ProductId);
                return View("BundleSettings", productsViewModel);
            }

            ProductViewModel product = _cartAgent.WishlistCheckInventory(cartItem);

            if (!string.IsNullOrEmpty(product.InventoryMessage) && !product.ShowAddToCart)
            {
                Session.Add("ErrorMessage", product.InventoryMessage);

                return RedirectToAction("Index", "Cart");
            }

            _cartAgent.Create(cartItem);
            return RedirectToAction("Index", "Cart");
        }

        public ActionResult AddToCart()
        {
            if (!Equals(Session[MvcDemoConstants.BundleProductId], null))
            {
                int productId = Convert.ToInt32(Session[MvcDemoConstants.BundleProductId]);
                Session[MvcDemoConstants.BundleProductId] = null;
                return Redirect(_productAgent.GetProductUrl(productId, Url));
            }
            return RedirectToAction(IndexAction, HomeController);
        }

        public ActionResult UpdateBundles()
        {
            if (!Equals(Session[MvcDemoConstants.BundleProductId], null))
            {
                int productId = Convert.ToInt32(Session[MvcDemoConstants.BundleProductId]);
                Session[MvcDemoConstants.BundleProductId] = null;
                return Redirect(_productAgent.GetProductUrl(productId, Url));
            }
            return RedirectToAction(IndexAction, HomeController);
        }

        [HttpPost]
        public ActionResult UpdateBundles(BundleDetailsViewModel bundleCartItem)
        {
            var productsViewModel = _productAgent.GetUpdatedBundles(bundleCartItem);
            ModelState.Clear();
            return View("BundleSettings", productsViewModel);
        }

        [HttpPost]
        public ActionResult ContinueToCart(BundleDetailsViewModel bundleCartItem)
        {
            var item = CartItemViewModelMap.ToViewModel(bundleCartItem);

            var viewModel = _cartAgent.Create(item);

            return RedirectToAction("Index", "Cart");
        }

        public ActionResult WishList(int? id)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return Json(new
                {
                    success = false,
                    message = Resources.ZnodeResource.ErrorLoginWishlist,
                    data = new { style = "info", redirect = "/Account/Login?returnurl=/Product/Wishlist/" + id }
                }, JsonRequestBehavior.AllowGet);
            }

            var wishListViewModel = _productAgent.CreateWishList(id.GetValueOrDefault(0));

            if (Request.IsAjaxRequest())
            {
                return Json(new
                {
                    success = !wishListViewModel.HasError,
                    message = !wishListViewModel.HasError ? Resources.ZnodeResource.SuccessProductAddWishlist : Resources.ZnodeResource.ErrorProductAddWishlist,
                    data = new { style = !wishListViewModel.HasError ? "success-msg" : "error-msg", link = "/Account/Wishlist" }
                }, JsonRequestBehavior.AllowGet);
            }

            if (wishListViewModel.HasError)
            {
                Session.Add("ErrorMessage", Resources.ZnodeResource.ErrorProductAddWishlist);
            }
            else
            {
                Session.Add("SuccessMessage", Resources.ZnodeResource.SuccessProductAddWishlist);
            }

            return Redirect(_productAgent.GetProductUrl(id.GetValueOrDefault(0), Url));
        }

        public ActionResult Allreviews(int id)
        {
            var reviewModelList = _productAgent.GetAllReviews(id);
            return View("AllReviews", reviewModelList);
        }

        [HttpGet]
        public ActionResult GetHomePageSpecials(bool? isNewProduct)
        {

            var viewModel = _productAgent.GetHomeSpecials();
            PortalViewModel portal = PortalAgent.CurrentPortal;
            if (!Equals(isNewProduct, null))
            {
                viewModel = (bool)isNewProduct ? new Collection<ProductViewModel>(viewModel.Where(x => x.IsNewProduct).ToList()) : new Collection<ProductViewModel>(viewModel.Where(x => x.IsNewProduct.Equals(false)).ToList());
            }

            ViewBag.IsEnableCompare = portal.IsEnableCompare;
            if (Equals(viewModel, null) || viewModel.Count < 1)
            {
                return new EmptyResult();
            }
            return PartialView("_ProductGrid", viewModel);
        }

        [HttpGet]
        public ActionResult GetHomePageSpecialsNewTheme(bool? isNewProduct)
        {

            var viewModel = _productAgent.GetHomeSpecials();
            PortalViewModel portal = PortalAgent.CurrentPortal;
            if (!Equals(viewModel, null) && viewModel.Count > 0 && !Equals(isNewProduct, null))
            {
                viewModel = (bool)isNewProduct ? new Collection<ProductViewModel>(viewModel.Where(x => x.IsNewProduct).ToList()) : new Collection<ProductViewModel>(viewModel.Where(x => x.IsNewProduct.Equals(false)).ToList());
            }

            ViewBag.IsEnableCompare = portal.IsEnableCompare;
            if (Equals(viewModel, null) || viewModel.Count < 1)
            {
                return new EmptyResult();
            }
            return PartialView("_ProductGridHome", viewModel);
        }

        [HttpGet]
        public ActionResult GetProductsByIds(string ids)
        {

            var viewModel = new Collection<ProductViewModel>(_productAgent.GetProductByIds(ids).ToList());
            if (!viewModel.Any())
            {
                return new EmptyResult();
            }
            return PartialView("_ProductGrid", viewModel);
        }


        public ActionResult GetBundleDisplay(string ids)
        {
            if (string.IsNullOrEmpty(ids))
            {
                return new EmptyResult();
            }
            var viewModel = _productAgent.GetBundles(ids);
            if (viewModel == null || viewModel.Count < 1)
            {
                return new EmptyResult();
            }
            return PartialView("_Bundles", viewModel);
        }

        public ActionResult Writereview(int id, string name)
        {
            var model = new ReviewItemViewModel();
            model.Product = new ProductViewModel();
            model.ProductId = id;
            model.Product.Name = name;
            ProductViewModel productDetails = _productAgent.GetProduct(id);
            model.Product.SeoPageName = (!Equals(productDetails, null) && !string.IsNullOrEmpty(productDetails.SeoPageName)) ? productDetails.SeoPageName : string.Empty;
            model.Rating = 1;
            return View("WriteReview", model);
        }

        [HttpPost]
        public ActionResult Writereview(ReviewItemViewModel reviewItem, int id, string name)
        {
            reviewItem.Product = new ProductViewModel { Name = name };
            reviewItem.ProductId = id;

            if (ModelState.IsValid)
            {
                reviewItem.ProductId = id;
                var model = _productAgent.CreateReview(reviewItem);

                if (!model.HasError)
                {
                    ModelState.Clear();
                    var reviewItemViewModel = new ReviewItemViewModel { Product = new ProductViewModel(), ProductId = id };
                    reviewItemViewModel.Product.Name = name;
                    reviewItemViewModel.Rating = 1;
                    ProductViewModel productDetails = _productAgent.GetProduct(id);
                    reviewItemViewModel.Product.SeoPageName = (!Equals(productDetails, null) && !string.IsNullOrEmpty(productDetails.SeoPageName)) ? productDetails.SeoPageName : string.Empty;
                    reviewItemViewModel.SuccessMessage = model.SuccessMessage;

                    return View("WriteReview", reviewItemViewModel);
                }
            }
            if (id > 0)
            {
                ProductViewModel productDetails = _productAgent.GetProduct(id);
                reviewItem.Product.SeoPageName = (!Equals(productDetails, null) && !string.IsNullOrEmpty(productDetails.SeoPageName)) ? productDetails.SeoPageName : string.Empty;
            }
            return View("WriteReview", reviewItem);
        }

        public ActionResult wfbundlesettings()
        {
            return View("BundleSettings");

        }

        public IEnumerable<SelectedBundleViewModel> GetBundles(FormCollection formCollection)
        {
            var bundles = new Collection<SelectedBundleViewModel>();

            if (formCollection.Count > 0)
            {
                if (formCollection.GetValues("Attribute") != null)
                {
                    var values = formCollection.GetValues("Attribute");
                }

            }

            return bundles;
        }

        /// <summary>
        /// Get Product sku list contaning sku as a substring.
        /// </summary>
        /// <param name="sku">Sku for the products.</param>
        /// <returns>Returns the Sku product list.</returns>
        public JsonResult GetProductListBySKU(string sku)
        {
            return Json(_productAgent.GetSkuProductListBySKU(sku), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get the view of Quick Order pad.
        /// </summary>
        /// <returns>Returns the view of Quick Order pad.</returns>
        public ActionResult QuickOrderPad()
        {
            return View(QuickOrderPadView);
        }

        /// <summary>
        /// Add to cart multiple orders.
        /// </summary>
        /// <param name="cartItems">cartItems to add to cart.</param>
        /// <returns>Returns the Quick order pad view.</returns>
        [HttpPost]
        public ActionResult AddMultipleOrdersToCart(List<CartItemViewModel> cartItems)
        {
            string errorMessage = _productAgent.AddMultipleProductsToCart(cartItems);
            return Json(new
            {
                isSuccess = string.IsNullOrEmpty(errorMessage),
                message = string.IsNullOrEmpty(errorMessage) ? ZnodeResource.MessageSuccesfullyAdded : ZnodeResource.MessageNotAddedSucessfully,
                cartCount = _cartAgent.GetCartCount(),
            }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get all associated product highlights on the basis od product id.
        /// </summary>
        /// <param name="productId">Product Id.</param>
        /// <returns>Returns partial view _ProductHighlight.</returns>
        public ActionResult GetAssociatedProductHighlights(int productId)
        {
            HighlightListViewModel associatedProductHighlights = _productAgent.GetProductHighlights(productId, null);
            associatedProductHighlights.Highlights.ForEach(x => x.ProductId = productId);
            return View(ProductHighlight, associatedProductHighlights);
        }

        /// <summary>
        /// Get highlight description.
        /// </summary>
        /// <param name="highLight">highlight</param>
        /// <returns>Returns highlight info view.</returns>
        public ActionResult GetHighlightInfo(int highLight, int productId)
        {
            HighlightListViewModel _list = _productAgent.GetProductHighlights(productId, null);
            HighlightViewModel _highlight = _list.Highlights.FirstOrDefault(x => x.HighlightId == highLight);
            return View(HighlightInfo, _highlight);
        }
        #region Znode Version 7.2.2

        /// <summary>
        /// To get recently view product asynchronous
        /// </summary>
        /// <param name="productId">nullable int productId</param>
        /// <returns>returns JsonResult</returns>
        [HttpGet]
        public JsonResult GetRecentProducts(int? productId, int? productCount)
        {
            Collection<ProductViewModel> _ProductList = GetRecentlyViewItems(productId);
            
            ViewBag.ProductCount = productCount.HasValue?productCount:0;
            return Json(new
            {
                success = true,
                message = productId,
                data = new
                {
                    style = "success",
                    html = _ProductList == null ? "" : RenderHelper.PartialView(this, "_RelatedItems", _ProductList),
                }
            }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// To get max number of recently view item of current session
        /// Max number of items set from admin
        /// </summary>
        /// <param name="productId">nullable int productId</param>
        /// <returns>collection of Recently View Items</returns>
        private Collection<ProductViewModel> GetRecentlyViewItems(int? productId)
        {
            Collection<ProductViewModel> _ProductList = new Collection<ProductViewModel>();
            if (!productId.Equals(null))
            {

                _ProductList = GetRecentProductFromSession();
                if (!IsProductExistInList(_ProductList, productId.Value))
                {
                    _ProductList = AddProductInList(_ProductList, productId);
                    _ProductList = SetMaxRecentProductInSession(_ProductList);
                }
            }
            else
            {
                _ProductList = GetRecentProductFromSession();
            }
            //To reverse the product list for showing latest item at first position
            if (!Equals(_ProductList, null))
            {
                _ProductList = new Collection<ProductViewModel>
                    (
                      _ProductList.Reverse().ToList()
                    );
            }
            return _ProductList;
        }

        /// <summary>
        /// To get all recently view item from session object
        /// </summary>
        /// <returns>collection of Recently View Items</returns>
        private Collection<ProductViewModel> GetRecentProductFromSession()
        {
            Collection<ProductViewModel> _ProductList = null;
            try
            {
                if (!Equals(Session[_recentlyViewProductList], null))
                {
                    _ProductList = new Collection<ProductViewModel>();
                    _ProductList = Session[_recentlyViewProductList] as Collection<ProductViewModel>;
                }
            }
            catch
            {
            }
            return _ProductList;
        }

        /// <summary>
        /// To set recently view item in session object
        /// </summary>
        /// <param name="productList">collection of Recently View Items</param>
        /// <returns>collection of Recently View Items as per maximum items</returns>
        private Collection<ProductViewModel> SetMaxRecentProductInSession(Collection<ProductViewModel> productList)
        {
            try
            {
                int maxItemToDisplay = MvcDemoConstants.MaxRecentViewItemToDisplay;
                if (productList.Count > maxItemToDisplay)
                {
                    int difference = productList.Count - maxItemToDisplay;
                    for (int count = 0; count < difference; count++)
                    {
                        productList.RemoveAt(0);
                    }
                }

                if (productList.Count > 0)
                {
                    Session[_recentlyViewProductList] = productList;
                }
            }
            catch
            {
            }
            return productList;
        }

        /// <summary>
        /// To check current product is exist in recently viewed product List by ProductId
        /// </summary>
        /// <param name="productList">collection of Recently View Items</param>
        /// <param name="productId">int productId</param>
        /// <returns>bool true/false</returns>
        private bool IsProductExistInList(Collection<ProductViewModel> productList, int productId)
        {
            if (!Equals(productList, null))
            {
                var items = productList.FirstOrDefault(p => p.ProductId.Equals(productId));
                if (Equals(items, null))
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// To add current product in recently viewed product List by ProductId
        /// </summary>
        /// <param name="productList">collection of Recently View Items</param>
        /// <param name="productId">int productId</param>
        /// <returns>collection of Recently View Items</returns>
        private Collection<ProductViewModel> AddProductInList(Collection<ProductViewModel> productList, int? productId)
        {
            if (!productId.Equals(null))
            {
                ProductViewModel product = _productAgent.GetProduct(productId.Value);
                if (Equals(productList, null))
                {
                    productList = new Collection<ProductViewModel>();
                }
                productList.Add(product);
            }
            return productList;
        }

        #endregion

        #region Product Compare
        /// <summary>
        /// Add Product in compare product list.
        /// </summary>
        /// <param name="productId">The productId to add in compare product list</param>
        /// <param name="categoryId">The categoryId to add in compare product list</param>
        /// <returns>Returns json result containing product comparison View</returns>
        public ActionResult GlobalLevelCompareProduct(int productId, int categoryId)
        {
            string message = string.Empty;
            int errorCode;
            Collection<ProductViewModel> products = new Collection<ProductViewModel>();
            bool status = _productAgent.GlobalAddProductToCompareList(productId, categoryId, out message, out errorCode);
            if (status)
            {
                products = _productAgent.GetCompareProducts();
            }
            return Json(new
            {
                success = status,
                message = message,
                count = products.Count,
                errorCode = errorCode,
                data = new
                {
                    html = Equals(products, null) ? string.Empty : RenderHelper.PartialView(this, ComparableProductsView, products),
                }
            }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Add Product in compare product list.
        /// </summary>
        /// <param name="productId">The productId to add in compare product list</param>
        /// <param name="categoryId">The categoryId to add in compare product list</param>
        /// <returns>Returns json result containing product comparison View</returns>
        public ActionResult CategoryLevelCompareProduct(int productId, int categoryId)
        {
            string message = string.Empty;
            Collection<ProductViewModel> products = new Collection<ProductViewModel>();
            bool status = _productAgent.CategoryAddProductToCompareList(productId, categoryId, out message);
            if (status)
            {
                products = _productAgent.GetCompareProducts();
            }
            return Json(new
            {
                success = status,
                message = message,
                data = new
                {
                    html = Equals(products, null) ? string.Empty : RenderHelper.PartialView(this, ComparableProductsView, products),
                }
            }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get the list of products added for comparison
        /// </summary>
        /// <returns>Returns json result containing comparable product View</returns>
        public ActionResult ShowCompareProduct()
        {
            Collection<ProductViewModel> products = _productAgent.GetCompareProducts();
            return Json(new
            {
                count = products.Count,
                data = new
                {
                    html = Equals(products, null) ? string.Empty : RenderHelper.PartialView(this, ComparableProductsView, products),
                }
            }, JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// Display comparison between products added for comparison
        /// </summary>
        /// <returns>Return View Comparison View</returns>
        public ActionResult ViewComparison()
        {
            CompareProductViewModel compareProducts = new CompareProductViewModel();
            compareProducts.ProductList = _productAgent.GetCompareProductsDetails(compareProducts);
            if (compareProducts.ProductList.Count <= 0)
            {
                return RedirectToAction(IndexAction, HomeController);
            }
            return View(ViewComparisonView, compareProducts);
        }

        /// <summary>
        /// Display error message for product limit to compare.
        /// </summary>
        /// <returns>Returns json result</returns>
        public JsonResult ViewProductComparison()
        {
            bool status = false;
            string message = string.Empty;
            CompareProductViewModel compareProducts = new CompareProductViewModel();
            compareProducts.ProductList = _productAgent.GetCompareProductsDetails(compareProducts);
            if (Equals(compareProducts.ProductList, null) || compareProducts.ProductList.Count <= 1)
            {
                status = true;
                message = ZnodeResource.ProductCompareQauntityErrorMessage;
            }
            return Json(new { status = status, message = message });
        }

        /// <summary>
        /// Remove a product from compare list
        /// </summary>
        /// <param name="productId">The productId to remove from compare list</param>
        /// <param name="control">The control is controller name from query string</param>
        /// <returns>Returns json result containing comparable product or product comparison view</returns>
        public ActionResult RemoveProductFormSession(int productId, string control)
        {
            bool status = false;
            string message = string.Empty;
            CompareProductViewModel compareProducts = new CompareProductViewModel();
            if (!Equals(productId, 0))
            {
                status = _productAgent.RemoveProductFormSession(productId);
            }

            if (Equals(control, MvcDemoConstants.ProductControllerText))
            {
                compareProducts.ProductList = _productAgent.GetCompareProductsDetails(compareProducts);
                if (Equals(compareProducts.ProductList, null) || compareProducts.ProductList.Count <= 1)
                {
                    message = ZnodeResource.ProductCompareErrorMessage;
                }
                return Json(new
                {
                    success = status,
                    message = message,
                    count = compareProducts.ProductList.Count,
                    data = new
                    {
                        html = Equals(compareProducts.ProductList, null) ? string.Empty : RenderHelper.PartialView(this, ProductComparisonListView, compareProducts.ProductList),
                    }
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                compareProducts.ProductList = _productAgent.GetCompareProducts();
                return Json(new
                {
                    success = status,
                    count = compareProducts.ProductList.Count,
                    data = new
                    {
                        html = Equals(compareProducts.ProductList, null) ? string.Empty : RenderHelper.PartialView(this, ComparableProductsView, compareProducts.ProductList),
                    }
                }, JsonRequestBehavior.AllowGet);
            }

        }

        /// <summary>
        /// Remove all the products from compare list
        /// </summary>
        /// <returns>Redirect to View Comparison View</returns>
        public ActionResult DeleteComparableProducts()
        {
            bool status = _productAgent.DeleteComparableProducts();
            return RedirectToAction(IndexAction, HomeController);
        }

        /// <summary>
        /// Send a mail to friend
        /// </summary>
        /// <returns>Send Mail View</returns>
        public ActionResult SendComparedProductMail()
        {
            ViewBag.IsCompare = true;
            return View(SendMailPartialView, new CompareProductViewModel());
        }

        /// <summary>
        /// Send a mail to friend
        /// </summary>
        /// <param name="compareProducts">Compare product contains sender nad receiver mail addresses</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SendComparedProductMail(string senderMailId, string recieverMailId)
        {
            CompareProductViewModel compareProducts = new CompareProductViewModel();
            compareProducts.SenderEmailAddress = senderMailId;
            compareProducts.RecieverEmailAddress = recieverMailId;
            compareProducts.ProductList = _productAgent.GetCompareProductsDetails(compareProducts);
            bool isMailSend = _productAgent.SendComparedProductMail(compareProducts);
            return Json(new { success = isMailSend }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        [HttpGet]
        public ActionResult SendMailToFriend(int productId, string productName)
        {
            CompareProductViewModel product = new CompareProductViewModel();
            product.ProductId = productId;
            product.ProductName = productName;
            return View(SendMailPartialView, product);
        }

        //[HttpPost]
        //public ActionResult SendMailToFriend(int productId, string senderMailId, string recieverMailId)
        //{
        //    CompareProductViewModel compareProducts = new CompareProductViewModel();
        //    compareProducts.SenderEmailAddress = senderMailId;
        //    compareProducts.ProductId = productId;
        //    compareProducts.RecieverEmailAddress = recieverMailId;
        //    bool mailSend = _productAgent.SendMailToFriend(compareProducts);
        //    return Json(new { success = mailSend }, JsonRequestBehavior.AllowGet);
        //}

        [HttpPost]
        public ActionResult SendMailToFriend(int productId, string senderMailId, string recieverMailId,string captchaResponse)
        {
            bool mailSend = false;
            if (RecaptchaValidation(captchaResponse))
            {
                CompareProductViewModel compareProducts = new CompareProductViewModel();
                compareProducts.SenderEmailAddress = senderMailId;
                compareProducts.ProductId = productId;
                compareProducts.RecieverEmailAddress = recieverMailId;
                mailSend = _productAgent.SendMailToFriend(compareProducts);
            }
            return Json(new { success = mailSend }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetProductBreadCrumb(int? productId, string productName, bool isQuickView)
        {
            HtmlString productBreadCrumb = null;
            string categoryName = string.Empty;
            productName = isQuickView ? string.Empty : productName;
            if (!Equals(productId, null) && productId > 0 && !string.IsNullOrEmpty(productName))
            {
                productBreadCrumb = BreadCrumbHelper.GetProductSiteMapById(productId.Value, productName, out categoryName);
            }
            return Json(new
            {
                data = new
                {
                    ProductBreadCrumb = Convert.ToString(productBreadCrumb),
                    CategoryName = categoryName,
                    CategoryUrl = "/search?category=" + WebUtility.UrlEncode(categoryName)
                }
            }, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult SetProductCategory(int? categoryId, string categoryName)
        {
            bool isUpdated = false;

            if (!Equals(categoryId, null) && categoryId > 0 && !string.IsNullOrEmpty(categoryName))
            {
                Session[MvcDemoConstants.CategoryName] = categoryName;
                Session[MvcDemoConstants.CategoryId] = categoryId;
                isUpdated = true;
            }
            return Json(new
            {
                data = isUpdated
            }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Method to get out of stock details for a product.
        /// </summary>
        /// <param name="productId">productId</param>
        /// <returns>Returns boolean value ShowAddToCart.</returns>
        public bool GetProductOutOfStockDetails(int productId)
        {
            var product = _productAgent.GetProduct(productId);
            if (!string.IsNullOrEmpty(product.BundleItemsIds))
            {
                BundleDetailsViewModel bundleProduct = _productAgent.GetCartModel(product);
                product.ShowAddToCart = bundleProduct.Bundles.FirstOrDefault().ShowAddToCart;
            }
            return product.ShowAddToCart;
        }

        #region PRFT Custom Code
        private bool RecaptchaValidation(string response)
        {
            bool isValid = false;
            //secret that was generated in key value pair
            string secret = ConfigurationManager.AppSettings["recaptchaPrivateKey"];
            var client = new WebClient();
            var reply = client.DownloadString(string.Format(ConfigurationManager.AppSettings["recaptchaValidationUrl"], secret, response));

            var captchaResponse = JsonConvert.DeserializeObject<CaptchaResponse>(reply);

            //when response is false check for the error message
            if (captchaResponse != null && captchaResponse.Success)
            {
                isValid = true;
            }
            return isValid;
        }
        #endregion
    }
}
