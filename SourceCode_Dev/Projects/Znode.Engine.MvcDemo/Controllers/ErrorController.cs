﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Znode.Engine.MvcDemo.Controllers
{
    public class ErrorController : BaseController
    {
        // 
        // GET: /Error/

        public ActionResult Error404(string aspxErrorPath)
        {
            HttpContext.Response.StatusCode = 404;
            HttpContext.Response.TrySkipIisCustomErrors = true;
            return View("Error404");
        }

        public ActionResult UnhandledError(string aspxerrorpath)
        {
            return View("Error");
        }

    }
}
