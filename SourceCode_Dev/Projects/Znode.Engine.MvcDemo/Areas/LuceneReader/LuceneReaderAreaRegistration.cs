﻿using System.Web.Mvc;

namespace Znode.Engine.MvcDemo.Areas.LuceneReader
{
    public class LuceneReaderAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "LuceneReader";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "LuceneReader_default",
                "LuceneReader/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}