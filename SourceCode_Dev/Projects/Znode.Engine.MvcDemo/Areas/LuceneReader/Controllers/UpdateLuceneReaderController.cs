﻿using Resources;
using System.Web.Mvc;
using Znode.Engine.MvcDemo.Agents;
using Znode.Engine.MvcDemo.Controllers;

namespace Znode.Engine.MvcDemo.Areas.LuceneReader.Controllers
{
    public class UpdateLuceneReaderController : BaseController
    {
        #region Variables
        private IUpdateLuceneReaderAgent _agent;
        #endregion

        #region Constructor
        public UpdateLuceneReaderController()
        {
            _agent = new UpdateLuceneReaderAgent();
        }
        #endregion

        #region Index
        /// <summary>
        /// This method updates the lucene reader
        /// </summary>
        /// <returns>returns null value if updated successfull, or returns the error message.</returns>
        public ActionResult Index()
        {
            try
            {
                _agent.UpdateReader();
                return null;
            }
            catch
            {
                return Content(ZnodeResource.ErrorUpdatingLuceneReader);
            }
        }
        #endregion
    }
}