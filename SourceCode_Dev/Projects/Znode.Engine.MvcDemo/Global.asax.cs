﻿using System;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Znode.Engine.MvcDemo.Agents;
using Znode.Engine.MvcDemo.Controllers;
using Znode.Engine.MvcDemo.Helpers;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            //Removing X-AspNetMvc-Version
            MvcHandler.DisableMvcResponseHeader = true;

            AreaRegistration.RegisterAllAreas();
            MvcConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new ThemedViewEngine());
            RegisterSocialClient();
        }

        protected void Application_BeginRequest(Object sender, EventArgs e)
        {
            //PRFT Custom Code for allowing TLS12 with the new FedEx updates
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            string currentUrl = string.Format("~{0}", Request.Path.ToLower());

            var redirectUrl = UrlRedirectAgent.Has301Redirect(currentUrl);
            if (!Equals(redirectUrl, null))
            {
                Context.Response.RedirectPermanent(redirectUrl.NewUrl);
            }
            #region PRFT Custom Code: SSL Enable

            HttpContextBase currentContext = new HttpContextWrapper(HttpContext.Current);
            UrlHelper urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);

            if (!Equals(currentContext, null) && !Equals(urlHelper, null))
            {
                RouteData routeData = urlHelper.RouteCollection.GetRouteData(currentContext);
                string action = Equals(routeData, null) ? string.Empty : routeData.Values["action"] as string;
                string controllerName = Equals(routeData, null) ? string.Empty : routeData.Values["controller"] as string;
                //string sslEnableActionName = ConfigurationManager.AppSettings["SSLEnableActionName"].ToString();
                string sslEnableActionName = Resources.ZnodeResource.SSLEnableActionName.ToString();
                string[] sslEnableActionNameArray = sslEnableActionName.Split(',');
                if (!string.IsNullOrEmpty(action) && (sslEnableActionNameArray.Contains(action, StringComparer.OrdinalIgnoreCase) || controllerName.Equals("checkout", StringComparison.CurrentCultureIgnoreCase)) && PortalAgent.CurrentPortal.UseSsl && !HttpContext.Current.Request.IsSecureConnection)
                {
                    Response.Redirect("https://" + Request.ServerVariables["HTTP_HOST"] + HttpContext.Current.Request.RawUrl);
                }
            }
            #endregion
        }

        private void Session_Start(object sender, EventArgs e)
        {
            string id = Session.SessionID;

            if (PortalAgent.CurrentPortal.PersistentCartEnabled)
            {
                var _cartAgent = new CartAgent();

                var cartViewModel = _cartAgent.GetCart();

                if (cartViewModel != null && cartViewModel.Items.Any())
                {
                    Session[MvcDemoConstants.CartMerged] = true;
                    Response.Redirect("~/Cart");
                }
            }
        }

        public override string GetVaryByCustomString(HttpContext context, string custom)
        {
            var customstring = string.Empty;
            if (custom.ToLower().Contains("portal"))
            {
                customstring = context.Request.Url.Authority;
            }

            if (custom.ToLower().Contains("account") && HttpContext.Current.User.Identity.IsAuthenticated)
            {
                customstring += context.User.Identity.Name;
            }

            if (!string.IsNullOrEmpty(customstring))
            {
                return customstring;
            }

            return base.GetVaryByCustomString(context, custom);
        }


        protected void Application_Error(Object sender, EventArgs e)
        {
            Exception exception = null;
            try
            {
                exception = Server.GetLastError().GetBaseException();
                LogMessage(exception);
                RedirectToErrorPage(sender, exception);
            }
            catch (Exception ex)
            {
                LogMessage(ex);
                RedirectToErrorPage(sender, ex);
            }
        }

        private void LogMessage(Exception exception)
        {
            if (!Equals(exception, null))
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(exception);
            }
        }

        private void RedirectToErrorPage(Object sender, Exception exception)
        {
            var controller = new HomeController();
            var httpContext = ((MvcApplication)sender).Context;

            var routeData = new RouteData();
            httpContext.ClearError();
            httpContext.Response.Clear();

            SetErrorPage(exception, routeData);

            ((IController)controller).Execute(new RequestContext(new HttpContextWrapper(httpContext), routeData));
        }

        private void SetErrorPage(Exception exception, RouteData routeData)
        {
            routeData.Values.Add("controller", "home");
            routeData.Values.Add("action", "ErrorHandler");
            routeData.Values.Add("exception", exception);
        }

        private void RegisterSocialClient()
        {
            try
            {
                RegisteredSocialClientListViewModel model = AccountAgent.GetRegisteredSocialClientDetails();
                AuthConfig.RegisterAuth(model);
            }
            catch { }
        }


    }
}
