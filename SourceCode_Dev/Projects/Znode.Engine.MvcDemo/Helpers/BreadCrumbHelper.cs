﻿using System;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.MvcDemo.Agents;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Helpers
{
    public static class BreadCrumbHelper
    {
        /// <summary>
        /// Generate BreadCrumb by taking Category Name from Request
        /// </summary>
        /// <param name="helper">HtmlHelper helper</param>
        /// <param name="request">HttpRequestBase request</param>
        /// <returns>HtmlString</returns>
        public static HtmlString GetSiteMapByRequest(this HtmlHelper helper, HttpRequestBase request)
        {
            if (Equals(request, null)) return new HtmlString(string.Empty);
            string actionName = !Equals(Convert.ToString(request.RequestContext.RouteData.Values[MvcDemoConstants.Action]), null) ? Convert.ToString(request.RequestContext.RouteData.Values["Action"]) : string.Empty;
            if (string.Equals(actionName.ToLower(), "writereview"))
            {
                return new HtmlString(string.Empty);
            }
            HtmlString breadcrumsPath = null;
            string productName = string.Empty;
            try
            {
                int categoryId = !Equals(Convert.ToString(request.RequestContext.RouteData.Values[MvcDemoConstants.CategoryId]), null) ? Convert.ToInt32(request.RequestContext.RouteData.Values[MvcDemoConstants.CategoryId]) : 0;
                int productId = !Equals(Convert.ToInt32(request.RequestContext.RouteData.Values[MvcDemoConstants.Id]), null) ? Convert.ToInt32(request.RequestContext.RouteData.Values[MvcDemoConstants.Id]) : 0;
                string seoUrl = !Equals(Convert.ToString(request.RequestContext.RouteData.Values[MvcDemoConstants.SEOURL]), null) ? Convert.ToString(request.RequestContext.RouteData.Values[MvcDemoConstants.SEOURL]) : string.Empty;

                if (!Equals(productId, null) && productId > 0)
                {
                    ProductAgent _productAgent = new ProductAgent();
                    ProductViewModel product = _productAgent.GetProduct(productId);
                    categoryId = !Equals(product, null) ? product.CategoryId : 0;
                    productName = !Equals(product, null) ? string.Format("   \\   {0}", product.Name) : string.Empty;
                }
                seoUrl = categoryId > 0 ? string.Empty : seoUrl;
                bool isProductPresent = !string.IsNullOrEmpty(productName) ? true : false;
                return breadcrumsPath = GetSiteMap(categoryId, productName, seoUrl, isProductPresent);
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Get the Product BreadCrumb
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <param name="productName">string productName</param>
        /// <param name="categoryName">string categoryName</param>
        /// <returns>Return the BreadCrumb String</returns>
        public static HtmlString GetProductSiteMapById(int productId, string productName, out string categoryName)
        {
            HtmlString breadcrumbPath = null;
            categoryName = Convert.ToString(HttpContext.Current.Session[MvcDemoConstants.CategoryName]);
            try
            {

                if (!Equals(productId, null) && productId > 0 && !string.IsNullOrEmpty(productName))
                {
                    int categoryId = (!Equals(HttpContext.Current.Session[MvcDemoConstants.CategoryId], null)) ? Convert.ToInt32(HttpContext.Current.Session[MvcDemoConstants.CategoryId]) : 0;
                    if (categoryId > 0)
                    {
                        productName = string.Format("   \\   <span>{0}<span>", productName);
                        breadcrumbPath = GetSiteMap(categoryId, productName, string.Empty, true);
                    }
                }
            }
            catch (Exception)
            {
                return null;
            }
            return breadcrumbPath;
        }

        /// <summary>
        /// Generate Complete BreadCrumb
        /// </summary>
        /// <param name="categoryName">string categoryName</param>
        /// <param name="productName">string productName</param>
        /// <param name="seoTitle">string seoTitle</param>
        /// <returns>HtmlString</returns>
        private static HtmlString GetSiteMap(int categoryId, string productName, string seoUrl, bool isProductPresent)
        {
            CategoryAgent _categoryAgent = new CategoryAgent();
            if (categoryId < 1 && string.IsNullOrEmpty(seoUrl))
            {
                return new HtmlString(string.Empty);
            }
            string breadCrum = string.Empty;
            MessageConfigsAgent _messageConfigsAgent = new MessageConfigsAgent();

            string messageconfig = _messageConfigsAgent.GetByKey("BreadCrumbsHomeLinkText");
            breadCrum = string.Format("<a href='/'>{0}</a>", messageconfig);

            breadCrum = string.Format("{0}{1}", breadCrum, _categoryAgent.GetBreadCrumb(categoryId, seoUrl, isProductPresent)) + productName;
            //PRFT :CUSTOM CODE:STARTS
            breadCrum=breadCrum.Replace("\\", "/");
            //PRFT :CUSTOM CODE:ENDS
            return new HtmlString(breadCrum);
        }
    }
}
