﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcDemo.Helpers
{
    public static class SearchHelper
    {
        //const string QuerystringDefaultParams = "category,text,pagenumber,swt,sort,pagesize,searchterm";
        //static string QuerystringDefaultParams = Convert.ToString(ConfigurationManager.AppSettings["QuerystringDefaultParams"]);

        //static string[] QuerystringDefaultParams = { "category","text","pagenumber","swt","sort", "pagesize", "searchterm" };
        static string[] QuerystringDefaultParams = ConfigurationManager.AppSettings["QuerystringDefaultParams"].Split(',');

        const string DefaultSortOption = "1";

        public static Collection<KeyValuePair<string, IEnumerable<string>>> GetRefineBy()
        {
            // Written the below 2 lines to handle the test methods (since we don't have page querystrings.
            if (HttpContext.Current.Request == null)
                return new Collection<KeyValuePair<string, IEnumerable<string>>>();


            var refineBy = (from object item in HttpContext.Current.Request.QueryString.Keys
                            select item.ToString()
                                into itemString
                            where !QuerystringDefaultParams.Contains(itemString.ToLower())
                            select new KeyValuePair<string, IEnumerable<string>>(itemString, HttpContext.Current.Request.QueryString.GetValues(itemString).SelectMany(x => x.Split(',')))).ToList();


            if (refineBy.Any())
                return new Collection<KeyValuePair<string, IEnumerable<string>>>(refineBy);

            return new Collection<KeyValuePair<string, IEnumerable<string>>>();
        }

        public static string GetSortBy(string sortBy)
        {
            switch (sortBy)
            {
                case "2":
                    return @"rating|desc";
                case "3":
                    return @"price|desc";
                case "4":
                    return @"price|asc";
                case "5":
                    return @"name|asc";
                case "6":
                    return @"name|desc";
                default:
                    return @"relevance|asc";
            }
        }
    }
}