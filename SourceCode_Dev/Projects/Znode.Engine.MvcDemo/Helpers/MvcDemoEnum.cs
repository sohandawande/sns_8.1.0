﻿
namespace Znode.Engine.MvcDemo.Helpers
{
    #region Reset Password Status Types
    public enum ResetPasswordStatusTypes
    {
        Continue = 2001,
        LinkExpired = 2002,
        TokenMismatch = 2003,
        NoRecord = 2004,
    }
    #endregion

    /// <summary>
    /// This enum have Product compare value
    /// </summary>
    public enum ProductCompare
    {
        GlobalLevelCompare,
        CategoryLevelCompare
    }

    /// <summary>
    /// This enum have error code value for product compare
    /// </summary>
    public enum ProductCompareErrorCode
    {
        CategoryChanged = 1,
        ProductExist = 2,
        MaxProductLimit = 3
    }

    //PRFT Custom Code: Start
    public enum CustomValue
    {
        One=1,
        Two=2,
        Three=3
    }
    //PRFT Custom Code: End
}