﻿using System.IO;
using System.Web.Mvc;

namespace Znode.Engine.MvcDemo.Helpers
{
    public static class RenderHelper
    {
        public static string PartialView(Controller controller, string viewName, object model)
        {
            // Added to handle nunit test cases, as context not available when nunit runs.
            if (controller.ControllerContext == null) return string.Empty;

            controller.ViewData.Model = model;

            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);
                var viewContext = new ViewContext(controller.ControllerContext, viewResult.View, controller.ViewData, controller.TempData, sw);

                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(controller.ControllerContext, viewResult.View);

                return sw.ToString();
            }
        }
    }
}