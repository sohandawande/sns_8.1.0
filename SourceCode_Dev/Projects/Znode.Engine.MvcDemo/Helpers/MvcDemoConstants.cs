﻿using Znode.Engine.MvcDemo.Agents;

namespace Znode.Engine.MvcDemo.Helpers
{
    public static class MvcDemoConstants
    {

        public static string CartModelSessionKey = "ShoppingCartModel";
        public static string TempCartModelSessionKey = "TempShoppingCartModel";

        public static string CartViewModelSessionKey = "ShoppingCartModel";
        public static string StatesSessionKey = "States";
        public static string CartCookieKey = "CookieMappingID";
        public static string CartMerged = "CookieMerged";
        public static string PaypalSessionKey = "PaypalModel";
        public static string AffiliateIdSessionKey = "AffiliateId";

        //PRFT Custom Code : Start
        //public static int DefaultPageSize = 8;
        public static int DefaultPageSize = 12;
        //PRFT Custom Code : End

        public static int DefaultPageNumber = 1;
        public static int DefaultHomeSpecialsSize = 20;

        public static string ShippingAddressKey = "ShippingAddress";
        public static string BillingAddressKey = "BillingAddress";
        public static string BillingEmailAddress = "BillingEmailAddress";
        public static string DefaultAddressKey = "Default Address";
        public static string NewAddressKey = "New Address";
        public static string LoginKey = "LoginDetails";

        public static string RouteBillingAddress = "/checkout/billingaddress";
        public static string RouteGuestCheckoutShipping = "/checkout/shipping";

        public static string RouteShoppingCart = "Cart";
        public static string RouteCheckOutPaypal = "checkout/Paypal";
        public static string RouteCheckOutSuccess = "checkout/custom";

        public static string AccountKey = "UserAccount";

        public static int DashboardOrderCount = 3;

        public static string AddressLink = "/account/Address/{0}";

        public static int ReviewStarCount = 5;

        public static int ReviewCommentsCount = 300;

        public static string DefaultReviewStatus = "N";

        public static string ApprovedReviewStatus = "A";

        public static long ReviewTimeSixty = 60;

        public static long ReviewTimeOneTwenty = 120;

        public static long ReviewTimeTwoSevenHundred = 2700;

        public static long ReviewTimeFiveFourHundred = 5400;

        public static long ReviewTimeEightySixFourHundred = 86400;

        public static long ReviewTimeOneSeventyTwoEightHundred = 172800;

        public static long ReviewTimeTwentyFiveNinetyTwo = 2592000;

        public static long ReviewTimeThreeoneonezeroFour = 31104000;

        public static int DaysInMonth = 30;

        public static int DaysInYear = 365;

        public static int ProductReviewCount = 4;

        public static string AddonDisplayTypeDropDown = "DropDownList";

        public static string AddonDisplayTypeRadioButton = "RadioButtons";

        public static string AddonDisplayTypeCheckBox = "CheckBox";

        public static string NoImageName = "noimage.gif";

        public static string LoginPage = "/account/login";

        public static string PaypalPaymentType = "paypal";

        //Znode Version 7.2.2
        public static int MaxRecentViewItemToDisplay = PortalAgent.CurrentPortal.MaxRecentViewItemToDisplay;
        public static string CountriesSessionKey = "Countries";
        public static string SortAscending = "ASC";
        public static string NoSwatchImageName = "noimage-swatch.gif";
        public static string MultiCartModelSessionKey = "MultiShoppingCartModel";
        public static string CheckoutViewModelSessionKey = "CheckoutViewModel";

        public static string CheckoutShippingBilling = "/checkout/shipping";
        public static string RouteMultipleShipto = "/checkout/Multipleshipto";
        public static string MultiCartShipmentSessionKey = "MultipleOrderShipment";
        public static string OrderIdCookieKey = "orderId";

        public static string FooterCopyrightText = "FooterCopyrightText";
        public const string ErrorView = "ElmahError";
        public static string CompareProducts = "CompareProducts";
        public static int CompareProductLimit = 4;
        public static string BundleProductId = "bundleproductid";
        public static int QuickOrderPadRows = 13;
        public static int DefaultQuantityNumber = 1;
        public static string ProductControllerText = "Product";
        public static string Franchise = "franchise";
        public static string RefreshCartItem = "refreshcartitem";
        public static int QuickOrderQuantityLength = 4;
        public const string ContentPageHtml = "contentPageHtml";
        public const string ContentPageTitle = "ContentPageTitle";

        public const string GlobalLevelCompare = "Global Level Compare";

        public static string PaypalError = "PaypalError";
        public static string PaymentType = "PaymentType";
        public static string CustomError = "CustomError";
        public static string CartError = "CartError";
        public static string AddressValidationError = "AddressValidationError";
        public static string DeleteAddressValidationError = "DeleteAddressValidationError";
        public static string CategoryId = "categoryId";
        public static string Id = "id";
        public static string SEOURL = "slug";
        public static string BreadCrumbUrlFormat = "   &gt;   <a href='/{0}' title='{1}'>{2}</a>";

        public const int CasePriorityId = 3;
        public const int CaseStatusId = 1;
        public const int CaseTypeId = 0;

        public static string FooterWeAcceptCardsText = "FooterWeAcceptCardsText";
        public static string FooterShippingText = "FooterShippingText";
        public static string CategoryName = "CategoryName";
        public static string ResetPasswordSuccessMessage = "ResetPasswordSuccessMessage";
        public static string ShoppingCartFooterText = "ShoppingCartFooterText";
        public static string FooterShareUsText = "FooterShareUsText";
        public static string CategoryFeaturedCategoriesTitle = "CategoryFeaturedCategoriesTitle";
        public static string AddonDisplayTypeTextBox = "TextBox";
        public static string MVCDemoSplash = "MVCDemoSplash";
        public static string StoreSpecials = "StoreSpecials";
        public static string StoreSpecialsTitle = "StoreSpecialsTitle";
        public static string NewArrival = "NewArrival";
        public static string Brands = "Brands";
        public static string PageSizeValue = "PageSizeValue";
        public const string Action = "Action";
        public static string SocialLoginFailedMessage = "SocialLoginFailedMessage";
        public static string SocialLoginDetails = "SocialLoginDetails";
        public static string MVCDemoSplash2 = "MVCDemoSplash2";
        public static string MVCDemoSplash3 = "MVCDemoSplash3";
        public static string FooterShareUsTextProductDetail = "FooterShareUsTextProductDetail";
    }
}