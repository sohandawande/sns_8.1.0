﻿using System;
using System.Configuration;
using System.Globalization;
using System.Web;
using Znode.Engine.MvcDemo.Agents;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Helpers
{
    public static class HelperMethods
    {

        public static string GetImagePath(string image)
        {

            if (!string.IsNullOrEmpty(image))
            {
                return image.Replace("~", ConfigurationManager.AppSettings["ZnodeApiRootUri"]);
            }
            // Provide a static image 
            return string.Empty;
        }

        public static bool IsDebugMode
        {
            get
            {
                return Convert.ToBoolean(ConfigurationManager.AppSettings["IsDebugMode"]);
            }
        }

        public static string GetDomainUrl()
        {
            return (!string.IsNullOrEmpty(HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority))) ? HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) : string.Empty;
        }

        /// <summary>
        /// To get formatted price with current currency symbol and suffix
        /// </summary>
        /// <param name="price">decimal price</param>
        /// <returns>returns string with two decimal value and current currency symbol</returns>
        public static string FormatPriceWithCurrency(decimal? price)
        {
            string currencySuffix = string.IsNullOrEmpty(PortalAgent.CurrentPortal.CurrencySuffix) ? string.Empty : string.Format("({0})", PortalAgent.CurrentPortal.CurrencySuffix);
            CultureInfo info = new CultureInfo(PortalAgent.CurrentPortal.CurrencyName);
            string currencyValue = string.Format("{0}{1}", price.GetValueOrDefault().ToString("c", info.NumberFormat), currencySuffix);            
            return currencyValue;
        }

        /// <summary>
        /// To get formatted price with current currency symbol and suffix
        /// </summary>
        /// <param name="price">double price</param>
        /// <returns>returns string with two decimal value and current currency symbol</returns>
        public static string FormatPriceWithCurrency(double? price)
        {
            string currencySuffix = string.IsNullOrEmpty(PortalAgent.CurrentPortal.CurrencySuffix) ? string.Empty : string.Format("({0})", PortalAgent.CurrentPortal.CurrencySuffix);
            CultureInfo info = new CultureInfo(PortalAgent.CurrentPortal.CurrencyName);
            string currencyValue = string.Format("{0}{1}", price.GetValueOrDefault().ToString("c", info.NumberFormat), currencySuffix);
            return currencyValue;
        }

        /// <summary>
        /// To get formatted price 
        /// </summary>
        /// <param name="price">decimal price</param>
        /// <returns>returns two decimal value</returns>
        public static decimal FormatPrice(decimal? price)
        {
            return Equals(price, null) ? 0 : Math.Round(price.Value, 2);
        }

        #region PRFT Custom Method
        public static string GetCreditApplicationPDFFile()
        {
            return string.Format(System.Configuration.ConfigurationManager.AppSettings["CreditApplicationPDFPath"], System.Configuration.ConfigurationManager.AppSettings["ZnodeApiRootUri"]);
        }

        public static string GetTechnicalDocumentsFilePath()
        {
            return string.Format(System.Configuration.ConfigurationManager.AppSettings["TechnicalDocumentsPath"], System.Configuration.ConfigurationManager.AppSettings["ZnodeApiRootUri"]);
        }

        /// <summary>
        /// Check if Technical documentation present or not.
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static bool CheckFileExists(string filePath)
        {
            bool isFileExists = false;

            string fileOriginalPath = System.Configuration.ConfigurationManager.AppSettings["TechnicalDocPhysicalPath"] + filePath;
            if (System.IO.File.Exists((fileOriginalPath)))
            {
                isFileExists = true;
            }
            else
            {
                isFileExists = false;
            }
            return isFileExists;
        }

        public static string GetAssociatedCustomerExternalId()
        {
            if(HttpContext.Current.Session["AssociatedCustomerExternalId"] != null)
            {
                return Convert.ToString(HttpContext.Current.Session["AssociatedCustomerExternalId"]);
            }
            return string.Empty;
        }

        public static string GetERPCustomerType()
        {
            string customerType = ConfigurationManager.AppSettings["GenericERPCustomerType"];
            if (HttpContext.Current.Session["ERPCustomerType"] != null) //For B2B customer this information is always set as during login erp call is made to update other details with customer types
            {
                customerType = Convert.ToString(HttpContext.Current.Session["ERPCustomerType"]);
            }
            else
            {
                if (HttpContext.Current.Session["AssociatedCustomerExternalId"] != null)
                {
                    string genericUserID = ConfigurationManager.AppSettings["GenericUserExternalID"];
                    string actualCustomerExternalID = Convert.ToString(HttpContext.Current.Session["AssociatedCustomerExternalId"]);
                    if(!genericUserID.Trim().Equals(actualCustomerExternalID.Trim()))
                    {
                        //Get Details from ERP
                        PRFTERPAccountDetailsViewModel erpCustomerModel = new PRFTERPAccountDetailsViewModel();
                        erpCustomerModel = new PRFTERPCustomerAgent().GetAccountDetailsFromERP(new PRFTHelper().ExternalId(actualCustomerExternalID));
                        if (erpCustomerModel != null && erpCustomerModel.HasError == false && !string.IsNullOrEmpty(erpCustomerModel.CompanyCodeDebtorAccountID))
                        {
                            HttpContext.Current.Session["ERPCustomerType"] = erpCustomerModel.CustomerType;
                            customerType = erpCustomerModel.CustomerType;
                        }
                    }
                }
            }
            return customerType;
        }
        #endregion


    }
}