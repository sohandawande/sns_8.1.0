﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Helpers
{
    public class PRFTHelper
    {
        #region Method
        /// <summary>
        /// Check if image exists else return noimage path
        /// </summary>
        /// <param name="imageFilePath"></param>
        /// <param name="imageFileName"></param>
        /// <returns></returns>
        public string CheckImageExists(string imageFilePath, string imageFileName)
        {
            string originalImageFileName = imageFileName;
            string filePath = string.Empty;
            //string imageName = imageFileName;

            if (!string.IsNullOrEmpty(imageFileName) && imageFileName.Contains("http"))
            {
                imageFileName = imageFileName.Split('/').Last();
            }
           

            string fullImagePath = imageFilePath + imageFileName;

            
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(fullImagePath);
            request.Method = "HEAD";

            // bool exists;
            try
            {
                request.GetResponse();
                // exists = true;
                filePath = fullImagePath;
            }
            catch
            {
                //  exists = false;
                fullImagePath = string.Format(System.Configuration.ConfigurationManager.AppSettings["OriginalTurnkeyImagePath"], System.Configuration.ConfigurationManager.AppSettings["ZnodeApiRootUri"]) + imageFileName;
                HttpWebRequest requestTurnkeyImage = (HttpWebRequest)HttpWebRequest.Create(fullImagePath);
                requestTurnkeyImage.Method = "HEAD";
                try
                {
                    requestTurnkeyImage.GetResponse();
                    // exists = true;
                    filePath = fullImagePath;
                }
                catch
                {
                    filePath = imageFilePath + "noimage.jpg";
                }
                //filePath = imageFilePath + "noimage.jpg";
            }
            return filePath;
        }

        public bool IsB2BUser()
        {
            bool isB2BUser = false;
            var accountModel = (AccountViewModel)HttpContext.Current.Session[MvcDemoConstants.AccountKey];            
            
            if (accountModel != null && !string.IsNullOrEmpty(accountModel.Custom2))
            {
                string customerType = Convert.ToString(Convert.ToInt32(CustomValue.One));
                isB2BUser =  accountModel.Custom2.Equals(customerType) ? true : false;
            }
            return isB2BUser;
        }
        /// <summary>
        /// Check Length of ExternalId and Add WhiteSpace if ExternalId length less than 20
        /// </summary>
        /// <param name="externalId"></param>
        /// <returns></returns>
        public string ExternalId(string externalId)
        {
            if(!string.IsNullOrEmpty(externalId))
            {
                int addWhiteSpacelength = Convert.ToInt32(ConfigurationManager.AppSettings["ExternalIdLength"]);
                int length = externalId.Length;
                if (!length.Equals(addWhiteSpacelength))
                {
                    externalId = externalId.PadLeft(addWhiteSpacelength);
                    return externalId;
                }
            }           
            return externalId;
        }

        public bool IsAddressEditable(AddressViewModel address)
        {
            bool isAddressEditable = true;
            if (IsB2BUser() && address.IsDefaultBilling)
            {
                isAddressEditable = false;
            }
            return isAddressEditable;
        }
        #endregion
    }
}