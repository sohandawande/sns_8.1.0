﻿using System.Web;
using System.Web.Routing;
using Znode.Engine.MvcDemo.Agents;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Helpers
{
    public class SEOUrlRouteData : Route
    {

        #region constructor
        /// <summary>
        /// Initializes a new instance, using the specified URL pattern and handler class.
        /// </summary>
        /// <param name="url">The URL pattern for the route.</param>
        /// <param name="routeHandler">The object that processes requests for the route.</param>
        public SEOUrlRouteData(string url, IRouteHandler routeHandler)
            : base(url, routeHandler)
        {
        }
        /// <summary>
        /// Initializes a new instance, using the specified URL pattern, handler class and default parameter values.
        /// </summary>
        /// <param name="url">The URL pattern for the route.</param>
        /// <param name="defaults">The values to use if the URL does not contain all the parameters.</param>
        /// <param name="routeHandler">The object that processes requests for the route.</param>
        public SEOUrlRouteData(string url, RouteValueDictionary defaults, IRouteHandler routeHandler)
            : base(url, defaults, routeHandler)
        {
        }
        #endregion

        #region Methods
        /// <summary>
        /// Returns modified information about the route.
        /// </summary>
        /// <param name="httpContext">An object that encapsulates information about the HTTP request.</param>
        /// <returns>
        /// An object that contains the values from the route definition.
        /// </returns>
        public override RouteData GetRouteData(HttpContextBase httpContext)
        {

            RouteData routeData = base.GetRouteData(httpContext);
            if (Equals(routeData, null) || Equals(routeData.Values.ContainsKey("slug"), false))
            {
                return null;
            }
            string currentSlug = routeData.Values["slug"].ToString();
            //if currentSlug found
            if (!string.IsNullOrEmpty(currentSlug))
            {
                //Get the Seo Url Details.
                ISearchAgent searchAgent = new SearchAgent();
                SEOUrlViewModel model = searchAgent.GetSeoUrlDetail(currentSlug);
                if (!Equals(model, null) && !string.IsNullOrEmpty(model.SeoUrl))
                {
                    //Check in Case the Seo url belongs to Product.
                    if (!Equals(model.ProductId, null) && model.ProductId > 0)
                    {
                        routeData.Values["controller"] = "Product";
                        routeData.Values["action"] = "Details";
                        routeData.Values["id"] = model.ProductId;
                    }
                    else //Seo Url belongs to Category.
                    {
                        routeData.Values["controller"] = "Category";
                        routeData.Values["action"] = "Index";
                        routeData.Values["category"] = model.CategoryName;
                    }
                }
                else
                {
                    routeData = null;
                }
            }
            else
            {
                routeData = null;
            }
            return routeData;
        }
        #endregion

    }
}