﻿using System;
using System.Web.Mvc;
using System.Web.Routing;

namespace Znode.Engine.MvcDemo.Helpers
{
    public static class SEORouteCollection
    {
        public static Route MapSEORoute(this RouteCollection routes, string name, string url, object defaults, object constraints = null)
        {
            if (Equals(routes, null))
            {
                throw new ArgumentNullException("routes");
            }
            if (Equals(url, null))
            {
                throw new ArgumentNullException("url");
            }
            SEOUrlRouteData route = new SEOUrlRouteData(url, new MvcRouteHandler())
            {
                Defaults = new RouteValueDictionary(defaults),
                Constraints = new RouteValueDictionary(constraints)
            };
            routes.Add(name, route);
            return route;
        }
    }
}