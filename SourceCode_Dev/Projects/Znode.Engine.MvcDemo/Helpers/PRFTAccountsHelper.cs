﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcDemo.Helpers
{
    public static class PRFTAccountsHelper
    {
        const string QuerystringDefaultParams = "text,pagenumber,swt,sort,pagesize";
        const string DefaultSortOption = "1";

        public static Collection<KeyValuePair<string, IEnumerable<string>>> GetRefineBy()
        {
            // Written the below 2 lines to handle the test methods (since we don't have page querystrings.
            if (HttpContext.Current.Request == null)
                return new Collection<KeyValuePair<string, IEnumerable<string>>>();

            var refineBy = (from object item in HttpContext.Current.Request.QueryString.Keys
                            select item.ToString()
                                into itemString
                                where !QuerystringDefaultParams.Contains(itemString.ToLower())
                                select new KeyValuePair<string, IEnumerable<string>>(itemString, HttpContext.Current.Request.QueryString.GetValues(itemString).SelectMany(x => x.Split(',')))).ToList();

            if (refineBy.Any())
                return new Collection<KeyValuePair<string, IEnumerable<string>>>(refineBy);

            return new Collection<KeyValuePair<string, IEnumerable<string>>>();
        }

        public static string GetSortBy(string sortBy)
        {
            switch (sortBy)
            {
                case "2":
                    return @"username|desc";
                case "3":
                    return @"name|asc";
                case "4":
                    return @"name|desc";
                default:
                    return @"relevance|asc";
            }
        }
    }
}