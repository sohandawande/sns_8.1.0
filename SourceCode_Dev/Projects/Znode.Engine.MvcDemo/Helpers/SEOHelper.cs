﻿using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace Znode.Engine.MvcDemo.Helpers
{
    public static class SEOHelper
    {
        public static string GetCategoryUrl(this UrlHelper url, string page, string category, int? categoryId)
        {

            bool _result = Regex.IsMatch(category, @"^[a-zA-Z0-9@.]*$@");
            if (!_result)
            {
                category = System.Text.RegularExpressions.Regex.Replace(category, "[^a-zA-Z0-9]", string.Empty);
            }
            if (!Equals(categoryId, null))
            {
                return string.IsNullOrEmpty(page)
                    ? url.RouteUrl("category-details", new { category = category, categoryId = categoryId })
                    : url.RouteUrl("SeoSlug", new { slug = page.ToLower() });
            }
            else
            {
                return string.IsNullOrEmpty(page)
                  ? url.RouteUrl("category-details", new { category = category })
                  : url.RouteUrl("SeoSlug", new { slug = page.ToLower() });
            }
        }

        public static string GetProductUrl(this UrlHelper url, string page, string id)
        {
            return string.IsNullOrEmpty(page)
                ? url.RouteUrl("product-details", new { id })
                : url.RouteUrl("SeoSlug", new { slug = page.ToLower() });
        }

    }
}