﻿using System.Collections.Generic;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class SubcategoriesViewModel : BaseViewModel
    {
        public SubcategoriesViewModel()
        {
            Subcategories = new List<CategoryViewModel>();
        }

        public string ParentCategoryName { get; set; }
        public List<CategoryViewModel> Subcategories { get; set; }
    }
}