﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class StoreLocatorViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for StoreLocatorViewModel
        /// </summary>
        public StoreLocatorViewModel()
        {
            StoreList = new Collection<StoreLocatorViewModel>();
        }

        [RegularExpression(@"\d+", ErrorMessageResourceType = typeof(Resources.ZnodeResource), ErrorMessageResourceName = "ErrorMessageOfZipCode")]
        public string ZipCode { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string AreaCode { get; set; }
        public int Radius { get; set; }
        public string Name { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Phone { get; set; }
        public string Custom1 { get; set; }
        public string Custom2 { get; set; }
        public string Custom3 { get; set; }
        public string Fax { get; set; }
        public string MapQuestURL { get; set; }
        public string ErrorMessage { get; set; }
        public Collection<StoreLocatorViewModel> StoreList { get; set; }
        public List<SelectListItem> RadiusList { get; set; }
    }
}