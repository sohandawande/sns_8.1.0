﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class KeywordSearchViewModel : BaseViewModel
    {
        // Response
        public Collection<SearchCategoryViewModel> Categories { get; set; }
        public Collection<FacetViewModel> Facets { get; set; }
        public Collection<ProductViewModel> Products { get; set; }
        public Collection<FacetViewModel> SelectedFacets { get; set; }
        public SubcategoriesViewModel Subcategories { get; set; }
        public int OldPageNumber { get; set; }
        public int TotalPages { get; set; }
        public int TotalProducts { get; set; }
        public string NextPageUrl { get; set; }
        public string PreviousPageurl { get; set; }
        public string Category { get; set; }
        public string SearchTerm { get; set; }
        public string SeoDescription { get; set; }
        public string SeoKeywords { get; set; }
        public string SeoTitle { get; set; }
        public string CategoryTitle { get; set; }
        //Znode version 7.2.2 To get Category Banner
        public string CategoryBanner { get; set; }
        public int SelectedCategoryId { get; set; }

        //PRFT Custom Code : Start
        public string ShortDescription { get; set; }
        public Collection<CategoryViewModel> SubCategories { get; set; }
        public IEnumerable<SelectListItem> PageSizes { get; set; }

        public int? PageNumber { get; set; }
        public int? PageSize { get; set; }
        public int FirstPageNumber { get; set; }
        public string FirstPageUrl { get; set; }
        public int LastPageNumber { get; set; }
        public string LastPageUrl { get; set; }
        //PRFT Custom : End

        public KeywordSearchViewModel()
        {
            Facets = new Collection<FacetViewModel>();
            Products = new Collection<ProductViewModel>();
            SelectedFacets = new Collection<FacetViewModel>();
            //PRFT Custom Code: Start
            SubCategories = new Collection<CategoryViewModel>();
            //PRFT Custom Code: End
        }
    }
}