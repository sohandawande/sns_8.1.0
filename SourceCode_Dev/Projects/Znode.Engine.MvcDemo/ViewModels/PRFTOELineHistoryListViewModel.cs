﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class PRFTOELineHistoryListViewModel : BaseViewModel
    {
        public Collection<PRFTOELineHistoryViewModel> OELineHistory { get; set; }

        public PRFTOELineHistoryListViewModel()
        {
            OELineHistory = new Collection<PRFTOELineHistoryViewModel>();
        }

    }
}