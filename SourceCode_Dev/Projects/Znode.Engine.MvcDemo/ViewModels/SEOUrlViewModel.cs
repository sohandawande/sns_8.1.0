﻿
namespace Znode.Engine.MvcDemo.ViewModels
{
    public class SEOUrlViewModel : BaseViewModel
    {
        public string SeoUrl { get; set; }
        public int? ProductId { get; set; }
        public string CategoryName { get; set; }
        public string ContentPageName { get; set; }
    }
}