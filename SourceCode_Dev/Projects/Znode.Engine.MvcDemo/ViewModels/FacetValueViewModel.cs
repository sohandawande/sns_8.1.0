﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class FacetValueViewModel : BaseViewModel
    {
        public string AttributeValue { get; set; }
        public long FacetCount { set; get; }
        public string RefineByUrl { get; set; }
    }
}