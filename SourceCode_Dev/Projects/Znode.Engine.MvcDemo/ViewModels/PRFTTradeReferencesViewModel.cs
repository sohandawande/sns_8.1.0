﻿
using System.ComponentModel.DataAnnotations;
namespace Znode.Engine.MvcDemo.ViewModels
{
    public class PRFTTradeReferencesViewModel:BaseViewModel
    {
        public int TradeRefID { get; set; }
        public int CreditApplicationID { get; set; }
        public string ReferenceName { get; set; }
        public string BusinessName { get; set; }
        public string Street { get; set; }
        public string Street1 { get; set; }
        public string City { get; set; }
        public string StateCode { get; set; }
        public string CountryCode { get; set; }
        public string PostalCode { get; set; }
        public string PhoneNumber { get; set; }
        public string FaxNumber { get; set; }
        public string Custom1 { get; set; }
        public string Custom2 { get; set; }
        public string Custom3 { get; set; }
        public string Custom4 { get; set; }
        public string Custom5 { get; set; }
    }
}