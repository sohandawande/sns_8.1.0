﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class CouponViewModel : BaseViewModel
    {
        public string Coupon { get; set; }
        public string CouponMessage { get; set; }
        public bool CouponApplied { get; set; }
        public bool CouponValid { get; set; }
    }
}