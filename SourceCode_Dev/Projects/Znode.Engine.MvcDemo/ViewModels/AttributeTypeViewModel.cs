﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using Znode.Engine.MvcDemo.Helpers;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class AttributeTypeViewModel : BaseViewModel
    {
        public int AttributeTypeId { get; set; }
        public string Name { get; set; }
        public Collection<AttributeViewModel> ProductAttributes { get; set; }
        public Collection<AttributeSelectListItem> DropdownAttributes {
            get
            {
                if (ProductAttributes.Any())
                {
                    return new Collection<AttributeSelectListItem>(ProductAttributes.Select(
                        x =>
                        new AttributeSelectListItem()
                            {
                                Text = x.Name,
                                Value = x.AttributeId.ToString(),
                                Highlighted = x.Available,
                                Selected = x.Selected,
                            }).ToList());
                }
   
                return new Collection<AttributeSelectListItem>();
            }
        }

        public bool IsSelectedOption { get; set; }
        public int SelectedAttributeId { get; set; }
        public bool ToBeFiltered { get; set; }

        public AttributeTypeViewModel()
        {
            ProductAttributes = new Collection<AttributeViewModel>();
        }
    }
}