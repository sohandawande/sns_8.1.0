﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class CompareProductViewModel : BaseViewModel
    {
        public string SenderEmailAddress { get; set; }
        public string RecieverEmailAddress { get; set; }        
        public int CategoryId { get; set; }
        public int ProductId { get; set; }
        public int PortalId { get; set; }
        public string ProductIds { get; set; }
        public string BaseUrl { get; set; }
        public string ProductName { get; set; }
        public bool IsProductDetails { get; set; }
        public Collection<ProductViewModel> ProductList { get; set; }

        public CompareProductViewModel()
        {
            ProductList = new Collection<ProductViewModel>();
        }
    }
}