﻿
namespace Znode.Engine.MvcDemo.ViewModels
{
    public class CartItemModel
    {
        public int ProductQuantity { get; set; }

        public decimal ProductAmount { get; set; }

        public string ProductDescription { get; set; }
        public string ProductName { get; set; }
        public string ProductNumber { get; set; }
        
    }
}