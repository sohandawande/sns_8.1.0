﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class WishListViewModel : BaseViewModel
    {
        public Collection<WishListItemViewModel> Items { get; set; }
    }
}