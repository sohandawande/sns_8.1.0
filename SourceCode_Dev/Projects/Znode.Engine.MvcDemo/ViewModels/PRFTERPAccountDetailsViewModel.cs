﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class PRFTERPAccountDetailsViewModel : BaseViewModel
    {
        public string DebtorCode { get; set; }
        public string CustomerType { get; set; }
        public string CustomerTypeDescription { get; set; }
        public string CompanyCodeDebtorAccountID { get; set; }
        public string CustomerStatus { get; set; }
        public string CustomerStatusDescription { get; set; }
        public string PaymentCondition { get; set; }
        public string PaymentConditionDescription { get; set; }
        public string Terms { get; set; }
        public DateTime ModifiedDate { get; set; }
        public AddressViewModel BillingAddress { get; set; }

        public Collection<AddressViewModel> ShippingAddresses { get; set; }

        public PRFTERPAccountDetailsViewModel()
        {
            ModifiedDate = DateTime.Now;
            BillingAddress = new AddressViewModel();
            ShippingAddresses = new Collection<AddressViewModel>();
        }
    }
}