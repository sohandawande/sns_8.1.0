﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class PRFTOELineHistoryViewModel : BaseViewModel
    {
        public string ItemNo { get; set; }
        public string ItemDescription1 { get; set; }
        public string ItemDescription2 { get; set; }
        public int QuantityOrdered { get; set; }
        public int QuantityToShip { get; set; }
        public int QuantityReturnedtoStock { get; set; }
        public int QuantityBackordered { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal DiscountPercent { get; set; }
        public string UnitOfMeasure { get; set; }
        public decimal SalesAmount { get; set; }
        
    }
}