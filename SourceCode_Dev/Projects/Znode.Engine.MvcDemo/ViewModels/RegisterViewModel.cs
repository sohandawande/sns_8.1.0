﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using Resources;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class RegisterViewModel : BaseViewModel
    {
        public RegisterViewModel()
        {
            
        }

        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResource), ErrorMessageResourceName = "RequiredEmailID")]
        [EmailAddress(ErrorMessageResourceType = typeof(Resources.ZnodeResource), ErrorMessageResourceName = "ValidEmailAddress", ErrorMessage = "")]
        public string EmailAddress { get; set; }

        public bool EmailOptIn { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResource), ErrorMessageResourceName = "RequiredPassword")]
        [RegularExpression("^(?=.*[0-9])(?=.*[a-zA-Z]).{8,}$", ErrorMessageResourceType = typeof(Resources.ZnodeResource), ErrorMessageResourceName = "ValidPassword")]	
        public string Password { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResource), ErrorMessageResourceName = "RequiredConfirmPassword")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessageResourceType = typeof(Resources.ZnodeResource), ErrorMessageResourceName = "ErrorPasswordMatch")]
        public string ReTypePassword { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResource), ErrorMessageResourceName = "RequiredUserName")]
        public string UserName { get; set; }

        public string ExternalLoginData { get; set; }

        //PRFT Custom Code: Start
        public int? ParentAccountId { get; set; }
        //PRFT Custom Code: End
    }    
}