﻿using System.Collections.ObjectModel;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class CategoryViewModel : BaseViewModel
    {
        public int CategoryId { get; set; }
        public string Title { get; set; }
        public string Name { get; set; }
        public string SeoKeywords { get; set; }
        public string SeoTitle { get; set; }
        public string SeoUrl { get; set; }
        public string SeoDescription { get; set; }
        public string ShortDescription { get; set; }
        public int ProductCount { get; set; }
        public int ActiveProductCount { get; set; }
        public Collection<CategoryViewModel> Subcategories { get; set; }
        //Znode version 7.2.2 To get or set the value of Category Banner
        public string CategoryBanner { get; set; }
        public string CategoryTitle { get; set; }

        public string ImageAltTag { get; set; }
        public string ImageFile { get; set; }
        public string ImageLargePath { get; set; }
        public string ImageMediumPath { get; set; }
        public string ImageSmallPath { get; set; }
        public string ImageSmallThumbnailPath { get; set; }
        public string ImageThumbnailPath { get; set; }
        public string Custom1 { get; set; } 
    }
}