﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class GiftCardViewModel : BaseViewModel
    {
        public int? AccountId { get; set; }
	    public decimal? Amount { get; set; }
       
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResource), ErrorMessageResourceName = "RequiredGiftCard")]
		public string CardNumber { get; set; }
		
        public int CreatedBy { get; set; }
		public DateTime CreateDate { get; set; }
		public DateTime? ExpirationDate { get; set; }
		public int GiftCardId { get; set; }
		public string Name { get; set; }
		public OrderLineItemViewModel OrderLineItem { get; set; }
		public int? OrderLineItemId { get; set; }
		public int PortalId { get; set; }
    }
}