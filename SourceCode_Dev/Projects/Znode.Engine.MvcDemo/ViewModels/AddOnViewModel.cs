﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class AddOnViewModel : BaseViewModel
    {
        public string DisplayType { get; set; }
        public string Title { get; set; }
        public int AddOnId { get; set; }
        public bool OptionalIndicator { get; set; }
        public string Description { get; set; }
        public IEnumerable<AddOnValuesViewModel> AddOnValues { get; set; }
        public int[] SelectedAddOnValue { get; set; }
        public bool IsRequired { get; set; }
        public bool AllowBackOrder { get; set; }
        public string BackOrderMessage { get; set; }
        public string InStockMessage { get; set; }
        public string OutOfStockMessage { get; set; }
        public bool IsOutOfStock { get; set; }
        public string PromptMessage { get; set; }
        public bool TrackInventory { get; set; }
        public string Name { get; set; }
    }
}