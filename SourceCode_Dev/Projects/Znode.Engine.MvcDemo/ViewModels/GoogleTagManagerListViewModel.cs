﻿using System.Collections.Generic;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class GoogleTagManagerListViewModel
    {
        public GoogleTagManagerListViewModel()
        {
            GoogleTagManagers = new List<GoogleTagManagerViewModel>();
        }
        public List<GoogleTagManagerViewModel> GoogleTagManagers { get; set; }
        public GoogleTagManagerViewModel GoogleTagManager { get; set; }
    }
}
