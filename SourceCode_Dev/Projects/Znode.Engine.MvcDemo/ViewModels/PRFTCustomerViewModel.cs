﻿using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Libraries.Helpers;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class PRFTCustomerViewModel : BaseViewModel
    {
        public PRFTCustomerViewModel()
        {
            CustomerAccount = new AccountViewModel();
            //GridModel = new ReportModel();
            //AddressList = new AddressListViewModel();
            Address = new AddressViewModel();
            //GetAffilateApprovalStatusList = new List<SelectListItem>();
        }
        [LocalizedDisplayName(Name = RZnodeResources.DisplayNameBilling)]
        public string FullName { get; set; }

        //[LocalizedDisplayName(Name = RZnodeResources.LabelCompanyName)]
        public string CompanyName { get; set; }
        //[LocalizedDisplayName(Name = RZnodeResources.LabelPhoneBilling)]
        public string PhoneNumber { get; set; }

        public int AccountId { get; set; }
        public AccountViewModel CustomerAccount { get; set; }
        
        public AddressViewModel Address { get; set; }
        public bool? EnableCustomerPricing { get; set; }
        public string LoginName { get; set; }
        public bool IsConfirmed { get; set; }        
        public int? PortalId { get; set; }

        public bool IsUserAdmin { get; set; }        

        //PRFT Custom Code : Start     
        public bool IsSalesRepFound { get; set; }
        public int SalesRepId { get; set; }

        public PRFTCustomerListViewModel customerList { get; set; }
        public string Custom1 { get; set; }
        public string Custom2 { get; set; }
        //public string Custom3 { get; set; }
        public int ParentAccountId { get; set; }
        public bool IsCustParentAccountFound { get; set; }
        public int? CustParentAccountId { get; set; }
        public int CustomerUserMappingId { get; set; }

        public string ExternalId { get; set; }
        //PRFT Custom Code : End
    }
}