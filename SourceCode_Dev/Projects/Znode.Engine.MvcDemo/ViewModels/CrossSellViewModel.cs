﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class CrossSellViewModel : BaseViewModel
    {
        public int? DisplayOrder { get; set; }
        public int ProductCrossSellTypeId { get; set; }
        public int ProductId { get; set; }
        public int RelatedProductId { get; set; }
        public int RelationTypeId { get; set; }
        public int ProductRelation { get; set; }
    }
}