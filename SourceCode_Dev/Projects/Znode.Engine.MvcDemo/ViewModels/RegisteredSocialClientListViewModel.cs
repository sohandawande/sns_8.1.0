﻿using System.Collections.Generic;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class RegisteredSocialClientListViewModel : BaseViewModel
    {
        public RegisteredSocialClientListViewModel()
        {
            SocialClients = new List<RegisteredSocialClientViewModel>();
        }

        public List<RegisteredSocialClientViewModel> SocialClients { get; set; }
    }
}