﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class CartItemViewModel : BaseViewModel
    {
        public string BundleItemsIds { get; set; }
        public string Description { get; set; }
		public decimal ExtendedPrice { get; set; }
		public string ExternalId { get; set; }
		public int ProductId { get; set; }
		public int Quantity { get; set; }
		public decimal ShippingCost { get; set; }
        public decimal ProductDiscountAmount { get; set; }
		public int ShippingOptionId { get; set; }
		public string Sku { get; set; }
		public int SkuId { get; set; }
		public int[] AttributeIds { get; set; }
        public string Attributes { get; set; }
		public decimal UnitPrice { get; set; }
		public bool InsufficientQuantity { get; set; }
        public string AddOnValueIds { get; set; }
        public string ProductName { get; set; }
        public bool RecurringBillingInd { get; set; } 
        public decimal? RecurringBillingInitialAmount { get; set; }
        public string RecurringBillingFrequency { get; set; }
        public string SeoPageName { get; set; }
        public ProductViewModel Product { get; set; }
        //ZNode Version 7.2.2 - Add ImagePath
        public string ImagePath { get; set; }

        public Collection<SelectedBundleViewModel> SelectedBundles { get; set; }
        public List<AddressViewModel> Addresses { get; set; }
        public int SelectedAddressId { get; set; }
        public string AddOnValuesCustomText { get; set; }
        public CartItemViewModel()
		{
			ExternalId = Guid.NewGuid().ToString();
            Addresses = new List<AddressViewModel>();
		}

        #region PRFT Custom  
        public decimal ERPUnitPrice { get; set; }
        public decimal ERPExtendedPrice { get; set; }
        public string BackOrderMessage { get; set; }
        public bool AllowBackOrder { get; set; }        
        
        #endregion
    }
}