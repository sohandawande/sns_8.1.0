﻿
namespace Znode.Engine.MvcDemo.ViewModels
{
    public class PortalViewModel : BaseViewModel
    {
        public int PortalId { get; set; }
        public string Name { get; set; }
        public bool PersistentCartEnabled { get; set; }
        public int CatalogId { get; set; }
        public int DefaultAnonymousProfileId { get; set; }
        public int MaxRecentViewItemToDisplay { get; set; }
        public string Css { get; set; }
        public string Theme { get; set; }
        public bool IsEnableCompare { get; set; }
        public string CompareType { get; set; }
        public bool IsMultipleCouponAllowed { get; set; }
        public string CustomerServiceEmail { get; set; }
        public string CustomerServicePhoneNumber { get; set; }
        public bool? IsEnableSinglePageCheckout { get; set; }
        public bool? EnableAddressValidation { get; set; }
        public bool? RequireValidatedAddress { get; set; }
        public int? CurrencyTypeId { get; set; }
        public int MaxCatalogCategoryDisplayThumbnails { get; set; }
        public byte MaxCatalogDisplayColumns { get; set; }
        public int MaxCatalogDisplayItems { get; set; }
        public string LogoPath { get; set; }
        public string CurrencySuffix { get; set; }
        public string ImageNotAvailablePath { get; set; }
        public string CurrencyName { get; set; }

        public string SiteWideTopJavascript { get; set; } //PRFT Custom Code
        public bool UseSsl { get; set; }
        
    }
}