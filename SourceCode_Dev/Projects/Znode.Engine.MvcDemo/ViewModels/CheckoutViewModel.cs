﻿
namespace Znode.Engine.MvcDemo.ViewModels
{
    public class CheckoutViewModel : BaseViewModel
    {
        public CartViewModel CartViewModel { get; set; }
        public AccountViewModel AccountViewModel { get; set; }
        public ShippingBillingAddressViewModel ShippingBillingAddressViewModel { get; set; }
        public PaymentsViewModel PaymentsViewModel { get; set; }
        public ShippingOptionListViewModel ShippingOptionListViewModel { get; set; }
        public int ShippingOptions { get; set; }
        public string AdditionalInstruction { get; set; }
        public CheckoutViewModel()
        {
            CartViewModel = new CartViewModel();
            ShippingBillingAddressViewModel = new ShippingBillingAddressViewModel();
            PaymentsViewModel = new PaymentsViewModel();
            AccountViewModel = new AccountViewModel();
            ShippingOptionListViewModel = new ShippingOptionListViewModel();
        }
    }
}