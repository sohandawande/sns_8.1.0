﻿using System;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class GiftCardHistoryViewModel : BaseViewModel
    {
        public int GiftCardHistoryId { get; set; }
        public string GiftCardNumber { get; set; }
        public int GiftCardId { get; set; }
        public int OrderId { get; set; }
        public DateTime TransactionDate { get; set; }
        public decimal TransactionAmount { get; set; }
    }
}