﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class PRFTOEHeaderHistoryListViewModel : BaseViewModel
    {
        public Collection<PRFTOEHeaderHistoryViewModel> OEHeaderHistory { get; set; }

        public PRFTOEHeaderHistoryListViewModel()
        {
            OEHeaderHistory = new Collection<PRFTOEHeaderHistoryViewModel>();
        }

    }
}