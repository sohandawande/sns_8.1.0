﻿
namespace Znode.Engine.MvcDemo.ViewModels
{
    public class SocialLoginViewModel : BaseViewModel
    {
        public string Provider { get; set; }
        public string ProviderUserId { get; set; }
        public bool CreatePersistentCookie { get; set; }
    }
}