﻿
namespace Znode.Engine.MvcDemo.ViewModels
{
    public class SkuProductViewModel : BaseViewModel
    {
        public int ProductId { get; set; }
        public int SkuId { get; set; }
        public int Quantity { get; set; }
        public int PortalId { get; set; }
        public int CartQuantity { get; set; }
        public int QuantityOnHand { get; set; }
        public string BundleQuantityOnHand { get; set; }
        public string AddOnQuantityOnHand { get; set; }
        public int MinQty { get; set; }
        public int MaxQty { get; set; }

        public string AddonValueIds { get; set; }
        public string Sku { get; set; }
        public string InStockMessage { get; set; }
        public string OutOfStockMessage { get; set; }
        public string BundleItemIds { get; set; }
        public string ProductNumber { get; set; }
        public string ImageFile { get; set; }
        public string ProductName { get; set; }
        public string ShortDescription { get; set; }
        public string ImageSmallPath { get; set; }
        public string ImageSmallThumbnailPath { get; set; }
        public string InventoryMessage { get; set; }
        public string AutoCompleteLabel { get; set; }
        public string BundleAllowBackOrder { get; set; }
        public string BundleTrackInventoryInd { get; set; }
        public string SeoPageName { get; set; }
        public decimal Price { get; set; }

        public bool CallForPricing { get; set; }
        public bool AllowBackOrder { get; set; }
        public bool TrackInventoryInd { get; set; }
    }
}