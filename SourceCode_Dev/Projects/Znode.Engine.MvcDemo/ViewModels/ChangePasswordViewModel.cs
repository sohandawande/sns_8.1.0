﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class ChangePasswordViewModel : BaseViewModel
    {
        public ChangePasswordViewModel()
        {
            
        }

        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResource), ErrorMessageResourceName = "RequiredPassword")]
        public string OldPassword { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResource), ErrorMessageResourceName = "RequiredNewPassword")]
        [RegularExpression("^(?=.*[0-9])(?=.*[a-zA-Z]).{8,}$", ErrorMessageResourceType = typeof(Resources.ZnodeResource), ErrorMessageResourceName = "ValidPassword")]	
        public string NewPassword { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResource), ErrorMessageResourceName = "RequiredNewConfirmPassword")]
        [Compare("NewPassword", ErrorMessageResourceType = typeof(Resources.ZnodeResource), ErrorMessageResourceName = "ErrorPasswordMatch")]
        public string ReTypeNewPassword { get; set; }

        public string UserName { get; set; }
        public bool IsResetPassword { get; set; }
        public string PasswordToken { get; set; }
    }
}