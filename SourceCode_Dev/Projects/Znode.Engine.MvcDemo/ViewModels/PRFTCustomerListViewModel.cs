﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class PRFTCustomerListViewModel : BaseViewModel
    {
        public List<PRFTCustomerViewModel> Customers { get; set; }
        //public ReportModel GridModel { get; set; }
        public bool IsEnableCustomerPricing { get; set; }

        public PRFTCustomerListViewModel()
        {
            Customers = new List<PRFTCustomerViewModel>();
            //GridModel = new ReportModel();
        }

        public int AccountId { get; set; } //PRFT Custom Code

        public static implicit operator PRFTCustomerListViewModel(bool v)
        {
            throw new NotImplementedException();
        }
    }
}