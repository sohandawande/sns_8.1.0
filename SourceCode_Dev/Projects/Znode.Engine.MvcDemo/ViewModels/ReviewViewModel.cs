﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class ReviewViewModel : BaseViewModel
    {
        public Collection<ReviewItemViewModel> Items { get; set; }
    }
}