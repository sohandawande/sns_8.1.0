﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class ReviewOrderViewModel : BaseViewModel
    {

        public AddressViewModel ShippingAddress { get; set; }

        public CartViewModel ShoppingCart { get; set; }

        public ShippingOptionViewModel ShippingOption { get; set; }

        public ShippingOptionListViewModel ShippingOptionList { set; get; }

        public int OrderViewModelIndex { get; set; }
         
    }
}