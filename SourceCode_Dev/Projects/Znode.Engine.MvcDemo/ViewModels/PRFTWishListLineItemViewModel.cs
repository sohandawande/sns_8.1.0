﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class PRFTWishListLineItemViewModel : BaseViewModel
    {
        public int PRFTWishListLineItemID { get; set; }
        public int PRFTWishListID { get; set; }
        public int SkuID { get; set; }
        public string SkuName { get; set; }
        public int ProductID { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public string Custom1 { get; set; }
        public string Custom2 { get; set; }
        public string Custom3 { get; set; }
        public ProductViewModel Product { get; set; }
        public PRFTSkuViewModel Sku { get; set; }
    }
}