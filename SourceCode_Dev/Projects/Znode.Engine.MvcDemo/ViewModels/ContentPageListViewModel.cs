﻿using System.Collections.Generic;

namespace Znode.Engine.MvcDemo.ViewModels
{
    /// <summary>
    /// List View Model for ContentPage
    /// </summary>
    public class ContentPageListViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for ContentPageListViewModel
        /// </summary>
        public ContentPageListViewModel()
        {
            ContentPages = new List<ContentPageViewModel>();
        }

        public List<ContentPageViewModel> ContentPages { get; set; }
        public string PageTitle { get; set; }
        public string SEOTitle { get; set; }
        public string Name { get; set; }

        public int? PortalId { get; set; }
    }
}
