﻿
namespace Znode.Engine.MvcDemo.ViewModels
{
    public class RegisteredSocialClientViewModel : BaseViewModel
    {
        public string ProviderName { get; set; }
        public string ProviderClientId { get; set; }
        public string ProviderClientSecret { get; set; }
    }
}