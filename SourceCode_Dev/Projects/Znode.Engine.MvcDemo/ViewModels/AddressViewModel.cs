﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using Znode.Engine.Api.Models;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class AddressViewModel : BaseViewModel
    {
        public AddressViewModel()
        {
            AddressExtn = new PRFTAddressExtnViewModel();
        }
       
        public int AccountId { get; set; }
        public int AddressId { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResource), ErrorMessageResourceName = "RequiredCity")]
        public string City { get; set; }

        public string CompanyName { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResource), ErrorMessageResourceName = "RequiredCountryCode")]
        public string CountryCode { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResource), ErrorMessageResourceName = "RequiredFirstName")]
        public string FirstName { get; set; }

        public bool IsDefaultBilling { get; set; }

        public bool IsDefaultShipping { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResource), ErrorMessageResourceName = "RequiredLastName")]
        public string LastName { get; set; }

        public string MiddleName { get; set; }

        //[Required(ErrorMessageResourceType = typeof(Resources.ZnodeResource), ErrorMessageResourceName = "RequiredDisplayName")]
        public string Name { get; set; }

        //[Required(ErrorMessageResourceType = typeof(Resources.ZnodeResource), ErrorMessageResourceName = "RequiredPhoneNumber")]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResource), ErrorMessageResourceName = "RequiredZipCode")]
        public string PostalCode { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResource), ErrorMessageResourceName = "RequiredState")]
        public string StateCode { get; set; }       

        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResource), ErrorMessageResourceName = "RequiredStreetAddress")]
        public string StreetAddress1 { get; set; }

        public string StreetAddress2 { get; set; }

        public string SourceLink { get; set; }

        public bool UseSameAsShippingAddress { get; set; }

        public string Email { get; set; }

        public Collection<StateModel> States { set; get; }

        //Znode Version 7.2.2
        public Collection<CountryModel> Countries { set; get; }

        //PRFT Custom Properties:Start
        public PRFTAddressExtnViewModel AddressExtn { get; set; }
        //PRFT Custom Properties:End
    }
}