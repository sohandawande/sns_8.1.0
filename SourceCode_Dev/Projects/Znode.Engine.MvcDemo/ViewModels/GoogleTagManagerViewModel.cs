﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class GoogleTagManagerViewModel
    {
        public int GoogleTagManagerPluginId { get; set; }
        public int? PortalId { get; set; }

        public string StoreName { get; set; }
        public string ContainerId { get; set; }
        public string Script { get; set; }
        public string MergedScript { get; set; }

        public bool IsActive { get; set; }
    }
}
