﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Znode.Engine.MvcDemo.Helpers;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class PRFTRegisterSubUserViewModel : BaseViewModel
    {
        public PRFTRegisterSubUserViewModel()
        {

        }
        

        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResource), ErrorMessageResourceName = "RequiredEmailID")]
        [EmailAddress(ErrorMessageResourceType = typeof(Resources.ZnodeResource), ErrorMessageResourceName = "ValidEmailAddress", ErrorMessage = "")]
        public string EmailAddress { get; set; }

        public bool EmailOptIn { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResource), ErrorMessageResourceName = "RequiredPassword")]
        [RegularExpression("^(?=.*[0-9])(?=.*[a-zA-Z]).{8,}$", ErrorMessageResourceType = typeof(Resources.ZnodeResource), ErrorMessageResourceName = "ValidPassword")]
        public string Password { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResource), ErrorMessageResourceName = "RequiredConfirmPassword")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessageResourceType = typeof(Resources.ZnodeResource), ErrorMessageResourceName = "ErrorPasswordMatch")]
        public string ReTypePassword { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResource), ErrorMessageResourceName = "RequiredUserName")]
        public string UserName { get; set; }

        //public bool IsActive { get; set; }

        //public string FirstName { get; set; }

        //public string LastName { get; set; }

        
        public int? ParentAccountId { get; set; }
        public int AccountId { get; set; }
        public Collection<AddressViewModel> Addresses { get; set; }
        public AccountViewModel SuperUserAccount
        {
            get
            {
                return (AccountViewModel)HttpContext.Current.Session[MvcDemoConstants.AccountKey];
            }
        }

        public AddressViewModel BillingAddress{ get; set; }

    }
}