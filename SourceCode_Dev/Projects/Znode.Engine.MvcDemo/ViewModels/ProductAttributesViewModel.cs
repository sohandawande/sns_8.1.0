﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class ProductAttributesViewModel : BaseViewModel
    {
        public int ProductId { get; set; }
        public Collection<AttributeTypeViewModel> Attributes { get; set; }

        public ProductAttributesViewModel()
        {
            Attributes = new Collection<AttributeTypeViewModel>();
        }
    }
}