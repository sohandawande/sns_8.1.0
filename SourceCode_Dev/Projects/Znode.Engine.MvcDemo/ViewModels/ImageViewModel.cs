﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class ImageViewModel : BaseViewModel
    {
        public string AlternateThumbnailImageFile { get; set; }
        public int? DisplayOrder { get; set; }
        public string ImageAltTag { get; set; }
        public string ImageFile { get; set; }
        public string ImageLargePath { get; set; }
        public string ImageMediumPath { get; set; }
        public string ImageSmallPath { get; set; }
        public string ImageSmallThumbnailPath { get; set; }
        public string ImageThumbnailPath { get; set; }
        public bool IsActive { get; set; }
        public string Name { get; set; }
        public int ProductId { get; set; }
        public int ProductImageId { get; set; }
        public int? ProductImageTypeId { get; set; }
        public int? ReviewStateId { get; set; }
        public bool ShowOnCategoryPage { get; set; }
    }
}