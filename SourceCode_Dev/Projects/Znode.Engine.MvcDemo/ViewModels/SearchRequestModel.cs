﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class SearchRequestModel : BaseViewModel
    {
        // Request
        public string Category { get; set; }
        public int? CategoryId { get; set; }
        public string InnerSearchKeywords { get; set; }
        public string SearchTerm { get; set; }
        public int? PageNumber { get; set; }
        public int? PageSize { get; set; }
        public Collection<KeyValuePair<string, IEnumerable<string>>> RefineBy { get; set; }
        public string Sort { get; set; }
        //PRFT Custom Code:START
        public IEnumerable<SelectListItem> PageSizes { get; set; }
        public int TotalPages { get; set; }
        //PRFT Custom Code:END
    }
}