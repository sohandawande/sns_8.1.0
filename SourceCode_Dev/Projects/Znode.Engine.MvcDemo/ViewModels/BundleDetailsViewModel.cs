﻿using System.Collections.Generic;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class BundleDetailsViewModel : BaseViewModel
    {

        public string BundleItemsIds { get; set; }
        public int SelectedProductId { get; set; }
        public int SelectedQuantity { get; set; }
        public string SelectedSku { get; set; }
        public string SelectedAddOnValueIds { get; set; }
        public string AddOnValuesCustomText { get; set; }
        public int[] SelectedAttributesIds { get; set; }
        public List<ProductViewModel> Bundles { get; set; }

        public string SelectedBundleAttributeId { get; set; }
        public string SelectedBundleProductId { get; set; }

        public string ProductName { get; set; }
        public string SeoPageName { get; set; }
        public BundleDetailsViewModel()
        {
            Bundles = new List<ProductViewModel>();        
        }
    }
}