﻿using System.Collections.Generic;

namespace Znode.Engine.MvcDemo.ViewModels
{
    /// <summary>
    /// This is the model for CategorySubHeaderViewModel to show the Sub Menus
    /// </summary>
    public class CategorySubHeaderViewModel : BaseViewModel
    {
        public CategorySubHeaderViewModel()
        {
            ChildCategoryItems = new List<CategorySubHeaderViewModel>();
        }

        public string CategoryName { set; get; }
        public int CategoryId { set; get; }
        public string SEOPageName { set; get; }
        public List<CategorySubHeaderViewModel> ChildCategoryItems { get; set; }
    }
}