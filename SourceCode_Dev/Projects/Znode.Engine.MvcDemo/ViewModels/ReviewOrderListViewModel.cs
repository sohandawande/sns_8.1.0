﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class ReviewOrderListViewModel : BaseViewModel
    {
        public ReviewOrderListViewModel()
        {
            ReviewOrderList = new List<ReviewOrderViewModel>();
            Coupons = new List<CouponViewModel>();
        }

        public List<ReviewOrderViewModel> ReviewOrderList { set; get; }

        public decimal Discount { get; set; }
        public decimal TotalShipping { get; set; }
        public decimal TotalTax { get; set; }
        public decimal SubTotal { get; set; }
        public decimal Total { get; set; }
        public List<CouponViewModel> Coupons { get; set; }
       
    }
}