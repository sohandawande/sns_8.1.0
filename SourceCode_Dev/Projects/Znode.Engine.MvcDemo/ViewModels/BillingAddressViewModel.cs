﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class BillingAddressViewModel : BaseViewModel
    {
        public AddressViewModel ShippingAddress { get; set; }

        public AddressViewModel BillingAddress { get; set; }

        public bool UseSameAsShippingAddress { get; set; }

        public BillingAddressViewModel()
        {
            ShippingAddress = new AddressViewModel();
            BillingAddress= new AddressViewModel();
        }
    }
}