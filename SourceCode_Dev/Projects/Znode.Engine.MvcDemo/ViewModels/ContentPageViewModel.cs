﻿
namespace Znode.Engine.MvcDemo.ViewModels
{
    /// <summary>
    /// Content page view model.
    /// </summary>
    public class ContentPageViewModel : BaseViewModel
    {
        #region Public Properties

        public string PageTitle { get; set; }
        public string SEOTitle { get; set; }
        public string Name { get; set; }

        //PRFT Custom Code : Start
        public string SEOUrl { get; set; }
        //PRFT Custom Code : End

        #endregion
    }
}