﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class AttributeViewModel : BaseViewModel
    {
        public int AttributeId { get; set; }
        public int AttributeTypeId { get; set; }
        public string Name { get; set; }
        public string ExternalId { get; set; }
        public int DisplayOrder { get; set; }
        public Collection<int> SkuIds { get; set; }
        public bool Available { get; set; }
        public bool Selected { get; set; }
    }
}