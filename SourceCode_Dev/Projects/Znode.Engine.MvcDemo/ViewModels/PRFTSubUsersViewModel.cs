﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class PRFTSubUsersViewModel : BaseViewModel
    {
        // Response
        public Collection<AccountViewModel> SubUserAccounts { get; set; }

        public int OldPageNumber { get; set; }
        public int TotalPages { get; set; }
        public int TotalAccounts { get; set; }
        public string NextPageUrl { get; set; }
        public string PreviousPageurl { get; set; }
        public string Username { get; set; }

        //PRFT Custom Code: Start

        public int FirstPageNumber { get; set; }
        public string FirstPageUrl { get; set; }
        public int LastPageNumber { get; set; }
        public string LastPageUrl { get; set; }
        //PRFT Custom Code: End

        public IEnumerable<SelectListItem> PageSizes { get; set; }

        public int? PageNumber { get; set; }
        public int? PageSize { get; set; }
        public string searchKeyword { get; set; }

        public PRFTSubUsersViewModel()
        {
            SubUserAccounts = new Collection<AccountViewModel>();
        }
    }
}