﻿using System.ComponentModel.DataAnnotations;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class ShippingBillingAddressViewModel : BaseViewModel
    {
        public AddressViewModel ShippingAddressModel { get; set; }
        public AddressViewModel BillingAddressModel { get; set; }
        public LoginViewModel LoginViewModel { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResource), ErrorMessageResourceName = "RequiredEmailID")]
        public string EmailAddress { get; set; }
        public int ShippingAddressId { get; set; }
        public int BillingAddressId { get; set; }
        public bool CreateLogin { get; set; }
        public bool IsAddressChange { get; set; }
        public ShippingBillingAddressViewModel()
        {
            ShippingAddressModel = new AddressViewModel();
            BillingAddressModel = new AddressViewModel();
            LoginViewModel = new LoginViewModel();
        }
        public bool UseSameAsShippingAddress { get; set; }
    }
}