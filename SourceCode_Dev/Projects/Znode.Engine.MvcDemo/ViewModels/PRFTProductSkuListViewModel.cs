﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class PRFTProductSkuListViewModel : BaseViewModel
    {
        public PRFTProductSkuListViewModel()
        {
            SkuList = new PRFTSkuListViewModel();
            ExternalId = Guid.NewGuid().ToString();
        }
        public PRFTSkuListViewModel SkuList { get; set; }
        public int ProductId { get; set; }
        public int AttributeCount { get; set; }
        public bool ParentProduct { get; set; }
        public bool IsGiftCard { get; set; }
        public int? PortalId { get; set; }
        public string ProductName { get; set; }
        public decimal ProductPrice { get; set; }
        public int MinQuantity { get; set; }
        public int MaxQuantity { get; set; }
        public decimal Price { get; set; }
        public bool IsCallForPricing { get; set; }
        public bool ShowAddToCart { get; set; }

        [Required]
        //[LocalizedDisplayName(Name = RZnodeResources.LabelQuantityOnHand)]
        [Range(0, 999999, ErrorMessageResourceType = typeof(Resources.ZnodeResource), ErrorMessageResourceName = "ErrorAddOnNumberRange")]
        public int Quantity { get; set; }

        public string ExternalId { get; set; }
        public IEnumerable<HighlightViewModel> HighLights { get; set; }

        public string AttributeDescription { get; set; }
    }
}