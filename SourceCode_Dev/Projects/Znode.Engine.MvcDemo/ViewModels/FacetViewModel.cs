﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class FacetViewModel : BaseViewModel
    {
        public string AttributeName { set; get; }
        public Collection<FacetValueViewModel> AttributeValues { set; get; }
        public int ControlTypeId { get; set; }
        public int RecordCount { get; set; }
    }
}