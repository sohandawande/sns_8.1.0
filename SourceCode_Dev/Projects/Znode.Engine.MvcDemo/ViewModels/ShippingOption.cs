﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class ShippingOptionViewModel : BaseViewModel
    {
        public string ShippingDescription { get; set; }
        public int ShippingId { get; set; }
        public string ShippingCode { get; set; }
        public int SelectedShippingCode { get; set; }
    }
}