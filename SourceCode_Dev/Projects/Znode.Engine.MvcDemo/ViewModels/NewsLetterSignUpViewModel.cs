﻿
namespace Znode.Engine.MvcDemo.ViewModels
{
    public class NewsLetterSignUpViewModel : BaseViewModel
    {
        public string Email { get; set; }
    }
}