﻿using System.Collections.Generic;
using System.Web.WebPages.Html;

namespace Znode.Engine.MvcDemo.ViewModels
{
    /// <summary>
    /// Highlight List View Model
    /// </summary>
    public class HighlightListViewModel : BaseViewModel
    {
        #region Constructor
        /// <summary>
        /// Constructor for HighlightsListViewModel
        /// </summary>
        public HighlightListViewModel()
        {
            Highlights = new List<HighlightViewModel>();
            HighlightTypeList = new List<SelectListItem>();
        } 
        #endregion

        public string Name { get; set; }
        public int TypeId { get; set; }
        public string SelectedHighlights { get; set; }
        public List<HighlightViewModel> Highlights { get; set; }
        public List<SelectListItem> HighlightTypeList { get; set; }
        public int HighlightTypeId { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
    }
}