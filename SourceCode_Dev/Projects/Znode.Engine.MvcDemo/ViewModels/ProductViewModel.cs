﻿using System.Collections.Generic;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class ProductViewModel : BaseViewModel
    {
        public ProductViewModel()
        {
            ProductAttributes = new ProductAttributesViewModel();
            ShowQuantity = true;
        }
        
        public bool IsCallForPricing { get; set; }
        public string BundleItemsIds { get; set; }
        public string ShortDescription { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public string ImageAltTag { get; set; }
        public string ImageFile { get; set; }
        public string ImageSmallPath { get; set; }
        public string ImageLargePath { get; set; }
        public string ImageMediumPath { get; set; }
        public string ImageSmallThumbnailPath { get; set; }
        public string InStockMessage { get; set; }
        public string BackOrderMessage { get; set; }
        public string OutOfStockMessage { get; set; }
        public int ProductId { get; set; }
        public decimal Price { get; set; }
        public decimal ProductPrice { get; set; }
        public decimal OriginalPrice { get; set; }
        public string ProductNumber { get; set; }
        public string Sku { get; set; }
        public int MinQuantity { get; set; }
        public int MaxQuantity { get; set; }
        public bool? TrackInventory { get; set; }
        public bool? AllowBackOrder { get; set; }
        public bool IsActive { get; set; }
        public string InventoryMessage { get; set; }
        public string SeoDescription { get; set; }
        public string SeoKeywords { get; set; }
        public string SeoTitle { get; set; }
        public string SeoPageName { get; set; }
        public bool ShowAddToCart { get; set; }
        public bool ShowWishlist { get; set; }
        public bool ShowQuantity { get; set; }
        public int SelectedQuantity { get; set; }
        public string CategoryName { get; set; }
        public int Rating { get; set; }
        public int WishListID { get; set; }
        public string FBTProductsIds { get; set; }
        public string YMALProductsIds { get; set; }
        public ProductAttributesViewModel ProductAttributes { get; set; }
        public IEnumerable<AddOnViewModel> AddOns { get; set; }
        public ReviewViewModel ProductReviews { get; set; }
        public IEnumerable<ImageViewModel> Images { get; set; }
        public List<ImageViewModel> SwatchImages { get; set; }
        public decimal? Weight { get; set; }
        public decimal? Width { get; set; }
        public string VendorName { get; set; }
        public decimal? Length { get; set; }
        public string Manufacturer { get; set; }       
        public string ProductSpecification { get; set; }
        public string VendorEmail { get; set; }

        public bool RecurringBillingInd { get; set; }
        public decimal? RecurringBillingInitialAmount { get; set; }
        public string RecurringBillingFrequency { get; set; }
        public string FeaturesDescription { get; set; }
        public int CategoryId { get; set; }
        public bool IsFeatured { get; set; }
        public bool IsNewProduct { get; set; }
        public decimal? Height { get; set; }

        public bool IsQuickView { get; set; }

        #region PRFT Custom Code        
        public IEnumerable<HighlightViewModel> HighLights { get; set; }
        public string DownloadLink { get; set; }
        #endregion
    }
}