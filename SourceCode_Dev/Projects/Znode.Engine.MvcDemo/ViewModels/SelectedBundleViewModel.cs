﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class SelectedBundleViewModel
    {

        public string ExternalId { get; set; }
        public int[] AddOnValueIds { get; set; }
        public int ProductId { set; get; }
        public string SelectedAttributes { get; set; }
        public string Sku { set; get; }
    }
}