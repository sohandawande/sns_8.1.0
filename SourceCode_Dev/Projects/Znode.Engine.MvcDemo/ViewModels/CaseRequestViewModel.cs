﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Znode.Engine.MvcDemo.ViewModels
{
    /// <summary>
    /// Case Request View Model
    /// </summary>
    public partial class CaseRequestViewModel : BaseViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CompanyName { get; set; }
        public string EmailAddress { get; set; }
        public string PhoneNumber { get; set; }
        public string Message { get; set; }
        public string CaseOrigin { get; set; }
        public string CaseRequestTitle { get; set; }
        public string CreateUser { get; set; }
        public string StoreName { get; set; }
        public string CaseStatusName { get; set; }
        public string CasePriorityName { get; set; }
        public string CustomerCity { get; set; }
        public string CustomerState { get; set; }

        public int CaseRequestId { get; set; }
        public int CaseStatusId { get; set; }
        public int PortalId { get; set; }
        public int? AccountId { get; set; }
        public int CasePriorityId { get; set; }
        public int CaseTypeId { get; set; }

        public DateTime? CreatedDate { get; set; }

        public bool AllowSharingWithCustomer { get; set; }
    }
}