﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class ShippingOptionListViewModel : BaseViewModel
    {

        public ShippingOptionListViewModel()
        {
            ShippingOptions = new List<ShippingOptionViewModel>();
        }

        public List<ShippingOptionViewModel> ShippingOptions { get; set; }

        public AddressViewModel ShippingAddress { get; set; }

        public bool IsMultipleShipTo
        {
            get
            {
                Agents.CheckoutAgent _checkout = new Agents.CheckoutAgent();
                var cart = _checkout.GetMultiCartOrderReview();
                if (cart != null)
                {
                    if (cart.ReviewOrderList.Count > 1)
                        return true;
                    else
                        return false;
                }
                else
                    return false;

            }
        }
    }
}