﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class PRFTWishListListViewModel : BaseViewModel
    {
        public Collection<PRFTWishListViewModel> Items { get; set; }

        public PRFTWishListListViewModel()
        {
            Items = new Collection<PRFTWishListViewModel>();
        }
    }
}