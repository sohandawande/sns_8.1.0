﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Znode.Engine.Api.Models;
using System.Collections.ObjectModel;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public partial class CaseRequestViewModel : BaseViewModel
    {

        public Collection<PRFTSalesRepresentative> SalesReps { get; set; }
        public int SalesRepId { get; set; }
        public bool IsRequestAQuoteForm { get; set; }
        public RequestAQuoteViewModel RequestAQuote { get; set; }
        public ContactUsExtnViewModel ContactUsExtn { get; set; }
        public bool IsSalesRepFound { get; set; }
        public string SaleRepNameWithLocation { get; set; }
        public string SalesRepEmail { get; set; }
    }
    public class RequestAQuoteViewModel : BaseViewModel
    {
        [DataType(DataType.Date)]
        public DateTime DueDate { get; set; }

        public string FaxNumber { get; set; }

        public string ContactChoiceSelected { get; set; }

        public List<ContactChoice> ContactChoices { get; set; }
    }
    public class ContactUsExtnViewModel : BaseViewModel
    {
        public string City { get; set; }

        public string CountryCode { get; set; }

        public string StateCode { get; set; }

        public Collection<CountryModel> Countries { set; get; }

        public Collection<StateModel> States { set; get; }

    }
    public class ContactChoice
    {
        public string ChoiceName { get; set; }
        public bool IsSelected { get; set; }
    }
}