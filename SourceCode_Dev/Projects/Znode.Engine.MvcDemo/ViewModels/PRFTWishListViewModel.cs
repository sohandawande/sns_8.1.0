﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class PRFTWishListViewModel : BaseViewModel
    {
        public int PRFTWishListID { get; set; }
        public int ShopperAccountID { get; set; }
        public string WishListName { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastModified { get; set; }
        public string Custom1 { get; set; }
        public string Custom2 { get; set; }
        public string Custom3 { get; set; }

        public Collection<PRFTWishListLineItemViewModel> WishListItems { get; set; }
        public PRFTWishListViewModel()
        {
            WishListItems = new Collection<PRFTWishListLineItemViewModel>();
        }
    }
}