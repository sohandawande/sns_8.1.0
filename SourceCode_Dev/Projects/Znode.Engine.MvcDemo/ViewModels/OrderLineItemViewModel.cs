﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class OrderLineItemViewModel : BaseViewModel
    {
        public int OrderLineItemId { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public decimal Total { get; set; }
        public decimal SubTotal { get; set; }
        public decimal Discount { get; set; }
        public decimal Tax { get; set; }
        public decimal Shipping { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? ParentOrderLineItemId { get; set; }
        public int? OrderLineItemRelationshipTypeId { get; set; }
        public string ShippingAddress { get; set; }
        public int ShippingId { get; set; }
        public string DigitalAsset { get; set; }

        #region PRFT Custom Code
        public string ProductCategory { get; set; }        
        #endregion
    }
}