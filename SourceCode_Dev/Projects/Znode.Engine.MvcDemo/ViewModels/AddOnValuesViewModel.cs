﻿using Znode.Engine.MvcDemo.Helpers;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class AddOnValuesViewModel : BaseViewModel
    {
        public int AddOnValueId { get; set; }
        public bool DefaultIndicator { get; set; }
        public string Name { get; set; }
        public bool IsSelected { get; set; }
        public string CustomText { get; set; }
        public decimal RetailPrice { get; set; }
        public decimal? SalePrice { get; set; }
        public decimal? WholeSalePrice { get; set; }
        public decimal AddOnFinalPrice { get; set; }
        public string SKU { get; set; }
        public int QuantityOnHand { get; set; }
        public int DisplayOrder { get; set; }

        public string AddOnValueText
        {
            get
            {
                if (AddOnFinalPrice > 0)
                    return string.Format("{0} + {1}", Name, HelperMethods.FormatPriceWithCurrency(AddOnFinalPrice));

                return Name;
            }
        }

    }
}