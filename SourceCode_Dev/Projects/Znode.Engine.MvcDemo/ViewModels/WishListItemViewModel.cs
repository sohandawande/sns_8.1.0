﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class WishListItemViewModel : BaseViewModel
    {
        public int AccountId { get; set; }
        public DateTime CreateDate { get; set; }
        public string Custom { get; set; }
        public int ProductId { get; set; }
        public int WishListId { get; set; }
        public ProductViewModel Product { get; set; }

        public WishListItemViewModel()
        {
            Product = new ProductViewModel();
        }
    }
}