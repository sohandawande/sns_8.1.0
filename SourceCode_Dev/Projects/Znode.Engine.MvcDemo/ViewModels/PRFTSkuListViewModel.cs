﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcDemo.ViewModels
{
    /// <summary>
    /// View Model For SkuList
    /// </summary>
    public class PRFTSkuListViewModel : BaseViewModel
    {
        /// <summary>
        ///  Constructor for SkuListViewModel
        /// </summary>
        public PRFTSkuListViewModel()
        {
            Skus = new List<PRFTSkuViewModel>();
        }

        public List<PRFTSkuViewModel> Skus { get; set; }
    }
}