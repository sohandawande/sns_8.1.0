﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.Helpers;
using Znode.Libraries.Helpers.Attributes;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class PRFTCreditApplicationViewModel:BaseViewModel
    {
        public int CreditApplicationID { get; set; }
        public string BusinessName { get; set; }
        public string BusinessStreet { get; set; }
        public string BusinessStreet1 { get; set; }
        public string BusinessCity { get; set; }
        public string BusinessStateCode { get; set; }
        public string BusinessPostalCode { get; set; }
        public string BusinessCountryCode { get; set; }
        public string BusinessPhoneNumber { get; set; }
        public string BusinessFaxNumber { get; set; }
        public string BusinessEmail { get; set; }
        public int? BusinessYearEstablished { get; set; }
        public string BusinessType { get; set; }
        public bool? IsTaxExempt { get; set; }
        public string TaxExemptCertificate { get; set; }
        public string BillingStreet { get; set; }
        public string BillingStreet1 { get; set; }
        public string BillingCity { get; set; }
        public string BillingStateCode { get; set; }
        public string BillingPostalCode { get; set; }
        public string BillingCountryCode { get; set; }
        public string BankName { get; set; }
        public string BankAddress { get; set; }
        public string BankPhoneNumber { get; set; }
        public string BankAccountNumber { get; set; }
        public DateTime? RequestDate { get; set; }
        public bool? IsCreditApproved { get; set; }
        public int? CreditDays { get; set; }
        public DateTime? CreditApprovedDate { get; set; }
        public Collection<PRFTTradeReferencesViewModel> TradeReferences { get; set; }
        public HttpPostedFileBase TaxExemptCertificateAttachments { get; set; }
        public string Custom1 { get; set; }
        public string Custom2 { get; set; }
        public string Custom3 { get; set; }
        public string Custom4 { get; set; }
        public string Custom5 { get; set; }

        public PRFTCreditApplicationViewModel()
        {
            IsTaxExempt = false;
            RequestDate = DateTime.Now;
            IsCreditApproved = false;
            CreditDays = 0;
            CreditApprovedDate = DateTime.Now;
            TradeReferences = new Collection<PRFTTradeReferencesViewModel>();
        }
    }
}