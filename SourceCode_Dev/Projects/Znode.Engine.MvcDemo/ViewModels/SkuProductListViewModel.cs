﻿using System.Collections.Generic;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class SkuProductListViewModel : BaseViewModel
    {
        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public SkuProductListViewModel()
        {
            SkuProductList = new List<SkuProductViewModel>();
        } 
        #endregion

        public List<SkuProductViewModel> SkuProductList { set; get; }
    }
}