﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class PRFTSkuViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for SkuViewModel
        /// </summary>
        public PRFTSkuViewModel()
        {
            ExternalId = Guid.NewGuid().ToString();
        }

        [Required]
        public string SKU { get; set; }
        public string SKUPicturePath { get; set; }
        public string ImageAltTag { get; set; }
        public string ImageLargePath { get; set; }
        public string ImageMediumPath { get; set; }
        public string ImageSmallPath { get; set; }
        public string ImageSmallThumbnailPath { get; set; }
        public string ImageThumbnailPath { get; set; }
        public string RecurringBillingPeriod { get; set; }
        public string RecurringBillingFrequency { get; set; }
        public string ProductTypeName { get; set; }
        public string Name { get; set; }
        public int Id { get; set; }
        public int ProductId { get; set; }

        public int? SupplierId { get; set; }
        public int? RecurringBillingTotalCycles { get; set; }

        public decimal? WeightAdditional { get; set; }

        public decimal? RetailPriceOverride { get; set; }

        public decimal? SalePriceOverride { get; set; }

        public decimal? WholesalePriceOverride { get; set; }
        public decimal? RecurringBillingInitialAmount { get; set; }

        public bool IsActive { get; set; }
        public bool IsGiftCard { get; set; }
        public bool ParentChildProduct { get; set; }
        public int AttributeCount { get; set; }

        public int InventoryId { get; set; }
        [Required]
        public int? QuantityOnHand { get; set; }

        public int? ReorderLevel { get; set; }

        public int ProductTypeId { get; set; }

        public string ProductAttributes { get; set; }
        public string ErrorMessage { get; set; }

        public List<SelectListItem> SupplierList { get; set; }

        public HttpPostedFileBase SkuImage { get; set; }
        
        public bool IsEnableActive { get; set; }
        public bool NoImage { get; set; }
        public string UserType { get; set; }

        //For group product
        public string Description { get; set; }
        public string Custom1 { get; set; }
        public string ExternalId { get; set; }

        public string AttributeDescription { get; set; }
    }
}