﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class AccountViewModel : BaseViewModel
    {
        public int AccountId { get; set; }
        public Collection<AddressViewModel> Addresses { get; set; }
        public string CompanyName { get; set; }
        public bool EmailOptIn { get; set; }
        public bool? EnableCustomerPricing { get; set; }
        public string ExternalId { get; set; }
        //public bool? IsActive { get; set; } //PRFT Custom Code
        public int? ProfileId { get; set; }
        public Guid? UserId { get; set; }
        public WishListViewModel WishList { get; set; }
        public Collection<OrdersViewModel> Orders { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResource), ErrorMessageResourceName = "RequiredUserName")]
        public string UserName { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResource), ErrorMessageResourceName = "RequiredEmailID")]
        [EmailAddress(ErrorMessageResourceType = typeof(Resources.ZnodeResource), ErrorMessageResourceName = "ValidEmailAddress", ErrorMessage = "")]
        public string EmailAddress { get; set; }
        public string PasswordQuestion { get; set; }

        public string PasswordAnswer { get; set; }
        public bool UseWholeSalePricing { get; set; }
        public bool IsResetPassword { get; set; }
        public string BaseUrl { get; set; }
        public bool IsSinglePageCheckoutUser { get; set; }
        public bool IsLockedUser { get; set; }

        //PRFT Custom Code:Start
        public string Custom1 { get; set; }
        public string Custom2 { get; set; }
        public string Custom3 { get; set; }        
        public int? ParentAccountId { get; set; }
        public bool IsActive { get; set; }

        public int AssociatedCustomerId { get; set; }
        //PRFT Custom Code:End
        
        public AddressViewModel BillingAddress
        {
            get
            {
                return Addresses.FirstOrDefault(x => x.IsDefaultBilling);
            }
        }

        public AddressViewModel ShippingAddress
        {
            get
            {
                return Addresses.FirstOrDefault(x => x.IsDefaultShipping);
            }
        }

        public int WishlistCount
        {
            get
            {
                if (WishList.Items != null && WishList.Items.Any())
                {
                    return WishList.Items.Count(x => x.Product.IsActive);
                }
                return 0;
            }
        }

        public int ReviewCount { get; set; }

        public IEnumerable<OrdersViewModel> DashboardOrders
        {
            get
            {
                return Orders.OrderByDescending(x => x.OrderId).Take(Helpers.MvcDemoConstants.DashboardOrderCount);
            }
        }

        public Collection<GiftCardHistoryViewModel> GiftCardHistory { get; set; }

        public int GiftCardHistoryCount { get; set; }
        public bool IsValidToMultipleShip
        {
            get
            {
                Agents.CartAgent _cart = new Agents.CartAgent();
                int count = _cart.GetCart().Items.Count();
                if (count > 1)
                {
                    return true;
                }
                else if (count.Equals(1))
                {
                    int quantity = _cart.GetCart().Items[0].Quantity;
                    if (quantity > 1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }

            }
        }

        public string TrackingLink { get; set; }
        public decimal? ReferralCommission { get; set; }
        public string TaxId { get; set; }
        public string ReferralCommissionType { get; set; }
        public string ReferralStatus { get; set; }
        public int? ReferralCommissionTypeId { get; set; }

        public AccountViewModel()
        {
            Addresses = new Collection<AddressViewModel>();
            WishList = new WishListViewModel();
            Orders = new Collection<OrdersViewModel>();
            GiftCardHistory = new Collection<GiftCardHistoryViewModel>();
        }
    }
}