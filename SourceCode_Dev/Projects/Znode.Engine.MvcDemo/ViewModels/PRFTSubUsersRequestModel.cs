﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class PRFTSubUsersRequestModel : BaseViewModel
    {
        // Request
        public string ExternalAccountId { get; set; }
        public int? PageNumber { get; set; }
        public int? PageSize { get; set; }
        public Collection<KeyValuePair<string, IEnumerable<string>>> RefineBy { get; set; }
        public string Sort { get; set; }
        public string ParentAccountId { get; set; } 

        //PRFT Custom Code:START
        public IEnumerable<SelectListItem> PageSizes { get; set; }
        public int TotalPages { get; set; }
        public string searchTerm { get; set; }
        //PRFT Custom Code:END
    }
}