﻿
namespace Znode.Engine.MvcDemo.ViewModels
{
    /// <summary>
    /// Highlight View Model
    /// </summary>
    public class HighlightViewModel : BaseViewModel
    {
        #region Constructor
        /// <summary>
        /// Constructor for Highlight View Model
        /// </summary>
        public HighlightViewModel()
        {

        }
        #endregion

        public string ImageAltTag { get; set; }
        public string ImageFile { get; set; }
        public string ImageLargePath { get; set; }
        public string ImageMediumPath { get; set; }
        public string ImageSmallPath { get; set; }
        public string ImageSmallThumbnailPath { get; set; }
        public string ImageThumbnailPath { get; set; }
        public string NewImageFileName { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public int HighlightId { get; set; }
        public int ProductHighlightID { get; set; }
        public int ProductId { get; set; }
        public int? HighlightTypeId { get; set; }
        public string HighlightTypeName { get; set; }
        public string Hyperlink { get; set; }
        public bool DispalyText { get; set; }
        public bool DisplayHyperlink { get; set; }
        public bool ShowPopup { get; set; }
    }
}
