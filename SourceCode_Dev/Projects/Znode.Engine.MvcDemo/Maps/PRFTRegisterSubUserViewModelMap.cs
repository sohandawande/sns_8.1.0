﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Maps
{
    public static class PRFTRegisterSubUserViewModelMap
    {
        public static PRFTRegisterSubUserViewModel ToModel(AccountViewModel model)
        {
            return new PRFTRegisterSubUserViewModel()
            {
                EmailAddress = model.EmailAddress,
                UserName = model.UserName,
                AccountId=model.AccountId
            };
        }
    }
}