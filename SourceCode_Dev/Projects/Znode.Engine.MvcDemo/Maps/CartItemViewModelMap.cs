﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.Helpers;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Maps
{
    public static class CartItemViewModelMap
    {

        public static CartItemViewModel ToViewModel(ShoppingCartItemModel model)
        {
            var viewModel = new CartItemViewModel()
                {
                    Description = model.CartDescription,
                    ExternalId = model.ExternalId,
                    AttributeIds = model.AttributeIds,
                    ProductId = model.ProductId,
                    Quantity = model.Quantity,
                    ShippingCost = model.ShippingCost,
                    Sku = model.Sku,
                    UnitPrice = model.UnitPrice,
                    ExtendedPrice = model.ExtendedPrice,
                    InsufficientQuantity = model.InsufficientQuantity,
                    AddOnValueIds = model.AddOnValueIds != null ? string.Join(",", model.AddOnValueIds) : string.Empty,
                    //ZNode Version 7.2.2 - Map SKU Image Path
                    ImagePath = HelperMethods.GetImagePath(model.ImagePath),
                    RecurringBillingFrequency = model.RecurringBillingFrequency,
                    RecurringBillingInd = model.RecurringBillingInd,
                    RecurringBillingInitialAmount = model.RecurringBillingInitialAmount,
                    ProductDiscountAmount = model.ProductDiscountAmount,
                    //PRFT Custom Code:Start
                    ERPUnitPrice = model.ERPUnitPrice,
                    ERPExtendedPrice=model.ERPExtendedPrice,
                    BackOrderMessage=model.BackOrderMessage                    
                    //PRFT Custom Code:end
            };

            if (model.BundleItems != null && model.BundleItems.Any())
            {
                viewModel.SelectedBundles =
                    new Collection<SelectedBundleViewModel>(model.BundleItems.Select(ToBundleItemViewModel).ToList());
            }

            if (!Equals(model.AddOnValuesCustomText, null) && !Equals(model.AddOnValuesCustomText.Count, 0))
            {
                for (int index = 0; index < model.AddOnValueIds.Length; index++)
                {
                    viewModel.AddOnValuesCustomText = Equals(viewModel.AddOnValuesCustomText,null) ? model.AddOnValuesCustomText[Convert.ToInt32(model.AddOnValueIds[index])] : viewModel.AddOnValuesCustomText + "," + model.AddOnValuesCustomText[Convert.ToInt32(model.AddOnValueIds[index])];
                }
            }

            return viewModel;
        }

        public static ShoppingCartItemModel ToModel(CartItemViewModel viewModel)
        {
            var itemModel = new ShoppingCartItemModel()
            {

                ExternalId = viewModel.ExternalId,
                AttributeIds = viewModel.AttributeIds,
                ProductId = viewModel.ProductId,
                Quantity = viewModel.Quantity,
                ShippingCost = viewModel.ShippingCost,
                Sku = viewModel.Sku,
                CartDescription = viewModel.Description,
                UnitPrice = viewModel.UnitPrice,
                ExtendedPrice = viewModel.ExtendedPrice,
                AddOnValueIds = (!string.IsNullOrEmpty(viewModel.AddOnValueIds) ? viewModel.AddOnValueIds.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToArray() : null),
                ProductDiscountAmount = viewModel.ProductDiscountAmount,
                //PRFT Custom Code:Start
                ERPUnitPrice=viewModel.ERPUnitPrice,
                ERPExtendedPrice=viewModel.ERPExtendedPrice,
                BackOrderMessage=viewModel.BackOrderMessage,
                AllowBackOrder=viewModel.AllowBackOrder
                //PRFT Custom Code:End
            };

            if (!string.IsNullOrEmpty(viewModel.AddOnValuesCustomText))
            {
                itemModel.AddOnValuesCustomText = GetAddOnValuesCustomTextDictionary(viewModel);
            }

            if (viewModel.SelectedBundles != null && viewModel.SelectedBundles.Any())
            {
                itemModel.BundleItems =
                    new Collection<BundleItemModel>(viewModel.SelectedBundles.Select(ToBundleItemModel).ToList());
            }
            return itemModel;
        }

        public static BundleItemModel ToBundleItemModel(SelectedBundleViewModel model)
        {
            var itemModel = new BundleItemModel()
            {
                AddOnValueIds = model.AddOnValueIds,
                ProductId = model.ProductId,
                Sku = model.Sku,
                ExternalId = model.ExternalId
            };
            return itemModel;
        }

        public static SelectedBundleViewModel ToBundleItemViewModel(BundleItemModel model)
        {
            var itemModel = new SelectedBundleViewModel()
            {
                AddOnValueIds = model.AddOnValueIds,
                ProductId = model.ProductId,
                Sku = model.Sku,
                ExternalId = model.ExternalId
            };
            return itemModel;
        }

        public static ShoppingCartItemModel ToShoppingCartModel(int productId, int quantity, string sku)
        {
            var itemModel = new ShoppingCartItemModel()
                {
                    ProductId = productId,
                    Sku = sku,
                    Quantity = quantity,
                };
            return itemModel;
        }

        public static CartItemViewModel ToViewModel(BundleDetailsViewModel bundleDetailsView)
        {
            var itemViewModel = new CartItemViewModel()
                {
                    AttributeIds = bundleDetailsView.SelectedAttributesIds,
                    ProductId = bundleDetailsView.SelectedProductId,
                    Quantity = bundleDetailsView.SelectedQuantity,
                    Sku = bundleDetailsView.SelectedSku,
                    AddOnValueIds = bundleDetailsView.SelectedAddOnValueIds,
                    AddOnValuesCustomText = bundleDetailsView.AddOnValuesCustomText,

                };

            if (bundleDetailsView.Bundles != null && bundleDetailsView.Bundles.Any())
            {
                itemViewModel.SelectedBundles = new Collection<SelectedBundleViewModel>();
                foreach (var bundle in bundleDetailsView.Bundles)
                {
                    var bundleAttributes =
                        bundle.ProductAttributes.Attributes.Select(x => x.SelectedAttributeId).Where(x => x != null);
                    itemViewModel.SelectedBundles.Add(new SelectedBundleViewModel()
                        {
                            SelectedAttributes = bundleAttributes.Any() ? string.Join(",", bundleAttributes.ToArray()) : "",
                            ProductId = bundle.ProductId,
                            AddOnValueIds = bundle.AddOns != null ? bundle.AddOns.Where(y => y.SelectedAddOnValue != null).SelectMany(x => x.SelectedAddOnValue).ToArray() : null,
                            Sku = bundle.Sku
                        });
                }
            }
            return itemViewModel;
        }

        private static Dictionary<int, string> GetAddOnValuesCustomTextDictionary(CartItemViewModel viewModel)
        {
            Dictionary<int, string> addOnValuesCustomText = new Dictionary<int, string>();

            string[] addOnValueIds =  !Equals(viewModel.AddOnValueIds,null)?viewModel.AddOnValueIds.Split(','):null;

            string[] addOnValuesText = !Equals(viewModel.AddOnValueIds,null)?viewModel.AddOnValuesCustomText.Split(','):null;
            if (!Equals(addOnValueIds,null) && !Equals(addOnValuesText,null))
            {
                for (int index = 0; index < addOnValueIds.Length; index++)
                {
                    if (!string.IsNullOrEmpty(addOnValueIds[index]))
                    {
                        addOnValuesCustomText.Add(Convert.ToInt32(addOnValueIds[index]), addOnValuesText[index]);
                    }
                }
            }
            
            return addOnValuesCustomText;
        }
    }
}