﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.ViewModels;
namespace Znode.Engine.MvcDemo.Maps
{
    public static class SearchMap
    {

        public static SearchCategoryViewModel ToCategoryViewModel(SearchCategoryModel searchCategoryModel)
        {
            var categories = new SearchCategoryViewModel()
            {
                Count = searchCategoryModel.Count,
                Hierarchy = PopulateCategory(searchCategoryModel.Hierarchy),
                Name = searchCategoryModel.Name,
                Categoryurl = UpdateUrlQueryString("category", searchCategoryModel.Name, "Search", false),
            };

            return categories;
        }

        private static Collection<SearchCategoryViewModel> PopulateCategory(Collection<SearchCategoryModel> searchCategoryModel)
        {
            if (searchCategoryModel != null && searchCategoryModel.Any())
            {
                var searchCategoryViewList = searchCategoryModel.Select(x => new SearchCategoryViewModel()
                {
                    Name = x.Name,
                    Count = x.Count,
                    Hierarchy = PopulateCategory(x.Hierarchy),
                    Categoryurl = UpdateUrlQueryString("category", x.Name, "Search", false)

                }).ToList();

                if (searchCategoryViewList.Any())
                    return new Collection<SearchCategoryViewModel>(searchCategoryViewList);
            }

            return new Collection<SearchCategoryViewModel>();
        }

        public static FacetViewModel ToFacetViewModel(SearchFacetModel searchFacetModel)
        {
            var facet = new FacetViewModel()
            {
                //AttributeName = searchFacetModel.AttributeName.Split('|')[0],
                AttributeName = searchFacetModel.AttributeName.Split('|')[0].Replace('_',' '),
                AttributeValues = new Collection<FacetValueViewModel>(searchFacetModel.AttributeValues.Select(x => new FacetValueViewModel()
                {
                    AttributeValue = x.AttributeValue,
                    FacetCount = x.FacetCount,
                    RefineByUrl = UpdateUrlQueryString(searchFacetModel.AttributeName.Split(new string[] { "|" }, StringSplitOptions.None)[0], x.AttributeValue, "Search", true),
                }).ToList()),
                ControlTypeId = searchFacetModel.ControlTypeId,
            };

            return facet;
        }

        public static SuggestedSearchModel ToModel(string searchTerm, string category)
        {
            return new SuggestedSearchModel() { Category = category, Term = searchTerm };
        }


        public static KeywordSearchModel ToKeywordSearchModel(string searchKeyword, string category, Collection<KeyValuePair<string, IEnumerable<string>>> refineBy, List<string> innerSearchTerm)
        {
            var keywordSearch = new KeywordSearchModel()
                {
                    Category = category,
                    Keyword = searchKeyword,
                    InnerSearchKeywords = new Collection<string>(innerSearchTerm),
                    RefineBy = refineBy
                };

            return keywordSearch;
        }

        public static string UpdateUrlQueryString(string key, string value, bool keepExisting = false, bool toRemove = false)
        {
            return UpdateUrlQueryString(key, value, null, keepExisting, toRemove);
        }

        public static string UpdateUrlQueryString(string key, string value, string urlPath, bool keepExisting = false, bool toRemove = false)
        {
            // To run NUnit test cases the below code added
            if (HttpContext.Current == null)
                return string.Empty;


            var nameValues = HttpUtility.ParseQueryString(HttpContext.Current.Request.QueryString.ToString());
            var currentValue = value;

            if (keepExisting && !string.IsNullOrEmpty(nameValues.Get(key)))
            {
                currentValue = string.Format("{0},{1}", nameValues.Get(key), currentValue);
            }
            else if (toRemove && !string.IsNullOrEmpty(nameValues.Get(key)) && !string.IsNullOrEmpty(currentValue))
            {
                var existingValues = nameValues.Get(key).Split(',');
                var newValues = existingValues.Where(x => x != currentValue);
                currentValue = string.Join(",", newValues);
            }

            if (string.IsNullOrEmpty(currentValue))
                nameValues.Remove(key);
            else
                nameValues.Set(key, currentValue);

            //PRFT Custom Code- To convert url in lower case : Start
            //string url = (!string.IsNullOrEmpty(urlPath)) ? urlPath : HttpContext.Current.Request.Url.AbsolutePath;
            string url = (!string.IsNullOrEmpty(urlPath)) ? urlPath.ToLower() : HttpContext.Current.Request.Url.AbsolutePath;
            //PRFT Custom Code : End

            if (nameValues.Get("category") == null && HttpContext.Current.Request.RequestContext.RouteData.Values["category"] != null)
            {
                nameValues.Set("category", HttpContext.Current.Request.RequestContext.RouteData.Values["category"].ToString());
            }

            string updatedQueryString = "?" + nameValues.ToString();
            return url + updatedQueryString;

        }

        /// <summary>
        /// Map SEO Url Model to SEO Url View Model.
        /// </summary>
        /// <param name="model">SEOUrlModel model</param>
        /// <returns>Return SEO Url detail in SEOUrlViewModel format.</returns>
        public static SEOUrlViewModel ToSeoViewModel(SEOUrlModel model)
        {
            SEOUrlViewModel viewModel = new SEOUrlViewModel();
            if (!Equals(model, null))
            {
                viewModel.SeoUrl = model.SeoUrl;
                viewModel.ProductId = model.ProductId;
                viewModel.CategoryName = model.CategoryName;
                viewModel.ContentPageName = model.ContentPageName;
            }
            return viewModel;
        }
    }
}