﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Maps
{
    public class PRFTERPCustomerAccountModelMap
    {
        public static PRFTERPAccountDetailsViewModel ToERPCustomerAccountViewModel(PRFTERPAccountDetailsModel model)
        {
            if (model != null)
            {
                var eRPAccountModel = new PRFTERPAccountDetailsViewModel()
                {
                    BillingAddress = AddressViewModelMap.ToModel(model.BillingAddress),
                    DebtorCode = model.DebtorCode,
                    CustomerType = model.CustomerType,
                    CustomerTypeDescription = model.CustomerTypeDescription,
                    CompanyCodeDebtorAccountID = model.CompanyCodeDebtorAccountID,
                    CustomerStatus = model.CustomerStatus,
                    CustomerStatusDescription = model.CustomerStatusDescription,
                    PaymentCondition = model.PaymentCondition,
                    PaymentConditionDescription = model.PaymentConditionDescription,
                    Terms = model.Terms,
                    ModifiedDate = model.ModifiedDate,
                    ShippingAddresses = new Collection<AddressViewModel>(model.ShippingAddresses.Select(AddressViewModelMap.ToModel).ToList())
                };

                return eRPAccountModel;
            }

            return new PRFTERPAccountDetailsViewModel();
        }
    }
}