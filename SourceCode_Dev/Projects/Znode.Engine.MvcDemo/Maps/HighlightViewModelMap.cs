﻿using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.Helpers;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Maps
{
    /// <summary>
    /// Highlight View Model Map
    /// </summary>
    public class HighlightViewModelMap
    {
        /// <summary>
        /// Convert IEnumerable<HighlightModel> to HighlightsListViewModel.
        /// </summary>
        /// <param name="models">To convert to list view model.</param>
        /// <returns>Returns HighlightsListViewModel.</returns>
        public static HighlightListViewModel ToListViewModels(HighlightListModel models)
        {
            if (!Equals(models, null) && (!Equals(models.Highlights, null)))
            {
                HighlightListViewModel viewModel = new HighlightListViewModel()
                {
                    Highlights = models.Highlights.ToList().Select(
                    model => new HighlightViewModel()
                    {
                        ImageAltTag = model.ImageAltTag,
                        ImageFile = model.ImageFile,
                        HighlightTypeId = model.HighlightTypeId,
                        HighlightId = model.HighlightId,
                        Name = model.Name,
                        Description = model.Description,
                        ProductHighlightID = model.ProductHighlightID,
                        HighlightTypeName = string.IsNullOrEmpty(model.HighlightTypeName) ? string.Empty : model.HighlightTypeName,
                        ImageLargePath = HelperMethods.GetImagePath(model.ImageLargePath),
                        ImageMediumPath = HelperMethods.GetImagePath(model.ImageMediumPath),
                        ImageSmallThumbnailPath = HelperMethods.GetImagePath(model.ImageSmallPath),
                        Hyperlink = model.Hyperlink,
                        ShowPopup = model.DisplayPopup
                    }).ToList()
                };
               
                return viewModel;
            }
            else
            {
                return new HighlightListViewModel();
            }
        }
    }
}