﻿using System.Collections.Generic;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.ViewModels;
using Znode.Engine.MvcDemo.Helpers;

namespace Znode.Engine.MvcDemo.Maps
{
    /// <summary>
    /// This is the Mapper class to Map the entities of one type to another. This class is specially used for mapping pf the Categories
    /// </summary>
    public static class CategoryListViewModelMap
    {
        /// <summary>
        /// This method is responsible to map the list of category model to CategoryListViewModel
        /// </summary>
        /// <param name="models">List of CategoryModel</param>
        /// <param name="catalogId">int Catalog Id</param>
        /// <returns>return the object of type CategoryListViewModel</returns>
        public static CategoryListViewModel ToViewModel(IEnumerable<CategoryModel> models, int catalogId)
        {
            CategoryListViewModel viewModel = new CategoryListViewModel()
                {
                    Categories = models.Where(p => p.CategoryNodes.Any(y => Equals(y.ParentCategoryNodeId , null) && y.IsActive && Equals(y.CatalogId , catalogId))).ToList().Select(x =>
                           new CategoryHeaderViewModel()
                           {
                               CategoryId = x.CategoryId,
                               CategoryName = x.Name,
                               SEOPageName = x.SeoUrl,
                               DisplayOrder = x.CategoryNodes.ToList().Find(c => Equals(c.CatalogId , catalogId)).DisplayOrder,
                               SubCategoryItems = ToChildMenuItemViewModel(models, catalogId, x.CategoryNodes[0].CategoryNodeId),
                               ProductCount=x.ProductCount,
                               ActiveProductCount=x.ActiveProductCount,

                               //PRFT Custom Code: Start
                               ImageFile=x.ImageFile,
                               ImageAltTag=x.ImageAltTag,
                               ImageMediumPath=HelperMethods.GetImagePath(x.ImageMediumPath),
                               Custom1 = x.Custom1
                               //PRFT Custom Code: End
                           }).OrderBy(nodeorder => nodeorder.DisplayOrder).ThenBy(name => name.CategoryName).ToList()
                };

            return viewModel;
        }

        /// <summary>
        /// This method is used to add the Child Menus to the sub menus.
        /// </summary>
        /// <param name="models">List of CategoryModel</param>
        /// <param name="catalogId">int Catalog Id</param>
        /// <param name="parentCategoryId">Int Parent Category Id</param>
        /// <returns>returns the list of CategorySubHeaderViewModel</returns>
        private static List<CategorySubHeaderViewModel> ToChildMenuItemViewModel(IEnumerable<CategoryModel> models, int catalogId, int? parentCategoryId)
        {
            List<CategorySubHeaderViewModel> list = null;
            if (!Equals(parentCategoryId, null))
            {
                list = models.Where(p => p.CategoryNodes.Any(y => y.IsActive && Equals(y.ParentCategoryNodeId, parentCategoryId) && Equals(y.CatalogId, catalogId))).ToList().Select(x =>
                    new CategorySubHeaderViewModel()
                    {
                        CategoryId = x.CategoryId,
                        CategoryName = x.Name,
                        SEOPageName = x.SeoUrl,
                        ChildCategoryItems = ToChildMenuItemViewModel(models, catalogId, x.CategoryNodes[0].CategoryNodeId)
                    }).ToList();

            }
            return list;
        }
    }
}