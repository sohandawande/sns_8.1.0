﻿using System.Globalization;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.Agents;
using Znode.Engine.MvcDemo.Helpers;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Maps
{
    public static class PortalViewModelMap
    {
        public static PortalViewModel ToModel(PortalModel model)
        {
            if (!Equals(model, null))
            {
                return new PortalViewModel()
                    {
                        PortalId = model.PortalId,
                        Name = model.StoreName,
                        PersistentCartEnabled = model.PersistentCartEnabled.GetValueOrDefault(false),
                        CatalogId = model.Catalogs.First().CatalogId,
                        DefaultAnonymousProfileId = model.DefaultAnonymousProfileId.GetValueOrDefault(0),
                        MaxRecentViewItemToDisplay = model.MaxCatalogCategoryDisplayThumbnails,
                        Theme = model.ThemeName,
                        CustomerServiceEmail = model.CustomerServiceEmail,
                        CustomerServicePhoneNumber = model.CustomerServicePhoneNumber,
                        IsMultipleCouponAllowed = model.IsMultipleCouponAllowed,
                        Css = model.CssName,
                        IsEnableCompare = Equals(model.IsEnableCompare, null) ? false : model.IsEnableCompare.Value,
                        CompareType = model.CompareType,
                        IsEnableSinglePageCheckout = Equals(model.IsEnableSinglePageCheckout, null) ? false : model.IsEnableSinglePageCheckout.Value,
                        EnableAddressValidation = Equals(model.EnableAddressValidation, null) ? false : model.EnableAddressValidation,
                        RequireValidatedAddress = Equals(model.RequireValidatedAddress, null) ? false : model.RequireValidatedAddress,
                        CurrencyTypeId = model.CurrencyTypeId,
                        MaxCatalogCategoryDisplayThumbnails = model.MaxCatalogCategoryDisplayThumbnails,
                        MaxCatalogDisplayColumns = model.MaxCatalogDisplayColumns,
                        MaxCatalogDisplayItems = model.MaxCatalogDisplayItems,
                        LogoPath = HelperMethods.GetImagePath(model.LogoPath),
                        CurrencySuffix = (!Equals(model.CurrencyTypeModel, null) && !string.IsNullOrEmpty(model.CurrencyTypeModel.CurrencySuffix)) ? model.CurrencyTypeModel.CurrencySuffix : string.Empty,
                        CurrencyName = (!Equals(model.CurrencyTypeModel, null) && !string.IsNullOrEmpty(model.CurrencyTypeModel.Name)) ? model.CurrencyTypeModel.Name : string.Empty,
                        ImageNotAvailablePath = HelperMethods.GetImagePath(model.ImageNotAvailablePath),
                        SiteWideTopJavascript=model.SiteWideTopJavascript,
                        UseSsl = model.UseSsl
                };
            }
            else
            {
                return new PortalViewModel();
            }
        }
    }
}