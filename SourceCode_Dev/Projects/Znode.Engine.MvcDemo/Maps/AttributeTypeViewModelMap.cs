﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Maps
{
    public static class AttributeTypeViewModelMap
    {

        public static AttributeTypeViewModel ToEntity(AttributeTypeModel model)
        {
            if (model != null)
            {
                var viewModel = new AttributeTypeViewModel()
                    {
                        AttributeTypeId = model.AttributeTypeId,
                        Name = model.Name,
                       
                    };

                return viewModel;
            }
            return null;
        }
    }
}