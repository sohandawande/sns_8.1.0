﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Maps
{
    public class PRFTCreditApplicationViewModelMap
    {
        public static PRFTCreditApplicationModel ToModel(PRFTCreditApplicationViewModel viewmodel)
        {
            if (viewmodel == null)
            {
                return null;
            }

            var model = new PRFTCreditApplicationModel
            {
                CreditApplicationID = viewmodel.CreditApplicationID,
                BusinessName = viewmodel.BusinessName,
                BusinessStreet = viewmodel.BusinessStreet,
                BusinessStreet1 = viewmodel.BusinessStreet1,
                BusinessCity = viewmodel.BusinessCity,
                BusinessStateCode = viewmodel.BusinessStateCode,
                BusinessPostalCode = viewmodel.BusinessPostalCode,
                BusinessCountryCode = viewmodel.BusinessCountryCode,
                BusinessPhoneNumber = viewmodel.BusinessPhoneNumber,
                BusinessFaxNumber = viewmodel.BusinessFaxNumber,
                BusinessEmail = viewmodel.BusinessEmail,
                BusinessYearEstablished = viewmodel.BusinessYearEstablished,
                BusinessType = viewmodel.BusinessType,
                IsTaxExempt = viewmodel.IsTaxExempt,
                TaxExemptCertificate = viewmodel.TaxExemptCertificate,
                BillingStreet = viewmodel.BillingStreet,
                BillingStreet1 = viewmodel.BillingStreet1,
                BillingCity = viewmodel.BillingCity,
                BillingStateCode = viewmodel.BillingStateCode,
                BillingPostalCode = viewmodel.BillingPostalCode,
                BillingCountryCode = viewmodel.BillingCountryCode,
                BankName = viewmodel.BankName,
                BankAddress = viewmodel.BankAddress,
                BankPhoneNumber = viewmodel.BankPhoneNumber,
                BankAccountNumber = viewmodel.BankAccountNumber,
                RequestDate = viewmodel.RequestDate,
                IsCreditApproved = viewmodel.IsCreditApproved,
                CreditDays = viewmodel.CreditDays,
                CreditApprovedDate = viewmodel.CreditApprovedDate,
                Custom1 = viewmodel.Custom1,
                Custom2 = viewmodel.Custom2,
                Custom3 = viewmodel.Custom3,
                Custom4 = viewmodel.Custom4,
                Custom5 = viewmodel.Custom5
            };

            foreach (var item in viewmodel.TradeReferences)
            {
                model.TradeReferences.Add(PRFTTradeReferencesViewModelMap.ToModel(item));
            }

            return model;
        }
    }
}