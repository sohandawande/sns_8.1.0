﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Maps
{
    public class StoreLocatorViewModelMap
    {
        /// <summary>
        /// Map the collection of store model to store locator view model
        /// </summary>
        /// <param name="model">Collection of StoreModel</param>
        /// <returns>Collection of StoreLocatorViewModel</returns>
        public static Collection<StoreLocatorViewModel> ToListViewModel(Collection<StoreModel> model)
        {
            Collection<StoreLocatorViewModel> list = new Collection<StoreLocatorViewModel>();
            if (!Equals(model, null))
            {
                foreach (StoreModel store in model)
                {
                    list.Add(ToViewModel(store));
                }
            }
            return list;
        }

        /// <summary>
        /// Map store model to store locator view model
        /// </summary>
        /// <param name="viewModel">View Model of StoreLocator</param>
        /// <returns>Model of Store</returns>
        public static StoreModel ToModel(StoreLocatorViewModel viewModel)
        {
            if (!Equals(viewModel, null))
            {
                return new StoreModel()
                {
                    Zip = viewModel.ZipCode,
                    City = Equals(viewModel.City,null)?string.Empty:viewModel.City,
                    State = Equals(viewModel.State, null) ? string.Empty : viewModel.State,
                    Radius = viewModel.Radius,
                    AreaCode = Equals(viewModel.AreaCode, null) ? string.Empty : viewModel.AreaCode,
                };
            }
            else
            {
                return new StoreModel();
            }
        }

        /// <summary>
        /// Map store locator view model to store model
        /// </summary>
        /// <param name="model">Model of store </param>
        /// <returns>VIew model of store locator</returns>
        public static StoreLocatorViewModel ToViewModel(StoreModel model)
        {
            if (!Equals(model, null))
            {
                return new StoreLocatorViewModel()
                {
                    ZipCode = model.Zip,
                    City = model.City,
                    State = model.State,
                    Name = model.ContactName,
                    Address1 = model.Address1,
                    Address2 = model.Address2,
                    Address3 = model.Address3,
                    Phone = model.Phone,
                    Fax = model.Fax,
                    MapQuestURL = model.MapQuestURL,
                    Custom1 = model.Custom1,
                    Custom2 = model.Custom2,
                    Custom3 = model.Custom3
                };
            }
            else
            {
                return new StoreLocatorViewModel();
            }
        }

        /// <summary>
        /// Bind Lst of distances to dropdown
        /// </summary>
        /// <returns>List of distances in SelectListItem type</returns>
        public static List<SelectListItem> GetDistances()
        {
            List<SelectListItem> statusList = new List<SelectListItem>();
            statusList = (from item in GetDistanceDictionary()
                          select new SelectListItem
                          {
                              Text = item.Value,
                              Value = item.Key,
                              Selected = Equals(item.Key, 0),
                          }).ToList();

            return statusList;
        }

        /// <summary>
        ///Dropdown for Distances
        /// </summary>
        /// <returns>List of distances in key value pair</returns>
        private static Dictionary<string, string> GetDistanceDictionary()
        {
            Dictionary<string, string> Distance = new Dictionary<string, string>();
            Distance.Add("0", "Distance from Zip");
            Distance.Add("5", "5 Miles");
            Distance.Add("10", "10 Miles");
            Distance.Add("25", "25 Miles");
            Distance.Add("50", "50 Miles");
            Distance.Add("75", "75 Miles");
            Distance.Add("100", "100 Miles");
            return Distance;
        }
    }
}