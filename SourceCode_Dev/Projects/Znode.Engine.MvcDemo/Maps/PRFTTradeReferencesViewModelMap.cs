﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Maps
{
    public class PRFTTradeReferencesViewModelMap
    {
        public static PRFTTradeReferencesModel ToModel(PRFTTradeReferencesViewModel viewmodel)
        {
            if (viewmodel == null)
            {
                return null;
            }

            var model = new PRFTTradeReferencesModel
            {
                TradeRefID = viewmodel.TradeRefID,
                CreditApplicationID = viewmodel.CreditApplicationID,
                ReferenceName = viewmodel.ReferenceName,
                BusinessName = viewmodel.BusinessName,
                Street = viewmodel.Street,
                Street1 = viewmodel.Street1,
                City = viewmodel.City,
                StateCode = viewmodel.StateCode,
                CountryCode = viewmodel.CountryCode,
                PostalCode = viewmodel.PostalCode,
                PhoneNumber = viewmodel.PhoneNumber,
                FaxNumber = viewmodel.FaxNumber,
                Custom1 = viewmodel.Custom1,
                Custom2 = viewmodel.Custom2,
                Custom3 = viewmodel.Custom3,
                Custom4 = viewmodel.Custom4,
                Custom5 = viewmodel.Custom5
            };

            return model;
        }
    }
}