﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.Helpers;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Maps
{
    public class CategoryViewModelMap
    {
        public static CategoryViewModel ToCategoryViewModel(CategoryModel searchCategoryModel)
        {
            var category = new CategoryViewModel()
            {
                CategoryId = searchCategoryModel.CategoryId,
                Name = searchCategoryModel.Name,
                Title = searchCategoryModel.Title,
                ShortDescription = searchCategoryModel.ShortDescription,
                SeoTitle = searchCategoryModel.SeoTitle,
                SeoUrl = searchCategoryModel.SeoUrl,
                SeoKeywords = searchCategoryModel.SeoKeywords,
                SeoDescription = searchCategoryModel.SeoDescription,
                Subcategories = PopulateCategory(searchCategoryModel.Subcategories),
                //Znode version 7.2.2 To map Category Banner
                CategoryBanner = searchCategoryModel.CategoryBanner,
                ImageAltTag = searchCategoryModel.ImageAltTag,
                ImageFile = HelperMethods.GetImagePath(searchCategoryModel.ImageFile),
                ImageLargePath = HelperMethods.GetImagePath(searchCategoryModel.ImageLargePath),
                ImageMediumPath = HelperMethods.GetImagePath(searchCategoryModel.ImageMediumPath),
                ImageSmallPath = HelperMethods.GetImagePath(searchCategoryModel.ImageSmallPath),
                ImageSmallThumbnailPath = HelperMethods.GetImagePath(searchCategoryModel.ImageSmallThumbnailPath),
                ImageThumbnailPath = HelperMethods.GetImagePath(searchCategoryModel.ImageThumbnailPath)
            };

            return category;
        }

        private static Collection<CategoryViewModel> PopulateCategory(Collection<CategoryModel> categoryModel)
        {
            if (categoryModel != null && categoryModel.Any())
            {
                var categoryViewList = categoryModel.Select(x => new CategoryViewModel()
                {
                    Name = x.Name,
                    Title = x.Title,
                    CategoryId = x.CategoryId,
                    SeoTitle = x.SeoTitle,
                    SeoUrl = x.SeoUrl,
                    ImageAltTag = x.ImageAltTag,
                    ImageFile = HelperMethods.GetImagePath(x.ImageFile),
                    ImageLargePath = HelperMethods.GetImagePath(x.ImageLargePath),
                    ImageMediumPath = HelperMethods.GetImagePath(x.ImageMediumPath),
                    ImageSmallPath = HelperMethods.GetImagePath(x.ImageSmallPath),
                    ImageSmallThumbnailPath = HelperMethods.GetImagePath(x.ImageSmallThumbnailPath),
                    ImageThumbnailPath = HelperMethods.GetImagePath(x.ImageThumbnailPath),
                    Subcategories = PopulateCategory(x.Subcategories),
                    ShortDescription = x.ShortDescription
                }).ToList();

                if (categoryViewList.Any())
                    return new Collection<CategoryViewModel>(categoryViewList);
            }

            return new Collection<CategoryViewModel>();
        }
    }
}