﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Maps
{
    public partial class AccountViewModelMap
    {
        public static AccountModel ToRegisterSubUserModel(PRFTRegisterSubUserViewModel model)
        {
            var signUpModel = new AccountModel()
            {
                User = new UserModel()
                {
                    Username = model.UserName,
                    Password = model.Password,
                    Email = model.EmailAddress,
                    IsApproved = true,
                    ParentAccountId = model.SuperUserAccount.AccountId, //PRFT Custom Code
                },
                IsActive = true,
                EmailOptIn = model.EmailOptIn

            };
            return signUpModel;           
        }
        
        public static PRFTCustomerListViewModel ToListViewModel(IEnumerable<CustomerModel> models, int? totalResults = 0, int? recordPerPage = 0, int? pageIndex = 0, int? totalPages = 0)
        {
            if (!Equals(models, null) && models.Count() > 0)
            {
                var viewModel = new PRFTCustomerListViewModel()
                {
                    Customers = models.ToList().Select(
                    x => new PRFTCustomerViewModel()
                    {
                        Address = new AddressViewModel()
                        {
                            FirstName = x.FirstName,
                            LastName = x.LastName,
                            AccountId = x.AccountId,
                            Email = x.Email,
                            City = x.City,
                            PostalCode = x.PostalCode,
                            StateCode = x.StateCode
                        },
                        PhoneNumber = x.PhoneNumber,
                        FullName = x.FullName,
                        AccountId = x.AccountId,
                        CompanyName = x.CompanyName,
                        LoginName = x.UserName,
                        IsConfirmed = x.IsConfirmed,
                        CustomerAccount = new AccountViewModel()
                        {
                            ExternalId = (x.ExternalAccountNo),
                        },
                        EnableCustomerPricing = Equals(x.EnableCustomerPricing, null) ? false : x.EnableCustomerPricing,
                        Custom1 = x.Custom1,
                        Custom2 = x.Custom2,
                        //Custom3=x.Custom3 //PRFT Custom Code
                        CustomerUserMappingId = x.CustomerUserMappingId,
                        ExternalId=x.ExternalAccountNo,
                    }).ToList()
                };

                //viewModel.RecordPerPage = recordPerPage ?? 10;
                //viewModel.Page = pageIndex ?? 1;
                //viewModel.TotalPages = Convert.ToInt32(totalPages);
                //viewModel.TotalResults = (Equals(totalResults, 0)) ? Convert.ToInt32(viewModel.TotalResults) : Convert.ToInt32(totalResults);
                return viewModel;
            }
            return new PRFTCustomerListViewModel();
        }
    }
}