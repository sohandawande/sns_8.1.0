﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Maps
{
    public class PRFTERPOrderViewModelMap
    {
        public static PRFTOEHeaderHistoryViewModel ToERPOEHeaderViewModel(PRFTOEHeaderHistoryModel model)
        {
            if (model != null)
            {
                decimal salesTax = Convert.ToDecimal(model.SalesTaxAmount1) + Convert.ToDecimal(model.SalesTaxAmount2) + Convert.ToDecimal(model.SalesTaxAmount3);
                decimal subTotal = Convert.ToDecimal(model.TotalSaleAmt) + Convert.ToDecimal(model.MiscellaneousAmount) + Convert.ToDecimal(model.FreightAmount) + salesTax;
                decimal totalAmountDue = subTotal - Convert.ToDecimal(model.PaymentAmount) - Convert.ToDecimal(model.PaymentDiscountAmount);
                var eRPOEHeaderModel = new PRFTOEHeaderHistoryViewModel()
                {                    
                    InvoiceNumber = model.InvoiceNumber,
                    InvoiceDate = model.InvoiceDate,
                    BillToCustomerName = model.BillToCustomerName,
                    BillToAddressLine1 = model.BillToAddressLine1,
                    BillToAddressLine2 = model.BillToAddressLine2,
                    BillToAddressLine3 = model.BillToAddressLine3,
                    BillToAddressLine4 = model.BillToAddressLine4,
                    BillToCity = model.BillToCity,
                    BillToState = model.BillToState,
                    BillToZip = model.BillToZip,
                    ShipToName = model.ShipToName,
                    ShipToAddressLine1 = model.ShipToAddressLine1,
                    ShipToAddressLine2 = model.ShipToAddressLine2,
                    ShipToAddressLine3 = model.ShipToAddressLine3,
                    ShipToAddressLine4 = model.ShipToAddressLine4,
                    ShipToCity = model.ShipToCity,
                    ShipToState = model.ShipToState,
                    ShipToZip = model.ShipToZip,
                    ShipToCountry = model.ShipToCountry,
                    OrderNumber = model.OrderNumber,
                    OrderDate = model.OrderDate,
                    CustomerNumber = model.CustomerNumber,
                    ManufacturingLocation = model.ManufacturingLocation,
                    SalesPersonNumber = model.SalesPersonNumber,
                    PurchaseOrderNumber = model.PurchaseOrderNumber,
                    JobNumber = model.JobNumber,
                    ShipViaCode = model.ShipViaCode,
                    FreightPaymentCode = model.FreightPaymentCode,
                    TotalSaleAmt = Convert.ToDecimal(model.TotalSaleAmt),
                    MiscellaneousAmount = Convert.ToDecimal(model.MiscellaneousAmount),
                    FreightAmount = Convert.ToDecimal(model.FreightAmount),
                    SalesTaxAmount1 = model.SalesTaxAmount1,
                    SalesTaxAmount2 = model.SalesTaxAmount2,
                    SalesTaxAmount3 = model.SalesTaxAmount3,
                    PaymentAmount = Convert.ToDecimal(model.PaymentAmount),
                    PaymentDiscountAmount = Convert.ToDecimal(model.PaymentDiscountAmount),
                    PaymentTerms=model.PaymentTerms,
                    Comments=model.Comments,
                    SalesTax = salesTax,
                    SubTotal = subTotal,
                    TotalAmountDue = totalAmountDue
                };
                foreach (var item in model.OELineHistoryLists)
                {
                    eRPOEHeaderModel.OELineHistoryLists.Add(ToERPOELineHistoryViewModel(item));
                }
                return eRPOEHeaderModel;
            }

            return null;

        }

        public static PRFTOELineHistoryViewModel ToERPOELineHistoryViewModel(PRFTOELineHistoryModel model)
        {
            if (model == null)
            {
                return null;
            }            

            var erpOELineHistoryViewModel = new PRFTOELineHistoryViewModel
            {
                ItemNo = model.ItemNo,
                ItemDescription1 = model.ItemDescription1,
                ItemDescription2 = model.ItemDescription2,
                QuantityOrdered = Convert.ToInt32(Convert.ToDecimal(model.QuantityOrdered)),
                QuantityToShip = Convert.ToInt32(Convert.ToDecimal(model.QuantityToShip)),
                QuantityReturnedtoStock = Convert.ToInt32(Convert.ToDecimal(model.QuantityReturnedtoStock)),
                QuantityBackordered = Convert.ToInt32(Convert.ToDecimal(model.QuantityBackordered)),
                UnitPrice = Convert.ToDecimal(model.UnitPrice),
                DiscountPercent = Convert.ToDecimal(model.DiscountPercent),
                UnitOfMeasure = model.UnitOfMeasure,
                SalesAmount = Convert.ToDecimal(model.SalesAmount)
            };
            return erpOELineHistoryViewModel;
        }
    }
}