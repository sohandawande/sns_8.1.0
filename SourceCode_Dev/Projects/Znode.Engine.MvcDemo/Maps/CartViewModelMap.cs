﻿using System.Configuration;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.Agents;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Maps
{
    public static class CartViewModelMap
    {
        public static CartViewModel ToViewModel(ShoppingCartModel model)
        {
            var viewModel = new CartViewModel
            {
                GiftCardApplied = model.GiftCardApplied,
                GiftCardAmount = model.GiftCardAmount,
                GiftCardMessage = model.GiftCardMessage,
                GiftCardNumber = model.GiftCardNumber,
                GiftCardValid = model.GiftCardValid,
                GiftCardBalance = model.GiftCardBalance,
                CookieId = model.CookieId,
                SubTotal = model.SubTotal,
                TaxCost = model.TaxCost,
                Discount = model.Discount,
                TaxRate = model.TaxRate,
                Total = model.Total,
                Vat = model.Vat,
                SalesTax=model.SalesTax,//PRFT custom code
                AdditionalInstructions = model.AdditionalInstructions, //PRFT custom code
            };

            foreach (var item in model.ShoppingCartItems)
            {
                viewModel.Items.Add(CartItemViewModelMap.ToViewModel(item));
            }

            foreach (CouponModel coupon in model.Coupons)
            {
                viewModel.Coupons.Add(CouponViewModelMap.ToViewModel(coupon));
            }

            viewModel.Count = model.ShoppingCartItems.Sum(x => x.Quantity);

            viewModel.ShippingCost = model.ShoppingCartItems.Sum(itemModel => itemModel.ShippingCost) +
                                     model.OrderLevelShipping - model.Shipping.ShippingDiscount;

            return viewModel;
        }

        public static ShoppingCartModel ToModel(CartViewModel viewmodel)
        {
            var Model = new ShoppingCartModel
            {
                CookieId = viewmodel.CookieId,
                SubTotal = viewmodel.SubTotal,
                TaxCost = viewmodel.TaxCost,
                Discount = viewmodel.Discount,
                TaxRate = viewmodel.TaxRate,
                Total = viewmodel.Total,
                Vat = viewmodel.Vat
            };

            foreach (var item in viewmodel.Items)
            {
                Model.ShoppingCartItems.Add(CartItemViewModelMap.ToModel(item));
            }

            return Model;
        }

        public static ShoppingCartModel ToShoppingCartModel(AccountModel account, ShoppingCartModel shoppingCart, string purchaseOrderNumber)
        {            
            var model = new ShoppingCartModel
            {
                Account = account,
                GiftCardApplied = shoppingCart.GiftCardApplied,
                GiftCardAmount = shoppingCart.GiftCardAmount,
                GiftCardBalance = shoppingCart.GiftCardBalance,
                GiftCardMessage = shoppingCart.GiftCardMessage,
                GiftCardNumber = shoppingCart.GiftCardNumber,
                GiftCardValid = shoppingCart.GiftCardValid,
                Gst = 0,
                Hst = 0,
                Pst = 0,
                Vat = 0,
                SalesTax = shoppingCart.SalesTax,
                TaxCost = shoppingCart.TaxCost,
                SubTotal = shoppingCart.SubTotal,
                Total = shoppingCart.Total,
                PurchaseOrderNumber = purchaseOrderNumber,
                ShoppingCartItems = shoppingCart.ShoppingCartItems,
                ShippingAddress = shoppingCart.ShippingAddress,
                Payment = shoppingCart.Payment,
                Shipping = shoppingCart.Shipping,
                AdditionalInstructions = shoppingCart.AdditionalInstructions,
                ReturnUrl = shoppingCart.ReturnUrl,
                CancelUrl = shoppingCart.CancelUrl,
                CookieId = shoppingCart.CookieId,
                Token = shoppingCart.Token,
                Payerid = shoppingCart.Payerid,
                OrderShipment = shoppingCart.OrderShipment,
                CurrentURL = HttpContext.Current.Request.Url.Host,
                CustomerExternalAccountId = HttpContext.Current.Session["AssociatedCustomerExternalId"]!=null ? HttpContext.Current.Session["AssociatedCustomerExternalId"].ToString() : ConfigurationManager.AppSettings["GenericUserExternalID"].ToString(),
            };

            foreach (CouponModel coupon in shoppingCart.Coupons)
            {
                model.Coupons.Add(coupon);
            }
            return model;
        }
    }
}