﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.Agents;
using Znode.Engine.MvcDemo.Helpers;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Maps
{
    public static class ProductViewModelMap
    {
        #region Constant Variables
        private const string _OriginalPriceTextinLowercase = "originalprice";
        #endregion

        /// <summary>
        /// This function will convert entity to view model for products.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static ProductViewModel ToViewModel(ProductModel model)
        {
            if (!Equals(model, null))
            {
                var viewModel = new ProductViewModel()
                {
                    BundleItemsIds = model.BundledProductIds,
                    Description = string.IsNullOrEmpty(model.Description) ? String.Empty : HttpUtility.HtmlDecode(model.Description),
                    IsCallForPricing = model.CallForPricing,
                    ImageAltTag = model.ImageAltTag,
                    ImageFile = model.ImageFile,
                    ImageSmallPath = HelperMethods.GetImagePath(model.ImageSmallPath),
                    ImageLargePath = HelperMethods.GetImagePath(model.ImageLargePath),
                    ImageMediumPath=HelperMethods.GetImagePath(model.ImageMediumPath), //PRFT Custom Code
                    ImageSmallThumbnailPath=HelperMethods.GetImagePath(model.ImageSmallThumbnailPath),  //PRFT Custom Code
                    MaxQuantity = model.MaxSelectableQuantity.GetValueOrDefault(10),
                    MinQuantity = model.MinSelectableQuantity.GetValueOrDefault(1),
                    Name = model.Name,
                    OutOfStockMessage = model.OutOfStockMessage,
                    Price = GetProductPrice(model, "originalprice"),
                    ProductPrice = GetProductPrice(model, "price"),
                    OriginalPrice = GetProductPrice(model, "originalprice"),
                    ProductId = model.ProductId,
                    ProductNumber = model.ProductNumber,
                    SeoDescription = string.IsNullOrEmpty(model.SeoDescription) ? String.Empty : HttpUtility.HtmlDecode(model.SeoDescription),
                    SeoKeywords = model.SeoKeywords,
                    SeoTitle = model.SeoTitle,
                    SeoPageName = model.SeoUrl,
                    ShortDescription = string.IsNullOrEmpty(model.ShortDescription) ? String.Empty : HttpUtility.HtmlDecode(model.ShortDescription),
                    IsActive = model.IsActive,
                    AllowBackOrder = model.AllowBackOrder,
                    TrackInventory = model.TrackInventory,
                    BackOrderMessage = model.BackOrderMessage,
                    InStockMessage = model.InStockMessage,
                    Images = ToImageViewModel(model.Images),
                   
                    ProductSpecification = string.IsNullOrEmpty(model.Specifications) ? String.Empty : HttpUtility.HtmlDecode(model.Specifications),
                    RecurringBillingFrequency = model.RecurringBillingFrequency,
                    RecurringBillingInd = model.AllowRecurringBilling,
                    RecurringBillingInitialAmount = model.RecurringBillingInitialAmount,
                    FeaturesDescription = model.FeaturesDescription,
                    Width = model.Width,
                    Length = model.Length,
                    Weight = model.Weight,
                    Height = model.Height,
                    Manufacturer = !Equals(model.Manufacturer, null) ? model.Manufacturer.Name : string.Empty,
                    IsNewProduct = Equals(model.IsNewProduct, null) ? false : model.IsNewProduct.GetValueOrDefault(),
                    IsFeatured = model.IsFeatured,
                    DownloadLink=model.DownloadLink
                };

                //PRFT Commented Code : Start ( For displaying only product image on PLP)
                //if (!Equals(model, null) && !Equals(model.Skus, null) && model.Skus.Any())
                //{
                //    viewModel.ImageMediumPath = GetDisplayImage(model, model.Skus.FirstOrDefault().ImageMediumPath, model.ImageMediumPath);
                //    viewModel.ImageSmallThumbnailPath = GetDisplayImage(model, model.Skus.FirstOrDefault().ImageSmallThumbnailPath, model.ImageSmallThumbnailPath);
                //}
                //PRFT Custom Code : End

                if (model.Skus.Count > 0)
                {
                    viewModel.Sku = model.Skus.FirstOrDefault().Sku;
                }

                viewModel.ProductAttributes = ProductAttributeViewModelMap.ToViewModel(model.Attributes, model);

                if (model.AddOns != null && model.AddOns.Count > 0)
                {
                    viewModel.AddOns = model.AddOns.Select(AddOnViewModelMap.ToViewModel).Where(x => x != null).ToList();
                }

                if (model.Reviews.Any())
                {
                    var reviewModel = model.Reviews.Where(x => x.Status == MvcDemoConstants.ApprovedReviewStatus).OrderByDescending(x => x.CreateDate);
                    viewModel.Rating = reviewModel.Any() ? Convert.ToInt16(reviewModel.Average(x => x.Rating)) : 0;
                    viewModel.ProductReviews = AccountViewModelMap.ToReviewViewModel(new Collection<ReviewModel>(reviewModel.ToList()));
                }
                else
                {
                    viewModel.Rating = 0;
                    viewModel.ProductReviews = null;
                }

                if (model.FrequentlyBoughtTogether.Any())
                {
                    viewModel.FBTProductsIds = string.Join(",", model.FrequentlyBoughtTogether.Select(fbt => fbt.RelatedProductId.ToString()));

                }
                if (model.YouMayAlsoLike.Any())
                {
                    viewModel.YMALProductsIds = string.Join(",", model.YouMayAlsoLike.Select(ymal => ymal.RelatedProductId.ToString()));
                }


                return viewModel;
            }
            else
            {
                return new ProductViewModel();
            }

        }

        /// <summary>
        /// Convert multiple Product models to list of product view models
        /// </summary>
        /// <param name="models"></param>
        /// <returns></returns>
        public static List<ProductViewModel> ToViewModels(Collection<ProductModel> models)
        {
            if (models == null)
            {
                return null;
            }
            //Znode 7.2.2
            //Showing Revies and Ratings on Home page-Starts
            var list = new List<ProductViewModel>();

            foreach (ProductModel model in models)
            {
                list.Add(ToViewModel(model));
            }
            //Showing Revies and Ratings on Home page-Ends
            return list;
        }

        public static Collection<ProductViewModel> ToCollectionViewModels(Collection<ProductModel> models)
        {
            if (Equals(models, null))
            {
                return null;
            }
            //Znode 7.2.2
            //Showing Revies and Ratings on Home page-Starts
            var list = new Collection<ProductViewModel>();

            foreach (ProductModel model in models)
            {
                list.Add(ToViewModel(model));
            }
            //Showing Revies and Ratings on Home page-Ends
            return list;
        }

        /// <summary>
        /// Convert list cross sell items model to list of cross sell items view model
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static Collection<CrossSellViewModel> ToCrossSellViewModel(Collection<CrossSellModel> model)
        {
            var crossSellList =
                new Collection<CrossSellViewModel>(model.Select(x => new CrossSellViewModel()
                {
                    DisplayOrder = x.DisplayOrder,
                    ProductCrossSellTypeId = x.ProductCrossSellTypeId,
                    ProductId = x.ProductId,
                    RelatedProductId = x.RelatedProductId,
                    RelationTypeId = x.RelationTypeId,
                    ProductRelation = (int)x.ProductRelation,
                }).ToList());

            return crossSellList;
        }

        public static CompareProductModel ToModel(CompareProductViewModel viewModel)
        {
            if (!Equals(viewModel, null))
            {
                return new CompareProductModel()
                {
                    SenderEmailAddress = viewModel.SenderEmailAddress,
                    RecieverEmailAddress = viewModel.RecieverEmailAddress,
                    ProductIds = viewModel.ProductIds,
                    BaseUrl = viewModel.BaseUrl,
                    PortalId = viewModel.PortalId,
                    ProductId = viewModel.ProductId,
                    IsProductDetails = viewModel.IsProductDetails

                };
            }
            else
            {
                return new CompareProductModel();
            }
        }

        /// <summary>
        /// Convert list of image model to list of image view model
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private static List<ImageViewModel> ToImageViewModel(Collection<ImageModel> model)
        {
            var imageViewModel = model.Select(x => new ImageViewModel()
                {
                    AlternateThumbnailImageFile = x.AlternateThumbnailImageFile,
                    DisplayOrder = x.DisplayOrder,
                    ImageAltTag = x.ImageAltTag,
                    ImageFile = x.ImageFile,
                    ImageLargePath = HelperMethods.GetImagePath(x.ImageLargePath),
                    ImageMediumPath = HelperMethods.GetImagePath(x.ImageMediumPath),
                    ImageSmallPath = HelperMethods.GetImagePath(x.ImageSmallPath),
                    ImageSmallThumbnailPath = HelperMethods.GetImagePath(x.ImageSmallThumbnailPath),
                    ImageThumbnailPath = HelperMethods.GetImagePath(x.ImageThumbnailPath),
                    IsActive = x.IsActive,
                    Name = x.Name,
                    ProductId = x.ProductId,
                    ProductImageId = x.ProductImageId,
                    ProductImageTypeId = x.ProductImageTypeId,
                    ReviewStateId = x.ReviewStateId,
                    ShowOnCategoryPage = x.ShowOnCategoryPage
                }).ToList();

            return imageViewModel;
        }

        /// <summary>
        /// Get the product price based on product model and price type
        /// </summary>
        /// <param name="model">ProductModel model</param>
        /// <param name="priceType">string price type</param>
        /// <returns>decimal product price</returns>
        private static decimal GetProductPrice(ProductModel model, string priceType)
        {
            decimal finalPrice = 0.00m;

            if (priceType.ToLower().Equals(_OriginalPriceTextinLowercase))
            {
                finalPrice = model.RetailPrice.HasValue ? model.RetailPrice.Value : 0;
                return finalPrice;
            }

            if (model.PromotionPrice.HasValue)
            {
                finalPrice = model.PromotionPrice.Value;
                return finalPrice;
            }

            if (model.SalePrice.HasValue)
            {
                finalPrice = model.SalePrice.Value;
                return finalPrice;
            }

            finalPrice = model.RetailPrice.HasValue ? model.RetailPrice.Value : 0;
            return finalPrice;
        }

        /// <summary>
        /// Gets the display image of the product.
        /// </summary>
        /// <param name="model">Product Model</param>
        /// <param name="skuImagePath">Product's first sku path.</param>
        /// <param name="productImagePath">Product's image path.</param>
        /// <returns>Gets display image of the product.</returns>
        private static string GetDisplayImage(ProductModel model, string skuImagePath, string productImagePath)
        {
            return !string.IsNullOrEmpty(skuImagePath) && !Path.GetFileName(HelperMethods.GetImagePath(skuImagePath)).Equals(Path.GetFileName(PortalAgent.CurrentPortal.ImageNotAvailablePath)) ?
                   HelperMethods.GetImagePath(skuImagePath) : (!string.IsNullOrEmpty(productImagePath) ?
                   HelperMethods.GetImagePath(productImagePath) : PortalAgent.CurrentPortal.ImageNotAvailablePath);
        }
    }
}