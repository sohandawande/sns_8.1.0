﻿using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Maps
{
    /// <summary>
    /// View Model Map for Content pages.
    /// </summary>
    public class ContentPageViewModelMap
    {

        /// <summary>
        /// Convert ContentPageModel to ContentPageViewModel
        /// </summary>
        /// <param name="model">ContentPageModel model</param>
        /// <returns>Returns the view model of content page</returns>
        public static ContentPageViewModel ToViewModel(ContentPageModel model)
        {
            if (Equals(model, null))
            {
                return null;
            }
            return new ContentPageViewModel()
            {
                Name = model.Name,
                PageTitle = model.Title,
                SEOTitle = model.SEOTitle,
            };
        }

        /// <summary>
        /// Convert ContentPageModel to ContentPageListViewModel
        /// </summary>
        /// <param name="model">IEnumerable ContentPageModel model</param>
        /// <returns>Returns list of content pages</returns>
        public static ContentPageListViewModel ToListViewModel(ContentPageListModel model)
        {
            if (!Equals(model, null) && !Equals(model.ContentPages, null))
            {
                var viewModel = new ContentPageListViewModel()
                {
                    ContentPages = model.ContentPages.ToList().Select(
                x => new ContentPageViewModel()
                {
                    Name = x.Name,
                    PageTitle = x.Title,
                    SEOTitle = x.SEOTitle,
                    SEOUrl=x.SEOURL
                }).ToList()
                };
                return viewModel;
            }
            else
            {
                return new ContentPageListViewModel();
            }
        }
    }
}