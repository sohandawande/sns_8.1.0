﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Maps
{
    /// <summary>
    /// Case Request View Model Map
    /// </summary>
    public class CaseRequestViewModelMap
    {
        public static CaseRequestModel ToModel(CaseRequestViewModel viewmodel)
        {
            var Model = new CaseRequestModel
            {
                FirstName = Equals(viewmodel.FirstName, null) ? string.Empty : viewmodel.FirstName,
                LastName = Equals(viewmodel.LastName, null) ? string.Empty : viewmodel.LastName,
                CompanyName = Equals(viewmodel.CompanyName, null) ? string.Empty : viewmodel.CompanyName,
                Email = viewmodel.EmailAddress,
                PhoneNumber = Equals(viewmodel.PhoneNumber, null) ? string.Empty : viewmodel.PhoneNumber,
                Description = Equals(viewmodel.Message, null) ? string.Empty : viewmodel.Message,
                CaseOrigin = viewmodel.CaseOrigin,
                Title = viewmodel.CaseRequestTitle,
                CreateUser = viewmodel.CreateUser,
                CreateDate = viewmodel.CreatedDate.Value,
                CasePriorityId = viewmodel.CasePriorityId,
                CasePriorityName = viewmodel.CasePriorityName,
                StoreName = viewmodel.StoreName,
                CaseStatusId = viewmodel.CaseStatusId,
                CaseStatusName = viewmodel.CaseStatusName,
                CaseRequestId = viewmodel.CaseRequestId,
                PortalId = viewmodel.PortalId,
                AccountId = viewmodel.AccountId,
                CaseTypeId = viewmodel.CaseTypeId

                //PRFT Custom Code : Start
                ,
                SalesRepId = viewmodel.SalesRepId
                ,
                IsRequestAQuoteForm = viewmodel.IsRequestAQuoteForm,
                SalesRepEmail=viewmodel.SalesRepEmail,
                SalesRepName=viewmodel.SaleRepNameWithLocation

                //PRFT Custom Code : End
            };
            //PRFT Custom Code : Start
            if (Model.IsRequestAQuoteForm)
            {
                Model.FaxNumber = viewmodel.RequestAQuote.FaxNumber;
                Model.DueDate = viewmodel.RequestAQuote.DueDate;
                Model.ContactChoiceSelected = viewmodel.RequestAQuote.ContactChoiceSelected;
            }
            else
            {
                if(viewmodel.ContactUsExtn!=null)
                {
                    Model.CountryCode = viewmodel.ContactUsExtn.CountryCode;
                    Model.StateCode = viewmodel.ContactUsExtn.StateCode;
                    Model.City = viewmodel.ContactUsExtn.City;
                }

            }
            //PRFT Custom Code : End
            return Model;
        }
    }
}