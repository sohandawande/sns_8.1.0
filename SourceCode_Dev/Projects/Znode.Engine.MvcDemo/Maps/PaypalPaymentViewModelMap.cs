﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.Agents;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Maps
{
    public static class PaypalPaymentViewModelMap
    {
        public static PaymentViewModel ToViewModel(ShoppingCartModel shoppingCart, AddressViewModel addressViewModel)
        {
            PaymentViewModel model = new PaymentViewModel();

            if (!Equals(shoppingCart, null) && !Equals(shoppingCart.BillingAddress, null))
            {
                model.BillingCity = shoppingCart.BillingAddress.City;
                model.BillingFirstName = shoppingCart.BillingAddress.FirstName;
                model.BillingLastName = shoppingCart.BillingAddress.LastName;
                model.BillingCountryCode = shoppingCart.BillingAddress.CountryCode;
                model.BillingEmailId = shoppingCart.BillingAddress.Email;
                model.BillingName = shoppingCart.BillingAddress.Name;
                model.BillingPhoneNumber = shoppingCart.BillingAddress.PhoneNumber;
                model.BillingPostalCode = shoppingCart.BillingAddress.PostalCode;
                model.BillingStateCode = shoppingCart.BillingAddress.StateCode;
                model.BillingStreetAddress1 = shoppingCart.BillingAddress.StreetAddress1;
                model.BillingStreetAddress2 = shoppingCart.BillingAddress.StreetAddress2;
                model.BillingCompanyName = shoppingCart.BillingAddress.CompanyName;
            }
            else
            {
                model.BillingCity = addressViewModel.City;
                model.BillingFirstName = addressViewModel.FirstName;
                model.BillingLastName = addressViewModel.LastName;
                model.BillingCountryCode = addressViewModel.CountryCode;
                model.BillingEmailId = addressViewModel.Email;
                model.BillingName = addressViewModel.Name;
                model.BillingPhoneNumber = addressViewModel.PhoneNumber;
                model.BillingPostalCode = addressViewModel.PostalCode;
                model.BillingStateCode = addressViewModel.StateCode;
                model.BillingStreetAddress1 = addressViewModel.StreetAddress1;
                model.BillingStreetAddress2 = addressViewModel.StreetAddress2;
                model.BillingCompanyName = addressViewModel.CompanyName;
            }

            model.SubTotal = shoppingCart.SubTotal.ToString();
            model.Total = shoppingCart.Total.ToString();
            model.OrderId = "1";
            model.TaxCost = shoppingCart.TaxCost.ToString();
            model.GiftCardAmount = shoppingCart.GiftCardAmount.ToString();
            model.GatewayCurrencyCode = PortalAgent.CurrentPortal.CurrencySuffix;
            model.ShippingCity = shoppingCart.ShippingAddress.City;
            model.ShippingFirstName = shoppingCart.ShippingAddress.FirstName;
            model.ShippingLastName = shoppingCart.ShippingAddress.LastName;
            model.ShippingCountryCode = shoppingCart.ShippingAddress.CountryCode;
            model.ShippingPhoneNumber = shoppingCart.ShippingAddress.PhoneNumber;
            model.ShippingPostalCode = shoppingCart.ShippingAddress.PostalCode;
            model.ShippingStateCode = shoppingCart.ShippingAddress.StateCode;
            model.ShippingStreetAddress1 = shoppingCart.ShippingAddress.StreetAddress1;
            model.ShippingStreetAddress2 = shoppingCart.ShippingAddress.StreetAddress2;
            model.ShippingCost = shoppingCart.ShippingCost.ToString();
            model.Discount = shoppingCart.Discount.ToString();
            shoppingCart.ShoppingCartItems.ToList().ForEach(item =>
            {

                var cartCount = shoppingCart.ShoppingCartItems.Count;
                CartItemModel objCartItem = new CartItemModel();
                objCartItem.ProductName = item.ProductName;
                objCartItem.ProductAmount = item.UnitPrice;
                objCartItem.ProductDescription = item.Description;
                objCartItem.ProductNumber = item.Sku;
                objCartItem.ProductQuantity = item.Quantity;
                model.CartItems.Add(objCartItem);
            });

            return model;
        }
    }
}