﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Maps
{
    public static class AddressViewModelMap
    {

        public static AddressModel ToModel(AddressViewModel viewModel)
        {
            if (viewModel != null)
            {
                var model = new AddressModel()
                    {
                        AccountId = viewModel.AccountId,
                        AddressId = viewModel.AddressId,
                        City = viewModel.City,
                        CompanyName = viewModel.CompanyName,
                        FirstName = viewModel.FirstName,
                        IsDefaultBilling = viewModel.IsDefaultBilling,
                        IsDefaultShipping = viewModel.IsDefaultShipping,
                        LastName = viewModel.LastName,
                        Name = viewModel.Name,
                        MiddleName = viewModel.MiddleName,
                        PhoneNumber = viewModel.PhoneNumber,
                        PostalCode = viewModel.PostalCode,
                        StateCode = viewModel.StateCode,
                        StreetAddress1 = viewModel.StreetAddress1,
                        StreetAddress2 = viewModel.StreetAddress2,
                        CountryCode = viewModel.CountryCode,
                        Email = viewModel.Email,

                        //PRFT Custom Code
                        AddressExtn = ToModel(viewModel.AddressExtn)
                    };
                return model;
            }

            return new AddressModel();

        }

        public static AddressViewModel ToModel(AddressModel viewModel)
        {
            var model = new AddressViewModel()
            {
                AccountId = viewModel.AccountId,
                AddressId = viewModel.AddressId,
                City = viewModel.City,
                CompanyName = viewModel.CompanyName,
                FirstName = viewModel.FirstName,
                IsDefaultBilling = viewModel.IsDefaultBilling,
                IsDefaultShipping = viewModel.IsDefaultShipping,
                LastName = viewModel.LastName,
                Name = viewModel.Name,
                MiddleName = viewModel.MiddleName,
                PhoneNumber = viewModel.PhoneNumber,
                PostalCode = viewModel.PostalCode,
                StateCode = viewModel.StateCode,
                StreetAddress1 = viewModel.StreetAddress1,
                StreetAddress2 = viewModel.StreetAddress2,
                CountryCode = viewModel.CountryCode,

                //PRFT Custom Code
                AddressExtn = ToModel(viewModel.AddressExtn)
            };
            return model;
        }

        public static AddressViewModel ToModel(OrderModel viewModel)
        {
            var model = new AddressViewModel()
            {
                City = viewModel.BillingCity,
                CompanyName = viewModel.BillingCompanyName,
                FirstName = viewModel.BillingFirstName,
                LastName = viewModel.BillingLastName,
                PhoneNumber = viewModel.BillingPhoneNumber,
                PostalCode = viewModel.BillingPostalCode,
                StateCode = viewModel.BillingStateCode,
                StreetAddress1 = viewModel.BillingStreetAddress1,
                StreetAddress2 = viewModel.BillingStreetAddress2,
                CountryCode = viewModel.BillingCountryCode
            };
            return model;
        }

        public static AddressViewModel ToModel(OrderShipmentModel viewModel)
        {
            var model = new AddressViewModel()
            {
                City = viewModel.ShipToCity,
                CompanyName = viewModel.ShipToCompanyName,
                FirstName = viewModel.ShipToFirstName,
                LastName = viewModel.ShipToLastName,
                PhoneNumber = viewModel.ShipToPhoneNumber,
                PostalCode = viewModel.ShipToPostalCode,
                StateCode = viewModel.ShipToStateCode,
                StreetAddress1 = viewModel.ShipToStreetAddress1,
                StreetAddress2 = viewModel.ShipToStreetAddress2,
                CountryCode = viewModel.ShipToCountryCode
            };
            return model;
        }

        public static PRFTAddressExtnModel ToModel(PRFTAddressExtnViewModel viewModel)
        {
            var model = new PRFTAddressExtnModel()
            {
                AddressExtnID = viewModel.AddressExtnID,
                ERPAddressID = viewModel.ERPAddressID,
                ModifiedDate = viewModel.ModifiedDate,
                CreatedDate = viewModel.CreatedDate,
                AddressID = viewModel.AddressID,
                Custom1 = viewModel.Custom1,
                Custom2 = viewModel.Custom2,
                Custom3 = viewModel.Custom3,
                Custom4 = viewModel.Custom4,
                Custom5 = viewModel.Custom5
            };
            return model;
        }

        public static PRFTAddressExtnViewModel ToModel(PRFTAddressExtnModel model)
        {
            var viewModel = new PRFTAddressExtnViewModel()
            {
                AddressExtnID = model.AddressExtnID,
                ERPAddressID = model.ERPAddressID,
                ModifiedDate = model.ModifiedDate,
                CreatedDate = model.CreatedDate,
                AddressID = model.AddressID,
                Custom1 = model.Custom1,
                Custom2 = model.Custom2,
                Custom3 = model.Custom3,
                Custom4 = model.Custom4,
                Custom5 = model.Custom5
            };
            return viewModel;
        }
    }
}