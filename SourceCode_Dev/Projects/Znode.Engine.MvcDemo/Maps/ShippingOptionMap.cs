﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Maps
{
    public static class ShippingOptionMap
    {
        public static ShippingOptionListViewModel ToViewModel(List<ShippingOptionModel> model)
        {
            var viewModel = new ShippingOptionListViewModel();

            //prft commented code: start

            //viewModel.ShippingOptions =
            //    model.Select(
            //        x =>
            //        new ShippingOptionViewModel() { ShippingDescription = x.Description, ShippingId = x.ShippingId, ShippingCode = x.ShippingCode })
            //         .ToList();

            //return viewModel;

            //prft commented code: start

            viewModel.ShippingOptions =
                model.Where(x => x.ActiveInd).Select(
                    x =>
                    new ShippingOptionViewModel() { ShippingDescription = x.Description, ShippingId = x.ShippingId, ShippingCode = x.ShippingCode })
                     .ToList();
            return viewModel;
        }

        public static ShippingOptionViewModel ToViewModel(ShippingOptionModel model)
        {
            if (model == null)
            {
                return new ShippingOptionViewModel();
            }

            return new ShippingOptionViewModel()
                {
                    ShippingDescription = model.Description,
                    ShippingId = model.ShippingId,
                    ShippingCode = model.ShippingCode
                };

        }
    }
}