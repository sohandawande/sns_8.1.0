﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Resources;
using Znode.Engine.MvcDemo.ViewModels;
using System.Web.Mvc;
namespace Znode.Engine.MvcDemo.Maps
{
    public static class RegisterViewModelMap
    {

        public static RegisterViewModel ToViewModel()
        {
            return new RegisterViewModel() {};
        }

        public static RegisterViewModel ToModel(AccountViewModel model)
        {
            return new RegisterViewModel()
                {
                    EmailAddress = model.EmailAddress,
                    UserName = model.UserName,
                    ParentAccountId = model.ParentAccountId
                };
        }
    }
}