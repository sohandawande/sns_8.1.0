﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Maps
{
    public class PRFTSkuViewModelMap
    {
        /// <summary>
        /// To map list view model
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static PRFTSkuListViewModel ToListModel(ProductSkuListModel model)
        {
            if (!Equals(model, null) && !(Equals(model.ProductSkus, null)))
            {
                var viewModel = new PRFTSkuListViewModel()
                {
                    Skus = model.ProductSkus.ToList().Select(
                    x => new PRFTSkuViewModel()
                    {
                        Id = x.SKUId,
                        SKU = x.SKU,
                        QuantityOnHand = x.QuantityOnHand,
                        ReorderLevel = x.ReorderLevel,
                        IsActive = x.IsActive,
                        ProductTypeName = x.ProductTypeName,
                        IsGiftCard = x.IsGiftCard,
                        ParentChildProduct = x.ParentChildProduct,
                        AttributeCount = x.AttributeCount,
                        SKUPicturePath = x.SKUPicturePath,
                        AttributeDescription=x.AttributeDescription,
                        //Description = x.Description,
                        Custom1 = x.Custom1,
                    }).ToList()
                };
                //viewModel.RecordPerPage = model.PageSize ?? 10;
                //viewModel.Page = model.PageIndex ?? 1;
                //viewModel.TotalPages = Convert.ToInt32(model.TotalPages);
                //viewModel.TotalResults = Convert.ToInt32(model.TotalResults);
                return viewModel;
            }
            else
            {
                return new PRFTSkuListViewModel();
            }
        }

    }
}