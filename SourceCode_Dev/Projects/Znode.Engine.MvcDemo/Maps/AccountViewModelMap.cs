﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.Helpers;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Maps
{
    public partial class AccountViewModelMap
    {
        public static AccountModel ToLoginModel(LoginViewModel model)
        {
            return new AccountModel()
            {
                User = new UserModel()
                {
                    Username = model.Username,
                    Password = model.Password,
                    PasswordQuestion = model.PasswordQuestion,
                },
                BaseUrl = (!string.IsNullOrEmpty(HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority))) ? HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) : string.Empty
            };
        }

        public static LoginViewModel ToLoginViewModel(AccountModel model)
        {
            return new LoginViewModel()
            {
                Username = model.User.Username,
                Password = model.User.Password,
                PasswordQuestion = model.User.PasswordQuestion,
                IsResetPassword = model.User.IsLockedOut,
                PasswordResetToken = model.User.PasswordToken,
            };
        }

        public static AccountViewModel ToAccountViewModel(AccountModel model)
        {
            var accountViewModel = new AccountViewModel()
            {
                AccountId = model.AccountId,
                //PRFT CUstom Code: Start
                //EmailAddress = Equals(model.User, null) ? string.Empty : model.User.Email,
                EmailAddress = Equals(model.User, null) ? model.Email : model.User.Email,
                //PRFT CUstom Code: End
                UserName = Equals(model.User, null) ? string.Empty : model.User.Username,
                PasswordQuestion = Equals(model.User, null) ? string.Empty : model.User.PasswordQuestion,
                PasswordAnswer = Equals(model.User, null) ? string.Empty : model.User.PasswordAnswer,
                UserId = Equals(model.User, null) ? new Guid() : model.User.UserId,
                CompanyName = model.CompanyName,
                EmailOptIn = model.EmailOptIn,
                EnableCustomerPricing = model.EnableCustomerPricing,
                ExternalId = model.ExternalId,
                IsActive = model.IsActive.GetValueOrDefault(true),
                ProfileId = model.Profiles.Any() ? model.Profiles.First().ProfileId : 0,
                UseWholeSalePricing = model.Profiles.Any() ? model.Profiles.First().UseWholesalePricing.GetValueOrDefault(false) : false,
                Addresses = new Collection<AddressViewModel>(model.Addresses.Select(AddressViewModelMap.ToModel).OrderBy(x => x.AddressId).ToList()),
                WishList = ToWishListViewModel(model.WishList),
                Orders = ToOrdersViewModel(model.Orders),
                TrackingLink = model.TrackingLink,
                ReferralCommissionType = model.ReferralCommissionType,
                TaxId = model.TaxId,
                ReferralCommission = model.ReferralCommission,
                ReferralStatus = model.ReferralStatus,
                GiftCardHistory = ToGiftCardHistoryListModel(model.GiftCardHistoryList),
                ReferralCommissionTypeId = model.ReferralCommissionTypeId,
                //PRFT Custom Code:Start
                Custom1=model.Custom1,
                Custom2=model.Custom2,
                Custom3=model.Custom3,
                ParentAccountId=model.ParentAccountId
                //PRFT Custom Code:End
            };

            if (accountViewModel.Orders.Any() && model.GiftCardHistoryList != null)
            {
                accountViewModel.Orders.ToList().ForEach(x =>
                {
                    var giftCardHistoryModel = model.GiftCardHistoryList.FirstOrDefault(y => y.OrderId == x.OrderId);
                    if (giftCardHistoryModel != null)
                        x.GiftCardAmount = -giftCardHistoryModel.TransactionAmount;
                });
            }

            return accountViewModel;
        }
        public static ReviewViewModel ToReviewViewModel(Collection<ReviewModel> model)
        {
            var reviewModel = new ReviewViewModel();
            reviewModel.Items = new Collection<ReviewItemViewModel>(
                model.Select(x => new ReviewItemViewModel()
                {
                    ReviewId = x.ReviewId,
                    Rating = x.Rating,
                    AccountId = x.AccountId,
                    Comments = WebUtility.HtmlDecode(x.Comments),
                    CreateDate = x.CreateDate.GetValueOrDefault().ToShortDateString(),
                    CreateUser = x.CreateUser,
                    ProductId = x.ProductId,
                    Status = x.Status,
                    Subject = x.Subject,
                    Duration = GetReviewDuration(x.CreateDate.GetValueOrDefault()),
                    UserLocation = x.UserLocation,
                }).ToList());

            return reviewModel;
        }

        public static ReviewModel ToReviewViewModel(ReviewItemViewModel model)
        {
            return new ReviewModel()
            {
                AccountId = model.AccountId,
                Comments = model.Comments,
                Cons = string.Empty,
                CreateDate = Convert.ToDateTime(model.CreateDate),
                CreateUser = model.CreateUser,
                Custom1 = string.Empty,
                Custom2 = string.Empty,
                Custom3 = string.Empty,
                Product = null,
                Status = MvcDemoConstants.DefaultReviewStatus,
                Subject = model.Subject,
                Rating = model.Rating,
                UserLocation = model.UserLocation,
                ProductId = model.ProductId,
                Pros = string.Empty,
            };
        }

        public static AccountModel ToAccountModel(AccountViewModel model)
        {
            return new AccountModel()
            {
                AccountId = model.AccountId,
                Email = model.EmailAddress,
                User = new UserModel()
                {
                    Username = model.UserName,
                    Email = model.EmailAddress,
                    UserId = model.UserId.GetValueOrDefault(),
                    PasswordQuestion = model.PasswordQuestion,
                    PasswordAnswer = model.PasswordAnswer,

                },
                CompanyName = model.CompanyName,
                EmailOptIn = model.EmailOptIn,
                EnableCustomerPricing = model.EnableCustomerPricing,
                ExternalId = model.ExternalId,
                IsActive = model.IsActive,
                ProfileId = model.ProfileId,
                UserId = model.UserId,
                Addresses = new Collection<AddressModel>(model.Addresses.Select(AddressViewModelMap.ToModel).ToList()),
                //Znode Version 7.2.2
                //Map BaseUrl to MVC Domain Url for Reseting Password - Start
                BaseUrl = model.BaseUrl,
                //Map BaseUrl to MVC Domain Url for Reseting Password - End
                ReferralCommission = model.ReferralCommission,
                ReferralCommissionType = model.ReferralCommissionType,
                ReferralStatus = model.ReferralStatus,
                ReferralCommissionTypeId = model.ReferralCommissionTypeId,

                //PRFT Custom Code:Start
                Custom1 = model.Custom1,
                Custom2 = model.Custom2,
                Custom3 = model.Custom3,
                ParentAccountId=model.ParentAccountId
                //PRFT Custom Code:End

            };
        }

        public static AccountModel ToAccountModel(AccountViewModel model, ChangePasswordViewModel passwordModel)
        {
            return new AccountModel()
            {
                AccountId = model.AccountId,
                Email = model.EmailAddress,
                User = new UserModel()
                {
                    Username = model.UserName,
                    Email = model.EmailAddress,
                    UserId = model.UserId.GetValueOrDefault(),
                    PasswordQuestion = model.PasswordQuestion,
                    PasswordAnswer = model.PasswordAnswer,
                    Password = passwordModel.OldPassword,
                    NewPassword = passwordModel.NewPassword,
                },
                CompanyName = model.CompanyName,
                EmailOptIn = model.EmailOptIn,
                EnableCustomerPricing = model.EnableCustomerPricing,
                ExternalId = model.ExternalId,
                IsActive = model.IsActive,
                ProfileId = model.ProfileId,
                UserId = model.UserId,
                Addresses = new Collection<AddressModel>(model.Addresses.Select(AddressViewModelMap.ToModel).ToList()),
            };
        }

        public static Collection<OrdersViewModel> ToOrdersViewModel(Collection<OrderModel> model)
        {
            var ordersModel = new Collection<OrdersViewModel>(model.Select(ToOrderViewModel).OrderByDescending(x => x.OrderId).ToList());

            return ordersModel;
        }

        public static Collection<GiftCardHistoryViewModel> ToGiftCardHistoryListModel(Collection<GiftCardHistoryModel> model)
        {
            var giftCardHistoryModel = new Collection<GiftCardHistoryViewModel>(model.Select(ToGiftCardHistoryViewModel).OrderByDescending(x => x.OrderId).ToList());

            return giftCardHistoryModel;
        }

        public static OrdersViewModel ToOrderViewModel(OrderModel model)
        {
            if (model.OrderId == 0)
            {
                return null;
            }
            return new OrdersViewModel()
            {
                OrderId = model.OrderId,
                OrderDate = model.OrderDate.GetValueOrDefault().ToShortDateString(),
                OrderTotal = model.Total.GetValueOrDefault(0),
                PaymentMethod = (model.PaymentTypeId != null) ? ((EnumPaymentType)model.PaymentTypeId).ToString() : string.Empty,
                TrackingNumber = model.TrackingNumber,
                AccountId = model.AccountId,
                SubTotal = model.SubTotal.GetValueOrDefault(0),
                Discount = -model.DiscountAmount.GetValueOrDefault(0),
                Tax = model.TaxCost.GetValueOrDefault(0),
                Shipping = model.ShippingCost.GetValueOrDefault(0),
                OrderLineItems = ToOrderLineItemViewModel(model.OrderLineItems),
                BillingAddress = AddressViewModelMap.ToModel(model),
                ShippingAddress = AddressViewModelMap.ToModel(model.OrderLineItems.First().OrderShipment),
                Custom1=model.Custom1,
                ExternalId = model.ExternalId,
                PurchaseOrderNumber=model.PurchaseOrderNumber,
                CardTransactionId=model.CardTransactionId

            };
        }

        public static GiftCardHistoryViewModel ToGiftCardHistoryViewModel(GiftCardHistoryModel model)
        {
            if (Equals(model, null))
            {
                return null;
            }
            return new GiftCardHistoryViewModel()
            {
                OrderId = model.OrderId,
                GiftCardHistoryId = model.GiftCardHistoryId,
                GiftCardId = model.GiftCardId,
                GiftCardNumber = model.GiftCardNumber,
                TransactionAmount = model.TransactionAmount,
                TransactionDate = model.TransactionDate,
            };
        }

        public static Collection<OrderLineItemViewModel> ToOrderLineItemViewModel(Collection<OrderLineItemModel> model)
        {
            var orderLineItemModel =
                new Collection<OrderLineItemViewModel>(model.Select(x => new OrderLineItemViewModel()
                {
                    OrderLineItemId = x.OrderLineItemId,
                    Quantity = x.Quantity.GetValueOrDefault(0),
                    Price = x.Price.GetValueOrDefault(0),
                    Total = (x.Quantity * x.Price).GetValueOrDefault(0),
                    Discount = x.DiscountAmount.GetValueOrDefault(0),
                    Tax = x.SalesTax.GetValueOrDefault(0),
                    Shipping = x.ShippingCost.GetValueOrDefault(0),
                    Name = x.Name,
                    Description = x.Description,
                    ParentOrderLineItemId = x.ParentOrderLineItemId,
                    OrderLineItemRelationshipTypeId = x.OrderLineItemRelationshipTypeId
                }).ToList());

            foreach (var orderLineItemViewModel in orderLineItemModel.Where(x => x.ParentOrderLineItemId == null))
            {
                var childLineItems = orderLineItemModel.Where(x => x.ParentOrderLineItemId == orderLineItemViewModel.OrderLineItemId);
                if (childLineItems.Any())
                {
                    orderLineItemViewModel.Price += childLineItems.Sum(x => x.Price);
                    orderLineItemViewModel.Total += childLineItems.Sum(x => x.Total);
                }
            }

            return new Collection<OrderLineItemViewModel>(orderLineItemModel.Where(y => y.ParentOrderLineItemId == null).ToList());
        }

        public static AccountModel ToSignUpModel(RegisterViewModel model)
        {
            var signUpModel = new AccountModel()
            {
                User = new UserModel()
                {
                    Username = model.UserName,
                    Password = model.Password,
                    Email = model.EmailAddress,
                    IsApproved = true,
                    ParentAccountId = model.ParentAccountId //PRFT Custom Code
                },
                IsActive = true,
                EmailOptIn = model.EmailOptIn

            };

            return signUpModel;
        }

        public static WishListViewModel ToWishListViewModel(Collection<WishListModel> wishListModel)
        {
            var wishListView = new WishListViewModel();
            wishListView.Items = new Collection<WishListItemViewModel>(
                wishListModel.Select(x => new WishListItemViewModel()
                {
                    ProductId = x.ProductId,
                    AccountId = x.AccountId,
                    CreateDate = x.CreateDate,
                    Custom = x.Custom,
                    WishListId = x.WishListId,
                }).ToList());

            return wishListView;
        }
        public static WishListItemViewModel ToWishListViewModel(WishListModel wishListModel)
        {
            var wishListView = new WishListItemViewModel();
            wishListView.AccountId = wishListModel.AccountId;
            wishListView.CreateDate = wishListModel.CreateDate;

            return new WishListItemViewModel()
            {
                ProductId = wishListModel.ProductId,
                AccountId = wishListModel.AccountId,
                CreateDate = wishListModel.CreateDate,
                Custom = wishListModel.Custom,
                WishListId = wishListModel.WishListId,
            };
        }

        public static WishListModel ToWishListModel(int accountId, int productId)
        {
            var wishList = new WishListModel()
            {
                ProductId = productId,
                AccountId = accountId,
                CreateDate = DateTime.Today,
                Custom = string.Empty,
            };

            return wishList;
        }

        public static AccountModel ToChangePasswordModel(ChangePasswordViewModel model)
        {
            var changePasswordModel = new AccountModel()
            {
                User = new UserModel()
                {
                    Username = model.UserName,
                    Password = model.OldPassword,
                    NewPassword = model.NewPassword,
                    PasswordToken = model.PasswordToken,
                }
            };

            return changePasswordModel;
        }

        public static GiftCardViewModel ToGiftCardViewModel(GiftCardModel model)
        {
            return new GiftCardViewModel()
            {
                AccountId = model.AccountId,
                Amount = model.Amount,
                CardNumber = model.CardNumber,
                GiftCardId = model.GiftCardId,
            };
        }

        private static string GetReviewDuration(DateTime reviewDate)
        {
            var ts = new TimeSpan(DateTime.UtcNow.Ticks - reviewDate.ToUniversalTime().Ticks);
            double delta = Math.Abs(ts.TotalSeconds);

            if (delta < MvcDemoConstants.ReviewTimeSixty)
            {
                return ts.Seconds == 1 ? "one second ago" : ts.Seconds + " seconds ago";
            }
            if (delta < MvcDemoConstants.ReviewTimeOneTwenty)
            {
                return "a minute ago";
            }
            if (delta < MvcDemoConstants.ReviewTimeTwoSevenHundred) // 45 * 60
            {
                return ts.Minutes + " minutes ago";
            }
            if (delta < MvcDemoConstants.ReviewTimeFiveFourHundred) // 90 * 60
            {
                return "an hour ago";
            }
            if (delta < MvcDemoConstants.ReviewTimeEightySixFourHundred) // 24 * 60 * 60
            {
                return ts.Hours + " hours ago";
            }
            if (delta < MvcDemoConstants.ReviewTimeOneSeventyTwoEightHundred) // 48 * 60 * 60
            {
                return "yesterday";
            }
            if (delta < MvcDemoConstants.ReviewTimeTwentyFiveNinetyTwo) // 30 * 24 * 60 * 60
            {
                return ts.Days + " days ago";
            }
            if (delta < MvcDemoConstants.ReviewTimeThreeoneonezeroFour) // 12 * 30 * 24 * 60 * 60
            {
                int months = Convert.ToInt32(Math.Floor((double)ts.Days / MvcDemoConstants.DaysInMonth));
                return months <= 1 ? "one month ago" : months + " months ago";
            }
            int years = Convert.ToInt32(Math.Floor((double)ts.Days / MvcDemoConstants.DaysInYear));
            return years <= 1 ? "one year ago" : years + " years ago";
        }

        public static SocialLoginModel ToSocialLoginModel(string provider, string providerUserId, bool createPersistentCookie)
        {
            SocialLoginModel model = new SocialLoginModel();
            model.Provider = provider;
            model.ProviderUserId = providerUserId;
            model.CreatePersistentCookie = createPersistentCookie;
            return model;
        }

        public static SocialLoginModel ToSocialUserRegisterModel(string provider, string providerUserId, string userName)
        {
            SocialLoginModel model = new SocialLoginModel();
            model.Provider = provider;
            model.ProviderUserId = providerUserId;
            model.UserName = userName;
            return model;
        }

        public static RegisteredSocialClientListViewModel ToSocialClientViewModel(RegisteredSocialClientListModel model)
        {
            RegisteredSocialClientListViewModel viewModel = new RegisteredSocialClientListViewModel();
            if (!Equals(model, null) && !Equals(model.SocialClients, null))
            {
                viewModel.SocialClients = new List<RegisteredSocialClientViewModel>(
                    model.SocialClients.Select(x => new RegisteredSocialClientViewModel()
                    {
                        ProviderName = x.ProviderName,
                        ProviderClientId = x.ProviderClientId,
                        ProviderClientSecret = x.ProviderClientSecret,
                    }));
            }
            return viewModel;
        }

        #region PRFT Custom Code : Start
        public static PRFTSubUsersViewModel ToSubUsersViewModel(AccountListModel listModel)
        {
            if (listModel != null && listModel.Accounts.Count > 0)
            {
                var subUserList = new PRFTSubUsersViewModel();
                listModel.Accounts.ToList().ForEach(x =>
                {
                    AccountViewModel viewModel = new AccountViewModel();
                    viewModel = ToAccountViewModel(x);
                    subUserList.SubUserAccounts.Add(viewModel);
                });

                subUserList.PageNumber = listModel.PageIndex;
                subUserList.PageSize = listModel.PageSize;
                subUserList.TotalPages = listModel.TotalPages.GetValueOrDefault(1);
                subUserList.TotalAccounts = listModel.TotalResults.GetValueOrDefault(10);

                return subUserList;
            }

            return new PRFTSubUsersViewModel();

        }
        #endregion
    }
}