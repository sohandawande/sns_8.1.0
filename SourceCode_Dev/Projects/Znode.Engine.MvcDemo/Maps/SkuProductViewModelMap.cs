﻿using Resources;
using System.Collections.Generic;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.Agents;
using Znode.Engine.MvcDemo.Helpers;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Maps
{
    public static class SkuProductViewModelMap
    {
        /// <summary>
        /// Convert sku product model to view model.
        /// </summary>
        /// <param name="model">model to convert.</param>
        /// <returns>Returns SkuProductViewModel</returns>
        public static SkuProductViewModel ToViewModel(SkuProductModel model)
        {
            if (!Equals(model, null))
            {
                IProductAgent productAgent = new ProductAgent();

                return new SkuProductViewModel()
                {
                    ProductId = model.ProductId,
                    SkuId = model.SkuId,
                    AddonValueIds = model.AddonValueIds,
                    BundleItemIds = model.BundleItemIds,
                    ImageFile = model.ImageFile,
                    InStockMessage = string.IsNullOrEmpty(model.InStockMessage) ? ZnodeResource.TextInstock : model.InStockMessage,
                    OutOfStockMessage = string.IsNullOrEmpty(model.OutOfStockMessage) ? ZnodeResource.TextOutofStock : model.OutOfStockMessage,
                    PortalId = model.PortalId,
                    Price = model.Price,
                    ProductNumber = model.ProductNumber,
                    Quantity = model.Quantity,
                    Sku = model.Sku,
                    ProductName = model.ProductName,
                    ShortDescription = model.ShortDescription,
                    ImageSmallPath = HelperMethods.GetImagePath(model.ImageSmallPath),
                    CartQuantity = productAgent.GetOrderedItemQuantity(model.Sku),
                    QuantityOnHand = model.QuantityOnHand,
                    InventoryMessage = GetInventoryMessage(model),
                    //AutoCompleteLabel = string.Format(ZnodeResource.AutoCompleteLabelQuickOrder, model.ProductName, model.ShortDescription, model.Sku, GetInventoryMessage(model), HelperMethods.GetImagePath(model.ImageSmallPath)),
                    AutoCompleteLabel = string.Format(ZnodeResource.AutoCompleteLabelQuickOrder, model.ProductName, model.Sku, HelperMethods.GetImagePath(model.ImageSmallPath)),
                    CallForPricing = model.CallForPricing,
                    MinQty = model.MinQty,
                    MaxQty = model.MaxQty,
                    BundleQuantityOnHand = model.BundleQuantityOnHand,
                    AddOnQuantityOnHand = model.AddOnQuantityOnHand,
                    AllowBackOrder = model.AllowBackOrder,
                    TrackInventoryInd = model.TrackInventoryInd,
                    BundleAllowBackOrder = model.BundleAllowBackOrder,
                    BundleTrackInventoryInd = model.BundleTrackInventoryInd,
                    SeoPageName=model.SeoPageName,
                };
            }
            return null;
        }

        /// <summary>
        /// Convert the list of models to list of view models.
        /// </summary>
        /// <param name="models">SkuProductListModel</param>
        /// <returns>Returns List<SkuProductViewModel></returns>
        public static List<SkuProductViewModel> ToViewModels(SkuProductListModel models)
        {
            List<SkuProductViewModel> list = new List<SkuProductViewModel>();
            if (!Equals(models, null) && !Equals(models.SkuProductList, null))
            {
                foreach (SkuProductModel model in models.SkuProductList)
                {
                    list.Add(ToViewModel(model));
                }
            }
            return list;
        }

        /// <summary>
        /// Gets the inventory message according to the quantity in hand.
        /// </summary>
        /// <param name="model">Sku product model to check the the inventory.</param>
        /// <returns>Returns the inventory message.</returns>
        public static string GetInventoryMessage(SkuProductModel model)
        {
            string inventoryMessage = string.IsNullOrEmpty(model.InStockMessage) ? ZnodeResource.TextInstock : model.InStockMessage;
            if ((model.TrackInventoryInd && !model.AllowBackOrder) && (!Equals(model.QuantityOnHand, null) && model.QuantityOnHand <= 0))
            {
                inventoryMessage = (string.IsNullOrEmpty(model.OutOfStockMessage) ? ZnodeResource.TextOutofStock : model.OutOfStockMessage);
            }
            return inventoryMessage;
        }
    }
}