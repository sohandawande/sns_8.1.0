﻿using DevTrends.MvcDonutCaching;
using System.Web;
using System.Web.Http;
using Znode.Engine.MvcDemo.Agents;

namespace Znode.Engine.MvcDemo.Service
{
    /// <summary>
    /// This class wil act as a API controller.  All the API which needs to be called in from Admin will be written in this class.
    /// </summary>
    public class ServiceController : ApiController
    {
        #region Private Variables

        private readonly IClearAPICacheAgent _clearAgent;
        private const string CacheCleredMessage = "Cache Cleared.";
        private const string CacheNotCleredMessage = "Error occurred during clear cache: ";

        #endregion

        #region Constructor

        public ServiceController()
        {
            _clearAgent = new ClearAPICacheAgent();
        }

        #endregion

        #region Publid Methods

        /// <summary>
        /// This method will clear the cache of Search and Category page
        /// </summary>
        [System.Web.Http.HttpGet]
        public string ClearCache()
        {
            try
            {
                HttpResponse.RemoveOutputCacheItem("/Account");
                HttpResponse.RemoveOutputCacheItem("/Cart");
                HttpResponse.RemoveOutputCacheItem("/CaseRequest");
                HttpResponse.RemoveOutputCacheItem("/Category");
                HttpResponse.RemoveOutputCacheItem("/Checkout");
                HttpResponse.RemoveOutputCacheItem("/Error");
                HttpResponse.RemoveOutputCacheItem("/Home");
                HttpResponse.RemoveOutputCacheItem("/MessageConfig");
                HttpResponse.RemoveOutputCacheItem("/Monitor");
                HttpResponse.RemoveOutputCacheItem("/ProductAsync");
                HttpResponse.RemoveOutputCacheItem("/Product");
                HttpResponse.RemoveOutputCacheItem("/SiteMap");
                HttpResponse.RemoveOutputCacheItem("/Search");
                    
                var cacheManager = new OutputCacheManager();
                cacheManager.RemoveItems();

                _clearAgent.ClearApiCache();

                return CacheCleredMessage;
            }
            catch (System.Exception ex)
            {
                return string.Concat(CacheNotCleredMessage, ex.Message.ToString());
            }
        }

        #endregion
    }
}
