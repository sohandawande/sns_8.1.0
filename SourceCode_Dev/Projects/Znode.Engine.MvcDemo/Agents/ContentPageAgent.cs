﻿using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.Maps;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Agents
{
    /// <summary>
    /// Content Page Agent
    /// </summary>
    public class ContentPageAgent : BaseAgent, IContentPageAgent
    {
        #region Private readonly variables
        private readonly IContentPageClient _contentPageClient;
        private readonly PortalAgent _portalAgent;
        #endregion

        #region Constructor
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public ContentPageAgent()
        {
            _contentPageClient = GetClient<ContentPageClient>();
            _portalAgent = new PortalAgent();
        }
        #endregion

        public string GetContentPageByName(string contentPageName, string extension)
        {
            string contentPage = string.Empty;
            if (!string.IsNullOrEmpty(contentPageName) && !string.IsNullOrEmpty(extension))
            {
                contentPage = _contentPageClient.GetContentPageByName(contentPageName, extension);

                if (!string.IsNullOrEmpty(contentPage))
                {
                    return contentPage;
                }
            }
            return contentPage;
        }

        public ContentPageListViewModel GetContentPageDetails(FilterCollection filters = null)
        {
            if (Equals(filters, null))
            {
                filters = new FilterCollection();
            }
            PortalViewModel currentPortal = PortalAgent.CurrentPortal;
            filters.Add(FilterKeys.PortalId, FilterOperators.Equals, currentPortal.PortalId.ToString());
            ContentPageListModel list = _contentPageClient.GetContentPages(null, filters, null, null, null);
            return Equals(list, null) ? new ContentPageListViewModel() : ContentPageViewModelMap.ToListViewModel(list);
        }
    }
}