﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using WebGrease.Css.Extensions;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.Helpers;
using Znode.Engine.MvcDemo.Maps;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Agents
{
    public class SearchAgent : BaseAgent, ISearchAgent
    {
        private readonly ISearchClient _searchClient;
        private readonly IProductsClient _productClient;
        private readonly ISkuAgent _skuAgent;
        private readonly ICategoryAgent _categoryAgent;
        private readonly IAccountAgent _accountAgent;

        private readonly ExpandCollection expands = new ExpandCollection()
            {
                ExpandKeys.Images,
                ExpandKeys.Skus,
                ExpandKeys.Promotions,
                ExpandKeys.Reviews,
                ExpandKeys.Facets,
                ExpandKeys.Categories,
                ExpandKeys.Attributes,
                ExpandKeys.AddOns
            };

        public SearchAgent()
        {
            _searchClient = GetClient<SearchClient>();
            _productClient = GetClient<ProductsClient>();
            _skuAgent = new SkuAgent();
            _categoryAgent = new CategoryAgent();
            _accountAgent = new AccountAgent();
        }

        /// <summary>
        /// Gets the suggestions for the search
        /// </summary>
        /// <param name="searchTerm">Search Term</param>
        /// <param name="category">Category Name</param>
        /// <returns>Returns a list of suggestion as suggested search model</returns>
        public List<SuggestedSearchModel> GetSuggestions(string searchTerm, string category)
        {
            var model = _searchClient.GetTypeAheadResponse(SearchMap.ToModel(searchTerm, category));
            if (!Equals(model, null) && !Equals(model.SuggestedSearchResults, null) && model.SuggestedSearchResults.Count > 0)
            {
                return model.SuggestedSearchResults.ToList();
            }
            return new List<SuggestedSearchModel>();
        }

        /// <summary>
        /// Search based on the search term
        /// </summary>
        /// <param name="requestModel">Search Request Model</param>
        /// <returns>Returns the list of Keyword search view model</returns>
        public KeywordSearchViewModel Search(SearchRequestModel requestModel)
        {
            try
            {
                var searchResult = SearchProducts(requestModel.SearchTerm, requestModel.Category, requestModel.RefineBy, GetInnerSearchList(requestModel.InnerSearchKeywords), expands, GetSortCollection(requestModel.Sort));

                if (searchResult != null)
                {
                    var pageNumber = requestModel.PageNumber.GetValueOrDefault(1);
                    var pageSize = requestModel.PageSize.GetValueOrDefault(MvcDemoConstants.DefaultPageSize);

                    // check current page number exceeds total products, then set page = 1;
                    if (pageNumber > Math.Ceiling(searchResult.Products.Count / (float)pageSize))
                    {
                        pageNumber = 1;
                    }

                    requestModel.PageNumber = pageNumber;
                    //PRFT Custom Code:START
                    requestModel.TotalPages = (int)Math.Ceiling(searchResult.Products.Count / (float)pageSize);
                    //PRFT Custom Code:END

                    var productList = GetProducts(searchResult, pageNumber, pageSize, requestModel.Sort, expands);

                    if (productList != null && productList.Count > 0)
                    {
                        var searchFacets = new Collection<SearchFacetModel>();
                        var facetsRefinedBy = requestModel.RefineBy.SelectMany(x => x.Value.Select(y => new KeyValuePair<string, string>(x.Key, y)));

                        if (searchResult.Facets != null)
                        {
                            searchResult.Facets.ToList().ForEach
                                (x => x.AttributeValues = new Collection<SearchFacetValueModel>(x.AttributeValues.Where
                                                                                                    (y =>
                                                                                                        !facetsRefinedBy.Any(
                                                                                                            z =>
                                                                                                            z.Key ==
                                                                                                            x.AttributeName
                                                                                                            .Split('|')[0] &&
                                                                                                            z.Value ==
                                                                                                            y.AttributeValue))
                                                                                                    .ToList()));

                            searchFacets = new Collection<SearchFacetModel>(searchResult.Facets.Where(x => x.AttributeValues.Any()).ToList());
                        }

                        // To retrieve  Selected Facets 
                        var selectedFacets = new List<KeyValuePair<string, KeyValuePair<string, string>>>();

                        requestModel.RefineBy.ForEach(
                            x =>
                                selectedFacets.Add(new KeyValuePair<string, KeyValuePair<string, string>>(
                                                        x.Key, new KeyValuePair<string, string>(
                                                                    string.Join(",", x.Value), SearchMap.UpdateUrlQueryString(x.Key, string.Empty, false, true)))));

                        var facetList = new Collection<FacetViewModel>(selectedFacets.Select(x => new FacetViewModel()
                        {
                            AttributeName = x.Key,
                            AttributeValues = new Collection<FacetValueViewModel>(x.Value.ToString().Split(',').Take(x.Value.ToString().Split(',').Length - 1).Select(y => new FacetValueViewModel()
                            {
                                AttributeValue = y.Replace("[", ""),
                                RefineByUrl = SearchMap.UpdateUrlQueryString(x.Key, y.Replace("[", ""), false, true).Replace("]", "")
                            }).ToList())
                        }).ToList());

                        SaveInSession(MvcDemoConstants.CategoryName, requestModel.Category);

                        CategoryViewModel categoryModel = null;

                        if (!string.IsNullOrEmpty(requestModel.Category))
                        {
                            categoryModel = _categoryAgent.GetCategoryByName(requestModel.Category, false, requestModel.CategoryId.GetValueOrDefault());

                            SaveInSession(MvcDemoConstants.CategoryId, categoryModel.CategoryId);
                        }

                        return new KeywordSearchViewModel()
                        {
                            Products = new Collection<ProductViewModel>(productList),
                            TotalProducts = searchResult.Products.Count,
                            Facets = searchFacets != null ? new Collection<FacetViewModel>(searchFacets.Select(SearchMap.ToFacetViewModel).ToList()) : null,
                            Categories = searchResult.Categories != null ? new Collection<SearchCategoryViewModel>(searchResult.Categories.Select(SearchMap.ToCategoryViewModel).ToList()) : null,
                            SelectedFacets = selectedFacets != null ? new Collection<FacetViewModel>(facetList) : null,
                            //TotalPages = (int)Math.Ceiling(searchResult.Products.Count() / (decimal)MvcDemoConstants.DefaultPageSize), //Old Code
                            //PRFT Custom Code:START
                            TotalPages = requestModel.TotalPages,
                            //PRFT Custom Code:END
                            SearchTerm = !Equals(requestModel.SearchTerm, null) ? requestModel.SearchTerm : string.Empty,
                            Category = WebUtility.UrlDecode(requestModel.Category),
                            SeoDescription = categoryModel != null ? categoryModel.SeoDescription : string.Empty,
                            SeoTitle = categoryModel != null ? categoryModel.SeoTitle : string.Empty,
                            SeoKeywords = categoryModel != null ? categoryModel.SeoKeywords : string.Empty,
                            CategoryBanner = categoryModel != null ? categoryModel.CategoryBanner : string.Empty,
                            Title = !Equals(categoryModel, null) ? categoryModel.Title : string.Empty,
                            CategoryTitle = !Equals(categoryModel, null) ? categoryModel.CategoryTitle : string.Empty,
                            SelectedCategoryId=!Equals(categoryModel, null) ? categoryModel.CategoryId : 0,
                        };
                    }
                }
            }
            catch (Exception ex)
            {
                return new KeywordSearchViewModel() { HasError = true, ErrorMessage = Resources.ZnodeResource.ErrorProductSearchResult };
            }

            return new KeywordSearchViewModel() { HasError = true, ErrorMessage = Resources.ZnodeResource.ErrorProductSearchResult };
        }

        /// <summary>
        /// Search the products
        /// </summary>
        /// <param name="searchTerm">Search Term</param>
        /// <param name="category">Category Name</param>
        /// <param name="refineBy">Refine By Key and values</param>
        /// <param name="innerSearchList">Inner Search List</param>
        /// <param name="expands">Expands</param>
        /// <param name="sortCollection">Sort Collection</param>
        /// <returns>Returns the list of Keyword search model</returns>
        private KeywordSearchModel SearchProducts(string searchTerm, string category, Collection<KeyValuePair<string, IEnumerable<string>>> refineBy, List<string> innerSearchList, ExpandCollection expands, SortCollection sortCollection)
        {
            return _searchClient.Search(SearchMap.ToKeywordSearchModel(searchTerm, category, refineBy, innerSearchList), expands, sortCollection);
        }

        /// <summary>
        /// Get the products based on the search result.
        /// </summary>
        /// <param name="searchResult">Search Result</param>
        /// <param name="pageNumber">Page Number</param>
        /// <param name="pageSize">Page Size</param>
        /// <param name="sortBy">Sort By</param>
        /// <param name="expands">Expands</param>
        /// <param name="status">Status</param>
        /// <returns>Returns the list of products as Product View Model</returns>
        private List<ProductViewModel> GetProducts(KeywordSearchModel searchResult, int pageNumber, int pageSize, string sortBy, ExpandCollection expands)
        {
            var productList = new Collection<ProductModel>();
            string selectedSku = string.Empty;
            string imagePath = string.Empty;
            //ZNode Version 7.2.2 - Add imageMediumPath Out parameter
            string imageMediumPath = string.Empty;

            var currentProductIds = GetCurrentPageProductIDs(searchResult, pageNumber, pageSize, sortBy);

            // API Call to get Product Information
            if (searchResult.Products.Count == 1)
            {
                productList.Add(_productClient.GetProduct(Convert.ToInt32(searchResult.Products.First().Id), expands));
            }
            else
            {
                productList = GetProductsByIds(currentProductIds);
            }

            var currentProductList = currentProductIds.Select(x => ProductViewModelMap.ToViewModel(productList.FirstOrDefault(y => y.ProductId == int.Parse(x)))).ToList();

            AccountViewModel accountViewModel = _accountAgent.GetAccountViewModel();
            if (!Equals(accountViewModel, null) && Convert.ToBoolean(accountViewModel.EnableCustomerPricing))
            {
                var productView = ProductViewModelMap.ToViewModels(new Collection<ProductModel>(productList.Where(x => x.IsActive).ToList()));

                foreach (ProductViewModel item in currentProductList)
                {
                    try
                    {
                        var data = (from product in productList.ToList()
                                    where product.ProductId == item.ProductId && product.Skus != null

                                    from productSku in product.Skus.ToList()
                                    from CustomerPricingSku in productSku.CustomerPricingSku.ToList()
                                    where CustomerPricingSku.ExternalAccountNo == accountViewModel.ExternalId
                                    select CustomerPricingSku.NegotiatedPrice.Value).FirstOrDefault();

                        if (!Equals(data, null) && data > 0)
                        {
                            item.ProductPrice = data;
                        }
                    }
                    catch (Exception ex)
                    { }
                }
            }

            currentProductList.ForEach(x =>
                {
                    x.Price = x.ProductAttributes.Attributes.Any() ? _skuAgent.GetSkusPrice(x.ProductId,
                                               x.ProductAttributes.Attributes.FirstOrDefault().ProductAttributes.FirstOrDefault().AttributeId,
                                               x.Price, out selectedSku, out imagePath, out imageMediumPath) : x.Price;
                    //x.Sku = selectedSku;
                    x.Sku = !string.IsNullOrEmpty(selectedSku) ? selectedSku : x.Sku;
                    selectedSku = string.Empty;
                    //Znode Version 7.2.2
                    //Populate Swatch Images - Start 
                    x.SwatchImages = GetSwatchImages(x);
                    //Populate Swatch Images - End 
                });

            // Price to be sorted from the DB
            if (!string.IsNullOrEmpty(sortBy) && sortBy.StartsWith("price|"))
            {
                // Price to be sorted from the DB as ASC or DESC
                // Modified the code replace Price with ProductPrice for sorting by sale price
                currentProductList = sortBy.EndsWith("|desc") ? currentProductList.OrderBy(x => x.IsCallForPricing).ThenByDescending(x => x.ProductPrice).ToList() : currentProductList.OrderBy(x => x.IsCallForPricing).ThenBy(x => x.ProductPrice).ToList();
                if(!pageSize.Equals(-1))
                {
                    currentProductList = currentProductList.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
                }
            }

            return currentProductList;
        }

        /// <summary>
        /// Get the product by IDs
        /// </summary>
        /// <param name="ids">Product IDs</param>
        /// <returns>Returns the list of product as product model</returns>
        public Collection<ProductModel> GetProductsByIds(List<string> ids)
        {
            var productIds = ids.Distinct().ToList();

            if (productIds.Count() == 1)
            {
                var singleProduct = _productClient.GetProduct(int.Parse(productIds.First()), expands);
                return new Collection<ProductModel>() { singleProduct };
            }

            IEnumerable<ProductModel> products = new Collection<ProductModel>();

            var page = 1;
            const int pagesize = 10;
            do
            {
                var currentProductIds = productIds.Skip((page - 1) * pagesize).Take(pagesize).ToList();

                if (currentProductIds.Count() == 1)
                {
                    var singleProduct = _productClient.GetProduct(int.Parse(currentProductIds.First()), expands);
                    products = products.Union(new Collection<ProductModel>() { singleProduct });
                }
                else
                {
                    var idString = String.Join(",", currentProductIds);
                    var list = _productClient.GetProductsByProductIds(idString, expands, new SortCollection());
                    products = products.Union(list.Products);
                }

                page++;
            } while (page <= Math.Ceiling(productIds.Count() / (decimal)pagesize));

            return new Collection<ProductModel>(productIds.Select(x => products.FirstOrDefault(y => y.ProductId.ToString() == x)).ToList());
        }

        /// <summary>
        /// Get the current page product ids
        /// </summary>
        /// <param name="searchResult">Search Result</param>
        /// <param name="pageNumber">Page Number</param>
        /// <param name="pageSize">Page Size</param>
        /// <param name="sortBy">Sort By</param>
        /// <returns>Returns the list of string of product ids </returns>
        private List<string> GetCurrentPageProductIDs(KeywordSearchModel searchResult, int pageNumber, int pageSize, string sortBy)
        {
            // Do Paging
            if (pageNumber == 0 && pageSize == 0)
            {
                pageNumber = MvcDemoConstants.DefaultPageNumber;
                pageSize = MvcDemoConstants.DefaultPageSize;
            }

            // Pass -1 for Show ALL option
            if (pageSize == -1 || pageSize > searchResult.Products.Count)
            {
                pageSize = searchResult.Products.Count;
                pageNumber = MvcDemoConstants.DefaultPageNumber;
            }

            var currentProductIds = searchResult.Products.Skip((pageNumber - 1) * pageSize).Take(pageSize).Select(x => x.Id);

            // Price to be sorted from the DB
            if (!string.IsNullOrEmpty(sortBy) && sortBy.StartsWith("price|"))
            {
                currentProductIds = searchResult.Products.Select(x => x.Id);
            }

            return currentProductIds.ToList();
        }

        /// <summary>
        /// get the inner search list.
        /// </summary>
        /// <param name="innerSearchTerm">Inner Search Term</param>
        /// <returns>Returns the list of string for the inner search list</returns>
        private List<string> GetInnerSearchList(string innerSearchTerm)
        {
            var innerSearchList = new List<string>();

            if (!string.IsNullOrEmpty(innerSearchTerm))
            {
                var stringSeparators = new[] { "||" };
                innerSearchList = innerSearchTerm.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries).ToList();
            }

            return innerSearchList;
        }

        /// <summary>
        /// Get the Sort Collection
        /// </summary>
        /// <param name="sortBy">Sort By</param>
        /// <returns>Returns the sort collection object</returns>
        private SortCollection GetSortCollection(string sortBy)
        {
            var sortCollection = new SortCollection();

            if (!string.IsNullOrEmpty(sortBy))
            {
                sortCollection.Add(sortBy.Split('|')[0], sortBy.Split('|')[1]);
            }

            return sortCollection;
        }

        #region Znode Version 7.2.2

        //Populate Swatch Images - Start 

        /// <summary>
        /// Get All Swatch Images
        /// Filter All Product images to get the swatch iamges based on ProductImageTypeId.
        /// </summary>
        /// <param name="ProductViewModel">ProductViewModel</param>
        /// <returns>Returns swatch images collection object</returns>
        private List<ImageViewModel> GetSwatchImages(ProductViewModel model)
        {
            List<ImageViewModel> swatchModel = new List<ImageViewModel>();
            swatchModel = (model.Images != null && model.Images.Any()) ? model.Images.Where(y => y.ProductImageTypeId == 2 && y.ShowOnCategoryPage).OrderBy(y => y.DisplayOrder).ToList() : null;
            if (swatchModel != null && swatchModel.Any())
            {
                swatchModel.Add(new ImageViewModel { ImageAltTag = model.ImageAltTag, ImageMediumPath = model.ImageMediumPath, ImageSmallThumbnailPath = model.ImageSmallThumbnailPath });
            }
            return swatchModel;
        }
        #endregion

        /// <summary>
        /// To Get Seo Urls Details based on Seo Url.
        /// </summary>
        /// <param name="seoUrl">string seoUrl</param>
        /// <returns>Return SEO Url Details in SEOUrlViewModel format.</returns>
        public SEOUrlViewModel GetSeoUrlDetail(string seoUrl)
        {
            return SearchMap.ToSeoViewModel(_searchClient.GetSeoUrlDetail(new SEOUrlModel { SeoUrl = seoUrl }));
        }
    }
}