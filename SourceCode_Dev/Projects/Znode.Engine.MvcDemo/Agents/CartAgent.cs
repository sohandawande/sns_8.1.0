﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.MvcDemo.Helpers;
using Znode.Engine.MvcDemo.Maps;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Agents
{
    public partial class CartAgent : BaseAgent, ICartAgent
    {
        private readonly IShoppingCartsClient _shoppingCartsClient;
        private readonly IPromotionsClient _promotionsClient;
        readonly ISkuAgent _skuAgent;
        private readonly IProductAgent _productAgent;

        /// <summary>
        /// Cart Agent Constructor
        /// </summary>
        public CartAgent()
        {
            _shoppingCartsClient = GetClient<ShoppingCartsClient>();
            _skuAgent = new SkuAgent();
            _productAgent = new ProductAgent();
            _promotionsClient = new PromotionsClient();
        }

        /// <summary>
        /// Check Inventory for Wishlist product
        /// </summary>        
        /// <returns>Returns the productview model with inventory message</returns>               
        public ProductViewModel WishlistCheckInventory(CartItemViewModel cartItem)
        {
            ProductViewModel product = new ProductViewModel();

            if (!string.IsNullOrEmpty(cartItem.ProductId.ToString()))
            {
                string selectedSku = string.Empty;
                string imagePath = string.Empty;
                string imageMediumPath = string.Empty;
                decimal productPrice;
                decimal finalProductPrice;

                if (!Equals(cartItem, null) && !Equals(cartItem.Attributes, null))
                {
                    // Get the product
                    product = _productAgent.GetAttributes(cartItem.ProductId, Convert.ToInt32(cartItem.Attributes[0]), cartItem.Attributes, cartItem.Quantity, out productPrice, out selectedSku, out imagePath, out imageMediumPath);

                    if (product.AddOns != null && !string.IsNullOrEmpty(cartItem.AddOnValueIds))
                    {
                        product.AddOns = _productAgent.GetAddOnDetails(product, cartItem.AddOnValueIds, cartItem.Quantity,
                                                                       productPrice, out finalProductPrice);
                    }
                    else
                    {
                        finalProductPrice = productPrice;
                    }
                }
            }
            return product;
        }

        /// <summary>
        /// Create the shopping cart object and add cart items in the session and cookie.
        /// </summary>
        /// <param name="cartItem">Cart Item</param>
        /// <returns>Returns the shopping cart items as cart view model</returns>
        public CartViewModel Create(CartItemViewModel cartItem)
        {
            var cart = GetFromSession<ShoppingCartModel>(MvcDemoConstants.CartModelSessionKey) ??
                       GetCartFromCookie() ?? new ShoppingCartModel();

            //PRFT Custom Code : Start
            IProductAgent productAgent = new ProductAgent();
            var productsViewModel = productAgent.GetProduct(cartItem.ProductId);
            
            cartItem.ERPUnitPrice = cartItem.UnitPrice;
            string associatedCustomerExternalId = HelperMethods.GetAssociatedCustomerExternalId();
            if (!string.IsNullOrEmpty(associatedCustomerExternalId))
            {
                string customerType = HelperMethods.GetERPCustomerType();
                PRFTERPItemDetailsModel prftERPItemDetailModel = new PRFTERPItemDetailsModel();
                prftERPItemDetailModel = productAgent.GetItemDetailsFromERP(productsViewModel.ProductNumber, associatedCustomerExternalId, customerType);
                if (prftERPItemDetailModel != null && prftERPItemDetailModel.HasError == false)
                {
                    cartItem.ERPUnitPrice = prftERPItemDetailModel.UnitPrice;
                    if (prftERPItemDetailModel.IsCallForPricing == true || prftERPItemDetailModel.UnitPrice <= 0) //Do not add items to cart
                    {
                        HttpContext.Current.Session.Add("ErrorMessage", Resources.ZnodeResource.CallForPriceItemErrorMessage);
                        return new CartViewModel() { HasError = true, ErrorMessage = Resources.ZnodeResource.ErrorAddProductToCart };
                    }
                }
            }

            //Inventory Handling
            //Get Inventory From ERP
            int availableQty = _productAgent.GetInventoryFromERP(productsViewModel.ProductNumber);
            var isB2BCustomer = new PRFTHelper().IsB2BUser();
            int cartQuantity = _productAgent.GetOrderedItemQuantity(cartItem.Sku); 
            int combinedQuantity = cartQuantity + cartItem.Quantity;
            cartItem.BackOrderMessage = string.Empty;
            cartItem.AllowBackOrder = false;
            if (availableQty <= 0 && !isB2BCustomer)
            {
                HttpContext.Current.Session.Add("ErrorMessage", Resources.ZnodeResource.OutOfStockItemErrorMessage);
                return new CartViewModel() { HasError = true, ErrorMessage = Resources.ZnodeResource.ErrorAddProductToCart };
            }
            else if(availableQty < combinedQuantity)
            {
                if(isB2BCustomer)
                {
                        cartItem.BackOrderMessage = string.Format(Resources.ZnodeResource.BackOrderMessage, combinedQuantity - availableQty);
                        cartItem.AllowBackOrder = true;
                        cartItem.Quantity = combinedQuantity;
                }
                else
                {
                    cartItem.Quantity = availableQty;
                }
            }
            else
            {
                cartItem.Quantity = combinedQuantity;
            }

            //PRFT Custom Code : Start

            UpdateAccountInfo(cart);
            
            cart.ShoppingCartItems.Add(CartItemViewModelMap.ToModel(cartItem));

            var shoppingCartModel = _shoppingCartsClient.CreateCart(cart);

            if (!Equals(shoppingCartModel, null))
            {
                
                foreach (CouponModel coupon in cart.Coupons)
                {
                    shoppingCartModel.Coupons.Add(coupon);
                }
                shoppingCartModel.GiftCardNumber = cart.GiftCardNumber;
                shoppingCartModel.GiftCardValid = cart.GiftCardValid;
                shoppingCartModel.GiftCardMessage = cart.GiftCardMessage;
                shoppingCartModel.GiftCardApplied = cart.GiftCardApplied;
                shoppingCartModel.GiftCardAmount = cart.GiftCardAmount;
                shoppingCartModel.GiftCardBalance = cart.GiftCardBalance;

                foreach (var cartItems in shoppingCartModel.ShoppingCartItems)
                {
                    foreach (var shoppingCartItems in cart.ShoppingCartItems)
                    {
                        if (!Equals(cartItems, null) && Equals(cartItems.ProductId, shoppingCartItems.ProductId) && Equals(cartItems.CartDescription, shoppingCartItems.CartDescription))
                        {
                            cartItems.AddOnValuesCustomText = shoppingCartItems.AddOnValuesCustomText;                                                      
                        }                       
                    }
                    //PRFT custom code : start
                    if (!Equals(cartItems, null) && Equals(cartItems.Sku, cartItem.Sku))
                    {
                        cartItems.Quantity = cartItem.Quantity;
                    }
                    //PRFT custom code : end

                }

                SaveInSession(MvcDemoConstants.CartModelSessionKey, shoppingCartModel);

                // if persistent cart disabled, we need not call below method, need to check with portal record.
                if ((Equals(shoppingCartModel.Account, null) || Equals(shoppingCartModel.Account.AccountId, 0)) && PortalAgent.CurrentPortal.PersistentCartEnabled)
                {
                    SaveInCookie(MvcDemoConstants.CartCookieKey, shoppingCartModel.CookieId.ToString());
                }

                return CartViewModelMap.ToViewModel(shoppingCartModel);
            }

            return new CartViewModel() { HasError = true, ErrorMessage = Resources.ZnodeResource.ErrorAddProductToCart };
        }

        

        /// <summary>
        /// Get Cart method to check Session or Cookie to get the existing shopping cart.
        /// </summary>
        /// <returns>Returns the cart items as cart view Model</returns>
        public CartViewModel GetCart()
        {
            var cart = GetFromSession<ShoppingCartModel>(MvcDemoConstants.CartModelSessionKey) ??
                       GetCartFromCookie();


            if (Equals(cart, null))
            {
                return new CartViewModel()
                    {
                        HasError = true
                    };
            }

            //PRFT Custom Code : Start - When cart is fatching from session or from cookie we need to reasign all ERP values
            string associatedCustomerExternalId = HelperMethods.GetAssociatedCustomerExternalId();
            string customerType = HelperMethods.GetERPCustomerType();
            PRFTERPItemDetailsModel prftERPItemDetailModel = new PRFTERPItemDetailsModel();
            IProductAgent productAgent = new ProductAgent();

            for (int i = 0; i < cart.ShoppingCartItems.Count(); i++)
            {
                cart.ShoppingCartItems[i].ERPUnitPrice = cart.ShoppingCartItems[i].UnitPrice;
                var productsViewModel = productAgent.GetProduct(cart.ShoppingCartItems[i].ProductId);
                bool isCallForPriceItem = false;
                if (!string.IsNullOrEmpty(associatedCustomerExternalId))
                {
                    
                    prftERPItemDetailModel = productAgent.GetItemDetailsFromERP(productsViewModel.ProductNumber, associatedCustomerExternalId, customerType);
                    if (prftERPItemDetailModel != null && prftERPItemDetailModel.HasError == false)
                    {
                        cart.ShoppingCartItems[i].ERPUnitPrice = prftERPItemDetailModel.UnitPrice;
                        if (prftERPItemDetailModel.IsCallForPricing == true || prftERPItemDetailModel.UnitPrice <= 0)
                        {
                            isCallForPriceItem = true;
                            cart.ShoppingCartItems.Remove(cart.ShoppingCartItems[i]);
                            i--;
                        }
                    }
                }

                //Inventory Handling
                //Get Inventory From ERP
                cart.ShoppingCartItems[i].BackOrderMessage = string.Empty;
                cart.ShoppingCartItems[i].AllowBackOrder = false;
                if (!isCallForPriceItem)
                {

                    int availableQty = _productAgent.GetInventoryFromERP(productsViewModel.ProductNumber);
                    var isB2BCustomer = new PRFTHelper().IsB2BUser();
                    if (availableQty <= 0 && !isB2BCustomer)
                    {
                        cart.ShoppingCartItems.Remove(cart.ShoppingCartItems[i]);
                        i--;
                    }
                    else if (availableQty < cart.ShoppingCartItems[i].Quantity)
                    {
                        if (isB2BCustomer)
                        {
                            cart.ShoppingCartItems[i].BackOrderMessage = string.Format(Resources.ZnodeResource.BackOrderMessage, cart.ShoppingCartItems[i].Quantity - availableQty);
                            cart.ShoppingCartItems[i].AllowBackOrder = true;
                        }
                        else {
                            cart.ShoppingCartItems[i].Quantity = availableQty;
                        }
                    }
                }
            }
            //PRFT Custom Code : End

            CartViewModel cartModel = Calculate(cart);
            if (GetFromSession<bool>(MvcDemoConstants.CartMerged))
            {
                cartModel.ErrorMessage = Resources.ZnodeResource.SavedCartText;
                cartModel.HasError = true;
                SaveInSession(MvcDemoConstants.CartMerged, false);
            }
            return cartModel;
        }

        /// <summary>
        /// Get Cart method to check Session or Cookie to get the existing shopping cart.
        /// </summary>
        /// <returns>Returns the cart items as cart view Model</returns>
        public int GetCartCount()
        {
            var cart = GetFromSession<ShoppingCartModel>(MvcDemoConstants.CartModelSessionKey) ??
                       GetCartFromCookie();

            if (!Equals(cart, null)) { return cart.ShoppingCartItems.Count; }

            return 0;
        }

        private void UpdateAccountInfo(ShoppingCartModel cart)
        {
            if (Equals(cart, null))
            {
                return;
            }
            var account = GetFromSession<AccountViewModel>(MvcDemoConstants.AccountKey);

            if (!Equals(account, null))
            {
                cart.Account = AccountViewModelMap.ToAccountModel(account);

                //PRFT Custom Code to store associated customer external id into shopping cart
                cart.CustomerExternalAccountId = HttpContext.Current.Session["AssociatedCustomerExternalId"] != null ? Convert.ToString(HttpContext.Current.Session["AssociatedCustomerExternalId"].ToString()) : ConfigurationManager.AppSettings["GenericUserExternalID"].ToString();
            }
        }

        /// <summary>
        /// Get Cart method to check Session or Cookie to get the existing shopping cart.
        /// </summary>
        /// <returns>Returns the calculated cart items as cart view model</returns>
        public CartViewModel Calculate(ShoppingCartModel shoppingCartModel)
        {

            IProductAgent productAgent = new ProductAgent();
            var calculatedModel = shoppingCartModel;

            UpdateAccountInfo(shoppingCartModel);
            if (!Equals(shoppingCartModel, null) && !shoppingCartModel.ShoppingCartItems.Any(item => item.InsufficientQuantity))
            { calculatedModel = _shoppingCartsClient.Calculate(shoppingCartModel); }

            if (!Equals(calculatedModel, null) && !Equals(calculatedModel.ShoppingCartItems, null))
            {                
                var ids = string.Join(",", calculatedModel.ShoppingCartItems.Select(x => x.ProductId.ToString()));
                var productsViewModel = productAgent.GetProductByIds(ids);
                var cartViewModel = CartViewModelMap.ToViewModel(calculatedModel);
                if (!string.IsNullOrEmpty(calculatedModel.Shipping.ResponseCode) && !Equals(calculatedModel.Shipping.ResponseCode,"0"))
                {
                    cartViewModel.HasError = true;
                    cartViewModel.ErrorMessage = calculatedModel.Shipping.ResponseMessage;
                }
                if (!Equals(productsViewModel,null))
                {
                    productsViewModel.ToList().ForEach(productModel =>
                    {
                        var items = cartViewModel.Items.Where(itemModel => itemModel.ProductId == productModel.ProductId);

                        items.ToList().ForEach(itemModel => itemModel.Product = productModel);
                    });

                    productsViewModel.ToList().ForEach(productModel =>
                    {
                        cartViewModel.Items.Where(cartItem => cartItem.ProductId == productModel.ProductId).FirstOrDefault().ShippingCost = cartViewModel.ShippingCost;
                    });
                }
                

                //Update SKU Image Path with Product Main Image in Case no image found -Start
                foreach (var data in cartViewModel.Items)
                {
                    if (Path.GetFileName(data.ImagePath).Equals(MvcDemoConstants.NoImageName))
                    {
                        data.ImagePath = data.Product.ImageLargePath;
                    }
                }
                //Update SKU Image Path with Product Main Image in Case no image found -End

                // Set Account Id or Cookie Id.
                if (Equals(shoppingCartModel.Account, null) || Equals(shoppingCartModel.Account.AccountId, 0))
                { calculatedModel.CookieId = shoppingCartModel.CookieId; }
                else
                { calculatedModel.Account = shoppingCartModel.Account; }

                if (!Equals(calculatedModel, null) && !Equals(calculatedModel.Coupons, null))
                {
                    foreach (CouponModel coupon in calculatedModel.Coupons)
                    {
                        if (!Equals(coupon, null) && !coupon.CouponApplied)
                        {
                            cartViewModel.HasError = true;
                            break;
                        }
                    }
                }

                // Model into Session 
                SaveInSession(MvcDemoConstants.CartModelSessionKey, calculatedModel);
                return cartViewModel;
            }


            return new CartViewModel() { HasError = true };
        }

        /// <summary>
        /// Removes the items from the shopping cart.
        /// </summary>
        /// <param name="guid">GUID of the cart item</param>
        /// <returns>Bool value if the items are removed or not.</returns>
        public bool RemoveItem(string guid)
        {
            // Get cart from Session.
            var cart = GetFromSession<ShoppingCartModel>(MvcDemoConstants.CartModelSessionKey);
            if (Equals(cart, null) || !cart.ShoppingCartItems.Any()) { return false; }
            if (cart.ShoppingCartItems.Count.Equals(1)) { RemoveAllCartItems(); return true; }

            // Check if item exists.
            var item = cart.ShoppingCartItems.FirstOrDefault(x => x.ExternalId == guid);
            if (Equals(item, null)) { return false; }

            UpdateAccountInfo(cart);

            // Remove item and update the cart in Session and API.
            cart.ShoppingCartItems.Remove(item);

            var shoppingCartModel = _shoppingCartsClient.CreateCart(cart);

            if (!Equals(shoppingCartModel, null))
            {
                foreach (CouponModel coupon in cart.Coupons)
                {
                    shoppingCartModel.Coupons.Add(coupon);
                }

                shoppingCartModel.GiftCardNumber = cart.GiftCardNumber;
                shoppingCartModel.GiftCardValid = cart.GiftCardValid;
                shoppingCartModel.GiftCardMessage = cart.GiftCardMessage;
                shoppingCartModel.GiftCardApplied = cart.GiftCardApplied;
                shoppingCartModel.GiftCardAmount = cart.GiftCardAmount;
                shoppingCartModel.GiftCardBalance = cart.GiftCardBalance;
            }

            SaveInSession(MvcDemoConstants.CartModelSessionKey, shoppingCartModel);
            return true;
        }

        /// <summary>
        /// Updates the Quantity updated in the shopping cart page.
        /// </summary>
        /// <param name="guid">GUID of the cart item</param>
        /// <param name="qty">Quantity Selected</param>
        /// <returns>Bool value if the items are updated or not.</returns>
        public bool UpdateItem(string guid, int qty)
        {
            // Get cart from Session.
            var cart = GetFromSession<ShoppingCartModel>(MvcDemoConstants.CartModelSessionKey);
            if (Equals(cart, null) || !cart.ShoppingCartItems.Any()) { return false; }

            // Check if item exists.
            var item = cart.ShoppingCartItems.FirstOrDefault(x => x.ExternalId == guid);
            if (Equals(item, null)) { return false; }

            //Check Inventory
            item.InsufficientQuantity = false;
            var skumodel = _skuAgent.GetBySku(item.ProductId, item.Sku);
            var product = _productAgent.GetProduct(item.ProductId);
            item.AllowBackOrder = false;
            item.BackOrderMessage = string.Empty;

            //PRFT custom code: start
            int availableQty = _productAgent.GetInventoryFromERP(product.ProductNumber);

            var isB2BCustomer = new PRFTHelper().IsB2BUser();
            if (isB2BCustomer && qty > availableQty)
            {
                
                    item.AllowBackOrder = true;
                    item.BackOrderMessage = string.Format(Resources.ZnodeResource.BackOrderMessage,qty-availableQty);
            }
            //else
            //{
            //    if(!isB2BCustomer && qty > availableQty)
            //    {
            //        //qty = availableQty;
            //    }
            //}

            //PRFT custom code: End

            if (skumodel.Inventory.QuantityOnHand < qty &&
                (!product.AllowBackOrder.GetValueOrDefault() && product.TrackInventory.GetValueOrDefault()))
            {
                item.InsufficientQuantity = true;
                SaveInSession(MvcDemoConstants.CartModelSessionKey, cart);
                return false;
            }

            //PRFT Custom code:Start
            //Check inventory against ERP Available Quantity
            if (availableQty < qty && !isB2BCustomer)
            {
                item.InsufficientQuantity = true;
                item.Quantity = availableQty;
                SaveInSession(MvcDemoConstants.CartModelSessionKey, cart);
                return false;
            }
            //PRFT Custom code:End
            // update item and update the cart in Session and API.
            item.Quantity = qty;

            UpdateAccountInfo(cart);

            var shoppingCartModel = _shoppingCartsClient.CreateCart(cart);

            if (!Equals(shoppingCartModel, null))
            {

                foreach (CouponModel coupon in cart.Coupons)
                {
                    shoppingCartModel.Coupons.Add(coupon);
                }
                shoppingCartModel.GiftCardNumber = cart.GiftCardNumber;
                shoppingCartModel.GiftCardValid = cart.GiftCardValid;
                shoppingCartModel.GiftCardMessage = cart.GiftCardMessage;
                shoppingCartModel.GiftCardApplied = cart.GiftCardApplied;
                shoppingCartModel.GiftCardAmount = cart.GiftCardAmount;
                shoppingCartModel.GiftCardBalance = cart.GiftCardBalance;
            }

            SaveInSession(MvcDemoConstants.CartModelSessionKey, shoppingCartModel);
            return true;
        }

        /// <summary>
        /// Merge the cart after login
        /// </summary>
        /// <returns>Returns status of merge.</returns>
        public bool Merge()
        {
            bool returnStatus = false;

            var cart = GetFromSession<ShoppingCartModel>(MvcDemoConstants.CartModelSessionKey);

            var accountId = GetFromSession<AccountViewModel>(MvcDemoConstants.AccountKey).AccountId;
            var model = _shoppingCartsClient.GetCartByAccount(accountId);

            if (!Equals(model, null))
            {
                returnStatus = model.ShoppingCartItems.Any() && !Equals(cart, null) && cart.ShoppingCartItems.Any();
                if (!Equals(cart, null))
                { cart.ShoppingCartItems.ToList().ForEach(model.ShoppingCartItems.Add); }
            }
            else
            {
                if (Equals(cart, null)) { return false; }
                model = cart;
            }

            if (PortalAgent.CurrentPortal.PersistentCartEnabled)
            { model.Account = new AccountModel() { AccountId = accountId }; }

           
            try
            {
                //PRFT Custom Code : Start - When cart is fatching from session or from cookie we need to reasign all ERP values
                if (model != null && model.ShoppingCartItems != null && model.ShoppingCartItems.Count > 0)
                {
                    string associatedCustomerExternalId = HelperMethods.GetAssociatedCustomerExternalId();
                    string customerType = HelperMethods.GetERPCustomerType();
                    PRFTERPItemDetailsModel prftERPItemDetailModel = new PRFTERPItemDetailsModel();
                    IProductAgent productAgent = new ProductAgent();
                    bool isAnyItemRemovedFromCart = false;
                    for (int i = 0; i < model.ShoppingCartItems.Count(); i++)
                    {
                        model.ShoppingCartItems[i].ERPUnitPrice = model.ShoppingCartItems[i].UnitPrice;
                        bool isCallForPriceItem = false;
                        var productsViewModel = productAgent.GetProduct(model.ShoppingCartItems[i].ProductId);
                        if (!string.IsNullOrEmpty(associatedCustomerExternalId))
                        {
                            prftERPItemDetailModel = productAgent.GetItemDetailsFromERP(productsViewModel.ProductNumber, associatedCustomerExternalId, customerType);
                            if (prftERPItemDetailModel != null && prftERPItemDetailModel.HasError == false)
                            {
                                model.ShoppingCartItems[i].ERPUnitPrice = prftERPItemDetailModel.UnitPrice;
                                if (prftERPItemDetailModel.IsCallForPricing == true || prftERPItemDetailModel.UnitPrice <= 0)
                                {
                                    isCallForPriceItem = true;
                                    var currentItem = model.ShoppingCartItems[i];
                                    model.ShoppingCartItems.Remove(currentItem);
                                    i--;
                                    isAnyItemRemovedFromCart = true;
                                }
                            }
                        }

                        //Inventory Handling
                        //Get Inventory From ERP
                        if (!isCallForPriceItem)
                        {
                            int availableQty = _productAgent.GetInventoryFromERP(productsViewModel.ProductNumber);
                            var isB2BCustomer = new PRFTHelper().IsB2BUser();
                            if (availableQty <= 0 && !isB2BCustomer)
                            {
                                var currentItem = model.ShoppingCartItems[i];
                                model.ShoppingCartItems.Remove(currentItem);
                                i--;
                                isAnyItemRemovedFromCart = true;
                            }
                            else if(availableQty< model.ShoppingCartItems[i].Quantity && !isB2BCustomer)
                            {
                                model.ShoppingCartItems[i].Quantity = availableQty;
                            }
                        }
                    }

                    if (model.ShoppingCartItems.Count <= 0 && isAnyItemRemovedFromCart == true)
                    {
                        HttpContext.Current.Session["CallForPriceItemRemoved"] = true;
                    }
                }
                //PRFT Custom Code : End

                model = _shoppingCartsClient.CreateCart(model);
                
                //PRFT Custom Code: Start
                //CreateCart may return null in case model do not have any item based on above PRFT logic
                if(Equals(model,null))
                {
                    return false;
                }
                //PRFT Custom Code:End

                if (!Equals(cart, null) && !Equals(cart.Coupons, null))
                {
                    foreach (CouponModel coupon in cart.Coupons)
                    {
                        CouponModel couponModel = new CouponModel
                        {
                            Coupon = coupon.Coupon,
                            CouponApplied = coupon.CouponApplied,
                            CouponMessage = coupon.CouponMessage,
                            CouponValid = coupon.CouponValid
                        };
                        model.Coupons.Add(couponModel);
                    }
                }

            }
            catch (ZnodeException)
            {
                return false;
            }

            if (PortalAgent.CurrentPortal.PersistentCartEnabled)
            { model.Account = new AccountModel() { AccountId = accountId }; }

            if (returnStatus) { SaveInSession(MvcDemoConstants.CartMerged, true); }
            SaveInSession(MvcDemoConstants.CartModelSessionKey, model);

            RemoveCookie(MvcDemoConstants.CartCookieKey);

            return returnStatus;
        }

        /// <summary>
        /// Finds the couponCode is valid or invalid
        /// </summary>
        /// <param name="couponCode">CouponCode of the cart</param>
        /// <returns>Returns valid coupon or not</returns>
        public bool IsCouponValid(string couponCode)
        {
            var promotions = _promotionsClient.GetPromotions(null, null, null);

            if (Equals(promotions, null) || !promotions.Promotions.Any())
            {
                return false;
            }

            var now = DateTime.Now;

            return promotions.Promotions.Any(model => model.CouponCode.Equals(couponCode) && model.StartDate <= now && model.EndDate >= now);
        }

        /// <summary>
        /// Adds the Coupon Code to the Cart
        /// </summary>
        /// <param name="coupon">coupon applied on the Cart</param>
        /// <returns>Returns cart view model.</returns>
        public CartViewModel ApplyCoupon(string couponCode)
        {
            List<CouponModel> promoCodes = new List<CouponModel>();
            List<CouponViewModel> invalidCoupons = new List<CouponViewModel>();

            var cart = GetFromSession<ShoppingCartModel>(MvcDemoConstants.CartModelSessionKey) ??
                       GetCartFromCookie();

            if (cart == null)
                return new CartViewModel()
                {
                    HasError = true
                };

            if (!Equals(cart.Coupons, null))
            {
                foreach (CouponModel coupon in cart.Coupons)
                {
                    if (coupon.CouponApplied)
                    {
                        promoCodes.Add(coupon);
                    }
                }
            }
            if (!string.IsNullOrEmpty(couponCode))
            {
                if (PortalAgent.CurrentPortal.IsMultipleCouponAllowed)
                {

                    bool isCouponNotAvailableInCart = Equals(cart.Coupons.Where(p => Equals(p.Coupon, couponCode)).ToList().Count, 0);
                    if (isCouponNotAvailableInCart)
                    {
                        CouponModel newCoupon = new CouponModel { Coupon = couponCode };
                        cart.Coupons.Add(newCoupon);
                    }
                }
                else
                {
                    cart.Coupons.Clear();
                    CouponModel newCoupon = new CouponModel { Coupon = couponCode };
                    cart.Coupons.Add(newCoupon);
                }
            }
            else
            {
                invalidCoupons.Add(new CouponViewModel { Coupon = couponCode, CouponMessage = "Coupon Code cannot be empty.", CouponApplied = false, CouponValid = false });
            }

            var cartModel = Calculate(cart);

            bool cartCouponsApplied = (invalidCoupons.Count > 0 ? false : true);

            foreach (CouponViewModel couponViewModel in cartModel.Coupons)
            {
                if (couponViewModel.CouponApplied)
                {
                    cartCouponsApplied &= true;
                }
                else
                {
                    cartCouponsApplied &= false;
                    invalidCoupons.Add(couponViewModel);
                }
            }

            if (!cartCouponsApplied)
            {

                cart.Coupons.Clear();
                foreach (CouponModel couponModel in promoCodes)
                {
                    cart.Coupons.Add(couponModel);
                }
                cartModel = Calculate(cart);
                cartModel.HasError = true;
                foreach (CouponViewModel invalidCoupon in invalidCoupons)
                {
                    cartModel.Coupons.Add(invalidCoupon);
                }
            }

            return cartModel;
        }


        /// <summary>
        /// Set the Coupon Applied success and error message
        /// </summary>
        /// <param name="model">Cart View Model</param>
        /// <param name="success">Coupon applied or not</param>
        public void SetMessages(CartViewModel model)
        {
            SetStatusMessage(model);
        }

        /// <summary>
        /// This function will Removes all item of shopping cart from Session & Cookie.
        /// </summary>
        /// <returns>Returns true or false.</returns>
        public bool RemoveAllCartItems()
        {
            RemoveCookie(MvcDemoConstants.CartCookieKey);
            var cart = new ShoppingCartModel();
            UpdateAccountInfo(cart);
            var shoppingCartModel = _shoppingCartsClient.CreateCart(cart);
            SaveInSession(MvcDemoConstants.CartModelSessionKey, new ShoppingCartModel());
            return true;
        }

        /// <summary>
        /// Removes multiple cart items.
        /// </summary>
        /// <returns>Status of removal of multiple cart items</returns>
        public bool RemoveMultiCartItem()
        {
            RemoveInSession(MvcDemoConstants.MultiCartModelSessionKey);
            RemoveInSession(MvcDemoConstants.MultiCartShipmentSessionKey);

            AccountAgent _accountAgent = new AccountAgent();
            var accountModel = _accountAgent.GetAccountViewModel();
            if (!Equals(accountModel, null))
            { SaveInSession(MvcDemoConstants.ShippingAddressKey, accountModel.Addresses.FirstOrDefault(x => x.IsDefaultShipping)); }

            return true;
        }

        /// <summary>
        /// Gets a promotion URL enabled value having coupon code equals to the required coupon code.
        /// </summary>
        /// <param name="CouponCode">Coupon code required.</param>
        /// <returns>Returns Coupon URL enabled value.</returns>
        public bool IsCouponUrlEnabled(string couponCode)
        {
            if (!string.IsNullOrEmpty(couponCode))
            {
                PromotionModel promotionModel = _promotionsClient.GetPromotions(null, null, null).Promotions.Where(promotion => Equals(promotion.CouponCode, couponCode)).FirstOrDefault();
                return promotionModel.EnableCouponUrl.GetValueOrDefault();
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Get the cart items based on the cookie mapping id
        /// </summary>
        /// <returns>Returns the shopping cart items Shopping Cart Model</returns>
        private ShoppingCartModel GetCartFromCookie()
        {
            if (!PortalAgent.CurrentPortal.PersistentCartEnabled)
            { return null; }

            var cookieValue = GetFromCookie(MvcDemoConstants.CartCookieKey);
            if (!string.IsNullOrEmpty(cookieValue))
            {
                int cookieId;
                if (int.TryParse(cookieValue, out cookieId))
                {
                    try
                    {
                        var shoppingCartModel = _shoppingCartsClient.GetCartByCookie(cookieId);

                        if (!Equals(shoppingCartModel, null))
                        {
                            return shoppingCartModel;
                        }
                    }
                    catch (Exception)
                    {

                    }

                    return null;
                }
            }
            return null;
        }
    }
}