﻿using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Agents
{
    public partial interface ICategoryAgent
    {
        CategoryListViewModel GetCategories();
        CategoryViewModel GetCategory(int categoryId);

        /// <summary>
        /// Get category view model.
        /// </summary>
        /// <param name="categoryName">Name of the category.</param>
        /// <param name="showSubCategory">Show sub category.</param>
        /// <param name="categoryId">Category Id.</param>
        /// <returns>Category view model.</returns>
        CategoryViewModel GetCategoryByName(string categoryName, bool showSubCategory = false, int categoryId = 0);

        /// <summary>
        /// Gets the list of Sub Categories.
        /// </summary>
        /// <param name="model">Category view model of the parent category.</param>
        /// <param name="searchRequestModel">Search request model for search.</param>
        /// <returns>List of Sub Categories.</returns>
        SubcategoriesViewModel GetSubCategories(CategoryViewModel model, SearchRequestModel searchRequestModel);

        /// <summary>
        /// Get the category for products
        /// </summary>
        /// <param name="result">Keyword Search View Model</param>
        /// <param name="category">current category Name</param>
        /// <param name="categoryDetails">Category View Model</param>
        void GetProductCategory(KeywordSearchViewModel result, string category, CategoryViewModel categoryDetails);

        /// <summary>
        /// Get theme name by theme id.
        /// </summary>
        /// <param name="themeId">Id of the theme.</param>
        /// <returns>Theme name.</returns>
        string GetThemeNameById(int themeId);

        /// <summary>
        /// Get BreadCrumb
        /// </summary>
        /// <param name="categoryId">Id of the Category</param>
        /// <param name="seoUrl">seoUrl</param>
        /// <param name="isProductPresent">isProductPresent</param>
        /// <returns>BreadCrumb String</returns>
        string GetBreadCrumb(int categoryId, string seoUrl, bool isProductPresent);

        /// <summary>
        /// Gets site map category list.
        /// </summary>
        /// <returns>Category List View Model containing parent category nodes.</returns>
        CategoryListViewModel GetSiteMapDetails();
    }
}