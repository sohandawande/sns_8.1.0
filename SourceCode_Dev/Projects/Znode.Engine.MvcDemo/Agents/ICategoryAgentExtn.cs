﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Agents
{
    public partial interface ICategoryAgent
    {
        /// <summary>
        /// Get the category based on the category name.
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns>Returns the category as category view model</returns>
        CategoryViewModel GetExpandedCategoryByName(string categoryName);
    }
}
