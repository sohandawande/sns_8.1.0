﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.Helpers;
using Znode.Engine.MvcDemo.Maps;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Agents
{
    public class SkuAgent : BaseAgent, ISkuAgent
    {
        private readonly ISkusClient _client;

        public SkuAgent()
        {
            _client = GetClient<SkusClient>();
        }

        /// <summary>
        /// Get the SKUs Price
        /// </summary>
        /// <param name="productId">Product ID</param>
        /// <param name="attributeId">Attribute ID</param>
        /// <param name="productPrice">Product Price</param>
        /// <param name="selectedSku">Selected SKU</param>
        /// <param name="imageMediumPath">Selected imageMediumPath</param>
        /// <returns>Returns the SKU Price</returns>
        public Decimal GetSkusPrice(int productId, int attributeId, decimal productPrice, out string selectedSku, out string imagePath, out string imageMediumPath)
        {
            decimal SKUPrice = productPrice;

            Expands = new ExpandCollection();
            Expands.Add(ExpandKeys.Attributes);
            Expands.Add(ExpandKeys.Promotions);
            Filters = new FilterCollection();
            Filters.Add(FilterKeys.ProductId, FilterOperators.Equals, productId.ToString());
            var list = _client.GetSkus(Expands, Filters, null, null, null);

            if (list != null)
            {
                foreach (var sku in list.Skus)
                {
                    var attributeSelected =
                        sku.Attributes.FirstOrDefault(x => x.AttributeId.Equals(attributeId));
                    if (attributeSelected != null)
                    {
                        selectedSku = sku.Sku;
                        imagePath = HelperMethods.GetImagePath(sku.ImageLargePath);
                        //ZNode Version 7.2.2 - Assign SkU Medium image Path
                        imageMediumPath = HelperMethods.GetImagePath(sku.ImageMediumPath);

                        if (sku.PromotionPrice.HasValue)
                        {
                            SKUPrice = sku.PromotionPrice.Value;
                            return SKUPrice;
                        }

                        if (sku.SalePriceOverride.HasValue)
                        {
                            SKUPrice = sku.SalePriceOverride.Value;
                            return SKUPrice;
                        }

                        if (sku.RetailPriceOverride.HasValue)
                        {
                            SKUPrice = sku.RetailPriceOverride.Value;
                            return SKUPrice;
                        }

                        return SKUPrice;
                    }
                }
            }

            selectedSku = (list != null) ? list.Skus.First().Sku : string.Empty;
            imagePath = (list != null) ? list.Skus.First().ImageLargePath : string.Empty;
            //ZNode Version 7.2.2 - Add imageMediumPath parameter
            imageMediumPath = (list != null) ? list.Skus.First().ImageMediumPath : string.Empty;
            return productPrice;
        }

        /// <summary>
        /// Get SKU with inventory for the selected attribute
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="attributeId"></param>
        /// <returns></returns>
        public SkuModel GetSkuInventory(int productId, string attributeId)
        {
            Expands = new ExpandCollection { ExpandKeys.Inventory, ExpandKeys.Attributes };

            Filters = new FilterCollection { { FilterKeys.ProductId, FilterOperators.Equals, productId.ToString() } };
            var list = _client.GetSkus(Expands, Filters, null, null, null);

            if (list.Skus.Count == 1 && !list.Skus[0].Attributes.Any())
            {
                return list.Skus.First();
            }

            var attributeIds = attributeId.Split(',');
            if (!Equals(attributeIds[0],string.Empty))
            {
                return (from sku in list.Skus
                        where attributeIds.All(x => sku.Attributes.Any(y => Convert.ToInt32(x) == y.AttributeId))
                        select sku).FirstOrDefault();
            }
            else
            {
                return new SkuModel();
            }            

        }

        /// <summary>
        /// Get SKU Model with inventory based on given Product Id and SKU
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="sku"></param>
        /// <returns></returns>
        public SkuModel GetBySku(int productId, string sku)
        {
            Expands = new ExpandCollection {ExpandKeys.Inventory, ExpandKeys.Attributes};

            Filters = new FilterCollection
            {
                {FilterKeys.ProductId, FilterOperators.Equals, productId.ToString()},
                {FilterKeys.Sku, FilterOperators.Equals, sku}
            };

            var list = _client.GetSkus(Expands, Filters, null, null, null);

            return (list != null && list.Skus.Any()) ? list.Skus.First() : null;
        }
    }
}