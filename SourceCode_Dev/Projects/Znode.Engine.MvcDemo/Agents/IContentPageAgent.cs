﻿
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.MvcDemo.ViewModels;
namespace Znode.Engine.MvcDemo.Agents
{
    /// <summary>
    /// Interface for Content Page Agent
    /// </summary>
    public interface IContentPageAgent
    {
        /// <summary>
        /// Get Content page by content page name and its extension.
        /// </summary>
        /// <param name="contentPageName">Name of content page.</param>
        /// <param name="extension">Extension for content page file.</param>
        /// <returns>Returns string of content page data.</returns>
        string GetContentPageByName(string contentPageName, string extension);

        /// <summary>
        /// Get all content page details.
        /// </summary>
        /// <param name="filters">Filter Collection</param>
        /// <returns>Returns list of all content pages.</returns>
        ContentPageListViewModel GetContentPageDetails(FilterCollection filters = null);
    }
}
