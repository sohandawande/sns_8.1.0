﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Web.Mvc;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Agents
{
    public partial interface IProductAgent
    {
        ProductViewModel GetProduct(int productId);
        ProductAttributesViewModel UpdateProductAttributes(ProductViewModel product, int? attributeId, string selectedAttributeIds);
        IEnumerable<ProductViewModel> GetProductByIds(string ids);
        decimal GetSkuPrice(int attributeId, int productId);
        decimal GetExtendedPrice(int productId, int quantity, string sku, decimal prodPrice);
        void CheckInventory(ProductViewModel model, string attributeId, int? quantity);
        int GetOrderedItemQuantity(string sku);
        //ZNode Version 7.2.2 - Add imageMediumPath Out parameter
        ProductViewModel GetAttributes(int? id, int? attributeId, string selectedIds, int? quantity, out decimal prodPrice, out string selectedSku, out string imagePath, out string imageMediumPath);
        Collection<ProductViewModel> GetHomeSpecials();
        BundleDetailsViewModel GetBundleProductsDetails(CartItemViewModel cartItemViewModel);
        List<BundleDisplayViewModel> GetBundles(string bundleProductIds);
        WishListViewModel CreateWishList(int productId);
        IEnumerable<AddOnViewModel> GetAddOnDetails(ProductViewModel product, string selectedAddOnIds, int? quantity, decimal productPrice, out decimal finalProductPrice, string selectedSku = null);
        void SetMessages(ProductViewModel model);
        ReviewItemViewModel CreateReview(ReviewItemViewModel model);
        ReviewViewModel GetAllReviews(int productId);

        BundleDetailsViewModel GetUpdatedBundles(BundleDetailsViewModel bundleCartItem);
        Task<IEnumerable<ProductViewModel>> GetProductByIdsAsyn(string ids, int accountId);
        Task<Collection<ProductViewModel>> GetHomeSpecialsAsync();
        string GetProductUrl(int id, UrlHelper urlHelper);

        /// <summary>
        /// This method add productId and categoryId to the current session
        /// for global level comparison
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <param name="categoryId">int categoryId</param>
        /// <param name="message">strung message</param>
        /// <param name="errorCode">int errorCode</param>
        /// <returns>Returns boolean value true/false</returns>
        bool GlobalAddProductToCompareList(int productId, int categoryId, out string message, out int errorCode);

        /// <summary>
        /// This method add productId and categoryId to the current session 
        /// for category level product comparison
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <param name="categoryId">int categoryId</param>
        /// <param name="message">strung message</param>
        /// <returns>Returns boolean value true/false</returns>
        bool CategoryAddProductToCompareList(int productId, int categoryId, out string message);

        /// <summary>
        /// This method returns list of products added for comparison
        /// </summary>
        /// <returns>Returns Collection of ProductViewModel</returns>
        Collection<ProductViewModel> GetCompareProducts();

        /// <summary>
        /// Get the sku product list by sku.
        /// </summary>
        /// <param name="sku">sku of the product.</param>
        /// <returns>List of sku product.</returns>
        List<SkuProductViewModel> GetSkuProductListBySKU(string sku);

        /// <summary>
        /// Get the details of products added in session
        /// </summary>
        /// <returns>List of ProductViewModel</returns>
        Collection<ProductViewModel> GetCompareProductsDetails(CompareProductViewModel compareProducts);

        /// <summary>
        /// Remove a perticular product from compare list
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <returns>Boolean value true/false</returns>
        bool RemoveProductFormSession(int productId);

        /// <summary>
        /// Remove all the products from session.
        /// </summary>
        /// <returns>Boolean value true/false</returns>
        bool DeleteComparableProducts();

        /// <summary>
        /// Add multiple products to cart.
        /// </summary>
        /// <param name="cartItems">Cart item view models to add to cart.</param>
        /// <returns>Returns error message.</returns>
        string AddMultipleProductsToCart(List<CartItemViewModel> cartItems);

        /// <summary>
        /// Send Mail of compared product to friend
        /// </summary>
        /// <param name="compareProduct">Compare Product View Model contains productIds</param>
        /// <returns>Boolean value true/false</returns>
        bool SendComparedProductMail(CompareProductViewModel compareProduct);

        /// <summary>
        /// Get all associated product highlights on the basis of product id.
        /// </summary>
        /// <param name="productId">Product Id</param>
        /// <param name="filters">FilterCollection filters</param>
        /// <returns></returns>
        HighlightListViewModel GetProductHighlights(int productId, FilterCollection filters);

        /// <summary>
        /// Send Mail to firnd about Product
        /// </summary>
        /// <param name="product">Compare Product View Model</param>
        /// <returns>Boolean value true/false</returns>
        bool SendMailToFriend(CompareProductViewModel product);


        /// <summary>
        /// Get the BundleDetails of product
        /// </summary>
        /// <param name="model">Product View Model</param>
        /// <returns>Bundle Details View Model</returns>
        BundleDetailsViewModel GetCartModel(ProductViewModel model);
    }
}