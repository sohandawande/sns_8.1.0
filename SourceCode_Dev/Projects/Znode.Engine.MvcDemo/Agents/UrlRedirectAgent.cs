﻿using System;
using System.Linq;
using System.Web;
using System.Web.Caching;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.Helpers;

namespace Znode.Engine.MvcDemo.Agents
{
    public class UrlRedirectAgent : BaseAgent, IUrlRedirectAgent
    {
        #region Private Variables
        private readonly IUrlRedirectClient _client;
        #endregion

        #region Constructor
        public UrlRedirectAgent()
        {
            _client = GetClient<UrlRedirectClient>();
        }
        #endregion

        #region Public Methods
        public UrlRedirectListModel GetActive301Redirects()
        {
            Filters = new FilterCollection { { FilterKeys.IsActive, FilterOperators.Equals, "true" } };

            return _client.Get301Redirects(null, Filters, new SortCollection(), null, null);
        }

        public static UrlRedirectModel Has301Redirect(string url)
        {
            UrlRedirectListModel redirects = GetActive301RedirectsFromCache();

            if (Equals(redirects, null) || Equals(redirects.UrlRedirects, null))
            {
                return null;
            }

            var item = redirects.UrlRedirects.FirstOrDefault(x => x.OldUrl.ToLower() == url.ToLower());

            return item;
        }
        #endregion

        #region Private Methods
        private static UrlRedirectListModel GetActive301RedirectsFromCache()
        {
            const string cacheKey = "301Redirects";
            if (Equals(HttpContext.Current.Cache[cacheKey], null))
            {
                UrlRedirectAgent agent = new UrlRedirectAgent();
                UrlRedirectListModel model = agent.GetActive301Redirects();

                if (!Equals(model, null))
                {
                    CacheDurationSettings cacheDurationSetting = CacheDurationSettings.GetCacheDurationSettingsByMappingKey("301URLRedirectCacheDuration");
                    HttpContext.Current.Cache.Insert(cacheKey, model, null, DateTime.Now.AddMinutes(Equals(cacheDurationSetting, null) ? 0 : Convert.ToDouble(cacheDurationSetting.Value)),
                                                     Cache.NoSlidingExpiration);
                }
            }

            return HttpContext.Current.Cache[cacheKey] as UrlRedirectListModel;
        }
        #endregion
    }
}