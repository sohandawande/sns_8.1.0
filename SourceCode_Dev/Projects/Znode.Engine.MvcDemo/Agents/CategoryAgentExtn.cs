﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.MvcDemo.Maps;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Agents
{
    public partial class CategoryAgent
    {
        /// <summary>
        /// Get the category based on the category name.
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns>Returns the category as category view model</returns>
        public CategoryViewModel GetExpandedCategoryByName(string categoryName)
        {
            var expand = new ExpandCollection()
            {
                ExpandKeys.Images,
                ExpandKeys.Subcategories,
                ExpandKeys.CategoryNodes,
                ExpandKeys.Facets,
            };
            var categoryList = _categoriesClient.GetCategories(expand, new FilterCollection()
                        {
                            new FilterTuple(FilterKeys.Name, FilterOperators.Equals,categoryName),

                        }, null);

            if (categoryList != null && categoryList.Categories != null)
            {
                var categoryDetails = CategoryViewModelMap.ToCategoryViewModel(categoryList.Categories.First());
                if (categoryDetails != null)
                {
                    categoryDetails.Title =
                          !string.IsNullOrEmpty(categoryDetails.SeoTitle)
                                 ? categoryDetails.SeoTitle
                                 : categoryName;
                    return categoryDetails;
                }
            }

            return new CategoryViewModel() { HasError = true, ErrorMessage = Resources.ZnodeResource.ErrorNoCategoryFound };
        }
    }
}