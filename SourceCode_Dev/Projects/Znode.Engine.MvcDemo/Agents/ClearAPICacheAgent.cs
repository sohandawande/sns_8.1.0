﻿using Znode.Engine.Api.Client;

namespace Znode.Engine.MvcDemo.Agents
{
    /// <summary>
    /// This is the class which will deal with the Clear API functionality
    /// </summary>
    public class ClearAPICacheAgent : BaseAgent, IClearAPICacheAgent
    {
        #region Private Variables

        private readonly IClearCacheClient _clearCacheClient;

        #endregion

        #region Constructor

        public ClearAPICacheAgent()
        {
            _clearCacheClient = GetClient<ClearCacheClient>(); 
        }

        #endregion

        #region Public Methods

        public bool ClearApiCache()
        {
            return _clearCacheClient.ClearAPICache();
        }

        #endregion
    }
}