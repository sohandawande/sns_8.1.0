﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.MvcDemo.Maps;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Agents
{
    public partial class AccountAgent : BaseAgent, IAccountAgent
    {
        public AccountListModel GetSubUserAccounts(int parentAccountId, PRFTSubUsersRequestModel subUsersRequestModel)
        {
            var filterCollection = new FilterCollection();
            var parentAccountNoFilter = new FilterTuple(FilterKeys.ParentAccountId, FilterOperators.Equals, subUsersRequestModel.ParentAccountId.ToString());
            //var superUserfilter = new FilterTuple(FilterKeys.Custom2, FilterOperators.NotEquals, "1");
            var searchTerm = string.Empty;
            if (!string.IsNullOrEmpty(subUsersRequestModel.searchTerm))
            {
                searchTerm = subUsersRequestModel.searchTerm.ToLower();
            }


            filterCollection.Add(parentAccountNoFilter);
            //filterCollection.Add(superUserfilter);

            if (subUsersRequestModel.RefineBy.Count > 0)
            {
                //searchTerm = subUsersRequestModel.RefineBy[0].Value.ToString();
                //var filterUsername = new FilterTuple(FilterKeys.Username, FilterOperators.Equals, searchterm.ToString());
                //filterCollection.Add(filterUsername);
                subUsersRequestModel.RefineBy.Clear();
            }

            //var pagenumber = subUsersRequestModel.PageNumber.GetValueOrDefault(0) > 1 ? subUsersRequestModel.PageNumber.Value - 1 : 0;
            //var userList = _accountClient.GetAccounts(GetExpands(), filterCollection, GetSortCollection(subUsersRequestModel.Sort), pagenumber, subUsersRequestModel.PageSize.GetValueOrDefault(10));
            var userList = _customerUserMappingClient.GetSubUserList(parentAccountId, GetExpands(), filterCollection, GetSortCollection(subUsersRequestModel.Sort), subUsersRequestModel.PageNumber.GetValueOrDefault(1), subUsersRequestModel.PageSize.GetValueOrDefault(10));

            if (userList != null && userList.Accounts != null && userList.Accounts.Count > 0)
            {
                //userList.Accounts.ToList().ForEach(x =>
                //{
                //    if (x.Custom2 == "1" || x.User == null)
                //    {
                //        userList.Accounts.Remove(x);
                //    }


                //});

                //if (!string.IsNullOrEmpty(searchTerm))
                //{
                //    userList.Accounts.ToList().ForEach(x =>
                //    {
                //        string UserName = x.UserName;
                //        if (!x.User.Username.ToLower().Contains(searchTerm))
                //        {
                //            userList.Accounts.Remove(x);
                //        }
                //        else if (!string.IsNullOrEmpty(UserName) && !UserName.ToLower().Contains(searchTerm))
                //        {
                //            userList.Accounts.Remove(x);
                //        }
                //    });
                //}

                if (!string.IsNullOrEmpty(searchTerm))
                {
                    userList.Accounts.ToList().ForEach(x =>
                    {
                        string UserName = x.AccountId.ToString();
                        if (!x.AccountId.ToString().ToLower().Contains(searchTerm))
                        {
                            userList.Accounts.Remove(x);
                        }
                        else if (!string.IsNullOrEmpty(UserName) && !UserName.ToLower().Contains(searchTerm))
                        {
                            userList.Accounts.Remove(x);
                        }
                    });
                }

                //var superUser = userList.Accounts.Where(x => x.Custom3 == "1").First();
                //userList.Accounts.Remove(superUser);

                return userList;
            }

            return new AccountListModel();
        }

        /// <summary>
        /// Signup based on the details given.
        /// </summary>
        /// <param name="model">Login details as Register View Model</param>
        /// <returns>Created account or not</returns>
        public PRFTRegisterSubUserViewModel RegisterSubUser(PRFTRegisterSubUserViewModel model)
        {   
            try
            {
                var registerModel = _accountClient.CreateAccount(AccountViewModelMap.ToRegisterSubUserModel(model));

                var viewModel = AccountViewModelMap.ToAccountViewModel(registerModel);
                //SaveInSession(MvcDemoConstants.AccountKey, viewModel);
                return PRFTRegisterSubUserViewModelMap.ToModel(viewModel);
            }
            catch (ZnodeException exception)
            {
                if (exception.ErrorCode == ErrorCodes.UserNameUnavailable)
                {
                    return new PRFTRegisterSubUserViewModel() { HasError = true, ErrorMessage = model.UserName + " User Name is  not available" };
                }


            }

            return new PRFTRegisterSubUserViewModel() { HasError = true, ErrorMessage = Resources.ZnodeResource.ErrorAccountCreation };

        }


        public AccountViewModel GetAccountByAccountId(int accountId)
        {
            AccountViewModel account = new AccountViewModel();

            var model = _accountClient.GetAccount(accountId, GetExpands());

            account = AccountViewModelMap.ToAccountViewModel(model);

            return account;
        }

        public AccountViewModel UpdateUserAccount(AccountViewModel model)
        {
            AccountViewModel viewModel = new AccountViewModel();
            try
            {
                var account = _accountClient.UpdateCustomerAccount(AccountViewModelMap.ToAccountModel(model));
                viewModel = AccountViewModelMap.ToAccountViewModel(account);
            }
            catch (ZnodeException ex)
            {
                switch (ex.ErrorCode)
                {
                    case ErrorCodes.CustomerAccountError:
                        return new AccountViewModel { HasError = true, ErrorMessage = ex.ErrorMessage };
                    default:
                        return viewModel;
                }
            }
            return viewModel;
        }

        /// <summary>
        /// Get the details for a particular order of Subuser
        /// </summary>
        /// <param name="orderId">Order ID</param>
        /// <returns>Return Order Details of Subuser</returns>
        public OrdersViewModel GetSubUserOrderReceiptDetails(int orderId, int accountId)
        {
            var accountViewModel = GetAccountByAccountId(accountId);

            if (!Equals(accountViewModel, null) && accountViewModel.Orders.Any())
            {
                FilterCollection filters = new FilterCollection();
                filters.Add(new FilterTuple(FilterKeys.OrderId, FilterOperators.Equals, orderId.ToString()));

                var model = accountViewModel.Orders.FirstOrDefault(x => x.OrderId == orderId);
                OrderModel orderModel = _ordersClient.GetOrderDetails(null, filters, null, null, null);

                if (!Equals(model, null) && !Equals(orderModel, null) && !string.IsNullOrEmpty(orderModel.PaymentMethod))
                {
                    model.PaymentMethod = orderModel.PaymentMethod;
                }

                Collection<OrderLineItemViewModel> _lineItemsList = new Collection<OrderLineItemViewModel>();
                foreach (var _lineItems in orderModel.OrderLineItemList.OrdersLineItems)
                {
                    if (Equals(_lineItems.ParentOrderLineItemId, null))
                    {
                        foreach (var _accountLineItem in model.OrderLineItems)
                        {
                            if (Equals(_accountLineItem.ParentOrderLineItemId, null) && _accountLineItem.OrderLineItemId.Equals(_lineItems.OrderLineItemId))
                            {
                                _accountLineItem.ShippingAddress = _lineItems.ShippingAddress;
                                _accountLineItem.ShippingId = Convert.ToInt32(_lineItems.OrderShipmentId);
                                _accountLineItem.DigitalAsset = _lineItems.DigitalAsset;
                                _lineItemsList.Add(_accountLineItem);
                            }
                        }
                    }

                }

                return model;
            }

            return new OrdersViewModel() { HasError = true, ErrorMessage = "No Order Found" };

        }



        /// <summary>
        /// Get the Sort Collection
        /// </summary>
        /// <param name="sortBy">Sort By</param>
        /// <returns>Returns the sort collection object</returns>
        private SortCollection GetSortCollection(string sortBy)
        {
            var sortCollection = new SortCollection();

            if (!string.IsNullOrEmpty(sortBy))
            {
                sortCollection.Add(sortBy.Split('|')[0], sortBy.Split('|')[1]);
            }

            return sortCollection;
        }

        #region CustomerUserMapping
        public bool SaveAssociateCustomer(int accountId, string customerAccountIds)
        {
            return Equals(_customerUserMappingClient.UserAssociatedCustomers(accountId, customerAccountIds), null) ? false : true;
        }
        #endregion
    }
}