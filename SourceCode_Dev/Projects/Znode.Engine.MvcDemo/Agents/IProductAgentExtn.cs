﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Agents
{
    public partial interface IProductAgent
    {
        /// <summary>
        /// Get page sizes options for binding drop down list.
        /// </summary>        
        IEnumerable<SelectListItem> GetPageSizesItems();

        /// <summary>
        /// Get Sku's by Product Id
        /// </summary>       
        PRFTSkuListViewModel GetProductSkuByProductId(int productId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// Get product details from ERP
        /// </summary>
        /// <param name="productNum"></param>
        /// <param name="associatedCustomerExternalId"></param>
        /// <param name="customerType"></param>
        /// <returns></returns>
        PRFTERPItemDetailsModel GetItemDetailsFromERP(string productNum, string associatedCustomerExternalId, string customerType);

        /// <summary>
        /// Get Inventory details from ERP based on Product Number
        /// </summary>
        /// <param name="productNum"></param>
        /// <returns></returns>
        int GetInventoryFromERP(string productNum);

        void CheckInventoryAgainstERP(ProductViewModel model, string attributeId, int? quantity);
    }
}
