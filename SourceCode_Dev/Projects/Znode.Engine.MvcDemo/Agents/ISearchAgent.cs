﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Agents
{
    public interface ISearchAgent
    {
        List<SuggestedSearchModel> GetSuggestions(string searchTerm, string category);
        KeywordSearchViewModel Search(SearchRequestModel searchRequestModel);
        Collection<ProductModel> GetProductsByIds(List<string> ids);

        /// <summary>
        /// To Get the SEO Url details.
        /// </summary>
        /// <param name="seoUrl">Seo Url of Product or Category</param>
        /// <returns>Return the SEO Url Details like ProductId, CategoryName in SEOUrlViewModel format.</returns>
        SEOUrlViewModel GetSeoUrlDetail(string seoUrl);
    }
}