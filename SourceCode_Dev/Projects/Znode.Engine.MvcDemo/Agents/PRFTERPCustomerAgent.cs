﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Client;
using Znode.Engine.Exceptions;
using Znode.Engine.MvcDemo.Maps;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Agents
{
    public class PRFTERPCustomerAgent : BaseAgent, IPRFTERPCustomerAgent
    {
        private readonly IPRFTERPAccountsClient _erpAccountClient;

        public PRFTERPCustomerAgent()
        {
            _erpAccountClient = new PRFTERPAccountsClient();
        }
        public PRFTERPAccountDetailsViewModel GetAccountDetailsFromERP(string externalId)
        {
            try
            {
                var erpAccountDetails = _erpAccountClient.GetAccountDetailsFromERP(externalId);
                var  viewModel = PRFTERPCustomerAccountModelMap.ToERPCustomerAccountViewModel(erpAccountDetails);
                return viewModel;
            }
            catch (ZnodeException)
            {
                return new PRFTERPAccountDetailsViewModel() { HasError = true };
            }
        }
    }
}