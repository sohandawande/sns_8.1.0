﻿using System;
using System.Linq;
using System.Web;
using System.Web.Caching;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.MvcDemo.Helpers;
using Znode.Engine.MvcDemo.Maps;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Agents
{
    public class PortalAgent : BaseAgent
    {
        private readonly PortalsClient _portalClient;
        private readonly DomainsClient _domainClient;

        public PortalAgent()
        {
            _portalClient = GetClient<PortalsClient>();
            _domainClient = GetClient<DomainsClient>();
        }

        public static PortalViewModel CurrentPortal
        {
            get
            {
                if (HttpContext.Current.Cache[HttpContext.Current.Request.Url.Authority] == null)
                {
                    var agent = new PortalAgent();
                    var model = agent.GetCurrentPortal();

                    if (model != null)
                    {
                        CacheDurationSettings cacheDurationSetting = CacheDurationSettings.GetCacheDurationSettingsByMappingKey("CurrentPortalCacheDuration");

                        HttpContext.Current.Cache.Insert(HttpContext.Current.Request.Url.Authority, model, null, DateTime.Now.AddMinutes(Equals(cacheDurationSetting, null) ? 0 : Convert.ToDouble(cacheDurationSetting.Value)),
                                                         Cache.NoSlidingExpiration);
                    }
                }

                return HttpContext.Current.Cache[HttpContext.Current.Request.Url.Authority] as PortalViewModel;
            }
        }

        private PortalViewModel GetCurrentPortal()
        {
            var list =
                _domainClient.GetDomains(
                    new FilterCollection()
                        {
                            new FilterTuple(FilterKeys.DomainName, FilterOperators.Equals,
                                            HttpContext.Current.Request.Url.Authority)
                        }, new SortCollection());

            if (list.Domains.Any())
            {
                var portal = _portalClient.GetPortal(list.Domains.First().PortalId, new ExpandCollection() { ExpandKeys.PortalCatalogs, ExpandKeys.Catalogs, ExpandKeys.CurrencyType });
                return PortalViewModelMap.ToModel(portal);
            }

            return null;
        } 
    }
}