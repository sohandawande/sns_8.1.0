﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Web;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.Maps;
using Znode.Engine.MvcDemo.ViewModels;
using Znode.Engine.Api.Client;
using Znode.Engine.MvcDemo.Helpers;
namespace Znode.Engine.MvcDemo.Agents
{
    public class ShippingOptionAgent : BaseAgent, IShippingOptionAgent
    {
        private readonly IAccountAgent _accountAgent;
        private readonly IShippingOptionsClient _shippingOptionsClient;

        public ShippingOptionAgent()
        {
            _shippingOptionsClient = GetClient<ShippingOptionsClient>();
            _accountAgent = new AccountAgent();
        }

        /// <summary>
        /// Get the shipping List for current logged in account profile
        /// </summary>        
        /// <returns>Returns the shipping list based on the profile id</returns>
        public ShippingOptionListViewModel GetShippingList()
        {
            AddressViewModel shippingCountry = GetShippingAddess();           
            ShippingOptionListModel allList = new ShippingOptionListModel();
            ShippingOptionListModel countryFilteredList = new ShippingOptionListModel();
            if (Equals(shippingCountry, null))
            {
                 allList = _shippingOptionsClient.GetShippingOptions(null, new FilterCollection()
                        {
                            new FilterTuple(FilterKeys.ProfileId, FilterOperators.Equals, "null"),
                            new FilterTuple(FilterKeys.IsActive, FilterOperators.Equals, "true" ),
                        }, null);

            }
            else
            {
                string shippingCountryCode = shippingCountry.CountryCode;
                // Get All profile shipping options.
                 allList = _shippingOptionsClient.GetShippingOptions(null, new FilterCollection()
                        {
                            new FilterTuple(FilterKeys.ProfileId, FilterOperators.Equals, "null"),
                            new FilterTuple(FilterKeys.IsActive, FilterOperators.Equals, "true" ),
                            new FilterTuple(FilterKeys.CountryCode, FilterOperators.Equals, shippingCountryCode )

                        }, null);
            }

            var accountModel = _accountAgent.GetAccountViewModel();

            if (allList == null)
                allList = new ShippingOptionListModel();

            if (allList.ShippingOptions == null)
                allList.ShippingOptions = new Collection<ShippingOptionModel>();

            // Get Account ProfileID, or Get Default Anonymous ProfileId from Portal
            int? profileId = null;
            if (accountModel != null)
            {
                profileId = accountModel.ProfileId;
            }
            else
            {
                profileId = PortalAgent.CurrentPortal.DefaultAnonymousProfileId;
            }
            
            // Get Profile based options and merge with All Profile options.
            if (profileId.HasValue)
            {
                foreach (var item in allList.ShippingOptions)
                {
                    if (Equals(item.ProfileId, profileId) || Equals(item.ProfileId, null))
                    {
                        countryFilteredList.ShippingOptions.Add(item);
                    }
                }

                var profileList = _shippingOptionsClient.GetShippingOptions(null,
                    new FilterCollection()
                        {
                            new FilterTuple(FilterKeys.ProfileId, FilterOperators.Equals, profileId.ToString()),
                            new FilterTuple(FilterKeys.IsActive, FilterOperators.Equals, "true")
                        },
                    new SortCollection());

                if (profileList.ShippingOptions != null && profileList.ShippingOptions.Any())
                {
                    countryFilteredList.ShippingOptions = new Collection<ShippingOptionModel>(profileList.ShippingOptions.Union(countryFilteredList.ShippingOptions).Distinct().Except(profileList.ShippingOptions).ToList());
                }
            }
            ShippingOptionListViewModel shippingOptionListViewModel = ShippingOptionMap.ToViewModel(countryFilteredList.ShippingOptions.OrderBy(x => x.DisplayOrder).ToList());
            
            var selectedShippingCode = Equals(GetFromSession<ShoppingCartModel>(MvcDemoConstants.CartModelSessionKey), null) ? shippingOptionListViewModel.ShippingOptions.First().ShippingId : GetFromSession<ShoppingCartModel>(MvcDemoConstants.CartModelSessionKey).Shipping.ShippingOptionId;

            shippingOptionListViewModel.ShippingOptions.Select(c => { c.SelectedShippingCode = selectedShippingCode; return c; }).ToList();
            return shippingOptionListViewModel;
            
        }

        /// <summary>
        /// Get the shipping options based on the shipping option id
        /// </summary>
        /// <param name="shippingOptionId">Shipping Option ID</param>
        /// <returns>Returns the Shipping Option as Shipping Option View model</returns>
        public ShippingOptionViewModel GetShippingOption(int shippingOptionId)
        {
            var shippingOption = _shippingOptionsClient.GetShippingOption(shippingOptionId, null);

            return ShippingOptionMap.ToViewModel(shippingOption);
        }


        #region Private Method
        private AddressViewModel GetShippingAddess()
        {
            var model = GetFromSession<AddressViewModel>(MvcDemoConstants.ShippingAddressKey);
            if (Equals(model, null))
            {
                var accountModel = _accountAgent.GetAccountViewModel();
                if (!Equals(accountModel, null))
                    model = accountModel.Addresses.FirstOrDefault(x => x.IsDefaultShipping);
            }
            return model;
        }
        #endregion
    }
}