﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Agents
{
    public interface IPRFTCreditApplicationAgent
    {
        /// <summary>
        /// Get List of states
        /// </summary>
        /// <returns></returns>
        Collection<StateModel> GetStates();

        /// <summary>
        /// Get List of country
        /// </summary>
        /// <param name="billingShippingFlag"></param>
        /// <returns></returns>
        Collection<CountryModel> GetCountries(int billingShippingFlag = 0);

        /// <summary>
        /// Save Credit Application 
        /// </summary>
        /// <param name="model">Credit Application View Model</param>
        /// <returns>Returns true/false</returns>
        bool SaveCreditApplicationInfo(PRFTCreditApplicationViewModel model);
    }
}