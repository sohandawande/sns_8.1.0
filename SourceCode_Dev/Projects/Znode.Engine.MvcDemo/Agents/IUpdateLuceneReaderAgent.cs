﻿
namespace Znode.Engine.MvcDemo.Agents
{
    public interface IUpdateLuceneReaderAgent
    {
        /// <summary>
        /// This method updates the lucene reader
        /// </summary>
        void UpdateReader();
    }
}
