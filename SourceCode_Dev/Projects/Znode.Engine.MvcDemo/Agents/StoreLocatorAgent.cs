﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Web.Mvc;
using Znode.Engine.Api.Client;
using Znode.Engine.Exceptions;
using Znode.Engine.MvcDemo.Maps;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Agents
{
    public class StoreLocatorAgent : IStoreLocatorAgent
    {
        private readonly StoreLocatorClient _storeLocatorClient;

        /// <summary>
        /// Constructor for Store Locator
        /// </summary>
        public StoreLocatorAgent()
        {
            _storeLocatorClient = new StoreLocatorClient();
        }

        public Collection<StoreLocatorViewModel> GetStoresList(StoreLocatorViewModel model)
        {
            try
            {
                Collection<StoreLocatorViewModel> storeList = new Collection<StoreLocatorViewModel>();
                return storeList = StoreLocatorViewModelMap.ToListViewModel(_storeLocatorClient.GetStoresList(StoreLocatorViewModelMap.ToModel(model)).Stores);           
            }
            catch (ZnodeException ex)
            { 
                model.ErrorMessage = ex.ErrorMessage;
                return null;
            }            
        }

        #region Distances
        public List<SelectListItem> GetDistanceList()
        {
            List<SelectListItem> listItems = new List<SelectListItem>();
            listItems = StoreLocatorViewModelMap.GetDistances();
            return listItems;
        }
        #endregion
    }
}