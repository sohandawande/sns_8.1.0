﻿using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Agents
{
    /// <summary>
    /// Case Request Agent Interface
    /// </summary>
    public partial interface ICaseRequestAgent
    {
        /// <summary>
        /// Contact us in Service Request
        /// </summary>
        /// <param name="model">Case Request View Model</param>
        /// <returns>Returns true/false</returns>
        bool SaveContactUsInfo(CaseRequestViewModel model);

        /// <summary>
        /// Customer Feedback in Service Request.
        /// </summary>
        /// <param name="model">Case Request View Model</param>
        /// <returns>Returns true/false</returns>
        bool SaveCustomerFeedbackInfo(CaseRequestViewModel model);
    }
}
