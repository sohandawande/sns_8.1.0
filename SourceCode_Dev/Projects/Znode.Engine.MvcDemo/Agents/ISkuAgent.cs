﻿using System;
using Znode.Engine.Api.Models;

namespace Znode.Engine.MvcDemo.Agents
{
    public interface ISkuAgent
    {
        //ZNode Version 7.2.2 - Add imageMediumPath Out parameter
        Decimal GetSkusPrice(int productId, int attributeId, decimal productPrice, out string selectedSku, out string imagePath, out string imageMediumPath);
        SkuModel GetSkuInventory(int productId, string attributeId);
        SkuModel GetBySku(int productId, string sku);
    }
}