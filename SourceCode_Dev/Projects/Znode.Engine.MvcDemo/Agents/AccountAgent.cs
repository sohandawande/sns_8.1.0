﻿using DotNetOpenAuth.AspNet;
using Facebook;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Security;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.MvcDemo.Helpers;
using Znode.Engine.MvcDemo.Maps;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Agents
{
    public partial class AccountAgent : BaseAgent, IAccountAgent
    {
        private readonly IAccountsClient _accountClient;
        private readonly IWishListsClient _wishListClient;
        private readonly IProductsClient _productsClient;
        private readonly IReviewsClient _reviewsClient;
        private readonly IAddressesClient _addressClient;
        private readonly IStatesClient _statesClient;
        private readonly IGiftCardsClient _giftCardsClient;
        private readonly IReorderClient _reorderClient;
        private readonly IOrdersClient _ordersClient;
        private readonly ICustomerUserMappingClient _customerUserMappingClient; //PRFT Custom Code

        //Znode Version 7.2.2
        private readonly ICountriesClient _countriesClient;

        /// <summary>
        /// Account Agent Constructor
        /// </summary>
        public AccountAgent()
        {
            _accountClient = GetClient<AccountsClient>();
            _wishListClient = GetClient<WishListsClient>();
            _productsClient = GetClient<ProductsClient>();
            _reviewsClient = GetClient<ReviewsClient>();
            _addressClient = GetClient<AddressesClient>();
            _giftCardsClient = GetClient<GiftCardsClient>();
            _statesClient = new StatesClient();
            _reorderClient = GetClient<ReorderClient>();
            _countriesClient = new CountriesClient();
            _ordersClient = GetClient<OrdersClient>();
            _customerUserMappingClient = GetClient<CustomerUserMappingClient>();
        }

        /// <summary>
        /// Gets the Security Questions for the Signup page
        /// </summary>
        /// <returns>Returns the register view model</returns>
        public RegisterViewModel GetRegisterViewModel()
        {
            return RegisterViewModelMap.ToViewModel();
        }

        /// <summary>
        /// Signup based on the details given.
        /// </summary>
        /// <param name="model">Login details as Register View Model</param>
        /// <returns>Created account or not</returns>
        public RegisterViewModel SignUp(RegisterViewModel model)
        {
            try
            {
                var registerModel = _accountClient.CreateAccount(AccountViewModelMap.ToSignUpModel(model));

                var viewModel = AccountViewModelMap.ToAccountViewModel(registerModel);
                SaveInSession(MvcDemoConstants.AccountKey, viewModel);
                return RegisterViewModelMap.ToModel(viewModel);
            }
            catch (ZnodeException exception)
            {
                if (exception.ErrorCode == ErrorCodes.UserNameUnavailable)
                {
                    return new RegisterViewModel() { HasError = true, ErrorMessage = model.UserName + " User Name is  not available" };
                }


            }

            return new RegisterViewModel() { HasError = true, ErrorMessage = Resources.ZnodeResource.ErrorAccountCreation };

        }

        /// <summary>
        ///      based on the username and password
        /// </summary>
        /// <param name="model">Login details as Login View Model</param>
        /// <returns>Success or not</returns>
        public LoginViewModel Login(LoginViewModel model)
        {
            AccountModel accountModel;
            LoginViewModel loginViewModel;
            try
            {
                accountModel = _accountClient.Login(AccountViewModelMap.ToLoginModel(model), GetExpands());

                //In case user requested for Reset password,
                if (accountModel.User != null && !string.IsNullOrEmpty(accountModel.User.PasswordToken))
                {
                    loginViewModel = AccountViewModelMap.ToLoginViewModel(accountModel);
                    loginViewModel.IsResetPassword = true;
                    loginViewModel.HasError = true;
                    loginViewModel.ErrorMessage = Resources.ZnodeResource.ErrorLoginFailed;
                    return loginViewModel;
                }

                // Check user profiles.                
                if (!accountModel.Profiles.Any())
                {
                    return new LoginViewModel() { HasError = true };
                }

                SaveInSession(MvcDemoConstants.AccountKey, AccountViewModelMap.ToAccountViewModel(accountModel));
                RemoveInSession(MvcDemoConstants.BillingAddressKey);
                RemoveInSession(MvcDemoConstants.ShippingAddressKey);
                return AccountViewModelMap.ToLoginViewModel(accountModel);

            }
            catch (ZnodeException ex)
            {
                if (Equals(ex.ErrorCode, 2))
                {
                    loginViewModel = new LoginViewModel();
                    loginViewModel.IsResetPassword = true;
                    loginViewModel.HasError = true;
                    loginViewModel.ErrorMessage = ex.ErrorMessage;
                    return loginViewModel;
                }
                if (Equals(ex.ErrorCode, ErrorCodes.LoginFailed) || Equals(ex.ErrorCode, ErrorCodes.AccountLocked))
                {
                    loginViewModel = new LoginViewModel();
                    loginViewModel.HasError = true;
                    loginViewModel.ErrorMessage = ex.ErrorMessage;

                    return loginViewModel;
                }
            }

            return new LoginViewModel() { HasError = true, ErrorMessage = Resources.ZnodeResource.ErrorLoginFailed };

        }

        /// <summary>
        /// Gets the account view model from the session
        /// </summary>
        /// <returns>Returns the account view model with the orders, order history, wish list, profile for the user</returns>
        public AccountViewModel GetAccountViewModel()
        {
            var accountViewModel = GetFromSession<AccountViewModel>(MvcDemoConstants.AccountKey);

            if (accountViewModel == null)
                return null;

            // To hold the last action message for certain scenarios.
            var returnModel = accountViewModel.Clone() as AccountViewModel;

            // Reset the messages from session for certain scenarios.
            accountViewModel.HasError = false;
            accountViewModel.SuccessMessage = accountViewModel.ErrorMessage = string.Empty;

            SaveInSession(MvcDemoConstants.AccountKey, accountViewModel);

            return returnModel;
        }

        /// <summary>
        /// Gets the account view model from the the API 
        /// </summary>
        /// <returns>Returns the account view model with the orders, order history, wish list, profile for the user</returns>
        public AccountViewModel GetDashboard()
        {
            var accountViewModel = GetFromSession<AccountViewModel>(MvcDemoConstants.AccountKey);
            _accountClient.RefreshCache = true;

            AccountModel accountModel = null;

            try
            {
                if (accountViewModel == null)
                {
                    Logout();
                    return null;
                }

                accountModel = _accountClient.GetAccount(accountViewModel.AccountId, GetExpands());

                // Check user profiles.
                if (!accountModel.Profiles.Any())
                {
                    return new AccountViewModel()
                    {
                        HasError = true,

                    };
                }

                var dashBoardViewModel = AccountViewModelMap.ToAccountViewModel(accountModel);
                if (dashBoardViewModel != null && dashBoardViewModel.WishList != null)
                {
                    dashBoardViewModel.WishList.Items.ToList().ForEach(x =>
                                                                       x.Product =
                                                                       ProductViewModelMap.ToViewModel(
                                                                           _productsClient.GetProduct(x.ProductId,
                                                                                                      new ExpandCollection
	                                                                                                      {
	                                                                                                          ExpandKeys.AddOns,
	                                                                                                          ExpandKeys.BundleItems,
	                                                                                                          ExpandKeys.Attributes,
	                                                                                                          ExpandKeys.Categories,
	                                                                                                          ExpandKeys.Promotions,
	                                                                                                          ExpandKeys.Skus
	                                                                                                      })));
                }

                _reviewsClient.RefreshCache = true;
                var list = _reviewsClient.GetReviews(new ExpandCollection { ExpandKeys.Reviews },
                      new FilterCollection { new FilterTuple(FilterKeys.AccountId, FilterOperators.Equals, accountModel.AccountId.ToString()) },
                      new SortCollection());
                dashBoardViewModel.ReviewCount = list.Reviews != null ? list.Reviews.Count : 0;
                dashBoardViewModel.GiftCardHistoryCount = (!Equals(dashBoardViewModel.GiftCardHistory, null)) ? dashBoardViewModel.GiftCardHistory.Count : 0;
                SetMessages(dashBoardViewModel);
                SaveInSession(MvcDemoConstants.AccountKey, dashBoardViewModel);

                return dashBoardViewModel;
            }
            catch (ZnodeException ex)
            {
                if (ex.ErrorCode == 2)
                {
                    var dashBoardViewModel = AccountViewModelMap.ToAccountViewModel(accountModel);
                    dashBoardViewModel.IsResetPassword = true;
                    SaveInSession(MvcDemoConstants.AccountKey, dashBoardViewModel);
                    return dashBoardViewModel;
                }
            }

            return accountViewModel;
        }

        /// <summary>
        /// Update the account view model to the session
        /// </summary>
        /// <returns>Returns the account view model with the orders, order history, wish list, profile for the user</returns>
        public void UpdateAccountViewModel(object model)
        {
            var accountModel = GetFromSession<AccountViewModel>(MvcDemoConstants.AccountKey);

            if (!HttpContext.Current.User.Identity.IsAuthenticated || accountModel == null) return;

            if (model is WishListItemViewModel)
            {
                var viewModel = model as WishListItemViewModel;
                if (accountModel.WishList.Items.Any(x => x.WishListId == viewModel.WishListId))
                    accountModel.WishList.Items.Remove(accountModel.WishList.Items.First(x => x.WishListId == viewModel.WishListId));

                viewModel.Product = ProductViewModelMap.ToViewModel(
                    _productsClient.GetProduct(viewModel.ProductId,
                                               new ExpandCollection
                                                   {
                                                       ExpandKeys.AddOns, ExpandKeys.BundleItems, ExpandKeys.Attributes, 
                                                       ExpandKeys.Categories, ExpandKeys.Promotions, ExpandKeys.Skus
                                                   }));

                accountModel.WishList.Items.Add(viewModel);
            }
            else if (model is AddressViewModel)
            {
                var viewModel = model as AddressViewModel;
                if (accountModel.Addresses.Any(x => x.AddressId == viewModel.AddressId))
                    accountModel.Addresses.Remove(accountModel.Addresses.First(x => x.AddressId == viewModel.AddressId));
                accountModel.Addresses.Add(viewModel);
                accountModel.Addresses = new Collection<AddressViewModel>(accountModel.Addresses.OrderBy(x => x.AddressId).ToList());

                // Set the address success message to account model
                SetMessage(accountModel, viewModel);
            }
            else if (model is OrdersViewModel)
            {
                var viewModel = model as OrdersViewModel;
                if (accountModel.Orders.Any(x => x.OrderId == viewModel.OrderId))
                    accountModel.Orders.Remove(accountModel.Orders.First(x => x.OrderId == viewModel.OrderId));
                accountModel.Orders.Add(viewModel);
                accountModel.Orders = new Collection<OrdersViewModel>(accountModel.Orders.OrderByDescending(x => x.OrderDate).ToList());
            }

            SaveInSession(MvcDemoConstants.AccountKey, accountModel);
        }

        /// <summary>
        /// Gets the account view model from the session if it is in session else from the DB
        /// </summary>
        /// <returns>Returns the account view model with the orders, order history, wish list, profile for the user</returns>
        public void RemoveAccountViewModel(object model)
        {
            var accountModel = GetFromSession<AccountViewModel>(MvcDemoConstants.AccountKey);

            if (model is WishListItemViewModel)
            {
                var viewModel = model as WishListItemViewModel;
                if (accountModel.WishList.Items.Any(x => x.WishListId == viewModel.WishListId))
                    accountModel.WishList.Items.Remove(
                        accountModel.WishList.Items.First(x => x.WishListId == viewModel.WishListId));
            }

            else if (model is AddressViewModel)
            {
                var viewModel = model as AddressViewModel;
                if (accountModel.Addresses.Any(x => x.AddressId == viewModel.AddressId))
                    accountModel.Addresses.Remove(accountModel.Addresses.First(x => x.AddressId == viewModel.AddressId));
                accountModel.Addresses = new Collection<AddressViewModel>(accountModel.Addresses.OrderBy(x => x.AddressId).ToList());
            }

            SaveInSession(MvcDemoConstants.AccountKey, accountModel);
        }

        /// <summary>
        /// Get wishlistId based on productId
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        public int GetWishlistByProductId(int productId)
        {
            var accountViewModel = GetAccountViewModel();
            return accountViewModel.WishList.Items.ToList()
                            .First(x => x.AccountId == accountViewModel.AccountId && x.ProductId == productId).WishListId;
        }

        /// <summary>
        /// Retrieves all the wishlist based on current account
        /// </summary>
        /// <returns>Returns wishlist collection</returns>
        public WishListViewModel GetWishList()
        {
            AccountViewModel accountViewModel = GetAccountViewModel();
            if (accountViewModel != null)
            {
                accountViewModel.WishList.Items.Where(x => x.Product == null).ToList().ForEach(x =>
                                                                      x.Product =
                                                                      ProductViewModelMap.ToViewModel(
                                                                          _productsClient.GetProduct(x.ProductId,
                                                                                                     new ExpandCollection
	                                                                                                      {
	                                                                                                          ExpandKeys.AddOns,
	                                                                                                          ExpandKeys.BundleItems,
	                                                                                                          ExpandKeys.Attributes,
	                                                                                                          ExpandKeys.Categories,
	                                                                                                          ExpandKeys.Promotions,
	                                                                                                          ExpandKeys.Skus
	                                                                                                      })));

                if (!Equals(accountViewModel, null) && Convert.ToBoolean(accountViewModel.EnableCustomerPricing))
                {
                    var wishListProduct = accountViewModel.WishList.Items.Select(m => m.Product);
                    string productIds = string.Join(",", wishListProduct.Select(p => p.ProductId.ToString()));
                    ProductListModel customerPricingProduct = _productsClient.GetProductsByProductIds(productIds, new ExpandCollection { ExpandKeys.Skus }, null);
                    foreach (ProductViewModel item in wishListProduct)
                    {
                        if (customerPricingProduct.Products.Where(m => m.ProductId == item.ProductId).FirstOrDefault().Skus.FirstOrDefault().CustomerPricingSku.Count > 0)
                        {
                            item.ProductPrice = (from productList in customerPricingProduct.Products.Where(m => m.ProductId == item.ProductId).ToList()
                                                 from sku in productList.Skus.ToList()
                                                 from customerPricingSku in sku.CustomerPricingSku.ToList()
                                                 select Equals(customerPricingSku.ExternalAccountNo, accountViewModel.ExternalId)
                                                 ? (Equals(customerPricingSku.NegotiatedPrice.Value, null) ? sku.ProductPrice.Value : customerPricingSku.NegotiatedPrice.Value)
                                                 : productList.RetailPrice.Value).FirstOrDefault();
                        }
                    }
                }

                //PRFT Custom Code:Start
                //Geting Price form ERP
                string associatedCustomerExternalId = HelperMethods.GetAssociatedCustomerExternalId();
                if (!string.IsNullOrEmpty(associatedCustomerExternalId))
                {
                    string customerType = HelperMethods.GetERPCustomerType();
                    PRFTERPItemDetailsModel prftERPItemDetailModel = null;
                    var wishListProduct = accountViewModel.WishList.Items.Select(m => m.Product);
                    foreach (ProductViewModel item in wishListProduct)
                    {
                        prftERPItemDetailModel = _productsClient.GetItemDetailsFromERP(item.ProductNumber, associatedCustomerExternalId, customerType);
                        if (prftERPItemDetailModel != null && prftERPItemDetailModel.HasError == false)
                        {
                            item.Price = item.ProductPrice = prftERPItemDetailModel.UnitPrice; //Required by JS to update total price.
                            if (prftERPItemDetailModel.IsCallForPricing == true || prftERPItemDetailModel.UnitPrice<=0)
                            {
                                item.IsCallForPricing = true;
                            }
                        }
                    }
                }
                //PRFT Custom code: End
                 

                SetWishlistMessages(accountViewModel.WishList);
                return accountViewModel.WishList;
            }
            return new WishListViewModel();

        }

        /// <summary>
        /// Create wishlist based on productId, AccountId
        /// </summary>
        /// <param name="productId">ProductId to add in wishlist</param>
        /// <returns>Returns new wishlist collection after insert</returns>
        public WishListViewModel CreateWishList(int productId)
        {
            var accountModel = GetAccountViewModel();

            WishListViewModel wishlistModel;

            if (accountModel != null && CheckWishListExists(productId, accountModel.AccountId) == 0)
            {
                var accountId = accountModel.AccountId;
                var wishListModel = AccountViewModelMap.ToWishListModel(accountId, productId);

                try
                {
                    wishListModel = _wishListClient.CreateWishList(wishListModel);

                    UpdateAccountViewModel(AccountViewModelMap.ToWishListViewModel(wishListModel));

                    wishlistModel = GetWishList();

                    if (wishlistModel.Items.Any())
                    {
                        return wishlistModel;
                    }
                }
                catch (ZnodeException)
                {
                    return new WishListViewModel()
                    {
                        HasError = true
                    };
                }
            }

            wishlistModel = GetWishList();

            return wishlistModel;
        }

        /// <summary>
        /// Deletes the wishlist item from Wishlist
        /// </summary>
        /// <param name="wishlistId">WishlistId to delete</param>
        /// <returns>Returns new wishlist collection after delete</returns>
        public bool DeleteWishList(int wishlistId)
        {
            var deleteStatus = _wishListClient.DeleteWishList(wishlistId);
            if (deleteStatus)
            {
                RemoveAccountViewModel(new WishListItemViewModel() { WishListId = wishlistId });
                return true;
            }
            return false;
        }

        /// <summary>
        /// Checks Product already added to Wishlist
        /// </summary>
        /// <param name="productId">ProductId to check exists in wishlist</param>
        /// <param name="accountId">AccountId to check exists in wishlist</param>
        /// <returns>Product exist in wishlist or not</returns>
        public int CheckWishListExists(int productId, int accountId)
        {
            var accountModel = GetAccountViewModel();

            if (accountModel != null && accountModel.WishList.Items.Any())
            {
                var productExist = accountModel.WishList.Items.FirstOrDefault(x => x.ProductId == productId && x.AccountId == accountId);

                if (productExist != null)
                {
                    return productExist.WishListId;
                }

                return 0;
            }

            return 0;
        }

        /// <summary>
        /// Get the details for a particular order
        /// </summary>
        /// <param name="orderId">Order ID</param>
        /// <returns>Return Order Details</returns>
        public OrdersViewModel GetOrderReceiptDetails(int orderId)
        {
            var accountViewModel = GetAccountViewModel();

            if (!Equals(accountViewModel, null) && accountViewModel.Orders.Any())
            {
                FilterCollection filters = new FilterCollection();
                filters.Add(new FilterTuple(FilterKeys.OrderId, FilterOperators.Equals, orderId.ToString()));

                var model = accountViewModel.Orders.FirstOrDefault(x => x.OrderId == orderId);
                OrderModel orderModel = _ordersClient.GetOrderDetails(null, filters, null, null, null);

                if (!Equals(model, null) && !Equals(orderModel, null) && !string.IsNullOrEmpty(orderModel.PaymentMethod))
                {
                    model.PaymentMethod = orderModel.PaymentMethod;
                }

                Collection<OrderLineItemViewModel> _lineItemsList = new Collection<OrderLineItemViewModel>();
                foreach (var _lineItems in orderModel.OrderLineItemList.OrdersLineItems)
                {
                    if (Equals(_lineItems.ParentOrderLineItemId, null))
                    {
                        foreach (var _accountLineItem in model.OrderLineItems)
                        {
                            if (Equals(_accountLineItem.ParentOrderLineItemId, null) && _accountLineItem.OrderLineItemId.Equals(_lineItems.OrderLineItemId))
                            {
                                _accountLineItem.ShippingAddress = _lineItems.ShippingAddress;
                                _accountLineItem.ShippingId = Convert.ToInt32(_lineItems.OrderShipmentId);
                                _accountLineItem.DigitalAsset = _lineItems.DigitalAsset;
                                _lineItemsList.Add(_accountLineItem);
                            }
                        }
                    }

                }

                return model;
            }

            return new OrdersViewModel() { HasError = true, ErrorMessage = "No Order Found" };

        }

        /// <summary>
        /// Get the address based on the addressid
        /// </summary>
        /// <param name="addressId">Address ID</param>
        /// <returns>Returns the address as Address view model</returns>
        public AddressViewModel GetAddress(int addressId)
        {
            var accountViewModel = GetAccountViewModel();

            if (accountViewModel.Addresses.All(x => x.AddressId != addressId))
            {
                return null;
            }

            var address = _addressClient.GetAddress(addressId, true);

            if (address != null)
                return AddressViewModelMap.ToModel(address);

            return new AddressViewModel();
        }

        #region Znode Version 7.2.2 Reorder

        /// <summary>
        /// Znode Version 7.2.2
        /// Get the reorder product to add cart by order id.
        /// This function helps to reorder the complite order.
        /// </summary>
        /// <param name="orderId">orderId to get CartItem list</param>
        /// <returns>returns List of CartItemViewModel</returns>
        public List<CartItemViewModel> GetReorderItems(int orderId)
        {
            List<CartItemViewModel> model = new List<CartItemViewModel>();

            var reorder = _reorderClient.GetReorderItems(orderId);

            foreach (var m in reorder.ReorderItems)
            {
                Collection<SelectedBundleViewModel> _selectedbundles = new Collection<SelectedBundleViewModel>();
                m.SelectedBundles.ToList().ForEach(bundle =>
                {
                    _selectedbundles.Add(new SelectedBundleViewModel { AddOnValueIds = bundle.AddOnValueIds, ExternalId = bundle.ExternalId, ProductId = bundle.ProductId, SelectedAttributes = bundle.SelectedAttributes, Sku = bundle.Sku });
                });
                model.Add(new CartItemViewModel { ProductId = m.ProductId, Quantity = m.Quantity, Sku = m.Sku, AddOnValueIds = m.AddOnValueIds, BundleItemsIds = m.BundleItemsIds, SelectedBundles = _selectedbundles });// AddOnValuesCustomText = m.AddOnValuesCustomText 
            }

            return model;
        }

        /// <summary>
        /// Znode Version 7.2.2
        /// Get the single item of order to reorder. 
        /// This function helps to reorder single item.
        /// </summary>
        /// <param name="orderLineItemId">orderLineItemId to get item</param>
        /// <returns>return CartItemViewModel</returns>
        public CartItemViewModel GetReorderSingleItems(int orderLineItemId)
        {
            var reorder = _reorderClient.GetReorderLineItem(orderLineItemId);

            Collection<SelectedBundleViewModel> _selectedbundles = new Collection<SelectedBundleViewModel>();
            reorder.SelectedBundles.ToList().ForEach(bundle =>
            {
                _selectedbundles.Add(new SelectedBundleViewModel { AddOnValueIds = bundle.AddOnValueIds, ExternalId = bundle.ExternalId, ProductId = bundle.ProductId, SelectedAttributes = bundle.SelectedAttributes, Sku = bundle.Sku });
            });

            return new CartItemViewModel { ProductId = reorder.ProductId, Quantity = reorder.Quantity, Sku = reorder.Sku, AddOnValueIds = reorder.AddOnValueIds, SelectedBundles = _selectedbundles };//, AddOnValuesCustomText = reorder.AddOnValuesCustomText 
        }

        //TODO
        public List<CartItemViewModel> GetReorderItemsList(int orderId, int orderLineItemId, bool isOrder)
        {
            List<CartItemViewModel> model = new List<CartItemViewModel>();

            var reorder = _reorderClient.GetReorderItemsList(orderId, orderLineItemId, isOrder);

            foreach (var m in reorder.ReorderItems)
            {
                CartItemViewModel CartItemViewModel = new CartItemViewModel();
                CartItemViewModel.ProductId = m.ProductId;
                CartItemViewModel.Quantity = m.Quantity;
                CartItemViewModel.Sku = m.Sku;

                model.Add(CartItemViewModel);
            }
            return model;
        }

        #endregion

        /// <summary>
        /// Get the States depending on the country code 
        /// </summary>
        /// <param name="addressId">Address ID</param>
        /// <returns>Returns the address as Address view model</returns>
        public Collection<StateModel> GetStates()
        {
            var states = GetFromSession<Collection<StateModel>>(MvcDemoConstants.StatesSessionKey);
            if (states != null)
            {
                return states;
            }
            var stateListModel = _statesClient.GetStates(null, null);

            if (stateListModel != null && stateListModel.States != null)
            {
                SaveInSession(MvcDemoConstants.StatesSessionKey, stateListModel.States);
                return stateListModel.States;
            }

            return null;
        }

        /// <summary>
        /// This function returns list of all active billing and shipping countries on the basis of portal id.
        /// </summary>
        /// <param name="billingShippingFlag">int billingShippingFlag</param>
        /// <returns>Returns list of all active billing and shipping countries on the basis of portal id.</returns>
        public Collection<CountryModel> GetCountries(int billingShippingFlag = 0)
        {
            int portalId = PortalAgent.CurrentPortal.PortalId;
            _countriesClient.RefreshCache = true;
            var countryListModel = _countriesClient.GetActiveCountryByPortalId(portalId, billingShippingFlag);

            CountryModel model = new CountryModel();
            model.Code = String.Empty;
            model.Name = "Please Select";
            countryListModel.Countries.Insert(0, model);

            if (countryListModel != null && countryListModel.Countries != null)
            {
                SaveInSession(MvcDemoConstants.CountriesSessionKey, countryListModel.Countries);
                return countryListModel.Countries;
            }
            return null;
        }

        /// <summary>
        /// This function is used to get all state list by countryCode
        /// </summary>
        /// <param name="countryCode">string countryCode</param>
        /// <returns>list of state</returns>
        public IEnumerable<StateModel> GetStateByCountryCode(string countryCode)
        {
            var states = GetStates();
            if (states != null)
            {
                return states.Where(x => x.CountryCode.Equals(countryCode));
            }
            return null;
        }

        /// <summary>
        /// Set the address link
        /// </summary>
        /// <param name="model">Account View Model</param>
        public void SetAddressLink(AccountViewModel model)
        {
            model.Addresses.ToList().ForEach(x => x.SourceLink = string.Format(MvcDemoConstants.AddressLink, x.AddressId));
        }

        /// <summary>
        /// Update the address of the user
        /// </summary>
        /// <param name="address">Address View Model</param>
        /// <returns>Returns the error message as Address view model</returns>
        public AddressViewModel UpdateAddress(AddressViewModel address)
        {
            var addressModel = new AddressModel();

            var addAddress = AddressViewModelMap.ToModel(address);

            address = ValidateDefaultAddress(address);

            if (address.HasError)
                return address;

            try
            {
                if (addAddress.AddressId == 0)
                {
                    addAddress.AccountId = GetAccountViewModel().AccountId;
                    addressModel = _addressClient.CreateAddress(addAddress);
                }
                else
                {
                    addressModel = _addressClient.UpdateAddress(address.AddressId, addAddress);
                }

                address = AddressViewModelMap.ToModel(addressModel);
                address.SuccessMessage = addAddress.AddressId == 0 ? Resources.ZnodeResource.SuccessAddressAdded : Resources.ZnodeResource.SuccessAddressUpdated;
                UpdateAccountViewModel(address);
            }
            catch (ZnodeException)
            {
                address.HasError = true;
                address.ErrorMessage = Resources.ZnodeResource.ErrorAddAddress;
            }

            return address;
        }

        /// <summary>
        /// Updated the address based on the address id and the setting the Default billing / shipping
        /// </summary>
        /// <param name="addressId">Address ID</param>
        /// <param name="isDefaultBillingAddress">Default Billing / Shipping Address True = Default Billing, False = Default Shipping</param>
        /// <returns>Update the address and returns the success or error message</returns>
        public AddressViewModel UpdateAddress(int addressId, bool isDefaultBillingAddress)
        {
            var addAddress = GetAccountViewModel().Addresses.FirstOrDefault(x => x.AddressId == addressId);

            if (addAddress != null)
            {
                if (isDefaultBillingAddress)
                {
                    addAddress.IsDefaultBilling = true;
                }
                else
                {
                    addAddress.IsDefaultShipping = true;
                }
            }

            addAddress = ValidateDefaultAddress(addAddress);

            if (addAddress.HasError)
                return addAddress;

            var addresses = new AddressViewModel();

            try
            {
                var address = _addressClient.UpdateAddress(addressId, AddressViewModelMap.ToModel(addAddress));

                addresses = AddressViewModelMap.ToModel(address);
                UpdateAccountViewModel(addAddress);

                addresses.SuccessMessage = isDefaultBillingAddress
                                               ? Resources.ZnodeResource.SuccessPrimaryBilling
                                               : Resources.ZnodeResource.SuccessPrimaryShipping;
            }
            catch (ZnodeException)
            {
                addresses.HasError = true;
                addresses.ErrorMessage = isDefaultBillingAddress
                                             ? Resources.ZnodeResource.ErrorPrimaryBilling
                                             : Resources.ZnodeResource.ErrorPrimaryShipping;
            }

            //Update the session in the account view model

            var accountViewModel = GetAccountViewModel();
            var existingAddress = accountViewModel.Addresses.FirstOrDefault(x => x.AddressId == addressId);

            if (existingAddress != null)
            {
                existingAddress.IsDefaultBilling = addresses.IsDefaultBilling;
                existingAddress.IsDefaultShipping = addresses.IsDefaultShipping;
            }

            return addresses;
        }

        public AddressViewModel DeleteAddress(int addressId)
        {
            var model = new AddressViewModel();

            var allAddress = GetAccountViewModel().Addresses;

            model = allAddress.FirstOrDefault(x => x.AddressId == addressId && x.IsDefaultBilling);

            if (model != null)
            {
                model.HasError = true;
                model.ErrorMessage = Resources.ZnodeResource.ErrorDeleteBillingAddress;
                return model;
            }

            model = allAddress.FirstOrDefault(x => x.AddressId == addressId && x.IsDefaultShipping);

            if (model != null)
            {
                model.HasError = true;
                model.ErrorMessage = Resources.ZnodeResource.ErrorDeleteShippingAddress;
                return model;
            }

            model = allAddress.FirstOrDefault(x => x.AddressId == addressId);

            var deleteAddress = _addressClient.DeleteAddress(addressId);

            if (deleteAddress)
            {
                RemoveAccountViewModel(new AddressViewModel() { AddressId = addressId });

                return new AddressViewModel
                {
                    SuccessMessage = Resources.ZnodeResource.SuccessDeleteAddress
                };
            }

            model.HasError = true;
            model.ErrorMessage = Resources.ZnodeResource.ErrorDeleteAddress;
            return model;
        }

        /// <summary>
        /// Retrieves all the reviews based on current account
        /// </summary>
        /// <returns>Returns a</returns>
        public ReviewViewModel GetReviews()
        {
            var accountViewModel = GetAccountViewModel();

            if (accountViewModel != null)
            {
                var expands = new ExpandCollection { ExpandKeys.Reviews };
                var filters = new FilterCollection { new FilterTuple(FilterKeys.AccountId, FilterOperators.Equals, accountViewModel.AccountId.ToString()) };
                var sorts = new SortCollection();

                var list = _reviewsClient.GetReviews(expands, filters, sorts);
                if (list.Reviews != null)
                {
                    var reviewModel = list.Reviews.OrderByDescending(x => x.CreateDate);

                    var reviewModelList = AccountViewModelMap.ToReviewViewModel(new Collection<ReviewModel>(reviewModel.ToList()));

                    reviewModelList.Items.ToList()
                                   .ForEach(
                                       x =>
                                       x.Product =
                                       ProductViewModelMap.ToViewModel(_productsClient.GetProduct(x.ProductId.GetValueOrDefault())));

                    return reviewModelList;
                }
            }

            return new ReviewViewModel();
        }

        /// <summary>
        /// Update the profile
        /// </summary>
        /// <param name="model">Pass the user profiles to Account View Model</param>
        /// <returns>True or false if profile is updated or not</returns>
        public AccountViewModel UpdateProfile(AccountViewModel model)
        {
            var accountViewModel = GetAccountViewModel();

            if (!Equals(accountViewModel, null))
            {
                accountViewModel.EmailAddress = model.EmailAddress;
                accountViewModel.EmailOptIn = model.EmailOptIn;
            }

            try
            {
                _accountClient.UpdateAccount(accountViewModel.AccountId, AccountViewModelMap.ToAccountModel(accountViewModel));

                accountViewModel = GetAccountViewModel();
                if (!Equals(accountViewModel, null))
                {
                    accountViewModel.EmailAddress = model.EmailAddress;
                    accountViewModel.EmailOptIn = model.EmailOptIn;
                }
                SaveInSession(MvcDemoConstants.AccountKey, accountViewModel);
                accountViewModel.SuccessMessage = Resources.ZnodeResource.SuccessProfileUpdated;
            }
            catch (ZnodeException)
            {
                accountViewModel.HasError = true;
                accountViewModel.ErrorMessage = Resources.ZnodeResource.ErrorUpdateProfile;
            }

            return accountViewModel;
        }

        /// <summary>
        /// Used to change the password
        /// </summary>
        /// <param name="model">Old password and new password as Change password view model</param>
        /// <returns>Return true or false if password is changed or not</returns>
        public ChangePasswordViewModel ChangePassword(ChangePasswordViewModel model)
        {
            try
            {
                _accountClient.ChangePassword(AccountViewModelMap.ToChangePasswordModel(model));

                return new ChangePasswordViewModel
                {
                    SuccessMessage = Resources.ZnodeResource.SuccessPasswordChanged,
                };
            }
            catch (ZnodeException ex)
            {
                return new ChangePasswordViewModel
                {
                    HasError = true,
                    ErrorMessage = (string.IsNullOrEmpty(ex.ErrorMessage)) ? Resources.ZnodeResource.ErrorChangePassword : ex.ErrorMessage,
                };
            }
        }

        /// <summary>
        /// Forgot Password method
        /// </summary>
        /// <param name="model">AccountViewModel </param>
        /// <returns>Returns the password question</returns>
        public AccountViewModel ForgotPassword(AccountViewModel model)
        {
            if (model != null)
            {
                try
                {
                    var accountModel = _accountClient.GetAccountByUser(model.UserName, new ExpandCollection { ExpandKeys.User });

                    if (!model.HasError && accountModel != null)
                    {
                        if (model.EmailAddress == accountModel.Email)
                        {
                            model.BaseUrl = GetDomainUrl();
                            model = ResetPassword(model);
                            model.UserId = accountModel.User.UserId;
                            model.AccountId = accountModel.AccountId;
                            return model;
                        }//Znode Version 7.2.2, Add Invalid Email Error Message - Start
                        else
                        {
                            model.HasError = true;
                            model.ErrorMessage = Resources.ZnodeResource.ValidEmailAddress;
                            return model;
                        }
                        //Add Invalid Email Error Message - End
                    }
                }
                catch (ZnodeException)
                {
                    model.HasError = true;
                    model.ErrorMessage = Resources.ZnodeResource.InvalidAccountInformation;
                }
            }

            return model;
        }

        /// <summary>
        /// Reset password method
        /// </summary>
        /// <param name="model">AccountViewModel</param>
        /// <returns>Returns password changed successfully or not</returns>
        private AccountViewModel ResetPassword(AccountViewModel model)
        {
            try
            {
                _accountClient.ResetPassword(AccountViewModelMap.ToAccountModel(model));
                model.SuccessMessage = Resources.ZnodeResource.SuccessResetPassword;
            }
            catch (ZnodeException ex)
            {
                model.HasError = true;
                model.ErrorMessage = ex.ErrorMessage;

            }

            return model;
        }

        /// <summary>
        /// Logout the user
        /// </summary>
        public void Logout()
        {
            RemoveCookie(MvcDemoConstants.CartCookieKey);

            //PRFT Custom Code:Start
            HttpContext.Current.Session.Remove("AssociatedCustomerExternalId");
            HttpContext.Current.Session.Remove("ERPCustomerType");
            //PRFT Custom Code:End

            FormsAuthentication.SignOut();
            HttpContext.Current.Session.Abandon();
        }
        /// <summary>
        /// Set the Password changed success or error message
        /// </summary>
        /// <param name="model">Account View Model</param>
        /// <param name="success">Password Changed successfully or not</param>
        public void SetMessages(AccountViewModel model)
        {
            SetStatusMessage(model);
        }

        /// <summary>
        /// Set the wishlist success message or error message
        /// </summary>
        /// <param name="model">Account View Model</param>
        /// <param name="success">Password Changed successfully or not</param>
        public void SetWishlistMessages(WishListViewModel model)
        {
            SetStatusMessage(model);
        }


        /// <summary>
        /// Gets the giftcard balance amount 
        /// </summary>
        /// <param name="number">gift card number</param>
        /// <returns>Returns the giftcard balance amount</returns>
        public GiftCardViewModel GetGiftCardBalance(string giftcard)
        {
            var model = new GiftCardViewModel();

            var filters = new FilterCollection
                {
                    new FilterTuple(FilterKeys.CardNumber, FilterOperators.Equals, giftcard)
                };

            var list = _giftCardsClient.GetGiftCards(null, filters, null);

            if (list != null && list.GiftCards != null && list.GiftCards.Any())
            {
                model = AccountViewModelMap.ToGiftCardViewModel(list.GiftCards.First());
                model.SuccessMessage = Resources.ZnodeResource.SuccessGiftCardFound;
            }
            else
            {
                model.HasError = true;
                model.ErrorMessage = Resources.ZnodeResource.ErrorGiftCardNumber;
            }

            return model;

        }

        /// <summary>
        /// Returns the Expands needed for the account agent.
        /// </summary>
        /// <returns></returns>
        private ExpandCollection GetExpands()
        {
            return new ExpandCollection()
                {
                    ExpandKeys.Addresses,
                    ExpandKeys.Profiles,
                    ExpandKeys.WishLists,
                    ExpandKeys.Orders,
                    ExpandKeys.OrderLineItems,
                    ExpandKeys.User,
                    ExpandKeys.GiftCardHistory,
                    ExpandKeys.ReferralCommissionType
                };
        }

        /// <summary>
        /// validate the default billing and shipping address
        /// </summary>
        /// <param name="address">Address View Model</param>
        /// <returns>Returns the error message as Address view model</returns>
        private AddressViewModel ValidateDefaultAddress(AddressViewModel address)
        {
            var allAddress = GetAccountViewModel().Addresses;

            if (allAddress.Any())
            {
                var defaultBillingAddresses = allAddress.Where(x => x.AddressId != address.AddressId && x.IsDefaultBilling).ToList();
                var defaultShippingAddresses = allAddress.Where(x => x.AddressId != address.AddressId && x.IsDefaultShipping).ToList();

                if (!address.IsDefaultBilling && !address.IsDefaultShipping)
                {
                    if (!defaultBillingAddresses.Any() && !defaultShippingAddresses.Any())
                    {
                        address.HasError = true;
                        address.ErrorMessage = Resources.ZnodeResource.ErrorDefaultBillingShippingAddress;

                        return address;
                    }
                }

                if (address.IsDefaultBilling)
                {
                    if (defaultBillingAddresses.Any())
                    {
                        defaultBillingAddresses.ForEach(x =>
                        {
                            x.IsDefaultBilling = false;
                            _addressClient.UpdateAddress(x.AddressId, AddressViewModelMap.ToModel(x));
                        });
                    }
                }
                else
                {
                    if (!defaultBillingAddresses.Any())
                    {
                        address.HasError = true;
                        address.ErrorMessage = Resources.ZnodeResource.ErrorDefaultBillingAddress;

                        return address;
                    }
                }

                if (address.IsDefaultShipping)
                {
                    if (defaultShippingAddresses.Any())
                    {
                        defaultShippingAddresses.ForEach(x =>
                        {
                            x.IsDefaultShipping = false;
                            _addressClient.UpdateAddress(x.AddressId, AddressViewModelMap.ToModel(x));
                        });
                    }
                }
                else
                {
                    if (!defaultShippingAddresses.Any())
                    {
                        address.HasError = true;
                        address.ErrorMessage = Resources.ZnodeResource.ErrorDefaultShippingAddress;

                        return address;
                    }
                }

                address.HasError = false;
                address.ErrorMessage = string.Empty;

                return address;
            }
            else if (address.IsDefaultBilling && address.IsDefaultShipping)
            {
                return address;
            }
            else if (address.IsDefaultBilling && !address.IsDefaultShipping)
            {
                address.HasError = true;
                address.ErrorMessage = Resources.ZnodeResource.ErrorDefaultShippingAddress;
                return address;
            }
            else if (!address.IsDefaultBilling && address.IsDefaultShipping)
            {
                address.HasError = true;
                address.ErrorMessage = Resources.ZnodeResource.ErrorDefaultBillingAddress;
                return address;
            }
            else if (!address.IsDefaultBilling && !address.IsDefaultShipping)
            {
                address.HasError = true;
                address.ErrorMessage = Resources.ZnodeResource.ErrorDefaultBillingShippingAddress;
                return address;
            }

            address.HasError = true;
            address.ErrorMessage = Resources.ZnodeResource.ErrorAddAddress;
            return address;
        }

        /// <summary>
        /// Sets the messages
        /// </summary>
        /// <param name="accountModel">Account Model</param>
        /// <param name="viewModel">View Model</param>
        private void SetMessage(AccountViewModel accountModel, BaseViewModel viewModel)
        {
            accountModel.SuccessMessage = viewModel.SuccessMessage;
            accountModel.HasError = viewModel.HasError;
            accountModel.ErrorMessage = viewModel.ErrorMessage;

            viewModel.ErrorMessage = viewModel.SuccessMessage = string.Empty;
            viewModel.HasError = false;
        }

        /// <summary>
        /// Sign up the user for News letter.
        /// </summary>
        /// <param name="model">Model of type NewsLetterSignUpViewModel</param>
        /// <param name="message">string message</param>
        /// <returns>Return true or false</returns>
        public bool SignUpForNewsLetter(NewsLetterSignUpViewModel model, out string message)
        {
            message = string.Empty;
            try
            {
                return _accountClient.SignUpForNewsLetter(new NewsLetterSignUpModel() { Email = model.Email });
            }
            catch (ZnodeException ex)
            {
                message = ex.ErrorMessage;
            }
            return false;
        }

        //Method gets the Domain Base Url
        private string GetDomainUrl()
        {
            return (!string.IsNullOrEmpty(HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority))) ? HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) : string.Empty;
        }

        /// <summary>
        /// This function will check the Reset Password Link current status.
        /// </summary>
        /// <param name="model">ChangePasswordViewModel</param>
        /// <returns>Returns Status of Reset Password Link.</returns>
        public ResetPasswordStatusTypes CheckResetPasswordLinkStatus(ChangePasswordViewModel model)
        {
            try
            {
                string errorCode = string.Empty;
                var data = _accountClient.CheckResetPasswordLinkStatus(AccountViewModelMap.ToChangePasswordModel(model));
                return ResetPasswordStatusTypes.NoRecord;
            }
            catch (ZnodeException ex)
            {
                switch (ex.ErrorCode)
                {
                    case ErrorCodes.ResetPasswordContinue:
                        return ResetPasswordStatusTypes.Continue;
                    case ErrorCodes.ResetPasswordLinkExpired:
                        return ResetPasswordStatusTypes.LinkExpired;
                    case ErrorCodes.ResetPasswordNoRecord:
                        return ResetPasswordStatusTypes.NoRecord;
                    case ErrorCodes.ResetPasswordTokenMismatch:
                        return ResetPasswordStatusTypes.TokenMismatch;
                    default:
                        return ResetPasswordStatusTypes.NoRecord;
                }
            }
        }

        public bool IsAddressValid(AddressViewModel shippingAddress)
        {
            try
            {
                _accountClient.RefreshCache = true;
                var address = _accountClient.IsAddressValid(AddressViewModelMap.ToModel(shippingAddress));
                if (!Equals(address, null) && address.ValidAddress)
                {
                    return address.ValidAddress;
                }
                return false;
            }
            catch (ZnodeException)
            {
                return false;
            }
        }

        public AccountViewModel SocialLogin(string provider, string providerUserId, bool createPersistentCookie)
        {
            AccountModel accountModel;
            AccountViewModel model = null;
            try
            {
                accountModel = _accountClient.SocialUserLogin(AccountViewModelMap.ToSocialLoginModel(provider, providerUserId, createPersistentCookie), GetExpands());
                if (!accountModel.Profiles.Any())
                {
                    return new AccountViewModel() { HasError = true };
                }
                model = AccountViewModelMap.ToAccountViewModel(accountModel);
                SaveInSession(MvcDemoConstants.AccountKey, model);
                return model;
            }
            catch (ZnodeException ex)
            {
                if (Equals(ex.ErrorCode, ErrorCodes.AccountLocked))
                {
                    model = new AccountViewModel();
                    model.HasError = true;
                    model.IsLockedUser = true;
                    model.ErrorMessage = ex.ErrorMessage;
                    return model;
                }
            }
            return new AccountViewModel() { HasError = true, ErrorMessage = Resources.ZnodeResource.ErrorLoginFailed };
        }

        public bool SocialUserCreateOrUpdateAccount(string provider, string providerUserId, string userName)
        {
            try
            {
                return _accountClient.SocialUserCreateOrUpdateAccount(AccountViewModelMap.ToSocialUserRegisterModel(provider, providerUserId, userName));
            }
            catch { return false; }
        }

        public static RegisteredSocialClientListViewModel GetRegisteredSocialClientDetails()
        {
            try
            {
                string znodeAPIUrl = ConfigurationManager.AppSettings["ZnodeApiRootUri"].ToString();
                string apiUrl = string.Concat(znodeAPIUrl, "/Accounts/GetRegisteredSocialClientDetailsList");
                using (WebClient client = new WebClient())
                {
                    string jsonModel = client.DownloadString(apiUrl);
                    if (!string.IsNullOrEmpty(jsonModel))
                    {
                        RegisteredSocialClientListModel clientsModel = JsonConvert.DeserializeObject<RegisteredSocialClientListModel>(jsonModel);
                        return AccountViewModelMap.ToSocialClientViewModel(clientsModel);
                    }
                }
            }
            catch { }
            return new RegisteredSocialClientListViewModel();
        }

        public string SetSocialUserLogoutUrl(AuthenticationResult data)
        {
            string redirectUrl = GetDomainUrl();
            Logout();
            if (!Equals(data, null) && !string.IsNullOrEmpty(data.Provider))
            {
                string accessToken = string.Empty;
                data.ExtraData.TryGetValue("accesstoken", out accessToken);
                switch (data.Provider)
                {
                    case "facebook":
                        return string.Format("https://www.facebook.com/logout.php?next={0}&rd={1}&access_token={2}", redirectUrl, redirectUrl, accessToken);
                    case "google":
                        return string.Format("https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue={0}", redirectUrl);
                    default:
                        break;
                }
            }
            return redirectUrl;
        }

        public RegisterViewModel RegisertSocialUser(AuthenticationResult data)
        {
            RegisterViewModel model = new RegisterViewModel();
            string userIdorEmailAddress = string.Empty;
            data.ExtraData.TryGetValue("email", out userIdorEmailAddress);
            if (Equals(data.Provider, "facebook"))
            {
                string accessToken = string.Empty;
                data.ExtraData.TryGetValue("id", out userIdorEmailAddress);
                data.ExtraData.TryGetValue("accesstoken", out accessToken);
                FacebookClient client = new FacebookClient(accessToken);
                dynamic userDetails = client.Get("me?fields=id,name,email");
                model.EmailAddress = (!Equals(userDetails, null) && !string.IsNullOrEmpty(userDetails.email)) ? userDetails.email : string.Empty;
            }
            else
            {
                model.EmailAddress = userIdorEmailAddress;
            }
            model.UserName = userIdorEmailAddress;
            string password = GetUniqueKey(8);
            model.Password = password;
            model.ReTypePassword = password;
            model = SignUp(model);
            return model;
        }
        private static string GetUniqueKey(int maxSize)
        {
            char[] chars = new char[62];
            string strCharacters;
            strCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            chars = strCharacters.ToCharArray();
            int size = maxSize;
            byte[] data = new byte[1];
            RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
            crypto.GetNonZeroBytes(data);
            size = maxSize;
            data = new byte[size];
            crypto.GetNonZeroBytes(data);
            StringBuilder result = new StringBuilder(size);
            foreach (byte bytedata in data)
            {
                result.Append(chars[bytedata % (chars.Length - 1)]);
            }
            SetNumberInUniqueKey(result);
            return result.ToString();
        }
        /// <summary>
        /// Set the Number at the end of the autogenerated unique key.
        /// As for User, it is mandatory to have atlease one number in the password
        /// </summary>
        /// <param name="result"></param>
        private static void SetNumberInUniqueKey(StringBuilder result)
        {
            Random rnd = new Random();
            int dice = rnd.Next(1, 9);
            result.Append(Convert.ToString(dice));
        }
    }
}