﻿using DotNetOpenAuth.AspNet;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.Helpers;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Agents
{
    public partial interface IAccountAgent
    {
        RegisterViewModel GetRegisterViewModel();
        RegisterViewModel SignUp(RegisterViewModel model);
        LoginViewModel Login(LoginViewModel model);
        AccountViewModel GetAccountViewModel();
        AccountViewModel GetDashboard();
        Collection<StateModel> GetStates();
        void UpdateAccountViewModel(object model);
        int GetWishlistByProductId(int productId);
        WishListViewModel GetWishList();
        WishListViewModel CreateWishList(int productId);
        bool DeleteWishList(int wishlistId);
        int CheckWishListExists(int productId, int accountId);
        OrdersViewModel GetOrderReceiptDetails(int orderId);
        AddressViewModel GetAddress(int addressId);
        void SetAddressLink(AccountViewModel model);
        AddressViewModel UpdateAddress(AddressViewModel address);
        AddressViewModel UpdateAddress(int addressId, bool isDefaultBillingAddress);
        AddressViewModel DeleteAddress(int addressId);
        ReviewViewModel GetReviews();
        AccountViewModel UpdateProfile(AccountViewModel model);
        ChangePasswordViewModel ChangePassword(ChangePasswordViewModel model);
        AccountViewModel ForgotPassword(AccountViewModel model);
        void SetMessages(AccountViewModel model);
        void Logout();
        GiftCardViewModel GetGiftCardBalance(string giftcard);

        /// <summary>
        /// Sign Up the User for NewsLetters
        /// </summary>
        /// <param name="model">NewsLetterSignUpViewModel</param>
        /// <param name="message">Message</param>
        /// <returns>returns true/false</returns>
        bool SignUpForNewsLetter(NewsLetterSignUpViewModel model, out string message);

        List<CartItemViewModel> GetReorderItems(int orderId);
        CartItemViewModel GetReorderSingleItems(int orderLineItemId);

        //TODO
        List<CartItemViewModel> GetReorderItemsList(int orderId, int orderLineItemId, bool isOrder);

        /// <summary>      
        /// This function will check the Reset Password Link current status.
        /// </summary>
        /// <param name="model">ChangePasswordViewModel</param>
        /// <returns>Returns Status of Reset Password Link.</returns>
        ResetPasswordStatusTypes CheckResetPasswordLinkStatus(ChangePasswordViewModel model);

        /// <summary>
        /// This function is used to get all countries list
        /// </summary>
        /// <param name="billingShippingFlag">int billingShippingFlag</param>
        /// <returns>Returns collection of countries</returns>
        Collection<CountryModel> GetCountries(int billingShippingFlag = 0);

        /// <summary>
        /// This function is used to get all state list by countryCode
        /// </summary>
        /// <param name="countryCode">string countryCode</param>
        /// <returns>list of state</returns>
        IEnumerable<StateModel> GetStateByCountryCode(string countryCode);

        /// <summary>
        /// To check  Address is Valid
        /// </summary>
        /// <param name="shippingAddress">AddressViewModel shippingAddress</param>
        /// <returns>returns true/false</returns>
        bool IsAddressValid(AddressViewModel shippingAddress);

        /// <summary>
        /// Validate the Social User Login
        /// </summary>
        /// <param name="Provider">Social Media Provider Name</param>
        /// <param name="ProviderUserId">Social Media Provider Id</param>
        /// <param name="CreatePersistentCookie">bool CreatePersistentCookie</param>
        /// <returns>Return details in AccountViewModel</returns>
        AccountViewModel SocialLogin(string provider, string providerUserId, bool createPersistentCookie);

        /// <summary>
        /// Creates/Updated the Social User Account
        /// </summary>
        /// <param name="provider">Social Media Provider Name</param>
        /// <param name="providerUserId">Social Media Provider Id</param>
        /// <param name="userName">Social Media UserName</param>
        /// <returns>Return true or false</returns>
        bool SocialUserCreateOrUpdateAccount(string provider, string providerUserId, string userName);

        /// <summary>
        /// Set the Social User LogOut Urls based on the Current Provider.
        /// </summary>
        /// <param name="data">AuthenticationResult data</param>
        /// <returns>Returns the Logout Url Based on the Current Provider.</returns>
        string SetSocialUserLogoutUrl(AuthenticationResult data);

        /// <summary>
        /// Register the Social User within the Application
        /// </summary>
        /// <param name="data">AuthenticationResult data</param>
        /// <returns>Return the Details in RegisterViewModel</returns>
        RegisterViewModel RegisertSocialUser(AuthenticationResult data);
    }
}
