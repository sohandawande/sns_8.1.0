﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;
using WebGrease.Css.Extensions;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.MvcDemo.Helpers;
using Znode.Engine.MvcDemo.Maps;
using Znode.Engine.MvcDemo.ViewModels;
using Znode.Libraries.Helpers.Extensions;

namespace Znode.Engine.MvcDemo.Agents
{
    public class CheckoutAgent : BaseAgent, ICheckoutAgent
    {
        private readonly IShippingOptionAgent _shippingOptionAgent;
        private readonly ICartAgent _cartAgent;
        private readonly IAccountAgent _accountAgent;
        private readonly IPaymentOptionsClient _paymentOptionsClient;
        private readonly IOrdersClient _orderClient;
        private readonly IAccountsClient _accountClient;
        private readonly IAddressesClient _addressClient;
        private readonly IPaymentOptionsClient _paymentOptionClient;
        private readonly IAuthenticationAgent _authenticationAgent;
        private readonly IGiftCardsClient _giftCardClient;


        #region Private Constants
        private string payPalUrl = "/payment/paypal?callback=?";
        private string payNowUrl = "/payment/paynow?callback=?";
        #endregion

        /// <summary>
        /// Checkout Agent Constructor
        /// </summary>
        public CheckoutAgent()
        {
            _shippingOptionAgent = GetClient<ShippingOptionAgent>();
            _cartAgent = new CartAgent();
            _accountAgent = new AccountAgent();
            _paymentOptionsClient = GetClient<PaymentOptionsClient>();
            _orderClient = GetClient<OrdersClient>();
            _accountClient = GetClient<AccountsClient>();
            _addressClient = GetClient<AddressesClient>();
            _paymentOptionClient = GetClient<PaymentOptionsClient>();
            _authenticationAgent = new AuthenticationAgent();
            _giftCardClient = new GiftCardsClient();
        }

        /// <summary>
        /// Save the Shipping Address in the session and assign it to the cart object
        /// </summary>
        /// <param name="model">Address View Model</param>
        public void SaveShippingAddess(AddressViewModel model)
        {
            var cart = GetFromSession<ShoppingCartModel>(MvcDemoConstants.CartModelSessionKey);
            cart.ShippingAddress = AddressViewModelMap.ToModel(model);
            SaveInSession(MvcDemoConstants.CartModelSessionKey, cart);
            SaveInSession(MvcDemoConstants.ShippingAddressKey, model);
        }

        public void SaveLogin(LoginViewModel model, bool IsCreateLogin = false)
        {
            if (IsCreateLogin)
            {
                SaveInSession(MvcDemoConstants.LoginKey, model);
            }
            else
            {
                RemoveInSession(MvcDemoConstants.LoginKey);
            }
        }


        /// <summary>
        /// Save the Billing Address in the session and assign it to the cart object
        /// </summary>
        /// <param name="model">Address View Model</param>
        /// <param name="useShippingAddress">Flag to say Use Same Shipping Address</param>
        public BillingAddressViewModel SaveBillingAddess(AddressViewModel model, bool validModel, bool useShippingAddress = false)
        {
            if (!useShippingAddress || string.IsNullOrEmpty(model.Email))
            {
                if (!validModel || string.IsNullOrEmpty(model.Email))
                {
                    return new BillingAddressViewModel()
                    {
                        BillingAddress = model,
                        ShippingAddress = GetShippingAddess(),
                        UseSameAsShippingAddress = useShippingAddress,
                        HasError = true
                    };
                }
            }

            var cart = GetFromSession<ShoppingCartModel>(MvcDemoConstants.CartModelSessionKey);
            cart.Payment = new PaymentModel();
            if (useShippingAddress)
            {
                var billAddress = GetShippingAddess();
                billAddress.Email = model.Email;
                billAddress.UseSameAsShippingAddress = true;
                billAddress.IsDefaultBilling = true;
                model = billAddress;
            }

            cart.Payment.BillingAddress = AddressViewModelMap.ToModel(model);
            SaveInSession(MvcDemoConstants.CartModelSessionKey, cart);
            SaveInSession(MvcDemoConstants.BillingAddressKey, model);
            SaveInSession(MvcDemoConstants.BillingEmailAddress, model.Email);

            return new BillingAddressViewModel() { HasError = false };
        }

        /// <summary>
        /// Get the shipping address from session
        /// </summary>
        /// <returns>Returns the shipping address as address view model</returns>
        public AddressViewModel GetShippingAddess()
        {
            var model = GetFromSession<AddressViewModel>(MvcDemoConstants.ShippingAddressKey);
            if (model == null)
            {
                var accountModel = _accountAgent.GetAccountViewModel();
                if (accountModel != null)
                    model = accountModel.Addresses.FirstOrDefault(x => x.IsDefaultShipping);
            }

            return model;
        }



        /// <summary>
        /// Get the billing address from session
        /// </summary>
        /// <returns>Returns the billing address as address view model</returns>
        public AddressViewModel GetBillingAddessFromSession()
        {
            var model = GetFromSession<AddressViewModel>(MvcDemoConstants.BillingAddressKey);
            if (model == null)
            {
                var accountModel = _accountAgent.GetAccountViewModel();
                if (accountModel != null)
                {
                    if (accountModel.Addresses.Any())
                    {
                        model = accountModel.Addresses.FirstOrDefault(x => x.IsDefaultBilling);
                    }
                    else
                    {
                        model = new AddressViewModel();
                    }
                    model.Email = accountModel.EmailAddress;
                }
            }

            return model;
        }

        /// <summary>
        /// Get the billing and shipping address from the session.
        /// </summary>
        /// <returns>Returns the address as Billing address view model</returns>
        public BillingAddressViewModel GetBillingAddress()
        {
            var viewModel = new BillingAddressViewModel()
            {
                BillingAddress = GetBillingAddessFromSession() ?? new AddressViewModel(),
                ShippingAddress = GetShippingAddess(),
            };

            return viewModel;
        }


        public AddressViewModel GetAddressByAddressID(AddressViewModel address)
        {
            var accountViewModel = GetFromSession<AccountViewModel>(MvcDemoConstants.AccountKey);

            if (accountViewModel != null && accountViewModel.Addresses.Any())
            {
                var addressViewModel = accountViewModel.Addresses.FirstOrDefault(x => x.AddressId == address.AddressId);

                if (addressViewModel != null)
                {
                    addressViewModel.Email = accountViewModel.EmailAddress;
                }

                return addressViewModel; ;
            }

            return new AddressViewModel();
        }

        public void SaveEmailAddress(AddressViewModel model)
        {
            SaveInSession(MvcDemoConstants.BillingAddressKey, model);
            SaveInSession(MvcDemoConstants.BillingEmailAddress, model.Email);
        }
        /// <summary>
        ///  Sets the Selected Shipping Options and show the confirmation page details for the cart.
        /// </summary>
        /// <param name="shippingOptions">Shipping Options Selected</param>
        /// <returns>Return the cart items, address as review order view model</returns>
        public ReviewOrderViewModel GetCheckoutReview(int shippingOptions)
        {
            var selectedShippingOption = _shippingOptionAgent.GetShippingOption(shippingOptions);
            var cart = GetFromSession<ShoppingCartModel>(MvcDemoConstants.CartModelSessionKey);
            if (!Equals(cart, null))
            {
                cart.Shipping = new ShippingModel
                {
                    ShippingOptionId = selectedShippingOption.ShippingId,
                    ShippingName = selectedShippingOption.ShippingDescription
                };
                AddressViewModel _shippingAddress = GetShippingAddess();
                cart.ShippingAddress = AddressViewModelMap.ToModel(_shippingAddress);
                cart.ShoppingCartItems.ForEach(item => item.InsufficientQuantity = false);
                //PRFT Custom Code:Start
                if (string.IsNullOrEmpty(cart.CustomerExternalAccountId))
                {
                    cart.CustomerExternalAccountId = HttpContext.Current.Session["AssociatedCustomerExternalId"] != null ? Convert.ToString(HttpContext.Current.Session["AssociatedCustomerExternalId"].ToString()) : ConfigurationManager.AppSettings["GenericUserExternalID"].ToString();
                }
                //PRFT Custom Code:End

                SaveInSession(MvcDemoConstants.CartModelSessionKey, cart);
                var cartViewModel = _cartAgent.Calculate(cart);

                return new ReviewOrderViewModel()
                {
                    ShippingAddress = _shippingAddress,
                    ShoppingCart = cartViewModel,
                    ShippingOption = selectedShippingOption,
                    ErrorMessage = cartViewModel.ErrorMessage,
                    HasError = cartViewModel.HasError
                };
            }
            return null;
        }

        /// <summary>
        /// Gets the payment options to be displayed and the cart items
        /// </summary>        
        /// <returns>returns the payment types as payments view model</returns>
        public PaymentsViewModel GetPaymentModel()
        {
            // Get All profile payment options.
            var allList = _paymentOptionsClient.GetPaymentOptions(
                new ExpandCollection() { ExpandKeys.PaymentType, ExpandKeys.PaymentGateway },
                new FilterCollection()
                    {
                        new FilterTuple(FilterKeys.ProfileId, FilterOperators.Equals, "null"),
                        new FilterTuple(FilterKeys.IsActive, FilterOperators.Equals, "true")
                    },
                new SortCollection());


            var accountModel = _accountAgent.GetAccountViewModel();

            // Get Account ProfileID, or Get Default Anonymous ProfileId from Portal
            int? profileId = null;
            profileId = accountModel != null ? accountModel.ProfileId : PortalAgent.CurrentPortal.DefaultAnonymousProfileId;

            // Get Profile based options and merge with All Profile options.
            if (profileId.HasValue)
            {
                var profileList = _paymentOptionsClient.GetPaymentOptions(
                    new ExpandCollection() { ExpandKeys.PaymentType, ExpandKeys.PaymentGateway },
                    new FilterCollection()
                        {
                            new FilterTuple(FilterKeys.ProfileId, FilterOperators.Equals, profileId.ToString()),
                            new FilterTuple(FilterKeys.IsActive, FilterOperators.Equals, "true")
                        },
                    new SortCollection());

                if (profileList.PaymentOptions != null && profileList.PaymentOptions.Any())
                {
                    allList.PaymentOptions =
                        new Collection<PaymentOptionModel>(
                            allList.PaymentOptions.Union(profileList.PaymentOptions).OrderBy(x => x.DisplayOrder).ToList());
                }

                //For Franchise Website - only franchise option must show.
                bool isFranchiseStore = _accountClient.IsRoleExistForProfile(PortalAgent.CurrentPortal.DefaultAnonymousProfileId, MvcDemoConstants.Franchise);
                if (isFranchiseStore)
                {
                    allList.PaymentOptions = Equals(profileList.PaymentOptions, null) ? new Collection<PaymentOptionModel>() : profileList.PaymentOptions;
                }
                else if (Equals(allList.PaymentOptions, null))
                {
                    allList.PaymentOptions = new Collection<PaymentOptionModel>();
                }

            }

            var cart = GetFromSession<ShoppingCartModel>(MvcDemoConstants.CartModelSessionKey);
            if (!Equals(cart, null))
            {
                var billingAddress = cart.Payment.BillingAddress;
                if (billingAddress.AddressId.Equals(0))
                {
                    billingAddress = AddressViewModelMap.ToModel(GetBillingAddessFromSession());
                }
                var billAddress = AddressViewModelMap.ToModel(billingAddress);
                var shipAddress = GetShippingAddess();

                cart.ShippingAddress = AddressViewModelMap.ToModel(shipAddress);

                // set the actionname for change address.
                // Znode 7.2.2 
                // start
                billAddress.SourceLink = MvcDemoConstants.RouteGuestCheckoutShipping;
                // end
                if (!Equals(shipAddress, null))
                    shipAddress.SourceLink = MvcDemoConstants.RouteGuestCheckoutShipping;

                return PaymentsViewModelMap.ToPaymentsViewModel(billAddress, shipAddress, cart, _cartAgent.Calculate(cart),
                                                               allList.PaymentOptions.GroupBy(i => i.PaymentTypeId).Select(g => g.First()).ToList<PaymentOptionModel>().ToCollection());
            }
            return null;
        }

        public PaymentsViewModel GetPaymentModel(PaymentOptionViewModel model)
        {
            var paymentModel = GetPaymentModel();
            var paymentType =
                paymentModel.PaymentOptions.FirstOrDefault(x => x.PaymentTypeId == (int)EnumPaymentType.CreditCard);
            if (paymentType != null) paymentType.CreditCardModel = model.CreditCardModel;
            return paymentModel;
        }

        /// <summary>
        /// Process the order
        /// </summary>
        /// <param name="paymentInformation">Payment Details</param>
        /// <param name="purchaseOrderNumber">Purchase Order Number</param>
        /// <param name="paymentInformationViewModel">PaymentInformationViewModel</param>
        /// <returns>Returns if order is placed or not</returns>
        public OrderViewModel SubmitOrder(PaymentOptionViewModel paymentInformationViewModel, string purchaseOrderNumber, string additionalInstruction = "")
        {
            //Check order is multiple shipping cart
            var _multiCart = GetMultiCartOrderReview();
            if (_multiCart != null && _multiCart.ReviewOrderList.Count > 0)
            {
                return SubmitMutiCartOrder(paymentInformationViewModel, purchaseOrderNumber);
            }

            var shoppingCart = GetFromSession<ShoppingCartModel>(MvcDemoConstants.CartModelSessionKey);
            var loginAccount = _accountAgent.GetAccountViewModel() ?? new AccountViewModel();
            var loginViewModel = GetFromSession<LoginViewModel>(MvcDemoConstants.LoginKey);

            var paymentInformationModel = PaymentsViewModelMap.ToViewModel(paymentInformationViewModel);
            var account = AccountViewModelMap.ToAccountModel(loginAccount);

            if (Equals(account.AccountId, 0))
            {
                try
                {
                    if (!Equals(loginViewModel, null))
                    {
                        account.User.Username = loginViewModel.Username;
                        account.User.Password = loginViewModel.Password;
                        account.Email = GetBillingAddessFromSession().Email;
                        account.User.Email = account.Email;
                        account.IsSinglePageCheckoutUser = true;
                    }
                    account = _accountClient.CreateAccount(account);
                }
                catch (ZnodeException ex)
                {
                    return new OrderViewModel() { HasError = true, ErrorMessage = ex.ErrorMessage };
                }
            }

            var shippingAddress = GetShippingAddess();

            if (Equals(shippingAddress, null))
            {
                return null;
            }


            UpdateAddress(account, AddressViewModelMap.ToModel(shippingAddress));

            AddressViewModel addressFromSession = GetBillingAddessFromSession();

            if (!Equals(addressFromSession, null) && !addressFromSession.UseSameAsShippingAddress)
            {
                account.Email = addressFromSession.Email;
                UpdateAddress(account, AddressViewModelMap.ToModel(addressFromSession));
            }
            else
            {
                account.Email = addressFromSession.Email;
            }
            if (!Equals(paymentInformationModel, null) && !Equals(paymentInformationModel.PaymentOption, null) && Equals(paymentInformationModel.PaymentOption.PaymentTypeId, (int)EnumPaymentType.PaypalExpressCheckout))
            {
                string baseUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + HttpContext.Current.Request.ApplicationPath;

                shoppingCart.ReturnUrl = baseUrl + MvcDemoConstants.RouteCheckOutPaypal + "?PaymentOptionId=" +
                                         paymentInformationModel.PaymentOption.PaymentOptionId;
                shoppingCart.CancelUrl = baseUrl + MvcDemoConstants.RouteShoppingCart;
            }

            shoppingCart.Payment = paymentInformationModel;
            shoppingCart.Payment.BillingAddress =
                AddressViewModelMap.ToModel(addressFromSession ?? shippingAddress);
            shoppingCart.ShippingAddress = AddressViewModelMap.ToModel(shippingAddress);
            //shoppingCart.AdditionalInstructions = additionalInstruction;
            var checkout = CartViewModelMap.ToShoppingCartModel(account, shoppingCart, purchaseOrderNumber);
            var affiliateId = Equals(HttpContext.Current.Session[MvcDemoConstants.AffiliateIdSessionKey], null) ? null : HttpContext.Current.Session[MvcDemoConstants.AffiliateIdSessionKey].ToString();
            checkout.Account.ReferralAccountId = string.IsNullOrEmpty(affiliateId) ? (int?)null : int.Parse(affiliateId);

            OrderModel order = null;
            if (!IsValidAddressForCheckout(shippingAddress))
            {
                return new OrderViewModel() { HasError = true, ErrorMessage = Resources.ZnodeResource.AddressValidationFailed };
            }
            try
            {
                order = _orderClient.CreateOrder(checkout);

                if (order.IsOffsitePayment)
                {
                    HttpContext.Current.Response.Redirect(order.ReceiptHtml);
                    return new OrderViewModel() { HasError = true };
                }
            }
            catch (ZnodeException exception)
            {
                return new OrderViewModel() { HasError = true, ErrorMessage = exception.ErrorMessage };
            }

            // reset the cart
            RemoveCookie(MvcDemoConstants.CartCookieKey);
            SaveInSession(MvcDemoConstants.CartModelSessionKey, new ShoppingCartModel());
            // Reset the address.
            RemoveInSession(MvcDemoConstants.BillingAddressKey);
            RemoveInSession(MvcDemoConstants.ShippingAddressKey);

            //PRFT Commented Code: Start
            //_accountAgent.UpdateAccountViewModel(AccountViewModelMap.ToOrderViewModel(order));
            //return new OrderViewModel { EmailId = account.Email, OrderId = order.OrderId };
            //PRFT Commented Code: End

            //PRFT Custom Code: Start
            var orderViewModel = AccountViewModelMap.ToOrderViewModel(order);
            _accountAgent.UpdateAccountViewModel(orderViewModel);

            //var orderReceiptJavaScript = AddEcommerceTrackingCode(orderViewModel);

            var orderReceiptJavaScript = _orderClient.GetOrderReceiptJavascript(order);

            var productAgent = new ProductAgent();
            if (shoppingCart != null)
            {
                for (int index = 0; index <= checkout.ShoppingCartItems.Count - 1; index++)
                {
                    var lineItemProduct = productAgent.GetProduct(shoppingCart.ShoppingCartItems[index].ProductId);
                    orderViewModel.OrderLineItems[index].ProductCategory = lineItemProduct != null ? lineItemProduct.CategoryName : "";
                }
            }

            if (orderReceiptJavaScript != null)
            {
                foreach (var item in orderViewModel.OrderLineItems)
                {
                    orderReceiptJavaScript = orderReceiptJavaScript.Replace(("#categoryname" + item.OrderLineItemId + "#"), item.ProductCategory);
                }
            }

            HttpContext.Current.Session["OrderReceiptJavaScript"] = orderReceiptJavaScript;

            //return new OrderViewModel { EmailId = account.Email, OrderId = string.IsNullOrEmpty(order.ExternalId) ? order.OrderId : int.Parse(order.ExternalId) };
            return new OrderViewModel { EmailId = account.Email, OrderId = order.OrderId };
            //PRFT CUstom Code : End
        }

        public bool UpdateSingleCheckoutAddress()
        {
            AccountViewModel loginAccount = _accountAgent.GetAccountViewModel() ?? new AccountViewModel();
            AccountModel account = AccountViewModelMap.ToAccountModel(loginAccount);
            AddressViewModel shippingAddress = GetShippingAddess();

            if (Equals(shippingAddress, null))
            {
                return false;
            }

            UpdateAddress(account, AddressViewModelMap.ToModel(shippingAddress));

            AddressViewModel addressFromSession = GetBillingAddessFromSession();

            if (!Equals(addressFromSession, null) && !addressFromSession.UseSameAsShippingAddress)
            {
                account.Email = addressFromSession.Email;
                UpdateAddress(account, AddressViewModelMap.ToModel(addressFromSession));
            }
            else
            {
                account.Email = addressFromSession.Email;
            }
            if (!IsValidAddressForCheckout(shippingAddress))
            {
                return false;
            }

            return true;
        }

        public AccountViewModel CreateSingleCheckoutAccount()
        {
            var loginAccount = _accountAgent.GetAccountViewModel() ?? new AccountViewModel();
            var loginViewModel = GetFromSession<LoginViewModel>(MvcDemoConstants.LoginKey);

            var account = AccountViewModelMap.ToAccountModel(loginAccount);

            if (Equals(account.AccountId, 0))
            {
                try
                {
                    if (!Equals(loginViewModel, null))
                    {
                        account.User.Username = loginViewModel.Username;
                        account.User.Password = loginViewModel.Password;
                        account.Email = GetBillingAddessFromSession().Email;
                        account.User.Email = account.Email;
                        account.IsSinglePageCheckoutUser = true;
                    }
                    account = _accountClient.CreateAccount(account);
                }
                catch (ZnodeException ex)
                {
                    return new AccountViewModel() { HasError = true, ErrorMessage = ex.ErrorMessage };
                }
            }

            RemoveInSession(MvcDemoConstants.LoginKey);
            var viewModel = AccountViewModelMap.ToAccountViewModel(account);
            SaveInSession(MvcDemoConstants.AccountKey, viewModel);
            return viewModel;
        }

        /// <summary>
        /// Update the address
        /// </summary>
        /// <param name="account">Account Model/param>
        /// <param name="address">Address Model</param>
        private void UpdateAddress(AccountModel account, AddressModel address)
        {
            var existingAddress = account.Addresses.FirstOrDefault(x => x.AddressId == address.AddressId);

            if (existingAddress != null)
                account.Addresses.Remove(existingAddress);

            account.Addresses.Add(address);

            if (address.AddressId == 0)
            {
                address.AccountId = account.AccountId;
                address = _addressClient.CreateAddress(address);
            }
            else
                _addressClient.UpdateAddress(address.AddressId, address);

            _accountAgent.UpdateAccountViewModel(AddressViewModelMap.ToModel(address));
        }

        /// <summary>
        /// Submit the paypal after success from Paypal
        /// </summary>
        /// <param name="token"></param>
        /// <param name="payerId"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        public OrderViewModel SubmitPayPalOrder(string token, string payerId, int paymentOptionId)
        {
            var shoppingCart = GetFromSession<ShoppingCartModel>(MvcDemoConstants.CartModelSessionKey);

            shoppingCart.Token = token;
            shoppingCart.Payerid = payerId;

            var payments = GetPaymentModel();

            var paymentOptionViewModel = payments.PaymentOptions.FirstOrDefault(x => x.PaymentOptionId == paymentOptionId);

            SaveInSession(MvcDemoConstants.CartModelSessionKey, shoppingCart);

            return SubmitOrder(paymentOptionViewModel, string.Empty);
        }

        /// <summary>
        /// Process paypal payment.
        /// </summary>
        /// <param name="paymentSettingId">payment setting id</param>
        /// <param name="returnUrl">paypal payment url</param>
        /// <param name="cancelUrl">cancel url</param>
        /// <returns>request in the from of data</returns>
        public string ProcessPaypalPayment(int paymentSettingId, string returnUrl, string cancelUrl)
        {
            ShoppingCartModel shoppingCart = GetFromSession<ShoppingCartModel>(MvcDemoConstants.CartModelSessionKey);
            if (Equals(shoppingCart, null))
            {
                return Resources.ZnodeResource.ErrorNoItemsShoppingCart;
            }

            AddressViewModel _shippingAddress = GetShippingAddess();
            if (Equals(_shippingAddress, null))
            {
                return Resources.ZnodeResource.ErrorCustomerShippingAddress;
            }

            if (!IsValidAddressForCheckout(_shippingAddress))
            {
                return Resources.ZnodeResource.AddressValidationFailed;
            }

            AddressViewModel addressFromSession = GetBillingAddessFromSession();

            if (Equals(addressFromSession, null))
            {
                addressFromSession = _shippingAddress;
                SaveInSession(MvcDemoConstants.BillingAddressKey, _shippingAddress);

            }

            PaymentViewModel model = PaypalPaymentViewModelMap.ToViewModel(shoppingCart, addressFromSession);
            model.ReturnUrl = returnUrl;
            model.CancelUrl = cancelUrl;
            model.PaymentSettingId = paymentSettingId;
            return ProcessPaypalRequest(model);

        }

        /// <summary>
        /// To process Payment Gatway 
        /// </summary>
        /// <param name="customerProfileId"></param>
        /// <param name="customerPaymentId"></param>
        /// <returns>returns Submit Order url</returns>
        public string SubmitPayment(string paymentSettingId, string customerProfileId, string customerPaymentId)
        {
            ShoppingCartModel shoppingCart = GetFromSession<ShoppingCartModel>(MvcDemoConstants.CartModelSessionKey);
            if (Equals(shoppingCart, null))
            {
                return Resources.ZnodeResource.ErrorNoItemsShoppingCart;
            }

            AddressViewModel _shippingAddress = GetShippingAddess();
            if (Equals(_shippingAddress, null))
            {
                return Resources.ZnodeResource.ErrorCustomerShippingAddress;
            }

            // Validate the customer shipping address if address validation enabled.
            if (!Equals(PortalAgent.CurrentPortal.EnableAddressValidation, null) || (PortalAgent.CurrentPortal.EnableAddressValidation.Value))
            {
                bool isAddressValid = _accountAgent.IsAddressValid(_shippingAddress);
                // Do not allow the customer to go to next page if valid shipping address required is enabled.
                if (!isAddressValid && !Equals(PortalAgent.CurrentPortal.RequireValidatedAddress, null) && (PortalAgent.CurrentPortal.RequireValidatedAddress.Value))
                {
                    return Resources.ZnodeResource.ErrorCustomerShippingAddress;
                }
            }

            AddressViewModel addressFromSession = GetBillingAddessFromSession();

            if (Equals(addressFromSession, null))
            {
                addressFromSession = _shippingAddress;
            }

            PaymentViewModel model = PaypalPaymentViewModelMap.ToViewModel(shoppingCart, addressFromSession);
            model.CustomerProfileId = customerProfileId;
            model.CustomerPaymentProfileId = customerPaymentId;
            string returnUrl = string.Empty;
            string cancelUrl = string.Empty;

            string baseUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + HttpContext.Current.Request.ApplicationPath;

            returnUrl = baseUrl + MvcDemoConstants.RouteCheckOutSuccess + "?PaymentOptionId=" + paymentSettingId;
            cancelUrl = baseUrl + MvcDemoConstants.RouteShoppingCart;

            model.ReturnUrl = returnUrl;
            model.CancelUrl = cancelUrl;
            model.PaymentSettingId = Convert.ToInt32(paymentSettingId);
            return ProcessCreditCardRequest(model);
        }

        /// <summary>
        /// To Process credit card
        /// </summary>
        /// <param name="model"></param>
        /// <returns>returns response</returns>
        private string ProcessCreditCardRequest(PaymentViewModel model)
        {
            if (!Equals(model, null))
            {
                var data = JsonConvert.SerializeObject(model);

                using (WebClient client = new WebClient())
                {
                    client.Encoding = System.Text.Encoding.UTF8;
                    client.Headers[HttpRequestHeader.ContentType] = "application/json";
                    client.Headers[HttpRequestHeader.Accept] = "text/xml";

                    string jsonModel = client.UploadString(string.Format("{0}{1}", ConfigurationManager.AppSettings["PaymentApplicationUrl"], payNowUrl), data);

                    if (!string.IsNullOrEmpty(jsonModel))
                    {
                        try
                        {
                            JavaScriptSerializer serializer = new JavaScriptSerializer();
                            dynamic item = serializer.Deserialize<object>(jsonModel);
                            return item["Data"];
                        }
                        catch
                        {

                        }
                    }
                }
            }
            return string.Empty;
        }
        /// <summary>
        /// Process the paypal request.
        /// </summary>
        /// <param name="model">PaymentViewModel</param>
        /// <returns>request in the from of data</returns>
        private string ProcessPaypalRequest(PaymentViewModel model)
        {
            if (!Equals(model, null))
            {
                var data = JsonConvert.SerializeObject(model);

                using (WebClient client = new WebClient())
                {
                    client.Encoding = System.Text.Encoding.UTF8;
                    client.Headers[HttpRequestHeader.ContentType] = "application/json";
                    client.Headers[HttpRequestHeader.Accept] = "text/xml";

                    string jsonModel = client.UploadString(string.Format("{0}{1}", ConfigurationManager.AppSettings["PaymentApplicationUrl"], payPalUrl), data);

                    if (!string.IsNullOrEmpty(jsonModel))
                    {
                        try
                        {
                            JavaScriptSerializer serializer = new JavaScriptSerializer();
                            dynamic item = serializer.Deserialize<object>(jsonModel);
                            return item["Data"];
                        }
                        catch
                        {

                        }
                    }
                }
            }
            return string.Empty;
        }


        /// <summary>
        /// Apply GiftCard method
        /// </summary>
        /// <param name="giftCard">Giftcard number</param>
        /// <returns>Returns the updated CartViewModel</returns>
        public CartViewModel ApplyGiftCard(string giftCard)
        {
            var shoppingCart = GetFromSession<ShoppingCartModel>(MvcDemoConstants.CartModelSessionKey);
            shoppingCart.GiftCardNumber = giftCard;

            var cartModel = _cartAgent.Calculate(shoppingCart);

            if (cartModel.GiftCardApplied)
            {
                cartModel.SuccessMessage = Resources.ZnodeResource.ValidGiftCard;
            }
            else
            {
                cartModel.HasError = true;
                cartModel.ErrorMessage = Resources.ZnodeResource.ErrorGiftCard;
            }
            return cartModel;

        }

        public bool IsValidGiftCard(string giftCard)
        {
            var account = _accountAgent.GetAccountViewModel();
            if (!Equals(account, null) && account.AccountId > 0)
            {
                return _giftCardClient.IsGiftCardValid(giftCard, account.AccountId);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Get the All address from session
        /// </summary>
        /// <returns>Returns the address list as address view model</returns>
        public List<AddressViewModel> GetAllAddesses()
        {
            List<AddressViewModel> addressList = new List<AddressViewModel>();
            var model = GetFromSession<AddressViewModel>(MvcDemoConstants.ShippingAddressKey);

            if (model != null)
                addressList.Add(model);

            var accountModel = _accountAgent.GetAccountViewModel();
            if (accountModel != null)
            {
                foreach (var adr in accountModel.Addresses)
                {
                    if (!addressList.Exists(element => element.AddressId.Equals(adr.AddressId)))
                    {
                        addressList.Add(adr);
                    }

                }
            }

            return addressList;
        }

        /// <summary>
        /// Get cart item list for multiple shipping
        /// </summary>
        /// <returns>returns lost of CartItemViewModel</returns>
        public List<CartItemViewModel> GetMultipleShipingViewModel()
        {
            var cartViewModel = _cartAgent.GetCart();
            var shoppingCart = GetFromSession<ShoppingCartModel>(MvcDemoConstants.CartModelSessionKey);
            List<OrderShipmentModel> _MultipleShipToAddress = new List<OrderShipmentModel>();
            shoppingCart.ShoppingCartItems.ForEach(cartitems =>
            {
                _MultipleShipToAddress = cartitems.MultipleShipToAddress;
            });
            var shippingAddress = GetAllAddesses();

            List<CartItemViewModel> cartitemList = new List<CartItemViewModel>();

            cartViewModel.Items.ToList().ForEach(item =>
            {
                int count = item.Quantity;
                for (int i = 0; i < count; i++)
                {
                    CartItemViewModel objCartItem = new CartItemViewModel();
                    item.Quantity = 1;
                    objCartItem = item.Clone() as CartItemViewModel;
                    objCartItem.Addresses = shippingAddress;
                    cartitemList.Add(objCartItem);
                }
            });

            return cartitemList;
        }

        /// <summary>
        /// Get group by multiple shipping address collection.
        /// </summary>
        /// <param name="multipleAddress">collection of OrderShipmentDataModel</param>
        /// <returns>returns list of OrderShipmentDataModel</returns>
        public List<OrderShipmentDataModel> SortMultipleShippingAddress(IEnumerable<OrderShipmentDataModel> multipleAddress)
        {
            //Filter address
            List<OrderShipmentDataModel> multiAddressList = new List<OrderShipmentDataModel>();
            multipleAddress.ForEach(addressItem =>
            {
                if (!multiAddressList.Exists(element => element.addressid.Equals(addressItem.addressid)))
                {
                    multiAddressList.Add(addressItem);
                }
            });

            return multiAddressList;
        }

        /// <summary>
        /// create multi shipping cart.
        /// </summary>
        /// <param name="sortedAddress">selected shipping addresses</param>
        /// <param name="multipleAddress">collection of shipping addresses</param>
        /// <returns>returns ReviewOrderListViewModel</returns>
        public ReviewOrderListViewModel CreateMultipleShoppingCart(List<OrderShipmentDataModel> sortedAddress, IEnumerable<OrderShipmentDataModel> multipleAddress)
        {

            if (sortedAddress.Count.Equals(1))
            {
                var shippingaddress = GetAddressByAddressID(new AddressViewModel { AddressId = int.Parse(sortedAddress[0].addressid) });
                _cartAgent.RemoveMultiCartItem();
                SaveShippingAddess(shippingaddress);
                return null;
            }
            else
            {
                var cartViewModel = _cartAgent.GetCart();
                SaveInSession(MvcDemoConstants.TempCartModelSessionKey, CartViewModelMap.ToModel(cartViewModel));

                AddressViewModel ShippingAddress = new AddressViewModel();
                ReviewOrderListViewModel reviewOrder = new ReviewOrderListViewModel();
                int rowIndex = 0;

                sortedAddress.ForEach(adr =>
                {
                    //Empty crat
                    _cartAgent.RemoveAllCartItems();

                    multipleAddress.ForEach(product =>
                    {
                        if (adr.addressid.Equals(product.addressid))
                        {
                            int productid = int.Parse(product.productid);

                            //Create New cart
                            CartItemViewModel _Item = new CartItemViewModel();
                            _Item = cartViewModel.Items.FirstOrDefault(x => x.ProductId == Convert.ToInt32(productid));
                            _Item.Quantity = 1;
                            ShippingAddress.AddressId = Convert.ToInt32(product.addressid);
                            _Item.Addresses.Add(GetAddressByAddressID(ShippingAddress));
                            _Item.Sku = product.SKU;
                            _Item.AddOnValueIds = product.AddOnValueIds;
                            _Item.AddOnValuesCustomText = product.AddOnValuesCustomText;
                            _Item.ProductDiscountAmount = cartViewModel.Items.FirstOrDefault(x => x.ProductId == Convert.ToInt32(productid)).ProductDiscountAmount;
                            var cartmodel = _cartAgent.Create(_Item);

                        }
                    });

                    //Create Review arder list for multiple shipping
                    ReviewOrderViewModel _reviewOrderModel = new ReviewOrderViewModel();
                    _reviewOrderModel = GetCheckoutReview(9);
                    _reviewOrderModel.ShippingAddress = GetAddressByAddressID(ShippingAddress);
                    _reviewOrderModel.ShippingAddress.SourceLink = string.Empty;
                    _reviewOrderModel.ShippingOptionList = _shippingOptionAgent.GetShippingList();
                    _reviewOrderModel.OrderViewModelIndex = rowIndex;
                    reviewOrder.ReviewOrderList.Add(_reviewOrderModel);
                    rowIndex++;
                });

                reviewOrder.Discount = cartViewModel.Discount;
                reviewOrder.Total = cartViewModel.Total.GetValueOrDefault();
                foreach (CouponViewModel coupon in cartViewModel.Coupons)
                {
                    reviewOrder.Coupons.Add(coupon);
                }
                SaveInSession(MvcDemoConstants.MultiCartModelSessionKey, reviewOrder);

                RecreateActualCart();

                return reviewOrder;
            }
        }

        /// <summary>
        /// Recreate actual cart.
        /// </summary>
        public void RecreateActualCart()
        {

            _cartAgent.RemoveAllCartItems();
            ShoppingCartModel shoppingCart = GetFromSession<ShoppingCartModel>(MvcDemoConstants.TempCartModelSessionKey);

            shoppingCart.ShoppingCartItems.Clear();

            var orderList = GetFromSession<ReviewOrderListViewModel>(MvcDemoConstants.MultiCartModelSessionKey);
            foreach (CouponViewModel coupon in orderList.Coupons)
            {
                shoppingCart.Coupons.Add(CouponViewModelMap.ToViewModel(coupon));
            }


            SaveInSession(MvcDemoConstants.CartModelSessionKey, shoppingCart);

            orderList.ReviewOrderList.ForEach(ReviewOrder =>
            {
                ReviewOrder.ShoppingCart.Items.ForEach(item =>
                {
                    _cartAgent.Create(item);
                });
            });

            //Adding shipping option id to the cart and inserting updated cart in session.
            ShoppingCartModel cart = GetFromSession<ShoppingCartModel>(MvcDemoConstants.CartModelSessionKey);
            if (!Equals(orderList, null) && !Equals(orderList.ReviewOrderList, null) && orderList.ReviewOrderList.Count > 0)
            {
                cart.Shipping.ShippingOptionId = orderList.ReviewOrderList.FirstOrDefault().ShippingOption.ShippingId;
            }
            SaveInSession(MvcDemoConstants.CartModelSessionKey, cart);

            RemoveInSession(MvcDemoConstants.TempCartModelSessionKey);
        }

        /// <summary>
        /// get multi shipping cart from session
        /// </summary>
        /// <returns></returns>
        public ReviewOrderListViewModel GetMultiCartOrderReview()
        {
            return GetFromSession<ReviewOrderListViewModel>(MvcDemoConstants.MultiCartModelSessionKey);
        }

        /// <summary>
        /// get the calculated multi shipping cart
        /// </summary>
        /// <param name="rowIndex">index of collection</param>
        /// <param name="shippingOption">shipping option id</param>
        public void CalculateMultiCartShipping(int rowIndex, int shippingOption)
        {
            var reviewModel = GetMultiCartOrderReview();

            var _model = reviewModel.ReviewOrderList.FirstOrDefault(x => x.OrderViewModelIndex.Equals(rowIndex));

            reviewModel.ReviewOrderList.Remove(_model);
            //Empty crat
            SaveInSession(MvcDemoConstants.TempCartModelSessionKey, GetFromSession<ShoppingCartModel>(MvcDemoConstants.CartModelSessionKey));
            _cartAgent.RemoveAllCartItems();

            //Create New cart
            _model.ShoppingCart.Items.ForEach(itm =>
            {
                itm.Addresses.Add(GetAddressByAddressID(_model.ShippingAddress));
                var cartmodel = _cartAgent.Create(itm);
            });

            //Create Review arder list for multiple shipping
            _model = GetCheckoutReview(shippingOption);
            _model.ShippingAddress = GetAddressByAddressID(_model.ShippingAddress);
            _model.ShippingOptionList = _shippingOptionAgent.GetShippingList();
            _model.OrderViewModelIndex = rowIndex;

            reviewModel.ReviewOrderList.Insert(rowIndex, _model);
            SaveInSession(MvcDemoConstants.MultiCartModelSessionKey, reviewModel);
            RecreateActualCart();
        }

        /// <summary>
        /// get calculated multi cart shipping 
        /// </summary>
        /// <param name="shippingOption">shipping option id</param>
        public void CalculateMultiCartShipping(int shippingOption)
        {
            var reviewModel = GetMultiCartOrderReview();
            var DefaultShipAddresmodel = GetFromSession<AddressViewModel>(MvcDemoConstants.ShippingAddressKey);
            ReviewOrderListViewModel _modelList = new ReviewOrderListViewModel();
            reviewModel.ReviewOrderList.ForEach(_model =>
            {
                //Empty crat
                SaveInSession(MvcDemoConstants.TempCartModelSessionKey, GetFromSession<ShoppingCartModel>(MvcDemoConstants.CartModelSessionKey));
                _cartAgent.RemoveAllCartItems();

                //Create New cart
                _model.ShoppingCart.Items.ForEach(itm =>
                {
                    itm.Addresses.Add(GetAddressByAddressID(_model.ShippingAddress));
                    var cartmodel = _cartAgent.Create(itm);
                });

                //Create Review arder list for multiple shipping
                SaveInSession(MvcDemoConstants.ShippingAddressKey, GetAddressByAddressID(_model.ShippingAddress));

                _model = GetCheckoutReview(shippingOption);
                _model.ShippingOptionList = _shippingOptionAgent.GetShippingList();
                _modelList.ReviewOrderList.Add(_model);
            });

            _modelList.Discount = reviewModel.Discount;
            _modelList.Total = reviewModel.Total;
            foreach (CouponViewModel coupon in reviewModel.Coupons)
            {
                _modelList.Coupons.Add(coupon);
            }
            SaveInSession(MvcDemoConstants.ShippingAddressKey, DefaultShipAddresmodel);
            SaveInSession(MvcDemoConstants.MultiCartModelSessionKey, _modelList);

            RecreateActualCart();

        }

        /// <summary>
        /// Process the multi shipping order
        /// </summary>
        /// <param name="paymentInformation">Payment Details</param>
        /// <param name="purchaseOrderNumber">Purchase Order Number</param>
        /// <param name="paymentInformationViewModel">PaymentInformationViewModel</param>
        /// <returns>Returns if order is placed or not</returns>
        public OrderViewModel SubmitMutiCartOrder(PaymentOptionViewModel paymentInformationViewModel, string purchaseOrderNumber)
        {
            //Multiple shipping cart
            var model = GetMultiCartOrderReview();

            ShoppingCartModel shoppingCart = GetFromSession<ShoppingCartModel>(MvcDemoConstants.CartModelSessionKey);
            shoppingCart.MultipleShipToEnabled = true;

            shoppingCart.OrderShipment = GetFromSession<List<OrderShipmentDataModel>>(MvcDemoConstants.MultiCartShipmentSessionKey);

            model.ReviewOrderList.ForEach(cart =>
            {
                shoppingCart.Total += (decimal)cart.ShoppingCart.Total;
                shoppingCart.ShippingCost += (decimal)cart.ShoppingCart.ShippingCost;
                shoppingCart.Discount += (decimal)cart.ShoppingCart.Discount;
                shoppingCart.GiftCardAmount += (decimal)cart.ShoppingCart.GiftCardAmount;
                shoppingCart.SubTotal += (decimal)cart.ShoppingCart.SubTotal;
                shoppingCart.TaxCost += (decimal)cart.ShoppingCart.TaxCost;
                shoppingCart.TaxRate += (decimal)cart.ShoppingCart.TaxRate;

                shoppingCart.OrderShipment.ForEach(_ordershipment =>
                {
                    if (int.Parse(_ordershipment.addressid).Equals(cart.ShippingAddress.AddressId))
                    {
                        _ordershipment.ShippingId = cart.ShippingOption.ShippingId;
                        _ordershipment.ShippingCode = cart.ShippingOption.ShippingCode;
                        _ordershipment.ShippingDescription = cart.ShippingOption.ShippingDescription;
                    }
                });

            });

            foreach (var item in model.ReviewOrderList)
            {
                if (!Equals(item.ShippingAddress, null))
                {
                    if (Equals(IsValidAddressForCheckout(item.ShippingAddress), false))
                    {
                        return new OrderViewModel() { HasError = true, ErrorMessage = Resources.ZnodeResource.AddressValidationFailed };
                    }
                }
            }

            SaveInSession(MvcDemoConstants.CartModelSessionKey, shoppingCart);
            var loginAccount = _accountAgent.GetAccountViewModel() ?? new AccountViewModel();
            var paymentInformationModel = PaymentsViewModelMap.ToViewModel(paymentInformationViewModel);
            var account = AccountViewModelMap.ToAccountModel(loginAccount);

            if (account.AccountId == 0)
            {
                try
                {
                    account = _accountClient.CreateAccount(account);
                }
                catch (ZnodeException ex)
                {
                    return new OrderViewModel() { HasError = true, ErrorMessage = ex.ErrorMessage };
                }
            }

            var shippingAddress = GetShippingAddess();

            if (shippingAddress == null)
            {
                return null;
            }

            UpdateAddress(account, AddressViewModelMap.ToModel(shippingAddress));

            AddressViewModel _billingAddress = GetBillingAddessFromSession();

            if (!Equals(_billingAddress, null))
            {
                if (!_billingAddress.UseSameAsShippingAddress)
                {
                    UpdateAddress(account, AddressViewModelMap.ToModel(_billingAddress));
                }
                account.Email = _billingAddress.Email;
            }

            if (paymentInformationModel.PaymentOption.PaymentTypeId == (int)EnumPaymentType.PaypalExpressCheckout)
            {
                string baseUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + HttpContext.Current.Request.ApplicationPath;

                shoppingCart.ReturnUrl = baseUrl + MvcDemoConstants.RouteCheckOutPaypal + "?PaymentOptionId=" +
                                         paymentInformationModel.PaymentOption.PaymentOptionId;
                shoppingCart.CancelUrl = baseUrl + MvcDemoConstants.RouteShoppingCart;
            }

            shoppingCart.Payment = paymentInformationModel;
            shoppingCart.Payment.BillingAddress =
                AddressViewModelMap.ToModel(_billingAddress ?? shippingAddress);

            var checkout = CartViewModelMap.ToShoppingCartModel(account, shoppingCart, purchaseOrderNumber);

            OrderModel order = null;

            try
            {
                checkout.ShippingAddress = null;

                order = _orderClient.CreateOrder(checkout);

                if (order.IsOffsitePayment)
                {
                    HttpContext.Current.Response.Redirect(order.ReceiptHtml);
                    return new OrderViewModel() { HasError = true };
                }

            }
            catch (ZnodeException exception)
            {
                return new OrderViewModel() { HasError = true, ErrorMessage = exception.ErrorMessage };
            }

            // reset the cart
            RemoveCookie(MvcDemoConstants.CartCookieKey);
            SaveInSession(MvcDemoConstants.CartModelSessionKey, new ShoppingCartModel());

            // Reset the address.
            RemoveInSession(MvcDemoConstants.BillingAddressKey);
            RemoveInSession(MvcDemoConstants.ShippingAddressKey);
            RemoveInSession(MvcDemoConstants.MultiCartModelSessionKey);
            RemoveInSession(MvcDemoConstants.MultiCartShipmentSessionKey);

            _accountAgent.UpdateAccountViewModel(AccountViewModelMap.ToOrderViewModel(order));

            return new OrderViewModel { EmailId = account.Email, OrderId = order.OrderId };

        }

        /// <summary>
        /// Submitted the order from the payment library updating the order details
        /// </summary>
        /// <param name="token">string token</param>
        /// <param name="paymentOptionId">int payment option id</param>
        /// <returns>This method will submit the custom order </returns>
        public OrderViewModel SubmitCustomOrder(string token, int paymentOptionId)
        {
            ShoppingCartModel shoppingCart = GetFromSession<ShoppingCartModel>(MvcDemoConstants.CartModelSessionKey);

            shoppingCart.Token = token;

            if (Equals(shoppingCart, null) || Equals(shoppingCart.Payment, null) || Equals(shoppingCart.Payment.BillingAddress, null))
            {
                return new OrderViewModel() { HasError = true };
            }

            if (!token.ToLower().Equals("null"))
            {
                shoppingCart.Token = token;
            }

            PaymentsViewModel payments = GetPaymentModel();

            PaymentOptionViewModel paymentOptionViewModel = payments.PaymentOptions.FirstOrDefault(x => x.PaymentOptionId == paymentOptionId);

            SaveInSession(MvcDemoConstants.CartModelSessionKey, shoppingCart);

            return SubmitOrder(paymentOptionViewModel, string.Empty);
        }

        public string GetPaymentGatwayNameByPaymetOptionId(int? paymetnOptionId, out int? paymentProfileId)
        {
            paymentProfileId = null;
            string gatwayname = string.Empty;
            FilterCollection filters = new FilterCollection();

            filters.Add(new FilterTuple(FilterKeys.IsActive, FilterOperators.Equals, FilterKeys.ActiveIndTrue));
            filters.Add(new FilterTuple(FilterKeys.PaymentSettingID, FilterOperators.Equals, paymetnOptionId.Value.ToString()));

            SortCollection sort = new SortCollection();
            sort.Add(SortKeys.DisplayOrder, SortDirections.Ascending);

            _paymentOptionClient.RefreshCache = true;
            PaymentOptionListModel paymentOptions = _paymentOptionClient.GetPaymentOptions(new ExpandCollection { ExpandKeys.PaymentGateway, ExpandKeys.PaymentType }, filters, sort);

            PaymentOptionModel payModel = paymentOptions.PaymentOptions.FirstOrDefault(x => x.PaymentOptionId.Equals(paymetnOptionId));

            if (!Equals(payModel, null) && !Equals(payModel.PaymentGateway, null))
            {
                gatwayname = payModel.PaymentGateway.Name;
                paymentProfileId = payModel.ProfileId;
            }
            return gatwayname;
        }

        /// <summary>
        /// Set the payment option success and error message
        /// </summary>
        /// <param name="model">Payments View Model</param>
        /// <param name="success">payment option available or not</param>
        public void SetMessages(PaymentsViewModel model)
        {
            SetStatusMessage(model);
        }

        /// <summary>
        /// User is logged In from single page checkout page after Order Placed.
        /// </summary>
        /// <param name="model">ViewModel of Checkout</param>
        public void LoginUser(CheckoutViewModel model)
        {
            if (!Equals(model, null) && !Equals(model.ShippingBillingAddressViewModel, null) && !Equals(model.ShippingBillingAddressViewModel.LoginViewModel, null))
            {
                LoginViewModel loginViewModel = _accountAgent.Login(model.ShippingBillingAddressViewModel.LoginViewModel);

                if (!Equals(loginViewModel, null) && !loginViewModel.HasError && !string.IsNullOrEmpty(model.ShippingBillingAddressViewModel.LoginViewModel.Username))
                {
                    _authenticationAgent.SetAuthCookie(model.ShippingBillingAddressViewModel.LoginViewModel.Username, true);
                }
            }
        }

        public bool IsValidAddressForCheckout(AddressViewModel shippingAddress)
        {
            bool? enableAddressValidation = PortalAgent.CurrentPortal.EnableAddressValidation;
            bool? requireValidatedAddress = PortalAgent.CurrentPortal.RequireValidatedAddress;
            if (!Equals(enableAddressValidation, null) && Equals(enableAddressValidation, true) && !Equals(requireValidatedAddress, null) && Equals(requireValidatedAddress, true))
            {
                // Do not allow the customer to go to next page if valid shipping address required is enabled. 
                bool isAddressValid = _accountAgent.IsAddressValid(shippingAddress);
                return isAddressValid ? true : false;
            }
            return true;
        }

        public bool IsAddressExist(AddressViewModel address)
        {
            if (!Equals(address, null))
            {
                if (string.IsNullOrEmpty(address.StreetAddress1))
                {
                    return false;
                }
                else if (string.IsNullOrEmpty(address.City))
                {
                    return false;
                }
                else if (string.IsNullOrEmpty(address.StateCode))
                {
                    return false;
                }
                else if (string.IsNullOrEmpty(address.CountryCode))
                {
                    return false;
                }
                else if (string.IsNullOrEmpty(address.PostalCode))
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            return false;
        }

        public PaymentsViewModel GetSinglePageCheckoutPaymentModel()
        {
            // Get All profile payment options.
            var allList = _paymentOptionsClient.GetPaymentOptions(
                new ExpandCollection() { ExpandKeys.PaymentType, ExpandKeys.PaymentGateway },
                new FilterCollection()
                    {
                        new FilterTuple(FilterKeys.ProfileId, FilterOperators.Equals, "null"),
                        new FilterTuple(FilterKeys.IsActive, FilterOperators.Equals, "true")
                    },
                new SortCollection());


            var accountModel = _accountAgent.GetAccountViewModel();

            // Get Account ProfileID, or Get Default Anonymous ProfileId from Portal
            int? profileId = null;
            profileId = accountModel != null ? accountModel.ProfileId : PortalAgent.CurrentPortal.DefaultAnonymousProfileId;

            // Get Profile based options and merge with All Profile options.
            if (profileId.HasValue)
            {
                var profileList = _paymentOptionsClient.GetPaymentOptions(
                    new ExpandCollection() { ExpandKeys.PaymentType, ExpandKeys.PaymentGateway },
                    new FilterCollection()
                        {
                            new FilterTuple(FilterKeys.ProfileId, FilterOperators.Equals, profileId.ToString()),
                            new FilterTuple(FilterKeys.IsActive, FilterOperators.Equals, "true")
                        },
                    new SortCollection());

                if (profileList.PaymentOptions != null && profileList.PaymentOptions.Any())
                {
                    allList.PaymentOptions =
                        new Collection<PaymentOptionModel>(
                            allList.PaymentOptions.Union(profileList.PaymentOptions).OrderBy(x => x.DisplayOrder).ToList());
                }

                //For Franchise Website - only franchise option must show.
                bool isFranchiseStore = _accountClient.IsRoleExistForProfile(PortalAgent.CurrentPortal.DefaultAnonymousProfileId, MvcDemoConstants.Franchise);
                if (isFranchiseStore)
                {
                    allList.PaymentOptions = Equals(profileList.PaymentOptions, null) ? new Collection<PaymentOptionModel>() : profileList.PaymentOptions;
                }
                else if (Equals(allList.PaymentOptions, null))
                {
                    allList.PaymentOptions = new Collection<PaymentOptionModel>();
                }

            }
            return PaymentsViewModelMap.ToPaymentsViewModel(allList.PaymentOptions.GroupBy(i => i.PaymentTypeId).Select(g => g.First()).ToList<PaymentOptionModel>().ToCollection());

        }

        public string CallTwoCoPaymentMethod(int? paymentOptionId, string twoCOToken)
        {
            ShoppingCartModel shoppingCart = GetFromSession<ShoppingCartModel>(MvcDemoConstants.CartModelSessionKey);
            if (Equals(shoppingCart, null))
            {
                return Resources.ZnodeResource.ErrorNoItemsShoppingCart;
            }

            AddressViewModel _shippingAddress = GetShippingAddess();
            if (Equals(_shippingAddress, null))
            {
                return Resources.ZnodeResource.ErrorCustomerShippingAddress;
            }

            // Validate the customer shipping address if address validation enabled.
            if (!Equals(PortalAgent.CurrentPortal.EnableAddressValidation, null) || Equals(PortalAgent.CurrentPortal.EnableAddressValidation))
            {
                bool isAddressValid = _accountAgent.IsAddressValid(_shippingAddress);
                // Do not allow the customer to go to next page if valid shipping address required is enabled.
                if (!isAddressValid && !Equals(PortalAgent.CurrentPortal.RequireValidatedAddress, null) && Equals(PortalAgent.CurrentPortal.RequireValidatedAddress))
                {
                    return Resources.ZnodeResource.ErrorCustomerShippingAddress;
                }
            }

            AddressViewModel addressFromSession = GetBillingAddessFromSession();

            if (Equals(addressFromSession, null))
            {
                addressFromSession = _shippingAddress;
            }

            PaymentViewModel model = PaypalPaymentViewModelMap.ToViewModel(shoppingCart, addressFromSession);
            string baseUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + HttpContext.Current.Request.ApplicationPath;

            model.ReturnUrl = baseUrl + MvcDemoConstants.RouteCheckOutSuccess + "?PaymentOptionId=" + paymentOptionId.Value;
            model.CancelUrl = baseUrl + MvcDemoConstants.RouteShoppingCart;
            model.PaymentSettingId = paymentOptionId.Value;
            model.TwoCOToken = twoCOToken;

            return twoCoUrl(model);

        }

        private string twoCoUrl(PaymentViewModel model)
        {
            if (!Equals(model, null))
            {
                var data = JsonConvert.SerializeObject(model);

                using (WebClient client = new WebClient())
                {
                    client.Encoding = System.Text.Encoding.UTF8;
                    client.Headers[HttpRequestHeader.ContentType] = "application/json";
                    client.Headers[HttpRequestHeader.Accept] = "text/xml";

                    string jsonModel = client.UploadString(string.Format("{0}{1}", ConfigurationManager.AppSettings["PaymentApplicationUrl"], payNowUrl), data);

                    if (!string.IsNullOrEmpty(jsonModel))
                    {
                        try
                        {
                            JavaScriptSerializer serializer = new JavaScriptSerializer();
                            dynamic item = serializer.Deserialize<object>(jsonModel);
                            return item["Data"];
                        }
                        catch
                        {

                        }
                    }
                }
            }
            return string.Empty;
        }

        #region PRFT Custom Methods
        public void GetOrderDetailsForOrderReceipt(string AdditionalInstructions)
        {
            var cart = GetFromSession<ShoppingCartModel>(MvcDemoConstants.CartModelSessionKey);
            cart.AdditionalInstructions = AdditionalInstructions;
            SaveInSession(MvcDemoConstants.CartModelSessionKey, cart);

            var Shoppingcart = GetFromSession<ShoppingCartModel>(MvcDemoConstants.CartModelSessionKey);


        }
        #endregion region
    }
}