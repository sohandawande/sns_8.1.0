﻿using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Agents
{
    public partial interface ICartAgent
    {
        CartViewModel Create(CartItemViewModel cartItem);                
        CartViewModel GetCart();
        int GetCartCount();
        CartViewModel Calculate(ShoppingCartModel shoppingCartModel);
        bool RemoveItem(string guid);
        bool UpdateItem(string guid, int qty);
        bool Merge();
        bool IsCouponValid(string couponCode);
        CartViewModel ApplyCoupon(string couponCode);
        void SetMessages(CartViewModel model);        
        /// <summary>
        /// Znode version 7.2.2 
        /// This function will Removes all item from shopping cart.
        /// </summary>
        /// <returns>Returns true or false.</returns>
        bool RemoveAllCartItems();

        /// <summary>
        /// Znode version 7.2.2 
        /// This function will Removes all item from multi shopping cart.
        /// </summary>
        /// <returns>Returns true or false.</returns>
        bool RemoveMultiCartItem();

        /// <summary>
        /// To Check the quantity of attributes, addOns etc of items in cart
        /// </summary>
        /// <param name="cartItem">Current item in catr</param>
        /// <returns>ProductViewModel</returns>
        ProductViewModel WishlistCheckInventory(CartItemViewModel cartItem);

        /// <summary>
        /// Gets a promotion URL enabled value having coupon code equals to the required coupon code.
        /// </summary>
        /// <param name="CouponCode">Coupon code required.</param>
        /// <returns>Returns Coupon URL enabled value.</returns>
        bool IsCouponUrlEnabled(string CouponCode);
    }
}