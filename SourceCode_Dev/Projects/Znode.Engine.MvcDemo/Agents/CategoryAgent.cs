﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.Maps;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Agents
{
    public partial class CategoryAgent : BaseAgent, ICategoryAgent
    {
        private readonly ICategoriesClient _categoriesClient;
        private readonly IAccountAgent _accountAgent;
        private readonly IThemeClient _themeClient;
        private readonly ICategoryNodesClient _categoryNodesClient;

        private readonly ExpandCollection _expands = new ExpandCollection()
            {
                ExpandKeys.Images,
                ExpandKeys.Subcategories,
                ExpandKeys.CategoryNodes,
            };

        /// <summary>
        /// Category Agent Constructor
        /// </summary>
        public CategoryAgent()
        {
            _categoriesClient = GetClient<CategoriesClient>();
            _accountAgent = new AccountAgent();
            _themeClient = new ThemeClient();
            _categoryNodesClient = GetClient<CategoryNodesClient>();
        }

        /// <summary>
        /// Get the categories based on the catalog.
        /// </summary>
        /// <returns>Returns the categories as category list view model</returns>
        public CategoryListViewModel GetCategories()
        {
            var accountModel = _accountAgent.GetAccountViewModel();
            int? profileId = accountModel != null ? accountModel.ProfileId : PortalAgent.CurrentPortal.DefaultAnonymousProfileId;

            var list = _categoriesClient.GetCategoriesByCatalog(PortalAgent.CurrentPortal.CatalogId, new ExpandCollection() { ExpandKeys.CategoryNodes, ExpandKeys.CategoryProfiles }, new FilterCollection()
                        {
                            new FilterTuple(FilterKeys.IsVisible, FilterOperators.Equals,"true"),

                        }, null);


            if (list.Categories != null)
            {
                var tcategories =
                    list.Categories.Where(
                        p =>
                        p.CategoryProfiles.Any(y => y.ProfileID == profileId && y.EffectiveDate > System.DateTime.Now))
                              .ToList();


                return CategoryListViewModelMap.ToViewModel(list.Categories.Except(tcategories), PortalAgent.CurrentPortal.CatalogId);
            }
            return new CategoryListViewModel() { HasError = true, ErrorMessage = Resources.ZnodeResource.ErrorNoCategoryListFound };
        }

        /// <summary>
        /// Get the category based on the category id.
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns>Returns the category as category view model</returns>
        public CategoryViewModel GetCategory(int categoryId)
        {
            var categoryResult = _categoriesClient.GetCategory(categoryId, _expands);

            if (categoryResult != null)
            {
                return CategoryViewModelMap.ToCategoryViewModel(categoryResult);
            }

            return new CategoryViewModel() { HasError = true, ErrorMessage = Resources.ZnodeResource.ErrorNoCategoryFound };
        }

        /// <summary>
        /// Get the category based on the category name.
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns>Returns the category as category view model</returns>
        public CategoryViewModel GetCategoryByName(string categoryName, bool showSubCategory = false, int categoryId = 0)
        {
            ExpandCollection expands = null;
            FilterCollection filterCollection = new FilterCollection();

            if (showSubCategory)
            {
                expands = new ExpandCollection() { ExpandKeys.Subcategories, ExpandKeys.CategoryNodes };
            }
            if (categoryId > 0)
            {
                filterCollection.Add(new FilterTuple(FilterKeys.CategoryId, FilterOperators.Equals, categoryId.ToString()));
            }
            else
            {
                filterCollection.Add(new FilterTuple(FilterKeys.Name, FilterOperators.Equals, categoryName));
            }

            var categoryList = _categoriesClient.GetCategories(expands, filterCollection, null);

            if (categoryList != null && categoryList.Categories != null)
            {
                var categoryDetails = CategoryViewModelMap.ToCategoryViewModel(categoryList.Categories.First());
                if (categoryDetails != null)
                {
                    categoryDetails.CategoryTitle = categoryDetails.Title;
                    categoryDetails.Title =
                          !string.IsNullOrEmpty(categoryDetails.SeoTitle)
                                 ? categoryDetails.SeoTitle
                                 : categoryName;
                    return categoryDetails;
                }
            }

            return new CategoryViewModel() { HasError = true, ErrorMessage = Resources.ZnodeResource.ErrorNoCategoryFound };
        }

        public SubcategoriesViewModel GetSubCategories(CategoryViewModel model, SearchRequestModel searchRequestModel)
        {
            SubcategoriesViewModel _subcategories = new SubcategoriesViewModel();
            if (!Equals(model, null) && !Equals(model.Subcategories, null) && !Equals(searchRequestModel, null))
            {
                ISearchAgent _searchAgent = new SearchAgent();

                _subcategories.Subcategories.AddRange(model.Subcategories);
                _subcategories.Subcategories.ForEach(x =>
                {
                    searchRequestModel.Category = x.Name;
                    x.ProductCount = _searchAgent.Search(searchRequestModel).Products.Count;
                });
            }
            return _subcategories;
        }

        public void GetProductCategory(KeywordSearchViewModel result, string category, CategoryViewModel categoryDetails)
        {
            if (!Equals(result, null) && !Equals(result.Products, null))
            {
                foreach (ProductViewModel product in result.Products)
                {
                    product.CategoryId = categoryDetails.CategoryId;
                    product.CategoryName = category;
                }
            }
        }

        public string GetThemeNameById(int themeId)
        {
            if (themeId > 0)
            {
                var filters = new FilterCollection { new FilterTuple(FilterKeys.CSSThemeId, FilterOperators.Equals, themeId.ToString()) };
                var themeModel = _themeClient.GetThemes(filters, null);
                return (!Equals(themeModel, null) && !Equals(themeModel.Themes, null) && themeModel.Themes.Count > 0) ? themeModel.Themes.First().Name : "Default";
            }
            return "Default";
        }

        public string GetBreadCrumb(int categoryId, string seoUrl, bool isProductPresent)
        {
            BreadCrumbModel model = new BreadCrumbModel();
            model.CategoryID = categoryId;
            model.SEOUrl = seoUrl;
            model.IsProductPresent = isProductPresent;
            model.CatalogId = PortalAgent.CurrentPortal.CatalogId;
            BreadCrumbModel models = _categoryNodesClient.GetBreadCrumb(model);
            return Equals(models, null) && Equals(models.BreadCrumb, null) ? string.Empty : models.BreadCrumb;
        }

        public CategoryListViewModel GetSiteMapDetails()
        {
            int currentCatalogId = PortalAgent.CurrentPortal.CatalogId;

            _categoriesClient.RefreshCache = true;

            CategoryListModel categorylist = _categoriesClient.GetCategoryTree(null, new FilterCollection()
                       {
                           new FilterTuple(FilterKeys.CatalogId, FilterOperators.Equals,Convert.ToString(currentCatalogId)),
                           new FilterTuple(FilterKeys.CategoryName, FilterOperators.Equals,string.Empty),

                       }, null, null, null);
            CategoryListViewModel categoryListViewModel = new CategoryListViewModel();
            
            if (!Equals(categorylist, null) && !Equals(categorylist.Categories, null))
            {
                SearchAgent _searchAgent = new SearchAgent();
                SearchRequestModel searchRequestModel;
                foreach (CategoryModel category in categorylist.Categories)
                {
                    if (!Equals(category, null) && Equals(category.ParentCategoryNodeId, 0) && category.IsActive)//Filters only parent category.
                    {
                       searchRequestModel=new SearchRequestModel();
                        searchRequestModel.Category=category.Name;
                        searchRequestModel.RefineBy = new Collection<KeyValuePair<string, IEnumerable<string>>>();
                        searchRequestModel.Sort = string.Empty;
                        searchRequestModel.PageSize = -1;
                        int categoryProductCount = _searchAgent.Search(searchRequestModel).Products.Count;
                        categoryListViewModel.Categories.Add(new CategoryHeaderViewModel
                        {
                            CategoryName = category.Name,
                            ActiveProductCount = categoryProductCount,
                            SEOPageName = category.SeoUrl,
                            CategoryId = category.CategoryId,
                            
                            //PRFT Custom Code
                            Custom1=category.Custom1
                        });
                    }
                }
            }

            return categoryListViewModel;
        }
    }
}