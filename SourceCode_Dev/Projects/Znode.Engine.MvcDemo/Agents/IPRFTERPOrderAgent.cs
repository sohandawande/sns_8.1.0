﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Agents
{
    public interface IPRFTERPOrderAgent
    {
        PRFTOEHeaderHistoryViewModel GetInvoiceDetailsFromERP(string externalId);
    }
}