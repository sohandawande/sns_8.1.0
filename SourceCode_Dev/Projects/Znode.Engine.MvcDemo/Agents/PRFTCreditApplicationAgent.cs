﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.Maps;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Agents
{
    public class PRFTCreditApplicationAgent : BaseAgent, IPRFTCreditApplicationAgent
    {
        #region Private Readonly Variables
        private readonly IPRFTCreditApplicationClient _crditApplicationClient;
        private readonly IStatesClient _statesClient;
        private readonly ICountriesClient _countriesClient;
        #endregion

        #region Constructors
        public PRFTCreditApplicationAgent()
        {
            _crditApplicationClient = new PRFTCreditApplicationClient();
            _countriesClient = new CountriesClient();
            _statesClient = new StatesClient();
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Get the States depending on the country code 
        /// </summary>
        /// <param name="addressId">Address ID</param>
        /// <returns>Returns the address as Address view model</returns>
        public Collection<StateModel> GetStates()
        {
            var stateListModel = _statesClient.GetStates(null, null);
            if (stateListModel != null && stateListModel.States != null)
            {
                return stateListModel.States;
            }
            return null;
        }

        /// <summary>
        /// This function returns list of all active billing and shipping countries on the basis of portal id.
        /// </summary>
        /// <param name="billingShippingFlag">int billingShippingFlag</param>
        /// <returns>Returns list of all active billing and shipping countries on the basis of portal id.</returns>
        public Collection<CountryModel> GetCountries(int billingShippingFlag = 0)
        {
            _countriesClient.RefreshCache = true;
            var countryListModel = _countriesClient.GetActiveCountryByPortalId(PortalAgent.CurrentPortal.PortalId, billingShippingFlag);
            if (countryListModel != null && countryListModel.Countries != null)
            {
                return countryListModel.Countries;
            }
            return null;
        }

        public bool SaveCreditApplicationInfo(PRFTCreditApplicationViewModel model)
        {
            //Set All Basic Values
            if (model.IsTaxExempt != null && model.IsTaxExempt == true && !Equals(model.TaxExemptCertificateAttachments, null) && !Equals(model.TaxExemptCertificateAttachments.FileName, null))
            {
                model.TaxExemptCertificate = UploadTaxExcemptCertificateAndGetName(model.TaxExemptCertificateAttachments);
            }

            PRFTCreditApplicationModel creditApplicationModel = _crditApplicationClient.CreateCreditApplication(PRFTCreditApplicationViewModelMap.ToModel(model));
            return Equals(creditApplicationModel, null) ? false : true;
            
        }

        /// <summary>
        /// Save Tax Excempt Certificate and returns the name to save in database
        /// </summary>
        /// <param name="attachement"></param>
        /// <returns>Uploaded File Name</returns>
        private string UploadTaxExcemptCertificateAndGetName(HttpPostedFileBase attachement)
        {
            string fileName = string.Empty;
            if (attachement.ContentLength > 0)
            {
                try
                {
                    // Generate file name 
                    Guid tmpGuid = Guid.NewGuid();
                    fileName = tmpGuid.ToString().Replace("-", "") + Path.GetExtension(attachement.FileName);
                    if (!fileName.Equals(string.Empty))
                    {
                        string filePath = Path.Combine(ConfigurationManager.AppSettings["CreditApplicationTaxCertificatePath"], fileName);
                        attachement.SaveAs(filePath);
                    }
                }
                catch (Exception ex)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                }
            }
            return fileName;
        }
        #endregion
    }
}