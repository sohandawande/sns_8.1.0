﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.MvcDemo.Agents
{
    /// <summary>
    /// This is the interface which will deal with the Clear API functionality
    /// </summary>
    public interface IClearAPICacheAgent
    {
        /// <summary>
        /// This method will clear the API Cache.
        /// </summary>
        /// <returns>true if cache cleared</returns>
        bool ClearApiCache();
    }
}
