﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Web.Mvc;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Agents
{
    /// <summary>
    /// Store Locator Interface
    /// </summary>
    public interface IStoreLocatorAgent
    {
        /// <summary>
        /// Get the list of stores within area
        /// </summary>
        /// <param name="model">Model of Store Locator</param>
        /// <returns>Collection of store locator information</returns>
        Collection<StoreLocatorViewModel> GetStoresList(StoreLocatorViewModel model);

        /// <summary>
        /// Get the list of Distance in miles
        /// </summary>
        /// <returns>list of Distance in key value pair</returns>
        List<SelectListItem> GetDistanceList();
    }
}
