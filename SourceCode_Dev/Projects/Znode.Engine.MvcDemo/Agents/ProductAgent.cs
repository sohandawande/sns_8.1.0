﻿using Resources;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.MvcDemo.Helpers;
using Znode.Engine.MvcDemo.Maps;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Agents
{
    public partial class ProductAgent : BaseAgent, IProductAgent
    {
        readonly IProductsClient _client;
        readonly ISkuAgent _skuAgent;
        readonly IShoppingCartsClient _shoppingCartsClient;
        readonly IReviewsClient _reviewsClient;
        readonly ICategoriesClient _categoriesClient;
        readonly IAccountAgent _accountAgent;
        readonly ISearchAgent _searchAgent;
        readonly ISuppliersClient _supplierClient;
        readonly PortalAgent _portalAgent;
        readonly IAccountsClient _accountClients;
        readonly ICategoryNodesClient _categoryNodesClient;

        public ProductAgent()
        {
            _client = GetClient<ProductsClient>();
            _shoppingCartsClient = GetClient<ShoppingCartsClient>();
            _reviewsClient = GetClient<ReviewsClient>();
            _skuAgent = new SkuAgent();
            _categoriesClient = GetClient<CategoriesClient>();
            _accountAgent = new AccountAgent();
            _searchAgent = new SearchAgent();
            _supplierClient = GetClient<SuppliersClient>();
            _portalAgent = new PortalAgent();
            _accountClients = GetClient<AccountsClient>();
            _categoryNodesClient = GetClient<CategoryNodesClient>();
        }

        /// <summary>
        /// Get the product based on the product id
        /// </summary>
        /// <param name="productId">Product ID</param>
        /// <returns>Returns the product details as product view model</returns>
        public ProductViewModel GetProduct(int productId)
        {
            Expands = new ExpandCollection();
            Expands.Add(ExpandKeys.Skus);
            Expands.Add(ExpandKeys.Attributes);
            Expands.Add(ExpandKeys.Promotions);
            Expands.Add(ExpandKeys.Categories);
            Expands.Add(ExpandKeys.AddOns);
            Expands.Add(ExpandKeys.Reviews);
            Expands.Add(ExpandKeys.Images);
            Expands.Add(ExpandKeys.BundleItems);
            Expands.Add(ExpandKeys.FrequentlyBoughtTogether);
            Expands.Add(ExpandKeys.YouMayAlsoLike);
            Expands.Add(ExpandKeys.Manufacturer);

            var product = _client.GetProduct(productId, Expands);

            if (product == null)
            {
                return null;
            }
            // If the product is not active redirect to home page
            if (!product.IsActive)
            {
                return null;
            }

            // Check if it is this store product.
            bool isStoreProduct = this.IsStoreProduct(product);
            if (!isStoreProduct)
            {
                return null;
            }

            var productViewModel = ProductViewModelMap.ToViewModel(product);

            var accountViewModel = _accountAgent.GetAccountViewModel();
            if (!Equals(accountViewModel, null) && Convert.ToBoolean(accountViewModel.EnableCustomerPricing))
            {
                if (product.Skus.FirstOrDefault().CustomerPricingSku.Count > 0)
                {
                    productViewModel.ProductPrice = (from sku in product.Skus.ToList()
                                                     from customerPricingSku in sku.CustomerPricingSku.ToList()
                                                     select Equals(customerPricingSku.ExternalAccountNo, accountViewModel.ExternalId) ? (Equals(customerPricingSku.NegotiatedPrice.Value, null) ? sku.ProductPrice.Value : customerPricingSku.NegotiatedPrice.Value) : product.RetailPrice.Value).FirstOrDefault();
                }
            }

            // Check the bundles and populate it.
            if (!string.IsNullOrEmpty(productViewModel.BundleItemsIds))
            {
                var bundleViewModel = GetBundles(productViewModel.BundleItemsIds);

                if (bundleViewModel == null)
                {
                    return null;
                }
            }

            // Populate the product attributes
            if (productViewModel.ProductAttributes.Attributes.Any())
            {
                int attributeId = productViewModel.ProductAttributes.Attributes.FirstOrDefault().ProductAttributes.FirstOrDefault().AttributeId;
                string selectedSku = string.Empty;
                string imagePath = string.Empty;
                //ZNode Version 7.2.2 - Add imageMediumPath Out parameter
                string imageMediumPath = string.Empty;

                productViewModel.Price = _skuAgent.GetSkusPrice(productId, attributeId, productViewModel.Price, out selectedSku, out imagePath, out imageMediumPath);
                productViewModel.Sku = selectedSku;

                productViewModel.ProductAttributes = UpdateProductAttributes(productViewModel, attributeId, string.Empty);

                if (Path.GetFileName(imagePath) != Path.GetFileName(PortalAgent.CurrentPortal.ImageNotAvailablePath))
                {
                    productViewModel.ImageLargePath = imagePath;
                }

            }
            
            // Check the inventory
            if (!productViewModel.IsCallForPricing)
            {
                IEnumerable<int> attributeId = new List<int>();

                if (productViewModel.ProductAttributes.Attributes.Any())
                {
                    if (productViewModel.ProductAttributes.Attributes.First().ProductAttributes.Any())
                        attributeId = productViewModel.ProductAttributes.Attributes.Select(x => x.ProductAttributes.First(y => y.Available).AttributeId);
                }

                CheckInventory(productViewModel, string.Join(",", attributeId), productViewModel.SelectedQuantity);
            }
            else
            {
                productViewModel.InventoryMessage = string.Format(Resources.ZnodeResource.TextErrorFormat, Resources.ZnodeResource.TextCallForPricing);
                productViewModel.ShowAddToCart = false;
            }

            // Populate the product add-on
            if (productViewModel.AddOns != null && productViewModel.AddOns.Any())
            {
                SetAddOnPrice(productViewModel);

                decimal finalProductPrice;
                productViewModel.AddOns = GetAddOnDetails(productViewModel, string.Join(",", productViewModel.AddOns.Where(x => x.SelectedAddOnValue != null).SelectMany(x => x.SelectedAddOnValue)), 1, productViewModel.ProductPrice, out finalProductPrice, productViewModel.Sku);

                productViewModel.OriginalPrice = finalProductPrice;
            }



            // Populate the product Alternate Image
            if (productViewModel.Images != null && productViewModel.Images.Any())
            {
                //Filter All Product images to get the swatch iamges based on ProductImageTypeId.
                ProductViewModel swatchImageModel = productViewModel;
                productViewModel.SwatchImages = swatchImageModel.Images.Where(x => x.ProductImageTypeId == 2 && x.IsActive).OrderBy(x => x.DisplayOrder).ToList();
                if (productViewModel.SwatchImages != null && productViewModel.SwatchImages.Any())
                {
                    productViewModel.SwatchImages.Add(new ImageViewModel { ImageAltTag = productViewModel.ImageAltTag, ImageSmallThumbnailPath = productViewModel.ImageSmallThumbnailPath, ImageMediumPath = productViewModel.ImageMediumPath, ImageLargePath = productViewModel.ImageLargePath });
                }
                productViewModel.Images = productViewModel.Images.Where(x => x.ProductImageTypeId == 1 && x.IsActive).OrderBy(x => x.DisplayOrder).ToList();
            }

            // For Displaying the category name in the product page
            var categoryName = GetFromSession<string>(Helpers.MvcDemoConstants.CategoryName);
            int categoryId = GetFromSession<Int32>(Helpers.MvcDemoConstants.CategoryId);
            if (product.Categories.Any(x => Equals(x.Name, categoryName)))
            {
                productViewModel.CategoryName = categoryName;
                productViewModel.CategoryId = categoryId;
            }
            else
            {
                productViewModel.CategoryName = product.Categories.FirstOrDefault().Name;
                productViewModel.CategoryId = product.Categories.FirstOrDefault().CategoryId;
                SaveInSession(MvcDemoConstants.CategoryId, productViewModel.CategoryId);
                SaveInSession(MvcDemoConstants.CategoryName, productViewModel.CategoryName);
            }

            productViewModel.WishListID = _accountAgent.CheckWishListExists(productId, accountViewModel != null ? accountViewModel.AccountId : 0);
            productViewModel.ShowWishlist = productViewModel.WishListID > 0;

            //productViewModel.Title = (!string.IsNullOrEmpty(product.SeoTitle)) ? product.SeoTitle : product.Name;
            //PRFT Custom Code : Start
            string titleSuffix = ConfigurationManager.AppSettings["StoreName"];
            string productTitle = !string.IsNullOrEmpty(product.SeoTitle) ? product.SeoTitle : product.Name;
            productViewModel.Title = productTitle;
            if (!productTitle.Contains(titleSuffix))
            {
                productViewModel.Title = string.Format("{0} {1}", productTitle, titleSuffix);
            }
            
            //PRFT Custom Code : End

            //Show Vendor Name in Product Details, in case product belongs to Vendor - Start
            if (product.SupplierId > 0)
            {
                //Gets Supplier Details based on Id.
                var supplierDetails = _supplierClient.GetSupplier((int)product.SupplierId);
                productViewModel.VendorName = string.Format("{0} {1} ({2})", supplierDetails.ContactFirstName, supplierDetails.ContactLastName, supplierDetails.ContactEmail);
            }
            //Show Vendor Name in Product Details, in case product belongs to Vendor - End
            return productViewModel;
        }

        /// <summary>
        /// Get the Bundled product details 
        /// </summary>
        /// <param name="ids">bundled product ids</param>
        /// <returns>Collection of products</returns>
        public BundleDetailsViewModel GetBundleProductsDetails(CartItemViewModel cartItemViewModel)
        {
            var bundleViewModel = new BundleDetailsViewModel();
            bundleViewModel.SelectedAttributesIds = cartItemViewModel.AttributeIds;
            bundleViewModel.SelectedProductId = cartItemViewModel.ProductId;
            bundleViewModel.SelectedQuantity = cartItemViewModel.Quantity;
            bundleViewModel.SelectedAddOnValueIds = cartItemViewModel.AddOnValueIds;
            bundleViewModel.AddOnValuesCustomText = cartItemViewModel.AddOnValuesCustomText;
            bundleViewModel.SelectedSku = cartItemViewModel.Sku;
            bundleViewModel.ProductName = cartItemViewModel.ProductName;
            bundleViewModel.SeoPageName = cartItemViewModel.SeoPageName;

            var items = cartItemViewModel.BundleItemsIds.Split(',');

            var productsModel = _searchAgent.GetProductsByIds(items.ToList());

            if (productsModel != null && productsModel.Count > 0)
            {
                bundleViewModel.Bundles = productsModel.Where(x => x.IsActive).Select(ProductViewModelMap.ToViewModel).ToList();

                foreach (var bundle in bundleViewModel.Bundles)
                {
                    bundle.ShowAddToCart = true;
                    IEnumerable<int> attributeIds = new List<int>();

                    // Initial Load of the dropdown
                    if (bundle.ProductAttributes.Attributes.Any())
                    {
                        int attributeId =
                            bundle.ProductAttributes.Attributes.FirstOrDefault()
                                  .ProductAttributes.FirstOrDefault()
                                  .AttributeId;


                        string selectedSku = string.Empty;

                        bundle.Sku = selectedSku;
                        bundle.ProductAttributes = UpdateProductAttributes(bundle, attributeId, string.Empty);

                        attributeIds = bundle.ProductAttributes.Attributes.Select(x => x.ProductAttributes.First(y => y.Available).AttributeId);
                    }

                    CheckInventory(bundle, string.Join(",", attributeIds), bundle.SelectedQuantity);

                    if (bundle.AddOns != null && bundle.AddOns.Any())
                    {
                        var selectedAddOnValue = string.Join(",", bundle.AddOns.Where(x => x.SelectedAddOnValue != null).SelectMany(x => x.SelectedAddOnValue).ToArray());
                        //selectedAddOnValue = selectedAddOnValue + "," + String.Join(",", bundle.AddOns.Where(x => x.CheckBoxSelectedAddOnValue != null).SelectMany(y => y.CheckBoxSelectedAddOnValue).ToArray());
                        CheckAddOnInventory(bundle, selectedAddOnValue, bundle.SelectedQuantity);
                    }

                    bundle.ShowQuantity = false;
                }
            }

            return bundleViewModel;
        }

        public BundleDetailsViewModel GetUpdatedBundles(BundleDetailsViewModel bundleCartItem)
        {
            var cartItem = new CartItemViewModel()
            {
                AddOnValueIds = bundleCartItem.SelectedAddOnValueIds,
                AttributeIds = bundleCartItem.SelectedAttributesIds,
                ProductId = bundleCartItem.SelectedProductId,
                Quantity = bundleCartItem.SelectedQuantity,
                Sku = bundleCartItem.SelectedSku,
                BundleItemsIds = string.Join(",", bundleCartItem.Bundles.Select(x => x.ProductId)),
                ProductName = bundleCartItem.ProductName,
                SeoPageName = bundleCartItem.SeoPageName,
            };

            var productsViewModel = GetBundleProductsDetails(cartItem);

            foreach (var currentBundleItem in productsViewModel.Bundles)
            {
                var currentBundleCartItem =
                           bundleCartItem.Bundles.FirstOrDefault(x => x.ProductId == currentBundleItem.ProductId);
                if (currentBundleItem != null && currentBundleCartItem != null)
                {

                    if (currentBundleItem.ProductAttributes != null &&
                        currentBundleItem.ProductAttributes.Attributes.Any())
                    {
                        decimal prodPrice;
                        string selectedSku;
                        string imagePath;
                        //ZNode Version 7.2.2 - Add imageMediumPath Out parameter
                        string imageMediumPath = string.Empty;

                        var attributeId = currentBundleCartItem.ProductAttributes.Attributes.FirstOrDefault().SelectedAttributeId;

                        if (currentBundleItem.ProductId == bundleCartItem.SelectedProductId)
                        {
                            attributeId = Convert.ToInt32(bundleCartItem.SelectedBundleAttributeId);
                        }

                        // Update the selected attributes
                        var selectedAttributeIds = string.Join(",",
                                                               currentBundleCartItem.ProductAttributes.Attributes.Select
                                                                   (
                                                                       y => y.SelectedAttributeId));

                        var model = GetAttributes(currentBundleItem.ProductId, attributeId, selectedAttributeIds,
                                                  cartItem.Quantity, out prodPrice, out selectedSku, out imagePath, out imageMediumPath);

                        currentBundleItem.ProductAttributes = model.ProductAttributes;
                        currentBundleItem.InventoryMessage = model.InventoryMessage;
                        currentBundleItem.ShowAddToCart = model.ShowAddToCart;
                        currentBundleItem.Sku = selectedSku;
                    }

                    // Update the selected add-Ons  
                    if (currentBundleItem.AddOns != null && currentBundleItem.AddOns.Any())
                    {
                        currentBundleItem.AddOns.ToList().ForEach(x =>
                        {
                            x.SelectedAddOnValue = !Equals(currentBundleCartItem.AddOns, null) ? currentBundleCartItem.AddOns.FirstOrDefault(z => z.AddOnId == x.AddOnId).SelectedAddOnValue : null;
                        });

                        var selectedAddOnValue = !Equals(currentBundleCartItem.AddOns, null) ? string.Join(",", currentBundleCartItem.AddOns.Where(x => x.SelectedAddOnValue != null).SelectMany(x => x.SelectedAddOnValue).ToArray()) : null;
                        currentBundleItem.ShowAddToCart = true;
                        CheckAddOnInventory(currentBundleItem, selectedAddOnValue, currentBundleItem.SelectedQuantity);
                    }
                }
            }

            return productsViewModel;
        }

        /// <summary>
        /// Update product attributes availablity based on selected attributes.
        /// </summary>
        /// <param name="product"></param>
        /// <param name="attributeId"></param>
        /// <param name="selectedAttributeIds"></param>
        /// <returns></returns>
        public ProductAttributesViewModel UpdateProductAttributes(ProductViewModel product, int? attributeId, string selectedAttributeIds)
        {
            // to keep already selected attribute item
            var attributeIds = selectedAttributeIds.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries)
                                    .Select(x => int.Parse(x));

            // select all the dropdowns ids that are selected, 
            var selectedAttributes = attributeIds.TakeWhile(x => x != attributeId).Union(new List<int> { attributeId.GetValueOrDefault(0) });

            // Get distinct SKU Ids for selected attribute dropdown values.
            var selectedAttributeSkuIds = product.ProductAttributes.Attributes.Select(attributeType =>
                {
                    var attributeViewModel =
                        attributeType.ProductAttributes.FirstOrDefault(
                            attribute => selectedAttributes.Any(x => x == attribute.AttributeId));

                    if (attributeViewModel != null)
                        return new { attributeType.AttributeTypeId, attributeViewModel.SkuIds };

                    // return empty collction if no attribute found.
                    return null;

                }).Where(x => x != null).Distinct();

            // Update "Available" and "Selected" flag only based on SKU Id combination that is based on selected dropdown options. 
            // e.g. 2nd dropdown should populate 1st dropdown selection, 3rd dropdown based on 2nd dropdown selection.
            // if 1st dropdown selected, then it will populate all other dropdowns based on first dropdown only.
            // if 2nd dropdown selected, 
            //      then 1st dropdown will populate as it is, 
            //      then 2nd dropdown will populate based on 1st dropdown selection only. 
            //      all other dropdowns based on combination of 1st and 2nd dropdown dropdown only.

            // Loop all attribute types and its attributes to set Available and Selected flag value.
            var toBeFiltered = false;
            foreach (var attributeType in product.ProductAttributes.Attributes)
            {
                attributeType.ToBeFiltered = toBeFiltered;
                foreach (var attribute in attributeType.ProductAttributes)
                {
                    // select all dropdown values upto currently selected one.
                    var skuIds = selectedAttributeSkuIds.TakeWhile(x => x.AttributeTypeId != attribute.AttributeTypeId);

                    // set first one if not.
                    if (!skuIds.Any())
                        skuIds = skuIds.Union(new[] { selectedAttributeSkuIds.FirstOrDefault() });

                    // Set available flag only it matches all the selected dropdown values
                    attribute.Available = skuIds.All(x => x.SkuIds.Any(v => attribute.SkuIds.Any(skuid => v == skuid)));

                    // Set the selected flag to repopulate to all dropdowns.
                    attribute.Selected = attributeIds.Any(attId => attId == attribute.AttributeId);
                }

                toBeFiltered = true;

                // Set which is currently selected dropdown.
                attributeType.IsSelectedOption = attributeType.ProductAttributes.Any(x => x.AttributeId == attributeId);
            }

            return product.ProductAttributes;
        }

        /// <summary>
        /// Get the product details by Ids
        /// </summary>
        /// <param name="ids">Product IDs</param>
        /// <returns>Returns the list of products as product view model</returns>
        public IEnumerable<ProductViewModel> GetProductByIds(string ids)
        {
            if (string.IsNullOrEmpty(ids))
            {
                return null;
            }
            Expands = new ExpandCollection();
            Expands.Add(ExpandKeys.Skus);
            Expands.Add(ExpandKeys.Attributes);
            Expands.Add(ExpandKeys.Promotions);
            IEnumerable<ProductViewModel> products = new Collection<ProductViewModel>();
            var idString = (ids.Split(',').Length == 1) ? ids + "," : ids;
            var list = _client.GetProductsByProductIds(idString, Expands, new SortCollection());
            products = products.Union(ProductViewModelMap.ToViewModels(list.Products));
            return products;
        }

        /// <summary>
        /// Get the product details by Ids
        /// </summary>
        /// <param name="ids">Product IDs</param>
        /// <param name="accountId">Account ID</param>
        /// <returns>Returns the list of products as product view model</returns>
        public async Task<IEnumerable<ProductViewModel>> GetProductByIdsAsyn(string ids, int accountId)
        {
            Expands = new ExpandCollection();
            Expands.Add(ExpandKeys.Skus);
            Expands.Add(ExpandKeys.Attributes);
            Expands.Add(ExpandKeys.Promotions);

            IEnumerable<ProductViewModel> products = new Collection<ProductViewModel>();
            var idString = (ids.Split(',').Length == 1) ? ids + "," : ids;
            var list = await _client.GetProductsByProductIdsAsync(idString, accountId, Expands, new SortCollection());
            var currentList = ProductViewModelMap.ToViewModels(new Collection<ProductModel>(list.Products.Where(x => x.IsActive).ToList()));
            if (currentList != null)
            {
                products = products.Union(currentList);
                if (accountId > 0)
                {
                    AccountModel accountViewModel = _accountClients.GetAccount(accountId);
                    if (!Equals(accountViewModel, null) && Convert.ToBoolean(accountViewModel.EnableCustomerPricing))
                    {
                        foreach (ProductViewModel item in currentList)
                        {
                            try
                            {
                                var data = (from product in list.Products.ToList()
                                            where Equals(product.ProductId, item.ProductId) && !Equals(product.Skus, null)

                                            from productSku in product.Skus.ToList()
                                            from CustomerPricingSku in productSku.CustomerPricingSku.ToList()
                                            where Equals(CustomerPricingSku.ExternalAccountNo, accountViewModel.ExternalId)
                                            select Equals(CustomerPricingSku.ExternalAccountNo, accountViewModel.ExternalId) ? CustomerPricingSku.NegotiatedPrice.Value : product.RetailPrice.Value).FirstOrDefault();

                                if (!Equals(data, null) && data > 0)
                                {
                                    item.ProductPrice = data;
                                }
                            }
                            catch (Exception ex)
                            { }
                        }
                    }
                }
            }
            return products;
        }

        /// <summary>
        /// Get the SKU price based on the product and attribute
        /// </summary>
        /// <param name="productId">Product ID</param>
        /// <param name="attributeId">Attribute ID</param>
        /// <returns>Returns the SKU Price</returns>
        public decimal GetSkuPrice(int productId, int attributeId)
        {
            Expands = new ExpandCollection();
            Expands.Add(ExpandKeys.Skus);
            Expands.Add(ExpandKeys.Attributes);
            Expands.Add(ExpandKeys.Promotions);

            var model = _client.GetProduct(productId, Expands);

            if (model != null)
            {

                if (model.ProductId == productId)
                {
                    var attribute = model.Skus.Where(x => x.Attributes.Equals(attributeId)).First();

                    foreach (var sku in model.Skus)
                    {
                        var attributeSelected = sku.Attributes.Where(x => x.AttributeId.Equals(attributeId)).FirstOrDefault();
                        if (attributeSelected != null)
                        {
                            if (sku.ProductPrice.HasValue)
                            {
                                return sku.ProductPrice.Value;
                            }
                            return model.RetailPrice.Value;
                        }
                    }


                }
            }

            return 0;
        }
        /// <summary>
        /// Returns Home Page specials for a store
        /// </summary>
        /// <returns>Collection of ProductViewModel</returns>
        public Collection<ProductViewModel> GetHomeSpecials()
        {
            Expands = new ExpandCollection { ExpandKeys.Promotions, ExpandKeys.Skus, ExpandKeys.Reviews, ExpandKeys.AddOns, ExpandKeys.BundleItems, ExpandKeys.Categories };
            _client.RefreshCache = true;
            var model = _client.GetProductsByHomeSpecials(Expands, null, new SortCollection(), 0, MvcDemoConstants.DefaultHomeSpecialsSize);


            if (model != null && model.Products != null && model.Products.Count > 0)
            {
                AccountViewModel accountViewModel = _accountAgent.GetAccountViewModel();
                if (!Equals(accountViewModel, null) && Convert.ToBoolean(accountViewModel.EnableCustomerPricing))
                {
                    List<ProductViewModel> productView = ProductViewModelMap.ToViewModels(new Collection<ProductModel>(model.Products.Where(x => x.IsActive).ToList()));

                    foreach (ProductViewModel item in productView)
                    {
                        try
                        {
                            var data = (from products in model.Products.ToList()
                                        where products.ProductId == item.ProductId && products.Skus != null
                                        from productSku in products.Skus.ToList()
                                        from customerPricing in productSku.CustomerPricingSku.ToList()
                                        where customerPricing.ExternalAccountNo == accountViewModel.ExternalId
                                        select customerPricing.NegotiatedPrice.Value).FirstOrDefault();

                            if (!Equals(data, null) && data > 0)
                            {
                                item.ProductPrice = data;
                            }
                        }
                        catch (Exception ex)
                        { }
                    }
                    return new Collection<ProductViewModel>(productView);
                }

                Collection<CategoryNodeModel> categoryNodes = _categoryNodesClient.GetCategoryNodes(null, new FilterCollection()
                                                            {
                                                                new FilterTuple(FilterKeys.CatalogId, FilterOperators.Equals,
                                                                                PortalAgent.CurrentPortal.CatalogId.ToString())
                                                            }, null).CategoryNodes;

                var categoriesEnabledProducts = model.Products.Where(x => x.IsActive == true && x.ProductCategories.Any(y => categoryNodes.Any(n => n.CategoryId == y.CategoryId && y.IsActive == true))).ToList();

                return new Collection<ProductViewModel>(ProductViewModelMap.ToViewModels(new Collection<ProductModel>(categoriesEnabledProducts)));
            }

            return null;
        }

        /// <summary>
        /// Returns Home Page specials for a store
        /// </summary>
        /// <returns>Collection of ProductViewModel</returns>
        public async Task<Collection<ProductViewModel>> GetHomeSpecialsAsync()
        {
            Expands = new ExpandCollection { ExpandKeys.Promotions };


            var model = await _client.GetProductsByHomeSpecialsAsync(Expands, null, new SortCollection(), 0, MvcDemoConstants.DefaultHomeSpecialsSize);

            if (model != null && model.Products != null && model.Products.Count > 0)
            {
                return new Collection<ProductViewModel>(ProductViewModelMap.ToViewModels(new Collection<ProductModel>(model.Products.Where(x => x.IsActive).ToList())));

            }

            return null;
        }

        /// <summary>
        /// Returns Bundles for list of prdocuts
        /// </summary>
        /// <returns>Collection of ProductViewModel</returns>
        public List<BundleDisplayViewModel> GetBundles(string bundleProductIds)
        {
            if (bundleProductIds != null)
            {
                var items = bundleProductIds.Split(',');
                var model = _searchAgent.GetProductsByIds(items.ToList());

                if (model != null && model.Count > 0)
                {
                    var activeBundleProducts = model.Where(x => x.IsActive);

                    if (model.Count == activeBundleProducts.Count())
                    {
                        return BundleViewModelMap.GetBundleDisplayViewModel(model);
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Get the extented price
        /// </summary>
        /// <param name="productId">Product ID</param>
        /// <param name="quantity">Quantity</param>
        /// <param name="sku">SKU</param>
        /// <param name="prodPrice"></param>
        /// <returns>Returns the extended price</returns>
        public decimal GetExtendedPrice(int productId, int quantity, string sku, decimal prodPrice)
        {
            var cartModel = new ShoppingCartModel();
            cartModel.ShoppingCartItems.Add(CartItemViewModelMap.ToShoppingCartModel(productId, quantity, sku));

            AccountViewModel accountViewModel = _accountAgent.GetAccountViewModel();
            if (!Equals(accountViewModel, null))
            {
                if (Convert.ToBoolean(accountViewModel.EnableCustomerPricing))
                {
                    cartModel.Account.ExternalId = accountViewModel.ExternalId;
                }
            }

            var shoppingCartModel = _shoppingCartsClient.Calculate(cartModel);

            // Check if item exists after API call.
            if (shoppingCartModel != null && shoppingCartModel.ShoppingCartItems.Any())
            {
                var item = shoppingCartModel.ShoppingCartItems.FirstOrDefault(x => x.ProductId == productId);
                if (item == null) return 0;
                return item.ExtendedPrice;
            }
            return quantity * prodPrice;
        }

        /// <summary>
        /// Check Inventory for the selected product sku and displays the error message based on Quantity on hand.
        /// </summary>
        /// <param name="model">Product Model</param>
        /// <param name="attributeId">Selected attribute id</param>
        public void CheckInventory(ProductViewModel model, string attributeId, int? quantity)
        {
            var selectedSku = _skuAgent.GetSkuInventory(model.ProductId, attributeId);

            if (Equals(selectedSku, null))
            {
                model.InventoryMessage = Resources.ZnodeResource.ErrorSelectAttribute;
                model.ShowAddToCart = false;
                return;
            }

            if (!Equals(selectedSku, null))
            {
                int selectedQuantity = quantity.HasValue && quantity.Value > 0 ? quantity.Value : model.MinQuantity;

                int cartQuantity = GetOrderedItemQuantity(selectedSku.Sku);

                int combinedQuantity = selectedQuantity + cartQuantity;

                if (selectedSku.Inventory.QuantityOnHand < combinedQuantity && (!model.AllowBackOrder.GetValueOrDefault()) && model.TrackInventory.GetValueOrDefault())
                {
                    model.InventoryMessage = !string.IsNullOrEmpty(model.OutOfStockMessage) ? model.OutOfStockMessage : string.Format(Resources.ZnodeResource.TextErrorFormat, Resources.ZnodeResource.TextOutofStock);
                    model.ShowAddToCart = false;
                    return;
                }
                else if (selectedSku.Inventory.QuantityOnHand < combinedQuantity && model.AllowBackOrder.GetValueOrDefault() && model.TrackInventory.GetValueOrDefault())
                {
                    model.InventoryMessage = !string.IsNullOrEmpty(model.BackOrderMessage) ? model.BackOrderMessage : Resources.ZnodeResource.TextBackOrderMessage;
                    model.ShowAddToCart = true;
                    return;
                }

                if (combinedQuantity > model.MaxQuantity)
                {
                    model.InventoryMessage = string.Format(Resources.ZnodeResource.TextErrorFormat,
                        Resources.ZnodeResource.TextMaxQuantityReached);
                    model.ShowAddToCart = false;
                    return;
                }
            }
            model.InventoryMessage = !string.IsNullOrEmpty(model.InStockMessage) ? model.InStockMessage : Resources.ZnodeResource.TextInstock;
            model.ShowAddToCart = true;
        }

        /// <summary>
        /// Check Inventory for the selected addon and displays the error message based on Quantity on hand.
        /// </summary>
        /// <param name="model">Product Model</param>
        /// <param name="selectedAddOnId">Selected addon id</param>
        /// <param name="quantity"></param>
        public void CheckAddOnInventory(ProductViewModel model, string selectedAddOnId, int? quantity, string selectedSku = null)
        {
            string[] selectedAddOn = !Equals(selectedAddOnId, null) ? selectedAddOnId.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries) : null;

            if (!Equals(selectedAddOn, null))
            {
                foreach (var addOn in model.AddOns)
                {
                    addOn.IsOutOfStock = false;
                    addOn.OutOfStockMessage = string.Empty;
                }
                foreach (var addonvalueId in selectedAddOn)
                {
                    AddOnViewModel addOn = null;
                    if (!string.IsNullOrEmpty(addonvalueId))
                    {
                        addOn =
                            model.AddOns.FirstOrDefault(
                                x => x.AddOnValues.Any(y => y.AddOnValueId == int.Parse(addonvalueId)));
                    }

                    if (addOn != null)
                    {
                        var addOnValue = addOn.AddOnValues.FirstOrDefault(y => y.AddOnValueId == int.Parse(addonvalueId));

                        if (addOnValue != null)
                        {
                            int selectedQuantity = quantity.HasValue && quantity.Value > 0 ? quantity.Value : model.MinQuantity;

                            int cartQuantity = GetOrderedItemQuantity(selectedSku, Convert.ToInt32(addonvalueId));

                            int combinedQuantity = selectedQuantity + cartQuantity;

                            if (combinedQuantity > model.MaxQuantity)
                            {
                                addOn.OutOfStockMessage = string.Format(Resources.ZnodeResource.TextErrorFormat,
                                    Resources.ZnodeResource.TextMaxQuantityReached);
                                addOn.IsOutOfStock = true;
                                model.InventoryMessage = string.Empty;
                                model.ShowAddToCart = false;
                                return;
                            }

                            if (addOnValue.QuantityOnHand < combinedQuantity && (!addOn.AllowBackOrder) && addOn.TrackInventory)
                            {
                                addOn.OutOfStockMessage = string.Format(Resources.ZnodeResource.TextAddOnOutofStock, addOn.Name);
                                addOn.IsOutOfStock = true;
                                model.InventoryMessage = string.Empty;
                                model.ShowAddToCart = false;
                                return;
                            }
                            else if (addOnValue.QuantityOnHand < combinedQuantity && addOn.AllowBackOrder && addOn.TrackInventory)
                            {
                                addOn.OutOfStockMessage = !string.IsNullOrEmpty(addOn.BackOrderMessage) ? addOn.BackOrderMessage : Resources.ZnodeResource.TextBackOrderMessage;
                                model.ShowAddToCart = true;
                                return;
                            }

                            if (model.MaxQuantity < combinedQuantity && model.AllowBackOrder.GetValueOrDefault() && model.TrackInventory.GetValueOrDefault())
                            {
                                addOn.OutOfStockMessage = string.Format(Resources.ZnodeResource.TextErrorFormat,
                                Resources.ZnodeResource.TextMaxQuantityReached);
                                model.InventoryMessage = string.Empty;
                                model.ShowAddToCart = false;
                                return;
                            }

                            if (!Equals(model.InventoryMessage, Resources.ZnodeResource.TextOutofStock) && !Equals(model.InventoryMessage, model.OutOfStockMessage) && !model.IsCallForPricing && combinedQuantity <= model.MaxQuantity)
                            {
                                addOn.OutOfStockMessage = !string.IsNullOrEmpty(addOn.InStockMessage) ? addOn.InStockMessage : Resources.ZnodeResource.TextInstock;
                                model.ShowAddToCart = true;
                            }
                        }
                    }
                }
            }

            if (string.IsNullOrEmpty(model.InventoryMessage) && model.ShowAddToCart)
            {
                model.InventoryMessage = !string.IsNullOrEmpty(model.AddOns.FirstOrDefault().InStockMessage) ? model.AddOns.FirstOrDefault().InStockMessage : Resources.ZnodeResource.TextInstock;
                model.ShowAddToCart = true;
            }
        }

        /// <summary>
        /// Get ordered items quantity by the given sku.
        /// </summary>
        /// <param name="sku"></param>
        /// <returns></returns>
        public int GetOrderedItemQuantity(string sku)
        {
            var cart = GetFromSession<ShoppingCartModel>(MvcDemoConstants.CartModelSessionKey);
            var cartQuantity = default(int);

            if (cart != null && cart.ShoppingCartItems.Any(model => model.Sku.Equals(sku)))
            {
                var shoppingCartItemModel = cart.ShoppingCartItems.FirstOrDefault(model => model.Sku.Equals(sku));
                if (shoppingCartItemModel != null)
                    cartQuantity = shoppingCartItemModel.Quantity;
            }
            return cartQuantity;
        }

        /// <summary>
        /// Gets SelectedSku, Product Price based on selected attributeId
        /// </summary>
        /// <param name="id">Product Id</param>
        /// <param name="attributeId">Attribute Id</param>
        /// <param name="selectedIds">Selected Attribute Ids</param>
        /// <param name="quantity">Quantity</param>
        /// <param name="prodPrice">Product Price</param>
        /// <param name="selectedSku">Selected Sku</param>
        /// <param name="imageMediumPath">Selected Sku imageMediumPath</param>
        /// <returns></returns>
        public ProductViewModel GetAttributes(int? id, int? attributeId, string selectedIds, int? quantity, out decimal prodPrice, out string selectedSku, out string imagePath, out string imageMediumPath)
        {
            var product = GetProduct(id.Value);

            // update product attributes based on selection.
            product.ProductAttributes = UpdateProductAttributes(product, attributeId, selectedIds);

            var updatedSelectedIds = product.ProductAttributes.Attributes.Select(x =>
                {
                    var attributeViewModel =
                        x.ProductAttributes.FirstOrDefault(y => (y.Available && y.Selected));

                    if (attributeViewModel == null)
                        attributeViewModel = x.ProductAttributes.FirstOrDefault(y => (y.Available));

                    return attributeViewModel != null ? attributeViewModel.AttributeId : 0;
                }).Where(x => x > 0);

            if (!(product.IsCallForPricing))
            {
                CheckInventory(product, string.Join(",", updatedSelectedIds), quantity.GetValueOrDefault(1));
            }
            //ZNode Version 7.2.2 - Add imageMediumPath Out parameter
            prodPrice = _skuAgent.GetSkusPrice(id.Value, attributeId.GetValueOrDefault(0), product.ProductPrice, out  selectedSku, out imagePath, out imageMediumPath);

            prodPrice = GetExtendedPrice(id.Value, quantity.GetValueOrDefault(1), selectedSku, prodPrice);

            //PRFT Custom Code:Start
            //Geting Price form ERP
            string associatedCustomerExternalId = HelperMethods.GetAssociatedCustomerExternalId();
            if (!string.IsNullOrEmpty(associatedCustomerExternalId))
            {
                string customerType = HelperMethods.GetERPCustomerType();
                PRFTERPItemDetailsModel prftERPItemDetailModel = new PRFTERPItemDetailsModel();
                prftERPItemDetailModel = _client.GetItemDetailsFromERP(product.ProductNumber, associatedCustomerExternalId, customerType);

                if (prftERPItemDetailModel != null && prftERPItemDetailModel.HasError == false)
                {
                    var unitPrice = prftERPItemDetailModel.UnitPrice;
                    prodPrice = unitPrice * quantity.GetValueOrDefault(1);
                    product.Price=product.ProductPrice = prodPrice; //Required by JS to update total price.

                    if (prftERPItemDetailModel.IsCallForPricing == true)
                    {
                        product.IsCallForPricing = true;
                        product.ShowAddToCart = false;
                    }
                }
            }


            //If product price/extended price is 0 then show call for price
            if (prodPrice == 0)
            {
                product.IsCallForPricing = true;
                product.ShowAddToCart = false;
            }

            //Inventory Handling
            //Get Inventory From ERP
            product.InventoryMessage = string.Empty;
            if (!(product.IsCallForPricing))
            {
                CheckInventoryAgainstERP(product, string.Join(",", updatedSelectedIds), quantity.GetValueOrDefault(1));
            }
            //PRFT Custom code: End

            //Set Product Main Image Path in case no image found.
            if (Path.GetFileName(imagePath).Equals(Path.GetFileName(PortalAgent.CurrentPortal.ImageNotAvailablePath)))
            {
                imagePath = product.ImageLargePath;
                imageMediumPath = product.ImageMediumPath;
            }
            return product;
        }

        /// <summary>
        /// Get the add on details
        /// </summary>
        /// <param name="product"></param>
        /// <param name="selectedAddOnIds"></param>
        /// <param name="quantity"></param>
        /// <param name="productPrice"></param>
        /// <param name="finalProductPrice"></param>
        /// <returns></returns>
        public IEnumerable<AddOnViewModel> GetAddOnDetails(ProductViewModel product, string selectedAddOnIds, int? quantity, decimal productPrice, out decimal finalProductPrice, string selectedSku = null)
        {
            // To DO Check the inventory for the addon
            CheckAddOnInventory(product, selectedAddOnIds, quantity.GetValueOrDefault(1), selectedSku);

            finalProductPrice = productPrice;
            string[] selectedAddOn = selectedAddOnIds.Split(',');

            foreach (var addonvalueId in selectedAddOn)
            {
                if (!string.IsNullOrEmpty(addonvalueId))
                {
                    var addOnValuesViewModel =
                        product.AddOns.SelectMany(
                            y => y.AddOnValues.Where(x => x.AddOnValueId == int.Parse(addonvalueId))).FirstOrDefault();
                    if (addOnValuesViewModel == null) continue;
                    var price = addOnValuesViewModel.AddOnFinalPrice;
                    finalProductPrice += price * quantity.GetValueOrDefault(1);
                }
            }

            return product.AddOns;
        }

        /// <summary>
        /// Gets ProductViewModel based on wishlist added Productid
        /// </summary>
        /// <param name="productId">ProductId added to wishlist </param>
        /// <returns>Returns ProductViewModel with wishlist success message</returns>
        public WishListViewModel CreateWishList(int productId)
        {
            var wishListViewModel = _accountAgent.CreateWishList(productId);
            return wishListViewModel;
        }

        /// <summary>
        /// Set the wishlist success and error message
        /// </summary>
        /// <param name="model">Product View Model</param>
        /// <param name="success">wishlist added or not</param>
        public void SetMessages(ProductViewModel model)
        {
            SetStatusMessage(model);
        }

        /// <summary>
        /// Creates a new review for the product
        /// </summary>
        /// <param name="model">ReviewItemViewModel</param>
        /// <returns>Returns the updated ProductViewModel</returns>
        public ReviewItemViewModel CreateReview(ReviewItemViewModel model)
        {
            var accountViewModel = _accountAgent.GetAccountViewModel();

            model.AccountId = accountViewModel != null ? (int?)accountViewModel.AccountId : null;

            _reviewsClient.CreateReview(AccountViewModelMap.ToReviewViewModel(model));


            model.SuccessMessage = Resources.ZnodeResource.SuccessWriteReview;
            return model;


            model.ErrorMessage = Resources.ZnodeResource.ErrorWriteReview;
            model.HasError = true;
            return model;
        }

        /// <summary>
        /// Get all the reviews based on ProductId
        /// </summary>
        /// <param name="productId">ProductId</param>
        /// <returns>Returns a ReviewViewModel along with the product information</returns>
        public ReviewViewModel GetAllReviews(int productId)
        {

            var list = _reviewsClient.GetReviews(new ExpandCollection() { ExpandKeys.Reviews },
                                                        new FilterCollection()
                                                            {
                                                                new FilterTuple(FilterKeys.ProductId, FilterOperators.Equals,
                                                                                productId.ToString())
                                                            },
                                                        new SortCollection());


            var reviewModel = list.Reviews.Where(x => x.Status == MvcDemoConstants.ApprovedReviewStatus).OrderByDescending(x => x.CreateDate);

            var reviewModelList = AccountViewModelMap.ToReviewViewModel(new Collection<ReviewModel>(reviewModel.ToList()));

            reviewModelList.Items.ToList()
                              .ForEach(
                                  x =>
                                  x.Product =
                                  ProductViewModelMap.ToViewModel(
                                      _client.GetProduct(x.ProductId.GetValueOrDefault())));


            return reviewModelList;
        }

        public string GetProductUrl(int id, UrlHelper urlHelper)
        {
            if (id > 0)
            {
                var productViewModel = GetProduct(id);

                if (!string.IsNullOrEmpty(productViewModel.SeoPageName))
                {
                    return urlHelper.RouteUrl("SeoSlug", new { slug = productViewModel.SeoPageName.ToLower() });
                }

                return urlHelper.RouteUrl("product-details", new { id });
            }

            return string.Empty;
        }

        #region Product Compare
        #region Public Methods
        public bool GlobalAddProductToCompareList(int productId, int categoryId, out string message, out int errorCode)
        {
            bool isCompare = false;
            message = string.Empty;
            errorCode = 0;
            List<CompareProductViewModel> compareProductList = new List<CompareProductViewModel>();
            List<CompareProductViewModel> products = GetFromSession<List<CompareProductViewModel>>(Helpers.MvcDemoConstants.CompareProducts);

            PortalViewModel portal = PortalAgent.CurrentPortal;

            if (Equals(products, null))
            {
                if (Equals(portal.CompareType, MvcDemoConstants.GlobalLevelCompare))
                {
                    categoryId = 0;
                    CompareProduct(productId, categoryId, compareProductList);
                    isCompare = true;
                }
                else
                {
                    CompareProduct(productId, categoryId, compareProductList);
                    isCompare = true;
                }
            }
            else if (!Equals(products, null) && products.Count < Helpers.MvcDemoConstants.CompareProductLimit)
            {
                bool isProductExists = false;
                bool isCategoryChanged = false;
                compareProductList = products;


                if (Equals(portal.CompareType, MvcDemoConstants.GlobalLevelCompare))
                {
                    foreach (CompareProductViewModel item in compareProductList)
                    {
                        categoryId = 0;
                        if (Equals(item.ProductId, productId))
                        {
                            isProductExists = true;
                            errorCode = Convert.ToInt32(Helpers.ProductCompareErrorCode.ProductExist);
                        }
                    }
                }
                else
                {
                    foreach (CompareProductViewModel item in compareProductList)
                    {
                        if (!Equals(item.CategoryId, categoryId))
                        {
                            isCategoryChanged = true;
                            errorCode = Convert.ToInt32(Helpers.ProductCompareErrorCode.CategoryChanged);
                            break;
                        }

                        if (Equals(item.ProductId, productId))
                        {
                            isProductExists = true;
                            errorCode = Convert.ToInt32(Helpers.ProductCompareErrorCode.ProductExist);
                        }
                    }
                }

                if (isCategoryChanged)
                {
                    var productList = GetFromSession<List<CompareProductViewModel>>(Helpers.MvcDemoConstants.CompareProducts);
                    productList = null;
                }
                else
                {
                    if (!isProductExists)
                    {
                        if (Equals(compareProductList, null))
                        {
                            compareProductList = products;
                        }
                        CompareProduct(productId, categoryId, compareProductList);
                        isCompare = true;
                    }
                }
            }
            else
            {
                errorCode = Convert.ToInt32(Helpers.ProductCompareErrorCode.MaxProductLimit);
            }
            message = GetErrorMessage(errorCode);
            return isCompare;
        }

        public bool CategoryAddProductToCompareList(int productId, int categoryId, out string message)
        {
            try
            {
                List<CompareProductViewModel> compareProductList = new List<CompareProductViewModel>();
                List<CompareProductViewModel> products = GetFromSession<List<CompareProductViewModel>>(Helpers.MvcDemoConstants.CompareProducts);
                if (Equals(products, null))
                {
                    CompareProduct(productId, categoryId, compareProductList);
                }
                else
                {
                    products = null;
                    CompareProduct(productId, categoryId, compareProductList);
                }
                message = ZnodeResource.ProductCompareSucessMessage;
                return true;
            }
            catch (ZnodeException)
            {
                message = ZnodeResource.CategoryProductCompareErrorMessage;
                return false;
            }

        }

        public bool RemoveProductFormSession(int productId)
        {
            bool isCompare = false;
            List<CompareProductViewModel> comparableProducts = GetFromSession<List<CompareProductViewModel>>(Helpers.MvcDemoConstants.CompareProducts);
            if (!Equals(comparableProducts, null) && !Equals(comparableProducts.Count, 0))
            {
                foreach (CompareProductViewModel item in comparableProducts)
                {
                    if (Equals(item.ProductId, productId))
                    {
                        comparableProducts.Remove(item);
                        isCompare = true;
                        break;
                    }
                }
            }
            return isCompare;
        }

        public Collection<ProductViewModel> GetCompareProducts()
        {
            Collection<ProductViewModel> comparableProducts = new Collection<ProductViewModel>();
            List<CompareProductViewModel> products = GetFromSession<List<CompareProductViewModel>>(Helpers.MvcDemoConstants.CompareProducts);
            if (!Equals(products, null))
            {
                foreach (CompareProductViewModel item in products)
                {
                    ProductViewModel product = ProductViewModelMap.ToViewModel(_client.GetProduct(item.ProductId, new ExpandCollection
	                                                                                                      {
	                                                                                                          ExpandKeys.AddOns,
	                                                                                                          ExpandKeys.BundleItems,
	                                                                                                          ExpandKeys.Attributes,
	                                                                                                          ExpandKeys.Skus,
                                                                                                              ExpandKeys.Manufacturer
	                                                                                                      }));
                    comparableProducts.Add(product);
                }
            }
            return comparableProducts;
        }

        public Collection<ProductViewModel> GetCompareProductsDetails(CompareProductViewModel compareProducts)
        {
            List<CompareProductViewModel> comparableProducts = GetFromSession<List<CompareProductViewModel>>(Helpers.MvcDemoConstants.CompareProducts);
            if (!Equals(comparableProducts, null))
            {
                int productId = 0;
                foreach (CompareProductViewModel compareProduct in comparableProducts)
                {
                    productId = compareProduct.ProductId;
                    ProductViewModel product = GetProduct(productId);
                    compareProducts.ProductList.Add(product);
                }
            }
            return !Equals(compareProducts.ProductList, null) ? compareProducts.ProductList : new Collection<ProductViewModel>();
        }

        public bool DeleteComparableProducts()
        {
            bool isDeleted = false;
            List<CompareProductViewModel> comparableProducts = GetFromSession<List<CompareProductViewModel>>(Helpers.MvcDemoConstants.CompareProducts);
            if (!Equals(comparableProducts, null) && !Equals(comparableProducts.Count, 0))
            {
                comparableProducts = null;
                SaveInSession(Helpers.MvcDemoConstants.CompareProducts, comparableProducts);
                isDeleted = true;
            }
            return isDeleted;
        }

        public bool SendComparedProductMail(CompareProductViewModel compareProduct)
        {
            List<CompareProductViewModel> products = GetFromSession<List<CompareProductViewModel>>(Helpers.MvcDemoConstants.CompareProducts);
            compareProduct.ProductIds = string.Join(",", products.Select(x => x.ProductId));
            compareProduct.PortalId = PortalAgent.CurrentPortal.PortalId;
            compareProduct.BaseUrl = (!string.IsNullOrEmpty(HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority))) ? HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) : string.Empty;
            return _client.SendMailToFriend(ProductViewModelMap.ToModel(compareProduct));
        }

        #endregion

        #region Private Methods
        /// <summary>
        /// This method returns message according to error code
        /// </summary>
        /// <param name="errorCode">int errorCode</param>
        /// <returns>string message</returns>
        private string GetErrorMessage(int errorCode)
        {
            string message = string.Empty;
            if (errorCode == Convert.ToInt32(Helpers.ProductCompareErrorCode.CategoryChanged))
            {
                message = ZnodeResource.ProductCategoryChangeErrorMessage;
            }
            else if (errorCode == Convert.ToInt32(Helpers.ProductCompareErrorCode.ProductExist))
            {
                message = ZnodeResource.ProductCompareProductExistErrorMessage;
            }
            else if (errorCode == Convert.ToInt32(Helpers.ProductCompareErrorCode.MaxProductLimit))
            {
                message = ZnodeResource.ProductCompareLimitReachedErrorMessage;
            }
            else
            {
                message = ZnodeResource.ProductCompareSucessMessage;
            }
            return message;
        }

        private void CompareProduct(int productId, int categoryId, List<CompareProductViewModel> compareProducts)
        {
            CompareProductViewModel compareProduct = new CompareProductViewModel();
            compareProduct.CategoryId = categoryId;
            compareProduct.ProductId = productId;
            compareProducts.Add(compareProduct);
            SaveInSession(Helpers.MvcDemoConstants.CompareProducts, compareProducts);
        }
        #endregion

        #endregion

        public List<SkuProductViewModel> GetSkuProductListBySKU(string sku)
        {
            FilterCollection filters = new FilterCollection();
            filters.Add(new FilterTuple(FilterKeys.Sku, FilterOperators.Contains, sku));
            filters.Add(new FilterTuple(FilterKeys.PortalId, FilterOperators.Contains, PortalAgent.CurrentPortal.PortalId.ToString()));

            SkuProductListModel list = _client.GetSkuProductListBySku(filters);

            return !Equals(list, null) && !Equals(list.SkuProductList, null) && list.SkuProductList.Count > 0 ? SkuProductViewModelMap.ToViewModels(list) : new List<SkuProductViewModel>();
        }

        public string AddMultipleProductsToCart(List<CartItemViewModel> cartItems)
        {
            string errorMessage = string.Empty;
            try
            {
                if (!Equals(cartItems, null))
                {
                    ICartAgent _cartAgent = new CartAgent();
                    foreach (var cartItem in cartItems)
                    {
                        if (!Equals(cartItem, null))
                        {
                            if (!string.IsNullOrEmpty(cartItem.BundleItemsIds))
                            {
                                BundleDetailsViewModel productsViewModel = GetBundleProductsDetails(cartItem);
                                if (!Equals(productsViewModel, null))
                                {
                                    if (!Equals(productsViewModel.Bundles, null))
                                    {
                                        productsViewModel.Bundles.ForEach(adon => adon.AddOns = null);
                                    }
                                    CartItemViewModel item = CartItemViewModelMap.ToViewModel(productsViewModel);
                                    _cartAgent.Create(item);
                                }
                            }

                            ProductViewModel product = _cartAgent.WishlistCheckInventory(cartItem);

                            if (!Equals(product, null) && !string.IsNullOrEmpty(product.InventoryMessage) && !product.ShowAddToCart)
                            {
                                errorMessage = product.InventoryMessage;
                            }

                            if (string.IsNullOrEmpty(cartItem.BundleItemsIds))
                            {
                                _cartAgent.Create(cartItem);
                            }
                        }
                    }
                }
                return errorMessage;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public HighlightListViewModel GetProductHighlights(int productId, FilterCollection filters)
        {
            if (Equals(filters, null))
            {
                filters = new FilterCollection();
            }
            _client.RefreshCache = true;
            filters.Add(new FilterTuple(FilterKeys.ProductId, FilterOperators.Equals, productId.ToString()));
            HighlightListModel model = _client.GetProductHighlights(productId, null, filters, null, null, null);
            return HighlightViewModelMap.ToListViewModels(model);
        }

        public int GetOrderedItemQuantity(string selectedSku, int addonValueId)
        {
            var cart = GetFromSession<ShoppingCartModel>(MvcDemoConstants.CartModelSessionKey);
            var cartQuantity = default(int);

            if (!Equals(cart, null) && !Equals(cart.ShoppingCartItems, null))
            {
                return (
                    from ShoppingCartItemModel item in cart.ShoppingCartItems
                    where !Equals(item, null) && !Equals(item.AddOnValueIds, null)
                    where item.Sku == selectedSku
                    from int itemAddOn in item.AddOnValueIds
                    where itemAddOn == addonValueId
                    select item.Quantity
                    ).Sum();
            }
            return cartQuantity;
        }

        public bool SendMailToFriend(CompareProductViewModel product)
        {
            product.IsProductDetails = true;
            product.PortalId = PortalAgent.CurrentPortal.PortalId;
            product.BaseUrl = (!string.IsNullOrEmpty(HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority))) ? HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) : string.Empty;
            return _client.SendMailToFriend(ProductViewModelMap.ToModel(product));
        }

        /// <summary>
        /// To get all active categories assigned to current profile
        /// </summary>
        /// <returns>returns active CategoryModel list</returns>
        private List<CategoryModel> GetProfileCategories()
        {
            var accountModel = _accountAgent.GetAccountViewModel();
            int? profileId = accountModel != null ? accountModel.ProfileId : PortalAgent.CurrentPortal.DefaultAnonymousProfileId;

            var list = _categoriesClient.GetCategoriesByCatalog(PortalAgent.CurrentPortal.CatalogId, new ExpandCollection() { ExpandKeys.CategoryNodes, ExpandKeys.CategoryProfiles }, new FilterCollection()
                        {
                            new FilterTuple(FilterKeys.IsVisible, FilterOperators.Equals,"true"),

                        }, null);


            if (list.Categories != null)
            {
                var tcategories =
                    list.Categories.Where(
                        p =>
                        p.CategoryProfiles.Any(y => y.ProfileID == profileId && y.EffectiveDate > System.DateTime.Now))
                              .ToList();

                return list.Categories.Except(tcategories).ToList();
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// To Check if it is this store product in category , subcategory & child category.
        /// </summary>      
        /// <param name="product">product assigned categories</param>
        /// <returns>returns true/false</returns>
        private bool IsStoreProduct(ProductModel product)
        {
            try
            {
                var categories = this.GetProfileCategories();
                bool isStoreProduct = product.Categories.Any(x => categories.Any(y => x.CategoryId == y.CategoryId));
                return isStoreProduct;
            }
            catch
            {
                return true;
            }
        }

        /// <summary>
        /// Sets the addon price for the addons
        /// </summary>
        /// <param name="productViewModel"></param>
        private void SetAddOnPrice(ProductViewModel productViewModel)
        {
            productViewModel.AddOns.ToList().ForEach(x => x.AddOnValues.ToList().ForEach(y => y.AddOnFinalPrice = GetAddOnPrice(y)));
        }

        /// <summary>
        /// Gets the addon prices for an addonvalue
        /// </summary>
        /// <param name="addOnValuesViewModel"></param>
        /// <returns></returns>
        private decimal GetAddOnPrice(AddOnValuesViewModel addOnValuesViewModel)
        {
            decimal finalPrice = 0;
            finalPrice = addOnValuesViewModel.RetailPrice;

            if (addOnValuesViewModel.SalePrice.HasValue)
            {
                finalPrice = addOnValuesViewModel.SalePrice.GetValueOrDefault(0);
            }

            if (System.Web.HttpContext.Current.User.Identity.IsAuthenticated && addOnValuesViewModel.WholeSalePrice.HasValue)
            {
                var accountViewModel = _accountAgent.GetAccountViewModel();

                if (accountViewModel != null && accountViewModel.UseWholeSalePricing)
                {
                    finalPrice = addOnValuesViewModel.WholeSalePrice.GetValueOrDefault(0);
                }
            }

            return finalPrice;
        }

        public BundleDetailsViewModel GetCartModel(ProductViewModel model)
        {
            BundleDetailsViewModel bundle = new BundleDetailsViewModel();
            if (!Equals(model, null))
            {
                CartItemViewModel cartItem = new CartItemViewModel();
                cartItem.BundleItemsIds = model.BundleItemsIds;
                cartItem.ProductId = model.ProductId;
                cartItem.Quantity = 1;
                cartItem.AddOnValueIds = !Equals(model.AddOns, null) ? string.Join(",", model.AddOns.Select(x => x.AddOnValues.FirstOrDefault().AddOnValueId)) : string.Empty;
                cartItem.Sku = model.Sku;
                bundle = GetBundleProductsDetails(cartItem);
            }
            return bundle;
        }
    }
}