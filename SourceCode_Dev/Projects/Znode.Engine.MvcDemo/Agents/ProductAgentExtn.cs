﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.Helpers;
using Znode.Engine.MvcDemo.Maps;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Agents
{
    public partial class ProductAgent
    {
        /// <summary>
        /// Get page sizes options for binding drop down list.
        /// </summary>        
        public IEnumerable<SelectListItem> GetPageSizesItems()
        {
            var selectList = new List<SelectListItem>();
            string pagingText = "12 | 12 & 16 | 16 & 20 | 20 & All | -1";
            string[] pagingItems = pagingText.Split(new char[] { '&' });
            foreach (string pagingItem in pagingItems)
            {
                if (pagingItem.Contains("|") && pagingItem.Trim().Length > 0)
                {
                    SelectListItem listItem = new SelectListItem();
                    string[] itemText = pagingItem.Split(new char[] { '|' });
                    listItem.Text = itemText[0].Trim();
                    listItem.Value = itemText[1].Trim();
                    selectList.Add(listItem);
                }
            }
            return selectList;
        }

        /// <summary>
        /// Gets the Skus for specific product Id
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="filters"></param>
        /// <param name="sortCollection"></param>
        /// <param name="pageIndex"></param>
        /// <param name="recordPerPage"></param>
        /// <returns></returns>
        public PRFTSkuListViewModel GetProductSkuByProductId(int productId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            PRFTSkuListViewModel model = new PRFTSkuListViewModel();
            //ReplaceSortKeyName(sortCollection, MvcAdminConstants.Id, MvcAdminConstants.SkuId);
            filters = new FilterCollection();
            filters.Add(FilterKeys.ProductId, FilterOperators.Equals, productId.ToString());
            _client.RefreshCache = true;
            //var list = _client.GetProductSkuDetails(null, filters, sortCollection, pageIndex, recordPerPage);
            var list = _client.GetProductSkuDetails(null, filters, null, null, null);
            //ReplaceSortKeyName(sortCollection, MvcAdminConstants.SkuId, MvcAdminConstants.Id);
            if (!Equals(list, null))
            {
                model = PRFTSkuViewModelMap.ToListModel(list);
                return model;
            }
            return null;
        }

        //PRFT Custom Code:Start
        public PRFTERPItemDetailsModel GetItemDetailsFromERP(string productNum, string associatedCustomerExternalId,string customerType)
        {
            PRFTERPItemDetailsModel itemDetails = new PRFTERPItemDetailsModel();
            itemDetails = _client.GetItemDetailsFromERP(productNum, associatedCustomerExternalId, customerType);
            return itemDetails;
        }

        public int GetInventoryFromERP(string productNum)
        {
            _client.RefreshCache = true;
            PRFTInventoryModel inventoryDetails = _client.GetInventoryDetailsFromERP(productNum);
            int availableInventory = 0;
            if(inventoryDetails!=null)
            {
                availableInventory = inventoryDetails.Milwaukee + inventoryDetails.Bloomington + inventoryDetails.Nebraska+inventoryDetails.DesMoines;
            }

            return availableInventory;
        }

        /// <summary>
        /// Check Inventory for the selected product sku and displays the error message based on Quantity on hand.
        /// </summary>
        /// <param name="model">Product Model</param>

        public void CheckInventoryAgainstERP(ProductViewModel model, string attributeId, int? quantity)
        {
            model.MaxQuantity = GetInventoryFromERP(model.ProductNumber); //Get Total Qty from ERP;
            model.InventoryMessage = string.Empty;
            var isB2BCustomer = new PRFTHelper().IsB2BUser();

            if (model.MaxQuantity <= 0 && !isB2BCustomer)
            {
                model.MaxQuantity = 0;
                model.ShowAddToCart = false;
                return;
            }
            else {

                var selectedSku = _skuAgent.GetSkuInventory(model.ProductId, attributeId);
                if (!Equals(selectedSku, null))
                {

                    model.BackOrderMessage = string.Empty;
                    model.AllowBackOrder = false;
                    
                    int selectedQuantity = quantity.HasValue && quantity.Value > 0 ? quantity.Value : model.MinQuantity;
                    int cartQuantity = GetOrderedItemQuantity(selectedSku.Sku);

                    int combinedQuantity = selectedQuantity + cartQuantity;

                    if (model.MaxQuantity < combinedQuantity)
                    {
                        if (isB2BCustomer)
                        {
                            model.BackOrderMessage = string.Format(Resources.ZnodeResource.BackOrderMessage, combinedQuantity - model.MaxQuantity);
                            model.AllowBackOrder = true;                     
                            model.ShowAddToCart = true;
                        }
                        else
                        {
                            model.InventoryMessage = string.Format(Resources.ZnodeResource.TextErrorFormat, Resources.ZnodeResource.TextMaxQuantityReached);
                            model.ShowAddToCart = false;
                        }
                        return;
                    }
                }
            }
        }
        //PRFT Custom Code:End
    }
}