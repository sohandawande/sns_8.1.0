﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Agents
{
    public interface IPRFTERPCustomerAgent
    {
        PRFTERPAccountDetailsViewModel GetAccountDetailsFromERP(string externalId);
    }
}
