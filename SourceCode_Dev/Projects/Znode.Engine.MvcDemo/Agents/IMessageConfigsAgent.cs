﻿using System.Threading.Tasks;

namespace Znode.Engine.MvcDemo.Agents
{
    public interface IMessageConfigsAgent
    {
        string GetByKey(string key);
        Task<string> GetByKeyAsync(string key);
    }
}