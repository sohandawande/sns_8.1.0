﻿using Znode.Engine.Api.Client;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.Maps;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Agents
{
    /// <summary>
    /// Case Request Agent
    /// </summary>
    public partial class CaseRequestAgent : BaseAgent, ICaseRequestAgent
    {
        #region Private Readonly Variables
        private readonly ICaseRequestsClient _caseRequestClient;
        #endregion

        #region Constructors
        public CaseRequestAgent()
        {
            _caseRequestClient = GetClient<CaseRequestsClient>();
            //PRFT Custom Code : Start
            _countriesClient = new CountriesClient();
            _statesClient = new StatesClient();
            _accountClient = new AccountsClient();
            //PRFT Custom Code : End
        }
        #endregion

        #region Public Methods
        public bool SaveContactUsInfo(CaseRequestViewModel model)
        {
            model.CreateUser = System.Web.HttpContext.Current.User.Identity.Name;
            if(!string.IsNullOrEmpty(model.CreateUser))
            {
              AccountAgent _accountAgent = new AccountAgent();
              var accountViewModel = _accountAgent.GetAccountViewModel();
              if (!Equals(accountViewModel, null))
              {
                  model.AccountId = accountViewModel.AccountId;
              }
            }

            //PRFT Custom Code:Start
            if (model.IsRequestAQuoteForm)
            {
                model.Message = string.Format(Resources.ZnodeResource.MessageRequestAQuote, model.SaleRepNameWithLocation, model.RequestAQuote.DueDate.ToString("dd/MM/yyyy"), model.RequestAQuote.ContactChoiceSelected, model.RequestAQuote.FaxNumber, model.Message);
            }
            else
            {
                if (model.ContactUsExtn != null)
                {
                    model.Message = string.Format(Resources.ZnodeResource.MessageContactUs,model.SaleRepNameWithLocation,model.ContactUsExtn.City,model.ContactUsExtn.StateCode,model.ContactUsExtn.CountryCode, model.Message);
                }
            }
            //PRFT Custom Code:End
            CaseRequestModel caseRequestModel = _caseRequestClient.SaveContactUsCusotmerFeedbackInfo(CaseRequestViewModelMap.ToModel(model));
            return Equals(caseRequestModel, null) ? false : true;
        }

        public bool SaveCustomerFeedbackInfo(CaseRequestViewModel model)
        {
            model.CreateUser = System.Web.HttpContext.Current.User.Identity.Name;
            if (!string.IsNullOrEmpty(model.CreateUser))
            {
                AccountAgent _accountAgent = new AccountAgent();
                var accountViewModel = _accountAgent.GetAccountViewModel();
                if (!Equals(accountViewModel, null))
                {
                    model.AccountId = accountViewModel.AccountId;
                }
            }
            if (model.AllowSharingWithCustomer)
            {
                model.Message = string.Format(Resources.ZnodeResource.MessageCustomerFeedbackTrue, model.Message, model.CustomerCity, model.CustomerState);
            }
            else
            {
                model.Message = string.Format(Resources.ZnodeResource.MessageCustomerFeedbackFalse, model.Message, model.CustomerCity, model.CustomerState);
            }
            CaseRequestModel caseRequestModel = _caseRequestClient.SaveContactUsCusotmerFeedbackInfo(CaseRequestViewModelMap.ToModel(model));
            return Equals(caseRequestModel, null) ? false : true;
        }
        #endregion
    }
}