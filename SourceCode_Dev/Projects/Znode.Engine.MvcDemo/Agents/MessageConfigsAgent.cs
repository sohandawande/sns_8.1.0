﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;
using System.Web.Caching;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.Helpers;

namespace Znode.Engine.MvcDemo.Agents
{
    public class MessageConfigsAgent : BaseAgent, IMessageConfigsAgent
    {
        private readonly IMessageConfigsClient _messageConfigsClient;

        public MessageConfigsAgent()
        {
            _messageConfigsClient = new MessageConfigsClient();
        }

        public string GetByKey(string key)
        {
            try
            {
                var model = this.GetMessages();
                if (!Equals(model, null))
                {
                    return model.Find(x => x.Key.Equals(key)).Value;
                }
            }
            catch (Exception)
            {


            }
            return string.Empty;
        }

        public async Task<string> GetByKeyAsync(string key)
        {
            try
            {
                var model = await _messageConfigsClient.GetMessageConfigByKeyAsync(key, null);
                if (!String.IsNullOrEmpty(model.Value))
                {
                    return model.Value;
                }
            }
            catch (Exception)
            {


            }
            return string.Empty;
        }

        private List<MessageConfigModel> GetMessages()
        {
            const string cacheKey = "MessageKey";
            if (Equals(HttpContext.Current.Cache[cacheKey], null))
            {
                FilterCollection filters = new FilterCollection();
                filters.Add(new FilterTuple(FilterKeys.PortalId, FilterOperators.Equals, PortalAgent.CurrentPortal.PortalId.ToString()));
                var model = _messageConfigsClient.GetMessageConfigs(null, filters, null);

                if (!Equals(model, null))
                {
                    List<MessageConfigModel> _messageList = new List<MessageConfigModel>();
                    _messageList.AddRange(model.MessageConfigs);

                    CacheDurationSettings cacheDurationSetting = CacheDurationSettings.GetCacheDurationSettingsByMappingKey("ManageMessageCacheDuration");
                    HttpContext.Current.Cache.Insert(cacheKey, _messageList, null, DateTime.Now.AddMinutes(Equals(cacheDurationSetting, null) ? 0 : Convert.ToDouble(cacheDurationSetting.Value)),
                                                     Cache.NoSlidingExpiration);
                }
            }
            return HttpContext.Current.Cache[cacheKey] as List<MessageConfigModel>;
        }
    }
}