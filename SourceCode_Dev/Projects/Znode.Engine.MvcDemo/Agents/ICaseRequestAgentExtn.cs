﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;

namespace Znode.Engine.MvcDemo.Agents
{
    public partial interface ICaseRequestAgent
    {
        Collection<StateModel> GetStates();

        Collection<CountryModel> GetCountries(int billingShippingFlag = 0);

        Collection<PRFTSalesRepresentative> GetSalesRep();        
    }
}