﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcDemo.Agents
{
    public partial class CartAgent
    {
        /// <summary>
        /// GetCartCount method to get the current cart total for current account.
        /// </summary>
        /// <returns>Returns the cart total as decimal</returns>
        public decimal GetCartTotal()
        {
            var cart = GetCart();

            if (!cart.HasError && cart.Total != null) return Convert.ToDecimal(cart.Total.Value.ToString());

            return Convert.ToDecimal(0.0);
        }
    }
}