﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.MvcDemo.Agents
{
    public partial interface ICartAgent
    {
        decimal GetCartTotal();
    }
}
