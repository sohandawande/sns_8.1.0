﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Agents
{
    public partial interface IAccountAgent
    {
        AccountListModel GetSubUserAccounts(int parentAccountId, PRFTSubUsersRequestModel subUsersRequestModel);

        PRFTRegisterSubUserViewModel RegisterSubUser(PRFTRegisterSubUserViewModel model);

        AccountViewModel GetAccountByAccountId(int accountId);

        AccountViewModel UpdateUserAccount(AccountViewModel model);

        OrdersViewModel GetSubUserOrderReceiptDetails(int orderId, int accountId);

        bool SaveAssociateCustomer(int accountId, string customerAccountIds);

    }
}
