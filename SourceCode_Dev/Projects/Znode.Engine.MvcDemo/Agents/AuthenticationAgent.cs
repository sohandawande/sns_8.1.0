﻿using System;
using System.Web;
using System.Web.Security;

namespace Znode.Engine.MvcDemo.Agents
{
    public class AuthenticationAgent : IAuthenticationAgent
    {


        public void SetAuthCookie(string userName, bool createPersistantCookie)
        {
            TimeSpan currentTimeout = FormsAuthentication.Timeout;
            System.Web.Security.FormsAuthenticationTicket ticket = new System.Web.Security.FormsAuthenticationTicket(
                         1,                                     // ticket version
                         userName,                              // authenticated username
                         DateTime.Now,                          // issueDate
                         DateTime.Now.AddMinutes(currentTimeout.Minutes),           // expiryDate
                         true,                                  // true to persist across browser sessions
                         string.Empty,                          // can be used to store additional user data
                         FormsAuthentication.FormsCookiePath);  // the path for the cookie

            // Encrypt the ticket using the machine key
            string encryptedTicket = FormsAuthentication.Encrypt(ticket);

            // Add the cookie to the request to save it
            HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
            cookie.HttpOnly = true;
            HttpContext.Current.Response.Cookies.Add(cookie);
        }

        public void RedirectFromLoginPage(string userName, bool createPersistantCookie)
        {
            FormsAuthentication.RedirectFromLoginPage(userName, createPersistantCookie);
        }
    }
}