﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.Helpers;
using Znode.Engine.MvcDemo.Maps;
using Znode.Engine.MvcDemo.ViewModels;
namespace Znode.Engine.MvcDemo.Agents
{
    public partial class CaseRequestAgent : BaseAgent, ICaseRequestAgent
    {


        #region Private Readonly Variables
        private readonly string salesRepRoleName = ConfigurationManager.AppSettings["SalesRepRoleName"];
        private readonly IStatesClient _statesClient;
        private readonly ICountriesClient _countriesClient;
        private readonly IAccountsClient _accountClient;
        #endregion

        #region Public Methods     

        /// <summary>
        /// Get the States depending on the country code 
        /// </summary>
        /// <param name="addressId">Address ID</param>
        /// <returns>Returns the address as Address view model</returns>
        public Collection<StateModel> GetStates()
        {
            var stateListModel = _statesClient.GetStates(null, null);
            if (stateListModel != null && stateListModel.States != null)
            {
                return stateListModel.States;
            }
            return null;
        }

        /// <summary>
        /// This function returns list of all active billing and shipping countries on the basis of portal id.
        /// </summary>
        /// <param name="billingShippingFlag">int billingShippingFlag</param>
        /// <returns>Returns list of all active billing and shipping countries on the basis of portal id.</returns>
        public Collection<CountryModel> GetCountries(int billingShippingFlag = 0)
        {
            _countriesClient.RefreshCache = true;
            var countryListModel = _countriesClient.GetActiveCountryByPortalId(PortalAgent.CurrentPortal.PortalId, billingShippingFlag);

            if (countryListModel != null && countryListModel.Countries != null)
            {
                CountryModel model = new CountryModel();
                model.Code = String.Empty;
                model.Name = Resources.ZnodeResource.PleaseSelectText;
                countryListModel.Countries.Insert(0, model);

                return countryListModel.Countries;
            }
            return null;
        }

        public Collection<PRFTSalesRepresentative> GetSalesRep()
        {
            var list = _accountClient.GetSalesRepInfoByRoleName(salesRepRoleName);
            return list;
        }

        #endregion
    }
}