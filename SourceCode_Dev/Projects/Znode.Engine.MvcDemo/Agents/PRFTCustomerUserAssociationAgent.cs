﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.MvcDemo.Maps;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Agents
{
    public class PRFTCustomerUserAssociationAgent : BaseAgent, IPRFTCustomerUserAssociationAgent
    {
        #region Private Variables
        private readonly ICustomerClient _customerClient;
        #endregion

        public PRFTCustomerUserAssociationAgent()
        {
            _customerClient = GetClient<CustomerClient>();
        }
            
        public PRFTCustomerListViewModel GetAssociatedCustomerList(int accountId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            filters = new FilterCollection();

            filters.Add(new FilterTuple(FilterKeys.AccountId, FilterOperators.Equals, accountId.ToString()));
            var customerList = _customerClient.GetAssociatedCustomer(accountId, null, filters, sortCollection, pageIndex, recordPerPage);
            if (!Equals(customerList, null))
            {
                return AccountViewModelMap.ToListViewModel(customerList.CustomerList, customerList.TotalResults, customerList.PageSize, customerList.PageIndex, customerList.TotalPages);
            }
            return new PRFTCustomerListViewModel();

        }
    }
}