﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Client;
using Znode.Engine.Exceptions;
using Znode.Engine.MvcDemo.Maps;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Agents
{
    public class PRFTERPOrderAgent : BaseAgent, IPRFTERPOrderAgent
    {
        private readonly IPRFTERPOrdersClient _erpOrdersClient;
        public PRFTERPOrderAgent()
        {
            _erpOrdersClient = new PRFTERPOrdersClient();
        }
        public PRFTOEHeaderHistoryViewModel GetInvoiceDetailsFromERP(string externalId)
        {
            try
            {
                var erpAccountDetails = _erpOrdersClient.GetInvoiceDetailsFromERP(externalId);
                var viewModel = PRFTERPOrderViewModelMap.ToERPOEHeaderViewModel(erpAccountDetails);
                //PRFTOEHeaderHistoryViewModel viewModel = new PRFTOEHeaderHistoryViewModel();
                return viewModel;
            }
            catch (ZnodeException)
            {
                return new PRFTOEHeaderHistoryViewModel() { HasError = true };
            }
        }
    }
}