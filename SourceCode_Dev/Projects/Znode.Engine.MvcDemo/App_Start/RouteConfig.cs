﻿using System.Web.Mvc;
using System.Web.Routing;
using Znode.Engine.MvcDemo.Helpers;

namespace Znode.Engine.MvcDemo
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute("category-details", "category/{category}/{categoryId}", new { controller = "category", action = "Index" }, new { httpMethod = new HttpMethodConstraint("GET"), category = @"[\w- ]+$", categoryId = @"[\w- ]+$" });

            routes.MapRoute("seocategory-details", "category/{seo}/{category}", new { controller = "category", action = "Index" }, new { httpMethod = new HttpMethodConstraint("GET"), seo = @"[\w-]+$", category = @"[\w- ]+$" });

            //PRFT Custom Code: Start
            routes.MapRoute("Account-useradministration", "account/useradministration/{parentAccountId}", new { controller = "account", action = "UserAdministration" }, new { httpMethod = new HttpMethodConstraint("GET"), parentAccountId = @"[\w- ]+$" });
            routes.MapRoute("Account-subuserorderreceipt", "account/SubUserOrderReceipt/{orderId}", new { controller = "account", action = "SubUserOrderReceipt" }, new { httpMethod = new HttpMethodConstraint("GET"), orderId = @"[0-9]+$" });

            routes.MapRoute("Account-Invoice", "account/invoice/{externalId}", new { controller = "account", action = "invoice" }, new { httpMethod = new HttpMethodConstraint("GET"), externalId = @"[\w- ]+$" });
            // Credit Application
            routes.MapRoute("credit-application", "creditapplication", new { controller = "PRFTCreditApplication", action = "index" }, new { httpMethod = new HttpMethodConstraint("GET") });

            //PRFT Custom Code: End

            routes.MapRoute("AccountLoginRedirect", "login.aspx", new { controller = "account", action = "redirect" }, new { httpMethod = new HttpMethodConstraint("GET") });

            routes.MapRoute("product-details", "product/{id}", new { controller = "product", action = "Details" }, new { httpMethod = new HttpMethodConstraint("GET"), id = @"[0-9]+$" });
            routes.MapRoute("product-reviews-", "product/Allreviews/{id}", new { controller = "product", action = "Allreviews" }, new { httpMethod = new HttpMethodConstraint("GET"), id = @"[0-9]+$" });
            routes.MapRoute("product-writereviews-", "product/Writereview/{id}", new { controller = "product", action = "Writereview" }, new { httpMethod = new HttpMethodConstraint("GET"), id = @"[0-9]+$" });
            routes.MapRoute("product-writereviews1-", "product/Writereview/{id}", new { controller = "product", action = "Writereview" }, new { httpMethod = new HttpMethodConstraint("POST"), id = @"[0-9]+$" });

            routes.MapRoute("product-attribute-details", "product/GetAttributes/{id}", new { controller = "product", action = "GetAttributes" }, new { httpMethod = new HttpMethodConstraint("GET"), id = @"[0-9]+$" });

            routes.MapRoute("product-wishlist-details", "product/WishList/{id}", new { controller = "product", action = "WishList" }, new { httpMethod = new HttpMethodConstraint("GET"), id = @"[0-9]+$" });
            routes.MapRoute("product-wishlist-details1", "product/WishList/{id}", new { controller = "product", action = "WishList" }, new { httpMethod = new HttpMethodConstraint("POST"), id = @"[0-9]+$" });

            routes.MapRoute("seoproduct-details", "product/{seo}/{id}", new { controller = "product", action = "Details" }, new { httpMethod = new HttpMethodConstraint("GET"), seo = @"[\w-]+$", id = @"[0-9]+$" });

            routes.MapRoute("content-page-details", "contentpage/{contentPageName}", new { controller = "Home", action = "contentpage" }, new { httpMethod = new HttpMethodConstraint("GET"), contentPageName = @"[\w-]+$"});

            routes.MapSEORoute(
                  name: "SeoSlug",
                  url: "{slug}",
                  defaults: new { controller = "", action = "", slug = "", ElementId = "" });

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

        }
    }
}