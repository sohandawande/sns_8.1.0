﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Web.WebPages.OAuth;
using DotNetOpenAuth.GoogleOAuth2;
using System.Configuration;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo
{
    public static class AuthConfig
    {
        public static void RegisterAuth(RegisteredSocialClientListViewModel model)
        {
            // To let users of this site log in using their accounts from other sites such as Microsoft, Facebook, and Twitter,
            // you must update this site. For more information visit http://go.microsoft.com/fwlink/?LinkID=252166

            //OAuthWebSecurity.RegisterMicrosoftClient(
            //    clientId: "",
            //    clientSecret: "");

            //OAuthWebSecurity.RegisterTwitterClient(
            //    consumerKey: "",
            //    consumerSecret: "");

            if (!Equals(model, null) && !Equals(model.SocialClients, null) && model.SocialClients.Count > 0)
            {
                foreach (RegisteredSocialClientViewModel item in model.SocialClients)
                {
                    switch (item.ProviderName)
                    {
                        case "facebook":
                            OAuthWebSecurity.RegisterFacebookClient(
                                 appId: item.ProviderClientId,
                                 appSecret: item.ProviderClientSecret);
                            break;
                        case "google":
                            var client = new GoogleOAuth2Client(item.ProviderClientId, item.ProviderClientSecret);
                            var extraData = new Dictionary<string, object>();
                            OAuthWebSecurity.RegisterClient(client, "Google", extraData);
                            break;
                        default:
                            break;
                    }
                }
            }
        }
    }
}
