﻿using System;
using System.Configuration;
using System.Web;
using System.Web.Optimization;

namespace Znode.Engine.MvcDemo
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(
                new ScriptBundle("~/bundles/jquery")
                .Include("~/Scripts/jquery-1.10.2.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            //bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
            //            "~/Scripts/modernizr-*"));


            bundles.Add(new ScriptBundle("~/bundles/customJs")
                .Include("~/Scripts/helpers.js")
                .Include("~/Scripts/global.js")
                .Include("~/Scripts/api.js")
                .Include("~/Scripts/lib/mustache.js")
                .Include("~/Scripts/home.js")
                .Include("~/Scripts/cart.js")
                .Include("~/Scripts/product.js")
                .Include("~/Scripts/checkout.js")
                .Include("~/Scripts/search.js")
                .Include("~/Scripts/account.js")
                .Include("~/Scripts/category.js")
                .Include("~/Scripts/flexisel-product-slider.js")
                .Include("~/Scripts/jquery.simpleGallery.js")
                .Include("~/Scripts/jquery.simpleLens.js")
                .Include("~/Scripts/quickOrder.js")
                .Include("~/Scripts/serviceRequest.js"));

            bundles.Add(new ScriptBundle("~/bundles/Animation-Slider")
                .Include("~/Scripts/lib/modernizr.js")
                .Include("~/Scripts/jquery.animateSlider.js")
                .Include("~/Scripts/owl.carousel.js")
                .Include("~/Scripts/lib/lightbox-2.6.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/Nicescroll")
                .Include("~/Scripts/nicescroll.js"));

            bundles.Add(new ScriptBundle("~/bundles/QuantityJS").Include("~/Scripts/PRFTQuantity.js"));

            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                        "~/Content/themes/base/jquery.ui.core.css",
                        "~/Content/themes/base/jquery.ui.resizable.css",
                        "~/Content/themes/base/jquery.ui.selectable.css",
                        "~/Content/themes/base/jquery.ui.accordion.css",
                        "~/Content/themes/base/jquery.ui.autocomplete.css",
                        "~/Content/themes/base/jquery.ui.button.css",
                        "~/Content/themes/base/jquery.ui.dialog.css",
                        "~/Content/themes/base/jquery.ui.slider.css",
                        "~/Content/themes/base/jquery.ui.tabs.css",
                        "~/Content/themes/base/jquery.ui.datepicker.css",
                        "~/Content/themes/base/jquery.ui.progressbar.css",
                        "~/Content/themes/base/jquery.ui.theme.css"));
            //PRFT Custom Code:Start

            bundles.Add(new ScriptBundle("~/bundles/ShopByBrandsJS").Include("~/Scripts/PRFTShopByBrand.js"));
            BundleTable.EnableOptimizations = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableJSAndCssOptimization"]);
            bundles.Add(new ScriptBundle("~/bundles/CreditApplicationJS")
                .Include("~/Scripts/lib/angular.js")
                .Include("~/Scripts/CreditApplication.js")
                .Include("~/Scripts/lib/JSON2.js"));
            //PRFT Custom Code:End
        }
    }
}