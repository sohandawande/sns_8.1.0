
var Product = {
    init: function () {
        _debug("writeReviewForm.init");
        Product.bind.writeReviewStars();
        if (!Product.data.isQuickView) {
            // Pull in Lightbox library for image zooming
            $.getScript("/Scripts/lib/lightbox-2.6.min.js");

            $('#demo-1 .simpleLens-thumbnails-container img').simpleGallery({ loading_image: '/Images/lightbox/loading.gif' });
            $('#demo-1 .simpleLens-big-image').simpleLens({ loading_image: '/Images/lightbox/loading.gif' });
        }

        // Bindings

        Product.setProductBreadCrumb();
        Product.bind.productAttributes();
        Product.bind.productAddOns();
        Product.bind.productQuantity();
        Product.bind.addToWishlist();
        Product.bind.readReviews();
        Product.bind.ChangeMainImage();
        // get recent  product grids
        Product.getRecentProductList(function (res) {
            if (res === true) {
                loadSlider();
            }
        });
        Product.getRelatedProducts();
        Product.bind.jumpToReviews();
        Product.bind.getProductDetails();

        //PRFT Custom code: Start
        Product.bind.addToCartClick();        
        //PRFT Custom code: End

        // Lazy load related product grids


        // Disable addTocart button on renavigation when quantity reach to max quantity
        Product.quantityVerifieronRenavigation();
        Product.bind.writeReviewStars();
        Product.bind.bundleAttributes();
        Product.viewCompareProduct();
        Product.bind.productDetailSendMailPopup();


    },
    // Module variables. Use for local tracking
    data: {
        productId: 0,
        lastSelectedAttributeId: 0,
        //Show reviews on product listing pages.-Starts
        productContent: "scrollReview",
        //Show reviews on product listing pages.-Ends
        isQuickView: false
    },



    getRelatedProducts: function () {
        _debug("getRelatedProducts()");
        global.local.lazyLoadContent("#layout-product", function (el, params, renderAction) {
            Api.getRelatedProducts(params.ids, params.accountid, function (response) {
                renderAction(response);
                global.local.lazyLoadImages("#" + el.id); // Lazy load images in the incoming markup
            });

        });
    },

    // get recent  product grids
    getRecentProductList: function (fun_res) {
        var productId = Product.local.setProductId();
        Api.getRecentProducts(productId, 5, function (response) {
            var title = $("#recently-viewed-header").html();
            $("#layout-product .recent-view-items").html("<h4>" + title + "</h4>" + response.data.html);
            fun_res(true);
        });
    },



    // Disable addTocart button on renavigation when quantity reach to max quantity
    quantityVerifieronRenavigation: function () {
        $("#Quantity").change();
    },

    // Updates total price element
    refreshPrice: function (amount) {        
        //$("#layout-product .dynamic-product-price").html(amount);
        if (amount)
            amount = amount.replace("$", "");

        $("#layout-product .dynamic-product-price").html(amount);
    },
    refreshMainProductImage: function (data) {
        if (data.imagePath != null && data.imagePath != "") {
            //$("#product-image").attr('src', data.imageMediumPath);
            //$("#product-image").attr('data-src', data.imageMediumPath);
            //$("#product-lens-image").attr('data-lens-image', data.imagePath);
            $("#product-image").attr('src', data.imagePath);
            $("#product-image").attr('data-src', data.imagePath);
            $("#product-lens-image").attr('data-lens-image', data.imageOriginalPath);
        }
    },
    inventoryStatus: function (response) {
        if (response.data.isbackorder) {
            $("#dynamic-inventory").show().html(response.data.backordermessage);
            $("#button-addtocart").removeAttr("disabled");
        }
        else if (response.success) {
            // In stock
            $("#dynamic-inventory").hide();
            $("#button-addtocart").removeAttr("disabled");
        } else {
            // Out of stock
            //$('.form-error').show().html("Quantity is out of stock."); //As per requirements Inventory is always in stock.
            $("#button-addtocart").attr("disabled", "disabled");
            $("#dynamic-inventory").show().html(response.message);
            
            //console.log("Inventory out of stock");
        }
    },
    
    updateProductValues: function (response, quantity) {
        var selectedAddOnIds = Product.local.getAddOnIds();
        // Set form values for submit
        $("#dynamic-productid").val(Product.data.productId);
        $("#dynamic-sku").val(response.data.sku);
        $("#Quantity").val(quantity);
        $("#dynamic-addons").val(selectedAddOnIds);
        $("#dynamic-productName").val(Product.data.ProductName);
        if (response.data.addOnId != undefined && response.data.addOnMessage != undefined && response.data.isOutOfStock != undefined) {
            for (var index = 0; index < response.data.addOnId.length; index++) {
                if (response.data.isOutOfStock[index]) {
                    $("#dynamic-addOninventory_" + response.data.addOnId[index]).show();
                    $("#dynamic-addOninventory_" + response.data.addOnId[index]).html(response.data.addOnMessage[index]);
                }
                else {
                    $("#dynamic-addOninventory_" + response.data.addOnId[index]).hide();
                    $("#dynamic-addOninventory_" + response.data.addOnId[index]).html("");
                }
            }
        }
    },


    viewCompareProduct: function () {
        _debug("viewCompareProduct.init()");

        $(".remove-compare").on("click", function () {
            Product.bind.removeProduct($(this).attr("data-productid"));
        });

        $("#lnkSendMail").on("click", function () {
            Product.bind.sendMailPopup();
        });
    },
    // LOCAL ACTIONS, various views
    local: {

        setProductId: function () {
            Product.data.productId = $(".product-meta").data("id");

            return (Product.data.productId);
        },

        addToWishList: function (el) {
            var productId = Product.local.setProductId();
            var clicked = $(el);

            Api.addProductToWishList(productId, function (response) {

                $(".wishlist-status").remove(); // Clears any existing message
                $("#SuccessMessage").empty();

                response.data.message = response.message; // Adds response message to data object for template

                // Build status message template
                var template = $("#tmpl-wishliststatus").html();
                var html = Mustache.render(template, response.data);

                if (response.success) {
                    // Added to list
                    clicked.after(html);
                } else {
                    if (response.data.redirect) {
                        // Login needed
                        global.local.setAlertMessage(response.message, "info");
                        document.location = response.data.redirect;
                    } else {
                        // Catchall error
                        clicked.after(html);
                    }
                }
            });

        },

        updateProductVariations: function (htmlContainer, quantity, callback) {           
            var selectedIds = Product.local.getAttributeIds();
            var selectedAddOnIds = Product.local.getAddOnIds();
            if (Product.data.lastSelectedAttributeId <= 0) {
                Product.data.lastSelectedAttributeId = selectedIds;
            }

            var params = "attributeId=" + Product.data.lastSelectedAttributeId + "&selectedIds=" + selectedIds + "&quantity=" + quantity + "&selectedAddOnIds=" + selectedAddOnIds;

            Api.getProductAttributes(Product.local.setProductId(), params, function (response) {
                if (response.data.addOnId != undefined && response.data.addOnMessage != undefined && response.data.isOutOfStock != undefined) {
                    for (var index = 0; index < response.data.addOnId.length; index++) {
                        if (response.data.isOutOfStock[index]) {
                            $("#dynamic-addOninventory_" + response.data.addOnId[index]).show();
                            $("#dynamic-addOninventory_" + response.data.addOnId[index]).html(response.data.addOnMessage[index]);
                        }
                        else {
                            $("#dynamic-addOninventory_" + response.data.addOnId[index]).hide();
                            $("#dynamic-addOninventory_" + response.data.addOnId[index]).html("");
                        }                        
                    }
                }
                if (htmlContainer) {
                    $(htmlContainer).html(response.data.html);
                }

                if (callback) {
                    callback(response);
                }
            });
        },

        updateBundleVariations: function (htmlContainer, bundleProductId, callback) {
            var selectedIds = Product.local.getAttributeIds("#bundle-item-" + bundleProductId);
            var selectedAddOnIds = Product.local.getAddOnIds("#bundle-item-" + bundleProductId);
            if (Product.data.lastSelectedAttributeId <= 0) {
                Product.data.lastSelectedAttributeId = selectedIds;
            }

            var params = "attributeId=" + Product.data.lastSelectedAttributeId + "&selectedIds=" + selectedIds + "&quantity=1&selectedAddOnIds=" + selectedAddOnIds;

            Api.getProductAttributes(bundleProductId, params, function (response) {
                if (htmlContainer) {
                    $(htmlContainer).html(response.data.html);
                }

                if (callback) {
                    callback(response);
                }
            });
        },

        getAttributeIds: function (parentSelector) {
            var selectedIds = [];
            if (typeof parentSelector == "undefined") { parentSelector = ""; }

            $(parentSelector + " select.attribute").each(function () {
                selectedIds.push($(this).val());
            });

            return (selectedIds.join());
        },

        getAddOnIds: function (parentSelector) {
            var selectedIds = [];
            var addOnsCustomText = [];
            if (typeof parentSelector == "undefined") { parentSelector = ""; }
            $(parentSelector + " select.AddOn").each(function () {
                selectedIds.push($(this).val());
            });

            $(parentSelector + " input.AddOn:checked").each(function () {
                selectedIds.push($(this).val());
            });                       

            $(parentSelector + ".AddOnValues").each(function () {
                var optional = $(this).data("isoptional");
                var displayType = $(this).data("addondisplaytype");
                if (displayType == "TextBox" && optional == "False") {
                    selectedIds.push($(this).data("addonvalueid"));
                    addOnsCustomText.push($(this).val());
                }
                else if (displayType == "TextBox" && optional == "True" && $(this).val() != "") {
                    selectedIds.push($(this).data("addonvalueid"));
                    addOnsCustomText.push($(this).val());
                }
            });
            return (selectedIds.join());
        },

        GetDetails: function () {
            var selectedIds = [];
            var addOnsCustomText = [];
            var addOnValueIds = [];
            var addToCart = false;
            var checkBoxaAddOnCart = true;
            var textBoxAddOn = true;
            if (typeof parentSelector == "undefined") { parentSelector = ""; }

            //for dropdown
            $(parentSelector + " select.AddOn").each(function () {
                selectedIds.push($(this).val());
                addOnsCustomText.push("undefined");
            });
            
            $(parentSelector + " input.AddOn:checked").each(function () {
                selectedIds.push($(this).val());
                addOnsCustomText.push("undefined");
            });

            //for checkbox   
            $(".chk-product-addons").each(function () {
                var optional = $(this).data("isoptional");
                var id = $(this).attr("id");
                var addOnId = $(this).data("addonid");
                if (optional == "True") {                    
                    checkBoxaAddOnCart = true;
                }
                else {
                    var isError = true;
                    $('#' + id).find(':checkbox').each(function ()
                    {
                        if ($(this).is(':checked')) {                           
                            isError = false;                           
                        }
                    });
                    if (!isError) {
                        $("#paraComment-" + addOnId).css("display", "none");                       
                    }
                    else {
                        $("#paraComment-" + addOnId).removeAttr("style");
                        checkBoxaAddOnCart = false;
                    }
                }
               
            });

            //for textbox
            $(".AddOnValues").each(function () {
                var optional = $(this).data("isoptional");
                var displayType = $(this).data("addondisplaytype");
                var id = $(this).attr("id");
                var number = id.split('-');
                if (displayType == "TextBox" && optional == "False") {
                    if ($("#" + id).val() == "") {
                        $("#paraComment-" + number[1]).removeAttr("style");
                        textBoxAddOn = false;
                    }
                    else {
                        selectedIds.push($(this).data("addonvalueid"));
                        addOnValueIds.push($(this).data("addonvalueid"));
                        addOnsCustomText.push($(this).val());
                        $("#paraComment-" + number[1]).css("display", "none");
                        textBoxAddOn = true;
                    }
                }
                else if (displayType == "TextBox" && optional == "True" && $(this).val() != "") {
                    selectedIds.push($(this).data("addonvalueid"));
                    addOnsCustomText.push($(this).val());
                    textBoxAddOn = true;
                }
                $("#dynamic-addons").val(selectedIds);
                $("#dynamic-addonvaluecustomtext").val(addOnsCustomText);
            });
            if (textBoxAddOn && checkBoxaAddOnCart) {
                addToCart = true;
            }
            return addToCart;
        },
    },

    SendMail: function (isCompare) {
        var captchaResponse = grecaptcha.getResponse();
        isCompare = $("#dynamic-iscompare").val();
        var senderMailId = $("#txtSenderEmail").val();
        var recieverMailId = $("#txtReceiverEmail").val();
        var productId = $("#dynamic-productId").val();
        var url = "";
        if (isCompare === "True") {
            url = "/product/sendcomparedproductmail";
        }
        else {
            url = "/product/sendmailtofriend";
        }
        Api.getSenMailNOtification(url, productId, senderMailId, recieverMailId, captchaResponse, function (response) {
            $('#status-message').remove();
            $(".ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only ui-dialog-titlebar-close").click();
            $("#divSendMail").dialog("close");
            if (response.success) {
                $("#SuccessMessage").append('<div id="status-message" class="alert alert-success text-center">' + "Email send successfully" + '</div>')
            }
            else {
                $("#SuccessMessage").append('<div id="status-message" class="alert alert-danger text-center">' + "Email was not sent" + '</div>')
            }
        });
    },
    ProductSendMail: function () {
        var productId = $("#dynamic-productId").val();
        var productName = $("#dynamic-seoPageName").val();
        Api.getProductSendMailDetails(productId, productName, function (response) {
            $("#divSendMail").html(response);
            $("#btnSendMail").on("click", function () {
                if (!Product.bind.EmailValidation()) {
                    return false;
                }
                else {
                    if (grecaptcha != undefined && grecaptcha != null) {
                        grecaptcha.execute();
                    }
                    return false;
                }
            });
            $("#divSendMail").dialog({
                title: "Email a Friend",
                resizable: false,
                modal: true,
                width: 300,
                maxWidth: 300, //new option
                height: 'auto',
                modal: true,
                fluid: true, //new option
                create: function () { $(this).closest(".ui-dialog").addClass("ui-sm-popup send-mail-popup"); }
            });
        });
    },

    // BINDINGS
    // Keep all bindings in this object and call as needed from view init()	
    bind: {
        // Change to attributes on bundle options view; view does not include quantity option
        bundleAttributes: function () {
            _debug("bind.bundleAttributes()");
            $("#layout-bundlesettings .bundle-item-details").on("change", ".attribute, .AddOn", function (ev) {

                /*
				| !! - Need to find ajax-based solution rather than full POST on each change
				*/
                var form = $(this).closest("form");
                $("#dynamic-bundle-attributeId").val($(this).val());
                $("#dynamic-bundle-ProductId").val($(this).closest(".bundle-item").data("id"));
                form.attr('action', '/Product/UpdateBundles');
                form.submit();
            });

            $("#layout-bundlesettings .bundle-item-details").on("change", ".AddOn", function (ev) {
                $("#dynamic-addons").val($(this).val());
            });
        },

        // When attribute dropdown is changed, update region with new HTML
        productAttributes: function () {
            _debug("bind.productAttributes()");
            $("#dynamic-product-variations").on("change", "select.attribute", function (ev) {
                Product.data.lastSelectedAttributeId = $(this).val();
                //var quantity = $("select[id=Quantity]").val();
                var quantity = $("#Quantity").val();
                Product.local.updateProductVariations("#dynamic-product-variations", quantity, function (response) {                    
                    //quantity = response.quantity;
                    Product.updateProductValues(response, quantity);

                    // Update display
                    Product.refreshPrice(response.data.price);
                    Product.inventoryStatus(response);

                    Product.refreshMainProductImage(response.data);
                    $("#dynamic-product-variations").html(response.data.html);
                    $("select[id=Quantity]").change();
                });
            });
        },

        // When add-on input control is changed, update region with new HTML
        productAddOns: function () {
            _debug("bind.productAddOns()");
            $("#dynamic-product-addons").on("change", ".AddOn", function (ev) {                
                var quantity = $("select[id=Quantity]").val();
                Product.local.updateProductVariations(false, quantity, function (response) {
                    Product.updateProductValues(response, quantity);
                    Product.refreshPrice(response.data.price);
                    Product.inventoryStatus(response);
                });
            });
        },


        // When quantity is changed, update region with new HTML
        productQuantity: function () {
            _debug("bind.productQuantity()");            
            //$("#dynamic-product-variations").on("change", "select.quantity", function (ev) {
            //    Product.local.updateProductVariations(false, $(this).val(), function (response) {
            //        Product.refreshPrice(response.data.price);
            //        Product.inventoryStatus(response);
            //    });

            //debugger;
                //PRFT Custom Code:START               
            $(".quantity-options #Quantity").on("change", function (ev) {
                    $("#dynamic-inventory").html("").hide();

                    if ($(this).val() == '') {
                        $('.form-error').show().html("Quantity is required.");
                        $("#button-addtocart").attr("disabled", "disabled");
                    }
                    else if($(this).val() == 0)
                    {
                        $('.form-error').show().html("Quantity cannot be zero.");
                        $("#button-addtocart").attr("disabled", "disabled");
                    }
                    else {                        
                        $('.form-error').hide();
                        $('.error-msg').hide();
                        $("#button-addtocart").attr("disabled", "false");
                        Product.local.updateProductVariations(false, $(this).val(), function (response) {                            
                            Product.refreshPrice(response.data.price);
                            Product.inventoryStatus(response);                            
                        });
                    }
                    //PRFT Custom Code:END
            });
        },


        // Click for Add to Wishlist button
        addToWishlist: function () {
            _debug("bind.addToWishlist()");

            $("#layout-product .button-wishlist").click(function (e) {
                Product.local.addToWishList(this);
            });
        },

        //PRFT custom code for Click for Add to Cart Button: Start
        addToCartClick: function () {
            _debug("bind.addToCartClick()");

            $("#button-addtocart").click(function (event) {
                var quantity = parseInt($(".quantity-options #Quantity").val());

                if (parseInt(quantity % 1) != 0 || parseInt(quantity) <= 0) {
                    $('.form-error').show().html("Enter Valid Quantity");
                    $("#button-addtocart").attr("disabled", "disabled");
                    isSuccess = false;
                    return false;
                }
                else if (isNaN(quantity) || quantity == "") {
                    $('.form-error').show().html("Enter Whole Number");
                    $("#button-addtocart").attr("disabled", "disabled");
                    isSuccess = false;
                    return false;
                }
                else {
                    $('#button-addtocart').removeAttr('disabled');
                }
            });

        },
        //PRFT custom code for Click for Add to Cart Button: End
        // Click for "read reviews" link at top of product; jump and toggle
        readReviews: function () {

            _debug("bind.readReviews()");

            $("#jumpto-readreviews").click(function (ev) {
                ev.preventDefault();
                $("#tab-reviews").click();
                scrollToTop(300, "#scrollReview");
            });
        },

        jumpToReviews: function () {
            var url = document.URL.toString();
            var name = '';
            if (!(url.indexOf('#') === -1)) {
                var urlParts = url.split("#");
                name = urlParts[1];
            }
            if (name == Product.data.productContent.toString()) {
                $("#tab-reviews").click();
                scrollToTop(300, "#scrollReview");
            }
        },

        // Click for write review form stars
        writeReviewStars: function () {
            _debug("bind.writeReviewStars()");

            $("#layout-writereview .setrating label").on("click", function () {
                $("#layout-writereview .setrating label").removeClass("full").addClass("empty"); // Reset all to empty

                var stars = $(this).data("stars");

                for (a = 1; a <= stars; a += 1) {
                    $(".star" + a).removeClass("empty").addClass("full");
                }
            });
        },

        //Change the Main image in Product Details on click of Swatch(Color) Images. - Start 
        ChangeMainImage: function () {
            _debug("bind.ChangeMainImage()");
            $(".SwatchImage").on("click", function () {
                $("#product-image").attr('src', $(this).attr("data-mediumsrc"));
                $("#product-image").attr('data-src', $(this).attr("data-mediumsrc"));
                $("#product-lens-image").attr('data-lens-image', $(this).attr("data-largesrc"));
            });
        },
        //Change the Main image in Product Details on click of Swatch(Color) Images. - End 
       

        removeProduct: function (productId) {
            _debug("bind.removeProduct()");
            var url = window.location.href.toString().split('/')
            var control = url[3];
            Api.removeProduct(productId, control, function (response) {
                if (response.count == 0) {
                    window.location.href = "/Home/Index";
                }
                if (response.count == 1) {
                    $(".compare-print").html("");
                }

                $("#divRemoveProduct").html("");
                $("#divRemoveProduct").html(response.data.html);
                if (response.message != "" && response.count == 1) {
                    $("#SuccessMessage").append('<div id="status-message" class="alert alert-danger text-center">' + response.message + '</div>')
                }
                $(".remove-compare").unbind("click");
                $(".remove-compare").on("click", function () {
                    Product.bind.removeProduct($(this).attr("data-productid"));
                });
                return true;
            });
        },

        sendMailPopup: function () {
            _debug("bind.sendMailPopup()")
            Api.getComparedProductSendMailDetails(function (response) {
                $("#divSendMail").html(response);
                $("#btnSendMail").on("click", function () {
                    if (!Product.bind.EmailValidation()) {
                        return false;
                    }
                    else {
                        var isCompare = $("#dynamic-iscompare").val();
                        Product.SendMail(isCompare);
                    }
                });
                $("#divSendMail").dialog({
                    title: "Product Compare",
                    modal: true,
                    width:300,
                    height: 'auto',
                    maxWidth: 600,
                    modal: true,
                    fluid: true, //new option
                   create: function () { $(this).closest(".ui-dialog").addClass("ui-sm-popup send-mail-popup"); }
                });
            });
        },

        EmailValidation: function () {
            _debug("bind.EmailValidation()");
            $("#valSenderEmail").text('').removeClass("field-validation-error").hide();
            $("#valReceiverEmail").text('').removeClass("field-validation-error").hide();
            var senderEmail = $("#txtSenderEmail").val();
            var receiverEmail = $("#txtReceiverEmail").val();
            var pattern = new RegExp(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/);
            if (senderEmail.length < 1 && receiverEmail.length < 1) {
                //$("#valSenderEmail").text('').text('Enter email address').addClass("field-validation-error").show();
                $("#valSenderEmail").text('').text('Enter Your Name').addClass("field-validation-error").show();
                $("#valReceiverEmail").text('').text('Enter email address').addClass("field-validation-error").show();
                return false;
            }
            //else if (!pattern.test(senderEmail) && receiverEmail.length < 1) {
            //    $("#valSenderEmail").text('').text('Enter valid email address').addClass("field-validation-error").show();
            //    $("#valReceiverEmail").text('').text('Enter email address').addClass("field-validation-error").show();
            //    return false;
            //}
            else if (!Product.bind.validateEmail(receiverEmail, pattern) && senderEmail.length < 1) {
                $("#valReceiverEmail").text('').text('Enter valid email address').addClass("field-validation-error").show();
                //$("#valSenderEmail").text('').text('Enter email address').addClass("field-validation-error").show();
                $("#valSenderEmail").text('').text('Enter Your Name').addClass("field-validation-error").show();
                return false;
            }
            //else if (!pattern.test(senderEmail) && !Product.bind.validateEmail(receiverEmail, pattern)) {
            //    $("#valSenderEmail").text('').text('Enter valid email address').addClass("field-validation-error").show();
            //    $("#valReceiverEmail").text('').text('Enter valid email address').addClass("field-validation-error").show();
            //    return false;
            //}
            //else if (!pattern.test(senderEmail)) {
            //    $("#valSenderEmail").text('').text('Enter valid email address').addClass("field-validation-error").show();
            //    return false;
            //}
            else if (senderEmail.length < 1) {
                //$("#valSenderEmail").text('').text('Enter email address').addClass("field-validation-error").show();
                $("#valSenderEmail").text('').text('Enter Your Name').addClass("field-validation-error").show();
                return false;
            }
            else if (receiverEmail.length < 1) {
                $("#valReceiverEmail").text('').text('Enter email address').addClass("field-validation-error").show();
                return false;
            }
            else if (!Product.bind.validateEmail(receiverEmail, pattern)) {
                $("#valReceiverEmail").text('').text('Enter valid email address').addClass("field-validation-error").show();
                return false;
            }
            return true;
        },

        validateEmail: function (mailId, pattern) {
            var email = mailId.split(',');
            for (var i = 0; i < email.length; i++) {
                if (email[i].length < 1) {
                    return false;
                }
                else if (!pattern.test(email[i])) {
                    return false;
                }
            }
            return true;
        },

        productDetailSendMailPopup: function () {
            //PRFT commented code: start
            //$("#email-a-friend > li").find('a').on("click", function (e) {
            //PRFT commented code: end
            $("#email-a-friend").find('a').on("click", function (e) {

                e.preventDefault();
                Product.ProductSendMail();
            });
        },

        getProductDetails: function () {
            $("#button-addtocart").on("click", function () {
                var addToCart = Product.local.GetDetails()
                if (addToCart) {
                    return true;
                }
                else {
                    return false
                }
            });
        },
    },

    setProductBreadCrumb: function () {
        _debug("bind.setProductBreadCrumb()")
        var isQuickView = $("#hdnQuickView").val();
        var productName = $(".product-name").html();
        Api.getProductBreadCrumb(Product.local.setProductId(), productName, isQuickView, function (response) {
            if (response != undefined && response != null && response.data != undefined && response.data != null && response.data.ProductBreadCrumb != undefined && response.data.ProductBreadCrumb != null && response.data.ProductBreadCrumb != "") {
                $("#breadcrumb-content").html(response.data.ProductBreadCrumb);
                $("#categoryLink").attr("href", response.data.CategoryUrl);
                $("#categoryName").html(response.data.CategoryName);
            }
        });
    },
    GetProductOutOfStockDetails: function (control, e) {
        e.preventDefault();
        var productId = $(control).attr("data-value");
        Api.getProductOutOfStockDetails(productId, function (response) {
            if (response === "True") {
                $(control).closest("form").submit();
            }
            else {
                $(control).attr("disabled", "disabled")
                return false;
            }
        });
    }
}

//PRFT custom code for Responsive Jquery Dialog: start
$(window).resize(function () {
    fluidDialog();
});

$(document).on("dialogopen", ".ui-dialog", function (event, ui) {
    fluidDialog();
});

function fluidDialog() {
    var $visible = $(".ui-dialog:visible");
    // each open dialog
    $visible.each(function () {
        var $this = $(this);
        var dialog = $this.find(".ui-dialog-content").data("ui-dialog");
        // if fluid option == true
        if (dialog.options.fluid) {
            var wWidth = $(window).width();
            // check window width against dialog width
            if (wWidth < (parseInt(dialog.options.maxWidth) + 50)) {
                // keep dialog from filling entire screen
                $this.css("max-width", "90%");
            } else {
                // fix maxWidth bug
                $this.css("max-width", dialog.options.maxWidth + "px");
            }
            //reposition dialog
            dialog.option("position", dialog.options.position);
        }
    });

}
//PRFT custom code for Responsive Jquery Dialog: End