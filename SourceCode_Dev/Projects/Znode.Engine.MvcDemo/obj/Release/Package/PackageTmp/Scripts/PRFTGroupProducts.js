﻿var GroupedProduct = function (args) {

}
GroupedProduct.prototype = {
    Init: function () {
        this.ApplyNumericKeyPad();
        this.UpdatePrice();
        $(".addtocart").on("click", $.proxy(this.ValidateGroupProductAddToCart, this));
        $(".button-wishlist").on("click", $.proxy(this.ValidateGroupProductAddToCart, this));
    },
    ApplyNumericKeyPad: function () {
        //$('.group-product input[type=text]').keypress(function (event) {
        $('.group-product input[name="Quantity"]').keypress(function (event) {
            // Backspace, tab, enter, end, home, left, right
            // We don't support the del key in Opera because del == . == 46.
            // var controlKeys = [8, 9, 13, 35, 36, 37, 39];
            var controlKeys = [8, 9, 13];
            // IE doesn't support indexOf
            var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
            // Some browsers just don't raise events for control keys. Easy.
            // e.g. Safari backspace.

            if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
                (48 <= event.which && event.which <= 57) || // Always 1 through 9
                (48 == event.which && $(this).attr("value")) || // No 0 first digit
                isControlKey) { // Opera assigns values for control keys.
                return;
            }
            else {
                if (event.preventDefault) {
                    event.preventDefault();
                }
                if (event.stopPropagation) {
                    event.stopPropagation();
                }
            }
        });
    },
    ValidateGroupProductAddToCart: function () {
        var msgSpn = $('.noSkuSelectedError');
        var groupedProductsRow = $('.group-product');
        var groupedProductsRowLength = groupedProductsRow.length;
        var noSkuSelectedError = true;
        msgSpn.html('');
        for (var index = 0; index < groupedProductsRowLength; index++) {
            //var qtyBox = $(groupedProductsRow[index]).find("input[type=text]");
            var qtyBox = $(groupedProductsRow[index]).find('input[name="Quantity"]');
            var qtyError = $(groupedProductsRow[index]).find(".qtyError");
            if ($.isNumeric(qtyBox.val())) {

                qtyError.html('');
                var qty = qtyBox.val();
                //var qty = qtyBox.value;
                var minimumQtytxt = qtyBox.data('minqty');
                var minimumQty = parseInt(minimumQtytxt, 10);
                var maxmimumQtytxt = qtyBox.data("maxqty");
                var maxmimumQty = parseInt(maxmimumQtytxt, 10);                
                var isb2busertxt = qtyBox.data("isb2buser");                                
                if (isb2busertxt === "True") {                                      
                    noSkuSelectedError = false;
                    return true;
                }
                else {                    
                    if (qty > maxmimumQty) {                        
                        qtyError.html("Please enter Quantity less than" + maxmimumQty);
                        //break;                        
                        return false;
                    }
                    if (qty < minimumQty) {
                        qtyError.html("Please enter Quantity greater than 0");
                        //break;
                        return false;
                    }
                    noSkuSelectedError = false;
                }

               
            }
            //else {
            //    qtyError.html("Please Enter Numeric Value");
            //}
        }
        if (noSkuSelectedError) {
            $(".dialog").remove();
            msgSpn.html("Quantity is required for at least a product.");
            return false;
        }
        else { return true; }
        return false;
    },
    UpdatePrice: function () {
        var isAddToCartDisabled = $("button[id^=button-addtocart]").is(":disabled");
        if (!isAddToCartDisabled) {
            var totalPrice = 0;
            $('tr.group-product').each(function () {
                var sku = $(this).find('#sku').text();
                $("#" + sku + "_Quantity").change(function (e) {

                    e.preventDefault();                    
                    var Qty = $("#" + sku + "_Quantity").val();
                    var productPrice = $("#" + sku + "_unitPrice").val();

                    var price = $("#" + sku + "_productPrice").text().split("$")[1];
                    if (Qty.length <= 6) {
                        if (Qty == '') {
                            totalPrice = (totalPrice - price);
                            if (totalPrice == -0) {
                                totalPrice = 0;
                            }
                            $(".total-price").text("$" + parseFloat(totalPrice).toFixed(2));
                            $("#" + sku + "_productPrice").text("$" + parseFloat(productPrice).toFixed(2));
                            return false;
                        }
                        else {
                            var finalPrice = Qty * productPrice;
                            finalPrice = finalPrice.toFixed(2);
                            $("#" + sku + "_productPrice").text("$" + finalPrice);
                            ////Total Price
                            //totalPrice = (parseFloat(totalPrice) + parseFloat(finalPrice)).toFixed(2);
                            //$(".total-price").text("$" + totalPrice);
                            totalPrice = TotalPrice();
                        }
                    }
                });
            });
        }
    }
}
function TotalPrice() {
    var finalTotalPrice = 0;
    var isAddToCartDisabled = $("button[id^=button-addtocart]").is(":disabled");
    var isb2busertxt= $("isb2buser").val();
    if (!isAddToCartDisabled) {
        if (IsValidQuantity()) {
            $('tr.group-product').each(function () {                
                var price = $(this).find('.price-quan-outer .price').text().split("$")[1];
                if ($(this).find('input[name="Quantity"]').val() != "" && $(this).find('input[name="Quantity"]').val().length <= 6 && isb2busertxt=="False") {
                    finalTotalPrice = (parseFloat(finalTotalPrice) + parseFloat(price)).toFixed(2);
                    $(".total-price").text("$" + finalTotalPrice);
                }
                else {
                    finalTotalPrice = (parseFloat(finalTotalPrice) + parseFloat(price)).toFixed(2);
                    $(".total-price").text("$" + finalTotalPrice);
                }
            });
            return finalTotalPrice;
        }
    }

}
function IsValidQuantity() {
    var result = false;
    $('tr.group-product input[name="Quantity"]').each(function () {
        if ($(this).val() != "" && $(this).val().length <= 5) {
            result = true;
        }
    });
    return result;
}