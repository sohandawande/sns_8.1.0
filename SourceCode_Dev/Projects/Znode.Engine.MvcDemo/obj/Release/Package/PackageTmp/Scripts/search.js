
var Search = {

    // INIT	
    init: function () {
        $.getScript("/Scripts/lib/purl.js");
        Search.bind.hideControl();
        Search.bind.searchSort();

        Search.bind.changeSearchProductViewDisplay();
        Search.bind.setProductViewDisplay();
        Search.bind.SetSwatchImage();
        Search.bind.recentProductList(function (res) {
            if (res === true) { loadSlider(); }
        });
        Search.bind.addToCompare();
        Search.bind.compareProductList();
        Home.bind.quickView();
        //PRFT custom code: start
        Search.bind.ShowHideFacetList();
        //PRFT custom code: start
    },

    // BINDINGS
    bind: {
        // Redirects to new search results page when the sorting dropdown is changed
        // Uses purl library
        searchSort: function () {
            _debug("bind.searchSort()");

            $("#layout-search .search-sorting select").on("change", function () {
                $("#layout-search .search-results").html('<div class="search-results-wait">...</div>');

                var url = purl(),
					query = url.param();

                query.sort = $(this).val();
                query.pagenumber = 1;

                window.location.href = url.attr("path") + "?" + $.param(query);
            });
            Search.bind.searchPaging();
        },

        searchPaging: function () {
            _debug("bind.searchPaging()");

            $("#layout-paging .search-paging select").on("change", function () {
                $("#layout-search .search-results").html('<div class="search-results-wait">...</div>');

                var url = purl(),
					query = url.param();

                query.pageSize = $(this).val();
                query.pagenumber = 1;

                window.location.href = url.attr("path") + "?" + $.param(query);
            });
        },

        // get recent  product grids
        recentProductList: function (fun_res) {
            var productId = null;
            var htmlContainer = "#category-recent-product .recent-view-items";
            Api.getRecentProducts(productId,5, function (response) {
                if (htmlContainer) {
                    $(htmlContainer).html(response.data.html);
                }
                if (response.data.html == undefined || response.data.html.length < 1) {
                    $("#category-recent-product").hide();
                }
                else {
                    $("#category-recent-product").show();
                }
                fun_res(true);
            });
        },

        //PRFT Custom Code: Start
        //Show-Hide Facet List on Show More Link Click
        ShowHideFacetList: function () {
            _debug("bind.ShowHideFacetList()");
            //For Show/Hide category list on Show More Link click: Start
            var initialListItems = 4;
            $(".list-group-content").each(function () {
                var thisList = $(this);
                var wrap = thisList.find(".filter-list");
                var defHeight = wrap.height();
                if (wrap.find("li").length > initialListItems) {
                    $(this).append("<div class='link-text'><a class='show-more' href='javascript:void(0);'>show more</a></div>")
                    var readMore = thisList.find(".show-more");
                    wrap.find("li").each(function (index) {
                        if (index >= initialListItems) {
                            $(this).hide();
                        }
                    });


                    readMore.on("click", function (event) {
                        event.preventDefault();

                        var curLiLength = wrap.find("li:visible").length;
                        if (curLiLength == initialListItems) {
                            wrap.find("li").show("fast");
                            $(this).text("show less").removeClass('show-more').addClass("show-less");

                        }
                        else {
                            wrap.find("li").each(function (index) {
                                if (index >= initialListItems) {
                                    $(this).hide("fast");
                                }
                            });

                            $(this).text("show more").removeClass("show-less").addClass('show-more');

                        }
                        return false;
                    });
                }
            });
        },
        //PRFT Custom Code : End

        //Display products which are added for comparison.
        compareProductList: function () {
            var htmlContainer = "#compareProductList";
            Api.getCompareProducts(function (response) {
                if (htmlContainer) {
                    $(htmlContainer).html(response.data.html);
                    if (response.count > 0) {
                        $("#compareProductBox").removeAttr("style");
                    }
                    $(".remove-compare").unbind("click");
                    $(".remove-compare").on("click", function () { Category.removeProduct($(this).attr("data-productid")); });
                }
                if (response.data.html == undefined || response.data.html.length < 1) {
                    $("#compareProductList").hide();
                }
                else {
                    $("#compareProductList").show();
                }
                return true;
            });
        },

        changeSearchProductViewDisplay: function () {
            $(".productview").on("click", function () {
                var previousClass = $("#view-option").attr('class').split(' ')[1];
                var newClass = $(this).attr('title').toLowerCase().replace(" ", "-");

                $(".productview").each(function () {
                    if ($(this).attr("class").indexOf('-active') >= 0) {
                        var baseClass = $(this).attr('class').replace('-active', '');
                        $(this).removeClass($(this).attr('class'));
                        $(this).addClass(baseClass);
                    }
                });

                var activeclass = $(this).attr('class') + '-active';
                $(this).removeClass($(this).attr('class'));
                $(this).addClass(activeclass);

                if (previousClass != undefined && previousClass.length > 0) {
                    $("#view-option").removeClass(previousClass).addClass(newClass)
                } else {
                    $("#view-option").addClass(newClass)
                }
                localStorage["currentDisplayType"] = newClass;
            });
        },
        setProductViewDisplay: function () {
            var displayType = localStorage["currentDisplayType"];
            var previousClass = $("#view-option").attr('class').split(' ')[1];

            $(".productview").each(function () {
                if ($(this).attr("class").indexOf('-active') >= 0) {
                    var baseClass = $(this).attr('class').replace('-active', '');
                    $(this).removeClass($(this).attr('class'));
                    $(this).addClass(baseClass);
                }
            });

            $(".productview").each(function () {
                if (!displayType) {
                    if ($(this).attr("class").indexOf("grid-view") >= 0) {
                        var firstClass = $(this).attr('class');
                        $(this).removeClass(firstClass);
                        $(this).addClass(firstClass + "-active");
                    }
                }
                else {
                    if ($(this).attr("class").indexOf(displayType) >= 0) {
                        var activeclass = $(this).attr('class') + '-active';
                        $(this).removeClass($(this).attr('class'));
                        $(this).addClass(activeclass);
                    }
                }
            });
            if (!displayType) {
                $("#view-option").removeClass(previousClass).addClass("grid-view");
            }
            else {
                $("#view-option").removeClass(previousClass).addClass(displayType);
            }
            $(".view-opt").show();
        },
        SetSwatchImage: function () {
            _debug("bind.SetSwatchImage()");
            $(".SwatchImage").on("click", function () {
                var productImageId = $(this).attr("data-imgid");
                if (productImageId != null && productImageId > 0) {
                    $("#" + productImageId).attr('src', $(this).attr("data-mediumsrc"));
                    $("#" + productImageId).attr('data-src', $(this).attr("data-mediumsrc"));
                }
            });
        },
        hideControl: function () {
            $("#category-recent-product").hide();
            $(".view-opt").hide();
        },

        //add product in comparison list and display on category page
        addToCompare: function () {
            _debug("bind.addToCompare()");
            $(".button-compare").on("click", function () {
                var productId = $(this).attr("data-productid");
                var categoryId = $(this).attr("data-categoryid");
                var htmlContainer = "#compareProductList";
                Api.globalAddToCompare(productId, categoryId, function (response) {
                    if (response.success == true) {
                        $(htmlContainer).html(response.data.html);
                        $("#compareProductBox").removeAttr("style");
                        $(".remove-compare").unbind("click");
                        $(".remove-compare").on("click", function () { Category.removeProduct($(this).attr("data-productid")); });
                        $("#divAddedForCompare").text(response.message)
                        $("#divAddedForCompare").dialog({
                            title: "Product Compare",
                            resizable: false,
                            modal: true,
                            width: 400,
                            height: 170,
                            buttons: {
                                "Add  More": function () {
                                    var htmlContainer = "#compareProductList";
                                    $(".ui-dialog-titlebar-close").click();
                                },
                                "View  Comparison": function () {
                                    Api.getProductComparison(function (response) {
                                        if (response.status == true) {
                                            $(".ui-dialog-titlebar-close").click();
                                            $("#divAddedForCompare").text(response.message)
                                            $("#divAddedForCompare").dialog({
                                                title: "Product Compare",
                                                resizable: false,
                                                modal: true,
                                                width: 400,
                                                height: 170,
                                                buttons: {
                                                    "Add More": function () {
                                                        $(".ui-dialog-titlebar-close").click();
                                                    }
                                                }
                                            });
                                        }
                                        else {
                                            window.location.href = "/Product/ViewComparison";
                                        }
                                    });
                                }
                            },
                            create: function () { $(this).closest(".ui-dialog").addClass("ui-md-popup promotion-popup"); }
                        });
                    }
                    else if (response.errorCode == 1) {
                        $("#divAddedForCompare").text(response.message)
                        $("#divAddedForCompare").dialog({
                            title: "Product Compare",
                            resizable: false,
                            modal: true,
                            buttons: {
                                "OK": function () {
                                    Api.categoryAddToCompare(productId, categoryId, function (response) {
                                        if (response.success == true) {
                                            $(htmlContainer).html(response.data.html);
                                            $("#compareProductBox").removeAttr("style");
                                            $(".remove-compare").unbind("click");
                                            $(".remove-compare").on("click", function () { Category.removeProduct($(this).attr("data-productid")); });
                                            $(".ui-dialog-titlebar-close").click();
                                            $("#divAddedForCompare").text(response.message)
                                            $("#divAddedForCompare").dialog({
                                                title: "Product Compare",
                                                resizable: false,
                                                modal: true,
                                                width: 400,
                                                height: 170,
                                                buttons: {
                                                    "Add  More": function () {
                                                        var htmlContainer = "#compareProductList";
                                                        $(".ui-dialog-titlebar-close").click();
                                                        return true;
                                                    },
                                                    "View  Comparison": function () {
                                                        Api.getProductComparison(function (response) {
                                                            if (response.status == true) {
                                                                $(".ui-dialog-titlebar-close").click();
                                                                $("#divAddedForCompare").text(response.message)
                                                                $("#divAddedForCompare").dialog({
                                                                    title: "Product Compare",
                                                                    resizable: false,
                                                                    modal: true,
                                                                    width: 400,
                                                                    height: 170,
                                                                    buttons: {
                                                                        addMore: function () {
                                                                            $(".ui-dialog-titlebar-close").click();
                                                                        }
                                                                    }
                                                                });
                                                            }
                                                            else {
                                                                window.location.href = "/Product/ViewComparison";
                                                            }
                                                        });
                                                    }
                                                },
                                                create: function () { $(this).closest(".ui-dialog").addClass("ui-lg-popup promotion-popup"); }
                                            });
                                        }
                                    });
                                },
                                "Cancel": function () {
                                    $(".ui-dialog-titlebar-close").click();
                                }
                            },
                        });
                    }
                    else if (response.errorCode == 2) {
                        $("#divAddedForCompare").text(response.message)
                        $("#divAddedForCompare").dialog({
                            title: "Product Compare",
                            resizable: false,
                            modal: true,
                            width: 400,
                            height: 170,
                            buttons: {
                                "OK": function () {
                                    $(".ui-dialog-titlebar-close").click();
                                    return true;
                                }
                            }
                        });
                    }
                    else if (response.errorCode == 3) {
                        $("#divAddedForCompare").text(response.message)
                        $("#divAddedForCompare").dialog({
                            title: "Product Compare",
                            resizable: false,
                            modal: true,
                            width: 400,
                            height: 170,
                            buttons: {
                                "OK": function () {
                                    $(".ui-dialog-titlebar-close").click();
                                    return true;
                                }
                            }
                        });
                    }
                    if (response.data.html == undefined || response.data.html.length < 1) {
                        $("#compareProductList").hide();
                    }
                    else {
                        $("#compareProductList").show();
                    }
                });
            });
        },

        removeProduct: function (productId) {
            _debug("bind.removeProduct()");
            var htmlContainer = "#compareProductList";
            var url = window.location.href.toString().split('/')
            var control = url[3];
            Api.removeProduct(productId, control, function (response) {
                if (htmlContainer) {
                    $(htmlContainer).html(response.data.html);
                    if (response.count > 0) {
                        $("#compareProductBox").removeAttr("style");
                    }
                    $(".remove-compare").unbind("click");
                    $(".remove-compare").on("click", function () { Category.removeProduct($(this).attr("data-productid")); });
                }
                if (response.data.html == undefined || response.data.html.length < 1) {
                    $("#compareProductList").hide();
                }
                else {
                    $("#compareProductList").show();
                }
                return true;
            });
        }
    }
}
