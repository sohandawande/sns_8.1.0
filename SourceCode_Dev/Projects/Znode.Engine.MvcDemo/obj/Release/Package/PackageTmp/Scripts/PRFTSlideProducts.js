﻿var PRFTSlideProducts = function (args) {
    this.Arguments = args;
};

PRFTSlideProducts.prototype =
{
    Init: function () {
        this.FlexSlider();
    },
    FlexSlider: function () {
        var slidername = this.Arguments.sliderID;
        if (slidername != undefined) {
            var className = "";
            var totalItemsCount = 0;
            var actualCount = 0;
            className = $("#" + slidername).find('ul').attr('id');
            if (className != undefined) {
                totalItemsCount = $("#" + slidername).find('ul > li').length;
                actualCount = this.Arguments.MaxItemCount;

                if (totalItemsCount < this.Arguments.MaxItemCount) {
                    this.Arguments.MaxItemCount = totalItemsCount;

                    if (totalItemsCount == this.Arguments.MinimumItemsShown) {
                        this.Arguments.MobPortraitItemCount = this.Arguments.MobPortraitItemCount;
                        this.Arguments.MobLandItemCount = this.Arguments.MobLandItemCount;
                        this.Arguments.TabletItemCount = this.Arguments.TabletItemCount;
                    }
                }

                $('#' + className).flexisel({
                    visibleItems: this.Arguments.MaxItemCount,
                    autoPlay: false,
                    enableResponsiveBreakpoints: true,
                    responsiveBreakpoints: {
                        portrait: {
                            changePoint: 1024,
                            visibleItems: this.Arguments.MobPortraitItemCount
                        },
                        MoBportrait: {
                            changePoint: 361,
                            visibleItems: this.Arguments.MobPortraitItemCount
                        },
                        landscape: {
                            changePoint: 641,
                            visibleItems: this.Arguments.MobLandItemCount
                        },
                        tablet: {
                            changePoint: 801,
                            visibleItems: this.Arguments.TabletItemCount
                        }
                    }
                });
            }

            if ((totalItemsCount < actualCount || totalItemsCount == actualCount) && className != undefined) {
                var controlID = controlID = '#' + className;
                $(controlID).parent().next('div.nbs-flexisel-nav-left').addClass('disable');
                $(controlID).parent().next('div.nbs-flexisel-nav-left').next('div.nbs-flexisel-nav-right').addClass('disable');
            }
        }
    }
};