var count
var Category = {
    // INIT	
    init: function () {
        _debug("category.init()");
        //bind.recentProductList();
        Category.setSelectedCategory();
        Category.changeProductViewDisplay();
        Category.setProductViewDisplay();
        Category.SetSwatchImage();       
        Category.addToCompare();
        Category.recentProductList(function (res) {
            if (res === true) { loadSlider(); }
        });
        Category.compareProductList();
        Home.bind.quickView();
        $.getScript("/Scripts/lib/purl.js");
        Search.bind.searchSort();
        Home.bind.menuHandler();
        //PRFT custom code: start
        Category.ShowHideFacetList();
        //PRFT custom code: start
    },
    // get recent  product grids
    recentProductList: function (fun_res) {
        var productId = null;
        var htmlContainer = "#category-recent-product .recent-view-items";
        Api.getRecentProducts(productId, 5, function (response) {
            if (htmlContainer) {
                $(htmlContainer).html(response.data.html);
            }
            if (response.data.html == undefined || response.data.html.length < 1) {
                $("#category-recent-product").hide();
                $("#category-recent-product-header").hide();
            }
            else {
                $("#category-recent-product").show();
                $("#category-recent-product-header").show();
            }
            fun_res(true);
        });
    },

    //Display products which are added for comparison.
    compareProductList: function () {
        var htmlContainer = "#compareProductList";
        Api.getCompareProducts(function (response) {
            if (htmlContainer) {
                $(htmlContainer).html(response.data.html);
                if (response.count > 0) {
                    $("#compareProductBox").removeAttr("style");
                }
                $(".remove-compare").unbind("click");
                $(".remove-compare").on("click", function () { Category.removeProduct($(this).attr("data-productid")); });
            }
            if (response.data.html == undefined || response.data.html.length < 1) {
                $("#compareProductList").hide();
            }
            else {
                $("#compareProductList").show();
            }
            return true;
        });
    },

    changeProductViewDisplay: function () {
        $(".productview").on("click", function () {
            var previousClass = $("#view-option").attr('class').split(' ')[1];
            var newClass = $(this).attr('title').toLowerCase().replace(" ", "-");

            $(".productview").each(function () {
                if ($(this).attr("class").indexOf('-active') >= 0) {
                    var baseClass = $(this).attr('class').replace('-active', '');
                    $(this).removeClass($(this).attr('class'));
                    $(this).addClass(baseClass);
                }
            });

            var activeclass = $(this).attr('class') + '-active';
            $(this).removeClass($(this).attr('class'));
            $(this).addClass(activeclass);

            if (previousClass != undefined && previousClass.length > 0) {
                $("#view-option").removeClass(previousClass).addClass(newClass)
            } else {
                $("#view-option").addClass(newClass)
            }
            localStorage["currentDisplayType"] = newClass;
        });
    },
    setProductViewDisplay: function () {        
        var displayType = localStorage["currentDisplayType"];
        if ($("#view-option").html() != undefined) {
            var previousClass = $("#view-option").attr('class').split(' ')[1];

            $(".productview").each(function () {
                if ($(this).attr("class").indexOf('-active') >= 0) {
                    var baseClass = $(this).attr('class').replace('-active', '');
                    $(this).removeClass($(this).attr('class'))
                    $(this).addClass(baseClass);
                }
            });

            $(".productview").each(function () {
                if (!displayType) {
                    if ($(this).attr("class").indexOf("grid-view") >= 0) {
                        var firstClass = $(this).attr('class');
                        $(this).removeClass(firstClass);
                        $(this).addClass(firstClass + "-active");
                    }
                }
                else {
                    if ($(this).attr("class").indexOf(displayType) >= 0) {
                        var activeclass = $(this).attr('class') + '-active';
                        $(this).removeClass($(this).attr('class'));
                        $(this).addClass(activeclass);
                    }
                }
            });
            if (!displayType) {
                $("#view-option").removeClass(previousClass).addClass("grid-view");
            }
            else {
                $("#view-option").removeClass(previousClass).addClass(displayType);
            }
        }
    },

    //Change the Product image in Category view, on click of Swatch(Color) Images. - Start 
    SetSwatchImage: function () {
        _debug("bind.SetSwatchImage()");
        $(".SwatchImage").on("click", function () {
            var productImageId = $(this).attr("data-imgid");
            if (productImageId != null && productImageId > 0) {
                $("#" + productImageId).attr('src', $(this).attr("data-mediumsrc"));
                $("#" + productImageId).attr('data-src', $(this).attr("data-mediumsrc"));
            }
        });
    },
    //Change the Product image in Category view, on click of Swatch(Color) Images. - End 
    
    //PRFT Custom Code: Start
    //Show-Hide Facet List on Show More Link Click
    ShowHideFacetList: function ()
    {
        _debug("bind.ShowHideFacetList()");        
        //For Show/Hide category list on Show More Link click: Start
        var initialListItems = 4;        
        $(".list-group-content").each(function () {
            var thisList = $(this);
            var wrap = thisList.find(".filter-list");
            var defHeight = wrap.height();
            if (wrap.find("li").length > initialListItems) {
                $(this).append("<div class='link-text'><a class='show-more' href='javascript:void(0);'>show more</a></div>")
                var readMore = thisList.find(".show-more");
                wrap.find("li").each(function (index) {
                    if (index >= initialListItems) {
                        $(this).hide();
                    }
                });


                readMore.on("click", function (event) {
                    event.preventDefault();

                    var curLiLength = wrap.find("li:visible").length;
                    if (curLiLength == initialListItems) {
                        wrap.find("li").show("fast");
                        $(this).text("show less").removeClass('show-more').addClass("show-less");
                       
                    }
                    else {
                        wrap.find("li").each(function (index) {
                            if (index >= initialListItems) {
                                $(this).hide("fast");
                            }
                        });

                        $(this).text("show more").removeClass("show-less").addClass('show-more');

                    }
                    return false;
                });
            }
        });
    },

    //add product in comparison list and display on category page
    addToCompare: function () {
        _debug("bind.addToCompare()");
        $(".button-compare").on("click", function () {
            var productId = $(this).attr("data-productid");
            var categoryId = $(this).attr("data-categoryid");
            var htmlContainer = "#compareProductList";
            Api.globalAddToCompare(productId, categoryId, function (response) {
                if (response.success == true) {
                    $(htmlContainer).html(response.data.html);
                    $("#compareProductBox").removeAttr("style");
                    $(".remove-compare").unbind("click");
                    $(".remove-compare").on("click", function () { Category.removeProduct($(this).attr("data-productid")); });
                    $("#divAddedForCompare").text(response.message)
                    $("#divAddedForCompare").dialog({
                        title: "Product Compare",
                        resizable: false,
                        modal: true,
                        width: 400,
                        height: 170,
                        buttons: {
                            "Add  More": function () {
                                var htmlContainer = "#compareProductList";
                                $(".ui-dialog-titlebar-close").click();
                            },
                            "View  Comparison": function () {
                                Api.getProductComparison(function (response) {
                                    if (response.status == true) {
                                        $(".ui-dialog-titlebar-close").click();
                                        $("#divAddedForCompare").text(response.message)
                                        $("#divAddedForCompare").dialog({
                                            title: "Product Compare",
                                            resizable: false,
                                            modal: true,
                                            width: 400,
                                            height: 170,
                                            buttons: {
                                                "Add More": function () {
                                                    $(".ui-dialog-titlebar-close").click();
                                                }
                                            },
                                            create: function () { $(this).closest(".ui-dialog").addClass("ui-sm-popup"); }
                                        });
                                    }
                                    else {
                                        window.location.href = "/Product/ViewComparison";
                                    }
                                });
                            }
                        },
                        create: function () { $(this).closest(".ui-dialog").addClass("ui-sm-popup compare-popup-option"); }
                    });
                }
                else if (response.errorCode == 1) {
                    $("#divAddedForCompare").text(response.message)
                    $("#divAddedForCompare").dialog({
                        title: "Product Compare",
                        resizable: false,
                        modal: true,
                        buttons: {
                            "OK": function () {
                                Api.categoryAddToCompare(productId, categoryId, function (response) {
                                    if (response.success == true) {
                                        $(htmlContainer).html(response.data.html);
                                        $("#compareProductBox").removeAttr("style");
                                        $(".remove-compare").unbind("click");
                                        $(".remove-compare").on("click", function () { Category.removeProduct($(this).attr("data-productid")); });
                                        $(".ui-dialog-titlebar-close").click();
                                        $("#divAddedForCompare").text(response.message)
                                        $("#divAddedForCompare").dialog({
                                            title: "Product Compare",
                                            resizable: false,
                                            modal: true,
                                            width: 400,
                                            height: 170,
                                            buttons: {
                                                "Add  More": function () {
                                                    var htmlContainer = "#compareProductList";
                                                    $(".ui-dialog-titlebar-close").click();
                                                    return true;
                                                },
                                                "View  Comparison": function () {
                                                    Api.getProductComparison(function (response) {
                                                        if (response.status == true) {
                                                            $(".ui-dialog-titlebar-close").click();
                                                            $("#divAddedForCompare").text(response.message)
                                                            $("#divAddedForCompare").dialog({
                                                                title: "Product Compare",
                                                                resizable: false,
                                                                modal: true,
                                                                width: 400,
                                                                height: 170,
                                                                buttons: {
                                                                    "Add  More": function () {
                                                                        $(".ui-dialog-titlebar-close").click();
                                                                    }
                                                                },
                                                                create: function () { $(this).closest(".ui-dialog").addClass("ui-sm-popup compare-popup"); }
                                                            });
                                                        }
                                                        else {
                                                            window.location.href = "/Product/ViewComparison";
                                                        }
                                                    });
                                                }
                                            },
                                            create: function () { $(this).closest(".ui-dialog").addClass("ui-lg-popup compare-popup"); }
                                        });
                                    }
                                });
                            },
                            "Cancel": function () {
                                $(".ui-dialog-titlebar-close").click();
                            }
                        },
                    });
                }
                else if (response.errorCode == 2) {
                    $("#divAddedForCompare").text(response.message)
                    $("#divAddedForCompare").dialog({
                        title: "Product Compare",
                        resizable: false,
                        modal: true,
                        width: 400,
                        height: 170,
                        buttons: {
                            "OK": function () {
                                $(".ui-dialog-titlebar-close").click();
                                return true;
                            }
                        },
                        create: function () { $(this).closest(".ui-dialog").addClass("ui-sm-popup compare-popup"); }
                    });
                    
                }
                else if (response.errorCode == 3) {
                    $("#divAddedForCompare").text(response.message)
                    $("#divAddedForCompare").dialog({
                        title: "Product Compare",
                        resizable: false,
                        modal: true,
                        width: 400,
                        height: 170,
                        buttons: {
                            "OK": function () {
                                $(".ui-dialog-titlebar-close").click();
                                return true;
                            }
                        },
                        create: function () { $(this).closest(".ui-dialog").addClass("ui-sm-popup compare-popup"); }
                    });                    
                }
                if (response.data.html == undefined || response.data.html.length < 1) {
                    $("#compareProductList").hide();
                }
                else {
                    $("#compareProductList").show();
                }
            });
        });
    },

    removeProduct: function (productId) {
        _debug("bind.removeProduct()");
        var htmlContainer = "#compareProductList";
        var url = window.location.href.toString().split('/')
        var control = url[3];
        Api.removeProduct(productId, control, function (response) {
            if (htmlContainer) {
                $(htmlContainer).html(response.data.html);
                if (response.count > 0) {
                    $("#compareProductBox").removeAttr("style");
                }
                $(".remove-compare").unbind("click");
                $(".remove-compare").on("click", function () { Category.removeProduct($(this).attr("data-productid")); });
            }
            if (response.data.html == undefined || response.data.html.length < 1) {
                $("#compareProductList").hide();
            }
            else {
                $("#compareProductList").show();
            }
            return true;
        });
    },
    setSelectedCategory: function () {
        if ($('#hdnCategoryName').length && $('#hdnCategoryId').length) {
            Api.setProductCategory($("#hdnCategoryId").val(), $("#hdnCategoryName").val(), function (response) {
            });
        }
    }
}
