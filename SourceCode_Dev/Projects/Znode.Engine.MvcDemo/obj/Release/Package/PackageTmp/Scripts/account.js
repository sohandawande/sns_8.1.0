﻿
// Module variables. Use for local tracking
var addressId = 0;
var Account = {
    // FORGOT PASSWORD VIEW
    init: function () {
        Account.bind.setSelectedTab();
        _debug("Account.ForgotPassword.init()");
        Account.bind.buttonContinue();
        Account.bind.setStateByCountry();
        Account.bind.removeIcon();
        Account.bind.setDefaultState();
        Account.bind.OnStateChange();
        $("#breadcrumb-content").css("display", "none");
    },
    // Passes user credentials and returns security question if valid
    // Final action is handled by POST submit
    toggleSecurityQuestion: function () {
        Api.getUserSecurityQuestion($("#forgot_username").val(), $("#forgot_email").val(), function (response) {
            if (response.success) {
                global.clearAlertMessage();
                $("#display-securityquestion").html(response.data.question);
                $("#dynamic-userid").val(response.data.userid);

                $("#reset-securityquestion").show();
                $("#toggle-securityquestion").hide();
            } else {
                global.setAlertMessage(response.message, "error");
                global.displayAlertMessage();
                scrollToTop();
            }

        });
    },

    ResetCityAndZipCode: function () {
        $("#address_city").val("");
        $("#address_zipcode").val("");
    },
    // Removes item from account wish list
    removeWishlistItem: function (el) {
        var clicked = $(el);
        var wishlistId = clicked.data("id");
        var wishListCount = parseInt($("#wishlistcount").text());

        Api.removeProductFromWishList(wishlistId, function (response) {
            if (response.success) {
                clicked.closest(".wishlist-item").remove();

                $("#wishlistcount").html(response.data.total);
            } else {
                global.setAlertMessage(response.message, "error");
                global.displayAlertMessage();
                scrollToTop();
            }
        });
    },
    bind: {
        // Continue button to submit username/mail for security question
        buttonContinue: function () {
            _debug("bind.buttonContinue()");

            $("#toggle-securityquestion button").on("click", function () {
                Account.toggleSecurityQuestion();
            });
        },
        // Get State by country
        setStateByCountry: function () {
            $(".address_country").on("change", function (e) {
                Account.ResetCityAndZipCode();
                var stateCode = $(this).val();
                Api.getStateByCountryCode(stateCode, function (response) {
                    $('#ShippingAddressModel_StateCode').empty();
                    for (var i = 0; i < response.length; i++) {
                        $("#ShippingAddressModel_StateCode").append("<option value='" + response[i].Code + "'>" + response[i].Name + "</option>");
                    }
                    if (response.length < 1) {
                        $("#ShippingAddressModel_StateCode").append("<option value=''>Please Select</option>");
                    }
                });
            });
            if ($(".address_country").val() == "") {
                $('#ShippingAddressModel_StateCode').append("<option value=''>Please Select</option>");
            }
        },

        setDefaultState: function () {
            var state = $("#hdn_StateCode").val();
            if (state.length < 1) {
                $("#address_country").change();
            }
        },

        OnStateChange: function () {
            $("#ShippingAddressModel_StateCode").on("change", function (e) {
                Account.ResetCityAndZipCode();
            });
        },
        removeIcon: function () {
            _debug("bind.removeIcon()");

            $("#layout-account-wishlist .wishlist-item-remove a").on("click", function (ev) {
                ev.preventDefault();
                Account.removeWishlistItem(this);
            });
        },

        setSelectedTab: function () {
            var selectedTabhref = "/" + $("body").attr("id") + "/" + $("body").attr("class");
            $(".account-dashboard .account-menu a").each(function () {                                    
                //if ($.trim($(this).attr("href")) === $.trim(selectedTabhref) || $.trim($(this).attr("add-value")) === $.trim(selectedTabhref)) {
                if ($.trim($(this).attr("href")) === $.trim(selectedTabhref) || $.trim($(this).attr("add-value")) === $.trim(selectedTabhref) || $.trim($(this).attr("data-href")) === $.trim(selectedTabhref)
                    || $.trim($(this).attr("data-highlight")) === $.trim(selectedTabhref) || $.trim($(this).attr("data-highlightreceipt")) === $.trim(selectedTabhref)) {                   
                    $(this).addClass("selected");
                }
            });
        }
    }
}

