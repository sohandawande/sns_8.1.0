var Home = {
    init: function () {
        Home.bind.giftCardBalance();
        Home.bind.quickView();
        $("#breadcrumb-content").css("display", "none");
        Home.bind.menuHandler();
    },
    bind: {
        // Get balance of provided gift card number
        giftCardBalance: function () {
            $("#giftcard-balance-form").on("submit", function (e) {
                e.preventDefault();
                var form = $(this);
                var gcInput = $("#giftcard_number");
                // Call API for response
                Api.getGiftCardBalance(gcInput.val(), function (response) {
                    if (response.success) {
                        // Generate markup from inline template (within view)
                        var template = $("#tmpl-giftcardbalance").html();
                        var html = Mustache.render(template, response.data);
                        $("#giftcard-balance-result").html(html);
                        gcInput.val("");
                    } else {
                        form.find(".form-error").html('<div class="field-validation-error">' + response.message + '</div>');
                    }

                });
            });
        },
        quickView: function () {
            $(".btn-quick-view").click(function () {
                var productId = $(this).attr("data-value");
                Api.getQuickView(productId, function (responce) {
                    $("#quick-view-content").html(responce);
                    Product.data.isQuickView = true;
                    Product.init();
                    Product.local.updateProductVariations("#dynamic-product-variations", $("#dynamic-product-variations select[id=Quantity]").val())
                });
                var ipad = navigator.userAgent.match(/iPad/i) != null;
                if (ipad) {
                    $('body').css('overflow', 'hidden');
                    $('body').css('position', 'fixed');
                }
            });
        },
        menuHandler: function () {
            var agent = navigator.userAgent.toLowerCase();
            if (agent.indexOf('iphone') >= 0 || agent.indexOf('ipad') >= 0) {
                var node = $("#navbar-collapse-nav li");
                for (var i = 0; i < node.length ; i++) {
                    var item = node[i];
                    if (item.childElementCount > 1) {
                        var link = item.children[0];
                        var catgId = link.innerText.toLowerCase() + i;
                        var href = link.href;
                        link.id = catgId;
                        link.className = "_expandchildmenu";
                        link.href = "javascript:void(0);";
                        $("#" + catgId).attr("data-href", href);
                    }
                }
                Home.bind.HandleParentCategory();
            }
        },
        HandleParentCategory: function () {
            // Determine if we on iPhone or iPad           
            var isiOS = false;
            var agent = navigator.userAgent.toLowerCase();
            if (agent.indexOf('iphone') >= 0 || agent.indexOf('ipad') >= 0) {
                isiOS = true;
            }

            $.fn.doubletap = function (onDoubleTapCallback, onTapCallback, delay) {
                var eventName, action;
                delay = delay == null ? 500 : delay;
                eventName = isiOS == true ? 'touchend' : 'click';

                $(this).bind(eventName, function (event) {
                    var now = new Date().getTime();
                    var lastTouch = $(this).data('lastTouch') || now + 1 /** the first time this will make delta a negative number */;
                    var delta = now - lastTouch;
                    clearTimeout(action);
                    if (delta < 500 && delta > 0) {
                        if (onDoubleTapCallback != null && typeof onDoubleTapCallback == 'function') {
                            onDoubleTapCallback(event);
                        }
                    } else {
                        $(this).data('lastTouch', now);
                        action = setTimeout(function (evt) {
                            if (onTapCallback != null && typeof onTapCallback == 'function') {
                                onTapCallback(evt);
                            }
                            clearTimeout(action);   // clear the timeout
                        }, delay, [event]);
                    }
                    $(this).data('lastTouch', now);
                });
            };

            /** doubletap-dblclick delay (default is 500 ms) */
            var categories = $("#navbar-collapse-nav li ._expandchildmenu");
            for (var i = 0; i < categories.length ; i++) {
                var category = categories[i];
                var categoryId = category.id;
                var actualUrl = $("#" + categoryId).data("href");
                $("#" + categoryId).doubletap(
                   /** doubletap-dblclick callback */
                   function (event) {
                       if (actualUrl != undefined) {
                           window.location = actualUrl;
                       }
                   },
                   /** touch-click callback (touch) */
                   function (event) {
                       /** singletap-dblclick callback */
                   });
            }
        }
    }
}

