
var Cart = {


    // INIT	
    init: function () {
        _debug("cart.init()");

        Cart.bind.removeCartItem();
        Cart.bind.updateQuantity();
        Cart.bind.updateShipTo();
        Cart.bind.promoCode();

        //Removes all item from cart - Start 
        Cart.bind.removeAllCartItem();
        //Removes all item from cart - End 

        Cart.bind.submitMultipleShipping();
        Cart.bind.calculateShipping();
        Cart.bind.viewMultiAddressCart();
    },


    // BINDINGS
    bind: {
        // Removes item from cart
        removeCartItem: function () {
            _debug("bind.removeCartItem()");
            $("#layout-cart .cart-item-remove").on("click", function (e) {
                e.preventDefault();
                $(this).closest("form").submit();
            });
        },

        // Updates quantity of item 
        updateQuantity: function () {
            _debug("bind.updateQuantity()");
            $("#layout-cart .cart-item-total select.shipTo").on("change", function (ev) {
                $(this).closest("form").submit();
            });
        },

        // Updates quantity of item 
        updateShipTo: function () {
            $("#layout-cart .cart-item-quantity select.cartQuantity").on("change", function (ev) {
                $(this).closest("form").submit();
            });
        },

        // Updates quantity of item 
        submitMultipleShipping: function () {
            $("#layout-cart #submitMultipleShipping").on("click", function (ev) {
                var DataList = new Array;
                $("#layout-cart select.shipTo option:selected").each(function (e) {
                    DataList.push({
                        addressid: $(this).val(),
                        productid: $(this).parent().next().next().val(),
                        quantity: 1,
                        SKU: $(this).parent().next().next().next().val(),
                        AddOnValueIds: $(this).parent().next().next().next().next().next().val(),
                        AddOnValuesCustomText: $(this).parent().next().next().next().next().next().next().val()
                    });
                });

                var jArray = JSON.stringify(DataList);

                $.ajax({
                    url: "/Checkout/ContinueCheckout/",
                    type: "get",
                    dataType: "json",
                    data: { "multipleAddress": jArray },
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        window.location.href = "/Checkout/ShippingMethods?returnUrl='multipleshipto'";
                    },
                    error: function (msg) {
                        window.location.href = "/Checkout/ShippingMethods?returnUrl='multipleshipto'";
                    }
                });

            });
        },

        // Updates quantity of item 
        calculateShipping: function () {
            //shippingOptionList
            $(".multipleShippingOption").on("change", function (ev) {
                $(this).closest("form").submit();
            });
        },

        viewMultiAddressCart: function () {
            //shippingOptionList
            $(".view-multi-address-cart").on("click", function (ev) {
                $(this).closest("form").submit();
            });
        },


        promoCode: function () {
            _debug("bind.promoCode()");
            var couponCode = Cart.bind.getQueryString("coupon");
            if (couponCode != null && couponCode != undefined && couponCode != "") {
                Api.isCouponUrlEnabled(couponCode, function (response) {
                    if (response.couponUrlIsEnabled) {
                        Cart.bind.applyPromoCode(couponCode);
                    }
                });
            }
            $("#cart-promocode-container").on("submit", function (e) {
                e.preventDefault();
                Cart.bind.applyPromoCode($("#promocode").val());
            });
        },

        applyPromoCode: function (couponCode) {
            var template = $("#tmpl-promocodes").html();

            Api.useCouponCode(couponCode, function (response) {
                var html = "";
                if (!response.isMultipleCoupon) {
                    if (response.success) {
                        response.data.class = "success-msg";
                        response.data.coupon = response.data.promoCode[0].Coupon;
                        response.data.message = response.data.promoCode[0].CouponMessage;
                    }
                    else {
                        $("#cart-promocode-container p.error-msg").remove();
                        response.data.class = "error-msg";
                        response.data.coupon = couponCode;
                        response.data.message = (response.data.coupon === "") ? response.data.promoCode[response.data.promoCode.length - 1].CouponMessage : "Invalid coupon code ";
                    }
                    html = Mustache.render(template, response.data);

                    if (response.success) {
                        $(".cart-promocode-status .success-msg").css("text-decoration", "line-through");
                        $(".cart-promocode-status").prepend(html);
                        $("#cart-promocode-container input").val("");
                        $("#dynamic-discount-amount").html(response.data.discountamount);
                        $("#dynamic-order-total").find("h2").html(response.data.ordertotal);
                        $("#dynamic-tax-amount").html(response.data.taxamount);
                        $("#dynamic-order-subtotal").html(response.data.subtotal);
                        $("#dynamic-cart-order-total").html(response.data.ordertotal);
                    } else {
                        $(".cart-promocode-status").prepend(html);
                    }

                }
                else {
                    for (var dataIndex = 0; dataIndex < response.data.promoCode.length; dataIndex++) {
                        response.data.message = response.data.promoCode[dataIndex].CouponMessage;
                        if (response.data.promoCode[dataIndex].CouponValid === true) {
                            response.data.class = "success-msg";
                        }
                        else {
                            response.data.class = "error-msg";
                        }
                        response.data.coupon = response.data.promoCode[dataIndex].Coupon;
                        html = html + Mustache.render(template, response.data);
                    }
                    $(".cart-promocode-status").text("");
                    $(".cart-promocode-status").prepend(html);
                    $("#cart-promocode-container input").val("");
                    $("#dynamic-discount-amount").html(response.data.discountamount);
                    $("#dynamic-order-total").find("h2").html(response.data.ordertotal);
                    $("#dynamic-tax-amount").html(response.data.taxamount);
                    $("#dynamic-order-subtotal").html(response.data.subtotal);
                    $("#dynamic-cart-order-total").html(response.data.ordertotal);
                }
            });
        },

        removeAllCartItem: function () {
            _debug("bind.removeAllCartItem()");
            $("#layout-cart .cart-item-remove-all").on("click", function (e) {
                e.preventDefault();
                $(this).closest("form").submit();
            });
        },

        getQueryString: function (field) {
            var href = window.location.href;
            var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
            var string = reg.exec(href);
            return string ? string[1] : null;
        }
    },
    multiShippingCart: function () {
        $.ajax({
            url: "/Checkout/SetSelectedIndex/",
            type: "get",
            dataType: "json",
            data: {},
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data != null) {
                    var jsonData = data;
                    for (var i = 0; i < jsonData.length; i++) {
                        if ($("#layout-cart select.shipTo:eq(" + i + ")").next().next().val() === jsonData[i].productid) {
                            $("#layout-cart select.shipTo:eq(" + i + ")").val(jsonData[i].addressid);
                        }
                    }
                }
            },
            error: function (msg) {
                alert("error");
            }
        });
    }
};

// END module