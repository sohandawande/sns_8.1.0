// Global debug
var enableDebug = true;

function _debug(s) {
    if (enableDebug) {
        if (typeof console === "undefined") {
            //console = { log: function(s){ alert(s); } }
        } else {
            console.log(s);
        }
    }
}


// In-window scrolling, uses jQuery
function scrollToTop(speed, selector) {
    if (!speed) { speed = 300; }

    if (!selector) { selector = "body"; }

    $("html, body").animate({ scrollTop: $(selector).offset().top }, speed);
}


// jQuery plugin for disabled attribute on element
(function ($) {
    $.fn.toggleDisabled = function () {
        return this.each(function () {
            var $this = $(this);
            if ($this.attr('disabled')) {
                $this.removeAttr('disabled');
            } else {
                $this.attr('disabled', 'disabled');
            }
        });
    };
})(jQuery);

function Removetildslashfromstring(str, char) {
    var notildslash = "";
    var newstr = str.split(char);
    for (var i = 0; i < newstr.length; i++) {
        notildslash += newstr[i];
    }
    return notildslash;
}


function BlockHtmlTagForTextBox() {
    /* validated all text box in project*/
    $(':input').not('.AllowHtml').bind("paste keypress change", function (e) {
        if ($(this).val().indexOf("~") != -1) {
            var _inputValue = Removetildslashfromstring($(this).val(), "~");
            $(this).val(_inputValue);
        }
        if ($(this).val().indexOf("<") != -1) {
            var _inputValue = Removetildslashfromstring($(this).val(), "<");
            $(this).val(_inputValue);
        }
        if ($(this).val().indexOf(">") != -1) {
            var _inputValue = Removetildslashfromstring($(this).val(), ">");
            $(this).val(_inputValue);
        }
        /*new validation*/
        var key = [e.keyCode || e.which];
        if (key[0] != undefined) {
            var keychar = String.fromCharCode([e.keyCode || e.which]).toLowerCase();
            if ((key == null) || (key == 0) || (key == 126) || (key == 60) || (key == 62)) {
                return false;
            }
        }
    });
}

/*code for Overlay*/
function overlayforwaitnone() { $("#backgroundddivWait").hide(); } function overlayforwaitblock() { $("#backgroundddivWait").show(); }
/*code for Overlay*/

$(document).ready(function () {
    BlockHtmlTagForTextBox();
    overlayforwaitnone();
    //Check for the browser compatability for the TLS1.2
    var isBrowserAllowed = true;
    $("#divUpgradeBrowser").hide();
    $("#divUpgradeBrowser").dialog({
        width: 400,
        height: 250,
        modal: true,
        autoOpen: false,
        closeOnEscape: false,
        draggable: false,
        resizable: false,
        scrolllock: false,
        zIndex:900,
        dialogClass: "UpgradeBrowserPopup"
    });
    
    if ((bowser.msie && bowser.version <= 10) || (bowser.firefox && bowser.version <= 26) || (bowser.chrome && bowser.version <= 37) || (bowser.safari && bowser.version <=6) || (bowser.android && bowser.version <= 4.3) || (bowser.opera && bowser.version <= 5)) {
        isBrowserAllowed = false
        $("#divUpgradeBrowser").dialog("open");
    }
    
    if (isBrowserAllowed == true && $("#divUpgradeBrowser").dialog('isOpen') === true) {
        $("#divUpgradeBrowser").dialog("close");
        $("#divUpgradeBrowser").hide();
    }

});

$(".quick-view-popup .zf-close").on("click", function (){
    $(".modal-backdrop").hide()
},
$("#close-quick-view-popup-ipad").on("click", function () {
    var ipad = navigator.userAgent.match(/iPad/i) != null;
    if (ipad) {
        $('body').css('overflow', 'auto');
        $('body').css('position', 'relative');
    }
}));