﻿//This is an example file how to extend core framework js files.
//Using this feature developer can able to write its own function or logic in its own custom js files and access it with our existing Js Object.


//Extending api.js 
//This is an example for Api js object extession. 
//This is only callback function parameter example user can pass multiple parameters also. 
Api["ExtendFunction1"] = function (callback) {
    //Logic implemantation. 
};


//With multi parameters
Api["ExtendFunction2"] = function (parm1, parm2, callback) {
    //Logic implemantation. 
};


//With ajax request
Api["ExtendFunction3"] = function (parm1, parm2, callback) {
    //Logic implemantation. 
    //Example
    Api.ajax.request("/Cart/TestCall", "get", { "testPrm1": parm1, "testPrm2": parm2 }, callback, "html");
};


//Extending product.js 
//This is only callback function parameter example user can pass multiple parameters also. 
Product["ProductExtendFunction1"] = function (callback) {
    //Logic implemantation. 
};


//With multi parameters
Product["ProductExtendFunction2"] = function (parm1, parm2, callback) {
    //Logic implemantation. 
};
