﻿var Caserequest = {
    init: function () {
        Caserequest.ValidationForContactUsForm();
        Caserequest.ValidationForCustomerFeedbackForm();
        Caserequest.setStateByCountry();//PRFT Custom Code
    },
    ValidationForContactUsForm: function () {
        $("#contact-us").click(function () {

            var flag = true;
            var firstName = $("#valFirstName").val();
            if (firstName.length < 1) {
                $("#valFirstNameErr").html("First name is required.");
                flag = false;
            }
            else {
                $("#valFirstNameErr").html("");
            }

            var lastName = $("#valLastName").val();
            if (lastName.length < 1) {
                $("#valLastNameErr").html("Last name is required.");
                flag = false;
            }
            else {
                $("#valLastNameErr").html("");
            }

            var phoneNum = $("#valPhoneNum").val();
            var regexPhoneNum = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
            if(!(phoneNum.match(regexPhoneNum)) &&(!phoneNum.length<1))
            {
                $("#valPhoneNumErr").html("Enter valid phone number.");
                flag = false;
            }
            else {
                $("#valPhoneNumErr").html("");
            }

            var email = $("#valEmail").val();
            if (email.length < 1) {
                $("#valEmailErr").html("Email ID is required.");
                flag = false;
            }
            else {
                $("#valEmailErr").html("");
                var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                if (!regex.test(email)) {
                    $("#valEmailErr").html("Enter valid email address.");
                    flag = false;
                }
            }
            //PRFT Custom Code : Start
            //var SalesRep = $("#valCaseRequestModel_SalesRepId").val() === "";
            //if (SalesRep) {
            //    $("#valCaseRequestModel_SalesRepIdErr").html("Sales representative is required.");
            //    flag = false;
            //}
            //else {
            //    $("#valCaseRequestModel_SalesRepIdErr").html("");
            //}
            var country = $("#caseRequestModel_CountryCode").val() === "";
            if (country != undefined && country) {
                $("#caseRequestModel_CountryCodeRepIdErr").html("Country is required.");
                flag = false;
            }
            else {
                $("#caseRequestModel_CountryCodeRepIdErr").html("");
            }

            var state = $("#caseRequestModel_StateCode").val() === "";
            if (state != undefined && state) {
                $("#caseRequestModel_StateCodeErr").html("State is required.");
                flag = false;
            }
            else {
                $("#caseRequestModel_StateCodeErr").html("");
            }

            var city = $("#caseRequest_city").val();
            if (city != undefined && city.length < 1) {
                $("#caseRequest_cityErr").html("City is required.");
                flag = false;
            }
            else {
                $("#caseRequest_cityErr").html("");
            }

            var dueDate = $("#caseRequestModel_DueDate").val();
            if (dueDate != undefined && dueDate.length < 1) {
                $("#caseRequestModel_DueDateErr").html("Due date is required.");
                flag = false;
            }
            else {
                $("#caseRequestModel_DueDateErr").html("");
                try{
                    var today = new Date();
                    var todayDateOnly = new Date(today.getFullYear(), today.getMonth(), today.getDate()); //This will write a Date with time set to 00:00:00 so you kind of have date only
                    var enteredDueDate = new Date(dueDate);

                    if (enteredDueDate < todayDateOnly) {
                        $("#caseRequestModel_DueDateErr").html("Due date should not be past date.");
                        flag = false;
                    }
                } catch (err) {

                };
            }

            //PRFT Custom Code : End

            if (flag)
                return true;
            else
                return false;
        });
    },

    ValidationForCustomerFeedbackForm: function () {
        $("#customer-feedback").click(function () {
            var flag = true;
            var email = $("#valEmailAddress").val();
            if (email.length < 1) {
                $("#valEmailAddressErr").html("Email ID is required.");
                flag = false;
            }
            else {
                $("#valEmailAddressErr").html("");
                var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                if (!regex.test(email)) {
                    $("#valEmailAddressErr").html("Enter valid Email ID");
                    flag = false;
                }
            }
            if (flag)
                return true;
            else
                return false;
        });
    },
    //PRFT Custom Code : Start
    setStateByCountry: function () {
        $(".caserequest_country").on("change", function (e) {
            var countryId = $(this).attr('id');
            var stateId = countryId.replace("CountryCode", "StateCode");
            var countryCode = $(this).val();
            Api.getStateByCountryCode(countryCode, function (response) {
                $('#' + stateId).empty();
                for (var i = 0; i < response.length; i++) {
                    $("#" + stateId).append("<option value='" + response[i].Code + "'>" + response[i].Name + "</option>");
                }
                if (response.length < 1) {
                    $("#" + stateId).append("<option value=''>Please Select</option>");
                }
            });
        });
        if ($(".caserequest_country").val() == "") {
            $('#CaseRequestModel_StateCode').empty();
            $('#CaseRequestModel_StateCode').append("<option value=''>Please Select</option>");
            //$('#CaseRequestModel_SalesRepId').append("<option value='0' selected>Please Select</option>");
        }
        //PRFT Custom Code : End
    }
}
