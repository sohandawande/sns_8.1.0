﻿var QuickOrder = {
    Initialization: function () {
        QuickOrder.CloseQuickOrderpopup();
        QuickOrder.SetProperties();
        QuickOrder.Validation();
        QuickOrder.ShowHideQuickOrderPopUp();
        var agent = navigator.userAgent.toLowerCase();
        if (agent.indexOf('ipad') >= 0) {
            QuickOrder.AutocompleteSkuForDevice();
        }
        else {
            QuickOrder.AutocompleteSku();
        }
        QuickOrder.RemoveValidationMessage();
    },

    ShowHideQuickOrderPopUp: function () {
        var ipad = navigator.userAgent.match(/iPad/i) != null;
        if (ipad) {            
            $(".quickordercontainer").on('mouseenter touch', function () {
                $(".ui-autocomplete").css('display');
                $(".divQuickOrder").show();
            });
            $(".quickordercontainer").on('mouseleave touch', function () {
                if ((!$(".txtQuickOrderSku").is(":focus")) && (!$(".txtQuickOrderQuantity").is(":focus")) && ($(".txtQuickOrderSku").val() == "")) {
                    $(".divQuickOrder").hide();
                }
            });
        }        
        else {            
            var mq = window.matchMedia("(min-width: 768px)");
            if (mq.matches) {
                $(".quickordercontainer").on('mouseenter touch', function () {
                    $(".ui-autocomplete").css('display');
                    $(".divQuickOrder").show();
                });
                $(".quickordercontainer").on('mouseleave touch', function () {
                    if ((!$(".txtQuickOrderSku").is(":focus")) && (!$(".txtQuickOrderQuantity").is(":focus")) && ($(".txtQuickOrderSku").val() == "")) {
                        $(".divQuickOrder").hide();
                    }
                });
            }
            else {
                $(".quickordercontainer").on('mouseenter touch', function () {                    
                    $(".divQuickOrder").hide();
                });
                $(".quickordercontainer").on('mouseleave touch', function () {
                    if ((!$(".txtQuickOrderSku").is(":focus")) && (!$(".txtQuickOrderQuantity").is(":focus")) && ($(".txtQuickOrderSku").val() == "")) {
                        $(".divQuickOrder").hide();
                    }
                });
            }
        }
    },
   
    Validation: function () {
        $(".quickOrderAddToCart").click(function () {
            var quantity = parseInt($('.txtQuickOrderQuantity').val());
            var track_inventory = $('#hdnQuickOrderTrackInventoryInd').val();
            var back_order = $('#hdnQuickOrderAllowBackOrder').val();

            if ($('.txtQuickOrderSku').val() != $('#hdnQuickOrderSku').val()) {
                $('#inventorymsg').html("Please enter valid sku.");
                return false;
            }

            if (parseInt($('.txtQuickOrderQuantity').val() % 1) != 0 || parseInt($('.txtQuickOrderQuantity').val()) <= 0) {
                $('#inventorymsg').html("Enter Valid Quantity");
                return false;
            }

            if (isNaN($('.txtQuickOrderQuantity').val()) || $('.txtQuickOrderQuantity').val() == "") {
                $('#inventorymsg').html("Enter Whole Number");
                return false;
            }

            if ($('#hdnQuickOrderCallForPricing').val() == "true") {
                $('#inventorymsg').html("Call for pricing");
                return false;
            }

            if ((track_inventory == "true" && back_order == "false")) {
                if (parseInt($("#hdnQuickOrderQuantityOnHand").val()) <= 0) {
                    $('#inventorymsg').html($('#hdnQuickOrderOutOfStockMessage').val());
                    return false;
                }
            }

            if (parseInt($('#hdnQuickOrderMaxQty').val()) < quantity + parseInt($("#hdnQuickOrderCartQuantity").val())) {
                $('#inventorymsg').html('Selected quantity exceeds product maximum given current Shopping Cart quantities.');
                return false;
            }

            if (parseInt($('#hdnQuickOrderMinQty').val()) > quantity) {
                $('#inventorymsg').html('Selected quantity is less than minimum specified quantity.');
                return false;
            }

            if ((track_inventory == "true" && back_order == "false")) {
                if (parseInt($('#hdnQuickOrderQuantityOnHand').val()) == parseInt($("#hdnQuickOrderCartQuantity").val())) {
                    $('#inventorymsg').html($('#hdnQuickOrderOutOfStockMessage').val());
                    return false;
                }
            }

            if ((track_inventory == "true" && back_order == "false")) {
                if (quantity + parseInt($("#hdnQuickOrderCartQuantity").val()) > parseInt($('#hdnQuickOrderQuantityOnHand').val())) {
                    $('#inventorymsg').html("Only " + (parseInt($('#hdnQuickOrderQuantityOnHand').val()) - parseInt($("#hdnQuickOrderCartQuantity").val())) + " quantities are available for Add to cart/Shipping");
                    return false;
                }
            }

            if (QuickOrder.CheckInventory($('#hdnQuickOrderAddOnQuantityOnHand').val(), quantity + parseInt($("#hdnQuickOrderCartQuantity").val()))) {
                $('#inventorymsg').html("Add-On is out of stock");
                return false;
            }

            if (QuickOrder.CheckInventoryForBundle($('#hdnQuickOrderBundleQuantityOnHand').val(), quantity, $("#hdnQuickOrderBundleTrackInventoryInd").val(), $("#hdnQuickOrderBundleAllowBackOrder").val())) {
                $('#inventorymsg').html("Bundle product is out of stock");
                return false;
            }

            return true;
        })
    },
    AutocompleteSkuForDevice: function () {
    $(".txtQuickOrderSku").on("keyup", function (e) {
        $("#ui-id-1").show();
        var txt = $(".txtQuickOrderSku").val();
        Api.GetProductListBySKU(txt, function (data) {
            if ($("#ui-id-1").length < 1) {
                $(".txtQuickOrderSku").addClass("ui-autocomplete-input");
                $(".txtQuickOrderSku").attr("autocomplete", "off");
                $(".txtQuickOrderSku").attr("autocorrect", "off");
                $(".txtQuickOrderSku").attr("autocapitalize", "off");
                $(".txtQuickOrderSku").attr("spellcheck", "off");
                $("body").append('<ul class="ui-autocomplete ui-front scroll-default ui-menu ui-widget ui-widget-content quick-order-autocomplete" id="ui-id-1" tabindex="0" style="display: block; overflow: hidden; -webkit-tap-highlight-color: rgba(0, 0, 0, 0); -webkit-touch-callout: none; outline: none; cursor: -webkit-grab; width: 283px; top: 150px; left: 419.625px;"></ul>');
            }
            $("#ui-id-1").html('');
            var productContaits = "";
            for (var i = 0; i < data.length; i++) {
                var index = 2 + parseInt(i);
                var id = "ui-id-" + index;
                var hdnDetails = QuickOrder.BindHinddenData(data[i], index);
                if (i == 0) {
                    productContaits = '<li class="ui-menu-item" id=' + id + ' tabindex="-1" onclick="QuickOrder.SelectSku(this);">' + hdnDetails + data[i].AutoCompleteLabel + '</li>';
                }
                else {
                    productContaits += '<li class="ui-menu-item" id=' + id + ' tabindex="-1" onclick="QuickOrder.SelectSku(this);">' + hdnDetails + data[i].AutoCompleteLabel + '</li>';
                }
            }

            $("#ui-id-1").append(productContaits);
            $('.ui-autocomplete').off('menufocus hover mouseover mouseenter');
        });
    });
},
BindHinddenData: function (data, index) {
    var details = '<input type="hidden" value="' + data.Sku + '" id="hdnqosku' + index + '" class="hdnqosku">';
    details += '<input type="hidden" value=' + data.ProductId + ' id="hdnqoProductId' + index + '"  class="hdnqoProductId">';
    details += '<input type="hidden" value=' + data.BundleItemIds + ' id="hdnqoBundleItemsIds' + index + '" class="hdnqoBundleItemsIds">';
    details += '<input type="hidden" value="' + data.Price + '" id="hdnqoPrice' + index + '" class="hdnqoPrice">';
    details += '<input type="hidden" value=' + data.AddonValueIds + ' id="hdnqoAddOnValueIds' + index + '" class="hdnqoAddOnValueIds">';
    details += '<input type="hidden" value="' + data.ProductName + '" id="hdnqoProductName' + index + '" class="hdnqoProductName">';
    details += '<input type="hidden" value="' + data.Quantity + '" id="hdnqoQuantity' + index + '" class="hdnqoQuantity">';
    details += '<input type="hidden" value="' + data.QuantityOnHand + '" id="hdnqoQuantityOnHand' + index + '" class="hdnqoQuantityOnHand">';
    details += '<input type="hidden" value="' + data.InventoryMessage + '" id="hdnqoInventoryMessage' + index + '" class="hdnqoInventoryMessage">';
    details += '<input type="hidden" value="' + data.InStockMessage + '" id="hdnqoInStockMessage' + index + '" class="hdnqoInStockMessage">';
    details += '<input type="hidden" value="' + data.OutOfStockMessage + '" id="hdnqoOutOfStockMessage' + index + '" class="hdnqoOutOfStockMessage">';
    details += '<input type="hidden" value="' + data.MinQty + '" id="hdnqoMinQty' + index + '" class="hdnqoMinQty">';
    details += '<input type="hidden" value="' + data.MaxQty + '" id="hdnqoMaxQty' + index + '" class="hdnqoMaxQty">';
    details += '<input type="hidden" value="' + data.CallForPricing + '" id="hdnqoCallForPricing' + index + '" class="hdnqoCallForPricing">';
    details += '<input type="hidden" value="' + data.BundleQuantityOnHand + '" id="hdnqoBundleQuantityOnHand' + index + '" class="hdnqoBundleQuantityOnHand">';
    details += '<input type="hidden" value="' + data.AddOnQuantityOnHand + '" id="hdnqoAddOnQuantityOnHand' + index + '" class="hdnqoAddOnQuantityOnHand">';
    details += '<input type="hidden" value="' + data.AllowBackOrder + '" id="hdnqoAllowBackOrder' + index + '" class="hdnqoAllowBackOrder">';
    details += '<input type="hidden" value="' + data.TrackInventoryInd + '" id="hdnqoTrackInventoryInd' + index + '" class="hdnqoTrackInventoryInd">';
    details += '<input type="hidden" value="' + data.BundleAllowBackOrder + '" id="hdnqoBundleAllowBackOrder' + index + '" class="hdnqoBundleAllowBackOrder">';
    details += '<input type="hidden" value="' + data.BundleTrackInventoryInd + '" id="hdnqoBundleTrackInventoryInd' + index + '" class="hdnqoBundleTrackInventoryInd">';
    details += '<input type="hidden" value="' + data.SeoPageName + '" id="hdnqoOrderSeoPageName' + index + '" class="hdnqoOrderSeoPageName">';
    return details;
},
SelectSku: function (obj) {
    var li = obj.id;
    var sku = $("#" + li + " .hdnqosku").val();// $("#" + li + " input").val();
    $('.txtQuickOrderSku').val(sku);
    $("#ui-id-1").hide();
    $('#inventorymsg').html("");
    $('.quickOrderAddToCart').removeAttr('disabled');
    var productId = $("#" + li + " .hdnqoProductId").val();
    var bundleId = $("#" + li + " .hdnqoBundleItemsIds").val();
    if (bundleId == "null") {
        bundleId = null;
    }
    var price = $("#" + li + " .hdnqoPrice").val();
    var addonId = $("#" + li + " .hdnqoAddOnValueIds").val();
    if (addonId == "null") {
        addonId = null;
    }
    var productname = $("#" + li + " .hdnqoProductName").val();
    var qty = $("#" + li + " .hdnqoQuantity").val()
    var qtyonhand = $("#" + li + " .hdnqoQuantityOnHand").val();
    var inventorymsg = $("#" + li + " .hdnqoInventoryMessage").val();
    var instock = $("#" + li + " .hdnqoInStockMessage").val();
    var outofstock = $("#" + li + " .hdnqoOutOfStockMessage").val();
    var minqty = $("#" + li + " .hdnqoMinQty").val();
    var maxqty = $("#" + li + " .hdnqoMaxQty").val();
    var callforprice = $("#" + li + " .hdnqoCallForPricing").val();
    var bundleqty = $("#" + li + " .hdnqoBundleQuantityOnHand").val();
    var addonqty = $("#" + li + " .hdnqoAddOnQuantityOnHand").val();
    var allowbackorder = $("#" + li + " .hdnqoAllowBackOrder").val();
    var trackinv = $("#" + li + " .hdnqoTrackInventoryInd").val();
    var bundleallowbackorder = $("#" + li + " .hdnqoBundleAllowBackOrder").val();
    var bundletrackinventoryind = $("#" + li + " .hdnqoBundleTrackInventoryInd").val();
    var orderseopagename = $("#" + li + " .hdnqoOrderSeoPageName").val();
    if (bundleqty = "null") {
        bundleqty = null;
    }
    if (addonqty = "null") {
        addonqty = null;
    }
    if (bundleallowbackorder = "null") {
        bundleallowbackorder = null;
    }
    if (bundletrackinventoryind = "null") {
        bundletrackinventoryind = null;
    }
    $('#hdnQuickOrderProductId').val(productId);
    $("#hdnQuickOrderBundleItemsIds").val(bundleId);
    $('#hdnQuickOrderPrice').val(price);
    $("#hdnQuickOrderAddOnValueIds").val(addonId);
    $('#hdnQuickOrderSku').val(sku);
    $('#hdnQuickOrderProductName').val(productname);
    $('#hdnQuickOrderCartQuantity').val(qty);
    $('#hdnQuickOrderQuantityOnHand').val(qtyonhand);
    $('#hdnQuickOrderInventoryMessage').val(inventorymsg);
    $('#hdnQuickOrderInStockMessage').val(instock);
    $('#hdnQuickOrderOutOfStockMessage').val(outofstock);
    $('#hdnQuickOrderMinQty').val(minqty);
    $('#hdnQuickOrderMaxQty').val(maxqty);
    $('#hdnQuickOrderCallForPricing').val(callforprice);
    $('#hdnQuickOrderBundleQuantityOnHand').val(bundleqty);
    $('#hdnQuickOrderAddOnQuantityOnHand').val(addonqty);
    $('#hdnQuickOrderAllowBackOrder').val(allowbackorder);
    $('#hdnQuickOrderTrackInventoryInd').val(trackinv);
    $('#hdnQuickOrderBundleAllowBackOrder').val(bundleallowbackorder);
    $('#hdnQuickOrderBundleTrackInventoryInd').val(bundletrackinventoryind);
    $('#hdnQuickOrderSeoPageName').val(orderseopagename);
    $('#inventorymsg').html("");
    $('.quickOrderAddToCart').removeAttr('disabled');
    return false;
},
    AutocompleteSku: function () {
        $(".txtQuickOrderSku").autocomplete({
            source: function (request, response) {
                try {
                    Api.GetProductListBySKU(request.term, function (data) {
                        response($.map(data, function (item) {
                            return {
                                label: item.AutoCompleteLabel,
                                value: item.Sku,
                                productId: item.ProductId,
                                price: item.Price,
                                addonValueIds: item.AddonValueIds,
                                bundleItemsIds: item.BundleItemIds,
                                sku: item.Sku,
                                productName: item.ProductName,
                                cartQuantity: item.CartQuantity,
                                quantityOnHand: item.QuantityOnHand,
                                inStockMessage: item.InStockMessage,
                                outOfStockMessage: item.OutOfStockMessage,
                                inventoryMessage: item.InventoryMessage,
                                callForPricing: item.CallForPricing,
                                minQty: item.MinQty,
                                maxQty: item.MaxQty,
                                bundleQuantityOnHand: item.BundleQuantityOnHand,
                                addOnQuantityOnHand: item.AddOnQuantityOnHand,
                                allowBackOrder: item.AllowBackOrder,
                                trackInventoryInd: item.TrackInventoryInd,
                                bundleAllowBackOrder: item.BundleAllowBackOrder,
                                bundleTrackInventoryInd: item.BundleTrackInventoryInd,
                                SeoPageName: item.SeoPageName,
                            }
                        }));
                    });
                } catch (err) {
                }
            },
            html: true,
            select: function (event, ui) {
                $('.txtQuickOrderSku').val(ui.item.sku);
                $('#hdnQuickOrderProductId').val(ui.item.productId);
                $("#hdnQuickOrderBundleItemsIds").val(ui.item.bundleItemsIds);
                $('#hdnQuickOrderPrice').val(ui.item.price);
                $("#hdnQuickOrderAddOnValueIds").val(ui.item.addonValueIds);
                $('#hdnQuickOrderSku').val(ui.item.sku);
                $('#hdnQuickOrderProductName').val(ui.item.productName);
                $('#hdnQuickOrderCartQuantity').val(ui.item.cartQuantity);
                $('#hdnQuickOrderQuantityOnHand').val(ui.item.quantityOnHand);
                $('#hdnQuickOrderInventoryMessage').val(ui.item.inventoryMessage);
                $('#hdnQuickOrderInStockMessage').val(ui.item.inStockMessage);
                $('#hdnQuickOrderOutOfStockMessage').val(ui.item.outOfStockMessage);
                $('#hdnQuickOrderMinQty').val(ui.item.minQty);
                $('#hdnQuickOrderMaxQty').val(ui.item.maxQty);
                $('#hdnQuickOrderCallForPricing').val(ui.item.callForPricing);
                $('#hdnQuickOrderBundleQuantityOnHand').val(ui.item.bundleQuantityOnHand);
                $('#hdnQuickOrderAddOnQuantityOnHand').val(ui.item.addOnQuantityOnHand);
                $('#hdnQuickOrderAllowBackOrder').val(ui.item.allowBackOrder);
                $('#hdnQuickOrderTrackInventoryInd').val(ui.item.trackInventoryInd);
                $('#hdnQuickOrderBundleAllowBackOrder').val(ui.item.bundleAllowBackOrder);
                $('#hdnQuickOrderBundleTrackInventoryInd').val(ui.item.bundleTrackInventoryInd);
                $('#hdnQuickOrderSeoPageName').val(ui.item.SeoPageName);
                $('#inventorymsg').html("");
                $('.quickOrderAddToCart').removeAttr('disabled');
                return false;
            },
            open: function () {
                $(this).data("ui-autocomplete").menu.element.addClass("quick-order-autocomplete");
            }
        });
    },

    SetProperties: function () {
        $('.quickOrderAddToCart').attr('disabled', 'disabled');
        $('#txtQuickOrderQuantity').attr('Value', 1);
    },

    CloseQuickOrderpopup: function () {
        $('.close-quick-order-popup').bind('click', function () {
            $(".divQuickOrder").hide();
            $('.quickOrderAddToCart').attr('disabled', 'disabled');
            $('.txtQuickOrderSku').val("");
            $('.txtQuickOrderQuantity').val("1");
            $('#inventorymsg').html("");
        });
    },

    CheckInventory: function (quantitiesOnHand, quantity) {
        if (quantitiesOnHand != undefined && quantitiesOnHand != null && quantitiesOnHand.length > 0) {
            var quantityArray = quantitiesOnHand.split(',');
            for (var quantityIndex = 0; quantityIndex < quantityArray.length; quantityIndex++) {
                if ((quantityArray[quantityIndex] != "") && (parseInt(quantityArray[quantityIndex]) < quantity)) {
                    return true;
                }
            }
        }
        return false;
    },

    CheckInventoryForBundle: function (quantitiesOnHand, quantity, track_inventory_bundle, allow_back_order) {
        if (quantitiesOnHand != undefined && quantitiesOnHand != null && quantitiesOnHand.length > 0) {
            var quantityArray = quantitiesOnHand.split(',');
            var track_inventory_bundle_array = (track_inventory_bundle != undefined && track_inventory_bundle != null && track_inventory_bundle.length > 0) ? track_inventory_bundle.split(',') : [];
            var allow_back_order_array = (allow_back_order != undefined && allow_back_order != null && allow_back_order.length > 0) ? allow_back_order.split(',') : [];
            for (var quantityIndex = 0; quantityIndex < quantityArray.length; quantityIndex++) {
                if (track_inventory_bundle_array[quantityIndex] == "1" && allow_back_order_array[quantityIndex] == "0") {
                    if ((quantityArray[quantityIndex] != "") && (parseInt(quantityArray[quantityIndex]) < quantity)) {
                        return true;
                    }
                }
            }
        }
        return false;
    },

    RemoveValidationMessage: function () {
        $(".txtQuickOrderSku").on("focusout", function () {
            if ($(".txtQuickOrderSku").val() == "") {
                $('#inventorymsg').html("");
                $('.quickOrderAddToCart').attr('disabled', 'disabled');
                $('.txtQuickOrderQuantity').val("1");
            }
        });
    }
}