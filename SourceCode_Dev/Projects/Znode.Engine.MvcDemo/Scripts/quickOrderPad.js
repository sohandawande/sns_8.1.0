﻿var QuickOrderPad = {
    Initialization: function () {
        $('#btnQuickOrderPad').attr('disabled', 'disabled');
        QuickOrderPad.QuickOrderPadAutoComplete();
        QuickOrderPad.AddMultipleOrdersToCart();
        QuickOrderPad.ClearAll();
        QuickOrderPad.RemoveRow();
        QuickOrderPad.ShowRemoveItemBox();
        $(".remove_row").hide();
    },

    QuickOrderPadAutoComplete: function () {
        QuickOrderPad.DefaultQuantity();
        $(".quick-order-pad-autocomplete").autocomplete({
            source: function (request, response) {
                try {
                    Api.GetProductListBySKU(request.term, function (data) {
                        response($.map(data, function (item) {
                            return {
                                label: item.AutoCompleteLabel,
                                value: item.Sku,
                                productId: item.ProductId,
                                price: item.Price,
                                addonValueIds: item.AddonValueIds,
                                bundleItemsIds: item.BundleItemIds,
                                sku: item.Sku,
                                productName: item.ProductName,
                                cartQuantity: item.CartQuantity,
                                quantityOnHand: item.QuantityOnHand,
                                inStockMessage: item.InStockMessage,
                                outOfStockMessage: item.OutOfStockMessage,
                                minimumQuantity: item.MinQty,
                                maximunQuantity: item.MaxQty,
                                callForPricing: item.CallForPricing,
                                bundleQuantityOnHand: item.BundleQuantityOnHand,
                                addOnQuantityOnHand: item.AddOnQuantityOnHand,
                                allowBackOrder: item.AllowBackOrder,
                                trackInventoryInd: item.TrackInventoryInd,
                                bundleAllowBackOrder: item.BundleAllowBackOrder,
                                bundleTrackInventoryInd: item.BundleTrackInventoryInd,
                            }
                        }));
                    });
                } catch (err) {
                }
            },
            html: true,
            select: function (event, ui) {
                $(this).val(ui.item.sku);
                $(this).attr("data_qo_sku", ui.item.sku);
                $(this).attr("data_qo_product_id", ui.item.productId);
                $(this).attr("data_qo_product_name", ui.item.productName);
                $(this).attr("data_qo_addon_value_ids", ui.item.addonValueIds);
                $(this).attr("data_qo_bundle_item_ids", ui.item.bundleItemsIds);
                $(this).attr("data_qo_cart_quantity", ui.item.cartQuantity);
                $(this).attr("data_qo_quantity_on_hand", ui.item.quantityOnHand);
                $(this).attr("data_qo_in_stock_message", ui.item.inStockMessage);
                $(this).attr("data_qo_out_stock_message", ui.item.outOfStockMessage);
                $(this).attr("data_qo_min_quantity", ui.item.minimumQuantity);
                $(this).attr("data_qo_max_quantity", ui.item.maximunQuantity);
                $(this).attr("data_qo_call_for_pricing", ui.item.callForPricing);
                $(this).attr("data_qo_add_on_quantity_on_hand", ui.item.addOnQuantityOnHand);
                $(this).attr("data_qo_bundle_quantity_on_hand", ui.item.bundleQuantityOnHand);
                $(this).attr("data_qo_allow_back_order", ui.item.allowBackOrder);
                $(this).attr("data_qo_track_inventory_ind", ui.item.trackInventoryInd);
                $(this).attr("data_qo_bundle_allow_back_order", ui.item.bundleAllowBackOrder);
                $(this).attr("data_qo_bundle_track_inventory_ind", ui.item.bundleTrackInventoryInd);
                $(this).attr("data_is_first", "true");
                $('#btnQuickOrderPad').removeAttr('disabled');

                var skuId = $(this).attr("id");
                $("#inventoryMessage_" + skuId.split('_')[1]).html("");

                return false;
            },
            open: function () {
                $(this).data("ui-autocomplete").menu.element.addClass("order-pad-autocomplete");
            }
        });
    },

    AddMultipleOrdersToCart: function () {
        var cartItems = [];
        var isSuccess = true;

        $('#btnQuickOrderPad').click(function () {
            //debugger;
            cartItems.length = 0;
            var index = 0;
            isSuccess = true;
            $(".quick-order-pad-autocomplete").each(function () {

                var track_inventory = $(this).attr("data_qo_track_inventory_ind");
                var back_order = $(this).attr("data_qo_allow_back_order");
                var quantity_on_hand = $(this).attr("data_qo_quantity_on_hand");
                index++;

                $('#inventoryMessage_' + index).html("");

                if (!isNaN($(this).attr("data_qo_product_id")) && $(this).attr("data_qo_product_id") != "") {

                    var quantity = parseInt($('#txtQuickOrderPadQuantity_' + index).val());

                    if ($(this).attr("data_is_first") == "false") {
                        $('#inventoryMessage_' + index).html("Please enter valid sku");
                        isSuccess = false;
                        return false;
                    }

                    if ($('#txtQuickOrderPadSku_' + index).val() != $(this).attr("data_qo_sku")) {
                        $('#inventoryMessage_' + index).html("Please enter valid sku");
                        isSuccess = false;
                        return false;
                    }

                    if (parseInt(quantity % 1) != 0 || parseInt(quantity) <= 0) {
                        $('#inventoryMessage_' + index).html("Enter Valid Quantity");
                        isSuccess = false;
                        return false;
                    }

                    if (isNaN(quantity) || quantity == "") {
                        $('#inventoryMessage_' + index).html("Enter Whole Number");
                        isSuccess = false;
                        return false;
                    }

                    if ($(this).attr("data_qo_call_for_pricing") == "true") {
                        $("#inventoryMessage_" + index).html("Call for pricing");
                        isSuccess = false;
                        return false;
                    }

                    if ((track_inventory == "true" && back_order == "false")) {
                        if (parseInt(quantity_on_hand) <= 0) {
                            $('#inventoryMessage_' + index).html($(this).attr("data_qo_out_stock_message"));
                            isSuccess = false;
                            return false;
                        }
                    }

                    if (parseInt($(this).attr("data_qo_max_quantity")) < quantity + parseInt($(this).attr("data_qo_cart_quantity"))) {
                        $('#inventoryMessage_' + index).html("Selected quantity exceeds product maximum given current Shopping Cart quantities.");
                        isSuccess = false;
                        return false;
                    }

                    if (parseInt($(this).attr("data_qo_min_quantity")) > quantity) {
                        $('#inventoryMessage_' + index).html("Selected quantity is less than minimum specified quantity.");
                        isSuccess = false;
                        return false;
                    }

                    if ((track_inventory == "true" && back_order == "false")) {
                        if (parseInt(quantity_on_hand) == parseInt($(this).attr("data_qo_cart_quantity"))) {
                            $('#inventoryMessage_' + index).html($(this).attr("data_qo_out_stock_message"));
                            isSuccess = false;
                            return false;
                        }
                    }

                    if ((track_inventory == "true" && back_order == "false")) {
                        if ((quantity + parseInt($(this).attr("data_qo_cart_quantity"))) > parseInt($(this).attr("data_qo_quantity_on_hand"))) {
                            $('#inventoryMessage_' + index).html("Only " + (parseInt(quantity_on_hand) - parseInt($(this).attr("data_qo_cart_quantity"))) + " quantity are available for Add to cart/Shipping");
                            isSuccess = false;
                            return false;
                        }
                    }

                    if (QuickOrderPad.CheckInventory($(this).attr("data_qo_add_on_quantity_on_hand"), quantity + parseInt($(this).attr("data_qo_cart_quantity")))) {
                        $('#inventoryMessage_' + index).html("Add-On is out of stock");
                        isSuccess = false;
                        return false;
                    }

                    if (QuickOrderPad.CheckInventoryForBundle($(this).attr("data_qo_bundle_quantity_on_hand"), quantity + parseInt($(this).attr("data_qo_cart_quantity")), $(this).attr("data_qo_bundle_track_inventory_ind"), $(this).attr("data_qo_bundle_allow_back_order"))) {
                        $('#inventoryMessage_' + index).html("Bundle product is out of stock");
                        isSuccess = false;
                        return false;
                    }

                    if (isSuccess) {
                        var quickOrderModel =
                        {
                            "ProductId": $(this).attr("data_qo_product_id"),
                            "BundleItemsIds": $(this).attr("data_qo_bundle_item_ids"),
                            "AddOnValueIds": $(this).attr("data_qo_addon_value_ids"),
                            "ProductName": $(this).attr("data_qo_product_name"),
                            "Sku": $(this).attr("data_qo_sku"),
                            "Quantity": quantity
                        };
                        cartItems.push(quickOrderModel);
                    }
                }
                else if ($('#txtQuickOrderPadSku_' + index).val() != "") {
                    $('#inventoryMessage_' + index).html("Please enter valid sku");
                    $("#removeRow_" + index).show();
                    isSuccess = false;
                    return false;
                }
            });
            if (isSuccess) {
                $.ajax({
                    url: "/product/addmultipleorderstocart",
                    type: "post",
                    data: JSON.stringify(cartItems),
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.isSuccess) {
                            window.location.href = "/cart";
                        }
                        else {
                            $('#lblNotificationMessage').addClass('error-msg');
                            $('#lblNotificationMessage').html(data.message);
                        }
                    },
                    error: function (msg) {
                    }
                });
            }
        });
    },

    ClearAll: function () {
        $("#btnQuickOrderClearAll").click(function () {
            var index = 0;
            $(".quick-order-pad-autocomplete").each(function () {
                index++;
                $('#txtQuickOrderPadSku_' + index).val("");
                $('#txtQuickOrderPadQuantity_' + index).val('1');
                $('#inventoryMessage_' + index).html("");
                $(this).attr("data_qo_sku", "");
                $(this).attr("data_qo_product_id", "");
                $(this).attr("data_qo_product_name", "");
                $(this).attr("data_qo_addon_value_ids", "");
                $(this).attr("data_qo_bundle_item_ids", "");
                $(this).attr("data_qo_cart_quantity", "");
                $(this).attr("data_qo_quantity_on_hand", "");
                $(this).attr("data_qo_in_stock_message", "");
                $(this).attr("data_qo_out_stock_message", "");
                $(this).attr("data_qo_min_quantity", "");
                $(this).attr("data_qo_max_quantity", "");
                $(this).attr("data_qo_call_for_pricing", "");
                $(this).attr("data_qo_bundle_quantity_on_hand", "");
                $(this).attr("data_qo_add_on_quantity_on_hand", "");
                $(this).attr("data_is_first", "true");
                $("#removeRow_" + index).hide();
                $('#lblNotificationMessage').html("");
                $('#btnQuickOrderPad').attr('disabled', 'disabled');
            });
        });
    },

    DefaultQuantity: function () {
        $(".quick-order-pad-quantity").val('1');
    },

    CheckInventory: function (quantitiesOnHand, quantity) {
        if (quantitiesOnHand != undefined && quantitiesOnHand != null && quantitiesOnHand.length > 0) {
            var quantityArray = quantitiesOnHand.split(',');
            for (var quantityIndex = 0; quantityIndex < quantityArray.length; quantityIndex++) {
                if ((quantityArray[quantityIndex] != "") && parseInt(quantityArray[quantityIndex]) < quantity) {
                    return true;
                }
            }
        }
        return false;
    },

    RemoveRow: function () {
        $(".remove_row").click(function () {
            var removeId = $(this).attr("id");
            var selectedRow = removeId.split('_')[1];
            QuickOrderPad.ClearSelectedData(selectedRow);
            $(this).hide();

            var _existVal = 0;
            $('.quick-order-pad-autocomplete').each(function () {
                if ($(this).val() !== "") {
                    _existVal = 1;
                }
            });
            if (_existVal === 0) {
                $('#btnQuickOrderPad').attr('disabled', 'disabled');
                $('#lblNotificationMessage').html("");
            }
        });
    },

    ShowRemoveItemBox: function () {
        $(".quick-order-pad-autocomplete").on("focusout", function () {
            var removeId = $(this).attr("id");
            var selectedRow = removeId.split('_')[1];
            if ($(this).val() != "") {
                $("#removeRow_" + selectedRow).show();
            }
            else {
                QuickOrderPad.ClearSelectedData(selectedRow);
                $("#removeRow_" + selectedRow).hide();
                var _existVal = 0;
                $('.quick-order-pad-autocomplete').each(function () {
                    if ($(this).val() !== "") {
                        _existVal = 1;
                    }
                });
                if (_existVal === 0) {
                    $('#btnQuickOrderPad').attr('disabled', 'disabled');
                    $('#lblNotificationMessage').html("");
                }
            }
        });
    },

    CheckInventoryForBundle: function (quantitiesOnHand, quantity, track_inventory_bundle, allow_back_order) {
        if (quantitiesOnHand != undefined && quantitiesOnHand != null && quantitiesOnHand.length > 0) {
            var quantityArray = quantitiesOnHand.split(',');
            var track_inventory_bundle_array = (track_inventory_bundle != undefined && track_inventory_bundle != null && track_inventory_bundle.length > 0) ? track_inventory_bundle.split(',') : [];
            var allow_back_order_array = (allow_back_order != undefined && allow_back_order != null && allow_back_order.length > 0) ? allow_back_order.split(',') : [];
            for (var quantityIndex = 0; quantityIndex < quantityArray.length; quantityIndex++) {
                if (track_inventory_bundle_array[quantityIndex] == "1" && allow_back_order_array[quantityIndex] == "0") {
                    if ((quantityArray[quantityIndex] != "") && (parseInt(quantityArray[quantityIndex]) < quantity)) {
                        return true;
                    }
                }
            }
        }
        return false;
    },

    ClearSelectedData: function (selectedRow) {
        $('#txtQuickOrderPadSku_' + selectedRow).val("");
        $('#txtQuickOrderPadQuantity_' + selectedRow).val('1');
        $('#inventoryMessage_' + selectedRow).html("");
        $('#txtQuickOrderPadSku_' + selectedRow).attr("data_qo_sku", "");
        $('#txtQuickOrderPadSku_' + selectedRow).attr("data_qo_product_id", "");
        $('#txtQuickOrderPadSku_' + selectedRow).attr("data_qo_product_name", "");
        $('#txtQuickOrderPadSku_' + selectedRow).attr("data_qo_addon_value_ids", "");
        $('#txtQuickOrderPadSku_' + selectedRow).attr("data_qo_bundle_item_ids", "");
        $('#txtQuickOrderPadSku_' + selectedRow).attr("data_qo_cart_quantity", "");
        $('#txtQuickOrderPadSku_' + selectedRow).attr("data_qo_quantity_on_hand", "");
        $('#txtQuickOrderPadSku_' + selectedRow).attr("data_qo_in_stock_message", "");
        $('#txtQuickOrderPadSku_' + selectedRow).attr("data_qo_out_stock_message", "");
        $('#txtQuickOrderPadSku_' + selectedRow).attr("data_qo_min_quantity", "");
        $('#txtQuickOrderPadSku_' + selectedRow).attr("data_qo_max_quantity", "");
        $('#txtQuickOrderPadSku_' + selectedRow).attr("data_qo_call_for_pricing", "");
        $('#txtQuickOrderPadSku_' + selectedRow).attr("data_qo_bundle_quantity_on_hand", "");
        $('#txtQuickOrderPadSku_' + selectedRow).attr("data_qo_add_on_quantity_on_hand", "");
        $('#txtQuickOrderPadSku_' + selectedRow).attr("data_is_first", "true");
    }
}

$(document).ready(function () {
    QuickOrderPad.Initialization();
});