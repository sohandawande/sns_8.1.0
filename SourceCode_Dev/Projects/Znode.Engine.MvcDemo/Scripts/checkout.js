﻿
function SetPaymentUrl() {
    var payment_Url = "http://localhost:56247";
    return payment_Url;
}


var GloabalVariables = {
    PaymentApplicationUrl: SetPaymentUrl() + "/",
    PaymentApplicationJSCall: SetPaymentUrl() + "/script/znodeapijs?gateway={0}&profileId={1}&twoCoUrl={2}",
    PymentApplicationPaypalCall: SetPaymentUrl() + "/payment/paypal?callback=?",
    ZnodeMVCDemoUrl: location.protocol + "//" + location.host,
    ZnodeDemoPaymentCancelUrl: location.protocol + "//" + location.host + "/checkout",
    ZnodeDemoPaymentTwoCoUrl: location.protocol + "//" + location.host + "/checkout/twoco?paymentOptionId={0}",
    ZnodeDemoPaymentSuccessUrl: location.protocol + "//" + location.host + "/checkout/custom?paymentOptionId={0}",
}

String.format = function () {
    var theString = arguments[0];

    for (var i = 1; i < arguments.length; i++) {
        var regEx = new RegExp("\\{" + (i - 1) + "\\}", "gm");
        theString = theString.replace(regEx, arguments[i]);
    }

    return theString;
}
var Checkout = {
    init: function () {
        $("#payment-methods a").click(function () {
            Checkout.viewPayments.togglePaymentMethod(this);
        });
        $("#checkoutStep1").click(function () {            
            var email = $("#address_email").val();
            if (email === "" || email === undefined || email.length < 1) {
                $('.error-msg').show().html("Email address is required.");
                return false;
            }
        });
        Checkout.viewSinglePageCheckout.init();
        $("#payment-methods li a:eq(0)").click();
    },
    GloabalVariables: {
        PaymentApplicationUrl: SetPaymentUrl() + "/",
        PaymentApplicationJSCall: SetPaymentUrl() + "/script/znodeapijs?gateway={0}&profileId={1}&twoCoUrl={2}",
        PymentApplicationPaypalCall: SetPaymentUrl() + "/payment/paypal?callback=?",
        ZnodeMVCDemoUrl: location.protocol + "//" + location.host,
        ZnodeDemoPaymentCancelUrl: location.protocol + "//" + location.host + "/checkout",
        ZnodeDemoPaymentTwoCoUrl: location.protocol + "//" + location.host + "/checkout/twoco?paymentOptionId={0}",
        ZnodeDemoPaymentSuccessUrl: location.protocol + "//" + location.host + "/checkout/custom?paymentOptionId={0}",
    },

    // PAYMENT VIEW
    viewPayments: {

        // Toggle payment option tabs
        togglePaymentMethod: function (el) {
            var clicked = $(el).parent(),
            content = clicked.data("toggle");

            _debug("payment method: " + content);
            $("#payment-methods li").removeClass("active");

            clicked.addClass("active");
            $(".payment-method").hide();
            $("#" + content).show();

            $("#divPaymentOptions").show();
        }
    },

    viewSinglePageCheckout: {
        init: function () {
            Checkout.bind.setStateByCountry();
            _debug("checkout.singlecheckout.init()");
            Checkout.bind.singleCheckout();
            Checkout.bind.billingAddress();
            Checkout.bind.BillingSameAsShippingHandler();
            Checkout.bind.setDefaultState();
            Checkout.bind.callAddressChange(0);
            Checkout.bind.OnStateChange();
            Checkout.bind.payment();
            Checkout.bind.giftCard();
            Checkout.bind.ShowAppliedPromotion();
            Checkout.bind.RemoveDuplicateMessage();
        },

    },


    //NEW ADDRESS 
    viewAddnewaddress: {
        init: function () {
            _debug("checkout.addnewaddress.init()");
            Checkout.bind.setStateByCountryById();
            Checkout.bind.setDefaultStateById();
            Checkout.bind.OnStateChangeForAddNewAddress();
        }
    },

    // BINDINGS
    bind: {
        // Payment option toggling
        payment: function () {
            _debug("checkout.bind.payment()");

            if ($('#defaultpayment').length > 0) {
                _debug("default payment:" + $('#defaultpayment').val());
                Checkout.viewPayments.togglePaymentMethod('a[data-toggle=' + $('#defaultpayment').val() + ']');
            }

            // COD confirm to continue
            $("#pick-cod").on("change", function (e) {
                $("#payment-cod .button").toggleDisabled();
            });

            // Purchase Order click
            $("#btn-payment-po").on("click", function () {
                var form = $(this).closest(".form");
                form.find(".form-error").html('');
                if ($("#txt-po-number").val().trim().length < 1) {
                    form.find(".form-error").html('<div class="field-validation-error">Please enter purchase order number.</div>');
                    return false;
                }
                else {
                    $("#hdnPurchaseOrderNumber").val($("#txt-po-number").val().trim());
                }
            });

            $("#ccCheckoutButton").on("click", function (e) {
                e.preventDefault();
                $("#hdnGatwayName").val('');

                if ($("#CCPaymentOptionId").val().length > 0) {

                    if ($("#ajaxBusy").html() == undefined) {
                        $('body').append('<div id="ajaxBusy"><p id="ajaxBusyMsg">Please wait while we load the payment screen. Do not close this window.</p></div>');
                    }

                    $("#ajaxBusy").html('');
                    $("#ajaxBusy").html('Please wait while we load the payment screen. Do not close this window.');
                    $("#ajaxBusy").dialog({
                        title: 'Payment Application',
                        resizable: false,
                        modal: true,
                        open: function (event, ui) {
                            $("#ajaxBusy").parent().find(" .ui-dialog-titlebar-close").hide();
                        }
                    });

                    Api.GetPaymentGatwayName($("#CCPaymentOptionId").val(), function (response) {
                        if (response.success) {
                            $("#hdnGatwayName").val(response.name);
                            $("#paymentProfileId").val(response.profileId);

                            var gatewayName = $("#hdnGatwayName").val();
                            if (gatewayName != undefined && gatewayName.length > 0) {
                                var paymentAppGatewayName = gatewayName.toLowerCase();

                                if (gatewayName.toLowerCase() == 'authorize.net') {
                                    paymentAppGatewayName = 'authorizenet';
                                }

                                if (gatewayName.toLowerCase() == 'paymentech orbital') {
                                    paymentAppGatewayName = 'paymenttech';
                                }

                                var paymentUrl = Checkout.GloabalVariables.PaymentApplicationJSCall;
                                var twoCOUrl = String.format(Checkout.GloabalVariables.ZnodeDemoPaymentTwoCoUrl, $("#CCPaymentOptionId").val());
                                paymentUrl = String.format(paymentUrl, paymentAppGatewayName, $("#paymentProfileId").val(), twoCOUrl);

                                $.ajax({
                                    url: paymentUrl,
                                    dataType: "script",
                                    success: function (response) {
                                        $("#ajaxBusy").dialog('close');
                                        Checkout.bind.SetCreditCardValidations();
                                        $("#ccpopUp").show();
                                    },
                                    error: function () {
                                        $("#ajaxBusy").dialog('close');
                                        $("#ccpopUp .ErrorMessage").html('');
                                        $("#ccpopUp .ErrorMessage").html('Unable to contact payment application.');
                                    }
                                });

                            } else {
                                $("#ajaxBusy").html('');
                                $("#ajaxBusy").html('Could not procced with payment as no payment gateway available with this selection.');
                            }
                            return false;
                        } else {
                            $("#ccpopUp .ErrorMessage").html('');
                            $("#ajaxBusy").html('Could not procced with payment as no payment gateway available with this selection.');
                        }
                    });
                }
                return false;
            });

            $("#singlepage-paypal-express-checkout").on("click", function (e) {
                var isValid = true;
                overlayforwaitblock();
                e.preventDefault();
                if ($("#ddlPaymentTypes option:selected").text().toLowerCase() == 'select payment') {
                    isValid = false;
                    $("#ddlPaymentTypes").css({
                        "border": "1px solid red",
                        "background": "#FFCECE url('../../../Images/drop-arrow.png') no-repeat 98% center"
                    });
                }
                $('input[name="ShippingBillingAddressViewModel.ShippingAddressModel.FirstName"],input[name="ShippingBillingAddressViewModel.ShippingAddressModel.LastName"],input[name="ShippingBillingAddressViewModel.ShippingAddressModel.StreetAddress1"],input[name="ShippingBillingAddressViewModel.EmailAddress"],input[name="ShippingBillingAddressViewModel.ShippingAddressModel.PostalCode"],input[name="ShippingBillingAddressViewModel.ShippingAddressModel.City"],input[name="ShippingBillingAddressViewModel.ShippingAddressModel.PhoneNumber"]').each(function () {
                    if ($.trim($(this).val()) == '') {
                        isValid = false;
                        $(this).css({
                            "border": "1px solid red",
                            "background": "#FFCECE"
                        });
                    }
                    else {
                        $(this).css({
                            "border": "1px solid #C3C3C3",
                            "background": ""
                        });
                    }
                });

                var country = $('select[name="ShippingBillingAddressViewModel.ShippingAddressModel.CountryCode"] option:selected').val();
                if (country == "0" || country == "") {
                    isValid = false;
                    $('select[name="ShippingBillingAddressViewModel.ShippingAddressModel.CountryCode"]').css({
                        "border": "1px solid red",
                        "background": "#FFCECE"
                    });
                }
                else {
                    $('select[name="ShippingBillingAddressViewModel.ShippingAddressModel.CountryCode"]').css({
                        "border": "1px solid #C3C3C3",
                        "background": ""
                    });
                }

                if ($('#CreateLogin').is(":checked")) {
                    $('input[name="ShippingBillingAddressViewModel.LoginViewModel.Username"],input[name="ShippingBillingAddressViewModel.LoginViewModel.Password"]').each(function () {
                        if ($.trim($(this).val()) == '') {
                            isValid = false;
                            $(this).css({
                                "border": "1px solid red",
                                "background": "#FFCECE"
                            });
                        }
                        else {
                            $(this).css({
                                "border": "1px solid #C3C3C3",
                                "background": ""
                            });
                        }
                    });
                }

                if ($('input[name = "ShippingBillingAddressViewModel.EmailAddress"]').val() == "" || !Checkout.bind.ValidateEmail($('input[name = "ShippingBillingAddressViewModel.EmailAddress"]').val())) {
                    isValid = false;
                    $('input[name = "ShippingBillingAddressViewModel.EmailAddress"]').css({
                        "border": "1px solid red",
                        "background": "#FFCECE"
                    });
                }
                else {
                    $('input[name = "ShippingBillingAddressViewModel.EmailAddress"]').css({
                        "border": "1px solid #C3C3C3",
                        "background": ""
                    });
                }

                if (!($('#billingSameAsShippingAddress').is(":checked"))) {
                    $('input[name="ShippingBillingAddressViewModel.BillingAddressModel.FirstName"],input[name="ShippingBillingAddressViewModel.BillingAddressModel.LastName"],input[name="ShippingBillingAddressViewModel.BillingAddressModel.StreetAddress1"],input[name="ShippingBillingAddressViewModel.BillingAddressModel.PostalCode"],input[name="ShippingBillingAddressViewModel.BillingAddressModel.City"],input[name="ShippingBillingAddressViewModel.ShippingAddressModel.PhoneNumber"]').each(function () {
                        if ($.trim($(this).val()) == '') {
                            isValid = false;
                            $(this).css({
                                "border": "1px solid red",
                                "background": "#FFCECE"
                            });
                        }
                        else {
                            $(this).css({
                                "border": "1px solid #C3C3C3",
                                "background": ""
                            });
                        }
                    });

                    var country = $('select[name="ShippingBillingAddressViewModel.BillingAddressModel.CountryCode"] option:selected').val();
                    if (country == "0" || country == "") {
                        isValid = false;
                        $('select[name="ShippingBillingAddressViewModel.BillingAddressModel.CountryCode"]').css({
                            "border": "1px solid red",
                            "background": "#FFCECE"
                        });
                    }
                    else {
                        $('select[name="ShippingBillingAddressViewModel.BillingAddressModel.CountryCode"]').css({
                            "border": "1px solid #C3C3C3",
                            "background": ""
                        });
                    }
                }
                if ($('#CreateLogin').is(":checked")) {
                    var pattern = new RegExp(/^(?=.*[0-9])(?=.*[a-zA-Z]).{8,}$/);
                    if (!pattern.test($('#login_password').val())) {
                        isValid = false;
                        $("#valPassword").text('Password must be at least 8 alphanumeric characters and contain at least one number');
                    }
                }
                if ($('#CreateLogin').is(":checked") && $("#valPassword").text() != "") {
                    isValid = false;
                }
                if (isValid) {
                        $.ajax({
                            url: "createcustomerpaypalaccount",
                            type: 'post',
                            data: $("#checkoutform").serialize(),
                            success: function (data) {
                                $('#status-message').remove();
                                if (data.HasError) {
                                    overlayforwaitnone();
                                    $("#SuccessMessage").append('<div id="status-message" class="alert alert-danger text-center">' + data.ErrorMessage + '</div>');
                                    isValid = false;
                                    if (data.ErrorMessage != undefined && data.ErrorMessage != "" && data.ErrorMessage != null) {
                                        if (data.ErrorMessage.trim() == "Your Shipping Address is not a valid address. Please enter valid address to continue.") {
                                            window.location = "/checkout/singlecheckout?message=" + data.ErrorMessage;
                                        }
                                    }
                                    $("#ajaxProcessPayment").dialog('close');
                                    $(window).scrollTop(0);
                                    return isValid;
                                }
                                else {
                                    $('#status-message').remove();
                                    overlayforwaitnone();
                                    Checkout.bind.PayPalPaymentProcess();
                                }
                            }
                        })
                }
                else {
                    overlayforwaitnone();
                    return false;
                }

            });

            $("#paypal-express-checkout").on("click", function (e) {
                $("#ErrorMessage").html('');
                $("#ajaxProcessPayment").html('');
                if ($("#ajaxProcessPayment").html() == undefined) {
                    $('body').append('<div id="ajaxProcessPayment"><p id="ajaxProcessPaymentMsg">Your payment is processing. Please wait and do not close this window</p></div>');
                }

                $("#ajaxProcessPayment").html('<p id="ajaxProcessPaymentMsg">Your payment is processing. Please wait and do not close this window</p>');
                $("#ajaxProcessPayment").dialog({
                    title: 'Payment Application',
                    resizable: false,
                    modal: true,
                    create: function () {
                        $(this).closest(".ui-dialog").addClass("ui-md-popup ui-popup-top");
                    },
                    closeOnEscape: false,
                    open: function (event, ui) {
                        $("#ajaxProcessPayment").parent().find(" .ui-dialog-titlebar-close").hide();
                    }
                });
                var paymentSettingId = $('#PaymentPaypalOptionId').val();
                var ReturnUrl = String.format(GloabalVariables.ZnodeDemoPaymentSuccessUrl, paymentSettingId);
                var CancelUrl = GloabalVariables.ZnodeDemoPaymentCancelUrl;
                Api.ProcessPayPalPayment(paymentSettingId, ReturnUrl, CancelUrl, function (response) {
                    var message = response;
                    $("#SuccessMessage").html('');
                    $("#status-message").html();
                    if (message != undefined && message.indexOf('Message=') >= 0) {
                        var errorMessage = message.substr(message.indexOf('=') + 1);
                        $("#ajaxBusy").dialog('close');
                        $('#SuccessMessage').show();
                        $("#ajaxProcessPayment").dialog('close');
                        $("#SuccessMessage").append('<div id="status-message" class="alert alert-danger text-center">' + errorMessage + '</div>');
                    } else if (message.indexOf("http") != -1) {
                        $("#ajaxBusy").dialog('close');
                        window.location = response;
                    }
                    else {
                        $("#ajaxBusy").dialog('close');
                        $("#ajaxProcessPayment").dialog('close');
                        $("#SuccessMessage").append('<div id="status-message" class="alert alert-danger text-center">' + message + '</div>');
                        $(window).scrollTop(0);
                    }
                });
                return false;
            });

            $("#ccpopUp #submitPayment").on("click", function (e) {
                var Total = 0;
                var finalPrice = Number($('#dynamic-total').text().replace(/[^0-9\.]+/g, ""));
                if ($('#OrderTotalGiftCard').val() == "" || $('#OrderTotalGiftCard').val() == undefined) {
                    Total = parseFloat(finalPrice).toFixed(2);
                } else {
                    Total = parseFloat($('#OrderTotalGiftCard').val()).toFixed(2);
                }

                if (Checkout.bind.IsOrderTotalGreaterThanZero(Total)) {
                    var isValid = true;
                    $('input[data-payment="number"],input[data-payment="exp-month"],input[data-payment="exp-year"],input[data-payment="cvc"]').each(function () {
                        if ($.trim($(this).val()) == '') {
                            isValid = false;
                            $(this).css({
                                "border": "1px solid red",
                                "background": "#FFCECE"
                            });
                        }
                        else {
                            $(this).css({
                                "border": "1px solid #C3C3C3",
                                "background": ""
                            });
                        }
                    });

                    if (!Checkout.bind.Mod10($('input[data-payment="number"]').val())) {
                        isValid = false;
                        $('#errornumber').show();
                        $('input[data-payment="number"]').css({
                            "border": "1px solid red",
                            "background": "#FFCECE"
                        });
                    }

                    if ($('input[data-payment="exp-month"]').val() > 12 || $('input[data-payment="exp-month"]').val() < 1) {
                        isValid = false;
                        $('#errormonth').show();
                        $('input[data-payment="exp-month"]').css({
                            "border": "1px solid red",
                            "background": "#FFCECE"
                        });
                    }

                    var currentYear = (new Date).getFullYear();
                    var currentMonth = (new Date).getMonth() + 1;

                    if ($('input[data-payment="exp-year"]').val() < currentYear) {
                        isValid = false;
                        $('#erroryear').show();
                        $('input[data-payment="exp-year"]').css({
                            "border": "1px solid red",
                            "background": "#FFCECE"
                        });
                    }

                    if ($('input[data-payment="exp-year"]').val() == currentYear && $('input[data-payment="exp-month"]').val() < currentMonth) {
                        isValid = false;
                        $('#errormonth').show();
                        $('input[data-payment="exp-month"]').css({
                            "border": "1px solid red",
                            "background": "#FFCECE"
                        });
                        $('#erroryear').show();
                        $('input[data-payment="exp-year"]').css({
                            "border": "1px solid red",
                            "background": "#FFCECE"
                        });
                    }
                    if ($('input[data-payment="cvc"]').val().length < 3) {
                        isValid = false;
                        $('#errorcardnumber').show();
                        $('input[data-payment="cvc"]').css({
                            "border": "1px solid red",
                            "background": "#FFCECE"
                        });
                    }

                    if (isValid == false) {
                        e.preventDefault();
                    }

                    if (isValid) {

                        $("#ajaxProcessPayment").html('');
                        if ($("#ajaxProcessPayment").html() == undefined) {
                            $('body').append('<div id="ajaxProcessPayment"><p id="ajaxProcessPaymentMsg">Your payment is processing. Please wait and do not close this window</p></div>');
                        }

                        $("#ajaxProcessPayment").html('<p id="ajaxProcessPaymentMsg">Your payment is processing. Please wait and do not close this window</p>');
                        $("#ajaxProcessPayment").dialog({
                            title: 'Payment Application',
                            resizable: false,
                            modal: true,
                            create: function () { $(this).closest(".ui-dialog").addClass("ui-md-popup ui-popup-top"); },
                            closeOnEscape: false,
                            open: function (event, ui) {
                                $("#ajaxProcessPayment").parent().find(" .ui-dialog-titlebar-close").hide();
                            }
                        });

                        var cardNumber = $("#ccpopUp [data-payment='number']").val();
                        var cardCvcNum = $("#ccpopUp [data-payment='cvc']").val();
                        var cardExpMonth = $("#ccpopUp [data-payment='exp-month']").val();
                        var cardExpYear = $("#ccpopUp [data-payment='exp-year']").val();

                        var guid = $('#GUID').val();

                        var discount = parseFloat($('#Discount').val()).toFixed(2);
                        var ShippingCost = parseFloat($('#ShippingCost').val()).toFixed(2);
                        var TaxCost = parseFloat($('#TaxCost').val()).toFixed(2);
                        var SubTotal = parseFloat($('#SubTotal').val()).toFixed(2);

                        var cardType = Checkout.bind.detectCardType(cardNumber);
                        var OrderId = 1;

                        var BillingFirstName = $('#BillingFirstName').val();
                        var BillingLastName = $('#BillingLastName').val();
                        var BillingName = $('#BillingName').val();
                        var BillingStreetAddress1 = $('#BillingStreetAddress1').val();
                        var BillingStreetAddress2 = $('#BillingStreetAddress2').val();
                        var BillingCity = $('#BillingCity').val();
                        var BillingStateCode = $('#BillingStateCode').val();
                        var BillingCountryCode = $('#BillingCountryCode').val();
                        var BillingPhoneNumber = $('#BillingPhoneNumber').val();
                        var BillingPostalCode = $('#BillingPostalCode').val();
                        var BillingEmailId = $('#BillingEmailId').val();
                        var BillingCompanyName = $('#BillingCompanyName').val();

                        var paymentSettingId = $('#CCPaymentOptionId').val();

                        var GatewayLoginName = $('#GatewayLoginName').val();
                        var GatewayLoginPassword = $('#GatewayLoginPassword').val();
                        var GatewayTestMode = $('#GatewayTestMode').val();
                        var GatewayCustom1 = $('#GatewayCustom1').val();
                        var GatewayPreAuthorize = $('#GatewayPreAuthorize').val();
                        var GatewayTransactionKey = $('#GatewayTransactionKey').val();
                        var GatewayCurrencyCode = $('#GatewayCurrencyCode').val();
                        var Gateway = $('#GatewayType').val();

                        var ReturnUrl = String.format(Checkout.GloabalVariables.ZnodeDemoPaymentSuccessUrl, $("#CCPaymentOptionId").val());
                        var CancelUrl = Checkout.GloabalVariables.ZnodeDemoPaymentCancelUrl;

                        var CustomerPaymentProfileId = $('#CustomerPaymentProfileId').val();
                        var CustomerProfileId = $('#CustomerProfileId').val();
                        var CardDataToken = $('#CardDataToken').val();

                        var Subscriptions = new Array();

                        if ($('#RecurringBillingFrequency').val() != undefined) {
                            var loopCount = $('#RecurringBillingFrequency').length;
                            for (var iCount = 0; iCount < loopCount; iCount++) {

                                if ($($('#RecurringBillingInd')[iCount]).val() != "False") {

                                    var period = $($('#RecurringBillingFrequency')[iCount]).val();
                                    var billingInd = $($('#RecurringBillingInd')[iCount]).val();
                                    var billingIndinInt = 0;

                                    if (billingInd == 'True') {
                                        billingIndinInt = 1;
                                    }

                                    var item = {
                                        Amount: $($('#RecurringBillingInitialAmount')[iCount]).val(),
                                        Frequency: period,
                                        GUID: "",
                                        InitialAmount: $($('#RecurringBillingInitialAmount')[iCount]).val(),
                                        InstallmentInd: billingIndinInt,
                                        InvoiceNo: 1,
                                        Period: "DAY",
                                        TotalCycles: 1,
                                        ProfileName: $($('#RecurringProfileName')[iCount]).val()
                                    }

                                    Subscriptions.push(item);
                                }
                            }
                        }

                        var gatewayName = $("#hdnGatwayName").val();
                        var paymentAppGatewayName = gatewayName;
                        if (gatewayName != undefined && gatewayName.length > 0) {
                            var paymentAppGatewayName = gatewayName.toLowerCase();

                            if (gatewayName.toLowerCase() == 'authorize.net') {
                                paymentAppGatewayName = 'authorizenet';
                            }
                            else if (gatewayName.toLowerCase() == 'paymentech orbital') {
                                paymentAppGatewayName = 'paymenttech';
                            }
                            else if (gatewayName.toLowerCase() == 'worldpay') {
                                paymentAppGatewayName = 'securenet';
                            }
                        }
                        var payment = {
                            "GUID": guid,
                            "GatewayLoginName": GatewayLoginName,
                            "GatewayLoginPassword": GatewayLoginPassword,
                            "GatewayTestMode": GatewayTestMode,
                            "GatewayCustom1": GatewayCustom1,
                            "GatewayPreAuthorize": GatewayPreAuthorize,
                            "GatewayTransactionKey": GatewayTransactionKey,
                            "GatewayType": paymentAppGatewayName,
                            "GatewayCurrencyCode": GatewayCurrencyCode,
                            "OrderId": OrderId,
                            "BillingCity": BillingCity,
                            "BillingCountryCode": BillingCountryCode,
                            "BillingFirstName": BillingFirstName,
                            "BillingLastName": BillingLastName,
                            "BillingName": BillingName,
                            "BillingPhoneNumber": BillingPhoneNumber,
                            "BillingPostalCode": BillingPostalCode,
                            "BillingStateCode": BillingStateCode,
                            "BillingStreetAddress1": BillingStreetAddress1,
                            "BillingStreetAddress2": BillingStreetAddress2,
                            "BillingEmailId": BillingEmailId,
                            "ShippingCost": ShippingCost,
                            "SubTotal": SubTotal,
                            "TaxCost": TaxCost,
                            "Total": Total,
                            "Discount": discount,
                            "CardSecurityCode": cardCvcNum,
                            "CardNumber": cardNumber,
                            "CardExpirationMonth": cardExpMonth,
                            "CardExpirationYear": cardExpYear,
                            "CancelUrl": CancelUrl,
                            "ReturnUrl": ReturnUrl,

                            "CustomerPaymentProfileId": CustomerPaymentProfileId,
                            "CustomerProfileId": CustomerProfileId,
                            "CardDataToken": CardDataToken,
                            "CardType": cardType,
                            "PaymentSettingId": paymentSettingId,
                            "Subscriptions": Subscriptions,
                            "BillingCompanyName": BillingCompanyName
                        };

                        $("#ccpopUp").hide();
                        
                        submitCard(payment, function (response) {
                            if (response.Data == undefined) {
                                if (response.indexOf("Unauthorized") > 0) {

                                    $("#ajaxProcessPayment").html('');
                                    $("#ccpopUp [data-payment='number']").val('');
                                    $("#ccpopUp [data-payment='cvc']").val('');
                                    $("#ccpopUp [data-payment='exp-month']").val('');
                                    $("#ccpopUp [data-payment='exp-year']").val('');
                                    $("#ajaxProcessPayment").dialog('close');

                                    message = response;

                                    if ($("#ajaxProcessPaymentError").html() == undefined) {
                                        $('body').append('<div id="ajaxProcessPaymentError"><p id="ajaxProcessPaymentErrorMsg">We were unable to process your credit card payment. <br /><br />Reason:<br />' + message + '<br /><br />If the problem persists, contact us to complete your order.</p></div>');
                                    } else {
                                        $("#ajaxProcessPaymentError").html('We were unable to process your credit card payment. <br /><br />Reason:<br />' + message + '<br /><br />If the problem persists, contact us to complete your order.');
                                    }

                                    if ($('#ajaxProcessPaymentError').html() != undefined) {
                                        $('#ajaxProcessPaymentError').dialog({
                                            title: 'Payment Application',
                                            resizable: false,
                                            modal: true,
                                            create: function () {
                                                $(this).closest('.ui-dialog').addClass('ui-md-popup ui-popup-top');
                                            },
                                            buttons: {
                                                Ok: function () {
                                                    $(this).dialog('close');
                                                }
                                            },
                                            closeOnEscape: false,
                                            open: function (event, ui) {
                                                $('#ajaxProcessPaymentError').parent().find(' .ui-dialog-titlebar-close').hide();
                                            }
                                        });
                                    }

                                    return false;
                                }
                            } else {
                                
                                var isSuccess = response.Data.IsSuccess;
                                if (isSuccess) {
                                    var customerProfileId = response.Data.CustomerProfileId;
                                    var customerPaymentId = response.Data.CustomerPaymentProfileId;
                                    var paymentSettingId = $("#CCPaymentOptionId").val();
                                    Api.submitPayment(paymentSettingId, customerProfileId, customerPaymentId, function (response) {
                                        var newLocation = response.Data;
                                        if (newLocation != undefined && newLocation.toLowerCase().indexOf('missing card data') >= 0) {
                                            $('#ajaxProcessPayment').html('');
                                            $('#ccpopUp [data-payment="number"]').val('');
                                            $('#ccpopUp [data-payment="cvc"]').val('');
                                            $('#ccpopUp [data-payment="exp-month"]').val('');
                                            $('#ccpopUp [data-payment="exp-year"]').val('');
                                            $('#ajaxProcessPayment').dialog('close');
                                            if ($('#ajaxProcessPaymentError').html() == undefined) {
                                                $('body').append('<div id="ajaxProcessPaymentError"><p id="ajaxProcessPaymentErrorMsg">Error occurred during processing an order. Order could not be placed as card data is missing.</p></div>');
                                            } else {
                                                $('#ajaxProcessPaymentError').html('Error occurred during processing an order. Order could not be placed as card data is missing.');
                                            }
                                        } else if (newLocation != undefined && newLocation.indexOf('Message=') >= 0) {
                                            var message = newLocation.substr(newLocation.indexOf('=') + 1);
                                            $('#ajaxProcessPayment').html('');
                                            $('#ccpopUp [data-payment="number"]').val('');
                                            $('#ccpopUp [data-payment="cvc"]').val('');
                                            $('#ccpopUp [data-payment="exp-month"]').val('');
                                            $('#ccpopUp [data-payment="exp-year"]').val('');
                                            $('#ajaxProcessPayment').dialog('close');

                                            if ($('#ajaxProcessPaymentError').html() == undefined) {
                                                $('body').append('<div id="ajaxProcessPaymentError"><p id="ajaxProcessPaymentErrorMsg">We were unable to process your credit card payment. <br /><br />Reason:<br />' + message + '<br /><br />If the problem persists, contact us to complete your order.</p></div>');
                                            } else {
                                                $('#ajaxProcessPaymentError').html('We were unable to process your credit card payment. <br /><br />Reason:<br />' + message + '<br /><br />If the problem persists, contact us to complete your order.');
                                            }
                                        } else if (newLocation != undefined) {
                                            $('#ajaxProcessPaymentError').remove();
                                            window.location = newLocation;
                                        } else {
                                            $('#ccpopUp .ErrorMessage').html('');
                                            $('#ccpopUp [data-payment="number"]').val('');
                                            $('#ccpopUp [data-payment="cvc"]').val('');
                                            $('#ccpopUp [data-payment="exp-month"]').val('');
                                            $('#ccpopUp [data-payment="exp-year"]').val('');
                                            $('#ajaxProcessPayment').dialog('close');

                                            if ($('#ajaxProcessPaymentError').html() == undefined) {
                                                $('body').append('<div id="ajaxProcessPaymentError"><p id="ajaxProcessPaymentErrorMsg">Error occurred during processing an order. Order could not be placed.</p></div>');
                                            } else {
                                                $('#ajaxProcessPaymentError').html('Error occurred during processing an order. Order could not be placed.');
                                            }
                                        }

                                        if ($('#ajaxProcessPaymentError').html() != undefined) {
                                            $('#ajaxProcessPaymentError').dialog({
                                                title: 'Payment Application',
                                                resizable: false,
                                                modal: true,
                                                create: function () {
                                                    $(this).closest('.ui-dialog').addClass('ui-md-popup ui-popup-top');
                                                },
                                                buttons: {
                                                    Ok: function () {
                                                        $(this).dialog('close');
                                                    }
                                                },
                                                closeOnEscape: false,
                                                open: function (event, ui) {
                                                    $('#ajaxProcessPaymentError').parent().find(' .ui-dialog-titlebar-close').hide();
                                                }
                                            });
                                        }

                                        return false;
                                    });
                                }
                                else {
                                    var message = response.Data.ResponseText;
                                    if (message == undefined) {
                                        message = response.Data.GatewayResponseData;
                                        if (message == undefined) {
                                            message = response.Data;
                                        }
                                    }
                                    $("#ajaxProcessPayment").html('');
                                    $("#ccpopUp [data-payment='number']").val('');
                                    $("#ccpopUp [data-payment='cvc']").val('');
                                    $("#ccpopUp [data-payment='exp-month']").val('');
                                    $("#ccpopUp [data-payment='exp-year']").val('');
                                    $("#ajaxProcessPayment").dialog('close');
                                    if (message.length > 0 && message != null) {
                                        if ($("#ajaxProcessPaymentError").html() == undefined) {
                                            $('body').append('<div id="ajaxProcessPaymentError"><p id="ajaxProcessPaymentErrorMsg">We were unable to process your credit card payment. <br /><br />Reason:<br />' + message + '<br /><br />If the problem persists, contact us to complete your order.</p></div>');
                                        } else {
                                            $("#ajaxProcessPaymentError").html('We were unable to process your credit card payment. <br /><br />Reason:<br />' + message + '<br /><br />If the problem persists, contact us to complete your order.');
                                        }
                                    }
                                    else {
                                        if ($("#ajaxProcessPaymentError").html() == undefined) {
                                            $('body').append('<div id="ajaxProcessPaymentError"><p id="ajaxProcessPaymentErrorMsg">We were unable to process your credit card payment. <br /><br /><br />If the problem persists, contact us to complete your order.</p></div>');
                                        } else {
                                            $("#ajaxProcessPaymentError").html('We were unable to process your credit card payment. <br /><br /><br />If the problem persists, contact us to complete your order.');
                                        }
                                    }
                                    if ($('#ajaxProcessPaymentError').html() != undefined) {
                                        $('#ajaxProcessPaymentError').dialog({
                                            title: 'Payment Application',
                                            resizable: false,
                                            modal: true,
                                            create: function () {
                                                $(this).closest('.ui-dialog').addClass('ui-md-popup ui-popup-top');
                                            },
                                            buttons: {
                                                Ok: function () {
                                                    $(this).dialog('close');
                                                }
                                            },
                                            closeOnEscape: false,
                                            open: function (event, ui) {
                                                $('#ajaxProcessPaymentError').parent().find(' .ui-dialog-titlebar-close').hide();
                                            }
                                        });
                                    }
                                }
                            }
                        });
                        return false;
                    }
                    return false;
                }
            });
        },
        IsOrderTotalGreaterThanZero: function (total) {
            if (total > 0.00) {
                return true;
            } else {
                var paymentURL = String.format(Checkout.GloabalVariables.ZnodeDemoPaymentSuccessUrl, 0);
                paymentURL = paymentURL + "&token=null";
                window.location = paymentURL;
            }
        },
        SetCreditCardValidations: function () {
            $('input[data-payment="exp-month"]').on("keypress", function (e) {
                if (e.which != 8 && e.which != 0 && (e.wchich < 48 || e.which > 57)) {
                    return false;
                }
            });

            $('input[data-payment="exp-month"]').on("focusout", function (e) {
                var monthVal = $('input[data-payment="exp-month"]').val();
                if (monthVal.length == 1 && (monthVal >= 1 || monthVal <= 9)) {
                    monthVal = 0 + monthVal;
                    $('input[data-payment="exp-month"]').val(monthVal);
                }
            });

            $('input[data-payment="exp-year"]').on("keypress", function (e) {
                if (e.which != 8 && e.which != 0 && (e.wchich < 48 || e.which > 57)) {
                    return false;
                }
            });

            $('input[data-payment="cvc"]').on("keypress", function (e) {
                if (e.which != 8 && e.which != 0 && (e.wchich < 48 || e.which > 57)) {
                    return false;
                }
            });
        },
        Mod10: function (ccNum) {
            var valid = "0123456789";  // Valid digits in a credit card number
            var len = ccNum.length;  // The length of the submitted cc number
            var iCCN = parseInt(ccNum);  // integer of ccNum
            var sCCN = ccNum.toString();  // string of ccNum

            sCCN = sCCN.replace(/^\s+|\s+$/g, '');  // strip spaces

            var iTotal = 0;  // integer total set at zero
            var bNum = true;  // by default assume it is a number
            var bResult = false;  // by default assume it is NOT a valid cc
            var temp;  // temp variable for parsing string
            var calc;  // used for calculation of each digit

            // Determine if the ccNum is in fact all numbers
            for (var j = 0; j < len; j++) {
                temp = "" + sCCN.substring(j, j + 1);
                if (valid.indexOf(temp) == "-1") {
                    bNum = false;
                }
            }

            // if it is NOT a number, you can either alert to the fact, or just pass a failure
            if (!bNum) {
                bResult = false;
            }

            // Determine if it is the proper length
            if ((len == 0) && (bResult)) {  // nothing, field is blank AND passed above # check
                bResult = false;
            } else {  // ccNum is a number and the proper length - let's see if it is a valid card number
                if (len >= 15) {  // 15 or 16 for Amex or V/MC
                    for (var i = len; i > 0; i--) {  // LOOP throught the digits of the card
                        calc = parseInt(iCCN) % 10;  // right most digit
                        calc = parseInt(calc);  // assure it is an integer
                        iTotal += calc;  // running total of the card number as we loop - Do Nothing to first digit
                        i--;  // decrement the count - move to the next digit in the card
                        iCCN = iCCN / 10;                               // subtracts right most digit from ccNum
                        calc = parseInt(iCCN) % 10;    // NEXT right most digit
                        calc = calc * 2;                                 // multiply the digit by two
                        // Instead of some screwy method of converting 16 to a string and then parsing 1 and 6 and then adding them to make 7,
                        // I use a simple switch statement to change the value of calc2 to 7 if 16 is the multiple.
                        switch (calc) {
                            case 10: calc = 1; break;       //5*2=10 & 1+0 = 1
                            case 12: calc = 3; break;       //6*2=12 & 1+2 = 3
                            case 14: calc = 5; break;       //7*2=14 & 1+4 = 5
                            case 16: calc = 7; break;       //8*2=16 & 1+6 = 7
                            case 18: calc = 9; break;       //9*2=18 & 1+8 = 9
                            default: calc = calc;           //4*2= 8 &   8 = 8  -same for all lower numbers
                        }
                        iCCN = iCCN / 10;  // subtracts right most digit from ccNum
                        iTotal += calc;  // running total of the card number as we loop
                    }  // END OF LOOP
                    if ((iTotal % 10) == 0) {  // check to see if the sum Mod 10 is zero
                        bResult = true;  // This IS (or could be) a valid credit card number.
                    } else {
                        bResult = false;  // This could NOT be a valid credit card number
                    }
                }
            }
            return bResult; // Return the results
        },

        IsMatchRegularExpressionString: function (str, regax) {
            if (str.match(regax)) {
                return true;
            }
            else {
                return false;
            }
        },

        detectCardType: function (number) {

            var re = {
                electron: /^(4026|417500|4405|4508|4844|4913|4917)\d+$/,
                maestro: /^(5018|5020|5038|5612|5893|6304|6759|6761|6762|6763|0604|6390)\d+$/,
                dankort: /^(5019)\d+$/,
                interpayment: /^(636)\d+$/,
                unionpay: /^(62|88)\d+$/,
                visa: /^4[0-9]{12}(?:[0-9]{3})?$/,
                mastercard: /^5[1-5][0-9]{14}$/,
                amex: /^3[47][0-9]{13}$/,
                diners: /^3(?:0[0-5]|[68][0-9])[0-9]{11}$/,
                discover: /^6(?:011|5[0-9]{2})[0-9]{12}$/,
                jcb: /^(?:2131|1800|35\d{3})\d{11}$/
            };
            if (re.electron.test(number)) {
                return 'ELECTRON';
            } else if (re.maestro.test(number)) {
                return 'MAESTRO';
            } else if (re.dankort.test(number)) {
                return 'DANKORT';
            } else if (re.interpayment.test(number)) {
                return 'INTERPAYMENT';
            } else if (re.unionpay.test(number)) {
                return 'UNIONPAY';
            } else if (re.visa.test(number)) {
                return 'VISA';
            } else if (re.mastercard.test(number)) {
                return 'MASTERCARD';
            } else if (re.amex.test(number)) {
                return 'AMEX';
            } else if (re.diners.test(number)) {
                return 'DINERS';
            } else if (re.discover.test(number)) {
                return 'DISCOVER';
            } else if (re.jcb.test(number)) {
                return 'JCB';
            } else {
                return undefined;
            }
        },
        giftCard: function () {
            _debug("bind.giftcard()");

            // Close gift card after application
            $("#giftcard-action").on("click", ".close-giftcard", function (e) {
                e.preventDefault();
                $("#layout-checkout-payment .payment-method").hide();
                var defaultPayment = "#" + $("#payment-methods li").first().data("toggle");
                $(defaultPayment).show();

                $("#payment-methods li").removeClass("active").show();

                $("#payment-methods li a:eq(0)").click();
                scrollToTop(300, "#payment-container");
            });



            // Apply gift card, handles invalid and valid codes
            $("#payment-giftcard").on("click", ".giftcard-apply", function () {
                // Gets nearest gift card number
                // Need this because there are two gift card inputs after the first submit
                var form = $(this).closest(".form");
                var giftCardNumber = $("input[name='giftcard']").val();

                if (giftCardNumber == "" || giftCardNumber == null) {
                    isValid = false;
                    $("input[name='giftcard']").css({
                        "border": "1px solid red",
                        "background": "#FFCECE"
                    });
                }
                else {
                    $("input[name='giftcard']").css({
                        "border": "1px solid #C3C3C3",
                        "background": ""
                    });

                    // Apply card and get back new total details
                    Api.useGiftCard(giftCardNumber, function (response) {

                        // Is it a valid number?
                        if (response.success) {
                            $("#giftcard-initial").hide();

                            $("#GiftCardAmount").val(response.data.giftcardamount);

                            // If the gift card covers order total then offer button, else link to close results
                            if (response.data.paid) {
                                var template = $("#tmpl-giftcard-finish").html();
                            } else {
                                var template = $("#tmpl-giftcard-continue").html();
                            }


                            $("#dynamic-total").html(response.data.total);
                            var html = Mustache.render(template, response.data);

                            $("#giftcard-action").html(html);
                        } else {


                            $("form .error-msg").html('<div class="field-validation-error">' + response.message + '</div>');
                        }
                    });
                }
            });


        },
        // Toggle for billing address same-as
        billingAddress: function () {
            if ($("#UseSameAsShippingAddress").attr("checked") == "checked") {
                $("#billingaddressform .edit-address-form").toggle();
            }

            $("#UseSameAsShippingAddress").on("click", function () {
                $("#billingaddressform .edit-address-form").toggle();
            });
        },
        BillingSameAsShippingHandler: function () {

            if ($("#billingSameAsShippingAddress").is(':checked')) {
                $("#billingaddressform").hide();
            }

            $("#billingSameAsShippingAddress").on("click", function () {

                if ($("#billingSameAsShippingAddress").is(':checked')) {
                    $("#billingaddressform").hide();
                } else {
                    $("#billingaddressform").show();
                }
            });

        },
        // Get State by country
        setStateByCountry: function () {

            $(".address_country").on("change", function (e) {
                var countryId = $(this).attr('id');
                if (countryId.indexOf("ShippingAddressModel") > -1) {
                    Checkout.bind.ResetCityAndZipCodeShipping();
                }
                else {
                    Checkout.bind.ResetCityAndZipCodeBilling();
                }
                var stateId = countryId.replace("CountryCode", "StateCode");
                var countryCode = $(this).val();
                Api.getStateByCountryCode(countryCode, function (response) {
                    $('#' + stateId).empty();
                    for (var i = 0; i < response.length; i++) {
                        $("#" + stateId).append("<option value='" + response[i].Code + "'>" + response[i].Name + "</option>");
                    }
                    if (response.length < 1) {
                        $("#" + stateId).append("<option value=''>Please Select</option>");
                    }
                });
                if ($("#checkoutform").length > 0) {
                    if (countryCode) {
                        overlayforwaitblock();
                        $.ajax({
                            url: "/checkout/getupdatedcart?isCountryStateTax=true",
                            data: $("#checkoutform").serialize(),
                            type: 'POST',
                            success: function (data) {
                                overlayforwaitnone();
                                var cartData = $.parseHTML(data)
                                $(".cart-summary").html($(cartData).find('.cart-summary').html())

                            }
                        });
                    }
                }
            });
            if ($(".address_country").val() == "") {
                $('#ShippingAddressModel_StateCode').append("<option value=''>Please Select</option>");
            }
        },
        setDefaultState: function () {
            this.callAddressChange(0);
            this.callAddressChange(1);
        },
        callAddressChange: function (addressCnt) {
            var state = $($(".hdn_StateCode")[addressCnt]).val();
            if (state != undefined && state != null && state.length < 1) {
                $($(".address_country")[addressCnt]).change();
            }
        },
        // Get State by country
        setStateByCountryById: function () {
            $("#address_country").on("change", function (e) {
                var stateCode = $(this).val();
                Checkout.bind.ResetCityAndZipCode();
                Api.getStateByCountryCode(stateCode, function (response) {
                    $('#address_state').empty();
                    for (var i = 0; i < response.length; i++) {
                        $("#address_state").append("<option value='" + response[i].Code + "'>" + response[i].Name + "</option>");
                    }
                    if (response.length < 1) {
                        $("#address_state").append("<option value=''>Please Select</option>");
                    }
                });
            });
        },
        setDefaultStateById: function () {

            var state = $("#hdn_StateCode").val();
            if (state.length < 1) {
                $("#address_country").change();
            }
        },
        OnStateChange: function () {
            $(".address_state").on("change", function (e) {
                var stateId = $(this).attr('id');
                var state = $(".address_state").val();
                if (stateId.indexOf("ShippingAddressModel") > -1) {
                    Checkout.bind.ResetCityAndZipCodeShipping();
                }
                else {
                    Checkout.bind.ResetCityAndZipCodeBilling();
                }
                if ($("#checkoutform").length > 0) {
                    if (state) {
                        overlayforwaitblock();
                        $.ajax({
                            url: "/checkout/getupdatedcart?isCountryStateTax=true",
                            data: $("#checkoutform").serialize(),
                            type: 'POST',
                            success: function (data) {
                                overlayforwaitnone();
                                var cartData = $.parseHTML(data)
                                $(".cart-summary").html($(cartData).find('.cart-summary').html())

                            }
                        });
                    }
                }
            });
        },

        OnStateChangeForAddNewAddress: function () {
            $("#address_state").on("change", function (e) {
                Checkout.bind.ResetCityAndZipCode();
            });
        },
        ResetCityAndZipCodeBilling: function () {
            $('[name="ShippingBillingAddressViewModel.BillingAddressModel.City"]').val("");
            $('[name="ShippingBillingAddressViewModel.BillingAddressModel.PostalCode"]').val("");
            $('[name="BillingAddressModel.City"]').val("");
            $('[name="BillingAddressModel.PostalCode"]').val("");
            $("#ErrorMessage").html("");
        },
        ResetCityAndZipCodeShipping: function () {
            $('[name="ShippingBillingAddressViewModel.ShippingAddressModel.City"]').val("");
            $('[name="ShippingBillingAddressViewModel.ShippingAddressModel.PostalCode"]').val("");
            $('[name="ShippingAddressModel.City"]').val("");
            $('[name="ShippingAddressModel.PostalCode"]').val("");
            $("#ErrorMessage").html("");
            $('#address_city').val('');
            $('#address_zipcode').val('');
        },
        ResetCityAndZipCode: function () {
            $("#address_city").val("");
            $("#address_zipcode").val("");
        },
        ValidateEmail: function (email) {
            var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
            return expr.test(email);
        },

        SetCreditCardValidation: function () {
            var isValid = true;
            $('input[data-payment="number"],input[data-payment="exp-month"],input[data-payment="exp-year"],input[data-payment="cvc"]').each(function () {
                if ($.trim($(this).val()) == '') {
                    isValid = false;
                    $(this).css({
                        "border": "1px solid red",
                        "background": "#FFCECE"
                    });
                }
                else {
                    $(this).css({
                        "border": "1px solid #C3C3C3",
                        "background": ""
                    });
                }
            });
            if (!Checkout.bind.Mod10($('input[data-payment="number"]').val())) {
                isValid = false;
                $('#errornumber').show();
                $('input[data-payment="number"]').css({
                    "border": "1px solid red",
                    "background": "#FFCECE"
                });
            }

            if ($('input[data-payment="exp-month"]').val() > 12 || $('input[data-payment="exp-month"]').val() < 1) {
                isValid = false;
                $('#errormonth').show();
                $('input[data-payment="exp-month"]').css({
                    "border": "1px solid red",
                    "background": "#FFCECE"
                });
            }

            var currentYear = (new Date).getFullYear();
            var currentMonth = (new Date).getMonth() + 1;

            if ($('input[data-payment="exp-year"]').val() < currentYear) {
                isValid = false;
                $('#erroryear').show();
                $('input[data-payment="exp-year"]').css({
                    "border": "1px solid red",
                    "background": "#FFCECE"
                });
            }

            if ($('input[data-payment="exp-year"]').val() == currentYear && $('input[data-payment="exp-month"]').val() < currentMonth) {
                isValid = false;
                $('#errormonth').show();
                $('input[data-payment="exp-month"]').css({
                    "border": "1px solid red",
                    "background": "#FFCECE"
                });
                $('#erroryear').show();
                $('input[data-payment="exp-year"]').css({
                    "border": "1px solid red",
                    "background": "#FFCECE"
                });
            }

            if ($('input[data-payment="cvc"]').val().length < 3) {
                isValid = false;
                $('#errorcardnumber').show();
                $('input[data-payment="cvc"]').css({
                    "border": "1px solid red",
                    "background": "#FFCECE"
                });
            }
            return isValid;
        },

        PaymentProcess: function () {
            isValid = true;
            $('#status-message').remove();
            $("#ajaxProcessPayment").html('');
            if ($("#ajaxProcessPayment").html() == undefined) {
                $('body').append('<div id="ajaxProcessPayment"><p id="ajaxProcessPaymentMsg">Your payment is processing. Please wait and do not close this window</p></div>');
            }

            $("#ajaxProcessPayment").html('<p id="ajaxProcessPaymentMsg">Your payment is processing. Please wait and do not close this window</p>');
            $("#ajaxProcessPayment").dialog({
                title: 'Payment Application',
                resizable: false,
                modal: true,
                create: function () {
                    $(this).closest(".ui-dialog").addClass("ui-md-popup ui-popup-top");
                },
                closeOnEscape: false,
                open: function (event, ui) {
                    $("#ajaxProcessPayment").parent().find(" .ui-dialog-titlebar-close").hide();
                }
            });

            $.ajax({
                url: "validatesinglepagecheckout",
                type: 'post',
                data: $("#checkoutform").serialize(),
                success: function (data) {
                    if (!data.HasError) {
                        
                        var cardNumber = $("#ccpopUp [data-payment='number']").val();
                        var cardCvcNum = $("#ccpopUp [data-payment='cvc']").val();
                        var cardExpMonth = $("#ccpopUp [data-payment='exp-month']").val();
                        var cardExpYear = $("#ccpopUp [data-payment='exp-year']").val();

                        var guid = $('#GUID').val();

                        var discount = parseFloat($('#Discount').val()).toFixed(2);
                        var ShippingCost = parseFloat($('#ShippingCost').val()).toFixed(2);
                        var TaxCost = parseFloat($('#TaxCost').val()).toFixed(2);
                        var SubTotal = parseFloat($('#SubTotal').val()).toFixed(2);
                        var Total = parseFloat($('#Total').val()).toFixed(2);

                        var cardType = Checkout.bind.detectCardType(cardNumber);
                        var OrderId = 1;
                        var BillingFirstName = $('input[name="ShippingBillingAddressViewModel.BillingAddressModel.FirstName"]').val();
                        var BillingLastName = $('input[name="ShippingBillingAddressViewModel.BillingAddressModel.LastName"]').val();
                        var BillingName = $('input[name="ShippingBillingAddressViewModel.BillingAddressModel.Name"]').val();
                        var BillingStreetAddress1 = $('input[name="ShippingBillingAddressViewModel.BillingAddressModel.StreetAddress1"]').val();
                        var BillingStreetAddress2 = $('input[name="ShippingBillingAddressViewModel.BillingAddressModel.StreetAddress2"]').val();
                        var BillingCity = $('input[name="ShippingBillingAddressViewModel.BillingAddressModel.City"]').val();
                        var BillingStateCode = $('input[name="ShippingBillingAddressViewModel.BillingAddressModel.StateCode"]').val();
                        var BillingCountryCode = $('input[name="ShippingBillingAddressViewModel.BillingAddressModel.CountryCode"]').val();
                        var BillingPhoneNumber = $('input[name="ShippingBillingAddressViewModel.BillingAddressModel.PhoneNumber"]').val();
                        var BillingPostalCode = $('input[name="ShippingBillingAddressViewModel.BillingAddressModel.PostalCode"]').val();
                        var BillingEmailId = $('input[name="ShippingBillingAddressViewModel.EmailAddress"]').val();

                        if ($('#billingSameAsShippingAddress').is(":checked")) {
                            BillingFirstName = $('input[name="ShippingBillingAddressViewModel.ShippingAddressModel.FirstName"]').val();
                            BillingLastName = $('input[name="ShippingBillingAddressViewModel.ShippingAddressModel.LastName"]').val();
                            BillingName = $('input[name="ShippingBillingAddressViewModel.ShippingAddressModel.Name"]').val();
                            BillingStreetAddress1 = $('input[name="ShippingBillingAddressViewModel.ShippingAddressModel.StreetAddress1"]').val();
                            BillingStreetAddress2 = $('input[name="ShippingBillingAddressViewModel.ShippingAddressModel.StreetAddress2"]').val();
                            BillingCity = $('input[name="ShippingBillingAddressViewModel.ShippingAddressModel.City"]').val();
                            BillingStateCode = $('input[name="ShippingBillingAddressViewModel.ShippingAddressModel.StateCode"]').val();
                            BillingCountryCode = $('input[name="ShippingBillingAddressViewModel.ShippingAddressModel.CountryCode"]').val();
                            BillingPhoneNumber = $('input[name="ShippingBillingAddressViewModel.ShippingAddressModel.PhoneNumber"]').val();
                            BillingPostalCode = $('input[name="ShippingBillingAddressViewModel.ShippingAddressModel.PostalCode"]').val();
                        }

                        var GatewayCurrencyCode = $('#GatewayCurrencyCode').val();
                        var Gateway = $('#GatewayType').val();

                        var ReturnUrl = String.format(Checkout.GloabalVariables.ZnodeDemoPaymentSuccessUrl, $("#CCPaymentOptionId").val());
                        var CancelUrl = location.protocol + "//" + location.host + "/checkout/singlecheckout?";

                        var gatewayName = $("#hdnGatwayName").val();
                        var paymentAppGatewayName = gatewayName;
                        if (gatewayName != undefined && gatewayName.length > 0) {
                            var paymentAppGatewayName = gatewayName.toLowerCase();

                            if (gatewayName.toLowerCase() == 'authorize.net') {
                                paymentAppGatewayName = 'authorizenet';
                            }

                            else if (gatewayName.toLowerCase() == 'paymentech orbital') {
                                paymentAppGatewayName = 'paymenttech';
                            }
                            else if (gatewayName.toLowerCase() == 'worldpay') {
                                paymentAppGatewayName = 'securenet';
                            }
                        }

                        var payment = {
                            "GUID": guid,
                            "GatewayType": paymentAppGatewayName,
                            "GatewayCurrencyCode": GatewayCurrencyCode,
                            "OrderId": OrderId,
                            "BillingCity": BillingCity,
                            "BillingCountryCode": BillingCountryCode,
                            "BillingFirstName": BillingFirstName,
                            "BillingLastName": BillingLastName,
                            "BillingName": BillingName,
                            "BillingPhoneNumber": BillingPhoneNumber,
                            "BillingPostalCode": BillingPostalCode,
                            "BillingStateCode": BillingStateCode,
                            "BillingStreetAddress1": BillingStreetAddress1,
                            "BillingStreetAddress2": BillingStreetAddress2,
                            "BillingEmailId": BillingEmailId,
                            "ShippingCost": ShippingCost,
                            "SubTotal": SubTotal,
                            "TaxCost": TaxCost,
                            "Total": Total,
                            "Discount": discount,
                            "CardSecurityCode": cardCvcNum,
                            "CardNumber": cardNumber,
                            "CardExpirationMonth": cardExpMonth,
                            "CardExpirationYear": cardExpYear,
                            "CancelUrl": CancelUrl,
                            "ReturnUrl": ReturnUrl,
                            "CardType": cardType
                        };

                        $("#ccpopUp").hide();

                        submitCard(payment, function (response) {
                            if (response.Data == undefined) {
                                if (response.indexOf("Unauthorized") > 0) {

                                    $("#ajaxProcessPayment").html('');
                                    $("#ccpopUp [data-payment='number']").val('');
                                    $("#ccpopUp [data-payment='cvc']").val('');
                                    $("#ccpopUp [data-payment='exp-month']").val('');
                                    $("#ccpopUp [data-payment='exp-year']").val('');
                                    $("#ajaxProcessPayment").dialog('close');
                                    overlayforwaitnone();
                                    $('#ddlPaymentTypes').prop('selectedIndex', 0);
                                    message = response;

                                    if ($("#ajaxProcessPaymentError").html() == undefined) {
                                        $('body').append('<div id="ajaxProcessPaymentError"><p id="ajaxProcessPaymentErrorMsg">We were unable to process your credit card payment. <br /><br />Reason:<br />' + message + '<br /><br />If the problem persists, contact us to complete your order.</p></div>');
                                    } else {
                                        $("#ajaxProcessPaymentError").html('We were unable to process your credit card payment. <br /><br />Reason:<br />' + message + '<br /><br />If the problem persists, contact us to complete your order.');
                                    }

                                    if ($('#ajaxProcessPaymentError').html() != undefined) {
                                        $('#ajaxProcessPaymentError').dialog({
                                            title: 'Payment Application',
                                            resizable: false,
                                            modal: true,
                                            create: function () {
                                                $(this).closest('.ui-dialog').addClass('ui-md-popup ui-popup-top');
                                            },
                                            buttons: {
                                                Ok: function () {
                                                    $(this).dialog('close');
                                                }
                                            },
                                            closeOnEscape: false,
                                            open: function (event, ui) {
                                                $('#ajaxProcessPaymentError').parent().find(' .ui-dialog-titlebar-close').hide();
                                            }
                                        });
                                    }

                                    return false;
                                }
                            } else {
                                var isSuccess = response.Data.IsSuccess;
                                if (isSuccess) {
                                    var customerProfileId = response.Data.CustomerProfileId;
                                    var customerPaymentId = response.Data.CustomerPaymentProfileId;
                                    var paymentSettingId = $("#CCPaymentOptionId").val();
                                    Api.submitPayment(paymentSettingId, customerProfileId, customerPaymentId, function (response) {

                                        var newLocation = response.Data;
                                        if (newLocation != undefined && newLocation.toLowerCase().indexOf('missing card data') >= 0) {
                                            $('#ajaxProcessPayment').html('');
                                            $('#ccpopUp [data-payment="number"]').val('');
                                            $('#ccpopUp [data-payment="cvc"]').val('');
                                            $('#ccpopUp [data-payment="exp-month"]').val('');
                                            $('#ccpopUp [data-payment="exp-year"]').val('');
                                            $('#ajaxProcessPayment').dialog('close');
                                                overlayforwaitnone();
                                                $('#ddlPaymentTypes').prop('selectedIndex', 0);
                                            if ($('#ajaxProcessPaymentError').html() == undefined) {
                                                $('body').append('<div id="ajaxProcessPaymentError"><p id="ajaxProcessPaymentErrorMsg">Error occurred during processing an order. Order could not be placed as card data is missing.</p></div>');
                                            } else {
                                                $('#ajaxProcessPaymentError').html('Error occurred during processing an order. Order could not be placed as card data is missing.');
                                            }
                                        } else if (newLocation != undefined && newLocation.indexOf('Message=') >= 0) {
                                            var message = newLocation.substr(newLocation.indexOf('=') + 1);
                                            $('#ajaxProcessPayment').html('');
                                            $('#ccpopUp [data-payment="number"]').val('');
                                            $('#ccpopUp [data-payment="cvc"]').val('');
                                            $('#ccpopUp [data-payment="exp-month"]').val('');
                                            $('#ccpopUp [data-payment="exp-year"]').val('');
                                            $('#ajaxProcessPayment').dialog('close');
                                            overlayforwaitnone();
                                            $('#ddlPaymentTypes').prop('selectedIndex', 0);
                                            if ($('#ajaxProcessPaymentError').html() == undefined) {
                                                $('body').append('<div id="ajaxProcessPaymentError"><p id="ajaxProcessPaymentErrorMsg">We were unable to process your credit card payment. <br /><br />Reason:<br />' + message + '<br /><br />If the problem persists, contact us to complete your order.</p></div>');
                                            } else {
                                                $('#ajaxProcessPaymentError').html('We were unable to process your credit card payment. <br /><br />Reason:<br />' + message + '<br /><br />If the problem persists, contact us to complete your order.');
                                            }
                                        } else if (newLocation != undefined) {
                                            $('#ajaxProcessPaymentError').remove();
                                            window.location = newLocation;
                                        } else {
                                            $('#ccpopUp .ErrorMessage').html('');
                                            $('#ccpopUp [data-payment="number"]').val('');
                                            $('#ccpopUp [data-payment="cvc"]').val('');
                                            $('#ccpopUp [data-payment="exp-month"]').val('');
                                            $('#ccpopUp [data-payment="exp-year"]').val('');
                                            $('#ajaxProcessPayment').dialog('close');
                                                overlayforwaitnone();
                                                $('#ddlPaymentTypes').prop('selectedIndex', 0);
                                            if ($('#ajaxProcessPaymentError').html() == undefined) {
                                                $('body').append('<div id="ajaxProcessPaymentError"><p id="ajaxProcessPaymentErrorMsg">Error occurred during processing an order. Order could not be placed.</p></div>');
                                            } else {
                                                $('#ajaxProcessPaymentError').html('Error occurred during processing an order. Order could not be placed.');
                                            }
                                        }

                                        if ($('#ajaxProcessPaymentError').html() != undefined) {
                                            $('#ajaxProcessPaymentError').dialog({
                                                title: 'Payment Application',
                                                resizable: false,
                                                modal: true,
                                                create: function () {
                                                    $(this).closest('.ui-dialog').addClass('ui-md-popup ui-popup-top');
                                                },
                                                buttons: {
                                                    Ok: function () {
                                                        $(this).dialog('close');
                                                    }
                                                },
                                                closeOnEscape: false,
                                                open: function (event, ui) {
                                                    $('#ajaxProcessPaymentError').parent().find(' .ui-dialog-titlebar-close').hide();
                                                }
                                            });
                                        }

                                        return false;
                                    });
                                }
                                else {
                                    var message = response.Data.ResponseText;
                                    if (message == undefined) {
                                        message = response.Data.GatewayResponseData;
                                        if (message == undefined) {
                                            message = response.Data;
                                        }
                                    }
                                    $("#ajaxProcessPayment").html('');
                                    $("#ccpopUp [data-payment='number']").val('');
                                    $("#ccpopUp [data-payment='cvc']").val('');
                                    $("#ccpopUp [data-payment='exp-month']").val('');
                                    $("#ccpopUp [data-payment='exp-year']").val('');
                                    $("#ajaxProcessPayment").dialog('close');
                                    overlayforwaitnone();
                                    $('#ddlPaymentTypes').prop('selectedIndex', 0);
                                    if (message.length > 0 && message != null) {
                                        if ($("#ajaxProcessPaymentError").html() == undefined) {
                                            $('body').append('<div id="ajaxProcessPaymentError"><p id="ajaxProcessPaymentErrorMsg">We were unable to process your credit card payment. <br /><br />Reason:<br />' + message + '<br /><br />If the problem persists, contact us to complete your order.</p></div>');
                                        } else {
                                            $("#ajaxProcessPaymentError").html('We were unable to process your credit card payment. <br /><br />Reason:<br />' + message + '<br /><br />If the problem persists, contact us to complete your order.');
                                        }
                                    }
                                    else {
                                        if ($("#ajaxProcessPaymentError").html() == undefined) {
                                            $('body').append('<div id="ajaxProcessPaymentError"><p id="ajaxProcessPaymentErrorMsg">We were unable to process your credit card payment. <br /><br /><br />If the problem persists, contact us to complete your order.</p></div>');
                                        } else {
                                            $("#ajaxProcessPaymentError").html('We were unable to process your credit card payment. <br /><br /><br />If the problem persists, contact us to complete your order.');
                                        }
                                    }
                                    if ($('#ajaxProcessPaymentError').html() != undefined) {
                                        $('#ajaxProcessPaymentError').dialog({
                                            title: 'Payment Application',
                                            resizable: false,
                                            modal: true,
                                            create: function () {
                                                $(this).closest('.ui-dialog').addClass('ui-md-popup ui-popup-top');
                                            },
                                            buttons: {
                                                Ok: function () {
                                                    $(this).dialog('close');
                                                }
                                            },
                                            closeOnEscape: false,
                                            open: function (event, ui) {
                                                $('#ajaxProcessPaymentError').parent().find(' .ui-dialog-titlebar-close').hide();
                                            }
                                        });
                                    }
                                }
                                return false;
                            }
                        });
                    }
                    else {
                        if (data.ErrorMessage == "Logout") {
                            window.location.href = "/cart/index";
                        }
                        $("#ErrorMessage").html(data.ErrorMessage);
                        $("#ajaxBusy").dialog('close');
                    }
                },

                error: function (jqXHR, textStatus, errorThrown) {

                }

            });
        },

        PayPalPaymentProcess: function () {
            isValid = true;
            $('#status-message').remove();
            $("#ErrorMessage").html('');
            $("#ajaxProcessPayment").html('');
            if ($("#ajaxProcessPayment").html() == undefined) {
                $('body').append('<div id="ajaxProcessPayment"><p id="ajaxProcessPaymentMsg">Your payment is processing. Please wait and do not close this window</p></div>');
            }

            $("#ajaxProcessPayment").html('<p id="ajaxProcessPaymentMsg">Your payment is processing. Please wait and do not close this window</p>');
            $("#ajaxProcessPayment").dialog({
                title: 'Payment Application',
                resizable: false,
                modal: true,
                create: function () {
                    $(this).closest(".ui-dialog").addClass("ui-md-popup ui-popup-top");
                },
                closeOnEscape: false,
                open: function (event, ui) {
                    $("#ajaxProcessPayment").parent().find(" .ui-dialog-titlebar-close").hide();
                }
            });

            var paymentSettingId = $('#PaymentsViewModel_PaymentPaypalOptionId').val();
            var ReturnUrl = String.format(Checkout.GloabalVariables.ZnodeDemoPaymentSuccessUrl, paymentSettingId);
            var CancelUrl = location.protocol + "//" + location.host + "/checkout/singlecheckout?";
            Api.ProcessPayPalPayment(paymentSettingId, ReturnUrl, CancelUrl, function (response) {
                var message = response;
                if (message != undefined && message.indexOf('Message=') >= 0) {
                    var errorMessage = message.substr(message.indexOf('=') + 1);                    
                    $("#ajaxBusy").dialog('close');
                    $('#SuccessMessage').show();
                    $("#ajaxProcessPayment").dialog('close');
                    $("#SuccessMessage").append('<div id="status-message" class="alert alert-danger text-center">' + errorMessage + '</div>');
                } else if (message.indexOf("http") != -1) {
                    $("#ajaxBusy").dialog('close');
                    window.location = response;
                }
                else {
                    $("#ajaxBusy").dialog('close');
                    $('#SuccessMessage').show();
                    $("#ajaxProcessPayment").dialog('close');
                    $("#SuccessMessage").append('<div id="status-message" class="alert alert-danger text-center">' + message + '</div>');
                }
            });
        },

        singleCheckout: function () {

            $("#giftcard_number").keypress(function (event) {
                if (event.which == 13) {
                    event.preventDefault();
                    $(".singlecheckout-giftcard-apply").focus();
                }
            });
            $('#login_password').on("blur", function (e) {

                var password = $('#login_password').val();
                var regex = $('#login_password').attr('data-val-regex-pattern');
                if (password.trim().length > 0) {
                    $("#valPassword").text('').removeClass("field-validation-error");
                    $("#valPassword").hide();
                    if (Checkout.bind.IsMatchRegularExpressionString(password, regex) == false) {
                        $("#valPassword").text("Password must be at least 8 alphanumeric characters and contain at least one number").addClass("error-msg");
                        $("#valPassword").show();
                        return false;
                    }
                }
                return true;
            });


            $('[name="ShippingBillingAddressViewModel.ShippingAddressModel.City"]').on("focusout", function (e) {
                if ($('[name="ShippingBillingAddressViewModel.ShippingAddressModel.PostalCode"]').val() != "") {
                    overlayforwaitblock();
                    $.ajax({
                        url: "/checkout/getupdatedcart?isCountryStateTax=true",
                        data: $("#checkoutform").serialize(),
                        type: 'POST',
                        success: function (data) {
                            overlayforwaitnone();
                            var cartData = $.parseHTML(data)
                            $(".cart-summary").html($(cartData).find('.cart-summary').html())

                        }
                    });
                }
            });


            $('[name="ShippingBillingAddressViewModel.ShippingAddressModel.PostalCode"]').on("focusout", function (e) {
                if ($('[name="ShippingBillingAddressViewModel.ShippingAddressModel.City"]').val() != "") {
                    overlayforwaitblock();
                    $.ajax({
                        url: "/checkout/getupdatedcart?isCountryStateTax=true",
                        data: $("#checkoutform").serialize(),
                        type: 'POST',
                        success: function (data) {
                            overlayforwaitnone();
                            $('#status-message').remove();
                            var cartData = $.parseHTML(data)
                            $(".cart-summary").html($(cartData).find('.cart-summary').html())
                            var errorMessage = $(cartData).find('#status-message').html();
                            if (errorMessage != undefined && errorMessage != "") {
                                $("#SuccessMessage").show();

                                $("#SuccessMessage").append('<div id="status-message" class="alert alert-danger text-center">' + errorMessage + '</div>');
                            }
                            else {
                                $('#status-message').remove();
                            }
                        }
                    });
                }
            });

            $('#ccpopUp').hide();
            $('#ddlPaymentTypes').change(function () {
                var paymentType = $("#ddlPaymentTypes option:selected").text();
                var paymentTypeId = $('#ddlPaymentTypes').val()
                $("#singleCheckoutSubmitPayment").show();
                $('#purchase-order-number').hide();
                $("#div-paypal-express-checkout").hide();
                if (paymentType.toLowerCase() != 'select payment') {
                    $("#ddlPaymentTypes").css({
                        "border": "1px solid #C3C3C3",
                        "background": ""
                    });
                }
                if (paymentType.toLowerCase() == 'credit card') {
                    $("#hdnGatwayName").val('');

                    if ($("#CCPaymentOptionId").val().length > 0) {

                        $("#ajaxProcessPayment").html('');
                        if ($("#ajaxProcessPayment").html() == undefined) {
                            $('body').append('<div id="ajaxProcessPayment"><p id="ajaxProcessPaymentMsg">Please wait while we load the payment screen. Do not close this window.</p></div>');
                        }

                        $("#ajaxProcessPayment").html('<p id="ajaxProcessPaymentMsg">Please wait while we load the payment screen. Do not close this window.</p>');
                        $("#ajaxProcessPayment").dialog({
                            title: 'Payment Application',
                            resizable: false,
                            modal: true,
                            create: function () {
                                $(this).closest(".ui-dialog").addClass("ui-md-popup ui-popup-top");
                            },
                            closeOnEscape: false,
                            open: function (event, ui) {
                                $("#ajaxProcessPayment").parent().find(" .ui-dialog-titlebar-close").hide();
                            }
                        });

                        Api.GetPaymentGatwayName($("#CCPaymentOptionId").val(), function (response) {
                            if (response.success) {
                                $("#hdnGatwayName").val(response.name);
                                $("#paymentProfileId").val(response.profileId);

                                var gatewayName = $("#hdnGatwayName").val();
                                if (gatewayName != undefined && gatewayName.length > 0) {
                                    var paymentAppGatewayName = gatewayName.toLowerCase();

                                    if (gatewayName.toLowerCase() == 'authorize.net') {
                                        paymentAppGatewayName = 'authorizenet';
                                    }

                                    if (gatewayName.toLowerCase() == 'paymentech orbital') {
                                        paymentAppGatewayName = 'paymenttech';
                                    }

                                    var paymentUrl = Checkout.GloabalVariables.PaymentApplicationJSCall;
                                    var twoCOUrl = String.format(Checkout.GloabalVariables.ZnodeDemoPaymentTwoCoUrl, $("#CCPaymentOptionId").val());
                                    paymentUrl = String.format(paymentUrl, paymentAppGatewayName, $("#paymentProfileId").val(), twoCOUrl);
                                   
                                    $.ajax({
                                        url: paymentUrl,
                                        dataType: "script",
                                        success: function (response) {
                                            $("#ajaxProcessPayment").dialog('close');
                                            Checkout.bind.SetCreditCardValidations();
                                            $("#ccpopUp").show();
                                            $("#ccpopUp [data-payment='number']").focus();
                                        },
                                        error: function () {
                                            $("#ajaxProcessPayment").dialog('close');
                                            $("#ccpopUp .ErrorMessage").html('');
                                            $("#ccpopUp .ErrorMessage").html('Unable to contact payment application.');
                                        }
                                    });

                                } else {
                                    $("#ajaxProcessPayment").html('');
                                    $("#ajaxProcessPayment").html('Could not procced with payment as no payment gateway available with this selection.');
                                }
                                return false;
                            } else {
                                $("#ccpopUp .ErrorMessage").html('');
                                $("#ajaxProcessPayment").html('Could not procced with payment as no payment gateway available with this selection.');
                            }
                        });
                    }
                } else if (paymentTypeId == '2') {
                    $('#ccpopUp').hide();
                    $('#purchase-order-number').show();
                }
                else if (paymentTypeId == '3') {
                    $('#ccpopUp').hide();
                    $("#singleCheckoutSubmitPayment").hide();
                    $('#div-paypal-express-checkout').show();
                }
                else {
                    $('#ccpopUp').hide();
                }

            });

            $("#ShippingBillingAddressViewModel_ShippingAddressModel_CountryCode").change(function () {
                if ($("#ShippingBillingAddressViewModel_ShippingAddressModel_CountryCode").val().trim().length > 0) {
                    $.ajax({
                        url: "/checkout/getupdatedcart?isCountryStateTax=true",
                        data: $("#checkoutform").serialize(),
                        type: 'POST',
                        success: function (data) {
                            $("#SingleCheckoutCartItem").html(data);
                        }
                    });
                }
            });

            $("#singlepage-checkout-shipping-content [name=ShippingOptions]").on('change', function (event) {
                $('#status-message').remove();
                var value = $("input[name='ShippingOptions']:checked").val();
                var isValidAddress = true;
                $('input[name="ShippingBillingAddressViewModel.ShippingAddressModel.PostalCode"],input[name="ShippingBillingAddressViewModel.ShippingAddressModel.City"]').each(function () {
                    if ($.trim($(this).val()) == '') {
                        isValidAddress = false;
                        $(this).css({
                            "border": "1px solid red",
                            "background": "#FFCECE"
                        });
                    }
                    else {
                        $(this).css({
                            "border": "1px solid #C3C3C3",
                            "background": ""
                        });
                    }
                });

                var country = $('select[name="ShippingBillingAddressViewModel.ShippingAddressModel.CountryCode"] option:selected').val();
                if (country == "0" || country == "") {
                    isValidAddress = false;
                    $('select[name="ShippingBillingAddressViewModel.ShippingAddressModel.CountryCode"]').css({
                        "border": "1px solid red",
                        "background": "#FFCECE"
                    });
                }
                else {
                    $('select[name="ShippingBillingAddressViewModel.ShippingAddressModel.CountryCode"]').css({
                        "border": "1px solid #c3c3c3",
                        "background": ""
                    });
                }
                if (isValidAddress == false) {
                    event.preventDefault();
                }
                else {
                    overlayforwaitblock();
                    $("#SuccessMessage").show();
                    $.ajax({
                        url: "/checkout/getupdatedcart",
                        data: $("#checkoutform").serialize(),
                        type: 'POST',
                        success: function (data) {
                            overlayforwaitnone();
                            var cartData = $.parseHTML(data)
                            $(".cart-summary").html($(cartData).find('.cart-summary').html())
                            var errorMessage = $(cartData).find('#status-message').html();
                            if (errorMessage != undefined && errorMessage != "") {
                                $("#SuccessMessage").append('<div id="status-message" class="alert alert-danger text-center">' + errorMessage + '</div>');
                                $(cartData).find('#status-message').html('');
                            }
                            else {
                                if (errorMessage == undefined) {
                                    $("#SuccessMessage").hide();
                                    $("#SuccessMessage").html('');
                                }
                            }
                        }
                    });
                }
            });

            $("#btnApplyCoupon").on('click', function () {
                var couponCode = $("input[name='Coupon']").val();
                var url = "/checkout/applydiscount";
                Checkout.bind.ApplyPromotion(couponCode, url);
            });

            $(".singlecheckout-giftcard-apply").on('click', function () {
                var value = $("input[name='giftcard']").val();
                if (value == "" || value == null) {
                    isValid = false;
                    $("input[name='giftcard']").css({
                        "border": "1px solid red",
                        "background": "#FFCECE"
                    });
                }
                else {
                    $("input[name='giftcard']").css({
                        "border": "1px solid #C3C3C3",
                        "background": ""
                    });

                    $.ajax({
                        url: "/checkout/applygiftcard",
                        data: {
                            "number": value
                        },
                        type: 'GET',
                        success: function (response) {
                            if (response.success) {
                                $('#errorgiftcard').hide();
                                $("#giftcard-initial").hide();
                                $("#GiftCardAmount").val(response.data.giftcardamount);
                                // If the gift card covers order total then offer button, else link to close results
                                if (response.data.paid) {
                                    var template = $("#tmpl-giftcard-finish").html();
                                } else {
                                    var template = $("#tmpl-giftcard-continue").html();
                                }


                                $("#dynamic-order-total").find("h2").html(response.data.total);
                                var html = Mustache.render(template, response.data);

                                $("#giftcard-action").html(html);
                                if (response.data.total == "$0.00") {
                                    $("#TotalAmount").val(response.data.total)
                                }

                            } else {
                                isValid = false;
                                $('#errorgiftcard').show();
                                $("#errorgiftcard").html('<div class="field-validation-error">' + response.message + '</div>').addClass("error-msg");
                            }
                        }
                    });
                }

            });

            $('#singleCheckoutSubmitPayment').on("click", function (e) {
                $('#status-message').remove()
                var isValid = true;
                e.preventDefault();
                if ($("#ddlPaymentTypes option:selected").text().toLowerCase() == 'select payment') {
                    isValid = false;
                    $("#ddlPaymentTypes").css({
                        "border": "1px solid red",
                        "background": "#FFCECE url('../../../Images/drop-arrow.png') no-repeat 98% center"
                    });
                }
                $('input[name="ShippingBillingAddressViewModel.ShippingAddressModel.FirstName"],input[name="ShippingBillingAddressViewModel.ShippingAddressModel.LastName"],input[name="ShippingBillingAddressViewModel.ShippingAddressModel.StreetAddress1"],input[name="ShippingBillingAddressViewModel.EmailAddress"],input[name="ShippingBillingAddressViewModel.ShippingAddressModel.PostalCode"],input[name="ShippingBillingAddressViewModel.ShippingAddressModel.City"],input[name="ShippingBillingAddressViewModel.ShippingAddressModel.PhoneNumber"]').each(function () {
                    if ($.trim($(this).val()) == '') {
                        isValid = false;
                        $(this).css({
                            "border": "1px solid red",
                            "background": "#FFCECE"
                        });
                    }
                    else {
                        $(this).css({
                            "border": "1px solid #C3C3C3",
                            "background": ""
                        });
                    }
                });

                var country = $('select[name="ShippingBillingAddressViewModel.ShippingAddressModel.CountryCode"] option:selected').val();
                if (country == "0" || country == "") {
                    isValid = false;
                    $('select[name="ShippingBillingAddressViewModel.ShippingAddressModel.CountryCode"]').css({
                        "border": "1px solid red",
                        "background": "#FFCECE url('../../../Images/drop-arrow.png') no-repeat 98% center"
                    });
                }
                else {
                    $('select[name="ShippingBillingAddressViewModel.ShippingAddressModel.CountryCode"]').css({
                        "border": "1px solid #C3C3C3",
                        "background": ""
                    });
                }

                if ($('#CreateLogin').is(":checked")) {
                    $('input[name="ShippingBillingAddressViewModel.LoginViewModel.Username"],input[name="ShippingBillingAddressViewModel.LoginViewModel.Password"]').each(function () {
                        if ($.trim($(this).val()) == '') {
                            isValid = false;
                            $(this).css({
                                "border": "1px solid red",
                                "background": "#FFCECE"
                            });
                        }
                        else {
                            $(this).css({
                                "border": "1px solid #C3C3C3",
                                "background": ""
                            });
                        }
                    });
                }

                if ($('input[name = "ShippingBillingAddressViewModel.EmailAddress"]').val() == "" || !Checkout.bind.ValidateEmail($('input[name = "ShippingBillingAddressViewModel.EmailAddress"]').val())) {
                    isValid = false;
                    $('input[name = "ShippingBillingAddressViewModel.EmailAddress"]').css({
                        "border": "1px solid red",
                        "background": "#FFCECE"
                    });
                }
                else {
                    $('input[name = "ShippingBillingAddressViewModel.EmailAddress"]').css({
                        "border": "1px solid #C3C3C3",
                        "background": ""
                    });
                }

                if (!($('#billingSameAsShippingAddress').is(":checked"))) {
                    $('input[name="ShippingBillingAddressViewModel.BillingAddressModel.FirstName"],input[name="ShippingBillingAddressViewModel.BillingAddressModel.LastName"],input[name="ShippingBillingAddressViewModel.BillingAddressModel.StreetAddress1"],input[name="ShippingBillingAddressViewModel.BillingAddressModel.PostalCode"],input[name="ShippingBillingAddressViewModel.BillingAddressModel.City"]').each(function () {
                        if ($.trim($(this).val()) == '') {
                            isValid = false;
                            $(this).css({
                                "border": "1px solid red",
                                "background": "#FFCECE"
                            });
                        }
                        else {
                            $(this).css({
                                "border": "1px solid #C3C3C3",
                                "background": ""
                            });
                        }
                    });

                    var country = $('select[name="ShippingBillingAddressViewModel.BillingAddressModel.CountryCode"] option:selected').val();
                    if (country == "0" || country == "") {
                        isValid = false;
                        $('select[name="ShippingBillingAddressViewModel.BillingAddressModel.CountryCode"]').css({
                            "border": "1px solid red",
                            "background": "#FFCECE url('../../../Images/drop-arrow.png') no-repeat 98% center"
                        });
                    }
                    else {
                        $('select[name="ShippingBillingAddressViewModel.BillingAddressModel.CountryCode"]').css({
                            "border": "1px solid #C3C3C3",
                            "background": ""
                        });
                    }
                }

                if ($('#ddlPaymentTypes').val() == "2") {
                    var purchaseOrderNumber = $('#txtPurchaseOrderNumber').val().trim();
                    if (purchaseOrderNumber.length < 1) {
                        isValid = false;
                        $('#errorpurchaseorder').show();
                        $('#txtPurchaseOrderNumber').css({
                            "border": "1px solid red",
                            "background": "#FFCECE"
                        });
                    }
                    else {
                        $('#txtPurchaseOrderNumber').css({
                            "border": "1px solid #C3C3C3",
                            "background": ""
                        });
                    }
                }

                if ($('#ddlPaymentTypes').val() != "1" || ($("#TotalAmount").val() == "$0.00")) {
                    if (isValid) {
                        $('#status-message').remove();
                        if ($("#ajaxProcessPayment").html() == undefined) {
                            $('body').append('<div id="ajaxProcessPayment"><p id="ajaxProcessPaymentMsg">Please wait while processing and do not close this window</p></div>');
                        }

                        $("#ajaxProcessPayment").html('<p id="ajaxProcessPaymentMsg">Please wait while processing and do not close this window</p>');
                        $("#ajaxProcessPayment").dialog({
                            title: 'Order Processing',
                            resizable: false,
                            modal: true,
                            create: function () {
                                $(this).closest(".ui-dialog").addClass("ui-md-popup ui-popup-top");
                            },
                            closeOnEscape: false,
                            open: function (event, ui) {
                                $("#ajaxProcessPayment").parent().find(" .ui-dialog-titlebar-close").hide();
                            }
                        });
                        if ($('#CreateLogin').is(":checked")) {
                            $.ajax({
                                url: "createsinglecheckoutaccount",
                                type: 'post',
                                data: $("#checkoutform").serialize(),
                                success: function (data) {
                                    if (data.HasError) {
                                        $("#SuccessMessage").append('<div id="status-message" class="alert alert-danger text-center">' + data.ErrorMessage + '</div>');
                                        isValid = false;
                                        $("#ajaxProcessPayment").dialog('close');
                                        $(window).scrollTop(0);
                                        return isValid;
                                    }
                                    else {
                                        $('#status-message').remove();
                                        $.ajax({
                                            url: "submitsinglecheckout",
                                            type: 'post',
                                            data: $("#checkoutform").serialize(),
                                            success: function (data) {
                                                if (!data.HasError) {
                                                    var form = $('<form action="CheckoutReceipt" method="post">' +
                                                        '<input type="hidden" name="EmailId" value="' + data.EmailId + '" />' +
                                                        '<input type="hidden" name="OrderId" value="' + data.OrderId + '" />' +
                                                    '</form>');
                                                    $('body').append(form);
                                                    $(form).submit();
                                                }
                                                else {
                                                    var isError = data.ErrorMessage;
                                                    if (data.ErrorMessage == "Logout") {
                                                        window.location.href = "/cart/index";
                                                    }
                                                    if (isError != "" && isError != null) {
                                                        window.location = "/checkout/singlecheckout?message=" + data.ErrorMessage;
                                                    }

                                                    $("#ajaxProcessPayment").dialog('close');
                                                }
                                            },
                                            error: function (xhr, ajaxOptions, thrownError) {
                                                $("#ajaxProcessPayment").dialog('close');
                                                $("#SuccessMessage").append('<div id="status-message" class="alert alert-danger text-center">' + data.ErrorMessage + '</div>');
                                            },
                                        });
                                    }
                                }
                            })
                        }
                        else {
                            if (!$('#CreateLogin').is(":checked") && !$("#divnewLogin").is(':visible')) {
                                $('#status-message').remove();
                                $.ajax({
                                    url: "isvalidguestuser",
                                    type: 'post',
                                    data: $("#checkoutform").serialize(),
                                    success: function (data) {
                                        if (data.HasError) {
                                            isValid = false;
                                            $("#ajaxProcessPayment").dialog('close');
                                            $("#SuccessMessage").append('<div id="status-message" class="alert alert-danger text-center">' + data.ErrorMessage + '</div>');
                                            $(window).scrollTop(0);
                                        }
                                        else {
                                            isValid = true;
                                            $.ajax({
                                                url: "submitsinglecheckout",
                                                type: 'post',
                                                data: $("#checkoutform").serialize(),
                                                success: function (data) {
                                                    if (!data.HasError) {
                                                        var form = $('<form action="CheckoutReceipt" method="post">' +
                                                    '<input type="hidden" name="EmailId" value="' + data.EmailId + '" />' +
                                                    '<input type="hidden" name="OrderId" value="' + data.OrderId + '" />' +
                                                    '</form>');
                                                        $('body').append(form);
                                                        $(form).submit();
                                                    }
                                                    else {

                                                        if (data.ErrorMessage == "Logout") {
                                                            window.location.href = "/cart/index";
                                                        }
                                                        $("#ajaxProcessPayment").dialog('close');
                                                        $("#SuccessMessage").append('<div id="status-message" class="alert alert-danger text-center">' + data.ErrorMessage + '</div>');
                                                        $(window).scrollTop(0);
                                                    }
                                                },
                                                error: function (xhr, ajaxOptions, thrownError) {
                                                    $("#ajaxProcessPayment").dialog('close');
                                                    $("#SuccessMessage").append('<div id="status-message" class="alert alert-danger text-center">' + data.ErrorMessage + '</div>');
                                                },
                                            });
                                        }
                                    },
                                });
                            }
                            else {
                                $('#status-message').remove();
                                $.ajax({
                                    url: "submitsinglecheckout",
                                    type: 'post',
                                    data: $("#checkoutform").serialize(),
                                    success: function (data) {
                                        if (!data.HasError) {
                                            var form = $('<form action="CheckoutReceipt" method="post">' +
                                        '<input type="hidden" name="EmailId" value="' + data.EmailId + '" />' +
                                        '<input type="hidden" name="OrderId" value="' + data.OrderId + '" />' +
                                        '</form>');
                                            $('body').append(form);
                                            $(form).submit();
                                        }
                                        else {

                                            if (data.ErrorMessage == "Logout") {
                                                window.location.href = "/cart/index";
                                            }
                                            $("#ajaxProcessPayment").dialog('close');
                                            $("#SuccessMessage").append('<div id="status-message" class="alert alert-danger text-center">' + data.ErrorMessage + '</div>');
                                        }
                                    },
                                    error: function (xhr, ajaxOptions, thrownError) {
                                        $("#ajaxProcessPayment").dialog('close');
                                        $("#SuccessMessage").append('<div id="status-message" class="alert alert-danger text-center">' + data.ErrorMessage + '</div>');
                                    },
                                });
                            }
                        }
                    }
                }
                else {
                    $('#status-message').remove()
                    if (isValid == false) {
                        e.preventDefault();
                    }
                    else {
                        isValid = Checkout.bind.SetCreditCardValidation();
                    }
                    if (isValid) {
                        overlayforwaitblock();
                        $('#status-message').remove();
                        if ($('#CreateLogin').is(":checked")) {
                            $.ajax({
                                url: "createcreditcarduseraccount",
                                type: 'post',
                                data: $("#checkoutform").serialize(),
                                success: function (data) {
                                    overlayforwaitnone();
                                    if (data.HasError) {
                                        if (data.ErrorMessage != undefined && data.ErrorMessage != "" && data.ErrorMessage != null) {
                                            window.location = "/checkout/singlecheckout?message=" + data.ErrorMessage;
                                        }
                                        isValid = false;
                                        return isValid;
                                    }
                                    else {
                                        $.ajax({
                                            url: "/checkout/getupdatedcart",
                                            data: $("#checkoutform").serialize(),
                                            type: 'POST',
                                            success: function (data) {
                                                overlayforwaitnone();
                                                var cartData = $.parseHTML(data)
                                                $(".cart-summary").html($(cartData).find('.cart-summary').html())
                                                var errorMessage = $(cartData).find('#status-message').html();
                                                if (errorMessage != undefined && errorMessage != "") {
                                                    $("#ajaxProcessPayment").dialog('close');
                                                    isValid = false;
                                                    e.preventDefault();
                                                    $("#SuccessMessage").show();
                                                    $("#SuccessMessage").append('<div id="status-message" class="alert alert-danger text-center">' + errorMessage + '</div>');
                                                    if (errorMessage != undefined && errorMessage != "" && errorMessage != null) {
                                                        window.location = "/checkout/singlecheckout?message=" + errorMessage;
                                                    }
                                                    $(window).scrollTop(0);
                                                }
                                                else {
                                                    Checkout.bind.PaymentProcess();
                                                }
                                            }
                                        });
                                    }
                                }
                            })
                        }
                        else {
                            if (!$('#CreateLogin').is(":checked") && !$("#divnewLogin").is(':visible')) {
                                $.ajax({
                                    url: "isvalidguestuser",
                                    type: 'post',
                                    data: $("#checkoutform").serialize(),
                                    success: function (data) {
                                        overlayforwaitnone();
                                        if (data.HasError) {
                                            isValid = false;
                                            $("#ajaxProcessPayment").dialog('close');
                                            $("#SuccessMessage").append('<div id="status-message" class="alert alert-danger text-center">' + data.ErrorMessage + '</div>');
                                            $(window).scrollTop(0);
                                        }
                                        else {
                                            isValid = true;
                                            if ($('#ddlPaymentTypes').val() == "1") {
                                                $.ajax({
                                                    url: "/checkout/getupdatedcart",
                                                    data: $("#checkoutform").serialize(),
                                                    type: 'POST',
                                                    success: function (data) {
                                                        overlayforwaitblock();
                                                        var cartData = $.parseHTML(data)
                                                        $(".cart-summary").html($(cartData).find('.cart-summary').html())
                                                        var errorMessage = $(cartData).find('#status-message').html();
                                                        if (errorMessage != undefined && errorMessage != "") {
                                                            $("#ajaxProcessPayment").dialog('close');
                                                            isValid = false;
                                                            e.preventDefault();
                                                            $("#SuccessMessage").append('<div id="status-message" class="alert alert-danger text-center">' + errorMessage + '</div>');
                                                            $(window).scrollTop(0);
                                                        }
                                                        else {
                                                            overlayforwaitnone();
                                                            Checkout.bind.PaymentProcess();
                                                        }
                                                    }
                                                });
                                            }
                                        }
                                    },
                                });
                            }
                        }
                    }
                }
            });
        },

        ApplyPromotion: function (couponCode, url) {
            Api.applyDiscount(url, couponCode, function (response) {
                $('#layout-cart').html("");
                $('#layout-cart').html(response.html);

                var coupons = response.data;
                htmlString = "<div class='col-xs-12 nopadding'>";
                for (var couponIndex = 0; couponIndex < coupons.length; couponIndex++) {
                    var style = coupons[couponIndex].CouponApplied ? "success-msg" : "error-msg";
                    var message = coupons[couponIndex].CouponMessage;
                    var couponCode = coupons[couponIndex].Coupon;
                    htmlString = htmlString + "<p class='" + style + "'>" + "<b>" + couponCode + "</b>" + " - " + message + "</p>";
                }
                htmlString = htmlString + "</div>";

                $("#couponMessageContainer").html("");
                $("#couponMessageContainer").html(htmlString);
            });
        },

        ShowAppliedPromotion: function () {
            var couponCode = $("input[name='Coupon']").val();
            var url = "/checkout/applydiscount";
            if (couponCode != null && couponCode != "") {
                Checkout.bind.ApplyPromotion(couponCode, url);
            }
        },

        RemoveDuplicateMessage: function () {
            var errorMessageDiv = $("#ErrorMessage").html();
            var statusMessageDiv = $("#status-message").html();
            if (typeof statusMessageDiv != 'undefined' && typeof errorMessageDiv != 'undefined') {
                if ((errorMessageDiv != null && errorMessageDiv != "") && (statusMessageDiv != null && statusMessageDiv != "")) {
                    $("#ErrorMessage").hide();
                }
            }
        }

    }
}
