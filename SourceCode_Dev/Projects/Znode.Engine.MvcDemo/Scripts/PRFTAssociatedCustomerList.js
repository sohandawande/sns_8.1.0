﻿$(document).ready(function () {
   
    //Binding Cancel Button Click Event functionality.
    $("#cancelBtn").on("click", function (e) {                   
        Api.Logout(function (response) {                                       
            $('.dialog').remove();
            window.location.href = response.link;
            });           
    });

    //Binding OK Button click Event functionality.
    $("#LoginBtn").on("click", function (e) {        
        if ($("#customerlistDropDown")[0].selectedIndex != 0) {            
                //Clear and Reset error message
                $("#message")[0].innerHTML = "";
                $("#status-message-div").hide();                
                var companyName = $("#customerlistDropDown").find("option:selected").text();
                var customerExternalAccountNo = $("#customerlistDropDown").find("option:selected").val();
                
                LoginWithCustomer(customerExternalAccountNo);
            }
            else {
                //Show error message
                $("#message")[0].innerHTML = "<span style='color:red'>Please select any Customer.</span>";
                $("#status-message-div").show();
            }

        
    });

    //Method to login with selected customer.
    LoginWithCustomer = function (customerExternalAccountNo) {
        var customerExternalAccountNo = customerExternalAccountNo;
        var returnUrl = getUrlParameter('returnUrl');        
        Api.LoginWithAsssociatedCustomer(customerExternalAccountNo, returnUrl, function (response) {
            if (response.success) {                
                $('.dialog').remove();
                window.location.href = response.link;
                //showStatusMessage(response,element);
            }
            else {                
                $('.dialog').remove();
                window.location.href = response.link;

                // showStatusMessage(response,element);
            }
            $('.dialog').remove();
        });
    }

    var getUrlParameter = function getUrlParameter(sParam) {        
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0].toLowerCase() === sParam.toLowerCase()) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    };
   
});