
var Api = {
    ajax: {
        settings: {
            errorAsAlert: false // True will show error as alert dialog
        },

        // Common ajax request; includes cache buster for IE
        request: function (url, method, parameters, successCallback, responseType) {
            if (!method) { method = "GET"; }
            if (!responseType) { responseType = "json"; }

            // Requests must have callback method
            if (typeof successCallback != "function") {
                ajax.errorOut("Callback is not defined. No request made.");
            } else {
                $.ajax({
                    type: method,
                    url: url,
                    data: Api.ajax.cachestamp(parameters),
                    dataType: responseType,
                    success: function (response) {
                        _debug(response); // Always send response to console after request
                        successCallback(response);
                    },
                    error: function () {
                        Api.ajax.errorOut("API Endpoint not available: " + url);
                    }
                });
            }
        },

        // Timestamp for cache busting
        cachestamp: function (data) {
            var d = new Date();

            if (typeof data == "string") {
                data += "&_=" + d.getTime();
            } else if (typeof data == "object") {
                data["_"] = d.getTime();
            } else {
                data = { "_": d.getTime() };
            }


            return (data);
        },

        // Error output
        errorOut: function (message) {
            console.log(message);

            if (this.settings.errorAsAlert) {
                alert(message);
            }
        }
    },

    //Api App ajax helper to extend api.js from custom js
    getAjaxHelper: function () {
        return Api.ajax;
    },
    // App API endpoints
    // Add methods here as needed for additional API calls
    // All endpoint methods are public

    // Get grid of related products (FBT/YML); Returns raw HTML, not JSON
    getRelatedProducts: function (ids, accountId, callbackMethod) {
        Api.ajax.request("/productasync/getproductsbyids", "get", { "ids": ids, "accountId": accountId }, callbackMethod, "html");
    },


    // Get grid of related products (FBT/YML); Returns raw HTML, not JSON
    countinueCheckout: function (address, callbackMethod) {
        Api.ajax.request("/checkout/continuecheckout", "post", { "multipleAddress": address }, callbackMethod, "json");
    },


    // get recent product
    getRecentProducts: function (productId, productCount, callbackMethod) {
        Api.ajax.request("/product/getrecentproducts", "get", { "productId": productId, "productCount": productCount }, callbackMethod);
    },

    // Get number of items in cart; Returns raw integer value, not JSON
    getCartCount: function (callbackMethod) {
        Api.ajax.request("/cart/cartcount", "get", false, callbackMethod);
    },

    //PRFT Custom Code: Start
    // Get total of items in cart; Returns raw float value, not JSON
    getCartTotal: function (callbackMethod) {
        Api.ajax.request("/cart/carttotal", "get", false, callbackMethod);
    },
    //PRFT Custom Code: End

    // Apply coupon code to cart order
    useCouponCode: function (couponCode, callbackMethod) {
        Api.ajax.request("/cart/applycoupon", "get", { "Coupon": couponCode }, callbackMethod, "json");
    },

    // Returns balance of gift card
    getGiftCardBalance: function (gcNumber, callbackMethod) {
        Api.ajax.request("/home/giftcard", "get", { "giftcard": gcNumber }, callbackMethod);
    },

    // Apply gift card to order during checkout
    useGiftCard: function (gcNumber, callbackMethod) {
        Api.ajax.request("/checkout/applygiftcard", "get", { "number": gcNumber }, callbackMethod);
    },

    // Add single item to wish list
    addProductToWishList: function (productId, callbackMethod) {
        Api.ajax.request("/product/wishlist", "get", { "id": productId }, callbackMethod);
    },

    // Remove item from account wish list
    removeProductFromWishList: function (wishlistId, callbackMethod) {
        Api.ajax.request("/account/wishlist", "post", { "wishid": wishlistId }, callbackMethod);
    },

    // Update product attributes
    getProductAttributes: function (productId, data, callbackMethod) {
        Api.ajax.request("/product/getattributes/" + productId, "get", data, callbackMethod);
    },

    // Get security question of specified user
    getUserSecurityQuestion: function (username, email, callbackMethod) {
        Api.ajax.request("/account/forgotpassword/", "post", { "UserName": username, "EmailAddress": email }, callbackMethod);
    },    

    // Get suggestions for search term (typeahead)
    getSearchSuggestions: function (searchTerm, callbackMethod) {
        Api.ajax.request("/search/getsuggestions", "get", { "searchTerm": searchTerm }, callbackMethod);
    },

    // get list of State bt CountryCode .
    getStateByCountryCode: function (countryCode, callbackMethod) {
        Api.ajax.request("/account/getstatebycountrycode", "get", { "countryCode": countryCode }, callbackMethod);
    },

    GetPaymentGatwayName: function (paymentOptionId, callbackMethod) {
        Api.ajax.request("/checkout/getpaymentgatwaynamebypaymetoptionid", "get", { "paymentOptionId": paymentOptionId }, callbackMethod);
    },

    GetProductListBySKU: function (sku, callbackMethod) {
        Api.ajax.request("/product/getproductlistbysku", "get", { "sku": sku }, callbackMethod);
    },

    globalAddToCompare: function (productId, categoryId, callbackMethod) {
        Api.ajax.request("/product/globallevelcompareproduct", "get", { "productId": productId, "categoryId": categoryId }, callbackMethod);
    },

    categoryAddToCompare: function (productId, categoryId, callbackMethod) {
        Api.ajax.request("/product/categorylevelcompareproduct", "get", { "productId": productId, "categoryId": categoryId }, callbackMethod);
    },

    getCompareProducts: function (callBackMethod) {
        Api.ajax.request("/product/showcompareproduct", "get", {}, callBackMethod);
    },

    getProductComparison: function (callBackMethod) {
        Api.ajax.request("/product/viewproductcomparison", "post", {}, callBackMethod);
    },

    removeProduct: function (productId, control, callBackMethod) {
        Api.ajax.request("/product/removeproductformsession", "post", { "productId": productId, "control": control }, callBackMethod)
    },

    productCompareAddToCart: function (cartItem, callBackMethod) {
        Api.ajax.request("/product/addtocart", "post", { "cartItem": cartItem }, callBackMethod, "html");
    },

    getComparedProductSendMailDetails: function (callBackMethod) {
        Api.ajax.request("/product/sendcomparedproductmail", "get", {}, callBackMethod, "html");
    },

    applyDiscount: function (url, couponCode, callBackMethod) {
        Api.ajax.request(url, "get", { "couponCode": couponCode }, callBackMethod, "json");
    },
    signUpForNewsLetter: function (emailId, callbackMethod) {
        Api.ajax.request("/account/signupfornewsletter", "post", { "emailId": emailId }, callbackMethod);
    },

    getProductSendMailDetails: function (productId, productName, callBackMethod) {
        Api.ajax.request("/product/sendmailtofriend", "get", { "productId": productId, "productName": productName }, callBackMethod, "html");
    },
    getQuickView: function (productId, callBackMethod) {
        Api.ajax.request("/product/details", "get", { "id": productId, "isQuickView": true }, callBackMethod, "html");
    },
    getProductBreadCrumb: function (productId, productName,isQuickView, callbackMethod) {
        Api.ajax.request("/product/getproductbreadcrumb", "get", { "productId": productId, "productName": productName, "isQuickView": isQuickView }, callbackMethod);
    },
    setProductCategory: function (categoryId, categoryName, callbackMethod) {
        Api.ajax.request("/product/setproductcategory", "get", { "categoryId": categoryId, "categoryName": categoryName }, callbackMethod);
    },
    isCouponUrlEnabled: function (couponCode, callbackMethod) {
        Api.ajax.request("/cart/iscouponurlenabled", "get", { "Coupon": couponCode }, callbackMethod, "json");
    },
    ProcessPayPalPayment: function (paymentOptionId, returnUrl, cancelUrl, callbackMethod) {
        Api.ajax.request("/checkout/processpaypalpayment", "post", { "paymentSettingId": paymentOptionId, "returnUrl": returnUrl, "cancelUrl": cancelUrl }, callbackMethod, "json");
    },
    getProductOutOfStockDetails: function (productId, callBackMethod) {
        Api.ajax.request("/product/getproductoutofstockdetails", "get", { "productId": productId }, callBackMethod, "html");
    },
    submitPayment: function (paymentSettingId, customerProfileId, customerPaymentId, callBackMethod) {
        Api.ajax.request("/checkout/submitpayment", "post", { "paymentSettingId": paymentSettingId, "customerProfileId": customerProfileId, "customerPaymentId": customerPaymentId }, callBackMethod, "json");
    },
    getSenMailNOtification: function (url, productId, senderMailId, recieverMailId, callBackMethod) {
        Api.ajax.request(url, "post", { "productId": productId, "senderMailId": senderMailId, "recieverMailId": recieverMailId }, callBackMethod, "json");
    },
    //PRFT Custom Code : Start
    getSenMailNOtification: function (url, productId, senderMailId, recieverMailId,captchaResponse, callBackMethod) {
        Api.ajax.request(url, "post", { "productId": productId, "senderMailId": senderMailId, "recieverMailId": recieverMailId, "captchaResponse": captchaResponse }, callBackMethod, "json");
    },
    Logout: function (callBackMethod) {
        Api.ajax.request("/account/logout", "get", {}, callBackMethod, "json");
    },
    LoginWithAsssociatedCustomer: function (customerExternalAccountNo, returnUrl, callBackMethod) {
        Api.ajax.request("/account/prftloginwithasssociatedcustomer", "get", { "customerExternalAccountNo": customerExternalAccountNo, "returnUrl": returnUrl }, callBackMethod, "json");
    },
    //PRFT Custom Code : End
}
// END module