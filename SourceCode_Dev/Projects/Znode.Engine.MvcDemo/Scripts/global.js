
var menuUsed = false;
var global = {
    // Instant method runs inline when this script loads
    instant: function () {
        _debug("global.instant()");

    },
    executeFunctionByName: function (functionName, context /*, args */) {
        try {
            var args = [].slice.call(arguments).splice(2);
            var namespaces = functionName.split(".");
            var func = namespaces.pop();
            for (var i = 0; i < namespaces.length; i++) {
                context = context[namespaces[i]];
            }
            return context[func].apply(this, args);
        }
        catch (ex) {
            _debug("Invalid:" + functionName)
        }
    },

    // Onready method runs when document is loaded and ready
    onready: function () {
        global.bind.init();

        // Set the controller and action names
        var modules = $("body").data("controller").split(".");
        var action = $("body").data("view");
        // Loads modules based on controller and view
        // If init() methods are present, they will be run on load
        modules.forEach(function (module) {
            if (module !== 'undefined') {
                var functionName = module + ".init";
                global.executeFunctionByName(functionName, window, arguments);
            }
        });

        // Display any alert messages that were stored
        global.local.displayAlertMessage();

        // Lazy load async images
        global.local.lazyLoadImages();

        global.local.cartCount();

        global.local.cartTotal();
    },

    // LOCAL METHODS	
    local: {
        cartCount: function () {
            _debug("local.cartCount()");

            Api.getCartCount(function (response) {
                var count = 0;

                if (parseInt(response) > 0) {
                    count = parseInt(response);
                }

                $(".cartcount").html(count);
                //$(".cartcount").css({ "position": "absolute" });
            });           
        },

        //PRFT Custom Code: Start
        //To get the Cart Total for current user.
        cartTotal: function () {
            _debug("local.cartTotal()");            
            Api.getCartTotal(function (response) {                
                var total = 0.0;

                if (parseFloat(response) > 0.0) {

                    total = parseFloat(response);
                }               
                $("#layout-header .top-cart-price").html("$" + total.toFixed(2));
                $("#layout-header-mobile .top-cart-price").html("$" + total.toFixed(2));
                $("#mobile-menu .cart-price").html("$" + total.toFixed(2));
            });            
        },
        //PRFT Custom Code: End


        // Lazy loader for content/markup. Intended for use with calling method that provides endpoint
        lazyLoadContent: function (container, handlerMethod) {
            _debug("lazy content");

            if (typeof container == "undefined") { container = ""; }

            var items = $(container + " .lazycontent"); // Find all <img> tags with data-src attribute

            $.each(items, function (k, v) {
                
                var el = $(this);
                var params = el.data("params");
                var appendTo = el.data("append");
                var prependTo = el.data("prepend");
                var replaceHtml = el.data("replace");
                //var accountId = el.data("accountid");

                if (typeof appendTo != "undefined") {
                    var render = function (html) {
                        //html = html.replace("items: ,", "items:5,"); //if items value is empty then it will be replaced with value 5
                        $(appendTo).append(html);
                    }
                }

                if (typeof prependTo != "undefined") {
                    var render = function (html) { $(prependTo).prepend(html); }
                }

                if (typeof replaceHtml != "undefined") {
                    var render = function (html) { $(prependTo).html(html); }
                }

                handlerMethod(this, params, render);
            });

        },

        // Lazy load images that have required data attributes
        lazyLoadImages: function (container) {
            if (typeof container == "undefined") { container = ""; }

            var images = $(container + " img[data-src]"); // Find all <img> tags with data-src attribute


            _debug("> Lazy loading " + images.length + " images in " + container + " img[data-src]");


            $.each(images, function (k, v) {
                var image = $(v);
                var imgsrc = image.data("src");
                var small = image.data("small");
                var large = image.data("large");



                image.attr("src", imgsrc);
            });
        },

        // Generic element toggler
        toggleElements: function (el) {
            var toggle = el.data("toggle"),
				hide = el.data("hide"),
				inactive = el.data("reset");

            // Remove 'active' class from matching selector
            if (inactive) {
                $(inactive).removeClass("active");
            }

            // Force hides from matching selector
            if (hide) {
                $(hide).hide();
            }

            el.toggleClass("active"); // Add active class to clicked element

            $(toggle).toggle(); // Toggles matching element		
        },


        // Puts alert message in browser session storage
        setAlertMessage: function (message, style) {
            sessionStorage.setItem("alertMessageText", message);
            sessionStorage.setItem("alertMessageStyle", style);
        },

        // Checks for alert message and inserts into DOM
        displayAlertMessage: function () {
            var message = sessionStorage.getItem("alertMessageText"); // Check for message

            if (message) {
                var template = $("#tmpl-alertmessage").html(); // Get inline template
                var render = Mustache.render(template, { "message": message, "style": sessionStorage.getItem("alertMessageStyle") });

                $("#SuccessMessage").html(render); // Insert

                // Clear temp storage vars
                sessionStorage.removeItem("alertMessageText");
                sessionStorage.removeItem("alertMessageStyle");
            }
        },

        // Clears any messages from session storage
        clearAlertMessage: function () {
            sessionStorage.removeItem("alertMessageText");
            sessionStorage.removeItem("alertMessageStyle");
            $("#status-message").remove();
        }
    },


    // TYPEAHEAD
    // Available on every page through header search (medium+)
    typeahead: {
        timer: false,
        start: function (input) {
            // Unset any started timers from previous keyup
            if (typeahead.timer != undefined) {
                clearTimeout(typeahead.timer);
            }

            // Set delay timer, won't fire ajax until timeout reached
            typeahead.timer = setTimeout(function () { typeahead.request(input); }, 300);
        },

        request: function (input) {
            var keyword = $(input).val();
            _debug("> typeahead request for " + keyword);

            // Only make a request if there is a string
            if (keyword != "") {
                Api.getSearchSuggestions(keyword, function (response) {
                    //var prevMatch = ""; // Clear prev matches
                    var matches = { "matches": [] }; // Reset matches set
                    var template = $("#tmpl-typeahead-results").html(); // Get inline template

                    // Loop through response
                    $.each(response, function (k, i) {
                        matches.matches.push({ "suffix": (i.CategoryName != '' ? " in " + i.CategoryName : ""), "product": i.ProductName, "term": encodeURIComponent(i.ProductName), "category": encodeURIComponent(i.CategoryName) });
                        prevMatch = i.ProductName;
                    });

                    if (matches.matches.length > 0) {
                        // Toss matches to the inline template
                        var render = Mustache.render(template, matches);

                        // Insert rendered HTML onto page
                        $("#typeahead-results").html(render).addClass("active");
                        $("#typeahead-results .close").click(function () { $("#typeahead-results").empty().removeClass("active") })
                    } else {
                        $("#typeahead-results").removeClass("active"); // Hide overlay if empty search (when deleting)
                    }
                });
            } else {
                $("#typeahead-results").empty().removeClass("active");// Hide overlay if empty search (when deleting)
            }
        },

        close: function () {
            $("#typeahead-results").empty().removeClass("active");
        }
    },
    // BINDINGS
    bind: {
        init: function () {
            _debug("global.bind.init()");
            $("#header-search-input").keyup(function () {
                global.typeahead.request(this);
            });

            // Click for "log off" in main menu, submits form with session token
            $(".account-logout").on("click", function (ev) {
                ev.preventDefault();
                $("#logout-form").submit();
            });


            // Generic toggle binding 
            this.attachToggle();

            this.newsLetterSignUp();
        },

        // Generic element toggler binding
        attachToggle: function () {
            _debug("bind.attachToggle()");

            $(".toggle").on("click", function (e) {
                e.preventDefault();
                global.local.toggleElements($(this));
            });
        },

        newsLetterSignUp: function () {
            _debug("bind.newsLetterSignUp()");

            $("#btnNewsLetterSignUp").on("click", function (e) {
                $("#newslettererrormessage").removeClass();
                var signUpEmail = $("#txtNewsLetterSignUp").val();
                var pattern = new RegExp(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/);
                if (signUpEmail != null && signUpEmail != "") {
                    if (!pattern.test(signUpEmail)) {
                        $("#newslettererrormessage").html("Please use a valid email address.")
                        $("#newslettererrormessage").addClass("error-msg");
                        $("#newslettererrormessage").show();
                        return false;
                    }
                    else {
                        $("#newslettererrormessage").html('');
                        $("#newslettererrormessage").removeClass("error-msg");
                        $("#newslettererrormessage").hide();
                        Api.signUpForNewsLetter(signUpEmail, function (response) {
                            if (response.sucess) {
                                $("#txtNewsLetterSignUp").val('');
                                $("#newslettererrormessage").addClass("success-msg");
                                $("#newslettererrormessage").show().html(response.message);
                            } else {
                                $("#newslettererrormessage").addClass("error-msg");
                                $("#newslettererrormessage").show().html(response.message);
                            }
                        });
                    }
                } else {
                    $("#newslettererrormessage").html('Email ID is required');
                    $("#newslettererrormessage").addClass("error-msg");
                    $("#newslettererrormessage").show();
                    return false;
                }
            });
            $('#txtNewsLetterSignUp').keyup(function (e) {
                if (e.keyCode == 13) {
                    $("#btnNewsLetterSignUp").click();
                }
            });
        },

        ResetTopCategory: function () {
            var _contenerWidth = $("#navbar-collapse-nav").css("width");
            var _menuWidth = 0;
            var html = document.createElement("div");

            $("#navbar-collapse-nav .dropdown").each(function () {
                _menuWidth += parseInt($(this).css("width").replace("px", ""));

                if (parseInt(_menuWidth) > (parseInt(_contenerWidth.replace("px", "")) - 50)) {
                    $(html).append($(this))
                    // $(this).remove();
                }
                if (parseInt(_menuWidth) < parseInt(_contenerWidth.replace("px", ""))) {
                    $("#navbar-collapse-nav .navbar-nav").append($("#others-menu li:eq(0)"))
                }
            });

            $("#others-menu").append($(html));
        }
    }
}


global.instant();

$(window).on('load', function () {

    global.onready();
    global.bind.ResetTopCategory();
    if ($("#others-menu .dropdown").length === 0) {
        $(".nav-drop-icon").hide();
    }
    else {
        $(".nav-drop-icon").show();
    }
});

$(window).bind('orientationchange', function (event) {
    if ($("#others-menu .dropdown").length === 0) {
        $(".nav-drop-icon").hide();
    }
    else {
        $(".nav-drop-icon").show();
    }
});

$(".control_next").on("click", function () {
    $("#others-menu").toggle();
});