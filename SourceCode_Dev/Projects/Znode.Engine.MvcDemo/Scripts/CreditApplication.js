﻿//Controller

(function () {
    'use strict';
    var CreditApplicationController = function ($scope, $window, CreditApplicationServices) {
        //Global variables//
        $scope.countryList = [];
        $scope.selectedBusinessCountry = {};
        $scope.selectedBillingCountry = {};

        $scope.stateLists = [];
        $scope.businessStateList = [];
        $scope.billingStateList = [];

        $scope.selectedBusinessState = {};
        $scope.selectedBillingState = {};

        $scope.selectedBusinessType = {};
        $scope.businessTypeList = [];

        $scope.selectedIsTaxExempt = {};
        $scope.isTaxExemptList = [];
        $scope.isBillingAddrIsSameAsBussinessAddr = false;
        $scope.isYearEstablishedInvalid = false;
        $scope.TaxExemptCertificateRequired = false;
        //$scope.emailRegEx = "/^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/";

        $scope.PRFTCreditApplicationModel =
            {
                CreditApplicationID: 0
                , BusinessName: ""
                , BusinessStreet: ""
                , BusinessStreet1: ""
                , BusinessCity: ""
                , BusinessStateCode: ""
                , BusinessPostalCode: ""
                , BusinessCountryCode: ""
                , BusinessPhoneNumber: ""
                , BusinessFaxNumber: ""
                , BusinessEmail: ""
                , BusinessYearEstablished: null
                , BusinessType: ""
                , IsTaxExempt: false
                , TaxExemptCertificate: ""
                , BillingStreet: ""
                , BillingStreet1: ""
                , BillingCity: ""
                , BillingStateCode: ""
                , BillingPostalCode: ""
                , BillingCountryCode: ""
                , BankName: ""
                , BankAddress: ""
                , BankPhoneNumber: ""
                , BankAccountNumber: ""
                , RequestDate: new Date()
                , IsCreditApproved: false
                , CreditDays: 0
                , CreditApprovedDate: new Date()
                , TradeReferences: [{
                    TradeRefID: 0
                                        , CreditApplicationID: 0
                                        , ReferenceName: ""
                                        , BusinessName: ""
                                        , Street: ""
                                        , Street1: ""
                                        , City: ""
                                        , StateCode: ""
                                        , CountryCode: ""
                                        , PostalCode: ""
                                        , PhoneNumber: ""
                                        , FaxNumber: ""
                },
                {
                    TradeRefID: 0
                                        , CreditApplicationID: 0
                                        , ReferenceName: ""
                                        , BusinessName: ""
                                        , Street: ""
                                        , Street1: ""
                                        , City: ""
                                        , StateCode: ""
                                        , CountryCode: ""
                                        , PostalCode: ""
                                        , PhoneNumber: ""
                                        , FaxNumber: ""
                },
                {
                    TradeRefID: 0
                                        , CreditApplicationID: 0
                                        , ReferenceName: ""
                                        , BusinessName: ""
                                        , Street: ""
                                        , Street1: ""
                                        , City: ""
                                        , StateCode: ""
                                        , CountryCode: ""
                                        , PostalCode: ""
                                        , PhoneNumber: ""
                                        , FaxNumber: ""
                }]
                , TaxExemptCertificateAttachments: null
                , Custom1: ""
            };


        $scope.initializeController = function () {
            //Get set country and states
            var getCountryResponse = CreditApplicationServices.getCountryList();
            getCountryResponse.then(function (response) {
                if (response.data && response.data.length > 0) {
                    $scope.countryList = response.data;
                    if ($scope.countryList.length > 0) {
                        $scope.selectedBusinessCountry = $scope.countryList[0];
                        $scope.selectedBillingCountry = $scope.countryList[0];

                        //Get Set State Lists
                        var getStateResponse = CreditApplicationServices.getStateList();
                        getStateResponse.then(function (response) {
                            if (response.data && response.data.length > 0) {
                                $scope.stateLists = response.data;
                                if ($scope.stateLists.length > 0) {

                                    $scope.setBusinessStates();
                                    $scope.setbillingStates();
                                }
                            }
                        }, function (error) {
                            //alert("Error while loading state list" + error);
                        });
                    }
                }
            }, function (error) {
                //alert("Error while loading country list" + error);
            });

            //Business Types
            $scope.businessTypeList = CreditApplicationServices.bussinessTypes;
            $scope.selectedBusinessType = $scope.businessTypeList[0];

            //Tax Exempt
            $scope.isTaxExemptList = CreditApplicationServices.isTaxExemptList;
            $scope.selectedIsTaxExempt = $scope.isTaxExemptList[0];
        };

        $scope.addTradeReference = function () {
            var tmpTradeReference = {
                TradeRefID: 0
                                    , CreditApplicationID: 0
                                    , ReferenceName: ""
                                    , BusinessName: ""
                                    , Street: ""
                                    , Street1: ""
                                    , City: ""
                                    , StateCode: ""
                                    , CountryCode: ""
                                    , PostalCode: ""
                                    , PhoneNumber: ""
                                    , FaxNumber: ""
            }
            $scope.PRFTCreditApplicationModel.TradeReferences.push(tmpTradeReference);

        };

        $scope.makeBillingAddresssSameAsBussiness = function () {
            //make billing address same as bussiness Address
            if ($scope.isBillingAddrIsSameAsBussinessAddr) {
                $scope.PRFTCreditApplicationModel.BillingStreet = $scope.PRFTCreditApplicationModel.BusinessStreet;
                $scope.PRFTCreditApplicationModel.BillingStreet1 = $scope.PRFTCreditApplicationModel.BusinessStreet1;
                $scope.selectedBillingCountry = $scope.selectedBusinessCountry;
                $scope.selectedBillingState = $scope.selectedBusinessState;
                $scope.PRFTCreditApplicationModel.BillingCity = $scope.PRFTCreditApplicationModel.BusinessCity;
                $scope.PRFTCreditApplicationModel.BillingPostalCode = $scope.PRFTCreditApplicationModel.BusinessPostalCode;
            }
            else {
                $scope.PRFTCreditApplicationModel.BillingStreet = "";
                $scope.PRFTCreditApplicationModel.BillingStreet1 = "";
                $scope.selectedBillingCountry = $scope.countryList[0];
                $scope.setbillingStates();
                $scope.PRFTCreditApplicationModel.BillingCity = "";
                $scope.PRFTCreditApplicationModel.BillingPostalCode = "";
            }
        };

        $scope.filterValue = function (event) {
            var currentKeyCode = (event.keyCode ? event.keyCode : event.which);
            try {
                if (currentKeyCode != 13 && currentKeyCode != 9 && currentKeyCode != 8 && !event.shiftKey && currentKeyCode != 46 && currentKeyCode != 35 && currentKeyCode != 36 && currentKeyCode != 37 && currentKeyCode != 39 && currentKeyCode != 46) {
                    var valStatus = (currentKeyCode >= 48 && currentKeyCode <= 57) || (currentKeyCode >= 96 && currentKeyCode <= 105);
                    if (valStatus === false) {
                        event.preventDefault();
                        return false;
                    }
                }
            }
            catch (err) {
                //Error while parsing year established 
            }
        };

        $scope.addNewTradeReference = function () {
            $scope.addTradeReference();
        };

        $scope.validateCreditApplication = function () {

            //Validate Year
            var d = new Date();
            var currentYear = d.getFullYear();
            $scope.isYearEstablishedInvalid = false;

            if (parseInt($scope.PRFTCreditApplicationModel.BusinessYearEstablished) > currentYear) {
                $scope.isYearEstablishedInvalid = true;
                return;
            }

            //Validate Tax Exempt
            $scope.TaxExemptCertificateRequired = false;
            if ($scope.selectedIsTaxExempt) {
                $scope.PRFTCreditApplicationModel.IsTaxExempt = $scope.selectedIsTaxExempt.Value;
                if ($scope.PRFTCreditApplicationModel.IsTaxExempt === true) {
                    var FUTaxExemptCertificate = angular.element("#FUTaxExemptCertificate")[0];
                    if (FUTaxExemptCertificate && FUTaxExemptCertificate.files.length <= 0) {
                        $scope.TaxExemptCertificateRequired = true;
                        return;
                    }
                    else {
                        $scope.PRFTCreditApplicationModel.TaxExemptCertificateAttachments = FUTaxExemptCertificate.files[0];
                    }
                }
            }

            //Set Selected Values for the drop downs
            $scope.PRFTCreditApplicationModel.BusinessStateCode = $scope.selectedBusinessState.Name;
            $scope.PRFTCreditApplicationModel.BusinessCountryCode = $scope.selectedBusinessCountry.Name;
            $scope.PRFTCreditApplicationModel.BillingStateCode = $scope.selectedBillingState.Name;
            $scope.PRFTCreditApplicationModel.BillingCountryCode = $scope.selectedBillingCountry.Name;
            $scope.PRFTCreditApplicationModel.BusinessType = $scope.selectedBusinessType.Name;

            //If everything looks good then call the recaptcha to executes
            if (grecaptcha != undefined && grecaptcha != null) {
                grecaptcha.execute();
            }
            return false;
        };

        $scope.submitCreditApplication = function () {

            //Get the google recaptcha response in credit application object.
            $scope.PRFTCreditApplicationModel.Custom1 = grecaptcha.getResponse();

            //Submit Credit Application Form
            var SubmitResponse = CreditApplicationServices.submitCreditApplicationForm($scope.PRFTCreditApplicationModel);

            SubmitResponse.then(function (response) {
                if (response.data && response.data == "True") {
                    angular.element(".credit-application-outer").hide();
                    angular.element(".success").show().focus();
                    $window.scrollTo(0, 0);
                }
            },
            function (err) {
                //Show proper message
            });
        };

        $scope.removeTradeReference = function (index) {
            $scope.PRFTCreditApplicationModel.TradeReferences.splice(index, 1);
        };

        $scope.setBusinessStates = function () {

            $scope.businessStateList = CreditApplicationServices.getStateListBasedOnCountryCode($scope.selectedBusinessCountry.Code, $scope.stateLists)
            if ($scope.businessStateList.length > 0) {
                $scope.selectedBusinessState = $scope.businessStateList[0];
            }
        };

        $scope.setbillingStates = function () {
            $scope.billingStateList = CreditApplicationServices.getStateListBasedOnCountryCode($scope.selectedBillingCountry.Code, $scope.stateLists)
            if ($scope.billingStateList.length > 0) {
                $scope.selectedBillingState = $scope.billingStateList[0];
            }
        };

        $scope.initializeController();

        //Attach Global method at windows level to angular method
        $window.submitCreditApplication = $scope.submitCreditApplication;
    };
    CreditApplicationController.$inject = ['$scope', '$window', 'CreditApplicationServices'];
    var CreditApplicationApp = angular.module('CreditApplicationApp', ['CreditApplicationServices']).controller('CreditApplicationController', CreditApplicationController).config(["$locationProvider", function ($locationProvider) {
        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
    }]);
})(window, document);


//Service
(function () {
    var CreditApplicationServices = function ($http, $q) {

        this.bussinessTypes = [{ "Value": "1", "Name": "Agriculture & Forestry/Wildlife" }, { "Value": "2", "Name": "Business & Information" }, { "Value": "3", "Name": "Construction/Utilities/Contracting" }, { "Value": "4", "Name": "Education" }, { "Value": "5", "Name": "Finance & Insurance" }, { "Value": "6", "Name": "Food & Hospitality" }, { "Value": "7", "Name": "Gaming" }, { "Value": "8", "Name": "Health Services" }, { "Value": "9", "Name": "Motor Vehicle" }, { "Value": "10", "Name": "Natural Resources/Environmental" }, { "Value": "11", "Name": "Personal Services" }, { "Value": "12", "Name": "Real Estate & Housing" }, { "Value": "13", "Name": "Safety/Security & Legal" }, { "Value": "14", "Name": "Transportation" }, { "Value": "15", "Name": "Other" }];
        this.isTaxExemptList = [{ "Value": false, "Text": "No" }, { "Value": true, "Text": "Yes" }];

        //Get Country Lists
        this.getCountryList = function () {
            var requestUrl = "/prftcreditapplication/getcountries";
            return $http.get(requestUrl);
        }

        //Get State Lists
        this.getStateList = function () {
            var requestUrl = "/prftcreditapplication/getstates";
            return $http.get(requestUrl);
        }

        //Get State based on Country Code
        this.getStateListBasedOnCountryCode = function (countryCode, States) {
            var stateLists = [];
            if (countryCode && States) {
                for (var i = 0, stateLen = States.length; i < stateLen; i++) {
                    if (countryCode == States[i].CountryCode) {
                        stateLists.push(States[i]);
                    }
                }
            }
            return stateLists;
        }

        //Submit Credit Application Form
        this.submitCreditApplicationForm = function (prftCreditApplication) {
            var posturl = "/prftcreditapplication/addcreditapplication";
            var dataToPost = this.objectToFormData(prftCreditApplication);
            //var dataToPost = this.getModelAsFormData(prftCreditApplication);
            var request = $http({
                method: "POST",
                url: posturl,
                data: dataToPost,
                transformRequest: angular.identity,
                headers: { 'Content-Type': undefined }
            });
            return request;
        }

        //Method to be used when objectToFormData will not work
        //this.getModelAsFormData = function (data) {
        //    var dataAsFormData = new FormData();
        //    angular.forEach(data, function (value, key) {
        //        if (key == "TradeReferences") {
        //            dataAsFormData.append("StrTradeReferences", JSON.stringify(value));
        //        }
        //        else {
        //            dataAsFormData.append(key, value);
        //        }
        //    });
        //    return dataAsFormData;
        //}


        // takes a {} object of any complex type and returns a FormData object
        this.objectToFormData = function (obj, form, namespace) {
            var fd = form || new FormData();
            var formKey;
            for (var property in obj) {
                if (obj.hasOwnProperty(property)) {
                    if (namespace) {
                        formKey = namespace + '[' + property + ']';
                    } else {
                        formKey = property;
                    }

                    // if the property is an object, but not a File,
                    // use recursivity.
                    if (typeof obj[property] === 'object' && !(obj[property] instanceof File)) {
                        this.objectToFormData(obj[property], fd, formKey);
                    } else {
                        // if it's a string or a File object
                        fd.append(formKey, obj[property]);
                    }
                }
            }
            return fd;
        }
    }
    angular.module("CreditApplicationServices", []).service("CreditApplicationServices", CreditApplicationServices);
    CreditApplicationServices.$inject = ['$http', '$q'];
})(window, document);

// Hack to remove js error calling init method by znode
var PRFTCreditApplication = {
    init: function () {
        //Nothing to do here as already handled in angular js
    }
}

function submitCreditApplication()
{

}

