﻿$(document).ready(function () {
    $('div #BindAlphabetical ul li:last-child a').addClass('selected');
    $('div #BindAlphabetical a').click(function () {
        $('div #BindAlphabetical a').removeClass('selected');
        $(this).addClass('selected');
        $('#ViewBy').val($(this).html());
        var selectedAlphabet = $('#ViewBy').val();
        var url = "/manufacturer/bindliteralforbrandlist";
        $.post(url, { alpha: selectedAlphabet },
         function (data) {
             $('#bindBrandList').html(data);
         });
    });
    var AlphabetOnLoad = '';
    var url = "/manufacturer/bindliteralforbrandlist";
    $.post(url, { alpha: AlphabetOnLoad },
     function (data) {
         $('#bindBrandList').html(data);
     });
    $('div #BindAlphabetical a').attr('href', 'javascript:void(12);');
});