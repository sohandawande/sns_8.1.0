﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZNode.Libraries.DataAccess.Custom
{
    public class PRFTImportCustomerHelper
    {
        public DataSet GetUser()
        {
            DataSet dataSet = new DataSet();
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                try
                {
                    SqlDataAdapter adapter = new SqlDataAdapter("PRFT_GetUser", connection);

                    // Mark the command as store procedure
                    adapter.SelectCommand.CommandType = CommandType.StoredProcedure;

                    connection.Open();
                    adapter.Fill(dataSet);

                    // Release the resources
                    adapter.Dispose();
                    connection.Close();

                    // Return the datadet.
                }
                catch (Exception ex)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("Class-PRFTImportCustomerHelper:Method-CreateUser:SP-PRFT_GetUser - " + ex.Message);
                }
            }
            return dataSet;
        }

        public bool UpdateUserID(string userId, int accountId)
        {
            int affectedRows = 0;
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                try
                {
                    SqlCommand command = new SqlCommand("PRFT_Update_ZnodeAccount", connection);

                    // Mark the command as store procedure
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@UserID", userId);
                    command.Parameters.AddWithValue("@AccountId", accountId);

                    connection.Open();
                    affectedRows = command.ExecuteNonQuery();

                    // Release the resources
                    command.Dispose();
                    connection.Close();

                    // Return the datadet.
                }
                catch (Exception ex)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("Class-PRFTImportCustomerHelper:Method-UpdateUserID:SP-PRFT_Update_ZnodeAccount - " + ex.Message);
                }
            }
            return Convert.ToBoolean(affectedRows);
        }
    }
}
