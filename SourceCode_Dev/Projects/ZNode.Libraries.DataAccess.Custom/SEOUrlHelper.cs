using System;
using System.Data;
using System.Data.SqlClient;

namespace ZNode.Libraries.DataAccess.Custom
{
    /// <summary>
    /// Manages the methods related to seo friendly urls of product, cateory and content pages
    /// </summary>
    public class SEOUrlHelper
    {
        #region Public Methods
        /// <summary>
        /// Returns SEO friendly url list by portal Id.
        /// </summary>
        /// <param name="portalId">Portal Id to get the SEO url.</param>
        /// <returns>Returns the SEO friendly url dataset for the store.</returns>
        public DataSet GetSEOUrlListByPortalID(int portalId)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetSEOUrlList", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@PortalID", portalId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Check whether the Seo url Exists or not. 
        /// </summary>
        /// <param name="seoUrl">url to check.</param>
        /// <returns>Returns True or False.</returns>
        public bool CheckSeoUrlExists(string seoUrl)
        {
            int results = 0;
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlCommand command = new SqlCommand("ZNode_GetSeoUrl", connection);

                // Mark the command as store procedure
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@SeoUrl", seoUrl);

                // Open the connection 
                connection.Open();

                results =Convert.ToInt32(command.ExecuteScalar());

                // Release the resources
                command.Dispose();
                connection.Close();
            }
            var res = Convert.ToBoolean(results);
            // Return the result.
            return Convert.ToBoolean(results);
        }


        /// <summary>
        /// Returns SEO url details based on seo url.
        /// </summary>
        /// <param name="seoUrl">url to check.</param>
        /// <returns>Returns the SEO url dataset.</returns>
        public DataSet GetSEOUrlDetail(string seoUrl)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("Znode_GetSEODetails", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@seourl", seoUrl);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// To Check whether provided Seo Url is restricted or not.
        /// </summary>
        /// <param name="seoUrl">string seoUrl</param>
        /// <returns>Return true or false.</returns>
        public bool IsRestrictedSeoUrl(string seoUrl)
        {
            bool isExist = false;

            // Create instance of connection and Command Object
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlCommand command = new SqlCommand("Znode_RestictedSEOUrls", connection);
                // Mark the command as store procedure
                command.CommandType = CommandType.StoredProcedure;
                // Add parameters to command object                
                command.Parameters.AddWithValue("@RestictedSEOUrls", seoUrl);
                connection.Open();
                object scalarValue = command.ExecuteScalar();
                isExist = Convert.ToBoolean(scalarValue);
                connection.Close();
            }

            return isExist;
        }
        #endregion
    }
}
