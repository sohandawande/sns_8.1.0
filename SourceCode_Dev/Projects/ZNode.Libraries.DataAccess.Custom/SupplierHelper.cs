using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace ZNode.Libraries.DataAccess.Custom
{
    /// <summary>
    /// Manages the methods related to the product suppliers
    /// </summary>
    public class SupplierHelper
    {
        /// <summary>
        /// Get the supplier details for the given supplier Id
        /// </summary>
        /// <param name="supplierId">Supplier Id to get the supplier details in XML.</param>
        /// <returns>Returns the supplier details in XML.</returns>
        public string GetSupplierXMLById(int supplierId)
        {
            // Create instance of connection and Command Object
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlDataReader reader = null;
                System.Text.StringBuilder xmlOut = new System.Text.StringBuilder();

                try
                {
                    SqlCommand command = new SqlCommand("ZNode_GetSupplierBySupplierID_XML", connection);

                    // Mark the command as store procedure
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@SupplierId", supplierId);

                    // Execute the command
                    connection.Open();

                    reader = command.ExecuteReader(CommandBehavior.CloseConnection);

                    while (reader.Read())
                    {
                        xmlOut.Append(reader[0].ToString());
                    }
                }
                finally
                {
                    // Close the reader
                    if (reader != null)
                    {
                        reader.Close();
                    }

                    // Close the connection
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }

                return xmlOut.ToString();
            }
        }

        /// <summary>
        /// Search the supplier and return the dataset
        /// </summary>
        /// <param name="name">Supplier name to search.</param>
        /// <param name="status">Supplier status to search.</param>
        /// <returns>Returns the supplier search result dataset.</returns>
        public DataSet SearchSupplier(string name, string status)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_SearchSupplier", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@Name", name);
                adapter.SelectCommand.Parameters.AddWithValue("@Status", status);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Get the account by supplier Id.
        /// </summary>
        /// <param name="supplierId">Supplier Id to get the account dataset.</param>
        /// <returns>Returns the accounts dataset.</returns>
        public DataSet GetAccountsBySupplierId(int supplierId)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetAccountsBySupplierId", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@SupplierId", supplierId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        public string GetSupplierIDByExternalID(string externalId)
        {
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                var adapter = new SqlDataAdapter("ZNode_GetSupplierByExternalID", connection) { SelectCommand = { CommandType = CommandType.StoredProcedure } };

                adapter.SelectCommand.Parameters.AddWithValue("@externalId", externalId);

                connection.Open();
                var reader = adapter.SelectCommand.ExecuteReader(CommandBehavior.CloseConnection);

                reader.Read();
                string supplierId = null;

                if (reader.HasRows)
                {
                    supplierId = reader[0].ToString();
                }

                connection.Close();

                return supplierId;
            }
        }

        public List<string> GetSupplierIDsByFilters(string query, string[,] array)
        {
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                var supplierIds = new List<string>();
                var adapter = new SqlDataAdapter(query, connection);
                for (var i = 0; i < array.GetLength(0); i++)
                {
                    if (array[i, 0].Equals("@p" + i))
                    {
                        adapter.SelectCommand.Parameters.AddWithValue(array[i, 0], (DateTime?)Convert.ToDateTime(array[i, 1]));
                    }
                    else
                    {
                        if (array[i, 1] == null)
                        {
                            adapter.SelectCommand.Parameters.AddWithValue(array[i, 0], DBNull.Value);
                        }
                        else
                        {
                            adapter.SelectCommand.Parameters.AddWithValue(array[i, 0], array[i, 1]);
                        }
                    }
                }
                connection.Open();
                var reader = adapter.SelectCommand.ExecuteReader(CommandBehavior.CloseConnection);
                while (reader.Read())
                {
                    supplierIds.Add(reader[0].ToString());
                }

                connection.Close();

                return supplierIds;
            }
        }

        public string GetSupplierTypeIDByExternalID(string externalId)
        {
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                var adapter = new SqlDataAdapter("ZNode_GetSupplierTypeByExternalID", connection) { SelectCommand = { CommandType = CommandType.StoredProcedure } };

                adapter.SelectCommand.Parameters.AddWithValue("@externalId", externalId);

                connection.Open();
                var reader = adapter.SelectCommand.ExecuteReader(CommandBehavior.CloseConnection);

                reader.Read();
                string supplierTypeId = null;

                if (reader.HasRows)
                {
                    supplierTypeId = reader[0].ToString();
                }

                connection.Close();

                return supplierTypeId;
            }
        }

        public List<string> GetSupplierTypeIDsByFilters(string query, string[,] array)
        {
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                var supplierTypeIds = new List<string>();
                var adapter = new SqlDataAdapter(query, connection);
                for (var i = 0; i < array.GetLength(0); i++)
                {
                    if (array[i, 0].Equals("@p" + i))
                    {
                        adapter.SelectCommand.Parameters.AddWithValue(array[i, 0], (DateTime?)Convert.ToDateTime(array[i, 1]));
                    }
                    else
                    {
                        if (array[i, 1] == null)
                        {
                            adapter.SelectCommand.Parameters.AddWithValue(array[i, 0], DBNull.Value);
                        }
                        else
                        {
                            adapter.SelectCommand.Parameters.AddWithValue(array[i, 0], array[i, 1]);
                        }
                    }
                }
                connection.Open();
                var reader = adapter.SelectCommand.ExecuteReader(CommandBehavior.CloseConnection);
                while (reader.Read())
                {
                    supplierTypeIds.Add(reader[0].ToString());
                }

                connection.Close();

                return supplierTypeIds;
            }
        }

        #region Znode Version 8.0
        /// <summary>
        /// Checks supplier is associated with any product or SKU.
        /// </summary>
        /// <param name="supplierId">Id of the supplier.</param>
        /// <returns>Returns true or false.</returns>
        public bool IsAssociatedSupplier(int supplierId)
        {
            int result = 0;
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlCommand command = new SqlCommand("ZNode_CheckAssociatedSupplier", connection);

                // Mark the command as store procedure
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@SupplierId", supplierId);

                // Open the connection 
                connection.Open();

                result = Convert.ToInt32(command.ExecuteScalar());

                // Release the resources
                command.Dispose();
                connection.Close();
            }
            // Return the result.
            return Convert.ToBoolean(result);
        }
        /// <summary>
        /// Check Supplier Type associated with Suppliers 
        /// </summary>
        /// <param name="SupplierTypeId">int SupplierTypeId</param>
        /// <returns>Return true is associate with else return fale</returns>
        public bool CheckAssociatedSupplierTypes(int SupplierTypeId)
        {
            int result = 0;
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlCommand command = new SqlCommand("ZNode_CheckAssociatedZNodeSupplierType", connection);

                // Mark the command as store procedure
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@SupplierTypeID", SupplierTypeId);

                // Open the connection 
                connection.Open();

                result = Convert.ToInt32(command.ExecuteScalar());

                // Release the resources
                command.Dispose();
                connection.Close();
            }
            // Return the result.
            return Convert.ToBoolean(result);
        }
        #endregion
    }
}