﻿
using System;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace ZNode.Libraries.DataAccess.Custom
{
    public partial class AccountHelper
    {
        public DataSet GetSalesRepInfoByRoleName(string roleName)
        {
            DataSet dataSet = new DataSet();
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                try
                {
                    // Create instance of command object
                    SqlDataAdapter adapter = new SqlDataAdapter("PRFT_GetSalesRepInfoByRoleName", connection);

                    // Mark the command as store procedure
                    adapter.SelectCommand.CommandType = CommandType.StoredProcedure;

                    // Add parameters to command object                
                    adapter.SelectCommand.Parameters.AddWithValue("@RoleName", roleName);                    
                    // Create and fill the dataset

                    connection.Open();
                    adapter.Fill(dataSet);

                    // Release the resources
                    adapter.Dispose();
                    connection.Close();

                    // Return the datadet.

                }
                catch (Exception ex)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("Class-AccountHelper:Method-GetSalesRepInfoByRoleName:SP-[PRFT_GetSalesRepInfoByRoleName] - " + ex.Message);
                }
            }
            return dataSet;
        }

        public void UpdateAcountProfileTable(int accountId, int? profileId)
        {           
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                try
                {
                    SqlCommand command = new SqlCommand("PRFT_UpdateAcountProfile", connection);
                    connection.Open();

                    command.CommandType = CommandType.StoredProcedure;
                         
                    // Add parameters to command object                
                    command.Parameters.AddWithValue("@AccountId", accountId);
                    command.Parameters.AddWithValue("@ProfileId", profileId);

                    command.ExecuteNonQuery();
                    connection.Close();                    

                }
                catch (Exception ex)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("Class-AccountHelper:Method-UpdateAcountProfileTable:SP-[PRFT_UpdateAcountProfile] - " + ex.Message);
                }
            }            
        }

        /// <summary>
        /// Returns Address Extension list for an account.
        /// </summary>        
        /// <returns>Returns Address extension dataset.</returns>
        public DataSet GetAddressExtnDetailsByAccountId(int accountId)
        {
            // Create instance of connection
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("PRFT_GetAddressExtnDetailsByAccountId", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;

                // Add parameters to command object                
                adapter.SelectCommand.Parameters.AddWithValue("@AccountID", accountId);


                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

    }
}
