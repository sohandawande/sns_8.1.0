using System.Data;
using System.Data.SqlClient;

namespace ZNode.Libraries.DataAccess.Custom
{
    /// <summary>
    /// This Class Manages Product Attribute Type functionalities
    /// </summary>
    public class AttributeTypeHelper
    {
        /// <summary>
        /// Get the attribute type for this product type
        /// </summary>
        /// <param name="productTypeId">Product type Id to get the attribute type dataset.</param>
        /// <returns>Returns the attribute type dataset.</returns>
        public DataSet GetByProductTypeID(int productTypeId)
        {
            // Create instance of connection
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetByProductTypeID", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;

                adapter.SelectCommand.Parameters.AddWithValue("@ProductTypeId", productTypeId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();

                // Execute the Query
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Get the attributes by attribute type and product Id.
        /// </summary>
        /// <param name="attributeTypeId">Attribute type Id to get the attribute dataset.</param>
        /// <param name="productId">Product Id to get the attribute dataset.</param>
        /// <returns>Returns the attribute dataset.</returns>
        public DataSet GetAttributesByAttributeTypeIdandProductID(int attributeTypeId, int productId)
        {
            // Create instance of connection
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetAttributesByAttributeTypeIdandProductID", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;

                // Add Parameters to SPROC                
                adapter.SelectCommand.Parameters.AddWithValue("@ProductId", productId);
                adapter.SelectCommand.Parameters.AddWithValue("@AttributeTypeId", attributeTypeId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Get the attribute Types for this product type
        /// </summary>
        /// <param name="productTypeId">Product type Id to get the attribute type dataset.</param>
        /// <returns>Returns the attribute type dataset.</returns>
        public DataSet GetAttributeTypesByProductTypeID(int productTypeId)
        {
            // Create instance of connection
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetAttributeTypesByProductType", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@ProductTypeId", productTypeId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Get the attribute types for this product type
        /// </summary>
        /// <param name="productTypeId">Product type Id to get the attribute type dataset.</param>
        /// <returns>Returns the attribute type dataset.</returns>
        public DataSet GetAttributeTypesByProductType(int productTypeId)
        {
            // Create instance of connection
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetAttributeTypesByProductTypeID", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@ProductTypeId", productTypeId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Get the attributes for this product type
        /// </summary>
        /// <param name="productTypeId">Product type Id to get the attribute dataset.</param>
        /// <returns>Returns the attribute dataset.</returns>
        public DataSet GetAttributesByProductTypeID(int productTypeId)
        {
            // Create instance of connection     
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetAttributesByProductType", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@ProductTypeId", productTypeId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Get the attribute for this catalog Id.
        /// </summary>
        /// <param name="CatalogId">Catalog Id to get the attribute type dataset.</param>
        /// <returns>Returns the attribute dataset.</returns>
        public DataSet GetAttributesByCatalogID(int CatalogId)
        {
            // Create instance of connection     
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetAttributesByCatalogID", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;

                adapter.SelectCommand.Parameters.AddWithValue("@CatalogID", CatalogId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }
    }
}
