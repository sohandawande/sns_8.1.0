﻿using System;
using System.Data;
using System.Data.SqlClient;


namespace ZNode.Libraries.DataAccess.Custom
{
    public class StateHelper
    {
        /// <summary>
        /// Get the State Details based on the CountryCode.
        /// </summary>
        /// <param name="countryCode">Code for the Country.</param>
        /// <returns>DataSet</returns>
        public DataSet GetStateDetails(string countryCode)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetZNodeState", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                if (string.IsNullOrEmpty(countryCode) || Equals(countryCode, "0"))
                {
                    adapter.SelectCommand.Parameters.AddWithValue("@SearchUsingOR", false);
                }
                else
                {
                    adapter.SelectCommand.Parameters.AddWithValue("@CountryCode", countryCode);
                }

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }
    }
}
