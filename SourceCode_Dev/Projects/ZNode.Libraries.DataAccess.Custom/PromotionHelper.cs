using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace ZNode.Libraries.DataAccess.Custom
{
    public class PromotionHelper
    {
        /// <summary>
        /// Get all the promotions.
        /// </summary>        
        /// <returns>Returns promotion dataset.</returns>
        public DataSet GetAllPromotions()
        {
            // Create instance of connection Object
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetAllPromotions", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        public string GetPromotionIDByExternalID(string externalId)
        {
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                var adapter = new SqlDataAdapter("ZNode_GetPromotionByExternalID", connection) { SelectCommand = { CommandType = CommandType.StoredProcedure } };

                adapter.SelectCommand.Parameters.AddWithValue("@externalId", externalId);

                connection.Open();
                var reader = adapter.SelectCommand.ExecuteReader(CommandBehavior.CloseConnection);

                reader.Read();
                string promotionId = null;

                if (reader.HasRows)
                {
                    promotionId = reader[0].ToString();
                }

                connection.Close();

                return promotionId;
            }
        }

        public List<string> GetPromotionIDsByFilters(string query, string[,] array)
        {
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                var promotionIds = new List<string>();
                var adapter = new SqlDataAdapter(query, connection);
                for (var i = 0; i < array.GetLength(0); i++)
                {
                    if (array[i, 0].Equals("@p" + i))
                    {
                        adapter.SelectCommand.Parameters.AddWithValue(array[i, 0], (DateTime?)Convert.ToDateTime(array[i, 1]));
                    }
                    else
                    {
                        if (array[i, 1] == null)
                        {
                            adapter.SelectCommand.Parameters.AddWithValue(array[i, 0], DBNull.Value);
                        }
                        else
                        {
                            adapter.SelectCommand.Parameters.AddWithValue(array[i, 0], array[i, 1]);
                        }
                    }
                }
                connection.Open();
                var reader = adapter.SelectCommand.ExecuteReader(CommandBehavior.CloseConnection);
                while (reader.Read())
                {
                    promotionIds.Add(reader[0].ToString());
                }

                connection.Close();

                return promotionIds;
            }
        }

        public List<string> GetDiscountTypeIDsByFilters(string query, string[,] array)
        {
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                var discountTypeIds = new List<string>();
                var adapter = new SqlDataAdapter(query, connection);
                for (var i = 0; i < array.GetLength(0); i++)
                {
                    if (array[i, 0].Equals("@p" + i))
                    {
                        adapter.SelectCommand.Parameters.AddWithValue(array[i, 0], (DateTime?)Convert.ToDateTime(array[i, 1]));
                    }
                    else
                    {
                        if (array[i, 1] == null)
                        {
                            adapter.SelectCommand.Parameters.AddWithValue(array[i, 0], DBNull.Value);
                        }
                        else
                        {
                            adapter.SelectCommand.Parameters.AddWithValue(array[i, 0], array[i, 1]);
                        }
                    }
                }
                connection.Open();
                var reader = adapter.SelectCommand.ExecuteReader(CommandBehavior.CloseConnection);
                while (reader.Read())
                {
                    discountTypeIds.Add(reader[0].ToString());
                }

                connection.Close();

                return discountTypeIds;
            }
        }

        #region Znode Version 8.0
        /// <summary>
        /// Check PromotionType associated with Promotions 
        /// </summary>
        /// <param name="PromotionTypeId">int PromotionTypeId</param>
        /// <returns>Return true is associate with else return fale</returns>
        public bool CheckAssociatedPromtions(int PromotionTypeId)
        {
            int result = 0;
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlCommand command = new SqlCommand("ZNode_CheckAssociatedZnodeDiscountType", connection);

                // Mark the command as store procedure
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@DiscountTypeId", PromotionTypeId);

                // Open the connection 
                connection.Open();

                result = Convert.ToInt32(command.ExecuteScalar());

                // Release the resources
                command.Dispose();
                connection.Close();
            }
            // Return the result.
            return Convert.ToBoolean(result);
        }
        #endregion
    }
}
