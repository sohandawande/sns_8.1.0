﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace ZNode.Libraries.DataAccess.Custom
{
    public class RejectionMessageHelper
    {
        /// <summary>
        /// get the Rejection message by rejection message Id.
        /// </summary>
        /// <param name="rejectionMessageId">Id of the rejection message.</param>
        /// <returns>DataSet</returns>
        public DataSet GetRejectionMessage(int rejectionMessageId)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_RejectionMessages", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@RejectionMessagesId", rejectionMessageId);
                adapter.SelectCommand.Parameters.AddWithValue("@Action", "GET");

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Create or update the rejection message.
        /// </summary>
        /// <param name="rejectionMessageId">Id of the rejection message.</param>
        /// <param name="messageKey">Message Key.</param>
        /// <param name="messageValue">Message Value.</param>
        /// <param name="portalId">Portal Id.</param>
        /// <param name="localeId">Locale Id.</param>
        /// <param name="action">specified action.</param>
        /// <returns>DataSet</returns>
        public DataSet CreateUpdateRejectionMessage(int rejectionMessageId, string messageKey, string messageValue, int portalId, int localeId, string action)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_RejectionMessages", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@RejectionMessagesId", rejectionMessageId);
                adapter.SelectCommand.Parameters.AddWithValue("@MessageKey", messageKey);
                adapter.SelectCommand.Parameters.AddWithValue("@MessageValue", messageValue);
                adapter.SelectCommand.Parameters.AddWithValue("@PortalId", portalId);
                adapter.SelectCommand.Parameters.AddWithValue("@LocaleId", localeId);
                adapter.SelectCommand.Parameters.AddWithValue("@Action", action);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Delete the rejection message.
        /// </summary>
        /// <param name="rejectionMessageId">Id of the rejection message.</param>
        /// <returns>Returns True or False.</returns>
        public bool DeleteRejectionMessage(int rejectionMessageId)
        {
            int results = 0;
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlCommand command = new SqlCommand("ZNode_RejectionMessages", connection);

                // Mark the command as store procedure
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@RejectionMessagesId", rejectionMessageId);
                command.Parameters.AddWithValue("@Action", "DELETE");

                // Open the connection 
                connection.Open();

                results = Convert.ToInt32(command.ExecuteScalar());

                // Release the resources
                command.Dispose();
                connection.Close();
            }
            // Return the result.
            return Convert.ToBoolean(results);
        }
    }
}
