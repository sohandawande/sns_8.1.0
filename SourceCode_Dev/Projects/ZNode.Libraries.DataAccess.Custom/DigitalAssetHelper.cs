using System.Data;
using System.Data.SqlClient;

namespace ZNode.Libraries.DataAccess.Custom
{
    /// <summary>
    /// Digital Asset helper methods related to custom data access
    /// </summary>
    public class DigitalAssetHelper
    {
        /// <summary>
        /// Get the digital assets for a product
        /// </summary>
        /// <param name="productId">Product Id to get the digitial asset.</param>
        /// <param name="quantity">Get top N disgital assets items.</param>
        /// <returns>Returns the product top n digital assets dataset.</returns>
        public string GetDigitalAssetsByProductAndQuantityXML(int productId, int quantity)
        {
            // Create instance of connection and Command Object
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlDataReader reader = null;
                System.Text.StringBuilder xmlOut = new System.Text.StringBuilder();

                try
                {
                    SqlCommand command = new SqlCommand("ZNODE_GetDigitalAssetsByProductId_XML", connection);

                    // Mark the command as store procedure
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@ProductID", productId);
                    command.Parameters.AddWithValue("@Quantity", quantity);

                    // Execute the command
                    connection.Open();

                    reader = command.ExecuteReader(CommandBehavior.CloseConnection);

                    xmlOut.Append("<ZNodeDigitalAssetList>");

                    while (reader.Read())
                    {
                        xmlOut.Append(reader[0].ToString());
                    }

                    xmlOut.Append("</ZNodeDigitalAssetList>");
                }
                finally
                {
                    if (reader != null)
                    {
                        // Close the reader
                        reader.Close();
                    }

                    if (connection != null)
                    {
                        // Close the connection
                        connection.Close();
                    }
                }

                return xmlOut.ToString();
            }
        }
    }
}
