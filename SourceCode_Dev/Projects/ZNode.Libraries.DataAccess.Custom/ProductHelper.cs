using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.DataAccess.Custom
{
    /// <summary>
    /// Represents the ProductHelper class.
    /// </summary>
    public class ProductHelper
    {

        #region Public Methods

        /// <summary>
        /// Get the category hierarchical path (Upto 3 Levels)
        /// </summary>
        /// <param name="categoryName">Root category name.</param>
        /// <param name="parentCategoryName1">Parent category name1.</param>
        /// <param name="parentCategoryName2">Parent category name2.</param>
        /// <returns>Returns the category hierarchical path(upto 3 levels)</returns>
        public static string GetCategoryPath(string categoryName, string parentCategoryName1, string parentCategoryName2)
        {
            var categoryPath = new StringBuilder();
            if (parentCategoryName2.Trim().Length > 0)
            {
                categoryPath.Append(parentCategoryName2);
                categoryPath.Append(" > ");
            }

            if (parentCategoryName1.Trim().Length > 0)
            {
                categoryPath.Append(parentCategoryName1);
                categoryPath.Append(" > ");
            }

            categoryPath.Append(categoryName);
            return categoryPath.ToString();
        }

        /// <summary>
        /// Get products by products Ids.
        /// </summary>
        /// <param name="productIds">Comma seperated product Ids.</param> 
        /// <param name="accountId">Account Id of the product.</param>       
        /// <returns>Returns the product details in XML format.</returns>
        public string GetProductsByIds(string productIds, int accountId = 0)
        {
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                var command = new SqlCommand("ZNode_GetProductsByIds", connection) { CommandType = CommandType.StoredProcedure };

                // Mark the command as store procedure

                // Add Parameters to SPROC
                var parameterProductList = new SqlParameter("@ProductIds", productIds);
                command.Parameters.Add(parameterProductList);
                var parameterAccountId = new SqlParameter("@AccountId", accountId);
                command.Parameters.Add(parameterAccountId);

                // Execute the command
                connection.Open();

                var reader = command.ExecuteReader(CommandBehavior.CloseConnection);

                var xmlOut = new System.Text.StringBuilder();

                while (reader.Read())
                {
                    xmlOut.Append(reader[0].ToString());
                }

                reader.Close();
                connection.Close();

                return xmlOut.ToString();
            }
        }

        /// <summary>
        /// Get all product details for a product Id.
        /// </summary>
        /// <param name="productId">Get product details for the product.</param>
        /// <returns>Returns the product details dataset.</returns>
        public DataSet GetProductDetails(int productId)
        {
            // Create instance of connection  Object   
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                var adapter = new SqlDataAdapter("ZNODE_GETPRODUCTDETAILS", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@PRODUCT_ID", productId);

                // Create and fill the dataset
                var dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Get a category for a product 
        /// </summary>
        /// <param name="productId">Get category details by product Id.</param>
        /// <returns>Returns the category details dataset.</returns>
        public DataSet Get_CategoryByProductID(int productId)
        {
            // Create instance of connection  Object   
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                var adapter = new SqlDataAdapter("ZNODE_GETCATEGORYBYPRODUCTID", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@PRODUCT_ID", productId);

                // Create and fill the dataset
                var dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Get products list by category Id, portal Id and locale Id.
        /// </summary>
        /// <param name="categoryId">Category Id to get product dataset.</param>
        /// <param name="portalId">Portal Id to get product dataset.</param>
        /// <param name="localeId">Locale Id to get product dataset.</param>
        /// <returns>Returns the product dataset.</returns>
        public DataTable GetProductsByCategoryID(int categoryId, int portalId, int localeId, int profileId)
        {
            // Create instance of connection  Object   
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                var adapter = new SqlDataAdapter("ZNode_GetProductsByCategoryID", connection);

                // Mark the Command as a Stored Procedures
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@categoryId", categoryId);
                adapter.SelectCommand.Parameters.AddWithValue("@PortalID", portalId);
                adapter.SelectCommand.Parameters.AddWithValue("@LocaleId", localeId);
                adapter.SelectCommand.Parameters.AddWithValue("@profileId", profileId);

                var dataTable = new DataTable();
                adapter.Fill(dataTable);

                connection.Close();

                // Return datatable
                return dataTable;
            }
        }

        /// <summary>
        /// Get all products by catalog Id.
        /// </summary>
        /// <param name="catalogId">Catalog Id to get the products dataset.</param>
        /// <returns>Returns the products dataset.</returns>
        public DataSet GetProductsByCatalogID(int catalogId)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                var adapter = new SqlDataAdapter("ZNode_GetProductsByCatalogID", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@CatalogId", catalogId);

                // Create and fill the dataset
                var dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Update the display order.
        /// </summary>
        /// <param name="catalogId">Catalog Id of the product</param>
        /// <param name="productId">Product Id to update the display order.</param>
        /// <param name="displayOrder">Display order number to update.</param>        
        public void UpdateDisplayOrder(int catalogId, int productId, int displayOrder)
        {
            // Create instance of connection  Object   
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                var command = new SqlCommand("ZNode_UpdateDisplayOrderOfProduct", connection);

                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@CatalogId", catalogId);
                command.Parameters.AddWithValue("@ProductId", productId);
                command.Parameters.AddWithValue("@DisplayOrder", displayOrder);

                connection.Open();

                command.ExecuteNonQuery();

                connection.Close();
            }
        }

        /// <summary>
        /// Empty catalog data
        /// </summary>
        /// <param name="userName">Current logged in admin user</param>
        /// <returns>Returns true if catalog items removed otherwise false.</returns>        
        public bool EmptyCatalog(string userName)
        {
            bool isSuccess = false;

            // Create instance of connection  Object   
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlCommand command = null;
                System.Data.SqlClient.SqlTransaction transaction = null;

                try
                {
                    // Open Connection
                    connection.Open();

                    // BeginTransaction() Requires Open Connection
                    transaction = connection.BeginTransaction();

                    // Create instance of Command Object
                    command = new SqlCommand("ZNode_EMPTYCATALOG", connection);

                    // Mark the Command as a Stored Procedure
                    command.CommandType = CommandType.StoredProcedure;

                    // Assign Transaction to Command
                    command.Transaction = transaction;

                    command.Parameters.AddWithValue("@UserName", userName);

                    // Execute Query
                    command.ExecuteNonQuery();

                    // Set Return values 
                    isSuccess = true;

                    // If we reach here, all command succeeded, so commit the transaction
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    ZNodeLoggingBase.LogMessage("Empty Cataog - " + ex.Message);

                    // Something went wrong, so rollback the transaction
                    transaction.Rollback();
                    isSuccess = false;
                }
                finally
                {
                    if (transaction != null)
                    {
                        transaction.Dispose();
                    }

                    if (connection != null)
                    {
                        // Finally, close the connection
                        connection.Close();

                        // Release the resources
                        connection.Dispose();
                    }
                }
            }

            // Return boolean value
            return isSuccess;
        }

        /// <summary>
        /// Znode Version 8.0
        /// To Associate Product Category
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <param name="categoryIds"> string categoryIds</param>
        /// <returns>returns true/false</returns>
        public bool AssociateProductCategory(int productId, string categoryIds)
        {
            bool isSuccess = false;

            // Create instance of connection  Object   
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlCommand command = null;
                System.Data.SqlClient.SqlTransaction transaction = null;

                try
                {
                    // Open Connection
                    connection.Open();

                    // BeginTransaction() Requires Open Connection
                    transaction = connection.BeginTransaction();

                    // Create instance of Command Object
                    command = new SqlCommand("ZNode_InsertProductToMultipleCategory", connection);

                    // Mark the Command as a Stored Procedure
                    command.CommandType = CommandType.StoredProcedure;

                    // Assign Transaction to Command
                    command.Transaction = transaction;

                    // Add parameters
                    command.Parameters.AddWithValue("@ProductId", productId);
                    command.Parameters.AddWithValue("@CategoryId", categoryIds);

                    // Execute Query
                    command.ExecuteNonQuery();

                    // Set Return values 
                    isSuccess = true;

                    // If we reach here, all command succeeded, so commit the transaction
                    transaction.Commit();
                }
                catch (Exception)
                {
                    // Something went wrong, so rollback the transaction
                    transaction.Rollback();
                    isSuccess = false;
                }
                finally
                {
                    if (transaction != null)
                    {
                        transaction.Dispose();
                    }

                    if (connection != null)
                    {
                        // Finally, close the connection
                        connection.Close();

                        // Release the resources
                        connection.Dispose();
                    }
                }
            }

            // Return boolean value
            return isSuccess;
        }

        /// <summary>
        /// To Associate Product Category
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <param name="categoryId">string categoryIds</param>
        /// <returns>returns true/false</returns>
        public bool UnAssociateProductCategory(int productId, int categoryId)
        {
            bool isSuccess = false;

            // Create instance of connection  Object   
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlCommand command = null;
                System.Data.SqlClient.SqlTransaction transaction = null;

                try
                {
                    // Open Connection
                    connection.Open();

                    // BeginTransaction() Requires Open Connection
                    transaction = connection.BeginTransaction();

                    // Create instance of Command Object
                    command = new SqlCommand("ZNode_DeleteProductToMultipleCategory", connection);

                    // Mark the Command as a Stored Procedure
                    command.CommandType = CommandType.StoredProcedure;

                    // Assign Transaction to Command
                    command.Transaction = transaction;

                    // Add parameters
                    command.Parameters.AddWithValue("@ProductId", productId);
                    command.Parameters.AddWithValue("@CategoryId", categoryId);

                    // Execute Query
                    command.ExecuteNonQuery();

                    // Set Return values 
                    isSuccess = true;

                    // If we reach here, all command succeeded, so commit the transaction
                    transaction.Commit();
                }
                catch (Exception)
                {
                    // Something went wrong, so rollback the transaction
                    transaction.Rollback();
                    isSuccess = false;
                }
                finally
                {
                    if (transaction != null)
                    {
                        transaction.Dispose();
                    }

                    if (connection != null)
                    {
                        // Finally, close the connection
                        connection.Close();

                        // Release the resources
                        connection.Dispose();
                    }
                }
            }

            // Return boolean value
            return isSuccess;
        }

        /// <summary>
        /// To Delete Sku Associated Facets
        /// </summary>
        /// <param name="skuId">int skuId</param>
        /// <param name="facetGroupId">int facetGroupId</param>
        /// <returns>returns true/false</returns>
        public bool DeleteSkuAssociatedFacets(int skuId, int facetGroupId)
        {
            bool isSuccess = false;

            // Create instance of connection  Object   
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlCommand command = null;
                System.Data.SqlClient.SqlTransaction transaction = null;

                try
                {
                    // Open Connection
                    connection.Open();

                    // BeginTransaction() Requires Open Connection
                    transaction = connection.BeginTransaction();

                    // Create instance of Command Object
                    command = new SqlCommand("ZNode_DeleteFacetProductSKU", connection);

                    // Mark the Command as a Stored Procedure
                    command.CommandType = CommandType.StoredProcedure;

                    // Assign Transaction to Command
                    command.Transaction = transaction;

                    // Add parameters
                    command.Parameters.AddWithValue("@SKUID", skuId);
                    command.Parameters.AddWithValue("@FacetGroupID", facetGroupId);

                    // Execute Query
                    command.ExecuteNonQuery();

                    // Set Return values 
                    isSuccess = true;

                    // If we reach here, all command succeeded, so commit the transaction
                    transaction.Commit();
                }
                catch (Exception)
                {
                    // Something went wrong, so rollback the transaction
                    transaction.Rollback();
                    isSuccess = false;
                }
                finally
                {
                    if (transaction != null)
                    {
                        transaction.Dispose();
                    }

                    if (connection != null)
                    {
                        // Finally, close the connection
                        connection.Close();

                        // Release the resources
                        connection.Dispose();
                    }
                }
            }

            // Return boolean value
            return isSuccess;
        }

        /// <summary>
        /// To Associate Sku Facets
        /// </summary>
        /// <param name="associateFacetIds">string associateFacetIds</param>
        /// <param name="unassociateFacetIds">string unassociateFacetIds</param>
        /// <param name="productId">int? productId</param>
        /// <param name="skuId">int? skuId</param>
        /// <returns>returns true/false</returns>
        public bool AssociateSkuFacets(int? skuId, string associateFacetIds, string unassociateFacetIds)
        {
            bool isSuccess = false;

            // Create instance of connection  Object   
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlCommand command = null;
                System.Data.SqlClient.SqlTransaction transaction = null;

                try
                {
                    //If already assing sku with facets the remove existing facets
                    if (!string.IsNullOrEmpty(unassociateFacetIds) && unassociateFacetIds != "0")
                    {
                        // Open Connection
                        connection.Open();

                        // BeginTransaction() Requires Open Connection
                        transaction = connection.BeginTransaction();

                        // Create instance of Command Object
                        command = new SqlCommand("ZNode_FacetProductSKU", connection);

                        // Mark the Command as a Stored Procedure
                        command.CommandType = CommandType.StoredProcedure;

                        // Assign Transaction to Command
                        command.Transaction = transaction;

                        // Add parameters
                        command.Parameters.AddWithValue("@ProductID", 0);
                        command.Parameters.AddWithValue("@SKUID", skuId);
                        command.Parameters.AddWithValue("@FacetID", unassociateFacetIds);
                        command.Parameters.AddWithValue("@Action", "DELETE");
                        // Execute Query
                        command.ExecuteNonQuery();
                        transaction.Commit();
                        connection.Close();
                    }

                    if (!string.IsNullOrEmpty(associateFacetIds) && associateFacetIds != "0")
                    {
                        // Open Connection
                        connection.Open();

                        // BeginTransaction() Requires Open Connection
                        transaction = connection.BeginTransaction();

                        // Create instance of Command Object
                        command = new SqlCommand("ZNode_FacetProductSKU", connection);

                        // Mark the Command as a Stored Procedure
                        command.CommandType = CommandType.StoredProcedure;

                        // Assign Transaction to Command
                        command.Transaction = transaction;

                        // Add parameters
                        command.Parameters.AddWithValue("@ProductID", null);
                        command.Parameters.AddWithValue("@SKUID", skuId);
                        command.Parameters.AddWithValue("@FacetID", associateFacetIds);
                        command.Parameters.AddWithValue("@Action", "Insert");
                        // Execute Query
                        command.ExecuteNonQuery();
                    }

                    // Set Return values 
                    isSuccess = true;

                    // If we reach here, all command succeeded, so commit the transaction
                    transaction.Commit();
                }
                catch (Exception)
                {
                    // Something went wrong, so rollback the transaction
                    transaction.Rollback();
                    isSuccess = false;
                }
                finally
                {
                    if (transaction != null)
                    {
                        transaction.Dispose();
                    }

                    if (connection != null)
                    {
                        // Finally, close the connection
                        connection.Close();

                        // Release the resources
                        connection.Dispose();
                    }
                }
            }

            // Return boolean value
            return isSuccess;
        }

        /// <summary>
        /// Get product data as XML for serialization
        /// </summary>
        /// <param name="productId">Product Id to get the product dataset.</param>
        /// <param name="portalId">Portal Id to get the product dataset.</param>
        /// <param name="localeId">Locale Id of the product.</param>
        /// <param name="profileId">Profile Id of the product.</param>
        /// <param name="accountId">Account Id of the product.</param>
        /// <returns>Returns the product details dataset.</returns>
        public string GetProductXML(int productId, int portalId, int localeId, int profileId, int accountId = 0)
        {
            // Create instance of connection  Object   
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlDataReader reader = null;
                var xmlOut = new System.Text.StringBuilder();

                try
                {
                    var command = new SqlCommand("ZNode_GetProductByProductId_XML", connection);

                    // Mark the command as store procedure
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@PortalID", portalId);
                    command.Parameters.AddWithValue("@LocaleId", localeId);
                    command.Parameters.AddWithValue("@ProductID", productId);
                    command.Parameters.AddWithValue("@profileId", profileId);
                    command.Parameters.AddWithValue("@accountId", accountId);
                    // Execute the command
                    connection.Open();

                    reader = command.ExecuteReader(CommandBehavior.CloseConnection);

                    while (reader.Read())
                    {
                        xmlOut.Append(reader[0].ToString());
                    }
                }
                finally
                {
                    if (reader != null)
                    {
                        // Close the reader
                        reader.Close();
                    }

                    if (connection != null)
                    {
                        // Close the connection
                        connection.Close();
                    }
                }

                // return XML string
                return xmlOut.ToString();
            }
        }

        /// <summary>
        /// Get product data as XML for serialization
        /// </summary>
        /// <param name="productId">Product Id to get the product details as XML.</param>        
        /// <returns>Returns the product details XML data.</returns>
        public string GetProductByProductId(int productId)
        {
            // Create instance of connection  Object   
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlDataReader reader = null;
                var xmlOut = new System.Text.StringBuilder();

                try
                {
                    SqlCommand command = new SqlCommand("ZNode_GetProductByProductId", connection);

                    // Mark the command as store procedure
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@ProductID", productId);

                    // Execute the command
                    connection.Open();

                    reader = command.ExecuteReader(CommandBehavior.CloseConnection);

                    while (reader.Read())
                    {
                        xmlOut.Append(reader[0].ToString());
                    }
                }
                finally
                {
                    if (reader != null)
                    {
                        // Close the reader
                        reader.Close();
                    }

                    if (connection != null)
                    {
                        // Close the connection
                        connection.Close();
                    }
                }

                return xmlOut.ToString();
            }
        }

        /// <summary>
        /// Gets a page of products from the DataSource.
        /// </summary>
        /// <param name="portalId">The portal Id to search for.</param>
        /// <param name="name">The product name to search for.</param>
        /// <param name="productNum">The product number to search for.</param>
        /// <param name="sku">The product Sku to search for.</param>
        /// <param name="brand">Brand name to search for.</param>
        /// <param name="categoryName">Category name to search for.</param>
        /// <param name="pageIndex">Row number at which to start reading.</param>
        /// <param name="pageSize">Number of rows to return.</param>
        /// <param name="catalogId">Catalog Id to search for.</param>
        /// <param name="totalPages">Out Parameter, Number of rows in the DataSource.</param>
        /// <returns>Returns a product collection as datatable.</returns>
        public DataTable GetPagedProductListByPortalID(int portalId, string name, string productNum, string sku, string brand, string categoryName, int pageIndex, int pageSize, int catalogId, int? filterPortalID, out int totalPages, string externalAccountNo)
        {
            totalPages = 0;

            // Create instance of connection and Command Object
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                var command = new SqlCommand("ZNODE_GetPagedProductListByPortalID", connection);

                // Mark the command as store procedure
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@PortalID", portalId);
                command.Parameters.AddWithValue("@Name", name);
                command.Parameters.AddWithValue("@ProductNum", productNum);
                command.Parameters.AddWithValue("@Sku", sku);
                command.Parameters.AddWithValue("@Brand", brand);
                command.Parameters.AddWithValue("@Category", categoryName);
                command.Parameters.AddWithValue("@PageIndex", pageIndex);
                command.Parameters.AddWithValue("@PageSize", pageSize);
                command.Parameters.AddWithValue("@CatalogId", catalogId);
                command.Parameters.AddWithValue("@FilterPortalID", filterPortalID);
                command.Parameters.AddWithValue("@ExternalAccountNo", externalAccountNo);
                var parameterTotalPages = new SqlParameter("@TotalPages", SqlDbType.Int);
                parameterTotalPages.Direction = ParameterDirection.Output;
                command.Parameters.Add(parameterTotalPages);

                // Execute the command
                connection.Open();

                var adapter = new SqlDataAdapter(command);

                var productList = new DataTable();

                adapter.Fill(productList);

                totalPages = (int)parameterTotalPages.Value;

                // Close the connection
                command.Clone();
                connection.Close();

                return productList;
            }
        }

        /// <summary>
        /// Gets a page of products from the DataSource.
        /// </summary>
        /// <param name="portalId">The portal Id to search for.</param>
        /// <param name="name">The product name to search for.</param>
        /// <param name="productNum">The product number to search for.</param>
        /// <param name="sku">The product Sku to search for.</param>
        /// <param name="brand">Brand name to search for.</param>
        /// <param name="categoryName">Category name to search for.</param>
        /// <param name="pageIndex">Row number at which to start reading.</param>
        /// <param name="pageSize">Number of rows to return.</param>
        /// <param name="catalogId">Catalog Id to search for.</param>
        /// <param name="totalPages">Out Parameter, Number of rows in the DataSource.</param>
        /// <returns>Returns a product collection as datatable.</returns>
        public DataTable GetPagedProductListByPortalID(int portalId, string name, string productNum, string sku, string brand, string categoryName, int pageIndex, int pageSize, int catalogId, int? filterPortalID, out int totalPages, string externalAccountNo, out int totalRecordCount)
        {
            totalPages = 0;
            totalRecordCount = 0;

            // Create instance of connection and Command Object
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                var command = new SqlCommand("ZNODE_GetPagedProductListByPortalID", connection);

                // Mark the command as store procedure
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@PortalID", portalId);
                command.Parameters.AddWithValue("@Name", name);
                command.Parameters.AddWithValue("@ProductNum", productNum);
                command.Parameters.AddWithValue("@Sku", sku);
                command.Parameters.AddWithValue("@Brand", brand);
                command.Parameters.AddWithValue("@Category", categoryName);
                command.Parameters.AddWithValue("@PageIndex", pageIndex);
                command.Parameters.AddWithValue("@PageSize", pageSize);
                command.Parameters.AddWithValue("@CatalogId", catalogId);
                command.Parameters.AddWithValue("@FilterPortalID", filterPortalID);
                command.Parameters.AddWithValue("@ExternalAccountNo", externalAccountNo);
                var parameterTotalPages = new SqlParameter("@TotalPages", SqlDbType.Int);
                var parameterTotalRowCount = new SqlParameter("@TotalRecordCount", SqlDbType.Int);
                parameterTotalRowCount.Direction = ParameterDirection.Output;
                parameterTotalPages.Direction = ParameterDirection.Output;
                command.Parameters.Add(parameterTotalPages);
                command.Parameters.Add(parameterTotalRowCount);

                // Execute the command
                connection.Open();

                var adapter = new SqlDataAdapter(command);

                var productList = new DataTable();

                adapter.Fill(productList);

                totalPages = (int)parameterTotalPages.Value;

                totalRecordCount = (int)parameterTotalRowCount.Value;
                // Close the connection
                command.Clone();
                connection.Close();

                return productList;
            }
        }

        /// <summary>
        /// Search products and return XML
        /// </summary>
        /// <param name="portalId">The portal Id to search for.</param>        
        /// <param name="catalogId">The Catalog id to search for.</param>
        /// <param name="keywords">Keyword to search.</param>
        /// <param name="delimiter">Delimiter character</param>
        /// <param name="categoryId">Product category Id.</param>
        /// <param name="searchOption">Search option (0, 1, 2)</param>
        /// <param name="sku">The product sku to search for.</param>
        /// <param name="productNum">Product number to search.</param>        
        /// <returns>Returns a product collection as xml string.</returns>
        public string SearchProductsXML(int portalId, int catalogId, string keywords, string delimiter, int categoryId, int searchOption, string sku, string productNum, int profileId)
        {
            // Create instance of connection and Command Object
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                var command = new SqlCommand("ZNode_GetProductsBySearch_XML", connection);

                // Mark the command as store procedure
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@PortalID", portalId);
                command.Parameters.AddWithValue("@CatalogId", catalogId);
                command.Parameters.AddWithValue("@Keywords", keywords);
                command.Parameters.AddWithValue("@Delimiter", delimiter);
                command.Parameters.AddWithValue("@CategoryId", categoryId);
                command.Parameters.AddWithValue("@SearchOption", searchOption);
                command.Parameters.AddWithValue("@SKU", sku);
                command.Parameters.AddWithValue("@ProductNum", productNum);

                command.Parameters.AddWithValue("@profileId", profileId);

                // Execute the command
                connection.Open();

                var dataAdapter = new SqlDataAdapter();
                dataAdapter.SelectCommand = command;

                var dataTable = new DataTable();

                dataAdapter.Fill(dataTable);

                System.Web.HttpContext.Current.Session["ProductList"] = dataTable;

                connection.Close();

                return string.Empty;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="portalId"></param>
        /// <param name="localeId"></param>
        /// <param name="displayItem"></param>
        /// <param name="profileId"></param>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public string GetHomePageSpecialsXML(int portalId, int localeId, int displayItem, int profileId, int accountId = 0)
        {
            // Create instance of connection and Command Object
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlDataReader reader = null;
                var xmlOut = new System.Text.StringBuilder();

                try
                {
                    var command = new SqlCommand("ZNode_GetHomePageSpecials_XML", connection);

                    // Mark the command as store procedure
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@PortalID", portalId);
                    command.Parameters.AddWithValue("@LocaleId", localeId);
                    command.Parameters.AddWithValue("@DisplayItem", displayItem);
                    command.Parameters.AddWithValue("@ProfileId", profileId);
                    command.Parameters.AddWithValue("@accountId", accountId);
                    // Execute the command
                    connection.Open();

                    reader = command.ExecuteReader(CommandBehavior.CloseConnection);

                    while (reader.Read())
                    {
                        xmlOut.Append(reader[0].ToString());
                    }
                }
                finally
                {
                    if (reader != null)
                    {
                        // Close the reader
                        reader.Close();
                    }

                    if (connection != null)
                    {
                        // Close the connection
                        connection.Close();
                    }
                }

                return xmlOut.ToString();
            }
        }

        /// <summary>
        /// Get the Products for a brand
        /// </summary>
        /// <param name="portalId">Portal Id to get the products by brand.</param>
        /// <param name="manufacturerId">Manufacturer Id of the products.</param>
        /// <param name="localeId">Locale Id of the product.</param>        
        /// <returns>Returns the product details by product XML.</returns>
        public string GetProductsByBrandXML(int portalId, int manufacturerId, int localeId)
        {
            // Create instance of connection and Command Object
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlDataReader reader = null;
                var xmlOut = new System.Text.StringBuilder();

                try
                {
                    var command = new SqlCommand("ZNode_GetProductsByBrand_XML", connection);

                    // Mark the command as store procedure
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@PortalID", portalId);
                    command.Parameters.AddWithValue("@ManufacturerID", manufacturerId);
                    command.Parameters.AddWithValue("@LocaleId", localeId);

                    // Execute the command
                    connection.Open();

                    reader = command.ExecuteReader(CommandBehavior.CloseConnection);

                    while (reader.Read())
                    {
                        xmlOut.Append(reader[0].ToString());
                    }
                }
                finally
                {
                    if (reader != null)
                    {
                        // Close the reader
                        reader.Close();
                    }

                    if (connection != null)
                    {
                        // Close the connection
                        connection.Close();
                    }
                }

                return xmlOut.ToString();
            }
        }

        /// <summary>
        /// Get the products by price.
        /// </summary>
        /// <param name="portalId">Portal Id to get the products by price.</param>
        /// <param name="localeId">Locale Id of the product.</param>
        /// <param name="minimumPrice">Get products minimum from this price.</param>
        /// <param name="maximumPrice">Get products maximum to this price.</param>
        /// <returns>Returns the products XML by price.</returns>
        public string GetProductsByPriceXML(int portalId, int localeId, decimal minimumPrice, decimal maximumPrice)
        {
            // Create instance of connection and Command Object
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlDataReader reader = null;
                var xmlOut = new System.Text.StringBuilder();

                try
                {
                    var command = new SqlCommand("ZNode_GetProductsByPrice_XML", connection);

                    // Mark the command as store procedure
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@PortalID", portalId);
                    command.Parameters.AddWithValue("@LocaleId", localeId);
                    command.Parameters.AddWithValue("@StartValue", minimumPrice);
                    command.Parameters.AddWithValue("@EndValue", maximumPrice);

                    // Execute the command
                    connection.Open();

                    reader = command.ExecuteReader(CommandBehavior.CloseConnection);

                    while (reader.Read())
                    {
                        xmlOut.Append(reader[0].ToString());
                    }
                }
                finally
                {
                    if (reader != null)
                    {
                        // Close the reader
                        reader.Close();
                    }

                    if (connection != null)
                    {
                        // Close the connection
                        connection.Close();
                    }
                }

                return xmlOut.ToString();
            }
        }

        /// <summary>
        /// Gets sku by product attribute.
        /// </summary>
        /// <param name="productId">Product Id to get the Sku attribute</param>
        /// <param name="attributes">Attributes to get the product sku.</param>
        /// <param name="accountId">AccountId string to get the sku object.</param>
        /// <returns>Returns the sku by product attributes.</returns>
        public string GetSKUByAttributes(int productId, string attributes, int accountId = 0)
        {
            // Create instance of connection and Command Object
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlDataReader reader = null;
                var xmlOut = new System.Text.StringBuilder();

                try
                {
                    var command = new SqlCommand("ZNODE_GetSKUByAttributes_XML", connection);

                    // Mark the command as store procedure
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@ProductID", productId);
                    command.Parameters.AddWithValue("@Attributes", attributes);
                    command.Parameters.AddWithValue("@AccountId", accountId);
                    // Execute the command
                    connection.Open();

                    reader = command.ExecuteReader(CommandBehavior.CloseConnection);

                    while (reader.Read())
                    {
                        xmlOut.Append(reader[0].ToString());
                    }
                }
                finally
                {
                    if (reader != null)
                    {
                        // Close the reader
                        reader.Close();
                    }

                    if (connection != null)
                    {
                        // Close the connection
                        connection.Close();
                    }
                }

                return xmlOut.ToString();
            }
        }

        /// <summary>
        /// Gets a product AddOns 
        /// </summary>
        /// <param name="productId">Product Id to get the addon value.</param>
        /// <param name="selectedValues">Selected add on values.</param>
        /// <returns>Returns the addon values for the product.</returns>
        public string GetAddOnByValues(int productId, string selectedValues, int savedCartLineItemId)
        {
            // Create instance of connection and Command Object
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlDataReader reader = null;
                var xmlOut = new System.Text.StringBuilder();

                try
                {
                    var command = new SqlCommand("ZNODE_GetAddOnsByAddOnValues_XML", connection);

                    // Mark the command as store procedure
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@ProductID", productId);
                    command.Parameters.AddWithValue("@AddOnValues", selectedValues);
                    command.Parameters.AddWithValue("@ParentSavedCartLineItemID", savedCartLineItemId);

                    // Execute the command
                    connection.Open();

                    reader = command.ExecuteReader(CommandBehavior.CloseConnection);

                    xmlOut.Append("<ZNodeAddOnList>");

                    while (reader.Read())
                    {
                        xmlOut.Append(reader[0].ToString());
                    }

                    xmlOut.Append("</ZNodeAddOnList>");
                }
                finally
                {
                    if (reader != null)
                    {
                        // Close the reader
                        reader.Close();
                    }

                    if (connection != null)
                    {
                        // Close the connection
                        connection.Close();
                    }
                }

                return xmlOut.ToString();
            }
        }

        /// <summary>
        /// Get the sku object by sku.
        /// </summary>
        /// <param name="sku">Sku string to get the sku object.</param>
        /// <param name="accountId">Account string to get the sku object.</param>
        /// <returns>Returns the sku details XMl.</returns>
        public string GetBySKU(string sku, int accountId = 0)
        {
            // Create instance of connection and Command Object
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlDataReader reader = null;
                var xmlOut = new System.Text.StringBuilder();

                try
                {
                    var command = new SqlCommand("ZNODE_GetSKUBySKU_XML", connection);

                    // Mark the command as store procedure
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@sku", sku);
                    command.Parameters.AddWithValue("@accountId", accountId);
                    // Execute the command
                    connection.Open();

                    reader = command.ExecuteReader(CommandBehavior.CloseConnection);

                    while (reader.Read())
                    {
                        xmlOut.Append(reader[0].ToString());
                    }
                }
                finally
                {
                    if (reader != null)
                    {
                        // Close the reader
                        reader.Close();
                    }

                    if (connection != null)
                    {
                        // Close the connection
                        connection.Close();
                    }
                }

                // Return Value
                return xmlOut.ToString();
            }
        }

        /// <summary>
        /// Returns the default sku for a product with NO attributes
        /// </summary>
        /// <param name="productId">Product Id to get the default sku.</param>
        /// <param name="accountId">Account Id to get the default sku.</param>
        /// <returns>Returns the default sku details XML.</returns>
        public string GetDefaultSKU(int productId, int accountId = 0)
        {
            // Create instance of connection and Command Object
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlDataReader reader = null;
                var xmlOut = new System.Text.StringBuilder();

                try
                {
                    var command = new SqlCommand("ZNODE_GetDefaultSKUByProduct_XML", connection);

                    // Mark the command as store procedure
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@ProductID", productId);
                    command.Parameters.AddWithValue("@AccountID", accountId);
                    // Execute the command
                    connection.Open();

                    reader = command.ExecuteReader(CommandBehavior.CloseConnection);

                    while (reader.Read())
                    {
                        xmlOut.Append(reader[0].ToString());
                    }
                }
                finally
                {
                    if (reader != null)
                    {
                        // Close the reader
                        reader.Close();
                    }

                    if (connection != null)
                    {
                        // Close the connection
                        connection.Close();
                    }
                }

                return xmlOut.ToString();
            }
        }

        /// <summary>
        /// Get the manufactures details as XML
        /// </summary>
        /// <param name="manufacturerId">Manufacturer Id to get the details as XML.</param>
        /// <param name="portalId">Portal Id of the manufacturer.</param>
        /// <returns>Returns the manufacturer details XML.</returns>
        public string GetManufacturerXML(int manufacturerId, int portalId)
        {
            // Create instance of connection and Command Object
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlDataReader reader = null;
                var xmlOut = new System.Text.StringBuilder();

                try
                {
                    SqlCommand command = new SqlCommand("ZNODE_GetManufacturerXML", connection);

                    // Mark the command as store procedure
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@ManufacturerID", manufacturerId);

                    // Execute the command
                    connection.Open();

                    reader = command.ExecuteReader(CommandBehavior.CloseConnection);

                    while (reader.Read())
                    {
                        xmlOut.Append(reader[0].ToString());
                    }
                }
                finally
                {
                    if (reader != null)
                    {
                        // Close the reader
                        reader.Close();
                    }

                    if (connection != null)
                    {
                        // Close the connection
                        connection.Close();
                    }
                }

                return xmlOut.ToString();
            }
        }

        /// <summary>
        /// Get the Product type name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public int GetProductTypeName(string name)
        {
            var myConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString);
            SqlDataReader reader = null;

            int xmlOut = 0;

            try
            {
                var mycommand = new SqlCommand("ZNode_NT_ZNodeProductType_GetByName", myConnection);

                mycommand.CommandType = CommandType.StoredProcedure;

                var parameterName = new SqlParameter("@Name", SqlDbType.VarChar, 4000);
                parameterName.Value = name;
                mycommand.Parameters.Add(parameterName);

                myConnection.Open();

                reader = mycommand.ExecuteReader(CommandBehavior.CloseConnection);

                while (reader.Read())
                {
                    xmlOut = Convert.ToInt32(xmlOut + reader[0].ToString());
                }
            }
            finally
            {
                if (reader != null)
                {
                    // Close the reader
                    reader.Close();
                }

                if (myConnection != null)
                {
                    // Close the connection
                    myConnection.Close();
                }
            }

            return xmlOut;
        }

        /// <summary>
        /// Search the product by keyword.
        /// </summary>
        /// <param name="keyword">Keyword to search.</param>
        /// <param name="manufacturerId">Manufacturer Id to search.</param>
        /// <param name="productTypeId">Product type Id to search.</param>
        /// <param name="categoryId">Category Id to search.</param>        
        /// <returns>Retutns the filtered dataset.</returns>
        public DataSet SearchProductsByKeyword(string keyword, string manufacturerId, string productTypeId, string categoryId)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                var adapter = new SqlDataAdapter("ZNode_SearchProductsByKeyword", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@Keyword", keyword);
                adapter.SelectCommand.Parameters.AddWithValue("@ManufacturerId", manufacturerId);
                adapter.SelectCommand.Parameters.AddWithValue("@ProductTypeId", productTypeId);
                adapter.SelectCommand.Parameters.AddWithValue("@CategoryId", categoryId);

                // Create and fill the dataset
                var dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Search the product and returns the filtered Product DataSet
        /// </summary>
        /// <param name="name">Product name to search.</param>
        /// <param name="productNum">Product number to search.</param>
        /// <param name="sku">Sku to search.</param>
        /// <param name="manufacturerId">Manufacturer Id to search.</param>
        /// <param name="productTypeId">Product type Id to search.</param>
        /// <param name="categoryId">Category Id to search.</param>                
        /// <param name="catalogId">Catalog Id to search.</param>
        /// <param name="supplierId">Supplier Id (Vendor) to search.</param>
        /// <param name="portalId">Portal Id to search.</param>
        /// <returns>Returns product details dataset.</returns>
        public DataSet SearchProduct(string name, string productNum, string sku, string manufacturerId, string productTypeId, string categoryId, string catalogId, string supplierId, int portalId)
        {
            return SearchProduct(name, productNum, sku, manufacturerId, productTypeId, categoryId, catalogId, supplierId, portalId, null, null);
        }

        /// <summary>
        /// Search the product and returns the filtered Product DataSet
        /// </summary>
        /// <param name="name">Product name to search.</param>
        /// <param name="productNum">Product number to search.</param>
        /// <param name="sku">Sku to search.</param>
        /// <param name="manufacturerId">Manufacturer Id to search.</param>
        /// <param name="productTypeId">Product type Id to search.</param>
        /// <param name="categoryId">Category Id to search.</param>                
        /// <param name="catalogId">Catalog Id to search.</param>
        /// <param name="supplierId">Supplier Id (Vendor) to search.</param>
        /// <param name="portalId">Portal Id to search.</param>
        /// <param name="startRecord">Starting record for paging.</param>
        /// <param name="maxRecords">Number of records to retrieve for paging.</param>
        /// <returns>Returns product details dataset.</returns>
        public DataSet SearchProduct(string name, string productNum, string sku, string manufacturerId, string productTypeId, string categoryId, string catalogId, string supplierId, int portalId, int? startRecord, int? maxRecords)
        {
            // Create instance of connection  Object   
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                var adapter = new SqlDataAdapter("ZNode_SearchProduct", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;

                adapter.SelectCommand.Parameters.AddWithValue("@Name", name);
                adapter.SelectCommand.Parameters.AddWithValue("@Productnum", productNum);
                adapter.SelectCommand.Parameters.AddWithValue("@Sku", sku);
                adapter.SelectCommand.Parameters.AddWithValue("@ManufacturerId", manufacturerId);
                adapter.SelectCommand.Parameters.AddWithValue("@ProductTypeId", productTypeId);
                adapter.SelectCommand.Parameters.AddWithValue("@CategoryId", categoryId);
                adapter.SelectCommand.Parameters.AddWithValue("@CatalogId", catalogId);
                adapter.SelectCommand.Parameters.AddWithValue("@SupplierID", supplierId);
                adapter.SelectCommand.Parameters.AddWithValue("@PortalID", portalId);

                // Create and fill the dataset
                var dataSet = new DataSet("Products");
                connection.Open();

                // Check to do paging
                if (startRecord.HasValue && maxRecords.HasValue)
                {
                    adapter.Fill(dataSet, startRecord.Value, maxRecords.Value, "Products");
                }
                else
                {
                    adapter.Fill(dataSet, "Products");
                }

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        #region New function created for searching products in products in franchise admin
        /// <summary>
        /// Search the product and returns the filtered Product DataSet
        /// </summary>
        /// <param name="name">Product name to search.</param>
        /// <param name="productNum">Product number to search.</param>
        /// <param name="sku">Sku to search.</param>
        /// <param name="manufacturerId">Manufacturer Id to search.</param>
        /// <param name="productTypeId">Product type Id to search.</param>
        /// <param name="categoryId">Category Id to search.</param>                
        /// <param name="catalogId">Catalog Id to search.</param>
        /// <param name="supplierId">Supplier Id (Vendor) to search.</param>
        /// <param name="portalId">Portal Id to search.</param>
        /// <returns>Returns product details dataset.</returns>
        public DataSet FranchiseSearchProduct(string name, string productNum, string sku, string manufacturerId, string productTypeId, string categoryId, string catalogId, string supplierId, int portalId)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_SearchFranchiseProduct", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;

                adapter.SelectCommand.Parameters.AddWithValue("@Name", name);
                adapter.SelectCommand.Parameters.AddWithValue("@Productnum", productNum);
                adapter.SelectCommand.Parameters.AddWithValue("@Sku", sku);
                adapter.SelectCommand.Parameters.AddWithValue("@ManufacturerId", manufacturerId);
                adapter.SelectCommand.Parameters.AddWithValue("@ProductTypeId", productTypeId);
                adapter.SelectCommand.Parameters.AddWithValue("@CategoryId", categoryId);
                adapter.SelectCommand.Parameters.AddWithValue("@CatalogId", catalogId);
                adapter.SelectCommand.Parameters.AddWithValue("@SupplierID", supplierId);
                adapter.SelectCommand.Parameters.AddWithValue("@PortalID", portalId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }
        #endregion

        /// <summary>
        /// Search the product and return filtered product dataset
        /// </summary>
        /// <param name="name">Product Name to searh.</param>
        /// <param name="productNum">Product Number to searh.</param>
        /// <param name="sku">Sku to search.</param>
        /// <param name="manufacturerId">Manufacturer ID to searh.</param>
        /// <param name="productTypeId">Product Type ID to searh.</param>
        /// <param name="categoryId">Category ID to searh.</param>        
        /// <param name="catalogId">Catalog ID to searh.</param>        
        /// <returns>Returns product details dataset.</returns>
        public DataSet SearchProduct(string name, string productNum, string sku, string manufacturerId, string productTypeId, string categoryId, string catalogId)
        {
            return this.SearchProduct(name, productNum, sku, manufacturerId, productTypeId, categoryId, catalogId, string.Empty, 0, null, null);
        }

        /// <summary>
        /// Search the product and return filtered product dataset
        /// </summary>
        /// <param name="name">Product Name to searh.</param>
        /// <param name="productNum">Product Number to searh.</param>
        /// <param name="sku">Sku to search.</param>
        /// <param name="manufacturerId">Manufacturer ID to searh.</param>
        /// <param name="productTypeId">Product Type ID to searh.</param>
        /// <param name="categoryId">Category ID to searh.</param>        
        /// <param name="catalogId">Catalog ID to searh.</param>        
        /// <returns>Returns product details dataset.</returns>
        public DataSet SearchProduct(string name, string productNum, string sku, string manufacturerId, string productTypeId, string categoryId, string catalogId, int? startRecord, int? maxRecords)
        {
            return this.SearchProduct(name, productNum, sku, manufacturerId, productTypeId, categoryId, catalogId, string.Empty, 0, startRecord, maxRecords);
        }

        /// <summary>
        /// Search the product and return filtered product dataset
        /// </summary>
        /// <param name="name">Product Name to searh.</param>
        /// <param name="productNum">Product Number to searh.</param>
        /// <param name="sku">Sku to search.</param>
        /// <param name="manufacturerId">Manufacturer ID to searh.</param>
        /// <param name="productTypeId">Product Type ID to searh.</param>
        /// <param name="categoryId">Category ID to searh.</param>        
        /// <param name="catalogId">Catalog ID to searh.</param>
        /// <param name="supplierId">Supplier Id to search.</param>
        /// <returns>Returns product details dataset.</returns>        
        public DataSet SearchProduct(string name, string productNum, string sku, string manufacturerId, string productTypeId, string categoryId, string catalogId, string supplierId)
        {
            return this.SearchProduct(name, productNum, sku, manufacturerId, productTypeId, categoryId, catalogId, supplierId, 0);
        }

        /// <summary>
        /// Search the vendor product and return the filtered dataset.
        /// </summary>
        /// <param name="productName">Product name to search.</param>
        /// <param name="productId">Product Id to search.</param>
        /// <param name="vendorName">Vendor name to search.</param>
        /// <param name="vendorId">Vendor Id to search.</param>
        /// <param name="productStatus">Product status to search.</param>        
        /// <param name="twoCheckoutId">2Checkout product Id to search.</param>
        /// <returns>Returns the vendor product search result dataset.</returns>
        public DataSet SearchVendorProduct(string productName, string productId, string vendorName, string vendorId, string productStatus, string twoCheckoutId)
        {
            // Create instance of connection  Object   
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                var adapter = new SqlDataAdapter("ZNode_SearchVendorProduct", connection)
                    {
                        SelectCommand = { CommandType = CommandType.StoredProcedure }
                    };

                // Mark the command as store procedure
                adapter.SelectCommand.Parameters.AddWithValue("@ProductName", productName);
                adapter.SelectCommand.Parameters.AddWithValue("@ProductId", productId);
                adapter.SelectCommand.Parameters.AddWithValue("@vendor", vendorName);
                adapter.SelectCommand.Parameters.AddWithValue("@vendorId", vendorId);
                adapter.SelectCommand.Parameters.AddWithValue("@ProductStatus", productStatus);
                adapter.SelectCommand.Parameters.AddWithValue("@COId", twoCheckoutId);

                // Create and fill the dataset
                var dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Search the product by names.
        /// </summary>
        /// <param name="productName">Product name to search.</param>
        /// <param name="productNum">Product number to search.</param>
        /// <param name="sku">Product sku to search.</param>
        /// <param name="manufacturerName">Manufacturer name to search.</param>
        /// <param name="productTypeId">Product type Id to search.</param>
        /// <param name="categoryName">Category name to search.</param>
        /// <param name="portalIds">Comma seperated portal Ids to search.</param>
        /// <returns>Returns the product search result dataset.</returns>
        public DataSet SearchProductByNames(string productName, string productNum, string sku, string manufacturerName, string productTypeId, string categoryName, string portalIds)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                var adapter = new SqlDataAdapter("ZNode_SearchProductByNames", connection)
                    {
                        SelectCommand = { CommandType = CommandType.StoredProcedure }
                    };

                // Mark the command as store procedure
                adapter.SelectCommand.Parameters.AddWithValue("@Name", productName);
                adapter.SelectCommand.Parameters.AddWithValue("@ProductNum", productNum);
                adapter.SelectCommand.Parameters.AddWithValue("@sku", sku);
                adapter.SelectCommand.Parameters.AddWithValue("@ManufacturerName", manufacturerName);
                adapter.SelectCommand.Parameters.AddWithValue("@ProductTypeId", productTypeId);
                adapter.SelectCommand.Parameters.AddWithValue("@CategoryName", categoryName);
                adapter.SelectCommand.Parameters.AddWithValue("@PortalIds", portalIds);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Search the portal by prodcut.
        /// </summary>
        /// <returns>Returns the portal search result dataset.</returns>
        public DataSet GetPortalByProduct(string portalIds)
        {
            // Create instance of connection  Object   
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                var adapter = new SqlDataAdapter("ZNode_GetPortalsByProduct", connection)
                    {
                        SelectCommand = { CommandType = CommandType.StoredProcedure }
                    };

                // Mark the command as store procedure
                adapter.SelectCommand.Parameters.AddWithValue("@PortalIds", portalIds);

                // Create and fill the dataset
                var dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Get the attributeId for that productId. Also returns the AttributeId if the selectorName is passed.
        /// </summary>
        /// <param name="productId">Product Id to get the attribute.</param>
        /// <param name="attributeId">Attribute Id to get the attribute.</param>  
        /// <param name="productTypeId">Product type Id to get the product type.</param> 
        /// <param name="selectorName">Selector name.</param> 
        /// <param name="selectorXaxis">Selector x-axis.</param> 
        /// <param name="selectorYaxis">Selectot y-axis.</param> 
        /// <returns>Returns the the product attribute datatable.</returns>
        public DataTable GetByAttributeID(int productId, int attributeId, int productTypeId, string selectorName, string selectorXaxis, string selectorYaxis)
        {
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlDataAdapter adapter = new SqlDataAdapter("ZNODE_GetByAttributeID", connection);

                // Mark the Command as a Stored Procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@ProductId", productId);
                adapter.SelectCommand.Parameters.AddWithValue("@AttributeId", attributeId);
                adapter.SelectCommand.Parameters.AddWithValue("@ProductTypeId", productTypeId);
                adapter.SelectCommand.Parameters.AddWithValue("@SelectorName", selectorName);
                adapter.SelectCommand.Parameters.AddWithValue("@SelectorXaxis", selectorXaxis);
                adapter.SelectCommand.Parameters.AddWithValue("@SelectorYaxis", selectorYaxis);
                DataTable dataTable = new DataTable();

                // Get the data.
                connection.Open();
                adapter.Fill(dataTable);

                // closes the connection to the database.
                connection.Close();
                return dataTable;
            }
        }

        #endregion

        #region Public Methods - Related to Product AddOn and Values
        /// <summary>
        /// Search the AddOns and return dataset for the given name and product or sku
        /// </summary>
        /// <param name="name">Addon name to search.</param>
        ///	<param name="addOnTitle">Addon title to search.</param>
        /// <param name="productSku">Product sku to search.</param>
        /// <param name="localeId">Locale Id to search.</param>
        /// <param name="accountId">Account Id to search.</param>
        /// <param name="portalId">Portal Id to search</param>
        /// <returns>Returns the Addon search result dataset.</returns>
        public DataSet SearchAddOns(string name, string addOnTitle, string productSku, int localeId, int accountId, int portalId)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNODE_SEARCH_ADDONS", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@Name", name);
                adapter.SelectCommand.Parameters.AddWithValue("@Title", addOnTitle);
                adapter.SelectCommand.Parameters.AddWithValue("@SKUPRODUCT", productSku);

                if (localeId > 0)
                {
                    adapter.SelectCommand.Parameters.AddWithValue("@LocaleId", localeId);
                }

                if (portalId > 0)
                {
                    adapter.SelectCommand.Parameters.AddWithValue("@PortalID", portalId);
                }

                if (accountId > 0)
                {
                    adapter.SelectCommand.Parameters.AddWithValue("@AccountId", accountId);
                }

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }
        #endregion

        #region Public Methods - Related to Product Highlight

        /// <summary>
        /// Get a highlight for Given Inputs
        /// </summary>
        /// <param name="name">Highlight name to search.</param>
        /// <param name="highlightTypeId">Highlight type Id to serach.</param>        
        /// <returns>Returns the Highlight search result data set.</returns>
        public DataSet SearchProductHighlight(string name, int highlightTypeId)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_ProductSearchHighlight", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@Name", name);
                adapter.SelectCommand.Parameters.AddWithValue("@TypeId", highlightTypeId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Get all the Highlight for this portal
        /// </summary>
        /// <returns>Returns all higlights data set.</returns>
        public DataSet GetHighlightByPortal()
        {
            // Create instance of connection Object
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetHighlightType", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        #endregion

        #region Public Methods - Related to Product Alternate Images
        /// <summary>
        /// Get products alternate and swatch image. All parameters are optional.
        /// </summary>
        /// <param name="productId">Product Id to search.</param>
        /// <param name="productNum">Product Num to search.</param>
        /// <param name="productName">Product name to search.</param>
        /// <param name="vendorName">Vendor name to search.</param>
        /// <param name="vendorId">Vendor Id to search.</param>
        /// <param name="productStateId">Product review state Id to search.</param>
        /// <param name="twoCheckoutId">2Checkout Id to search.</param>
        /// <returns>Returns product alternate/swatch images dataset.</returns>
        public DataSet GetAlternateImage(int productId, string productNum, string productName, string vendorName, string vendorId, string productStateId, string twoCheckoutId)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetImageType", connection);

                // Mark the Command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;

                // Add procedure paramters
                adapter.SelectCommand.Parameters.AddWithValue("@ProductId", productId);
                adapter.SelectCommand.Parameters.AddWithValue("@ProductNum", productNum);
                adapter.SelectCommand.Parameters.AddWithValue("@ProductName", productName);
                adapter.SelectCommand.Parameters.AddWithValue("@Vendor", vendorName);
                adapter.SelectCommand.Parameters.AddWithValue("@VendorId", vendorId);
                adapter.SelectCommand.Parameters.AddWithValue("@ProductStatus", productStateId);
                adapter.SelectCommand.Parameters.AddWithValue("@COId", twoCheckoutId);

                // Create and fill the dataSet
                DataSet ds = new DataSet();
                connection.Open();
                adapter.Fill(ds);

                // release resources
                adapter.Dispose();
                connection.Close();

                // Return the result dataSet
                return ds;
            }
        }

        /// <summary>
        /// Get all the highlight images by product Id.
        /// </summary>
        /// <param name="productId">Product Id to get the product highlight images.</param>
        /// <returns>Returns the product highlight image dataset.</returns>
        public DataSet GetHighlightImages(int productId)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNODE_GetHighlightImageType", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@ProductId", productId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        #endregion

        #region Public Methods - Related to Best Seller List

        /// <summary>
        /// Get the most purchased product as XML.
        /// </summary>
        /// <param name="catalogId">Catalog Id to check.</param>
        /// <param name="displayItem">Number of items to retrieve.</param>
        /// <param name="categoryId">Category Id to check.</param>
        /// <param name="portalId">Portal Id to check.</param>
        /// <returns>Returns the bestseller items XML.</returns>
        public string GetBestSellerItems(int catalogId, int displayItem, int categoryId, int portalId)
        {
            // Create instance of connection and Command Object
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlCommand command = new SqlCommand("ZNode_SearchBestSellerList", connection);

                // Mark the command as store procedure
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@CatalogID", catalogId);
                command.Parameters.AddWithValue("@DisplayItem", displayItem);
                command.Parameters.AddWithValue("@CategoryId", categoryId);
                command.Parameters.AddWithValue("@PortalId", portalId);

                // Execute the command
                connection.Open();
                SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection);

                System.Text.StringBuilder xmlOut = new System.Text.StringBuilder();

                while (reader.Read())
                {
                    xmlOut.Append(reader[0].ToString());
                }

                connection.Close();

                return xmlOut.ToString();
            }
        }

        #endregion

        #region Public Methods - Related to wish list products

        /// <summary>
        /// Get the most purchased product as XML
        /// </summary>
        /// <param name="accountId">Account Id to get the wishlist product.</param>
        /// <param name="portalId">Portal Id to get the accounts wishlist.</param>
        /// <returns>Returns the wishlist products XML.</returns>
        public string GetWishListItemsByAccountID(int accountId, int portalId)
        {
            // Create instance of connection and Command Object
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlCommand command = new SqlCommand("ZNode_GetWishListByAccountID_XML", connection);

                // Mark the command as store procedure
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@AccountId", accountId);
                command.Parameters.AddWithValue("@PortalId", portalId);

                // Execute the command
                connection.Open();
                SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection);

                System.Text.StringBuilder xmlOut = new System.Text.StringBuilder();

                while (reader.Read())
                {
                    xmlOut.Append(reader[0].ToString());
                }

                // close the connection
                connection.Close();

                return xmlOut.ToString();
            }
        }

        #endregion

        #region Public Methods - Related to QB Item Inventory products
        /// <summary>
        /// Get the featured product as XML
        /// </summary>
        /// <param name="portalId">Portal Id to get the inventory items.</param>
        /// <returns>Returns the inventory items dataset.</returns>
        public DataSet GetItemInventoryList(int portalId)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNODE_GetWSItemInventoryList", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@PortalID", portalId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }
        #endregion

        #region Public Methods - Related to StoreLocator
        /// <summary>
        /// Get the zipcode count
        /// </summary>        
        /// <returns>Returns the number zipcodes.</returns>
        public int GetZipCodeCount()
        {
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlCommand command = new SqlCommand("ZNode_GetZipCodeCount", connection);

                // Local variables;
                int zipCodeCount = 0;

                // Mark the command as store procedure
                command.CommandType = CommandType.StoredProcedure;

                // Open Connection
                connection.Open();

                // Execute Query
                zipCodeCount = Convert.ToInt32(command.ExecuteScalar());

                // Close Connection
                connection.Close();

                return zipCodeCount;
            }
        }
        #endregion

        #region Related to Data Manager
        /// <summary>
        /// Search products based on the specified filter
        /// </summary>        
        /// <param name="productType">0 - All, 1 - Products, 2 - Attributes, 3 - AddOnValues</param>
        /// <returns>Returns the sku inventory dataset by product type.</returns>
        public DataSet GetSkuInventoryListByFilter(int productType)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNODE_GetSkuInventoryListByFilter", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@ProductType", productType);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Search products based on the specified filter and catalogId
        /// </summary>        
        /// <param name="productType">0 - All, 1 - Products, 2 - Attributes, 3 - AddOnValues</param>
        /// <param name="portalId">Portal Id to get the sku inventory items.</param>
        /// <returns>Returns the sku inventory dataset by product type.</returns>
        public DataSet GetSkuInventoryListByFilter(int productType, string portalId)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNODE_GetSkuInventoryListByFilterByPortalIds", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@ProductType", productType);
                adapter.SelectCommand.Parameters.AddWithValue("@PortalID", portalId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Search products based on the specified filter
        /// </summary>        
        /// <param name="productType">0 - All, 1 - Products, 2 - Attributes, 3 - AddOnValues</param>
        /// <returns>Returns the sku pricing dataset.</returns>
        public DataSet GetSkuPricingListByFilter(int productType)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNODE_GetSkuPricingListByFilter", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@ProductType", productType);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Search products based on the specified filter
        /// </summary>        
        /// <param name="productType">0 - All, 1 - Products, 2 - Attributes, 3 - AddOnValues</param>
        /// <param name="portalId">Portal Id to get the sku pricing list.</param>
        /// <returns>Returns the sku pricing dataset.</returns>
        public DataSet GetSkuPricingListByFilter(int productType, string portalId)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetSkuPricingListByFilterByPortalId", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@ProductType", productType);
                adapter.SelectCommand.Parameters.AddWithValue("@PortalID", portalId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Update the sku price.
        /// </summary>
        /// <param name="sku">SKU value to update.</param>
        /// <param name="productId">Product Id to update.</param>
        /// <param name="skuId">Sku Id value to update.</param>
        /// <param name="addOnValueId">Addon value to update.</param>
        /// <param name="retailPrice">Retail price value to update.</param>
        /// <param name="salePrice">Sale price value to update.</param>
        /// <param name="wholeSalePrice">Wholesale price value to update.</param>
        /// <returns>Returns the number of rows affected.</returns>
        public int UpdateSkuPrice(string sku, int productId, int skuId, int addOnValueId, decimal? retailPrice, decimal? salePrice, decimal? wholeSalePrice)
        {
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlCommand command = new SqlCommand("ZNODE_UpdateSkuPrice", connection);

                // Mark the Command as a Stored Procedures
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@Sku", sku);
                command.Parameters.AddWithValue("@ProductId", productId);
                command.Parameters.AddWithValue("@SkuId", skuId);
                command.Parameters.AddWithValue("@AddOnValueId", addOnValueId);
                command.Parameters.AddWithValue("@RetailPrice", retailPrice);
                command.Parameters.AddWithValue("@SalePrice", salePrice);
                command.Parameters.AddWithValue("@WholeSalePrice", wholeSalePrice);

                connection.Open();

                int rowsUpdated = (int)command.ExecuteScalar();

                command.Dispose();
                connection.Close();

                return rowsUpdated;
            }
        }

        /// <summary>
        /// Update the sku inventory.
        /// </summary>
        /// <param name="sku">SKU value to search.</param>
        /// <param name="productId">Product Id to search.</param>
        /// <param name="skuId">Sku Id to search.</param>
        /// <param name="addOnValueId">Addon value Id.</param>
        /// <param name="quantityOnHand">Quantity on hand value to udpate.</param>
        /// <param name="reorderLevel">Reorder level value to udpate.</param>
        /// <returns>Returns the number of rows affected.</returns>
        public int UpdateSkuInventory(string sku, int productId, int skuId, int addOnValueId, int quantityOnHand, int? reorderLevel)
        {
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlDataAdapter adapter = new SqlDataAdapter("ZNODE_UpdateSkuInventory", connection);

                // Mark the Command as a Stored Procedures
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@Sku", sku);
                adapter.SelectCommand.Parameters.AddWithValue("@ProductId", productId);
                adapter.SelectCommand.Parameters.AddWithValue("@SkuId", skuId);
                adapter.SelectCommand.Parameters.AddWithValue("@AddOnValueId", addOnValueId);
                adapter.SelectCommand.Parameters.AddWithValue("@QuantityOnHand", quantityOnHand);
                adapter.SelectCommand.Parameters.AddWithValue("@ReorderLevel", reorderLevel);

                connection.Open();

                int rowsUpdated = 0;

                rowsUpdated = (int)adapter.SelectCommand.ExecuteScalar();

                connection.Close();

                return rowsUpdated;
            }
        }

        /// <summary>
        /// Get product list by portal Id
        /// </summary>
        /// <param name="portalId">Portal Id to get the list of product dataset.</param>
        /// <returns>Returns the products dataset.</returns>
        public DataSet GetProductListByPortalId(int portalId)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetProductListByPortalId", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@PortalId", portalId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Get product list
        /// </summary>        
        /// <returns>Returns the products dataset.</returns>
        public DataSet GetProductList()
        {
            // Create instance of connection Object
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetProductList", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Update the Downloaded sku data
        /// </summary>
        /// <param name="actionCommand">Action command (I insert, U - Update)</param>
        /// <param name="skuId">Sku Id value to update.</param>
        /// <param name="productId">Product Id value to update.</param>
        /// <param name="sku">Sku value to update.</param>
        /// <param name="quantityOnHand">Quantity on hand value to update.</param>
        /// <param name="reorderLevel">Reorder level value to update.</param>
        /// <param name="skuPicturePath">Sku picture path value to update.</param>
        /// <param name="displayOrder">Display order value to update.</param>
        /// <param name="retailPrice">Retail price value to update.</param>
        /// <param name="salePrice">Sale price value to update.</param>
        /// <param name="wholeSalePrice">Whilesale price value to update.</param>
        /// <param name="isActive">Indicates whether the sku is active or not.</param>
        /// <returns>Returns true if udpated otherwise false.</returns>
        public bool UpdateDownloadedSku(string actionCommand, int skuId, int productId, string sku, int quantityOnHand, int? reorderLevel, string skuPicturePath, int? displayOrder, decimal? retailPrice, decimal? salePrice, decimal? wholeSalePrice, bool isActive)
        {
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlDataAdapter adapter = null;
                connection.Open();

                // Begin Transaction
                SqlTransaction transaction = connection.BeginTransaction();

                // Create Instance of Adapter Object
                adapter = new SqlDataAdapter("ZNODE_UpdateDownloadedSku", connection);

                // Mark the Command as a Stored Procedures
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;

                adapter.SelectCommand.Transaction = transaction;

                adapter.SelectCommand.Parameters.AddWithValue("@Action", actionCommand);
                adapter.SelectCommand.Parameters.AddWithValue("@Sku", sku);
                adapter.SelectCommand.Parameters.AddWithValue("@ProductId", productId);
                adapter.SelectCommand.Parameters.AddWithValue("@SkuId", skuId);
                adapter.SelectCommand.Parameters.AddWithValue("@QuantityOnHand", quantityOnHand);
                adapter.SelectCommand.Parameters.AddWithValue("@ReorderLevel", reorderLevel);
                adapter.SelectCommand.Parameters.AddWithValue("@SkuPicturePath", skuPicturePath);
                adapter.SelectCommand.Parameters.AddWithValue("@DisplayOrder", displayOrder);
                adapter.SelectCommand.Parameters.AddWithValue("@RetailPrice", retailPrice);
                adapter.SelectCommand.Parameters.AddWithValue("@SalePrice", salePrice);
                adapter.SelectCommand.Parameters.AddWithValue("@wholeSalePrice", wholeSalePrice);
                adapter.SelectCommand.Parameters.AddWithValue("@ActiveInd", isActive);
                try
                {
                    adapter.SelectCommand.ExecuteNonQuery();
                    transaction.Commit();
                    return true;
                }
                catch
                {
                    // Rollback the transaction, if it fails
                    transaction.Rollback();
                    return false;
                }
                finally
                {
                    // Close the connection
                    connection.Close();
                }
            }
        }

        #endregion

        #region Public Methods related to web Service(WCF)

        /// <summary>
        /// Search products based on the specified filter
        /// </summary>
        /// <param name="filter">0 - All , 1 - New , 2 - Modified</param>
        /// <param name="portalId">Retrieve info based on portalId</param>
        /// <returns>Returns the product list XML.</returns>
        public string GetProductListByFilter(int filter, int? portalId)
        {
            return this.GetProductListByFilter(filter, portalId, null);
        }

        /// <summary>
        /// Search products based on the specified filter
        /// </summary>
        /// <param name="filter">0 - All , 1 - New , 2 - Modified</param>
        /// <param name="portalId">Retrieve info based on portalId</param>
        /// <param name="localeId">Locale Id to search.</param>
        /// <returns>Returns the product list XML.</returns>
        public string GetProductListByFilter(int filter, int? portalId, int? localeId)
        {
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlDataAdapter adapter = new SqlDataAdapter("ZNODE_WS_GetProductsByFilter_XML", connection);

                // Mark the Command as a Stored Procedures
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@Filter", filter);
                adapter.SelectCommand.Parameters.AddWithValue("@PortalID", portalId);

                if (localeId != null)
                {
                    SqlParameter p3 = new SqlParameter("@LocaleId", SqlDbType.Int);
                    p3.Value = localeId;
                    adapter.SelectCommand.Parameters.Add(p3);
                }

                connection.Open();
                SqlDataReader reader = adapter.SelectCommand.ExecuteReader(CommandBehavior.CloseConnection);

                string xmlOut = string.Empty;

                while (reader.Read())
                {
                    xmlOut = xmlOut + reader[0].ToString();
                }

                reader.Close();
                connection.Close();

                return xmlOut;
            }
        }

        /// <summary>
        /// Search products based on the specified filter
        /// </summary>
        /// <param name="filter">0 - All , 1 - New , 2 - Modified</param>
        /// <param name="portalId">Retrieve info based on portalId</param>
        /// <returns>Retrusnt the sku quantity list XML.</returns>
        public string Get_WS_SkuQuantityListByFilter(int filter, int? portalId)
        {
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlDataAdapter adapter = new SqlDataAdapter("ZNODE_WS_GetSkuQuantityListByFilter_XML", connection);

                // Mark the Command as a Stored Procedures
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@Filter", filter);
                adapter.SelectCommand.Parameters.AddWithValue("@PortalID", portalId);

                connection.Open();
                SqlDataReader reader = adapter.SelectCommand.ExecuteReader(CommandBehavior.CloseConnection);
                string xmlOut = string.Empty;
                while (reader.Read())
                {
                    xmlOut = xmlOut + reader[0].ToString();
                }

                reader.Close();
                connection.Close();

                return xmlOut;
            }
        }

        /// <summary>
        /// Search products with it's retail price,sale price & wholesaleprice based on the specified filter
        /// </summary>        
        /// <param name="DownloadFlag">0 - All 1 - New 2 - Modified</param>
        /// <param name="portalId">Retrieve info based on portalId</param>
        /// <returns>Returns the Sku pricing list XML.</returns>
        public string Get_WS_SkuPricingListByFilter(int downloadFlag, int? portalId)
        {
            return this.Get_WS_SkuPricingListByFilter(downloadFlag, portalId, string.Empty);
        }

        /// <summary>
        /// Search products with it's retail price,sale price & wholesaleprice based on the specified filter
        /// </summary>        
        /// <param name="downloadFlag">0 - All 1 - New 2 - Modified</param>
        /// <param name="portalId">Retrieve info based on portalId</param>
        /// <param name="localeCode">Locale code to search.</param>
        /// <returns>Returns the Sku pricing list XML.</returns>
        public string Get_WS_SkuPricingListByFilter(int downloadFlag, int? portalId, string localeCode)
        {
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlDataAdapter adapter = new SqlDataAdapter("ZNODE_WS_GetSkuPricingListByFilter", connection);

                // Mark the Command as a Stored Procedures
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@Flag", downloadFlag);
                adapter.SelectCommand.Parameters.AddWithValue("@PortalID", portalId);
                adapter.SelectCommand.Parameters.AddWithValue("@LocaleCode", localeCode);
                connection.Open();

                SqlDataReader reader = adapter.SelectCommand.ExecuteReader(CommandBehavior.CloseConnection);

                string xmlOut = string.Empty;

                while (reader.Read())
                {
                    xmlOut = xmlOut + reader[0].ToString();
                }

                reader.Close();
                connection.Close();

                return xmlOut;
            }
        }

        /// <summary>
        /// Product with Reviews, Highlights, Cross Sell Items and Digital Assets as serializale XML string
        /// </summary>
        /// <param name="sku">The sku to search by.</param>
        /// <returns>returns a Product entity filled with data found from the sku.</returns>
        public string GetProductDetailBySku(string sku)
        {
            return this.GetProductDetailBySku(sku, null, null);
        }

        /// <summary>
        /// Product with Reviews, Highlights, Cross Sell Items and Digital Assets as serializale XML string
        /// </summary>
        /// <param name="sku">The sku to search by.</param>
        /// <param name="portalId">Portal Id to get the product details.</param>
        /// <param name="localeId">Locale id to get the product details.</param>
        /// <returns>returns a Product entity filled with data found from the sku.</returns>
        public string GetProductDetailBySku(string sku, int? portalId, int? localeId)
        {
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlDataAdapter adapter = new SqlDataAdapter("ZNODE_WS_GetProductDetailBySku_XML", connection);

                // Mark the Command as a Stored Procedures
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@Sku", sku);

                connection.Open();
                SqlDataReader reader = adapter.SelectCommand.ExecuteReader(CommandBehavior.CloseConnection);

                string xmlOut = string.Empty;

                while (reader.Read())
                {
                    xmlOut = xmlOut + reader[0].ToString();
                }

                reader.Close();
                connection.Close();

                return xmlOut;
            }
        }

        /// <summary>
        /// Get the Product object with collection of attributes.
        /// </summary>
        /// <param name="productId">The product ID for the product attributes you want to retreive.</param>
        /// <returns>A string object with data found from the productId.</returns>
        public string GetAttributesByProductId(int productId)
        {
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlDataAdapter adapter = new SqlDataAdapter("ZNODE_WS_GetProductAttributesByID_XML", connection);

                // Mark the Command as a Stored Procedures
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@ProductID", productId);

                connection.Open();
                SqlDataReader reader = adapter.SelectCommand.ExecuteReader(CommandBehavior.CloseConnection);

                string xmlOut = string.Empty;

                while (reader.Read())
                {
                    xmlOut = xmlOut + reader[0].ToString();
                }

                // Close the connection
                reader.Close();
                connection.Close();

                return xmlOut;
            }
        }

        /// <summary>
        /// Returns the Product object with collection of add-ons.
        /// </summary>
        /// <param name="productId">The product ID for the product add-Ons you want to retreive.</param>
        /// <returns>A string object with data found from the productId.</returns>
        public string GetAddOnsByProductId(int productId)
        {
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlDataAdapter adapter = null;
                SqlDataReader reader = null;
                StringBuilder xmlOut = new StringBuilder();

                try
                {
                    // Create Instance of Adapter Object
                    adapter = new SqlDataAdapter("ZNODE_WS_GetProductAddOnsByID_XML", connection);

                    // Mark the Command as a Stored Procedures
                    adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                    adapter.SelectCommand.Parameters.AddWithValue("@ProductID", productId);

                    connection.Open();

                    reader = adapter.SelectCommand.ExecuteReader(CommandBehavior.CloseConnection);

                    while (reader.Read())
                    {
                        xmlOut.Append(reader[0].ToString());
                    }
                }
                finally
                {
                    if (reader != null)
                    {
                        // Close the reader
                        reader.Close();
                    }

                    if (connection != null)
                    {
                        // Close the connection
                        connection.Close();
                    }
                }

                return xmlOut.ToString();
            }
        }

        /// <summary>
        /// Returns the Product object
        /// </summary>
        /// <param name="portalId">The portal ID for the XML.</param>
        /// <param name="feedType">Either Google or Bing</param>
        /// <returns>Dataset values with the product list.</returns>
        public DataSet GetProductListXMLSiteMap(string portalId, string feedType)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetProductListXMLSiteMap", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;

                adapter.SelectCommand.Parameters.AddWithValue("@PortalID", portalId);
                adapter.SelectCommand.Parameters.AddWithValue("@FeedType", feedType);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }
        #endregion

        #region Public Methods related to Locale

        /// <summary>
        /// Get Locale information by product id, if it has different language.
        /// </summary>
        /// <param name="productNum">Product number to search.</param>
        /// <param name="portalId">Portal Id to search.</param>
        /// <returns>Returns the locale Ids dataset.</returns>
        public DataSet GetLocaleIdsByProductId(string productNum, int portalId)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetLocaleListByProductId", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@ProductNum", productNum);
                adapter.SelectCommand.Parameters.AddWithValue("@PortalId", portalId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Get Locale information by AddOnValue id, if it has different language.
        /// </summary>
        /// <param name="addOnValueId">Addon value to search.</param>
        /// <param name="portalId">Portal Id to search.</param>
        /// <returns>Returns the locale Ids dataset.</returns>
        public DataSet GetLocaleIdsByAddOnValueId(int addOnValueId, int portalId)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetLocaleListByAddOnValueId", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@AddOnValueId", addOnValueId);
                adapter.SelectCommand.Parameters.AddWithValue("@PortalId", portalId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        #endregion

        #region Public Methods related to ProductReviewHistory
        /// <summary>
        /// Get all product details by vendor Id.
        /// </summary>
        /// <param name="vendorId">Vendor Id to get the product details.</param>
        /// <returns>Returns the vendor product details dataset.</returns>
        public DataSet GetProductNotificationStatus(int vendorId)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetProductReviewHistoryByNotificationDate", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@VendorId", vendorId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Get all vendors list.
        /// </summary>        
        /// <returns>Returns the vendor detail dataset.</returns>
        public DataSet GetAllProductVendor()
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetAllProductVendor", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Update the notification date in product review history.
        /// </summary>
        /// <param name="productReviewHistoryId">Product review history Id to update.</param>
        /// <returns>Returns true if udpated otherwise false.</returns>        
        public bool UpdateNotificationDate(int productReviewHistoryId)
        {
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlCommand command = null;
                bool isUdpated = false;

                try
                {
                    // Open Connection
                    connection.Open();

                    // Create instance of Command Object
                    command = new SqlCommand("ZNode_UpdateNotificationDate", connection);

                    // Mark the Command as a Stored Procedure
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.AddWithValue("@ProductReviewHistoryID", productReviewHistoryId);

                    // Execute Query
                    command.ExecuteNonQuery();

                    // Set Return values 
                    isUdpated = true;
                }
                catch (Exception ex)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("Empty Cataog - " + ex.Message);
                    isUdpated = false;
                }
                finally
                {
                    if (connection != null)
                    {
                        // Finally, close the connection
                        connection.Close();

                        // Release the resources
                        connection.Dispose();
                    }
                }

                // Return boolean value
                return isUdpated;
            }
        }
        #endregion

        public List<string> GetProductIDsByFilters(string query, string[,] array, string relatedParent = null)
        {
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                var productIds = new List<string>();
                var adapter = new SqlDataAdapter(query, connection);
                for (var i = 0; i < array.GetLength(0); i++)
                {
                    if (array[i, 0].Equals("@p" + i))
                    {
                        adapter.SelectCommand.Parameters.AddWithValue(array[i, 0], (DateTime?)Convert.ToDateTime(array[i, 1]));
                    }
                    else
                    {
                        if (array[i, 1] == null)
                        {
                            adapter.SelectCommand.Parameters.AddWithValue(array[i, 0], DBNull.Value);
                        }
                        else
                        {
                            adapter.SelectCommand.Parameters.AddWithValue(array[i, 0], array[i, 1]);
                        }
                    }
                }
                if (relatedParent != null)
                    adapter.SelectCommand.Parameters.AddWithValue("@RelatedParent", relatedParent);
                else
                {
                    adapter.SelectCommand.Parameters.AddWithValue("@RelatedParent", DBNull.Value);
                }

                connection.Open();
                var reader = adapter.SelectCommand.ExecuteReader(CommandBehavior.CloseConnection);
                while (reader.Read())
                {
                    productIds.Add(reader[0].ToString());
                }

                connection.Close();

                return productIds;
            }
        }

        public string GetProductIDByExternalID(string externalId)
        {
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                var adapter = new SqlDataAdapter("ZNode_GetProductByExternalID", connection) { SelectCommand = { CommandType = CommandType.StoredProcedure } };

                adapter.SelectCommand.Parameters.AddWithValue("@externalId", externalId);

                connection.Open();
                var reader = adapter.SelectCommand.ExecuteReader(CommandBehavior.CloseConnection);

                reader.Read();
                string productId = null;

                if (reader.HasRows)
                {
                    productId = reader[0].ToString();
                }

                connection.Close();

                return productId;
            }
        }

        public string GetSKUIDByExternalSKUID(string externalSkuId)
        {
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                var adapter = new SqlDataAdapter("ZNode_GetSKUIDByExternalSKUID", connection) { SelectCommand = { CommandType = CommandType.StoredProcedure } };

                adapter.SelectCommand.Parameters.AddWithValue("@externalSkuId", externalSkuId);

                connection.Open();
                var reader = adapter.SelectCommand.ExecuteReader(CommandBehavior.CloseConnection);

                reader.Read();
                string productId = null;

                if (reader.HasRows)
                {
                    productId = reader[0].ToString();
                }

                connection.Close();

                return productId;
            }
        }

        public string GetProductIDByExternalSKUID(string externalSkuId)
        {
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                var adapter = new SqlDataAdapter("ZNode_GetProductByExternalSKUID", connection) { SelectCommand = { CommandType = CommandType.StoredProcedure } };

                adapter.SelectCommand.Parameters.AddWithValue("@externalSkuId", externalSkuId);

                connection.Open();
                var reader = adapter.SelectCommand.ExecuteReader(CommandBehavior.CloseConnection);

                reader.Read();
                string productId = null;

                if (reader.HasRows)
                {
                    productId = reader[0].ToString();
                }

                connection.Close();

                return productId;
            }
        }

        public List<string> GetProductReviewStateIDsByFilters(string query, string[,] array, string relatedParent = null)
        {
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                var productReviewStateIds = new List<string>();
                var adapter = new SqlDataAdapter(query, connection);
                for (var i = 0; i < array.GetLength(0); i++)
                {
                    if (array[i, 0].Equals("@p" + i))
                    {
                        adapter.SelectCommand.Parameters.AddWithValue(array[i, 0], (DateTime?)Convert.ToDateTime(array[i, 1]));
                    }
                    else
                    {
                        if (array[i, 1] == null)
                        {
                            adapter.SelectCommand.Parameters.AddWithValue(array[i, 0], DBNull.Value);
                        }
                        else
                        {
                            adapter.SelectCommand.Parameters.AddWithValue(array[i, 0], array[i, 1]);
                        }
                    }
                }
                if (relatedParent != null)
                    adapter.SelectCommand.Parameters.AddWithValue("@RelatedParent", relatedParent);
                else
                {
                    adapter.SelectCommand.Parameters.AddWithValue("@RelatedParent", DBNull.Value);
                }

                connection.Open();
                var reader = adapter.SelectCommand.ExecuteReader(CommandBehavior.CloseConnection);
                while (reader.Read())
                {
                    productReviewStateIds.Add(reader[0].ToString());
                }

                connection.Close();

                return productReviewStateIds;
            }
        }

        public string GetProductAddOnIDByProductIDAddOnID(string productId, string addonId)
        {
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                var adapter = new SqlDataAdapter("ZNode_GetProductAddOnIDByProductIDAddOnID", connection) { SelectCommand = { CommandType = CommandType.StoredProcedure } };

                adapter.SelectCommand.Parameters.AddWithValue("@ProductID", productId);
                adapter.SelectCommand.Parameters.AddWithValue("@AddOnID", addonId);

                connection.Open();
                var reader = adapter.SelectCommand.ExecuteReader(CommandBehavior.CloseConnection);

                reader.Read();

                if (!reader.HasRows)
                {
                    return null;
                }

                var skuId = reader[0].ToString();

                connection.Close();

                return skuId;
            }
        }

        /// <summary>
        /// Get Account by portal Id 
        /// </summary>
        /// <param name="portalId">Portal Id to get the Franchise accountid.</param>
        /// <returns>Returns the Franchise account id</returns>
        public int GetFranchiseAccountIDByPortalID(int portalId)
        {
            //Create instance of connection object
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                //Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetFranchiseAccountIDByPortalID", connection);
                //Mark the command as stored procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@PortalId", portalId);
                connection.Open();
                int accountId = 0;
                accountId = (int)adapter.SelectCommand.ExecuteScalar();
                connection.Close();
                return accountId;
            }
        }

        /// <summary>
        /// Search the product and returns the filtered Product DataSet
        /// </summary>
        /// <param name="name">Product name to search.</param>
        /// <param name="externalId">ExternalId to search.</param>
        /// <param name="sku">sku to search.</param>
        /// <param name="manufacturerId">Manufacturer Id to search.</param>
        /// <param name="categoryId">Category Id to search.</param>
        /// <param name="portalId">Portal Id to search.</param>
        /// <param name="accountId">Account Id to search.</param>
        /// <returns>Returns product details dataset.</returns>
        public DataSet SearchCustomerPricingProduct(string name, string externalId, string sku, string manufacturerId, string categoryId, int portalId, int accountId)
        {
            // Create instance of connection  Object   
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {

                var adapter = new SqlDataAdapter("ZNode_SearchCustomerPricingProduct", connection)
                    {
                        SelectCommand = { CommandType = CommandType.StoredProcedure }
                    };

                adapter.SelectCommand.Parameters.AddWithValue("@Name", name);
                adapter.SelectCommand.Parameters.AddWithValue("@ExternalID", externalId);
                adapter.SelectCommand.Parameters.AddWithValue("@SKU", sku);
                adapter.SelectCommand.Parameters.AddWithValue("@ManufacturerId", manufacturerId);
                adapter.SelectCommand.Parameters.AddWithValue("@CategoryId", categoryId);
                adapter.SelectCommand.Parameters.AddWithValue("@PortalID", portalId);
                adapter.SelectCommand.Parameters.AddWithValue("@AccountID", accountId);

                // Create and fill the dataset
                var dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        ///<summary>
        /// Get OrderLine Item By Supplier id
        /// </summary>
        public int GetTotalOrderLineItemBySKU(int? supplierId, int portalId)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("Znode_GetOrderLineItemBySKU", connection);
                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@SupplierId", supplierId);
                adapter.SelectCommand.Parameters.AddWithValue("@PortalId", portalId);
                connection.Open();
                int totalOrderLineItemId = 0;
                totalOrderLineItemId = (int)adapter.SelectCommand.ExecuteScalar();
                connection.Close();
                return totalOrderLineItemId;
            }
        }

        /// <summary>
        /// Search the customers for a productid
        /// </summary>
        /// <param name="portalId"></param>
        /// <param name="externalAccountID"></param>
        /// <param name="name"></param>
        /// <param name="companyname"></param>
        public DataSet SearchCustomerBasedPricingByProductID(int portalId, string externalAccountId, string name, string companyname, int productId)
        {
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                var adapter = new SqlDataAdapter("ZNode_GetCustomerBasedPricingByProductID", connection);

                // Mark the Command as a Stored Procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@PortalId", portalId);
                adapter.SelectCommand.Parameters.AddWithValue("@ExternalAccountNumber", externalAccountId);
                adapter.SelectCommand.Parameters.AddWithValue("@Name", name);
                adapter.SelectCommand.Parameters.AddWithValue("@CompanyName", companyname);
                adapter.SelectCommand.Parameters.AddWithValue("@ProductId", productId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }

        }

        #region Znode Version 8.0
        /// <summary>
        /// Znode Version 8.0
        /// To get product details by productId
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <returns>returns product details dataset</returns>
        public DataSet GetProductDetailsByProductId(int productId)
        {
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                var adapter = new SqlDataAdapter("ZNode_GetProductInformation", connection);

                // Mark the Command as a Stored Procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@ProductID", productId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        public bool IsSeoUrlExist(int productId, string seoUrl)
        {
            bool isExist = false;

            // Create instance of connection and Command Object
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlCommand command = new SqlCommand("ZNode_GetProductByURL", connection);

                // Mark the command as store procedure
                command.CommandType = CommandType.StoredProcedure;

                // Add parameters to command object                
                command.Parameters.AddWithValue("@SEOURL", seoUrl);
                command.Parameters.AddWithValue("@ProductId", productId);

                connection.Open();

                object scalarValue = command.ExecuteScalar();

                isExist = Convert.ToBoolean(scalarValue);

                connection.Close();
            }

            return isExist;
        }

        public bool DeleteByProductId(int productId)
        {
            bool isDeleted = false;

            // Create instance of connection  Object   
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlCommand command = null;
                System.Data.SqlClient.SqlTransaction transaction = null;

                try
                {
                    // Open Connection
                    connection.Open();

                    // BeginTransaction() Requires Open Connection
                    transaction = connection.BeginTransaction();

                    // Create instance of Command Object
                    command = new SqlCommand("ZNODE_DeleteByProductID", connection);

                    // Mark the Command as a Stored Procedure
                    command.CommandType = CommandType.StoredProcedure;

                    // Assign Transaction to Command
                    command.Transaction = transaction;

                    // Add parameters
                    command.Parameters.AddWithValue("@ProductId", productId);

                    // Execute Query
                    command.ExecuteNonQuery();

                    // Set Return values 
                    isDeleted = true;

                    // If we reach here, all command succeeded, so commit the transaction
                    transaction.Commit();
                }
                catch (Exception)
                {
                    // Something went wrong, so rollback the transaction
                    transaction.Rollback();
                    isDeleted = false;
                }
                finally
                {
                    if (transaction != null)
                    {
                        transaction.Dispose();
                    }

                    if (connection != null)
                    {
                        // Finally, close the connection
                        connection.Close();

                        // Release the resources
                        connection.Dispose();
                    }
                }
            }

            // Return boolean value
            return isDeleted;
        }

        #endregion

        /// <summary>
        /// Znode Version 8.0
        /// Get the product facet details.
        /// </summary>
        /// <param name="portalId"></param>
        /// <param name="facetGroupId"></param>
        /// <returns>returns product facet details dataset</returns>
        public DataSet GetProductFacetsDetails(int productId, int facetGroupId)
        {
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                var adapter = new SqlDataAdapter("ZNode_GetProductFacet", connection);

                // Mark the Command as a Stored Procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@ProductId", productId);
                adapter.SelectCommand.Parameters.AddWithValue("@FacetGroupID", facetGroupId);
                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }

        }

        /// <summary>
        ///Znode Version 8.0
        /// Get the sku facet details.
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <param name="skuId">int skuId</param>
        /// <param name="facetGroupId">int facetGroupId</param>
        /// <returns>returns dataset of sku associated factes</returns>
        public DataSet GetSkuAssociatedFacets(int productId, int skuId, int facetGroupId)
        {
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                var adapter = new SqlDataAdapter("ZNode_GetProductFacetSku", connection);

                // Mark the Command as a Stored Procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@ProductId", productId);
                adapter.SelectCommand.Parameters.AddWithValue("@FacetGroupID", facetGroupId);
                adapter.SelectCommand.Parameters.AddWithValue("@SKUID", skuId);
                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }

        }

        /// <summary>
        /// To update image path 
        /// </summary>
        /// <param name="Id">int Id - entityId</param>
        /// <param name="imagePath">string imagePath</param>
        /// <param name="tableName">string tableName</param>
        public void UpdateImage(int Id, string imagePath, string tableName)
        {
            // Create instance of connection  Object   
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                var command = new SqlCommand("ZNode_UpdateImagePath", connection);

                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@TableName", tableName);
                command.Parameters.AddWithValue("@ID", Id);
                command.Parameters.AddWithValue("@ImagePath", imagePath);

                connection.Open();

                command.ExecuteNonQuery();

                connection.Close();
            }
        }

        /// <summary>
        /// Get the product list by sku and product id.
        /// </summary>
        /// <param name="sku">Sku</param>
        /// <param name="portalId">Portal id</param>
        /// <returns>Dataset of the product withe sku list.</returns>
        public DataSet GetProductListBySku(string sku, int portalId)
        {
            // Create instance of connection  Object   
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {

                var adapter = new SqlDataAdapter("ZNode_ProductForQuickOrder", connection)
                {
                    SelectCommand = { CommandType = CommandType.StoredProcedure }
                };

                adapter.SelectCommand.Parameters.AddWithValue("@SKU", sku);
                adapter.SelectCommand.Parameters.AddWithValue("@PortalID", portalId);

                // Create and fill the dataset
                var dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }
    }

}