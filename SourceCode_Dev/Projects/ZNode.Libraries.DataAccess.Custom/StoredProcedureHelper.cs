﻿using System;
using System.Data;

namespace ZNode.Libraries.DataAccess.Custom
{
    /// <summary>
    /// This is helper class for StoredProcedure
    /// </summary>
    public class StoredProcedureHelper
    {
        #region Data Details Sp Config   
     
        /// <summary>
        /// Executes the generic stored procedure using the parameters provided.
        /// </summary>
        /// <param name="whereClause">The where condition</param>
        /// <param name="havingClause">Having clause</param>
        /// <param name="spName">Stored procedure name</param>
        /// <param name="orderBy">Order By</param>
        /// <param name="pageIndex">Page Index</param>
        /// <param name="pageSize">Page Size</param>
        /// <param name="totalRowCount">Total results count returned.</param>
        /// <param name="strPrefix">Prefix</param>
        /// <param name="innerWhereClause">Inner where condition</param>
        /// <returns>Returns Dataset after executing the stored procedure.</returns>
        public DataSet SP_DataDetailsSpConfig(string whereClause, string havingClause, string spName, string orderBy, string pageIndex, string pageSize, out int totalRowCount, string strPrefix = null, string innerWhereClause = null)
        {
            ExecuteSpHelper executeSpHelper = new ExecuteSpHelper();
            totalRowCount = 0;
            if (Equals(strPrefix, null))
            {
                strPrefix = string.Empty;
            }
            try
            {
                if (!string.IsNullOrEmpty(spName))
                {
                    executeSpHelper.GetParameter("@SPName", spName, ParameterDirection.Input, SqlDbType.NVarChar);
                    executeSpHelper.GetParameter("@WhereClause", whereClause, ParameterDirection.Input, SqlDbType.NVarChar);
                    executeSpHelper.GetParameter("@HavingClause", havingClause, ParameterDirection.Input, SqlDbType.NVarChar);
                    executeSpHelper.GetParameter("@OrderBy", orderBy, ParameterDirection.Input, SqlDbType.NVarChar);
                    executeSpHelper.GetParameter("@PageIndex", pageIndex, ParameterDirection.Input, SqlDbType.Int);
                    executeSpHelper.GetParameter("@PageSize", pageSize, ParameterDirection.Input, SqlDbType.Int);
                    executeSpHelper.GetParameter("@TotalRowCount", null, ParameterDirection.Output, SqlDbType.Int);
                    executeSpHelper.GetParameter("@PreFix", strPrefix, ParameterDirection.Input, SqlDbType.NVarChar);
                    executeSpHelper.GetParameter("@InnerWhereClause", innerWhereClause, ParameterDirection.Input, SqlDbType.NVarChar);
                    
                    //PRFT Custom Code:Start
                    //var Result = executeSpHelper.GetSPResultInDataSet("ZNode_DataDetailsSpConfig");
                    var Result = executeSpHelper.GetSPResultInDataSet("ZNode_DataDetailsSpConfigExtn");
                    //PRFT Custom Code:End
                    if (!Equals(Result, null) && !Equals(Result.Tables, null) && !Equals(Result.Tables.Count, 0) && !Equals(Result.Tables[0], null))
                    {
                        totalRowCount = executeSpHelper.OutPutParamater(6); // pass out put paramater index value
                    }
                    else
                    {
                        totalRowCount = 0;
                    }
                    return Result;
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { executeSpHelper = null; }
        }

        /// <summary>
        /// To call ZNode_DataDetailsSpConfig by sp name & where condition
        /// </summary>
        /// <param name="whereClause">string whereClause</param>
        /// <param name="spName">string spName</param>
        /// <returns>returns out data as per sp </returns>
        public DataSet SP_DataDetailsSpConfig(string whereClause, string spName)
        {
            ExecuteSpHelper executeSpHelper = new ExecuteSpHelper();
            try
            {
                if (!string.IsNullOrEmpty(spName))
                {
                    executeSpHelper.GetParameter("@SPName", spName, ParameterDirection.Input, SqlDbType.NVarChar);
                    executeSpHelper.GetParameter("@WhereClause", whereClause, ParameterDirection.Input, SqlDbType.NVarChar);
                    executeSpHelper.GetParameter("@TotalRowCount", 0, ParameterDirection.Output, SqlDbType.Int);
                    var Result = executeSpHelper.GetSPResultInDataSet("ZNode_DataDetailsSpConfig");

                    return Result;
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { executeSpHelper = null; }

        }


      
        #endregion
    }
}
