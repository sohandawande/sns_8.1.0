using System.Data;
using System.Data.SqlClient;

namespace ZNode.Libraries.DataAccess.Custom
{
    /// <summary>
    /// Manages the customer Cases 
    /// </summary>
    public class CaseHelper
    {
        /// <summary>
        /// Searches cases for caseid,firstname,lastname,companyname,title and case status
        /// </summary>
        /// <param name="caseStatus">Case status to search.</param>
        /// <param name="caseId">Case Id to search.</param>
        /// <param name="firstName">First name to search.</param>
        /// <param name="lastName">Last name to search.</param>
        /// <param name="companyName">Company name to search.</param>
        /// <param name="title">Title to search.</param>
        /// <param name="portalId">Protal Id to search.</param>
        /// <param name="portalIds">Comma seperated portal Id to search.</param>
        /// <returns>Returns the case search result dataset.</returns>
        public DataSet SearchCase(int caseStatus, string caseId, string firstName, string lastName, string companyName, string title, int portalId, string portalIds)
        {
            // Create instance of connection     
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNODE_SEARCHCASE", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;

                adapter.SelectCommand.Parameters.AddWithValue("@CaseId", caseId);                
                adapter.SelectCommand.Parameters.AddWithValue("@FirstName", firstName);
                adapter.SelectCommand.Parameters.AddWithValue("@LastName", lastName);
                adapter.SelectCommand.Parameters.AddWithValue("@CompanyName", companyName);
                adapter.SelectCommand.Parameters.AddWithValue("@Title", title);
                adapter.SelectCommand.Parameters.AddWithValue("@CaseStatus", caseStatus);
                adapter.SelectCommand.Parameters.AddWithValue("@PortalId", portalId);
                adapter.SelectCommand.Parameters.AddWithValue("@PortalIds", portalIds);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Znode Version 8.0
        /// To GetCaseRequestById
        /// </summary>
        /// <param name="caseId">string caseId</param>
        /// <returns>DataSet</returns>
        public DataSet GetCaseRequestById(string caseId)
        {
            // Create instance of connection     
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetCaseById", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;

                adapter.SelectCommand.Parameters.AddWithValue("@CaseId", caseId);             

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }
    }
}
