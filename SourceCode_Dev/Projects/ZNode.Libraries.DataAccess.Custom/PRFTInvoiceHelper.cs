﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZNode.Libraries.DataAccess.Custom
{
    public class PRFTInvoiceHelper
    {
        public string GetShipviaDetails(string shipviaCode)
        {
            string shipviaDescription = string.Empty;

            // Create instance of connection and Command Object
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                try
                {
                    SqlCommand command = new SqlCommand("PRFT_GetShipviaCode", connection);

                    // Mark the command as store procedure
                    command.CommandType = CommandType.StoredProcedure;

                    // Add parameters to command object                
                    command.Parameters.AddWithValue("@ShipviaCode", shipviaCode);

                    connection.Open();

                    object scalarValue = command.ExecuteScalar();

                    shipviaDescription = Convert.ToString(scalarValue);

                    connection.Close();
                }
                catch(Exception ex)
                {                    
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("Class-PRFTInvoiceHelper:Method-GetShipviaDetails:SP-PRFT_GetShipviaCode - " + ex.Message);                    
                }
            }

            // Return shipvia Description
            return shipviaDescription;
        }

        public string GetPaymentTermsDetails(string termCode)
        {
            string paymentTermsDescription = string.Empty;

            using (SqlConnection connection=new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                try
                {
                    SqlCommand command = new SqlCommand("PRFT_GetPaymentTerms", connection);

                    // Mark the command as store procedure
                    command.CommandType = CommandType.StoredProcedure;

                    // Add parameters to command object  
                    command.Parameters.AddWithValue("@TermCode", termCode);

                    connection.Open();

                    object scalarValue = command.ExecuteScalar();
                    paymentTermsDescription = Convert.ToString(scalarValue);

                    connection.Close();
                }
                catch (Exception ex)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("Class-PRFTInvoiceHelper:Method-GetShipviaDetails:SP-PRFT_GetShipviaCode - " + ex.Message);
                }
            }

            return paymentTermsDescription;
        }
    }
}
