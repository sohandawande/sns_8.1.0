﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using ZNode.Libraries.ECommerce.Utilities;

namespace ZNode.Libraries.DataAccess.Custom
{
    public class ExecuteSpHelper
    {
        #region Variables
        List<SqlParameter> parameterList = new List<SqlParameter>();
        public string ReturnParameter = string.Empty;
        #endregion

        #region SetParameter
        /// <summary>
        /// Add given parameter and its info in parameter list
        /// </summary>
        /// <param name="parameterName"></param>
        /// <param name="parameterValue"></param>
        /// <param name="dbType"></param>
        /// <param name="Direction"></param>
        public void ClearParameters()
        {
            parameterList.Clear();
        }
        public void GetParameter(string ParameterName, object ParameterValue, ParameterDirection Direction, SqlDbType dbType)
        {
            SqlParameter parameter = new SqlParameter();
            parameter.ParameterName = ParameterName;
            parameter.Value = ParameterValue;
            parameter.SqlDbType = dbType;
            if (Direction != ParameterDirection.Output)
                parameter.Direction = ParameterDirection.Input;
            else
            {
                parameter.Direction = ParameterDirection.Output;
                ReturnParameter = ParameterName;
            }
            parameterList.Add(parameter);
        }

        #endregion       

        #region Get Out Put Paramater
        public int OutPutParamater(int indexOutParamater)
        {
            int ReturnParameterOutPut = (int)parameterList[indexOutParamater].Value;
            return ReturnParameterOutPut;
        }
        #endregion

        #region Convert List TO Datatable
        public DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }
        #endregion

        #region Get SP Result In dataset
        /// <summary>
        /// Execute Stored procedure.
        /// </summary>
        /// <param name="storedProcedureName"></param>
        /// <returns>Return dataset for given stored procedure</returns>
        public DataSet GetSPResultInDataSet(string storedProcedureName)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString;
            DataSet ds = new DataSet();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(storedProcedureName, connection))
                {
                    cmd.Parameters.AddRange(parameterList.ToArray());
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    try
                    {
                        adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                        adapter.Fill(ds);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ds;
        }

        public object GetSPResultInObject(string storedProcedureName)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString;
           
            object ReturnValue = null;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(storedProcedureName, connection))
                {
                    cmd.Parameters.AddRange(parameterList.ToArray());
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    try
                    {
                        adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                        cmd.Connection.Open();
                        ReturnValue = cmd.ExecuteScalar();
                        cmd.Connection.Close();

                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ReturnValue;
        }
        #endregion


        public DataTable SaveDataToDatabase(DataTable dataTableToSave, string mappingKey, string processFlag, string storedProcedureName)
        {

            DataTable datatable = new DataTable();
            try
            {

                string connectionString = ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString;
                if (dataTableToSave.Rows.Count > 0)
                {
                    using (SqlConnection con = new SqlConnection(connectionString))
                    {
                        SqlCommand cmd = new SqlCommand(storedProcedureName, con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ExcelZnodeProductData", dataTableToSave);
                        cmd.Parameters.AddWithValue("@ExcelSheetName", mappingKey);
                        SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                        adapter.Fill(datatable);
                    }
                }
            }
            catch (Exception ex)
            {
                ZNodeLogging.LogMessage("Exception in ExecuteSpHelper In DataAccess.Custom - SaveDataToDatabase() - ex = "+ex.ToString());
                if (ex is SqlException)
                {
                    throw ex;
                }
                throw;
            }
            return datatable;
        }

    }
}
