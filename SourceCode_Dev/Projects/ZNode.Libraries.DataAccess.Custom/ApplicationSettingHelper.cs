﻿using System;
using System.Data;

namespace ZNode.Libraries.DataAccess.Custom
{
    public class ApplicationSettingHelper
    {
       /// <summary>
       /// To get xml configuration list.
       /// </summary>
       /// <param name="type">Entity type</param>
       /// <param name="objectName">Entity Name</param>
       /// <returns></returns>
        public DataSet SP_ApplicationSettingColumnList(string type, string objectName)
        {
            ExecuteSpHelper executeSpHelper = new ExecuteSpHelper();
            try
            {
                if (!string.IsNullOrEmpty(type))
                {
                    executeSpHelper.GetParameter("@Type", type, ParameterDirection.Input, SqlDbType.NVarChar);
                    executeSpHelper.GetParameter("@Object_Name", objectName, ParameterDirection.Input, SqlDbType.NVarChar);
                    var Result = executeSpHelper.GetSPResultInDataSet("Znode_GetObjectColumnList");

                    return Result;
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { executeSpHelper = null; }
        }

        /// <summary>
        /// Insert/Update/Delete Xml
        /// </summary>
        /// <param name="settingTableName">Table Name</param>
        /// <param name="id">Config Id</param>
        /// <param name="groupName">Group Name</param>
        /// <param name="itemName">Item name</param>
        /// <param name="setting">Xml</param>
        /// <param name="viewOptions">View option</param>
        /// <param name="frontPageName">Front Page Name</param>
        /// <param name="frontObjectName">Front Object Name</param>
        /// <param name="isCompressed">Is Compressed</param>
        /// <param name="action">action</param>
        /// <returns></returns>
        public DataSet SP_SaveXml(string settingTableName, Int64 id, string groupName, string itemName, string setting, string viewOptions, string frontPageName, string frontObjectName, bool isCompressed, string action)
        {
            ExecuteSpHelper executeSpHelper = new ExecuteSpHelper();
            string error = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(setting) || id > 0)
                {
                    executeSpHelper.GetParameter("@Id", id, ParameterDirection.Input, SqlDbType.Int);
                    executeSpHelper.GetParameter("@GroupName", groupName, ParameterDirection.Input, SqlDbType.NVarChar);
                    executeSpHelper.GetParameter("@ItemName", itemName, ParameterDirection.Input, SqlDbType.NVarChar);
                    executeSpHelper.GetParameter("@Setting", setting, ParameterDirection.Input, SqlDbType.NVarChar);
                    executeSpHelper.GetParameter("@ViewOptions", viewOptions, ParameterDirection.Input, SqlDbType.NVarChar);
                    executeSpHelper.GetParameter("@FrontPageName", frontPageName, ParameterDirection.Input, SqlDbType.NVarChar);
                    executeSpHelper.GetParameter("@FrontObjectName", frontObjectName, ParameterDirection.Input, SqlDbType.NVarChar);
                    executeSpHelper.GetParameter("@IsCompressed", isCompressed, ParameterDirection.Input, SqlDbType.Bit);
                    executeSpHelper.GetParameter("@OrderByFields", null, ParameterDirection.Input, SqlDbType.NVarChar);

                    executeSpHelper.GetParameter("@Action", action, ParameterDirection.Input, SqlDbType.NVarChar);
                    executeSpHelper.GetParameter("@error", error, ParameterDirection.Output, SqlDbType.NVarChar);


                    var Result = executeSpHelper.GetSPResultInDataSet("ZNode_ApplicationSetting_CrudOperations");

                    return Result;
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { executeSpHelper = null; }
        }
    }
}


