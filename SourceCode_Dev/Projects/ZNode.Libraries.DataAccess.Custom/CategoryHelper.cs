using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace ZNode.Libraries.DataAccess.Custom
{
    /// <summary>
    /// Category related helper functions
    /// </summary>
    public class CategoryHelper
    {
        /// <summary>
        /// Get the categories excluded category items
        /// </summary>
        /// <param name="catalogId">Catalog Id to get the category dataset.</param>
        /// <param name="categoryId">Category Id to get the category dataset.</param>
        /// <returns>Rerurns the category dataset.</returns>
        public DataSet GetCategoryNodes(int catalogId, int categoryId)
        {
            // Create instance of connection     
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNODE_GetCategoryNodes", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;

                adapter.SelectCommand.Parameters.AddWithValue("@CatalogId", catalogId);
                adapter.SelectCommand.Parameters.AddWithValue("@CategoryId", categoryId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        ///  Get the List of Category Sub-Category & its Products
        /// </summary>
        /// <param name="catalogId">Catalog Id to get the category dataset.</param>
        /// <param name="categoryName">categoryName to get the category dataset.</param>
        /// <returns>Rerurns the category sub-category & its products dataset.</returns>
        public DataSet GetCategoryTree(int catalogId, string categoryName)
        {
            // Create instance of connection     
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetCategoryTree", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;

                adapter.SelectCommand.Parameters.AddWithValue("@CatalogId", catalogId);
                adapter.SelectCommand.Parameters.AddWithValue("@Name", categoryName);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }
        /// <summary>
        /// Get the categories by catalog Id.
        /// </summary>
        /// <param name="catalogId">Catalog Id to get the category.</param>
        /// <returns>Returns the categories dataset.</returns>
        public DataSet GetCategoryByCatalogID(int catalogId)
        {
            // Create instance of connection     
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetCategoryByCatlogID", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;

                adapter.SelectCommand.Parameters.AddWithValue("@CatalogId", catalogId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        public string GetCategoryIDByExternalID(string externalId)
        {
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                var adapter = new SqlDataAdapter("ZNode_GeCategoryByExternalID", connection) { SelectCommand = { CommandType = CommandType.StoredProcedure } };

                adapter.SelectCommand.Parameters.AddWithValue("@externalId", externalId);

                connection.Open();
                var reader = adapter.SelectCommand.ExecuteReader(CommandBehavior.CloseConnection);

                reader.Read();
                string categoryId = null;

                if (reader.HasRows)
                {
                    categoryId = reader[0].ToString();
                }

                connection.Close();

                return categoryId;
            }
        }

        /// <summary>
        /// Delete the category node by Id. First deletes the child nodes then parent.
        /// </summary>
        /// <param name="categoryNodeId">Category node Id to delete.</param>
        /// <returns>Returns true if deleted otherwise false.</returns>
        public bool DeleteCategoryNodeByID(int categoryNodeId)
        {
            bool isDeleted = false;

            // Create instance of connection     
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                System.Data.SqlClient.SqlTransaction transaction = null;

                try
                {
                    // Open Connection
                    connection.Open();

                    // BeginTransaction() Requires Open Connection
                    transaction = connection.BeginTransaction();

                    // Create instance of Command Object
                    SqlCommand command = new SqlCommand("ZNode_DeleteCategoryNode", connection);

                    // Mark the Command as a Stored Procedure
                    command.CommandType = CommandType.StoredProcedure;

                    // Assign Transaction to Command
                    command.Transaction = transaction;

                    // Add parameters
                    command.Parameters.AddWithValue("@CategoryNodeID", categoryNodeId);

                    // Execute Query
                    command.ExecuteNonQuery();

                    // Set Return values 
                    isDeleted = true;

                    // If we reach here, all command succeeded, so commit the transaction
                    transaction.Commit();
                }
                catch (Exception)
                {
                    // Something went wrong, so rollback the transaction
                    transaction.Rollback();
                    isDeleted = false;
                }
                finally
                {
                    if (transaction != null)
                    {
                        transaction.Dispose();
                    }

                    if (connection != null)
                    {
                        // Finally, close the connection
                        connection.Close();

                        // Release the resources
                        connection.Dispose();
                    }
                }
            }

            // Return boolean value
            return isDeleted;
        }

        /// <summary>
        /// Check whether the category node exists or not.
        /// </summary>
        /// <param name="catalogId">Catalog Id to check</param>
        /// <param name="categoryId">Category Id to check</param>
        /// <returns>Returns true if exists otherwise false.</returns>
        public bool IsCategoryNodeExists(int catalogId, int categoryId)
        {
            bool isExists = false;
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                try
                {
                    connection.Open();

                    SqlCommand command = new SqlCommand("ZNODE_CategoryNodeIsExist", connection);

                    // Mark the command as store procedure
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.AddWithValue("@CatalogId", catalogId);
                    command.Parameters.AddWithValue("@CategoryId", categoryId);

                    object rowsAffected = command.ExecuteScalar();

                    if (rowsAffected != null && rowsAffected.ToString() == "1")
                    {
                        isExists = true;
                    }
                }
                finally
                {
                    if (connection != null)
                    {
                        connection.Close();
                    }
                }
            }

            return isExists;
        }

        /// <summary>
        /// Check whether the category node exists or not.
        /// </summary>
        /// <param name="catalogId">Catalog Id to check</param>
        /// <param name="categoryId">Category Id to check</param>
        /// <param name="categoryNodeId">Category Node Id to check.</param>
        /// <returns>Returns true if exists otherwise false.</returns>
        public bool IsCategoryNodeExists(int catalogId, int categoryId, int categoryNodeId)
        {
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlCommand command = new SqlCommand("ZNODE_CategoryNodeIsExist", connection);

                // Mark the command as store procedure
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@CatalogId", catalogId);
                command.Parameters.AddWithValue("@CategoryId", categoryId);
                command.Parameters.AddWithValue("@CategoryNodeId", categoryNodeId);

                connection.Open();

                object rowsAffected = command.ExecuteScalar();
                if (rowsAffected != null && rowsAffected.ToString() == "1")
                {
                    // close connection
                    connection.Close();
                    return true;
                }

                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }

                return false;
            }
        }

        /// <summary>
        /// Get Category as XML
        /// </summary>
        /// <param name="categoryId">Category Id to get the category.</param>
        /// <param name="portalId">Portal Id to get the category.</param>
        /// <param name="localeId">Locale Id to get the category.</param>
        /// <returns>Returns the category details as XML.</returns>
        public string GetCategoryXML(int categoryId, int portalId, int localeId)
        {
            // Create instance of connection and Command Object
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlDataReader reader = null;
                System.Text.StringBuilder xmlOut = new System.Text.StringBuilder();

                try
                {
                    SqlCommand command = new SqlCommand("ZNODE_GetCategoryById_XML", connection);
                    SqlDataAdapter adapter = new SqlDataAdapter();

                    // Mark the command as store procedure
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.AddWithValue("@PortalID", portalId);
                    command.Parameters.AddWithValue("@LocaleId", localeId);
                    command.Parameters.AddWithValue("@categoryId", categoryId);

                    // Execute the command
                    connection.Open();

                    adapter.SelectCommand = command;

                    reader = command.ExecuteReader(CommandBehavior.CloseConnection);

                    while (reader.Read())
                    {
                        xmlOut.Append(reader[0].ToString());
                    }
                }
                finally
                {
                    if (reader != null)
                    {
                        // Close the reader
                        reader.Close();
                    }

                    if (connection != null)
                    {
                        // Close the connection
                        connection.Close();
                    }
                }

                return xmlOut.ToString();
            }
        }

        /// <summary>
        /// Get the navigation items for a portal
        /// </summary>
        /// <param name="portalId">Portal Id to get the shopping cart navigation item.</param>
        /// <param name="localeId">Locale Id to get the shopping cart navigation item.</param>
        /// <returns>Returns the shopping cart navigation item dataset.</returns>
        public DataSet GetNavigationItems(int portalId, int localeId)
        {
            return GetNavigationItems(portalId, localeId, 0);
        }

        /// <summary>
        /// Get the navigation items for a portal
        /// </summary>
        /// <param name="portalId">Portal Id to get the shopping cart navigation item.</param>
        /// <param name="localeId">Locale Id to get the shopping cart navigation item.</param>
        /// <returns>Returns the shopping cart navigation item dataset.</returns>
        public DataSet GetNavigationItems(int portalId, int localeId, int profileId)
        {
            // Create instance of connection     
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetShoppingCartNavigationItems", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@PortalId", portalId);
                adapter.SelectCommand.Parameters.AddWithValue("@LocaleId", localeId);
                adapter.SelectCommand.Parameters.AddWithValue("@ProfileId", profileId);
                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Get all the category nodes.
        /// </summary>        
        /// <returns>Returns the category node dataset.</returns>
        public DataSet GetAllCategoryNodes()
        {
            // Create instance of connection
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetAllCategoryNodes", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Get category nodes by catalog Id
        /// </summary>        
        /// <param name="catalogId">Catalog Id to get the category node dataset.</param>
        /// <returns>Returns the category node dataset.</returns>
        public DataSet GetCategoryNodesByCatalogId(int catalogId)
        {
            // Create instance of connection     
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetCategoryNodesByCatalogId", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;

                adapter.SelectCommand.Parameters.AddWithValue("@CatalogId", catalogId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Get root categories items.
        /// </summary>
        /// <param name="portalId">Portal Id to get the root category items.</param>
        /// <param name="catalogId">Catalog Id to get the root category items.</param>
        /// <returns>Returns the category items dataset.</returns>
        public DataSet GetRootCategoryItems(int portalId, int catalogId)
        {
            return GetRootCategoryItems(portalId, catalogId, 0);
        }

        /// <summary>
        /// Get root categories items.
        /// </summary>
        /// <param name="portalId">Portal Id to get the root category items.</param>
        /// <param name="catalogId">Catalog Id to get the root category items.</param>
        /// <returns>Returns the category items dataset.</returns>
        public DataSet GetRootCategoryItems(int portalId, int catalogId, int profileId)
        {
            // Create instance of connection     
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetCategoryRootItems", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@portalId", portalId);
                adapter.SelectCommand.Parameters.AddWithValue("@CatalogID", catalogId);
                adapter.SelectCommand.Parameters.AddWithValue("@ProfileId", profileId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// get category path by category Id.
        /// </summary>
        /// <param name="categoryId">Category Id to get the category path.</param>
        /// <param name="portalId">Portal Id to get the category path.</param>
        /// <returns>Returns the category path dataste.</returns>
        public string GetCategoryPathByCategoryId(int categoryId, int portalId)
        {
            // Create instance of connection and Command Object
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlDataReader reader = null;

                System.Text.StringBuilder strOut = new System.Text.StringBuilder();

                try
                {
                    SqlCommand command = new SqlCommand("ZNode_GetCategoryPathByCategoryID", connection);

                    // Mark the command as store procedure
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@CategoryId", categoryId);
                    command.Parameters.AddWithValue("@PortalId", portalId);

                    // Execute the command
                    connection.Open();

                    reader = command.ExecuteReader(CommandBehavior.CloseConnection);
                    while (reader.Read())
                    {
                        strOut.Append(reader[0].ToString());
                    }
                }
                finally
                {
                    if (reader != null)
                    {
                        // Close the reader
                        reader.Close();
                    }

                    if (connection != null)
                    {
                        // Close the connection
                        connection.Close();
                    }
                }

                return strOut.ToString();
            }
        }

        /// <summary>
        /// Get the category path by product Id
        /// </summary>
        /// <param name="productId">Product Id to get the category path.</param>
        /// <param name="portalId">Portal Id to get the category path.</param>
        /// <returns>Returns the category path.</returns>
        public string GetCategoryPathByProductId(int productId, int portalId)
        {
            // Create instance of connection and Command Object
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlDataReader reader = null;
                System.Text.StringBuilder strOut = new System.Text.StringBuilder();

                try
                {
                    SqlCommand command = new SqlCommand("ZNode_GetCategoryPathByProductID", connection);

                    // Mark the command as store procedure
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@ProductId", productId);
                    command.Parameters.AddWithValue("@PortalId", portalId);

                    // Execute the command
                    connection.Open();

                    reader = command.ExecuteReader(CommandBehavior.CloseConnection);

                    while (reader.Read())
                    {
                        strOut.Append(reader[0].ToString());
                    }
                }
                finally
                {
                    if (reader != null)
                    {
                        // Close the reader
                        reader.Close();
                    }

                    if (connection != null)
                    {
                        // Close the connection
                        connection.Close();
                    }
                }

                return strOut.ToString();
            }
        }
        /// <summary>
        /// Get the Parent category Id by Category Id
        /// </summary>
        /// <param name="categoryId">category Id to get the parent category </param>
        /// <returns>Returns the Parent CategoryId.</returns>
        public int GetParentCategoryByCategoryId(int categoryId)
        {
            int ParentCategoryId = 0;
            // Create instance of connection and Command Object
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlDataReader reader = null;
                System.Text.StringBuilder strOut = new System.Text.StringBuilder();


                SqlCommand command = new SqlCommand("ZNode_GetParentCategoryId", connection);

                // Mark the command as store procedure
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@CategoryId", categoryId);

                // Execute the command
                connection.Open();

                reader = command.ExecuteReader(CommandBehavior.CloseConnection);

                if (reader.Read())
                {
                    ParentCategoryId = Convert.ToInt32(reader[0].ToString());
                }

                reader.Close();
                // Close the connection
                connection.Close();

            }

            return ParentCategoryId;
        }


        /// <summary>
        /// Search the category.
        /// </summary>
        /// <param name="categoryName">Category name to search.</param>
        /// <param name="catalogId">Catalog Id to search.</param>
        /// <returns>Returns the category search result dataset.</returns>
        public DataSet GetCategoriesBySearchData(string categoryName, string catalogId)
        {
            return this.GetCategoriesBySearchData(categoryName, catalogId, false);
        }

        /// <summary>
        /// Returns Categories by Search data
        /// </summary>
        /// <param name="categoryName">Category name to search.</param>
        /// <param name="catalogId">Catalog Id to search.</param>
        /// <param name="isAssigned">Indicates whether the category is associated or not.</param>
        /// <returns>Returns the category search result dataset.</returns>
        public DataSet GetCategoriesBySearchData(string categoryName, string catalogId, bool isAssigned)
        {
            // Create instance of connection     
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_SearchCategories", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@Name", categoryName);
                adapter.SelectCommand.Parameters.AddWithValue("@CatalogID", catalogId);
                if (isAssigned)
                {
                    adapter.SelectCommand.Parameters.AddWithValue("@Unassigned", isAssigned);
                }

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Returns Manufacturers by Search data
        /// </summary>
        /// <param name="categoryName">Category name to search.</param>
        /// <param name="catalogId">Catalog Id to search.</param>
        /// <returns>Returns the categories search result.</returns>
        public DataSet GetCategoriesSearchByCatalog(string categoryName, int catalogId)
        {
            // Create instance of connection     
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_SearchCategoriesByCatalog", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;

                adapter.SelectCommand.Parameters.AddWithValue("@Name", categoryName);
                adapter.SelectCommand.Parameters.AddWithValue("@CatalogID", catalogId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        public List<string> GetCategoryIDsByFilters(string query, string[,] array, string relatedParent = null)
        {
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                var categoryIds = new List<string>();

                var adapter = new SqlDataAdapter(query, connection);

                for (var i = 0; i < array.GetLength(0); i++)
                {
                    if (array[i, 0].Equals("@p" + i))
                    {
                        adapter.SelectCommand.Parameters.AddWithValue(array[i, 0], (DateTime?)Convert.ToDateTime(array[i, 1]));
                    }
                    else
                    {
                        if (array[i, 1] == null)
                        {
                            adapter.SelectCommand.Parameters.AddWithValue(array[i, 0], DBNull.Value);
                        }
                        else
                        {
                            adapter.SelectCommand.Parameters.AddWithValue(array[i, 0], array[i, 1]);
                        }
                    }
                }

                if (relatedParent != null)
                    adapter.SelectCommand.Parameters.AddWithValue("@RelatedParent", relatedParent);
                else
                {
                    adapter.SelectCommand.Parameters.AddWithValue("@RelatedParent", DBNull.Value);
                }
                connection.Open();
                var reader = adapter.SelectCommand.ExecuteReader(CommandBehavior.CloseConnection);
                while (reader.Read())
                {
                    categoryIds.Add(reader[0].ToString());
                }

                connection.Close();

                return categoryIds;
            }
        }

        public DataSet GetCategoryHerarchyOfCategory(string categoryId, string seoUrl,int catalogId)
        {
            // Create instance of connection     
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetCategoryNodeHierarchy", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;

                if (!string.IsNullOrEmpty(categoryId)) { adapter.SelectCommand.Parameters.AddWithValue("@CategoryID", categoryId); }
                adapter.SelectCommand.Parameters.AddWithValue("@SEOURL", seoUrl);
                adapter.SelectCommand.Parameters.AddWithValue("@CatalogID", catalogId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        #region MultiStore Helper methods
        /// <summary>
        /// Get the category node details.
        /// </summary>
        /// <param name="categoryId">Category Id to get the category node details.</param>
        /// <returns>Returns the category node detail dataset.</returns>
        public DataSet GetCategoryNodeDetail(int categoryId)
        {
            // Create instance of connection     
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetNodeCategory", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@CategoryID", categoryId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Get the category node details.
        /// </summary>
        /// <param name="categoryId">Category Id to get the category node details.</param>
        /// <param name="catalogId">Catalog Id to get the category node details.</param>
        /// <returns>Returns the category details dataset.</returns>
        public DataSet GetCategoryDetail(int categoryId, int catalogId)
        {
            // Create instance of connection     
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetCategoryData", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@CategoryID", categoryId);
                adapter.SelectCommand.Parameters.AddWithValue("@CatalogID", catalogId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Get the categories.
        /// </summary>        
        /// <returns>Returns all the category dataset.</returns>
        public DataSet GetCategories()
        {
            // Create instance of connection
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetCategories", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        public bool DeleteCategoryByID(int categoryId)
        {
            bool isSuccess = false;
            // Create instance of connection     
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlCommand command = null;
                System.Data.SqlClient.SqlTransaction transaction = null;

                try
                {
                    // Open Connection
                    connection.Open();

                    // BeginTransaction() Requires Open Connection
                    transaction = connection.BeginTransaction();

                    // Create instance of Command Object
                    command = new SqlCommand("ZNode_DeleteZnodeCategory", connection);

                    // Mark the Command as a Stored Procedure
                    command.CommandType = CommandType.StoredProcedure;

                    // Assign Transaction to Command
                    command.Transaction = transaction;

                    // Add parameters
                    command.Parameters.AddWithValue("@CategoryId", categoryId);

                    // Execute Query
                    object rowsAffected = command.ExecuteScalar();

                    if (rowsAffected != null && rowsAffected.ToString() == "1")
                    {
                        // Set Return values 
                        isSuccess = true;
                    }

                    // If we reach here, all command succeeded, so commit the transaction
                    transaction.Commit();
                }
                catch (Exception)
                {
                    // Something went wrong, so rollback the transaction
                    transaction.Rollback();
                    isSuccess = false;
                }
                finally
                {
                    if (transaction != null)
                    {
                        transaction.Dispose();
                    }

                    if (connection != null)
                    {
                        // Finally, close the connection
                        connection.Close();

                        // Release the resources
                        connection.Dispose();
                    }
                }
            }
            return isSuccess;
        }

        #endregion

        #region Public Methods related to web Service(WCF)
        /// <summary>
        /// Get the caetegory list by portal Id.
        /// </summary>
        /// <param name="portalId">Portal Id to get the category list.</param>
        /// <returns>Returns the category list dataset.</returns>
        public string GetCategoryListByPortalID(int? portalId)
        {
            // Create instance of connection
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlDataAdapter adapter = null;
                SqlDataReader reader = null;
                StringBuilder xmlOut = new StringBuilder();

                try
                {
                    // Create Instance of Adapter Object
                    adapter = new SqlDataAdapter("ZNODE_WS_GetCategoriesByPortalID_XML", connection);

                    // Mark the Command as a Stored Procedures
                    adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                    adapter.SelectCommand.Parameters.AddWithValue("@PortalID", portalId);

                    connection.Open();

                    reader = adapter.SelectCommand.ExecuteReader(CommandBehavior.CloseConnection);

                    while (reader.Read())
                    {
                        xmlOut.Append(reader[0].ToString());
                    }
                }
                finally
                {
                    if (reader != null)
                    {
                        // Close the reader
                        reader.Close();
                    }

                    if (connection != null)
                    {
                        // Close the connection
                        connection.Close();
                    }
                }

                return xmlOut.ToString();
            }
        }

        /// <summary>
        /// Gets the category list for the site map
        /// </summary>
        /// <param name="portalId">Portal id for the xml</param>
        /// <returns>Returns the dataset which contains the categories list</returns>
        public DataSet GetCategoryListXMLSiteMap(string portalId)
        {
            // Create instance of connection     
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetCategoryListXMLSiteMap", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;

                adapter.SelectCommand.Parameters.AddWithValue("@PortalId", portalId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        #endregion
        #region Category Profile

        /// <summary>
        /// Get all the sku profile by skuid. 
        /// </summary>
        /// <returns>Returns the sku dataset.</returns>
        public DataSet GetCategoryProfileByCategoryID(int categoryId)
        {
            // Create instance of connection     
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetCategoryProfileByCategoryID", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;

                adapter.SelectCommand.Parameters.AddWithValue("@CategoryId", categoryId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        #endregion

        #region Category Associated Products
        /// <summary>
        /// Znode Version 8.0
        /// To Associate Product Category
        /// </summary>
        /// <param name="productId">int categoryId</param>
        /// <param name="categoryIds"> string productIds</param>
        /// <returns>returns true/false</returns>
        public bool AddCategoryAssociateProducts(int categoryId, string productIds)
        {
            bool isSuccess = false;

            // Create instance of connection  Object   
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlCommand command = null;
                System.Data.SqlClient.SqlTransaction transaction = null;

                try
                {
                    // Open Connection
                    connection.Open();

                    // BeginTransaction() Requires Open Connection
                    transaction = connection.BeginTransaction();

                    // Create instance of Command Object
                    command = new SqlCommand("ZNode_InsertCategoryToMultipleProduct", connection);

                    // Mark the Command as a Stored Procedure
                    command.CommandType = CommandType.StoredProcedure;

                    // Assign Transaction to Command
                    command.Transaction = transaction;

                    // Add parameters
                    command.Parameters.AddWithValue("@CategoryId", categoryId);
                    command.Parameters.AddWithValue("@ProductId", productIds);

                    // Execute Query
                    command.ExecuteNonQuery();

                    // Set Return values 
                    isSuccess = true;

                    // If we reach here, all command succeeded, so commit the transaction
                    transaction.Commit();
                }
                catch (Exception)
                {
                    // Something went wrong, so rollback the transaction
                    transaction.Rollback();
                    isSuccess = false;
                }
                finally
                {
                    if (transaction != null)
                    {
                        transaction.Dispose();
                    }

                    if (connection != null)
                    {
                        // Finally, close the connection
                        connection.Close();

                        // Release the resources
                        connection.Dispose();
                    }
                }
            }

            // Return boolean value
            return isSuccess;
        }
        #endregion
    }
}
