using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace ZNode.Libraries.DataAccess.Custom
{
    /// <summary>
    /// Order related Helper Functions
    /// </summary>
    public class OrderHelper
    {
        /// <summary>
        /// Search the order for first name,last name, company name, accountId, orderId or orderstate Id
        /// </summary>
        /// <param name="orderId">Order Id to search.</param>
        /// <param name="billingFirstName">Billing first name to search.</param>
        /// <param name="billingLastName">Billing last name to search.</param>
        /// <param name="billingCompanyName">Billing company name to search.</param>
        /// <param name="accountId">Account Id to search.</param>
        /// <param name="startDate">Order created from this date.</param>
        /// <param name="endDate">Order created to this date.</param>
        /// <param name="orderStateId">Order state Id to search.</param>
        /// <param name="portalId">Portal Id to search.</param>
        /// <param name="portalIds">Comma seperated portal Ids to search.</param>
        /// <returns>Returns the order search result dataset.</returns>
        public DataSet SearchOrder(int? orderId, string billingFirstName, string billingLastName, string billingCompanyName, string accountId, DateTime? startDate, DateTime? endDate, int? orderStateId, int? portalId, string portalIds)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNODE_SEARCHORDER", connection);
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@OrderId", orderId);
                adapter.SelectCommand.Parameters.AddWithValue("@BillingFirstName", billingFirstName);
                adapter.SelectCommand.Parameters.AddWithValue("@BillingLastName", billingLastName);
                adapter.SelectCommand.Parameters.AddWithValue("@BillingCompanyName", billingCompanyName);
                adapter.SelectCommand.Parameters.AddWithValue("@AccountId", accountId);
                adapter.SelectCommand.Parameters.AddWithValue("@StartDate", startDate);
                adapter.SelectCommand.Parameters.AddWithValue("@EndDate", endDate);
                adapter.SelectCommand.Parameters.AddWithValue("@OrderStateId", orderStateId);
                adapter.SelectCommand.Parameters.AddWithValue("@PortalId", portalId);
                adapter.SelectCommand.Parameters.AddWithValue("@PortalIds", portalIds);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Search the vendor order for first name,last name, company name, accountId, orderId or orderstate Id
        /// </summary>
        /// <param name="orderId">Order Id to search.</param>
        /// <param name="billingFirstName">Billing first name to search.</param>
        /// <param name="billingLastName">Billing last name to search.</param>
        /// <param name="billingCompanyName">Billing company name to search.</param>
        /// <param name="accountId">Account Id to search.</param>
        /// <param name="startDate">Order created from this date.</param>
        /// <param name="endDate">Order created to this date.</param>
        /// <param name="orderStateId">Order state Id to search.</param>
        /// <param name="portalId">Portal Id to search.</param>
        /// <param name="portalIds">Comma seperated portal Ids to search.</param>
        /// <returns>Returns the order search result dataset.</returns>
        public DataSet SearchVendorOrder(int? orderId, string billingFirstName, string billingLastName, string billingCompanyName, string accountId, DateTime? startDate, DateTime? endDate, int? orderStateId, int? portalId, string portalIds, string loggedAccountId)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_SearchVendorOrder", connection);
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@OrderId", orderId);
                adapter.SelectCommand.Parameters.AddWithValue("@BillingFirstName", billingFirstName);
                adapter.SelectCommand.Parameters.AddWithValue("@BillingLastName", billingLastName);
                adapter.SelectCommand.Parameters.AddWithValue("@BillingCompanyName", billingCompanyName);
                adapter.SelectCommand.Parameters.AddWithValue("@AccountId", accountId);
                adapter.SelectCommand.Parameters.AddWithValue("@StartDate", startDate);
                adapter.SelectCommand.Parameters.AddWithValue("@EndDate", endDate);
                adapter.SelectCommand.Parameters.AddWithValue("@OrderStateId", orderStateId);
                adapter.SelectCommand.Parameters.AddWithValue("@PortalId", portalId);
                adapter.SelectCommand.Parameters.AddWithValue("@PortalIds", portalIds);
                adapter.SelectCommand.Parameters.AddWithValue("@LoggedAccountId", loggedAccountId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Get the order Line items for orders
        /// </summary>
        /// <param name="orderId">Order Id to search.</param>
        /// <param name="billingFirstName">Billing first name to search.</param>
        /// <param name="billingLastName">Billing last name to search.</param>
        /// <param name="billingCompanyName">Billing company name to search.</param>
        /// <param name="accountId">Account Id to search.</param>
        /// <param name="startDate">Order created from this date.</param>
        /// <param name="endDate">Order created to this date.</param>
        /// <param name="orderStateId">Order state Id to search.</param>
        /// <param name="portalId">Portal Id to search.</param>
        /// <returns>Returns the order line item dataset.</returns>
        public DataSet GetOrderLineItems(string orderId, string billingFirstName, string billingLastName, string billingCompanyName, string accountId, string startDate, string endDate, int orderStateId, int portalId)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNODE_GetOrderLineItems", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@OrderId", orderId);
                adapter.SelectCommand.Parameters.AddWithValue("@BillingFirstName", billingFirstName);
                adapter.SelectCommand.Parameters.AddWithValue("@BillingLastName", billingLastName);
                adapter.SelectCommand.Parameters.AddWithValue("@BillingCompanyName", billingCompanyName);
                adapter.SelectCommand.Parameters.AddWithValue("@AccountId", accountId);
                adapter.SelectCommand.Parameters.AddWithValue("@StartDate", startDate);
                adapter.SelectCommand.Parameters.AddWithValue("@EndDate", endDate);
                adapter.SelectCommand.Parameters.AddWithValue("@OrderStateId", orderStateId);
                adapter.SelectCommand.Parameters.AddWithValue("@PortalId", portalId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Get order for a account id and portal Id.
        /// </summary>
        /// <param name="accountId">Account Id to get the orders</param>
        /// <param name="portalId">Portal Id to get the order for account.</param>
        /// <returns>Returns the order dataset.</returns>
        public DataSet GetByAccountID(int accountId, int portalId)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNODE_GETORDERBYACCOUNTID", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@AccountId", accountId);
                adapter.SelectCommand.Parameters.AddWithValue("@PortalId", portalId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Get orders based on orderId.
        /// </summary> 
        /// <param name="orderId">Order Id to get the order details.</param>
        /// <returns>Returns the order detail dataset.</returns> 
        public DataSet GetOrdersByOrderId(string orderId)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetOrderByOrderId", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@OrderId", orderId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Get Vendor orders based on orderId.
        /// </summary> 
        /// <param name="orderId">Order Id to get the order details.</param>
        /// <param name="loggedAccountId">Logged Account Id to get the order details.</param>
        /// <returns>Returns the order detail dataset.</returns> 
        public DataSet GetVendorOrdersByOrderId(string orderId, string loggedAccountId)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetVendorOrderByOrderId", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@OrderId", orderId);
                adapter.SelectCommand.Parameters.AddWithValue("@LoggedAccountId", loggedAccountId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Get the order line items by order Id.
        /// </summary>
        /// <param name="orderId">Order Id to get the order line items.</param>      
        /// <returns>Returns the order line item dataset.</returns>
        public DataSet GetOrderLineItemsByOrderId(string orderId)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetOrderLineItemByOrderId", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@OrderId", orderId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Get the Vendor order line items by order Id.
        /// </summary>
        /// <param name="orderId">Order Id to get the order line items.</param>      
        /// <param name="loggedAccountId">Logged Account Id to get the order details.</param>
        /// <returns>Returns the order line item dataset.</returns>
        public DataSet GetVendorOrderLineItemsByOrderId(string orderId, string loggedAccountId)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetVendorOrderLineItemByOrderId", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@OrderId", orderId);
                adapter.SelectCommand.Parameters.AddWithValue("@LoggedAccountId", loggedAccountId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Get the Vendor order line items by order Id.
        /// </summary>
        /// <param name="orderId">Order Id to get the order line items.</param>      
        /// <param name="loggedAccountId">Logged Account Id to get the order details.</param>
        /// <returns>Returns the order line item dataset.</returns>
        public DataSet GetVendorOrderLineItemByOrderID(string orderId, string loggedAccountId)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetVendorOrderLineItemsByOrderId", connection);
                                                                   
                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@OrderId", orderId);
                adapter.SelectCommand.Parameters.AddWithValue("@LoggedAccountId", loggedAccountId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Get order line items by order Id and order state Id.
        /// </summary>
        /// <param name="orderId">Order Id to get the order details.</param>      
        /// <param name="orderStatusId">Order state Id to get the order.</param>
        /// <returns>Returns the order line items dataset.</returns>
        public DataSet GetOrderLineItemsByOrderId(string orderId, int orderStatusId)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetOrderLineItemByOrderId", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@OrderId", orderId);
                adapter.SelectCommand.Parameters.AddWithValue("@OrderStatusId", orderStatusId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Get the next highest OrderId, which not downloaded yet for a Portal
        /// </summary>
        /// <param name="portalId">Portal Id to get the highest order Id.</param>
        /// <returns>Returns the next highest order Id which not downloaded yet for a Portal.</returns>
        public int GetHighestOrderId(string portalId)
        {
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                connection.Open();

                SqlCommand command = new SqlCommand("ZNode_GetHighestOrderId", connection);

                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@PortalID", portalId);

                int orderId = (int)command.ExecuteScalar();

                connection.Close();

                return orderId;
            }
        }

        public string GetOrderIDByExternalID(string externalId)
        {
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                var adapter = new SqlDataAdapter("ZNode_GetOrderByExternalID", connection) { SelectCommand = { CommandType = CommandType.StoredProcedure } };

                adapter.SelectCommand.Parameters.AddWithValue("@externalId", externalId);

                connection.Open();
                var reader = adapter.SelectCommand.ExecuteReader(CommandBehavior.CloseConnection);

                reader.Read();
                string orderId = null;

                if (reader.HasRows)
                {
                    orderId = reader[0].ToString();
                }

                connection.Close();

                return orderId;
            }
        }

        public List<string> GetSavedCartIDsByFilters(string query, string[,] array)
        {
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                var savedCartIds = new List<string>();

                var adapter = new SqlDataAdapter(query, connection);

                for (var i = 0; i < array.GetLength(0); i++)
                {
                    if (array[i, 0].Equals("@p" + i))
                    {
                        adapter.SelectCommand.Parameters.AddWithValue(array[i, 0], (DateTime?)Convert.ToDateTime(array[i, 1]));
                    }
                    else
                    {
                        if (array[i, 1] == null)
                        {
                            adapter.SelectCommand.Parameters.AddWithValue(array[i, 0], DBNull.Value);
                        }
                        else
                        {
                            adapter.SelectCommand.Parameters.AddWithValue(array[i, 0], array[i, 1]);
                        }
                    }
                }

                connection.Open();
                var reader = adapter.SelectCommand.ExecuteReader(CommandBehavior.CloseConnection);
                while (reader.Read())
                {
                    savedCartIds.Add(reader[0].ToString());
                }

                connection.Close();

                return savedCartIds;
            }
        }

        public string GetOrderLineItemIDByExternalID(string orderId, string externalId)
        {
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                var adapter = new SqlDataAdapter("ZNode_GetOrderLineItemByExternalID", connection) { SelectCommand = { CommandType = CommandType.StoredProcedure } };

                adapter.SelectCommand.Parameters.AddWithValue("@orderId", orderId);
                adapter.SelectCommand.Parameters.AddWithValue("@externalId", externalId);

                connection.Open();
                var reader = adapter.SelectCommand.ExecuteReader(CommandBehavior.CloseConnection);

                reader.Read();
                string orderLineItemId = null;

                if (reader.HasRows)
                {
                    orderLineItemId = reader[0].ToString();
                }

                connection.Close();

                return orderLineItemId;
            }
        }

        public List<string> GetOrderIDsByFilters(string query, string[,] array)
        {
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                var orderIds = new List<string>();

                var adapter = new SqlDataAdapter(query, connection);

                for (var i = 0; i < array.GetLength(0); i++)
                {
                    if (array[i, 0].Equals("@p" + i))
                    {
                        adapter.SelectCommand.Parameters.AddWithValue(array[i, 0], (DateTime?)Convert.ToDateTime(array[i, 1]));
                    }
                    else
                    {
                        if (array[i, 1] == null)
                        {
                            adapter.SelectCommand.Parameters.AddWithValue(array[i, 0], DBNull.Value);
                        }
                        else
                        {
                            adapter.SelectCommand.Parameters.AddWithValue(array[i, 0], array[i, 1]);
                        }
                    }
                }

                connection.Open();
                var reader = adapter.SelectCommand.ExecuteReader(CommandBehavior.CloseConnection);
                while (reader.Read())
                {
                    orderIds.Add(reader[0].ToString());
                }

                connection.Close();

                return orderIds;
            }
        }

        public List<string> GetOrderStateIDsByFilters(string query, string[,] array)
        {
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                var orderStateIds = new List<string>();

                var adapter = new SqlDataAdapter(query, connection);

                for (var i = 0; i < array.GetLength(0); i++)
                {
                    if (array[i, 0].Equals("@p" + i))
                    {
                        adapter.SelectCommand.Parameters.AddWithValue(array[i, 0], (DateTime?)Convert.ToDateTime(array[i, 1]));
                    }
                    else
                    {
                        if (array[i, 1] == null)
                        {
                            adapter.SelectCommand.Parameters.AddWithValue(array[i, 0], DBNull.Value);
                        }
                        else
                        {
                            adapter.SelectCommand.Parameters.AddWithValue(array[i, 0], array[i, 1]);
                        }
                    }
                }

                connection.Open();
                var reader = adapter.SelectCommand.ExecuteReader(CommandBehavior.CloseConnection);
                while (reader.Read())
                {
                    orderStateIds.Add(reader[0].ToString());
                }

                connection.Close();

                return orderStateIds;
            }
        }
        /// <summary>
        /// Get order details based on orderId.
        /// </summary> 
        /// <param name="orderId">Order Id to get the order details.</param>
        /// <returns>Returns the order detail dataset.</returns> 
        public DataSet GetOrderDetailsByOrderId(int orderId)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetShippingOrderExtn", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@OrderId", orderId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }
        

        #region Public Methods - related to Order Web service

        /// <summary>
        /// Get order XML by portal Id.
        /// </summary>
        /// <param name="portalId">Portal Id to get the orders in XML.</param>   
        /// <returns>Returns the order details in XML format.</returns>
        public string GetOrdersByPortalId(int? portalId)
        {
            return this.GetOrdersBySearch(null, null, null, null, null, null, null, null, null, null, portalId);
        }

        /// <summary>
        /// Get order XML based input values.
        /// </summary>
        /// <param name="accountId">Account Id to search.</param>
        /// <param name="orderStatusId">Order state Id to search.</param>
        /// <param name="filter">0 - All, 1 - New, 2 - Modified</param>
        /// <param name="startDate">Get orders from this date.</param>
        /// <param name="endDate">Get orders to this date.</param>
        /// <param name="portalId">Portal Id to check.</param>
        /// <returns>Returns the order details XML.</returns>
        public string GetOrdersByAccountId(int accountId, int? orderStatusId, int filter, DateTime? startDate, DateTime? endDate, int? portalId)
        {
            return this.GetOrdersBySearch(null, null, null, null, null, accountId, orderStatusId, filter, startDate, endDate, portalId);
        }

        /// <summary>
        /// Get the order XML by order status id
        /// </summary>
        /// <param name="orderStatusId">Order state Id to filter the order.</param>        
        /// <param name="filter">0 - All, 1 - New, 2 - Modified</param>
        /// <param name="startDate">Get orders from this date.</param>
        /// <param name="endDate">Get orders to this date.</param>
        /// <param name="portalId">Portal Id to check.</param>
        /// <returns>Returns the order details XML.</returns>
        public string GetOrdersByStatus(int? orderStatusId, int filter, DateTime? startDate, DateTime? endDate, int? portalId)
        {
            return this.GetOrdersBySearch(null, null, null, null, null, null, orderStatusId, filter, startDate, endDate, portalId);
        }

        /// <summary>
        /// Returns Order XMl for the specified filter value
        /// </summary>
        /// <param name="filter">0 - All, 1 - New, 2 - Modified</param>
        /// <param name="startDate">Get orders from this date.</param>
        /// <param name="endDate">Get orders to this date.</param>
        /// <param name="portalId">Portal Id to check.</param>
        /// <returns>Returns the order details XML.</returns>
        public string GetOrdersByFilter(int filter, DateTime? startDate, DateTime? endDate, int? portalId)
        {
            return this.GetOrdersBySearch(null, null, null, null, null, null, null, filter, startDate, endDate, portalId);
        }

        /// <summary>
        /// Search Order based on the specified field and returns Order XMl
        /// </summary>
        /// <param name="orderStateId">Order state Id to search.</param>
        /// <param name="filter">0 - All, 1 - New, 2 - Modified</param>        
        /// <param name="startDate">Get orders from this date.</param>
        /// <param name="endDate">Get orders to this date.</param>
        /// <param name="firstName">Customer first name to search.</param>
        /// <param name="lastName">Customer last name to search.</param>
        /// <param name="companyName">Billing company name to search.</param>
        /// <param name="zipCode">Zip code to search</param>
        /// <param name="phoneNumber">Phone number to search.</param>
        /// <param name="portalId">Portal Id to search</param>
        /// <returns>Returns the order search result XML.</returns>
        public string SearchOrders(int? orderStateId, int filter, DateTime? startDate, DateTime? endDate, string firstName, string lastName, string companyName, string zipCode, string phoneNumber, int? portalId)
        {
            return this.GetOrdersBySearch(firstName, lastName, companyName, zipCode, phoneNumber, null, orderStateId, filter, startDate, endDate, portalId);
        }

        /// <summary>
        /// Search Order based on the specified field and returns Order XMl
        /// </summary>
        /// <param name="firstName">Customer first name to search.</param>
        /// <param name="lastName">Customer last name to search.</param>
        /// <param name="companyName">Billing company name to search.</param>
        /// <param name="zipCode">Zip code to search</param>
        /// <param name="phoneNumber">Phone number to search.</param>
        /// <param name="accountId">Account Id to search.</param>
        /// <param name="orderStateId">Order state Id to search.</param>
        /// <param name="filter">0 - All, 1 - New, 2 - Modified</param>
        /// <param name="startDate">Get orders from this date.</param>
        /// <param name="endDate">Get orders to this date.</param>
        /// <param name="portalId">Portal Id to search</param>
        /// <returns>Returns the order search result XML.</returns>
        private string GetOrdersBySearch(string firstName, string lastName, string companyName, string zipCode, string phoneNumber, int? accountId, int? orderStateId, int? filter, DateTime? startDate, DateTime? endDate, int? portalId)
        {
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create Instance of Adapter Object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNODE_WS_SearchOrders_XML", connection);

                // Mark the Select command as Stored procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@FirstName", firstName);
                adapter.SelectCommand.Parameters.AddWithValue("@LastName", lastName);
                adapter.SelectCommand.Parameters.AddWithValue("@CompanyName", companyName);
                adapter.SelectCommand.Parameters.AddWithValue("@ZipCode", zipCode);
                adapter.SelectCommand.Parameters.AddWithValue("@PhoneNumber", phoneNumber);
                adapter.SelectCommand.Parameters.AddWithValue("@AccountID", accountId);
                adapter.SelectCommand.Parameters.AddWithValue("@OrderStatusID", orderStateId);
                adapter.SelectCommand.Parameters.AddWithValue("@Filter", filter);
                adapter.SelectCommand.Parameters.AddWithValue("@BeginDate", startDate);
                adapter.SelectCommand.Parameters.AddWithValue("@EndDate", endDate);
                adapter.SelectCommand.Parameters.AddWithValue("@PortalID", portalId);
                connection.Open();

                SqlDataReader reader = adapter.SelectCommand.ExecuteReader(CommandBehavior.CloseConnection);
                string xmlOut = string.Empty;

                while (reader.Read())
                {
                    xmlOut = xmlOut + reader[0].ToString();
                }

                // Close the Connection
                connection.Close();

                // Returns
                return xmlOut;
            }
        }
        #endregion

        public bool UpdateOrderPaymentStatus(int orderId, string paymentStatus)
        {
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                var adapter = new SqlDataAdapter("ZNode_UpdateOrderPaymentStatus", connection) { SelectCommand = { CommandType = CommandType.StoredProcedure } };

                adapter.SelectCommand.Parameters.AddWithValue("@OrderId", orderId);
                adapter.SelectCommand.Parameters.AddWithValue("@PaymentStatusName", paymentStatus);

                connection.Open();
                var reader = adapter.SelectCommand.ExecuteReader(CommandBehavior.CloseConnection);

                reader.Read();
                bool status = false;

                if (reader.HasRows)
                {
                    status = Convert.ToBoolean(reader[0].ToString());
                }

                connection.Close();

                return status;
            }
        }
    }
}
