using System.Data;
using System.Data.SqlClient;

namespace ZNode.Libraries.DataAccess.Custom
{
    /// <summary>
    /// Product Type related Helper  methods
    /// </summary>
    public class ProductTypeHelper
    {
        #region Public Methods
        /// <summary>
        /// Get the number of product types for this attribute type
        /// </summary>
        /// <param name="attributeTypeId">Attribute type Id to get the product type count.</param>
        /// <returns>Rerurns the product type count.</returns>
        public int GetProductTypeCount(int attributeTypeId)
        {
            int count = 0;

            // Create instance of connection and Command Object
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlCommand command = new SqlCommand("ZNode_GETPRODUCTTYPECOUNT", connection);

                // Mark the command as store procedure
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@AttributeTypeId", attributeTypeId);

                // execute the Command
                connection.Open();
                count = int.Parse(command.ExecuteScalar().ToString());
                connection.Close();

                // Release the resources
                command.Dispose();
                connection.Close();
            }

            // Return value
            return count;
        }

        /// <summary>
        /// Get all product types for  a portal
        /// </summary>
        /// <returns>Returns the product type dataset.</returns>
        public DataSet GetAllProductType()
        {
            // Create instance of connection Object
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNODE_GETALLPRODUCTTYPE", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet, "ProductType");

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Search the attributes by name and return the data.
        /// </summary>
        /// <param name="name">Attribute name to search.</param>
        /// <returns>Returns the attribute dataset.</returns>
        public DataSet GetAttributeBySearchData(string name)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_SearchAttribtues", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@Name", name);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet, "Attributes");

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Search the product type by name and description
        /// </summary>
        /// <param name="name">Product type name to search.</param>
        /// <param name="description">Product type description to search.</param>
        /// <returns>Returns the product type dataset.</returns>
        public DataSet GetProductTypeBySearchData(string name, string description)
        {
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_SearchProductTypes", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@Name", name);
                adapter.SelectCommand.Parameters.AddWithValue("@Description", description);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet, "ProductType");

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }
        /// <summary>
        /// Get Existance of Products associated with Product Type
        /// </summary>
        /// <param name="producttypeid">Product type Id.</param>        
        /// <returns></returns>
        public int GetProductsByProductTypeId(int producttypeid)
        {
            int count = 0;

            // Create instance of connection and Command Object
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlCommand command = new SqlCommand("ZNode_GetProductsByProductTypeId", connection);

                // Mark the command as store procedure
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@ProductTypeId", producttypeid);

                // execute the Command
                connection.Open();
                count = int.Parse(command.ExecuteScalar().ToString());
                connection.Close();

                // Release the resources
                command.Dispose();
                connection.Close();
            }

            // Return value
            return count;
        }
        #endregion
    }
}
