﻿using System.Data;
using System.Data.SqlClient;

namespace ZNode.Libraries.DataAccess.Custom
{
    /// <summary>
    /// Represents the Gift Card Helper Class.
    /// </summary>
    public class GiftCardHelper
    {        
        /// <summary>
        /// Returns the list of gift cards based on the search criteria.
        /// </summary>
        /// <param name="giftCardSearchParam">GiftCardSearchParam object.</param>
        /// <returns>Returns list of gift card object as DataSet.</returns>
        public DataSet Search(GiftCardSearchParam giftCardSearchParam)
        {
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlDataAdapter adapter = null;

                adapter = new SqlDataAdapter("ZNode_SearchGiftCards", connection);

                // Mark the Select command as Stored procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;

                // Add Paramters into Command
                adapter.SelectCommand.Parameters.AddWithValue("@Name", giftCardSearchParam.Name);
                adapter.SelectCommand.Parameters.AddWithValue("@Amount", giftCardSearchParam.Balance);
                adapter.SelectCommand.Parameters.AddWithValue("@CardNumber", giftCardSearchParam.CardNumber);
                adapter.SelectCommand.Parameters.AddWithValue("@AccountID", giftCardSearchParam.AccountID);
                adapter.SelectCommand.Parameters.AddWithValue("@DontShowExpired", giftCardSearchParam.DontShowExpired);
                
                // Fill Dataset
                DataSet ds = new DataSet();
                connection.Open();

                adapter.Fill(ds, "ZNodeGiftCards");

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // return DataSet
                return ds;
            }
        }
    }        
}
