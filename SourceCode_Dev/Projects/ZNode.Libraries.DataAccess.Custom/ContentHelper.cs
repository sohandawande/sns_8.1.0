﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace ZNode.Libraries.DataAccess.Custom
{
    /// <summary>
    /// Content related helper functions
    /// </summary>
    public class ContentHelper
    {
        /// <summary>
        /// Gets the content list for the site map
        /// </summary>
        /// <param name="portalId">Portal id for the xml</param>
        /// <returns>Returns the data set with content pages list</returns>
        public DataSet GetContentListXMLSiteMap(string portalId)
        {
            // Create instance of connection     
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetContentListXMLSiteMap", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;

                adapter.SelectCommand.Parameters.AddWithValue("@PortalId", portalId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }
    }
}
