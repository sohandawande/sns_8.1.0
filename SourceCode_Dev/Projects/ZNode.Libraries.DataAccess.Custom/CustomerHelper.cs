using System;
using System.Data;
using System.Data.SqlClient;

namespace ZNode.Libraries.DataAccess.Custom
{
    /// <summary>
    /// Customer Related Helper Methods
    /// </summary>
    public partial class CustomerHelper
    {
        #region Public Methods
        /// <summary>
        /// Search the customer and return the result based on input.
        /// </summary>
        /// <param name="firstName">First name to search.</param>
        /// <param name="lastName">Last name to search.</param>
        /// <param name="companyName">Company name to search.</param>
        /// <param name="loginName">Login name to search.</param>
        /// <param name="externalAccountNum">External account number to search.</param>
        /// <param name="accountId">Account Id to search.</param>
        /// <param name="startDate">Account created from this date.</param>
        /// <param name="endDate">Account created to this date.</param>
        /// <param name="phoneNumber">Phone number to search.</param>
        /// <param name="emailId">Email Id to search.</param>
        /// <param name="profileId">Profile Id to search.</param>
        /// <param name="referralStatus">Referral status to search.</param>
        /// <param name="portalId">Portal Id to search.</param>
        /// <param name="rolename">Role Name to search</param>
        /// <returns>Returns the search result dataset.</returns>
        public DataSet SearchCustomer(string firstName, string lastName, string companyName, string loginName, string externalAccountNum, string accountId, string startDate, string endDate, string phoneNumber, string emailId, string profileId, string referralStatus, string portalId, string rolename)
        {
            // Create instance of connection     
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_SearchCustomer", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;

                // Add parameters to the Sql Select Command
                adapter.SelectCommand.Parameters.AddWithValue("@FirstName", firstName);
                adapter.SelectCommand.Parameters.AddWithValue("@LastName", lastName);
                adapter.SelectCommand.Parameters.AddWithValue("@CompanyName", companyName);
                adapter.SelectCommand.Parameters.AddWithValue("@ProfileId", profileId);
                adapter.SelectCommand.Parameters.AddWithValue("@StartDate", startDate);
                adapter.SelectCommand.Parameters.AddWithValue("@EndDate", endDate);
                adapter.SelectCommand.Parameters.AddWithValue("@LoginName", loginName);
                adapter.SelectCommand.Parameters.AddWithValue("@AccountId", accountId);
                adapter.SelectCommand.Parameters.AddWithValue("@ExternalAccountNum", externalAccountNum);
                adapter.SelectCommand.Parameters.AddWithValue("@PhoneNum", phoneNumber);
                adapter.SelectCommand.Parameters.AddWithValue("@EmailId", emailId);
                adapter.SelectCommand.Parameters.AddWithValue("@ReferralStatus", referralStatus);
                adapter.SelectCommand.Parameters.AddWithValue("@PortalId", portalId);
                adapter.SelectCommand.Parameters.AddWithValue("@RoleName", rolename);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet, "Account");

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

         /// <summary>
        /// Search the customer and return the result based on input.
        /// </summary>
        /// <param name="firstName">First name to search.</param>
        /// <param name="lastName">Last name to search.</param>
        /// <param name="companyName">Company name to search.</param>
        /// <param name="loginName">Login name to search.</param>
        /// <param name="externalAccountNum">External account number to search.</param>
        /// <param name="accountId">Account Id to search.</param>
        /// <param name="startDate">Account created from this date.</param>
        /// <param name="endDate">Account created to this date.</param>
        /// <param name="phoneNumber">Phone number to search.</param>
        /// <param name="emailId">Email Id to search.</param>
        /// <param name="profileId">Profile Id to search.</param>
        /// <param name="referralStatus">Referral status to search.</param>
        /// <param name="portalId">Portal Id to search.</param>
        /// <returns>Returns the search result dataset.</returns>
        public DataSet SearchCustomer(string firstName, string lastName, string companyName, string loginName, string externalAccountNum, string accountId, string startDate, string endDate, string phoneNumber, string emailId, string profileId, string referralStatus, string portalId)
        {
            return this.SearchCustomer(firstName, lastName, companyName, loginName, externalAccountNum, accountId, startDate, endDate, phoneNumber, emailId, profileId, referralStatus, portalId, null);
        }

        /// <summary>
        /// Get a login name by user Id.
        /// </summary>
        /// <param name="userId">User Id to get the login name.</param>
        /// <returns>Returns the user login name.</returns>
        public string GetByUserID(int userId)
        {
            // Create instance of connection     
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create Instance for SqlDataAdapter Object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNODE_GetByUserID", connection);

                // Mark Select Command as Stored Procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@ACCOUNTID", userId);

                // Execute Query
                connection.Open();

                string loginName = Convert.ToString(adapter.SelectCommand.ExecuteScalar());
                connection.Close();

                // Return the login name
                return loginName;
            }
        }

        /// <summary>
        /// Get the filtered affiliate dataset
        /// </summary>
        /// <param name="firstName">First name to search.</param>
        /// <param name="lastName">Last name to search.</param>
        /// <param name="companyName">Company name to search.</param>        
        /// <param name="accountId">Account Id to search.</param>
        /// <param name="status">Status to search.</param>
        /// <returns>Returns the affiliate account search result dataset.</returns>
        public DataSet SearchAffiliate(string firstName, string lastName, string companyName, string accountId, string status)
        {
            // Create instance of connection Object  
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_SearchAffiliate", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@FirstName", firstName);
                adapter.SelectCommand.Parameters.AddWithValue("@LastName", lastName);
                adapter.SelectCommand.Parameters.AddWithValue("@CompanyName", companyName);
                adapter.SelectCommand.Parameters.AddWithValue("@AccountID", accountId);
                adapter.SelectCommand.Parameters.AddWithValue("@Status", status);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet, "Affiliate");

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Get all the affiliate account dataset
        /// </summary>        
        /// <returns>Returns the affiliate account dataset.</returns>
        public DataSet GetAffiliate()
        {
            // Create instance of connection Object
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetAffiliate", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet, "Affiliate");

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Search the customer from order desk.
        /// </summary>
        /// <param name="firstName">First name to search.</param>
        /// <param name="lastName">Last name to search.</param>
        /// <param name="companyName">Company name to search.</param>
        /// <param name="portalCode">Postal code to search.</param>
        /// <param name="loginName">Login name to search.</param>
        /// <param name="orderId">Order Id to search.</param>
        /// <param name="portalId">Portal Id to search.</param>
        /// <param name="portalIds">Comma seperated portal Id to search.</param>
        /// <returns>Returns the customer search result dataset.</returns>
        public DataSet CustomerSearch(string firstName, string lastName, string companyName, string portalCode, string loginName, int orderId, int portalId, string portalIds)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_CustomerSearch", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@FirstName", firstName);
                adapter.SelectCommand.Parameters.AddWithValue("@LastName", lastName);
                adapter.SelectCommand.Parameters.AddWithValue("@CompanyName", companyName);
                adapter.SelectCommand.Parameters.AddWithValue("@Postalcode", portalCode);
                adapter.SelectCommand.Parameters.AddWithValue("@LoginName", loginName);
                adapter.SelectCommand.Parameters.AddWithValue("@OrderId", orderId);
                adapter.SelectCommand.Parameters.AddWithValue("@PortalId", portalId);
                adapter.SelectCommand.Parameters.AddWithValue("@PortalIds", portalIds);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet, "Account");

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Insert Profiles Associated with single account id.
        /// </summary>
        /// <param name="accountId">account id</param>
        /// <param name="profileIds">profile id</param>
        /// <returns>True / False</returns>
        public bool InsertAccountAssociatedProfiles(int accountId, string profileIds)
        {
            int results = 0;
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlCommand command = new SqlCommand("ZNode_InsertMultipleAccountProfile", connection);

                // Mark the command as store procedure
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@AccountId", accountId);
                command.Parameters.AddWithValue("@ProfileId", profileIds);

                // Open the connection 
                connection.Open();

                results = Convert.ToInt32(command.ExecuteScalar());

                // Release the resources
                command.Dispose();
                connection.Close();
            }
            // Return the result.
            return Convert.ToBoolean(results);
        }
        #endregion
    }
}
