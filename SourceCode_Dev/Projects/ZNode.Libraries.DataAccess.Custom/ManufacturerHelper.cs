using System.Data;
using System.Data.SqlClient;

namespace ZNode.Libraries.DataAccess.Custom
{
    public class ManufacturerHelper
    {
        /// <summary>
        /// Search the manufaturer and returns the searh result.
        /// </summary>
        /// <param name="name">Manufaturer name to search.</param>
        /// <returns>Returns the search result dataset.</returns>
        public DataSet GetManufacturerBySearchData(string name)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_Search_Manufacturers", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;                
                adapter.SelectCommand.Parameters.AddWithValue("@Name", name);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet, "Manufacturer");

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Get the brand navigation items for a portal
        /// </summary>
        /// <param name="portalId">Portal Id to get the brand</param>
        /// <param name="localeId">Locale Id to get the brand. </param>
        /// <returns>Returns the brand navigation item data table.</returns>
        public DataTable GetBrandNavigationItems(int portalId, int localeId)
        {
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlDataAdapter command = new SqlDataAdapter("ZNode_GetBrandNavigationItems", connection);

                // Mark the command as store procedure
                command.SelectCommand.CommandType = CommandType.StoredProcedure;

                SqlParameter parameter = new SqlParameter("@portalId", SqlDbType.Int);
                parameter.Value = portalId;
                command.SelectCommand.Parameters.Add(parameter);

                SqlParameter parameterlocaleId = new SqlParameter("@localeId", SqlDbType.Int);
                parameterlocaleId.Value = localeId;
                command.SelectCommand.Parameters.Add(parameterlocaleId);
                
                // Create and Fill the datatable
                DataTable myDatatable = new DataTable();
                command.Fill(myDatatable);

                command.Dispose();

                // close connection
                connection.Close();

                // Return the datadet.
                return myDatatable;
            }
        }
    }
}
