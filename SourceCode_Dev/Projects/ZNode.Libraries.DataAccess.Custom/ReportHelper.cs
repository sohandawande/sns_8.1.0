﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace ZNode.Libraries.DataAccess.Custom
{
    #region Znode Report Enumeration

    /// <summary>
    /// ZnodeReport enumeration.
    /// </summary>
    public enum ZnodeReport
    {
        /// <summary>
        /// Note: If this enumeration name updated Stored procedure Znode_Reports shuold be updated for name chanegs
        /// </summary>
        None = 0,

        /// <summary>
        /// Best seller report
        /// </summary>
        BestSeller = 20,

        /// <summary>
        /// Best seller for the day report.
        /// </summary>
        BestSellerByDay = 201,

        /// <summary>
        /// Best seller for the week report.
        /// </summary>
        BestSellerByWeek = 202,

        /// <summary>
        /// Best seller for the month report.
        /// </summary>
        BestSellerByMonth = 203,

        /// <summary>
        /// Best seller for the quarter report.
        /// </summary>
        BestSellerByQuarter = 204,

        /// <summary>
        /// Best seller for the year report.
        /// </summary>
        BestSellerByYear = 205,

        /// <summary>
        /// Orders and details report.
        /// </summary>
        Orders = 12,

        /// <summary>
        /// Recurring Billing details report.
        /// </summary>
        RecurringBilling = 206,

        /// <summary>
        /// Order pick list report.
        /// </summary>
        Picklist = 13,

        /// <summary>
        /// Email opt-in customer report.
        /// </summary>
        EmailOptInCustomer = 14,

        /// <summary>
        /// Frequent customer report.
        /// </summary>
        FrequentCustomer = 15,

        /// <summary>
        /// Frequent customer for the day report.
        /// </summary>
        FrequentCustomerByDay = 151,

        /// <summary>
        /// Frequent customer for the week report.
        /// </summary>
        FrequentCustomerByWeek = 152,

        /// <summary>
        /// Frequent customer for the month report.
        /// </summary>
        FrequentCustomerByMonth = 153,

        /// <summary>
        /// Frequent customer for the quarter report.
        /// </summary>
        FrequentCustomerByQuarter = 154,

        /// <summary>
        /// Frequent customer for the year report.
        /// </summary>
        FrequentCustomerByYear = 155,

        /// <summary>
        /// Top spending customer report
        /// </summary>
        TopSpendingCustomer = 16,

        /// <summary>
        /// Top spending customer for the day report.
        /// </summary>
        TopSpendingCustomerByDay = 161,

        /// <summary>
        /// Top spending customer for the week report
        /// </summary>
        TopSpendingCustomerByWeek = 162,

        /// <summary>
        /// Top spending customer for the month repoort.
        /// </summary>
        TopSpendingCustomerByMonth = 163,

        /// <summary>
        /// Top spending customer for the quarter report.
        /// </summary>
        TopSpendingCustomerByQuarter = 164,

        /// <summary>
        /// Top spending customer for the year report.
        /// </summary>
        TopSpendingCustomerByYear = 165,

        /// <summary>
        /// Top earning report
        /// </summary>
        TopEarningProduct = 17,

        /// <summary>
        /// Top earning report for the day report
        /// </summary>
        TopEarningProductByDay = 171,

        /// <summary>
        /// Top earning report for the week report.
        /// </summary>
        TopEarningProductByWeek = 172,

        /// <summary>
        /// Top earning report for the month report.
        /// </summary>
        TopEarningProductByMonth = 173,

        /// <summary>
        /// Top earning report for the quarter report.
        /// </summary>
        TopEarningProductByQuarter = 174,

        /// <summary>
        /// Top earning report for the year report.
        /// </summary>
        TopEarningProductByYear = 175,

        /// <summary>
        /// Account list report.
        /// </summary>
        Accounts = 21,

        /// <summary>
        /// Activity log report.
        /// </summary>
        ActivityLog = 22,

        /// <summary>
        /// Reorder level report.
        /// </summary>
        ReOrder = 18,

        /// <summary>
        /// Service request report.
        /// </summary>
        ServiceRequest = 19,

        /// <summary>
        /// Popular search report.
        /// </summary>
        PopularSearch = 23,

        /// <summary>
        /// Popular search of the day report.
        /// </summary>
        PopularSearchByDay = 231,

        /// <summary>
        /// Popular search of the week report.
        /// </summary>
        PopularSearchByWeek = 232,

        /// <summary>
        /// Popular search of the month report.
        /// </summary>
        PopularSearchByMonth = 233,

        /// <summary>
        /// Popular search of the quarter report.
        /// </summary>
        PopularSearchByQuarter = 234,

        /// <summary>
        /// Popular search of the year report.
        /// </summary>
        PopularSearchByYear = 235,

        /// <summary>
        /// Coupon usage report.
        /// </summary>
        CouponUsage = 24,

        /// <summary>
        /// Coupon usage for the day report.
        /// </summary>
        CouponUsageByDay = 241,

        /// <summary>
        /// Coupon usage for the week report.
        /// </summary>
        CouponUsageByWeek = 242,

        /// <summary>
        /// Coupon usage for the month report.
        /// </summary>
        CouponUsageByMonth = 243,

        /// <summary>
        /// Coupon usage for the quarter report.
        /// </summary>
        CouponUsageByQuarter = 242,

        /// <summary>
        /// Coupon usage for the year report.
        /// </summary>
        CouponUsageByYear = 245,

        /// <summary>
        /// Sales tax report.
        /// </summary>
        SalesTax = 25,

        /// <summary>
        /// Sales tax for the month report.
        /// </summary>
        SalesTaxByMonth = 251,

        /// <summary>
        /// Sales tax for the quarter report.
        /// </summary>
        SalesTaxByQuarter = 252,

        /// <summary>
        /// Affiliate order report.
        /// </summary>
        AffiliateOrder = 26,

        /// <summary>
        /// Affiliate order for the day report.
        /// </summary>
        AffiliateOrderByDay = 261,

        /// <summary>
        /// Affiliate order for the week report.
        /// </summary>
        AffiliateOrderByWeek = 262,

        /// <summary>
        /// Affiliate order for the month report.
        /// </summary>
        AffiliateOrderByMonth = 263,

        /// <summary>
        /// Affiliate order for the quarter report.
        /// </summary>
        AffiliateOrderByQuarter = 264,

        /// <summary>
        /// Affiliate order for the year report.
        /// </summary>
        AffiliateOrderByYear = 265,

        /// <summary>
        /// Supplier list report.
        /// </summary>
        SupplierList = 27,

        /// <summary>
        /// Vendor income report.
        /// </summary>
        VendorRevenue = 266,

        /// <summary>
        /// Vendor revenue report.
        /// </summary>
        VendorProductRevenue = 267,

        /// <summary>
        /// Products sold on vendor site report 
        /// </summary>
        ProductSoldOnVendorSites = 268,

        /// <summary>
        /// RMA Report
        /// </summary>
        RMAReport = 270
    }
    #endregion

    /// <summary>
    /// Znode report period.
    /// </summary>
    public enum ZnodeReportInterval
    {
        /// <summary>
        /// Report interval not assigned.
        /// </summary>
        None = 0,

        /// <summary>
        /// Report interval is day.
        /// </summary>
        Day = 1,

        /// <summary>
        /// Report interval is weel.
        /// </summary>
        Week = 2,

        /// <summary>
        /// Report interval is month.
        /// </summary>
        Month = 3,

        /// <summary>
        /// Report interval is quarter.
        /// </summary>
        Quarter = 4,

        /// <summary>
        /// Report interval is year.
        /// </summary>
        Year = 5
    }

    /// <summary>
    /// Manage the multifront reports.
    /// </summary>
    public class ReportHelper
    {
        /// <summary>
        /// Return the dataset based on report criteria.
        /// </summary>
        /// <param name="filter">Filter index</param>
        /// <param name="fromDate">Get repport from this date</param>
        /// <param name="toDate">Get report to this date</param>
        /// <param name="supplierid">Supplier Id</param>
        /// <param name="custom1">Custom Field 1</param>
        /// <param name="custom2">Custom Field 2</param>
        /// <returns>Returns the result dataset.</returns>
        public DataSet ReportList(ZnodeReport filter, DateTime fromDate, DateTime toDate, string supplierid, string custom1, string custom2)
        {
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlDataAdapter adapter = null;

                adapter = new SqlDataAdapter("ZNode_Reports", connection);

                // Mark the Select command as Stored procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;

                // Add Paramters into Command
                adapter.SelectCommand.Parameters.AddWithValue("@Filter", filter.ToString());
                adapter.SelectCommand.Parameters.AddWithValue("@FromDate", fromDate);
                adapter.SelectCommand.Parameters.AddWithValue("@ToDate", toDate);
                adapter.SelectCommand.Parameters.AddWithValue("@SupplierId", supplierid);
                adapter.SelectCommand.Parameters.AddWithValue("@Custom1", custom1);
                adapter.SelectCommand.Parameters.AddWithValue("@Custom2", custom2);

                // Fill Dataset
                DataSet dataSet = new DataSet();
                connection.Open();

                adapter.Fill(dataSet, "ZnodeReport");

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // return DataSet
                return dataSet;
            }
        }

        /// <summary>
        /// REturn the data set based on recored id.
        /// </summary>
        /// <param name="orderId">Int Order Id</param>
        /// <param name="reportName">Report Name</param>
        /// <returns>Returns DataSet</returns>
        public DataSet GetOrderDetails(int orderId, string reportName)
        {
            // Create instance of connection
            string _proc = "ZNode_GetOrderLineItems";
            if (Equals(reportName, "ZNode_ReportsRecurring"))
            {
                _proc = "ZNode_GetRecurringBillingsubRecords";
            }
            else if (Equals(reportName, "ZNode_ReportsSupplierList"))
            {
               _proc = "ZNode_ReportsSupplierListFor80";
            }
            else if (Equals(reportName, "ZNode_ReportsPicklist"))
            {
                _proc = "ZNode_ReportsPicklistSubReport";
            }
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter(_proc, connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;

                // Add parameters to command object                
                adapter.SelectCommand.Parameters.AddWithValue("@orderId", orderId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }

        }
    }
}
