using System;
using System.Data;
using System.Data.SqlClient;

namespace ZNode.Libraries.DataAccess.Custom
{
    /// <summary>
    /// Contains data access functions for store settings
    /// </summary>
    public class StoreSettingsHelper
    {
        /// <summary>
        /// Get all the payment settings for a portal
        /// </summary>        
        /// <returns>Returns the payment settings dataset.</returns>
        public DataSet GetAllPaymentSettings()
        {
            return this.GetAllPaymentSettings(null);
        }

        /// <summary>
        /// Get all the payment settings for a portal
        /// </summary>
        /// <param name="profileId">Profile Id to get the associated payment settings.</param>
        /// <returns>Returns the payment settings dataset.</returns>
        public DataSet GetAllPaymentSettings(int? profileId)
        {
            // Create instance of connection Object
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetAllPaymentSettings", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;

                if (profileId.HasValue)
                {
                    adapter.SelectCommand.Parameters.AddWithValue("@ProfileID", @profileId);
                }

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Searcg the payment settings by keyword.
        /// </summary>
        /// <param name="keyword">Keyword to search the payment settings.</param>
        /// <returns>Returns the payment settting search result dataset.</returns>
        public DataSet SearchCountryByCodeAndName(string keyword)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_SearchCountry", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@Keyword", keyword);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Search the store by keyword.
        /// </summary>
        /// <param name="keyword">Keyword to search the store.</param>
        /// <returns>Returns the store search result dataset.</returns>
        public DataSet GetStoreByKeyword(string keyword)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetStores", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@Keyword", keyword);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Returns the FedExCSPKey and FedExCSPPassword.
        /// </summary>        
        /// <returns>Returns the FedEx key and password details dataset.</returns>
        public DataSet GetFedExKey()
        {
            // Create instance of connection Object
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetFedExKey", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Get the count of all portals in the application
        /// </summary>
        /// <returns>Returns th store count.</returns>
        public int GetStoreCount()
        {
            int count = 0;

            // Create instance of connection and Command Object
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlCommand command = new SqlCommand("ZNode_GetStoreCount", connection);

                // Mark the command as store procedure
                command.CommandType = CommandType.StoredProcedure;

                // execute the Command
                connection.Open();
                count = int.Parse(command.ExecuteScalar().ToString());
                connection.Close();

                // Release the resources
                command.Dispose();
                connection.Close();
            }

            // Return value
            return count;
        }

        /// <summary>
        /// Delete the portal by portal Id
        /// </summary>        
        /// <param name="portalId">Portal Id to delete the portal.</param>
        /// <returns>Returns true if deleted otherwise false.</returns>
        public bool DeleteByPortalId(int portalId)
        {
            // Create instance of connection and Command Object
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                System.Data.SqlClient.SqlTransaction transaction = null;
                bool isDeleted = false;

                try
                {                    
                    // Open Connection
                    connection.Open();

                    // BeginTransaction() Requires Open Connection
                    transaction = connection.BeginTransaction();

                    // Create instance of Command Object
                    SqlCommand command = new SqlCommand("ZNode_DeletePortalByPortalID", connection);

                    // Mark the Command as a Stored Procedure
                    command.CommandType = CommandType.StoredProcedure;

                    // Assign Transaction to Command
                    command.Transaction = transaction;
                    
                    command.Parameters.AddWithValue("@PortalID", portalId);

                    // Execute Query
                    command.ExecuteNonQuery();

                    // Set Return values 
                    isDeleted = true;

                    // If we reach here, all command succeeded, so commit the transaction
                    transaction.Commit();
                }
                catch (Exception)
                {
                    // Something went wrong, so rollback the transaction
                    transaction.Rollback();
                    isDeleted = false;
                }
                finally
                {
                    if (transaction != null)
                    {
                        transaction.Dispose();
                    }

                    if (connection != null)
                    {
                        // Finally, close the connection
                        connection.Close();

                        // Release the resources
                        connection.Dispose();
                    }
                }

                // Return boolean value
                return isDeleted;
            }
        }
    }
}
