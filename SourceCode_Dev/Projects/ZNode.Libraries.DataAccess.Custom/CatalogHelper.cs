﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace ZNode.Libraries.DataAccess.Custom
{
    public class CatalogHelper
    {
        /// <summary>
        /// Copy the catalog.
        /// </summary>
        /// <param name="catalogId">Source catalog Id.</param>
        /// <param name="catalogName">Destination catalog name.</param>
        /// <returns>Returns true if copied otherwise false.</returns>
        public bool CopyCatalog(int catalogId, string catalogName)
        {
            return this.CopyCatalog(catalogId, catalogName, null);
        }

        /// <summary>
        /// Copy the catalog.
        /// </summary>
        /// <param name="catalogId">Source catalog Id.</param>
        /// <param name="catalogName">Destination catalog name.</param>
        /// <param name="localeId">Locale Id.</param>
        /// <returns>Returns true if copied otherwise false.</returns>
        public bool CopyCatalog(int catalogId, string catalogName, int? localeId)
        {
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                connection.Open();
                SqlTransaction transaction = null;

                try
                {
                    transaction = connection.BeginTransaction();

                    SqlCommand command = new SqlCommand("ZNode_CopyCatalog", connection);

                    // Mark the command as store procedure
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@CatalogId", catalogId);
                    command.Parameters.AddWithValue("@CatalogName", catalogName);
                   
                    if (localeId.HasValue)
                    {
                        command.Parameters.AddWithValue("@LocaleID", localeId);
                    }

                    //To increase the command timeout for bulk data copy.
                    command.CommandTimeout = 120;

                    command.Transaction = transaction;
                    command.ExecuteNonQuery();

                    // Commit the transaction.
                    transaction.Commit();

                    return true;
                }
                catch (SqlException)
                {
                    // Rollback the transaction, if error occurred.
                    transaction.Rollback();

                    throw;
                }                
                finally
                {
                    // Close connection
                    connection.Close();
                }
            }
        }

        /// <summary>
        /// Empty the catalog data.
        /// </summary>
        /// <param name="catalogId">Catalog Id to delete the catalog object.</param>
        /// <returns>Returns true if cleared otherwise false.</returns>        
        public bool DeleteCatalog(int catalogId, bool preserve)
        {
            bool isDeleted = false;

            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                System.Data.SqlClient.SqlTransaction transaction = null;

                try
                {
                    // Open Connection
                    connection.Open();

                    // BeginTransaction() Requires Open Connection
                    transaction = connection.BeginTransaction();

                    // Create instance of Command Object
                    SqlCommand command = new SqlCommand("ZNode_DeleteCatalog", connection);

                    // Mark the Command as a Stored Procedure
                    command.CommandType = CommandType.StoredProcedure;

                    // Assign Transaction to Command
                    command.Transaction = transaction;

                    command.Parameters.AddWithValue("CatalogID", catalogId);
                    command.Parameters.AddWithValue("Preserve", preserve);

                    // Execute Query
                    command.ExecuteNonQuery();

                    // Set Return values 
                    isDeleted = true;

                    // If we reach here, all command succeeded, so commit the transaction
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("Delete Catalog - " + ex.Message);

                    // Something went wrong, so rollback the transaction
                    transaction.Rollback();

                    isDeleted = false;
                }
                finally
                {
                    if (transaction != null)
                    {
                        transaction.Dispose();
                    }

                    if (connection != null)
                    {
                        // Finally, close the connection
                        connection.Close();

                        // Release the resources
                        connection.Dispose();
                    }
                }
            }

            // Return boolean value
            return isDeleted;
        }

        public string GetCatalogIDByExternalID(string externalId)
        {
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                var adapter = new SqlDataAdapter("ZNode_GetCatalogByExternalID", connection) { SelectCommand = { CommandType = CommandType.StoredProcedure } };

                adapter.SelectCommand.Parameters.AddWithValue("@externalId", externalId);

                connection.Open();
                var reader = adapter.SelectCommand.ExecuteReader(CommandBehavior.CloseConnection);

                reader.Read();
                string catalogId = null;

                if (reader.HasRows)
                {
                    catalogId = reader[0].ToString();
                }

                connection.Close();

                return catalogId;
            }
        }

        public List<string> GetCatalogIDsByFilters(string query, string[,] array)
        {
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                var catalogIds = new List<string>();
                var adapter = new SqlDataAdapter(query, connection);
                for (var i = 0; i < array.GetLength(0); i++)
                {
                    if (array[i, 0].Equals("@p" + i))
                    {
                        adapter.SelectCommand.Parameters.AddWithValue(array[i, 0], (DateTime?)Convert.ToDateTime(array[i, 1]));
                    }
                    else
                    {
                        if (array[i, 1] == null)
                        {
                            adapter.SelectCommand.Parameters.AddWithValue(array[i, 0], DBNull.Value);
                        }
                        else
                        {
                            adapter.SelectCommand.Parameters.AddWithValue(array[i, 0], array[i, 1]);
                        }
                    }
                }
                connection.Open();
                var reader = adapter.SelectCommand.ExecuteReader(CommandBehavior.CloseConnection);
                while (reader.Read())
                {
                    catalogIds.Add(reader[0].ToString());
                }

                connection.Close();

                return catalogIds;
            }
        }

        /// <summary>
        /// Get catalog details by portal Id
        /// </summary>
        /// <param name="portalId">Portal Id to get all the catalog.</param>      
        /// <returns>Returns the catalog details dataset.</returns>
        public DataSet GetCatalogsByPortalId(int portalId)
        {
            // Create instance of connection     
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetCatalogsByPortalId", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;

                adapter.SelectCommand.Parameters.AddWithValue("@PortalId", portalId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }
    }
}
