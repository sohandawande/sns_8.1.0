﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace ZNode.Libraries.DataAccess.Custom
{
    public class ThemeHelper
    {
        public bool DeleteThemeByID(int themeId)
        {
            bool isSuccess = false;
            // Create instance of connection     
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlCommand command = null;
                System.Data.SqlClient.SqlTransaction transaction = null;

                try
                {
                    // Open Connection
                    connection.Open();

                    // BeginTransaction() Requires Open Connection
                    transaction = connection.BeginTransaction();

                    // Create instance of Command Object
                    command = new SqlCommand("ZNode_DeleteTheme", connection);

                    // Mark the Command as a Stored Procedure
                    command.CommandType = CommandType.StoredProcedure;

                    // Assign Transaction to Command
                    command.Transaction = transaction;

                    // Add parameters
                    command.Parameters.AddWithValue("@ThemeID", themeId);

                    // Execute Query
                    object rowsAffected = command.ExecuteScalar();

                    if (Equals(rowsAffected, true))
                    {
                        // Set Return values 
                        isSuccess = true;
                    }

                    // If we reach here, all command succeeded, so commit the transaction
                    transaction.Commit();
                }
                catch (Exception)
                {
                    // Something went wrong, so rollback the transaction
                    transaction.Rollback();
                    isSuccess = false;
                }
                finally
                {
                    if (!Equals(transaction, null))
                    {
                        transaction.Dispose();
                    }

                    if (!Equals(connection, null))
                    {
                        // Finally, close the connection
                        connection.Close();

                        // Release the resources
                        connection.Dispose();
                    }
                }
            }
            return isSuccess;
        }
    }
}
