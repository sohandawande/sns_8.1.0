﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace ZNode.Libraries.DataAccess.Custom
{
    public class LuceneSearchHelper
    {
        public void SwitchForLuceneTriggers(int triggerFlag)
        {
            // Create instance of connection
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlCommand command = new SqlCommand("ZNode_TurnLuceneTriggersOnOff", connection);
                connection.Open();

                command.CommandType = CommandType.StoredProcedure;

                // Add parameters to command object            
                command.Parameters.AddWithValue("@TriggerFlag", triggerFlag);

                command.ExecuteNonQuery();
                connection.Close();

                
               
            }
        }

        public int IsLuceneTriggersDisabled()
        {

            int isDisabled = 0;

            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZnodeECommerceDB"].ConnectionString))
            {
                SqlCommand command = new SqlCommand("ZNode_CheckLuceneTriggersDisabled", connection);

                // Mark the command as store procedure
                command.CommandType = CommandType.StoredProcedure;

                connection.Open();

                object scalarValue = command.ExecuteScalar();

                isDisabled = Convert.ToInt32(scalarValue);

                connection.Close();

                return isDisabled;
            }

        }

        public void CreateLuceneIndex(int intAccountID)
        {
            using (
                SqlConnection connection =
                    new SqlConnection(
                        System.Configuration.ConfigurationManager.ConnectionStrings["ZnodeECommerceDB"].ConnectionString)
                )
            {
                SqlCommand command = new SqlCommand("Znode_CreateIndexMonitor", connection);
                connection.Open();
                command.CommandType = CommandType.StoredProcedure;
               // command.Parameters.AddWithValue("@UserID", intAccountID);
                command.ExecuteNonQuery();
                connection.Close();

            }
        }

    }
}
