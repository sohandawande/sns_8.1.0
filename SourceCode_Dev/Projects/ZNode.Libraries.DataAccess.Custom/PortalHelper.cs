﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace ZNode.Libraries.DataAccess.Custom
{
    public class PortalHelper
    {
        public string GetPortalIDByExternalID(string externalId)
        {
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                var adapter = new SqlDataAdapter("ZNode_GetPortalByExternalID", connection) { SelectCommand = { CommandType = CommandType.StoredProcedure } };

                adapter.SelectCommand.Parameters.AddWithValue("@externalId", externalId);

                connection.Open();
                var reader = adapter.SelectCommand.ExecuteReader(CommandBehavior.CloseConnection);

                reader.Read();
                string portalId = null;

                if (reader.HasRows)
                {
                    portalId = reader[0].ToString();
                }

                connection.Close();

                return portalId;
            }
        }

        public List<string> GetPortalIDsByFilters(string query, string[,] array)
        {
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                var portalIds = new List<string>();
                var adapter = new SqlDataAdapter(query, connection);
                for (var i = 0; i < array.GetLength(0); i++)
                {
                    if (array[i, 0].Equals("@p" + i))
                    {
                        adapter.SelectCommand.Parameters.AddWithValue(array[i, 0], (DateTime?)Convert.ToDateTime(array[i, 1]));
                    }
                    else
                    {
                        if (array[i, 1] == null)
                        {
                            adapter.SelectCommand.Parameters.AddWithValue(array[i, 0], DBNull.Value);
                        }
                        else
                        {
                            adapter.SelectCommand.Parameters.AddWithValue(array[i, 0], array[i, 1]);
                        }
                    }
                }
                connection.Open();
                var reader = adapter.SelectCommand.ExecuteReader(CommandBehavior.CloseConnection);
                while (reader.Read())
                {
                    portalIds.Add(reader[0].ToString());
                }

                connection.Close();

                return portalIds;
            }
        }

        /// <summary>
        /// Returns Master Page based on theme and Pagetype(Product,Category)
        /// </summary>
        /// <param name="ProductID"></param>
        /// <returns>DataSet</returns>
        public DataSet GetMasterPage(int themeID, string pageType)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetMasterPageByType", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;

                adapter.SelectCommand.Parameters.AddWithValue("@ThemeID", themeID);
                adapter.SelectCommand.Parameters.AddWithValue("@PageType", pageType);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Znode Version 7.2.2
        /// This function Get all active shipping and billing countries for portalId.
        /// </summary>
        /// <param name="model">int portalId</param>
        /// <returns>Returns CountryListModel</returns>
        public DataSet GetActiveCountryByPortalId(int portalId, int billingShippingFlag = 0)
        {
            // Create instance of connection
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZnodeGetActiveCountryByPortal", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;

                // Add parameters to command object                
                adapter.SelectCommand.Parameters.AddWithValue("@PortalId", portalId);

                if (!Equals(billingShippingFlag, 0))
                {
                    adapter.SelectCommand.Parameters.AddWithValue("@BillingActive", billingShippingFlag.Equals(1) ? billingShippingFlag : 0);
                    adapter.SelectCommand.Parameters.AddWithValue("@ShippingActive", billingShippingFlag.Equals(2) ? 1 : 0);
                }

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Znode Version 8.0
        /// This function Get all information for portalId.
        /// </summary>
        /// <param name="model">int portalId</param>
        /// <returns>Returns Portal information</returns>
        public DataSet GetPortalInformationByPortalId(int portalId)
        {
            // Create instance of connection
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetPortalDetails", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;

                // Add parameters to command object                
                adapter.SelectCommand.Parameters.AddWithValue("@PortalId", portalId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }
    }
}
