using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace ZNode.Libraries.DataAccess.Custom
{
    public class ProfileHelper
    {
        /// <summary>
        /// Get profiles by portal Id which is not associated with the store.
        /// </summary>
        /// <param name="portalId">Portal Id to get the not associated profiles.</param>
        /// <returns>Returns the profiles not associated with the portal.</returns>
        public DataSet GetProfileByPortalID(int portalId)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetProfilesByPortalID", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@PortalId", portalId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Get the associated profiles by portal Id.
        /// </summary>
        /// <param name="portalId">Portal Id to get the associated profiles.</param>
        /// <returns>Returns the profiles not associated with the portal.</returns>
        public DataSet GetAssociatedProfilesByPortalID(int portalId)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetAssociatedProfilesByPortalID", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@PortalID", portalId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        public string GetProfileIDByExternalID(string externalId)
        {
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                var adapter = new SqlDataAdapter("ZNode_GetProfileByExternalID", connection) { SelectCommand = { CommandType = CommandType.StoredProcedure } };

                adapter.SelectCommand.Parameters.AddWithValue("@externalId", externalId);

                connection.Open();
                var reader = adapter.SelectCommand.ExecuteReader(CommandBehavior.CloseConnection);

                reader.Read();
                string profileId = null;

                if (reader.HasRows)
                {
                    profileId = reader[0].ToString();
                }

                connection.Close();

                return profileId;
            }
        }

        public List<string> GetProfileIDsByFilters(string query, string[,] array)
        {
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                var profileIds = new List<string>();
                var adapter = new SqlDataAdapter(query, connection);
                for (var i = 0; i < array.GetLength(0); i++)
                {
                    if (array[i, 0].Equals("@p" + i))
                    {
                        adapter.SelectCommand.Parameters.AddWithValue(array[i, 0], (DateTime?)Convert.ToDateTime(array[i, 1]));
                    }
                    else
                    {
                        if (array[i, 1] == null)
                        {
                            adapter.SelectCommand.Parameters.AddWithValue(array[i, 0], DBNull.Value);
                        }
                        else
                        {
                            adapter.SelectCommand.Parameters.AddWithValue(array[i, 0], array[i, 1]);
                        }
                    }
                }
                connection.Open();
                var reader = adapter.SelectCommand.ExecuteReader(CommandBehavior.CloseConnection);
                while (reader.Read())
                {
                    profileIds.Add(reader[0].ToString());
                }

                connection.Close();

                return profileIds;
            }
        }

        /// <summary>
        /// This method will call the Stored proc which will return the data of associted roles and stores
        /// </summary>
        /// <param name="userId">GUID user id</param>
        /// <param name="spName">string Store Procedure Name</param>
        /// <returns>returns out data as per sp</returns>
        public DataSet GetRolesAndPermissions(Guid userId)
        {         
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNodeGetRolesAndPermissions", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@UserId", userId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }

        }

        /// <summary>
        /// This method will update the Portals Assigned to the specific user
        /// </summary>
        /// <param name="userId">Guid User Id</param>
        /// <param name="portalIds">string ccomman separated Portal Ids</param>
        /// <returns>True if records updated in DB</returns>
        public bool UpdateRolesAndPermissions(Guid userId, string portalIds, string roleIds)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_InsertWebPagesProfile", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@UserId", userId);
                adapter.SelectCommand.Parameters.AddWithValue("@PortalIds", portalIds);
                adapter.SelectCommand.Parameters.AddWithValue("@RoleIds", roleIds);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                if (!Equals(dataSet, null) && !Equals(dataSet.Tables, null) && !Equals(dataSet.Tables[0], null))
                {
                    return true;
                }
                else
                { 
                    return false; 
                }
            }
        }
        /// <summary>
        /// This method will call the Stored proc which will return the data of associted portals for the user.
        /// </summary>
        /// <param name="userName">string  username</param>
        /// <returns>returns out data as per sp</returns>
        public DataSet GetProfileStoreAccess(string userName)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("webpage_GetProfileStoreAcess", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@UserName", userName);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }

        }

        /// <summary>
        /// Get available profiles for sku or categories.
        /// </summary>
        /// <param name="portalIds">portal ids.</param>
        /// <param name="skuId">sku id.</param>
        /// <param name="categoryId">category id.</param>
        /// <returns></returns>
        public DataSet GetAvailableProfileBySkuId(string portalIds, int skuId = 0, int categoryId = 0,int skuProfileEffectiveId=0, int categoryProfileId=0)
        {
            // Create instance of connection     
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetProfilenotAssociatedWithSKUAndCategory", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;

                adapter.SelectCommand.Parameters.AddWithValue("@PortalIds", portalIds);
                adapter.SelectCommand.Parameters.AddWithValue("@SkuId", skuId);
                adapter.SelectCommand.Parameters.AddWithValue("@CategoryId", categoryId);
                adapter.SelectCommand.Parameters.AddWithValue("@SkuProfileEffectiveID", skuProfileEffectiveId);
                adapter.SelectCommand.Parameters.AddWithValue("@CategoryProfileID", categoryProfileId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }


        /// <summary>
        /// This method will update the webpages profile details to the specific user
        /// </summary>
        /// <param name="oldUserId">Guid Current User Id</param>
        /// <param name="newUserId">Guid New User Id</param>
        /// <param name="portalId">string ccomman separated Portal Ids</param>
        /// <returns>True if records updated in DB</returns>
        public bool UpdateProfileUserId(Guid oldUserId, Guid newUserId, int portalId=0)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("webpage_profile_ModifyUser", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@OldUserId", oldUserId);
                adapter.SelectCommand.Parameters.AddWithValue("@NewUserId", newUserId);
                adapter.SelectCommand.Parameters.AddWithValue("@PortalId", portalId);
                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();
                return (!Equals(dataSet, null) && !Equals(dataSet.Tables, null) && !Equals(dataSet.Tables[0], null) && dataSet.Tables[0].Rows.Count > 0) ? true : false;
            }
        }


        /// <summary>
        /// Get Profiles Associated with Users.
        /// </summary>
        /// <param name="userName">String userName.</param>
        /// <returns>Returns the profiles associated with UserName.</returns>
        public DataSet GetProfilesAssociatedWithUsers(String userName)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetProfileDetails ", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@Username", userName);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }
    }
}
