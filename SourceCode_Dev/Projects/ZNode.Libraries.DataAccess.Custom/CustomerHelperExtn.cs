﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZNode.Libraries.DataAccess.Custom
{
    public partial class CustomerHelper
    {
        /// <summary>
        /// Insert multiple customer mapping with single accountid.
        /// </summary>
        /// <param name="accountId">account id</param>
        /// <param name="profileIds">profile id</param>
        /// <returns>True / False</returns>
        public bool InsertUserAssociatedCustomers(int accountId, string customerAccountIds)
        {
            int results = 0;
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlCommand command = new SqlCommand("PRFT_InsertMultipleCustomerUser", connection);

                // Mark the command as store procedure
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@AccountId", accountId);
                command.Parameters.AddWithValue("@CustomerId", customerAccountIds);

                // Open the connection 
                connection.Open();

                results = Convert.ToInt32(command.ExecuteScalar());

                // Release the resources
                command.Dispose();
                connection.Close();
            }
            // Return the result.
            return Convert.ToBoolean(results);
        }

        /// <summary>
        /// Delete all mapping of userAccountId.
        /// </summary>
        /// <param name="userAccountId">userAccountId</param>        
        /// <returns>True / False</returns>
        public bool DeleteAllUserMapping(int userAccountId)
        {
            int results = 0;
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlCommand command = new SqlCommand("PRFT_DeleteAllUserMapping", connection);

                // Mark the command as store procedure
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@UserAccountId", userAccountId);                

                // Open the connection 
                connection.Open();

                results = Convert.ToInt32(command.ExecuteScalar());

                // Release the resources
                command.Dispose();
                connection.Close();
            }
            // Return the result.
            return Convert.ToBoolean(results);
        }        
    }
}
