﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZNode.Libraries.DataAccess.Custom
{
    public class PRFTUpdatePriceAndInventoryHelper
    {
        public DataSet GetAllProductsFromZnode()
        {
            DataSet dataSet = new DataSet();

            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                try
                {
                    // Mark the command as store procedure
                    SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetProductList", connection);
                    adapter.SelectCommand.CommandType = CommandType.StoredProcedure;

                    connection.Open();
                    adapter.Fill(dataSet);

                    // Release the resources
                    adapter.Dispose();
                    connection.Close();

                }
                catch(Exception ex)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("Class-PRFTUpdatePriceAndInventoryHelper:PRFT_GetAllProducts - " + ex.Message);
                }
            }

            // Return the datadet.
            return dataSet;

        }

        public void UpdatePriceAndInventoryToZnode(DataTable dt)
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                try
                {
                    SqlCommand command = new SqlCommand("PRFT_InsertPriceInventory", connection);
                    connection.Open();

                    command.CommandType = CommandType.StoredProcedure;

                    // Add parameters to command object                
                    command.Parameters.AddWithValue("@IPI", dt);                    

                    command.ExecuteNonQuery();
                    connection.Close();                    

                }
                catch (Exception ex)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("Class-PRFTUpdatePriceAndInventoryHelper:PRFT_GetAllProducts - " + ex.Message);
                }
            }
        }
    }
}
