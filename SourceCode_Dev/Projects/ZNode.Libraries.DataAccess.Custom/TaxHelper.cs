using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace ZNode.Libraries.DataAccess.Custom
{
    /// <summary>
    /// Represents the TaxHelper class.
    /// </summary>
    public class TaxHelper
    {
        /// <summary>
        /// Get the coutry code by state code abbreviation.
        /// </summary>
        /// <param name="stateAbbr">State code abbreviation to get the country code dataset.</param>
        /// <returns>Returns the country code dataset.</returns>
        public DataSet GetCountyCodeByStatAbbr(string stateAbbr)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNODE_GETCOUNTYCODE", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@StateAbbr", stateAbbr);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Get the inclusive tax enabled items.
        /// </summary>   
        /// <returns>Returns the inclusive tax enabled items dataset.</returns>
        public DataTable GetInclusiveTaxRules()
        {
            // Create Instance for Connection Object
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create Instance for Adaptor Object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetInclusiveTaxRules", connection);

                // Mark the Select command as Stored procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;

                // Execute the Command
                connection.Open();

                // Create Datatable
                DataTable dt = new DataTable();

                // Fill the datatable
                adapter.Fill(dt);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                return dt;
            }
        }

        /// <summary>
        /// Get the active tax rules by portal Id.
        /// </summary>
        /// <param name="portalId">Portal Id to get the active tax rules.</param>
        /// <param name="countryCode">Country code to get the active tax rules.</param>
        /// <returns>Returns the active tax rules.</returns>
        public DataTable GetActiveTaxRulesByPortalId(int portalId, string countryCode)
        {
            // Create Instance for Connection Object
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create Instance for Adaptor Object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetActiveTaxRules", connection);

                // Mark the Select command as Stored procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;

                adapter.SelectCommand.Parameters.AddWithValue("@PortalId", portalId);
                adapter.SelectCommand.Parameters.AddWithValue("@CountryCode", countryCode);

                // Execute the Command
                connection.Open();

                // Create Datatable
                DataTable dt = new DataTable();

                // Fill the datatable
                adapter.Fill(dt);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                return dt;
            }
        }

        public string GetTaxClassIDByExternalID(string externalId)
        {
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                var adapter = new SqlDataAdapter("ZNode_GetTaxClassByExternalID", connection) { SelectCommand = { CommandType = CommandType.StoredProcedure } };

                adapter.SelectCommand.Parameters.AddWithValue("@externalId", externalId);

                connection.Open();
                var reader = adapter.SelectCommand.ExecuteReader(CommandBehavior.CloseConnection);

                reader.Read();
                string taxClassId = null;

                if (reader.HasRows)
                {
                    taxClassId = reader[0].ToString();
                }

                connection.Close();

                return taxClassId;
            }
        }

        public List<string> GetTaxClassIDsByFilters(string query, string[,] array)
        {
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                var taxClassIds = new List<string>();
                var adapter = new SqlDataAdapter(query, connection);
                for (var i = 0; i < array.GetLength(0); i++)
                {
                    if (array[i, 0].Equals("@p" + i))
                    {
                        adapter.SelectCommand.Parameters.AddWithValue(array[i, 0], (DateTime?)Convert.ToDateTime(array[i, 1]));
                    }
                    else
                    {
                        if (array[i, 1] == null)
                        {
                            adapter.SelectCommand.Parameters.AddWithValue(array[i, 0], DBNull.Value);
                        }
                        else
                        {
                            adapter.SelectCommand.Parameters.AddWithValue(array[i, 0], array[i, 1]);
                        }
                    }
                }
                connection.Open();
                var reader = adapter.SelectCommand.ExecuteReader(CommandBehavior.CloseConnection);
                while (reader.Read())
                {
                    taxClassIds.Add(reader[0].ToString());
                }

                connection.Close();

                return taxClassIds;
            }
        }

        public string GetTaxRuleIDByExternalID(string externalId)
        {
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                var adapter = new SqlDataAdapter("ZNode_GetTaxRuleByExternalID", connection) { SelectCommand = { CommandType = CommandType.StoredProcedure } };

                adapter.SelectCommand.Parameters.AddWithValue("@externalId", externalId);

                connection.Open();
                var reader = adapter.SelectCommand.ExecuteReader(CommandBehavior.CloseConnection);

                reader.Read();
                string taxRuleId = null;

                if (reader.HasRows)
                {
                    taxRuleId = reader[0].ToString();
                }

                connection.Close();

                return taxRuleId;
            }
        }

        public List<string> GetTaxRuleIDsByFilters(string query, string[,] array)
        {
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                var taxRuleIds = new List<string>();
                var adapter = new SqlDataAdapter(query, connection);
                for (var i = 0; i < array.GetLength(0); i++)
                {
                    if (array[i, 0].Equals("@p" + i))
                    {
                        adapter.SelectCommand.Parameters.AddWithValue(array[i, 0], (DateTime?)Convert.ToDateTime(array[i, 1]));
                    }
                    else
                    {
                        if (array[i, 1] == null)
                        {
                            adapter.SelectCommand.Parameters.AddWithValue(array[i, 0], DBNull.Value);
                        }
                        else
                        {
                            adapter.SelectCommand.Parameters.AddWithValue(array[i, 0], array[i, 1]);
                        }
                    }
                }
                connection.Open();
                var reader = adapter.SelectCommand.ExecuteReader(CommandBehavior.CloseConnection);
                while (reader.Read())
                {
                    taxRuleIds.Add(reader[0].ToString());
                }

                connection.Close();

                return taxRuleIds;
            }
        }

        public string GetTaxRuleTypeIDByExternalID(string externalId)
        {
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                var adapter = new SqlDataAdapter("ZNode_GetTaxRuleTypeByExternalID", connection) { SelectCommand = { CommandType = CommandType.StoredProcedure } };

                adapter.SelectCommand.Parameters.AddWithValue("@externalId", externalId);

                connection.Open();
                var reader = adapter.SelectCommand.ExecuteReader(CommandBehavior.CloseConnection);

                reader.Read();
                string taxRuleTypeId = null;

                if (reader.HasRows)
                {
                    taxRuleTypeId = reader[0].ToString();
                }

                connection.Close();

                return taxRuleTypeId;
            }
        }

        public List<string> GetTaxRuleTypeIDsByFilters(string query, string[,] array)
        {
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                var taxRuleTypeIds = new List<string>();
                var adapter = new SqlDataAdapter(query, connection);
                for (var i = 0; i < array.GetLength(0); i++)
                {
                    if (array[i, 0].Equals("@p" + i))
                    {
                        adapter.SelectCommand.Parameters.AddWithValue(array[i, 0], (DateTime?)Convert.ToDateTime(array[i, 1]));
                    }
                    else
                    {
                        if (array[i, 1] == null)
                        {
                            adapter.SelectCommand.Parameters.AddWithValue(array[i, 0], DBNull.Value);
                        }
                        else
                        {
                            adapter.SelectCommand.Parameters.AddWithValue(array[i, 0], array[i, 1]);
                        }
                    }
                }
                connection.Open();
                var reader = adapter.SelectCommand.ExecuteReader(CommandBehavior.CloseConnection);
                while (reader.Read())
                {
                    taxRuleTypeIds.Add(reader[0].ToString());
                }

                connection.Close();

                return taxRuleTypeIds;
            }
        }


        #region  Znode Version 8.0

        /// <summary>
        /// To get Active Tax Class by PortalId
        /// </summary>
        /// <param name="portalId">int PortalId</param>
        /// <returns>List of active tax class having portalId equal to supplied portalId or null</returns>
        public DataSet GetActiveTaxClassByPortalId(int portalId)
        {
            // Create Instance for Connection Object
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_NT_ZNodeActiveTaxClass_GetByPortalID", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;

                // Add parameters to command object                
                adapter.SelectCommand.Parameters.AddWithValue("@PortalID", portalId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Check Tax Type associated with tax 
        /// </summary>
        /// <param name="TaxTypeId">int TaxTypeId</param>
        /// <returns>Return true is associate with else return fale</returns>
        public bool CheckAssociatedTaxType(int TaxTypeId)
        {
            int result = 0;
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlCommand command = new SqlCommand("ZNode_CheckAssociatedZNodeTaxRuleType", connection);

                // Mark the command as store procedure
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@TaxRuleTypeID", TaxTypeId);

                // Open the connection 
                connection.Open();

                result = Convert.ToInt32(command.ExecuteScalar());

                // Release the resources
                command.Dispose();
                connection.Close();
            }
            // Return the result.
            return Convert.ToBoolean(result);
        }
        #endregion
    }
}
