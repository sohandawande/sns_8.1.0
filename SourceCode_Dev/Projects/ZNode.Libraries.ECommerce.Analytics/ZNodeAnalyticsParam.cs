﻿using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.ECommerce.Fulfillment;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Analytics
{
    /// <summary>
    /// Represents the ZNodeAnalyticsParam class inherited from ZNodeAnalyticsData.
    /// </summary>
    public class ZNodeAnalyticsParam : ZNodeAnalyticsData
    {
        private ZNodeUserAccount _UserAccount = new ZNodeUserAccount();
        private ZNodeOrderFulfillment _Order = new ZNodeOrderFulfillment();
        private ZNodeShoppingCart _ShoppingCart = new ZNodeShoppingCart();
        private bool _IsShoppingCartPage;
        private bool _IsAddressPage;
        private bool _IsCheckoutPage;
        private string _EmailCaptureContorlId = string.Empty;
        private string _BrowserFingerprintImageUrl = string.Empty;
        private string _EmailCaptureScript = string.Empty;

        /// <summary>
        /// Gets or sets the ZNodeUserAccount object.
        /// </summary>
        public ZNodeUserAccount UserAccount
        {
            get { return this._UserAccount; }
            set { this._UserAccount = value; }
        }

        /// <summary>
        /// Gets or sets the ZNodeOrderFulfillment object.
        /// </summary>
        public ZNodeOrderFulfillment Order
        {
            get { return this._Order; }
            set { this._Order = value; }
        }

        /// <summary>
        /// Gets or sets the shopping cart page.
        /// </summary>
        public ZNodeShoppingCart ShoppingCart
        {
            get { return this._ShoppingCart; }
            set { this._ShoppingCart = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the page is shopping cart.
        /// </summary>
        public bool IsShoppingCartPage
        {
            get { return this._IsShoppingCartPage; }
            set { this._IsShoppingCartPage = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the page is edit address.
        /// </summary>
        public bool IsAddressPage
        {
            get { return this._IsAddressPage; }
            set { this._IsAddressPage = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the page is checkout.
        /// </summary>
        public bool IsCheckoutPage
        {
            get { return this._IsCheckoutPage; }
            set { this._IsCheckoutPage = value; }
        }

        /// <summary>
        /// Gets or sets the email control Id in new user registraion page or in checkout without registraion page.
        /// </summary>
        public string EmailCaptureContorlId
        {
            get { return this._EmailCaptureContorlId; }
            set { this._EmailCaptureContorlId = value; }
        }

        /// <summary>
        /// Gets or sets the email capture script.
        /// </summary>
        public string EmailCaptureScript
        {
            get
            {
                return this._EmailCaptureScript;
            }

            set
            {
                this._EmailCaptureScript = value;
            }
        }

        /// <summary>
        /// Gets or sets the listrak browser fingerprint image url.
        /// </summary>
        public string BrowserFingerprintImageUrl
        {
            get
            {
                return this._BrowserFingerprintImageUrl;
            }

            set
            {
                this._BrowserFingerprintImageUrl = value;
            }
        }
    }
}
