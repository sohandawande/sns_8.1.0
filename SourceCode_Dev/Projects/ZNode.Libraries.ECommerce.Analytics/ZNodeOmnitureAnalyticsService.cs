﻿using System;
using System.Text;
using System.Linq;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.DataAccess.Service;

namespace ZNode.Libraries.ECommerce.Analytics
{
    /// <summary>
    /// Provides Analytics code specific to Omniture that will put javascript on selected pages.
    /// </summary>
    class ZNodeOmnitureAnalyticsService : ZNodeAnalyticsService
    {
        #region Public Methods

        /// <summary>
        /// Gets the order receipt page analytics javascript.
        /// </summary>
        public override string OrderReceiptJavascript
        {
            get { return this.GetOrderReceiptJavascript(); }
        }

        #endregion

        #region Private Helper Methods

        protected string GetOrderReceiptJavascript()
        {
            StringBuilder sb = new StringBuilder(string.Empty);
            ZNodeAnalyticsData analytics = new ZNodeAnalyticsData();
            
            // Build the additional Omniture Asynchronous Javascript for the order receipt page. 
            // <script language="JavaScript" type="text/javascript">   
            sb.Append("<script language=\"JavaScript\" type=\"text/javascript\" src=\"/js/s_code.js\"></script>");

            sb.Append("<script language=\"JavaScript\" type=\"text/javascript\">");
            sb.Append('\n');

            sb.Append("s.pageName=\"Check_out_Products\"");
            sb.Append('\n');

            sb.Append("s.server=\"\"");
            sb.Append('\n');

            sb.Append("s.channel=\"\"");
            sb.Append('\n');

            sb.Append("s.pageType=\"\"");
            sb.Append('\n');

            sb.Append("s.prop1=\"\"");
            sb.Append('\n');

            sb.Append("s.prop2=\"\"");
            sb.Append('\n');

            sb.Append("s.prop3=\"\"");
            sb.Append('\n');

            sb.Append("s.prop4=\"\"");
            sb.Append('\n');

            sb.Append("s.prop5=\"\"");
            sb.Append('\n');

            sb.Append("s.campaign=\"\"");
            sb.Append('\n');

            sb.Append("s.state=");
            sb.Append(CreateQuotedString(Order.BillingAddress.StateCode));
            sb.Append("");
            sb.Append('\n');

            sb.Append("s.zip=\"\"");
            sb.Append('\n');

            sb.Append("s.events=\"purchase\"");
            sb.Append('\n');
            //var skuInventoryIds = Order.OrderLineItems.Select(x => x.SKUInventoryID);
            SKUInventoryService srvc = new SKUInventoryService();
            //var skuInventoryList = srvc.Find(string.Format("SKUInventoryID IN ({0})", string.Join(",", skuInventoryIds)));
            foreach(OrderLineItem orderLineItem in Order.OrderLineItems)
            {
                string orderid = orderLineItem.OrderID.ToString();
                string name = orderLineItem.Name;
                string sku = srvc.GetBySKU(orderLineItem.SKU).SKU;
                string quantity = orderLineItem.Quantity.ToString();
                string price = ((decimal) orderLineItem.Price).ToString();
                string products = string.Format("{0}, {1}, {2}, {3}, {4}", orderid, name, sku, quantity, price);

                sb.Append("products=");

                sb.Append(CreateQuotedString(string.Format("Sale;{0};{1}", products, (decimal)Order.Total)));
                sb.Append('\n');

                sb.Append("purchaseID=");
                sb.Append(CreateQuotedString(orderid));
                sb.Append('\n');

                sb.Append("s.eVar1=");
                sb.Append(CreateQuotedString(sku));
                sb.Append('\n');

                sb.Append("s.eVar2=");
                sb.Append(CreateQuotedString(name));
                sb.Append('\n');

                sb.Append("s.eVar3=");
                sb.Append(CreateQuotedString(quantity));
                sb.Append('\n');

                sb.Append("s.eVar4=");
                sb.Append(CreateQuotedString(price));
                sb.Append('\n');
            }

            sb.Append("s.eVar5=");
                sb.Append(CreateQuotedString(((decimal)Order.TaxCost).ToString()));
                sb.Append('\n');

            sb.Append("s.eVar6=");
                sb.Append(CreateQuotedString(((decimal)Order.ShippingCost).ToString()));
                sb.Append('\n');

            sb.Append("s.eVar7=");
                sb.Append(CreateQuotedString(Order.OrderID.ToString()));
                sb.Append('\n');

            sb.Append("s.eVar8=");
                sb.Append(CreateQuotedString(Order.BillingAddress.City.ToString()));
                sb.Append('\n');

            sb.Append("s.eVar9=");
                sb.Append(CreateQuotedString(Order.BillingAddress.StateCode.ToString()));
                sb.Append('\n');

            sb.Append("s.eVar10=");
                sb.Append(CreateQuotedString(Order.BillingAddress.CountryCode.ToString()));
                sb.Append('\n');

            // Submits transaction to the Analytics servers
            sb.Append("</script>");

            sb.Append(analytics.OrderReceiptJavascript);
            return sb.ToString();                        
        }
        #endregion
    }
}
