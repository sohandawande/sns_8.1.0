using System;
using System.Web;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Analytics
{
    /// <summary>
    /// Provides functions for tracking visitors referrecd from affiliate.
    /// </summary>
    public class ZNodeTracking : ZNodeTrackingBase
    {
        #region Private Member Variables
        private int _AccountId = 0;
        private int _OrderId = 0;
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the account Id.
        /// </summary>
        public int AccountID
        {
            get
            {
                return this._AccountId;
            }

            set
            {
                this._AccountId = value;
            }
        }

        /// <summary>
        /// Gets or sets the order Id.
        /// </summary>
        public int OrderID
        {
            get
            {
                return this._OrderId;
            }

            set
            {
                this._OrderId = value;
            }
        }

        #endregion

        /// <summary>
        /// Load the tracking event.
        /// </summary>
        /// <param name="eventName">Tracking event name.</param>
        public void LogTrackingEvent(string eventName)
        {
            LogTrackingEvent(eventName, string.Empty);
        }

        /// <summary>
        /// Load the tracking event.
        /// </summary>
        /// <param name="eventName">Tracking event name.</param>
        /// <param name="currentURL">Current domain URL.</param>
        public void LogTrackingEvent(string eventName, string currentURL)
        {
            try
            {
                // If a tracking ID doesn't exists and the affiliate ID is set then we can start tracking this visitor.
                if (this._TrackingId.Length == 0 && this.AffiliateId.Length > 0)
                {
                    this.LogFirstVisit(currentURL);
                }

                // If a tracking ID already exists then we can log the event.
                int trackingId;
                if (int.TryParse(this._TrackingId, out trackingId))
                {
                    if (trackingId > 0)
                    {
                        TrackingEvent trackingEventInfo = new TrackingEvent();

                        trackingEventInfo.Date = System.DateTime.Now;
                        trackingEventInfo.SafeNameEvent = eventName;

                        if (this.AccountID > 0)
                        {
                            trackingEventInfo.AccountID = this.AccountID;
                        }

                        if (this.OrderID > 0)
                        {
                            trackingEventInfo.OrderID = this.OrderID;
                        }

                        if (!Equals(HttpContext.Current.Request.UrlReferrer, null))
                        {
                            trackingEventInfo.RefererDomain = HttpContext.Current.Request.UrlReferrer.DnsSafeHost;
                            trackingEventInfo.RefererQuery = HttpContext.Current.Request.UrlReferrer.Query;
                        }
                        else
                        {
                            trackingEventInfo.RefererDomain = HttpContext.Current.Request.UserHostAddress;
                        }

                        trackingEventInfo.TrackingID = trackingId;
                        TrackingEventService trackingService = new TrackingEventService();

                        trackingService.Insert(trackingEventInfo);
                    }
                }
            }
            catch (Exception e)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(e.Message);
            }
        }

        /// <summary>
        /// This method inserts a Tracking entity object into the datasource
        /// </summary>
        /// <param name="currentURL">Current domain URL.</param>
        /// <returns>Returns true if the operation is successful.</returns>
        private bool LogFirstVisit(string currentURL)
        {
            Tracking trackingInfo = new Tracking();

            // get default profile
            ProfileService ProfileService = new ProfileService();
            Domain currentDomain = string.IsNullOrEmpty(currentURL) ? ZNodeConfigManager.DomainConfig : ZNodeConfigManager.GetDomainConfig(ZNodeConfigManager.CurrentDomain(currentURL));
            Profile profile = null;

            // Get DefaultRegistered Profile
            if (!Equals(currentDomain, null))
            {
                PortalService portalService = new PortalService();
                Portal currentPortal = portalService.GetByPortalID(currentDomain.PortalID);
                profile = ProfileService.GetByProfileID(currentPortal.DefaultAnonymousProfileID.GetValueOrDefault());
            }

            int affiliateId = 0;
            int.TryParse(this.AffiliateId, out affiliateId);

            trackingInfo.AffiliateId = affiliateId;
            trackingInfo.CampaignId = this.CampaignId;
            trackingInfo.CampaignKeyword = this.CampaignKeyword;
            trackingInfo.CampaignSource = this.CampaignSource;
            trackingInfo.Custom1 = this.Custom1;
            trackingInfo.Custom2 = this.Custom2;
            trackingInfo.Custom3 = this.Custom3;
            trackingInfo.PortalID = ZNodeConfigManager.SiteConfig.PortalID;
            trackingInfo.DestinationDomain = currentDomain.DomainName;
            trackingInfo.ProfileID = profile.ProfileID;
            trackingInfo.Date = System.DateTime.Now;
            trackingInfo.RefererQuery = this.RefererQuery;

            // If we can't figure out the referrer use the users IP address.
            if (this.RefererDomain.Length > 0)
            {
                trackingInfo.RefererDomain = this.RefererDomain;
            }
            else
            {
                trackingInfo.RefererDomain = currentURL;
            }

            // Write out the info to the database and store the tracking ID.
            TrackingService trackingService = new TrackingService();

            bool isAdded = trackingService.Insert(trackingInfo);

            if (isAdded)
            {
                this._TrackingId = trackingInfo.TrackingID.ToString();

                // set session variable
                HttpContext.Current.Session[ZNodeTrackingBase.SessionVarNameTrackingId] = this._TrackingId;

                // Write cookie
                HttpContext.Current.Response.Cookies[ZNodeTrackingBase.CookieNameTrackingId].Value = this._TrackingId;
                HttpContext.Current.Response.Cookies[ZNodeTrackingBase.CookieNameTrackingId].Expires = System.DateTime.Now.AddMonths(12);
            }

            return isAdded;
        }

        /// <summary>
        /// This method inserts a Tracking entity object into the datasource
        /// </summary>
        /// <param name="TrackingInfo">Tracking entity object to add.</param>
        /// <returns>Returns true if the operation is successful</returns>
        private bool LogTrackingInfo(Tracking TrackingInfo)
        {
            TrackingService trackingService = new TrackingService();

            bool isAdded = trackingService.Insert(TrackingInfo);

            if (isAdded)
            {
                this._TrackingId = TrackingInfo.TrackingID.ToString();

                // set session variable
                HttpContext.Current.Session[ZNodeTrackingBase.SessionVarNameTrackingId] = this._TrackingId;

                // Write cookie
                HttpContext.Current.Response.Cookies[ZNodeTrackingBase.CookieNameTrackingId].Value = this._TrackingId;
                HttpContext.Current.Response.Cookies[ZNodeTrackingBase.CookieNameTrackingId].Expires = System.DateTime.Now.AddMonths(12);
            }

            return isAdded;
        }
    }
}
