using System.Collections.Generic;
using System.Text;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Analytics
{
    /// <summary>
    /// Provides a central place to manage how Analytics code is placed on a page.
    /// </summary>
    /// <remarks>
    /// The analytics capability requires that you derive your pages from ZNodeTemplate. 
    /// Because of the way that the ZNodeTemplate adds analytics code to the page (RegisterClientScriptBlock and RegisterStartupScript)
    /// you need to instantiate this class during the Page_Init event.    
    /// YOU ONLY NEED TO INSTANTIATE THIS CLASS IF YOU WANT TO OVERRIDE THE DEFAULT MESSAGES SET IN THE DATABASE.
    /// </remarks>
    public class ZNodeAnalytics : ZNodeBusinessBase
    {
        #region Private Member Variables
        private ZNodeAnalyticsParam _AnalyticsData = new ZNodeAnalyticsParam();
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the analytics data that will be used to write to the page. Defaults to the values set in the database.
        /// </summary>
        public ZNodeAnalyticsParam AnalyticsData
        {
            get { return this._AnalyticsData; }
            set { this._AnalyticsData = value; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Binds the analytics service code to the page. You only need to instantiate this class and call this function
        /// if you want to override the default behavior of the analytics. This call must be made in the Page_Init.
        /// </summary>        
        /// <remarks>
        /// If you need to add new analytics services then this is the place to instantiate them.
        /// </remarks>
        public void Bind()
        {
            List<ZNodeAnalyticsService> analyticsServices = new List<ZNodeAnalyticsService>();
            StringBuilder siteWideTopJavascript = new StringBuilder(string.Empty);
            StringBuilder siteWideBottomJavascript = new StringBuilder(string.Empty);
            StringBuilder siteWideAnalyticsJavascript = new StringBuilder(string.Empty);
            StringBuilder orderReceiptJavascript = new StringBuilder(string.Empty);

            // INSTANTIATE THE ANALTYICS CODE THAT IS SPECIFIC TO YOUR SERVICE. 
            // Add any other analytics services as needed.            
            analyticsServices.Add(new ZNodeGoogleAnalyticsService());
            analyticsServices.Add(new ZNodeListrakAnalyticsService());
            analyticsServices.Add(new ZNodeOmnitureAnalyticsService());

            // Set the analytics code for each service.
            foreach (ZNodeAnalyticsService service in analyticsServices)
            {
                siteWideTopJavascript.Append(service.SiteWideTopJavascript);
                siteWideBottomJavascript.Append(service.SiteWideBottomJavascript);
                siteWideAnalyticsJavascript.Append(service.SiteWideAnalyticsJavascript);

                // Add the script specific to shopping cart page
                if (this.AnalyticsData.IsShoppingCartPage)
                {
                    if (this.AnalyticsData.ShoppingCart != null && this.AnalyticsData.ShoppingCart.ShoppingCartItems.Count > 0)
                    {
                        service.ShoppingCart = this.AnalyticsData.ShoppingCart;
                        siteWideBottomJavascript.Append(service.ShoppingCartJavaScript);
                    }
                }

                if (this.AnalyticsData.UserAccount != null)
                {
                    if (this.AnalyticsData.IsAddressPage)
                    {
                        // Add the script specific to checkout address edit page
                        service.UserAccount = this.AnalyticsData.UserAccount;
                        siteWideBottomJavascript.Append(service.AddressJavaScript);
                    }

                    if (this.AnalyticsData.IsCheckoutPage)
                    {
                        // Add the script specific to checkout page
                        service.UserAccount = this.AnalyticsData.UserAccount;
                        siteWideBottomJavascript.Append(service.CheckoutJavaScript);
                    }
                }

                // Add the script specific to order receipt page
                if (this.AnalyticsData.IsOrderReceiptPage)
                {
                    if (this.AnalyticsData.Order != null && this.AnalyticsData.Order.OrderID > 0)
                    {
                        service.Order = this.AnalyticsData.Order;
                        orderReceiptJavascript.Append(service.OrderReceiptJavascript);
                    }                    
                }

                // Get the email capture script.
                service.EmailCaptureContorlId = this.AnalyticsData.EmailCaptureContorlId;
                if (service.EmailCaptureContorlId.Trim().Length > 0)
                {
                    this.AnalyticsData.EmailCaptureScript = service.EmailCaptureScript;
                }

                // Get the browser finger print script.
                this.AnalyticsData.BrowserFingerprintImageUrl = service.BrowserFingerprintImageUrl;
            }

            ZNodeAnalyticsData analytics = new ZNodeAnalyticsData();
            analytics.SiteWideTopJavascript = siteWideTopJavascript.ToString();
            analytics.SiteWideBottomJavascript = siteWideBottomJavascript.ToString();
            analytics.SiteWideAnalyticsJavascript = siteWideAnalyticsJavascript.ToString();

            if (this.AnalyticsData.Order != null && this.AnalyticsData.Order.OrderID > 0)
            {
                analytics.OrderReceiptJavascript = orderReceiptJavascript.ToString();
            }
        }
        #endregion
    }
}
