using System.Text;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.ECommerce.Fulfillment;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Analytics
{
    /// <summary>
    /// Provides generic Analitics services to place javascript on selected pages.
    /// </summary>
    public class ZNodeAnalyticsService : ZNodeBusinessBase
    {
        private ZNodeAnalyticsData _AnalyticsData = new ZNodeAnalyticsData();
        private ZNodeUserAccount _UserAccount = new ZNodeUserAccount();
        private ZNodeOrderFulfillment _Order = new ZNodeOrderFulfillment();
        private ZNodeShoppingCart _ShoppingCart = new ZNodeShoppingCart();
        private string _EmailCaptureContorlId = string.Empty;

        #region Public Properties
        /// <summary>
        /// Gets or sets the ZNodeUserAccount object.
        /// </summary>
        public virtual ZNodeUserAccount UserAccount
        {
            get { return this._UserAccount; }
            set { this._UserAccount = value; }
        }

        /// <summary>
        /// Gets or sets the ZNodeOrderFulfillment object.
        /// </summary>
        public virtual ZNodeOrderFulfillment Order
        {
            get { return this._Order; }
            set { this._Order = value; }
        }

        /// <summary>
        /// Gets or sets the shopping cart page.
        /// </summary>
        public virtual ZNodeShoppingCart ShoppingCart
        {
            get { return this._ShoppingCart; }
            set { this._ShoppingCart = value; }
        }

        /// <summary>
        /// Gets the Javascript that will be placed on the top of each page.
        /// </summary>
        public virtual string SiteWideTopJavascript
        {
            get
            {
                return this._AnalyticsData.SiteWideTopJavascript;
            }
        }

        /// <summary>
        /// Gets the Javascript that will be placed on the bottom of each page.
        /// </summary>
        public virtual string SiteWideBottomJavascript
        {
            get
            {
                return this._AnalyticsData.SiteWideBottomJavascript;
            }
        }

        /// <summary>
        /// Gets the Javascript that will be placed on the bottom of each page EXCEPT the receipt.
        /// </summary>
        public virtual string SiteWideAnalyticsJavascript
        {
            get
            {
                return this._AnalyticsData.SiteWideAnalyticsJavascript;
            }
        }

        /// <summary>
        /// Gets the Javascript for the shopping cart page (Step - 1).
        /// </summary>
        public virtual string ShoppingCartJavaScript
        {
            get
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the Javascript for the address page of the checkout (Step - 2).
        /// </summary>
        public virtual string AddressJavaScript
        {
            get
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the Javascript for the checkout page (Step - 3).
        /// </summary>
        public virtual string CheckoutJavaScript
        {
            get
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the Javascript for the order receipt page (Step - 4).
        /// </summary>
        public virtual string OrderReceiptJavascript
        {
            get
            {
                return this._AnalyticsData.OrderReceiptJavascript;
            }
        }

        /// <summary>
        /// Gets or sets the email control Id in new user registraion page or in checkout without registraion page.
        /// </summary>
        public virtual string EmailCaptureContorlId
        {
            get { return this._EmailCaptureContorlId; }
            set { this._EmailCaptureContorlId = value; }
        }

        /// <summary>
        /// Gets the browser finderprint image url for order receipt page.
        /// </summary>
        public virtual string EmailCaptureScript
        {
            get { return string.Empty; }
        }

        /// <summary>
        /// Gets the listrak browser fingerprint image url.
        /// </summary>
        public virtual string BrowserFingerprintImageUrl
        {
            get { return string.Empty; }
        }
        #endregion

        #region Protected Helper Methods
        /// <summary>
        /// Helper method to create a formatted string with quotes
        /// </summary>
        /// <param name="inputString">The string to clean and create quoted string.</param>
        /// <returns>Returns the quoted string.</returns>
        protected string CreateQuotedString(string inputString)
        {
            string cleanString = this.SanitizeString(inputString);
            StringBuilder sb = new StringBuilder();
            sb.Append("\"");
            sb.Append(cleanString);
            sb.Append("\"");

            return sb.ToString();
        }

        /// <summary>
        /// Remove the quote string with empty string.
        /// </summary>
        /// <param name="inputString">String value to sanitize.</param>
        /// <returns>Returns the sanitized string.</returns>
        protected string SanitizeString(string inputString)
        {
            inputString = inputString.Replace("\"", string.Empty);
            inputString = inputString.Replace("\'", string.Empty);

            return inputString.Trim();
        }
        #endregion
    }
}
