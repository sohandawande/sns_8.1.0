using System.Globalization;
using System.Text;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Analytics
{
    /// <summary>
    /// Provides Analytics code specific to Google that will put javascript on selected pages.
    /// </summary>
    public class ZNodeGoogleAnalyticsService : ZNodeAnalyticsService
    {
        #region Public Methods

        /// <summary>
        /// Gets the order receipt page analytics javascript.
        /// </summary>
        public override string OrderReceiptJavascript
        {
            get { return this.GetOrderReceiptJavascript(); }
        }
        #endregion

        #region Private Helper Methods
        /// <summary>
        /// Injects the ecommerce code in the checkout page
        /// </summary>
        /// <returns>Returns the order receipt page analytics script.</returns>
        protected string GetOrderReceiptJavascript()
        {
            StringBuilder sb = new StringBuilder(string.Empty);
            ZNodeAnalyticsData analytics = new ZNodeAnalyticsData();

            // Only output the cart information if the Google Analytics has been specified.
            if (analytics.IsOrderReceiptPage)
            {
                if (analytics.SiteWideBottomJavascript.Contains("google-analytics.com") || analytics.SiteWideTopJavascript.Contains("google-analytics.com"))
                {
                    // Build the additional Google Asynchronous Javascript for the order receipt page.                    
                    sb.Append("<script type='text/javascript'>");
                    sb.Append("_gaq.push(['_addTrans',");

                    sb.Append(CreateQuotedString(Order.OrderID.ToString()));
                    sb.Append(", ");

                    sb.Append(CreateQuotedString(Order.BillingAddress.FirstName + " " + Order.BillingAddress.LastName));
                    sb.Append(", ");

                    sb.Append(CreateQuotedString(((decimal)Order.Total).ToString("N2")));
                    sb.Append(", ");

                    sb.Append(CreateQuotedString(((decimal)Order.TaxCost).ToString("N2")));
                    sb.Append(", ");

                    sb.Append(CreateQuotedString(((decimal)Order.ShippingCost).ToString("N2")));
                    sb.Append(", ");

                    sb.Append(CreateQuotedString(Order.BillingAddress.City));
                    sb.Append(", ");

                    sb.Append(CreateQuotedString(Order.BillingAddress.StateCode));
                    sb.Append(", ");

                    sb.Append(CreateQuotedString(Order.BillingAddress.CountryCode));

                    sb.Append("]);");

                    // Append order line items
                    foreach (OrderLineItem orderLineItem in Order.OrderLineItems)
                    {
                        sb.Append(" _gaq.push(['_addItem',");

                        sb.Append(CreateQuotedString(orderLineItem.OrderID.ToString()));
                        sb.Append(", ");

                        sb.Append(CreateQuotedString(orderLineItem.SKU));
                        sb.Append(", ");

                        sb.Append(CreateQuotedString(orderLineItem.Name));
                        sb.Append(", ");

                        sb.Append(CreateQuotedString("Product"));
                        sb.Append(", ");

                        sb.Append(CreateQuotedString(((decimal)orderLineItem.Price).ToString("N2")));
                        sb.Append(", ");

                        sb.Append(CreateQuotedString(orderLineItem.Quantity.ToString()));

                        sb.Append("]); ");
                    }

                    // Submits transaction to the Analytics servers
                    sb.Append("_gaq.push(['_trackTrans']); ");

                    sb.Append("</script>");
                }

                sb.Append(analytics.OrderReceiptJavascript);
            }

            return sb.ToString();
        }
        #endregion

        #region PRFT Custom Code
        /// <summary>
        /// Get Ecommerce Tracking Javascript for Order Receipt Page.
        /// </summary>
        /// <param name="order"></param>
        /// <returns>Order receipt javascript string.</returns>
        public string GetOrderReceiptEcommerceTrackingJavascriptCode(Order order)
        {

            int count = 0;
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<script type='text/javascript'>");
            sb.Append("dataLayer = [{");
            sb.Append("'transactionId':");
            //if (!string.IsNullOrEmpty(order.ExternalID))
            //{
            //    sb.Append("'" + order.ExternalID + "',");
            //}
            //else
            //{
            //    sb.Append("'" + order.OrderID + "',");
            //}
            sb.Append("'" + order.OrderID + "',");
            //sb.Append("'transactionAffiliation':");
            //sb.Append("'NA',");
            sb.Append("'transactionTotal':");
            sb.Append(order.Total.Value.ToString("0.00", CultureInfo.InvariantCulture) + ",");
            sb.Append("'transactionTax':");
            sb.Append(order.TaxCost.Value.ToString("0.00", CultureInfo.InvariantCulture) + ",");
            sb.Append("'transactionShipping':");
            sb.Append(order.ShippingCost.Value.ToString("0.00", CultureInfo.InvariantCulture) + ",");
            sb.Append("'transactionPromoCode':");
            sb.Append("'" + order.CouponCode + "',");
            sb.Append("'transactionProducts': [");

            if (order.OrderLineItemCollection != null)
            {
                int orderLineItemsCount = order.OrderLineItemCollection.Count;
                var productService = new ProductService();
                var categoryService = new CategoryService();

                foreach (OrderLineItem item in order.OrderLineItemCollection)
                {
                    sb.Append("{");
                    sb.Append("'sku':");
                    sb.Append("'" + item.SKU + "',");
                    sb.Append("'name':");
                    sb.Append("'" + item.Name.Replace("'", "&#39;") + "',");
                    sb.Append("'category':");
                    sb.Append("'#categoryname" + item.OrderLineItemID + "#',");                    
                    sb.Append("'price':");
                    decimal orderLineItemPrice = 0;
                    if (item.Price > 0)
                    {
                        orderLineItemPrice = (decimal)item.Price;
                    }
                    sb.Append(orderLineItemPrice.ToString("0.00", CultureInfo.InvariantCulture) + ",");
                    sb.Append("'quantity':");
                    sb.Append(item.Quantity);
                    count++;
                    if (count == orderLineItemsCount)
                    {
                        sb.Append("}");
                    }
                    else
                    {
                        sb.Append("},");
                    }

                }
            }
            sb.Append("]");
            sb.Append("}];");
            sb.AppendLine("</script>");
            return sb.ToString();
        }
        #endregion
    }
}