﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Services
{
    public interface ICustomerUserMappingService
    {
        /// <summary>
        /// Delete Existing Account Profile.
        /// </summary>
        /// <param name="customerUserMappingId">customerUserMappingId</param>
        /// <returns>True / False</returns>
        bool DeleteUserAssociatedCustomer(int customerUserMappingId);

        /// <summary>
        /// Save User Associated Customers.
        /// </summary>
        /// <param name="accountId">account id</param>
        /// <param name="customerAccountIds">profile Ids</param>
        /// <returns>True / false when record is inserted</returns>
        bool InsertUserAssociatedCustomer(int accountId, string customerAccountIds);

        bool DeleteAllUserMapping(int userAccountId);
    }
}
