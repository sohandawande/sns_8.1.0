﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;
using Znode.Libraries.Helpers.Extensions;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using DiscountTypeRepository = ZNode.Libraries.DataAccess.Service.DiscountTypeService;
using PromotionRepository = ZNode.Libraries.DataAccess.Service.PromotionService;
using PortalRepository = ZNode.Libraries.DataAccess.Service.PortalService;
using ProfileRepository = ZNode.Libraries.DataAccess.Service.ProfileService;
using SkuRepository = ZNode.Libraries.DataAccess.Service.SKUService;
using Znode.Libraries.Helpers;
using ZNode.Libraries.DataAccess.Custom;
using System.Data;
using Znode.Libraries.Helpers.Constants;
using ZNode.Libraries.Admin;

namespace Znode.Engine.Services
{
    public class PromotionService : BaseService, IPromotionService
    {
        private readonly PromotionRepository _promotionRepository;
        private readonly DiscountTypeRepository _discountTypeRepository;
        private readonly PortalRepository _portalTypeRepository;
        private readonly ProfileRepository _profileRepository;
        private readonly SkuRepository _SKURepository;
        private readonly PromotionAdmin _promotionAdmin;

        public PromotionService()
        {
            _discountTypeRepository = new DiscountTypeRepository();
            _promotionRepository = new PromotionRepository();
            _portalTypeRepository = new PortalRepository();
            _profileRepository = new ProfileRepository();
            _SKURepository = new SkuRepository();
            _promotionAdmin = new PromotionAdmin();
        }

        public PromotionModel GetPromotion(int promotionId, NameValueCollection expands)
        {
            var promotion = _promotionRepository.GetByPromotionID(promotionId);
            if (promotion != null)
            {
                GetExpands(expands, promotion);
            }

            return PromotionMap.ToModel(promotion);
        }

        public PromotionListModel GetPromotions(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new PromotionListModel();
            var promotions = new TList<Promotion>();
            var tempList = new TList<Promotion>();

            var query = new PromotionQuery();
            var sortBuilder = new PromotionSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            tempList = _promotionRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                model.PageSize = model.TotalResults;
            }

            // Now go through the list and get any expands
            foreach (var promotion in tempList)
            {
                GetExpands(expands, promotion);
                promotions.Add(promotion);
            }

            // Map each item and add to the list
            foreach (var p in promotions)
            {
                model.Promotions.Add(PromotionMap.ToModel(p));
            }

            return model;
        }

        public ProductListModel GetProductsList(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            ProductListModel list = new ProductListModel();
            string whereClause = StringHelper.GenerateDynamicWhereClause(filters);
            int totalRowCount = 0;
            var pagingStart = 0;
            var pagingLength = 0;
            string orderBy = StringHelper.GenerateDynamicOrderByClause(sorts);

            pagingStart = Convert.ToInt32(page.Get(PageKeys.Index));
            pagingLength = Convert.ToInt32(page.Get(PageKeys.Size));

            StoredProcedureHelper storedProcedureHelper = new StoredProcedureHelper();
            DataSet resultDataSet = storedProcedureHelper.SP_DataDetailsSpConfig(whereClause, string.Empty/*HavingClause*/, StoredProcedureKeys.SPGetPromotionProduct, orderBy, pagingStart.ToString(), pagingLength.ToString(), out totalRowCount);

            list.Products = resultDataSet.Tables[0].ToList<ProductModel>().ToCollection();
            list.TotalResults = totalRowCount;
            list.PageSize = pagingLength;
            list.PageIndex = pagingStart;
            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                list.PageSize = list.TotalResults;
            }
            return list;
        }

        public PromotionModel CreatePromotion(PromotionModel model)
        {
            if (Equals(model, null))
            {
                throw new Exception("Promotion model cannot be null.");
            }

            var entity = PromotionMap.ToEntity(model);

            if (this.IsDuplicateCouponCode(model))
            {
                throw new Exception("This promoiton already exists.");
            }
            else if (this.IsDuplicatePromotion(entity))
            {
                throw new Exception("This promoiton already exists.");
            }

            var promotion = _promotionRepository.Save(entity);
            return PromotionMap.ToModel(promotion);
        }

        public PromotionModel UpdatePromotion(int promotionId, PromotionModel model)
        {
            if (promotionId < 1)
            {
                throw new Exception("Promotion ID cannot be less than 1.");
            }

            if (model == null)
            {
                throw new Exception("Promotion model cannot be null.");
            }
            else if (this.IsDuplicateCouponCode(model))
            {
                throw new Exception("This coupon code already exists.");
            }
            var promotion = _promotionRepository.GetByPromotionID(promotionId);
            if (promotion != null)
            {
                // Set the promotion ID and map
                model.PromotionId = promotionId;
                var promotionToUpdate = PromotionMap.ToEntity(model);

                var updated = _promotionRepository.Update(promotionToUpdate);
                if (updated)
                {
                    promotion = _promotionRepository.GetByPromotionID(promotionId);
                    return PromotionMap.ToModel(promotion);
                }
            }

            return null;
        }

        public bool DeletePromotion(int promotionId)
        {
            if (promotionId < 1)
            {
                throw new Exception("Promotion ID cannot be less than 1.");
            }

            var promotion = _promotionRepository.GetByPromotionID(promotionId);
            if (promotion != null)
            {
                return _promotionRepository.Delete(promotion);
            }

            return false;
        }

        private void GetExpands(NameValueCollection expands, Promotion promotion)
        {
            if (expands.HasKeys())
            {
                ExpandPromotionType(expands, promotion);
                ExpandPortal(expands, promotion);
                ExpandProfile(expands, promotion);
            }
        }

        private void ExpandPromotionType(NameValueCollection expands, Promotion promotion)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.PromotionType)))
            {
                var promotionType = _discountTypeRepository.GetByDiscountTypeID(promotion.DiscountTypeID);
                if (promotionType != null)
                {
                    promotion.DiscountTypeIDSource = promotionType;
                }
            }
        }

        /// <summary>
        /// This method gives the expand for portal
        /// </summary>
        /// <param name="expands">expands</param>
        /// <param name="promotion">promotion</param>
        private void ExpandPortal(NameValueCollection expands, Promotion promotion)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Portal)))
            {
                if (promotion.PortalID.HasValue)
                {
                    var portals = _portalTypeRepository.GetByPortalID(promotion.PortalID.Value);
                    if (!Equals(portals, null))
                    {
                        promotion.PortalIDSource = portals;
                    }
                }
            }
        }

        /// <summary>
        /// This method gives the expand for profile
        /// </summary>
        /// <param name="expands">expands</param>
        /// <param name="promotion">promotion</param>
        private void ExpandProfile(NameValueCollection expands, Promotion promotion)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Profiles)))
            {
                if (promotion.ProfileID.HasValue)
                {
                    var profiles = _profileRepository.GetByProfileID(promotion.ProfileID.Value);
                    if (!Equals(profiles, null))
                    {
                        promotion.ProfileIDSource = profiles;
                    }
                }
            }
        }

        private void SetFiltering(List<Tuple<string, string, string>> filters, PromotionQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;

                if (filterKey == FilterKeys.AccountId) SetQueryParameter(PromotionColumn.AccountID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.CouponCode) SetQueryParameter(PromotionColumn.CouponCode, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.CatalogId) SetQueryParameter(PromotionColumn.CatalogID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.CategoryId) SetQueryParameter(PromotionColumn.CategoryID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.DiscountAmount) SetQueryParameter(PromotionColumn.Discount, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.DiscountedProductId) SetQueryParameter(PromotionColumn.PromotionProductID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.EndDate) SetQueryParameter(PromotionColumn.EndDate, filterOperator, filterValue.Replace("'", ""), query);
                if (filterKey == FilterKeys.ExternalId) SetQueryParameter(PromotionColumn.ExternalID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.ManufacturerId) SetQueryParameter(PromotionColumn.ManufacturerID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.Name) SetQueryParameter(PromotionColumn.Name, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.PortalId) SetQueryParameter(PromotionColumn.PortalID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.ProfileId) SetQueryParameter(PromotionColumn.ProfileID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.PromotionTypeId) SetQueryParameter(PromotionColumn.DiscountTypeID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.RequiredProductId) SetQueryParameter(PromotionColumn.ProductID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.StartDate) SetQueryParameter(PromotionColumn.StartDate, filterOperator, filterValue.Replace("'", ""), query);
                if (filterKey == FilterKeys.DispalyOrder) SetQueryParameter(PromotionColumn.DisplayOrder, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.PromotionId) SetQueryParameter(PromotionColumn.PromotionID, filterOperator, filterValue, query);
            }
        }

        private void SetSorting(NameValueCollection sorts, PromotionSortBuilder sortBuilder)
        {
            if (sorts.HasKeys())
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);
                    if (value.Equals(SortKeys.Discount)) { SetSortDirection(PromotionColumn.Discount, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (value.Equals(SortKeys.DisplayOrder)) { SetSortDirection(PromotionColumn.DisplayOrder, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (value.Equals(SortKeys.EndDate)) { SetSortDirection(PromotionColumn.EndDate, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (value.Equals(SortKeys.Name)) { SetSortDirection(PromotionColumn.Name, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (value.Equals(SortKeys.PromotionId)) { SetSortDirection(PromotionColumn.PromotionID, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (value.Equals(SortKeys.StartDate)) { SetSortDirection(PromotionColumn.StartDate, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (value.Equals(SortKeys.CouponCode)) { SetSortDirection(PromotionColumn.CouponCode, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                }
            }
            else
            {
                SetSortDirection(PromotionColumn.PromotionID, sorts[StoredProcedureKeys.sortDirKey], sortBuilder);
            }
        }

        private void SetQueryParameter(PromotionColumn column, string filterOperator, string filterValue, PromotionQuery query)
        {
            base.SetQueryParameter(column, filterOperator, filterValue, query);
        }

        private void SetSortDirection(PromotionColumn column, string value, PromotionSortBuilder sortBuilder)
        {
            base.SetSortDirection(column, value, sortBuilder);
        }

        private bool IsDuplicatePromotion(Promotion promotionEntity)
        {
            return _promotionAdmin.IsPromotionOverlapped(promotionEntity);
        }

        private bool IsDuplicateCouponCode(PromotionModel model)
        {
            if (!string.IsNullOrEmpty(model.CouponCode))
            {
                var query = new PromotionQuery();
                query.AppendNotEquals(PromotionColumn.PromotionID, model.PromotionId.ToString());
                query.AppendEquals(PromotionColumn.CouponCode, model.CouponCode);
                var promotions = _promotionRepository.Find(query);
                if (promotions != null && promotions.Count > 0)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
