﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    public partial interface IAccountService
    {
        Collection<PRFTSalesRepresentative> GetSalesRepInfoByRoleName(string roleName);

        PRFTERPAccountDetailsModel GetAccountDetailsFromERP(string ExternalId);

        AccountListModel GetSubUserList(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
    }
}
