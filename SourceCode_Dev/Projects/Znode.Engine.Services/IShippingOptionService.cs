﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
	public interface IShippingOptionService
	{
        /// <summary>
        /// Get Shipping Option
        /// </summary>
        /// <param name="shippingOptionId">shippingOptionId</param>
        /// <param name="expands">expands</param>
        /// <returns>Returns ShippingOptionModel</returns>
		ShippingOptionModel GetShippingOption(int shippingOptionId, NameValueCollection expands);

        /// <summary>
        /// Get Shipping Options
        /// </summary>
        /// <param name="expands">expands</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <param name="page">page</param>
        /// <param name="shippingCountryCode">string shippingCountryCode</param>
        /// <returns>Returns ShippingOptionListModel</returns>
		ShippingOptionListModel GetShippingOptions(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page, string shippingCountryCode = null);
		
        /// <summary>
        /// Create Shipping Option
        /// </summary>
        /// <param name="model">ShippingOptionModel</param>
        /// <returns>Returns ShippingOptionModel</returns>
        ShippingOptionModel CreateShippingOption(ShippingOptionModel model);

        /// <summary>
        /// Update Shipping Option
        /// </summary>
        /// <param name="shippingOptionId">shippingOptionId</param>
        /// <param name="model">ShippingOptionModel</param>
        /// <returns>Returns ShippingOptionModel</returns>
		ShippingOptionModel UpdateShippingOption(int shippingOptionId, ShippingOptionModel model);

        /// <summary>
        /// Delete Shipping Option
        /// </summary>
        /// <param name="shippingOptionId">shippingOptionId</param>
        /// <returns>Returns True/False</returns>
		bool DeleteShippingOption(int shippingOptionId);

        /// <summary>
        /// Get the list of shipping option list for Franchise
        /// </summary>
        /// <param name="expands">Expand collection</param>
        /// <param name="filters">Filter collection</param>
        /// <param name="sorts">Sort collection</param>
        /// <param name="page">Page index</param>
        /// <returns>ShippingOptionListModel</returns>
        ShippingOptionListModel GetFranchiseShippingOptionList(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
	}
}
