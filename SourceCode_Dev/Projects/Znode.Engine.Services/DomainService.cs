﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using DomainRepository = ZNode.Libraries.DataAccess.Service.DomainService;

namespace Znode.Engine.Services
{
	public class DomainService : BaseService, IDomainService
	{
		private readonly DomainRepository _domainRepository;

		public DomainService()
		{
			_domainRepository = new DomainRepository();
		}

		public DomainModel GetDomain(int domainId)
		{
			var domain = _domainRepository.GetByDomainID(domainId);
			return DomainMap.ToModel(domain);
		}

		public DomainListModel GetDomains(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
		{
			var model = new DomainListModel();
			var domains = new TList<Domain>();

			var query = new DomainQuery();
			var sortBuilder = new DomainSortBuilder();
			var pagingStart = 0;
			var pagingLength = 0;

			SetFiltering(filters, query);
			SetSorting(sorts, sortBuilder);
			SetPaging(page, model, out pagingStart, out pagingLength);

			// Get the initial set
			var totalResults = 0;
			domains = _domainRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
			model.TotalResults = totalResults;

			// Check to set page size equal to the total number of results
			if (pagingLength == int.MaxValue)
			{
				model.PageSize = model.TotalResults;
			}

			// Map each item and add to the list
			foreach (var d in domains)
			{
				model.Domains.Add(DomainMap.ToModel(d));
			}

			return model;
		}

		public DomainModel CreateDomain(DomainModel model)
		{
            var query = new DomainQuery();

			if (model == null)
			{
				throw new Exception("Domain model cannot be null.");
			}

            // Set the DomainName
            var DomainName = model.DomainName.ToLower();
            var httpName = string.Empty;

            RemoveWwwFromDomain(ref DomainName, ref httpName);

            model.DomainName = DomainName;
            query.AppendEquals(DomainColumn.DomainName, model.DomainName);
            query.AppendNotEquals(DomainColumn.DomainID, model.DomainId.ToString());
            TList<Domain> list = _domainRepository.Find(query);
            if (list.Count > 0)
            {
                // Display error message
                throw new Exception("Domain names must be unique across all your stores.");
            }

            

			var entity = DomainMap.ToEntity(model);
			var domain = _domainRepository.Save(entity);
			return DomainMap.ToModel(domain);
		}

        private static void RemoveWwwFromDomain(ref string DomainName, ref string httpName)
        {
            if (DomainName.Contains("http://") || DomainName.Contains("https://"))
            {
                httpName = DomainName.Substring(0, 7);
                DomainName = DomainName.Replace("http://", string.Empty).Replace("https://", string.Empty);
            }

            if (DomainName.Contains(":"))
            {
                // Strip off the port number so we won't conflict with debuggers and other services that may specify a different port.
                DomainName = DomainName.Substring(0, DomainName.IndexOf(":"));
            }

            if (DomainName.StartsWith("www."))
            {
                DomainName = DomainName.Remove(0, 4);
            }

            // Remove any trailing "/"
            if (DomainName.EndsWith("/"))
            {
                DomainName = DomainName.Remove(DomainName.Length - 1);
            }
        }

		public DomainModel UpdateDomain(int domainId, DomainModel model)
		{
            var query = new DomainQuery();
			if (domainId < 1)
			{
				throw new Exception("Domain ID cannot be less than 1.");
			}

			if (model == null)
			{
				throw new Exception("Domain model cannot be null.");
			}

            // Set the DomainName
            var DomainName = model.DomainName.ToLower();
            var httpName = string.Empty;

            RemoveWwwFromDomain(ref DomainName, ref httpName);

            model.DomainName = DomainName;
            query.AppendEquals(DomainColumn.DomainName, model.DomainName);
            query.AppendNotEquals(DomainColumn.DomainID, model.DomainId.ToString());

            TList<Domain> list = _domainRepository.Find(query);
            if (list.Count > 0)
            {
                // Display error message
                throw new Exception("Domain names must be unique across all your stores.");
            }

			var domain = _domainRepository.GetByDomainID(domainId);
			if (domain != null)
			{
				// Set domain ID
				model.DomainId = domainId;

				var domainToUpdate = DomainMap.ToEntity(model);

				var updated = _domainRepository.Update(domainToUpdate);
				if (updated)
				{
					domain = _domainRepository.GetByDomainID(domainId);
					return DomainMap.ToModel(domain);
				}
			}

			return null;
		}

		public bool DeleteDomain(int domainId)
		{
			if (domainId < 1)
			{
				throw new Exception("Domain ID cannot be less than 1.");
			}

			var domain = _domainRepository.GetByDomainID(domainId);
			if (domain != null)
			{
				return _domainRepository.Delete(domain);
			}

			return false;
		}

		public Domain GetDomain(string domainName)
		{
			return _domainRepository.GetByDomainName(domainName.ToLower());
		}

		private void SetFiltering(List<Tuple<string, string, string>> filters, DomainQuery query)
		{
			foreach (var tuple in filters)
			{
				var filterKey = tuple.Item1;
				var filterOperator = tuple.Item2;
				var filterValue = tuple.Item3;

				if (filterKey == FilterKeys.ApiKey) SetQueryParameter(DomainColumn.ApiKey, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.DomainName) SetQueryParameter(DomainColumn.DomainName, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.IsActive) SetQueryParameter(DomainColumn.IsActive, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.PortalId) SetQueryParameter(DomainColumn.PortalID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.DomainId) SetQueryParameter(DomainColumn.DomainID, filterOperator, filterValue, query);
			}
		}

		private void SetSorting(NameValueCollection sorts, DomainSortBuilder sortBuilder)
		{
			if (sorts.HasKeys())
			{
				foreach (var key in sorts.AllKeys)
				{
					var value = sorts.Get(key);

                    if (value == SortKeys.DomainId) SetSortDirection(DomainColumn.DomainID, sorts["sortDir"], sortBuilder);
                    if (value == SortKeys.DomainName) SetSortDirection(DomainColumn.DomainName, sorts["sortDir"], sortBuilder);
                    if (value == SortKeys.ApiKey) SetSortDirection(DomainColumn.ApiKey, sorts["sortDir"], sortBuilder);
				}
			}
            
		}

		private void SetQueryParameter(DomainColumn column, string filterOperator, string filterValue, DomainQuery query)
		{
			base.SetQueryParameter(column, filterOperator, filterValue, query);
		}

		private void SetSortDirection(DomainColumn column, string value, DomainSortBuilder sortBuilder)
		{
			base.SetSortDirection(column, value, sortBuilder);
		}
	}
}
