﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Helpers.Constants;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;
using AddressRepository = ZNode.Libraries.DataAccess.Service.AddressService;
using AddressExtnRepository = ZNode.Libraries.DataAccess.Service.AddressExtnService;

namespace Znode.Engine.Services
{
	public class AddressService : BaseService, IAddressService
	{
		private readonly AddressRepository _addressRepository;
        //PRFT Custom Code: Start
        private readonly AddressExtnRepository _addressExtnRepository;
        //PRFT Custom Code: End
        private Address _BillingAddress = new Address();
        private Address _ShippingAddress = new Address();

		public AddressService()
		{
			_addressRepository = new AddressRepository();
            _addressExtnRepository = new AddressExtnRepository();
        }

		public AddressModel GetAddress(int addressId)
		{
			var address = _addressRepository.GetByAddressID(addressId);

			if (address != null)
			{
				return AddressMap.ToModel(address);
			}

			return new AddressModel();
		}

		public AddressListModel GetAddresses(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
		{
			var model = new AddressListModel();
			var addresses = new TList<Address>();

			var query = new AddressQuery();
			var sortBuilder = new AddressSortBuilder();
			var pagingStart = 0;
			var pagingLength = 0;

			SetFiltering(filters, query);
			SetSorting(sorts, sortBuilder);
			SetPaging(page, model, out pagingStart, out pagingLength);

			// Get the initial set
			var totalResults = 0;
			addresses = _addressRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
			model.TotalResults = totalResults;

			// Check to set page size equal to the total number of results
			if (pagingLength == int.MaxValue)
			{
				model.PageSize = model.TotalResults;
			}

			// Map each item and add to the list
			foreach (var a in addresses)
			{
				model.Addresses.Add(AddressMap.ToModel(a));
			}

			return model;
		}

		public AddressModel CreateAddress(AddressModel model)
		{
			if (model == null)
			{
				throw new Exception("Address model cannot be null.");
			}

			var address = AddressMap.ToEntity(model);
			_addressRepository.Save(address);

            //PRFT Custom Code:Start
            if(model.AddressExtn!=null)
            {
                var addressExtn = AddressExtnMap.ToEntity(model.AddressExtn);
                addressExtn.AddressID = address.AddressID;
                addressExtn.Custom1 = model.AddressExtn.Custom1;
                _addressExtnRepository.Save(addressExtn);
            }
            //PRFT Custom Code:Start

            return AddressMap.ToModel(address);
		}

		public AddressModel UpdateAddress(int addressId, AddressModel model)
		{
			if (addressId < 1)
			{
				throw new Exception("Address ID cannot be less than 1.");
			}

			if (model == null)
			{
				throw new Exception("Address model cannot be null.");
			}

			var address = _addressRepository.GetByAddressID(addressId);
			if (address != null)
			{
				// Set the address ID
				model.AddressId = addressId;

				var updatedEntity = AddressMap.ToEntity(model, address);
				var resultAddress = _addressRepository.Save(updatedEntity);

                //PRFT Custom Code:Start
                if (model.AddressExtn != null)
                {
                    if(model.AddressExtn.AddressExtnID>0)
                    {
                        var addressExtn = _addressExtnRepository.GetByAddressExtnID(model.AddressExtn.AddressExtnID);
                        if (addressExtn != null)
                        {
                            var updatedAddressExtnEntity = AddressExtnMap.ToEntity(model.AddressExtn, addressExtn);
                            var resultAddressExtn = _addressExtnRepository.Save(updatedAddressExtnEntity);
                        }
                    }
                    else
                    {
                        //With the existing imported data scenarion this may happens 
                        model.AddressExtn.AddressID = resultAddress.AddressID;
                        var addressExtn = AddressExtnMap.ToEntity(model.AddressExtn);
                        _addressExtnRepository.Save(addressExtn);
                    }
                }
                //PRFT Custom Code:Start

                return AddressMap.ToModel(resultAddress);
			}

			return null;
		}

        //new method
        public AddressModel UpdateAddressDefault(int addressId, AddressModel model, out string errorCode)
        {
            errorCode = "0";

            if (model == null)
            {
                throw new Exception("Address model cannot be null.");
            }

            //ZNode.Libraries.DataAccess.Service.AddressService addressService = new ZNode.Libraries.DataAccess.Service.AddressService();
            Address address = new Address();
            if (addressId > 0)
            {
                address = _addressRepository.GetByAddressID(addressId);
            }

            if (!this.HasDefaultAddress(model.AccountId, true,model))
            {
                errorCode = ErrorCodes.DefaultBillingAddressNoSet.ToString();
                return new AddressModel();
            }

            if (!this.HasDefaultAddress(model.AccountId, false, model))
            {
                errorCode = ErrorCodes.DefaultShippingNoSet.ToString();
                return new AddressModel();
            }
            if (model.IsDefaultShipping)
            {
                if (!ValidateCountry(model.CountryCode))
                {
                    errorCode = ErrorCodes.NoShippingFacility.ToString();
                    return new AddressModel(); 
                }
            }

            address.AccountID = model.AccountId;
            address.Name = model.Name;
            address.FirstName = model.FirstName;
            address.LastName = model.LastName;
            address.CompanyName = model.CompanyName;
            address.Street = model.StreetAddress1;
            address.Street1 = model.StreetAddress2;
            address.City = model.City;
            address.StateCode = model.StateCode.ToUpper();
            address.PostalCode = model.PostalCode;
            address.CountryCode = model.CountryCode;
            address.PhoneNumber = model.PhoneNumber;

            TList<Address> addressList = _addressRepository.GetByAccountID(model.AccountId);
            if (model.IsDefaultBilling)
            {
                // Clear all previous default billing address flag.
                foreach (Address currentItem in addressList)
                {
                    currentItem.IsDefaultBilling = false;
                }

                _addressRepository.Update(addressList);
            }

            if (model.IsDefaultShipping)
            {
                // Clear all previous default billing address flag.
                foreach (Address currentItem in addressList)
                {
                    currentItem.IsDefaultShipping = false;
                }

                _addressRepository.Update(addressList);
            }

            address.IsDefaultBilling = model.IsDefaultBilling;
            address.IsDefaultShipping = model.IsDefaultShipping;

            bool isSuccess = false;
            if (addressId > 0)
            {
                isSuccess = _addressRepository.Update(address);
            }
            else
            {
                _addressRepository.Save(address);
                return AddressMap.ToModel(address);
            }
            return model;
        }

        public bool ValidateCountry(string countryCode)
        {
            PortalCountryHelper portalCountryHelper = new PortalCountryHelper();
            DataSet countryDs = portalCountryHelper.GetCountriesByPortalID(ZNodeConfigManager.SiteConfig.PortalID);

            DataTable BillingView = countryDs.Tables[0];
            DataRow[] drBillingView = BillingView.Select("CountryCode='" + countryCode.ToUpper() + "' and ShippingActive = 'true'");
            
            return drBillingView != null && drBillingView.Length > 0 ? true:false;
        }

		public bool DeleteAddress(int addressId)
		{
			if (addressId < 1)
			{
				throw new Exception("Address ID cannot be less than 1.");
			}
            
            //PRFT Custom Code: Start
            var addressExtnCollection = _addressExtnRepository.GetByAddressID(addressId);
            foreach (var addressExtn in addressExtnCollection)
            {
                if (addressExtn.AddressID == addressId)
                {
                    _addressExtnRepository.Delete(addressExtn.AddressExtnID);
                }
            }
            //PRFT Custom Code: End
            return _addressRepository.Delete(addressId);
		}

		public bool AddDefaultAddresses(int accountId)
		{
			try
			{
				var billingAddress = AddressMap.GetDefaultBillingAddress(accountId);
				_addressRepository.Save(billingAddress);

				var shippingAddress = AddressMap.GetDefaultShippingAddress(accountId);
				_addressRepository.Save(shippingAddress);
			}
			catch (Exception)
			{
				return false;
			}

			return true;
		}

		private void SetFiltering(List<Tuple<string, string, string>> filters, AddressQuery query)
		{
			foreach (var tuple in filters)
			{
				var filterKey = tuple.Item1;
				var filterOperator = tuple.Item2;
				var filterValue = tuple.Item3;

				if (filterKey == FilterKeys.AccountId) SetQueryParameter(AddressColumn.AccountID, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.City) SetQueryParameter(AddressColumn.City, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.CompanyName) SetQueryParameter(AddressColumn.CompanyName, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.CountryCode) SetQueryParameter(AddressColumn.CountryCode, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.LastName) SetQueryParameter(AddressColumn.LastName, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.PostalCode) SetQueryParameter(AddressColumn.PostalCode, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.StateCode) SetQueryParameter(AddressColumn.StateCode, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.IsBillingActive) SetQueryParameter(AddressColumn.IsDefaultBilling, filterOperator, filterValue, query);
			}
		}

		private void SetSorting(NameValueCollection sorts, AddressSortBuilder sortBuilder)
		{
			if (sorts.HasKeys())
			{
				foreach (var key in sorts.AllKeys)
				{
					var value = sorts.Get(key);

                    if (Equals(value , SortKeys.AddressId)) SetSortDirection(AddressColumn.AddressID, sorts[StoredProcedureKeys.sortDirKey], sortBuilder);
                    if (Equals(value , SortKeys.CountryCode)) SetSortDirection(AddressColumn.CountryCode, sorts[StoredProcedureKeys.sortDirKey], sortBuilder);
                    if (Equals(value , SortKeys.PostalCode)) SetSortDirection(AddressColumn.PostalCode, sorts[StoredProcedureKeys.sortDirKey], sortBuilder);
                    if (Equals(value , SortKeys.StateCode)) SetSortDirection(AddressColumn.StateCode, sorts[StoredProcedureKeys.sortDirKey], sortBuilder);
                    if (Equals(value , SortKeys.Name)) SetSortDirection(AddressColumn.Name, sorts[StoredProcedureKeys.sortDirKey], sortBuilder);
				}
			}
		}

		private void SetQueryParameter(AddressColumn column, string filterOperator, string filterValue, AddressQuery query)
		{
			base.SetQueryParameter(column, filterOperator, filterValue, query);
		}

		private void SetSortDirection(AddressColumn column, string value, AddressSortBuilder sortBuilder)
		{
			base.SetSortDirection(column, value, sortBuilder);
		}

        //New Methods

        /// <summary>
        /// Check whether the account has default billing or shipping address.
        /// </summary>
        /// <param name="accountId">Account Id</param>
        /// <param name="isBillingAddress">True to check billing address, false to check shipping address.</param>
        /// <returns>Returns true if account has default address or false.</returns>
        private bool HasDefaultAddress(int accountId, bool isBillingAddress,AddressModel model)
        {
            bool hasDefault = true;

            AccountAdmin accountAdmin = new AccountAdmin();
            ZNode.Libraries.DataAccess.Entities.Address addresses = null;
            if (isBillingAddress)
            {
                addresses = accountAdmin.GetDefaultBillingAddress(accountId);
                hasDefault = this.ValidateDefaultAddress(addresses, model.IsDefaultBilling,model.AddressId);
            }
            else
            {
                addresses = accountAdmin.GetDefaultShippingAddress(accountId);
                hasDefault = this.ValidateDefaultAddress(addresses, model.IsDefaultShipping, model.AddressId);
            }

            return hasDefault;
        }

        /// <summary>
        /// Validate account had default address.
        /// </summary>
        /// <param name="address">Address object</param>
        /// <param name="cb">Check box control</param>
        /// <returns>Returns true if account has default address.</returns>
        private bool ValidateDefaultAddress(Address address, bool cb, int addressId)
        {
            if (address == null && !cb)
            {
                return false;
            }
            else
            {
                if (!cb && address != null && address.AddressID == addressId)
                {
                    return false;
                }
            }

            return true;
        }

        private void CheckSameAsBilling(int accountId)
        {
            
                Address address = null;
                TList<Address> addressList = _addressRepository.GetByAccountID(accountId);

                if (addressList.Count == 1)
                {
                    // Create new shipping address from existing billing address.
                    address = (Address)addressList[0].Clone();
                    address.AddressID = 0;
                    address.Name = "Default Shipping Address";
                    address.IsDefaultBilling = false;
                    address.IsDefaultShipping = true;
                    _addressRepository.Insert(address);

                    // Clear previous default shipping address.
                    foreach (Address currentItem in addressList)
                    {
                        currentItem.IsDefaultShipping = false;
                    }

                    _addressRepository.Update(addressList);

                    //this.BindAddressNames(false);
                }
                else
                {
                    // Load user's default shipping address from account.
                    AccountAdmin accountAdmin = new AccountAdmin();
                    address = accountAdmin.GetDefaultShippingAddress(accountId);
                }

                if (address != null)
                {
                    LoadAddress(address.AddressID, false);
                }

        }

        /// <summary>
        /// Load the selected address
        /// </summary>
        /// <param name="addressId">Address Id</param>
        /// <param name="isBillingAddress">IsBilling address (true/false)</param>
        public void LoadAddress(int addressId, bool isBillingAddress)
        {
            AddressService addressService = new AddressService();
            Address shippingAddress = new Address();
            Address address = _addressRepository.GetByAddressID(addressId);
            if (address != null)
            {
                AddressModel addressModel = new AddressModel();
                if (isBillingAddress)
                {
                    

                    this._BillingAddress = address;
                    addressModel.Name = address.Name;
                    addressModel.FirstName = address.FirstName;
                    addressModel.LastName = address.LastName;
                    addressModel.CompanyName = address.CompanyName;
                    addressModel.StreetAddress1 = address.Street;
                    addressModel.StreetAddress2 = address.Street1;
                    addressModel.City = address.City;
                    addressModel.StateCode = address.StateCode;
                    addressModel.PostalCode = address.PostalCode;

                    if (address.CountryCode != null && address.CountryCode.Length > 0)
                    {
                        addressModel.CountryCode = address.CountryCode;
                    }
                }
                else
                {

                    this._ShippingAddress = address;
                    addressModel.Name = address.Name;
                    addressModel.FirstName = address.FirstName;
                    addressModel.LastName = address.LastName;
                    addressModel.CompanyName = address.CompanyName;
                    addressModel.StreetAddress1 = address.Street;
                    addressModel.StreetAddress2 = address.Street1;
                    addressModel.City = address.City;
                    addressModel.StateCode = address.StateCode;
                    addressModel.PostalCode = address.PostalCode;
                    if (address.CountryCode != null && address.CountryCode.Length > 0)
                    {
                        addressModel.CountryCode = address.CountryCode;
                    }

                    addressModel.PhoneNumber = address.PhoneNumber;

                    shippingAddress = address;
                    addressModel.AddressId = address.AddressID;

                }
            }
        }

        public AddressListModel UpdateCustomerAddress(int accountId, AddressListModel listModel)
        {

            if (!listModel.IsSameAsBillingAddress)
            {
                CheckSameAsBilling(accountId);
            }
            AccountService accountService = new AccountService();
            ProfileAdmin prfAdmin = new ProfileAdmin();
            Profile profile = prfAdmin.GetDefaultProfileByPortalID(listModel.PortalId);

            int profileId = 0;

            if (profile != null)
            {
                profileId = profile.ProfileID;
            }
            else
            {
                throw new Exception("Profile cannot be null.");
            }

            AddressModel shippingAddressModel = listModel.Addresses[0];
            AddressModel billingAddressModel = listModel.Addresses[1];
            TList<Address> addressList = _addressRepository.GetByAccountID(accountId);
            ZNodeUserAccount _userAccount = new ZNodeUserAccount();

            
            
                Address billingAddress = new Address();
                Address shippingAddress = new Address();

                AddressService addressService = new AddressService();
                if (listModel.IsSameAsBillingAddress)
                {
                    // If both address are same then create single address entity
                    billingAddressModel.AccountId = accountId;
                    billingAddress.AddressID = addressList[0].AddressID;
                    billingAddressModel.IsDefaultBilling = true;
                    billingAddressModel.IsDefaultShipping = true;
                    billingAddressModel.Name = "Default Address";
                    var updatedEntity = AddressMap.ToEntity(billingAddressModel, billingAddress);

                    // Both address are same
                    _addressRepository.Update(updatedEntity);
                    
                }
                else
                {
                    billingAddressModel.AccountId = accountId;
                    billingAddress.AddressID = addressList[0].AddressID;
                    billingAddressModel.IsDefaultBilling = true;
                    billingAddressModel.IsDefaultShipping = false;
                    billingAddressModel.Name = "Default Address";
                    var updatedEntity = AddressMap.ToEntity(billingAddressModel, billingAddress);

                    _addressRepository.Update(updatedEntity);

                    shippingAddressModel.AccountId = accountId;
                    shippingAddress.AddressID = addressList[1].AddressID;
                    shippingAddressModel.AddressId = addressList[1].AddressID;
                    shippingAddressModel.IsDefaultBilling = addressList[1].IsDefaultBilling;
                    shippingAddressModel.IsDefaultShipping = addressList[1].IsDefaultShipping;
                    shippingAddressModel.Name = "Default Shipping Address";
                    var updatedEntity2 = AddressMap.ToEntity(shippingAddressModel, shippingAddress);
                    _addressRepository.Update(updatedEntity2);
                    
                }
                return listModel;
        }
	}
}
