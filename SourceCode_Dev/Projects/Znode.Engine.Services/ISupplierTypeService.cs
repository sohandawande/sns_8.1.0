﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
	public interface ISupplierTypeService
	{
		SupplierTypeModel GetSupplierType(int supplierTypeId);
		SupplierTypeListModel GetSupplierTypes(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
		SupplierTypeModel CreateSupplierType(SupplierTypeModel model);
		SupplierTypeModel UpdateSupplierType(int supplierTypeId, SupplierTypeModel model);
		bool DeleteSupplierType(int supplierTypeId);

        /// <summary>
        /// ZNode Version 8.0
        /// Get all Supplier Types not present in database.
        /// </summary>
        /// <returns>Return Supplier Type List Model</returns>
        SupplierTypeListModel GetAllSupplierTypesNotInDatabase();
	}
}
