﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Helpers;
using Znode.Libraries.Helpers.Constants;
using Znode.Libraries.Helpers.Extensions;
using ZNode.Libraries.DataAccess.Custom;
using AddOnRepository = ZNode.Libraries.DataAccess.Service.AddOnService;

namespace Znode.Engine.Services
{
    /// <summary>
    /// AddOn Service
    /// </summary>
    public class AddOnService : BaseService, IAddOnService
    {
        #region Private Variables
        private readonly AddOnRepository _addOnRepository;
        #endregion

        #region Constructor
        public AddOnService()
        {
            _addOnRepository = new AddOnRepository();
        }
        #endregion

        #region Public Methods
        public AddOnModel GetAddOn(int AddOnId, NameValueCollection expands)
        {
            var addOn = _addOnRepository.GetByAddOnID(AddOnId);

            return AddOnMap.ToModel(addOn);
        }

        public AddOnListModel GetAddOns(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            List<Tuple<string, string, string>> filterList = new List<Tuple<string, string, string>>();
            AddOnListModel list = new AddOnListModel();
            int totalRowCount = 0;
            var pagingStart = 0;
            var pagingLength = 0;
            string innerWhereClause = string.Empty;

            foreach (var item in filters)
            {
                if (Equals(item.Item1, "username"))
                {
                    filterList.Add(item);
                    innerWhereClause = StringHelper.GenerateDynamicWhereClause(filterList);
                    filters.Remove(item);
                    break;
                }
            }
            string orderBy = StringHelper.GenerateDynamicOrderByClause(sorts);
            pagingStart = Convert.ToInt32(page.Get(PageKeys.Index));
            pagingLength = Convert.ToInt32(page.Get(PageKeys.Size));
            string whereClause = StringHelper.GenerateDynamicWhereClause(filters);

            StoredProcedureHelper storedProcedureHelper = new StoredProcedureHelper();

            DataSet resultDataSet = storedProcedureHelper.SP_DataDetailsSpConfig(whereClause, null, StoredProcedureKeys.SpSearchAddOns, orderBy, pagingStart.ToString(), pagingLength.ToString(), out totalRowCount, null, innerWhereClause);
            if (!Equals(resultDataSet, null) && resultDataSet.Tables.Count > 0 && !Equals(resultDataSet.Tables[0], null) && resultDataSet.Tables[0].Rows.Count > 0)
            {
                list.AddOns = resultDataSet.Tables[0].ToList<AddOnModel>().ToCollection();
            }
            list.TotalResults = totalRowCount;
            list.PageSize = pagingLength;
            list.PageIndex = pagingStart;
            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                list.PageSize = list.TotalResults;
            }
            return list;
        }

        public AddOnModel CreateAddOn(AddOnModel model)
        {
            if (Equals(model, null))
            {
                throw new Exception("Add On model cannot be null.");
            }
            if (Equals(model.LocaleId, 0))
            {
                model.LocaleId = 43;
            }
            var entity = AddOnMap.ToEntity(model);
            var addOn = _addOnRepository.Save(entity);
            return AddOnMap.ToModel(addOn);
        }

        public AddOnModel UpdateAddOn(int addOnId, AddOnModel model)
        {
            if (addOnId < 1)
            {
                throw new Exception("AddOn Id cannot be less than 1.");
            }

            if (model == null)
            {
                throw new Exception("AddOn model cannot be null.");
            }

            var addOn = _addOnRepository.GetByAddOnID(addOnId);
            if (addOn != null)
            {
                // Set Add-on ID
                model.AddOnId = addOnId;
                if (Equals(model.LocaleId, 0))
                {
                    model.LocaleId = 43;
                }
                var addOnToUpdate = AddOnMap.ToEntity(model);

                var updated = _addOnRepository.Update(addOnToUpdate);
                if (updated)
                {
                    addOn = _addOnRepository.GetByAddOnID(addOnId);
                    return AddOnMap.ToModel(addOn);
                }
            }
            return null;
        }

        public bool DeleteAddOn(int addOnId)
        {
            if (addOnId < 1)
            {
                throw new Exception("AddOn ID cannot be less than 1.");
            }
            if (this.CheckAssociateAddOnValue(addOnId))
            {
                throw new Exception("An error occurred and the product Add-On could not be deleted. Please ensure that this Add-On does not contain Add-On Values or products. If it does, then delete the Add-On values and products first.");
            }
            var addOn = _addOnRepository.GetByAddOnID(addOnId);
            if (!Equals(addOn, null))
            {
                return _addOnRepository.Delete(addOn);
            }

            return false;
        }



        #endregion

        #region Private Methods
        /// <summary>
        /// Check Addon has associated addonvalue
        /// </summary>
        /// <param name="addOnId">int addOnId</param>
        /// <returns>Return true if addon has  associated addonvalue else return false</returns>
        private bool CheckAssociateAddOnValue(int addOnId)
        {
            AddOnModel model = new AddOnModel();
            AddOnHelper addOnHelper = new AddOnHelper();
            DataSet ds = addOnHelper.GetAssociatedAddOnValuesByAddOnId(addOnId);

            if (!Equals(ds, null) && !Equals(ds.Tables, null) && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion
    }
}