﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Helpers;
using Znode.Libraries.Helpers.Constants;
using Znode.Libraries.Helpers.Extensions;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using AddOnValueRepository = ZNode.Libraries.DataAccess.Service.AddOnValueService;

namespace Znode.Engine.Services
{
    /// <summary>
    /// AddOn Value Service
    /// </summary>
    public class AddOnValueService : BaseService, IAddOnValueService
    {
        #region Private Variables
        private readonly AddOnValueRepository _addOnValueRepository;
        private readonly ProductAddOnAdmin productAddOnAdmin;
        #endregion

        #region Constructor
        public AddOnValueService()
        {
            _addOnValueRepository = new AddOnValueRepository();
            productAddOnAdmin = new ProductAddOnAdmin();
        }
        #endregion

        #region Public Methods
        public AddOnValueModel CreateAddOnValue(AddOnValueModel model)
        {
            if (Equals(model, null))
            {
                throw new Exception("Add On Value model cannot be null.");
            }
            var entity = AddOnValueMap.ToEntity(model);
            ProductAdmin.UpdateQuantity(entity, model.QuantityOnHand, Convert.ToInt32(model.ReOrderLevel));
            var addOnValue = _addOnValueRepository.Save(entity);
            return AddOnValueMap.ToModel(addOnValue);
        }

        public bool DeleteAddOnValue(int addOnValueId)
        {

            if (addOnValueId < 1)
            {
                throw new Exception("AddOnValue ID cannot be less than 1.");
            }

            var addOnValue = _addOnValueRepository.GetByAddOnValueID(addOnValueId);
            if (!Equals(addOnValue, null))
            {
                return _addOnValueRepository.Delete(addOnValueId);
            }

            return false;
        }

        public AddOnValueModel UpdateAddOnValue(int addOnValueId, AddOnValueModel model)
        {
            if (addOnValueId < 1)
            {
                throw new Exception("AddOnValue Id cannot be less than 1.");
            }

            if (Equals(model, null))
            {
                throw new Exception("AddOnValue model cannot be null.");
            }

            var addOnValue = _addOnValueRepository.GetByAddOnValueID(addOnValueId);
            if (!Equals(addOnValue, null))
            {
                // Set Add-onValue ID
                model.AddOnValueId = addOnValueId;
                model.UpdateDate = DateTime.Now;
                var addOnValueToUpdate = AddOnValueMap.ToEntity(model);
                ProductAdmin.UpdateQuantity(addOnValueToUpdate, model.QuantityOnHand, Convert.ToInt32(model.ReOrderLevel));
                var updated = _addOnValueRepository.Update(addOnValueToUpdate);
                if (updated)
                {
                    addOnValue = _addOnValueRepository.GetByAddOnValueID(addOnValueId);
                    return AddOnValueMap.ToModel(addOnValue);
                }
            }
            return null;
        }

        public AddOnValueModel GetAddOnValue(int AddOnValueId, NameValueCollection expands)
        {
            var addOnValue = _addOnValueRepository.GetByAddOnValueID(AddOnValueId);
            SKUInventory skuInventory = ProductAdmin.GetAddOnInventory(addOnValue);
            AddOnValueModel model = new AddOnValueModel();
            model = AddOnValueMap.ToModel(addOnValue);
            model.ReOrderLevel = Convert.ToInt32(skuInventory.ReOrderLevel);
            model.QuantityOnHand = Convert.ToInt32(skuInventory.QuantityOnHand);
            return model;
        }

        public AddOnValueListModel GetAddOnValueByAddOnId(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new AddOnValueListModel();
            var sortBuilder = new AddOnValueSortBuilder();
            int totalRowCount = 0;
            var pagingStart = 0;
            var pagingLength = 0;
            string whereClause = StringHelper.GenerateDynamicWhereClause(filters);
            string orderBy = StringHelper.GenerateDynamicOrderByClause(sorts);
            pagingStart = Convert.ToInt32(page.Get(PageKeys.Index));
            pagingLength = Convert.ToInt32(page.Get(PageKeys.Size));
            StoredProcedureHelper storedProcedureHelper = new StoredProcedureHelper();
            DataSet resultDataSet = storedProcedureHelper.SP_DataDetailsSpConfig(whereClause, string.Empty/*HavingClause*/, StoredProcedureKeys.SpGetAddOnValues, orderBy, pagingStart.ToString(), pagingLength.ToString(), out totalRowCount);

            model.AddOnValues = resultDataSet.Tables[0].ToList<AddOnValueModel>().ToCollection();
            model.TotalResults = totalRowCount;
            model.PageSize = pagingLength;
            model.PageIndex = pagingStart;
            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                model.PageSize = model.TotalResults;
            }

            return model;
        }

        #endregion
    }
}
