﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    public interface IVendorAccountService
    {
        /// <summary>
        ///  This method will get the vendor Account list.
        /// </summary>
        /// <param name="expands">Expand Collection expands</param>
        /// <param name="filters">Filter criteria</param>
        /// <param name="sorts">sort criteria</param>
        /// <param name="page">page collection</param>
        /// <returns>Returns all vendor Account details in VendorAccountListModel format</returns>
        VendorAccountListModel GetVendorAccountList(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Get vendor account by account id
        /// </summary>
        /// <param name="accountId">Int accountId to get vendor account id</param>
        /// <returns>VendorAccountModel</returns>
        VendorAccountModel GetVendorAccountById(int accountId);
    }
}
