﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using ZipCodeRepository = ZNode.Libraries.DataAccess.Service.ZipCodeService;

namespace Znode.Engine.Services
{
    /// <summary>
    /// Zip Code Service
    /// </summary>
    public class ZipCodeService : BaseService, IZipCodeService
    {
        #region Private Variables
        private readonly ZipCodeRepository _zipCodeRepository; 
        #endregion

        #region Constructor
        public ZipCodeService()
        {
            _zipCodeRepository = new ZipCodeRepository();
        } 
        #endregion

        #region Public Methods
        public ZipCodeListModel GetZipCodes(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new ZipCodeListModel();
            var zipCodes = new TList<ZipCode>();

            var query = new ZipCodeQuery();
            var sortBuilder = new ZipCodeSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            zipCodes = _zipCodeRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (Equals(pagingLength, int.MaxValue))
            {
                model.PageSize = model.TotalResults;
            }

            // Map each item and add to the list
            foreach (var s in zipCodes)
            {
                model.ZipCode.Add(ZipCodeMap.ToModel(s));
            }

            return model;
        } 
        #endregion

        #region Private Methods
        private void SetFiltering(List<Tuple<string, string, string>> filters, ZipCodeQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;

                if (Equals(filterKey, FilterKeys.CountyFIPS)) { SetQueryParameter(ZipCodeColumn.CountyFIPS, filterOperator, filterValue, query); }
                if (Equals(filterKey, FilterKeys.StateCode)) { SetQueryParameter(ZipCodeColumn.StateAbbr, filterOperator, filterValue, query); }
                if (Equals(filterKey, FilterKeys.CountyName)) { SetQueryParameter(ZipCodeColumn.CountyName, filterOperator, filterValue, query); }
            }
        }

        private void SetSorting(NameValueCollection sorts, ZipCodeSortBuilder sortBuilder)
        {
            if (sorts.HasKeys())
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);

                    if (Equals(key, SortKeys.CountyFIPS)) { SetSortDirection(ZipCodeColumn.CountyFIPS, value, sortBuilder); }
                    if (Equals(key, SortKeys.CountyName)) { SetSortDirection(ZipCodeColumn.CountyName, value, sortBuilder); }
                }
            }
        } 
        #endregion
    }
}
