﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    public interface IUrlRedirectService
    {
        /// <summary>
        /// Get the URL redirect.
        /// </summary>
        /// <param name="urlRedirectId">Id of the url redirect.</param>
        /// <returns>Get the url redirect.</returns>
        UrlRedirectModel GetUrlRedirect(int urlRedirectId);

        /// <summary>
        /// Create the Url Redirect.
        /// </summary>
        /// <param name="model">Model to create.</param>
        /// <returns>Created Model</returns>
        UrlRedirectModel CreateUrlRedirect(UrlRedirectModel model);

        /// <summary>
        /// Update the existing url redirect.
        /// </summary>
        /// <param name="urlRedirectId">Id of the url redirect.</param>
        /// <param name="model">updated model.</param>
        /// <returns>Updated Model.</returns>
        UrlRedirectModel UpdateUrlRedirect(int urlRedirectId, UrlRedirectModel model);

        /// <summary>
        /// Delete the existing url redirect.
        /// </summary>
        /// <param name="urlRedirectId">Id of the url redirect.</param>
        /// <returns>return true or false.</returns>
        bool DeleteUrlRedirect(int urlRedirectId);

        /// <summary>
        /// Get the url redirect list.
        /// </summary>
        /// <param name="expands">Name value collection.</param>
        /// <param name="filters">filter collection.</param>
        /// <param name="sorts">Sorts colletion.</param>
        /// <param name="page">page</param>
        /// <returns>Returns url redirect list model</returns>
        UrlRedirectListModel GetUrlRedirects(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
    }
}
