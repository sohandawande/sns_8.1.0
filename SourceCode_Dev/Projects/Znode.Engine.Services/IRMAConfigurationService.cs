﻿using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    public interface IRMAConfigurationService
    {
        /// <summary>
        /// Get a RMAConfiguration on the basis of rmaConfigId
        /// </summary>
        /// <param name="rmaConfigId">rmaConfigId by which get existing rmaConfig</param>
        /// <returns>Model of RMAConfiguration</returns>
        RMAConfigurationModel GetRMAConfiguration(int rmaConfigId, NameValueCollection expands);

        /// <summary>
        /// Get the lsit of all RMAConfigurations
        /// </summary>
        /// <returns>ListModel of RMAConfiguration</returns>
        RMAConfigurationListModel GetRMAConfigurations();

        /// <summary>
        /// Create new RMAConfiguration
        /// </summary>
        /// <param name="model">Model of RMAConfiguration</param>
        /// <returns>Model of RMAConfiguration</returns>
        RMAConfigurationModel CreateRMAConfiguration(RMAConfigurationModel model);

        /// <summary>
        /// Update a RMAConfiguration
        /// </summary>
        ///<param name="rmaConfigId">rmaConfigId by which existing rmaConfig will update</param>
        /// <param name="model">Model of RMAConfiguration</param>
        /// <returns>Model of RMAConfiguration</returns>
        RMAConfigurationModel UpdateRMAConfiguration(int rmaConfigId, RMAConfigurationModel model);

        /// <summary>
        /// Get the lsit of all RMAConfigurations
        /// </summary>
        /// <returns>ListModel of RMAConfiguration</returns>
        RMAConfigurationListModel GetAllRMAConfiguration();
    }
}
