﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Helpers.Constants;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using SupplierRepository = ZNode.Libraries.DataAccess.Service.SupplierService;
using SupplierTypeRepository = ZNode.Libraries.DataAccess.Service.SupplierTypeService;

namespace Znode.Engine.Services
{
    public class SupplierService : BaseService, ISupplierService
    {
        #region Private Variables
        private readonly SupplierRepository _supplierRepository;
        private readonly SupplierTypeRepository _supplierTypeRepository;
        #endregion

        #region Constructor
        public SupplierService()
        {
            _supplierRepository = new SupplierRepository();
            _supplierTypeRepository = new SupplierTypeRepository();
        }
        #endregion

        #region Public Methods
        public SupplierModel GetSupplier(int supplierId, NameValueCollection expands)
        {
            SupplierModel model = new SupplierModel();
            var supplier = _supplierRepository.GetBySupplierID(supplierId);
            if (!Equals(supplier, null))
            {
                GetExpands(expands, supplier);
                model = SupplierMap.ToModel(supplier);
                model.ClassName = GetSupplierTypeClassName((model.SupplierTypeId.HasValue) ? model.SupplierTypeId.Value : 0);
            }
            return model;
        }

        public SupplierListModel GetSuppliers(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new SupplierListModel();
            var suppliers = new TList<Supplier>();
            var tempList = new TList<Supplier>();

            var query = new SupplierQuery();
            var sortBuilder = new SupplierSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            tempList = _supplierRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (pagingLength.Equals(int.MaxValue))
            {
                model.PageSize = model.TotalResults;
            }

            // Now go through the list and get any expands
            foreach (var supplier in tempList)
            {
                GetExpands(expands, supplier);
                suppliers.Add(supplier);
            }

            // Map each item and add to the list
            foreach (var s in suppliers)
            {
                model.Suppliers.Add(SupplierMap.ToModel(s));
            }

            return model;
        }

        public SupplierModel CreateSupplier(SupplierModel model)
        {
            if (Equals(model, null))
            {
                throw new Exception("Supplier model cannot be null.");
            }
            if (Equals(model.SupplierTypeId,null) || model.SupplierTypeId.Equals(0))
            {
                model.SupplierTypeId = GetSupplierTypeId(model.ClassName);
            }
            var entity = SupplierMap.ToEntity(model);
            var supplier = _supplierRepository.Save(entity);
            return SupplierMap.ToModel(supplier);
        }

        public SupplierModel UpdateSupplier(int supplierId, SupplierModel model)
        {
            if (supplierId < 1)
            {
                throw new Exception("Supplier ID cannot be less than 1.");
            }

            if (Equals(model, null))
            {
                throw new Exception("Supplier model cannot be null.");
            }

            var supplier = _supplierRepository.GetBySupplierID(supplierId);
            if (!Equals(supplier, null))
            {
                // Set the supplier ID and map
                model.SupplierId = supplierId;
                model.SupplierTypeId = GetSupplierTypeId(model.ClassName);
                var supplierToUpdate = SupplierMap.ToEntity(model);

                var updated = _supplierRepository.Update(supplierToUpdate);
                if (updated)
                {
                    supplier = _supplierRepository.GetBySupplierID(supplierId);
                    return SupplierMap.ToModel(supplier);
                }
            }

            return null;
        }

        public bool DeleteSupplier(int supplierId)
        {
            if (supplierId < 1)
            {
                throw new Exception("Supplier ID cannot be less than 1.");
            }

            if (this.IsAssociatedSupplier(supplierId))
            {
                throw new Exception("The supplier could not be deleted, please try again.");
            }

            var supplier = _supplierRepository.GetBySupplierID(supplierId);
            if (!Equals(supplier, null))
            {
                if (_supplierRepository.Delete(supplier))
                {
                    return true;
                }
            }

            return false;
        }
        #endregion

        #region Private Methods
        private void GetExpands(NameValueCollection expands, Supplier supplier)
        {
            if (expands.HasKeys())
            {
                ExpandSupplierType(expands, supplier);
            }
        }

        private void ExpandSupplierType(NameValueCollection expands, Supplier supplier)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.SupplierType)))
            {
                if (supplier.SupplierTypeID.HasValue)
                {
                    var supplierType = _supplierTypeRepository.GetBySupplierTypeID(supplier.SupplierTypeID.Value);
                    if (!Equals(supplierType, null))
                    {
                        supplier.SupplierTypeIDSource = supplierType;
                    }
                }
            }
        }

        private void SetFiltering(List<Tuple<string, string, string>> filters, SupplierQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;

                if (filterKey.Equals(FilterKeys.ExternalId)) { SetQueryParameter(SupplierColumn.ExternalID, filterOperator, filterValue, query); }
                if (filterKey.Equals(FilterKeys.ExternalSupplierNumber)) { SetQueryParameter(SupplierColumn.ExternalSupplierNo, filterOperator, filterValue, query); }
                if (filterKey.Equals(FilterKeys.IsActive)) { SetQueryParameter(SupplierColumn.ActiveInd, filterOperator, filterValue, query); }
                if (filterKey.Equals(FilterKeys.Name)) { SetQueryParameter(SupplierColumn.Name, filterOperator, filterValue, query); }
                if (filterKey.Equals(FilterKeys.PortalId)) { SetQueryParameter(SupplierColumn.PortalID, filterOperator, filterValue, query); }
                if (filterKey.Equals(FilterKeys.SupplierTypeId)) { SetQueryParameter(SupplierColumn.SupplierTypeID, filterOperator, filterValue, query); }
                if (filterKey.Equals(FilterKeys.SupplierId)) { SetQueryParameter(SupplierColumn.SupplierID, filterOperator, filterValue, query); }
            }
        }

        private void SetSorting(NameValueCollection sorts, SupplierSortBuilder sortBuilder)
        {
            if (sorts.HasKeys())
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);

                    if (value.Equals(SortKeys.DisplayOrder)) { SetSortDirection(SupplierColumn.DisplayOrder, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (value.Equals(SortKeys.Name)) { SetSortDirection(SupplierColumn.Name, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (value.Equals(SortKeys.SupplierId)) { SetSortDirection(SupplierColumn.SupplierID, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                }
            }
        }

        private void SetQueryParameter(SupplierColumn column, string filterOperator, string filterValue, SupplierQuery query)
        {
            base.SetQueryParameter(column, filterOperator, filterValue, query);
        }

        private void SetSortDirection(SupplierColumn column, string value, SupplierSortBuilder sortBuilder)
        {
            base.SetSortDirection(column, value, sortBuilder);
        }
        #endregion

        #region Znode Version 8.0
        /// <summary>
        /// Checks the supplier is associated with any product or not.
        /// </summary>
        /// <param name="supplierId">Id of the supplier.</param>
        /// <returns>Returns true or false.</returns>
        private bool IsAssociatedSupplier(int supplierId)
        {
            SupplierHelper supplierHelper = new SupplierHelper();
            if (supplierId > 0)
            {
                return supplierHelper.IsAssociatedSupplier(supplierId);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Get supplier type id by class name.
        /// </summary>
        /// <param name="className">class name of supplier type id.</param>
        /// <returns>Returns the supplier type id.</returns>
        private int GetSupplierTypeId(string className)
        {
            SupplierAdmin supplierAdmin = new SupplierAdmin();
            int supplierTypeId = 0;
            if (!string.IsNullOrEmpty(className))
            {
                supplierTypeId = supplierAdmin.GetSupplierTypeId(className);
                return supplierTypeId;
            }
            return supplierTypeId;
        }

        /// <summary>
        /// Get supplier type class name by supplier type id. 
        /// </summary>
        /// <param name="supplierTypeId">Id of the supplier type.</param>
        /// <returns>Returns the supplier type class name.</returns>
        private string GetSupplierTypeClassName(int supplierTypeId)
        {
            SupplierType supplierType = new SupplierType();
            string className = string.Empty;
            if (supplierTypeId > 0)
            {
                supplierType = _supplierTypeRepository.GetBySupplierTypeID(supplierTypeId);
                if (!Equals(supplierType, null))
                {
                    className = supplierType.ClassName;
                }
            }
            return className;
        }
        #endregion
    }
}
