﻿using System;
using System.Collections.Specialized;
using System.Collections.Generic;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Maps;
using ZNode.Libraries.DataAccess.Entities;
using Znode.Libraries.Helpers;
using RMACOnfigurationRepository = ZNode.Libraries.DataAccess.Service.RMAConfigurationService;
using RMAConfigurationAdminRepository = ZNode.Libraries.Admin.RMAConfigurationAdmin;
namespace Znode.Engine.Services
{
    public class RMAConfigurationService : BaseService, IRMAConfigurationService
    {
        #region Private Variables
        private readonly RMACOnfigurationRepository _rmaConfigurationRepository;
        #endregion

        #region Constructor
        public RMAConfigurationService()
        {
            _rmaConfigurationRepository = new RMACOnfigurationRepository();
        }
        #endregion

        #region Public Methods

        public RMAConfigurationModel GetRMAConfiguration(int rmaConfigId, NameValueCollection expands)
        {
            if (rmaConfigId > 0)
            {
                var rmaConfiguration = _rmaConfigurationRepository.GetByRMAConfigID(rmaConfigId);

                return RMAConfigurationMap.ToModel(rmaConfiguration);
            }
            return null;
        }

        public RMAConfigurationListModel GetRMAConfigurations()
        {
            RMAConfigurationListModel model = new RMAConfigurationListModel();
            var rmaConfigurations = new TList<RMAConfiguration>();

            rmaConfigurations = _rmaConfigurationRepository.GetAll();


            // Map each item and add to the list
            foreach (var rmaConfiguration in rmaConfigurations)
            {
                model.RMAConfigurations.Add(RMAConfigurationMap.ToModel(rmaConfiguration));
            }
            return model;
        }

        public RMAConfigurationListModel GetAllRMAConfiguration()
        {
            RMAConfigurationListModel model = new RMAConfigurationListModel();
            var rmaConfigurations = new TList<RMAConfiguration>();

            RMAConfigurationAdminRepository _adminRMAConfig = new RMAConfigurationAdminRepository();
            rmaConfigurations = _adminRMAConfig.GetAllRMAConfiguration();

            // Map each item and add to the list
            foreach (var rmaConfiguration in rmaConfigurations)
            {
                model.RMAConfigurations.Add(RMAConfigurationMap.ToModel(rmaConfiguration));
            }

            return model;
        }

        public RMAConfigurationModel CreateRMAConfiguration(RMAConfigurationModel model)
        {
            if (Equals(model, null))
            {
                throw new Exception("RMAConfiguration model cannot be null.");
            }
            var entity = RMAConfigurationMap.ToEntity(model);

            var rmaconfiguration = _rmaConfigurationRepository.Save(entity);
            if (!Equals(rmaconfiguration, null))
            {
                return RMAConfigurationMap.ToModel(rmaconfiguration);
            }
            return null;
        }

        public RMAConfigurationModel UpdateRMAConfiguration(int rmaConfigId, RMAConfigurationModel model)
        {
            if (rmaConfigId < 1)
            {
                throw new Exception("RMAConfigId ID cannot be less than 1.");
            }

            if (Equals(model, null))
            {
                throw new Exception("RMAConfiguration model cannot be null.");
            }

            var rmaConfiguration = _rmaConfigurationRepository.GetByRMAConfigID(rmaConfigId);

            if (!Equals(rmaConfiguration, null))
            {
                // Set the category ID
                model.RMAConfigId = rmaConfigId;

                var rmaConfigurationToUpdate = RMAConfigurationMap.ToEntity(model);

                var updated = _rmaConfigurationRepository.Update(rmaConfigurationToUpdate);
                if (updated)
                {
                    rmaConfiguration = _rmaConfigurationRepository.GetByRMAConfigID(rmaConfigId);
                    if (!Equals(rmaConfiguration, null))
                    {
                        return RMAConfigurationMap.ToModel(rmaConfiguration);
                    }
                }
            }
            return null;
        }

        #endregion
    }
}
