﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Helpers.Constants;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using MessageConfigRepository = ZNode.Libraries.DataAccess.Service.MessageConfigService;
using MessageTypeRepository = ZNode.Libraries.DataAccess.Service.MessageTypeService;
using PortalRepository = ZNode.Libraries.DataAccess.Service.PortalService;

namespace Znode.Engine.Services
{
    /// <summary>
    /// Message Config Service
    /// </summary>
    public class MessageConfigService : BaseService, IMessageConfigService
    {
        #region Private Variables

        private readonly MessageConfigRepository _messageConfigRepository;
        private readonly MessageTypeRepository _messageTypeRepository;
        private readonly PortalRepository _portalRepository;

        #endregion

        #region Constructor

        public MessageConfigService()
        {
            _messageConfigRepository = new MessageConfigRepository();
            _messageTypeRepository = new MessageTypeRepository();
            _portalRepository = new PortalRepository();
        }

        #endregion

        #region Public Methods

        public MessageConfigModel GetMessageConfig(int messageConfigId, NameValueCollection expands)
        {
            var messageConfig = _messageConfigRepository.GetByMessageID(messageConfigId);
            if (!Equals(messageConfig, null))
            {
                GetExpands(expands, messageConfig);
            }

            return MessageConfigMap.ToModel(messageConfig);
        }

        public MessageConfigModel GetMessageConfig(string key, int portalId, int language, int messageType)
        {
            var messageConfig = _messageConfigRepository.GetByKeyPortalIDLocaleIDMessageTypeID(key, portalId, language, messageType);

            if (messageConfig.Count > 0)
            {
                return MessageConfigMap.ToModel(messageConfig[0]);
            }

            return null;
        }

        public MessageConfigListModel GetMessageConfigs(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new MessageConfigListModel();
            var messageConfigs = new TList<MessageConfig>();
            var tempList = new TList<MessageConfig>();

            var query = new MessageConfigQuery { Junction = String.Empty };
            var sortBuilder = new MessageConfigSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            tempList = _messageConfigRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (Equals(pagingLength, int.MaxValue))
            {
                model.PageSize = model.TotalResults;
            }

            // Now go through the list and get any expands
            foreach (var messageConfig in tempList)
            {
                GetExpands(expands, messageConfig);
                messageConfigs.Add(messageConfig);
            }

            // Map each item and add to the list
            foreach (var m in messageConfigs)
            {
                model.MessageConfigs.Add(MessageConfigMap.ToModel(m));
            }

            return model;
        }

        public MessageConfigListModel GetMessageConfigsByKeys(string keys, NameValueCollection expands, NameValueCollection sorts)
        {
            if (String.IsNullOrEmpty(keys))
            {
                throw new Exception("List of keys cannot be null or empty.");
            }

            var model = new MessageConfigListModel();
            var messageConfigs = new TList<MessageConfig>();
            var tempList = new TList<MessageConfig>();

            var query = new MessageConfigQuery { Junction = String.Empty };
            var sortBuilder = new MessageConfigSortBuilder();

            var list = keys.Split(',');
            foreach (var key in list)
            {
                query.BeginGroup("OR");
                query.AppendEquals(MessageConfigColumn.Key, key);
                query.EndGroup();
            }

            SetSorting(sorts, sortBuilder);

            // Get the initial set
            tempList = _messageConfigRepository.Find(query, sortBuilder);

            // Now go through the list and get any expands
            foreach (var messageConfig in tempList)
            {
                GetExpands(expands, messageConfig);
                messageConfigs.Add(messageConfig);
            }

            // Map each item and add to the list
            foreach (var m in messageConfigs)
            {
                model.MessageConfigs.Add(MessageConfigMap.ToModel(m));
            }

            return model;
        }

        public MessageConfigModel CreateMessageConfig(MessageConfigModel model)
        {
            var query = new MessageConfigQuery();

            if (Equals(model, null))
            {
                throw new Exception("Message config model cannot be null.");
            }

            if (Equals(model.MessageTypeId, 1))
            {
                query.AppendEquals(MessageConfigColumn.Key, model.Key);
                query.AppendNotEquals(MessageConfigColumn.MessageID, model.MessageConfigId.ToString());
                TList<MessageConfig> list = _messageConfigRepository.Find(query);
                if (list.Count > 0)
                {
                    // Display error message
                    throw new Exception("Same key name already exist for the selected portal.");
                }

            }

            var entity = MessageConfigMap.ToEntity(model);
            var messageConfig = _messageConfigRepository.Save(entity);
            return MessageConfigMap.ToModel(messageConfig);
        }

        public MessageConfigModel UpdateMessageConfig(int messageConfigId, MessageConfigModel model)
        {
            if (messageConfigId < 1)
            {
                throw new Exception("Message config ID cannot be less than 1.");
            }

            if (Equals(model, null))
            {
                throw new Exception("Message config model cannot be null.");
            }

            var messageConfig = _messageConfigRepository.GetByMessageID(messageConfigId);
            if (!Equals(messageConfig, null))
            {
                // Set message config ID
                model.MessageConfigId = messageConfigId;

                var messageConfigToUpdate = MessageConfigMap.ToEntity(model);

                var updated = _messageConfigRepository.Update(messageConfigToUpdate);
                if (updated)
                {
                    messageConfig = _messageConfigRepository.GetByMessageID(messageConfigId);
                    return MessageConfigMap.ToModel(messageConfig);
                }
            }

            return null;
        }

        public bool DeleteMessageConfig(int messageId)
        {
            if (messageId < 1)
            {
                throw new Exception("Message config ID cannot be less than 1.");
            }

            var messageConfig = _messageConfigRepository.GetByMessageID(messageId);
            if (!Equals(messageConfig, null))
            {
                return _messageConfigRepository.Delete(messageConfig);
            }

            return false;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Gets the Expands.
        /// </summary>
        /// <param name="expands">NameValueCollection expands</param>
        /// <param name="messageConfig">MessageConfig messageConfig</param>     
        private void GetExpands(NameValueCollection expands, MessageConfig messageConfig)
        {
            if (expands.HasKeys())
            {
                ExpandMessageType(expands, messageConfig);
                ExpandPortal(expands, messageConfig);
            }
        }

        /// <summary>
        /// This method gives the expand for Message Type
        /// </summary>
        /// <param name="expands">NameValueCollection expands</param>
        /// <param name="messageConfig">MessageConfig messageConfig</param>
        private void ExpandMessageType(NameValueCollection expands, MessageConfig messageConfig)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.MessageType)))
            {
                var messageType = _messageTypeRepository.GetByMessageTypeID(messageConfig.MessageTypeID);
                if (!Equals(messageType, null))
                {
                    messageConfig.MessageTypeIDSource = messageType;
                }
            }
        }

        /// <summary>
        /// This method gives the expand for portal
        /// </summary>
        /// <param name="expands">NameValueCollection expands</param>
        /// <param name="messageConfig">MessageConfig messageConfig</param>
        private void ExpandPortal(NameValueCollection expands, MessageConfig messageConfig)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Portal)))
            {
                if ((messageConfig.PortalID > 0))
                {
                    var portals = _portalRepository.GetByPortalID(messageConfig.PortalID);

                    if (!Equals(portals, null))
                    {
                        messageConfig.PortalIDSource = portals;
                    }
                }
            }
        }

        /// <summary>
        /// Set the Filter
        /// </summary>
        /// <param name="filters">List<Tuple<string, string, string>> filters</param>
        /// <param name="query">MessageConfigQuery query</param>
        private void SetFiltering(List<Tuple<string, string, string>> filters, MessageConfigQuery query)
        {
            bool isPortalIdPresent = false;
            string UserName = string.Empty;
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;
                if (!Equals(filterKey, FilterKeys.UserName))
                {
                    if (filterKey.Equals(FilterKeys.PortalId))
                    {
                        isPortalIdPresent = true;
                        SetQueryParameter(MessageConfigColumn.PortalID, filterOperator, filterValue, query);
                    }

                    if (Equals(filterKey, FilterKeys.MessageConfigId)) { SetQueryParameter(MessageConfigColumn.MessageID, filterOperator, filterValue, query); }
                    if (Equals(filterKey, FilterKeys.Key)) { SetQueryParameter(MessageConfigColumn.Key, filterOperator, filterValue, query); }
                    if (Equals(filterKey, FilterKeys.LocaleId)) { SetQueryParameter(MessageConfigColumn.LocaleID, filterOperator, filterValue, query); }
                    if (Equals(filterKey, FilterKeys.MessageTypeId)) { SetQueryParameter(MessageConfigColumn.MessageTypeID, filterOperator, filterValue, query); }
                    if (Equals(filterKey, FilterKeys.Description)) { SetQueryParameter(MessageConfigColumn.Description, filterOperator, filterValue, query); }
                    if (Equals(filterKey, FilterKeys.MessageKey)) { SetQueryParameter(MessageConfigColumn.Key, filterOperator, filterValue, query); }
                }
                else
                {
                    UserName = filterValue;
                }
            }
            if (!isPortalIdPresent)
            {
                string portalIds = GetAvailablePortals(UserName);
                if (!string.IsNullOrEmpty(portalIds) && !Equals(portalIds, "0"))
                {
                    query.AppendIn(SqlUtil.AND, MessageConfigColumn.PortalID, portalIds.Split(','));
                }
            }
        }

        /// <summary>
        /// Set the Sort
        /// </summary>
        /// <param name="sorts">NameValueCollection sorts</param>
        /// <param name="sortBuilder">MessageConfigSortBuilder sortBuilder</param>
        private void SetSorting(NameValueCollection sorts, MessageConfigSortBuilder sortBuilder)
        {
            if (sorts.HasKeys())
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);

                    if (Equals(value, SortKeys.MessageConfigId)) { SetSortDirection(MessageConfigColumn.MessageID, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (Equals(value, SortKeys.Description)) { SetSortDirection(MessageConfigColumn.Description, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (Equals(value, SortKeys.MessageKey)) { SetSortDirection(MessageConfigColumn.Key, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (Equals(value, SortKeys.PortalName)) { SetSortDirection(MessageConfigColumn.PortalID, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                }
            }
        }

        /// <summary>
        /// Set Query Parameter
        /// </summary>
        /// <param name="column">MessageConfigColumn column</param>
        /// <param name="filterOperator">string filterOperator</param>
        /// <param name="filterValue">string filterValue</param>
        /// <param name="query">MessageConfigQuery query</param>
        private void SetQueryParameter(MessageConfigColumn column, string filterOperator, string filterValue, MessageConfigQuery query)
        {
            base.SetQueryParameter(column, filterOperator, filterValue, query);
        }

        /// <summary>
        /// Set Sort Direction
        /// </summary>
        /// <param name="column">MessageConfigColumn column</param>
        /// <param name="value">string value</param>
        /// <param name="sortBuilder">MessageConfigSortBuilder sortBuilder</param>
        private void SetSortDirection(MessageConfigColumn column, string value, MessageConfigSortBuilder sortBuilder)
        {
            base.SetSortDirection(column, value, sortBuilder);
        }

        #endregion
    }
}
