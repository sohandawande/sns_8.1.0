﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    public interface IVendorProductService
    {
        /// <summary>
        /// This method will get the vendor product list.
        /// </summary>
        /// <param name="expands">Expand Collection expands</param>
        /// <param name="filters">Filter criteria</param>
        /// <param name="sorts">sort criteria</param>
        /// <param name="page">page collection</param>
        /// <returns>Returns all vendor product details in VendorProductListModel format</returns>
        VendorProductListModel GetVendorProductList(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// This method will get the mall admin product list.
        /// </summary>
        /// <param name="filters">FilterCollection filters</param>
        /// <param name="sortCollection">SortCollection sortCollection</param>
        /// <param name="pageIndex">int? pageIndex</param>
        /// <param name="recordPerPage">int? recordPerPage</param>
        /// <returns>Return mall admin product list</returns>
        VendorProductListModel GetMallAdminProductList(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
        
        /// <summary>
        /// To Change the Product review state.
        /// </summary>
        /// <param name="model">RejectProductModel model</param>
        /// <param name="state">string state</param>
        /// <returns>Return true or false</returns>
        bool ChangeProductStatus(RejectProductModel model, string state);

        /// <summary>
        /// To Get Reject Product Details
        /// </summary>
        /// <param name="productIds">string productIds</param>
        /// <returns>Return Reject product details</returns>
        RejectProductModel BindRejectProduct(string productIds);

        
        /// <summary>
        /// To Get Vendor Product image List
        /// </summary>
        /// <param name="filters">FilterCollection filters</param>
        /// <param name="sortCollection">SortCollection sortCollection</param>
        /// <param name="pageIndex">int? pageIndex</param>
        /// <param name="recordPerPage">int? recordPerPage</param>
        /// <returns>Return Product image List.</returns>
        ProductAlternateImageListModel GetReviewImageList(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// To Change the Product image review state.
        /// </summary>
        /// <param name="productIds">string productIds</param>
        /// <param name="state">string state</param>
        /// <returns>Return true or false</returns>
        bool UpdateProductImageStatus(string productIds, string state);

        /// <summary>
        /// To Update the vendor product.
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <param name="model">ProductModel model</param>
        /// <returns>Return the Updated vendor product detail in ProductModel format.</returns>
        ProductModel UpdateVendorProduct(int productId, ProductModel model);

        /// <summary>
        /// To Update the vendor product marketing details.
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <param name="model">ProductModel model</param>
        /// <param name="errorCode"> Out Param string errorCode</param>
        /// <returns>Return the Updated vendor product detail in ProductModel format.</returns>
        ProductModel UpdateMarketingDetails(int productId, ProductModel model, out string errorCode);

        /// <summary>
        /// To Get the Product Category Node based on Product Id.
        /// </summary>
        /// <param name="productId">Id for the Product</param>
        /// <returns>Return Product Category Node List in CategoryNodeListModel model</returns>
        CategoryNodeListModel GetCategoryNode(int productId);

        /// <summary>
        /// To Delete the product details based on productId.
        /// </summary>
        /// <param name="productId">Id for the product</param>
        /// <returns>Return true or false</returns>
        bool DeleteProduct(int productId);
    }
}
