﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using ZNode.Libraries.Framework.Business;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using ProductRepository = ZNode.Libraries.DataAccess.Service.ProductService;
using ReviewRepository = ZNode.Libraries.DataAccess.Service.ReviewService;
using Znode.Libraries.Helpers;
using ZNode.Libraries.DataAccess.Custom;
using System.Data;
using Znode.Libraries.Helpers.Constants;
using Znode.Libraries.Helpers.Extensions;

namespace Znode.Engine.Services
{
	public class ReviewService : BaseService, IReviewService
	{
		private readonly ProductRepository _productRepository;
		private readonly ReviewRepository _reviewRepository;

		public ReviewService()
		{
			_productRepository = new ProductRepository();
			_reviewRepository = new ReviewRepository();
		}

		public ReviewModel GetReview(int reviewId, NameValueCollection expands)
		{
			var review = _reviewRepository.GetByReviewID(reviewId);
			if (review != null)
			{
				GetExpands(expands, review);
			}

			return ReviewMap.ToModel(review);
		}

		public ReviewListModel GetReviews(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
		{
            ReviewListModel list = new ReviewListModel();
            string whereClause = StringHelper.GenerateDynamicWhereClause(filters);
            int totalRowCount = 0;
            var pagingStart = 0;
            var pagingLength = 0;
            string orderBy = StringHelper.GenerateDynamicOrderByClause(sorts); 

            pagingStart = Convert.ToInt32(page.Get(PageKeys.Index));
            pagingLength = Convert.ToInt32(page.Get(PageKeys.Size));

            StoredProcedureHelper storedProcedureHelper = new StoredProcedureHelper();
            DataSet resultDataSet = storedProcedureHelper.SP_DataDetailsSpConfig(whereClause, string.Empty, StoredProcedureKeys.SpGetcustReview, orderBy, pagingStart.ToString(), pagingLength.ToString(), out totalRowCount);

            list.Reviews = resultDataSet.Tables[0].ToList<ReviewModel>().ToCollection();
            list.TotalResults = totalRowCount;
            list.PageSize = pagingLength;
            list.PageIndex = pagingStart;
            // Check to set page size equal to the total number of results
            if (Equals(pagingLength , int.MaxValue))
            {
                list.PageSize = list.TotalResults;
            }

            return list;
		}

		public ReviewModel CreateReview(ReviewModel model)
		{
			if (model == null)
			{
				throw new Exception("Review model cannot be null.");
			}

			// Set the create date
			model.CreateDate = DateTime.Now;

			// Set default review status if status is null
			if (String.IsNullOrEmpty(model.Status))
			{
				model.Status = ZNodeConfigManager.SiteConfig.DefaultReviewStatus;
			}

			var entity = ReviewMap.ToEntity(model);
			var review = _reviewRepository.Save(entity);
			return ReviewMap.ToModel(review);
		}

		public ReviewModel UpdateReview(int reviewId, ReviewModel model)
		{
			if (reviewId < 1)
			{
				throw new Exception("Review ID cannot be less than 1.");
			}

			if (model == null)
			{
				throw new Exception("Review model cannot be null.");
			}

			var review = _reviewRepository.GetByReviewID(reviewId);
			if (review != null)
			{
				// Set review ID
				model.ReviewId = reviewId;

				var reviewToUpdate = ReviewMap.ToEntity(model);

				var updated = _reviewRepository.Update(reviewToUpdate);
				if (updated)
				{
					review = _reviewRepository.GetByReviewID(reviewId);
					return ReviewMap.ToModel(review);
				}
			}

			return null;
		}

		public bool DeleteReview(int reviewId)
		{
			if (reviewId < 1)
			{
				throw new Exception("Review ID cannot be less than 1.");
			}

			var review = _reviewRepository.GetByReviewID(reviewId);
			if (review != null)
			{
				return _reviewRepository.Delete(review);
			}

			return false;
		}

		private void GetExpands(NameValueCollection expands, Review review)
		{
			if (expands.HasKeys())
			{
				ExpandProduct(expands, review);
			}
		}

		private void ExpandProduct(NameValueCollection expands, Review review)
		{
			if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Product)))
			{
				if (review.ProductID.HasValue)
				{
					var product = _productRepository.GetByProductID(review.ProductID.Value);
					if (product != null)
					{
						review.ProductIDSource = product;
					}
				}
			}
		}

		private void SetFiltering(List<Tuple<string, string, string>> filters, ReviewQuery query)
		{
			foreach (var tuple in filters)
			{
				var filterKey = tuple.Item1;
				var filterOperator = tuple.Item2;
				var filterValue = tuple.Item3;

				if (filterKey == FilterKeys.AccountId) SetQueryParameter(ReviewColumn.AccountID, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.ProductId) SetQueryParameter(ReviewColumn.ProductID, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.Rating) SetQueryParameter(ReviewColumn.Rating, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.ReviewStatus) SetQueryParameter(ReviewColumn.Status, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.Subject) SetQueryParameter(ReviewColumn.Subject, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.CreateUser) SetQueryParameter(ReviewColumn.CreateUser, filterOperator, filterValue, query);
			}
		}

		private void SetSorting(NameValueCollection sorts, ReviewSortBuilder sortBuilder)
		{
			if (sorts.HasKeys())
			{
				foreach (var key in sorts.AllKeys)
				{
					var value = sorts.Get(key);

					if (key == SortKeys.CreateDate) SetSortDirection(ReviewColumn.CreateDate, value, sortBuilder);
					if (key == SortKeys.ProductId) SetSortDirection(ReviewColumn.ProductID, value, sortBuilder);
					if (key == SortKeys.Rating) SetSortDirection(ReviewColumn.Rating, value, sortBuilder);
					if (key == SortKeys.ReviewId) SetSortDirection(ReviewColumn.ReviewID, value, sortBuilder);
                    if (key == SortKeys.Subject) SetSortDirection(ReviewColumn.Subject, value, sortBuilder);
                    if (key == SortKeys.CreateUser) SetSortDirection(ReviewColumn.CreateUser, value, sortBuilder);
				}
			}
		}

		private void SetQueryParameter(ReviewColumn column, string filterOperator, string filterValue, ReviewQuery query)
		{
			base.SetQueryParameter(column, filterOperator, filterValue, query);
		}

		private void SetSortDirection(ReviewColumn column, string value, ReviewSortBuilder sortBuilder)
		{
			base.SetSortDirection(column, value, sortBuilder);
		}
	}
}
