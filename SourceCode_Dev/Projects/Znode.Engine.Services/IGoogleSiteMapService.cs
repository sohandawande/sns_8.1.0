﻿using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    /// <summary>
    /// Interface for Google Site Map Service
    /// </summary>
    public interface IGoogleSiteMapService
    {
        /// <summary>
        /// Create Google Site Map for generating XML files
        /// </summary>
        /// <param name="model">GoogleSiteMapModel model</param>
        /// <param name="domainname">domainName</param>
        /// <returns>Returns GoogleSiteMapModel</returns>
        GoogleSiteMapModel CreateGoogleSiteMap(GoogleSiteMapModel model, string domainName);
    }
}
