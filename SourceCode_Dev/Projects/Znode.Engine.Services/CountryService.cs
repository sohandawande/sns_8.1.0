﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Helpers.Constants;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using CountryRepository = ZNode.Libraries.DataAccess.Service.CountryService;
using StateRepository = ZNode.Libraries.DataAccess.Service.StateService;

namespace Znode.Engine.Services
{
    public class CountryService : BaseService, ICountryService
    {
        #region Private Variables
        private readonly CountryRepository _countryRepository;
        private readonly StateRepository _stateRepository;
        #endregion

        #region Constructor
        public CountryService()
        {
            _countryRepository = new CountryRepository();
            _stateRepository = new StateRepository();
        }
        #endregion

        #region Public Methods
        public CountryModel GetCountry(string countryCode, NameValueCollection expands)
        {
            var country = _countryRepository.GetByCode(countryCode);
            if (!Equals(country, null))
            {
                GetExpands(expands, country);
            }

            return CountryMap.ToModel(country);
        }

        public CountryListModel GetCountries(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new CountryListModel();
            var countries = new TList<Country>();
            var tempList = new TList<Country>();

            var query = new CountryQuery();
            var sortBuilder = new CountrySortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            tempList = _countryRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (Equals(pagingLength, int.MaxValue))
            {
                model.PageSize = model.TotalResults;
            }

            // Now go through the list and get any expands
            foreach (var country in tempList)
            {
                GetExpands(expands, country);
                countries.Add(country);
            }

            // Map each item and add to the list
            foreach (var c in countries)
            {
                model.Countries.Add(CountryMap.ToModel(c));
            }

            return model;
        }

        public CountryModel CreateCountry(CountryModel model)
        {
            if (Equals(model, null))
            {
                throw new Exception("Country model cannot be null.");
            }

            var entity = CountryMap.ToEntity(model);
            var country = _countryRepository.Save(entity);
            return CountryMap.ToModel(country);
        }

        public CountryModel UpdateCountry(string countryCode, CountryModel model)
        {
            if (String.IsNullOrEmpty(countryCode))
            {
                throw new Exception("Country code cannot be null or empty.");
            }

            if (Equals(model, null))
            {
                throw new Exception("Country model cannot be null.");
            }

            var country = _countryRepository.GetByCode(countryCode);
            if (!Equals(country, null))
            {
                // Gotta set the original country code for the update to work
                var countryToUpdate = CountryMap.ToEntity(model);
                countryToUpdate.OriginalCode = countryCode;

                var updated = _countryRepository.Update(countryToUpdate);
                if (updated)
                {
                    country = _countryRepository.GetByCode(countryCode);
                    return CountryMap.ToModel(country);
                }
            }

            return null;
        }

        public bool DeleteCountry(string countryCode)
        {
            if (String.IsNullOrEmpty(countryCode))
            {
                throw new Exception("Country code cannot be null or empty.");
            }

            var country = _countryRepository.GetByCode(countryCode);
            if (!Equals(country, null))
            {
                return _countryRepository.Delete(country);
            }

            return false;
        }

        public CountryListModel GetActiveCountryByPortalId(int portalId, int billingShippingFlag = 0)
        {
            var portalHelper = new PortalHelper();
            CountryListModel list = new CountryListModel();
            DataSet dataset = portalHelper.GetActiveCountryByPortalId(portalId, billingShippingFlag);
            if (dataset != null && dataset.Tables.Count > 0 && dataset.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in dataset.Tables[0].Rows)
                {
                    CountryModel item = new CountryModel();
                    item.Code = dr[0].ToString();
                    item.Name = dr[1].ToString();
                    item.DisplayOrder = Convert.ToInt32(dr[2]);
                    item.IsActive = Convert.ToBoolean(dr[3]);
                    list.Countries.Add(item);
                }
            }
            return list;
        }

        public CountryListModel GetCountryByPortalId(int portalId)
        {
            PortalCountryHelper portalCountryHelper = new PortalCountryHelper();
            CountryListModel list = new CountryListModel();
            DataSet dataset = portalCountryHelper.GetCountriesByPortalID(portalId);
            if (dataset != null && dataset.Tables.Count > 0 && dataset.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in dataset.Tables[0].Rows)
                {
                    CountryModel item = new CountryModel();
                    item.Code = dr[0].ToString();
                    item.Name = dr[1].ToString();
                    item.DisplayOrder = Convert.ToInt32(dr[2]);
                    item.IsActive = Convert.ToBoolean(dr[3]);
                    list.Countries.Add(item);
                }
            }
            return list;
        }

        public CountryListModel GetShippingActiveCountryByPortalId(int portalId)
        {
            PortalCountryHelper portalCountryHelper = new PortalCountryHelper();
            CountryListModel list = new CountryListModel();
            DataSet dataset = portalCountryHelper.GetCountriesByPortalID(portalId);
            if (dataset != null && dataset.Tables.Count > 0 && dataset.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in dataset.Tables[0].Rows)
                {
                    if (Equals(Convert.ToString(dr[8]), Convert.ToString("True")))
                    {
                        CountryModel item = new CountryModel();
                        item.Code = dr[0].ToString();
                        item.Name = dr[1].ToString();
                        item.DisplayOrder = Convert.ToInt32(dr[2]);
                        item.IsActive = Convert.ToBoolean(dr[3]);
                        list.Countries.Add(item);
                    }
                }
            }
            return list;
        }

        #endregion

        #region Private Region
        private void GetExpands(NameValueCollection expands, Country country)
        {
            if (expands.HasKeys())
            {
                ExpandStates(expands, country);
            }
        }

        private void ExpandStates(NameValueCollection expands, Country country)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.States)))
            {
                var stateQuery = new StateQuery();
                stateQuery.AppendEquals(StateColumn.CountryCode, country.Code);

                var stateSort = new StateSortBuilder();
                stateSort.Append(StateColumn.Name, SqlSortDirection.ASC);

                var states = _stateRepository.Find(stateQuery, stateSort);
                foreach (var s in states)
                {
                    country.States.Add(s);
                }
            }
        }

        private void SetFiltering(List<Tuple<string, string, string>> filters, CountryQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;

                if (filterKey.Equals(FilterKeys.Code)) { SetQueryParameter(CountryColumn.Code, filterOperator, filterValue, query); }
                if (filterKey.Equals(FilterKeys.IsActive)) { SetQueryParameter(CountryColumn.ActiveInd, filterOperator, filterValue, query); }
                if (filterKey.Equals(FilterKeys.Name)) { SetQueryParameter(CountryColumn.Name, filterOperator, filterValue, query); }
            }
        }

        private void SetSorting(NameValueCollection sorts, CountrySortBuilder sortBuilder)
        {
            if (sorts.HasKeys())
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);

                    if (value.Equals(SortKeys.Code)) { SetSortDirection(CountryColumn.Code, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (value.Equals(SortKeys.Name)) { SetSortDirection(CountryColumn.Name, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                }
            }
            else
            {
                SetSortDirection(CountryColumn.Code, sorts[StoredProcedureKeys.sortDirKey], sortBuilder);
            }
        }

        private void SetQueryParameter(CountryColumn column, string filterOperator, string filterValue, CountryQuery query)
        {
            base.SetQueryParameter(column, filterOperator, filterValue, query);
        }

        private void SetSortDirection(CountryColumn column, string value, CountrySortBuilder sortBuilder)
        {
            base.SetSortDirection(column, value, sortBuilder);
        }
        #endregion
    }
}
