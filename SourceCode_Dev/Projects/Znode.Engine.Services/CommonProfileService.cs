﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using Znode.Libraries.Helpers.Extensions;

namespace Znode.Engine.Services
{
    public class CommonProfileService : BaseService, ICommonProfileService
    {
        public ProfileCommonListModel GetProfileStoreAccess(string userName)
        {
            ProfileCommonListModel model = new ProfileCommonListModel();
            if (!string.IsNullOrEmpty(userName))
            {
                ProfileHelper profileHelper = new ProfileHelper();
                DataSet resultDataSet = profileHelper.GetProfileStoreAccess(userName);
                model.Profiles = resultDataSet.Tables[0].ToList<ProfileCommonModel>().ToCollection();
                resultDataSet = null;
                profileHelper = null;
            }
            return model;
        }
    }
}
