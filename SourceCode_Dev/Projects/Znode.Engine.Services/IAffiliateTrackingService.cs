﻿using System;
using System.Collections.Generic;
using System.Data;

namespace Znode.Engine.Services
{
    public interface IAffiliateTrackingService
    {
        /// <summary>
        /// Gets the tracking data on basis of dates provided in filters.
        /// </summary>
        /// <param name="filters">List<Tuple<string, string, string>> of filters</param>
        /// <returns>Returns DataSet</returns>
        DataSet GetAffiliateTrackingData(List<Tuple<string, string, string>> filters);
    }
}
