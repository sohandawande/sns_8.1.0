﻿using System;
using System.Configuration;
using Znode.Engine.Api.Models;
using ZNode.Libraries.ECommerce.SEO;

namespace Znode.Engine.Services
{
    /// <summary>
    /// Google Site Map Service
    /// </summary>
    public partial class GoogleSiteMapService : BaseService, IGoogleSiteMapService
    {
        #region Private Variables

        private string strNamespace = ConfigurationManager.AppSettings["SiteMapNameSpace"];
        private string strGoogleFeedNamespace = ConfigurationManager.AppSettings["GoogleProductFeedNameSpace"];
        private readonly string ItemCategory = "Category";
        private readonly string ItemContentPages = "Content Pages";
        private readonly string ItemUrlset = "urlset";
        //PRFT Custom code Start
        private readonly string ItemProduct = "Product";
        //PRFT Custom code : End

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor for Google Site Map Service
        /// </summary>
        public GoogleSiteMapService()
        {

        }

        #endregion

        #region Public Method

        public GoogleSiteMapModel CreateGoogleSiteMap(GoogleSiteMapModel model, string domainName)
        {
            GoogleSiteMapModel generatedXml = new GoogleSiteMapModel();
            ZNodeXmlSitemap znodeXMLSiteMap = new ZNodeXmlSitemap();
            string portalidSrt = string.Join(",", Array.ConvertAll(model.PortalId, x => x.ToString()));
            string feedType = string.Empty;
            bool isFeed = false;

            switch (model.RootTag)
            {
                case "2":
                    feedType = "Google";
                    isFeed = true;
                    break;
                case "3":
                    feedType = "Bing";
                    isFeed = true;
                    break;
            }
            int fileNameCount = 0;
            string siteMapFile = string.Empty;
            if (isFeed)
            {
                fileNameCount = znodeXMLSiteMap.GetProductList(portalidSrt, this.strGoogleFeedNamespace, model.XmlFileName, model.Date, model.GoogleSiteMapFeedTitle, model.GoogleSiteMapFeedLink, model.GoogleSiteMapFeedDescription, feedType);
            }
            else
            {
                if (Equals(model.RootTagValue, ItemCategory))
                {
                    fileNameCount = znodeXMLSiteMap.GetCategoryList(portalidSrt, model.ChangeFreq, Convert.ToDecimal(model.Priority), ItemUrlset, this.strNamespace, model.XmlFileName, model.Date);
                }
                else if (Equals(model.RootTagValue, ItemContentPages))
                {
                    //fileNameCount = znodeXMLSiteMap.GetCategoryList(portalidSrt, model.ChangeFreq, Convert.ToDecimal(model.Priority), ItemUrlset, this.strNamespace, model.XmlFileName, model.Date);
                    fileNameCount = znodeXMLSiteMap.GetContentPagesList(portalidSrt, model.ChangeFreq, Convert.ToDecimal(model.Priority), ItemUrlset, this.strNamespace, model.XmlFileName, model.Date);
                }
                else if (Equals(model.RootTagValue, ItemProduct))
                {
                    //fileNameCount = znodeXMLSiteMap.GetCategoryList(portalidSrt, model.ChangeFreq, Convert.ToDecimal(model.Priority), ItemUrlset, this.strNamespace, model.XmlFileName, model.Date);
                    fileNameCount = znodeXMLSiteMap.GetProductList(portalidSrt, model.ChangeFreq, Convert.ToDecimal(model.Priority), ItemUrlset, this.strNamespace, model.XmlFileName, model.Date);
                }

            }

            if (Equals(fileNameCount, 0))
            {
                generatedXml.ErrorMessage = "Cannot Generate XML for this file.";
                return generatedXml;
            }

            if (fileNameCount > 0)
            {
                siteMapFile = znodeXMLSiteMap.GenerateGoogleSiteMapIndexFiles(fileNameCount, model.XmlFileName);
            }

            if (!string.IsNullOrEmpty(siteMapFile))
            {
                generatedXml.SuccessXMLGenerationMessage = siteMapFile;
                model.SuccessXMLGenerationMessage = siteMapFile;

                //model.SuccessXMLGenerationMessage = domainName + model.SuccessXMLGenerationMessage.Replace("~", string.Empty);
                model.SuccessXMLGenerationMessage = System.Configuration.ConfigurationManager.AppSettings["SiteMapPath"] + model.SuccessXMLGenerationMessage.Replace("~", string.Empty);
            }
            return model;
        }

        #endregion
    }
}



