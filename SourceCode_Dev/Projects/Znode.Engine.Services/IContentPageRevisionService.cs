﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    public interface IContentPageRevisionService
    {
        /// <summary>
        /// Gets the list of Content Page  Revisions.
        /// </summary>
        /// <param name="expands">NameValueCollection expands</param>
        /// <param name="filters"> List<Tuple<string, string, string>> filters</param>
        /// <param name="sorts">NameValueCollection sorts</param>
        /// <param name="page">NameValueCollection page</param>
        /// <returns>Returns ContentPageRevisionListModel</returns>
        ContentPageRevisionListModel GetContentPageRevisions(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Reverts the Content Page Revision.
        /// </summary>
        /// <param name="pageRevisionId">Id of the Content page revision</param>
        /// <param name="model">ContentPageRevisionModel model</param>
        /// <returns>Returns True or False</returns>
        bool RevertRevision(int pageRevisionId, ContentPageRevisionModel model);

        /// <summary>
        /// Get the content page revision by contentPageId
        /// </summary>
        /// <param name="contentPageId">Id of the Content page</param>
        /// <returns></returns>
        ContentPageRevisionListModel GetContentPageRevisionsById(int contentPageId);

        ContentPageRevisionListModel GetContentPageRevisionsById(int contentPageId, NameValueCollection sorts, NameValueCollection page, out int totalRowCount);
    }
}
