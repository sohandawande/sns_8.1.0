﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using ShippingRuleTypeRepository = ZNode.Libraries.DataAccess.Service.ShippingRuleTypeService;

namespace Znode.Engine.Services
{
    /// <summary>
    /// Shipping Rule Type Service
    /// </summary>
	public class ShippingRuleTypeService : BaseService, IShippingRuleTypeService
	{
        #region Private Variables

        private readonly ShippingRuleTypeRepository _shippingRuleTypeRepository; 

        #endregion

        #region Constructor
        public ShippingRuleTypeService()
        {
            _shippingRuleTypeRepository = new ShippingRuleTypeRepository();
        } 
        #endregion

        #region Public Methods

        public ShippingRuleTypeModel GetShippingRuleType(int shippingRuleTypeId)
        {
            var shippingRuleType = _shippingRuleTypeRepository.GetByShippingRuleTypeID(shippingRuleTypeId);
            return ShippingRuleTypeMap.ToModel(shippingRuleType);
        }

        public ShippingRuleTypeListModel GetShippingRuleTypes(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new ShippingRuleTypeListModel();
            var shippingRuleTypes = new TList<ShippingRuleType>();

            var query = new ShippingRuleTypeQuery();
            var sortBuilder = new ShippingRuleTypeSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            shippingRuleTypes = _shippingRuleTypeRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (Equals(pagingLength, int.MaxValue))
            {
                model.PageSize = model.TotalResults;
            }

            // Map each item and add to the list
            foreach (var s in shippingRuleTypes)
            {
                model.ShippingRuleTypes.Add(ShippingRuleTypeMap.ToModel(s));
            }

            return model;
        }

        public ShippingRuleTypeModel CreateShippingRuleType(ShippingRuleTypeModel model)
        {
            if (Equals(model, null))
            {
                throw new Exception("Shipping rule type model cannot be null.");
            }

            var entity = ShippingRuleTypeMap.ToEntity(model);
            var shippingRuleType = _shippingRuleTypeRepository.Save(entity);
            return ShippingRuleTypeMap.ToModel(shippingRuleType);
        }

        public ShippingRuleTypeModel UpdateShippingRuleType(int shippingRuleTypeId, ShippingRuleTypeModel model)
        {
            if (shippingRuleTypeId < 1)
            {
                throw new Exception("Shipping rule type ID cannot be less than 1.");
            }

            if (Equals(model, null))
            {
                throw new Exception("Shipping rule type model cannot be null.");
            }

            var shippingRuleType = _shippingRuleTypeRepository.GetByShippingRuleTypeID(shippingRuleTypeId);
            if (!Equals(shippingRuleType, null))
            {
                // Set the shipping rule type ID and map
                model.ShippingRuleTypeId = shippingRuleTypeId;
                var shippingRuleTypeToUpdate = ShippingRuleTypeMap.ToEntity(model);

                var updated = _shippingRuleTypeRepository.Update(shippingRuleTypeToUpdate);
                if (updated)
                {
                    shippingRuleType = _shippingRuleTypeRepository.GetByShippingRuleTypeID(shippingRuleTypeId);
                    return ShippingRuleTypeMap.ToModel(shippingRuleType);
                }
            }

            return null;
        }

        public bool DeleteShippingRuleType(int shippingRuleTypeId)
        {
            if (shippingRuleTypeId < 1)
            {
                throw new Exception("Shipping rule type ID cannot be less than 1.");
            }

            var shippingRuleType = _shippingRuleTypeRepository.GetByShippingRuleTypeID(shippingRuleTypeId);
            if (!Equals(shippingRuleType, null))
            {
                return _shippingRuleTypeRepository.Delete(shippingRuleType);
            }

            return false;
        } 

        #endregion

        #region Private Methods
        private void SetFiltering(List<Tuple<string, string, string>> filters, ShippingRuleTypeQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;

                if (Equals(filterKey, FilterKeys.ClassName)) SetQueryParameter(ShippingRuleTypeColumn.ClassName, filterOperator, filterValue, query);
                if (Equals(filterKey, FilterKeys.IsActive)) SetQueryParameter(ShippingRuleTypeColumn.ActiveInd, filterOperator, filterValue, query);
                if (Equals(filterKey, FilterKeys.Name)) SetQueryParameter(ShippingRuleTypeColumn.Name, filterOperator, filterValue, query);
            }
        }

        private void SetSorting(NameValueCollection sorts, ShippingRuleTypeSortBuilder sortBuilder)
        {
            if (sorts.HasKeys())
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);

                    if (Equals(key, SortKeys.Name)) SetSortDirection(ShippingRuleTypeColumn.Name, value, sortBuilder);
                    if (Equals(key, SortKeys.ShippingRuleTypeId)) SetSortDirection(ShippingRuleTypeColumn.ShippingRuleTypeID, value, sortBuilder);
                }
            }
        }

        private void SetQueryParameter(ShippingRuleTypeColumn column, string filterOperator, string filterValue, ShippingRuleTypeQuery query)
        {
            base.SetQueryParameter(column, filterOperator, filterValue, query);
        }

        private void SetSortDirection(ShippingRuleTypeColumn column, string value, ShippingRuleTypeSortBuilder sortBuilder)
        {
            base.SetSortDirection(column, value, sortBuilder);
        } 
        #endregion
	}
}
