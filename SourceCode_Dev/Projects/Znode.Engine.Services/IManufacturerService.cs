﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
	public interface IManufacturerService
	{
		ManufacturerModel GetManufacturer(int manufacturerId);
		ManufacturerListModel GetManufacturers(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
		ManufacturerModel CreateManufacturer(ManufacturerModel model);
		ManufacturerModel UpdateManufacturer(int manufacturerId, ManufacturerModel model);
		bool DeleteManufacturer(int manufacturerId);
	}
}
