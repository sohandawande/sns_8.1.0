﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Helpers;
using Znode.Libraries.Helpers.Constants;
using Znode.Libraries.Helpers.Extensions;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using LuceneDocumentMappingRepository = ZNode.Libraries.DataAccess.Service.LuceneDocumentMappingService;

namespace Znode.Engine.Services
{
    public class ProductSearchSettingService : BaseService, IProductSearchSettingService
    {
        private readonly LuceneDocumentMappingRepository _luceneDocumentMappingRepository;

        public ProductSearchSettingService()
        {
            _luceneDocumentMappingRepository = new LuceneDocumentMappingRepository();
        }

        public ProductLevelSettingListModel GetProductLevelSettings(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            ProductLevelSettingListModel list = new ProductLevelSettingListModel();
            int totalRowCount = 0;
            var pagingStart = 0;
            var pagingLength = 0;
            string orderBy = StringHelper.GenerateDynamicOrderByClause(sorts);

            pagingStart = Convert.ToInt32(page.Get(PageKeys.Index));
            pagingLength = Convert.ToInt32(page.Get(PageKeys.Size));
            string whereClause = StringHelper.GenerateDynamicWhereClause(filters);
            StoredProcedureHelper storedProcedureHelper = new StoredProcedureHelper();

            DataSet resultDataSet = storedProcedureHelper.SP_DataDetailsSpConfig(whereClause, null, StoredProcedureKeys.SpGetProductByProductBoost, orderBy, pagingStart.ToString(), pagingLength.ToString(), out totalRowCount);

            list.Products = resultDataSet.Tables[0].ToList<ProductLevelSettingModel>().ToCollection();
            list.TotalResults = totalRowCount;
            list.PageSize = pagingLength;
            list.PageIndex = pagingStart;
            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                list.PageSize = list.TotalResults;
            }
            return list;
        }

        public CategoryLevelSettingListModel GetCategoryLevelSettings(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            CategoryLevelSettingListModel list = new CategoryLevelSettingListModel();
            int totalRowCount = 0;
            var pagingStart = 0;
            var pagingLength = 0;
            string orderBy = StringHelper.GenerateDynamicOrderByClause(sorts);

            pagingStart = Convert.ToInt32(page.Get(PageKeys.Index));
            pagingLength = Convert.ToInt32(page.Get(PageKeys.Size));
            string whereClause = StringHelper.GenerateDynamicWhereClause(filters);
            StoredProcedureHelper storedProcedureHelper = new StoredProcedureHelper();

            DataSet resultDataSet = storedProcedureHelper.SP_DataDetailsSpConfig(whereClause, null, StoredProcedureKeys.SpGetProductByCategoryBoost, orderBy, pagingStart.ToString(), pagingLength.ToString(), out totalRowCount);

            list.Categories = resultDataSet.Tables[0].ToList<CategoryLevelSettingModel>().ToCollection();
            list.TotalResults = totalRowCount;
            list.PageSize = pagingLength;
            list.PageIndex = pagingStart;
            // Check to set page size equal to the total number of results
            if (Equals(pagingLength, int.MaxValue))
            {
                list.PageSize = list.TotalResults;
            }
            return list;
        }

        public FieldLevelSettingListModel GetFieldLevelSettings(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new FieldLevelSettingListModel();
            var fieldLevelSettings = new TList<LuceneDocumentMapping>();
            var tempList = new TList<LuceneDocumentMapping>();

            var query = new LuceneDocumentMappingQuery();
            var sortBuilder = new LuceneDocumentMappingSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            tempList = _luceneDocumentMappingRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results

            if (Equals(pagingLength, int.MaxValue))
            {
                model.PageSize = model.TotalResults;
            }

            // Now go through the list and get any expands
            foreach (var fieldLevelSetting in tempList)
            {
                fieldLevelSettings.Add(fieldLevelSetting);
            }

            // Map each item and add to the list
            foreach (var r in fieldLevelSettings)
            {
                model.FieldLevelSettings.Add(ProductSearchSettingsMap.ToModel(r));
            }

            return model;
        }

        public bool SaveBoostValues(BoostSearchSettingModel model)
        {
            bool saveResult = false;
            switch (model.Name.ToLower())
            {
                case "product":
                    saveResult = SaveProductBoostValues(model);
                    break;
                case "category":
                    saveResult = SaveProductCategoryBoostValues(model);
                    break;
                case "fields":
                    saveResult = SaveFildsBoostValues(model);
                    break;
            }
            return saveResult;
        }

        #region Private Methods
        private void SetFiltering(List<Tuple<string, string, string>> filters, LuceneDocumentMappingQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;
                if (string.Equals(filterKey, FilterKeys.PropertyName)) SetQueryParameter(LuceneDocumentMappingColumn.PropertyName, filterOperator, filterValue, query);
                if (string.Equals(filterKey, FilterKeys.Boost)) SetQueryParameter(LuceneDocumentMappingColumn.Boost, filterOperator, filterValue, query);
                if (string.Equals(filterKey, FilterKeys.FieldBoostable)) SetQueryParameter(LuceneDocumentMappingColumn.FieldBoostable, filterOperator, filterValue, query);
            }
        }

        private void SetSorting(NameValueCollection sorts, LuceneDocumentMappingSortBuilder sortBuilder)
        {
            if (sorts.HasKeys())
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);
                    if (string.Equals(value, SortKeys.PropertyName)) SetSortDirection(LuceneDocumentMappingColumn.PropertyName, sorts[StoredProcedureKeys.sortDirKey], sortBuilder);
                    if (string.Equals(value, SortKeys.Boost)) SetSortDirection(LuceneDocumentMappingColumn.Boost, key, sortBuilder);
                }
            }
        }
        private void SetQueryParameter(LuceneDocumentMappingColumn column, string filterOperator, string filterValue, LuceneDocumentMappingQuery query)
        {
            base.SetQueryParameter(column, filterOperator, filterValue, query);
        }

        private void SetSortDirection(LuceneDocumentMappingColumn column, string value, LuceneDocumentMappingSortBuilder sortBuilder)
        {
            base.SetSortDirection(column, value, sortBuilder);
        }

        /// <summary>
        /// Save Product level boost values
        /// </summary>
        /// <param name="model">BoostSearchSetting model</param>
        /// <returns>true/false</returns>
        private bool SaveProductBoostValues(BoostSearchSettingModel model)
        {
            var saveResult = false;

            var boostChanges = (Dictionary<int, double>)model.BoostCollection;

            var searchAdmin = new SearchAdmin();

            foreach (var boostChange in boostChanges)
            {
                var productBoost = searchAdmin.GetLuceneGlobalProductBoost(boostChange.Key);
                if (Equals(productBoost, null))
                {
                    var globalProductId = 0;
                    saveResult = searchAdmin.AddLuceneGlobalProductBoost(new LuceneGlobalProductBoost
                    {
                        ProductID = boostChange.Key,
                        Boost = boostChange.Value
                    }, out globalProductId);
                }
                else
                {
                    productBoost.Boost = FormatForTwoDecimalSpots(boostChange.Value);
                    saveResult = searchAdmin.UpdateLuceneGlobalProductBoost(productBoost);
                }
            }

            return saveResult;
        }

        /// <summary>
        /// Save category level boost values
        /// </summary>
        /// <param name="model">BoostSearchSetting model</param>
        /// <returns>true/false</returns>
        private bool SaveProductCategoryBoostValues(BoostSearchSettingModel model)
        {
            var saveResult = false;

            var boostChanges = (Dictionary<int, double>)model.BoostCollection;

            var searchAdmin = new SearchAdmin();

            foreach (var boostChange in boostChanges)
            {
                var productCategoryBoost = searchAdmin.GetLuceneGlobalProductCategoryBoost(boostChange.Key);
                if (Equals(productCategoryBoost, null))
                {
                    var globalProductId = 0;
                    saveResult = searchAdmin.AddLuceneGlobalProductCategoryBoost(new LuceneGlobalProductCategoryBoost
                    {
                        ProductCategoryID = boostChange.Key,
                        Boost = boostChange.Value
                    }, out globalProductId);
                }
                else
                {
                    productCategoryBoost.Boost = FormatForTwoDecimalSpots(boostChange.Value);
                    saveResult = searchAdmin.UpdateLuceneGlobalProductCategoryBoost(productCategoryBoost);
                }
            }

            return saveResult;
        }

        /// <summary>
        /// Save Document level boost values
        /// </summary>
        /// <param name="model">BoostSearchSetting model</param>
        /// <returns>true/false</returns>
        private bool SaveFildsBoostValues(BoostSearchSettingModel model)
        {
            var saveResult = false;

            var boostChanges = (Dictionary<int, double>)model.BoostCollection;

            var searchAdmin = new SearchAdmin();

            foreach (var boostChange in boostChanges)
            {
                var fieldLevelBoost = searchAdmin.GetLuceneFieldLevelBoost(boostChange.Key);
                if (Equals(fieldLevelBoost, null))
                {
                    saveResult = false;
                }
                else
                {
                    fieldLevelBoost.Boost = FormatForTwoDecimalSpots(boostChange.Value);
                    saveResult = searchAdmin.UpdateLuceneFieldLevelBoost(fieldLevelBoost);
                }
            }

            return saveResult;
        }

        /// <summary>
        /// Format For Two Decimal Spots.
        /// </summary>
        /// <param name="value">double value</param>
        /// <returns>Formated value</returns>
        public double FormatForTwoDecimalSpots(double value)
        {
            string truncValue = value.ToString("####.##");
            double result;

            if (double.TryParse(truncValue, out result))
            {
                return result;
            }
            else
            {
                return new double();
            }
        }

        #endregion
    }
}
