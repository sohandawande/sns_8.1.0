﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Helpers.Constants;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using UrlRedirectRepository = ZNode.Libraries.DataAccess.Service.UrlRedirectService;


namespace Znode.Engine.Services
{

    /// <summary>
    /// 
    /// </summary>
    public class UrlRedirectService : BaseService, IUrlRedirectService
    {
        #region Private Variables
        private readonly UrlRedirectRepository _urlRedirectRepository;
        private readonly UrlRedirectAdmin _urlRedirectAdmin; 
        #endregion

        #region Constructor
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public UrlRedirectService()
        {
            _urlRedirectRepository = new UrlRedirectRepository();
            _urlRedirectAdmin = new UrlRedirectAdmin();
        } 
        #endregion

        #region Public Methods
        /// <summary>
        /// Gets an Url redirect
        /// </summary>
        /// <param name="urlRedirectId"></param>
        /// <returns></returns>
        public UrlRedirectModel GetUrlRedirect(int urlRedirectId)
        {
            var urlRedirect = _urlRedirectRepository.GetByUrlRedirectID(urlRedirectId);
            return UrlRedirectMap.ToModel(urlRedirect);
        }

        /// <summary>
        /// Get Url Redirects list.
        /// </summary>
        /// <param name="filters"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        public UrlRedirectListModel GetUrlRedirects(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new UrlRedirectListModel();

            var query = new UrlRedirectQuery();
            var sortBuilder = new UrlRedirectSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            var urlRedirects = _urlRedirectRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (pagingLength.Equals(int.MaxValue))
            {
                model.PageSize = model.TotalResults;
            }

            if (!Equals(urlRedirects, null))
            {
                // Map each item and add to the list
                foreach (var c in urlRedirects)
                {
                    model.UrlRedirects.Add(UrlRedirectMap.ToModel(c));
                }
            }

            return model;
        }

        /// <summary>
        /// Create a new Url Redirect.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public UrlRedirectModel CreateUrlRedirect(UrlRedirectModel model)
        {
            if (Equals(model, null))
            {
                throw new Exception("Url Redirect model cannot be null.");
            }

            if (!this.SEOURLExists(FormattedUrl(model.OldUrl)))
            {
                throw new Exception("Please enter the valid URL in the To URL.");
            }

            if (!this.SEOURLExists(FormattedUrl(model.NewUrl)))
            {
                throw new Exception("Please enter the valid URL in the To URL.");
            }

            if (this.CheckLoopStatus(model.OldUrl))
            {
                throw new Exception("URL Redirect loop is not allowed.");
            }

            if (this.Exists(model))
            {
                throw new Exception("From URL already exists. Unable to update SEO Friendly URL.");
            }

            if (Equals(model.OldUrl, model.NewUrl))
            {
                throw new Exception("Please enter the valid URL.");
            }

            var entity = UrlRedirectMap.ToEntity(model);

            var urlRedirect = _urlRedirectRepository.Save(entity);
            return UrlRedirectMap.ToModel(urlRedirect);
        }

        /// <summary>
        /// Update an existing Url Redirect.
        /// </summary>
        /// <param name="urlRedirectId"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public UrlRedirectModel UpdateUrlRedirect(int urlRedirectId, UrlRedirectModel model)
        {
            if (urlRedirectId < 1)
            {
                throw new Exception("Url Redirect ID cannot be less than 1.");
            }

            if (Equals(model, null))
            {
                throw new Exception("Url Redirect model cannot be null.");
            }

            var urlRedirect = _urlRedirectRepository.GetByUrlRedirectID(urlRedirectId);
            if (!Equals(urlRedirect, null))
            {
                if (!Equals(urlRedirect.NewUrl, model.NewUrl))
                {
                    if (!this.SEOURLExists(FormattedUrl(model.NewUrl)))
                    {
                        throw new Exception("Please enter the valid URL in the To URL.");
                    }
                    
                }
                if (!Equals(urlRedirect.OldUrl, model.OldUrl))
                {
                    if (!this.SEOURLExists(FormattedUrl(model.OldUrl)))
                    {
                        throw new Exception("Please enter the valid URL in the To URL.");
                    } 
                }

                if (Equals(model.OldUrl, model.NewUrl))
                {
                    throw new Exception("Please enter the valid URL.");
                }

                if (this.Exists(model))
                {
                    throw new Exception("From URL already exists. Unable to update SEO Friendly URL.");
                }

                if (this.CheckLoopStatus(model.OldUrl))
                {
                    throw new Exception("URL Redirect loop is not allowed.");
                }

                model.UrlRedirectId = urlRedirectId;

                var urlRedirectToUpdate = UrlRedirectMap.ToEntity(model);

                var updated = _urlRedirectRepository.Update(urlRedirectToUpdate);
                if (updated)
                {
                    urlRedirect = _urlRedirectRepository.GetByUrlRedirectID(urlRedirectId);
                    return UrlRedirectMap.ToModel(urlRedirect);
                }
            }

            return null;
        }

        /// <summary>
        /// Delete an existing Url Redirect.
        /// </summary>
        /// <param name="urlRedirectId"></param>
        /// <returns></returns>
        public bool DeleteUrlRedirect(int urlRedirectId)
        {
            if (urlRedirectId < 1)
            {
                throw new Exception("Url Redirect ID cannot be less than 1.");
            }

            var urlRedirect = _urlRedirectRepository.GetByUrlRedirectID(urlRedirectId);
            return !Equals(urlRedirect, null) && _urlRedirectRepository.Delete(urlRedirect);
        } 
        #endregion

        #region Private Methods

        /// <summary>
        /// Formatted Url Method
        /// </summary>
        /// <param name="url">The value of url</param>
        /// <returns>Returns the formatted url</returns>
        private string FormattedUrl(string url)
        {
            if (url.Length > 0)
            {
                url = url.Replace("~/", string.Empty);
            }
            return url;
        }

        /// <summary>
        /// Check wheather the new url exists or not.
        /// </summary>
        /// <param name="model">model to check.</param>
        /// <returns>Returns True or False.</returns>
        private bool Exists(UrlRedirectModel model)
        {
            return _urlRedirectAdmin.Exists(model.OldUrl, model.NewUrl, model.UrlRedirectId);
        }

        /// <summary>
        /// Checks to see if an SEO URL exists.
        /// </summary>
        /// <param name="seoUrl">SEO url to check.</param>
        /// <returns>Returns true if exist otherwise false.</returns>
        private bool SEOURLExists(string seoUrl)
        {
            SEOUrlHelper seoUrlHelper = new SEOUrlHelper();
            return seoUrlHelper.CheckSeoUrlExists(seoUrl);
        }

        /// <summary>
        /// Check wheater the old url is repeated or not.
        /// </summary>
        /// <param name="oldUrl">url to check.</param>
        /// <returns>True or false.</returns>
        private bool CheckLoopStatus(string oldUrl)
        {
            TList<UrlRedirect> listUrl = _urlRedirectAdmin.GetAll();
            if (listUrl.Count > 0)
            {
                foreach (UrlRedirect urlRedirect in listUrl)
                {
                    if (Equals(oldUrl.ToLower(), urlRedirect.NewUrl.ToLower()))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// Sets the filter for url Redirect.
        /// </summary>
        /// <param name="filters">Filter for Url Redirect.</param>
        /// <param name="query">Query for Url Redirect.</param>
        private void SetFiltering(List<Tuple<string, string, string>> filters, UrlRedirectQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;

                if (Equals(filterKey, FilterKeys.UrlRedirectID))
                { SetQueryParameter(UrlRedirectColumn.UrlRedirectID, filterOperator, filterValue, query); }
                if (Equals(filterKey, FilterKeys.OldUrl))
                { SetQueryParameter(UrlRedirectColumn.OldUrl, filterOperator, filterValue, query); }
                if (Equals(filterKey, FilterKeys.NewUrl))
                { SetQueryParameter(UrlRedirectColumn.NewUrl, filterOperator, filterValue, query); }
                if (Equals(filterKey, FilterKeys.IsActive))
                { SetQueryParameter(UrlRedirectColumn.IsActive, filterOperator, filterValue, query); }
            }
        }

        private void SetSorting(NameValueCollection sorts, UrlRedirectSortBuilder sortBuilder)
        {
            if (sorts.HasKeys())
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);

                    if (value.Equals(SortKeys.UrlRedirectId)) { SetSortDirection(UrlRedirectColumn.UrlRedirectID, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (value.Equals(SortKeys.OldUrl)) { SetSortDirection(UrlRedirectColumn.OldUrl, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (value.Equals(SortKeys.NewUrl)) { SetSortDirection(UrlRedirectColumn.NewUrl, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (value.Equals(SortKeys.IsActive)) { SetSortDirection(UrlRedirectColumn.IsActive, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                }
            }
            else
            {
                SetSortDirection(HighlightColumn.HighlightID, sorts[StoredProcedureKeys.sortDirKey], sortBuilder);
            }
        }

        /// <summary>
        /// Set Query Parameter for url Redirect.
        /// </summary>
        /// <param name="column">Column name</param>
        /// <param name="filterOperator">filter Operator</param>
        /// <param name="filterValue">filter Value</param>
        /// <param name="query">query </param>
        private void SetQueryParameter(UrlRedirectColumn column, string filterOperator, string filterValue, UrlRedirectQuery query)
        {
            base.SetQueryParameter(column, filterOperator, filterValue, query);
        }

        private void SetSortDirection(UrlRedirectColumn column, string value, UrlRedirectSortBuilder sortBuilder)
        {
            base.SetSortDirection(column, value, sortBuilder);
        }
        #endregion

    }
}
