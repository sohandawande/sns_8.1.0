﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Helpers.Constants;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using ManufacturerRepository = ZNode.Libraries.DataAccess.Service.ManufacturerService;

namespace Znode.Engine.Services
{
	public class ManufacturerService : BaseService, IManufacturerService
	{
		private readonly ManufacturerRepository _manufacturerRepository;

		public ManufacturerService()
		{
			_manufacturerRepository = new ManufacturerRepository();
		}

		public ManufacturerModel GetManufacturer(int manufacturerId)
		{
			var manufacturer = _manufacturerRepository.GetByManufacturerID(manufacturerId);
			return ManufacturerMap.ToModel(manufacturer);
		}

		public ManufacturerListModel GetManufacturers(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
		{
			var model = new ManufacturerListModel();
			var manufacturers = new TList<Manufacturer>();

			var query = new ManufacturerQuery();
			var sort = new ManufacturerSortBuilder();
			var pagingStart = 0;
			var pagingLength = 0;

			SetFiltering(filters, query);
			SetSorting(sorts, sort);
			SetPaging(page, model, out pagingStart, out pagingLength);

			// Get the initial set
			var totalResults = 0;
			manufacturers = _manufacturerRepository.Find(query, sort, pagingStart, pagingLength, out totalResults);
			model.TotalResults = totalResults;

			// Check to set page size equal to the total number of results
			if (pagingLength == int.MaxValue)
			{
				model.PageSize = model.TotalResults;
			}

			// Map each item and add to the list
			foreach (var m in manufacturers)
			{
				model.Manufacturers.Add(ManufacturerMap.ToModel(m));
			}

			return model;
		}

		public ManufacturerModel CreateManufacturer(ManufacturerModel model)
		{
			if (model == null)
			{
				throw new Exception("Manufacturer model cannot be null.");
			}

			var entity = ManufacturerMap.ToEntity(model);
			var manufacturer = _manufacturerRepository.Save(entity);
			return ManufacturerMap.ToModel(manufacturer);
		}

		public ManufacturerModel UpdateManufacturer(int manufacturerId, ManufacturerModel model)
		{
			if (manufacturerId < 1)
			{
				throw new Exception("Manufacturer ID cannot be less than 1.");
			}

			if (model == null)
			{
				throw new Exception("Manufacturer model cannot be null.");
			}

			var manufacturer = _manufacturerRepository.GetByManufacturerID(manufacturerId);
			if (manufacturer != null)
			{
				// Set manufacturer ID
				model.ManufacturerId = manufacturerId;

				var manufacturerToUpdate = ManufacturerMap.ToEntity(model);

				var updated = _manufacturerRepository.Update(manufacturerToUpdate);
				if (updated)
				{
					manufacturer = _manufacturerRepository.GetByManufacturerID(manufacturerId);
					return ManufacturerMap.ToModel(manufacturer);
				}
			}

			return null;
		}

		public bool DeleteManufacturer(int manufacturerId)
		{
			if (manufacturerId < 1)
			{
				throw new Exception("Manufacturer ID cannot be less than 1.");
			}

			var manufacturer = _manufacturerRepository.GetByManufacturerID(manufacturerId);
			if (manufacturer != null)
			{
				return _manufacturerRepository.Delete(manufacturer);
			}

			return false;
		}

		private void SetFiltering(List<Tuple<string, string, string>> filters, ManufacturerQuery query)
		{
			foreach (var tuple in filters)
			{
				var filterKey = tuple.Item1;
				var filterOperator = tuple.Item2;
				var filterValue = tuple.Item3;

                if (filterKey == FilterKeys.ManufacturerId) SetQueryParameter(ManufacturerColumn.ManufacturerID, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.IsActive) SetQueryParameter(ManufacturerColumn.ActiveInd, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.Name) SetQueryParameter(ManufacturerColumn.Name, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.PortalId) SetQueryParameter(ManufacturerColumn.PortalID, filterOperator, filterValue, query);
			}
		}

		private void SetSorting(NameValueCollection sorts, ManufacturerSortBuilder sortBuilder)
		{
			if (sorts.HasKeys())
			{
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);

                    if (Equals(value, SortKeys.DisplayOrder)) { SetSortDirection(ManufacturerColumn.DisplayOrder, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (Equals(value, SortKeys.ManufacturerId)) { SetSortDirection(ManufacturerColumn.ManufacturerID, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (Equals(value, SortKeys.Name)) { SetSortDirection(ManufacturerColumn.Name, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                }
			}
		}

		private void SetQueryParameter(ManufacturerColumn column, string filterOperator, string filterValue, ManufacturerQuery query)
		{
			base.SetQueryParameter(column, filterOperator, filterValue, query);
		}

		private void SetSortDirection(ManufacturerColumn column, string value, ManufacturerSortBuilder sortBuilder)
		{
			base.SetSortDirection(column, value, sortBuilder);
		}
	}
}
