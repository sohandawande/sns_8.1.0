﻿using System.Web;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.PaymentHelper;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Services
{
    public class RotateKeyService : BaseService, IRotateKeyService
    {
        public bool GenerateNewRotateKey()
        {
            StoreSettingsAdmin storeAdmin = new StoreSettingsAdmin();
            ZNodeEncryption encrypt = new ZNodeEncryption();
            PaymentSettingService serv = new PaymentSettingService();
            
            TList<Portal> portalList = storeAdmin.GetAllStores();

            foreach (Portal portal in portalList)
            {
                // UPS Shipping Settings
                if (!string.IsNullOrEmpty(portal.UPSUserName))
                {
                    portal.UPSUserName = encrypt.DecryptData(portal.UPSUserName);
                }

                if (!string.IsNullOrEmpty(portal.UPSPassword))
                {
                    portal.UPSPassword = encrypt.DecryptData(portal.UPSPassword);
                }

                if (!string.IsNullOrEmpty(portal.UPSKey))
                {
                    portal.UPSKey = encrypt.DecryptData(portal.UPSKey);
                }

                // FedEx Shipping Settings
                if (!string.IsNullOrEmpty(portal.FedExAccountNumber))
                {
                    portal.FedExAccountNumber = encrypt.DecryptData(portal.FedExAccountNumber);
                }

                if (!string.IsNullOrEmpty(portal.FedExMeterNumber))
                {
                    portal.FedExMeterNumber = encrypt.DecryptData(portal.FedExMeterNumber);
                }

                if (!string.IsNullOrEmpty(portal.FedExProductionKey))
                {
                    portal.FedExProductionKey = encrypt.DecryptData(portal.FedExProductionKey);
                }

                if (!string.IsNullOrEmpty(portal.FedExSecurityCode))
                {
                    portal.FedExSecurityCode = encrypt.DecryptData(portal.FedExSecurityCode);
                }

                if (!string.IsNullOrEmpty(portal.SMTPUserName))
                {
                    portal.SMTPUserName = encrypt.DecryptData(portal.SMTPUserName);
                }

                if (!string.IsNullOrEmpty(portal.SMTPPassword))
                {
                    portal.SMTPPassword = encrypt.DecryptData(portal.SMTPPassword);
                }
            }

            TList<PaymentSetting> paymentSettingList = serv.GetAll();

            foreach (PaymentSetting paymentSetting in paymentSettingList)
            {
                if (!string.IsNullOrEmpty(paymentSetting.TransactionKey) &&
                    paymentSetting.PaymentTypeID != (int)ZNode.Libraries.ECommerce.Entities.PaymentType.PURCHASE_ORDER &&
                    paymentSetting.PaymentTypeID != (int)ZNode.Libraries.ECommerce.Entities.PaymentType.PAYPAL &&
                    paymentSetting.GatewayTypeID != (int)GatewayType.CYBERSOURCE &&
                    paymentSetting.GatewayTypeID != (int)GatewayType.PAYPAL)
                {
                    paymentSetting.TransactionKey = encrypt.DecryptData(paymentSetting.TransactionKey);
                }

                if (!string.IsNullOrEmpty(paymentSetting.GatewayPassword))
                {
                    paymentSetting.GatewayPassword = encrypt.DecryptData(paymentSetting.GatewayPassword);
                }

                if (!string.IsNullOrEmpty(paymentSetting.GatewayUsername))
                {
                    paymentSetting.GatewayUsername = encrypt.DecryptData(paymentSetting.GatewayUsername);
                }
                if (!string.IsNullOrEmpty(paymentSetting.Vendor))
                {
                    if (paymentSetting.PaymentTypeID == (int)ZNode.Libraries.ECommerce.Entities.PaymentType.COD)
                        paymentSetting.Vendor = encrypt.DecryptData(paymentSetting.Vendor);
                }
            }

            PasswordLogService passLog = new PasswordLogService();
            TList<PasswordLog> passwordLogList = passLog.GetAll();

            passwordLogList.ForEach(x => x.Password = encrypt.DecryptData(x.Password));

            // Rotate Key
            encrypt.RotateKey();

            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity(
                (int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.StoreKeyRotated);

            foreach (Portal portal in portalList)
            {
                // UPS Shipping Settings
                if (!string.IsNullOrEmpty(portal.UPSUserName))
                {
                    portal.UPSUserName = encrypt.EncryptData(portal.UPSUserName);
                }

                if (!string.IsNullOrEmpty(portal.UPSPassword))
                {
                    portal.UPSPassword = encrypt.EncryptData(portal.UPSPassword);
                }

                if (!string.IsNullOrEmpty(portal.UPSKey))
                {
                    portal.UPSKey = encrypt.EncryptData(portal.UPSKey);
                }

                // FedEx Shipping Settings
                if (!string.IsNullOrEmpty(portal.FedExAccountNumber))
                {
                    portal.FedExAccountNumber = encrypt.EncryptData(portal.FedExAccountNumber);
                }

                if (!string.IsNullOrEmpty(portal.FedExMeterNumber))
                {
                    portal.FedExMeterNumber = encrypt.EncryptData(portal.FedExMeterNumber);
                }

                if (!string.IsNullOrEmpty(portal.FedExProductionKey))
                {
                    portal.FedExProductionKey = encrypt.EncryptData(portal.FedExProductionKey);
                }

                if (!string.IsNullOrEmpty(portal.FedExSecurityCode))
                {
                    portal.FedExSecurityCode = encrypt.EncryptData(portal.FedExSecurityCode);
                }

                if (!string.IsNullOrEmpty(portal.SMTPUserName))
                {
                    portal.SMTPUserName = encrypt.EncryptData(portal.SMTPUserName);
                }

                if (!string.IsNullOrEmpty(portal.SMTPPassword))
                {
                    portal.SMTPPassword = encrypt.EncryptData(portal.SMTPPassword);
                }

                storeAdmin.UpdateStore(portal);
            }

            // Update Payment Option Settings
            foreach (PaymentSetting paymentSetting in paymentSettingList)
            {
                if (!string.IsNullOrEmpty(paymentSetting.TransactionKey) &&
                    paymentSetting.PaymentTypeID != (int)ZNode.Libraries.ECommerce.Entities.PaymentType.PURCHASE_ORDER &&
                    paymentSetting.PaymentTypeID != (int)ZNode.Libraries.ECommerce.Entities.PaymentType.PAYPAL &&
                    paymentSetting.GatewayTypeID != (int)GatewayType.CYBERSOURCE &&
                    paymentSetting.GatewayTypeID != (int)GatewayType.PAYPAL)
                {
                    paymentSetting.TransactionKey = encrypt.EncryptData(paymentSetting.TransactionKey);
                }

                if (!string.IsNullOrEmpty(paymentSetting.GatewayPassword))
                {
                    paymentSetting.GatewayPassword = encrypt.EncryptData(paymentSetting.GatewayPassword);
                }

                if (!string.IsNullOrEmpty(paymentSetting.GatewayUsername))
                {
                    paymentSetting.GatewayUsername = encrypt.EncryptData(paymentSetting.GatewayUsername);
                }
                if (!string.IsNullOrEmpty(paymentSetting.Vendor))
                {
                    if (paymentSetting.PaymentTypeID == (int)ZNode.Libraries.ECommerce.Entities.PaymentType.COD)
                        paymentSetting.Vendor = encrypt.EncryptData(paymentSetting.Vendor);
                }
                serv.Update(paymentSetting);
            }

            passwordLogList.ForEach(x => x.Password = encrypt.EncryptData(x.Password));

            passLog.Update(passwordLogList);

            // Log Activity
            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity(
                (int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject,
                UserStoreAccess.GetCurrentUserName(), null, null, null,
                HttpContext.Current.User.Identity.Name);

            return true;
        }
    }
}
