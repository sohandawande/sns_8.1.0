﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Custom;
using Znode.Libraries.Helpers.Extensions;
using PRFT.Engine.ERP;
using Znode.Libraries.Helpers;
using Znode.Engine.Services.Constants;
using System.Collections.Specialized;
using Znode.Libraries.Helpers.Constants;

namespace Znode.Engine.Services
{
    public partial class AccountService : BaseService, IAccountService
    {
        public Collection<PRFTSalesRepresentative> GetSalesRepInfoByRoleName(string roleName)
        {
            Collection<PRFTSalesRepresentative> salesRepList = new Collection<PRFTSalesRepresentative>();
            try
            {
                AccountHelper accHelperObj = new AccountHelper();
                DataSet dataset = accHelperObj.GetSalesRepInfoByRoleName(roleName);
                salesRepList = dataset.Tables[0].ToList<PRFTSalesRepresentative>().ToCollection();
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("Class-AccountService:Method-GetSalesRepInfoByRoleName:- " + ex.Message);
            }
            return salesRepList;
        }

        public PRFTERPAccountDetailsModel GetAccountDetailsFromERP(string ExternalId)
        {
            try
            {
                PRFTERPAccountDetailsModel accountDetails = new PRFTERPAccountDetailsModel();
                ERPCustomerService erpCustomerObj = new ERPCustomerService();
                accountDetails = erpCustomerObj.GetAccountDetailsFromERP(ExternalId);
                return accountDetails;
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("Class-AccountService:Method-GetSalesRepInfoByRoleName:- " + ex.Message);
            }

            return new PRFTERPAccountDetailsModel();
        }

        /// <summary>
        /// Gets the list of address extension details for the addresses.
        /// </summary>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public Collection<PRFTAddressExtnModel> GetAddressExtnDetailsByAccountId(int accountId)
        {
            AccountHelper objAccountHelperHelper = new AccountHelper();
            DataSet dsAddressExtnList = objAccountHelperHelper.GetAddressExtnDetailsByAccountId(accountId);
            Collection<PRFTAddressExtnModel> prftAddressExtnModel = new Collection<PRFTAddressExtnModel>();
            prftAddressExtnModel = GetAddressExtnModel(dsAddressExtnList);
            return prftAddressExtnModel;
        }

        private Collection<PRFTAddressExtnModel> GetAddressExtnModel(DataSet dsAddressExtnList)
        {
            Collection<PRFTAddressExtnModel> addressExtnList = new Collection<PRFTAddressExtnModel>();
            try
            {
                if (dsAddressExtnList != null && dsAddressExtnList.Tables[0] != null && dsAddressExtnList.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow addressExtnRow in dsAddressExtnList.Tables[0].Rows)
                    {
                        PRFTAddressExtnModel addressExtnModel = new PRFTAddressExtnModel();
                        addressExtnModel.AddressExtnID = Convert.ToInt32(addressExtnRow["AddressExtnID"]);
                        addressExtnModel.AddressID = Convert.ToInt32(addressExtnRow["AddressID"]);
                        addressExtnModel.ERPAddressID = Convert.ToString(addressExtnRow["ERPAddressID"]);
                        addressExtnModel.ModifiedDate = Convert.ToDateTime(addressExtnRow["ModifiedDate"]);
                        addressExtnModel.CreatedDate = Convert.ToDateTime(addressExtnRow["CreatedDate"]);
                        addressExtnModel.Custom1 = Convert.ToString(addressExtnRow["Custom1"]);
                        addressExtnModel.Custom2 = Convert.ToString(addressExtnRow["Custom2"]);
                        addressExtnModel.Custom3 = Convert.ToString(addressExtnRow["Custom3"]);
                        addressExtnModel.Custom4 = Convert.ToString(addressExtnRow["Custom4"]);
                        addressExtnModel.Custom5 = Convert.ToString(addressExtnRow["Custom5"]);
                        addressExtnList.Add(addressExtnModel);
                    }

                }
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("Class-AccountService:Method-GetAddressExtnDetailsByAccountId:- " + ex.Message);
            }
            return addressExtnList;
        }

        private void UpdateAddressExtensionDetails(AccountModel accountModel)
        {
            var PRFTAddressExtn = GetAddressExtnDetailsByAccountId(accountModel.AccountId);
            foreach (var address in accountModel.Addresses)
            {
                var addressExtn = PRFTAddressExtn.Where(x => x.AddressID == address.AddressId).FirstOrDefault();
                if (addressExtn != null)
                {
                    address.AddressExtn.AddressExtnID = addressExtn.AddressExtnID;
                    address.AddressExtn.AddressID = addressExtn.AddressID;
                    address.AddressExtn.CreatedDate = addressExtn.CreatedDate;
                    address.AddressExtn.Custom1 = addressExtn.Custom1;
                    address.AddressExtn.Custom2 = addressExtn.Custom2;
                    address.AddressExtn.Custom3 = addressExtn.Custom3;
                    address.AddressExtn.Custom4 = addressExtn.Custom4;
                    address.AddressExtn.Custom5 = addressExtn.Custom5;
                    address.AddressExtn.ERPAddressID = addressExtn.ERPAddressID;
                    address.AddressExtn.ModifiedDate = addressExtn.ModifiedDate;
                };
            }
        }

        public AccountListModel GetSubUserList(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            List<Tuple<string, string, string>> filterList = new List<Tuple<string, string, string>>();
            AccountListModel list = new AccountListModel();
            string innerWhereClause = string.Empty;

            string whereClause = StringHelper.GenerateDynamicWhereClause(filters);

            int totalRowCount = 0;
            var pagingStart = 0;
            var pagingLength = 0;
            string orderBy = StringHelper.GenerateDynamicOrderByClause(sorts);

            pagingStart = Convert.ToInt32(page.Get(PageKeys.Index));
            pagingLength = Convert.ToInt32(page.Get(PageKeys.Size));

            AccountHelper accHelperObj = new AccountHelper();
            StoredProcedureHelper storedProcedureHelper = new StoredProcedureHelper();
            DataSet resultDataSet = storedProcedureHelper.SP_DataDetailsSpConfig(whereClause, string.Empty, StoredProcedureKeys.SpGetSubUserListList, orderBy, pagingStart.ToString(), pagingLength.ToString(), out totalRowCount, null, innerWhereClause);
            list.Accounts = resultDataSet.Tables[0].ToList<AccountModel>().ToCollection();
            list.TotalResults = totalRowCount;
            list.PageSize = pagingLength;
            list.PageIndex = pagingStart;
            // Check to set page size equal to the total number of results
            if (Equals(pagingLength, int.MaxValue))
            {
                list.PageSize = list.TotalResults;
            }

            return list;
        }
    }
}
