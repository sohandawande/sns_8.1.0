﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Helpers;
using Znode.Libraries.Helpers.Constants;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using ProductAttributeRepository = ZNode.Libraries.DataAccess.Service.ProductAttributeService;

namespace Znode.Engine.Services
{
    public class ProductAttributesService : BaseService, IProductAttributesService
    {
        private readonly ProductAttributeRepository _productAttributesRepository;

        public ProductAttributesService()
		{
            _productAttributesRepository = new ProductAttributeRepository();
		}

		public AttributeModel GetAttribute(int attributeId)
		{
            var attributes = _productAttributesRepository.GetByAttributeId(attributeId);
			return AttributeMap.ToModel(attributes);
		}

		public ProductAttributeListModel GetAttributes(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
		{
			var model = new ProductAttributeListModel();
			var attributes = new TList<ProductAttribute>();

            var query = new ProductAttributeQuery();
            var sortBuilder = new ProductAttributeSortBuilder();
			var pagingStart = 0;
			var pagingLength = 0;

			SetFiltering(filters, query);
			SetSorting(sorts, sortBuilder);
			SetPaging(page, model, out pagingStart, out pagingLength);

			// Get the initial set
			var totalResults = 0;
            attributes = _productAttributesRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
			model.TotalResults = totalResults;

			// Check to set page size equal to the total number of results
			if (pagingLength == int.MaxValue)
			{
				model.PageSize = model.TotalResults;
			}

			// Map each item and add to the list
			foreach (var a in attributes)
			{
				model.Attributes.Add(AttributeMap.ToModel(a));
			}

			return model;
		}

        public ProductAttributeListModel GetAttributesByAttributeTypeId(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new ProductAttributeListModel();
            var sortBuilder = new ProductAttributeSortBuilder();
            int totalRowCount = 0;
            var pagingStart = 0;
            var pagingLength = 0;
            string whereClause = StringHelper.GenerateDynamicWhereClause(filters);
            string orderBy = StringHelper.GenerateDynamicOrderByClause(sorts);
            pagingStart = Convert.ToInt32(page.Get(PageKeys.Index));
            pagingLength = Convert.ToInt32(page.Get(PageKeys.Size));

            StoredProcedureHelper storedProcedureHelper = new StoredProcedureHelper();
            DataSet dataSet = storedProcedureHelper.SP_DataDetailsSpConfig(whereClause, string.Empty/*HavingClause*/, StoredProcedureKeys.SPGetAttributeTypeWiseAttributes, orderBy, pagingStart.ToString(), pagingLength.ToString(), out totalRowCount);
            
            model = AttributeMap.ToModelList(dataSet);
            model.TotalResults = totalRowCount;
            model.PageSize = pagingLength;
            model.PageIndex = pagingStart;
           
            if (pagingLength == int.MaxValue)
            {
                model.PageSize = model.TotalResults;
            }
            return model;
        }

		public AttributeModel CreateAttributes(AttributeModel model)
		{
			if (model == null)
			{
				throw new Exception("Attributes model cannot be null.");
			}

			var entity = AttributeMap.ToEntity(model);
            var attributes = _productAttributesRepository.Save(entity);
			return AttributeMap.ToModel(attributes);
		}

		public AttributeModel UpdateAttributes(int attributeId, AttributeModel model)
		{
			if (attributeId < 1)
			{
				throw new Exception("Attributes ID cannot be less than 1.");
			}

			if (model == null)
			{
				throw new Exception("Attributes model cannot be null.");
			}

            var attributes = _productAttributesRepository.GetByAttributeId(attributeId);
			if (attributes != null)
			{
				// Set attributes ID
				model.AttributeId = attributeId;

                //var attributesToUpdate = AttributeMap.ToEntity(model);
                var attributesToUpdate = AttributeMap.ToEntity(model, attributes);

                var updated = _productAttributesRepository.Update(attributesToUpdate);
				if (updated)
				{
                    attributes = _productAttributesRepository.GetByAttributeId(attributeId);
					return AttributeMap.ToModel(attributes);
				}
			}

			return null;
		}

		public bool DeleteAttributes(int attributeId)
		{
			if (attributeId < 1)
			{
				throw new Exception("Attributes  ID cannot be less than 1.");
			}

            var attributes = _productAttributesRepository.GetByAttributeId(attributeId);
			if (attributes != null)
			{
                if (_productAttributesRepository.Delete(attributes))
                {
                    return true;
                }
                else
                {
                    throw new Exception("Delete action could not be completed because the Product Attribute Value is in use.");
                }
                
			}
			return false;
		}

        private void SetFiltering(List<Tuple<string, string, string>> filters, ProductAttributeQuery query)
		{
			foreach (var tuple in filters)
			{
				var filterKey = tuple.Item1;
				var filterOperator = tuple.Item2;
				var filterValue = tuple.Item3;

                if (filterKey == FilterKeys.IsActive) SetQueryParameter(ProductAttributeColumn.IsActive, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.AttributeTypeId) SetQueryParameter(ProductAttributeColumn.AttributeTypeId, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.Name) SetQueryParameter(ProductAttributeColumn.Name, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.ExternalId) SetQueryParameter(ProductAttributeColumn.ExternalId, filterOperator, filterValue, query);
			}
		}

        private void SetSorting(NameValueCollection sorts, ProductAttributeSortBuilder sortBuilder)
		{
			if (sorts.HasKeys())
			{
				foreach (var key in sorts.AllKeys)
				{
					var value = sorts.Get(key);

                    if (key == SortKeys.AttributeId) SetSortDirection(ProductAttributeColumn.AttributeId, value, sortBuilder);
                    if (key == SortKeys.DisplayOrder) SetSortDirection(ProductAttributeColumn.DisplayOrder, value, sortBuilder);
                    if (key == SortKeys.DisplayOrder) SetSortDirection(ProductAttributeColumn.DisplayOrder, value, sortBuilder);
                    if (key == SortKeys.Name) SetSortDirection(ProductAttributeColumn.Name, value, sortBuilder);
				}
			}
		}

        private void SetQueryParameter(ProductAttributeColumn column, string filterOperator, string filterValue, ProductAttributeQuery query)
		{
			base.SetQueryParameter(column, filterOperator, filterValue, query);
		}

        private void SetSortDirection(ProductAttributeColumn column, string value, ProductAttributeSortBuilder sortBuilder)
		{
			base.SetSortDirection(column, value, sortBuilder);
		}
    }
}
