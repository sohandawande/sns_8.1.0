﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    /// <summary>
    /// Interface for Zip Code Service
    /// </summary>
    public interface IZipCodeService
    {
        /// <summary>
        /// Get Zip Codes
        /// </summary>
        /// <param name="filters">filters</param>
        /// <param name="sorts">NameValueCollection sort</param>
        /// <param name="page">NameValueCollection page</param>
        /// <returns>Returns Zip codes</returns>
        ZipCodeListModel GetZipCodes(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
    }
}
