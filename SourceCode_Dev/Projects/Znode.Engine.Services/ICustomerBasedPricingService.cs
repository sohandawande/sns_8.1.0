﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    /// <summary>
    /// Interface Service for Customer Based Pricing
    /// </summary>
    public interface ICustomerBasedPricingService
    {
        /// <summary>
        /// Get Customer Based Pricing Service.
        /// </summary>
        /// <param name="expands">NameValueCollection</param>
        /// <param name="filters"> List of filters of customer pricing</param>
        /// <param name="sorts">NameValueCollection for customer pricing</param>
        /// <param name="page">page collection for of customer pricing</param>
        /// <returns>CustomerBasedPricingListModel</returns>
        CustomerBasedPricingListModel GetCustomerBasedPricing(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Get Customer Based Pricing Product .
        /// </summary>
        /// <param name="expands">NameValueCollection</param>
        /// <param name="filters"> List of filters of customer pricing</param>
        /// <param name="sorts">NameValueCollection for customer pricing</param>
        /// <param name="page">page collection for of customer pricing</param>
        /// <returns>CustomerBasedPricingListModel</returns>
        CustomerBasedPricingListModel GetCustomerPricingProduct(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
    }
}
