﻿using System;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using ZNode.Libraries.DataAccess.Data;
using System.Linq;

namespace Znode.Engine.Services
{
	public abstract class BaseService
	{
		protected void SetPaging(NameValueCollection page, BaseListModel model, out int pagingStart, out int pagingLength)
		{
			// We use int.MaxValue for the paging length to ensure we always get total results back
			pagingStart = 0;
			pagingLength = int.MaxValue;

			if (page != null && page.HasKeys())
			{
				// Only do if both index and size are given
				if (!String.IsNullOrEmpty(page.Get(PageKeys.Index)) && !String.IsNullOrEmpty(page.Get(PageKeys.Size)))
				{
					var pageIndex = Convert.ToInt32(page.Get(PageKeys.Index));
					var pageSize = Convert.ToInt32(page.Get(PageKeys.Size));

					model.PageIndex = pageIndex;
					model.PageSize = pageSize;

					pagingStart = pageSize * pageIndex;
					pagingLength = pageSize;
				}
			}
			else
			{
				model.PageIndex = pagingStart;
				model.PageSize = pagingLength;
			}
		}

		protected void SetQueryParameter<TEntityColumn>(TEntityColumn column, string filterOperator, string filterValue, SqlFilterBuilder<TEntityColumn> query)
		{
			const string and = "AND";
			var value = filterValue == "null" ? null : filterValue;

			if (value == null)
			{
				// Nulls are special cases
				if (filterOperator == FilterOperators.Equals) query.AppendIsNull(and, column);
				if (filterOperator == FilterOperators.NotEquals) query.AppendIsNotNull(and, column);
			}
            else if (value == string.Empty)
            {
                // Emptys are special cases
                if (filterOperator == FilterOperators.Equals) query.AppendEmpty(and, column);
                if (filterOperator == FilterOperators.NotEquals) query.AppendIsNotEmpty(and, column);
            }
            else
            {
                if (filterOperator == FilterOperators.Equals) query.AppendEquals(and, column, value);
                if (filterOperator == FilterOperators.NotEquals) query.AppendNotEquals(and, column, value);
                if (filterOperator == FilterOperators.Contains) query.AppendContains(and, column, value);
                if (filterOperator == FilterOperators.GreaterThan) query.AppendGreaterThan(and, column, value);
                if (filterOperator == FilterOperators.GreaterThanOrEqual) query.AppendGreaterThanOrEqual(and, column, value);
                if (filterOperator == FilterOperators.LessThan) query.AppendLessThan(and, column, value);
                if (filterOperator == FilterOperators.LessThanOrEqual) query.AppendLessThanOrEqual(and, column, value);
                if (filterOperator == FilterOperators.StartsWith) query.AppendStartsWith(and, column, value);
                if (filterOperator == FilterOperators.EndsWith) query.AppendEndsWith(and, column, value);
            }
		}

		protected void SetSortDirection<TEntityColumn>(Enum column, string value, SqlSortBuilder<TEntityColumn> sortBuilder)
		{
			if (!String.IsNullOrEmpty(value))
			{
				var sortDirection = value.ToLower() == "desc" ? SqlSortDirection.DESC : SqlSortDirection.ASC;
				sortBuilder.Append(column, sortDirection);
			}
		}

        /// <summary>
        /// To Get the Authorized portal Ids for the login user.
        /// </summary>
        /// <returns>Return the portal Ids.</returns>
        protected string GetAvailablePortals(string userName)
        {
            string profileStoreAccess = string.Empty;
            if (!string.IsNullOrEmpty(userName))
            {
                CommonProfileService profileService = new CommonProfileService();
                var data = profileService.GetProfileStoreAccess(userName);
                if (!Equals(data, null) && !Equals(data.Profiles, null) && data.Profiles.Count > 0)
                {
                    profileStoreAccess = string.Join(",", data.Profiles.ToList().Select(x => x.PortalId.ToString()));
                }
            }
            return profileStoreAccess;
        }

        /// <summary>
        /// To Get the Authorized portal Id for the login user.
        /// </summary>
        /// <returns>Return the portal Id.</returns>
        protected int GetAuthorizedPortalId(string userName)
        {
            int portalId = 0;
            string portalIds = GetAvailablePortals(userName);
            return (!string.IsNullOrEmpty(portalIds) && int.TryParse(portalIds.Split(',')[0], out portalId)) ? portalId : 0;
        }

        /// <summary>
        /// To Get the Authorized profile Ids for the login user.
        /// </summary>
        /// <returns>Return the profile Ids.</returns>
        protected string GetAvailableProfiles(string userName)
        {
            string profileStoreAccess = string.Empty;
            if (!string.IsNullOrEmpty(userName))
            {
                ProfileService profileService = new ProfileService();
                var data = profileService.GetProfilesAssociatedWithUsers(userName);
                if (!Equals(data, null) && !Equals(data.Profiles, null) && data.Profiles.Count > 0)
                {
                    profileStoreAccess = string.Join(",", data.Profiles.ToList().Select(x => x.ProfileId.ToString()));
                }
            }
            return profileStoreAccess;
        }
	}
}
