﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Helpers;
using Znode.Libraries.Helpers.Constants;
using Znode.Libraries.Helpers.Extensions;
using Znode.Libraries.Helpers.Utility;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using PortalProfileRepository = ZNode.Libraries.DataAccess.Service.PortalProfileService;
using ProfileRepository = ZNode.Libraries.DataAccess.Service.ProfileService;
using StoreAdminRepository = ZNode.Libraries.Admin.StoreSettingsAdmin;
using UserAccessCommonRepository = Znode.Engine.Common.UserStoreAccess;

namespace Znode.Engine.Services
{
    public class ProfileService : BaseService, IProfileService
    {
        #region Private Variables
        private readonly ProfileRepository _profileRepository;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor for profile service
        /// </summary>
        public ProfileService()
        {
            _profileRepository = new ProfileRepository();
        }
        #endregion

        #region Public Methods
        public ProfileListModel GetProfiles(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new ProfileListModel();
            var profiles = new TList<Profile>();
            var tempList = new TList<Profile>();

            var query = new ProfileQuery();
            var sortBuilder = new ProfileSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            tempList = _profileRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (Equals(pagingLength, int.MaxValue))
            {
                model.PageSize = model.TotalResults;
            }

            // Now go through the list and get any expands
            foreach (var profile in tempList)
            {
                profiles.Add(profile);
            }

            // Map each item and add to the list
            foreach (var a in profiles)
            {
                model.Profiles.Add(ProfileMap.ToModel(a));
            }

            return model;
        }

        public ProfileModel GetProfile(int profileId, NameValueCollection expands)
        {
            var profile = _profileRepository.GetByProfileID(profileId);

            return ProfileMap.ToModel(profile);
        }

        public ProfileListModel GetProfiles()
        {
            StoreAdminRepository settingsAdmin = new StoreAdminRepository();
            TList<Profile> profiles = UserAccessCommonRepository.CheckProfileAccess(settingsAdmin.GetProfiles());
            return !Equals(profiles, null) ? ProfileMap.ToModelList(profiles) : null;
        }

        public ProfileModel CreateProfile(ProfileModel model)
        {
            if (Equals(model, null))
            {
                throw new Exception("Profile model cannot be null.");
            }

            var entity = ProfileMap.ToEntity(model);
            var profile = _profileRepository.Save(entity);
            return ProfileMap.ToModel(profile);
        }

        public ProfileModel UpdateProfile(int profileId, ProfileModel model)
        {
            if (profileId < 1)
            {
                throw new Exception("Profile ID cannot be less than 1.");
            }

            if (Equals(model, null))
            {
                throw new Exception("Profile model cannot be null.");
            }

            var profile = _profileRepository.GetByProfileID(profileId);
            if (!Equals(profile, null))
            {
                // Set profileId and update details.
                model.ProfileId = profileId;
                var profileToUpdate = ProfileMap.ToEntity(model);

                var updated = _profileRepository.Update(profileToUpdate);
                if (updated)
                {
                    profile = _profileRepository.GetByProfileID(profileId);
                    return ProfileMap.ToModel(profile);
                }
            }

            return null;
        }

        public bool DeleteProfile(int profileId)
        {
            PortalProfileRepository portalProfile = new PortalProfileRepository();
            if (profileId < 1)
            {
                throw new Exception("Profile ID cannot be less than 1.");
            }

            var profile = _profileRepository.GetByProfileID(profileId);

            TList<PortalProfile> portalProfileList = portalProfile.GetByProfileID(profileId);

            if (!Equals(profile, null) && Equals(portalProfileList.Count, 0))
            {
                return _profileRepository.Delete(profile);
            }
            return false;
        }

        public ProfileListModel GetCustomerProfile(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            ProfileListModel list = new ProfileListModel();
            string whereClause = StringHelper.GenerateDynamicWhereClause(filters);

            int totalRowCount = 0;
            var pagingStart = 0;
            var pagingLength = 0;
            string orderBy = StringHelper.GenerateDynamicOrderByClause(sorts);

            pagingStart = Convert.ToInt32(page.Get(PageKeys.Index));
            pagingLength = Convert.ToInt32(page.Get(PageKeys.Size));

            StoredProcedureHelper storedProcedureHelper = new StoredProcedureHelper();
            DataSet resultDataSet = storedProcedureHelper.SP_DataDetailsSpConfig(whereClause, string.Empty, StoredProcedureKeys.SPGetAccountAssociateProfile, orderBy, pagingStart.ToString(), pagingLength.ToString(), out totalRowCount);

            list.Profiles = resultDataSet.Tables[0].ToList<ProfileModel>().ToCollection();
            list.TotalResults = totalRowCount;
            list.PageSize = pagingLength;
            list.PageIndex = pagingStart;
            // Check to set page size equal to the total number of results
            if (Equals(pagingLength, int.MaxValue))
            {
                list.PageSize = list.TotalResults;
            }

            return list;
        }


        public ProfileListModel GetAccountNotAssociatedProfiles(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            ProfileListModel list = new ProfileListModel();
            string whereClause = string.Empty;            

            string innerWhereClause = StringHelper.GenerateDynamicWhereClause(filters);
            int totalRowCount = 0;
            var pagingStart = 0;
            var pagingLength = 0;
            string orderBy = StringHelper.GenerateDynamicOrderByClause(sorts);

            pagingStart = Convert.ToInt32(page.Get(PageKeys.Index));
            pagingLength = Convert.ToInt32(page.Get(PageKeys.Size));

            StoredProcedureHelper storedProcedureHelper = new StoredProcedureHelper();
            DataSet resultDataSet = storedProcedureHelper.SP_DataDetailsSpConfig(whereClause, null, StoredProcedureKeys.SPGetProfileNotAssociateCustomer, orderBy, pagingStart.ToString(), pagingLength.ToString(), out totalRowCount, null, innerWhereClause);

            list.Profiles = resultDataSet.Tables[0].ToList<ProfileModel>().ToCollection();
            list.TotalResults = totalRowCount;
            list.PageSize = pagingLength;
            list.PageIndex = pagingStart;
            // Check to set page size equal to the total number of results
            if (Equals(pagingLength, int.MaxValue))
            {
                list.PageSize = list.TotalResults;
            }

            return list;
        }

        #region Znode Version 8.0
        public ProfileListModel GetProfileListByProfileId(int profileId)
        {
            ProfileAdmin profileAdmin = new ProfileAdmin();
            DataSet profiles = profileAdmin.GetProfilesByPortalID(profileId);
            return ProfileMap.ToModelList(profiles);
        }

        public ProfileListModel GetProfileListByPortalId(int portalId)
        {
            ProfileAdmin profileAdmin = new ProfileAdmin();
            DataSet profiles = profileAdmin.GetAssociatedProfilesByPortalID(portalId);
            return ProfileMap.ToModelList(profiles);
        }

        public ProfileListModel GetAvailableProfileListBySkuIdCategoryId(AssociatedSkuCategoryProfileModel model)
        {
            ProfileListModel profileListModel = new ProfileListModel();
            ProfileHelper profileHelper = new ProfileHelper();
            string avilablePortals = GetAvailablePortals(model.UserName);
            if (!string.IsNullOrEmpty(avilablePortals))
            {
                avilablePortals = Equals(avilablePortals, "0") ? string.Empty : avilablePortals;
            }
            DataSet resultDataSet = profileHelper.GetAvailableProfileBySkuId(avilablePortals, model.SkuId, model.CategoryId, model.SkuEffectiveId, model.CategoryProfileId);
            if (!Equals(resultDataSet, null) && (resultDataSet.Tables.Count > 0) && !Equals(resultDataSet.Tables[0], null) && resultDataSet.Tables[0].Rows.Count > 0)
            {
                profileListModel.Profiles = resultDataSet.Tables[0].ToList<ProfileModel>().ToCollection();
            }
            return profileListModel;
        }
        public ProfileModel GetZNodeProfile()
        {
            ZNodeProfile znodeProfile = new ZNodeProfile();
            ProfileModel model = new ProfileModel();
            model.ShowPricing = znodeProfile.ShowPrice;
            model.UseWholesalePricing = znodeProfile.UseWholesalePricing;
            return model;
        }

        public ProfileListModel GetProfilesAssociatedWithUsers(string userName)
        {
            ProfileHelper profileHelper = new ProfileHelper();
            DataSet resultDataSet = profileHelper.GetProfilesAssociatedWithUsers(userName);
            if (!Equals(resultDataSet, null) && (resultDataSet.Tables.Count > 0) && !Equals(resultDataSet.Tables[0], null) && resultDataSet.Tables[0].Rows.Count > 0)
            {
                ProfileListModel list = new ProfileListModel();
                list.Profiles = resultDataSet.Tables[0].ToList<ProfileModel>().ToCollection();
                return list;
            }
            return new ProfileListModel();
        }
            
        #endregion


        #endregion

        #region Private Methods
        private void SetFiltering(List<Tuple<string, string, string>> filters, ProfileQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;

                if (Equals(filterKey, FilterKeys.ProfileName)) { SetQueryParameter(ProfileColumn.Name, filterOperator, filterValue, query); }
                if (Equals(filterKey, FilterKeys.Name)) { SetQueryParameter(ProfileColumn.Name, filterOperator, filterValue, query); }
                if (Equals(filterKey, FilterKeys.ProfileId)) { SetQueryParameter(ProfileColumn.ProfileID, filterOperator, filterValue, query); }
                if (Equals(filterKey, FilterKeys.ProductPrice)) { SetQueryParameter(ProfileColumn.ShowPricing, filterOperator, filterValue, query); }
                if (Equals(filterKey, FilterKeys.WholesalePrice)) { SetQueryParameter(ProfileColumn.UseWholesalePricing, filterOperator, filterValue, query); }
                if (Equals(filterKey, FilterKeys.TaxExempt)) { SetQueryParameter(ProfileColumn.TaxExempt, filterOperator, filterValue, query); }
                if (Equals(filterKey, FilterKeys.Affiliate)) { SetQueryParameter(ProfileColumn.ShowOnPartnerSignup, filterOperator, filterValue, query); }
            }
        }

        private void SetSorting(NameValueCollection sorts, ProfileSortBuilder sortBuilder)
        {
            if (sorts.HasKeys() && sorts.Count > 1)
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);

                    if (Equals(value, SortKeys.ProfileId)) { SetSortDirection(ProfileColumn.ProfileID, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (Equals(value, SortKeys.Name)) { SetSortDirection(ProfileColumn.Name, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                }
            }
            else
            {
                SetSortDirection(ProfileColumn.ProfileID, sorts[StoredProcedureKeys.sortDirKey], sortBuilder);
            }
        }
        #endregion
    }
}
