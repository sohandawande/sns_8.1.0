﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
	public interface IReviewService
	{
		ReviewModel GetReview(int reviewId, NameValueCollection expands);
		ReviewListModel GetReviews(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
		ReviewModel CreateReview(ReviewModel model);
		ReviewModel UpdateReview(int reviewId, ReviewModel model);
		bool DeleteReview(int reviewId);
	}
}
