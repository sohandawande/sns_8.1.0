﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.Promotions;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using Znode.Engine.Taxes;
using Znode.Libraries.Helpers;
using Znode.Libraries.Helpers.Constants;
using Znode.Libraries.Helpers.Extensions;
using Znode.Libraries.Helpers.Utility;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;
using ProfilesRepository = ZNode.Libraries.DataAccess.Service.ProfileService;
using ShippingRepository = ZNode.Libraries.DataAccess.Service.ShippingService;
using ShippingServiceCodeRepository = ZNode.Libraries.DataAccess.Service.ShippingServiceCodeService;
using ShippingTypeRepository = ZNode.Libraries.DataAccess.Service.ShippingTypeService;

namespace Znode.Engine.Services
{
    /// <summary>
    /// Shipping Option Service
    /// </summary>
    public class ShippingOptionService : BaseService, IShippingOptionService
    {
        #region Private Variables

        private readonly ShippingRepository _shippingRepository;
        private readonly ShippingTypeRepository _shippingTypeRepository;
        private readonly ProfilesRepository _profilesRepository;
        private readonly ShippingAdmin shippingAdmin;
        private readonly ShippingTypeService _shippingTypeService;

        #endregion

        #region Constructor

        public ShippingOptionService()
        {
            _shippingRepository = new ShippingRepository();
            _shippingTypeRepository = new ShippingTypeRepository();
            _profilesRepository = new ProfilesRepository();
            shippingAdmin = new ShippingAdmin();
            _shippingTypeService = new ShippingTypeService();
        }

        #endregion

        #region Public Methods

        public ShippingOptionModel GetShippingOption(int shippingOptionId, NameValueCollection expands)
        {
            ShippingOptionModel model = new ShippingOptionModel();
            var shippingOption = _shippingRepository.GetByShippingID(shippingOptionId);

            if (!Equals(shippingOption, null))
            {
                GetExpands(expands, shippingOption);
            }
            ShippingServiceCodeService codeService = new ShippingServiceCodeService();
            List<Tuple<string, string, string>> list = new List<Tuple<string, string, string>>();
            list.Add(new Tuple<string, string, string>(FilterKeys.ShippingTypeId.ToString(), FilterOperators.Equals, shippingOption.ShippingTypeID.ToString()));

            model = ShippingOptionMap.ToModel(shippingOption);
            NameValueCollection sorts = new NameValueCollection();
            NameValueCollection expand = new NameValueCollection();
            NameValueCollection page = new NameValueCollection();
            ShippingServiceCodeListModel ServiceCodeList = codeService.GetShippingServiceCodes(expand, list, sorts, page);
            if (!Equals(ServiceCodeList, null) && ServiceCodeList.ShippingServiceCodes.Count > 0)
            {
                foreach (var item in ServiceCodeList.ShippingServiceCodes)
                {
                    if (item.Code.Equals(shippingOption.ShippingCode))
                    {
                        model.ShippingServiceCodeId = item.ShippingServiceCodeId;
                        break;
                    }
                }
            }
            model.ClassName = GetShippingTypeClassName((model.ShippingTypeId) > 0 ? model.ShippingTypeId : 0);
            return model;
        }

        public ShippingOptionListModel GetShippingOptions(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page, string shippingCountryCode = null)
        {
            var model = new ShippingOptionListModel();
            var shippingOptions = new TList<ZNode.Libraries.DataAccess.Entities.Shipping>();
            var tempList = new TList<ZNode.Libraries.DataAccess.Entities.Shipping>();

            var query = new ShippingQuery();
            var sortBuilder = new ShippingSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);
            if (query.Length > 0)
            {
                // Get the initial set
                var totalResults = 0;
                tempList = _shippingRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
                model.TotalResults = totalResults;

                // Check to set page size equal to the total number of results
                if (Equals(pagingLength, int.MaxValue))
                {
                    model.PageSize = model.TotalResults;
                }

                // Now go through the list and get any expands
                foreach (var shippingOption in tempList)
                {
                    GetExpands(expands, shippingOption);
                    shippingOptions.Add(shippingOption);
                }
                // Gets the list of all profiles
                var profiles = _profilesRepository.GetAll();

                // Map each item and add to the list
                foreach (var s in shippingOptions)
                {
                    var shippingOption = ShippingOptionMap.ToModel(s);
                    shippingOption.ProfileName = Equals(s.ProfileID, null) ? "All Profiles" : profiles.Find(x => x.ProfileID.Equals(s.ProfileID)).Name;
                    model.ShippingOptions.Add(shippingOption);
                }
            }
            else
            {
                model.ShippingOptions = new System.Collections.ObjectModel.Collection<ShippingOptionModel>();
            }
            return model;
        }

        public ShippingOptionModel CreateShippingOption(ShippingOptionModel model)
        {
            if (Equals(model, null))
            {
                throw new Exception("Shipping option model cannot be null.");
            }

            if (!this.IsShippingSetupValid(model) && !Equals(model.UserType, UserTypes.FranchiseAdmin.ToString()))
            {
                throw new Exception("At least one shipping option should be enabled.");
            }

            if (this.IsDuplicateShipping(model))
            {
                throw new Exception("The selected shipping option already exists.");
            }

            if (Equals(model.ProfileId, -1))
            {
                model.ProfileId = null;
            }
            if ((Equals(model.ShippingType.ClassName, "ZnodeShippingFedEx")) || (Equals(model.ShippingType.ClassName, "ZnodeShippingUps")))
            {
                model = GetShippingServiceCodeData(model, model.ShippingServiceCodeId);
            }
            var entity = ShippingOptionMap.ToEntity(model);
            var shippingOption = _shippingRepository.Save(entity);
            return ShippingOptionMap.ToModel(shippingOption);
        }

        public ShippingOptionModel UpdateShippingOption(int shippingOptionId, ShippingOptionModel model)
        {
            if (shippingOptionId < 1)
            {
                throw new Exception("Shipping option ID cannot be less than 1.");
            }

            if (Equals(model, null))
            {
                throw new Exception("Shipping option model cannot be null.");
            }

            if (!this.IsShippingSetupValid(model) && !Equals(model.UserType, UserTypes.FranchiseAdmin.ToString()))
            {
                throw new Exception("At least one shipping option should be enabled.");
            }

            if (this.IsDuplicateShipping(model))
            {
                throw new Exception("The selected shipping option already exists.");
            }

            var shippingOption = _shippingRepository.GetByShippingID(shippingOptionId);
            if (!Equals(shippingOption, null))
            {
                // Set the shipping option ID and map
                model.ShippingId = shippingOptionId;
                var shippingOptionToUpdate = ShippingOptionMap.ToEntity(model);

                var updated = _shippingRepository.Update(shippingOptionToUpdate);
                if (updated)
                {
                    shippingOption = _shippingRepository.GetByShippingID(shippingOptionId);
                    return ShippingOptionMap.ToModel(shippingOption);
                }
            }

            return null;
        }

        public bool DeleteShippingOption(int shippingOptionId)
        {
            if (shippingOptionId < 1)
            {
                throw new Exception("Shipping option ID cannot be less than 1.");
            }

            var shippingOption = _shippingRepository.GetByShippingID(shippingOptionId);
            if (!Equals(shippingOption, null))
            {
                return _shippingRepository.Delete(shippingOption);
            }

            return false;
        }

        public ShippingOptionListModel GetFranchiseShippingOptionList(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new ShippingOptionListModel();
            Dictionary<string, string> StoredProc = new Dictionary<string, string>();

            string whereClause = StringHelper.GenerateDynamicWhereClause(filters);
            if (Equals(sorts["sort"], "shippingoptionid"))
            {
                sorts["sort"] = "shippingid";
            }
            string orderBy = StringHelper.GenerateDynamicOrderByClause(sorts);
            var pagingStart = 0;
            var pagingLength = 0;
            var totalRowCount = 0;
            SetPaging(page, model, out pagingStart, out pagingLength);
            pagingStart = Convert.ToInt32(page.Get(PageKeys.Index));

            StoredProcedureHelper storedProcedureHelper = new StoredProcedureHelper();

            DataSet resultDataSet = storedProcedureHelper.SP_DataDetailsSpConfig(whereClause, null, StoredProcedureKeys.SPGetShippingList, orderBy, pagingStart.ToString(), pagingLength.ToString(), out totalRowCount);
            model.TotalResults = totalRowCount;
            model.PageSize = pagingLength;
            model.PageIndex = pagingStart;
            model.ShippingOptions = resultDataSet.Tables[0].ToList<ShippingOptionModel>().ToCollection();
            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                model.PageSize = model.TotalResults;
            }
            return model;
        }

        #endregion

        #region Private Methods

        private void GetExpands(NameValueCollection expands, ZNode.Libraries.DataAccess.Entities.Shipping shippingOption)
        {
            if (expands.HasKeys())
            {
                ExpandShippingType(expands, shippingOption);
            }
        }

        private void ExpandShippingType(NameValueCollection expands, ZNode.Libraries.DataAccess.Entities.Shipping shippingOption)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.ShippingType)))
            {
                ShippingType shippingType = _shippingTypeRepository.GetByShippingTypeID(shippingOption.ShippingTypeID);
                var shippingTypeList = _shippingTypeRepository.GetAll();

                if (!Equals(shippingType, null))
                {
                    shippingType.ClassName = shippingTypeList.Find(x => x.ShippingTypeID.Equals(shippingType.ShippingTypeID)).ClassName;
                    shippingOption.ShippingTypeIDSource = shippingType;
                }
            }
        }

        private void SetFiltering(List<Tuple<string, string, string>> filters, ShippingQuery query)
        {
            bool isProfileIdPresent = false;
            bool isDestinationCountryCodePresent = false;
            string UserName = string.Empty;
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;
                if (!Equals(filterKey, FilterKeys.UserName))
                {
                    if (filterKey.Equals(FilterKeys.ProfileId))
                    {
                        isProfileIdPresent = true;
                        SetQueryParameter(ShippingColumn.ProfileID, filterOperator, filterValue, query);
                    }
                    if (Equals(filterKey, FilterKeys.ShippingOptionId)) { SetQueryParameter(ShippingColumn.ShippingID, filterOperator, filterValue, query); }
                    if (Equals(filterKey, FilterKeys.CountryCode))
                    {
                        isDestinationCountryCodePresent=true;
                        SetQueryParameter(ShippingColumn.DestinationCountryCode, filterOperator, filterValue, query); 
                    }
                    if (Equals(filterKey, FilterKeys.ExternalId)) { SetQueryParameter(ShippingColumn.ExternalID, filterOperator, filterValue, query); }
                    if (Equals(filterKey, FilterKeys.IsActive)) { SetQueryParameter(ShippingColumn.ActiveInd, filterOperator, filterValue, query); }
                    if (Equals(filterKey, FilterKeys.ProfileId)) { SetQueryParameter(ShippingColumn.ProfileID, filterOperator, filterValue, query); }
                    if (Equals(filterKey, FilterKeys.ShippingCode)) { SetQueryParameter(ShippingColumn.ShippingCode, filterOperator, filterValue, query); }
                    if (Equals(filterKey, FilterKeys.ShippingTypeId)) { SetQueryParameter(ShippingColumn.ShippingTypeID, filterOperator, filterValue, query); }
                    if (Equals(filterKey, FilterKeys.HandlingChargeWithDollar)) { SetQueryParameter(ShippingColumn.HandlingCharge, filterOperator, filterValue, query); }
                }
                else
                {
                    UserName = filterValue;
                }
            }
            if (!isProfileIdPresent)
            {
                string profileIds = GetAvailableProfiles(UserName);
                if (!string.IsNullOrEmpty(profileIds) && !Equals(profileIds, "-1"))
                {
                    query.BeginGroup("AND");
                    query.AppendIn(null, ShippingColumn.ProfileID, profileIds.Split(','));
                    query.AppendIsNull(SqlUtil.OR, ShippingColumn.ProfileID);
                    query.EndGroup();
                }
            }

            if(isDestinationCountryCodePresent)
            {
                query.AppendIsNull(SqlUtil.OR, ShippingColumn.DestinationCountryCode);
            }
        }

        private void SetSorting(NameValueCollection sorts, ShippingSortBuilder sortBuilder)
        {
            if (sorts.HasKeys())
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);

                    if (Equals(value, SortKeys.CountryCode)) { SetSortDirection(ShippingColumn.DestinationCountryCode, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (Equals(value, SortKeys.Description)) { SetSortDirection(ShippingColumn.Description, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (Equals(value, SortKeys.DisplayOrder)) { SetSortDirection(ShippingColumn.DisplayOrder, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (Equals(value, SortKeys.ShippingCode)) { SetSortDirection(ShippingColumn.ShippingCode, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (Equals(value, SortKeys.ShippingOptionId)) { SetSortDirection(ShippingColumn.ShippingID, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (Equals(value, SortKeys.ShippingTypeName)) { SetSortDirection(ShippingColumn.ShippingTypeID, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (Equals(value, SortKeys.ProfileName)) { SetSortDirection(ShippingColumn.ProfileID, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (Equals(value, SortKeys.HandlingChargeWithDollar)) { SetSortDirection(ShippingColumn.HandlingCharge, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                }
            }
        }

        private void SetQueryParameter(ShippingColumn column, string filterOperator, string filterValue, ShippingQuery query)
        {
            base.SetQueryParameter(column, filterOperator, filterValue, query);
        }

        private void SetSortDirection(ShippingColumn column, string value, ShippingSortBuilder sortBuilder)
        {
            base.SetSortDirection(column, value, sortBuilder);
        }

        private ShippingOptionModel GetShippingServiceCodeData(ShippingOptionModel model, int id)
        {
            ShippingServiceCodeRepository service = new ShippingServiceCodeRepository();
            ShippingServiceCode code = service.GetByShippingServiceCodeID(id);
            model.ShippingCode = code.Code;
            model.Description = code.Description;

            return model;
        }

        private bool IsDuplicateShipping(ShippingOptionModel shippingOptionModel)
        {
            string code1 = string.Empty;
            ShippingServiceCodeRepository service = new ShippingServiceCodeRepository();
            ShippingServiceCode code = service.GetByShippingServiceCodeID(shippingOptionModel.ShippingServiceCodeId);
            if (!Equals(code, null))
            {
                return shippingAdmin.IsDuplicateShipping(shippingOptionModel.ShippingId, shippingOptionModel.ProfileId.Value, code.Code);
            }
            else
            {
                return shippingAdmin.IsDuplicateShipping(shippingOptionModel.ShippingId, shippingOptionModel.ProfileId.Value, code1);
            }
        }

        /// <summary>
        /// Get shipping type class name by shipping type id. 
        /// </summary>
        /// <param name="shippingTypeId">Id of the shipping type.</param>
        /// <returns>Returns the shipping type class name.</returns>
        private string GetShippingTypeClassName(int shippingTypeId)
        {
            ShippingType shippingType = new ShippingType();
            string className = string.Empty;
            if (shippingTypeId > 0)
            {
                shippingType = _shippingTypeRepository.GetByShippingTypeID(shippingTypeId);
                if (!Equals(shippingType, null))
                {
                    className = shippingType.ClassName;
                }
            }
            return className;
        }

        private bool IsShippingSetupValid(ShippingOptionModel model)
        {
            ShippingAdmin _shippingAdmin = new ShippingAdmin();
            var allShippingOptions = _shippingAdmin.GetAll();
            if (!Equals(allShippingOptions, null))
            {
                var inactiveShippingOptions = allShippingOptions.FindAll(shipping => shipping.ActiveInd == false);
                if (Equals(inactiveShippingOptions.Count, (allShippingOptions.Count - 1)) && !Equals(model.ActiveInd, true))
                {
                    return false;
                }
            }
            // Otherwise
            return true;
        }
        #endregion
    }
}
