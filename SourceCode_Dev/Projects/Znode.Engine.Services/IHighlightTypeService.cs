﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
	public interface IHighlightTypeService
	{
		HighlightTypeModel GetHighlightType(int highlightTypeId);
		HighlightTypeListModel GetHighlightTypes(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
		HighlightTypeModel CreateHighlightType(HighlightTypeModel model);
		HighlightTypeModel UpdateHighlightType(int highlightTypeId, HighlightTypeModel model);
		bool DeleteHighlightType(int highlightTypeId);
	}
}
