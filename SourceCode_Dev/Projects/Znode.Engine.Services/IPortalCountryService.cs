﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
	public interface IPortalCountryService
	{
		PortalCountryModel GetPortalCountry(int portalCountryId, NameValueCollection expands);
		
        /// <summary>
        /// Gets Portal Countries.
        /// </summary>
        /// <param name="expands">Expands</param>
        /// <param name="filters">Filters</param>
        /// <param name="sorts">Sorts.</param>
        /// <param name="page">Page.</param>
        /// <returns>PortalCountryListModel</returns>
		PortalCountryListModel GetPortalCountries(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
		
        /// <summary>
        /// Creates a portal Country.
        /// </summary>
        /// <param name="model">Portal country model.</param>
        /// <returns>Portal country model.</returns>
		PortalCountryModel CreatePortalCountry(PortalCountryModel model);
		
        /// <summary>
        /// Updates aportal country.
        /// </summary>
        /// <param name="portalCountryId">Portal country id</param>
        /// <param name="model">Portal country model.</param>
        /// <returns>Portal country model.</returns>
		PortalCountryModel UpdatePortalCountry(int portalCountryId, PortalCountryModel model);

        /// <summary>
        /// Deletes a Portal Country.
        /// </summary>
        /// <param name="portalCountryId">Portal Country Id.</param>
        /// <returns>Bool value if Portal Country is deleted or not.</returns>
		bool DeletePortalCountry(int portalCountryId);

        /// <summary>
        /// ZNode version 8.0
        /// Get all Portal Countries
        /// </summary>
        /// <param name="expands">NameValueCollection expands</param>
        /// <param name="filters">List Tuple filters</param>
        /// <param name="sorts">NameValueCollection sorts</param>
        /// <param name="page">NameValueCollection page</param>
        /// <returns>Returns list of Portal Countries</returns>
        PortalCountryListModel GetAllPortalCountries(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Znode 8.0 Version
        /// Deletes a portal country if it is not associated with any address.
        /// </summary>
        /// <param name="portalCountryId">Portal Country Id.</param>
        /// <returns>Bool value if country is deleted or not.</returns>
        bool DeletePortalCountryIfNotAssociated(int portalCountryId);

       /// <summary>
        /// Creates portal countries.
        /// </summary>
        /// <param name="portalId">Portal ID against which portal countries are to be created.</param>
        /// <param name="billableCountryCodes">Billable country codes.</param>
        /// <param name="shipableCountryCodes">Shippable Country codes.</param>
        /// <returns>Returns true or false.</returns>
        bool CreatePortalCountries(int portalId, string billableCountryCodes, string shipableCountryCodes);
	}
}
