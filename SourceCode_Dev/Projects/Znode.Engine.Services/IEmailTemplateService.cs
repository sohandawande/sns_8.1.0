﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    public interface IEmailTemplateService
    {
        EmailTemplateListModel GetEmailTemplates(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Creates a Email template page.
        /// </summary>
        /// <param name="model">Email template model to be created.</param>
        /// <returns>Email template page model.</returns>
        EmailTemplateModel CreateTemplatePage(EmailTemplateModel model);

        EmailTemplateModel GetTemplatePageByName(string templateName, string extension, NameValueCollection expands);

        /// <summary>
        /// Update the Template Page.
        /// </summary>
        /// <param name="model">EmailTemplateModel model</param>
        /// <returns>Returns True or False</returns>
        bool UpdateTemplatePage(EmailTemplateModel model);

        bool DeleteTemplatePage(string templateName, string extension);

        EmailTemplateListModel GetDeletedTemplates(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

    }
}
