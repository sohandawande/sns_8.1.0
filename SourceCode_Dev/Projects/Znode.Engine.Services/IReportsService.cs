﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    public interface IReportsService
    {
        /// <summary>
        /// Get Report Data from DB by report Name
        /// </summary>
        /// <param name="reportName">Report Name</param>
        /// <param name="expands">Expands collection</param>
        /// <param name="filters">filters collection</param>
        /// <param name="sorts">sort collection</param>
        /// <param name="page"></param>
        /// <returns>Returns ReportsDataModel</returns>
        ReportsDataModel GetReportDataSet(string reportName, NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Get order details by order Id.
        /// </summary>
        /// <param name="orderId">int order id</param>
        /// <returns>returns ReportsDataModel</returns>
        ReportsDataModel GetOrderDetails(int orderId,string reportName);
    }
}
