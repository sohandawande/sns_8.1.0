﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using ZNode.Libraries.ECommerce.Fulfillment;
using ZNode.Libraries.ECommerce.ShoppingCart;

namespace Znode.Engine.Services
{
    public interface IPRFTERPOrderServices
    {
        PRFTERPOrderResponseModel SubmitOrderToERP(ZNodeOrderFulfillment order, ZNodePortalCart shoppingCart);

        PRFTOEHeaderHistoryModel GetOrderHistory(string erpOrderNumber);
        //PRFTERPOrderResponseModel SubmitOrderToERP(ZNodeOrderFulfillment order);
        //PRFTOrderStatusResponseModel GetOrderStatus(string orderId);
        //PRFTERPOrderResponseModel ReSubmitOrderToERP(OrderModel order);
    }
}
