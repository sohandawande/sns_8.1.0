﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    public interface IThemeService
    {
        /// <summary>
        /// Get the list of all themes
        /// </summary>
        /// <param name="filters">List of filter tuples</param>
        /// <param name="sorts">Collection of sorting parameters</param>
        /// <returns>List of Themes</returns>
        ThemeListModel GetThemes(List<Tuple<string, string, string>> filters, NameValueCollection sorts);

        /// <summary>
        /// Get the list of all themes with pagination
        /// </summary>
        /// <param name="filters">List of filter tuples</param>
        /// <param name="sorts">Collection of sorting parameters</param>
        /// <param name="page">Collection of paging parameters</param>
        /// <returns>List of Themes</returns>
        ThemeListModel GetThemes(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Create new theme
        /// </summary>
        /// <param name="model">Theme model</param>
        /// <returns>Theme Model</returns>
        ThemeModel CreateTheme(ThemeModel model);

        /// <summary>
        /// Get theme by themeId
        /// </summary>
        /// <param name="themeId">Get theme through themeId</param>
        /// <returns>Theme Model</returns>
        ThemeModel GetTheme(int themeId);

        /// <summary>
        /// Update the theme
        /// </summary>
        /// <param name="themeId">themeId to get theme</param>
        /// <param name="model">Theme Model</param>
        /// <returns>Theme Model</returns>
        ThemeModel UpdateTheme(int themeId, ThemeModel model);

        /// <summary>
        /// Delete Theme
        /// </summary>
        /// <param name="themeId">Theme ID to delete Theme</param>
        /// <returns>boolean value true/false</returns>
        bool DeleteTheme(int themeId);
    }
}
