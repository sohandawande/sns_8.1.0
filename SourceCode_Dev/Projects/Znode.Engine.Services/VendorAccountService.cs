﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Web.Security;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Libraries.Helpers;
using Znode.Libraries.Helpers.Constants;
using Znode.Libraries.Helpers.Extensions;
using ZNode.Libraries.DataAccess.Custom;

namespace Znode.Engine.Services
{
    public class VendorAccountService : BaseService, IVendorAccountService
    {
        public VendorAccountListModel GetVendorAccountList(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            List<Tuple<string, string, string>> filterList = new List<Tuple<string, string, string>>();
            VendorAccountListModel list = new VendorAccountListModel();
            
            int totalRowCount = 0;
            var pagingStart = 0;
            var pagingLength = 0;
            string orderBy = StringHelper.GenerateDynamicOrderByClause(sorts);
            string innerWhereClause = string.Empty;

            foreach (var item in filters)
            {
                if (Equals(item.Item1, "username"))
                {
                    filterList.Add(item);
                    innerWhereClause = StringHelper.GenerateDynamicWhereClause(filterList);
                    filters.Remove(item);
                    break;
                }
            }

            string whereClause = StringHelper.GenerateDynamicWhereClause(filters);
            pagingStart = Convert.ToInt32(page.Get(PageKeys.Index));
            pagingLength = Convert.ToInt32(page.Get(PageKeys.Size));

            StoredProcedureHelper storedProcedureHelper = new StoredProcedureHelper();
            DataSet resultDataSet = storedProcedureHelper.SP_DataDetailsSpConfig(whereClause, string.Empty, StoredProcedureKeys.SpGetAccountDetailsByRoleName, orderBy, pagingStart.ToString(), pagingLength.ToString(), out totalRowCount,null, innerWhereClause);

            list.VendorAccount = resultDataSet.Tables[0].ToList<VendorAccountModel>().ToCollection();
            list.TotalResults = totalRowCount;
            list.PageSize = pagingLength;
            list.PageIndex = pagingStart;
            // Check to set page size equal to the total number of results
            if (Equals(pagingLength, int.MaxValue))
            {
                list.PageSize = list.TotalResults;
            }

            return list;
        }

        public VendorAccountModel GetVendorAccountById(int accountId)
        {
            VendorAccountModel model = new VendorAccountModel();
            int totalRowCount = 0;
            var pagingStart = 0;
            var pagingLength = 0;
            var roleList = string.Empty;

            string whereClause = string.Format(StoredProcedureKeys.GetAccountId, accountId);
            StoredProcedureHelper storedProcedureHelper = new StoredProcedureHelper();
            DataSet resultDataSet = storedProcedureHelper.SP_DataDetailsSpConfig(whereClause, string.Empty, StoredProcedureKeys.SpGetAccountDetailsByRoleName, string.Empty, pagingStart.ToString(), pagingLength.ToString(), out totalRowCount);
            model = resultDataSet.Tables[0].Rows[0].ToSingleRow<VendorAccountModel>();

            // Get roles for this User account
            if (!Equals(model.UserName , null))
            {
                var roles = Roles.GetRolesForUser(model.UserName);

                roleList = roles.Aggregate(roleList, (current, role) => current + (role + "<br>"));
            }

            if (Equals(roleList , Convert.ToString(StoredProcedureKeys.FranchiseRole)) || Equals(roleList , Convert.ToString(StoredProcedureKeys.VendorRole)))
            {
                model.CheckRole = false;
            }
            else
            {
                model.CheckRole = true;
            }

            return model;
        }
    }


}
