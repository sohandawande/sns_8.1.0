﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    public interface IRMARequestItemService
    {
        /// <summary>
        /// Gets list of RMA request items.
        /// </summary>        
        /// <param name="expands">Expands for RMA list items.</param>
        /// <param name="filters">Filter collection for RMA item list.</param>
        /// <param name="sorts">Sorting of RMA list.</param>
        /// <param name="page">Paging for RMA item list.</param>
        /// <returns>RMA request item list.</returns>
        RMARequestItemListModel GetRMARequestItemList(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        ///  Gets list of RMA request items.
        /// </summary>
        /// <param name="orderLineItemList">Order line items.</param>
        /// <returns>Request item model.</returns>
        RMARequestItemListModel GetRMARequestItemsForGiftCard(string orderLineItemList);

        /// <summary>
        /// Creates an RMA request item.
        /// </summary>
        /// <param name="model">RMA request model.</param>
        /// <returns>RMARequestItemModel</returns>
        RMARequestItemModel CreateRMARequest(RMARequestItemModel model);  
    }
}
