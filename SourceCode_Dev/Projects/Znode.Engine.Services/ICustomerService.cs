﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    public partial interface ICustomerService
    {
        /// <summary>
        /// Creates a new customer.
        /// </summary>
        /// <param name="listModel"></param>
        /// <returns></returns>
        AddressListModel AddCustomer(AddressListModel listModel);

        /// <summary>
        ///  This method will get the customer Account list.
        /// </summary>
        /// <param name="expands">Expand Collection expands</param>
        /// <param name="filters">Filter criteria</param>
        /// <param name="sorts">sort criteria</param>
        /// <param name="page">page collection</param>
        /// <returns>Returns all Customer Account details in CustomerListModel format</returns>
        CustomerListModel GetCustomerList(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Get detail about CustomerAffiliate
        /// </summary>
        /// <param name="accountId">Account Id by which get detail about customerAffiliate</param>
        /// <returns>CustomerAffiliateModel</returns>
        CustomerAffiliateModel GetCustomerAffiliate(int accountId, NameValueCollection expands);

        /// <summary>
        /// Get List of ReferralCommissionType
        /// </summary>
        /// <param name="expands">Expand Collection expands</param>
        /// <param name="filters">Filter criteria</param>
        /// <param name="sorts">sort criteria</param>
        /// <param name="page">page collection</param>
        /// <returns>Returns all ReferralCommissionType details in ReferralCommissionTypeListModel format</returns>
        ReferralCommissionTypeListModel GetReferralCommissionTypeList(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Get List of ReferralCommission
        /// </summary>
        /// <param name="accountId">Account Id by which get list of ReferralCommissions</param>
        /// <param name="expands">Expand Collection expands</param>
        /// <param name="filters">Filter criteria</param>
        /// <param name="sorts">sort criteria</param>
        /// <param name="page">page collection</param>
        /// <returns>Returns all ReferralCommission details in ReferralCommissionListModel format</returns>
        ReferralCommissionListModel GetReferralCommissionList(int accountId,NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
    }
}
