﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Services
{
    public interface IConfigurationReaderService
    {
        /// <summary>
        /// Get dynamic grid configuration XML from Database.
        /// </summary>
        /// <param name="itemName">string itemName</param>
        /// <returns>Returns XMl string</returns>
        string GetFilterConfigurationXML(string itemName);
    }
}
