﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    /// <summary>
    /// Interface for Product Type Attribute Service
    /// </summary>
    public interface IProductTypeAttributeService
    {
        /// <summary>
        /// Get Attribute Type by product Type Attribute Id.
        /// </summary>
        /// <param name="productTypeAttributeId">product Type Attribute Id.</param>
        /// <returns>Returns ProductTypeAttributeModel</returns>
        ProductTypeAttributeModel GetAttributeType(int productTypeAttributeId);

        /// <summary>
        /// Get Attribute Types List 
        /// </summary>
        /// <param name="filters">List</param>
        /// <param name="sorts">NameValueCollection</param>
        /// <param name="page">NameValueCollection</param>
        /// <returns>Returns ProductTypeAttributeListModel</returns>
        ProductTypeAttributeListModel GetAttributeTypes(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Create Attribute Type.
        /// </summary>
        /// <param name="model">ProductTypeAttributeModel</param>
        /// <returns>Returns ProductTypeAttributeModel</returns>
        ProductTypeAttributeModel CreateAttributeType(ProductTypeAttributeModel model);

        /// <summary>
        /// Update Existing Attribute Type by product Type Attribute id.
        /// </summary>
        /// <param name="productTypeAttribute">product Type Attribute id</param>
        /// <param name="model">ProductTypeAttributeModel</param>
        /// <returns>Returns ProductTypeAttributeModel</returns>
        ProductTypeAttributeModel UpdateAttributeType(int productTypeAttribute, ProductTypeAttributeModel model);

        /// <summary>
        /// Delete Existing Attribute Type by Product Type Attribute Id.
        /// </summary>
        /// <param name="productTypeAttributeId">Product Type Attribute Id</param>
        /// <returns>Returns bool</returns>
        bool DeleteAttributeType(int productTypeAttributeId);

        /// <summary>
        /// Znode Version 8.0
        /// Gets list of categories by catalog Id.
        /// </summary>
        /// <param name="productTypeId">The Id of catalog.</param>
        /// <returns>Returns list of categories by catalog Id.</returns>
        ProductTypeAssociatedAttributeTypesListModel GetAttributeTypesByProductTypeId(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
        
        /// <summary>
        /// To check is Product type associated with product Type.
        /// </summary>
        /// <param name="productTypeId">product Id</param>
        /// <returns>returns true/ false</returns>
        bool IsProductAssociatedProductType(int productTypeId);
    }
}
