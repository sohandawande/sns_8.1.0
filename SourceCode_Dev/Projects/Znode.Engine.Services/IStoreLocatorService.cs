﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    public interface IStoreLocatorService
    {
        /// <summary>
        /// Get store locator
        /// </summary>
        /// <param name="storeId">Store Id</param>
        /// <returns>retruns StoreModel</returns>
        StoreModel GetStoreLocator(int storeId);

        /// <summary>
        /// Get store locators list.
        /// </summary>
        /// <param name="filters">Filter store locator list</param>
        /// <param name="sorts">Sort store locator list.</param>
        /// <param name="page">Paging for locator list</param>
        /// <returns>Returns StoreListModel</returns>
        StoreListModel GetStoreLocators(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Create store locator.
        /// </summary>
        /// <param name="model">StoreModel</param>
        /// <returns>Returns StoreModel</returns>
        StoreModel CreateStoreLocator(StoreModel model);

        /// <summary>
        /// Update existing store locator.
        /// </summary>
        /// <param name="storeId">Store Id</param>
        /// <param name="model">StoreModel</param>
        /// <returns>Returns StoreModel</returns>
        StoreModel UpdateStoreLocator(int storeId, StoreModel model);

        /// <summary>
        /// Delete existing store locator.
        /// </summary>
        /// <param name="storeId">Store Id</param>
        /// <returns>Returns true / false.</returns>
        bool DeleteStoreLocator(int storeId);

        /// <summary>
        /// Get the list of stores within the area
        /// </summary>
        /// <param name="model">Store Model</param>
        /// <returns>Store List Model</returns>
        StoreListModel GetStoresList(StoreModel model); 
    }
}
