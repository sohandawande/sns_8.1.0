﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Web.Security;
using Znode.Engine.Api.Models;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using System.Web;
using Znode.Libraries.Helpers.Constants;
using Znode.Engine.Exceptions;
using System.IO;
using System.Text;
using ZNode.Libraries.Framework.Business;
using Newtonsoft.Json;
using Znode.Engine.Services.Configuration;
using Znode.Engine.Services.Exceptions;
using Znode.Engine.Services.Model;
using Znode.Libraries.Helpers.Extensions;
using Znode.Libraries.Helpers;
using ZNode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.Services
{
    public class ImportExportService : BaseService, IImportExportService
    {
        #region Private Variable
        private int ErrorCount = 0;
        private StringBuilder errorMessage = new StringBuilder();
        private List<RecordErrorModel> errorList = new List<RecordErrorModel>();
        private DataSet invalidDataSet = new DataSet();
        private DataTable chunkData = new DataTable();
        private ExcelSettings excelSetting;
        private string fileName = string.Empty;
        private string connectionString = string.Empty;
        private string errorInRecordIdString = string.Empty;
        private string getMappingSpName = "Znode_Import_GetTableColumnDetails";
        private string columnName = "ColumnName";
        private string allowNull = "AllowNull";
        private string columnValue = "ColumnValue";
        private string recordIdColumn = "RecordId";
        private string idColumn = "Id";
        private string errorMessageColumn = "Error Message";
        private string excelDataParameter = "@ExcelData";
        private string excelSheetNameParameter = "@ExcelSheetName";
        private string processFlagParameter = "@ProcessFlag";
        private string extraParameter = "@ExtraParameter";
        private string errorInRecordIdStringParameter = "@ErrorInRecordIdString";
        private string getErrorInDataSetParameter = "@GetErrorInDataSet";
        private string col = "Col";
        private string defaultSPForSave = "P_ExcelInsertUploadData";
        private string defaultSPForEdit = "P_ExcelUpdateUploadData";
        private string defaultSpForExport = "P_ExcelDownloadData";
        private string getTableColumnDetailsSp = "P_GetTableColumnDetails";
        private string addProcess = "Add";
        private string editProcess = "Edit";
        private string columnSession = "ColumnSession";
        private int noOfColumn = 100;
        private int recordId = 1;
        private bool stopProcessingData = false;
        private bool anyError = false;
        private bool getErrorInDataSetFromSp = true;
        private string ErrorColumn = "Error Message";
        private string InsertImportDataSPName = "ZNode_ImportInsertDataToZnodeTemplate";
        private string ImportConfigHeaderDetailSPName = "ZNode_ImportConfigUploadHead";
        private string ImportMappingName = "@Name";
        #endregion

        #region Public Methods
        public ExportModel GetExports(ExportModel model)
        {
            ExportModel exportModel = new ExportModel();
            DataSet resultDataSet = null;

            //Get Export Details based on Export type.
            switch (model.Type)
            {
                case Constant.ExportInventory:
                    //Get the Product Inventory Details.
                    resultDataSet = GetProductInventory(model.UserName);
                    break;
                case Constant.ExportPricing:
                    //Get the Product Pricing Details.
                    resultDataSet = GetProductPricing(model.UserName);
                    break;
                case Constant.ExportProduct:
                    //Get the Product Details.
                    resultDataSet = GetProduct(model.UserName, model.CatalogId);
                    break;
                case Constant.ExportAttribute:
                    //Get the Product Attribute Details.
                    resultDataSet = GetProductAttribute(model.CatalogId, model.AttributeTypeId);
                    break;
                case Constant.ExportFacet:
                    //Get the Facet Details.
                    resultDataSet = GetFacets(model.CatalogId, model.CategoryId);
                    break;
                case Constant.ExportShipping:
                    //Get the Product Shipping Inventory Details.
                    resultDataSet = GetProductShippingInventory(model.UserName, model.StartDate, model.EndDate);
                    break;
                case Constant.ExportCustomerPricing:
                    //Get the Customer Pricing Details.
                    resultDataSet = GetCustomerPricing();
                    break;
                case Constant.ExportSku:
                    //Get the Product SKU's Details.
                    resultDataSet = GetProductSku(model.ProductId, model.CatalogId, model.ProductName, model.AttributeIds);
                    break;
                case Constant.ExportCustomer:
                    //Get the Customer's Details.
                    resultDataSet = GetCustomers(model.UserName);
                    break;
                case Constant.ExportState:
                    //Get the State's Details.
                    resultDataSet = GetStateDetails(model.CountryCode);
                    break;
                default:
                    resultDataSet = new DataSet();
                    break;
            }

            //Bind Data Table to Dynamic List.
            if (!Equals(resultDataSet, null) && !Equals(resultDataSet.Tables[0], null) && resultDataSet.Tables[0].Rows.Count > 0)
            {
                //Convert Data Table to Dynamic List.
                exportModel.RecordSet = DataTableToList(resultDataSet.Tables[0]);
            }
            return exportModel;
        }

        public static List<dynamic> DataTableToList(DataTable dataTable)
        {
            var result = new List<dynamic>();
            try
            {
                if (!Equals(dataTable, null) && dataTable.Rows.Count > 0)
                {
                    foreach (DataRow row in dataTable.Rows)
                    {
                        var obj = (IDictionary<string, object>)new ExpandoObject();
                        foreach (DataColumn col in dataTable.Columns)
                        {
                            obj.Add(col.ColumnName, row[col.ColumnName]);
                        }
                        result.Add(obj);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                dataTable.Dispose();
            }
            return result;
        }

        public ImportModel ImportData(ImportModel model)
        {
            ImportModel outModel = new ImportModel();
            //ZNodeLogging.LogMessage("Step5 - ImportExportService In Engine.Service  - ImportsData() starts");
            //To Upload the import Data & Return the error in DataTable Format.
            DataTable dataTable = UploadData(ToDataTable(model.ImportData), model);
            outModel.Status = (!Equals(dataTable, null) && dataTable.Rows.Count > 0) ? false : true;

            //Convert the Data Table to Dynamic List.
            outModel.ErrorData = DataTableToList(dataTable);
            //ZNodeLogging.LogMessage("Step23 - ImportExportService In Engine.Service  - ImportsData() end with status="+ outModel.Status);
            return outModel;
        }

        public ImportModel GetImportDetails(string type)
        {
            ImportModel model = new ImportModel();
            ExecuteSpHelper executeStoredProcedure = new ExecuteSpHelper();
            executeStoredProcedure.GetParameter(ImportMappingName, type, ParameterDirection.Input, SqlDbType.NVarChar);
            //Get Import Default Details based on its type.
            DataSet data = executeStoredProcedure.GetSPResultInDataSet(ImportConfigHeaderDetailSPName);
            model.ImportConfigData = data.Tables[0].Rows[0].ToSingleRow<ImportConfigUploadHeadModel>();
            return model;
        }
        #endregion

        #region Private Methods

        #region Export
        /// <summary>
        /// To Get Product Inventory Details.
        /// </summary>
        /// <param name="userName">User Name for the Login User.</param>
        /// <returns>Return Product Invnetory Details in DataSet Format</returns>
        private DataSet GetProductInventory(string userName)
        {
            DataDownloadAdmin adminAccess = new DataDownloadAdmin();
            DataManagerAdmin dataManager = new DataManagerAdmin();
            DataSet ds = null;
            ZNodeSKUInventoryDataset impProduct = new ZNodeSKUInventoryDataset();
            CatalogAdmin catalogAdmin = new CatalogAdmin();

            //Get User Details by user name.
            MembershipUser user = Membership.GetUser(userName);
            string roleList = string.Empty;

            // Get roles for this User account
            string[] roles = Roles.GetRolesForUser(user.UserName);

            foreach (string Role in roles)
            {
                roleList += Role + "<br>";
            }

            string rolename = roleList;

            if (!Roles.IsUserInRole(userName, "ADMIN"))
            {
                if (Roles.IsUserInRole(user.UserName, "CATALOG EDITOR"))
                {
                    //Get Accessible Portals for the user.
                    string portalIDs = GetAvailablePortals(userName);
                    string catalogIDs = string.Empty;
                    //Get the accessible catalog ids based on the Portal Ids.
                    catalogIDs = catalogAdmin.GetCatalogIDsByPortalIDs(portalIDs);

                    SKUHelper skuhelper = new SKUHelper();
                    int parsedInt;
                    SKUInventoryService aovis = new SKUInventoryService();
                    SKUInventory skuInv = new SKUInventory();
                    if (!Equals(catalogIDs, "0"))
                    {
                        string[] catalog = catalogIDs.Split(',');

                        for (int index = 0; index <= catalog.Length - 1; index++)
                        {
                            //Get tge SKu Details by catalog Id.
                            ds = skuhelper.GetSkuByCatalogID(int.Parse(catalog[index]));

                            foreach (DataRow data in ds.Tables[0].Rows)
                            {
                                ZNodeSKUInventoryDataset.ZNodeSKUInventoryRow dataRow = impProduct.ZNodeSKUInventory.NewZNodeSKUInventoryRow();
                                dataRow.SKU = data["SKU"].ToString();

                                skuInv = null;
                                skuInv = aovis.GetBySKU(data["SKU"].ToString());

                                if (int.TryParse(skuInv.QuantityOnHand.ToString(), out parsedInt))
                                {
                                    dataRow.QuantityOnHand = parsedInt;
                                }

                                if (int.TryParse(skuInv.ReOrderLevel.ToString(), out parsedInt))
                                {
                                    dataRow.ReOrderLevel = parsedInt;
                                }

                                impProduct.ZNodeSKUInventory.AddZNodeSKUInventoryRow(dataRow);
                            }
                        }

                        ds = (DataSet)impProduct;
                    }
                }
            }
            else
            {
                //Get the SKU Details.
                ds = dataManager.GetAllSkuInventory();
            }
            if (Equals(ds, null) || Equals(ds.Tables[0], null) || Equals(ds.Tables[0].Rows.Count, 0))
            {
                throw new ZnodeException(ErrorCodes.ExportError, "Unable to download Product Inventory. No inventory found.");
            }
            return ds;
        }

        /// <summary>
        /// To Get Product Pricing Details.
        /// </summary>
        /// <param name="userName">User Name for the Login User.</param>
        /// <returns>Return Product Pricing Details in DataSet Format</returns>
        private DataSet GetProductPricing(string userName)
        {
            DataDownloadAdmin adminAccess = new DataDownloadAdmin();
            DataManagerAdmin dataManager = new DataManagerAdmin();
            DataSet ds = null;
            //Get Accessible portals based on UserName.
            string profileStoreAccess = GetAvailablePortals(userName);

            //Get the Product Pricing Details based on the accessbil portals.
            //Portal "0" means all portal access.
            ds = (!Equals(profileStoreAccess, "0")) ? dataManager.GetProductPrices(0, profileStoreAccess) : dataManager.GetProductPrices(0);
            if (Equals(ds, null) || Equals(ds.Tables[0], null) || Equals(ds.Tables[0].Rows.Count, 0))
            {
                throw new ZnodeException(ErrorCodes.ExportError, "Unable to download Product Pricing. No Product pricing found.");
            }
            return ds;
        }

        /// <summary>
        /// To Get Product Details.
        /// </summary>
        /// <param name="userName">User Name for the Login User.</param>
        /// <param name="catalogId">Id for the Selected Catalog.</param>
        /// <returns>Return Product Details in DataSet Format</returns>
        private DataSet GetProduct(string userName, int catalogId)
        {
            DataDownloadAdmin adminAccess = new DataDownloadAdmin();
            DataManagerAdmin dataManager = new DataManagerAdmin();
            DataSet ds = new DataSet();
            ProductAdmin prodadmin = new ProductAdmin();
            if (Equals(catalogId, 0))
            {
                //Get Product Details
                TList<Product> ProductList = prodadmin.GetAllProducts();
                if (!Equals(ProductList, null))
                {
                    //Convert Product List to Dataset.
                    ds = ProductList.ToDataSet(false);
                }
                else
                {
                    throw new ZnodeException(ErrorCodes.ExportError, "No products found. Please try another catalog.");
                }
            }
            else
            {
                //Get Product Details by Selected Catalog Id.
                ds = prodadmin.GetProductsByCatalogID(catalogId);
            }
            ZNodeProductDataset impProduct = new ZNodeProductDataset();
            if (ds.Tables[0].Rows.Count > 0)
            {
                int parsedInt;
                decimal parsedDec;
                bool parsedBool;
                foreach (DataRow data in ds.Tables[0].Rows)
                {
                    try
                    {
                        ZNodeProductDataset.ZNodeProductRow dataRow = impProduct.ZNodeProduct.NewZNodeProductRow();

                        if (int.TryParse(data["ProductID"].ToString(), out parsedInt))
                        {
                            dataRow.ProductID = parsedInt;
                        }

                        dataRow.SKU = string.Empty;
                        dataRow.QuantityOnHand = 0;
                        dataRow.Name = data["Name"].ToString();
                        dataRow.ShortDescription = HttpUtility.HtmlDecode(data["ShortDescription"].ToString());
                        dataRow.Description = HttpUtility.HtmlDecode(data["Description"].ToString());
                        dataRow.FeaturesDesc = HttpUtility.HtmlDecode(data["FeaturesDesc"].ToString());
                        dataRow.ProductNum = data["ProductNum"].ToString();
                        dataRow.ProductTypeID = int.Parse(data["ProductTypeID"].ToString());
                        if (int.TryParse(data["ExpirationPeriod"].ToString(), out parsedInt))
                        {
                            dataRow.ExpirationPeriod = parsedInt;
                        }

                        if (int.TryParse(data["ExpirationFrequency"].ToString(), out parsedInt))
                        {
                            dataRow.ExpirationFrequency = parsedInt;
                        }

                        if (decimal.TryParse(data["RetailPrice"].ToString(), out parsedDec))
                        {
                            dataRow.RetailPrice = parsedDec;
                        }

                        if (decimal.TryParse(data["SalePrice"].ToString(), out parsedDec))
                        {
                            dataRow.SalePrice = parsedDec;
                        }

                        if (decimal.TryParse(data["WholesalePrice"].ToString(), out parsedDec))
                        {
                            dataRow.WholesalePrice = parsedDec;
                        }

                        if (int.TryParse(data["ReviewStateID"].ToString(), out parsedInt))
                        {
                            dataRow.ReviewStateID = parsedInt;
                        }

                        if (bool.TryParse(data["Franchisable"].ToString(), out parsedBool))
                        {
                            dataRow.Franchisable = parsedBool;
                        }

                        dataRow.ImageFile = data["ImageFile"].ToString();

                        if (decimal.TryParse(data["Weight"].ToString(), out parsedDec))
                        {
                            dataRow.Weight = parsedDec;
                        }

                        if (decimal.TryParse(data["Length"].ToString(), out parsedDec))
                        {
                            dataRow.Length = parsedDec;
                        }

                        if (decimal.TryParse(data["Width"].ToString(), out parsedDec))
                        {
                            dataRow.Width = parsedDec;
                        }

                        if (decimal.TryParse(data["Height"].ToString(), out parsedDec))
                        {
                            dataRow.Height = parsedDec;
                        }

                        if (int.TryParse(data["DisplayOrder"].ToString(), out parsedInt))
                        {
                            dataRow.DisplayOrder = parsedInt;
                        }

                        if (int.TryParse(data["ReviewStateID"].ToString(), out parsedInt))
                        {
                            dataRow.ReviewStateID = parsedInt;
                        }

                        dataRow.ActiveInd = bool.Parse(data["ActiveInd"].ToString());
                        dataRow.CallForPricing = bool.Parse(data["CallForPricing"].ToString());
                        dataRow.HomepageSpecial = bool.Parse(data["HomepageSpecial"].ToString());
                        dataRow.CategorySpecial = bool.Parse(data["CategorySpecial"].ToString());
                        dataRow.InventoryDisplay = int.Parse(data["InventoryDisplay"].ToString());

                        dataRow.Keywords = data["Keywords"].ToString();

                        if (int.TryParse(data["ManufacturerID"].ToString(), out parsedInt))
                        {
                            dataRow.ManufacturerID = parsedInt;
                        }

                        if (int.TryParse(data["ShippingRuleTypeID"].ToString(), out parsedInt))
                        {
                            dataRow.ShippingRuleTypeID = parsedInt;
                        }

                        dataRow.SEOTitle = data["SEOTitle"].ToString();
                        dataRow.SEOKeywords = data["SEOKeywords"].ToString();
                        dataRow.SEODescription = data["SEODescription"].ToString();
                        dataRow.InStockMsg = data["InStockMsg"].ToString();
                        dataRow.OutOfStockMsg = data["OutOfStockMsg"].ToString();

                        if (bool.TryParse(data["TrackInventoryInd"].ToString(), out parsedBool))
                        {
                            dataRow.TrackInventoryInd = parsedBool;
                        }

                        if (bool.TryParse(data["FreeShippingInd"].ToString(), out parsedBool))
                        {
                            dataRow.FreeShippingInd = parsedBool;
                        }

                        if (bool.TryParse(data["NewProductInd"].ToString(), out parsedBool))
                        {
                            dataRow.NewProductInd = parsedBool;
                        }

                        dataRow.SEOURL = data["SEOURL"].ToString();

                        if (int.TryParse(data["MaxQty"].ToString(), out parsedInt))
                        {
                            dataRow.MaxQty = parsedInt;
                        }

                        if (bool.TryParse(data["ShipSeparately"].ToString(), out parsedBool))
                        {
                            dataRow.ShipSeparately = parsedBool;
                        }

                        if (bool.TryParse(data["FeaturedInd"].ToString(), out parsedBool))
                        {
                            dataRow.FeaturedInd = parsedBool;
                        }

                        dataRow.RecurringBillingInd = data["RecurringBillingInd"].ToString();
                        dataRow.RecurringBillingInstallmentInd = data["RecurringBillingInstallmentInd"].ToString();
                        dataRow.PortalID = 0;

                        if (int.TryParse(data["PortalId"].ToString(), out parsedInt))
                        {
                            dataRow.PortalID = parsedInt;
                        }
                        if (int.TryParse(data["ReviewStateID"].ToString(), out parsedInt))
                        {
                            dataRow.ReviewStateID = parsedInt;
                        }

                        dataRow.Franchisable = bool.Parse(data["Franchisable"].ToString());

                        impProduct.ZNodeProduct.AddZNodeProductRow(dataRow);

                    }
                    catch (Exception ex)
                    {
                        throw new ZnodeException(ErrorCodes.ExportError, "Failed to process your request. Please try again.");
                    }
                }
            }
            else
            {
                throw new ZnodeException(ErrorCodes.ExportError, "Unable to download Product data. Please try another catalog.");
            }
            return impProduct;
        }

        /// <summary>
        /// To Get Product Attribute Details.
        /// </summary>
        /// <param name="catalogId">Id for the Selected Catalog.</param>
        /// <param name="attributeTypeId">Id for the Selected Attribute Type/</param>
        /// <returns>Return Product Attribute in DataSet Format</returns>
        private DataSet GetProductAttribute(int catalogId, int attributeTypeId)
        {
            ProductAttributeService attributeService = new ProductAttributeService();
            TList<ProductAttribute> attribute;

            if (!Equals(catalogId, 0) && Equals(attributeTypeId, 0))
            {
                AttributeTypeHelper helper = new AttributeTypeHelper();
                // Get the Product Attributes by Selected Catalog Id.
                DataSet ds = helper.GetAttributesByCatalogID(catalogId);
                return ds;
            }
            else if (!Equals(attributeTypeId, 0))
            {
                // Get the Product Attributes by Selected Attribute Type Id.
                attribute = attributeService.GetByAttributeTypeId(attributeTypeId);
            }
            else
            {
                // Get all the Product Attributes for default selection.
                attribute = attributeService.GetAll();
            }
            //Convert List of Attributes to Dataset.
            DataSet attributeData = attribute.ToDataSet(true);
            if (Equals(attributeData, null) || Equals(attributeData.Tables[0], null) || Equals(attributeData.Tables[0].Rows.Count, 0))
            {
                throw new ZnodeException(ErrorCodes.ExportError, "Unable to download attributes. Please try another catalog or attribute type.");
            }
            return attributeData;
        }

        /// <summary>
        /// To Get Facets Details.
        /// </summary>
        /// <param name="catalogId">Id for the Selected Catalog.</param>
        /// <param name="categoryId">Id for the Selected Category.</param>
        /// <returns>Return Facets in DataSet Format</returns>
        private DataSet GetFacets(int catalogId, int categoryId)
        {
            DataManagerAdmin datamanagerAdmin = new DataManagerAdmin();

            //Get ALL Facets based on Category & Catalog Ids.
            DataSet ds = datamanagerAdmin.GetDownloadFacets(categoryId, catalogId);

            ZNodeFacetsDataSet downloadFacets = new ZNodeFacetsDataSet();
            int parsedInt;
            if (ds.Tables[0].Rows.Count != 0)
            {
                foreach (DataRow data in ds.Tables[0].Rows)
                {
                    ZNodeFacetsDataSet.ZNodeFacetRow dataRow = downloadFacets.ZNodeFacet.NewZNodeFacetRow();

                    dataRow.FacetGroupLabel = data["FacetGroupLabel"].ToString();
                    dataRow.ControlType = data["ControlType"].ToString();
                    if (int.TryParse(data["CatalogID"].ToString(), out parsedInt))
                    {
                        dataRow.CatalogID = parsedInt;
                    }

                    if (int.TryParse(data["DisplayOrder"].ToString(), out parsedInt))
                    {
                        dataRow.DisplayOrder = parsedInt;
                    }

                    dataRow.FacetName = data["FacetName"].ToString().ToString();

                    if (int.TryParse(data["FacetDisplayOrder"].ToString(), out parsedInt))
                    {
                        dataRow.FacetDisplayOrder = parsedInt;
                    }

                    dataRow.IconPath = data["IconPath"].ToString();

                    if (int.TryParse(data["ProductID"].ToString(), out parsedInt))
                    {
                        dataRow.ProductID = parsedInt;
                    }

                    if (int.TryParse(data["CategoryID"].ToString(), out parsedInt))
                    {
                        dataRow.CategoryID = parsedInt;
                    }

                    if (int.TryParse(data["CategoryDisplayOrder"].ToString(), out parsedInt))
                    {
                        dataRow.CategoryDisplayOrder = parsedInt;
                    }
                    downloadFacets.ZNodeFacet.AddZNodeFacetRow(dataRow);
                }
            }
            else
            {
                throw new ZnodeException(ErrorCodes.ExportError, "No Facets found.");
            }
            return downloadFacets;
        }

        /// <summary>
        /// To Get Product Shipping Inventory Details.
        /// </summary>
        /// <param name="userName">User Name for the Login User.</param>
        /// <param name="startDate">Start date for the Order.</param>
        /// <param name="endDate">End date for the Order.</param>
        /// <returns>Return Product Shipping Inventory in DataSet Format</returns>
        private DataSet GetProductShippingInventory(string userName, string startDate, string endDate)
        {
            string Attributes = String.Empty;
            string AttributeList = string.Empty;
            DataSet MyDatas = new DataSet();
            SKUAdmin _SkuAdmin = new SKUAdmin();
            ProductAdmin _adminAccess = new ProductAdmin();
            string FirstName = null;
            string LastName = null;
            string CompanyName = null;
            string AccountNumber = null;
            DateTime? StartDate = null;
            DateTime? EndDate = null;
            int? OrderStateID = null;
            int? PortalID = null;
            int? OrderId = null;
            DataSet shippingDataSet = null;
            string portalIds = string.Empty;

            // Get accessible Portals based on the user name.
            string profileStores = GetAvailablePortals(userName);
            portalIds = string.IsNullOrEmpty(profileStores) ? null : profileStores;

            if (!string.IsNullOrEmpty(startDate))
            {
                StartDate = DateTime.Parse(startDate);
            }

            if (!string.IsNullOrEmpty(endDate))
            {
                EndDate = DateTime.Parse(endDate);
            }

            OrderAdmin _OrderAdmin = new OrderAdmin();
            // Get hte Order Details based on the searching criteria.
            shippingDataSet = _OrderAdmin.FindOrders(OrderId, FirstName, LastName, CompanyName, AccountNumber, StartDate, EndDate, OrderStateID, PortalID, portalIds);
            DataSet dsOrders = new DataSet();
            if (shippingDataSet.Tables[0].Rows.Count != 0)
            {
                dsOrders.Tables.Add(shippingDataSet.Tables[0].DefaultView.ToTable(true, "OrderId", "ShipDate", "TrackingNumber"));
            }
            else
            {
                throw new ZnodeException(ErrorCodes.ExportError, "No Orders Found.");
            }
            return dsOrders;
        }

        /// <summary>
        /// To Get the Customer Pricing Details.
        /// </summary>
        /// <returns>Return Customer Pricing in DataSet Format</returns>
        private DataSet GetCustomerPricing()
        {
            var dataManager = new DataManagerAdmin();
            //Get All the Customer Pricing Details.
            var ds = dataManager.GetAllCustomerPricing();
            return ds;
        }

        /// <summary>
        /// To Get the Customer Details.
        /// </summary>
        /// <returns>Return Customer in DataSet Format</returns>
        private DataSet GetCustomers(string userName)
        {
            List<Tuple<string, string, string>> filterList = new List<Tuple<string, string, string>>();
            string innerWhereClause = string.Empty;
            string whereClause = string.Empty;
            if (!string.IsNullOrEmpty(userName))
            {
                innerWhereClause = string.Format("{0}'{1}'", Constant.username, userName);
            }
            whereClause = string.Format("{0}", "~~rolename~~ IS NULL");
            int totalRowCount = 0;
            StoredProcedureHelper storedProcedureHelper = new StoredProcedureHelper();
            DataSet customerData = storedProcedureHelper.SP_DataDetailsSpConfig(whereClause, string.Empty, StoredProcedureKeys.SpGetCustomerData, null, null, null, out totalRowCount, null, innerWhereClause);
            return customerData;
        }

        /// <summary>
        /// To Get the Product Sku details.
        /// </summary>
        /// <param name="productId">Id for the Product</param>
        /// <param name="catalogId">Id for the Catalog</param>
        /// <param name="productName">Name of the Product</param>
        /// <param name="attributeIds">Ids of the Selected Attribute.</param>
        /// <returns></returns>
        private DataSet GetProductSku(int productId, int catalogId, string productName, string attributeIds)
        {
            DataSet skuData = new DataSet();
            SKUAdmin _SkuAdmin = new SKUAdmin();
            if (string.IsNullOrEmpty(attributeIds) && Equals(productId, 0) && string.IsNullOrEmpty(productName) && catalogId > 0)
            {
                SKUHelper helper = new SKUHelper();
                // Get the Product SKU Details by Catalog Id.
                skuData = helper.GetSkuByCatalogID(catalogId);
            }
            else if (string.IsNullOrEmpty(attributeIds) && Equals(productId, 0) && string.IsNullOrEmpty(productName) && Equals(catalogId, 0))
            {
                // Get All Sku details for default selection criteria.
                skuData = _SkuAdmin.GetAllSKU().ToDataSet(false);
            }
            else
            {
                // Get Sku details based on selected product id & attribute Ids.
                skuData = _SkuAdmin.GetBySKUAttributes(productId, (Equals(attributeIds, null)) ? string.Empty : attributeIds);
            }

            if (Equals(skuData, null) || Equals(skuData.Tables[0], null) || Equals(skuData.Tables[0].Rows.Count, 0))
            {
                throw new ZnodeException(ErrorCodes.ExportError, "No Sku Found.");
            }
            return skuData;
        }

       /// <summary>
       /// To Get the State Details based on Country Code
       /// </summary>
       /// <param name="countryCode">Country Code</param>
       /// <returns>Return the State Details in DataSet format</returns>
        private DataSet GetStateDetails(string countryCode)
        {
            StateHelper stateHelper=new StateHelper();
            DataSet ds = stateHelper.GetStateDetails(countryCode);
            return ds;
        }
        #endregion

        #region Import
        /// <summary>
        /// To Get Uploaded file in Stream Format.
        /// </summary>
        /// <param name="fileData">Uploaded File</param>
        /// <returns>Return Uploaded file in StreamReader format.</returns>
        private StreamReader GetData(string fileData)
        {
            byte[] byteArray = Encoding.UTF8.GetBytes(fileData);
            MemoryStream stream = new MemoryStream(byteArray);
            StreamReader fileReader = new StreamReader(stream);
            return fileReader;
        }

        private void DisplayError(string columnName, int row)
        {
            ZNodeLoggingBase.LogMessage(string.Format("Upload Inventory: Invalid {0} in row:{1}", columnName, row));
            ErrorCount++;
            errorMessage = errorMessage.Append(string.Format("Upload Inventory: Invalid {0} in row:{1}", columnName, row));
        }

        /// <summary>
        /// Convert List of dynamic to the DataTable.
        /// </summary>
        /// <typeparam name="TSource">Generic Type</typeparam>
        /// <param name="data">data for the dynamic list.</param>
        /// <returns>Return the Dynamic list data in DataTable format.</returns>
        public DataTable ToDataTable<TSource>(IList<TSource> data)
        {
            DataTable dt = new DataTable();
            try
            {
                //Convert Dynamic List To Json String.
                string jsonString = JsonConvert.SerializeObject(data);

                //Convert Json String to DataTable.
                dt = JsonConvert.DeserializeObject<DataTable>(jsonString);
            }
            catch (Exception ex) { }
            return dt;

        }

        /// <summary>
        /// To Save the Imported data.
        /// </summary>
        /// <param name="fileDataSet">Uploaded dataSet</param>
        /// <param name="model">Model of type ImportModel</param>
        /// <returns>Return the List of Error in DataTable Format.</returns>
        private DataTable UploadData(DataTable fileDataSet, ImportModel model)
        {
            try
            {
                //ZNodeLogging.LogMessage("Step6 - ImportExportService In Engine.Service - UploadData()");
                DataTable errorLogData = new DataTable();
                //Get Excel Setting from Web Config by import Type.
                excelSetting = ExcelSettings.GetExcelSettingsByMappingName(model.Type);
                //ZNodeLogging.LogMessage("Step7 - ImportExportService In Engine.Service - UploadData() - Excel setting Mapping");

                if (!Equals(excelSetting, null))
                {
                    // Get the List of Rules for the Import Type from Import Mapping Tables.
                    List<ValidationRulesModel> rulesModelList = GetRulesModelListFromSp(model.Type, addProcess);
                    //ZNodeLogging.LogMessage("Step8 - ImportExportService In Engine.Service - UploadData() - GetRulesModelListFromSp() complete");
                    
                    //To Check Whether the Uploaded files, Columns & there sequence match with the mapping.
                    if (IsValidColumnAndRow(fileDataSet, rulesModelList))
                    {
                        //ZNodeLogging.LogMessage("Step9 - ImportExportService In Engine.Service - UploadData() - IsValidColumnAndRow() passed");

                        if ((excelSetting.ErrorExcelSheet) || (excelSetting.InvalidDataSet))
                        {
                            //ZNodeLogging.LogMessage("Step10 - ImportExportService In Engine.Service - UploadData() - SetInvalidDataSet() called");
                            //Add Extra column to the Uploaded DataSet;
                            SetInvalidDataSet(fileDataSet);
                            //ZNodeLogging.LogMessage("Step11 - ImportExportService In Engine.Service - UploadData() - SetInvalidDataSet() returened");
                        }
                        int ChunkSize = excelSetting.ChunkSize;
                        int startIndex = 0;
                        int totalRows = fileDataSet.Rows.Count;
                        int totalRowCount = totalRows / ChunkSize;
                        if (totalRows % ChunkSize > 0)
                        {
                            totalRowCount++;
                        }
                        //Get the Unique Data set to pass on to the Stored Procedure as input type.
                        //ZNodeLogging.LogMessage("Step12 - ImportExportService In Engine.Service - UploadData() - SetValidDataSet() called");
                        DataTable validData = SetValidDataSet();
                        //ZNodeLogging.LogMessage("Step13 - ImportExportService In Engine.Service - UploadData() - SetValidDataSet() returned");
                        //Set Extra Columns.
                        DataTable extraColumntable = SetExtraColumns(null, model.Type);
                        //ZNodeLogging.LogMessage("Step14 - ImportExportService In Engine.Service - UploadData() - SetExtraColumns() returned");
                        for (int i = 0; i < totalRowCount; i++)
                        {
                            chunkData = fileDataSet.DefaultView.ToTable().Rows.Cast<DataRow>().Skip(startIndex).Take(ChunkSize).CopyToDataTable();
                            chunkData.TableName = fileDataSet.TableName;
                            startIndex = startIndex + ChunkSize;
                            //Validate & Save the Uploaded Data.
                            //ZNodeLogging.LogMessage("Step15 - ImportExportService In Engine.Service - UploadData() - ValidateAndSave() called for row="+i+1);
                            errorLogData = ValidateAndSave(chunkData, validData, model.Type, extraColumntable, new List<ValidationRulesModel>(), addProcess);
                            //ZNodeLogging.LogMessage("Step22 - ImportExportService In Engine.Service - UploadData() - ValidateAndSave() returend for row=" + i + 1);
                        }
                    }
                }
                else
                {
                    //ZNodeLogging.LogMessage("ImportExportService In Engine.Service - UploadData() - excel setting mapping error");
                    //ZNodeLoggingBase.LogMessage(string.Format("UploadData() - excel setting mapping error"));
                    throw new InvalidMappingKeyException("InvalidMappingKeyError");
                }
                return errorLogData;
            }
            catch (Exception ex)
            {
                //ZNodeLogging.LogMessage("error in ImportExportService In Engine.Service - UploadData() EX = "+ex.ToString());
                ZNodeLoggingBase.LogMessage(string.Format("Import Error {0} occurs while uploading file of type {1}", ex.Message, model.Type));
                ErrorHandling(ex);
                throw ex;
            }
        }

        #endregion

        #region Import Utitility
        /// <summary>
        /// To Remove Empty rows from the DataTable.
        /// </summary>
        /// <param name="datatable"></param>
        /// <returns>Return the Data in DataTable Format</returns>
        private DataTable RemoveEmptyRows(DataTable datatable)
        {
            //Removes Empty Rows from the DataTable.
            for (int i = datatable.Rows.Count - 1; i >= 0; i += -1)
            {
                DataRow row = datatable.Rows[i];
                if (Equals(row[0], null))
                {
                    datatable.Rows.Remove(row);
                }
                else if (string.IsNullOrEmpty(row[0].ToString()))
                {
                    datatable.Rows.Remove(row);
                }
            }
            return datatable;
        }

        /// <summary>
        /// To Get the Mapping Details based on the Mapping key.
        /// </summary>
        /// <param name="mappingKey">Key for the Mapping</param>
        /// <param name="processFlag">Key for the Process</param>
        /// <returns>Return the Mapping Details in List<ValidationRulesModel> format.</returns>
        private List<ValidationRulesModel> GetRulesModelListFromSp(string mappingKey, string processFlag)
        {
            List<ValidationRulesModel> rulesModelList = new List<ValidationRulesModel>();
            ExecuteSpHelper executeStoredProcedure = new ExecuteSpHelper();
            executeStoredProcedure.GetParameter(excelSheetNameParameter, mappingKey, ParameterDirection.Input, SqlDbType.NVarChar);
            executeStoredProcedure.GetParameter(processFlagParameter, processFlag, ParameterDirection.Input, SqlDbType.NVarChar);
            //Get Mapping Details used for Validating the Uploaded data.
            DataSet data = executeStoredProcedure.GetSPResultInDataSet(getMappingSpName);
            rulesModelList = data.Tables[0].ToList<ValidationRulesModel>();
            return rulesModelList;
        }

        /// <summary>
        /// To Check Valid Columns & Rows of the uploaded data with the mapping.
        /// </summary>
        /// <param name="fileData">Uploaded File Data</param>
        /// <param name="rulesModelList">List of the Mapping Details</param>
        /// <returns>Return true or false</returns>
        private bool IsValidColumnAndRow(DataTable fileData, List<ValidationRulesModel> rulesModelList)
        {
            //Get the column Names from the DataTable.
            string[] columnNamesInFile = (from column in fileData.Columns.Cast<DataColumn>()
                                          select column.ColumnName).ToArray();

            //Check whether the No. of Columns are match with the mapping.
            if (IsValidColumnLength(rulesModelList, columnNamesInFile))
            {
                // Check whether the Column sequence match with the Mapping.
                if (IsValidColumnSequence(rulesModelList, columnNamesInFile))
                {
                    //Check for Valid Row Count Based on Web.Config Setting.
                    return IsValidRowCount(fileData);
                }
                else
                {
                    //ZNodeLogging.LogMessage("ImportExportService In Engine.Service - IsValidColumnSequence() failed");
                    throw new ColumnMismatchException("ColumnMismatchError");
                }
            }
            else
            {
                //ZNodeLogging.LogMessage("ImportExportService In Engine.Service - IsValidColumnLength() failed");
                throw new ColumnLengthException("ColumnLengthError");
            }
        }

        /// <summary>
        /// To Check Valid Columns Length of the uploaded data with the mapping.
        /// </summary>
        /// <param name="rulesModelList">List of the Mapping Details</param>
        /// <param name="columnNamesInFile">Column names of the Uploaded file</param>
        /// <returns>Return true or false</returns>
        private bool IsValidColumnLength(List<ValidationRulesModel> rulesModelList, string[] columnNamesInFile)
        {
            return Equals(columnNamesInFile.Count(), rulesModelList.Count());
        }

        /// <summary>
        /// To Check Valid Columns Sequence of the uploaded data with the mapping.
        /// </summary>
        /// <param name="rulesModelList">List of the Mapping Details</param>
        /// <param name="columnNamesInFile">Column names of the Uploaded file</param>
        /// <returns>Return true or false</returns>
        private bool IsValidColumnSequence(List<ValidationRulesModel> rulesModelList, string[] columnNamesInFile)
        {
            for (int i = 0; i < columnNamesInFile.Count(); i++)
            {
                if (!Equals(columnNamesInFile[i], rulesModelList[i].ExcelColumnName))
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// To Check Valid Columns & Rows of the uploaded data with the mapping.
        /// </summary>
        /// <param name="fileData">Uploaded File Data</param>
        /// <returns>Return true or false</returns>
        private bool IsValidRowCount(DataTable fileData)
        {
            //Check for Valid Row Count, set in Web.Config file.
            int maxRowCount = excelSetting.MaxRowAllowed;
            if (fileData.Rows.Count < 1)
            {
                throw new InsufficientDataException(string.Format("InsufficientDataError"));
            }
            else if (fileData.Rows.Count > excelSetting.MaxRowAllowed)
            {
                throw new DataExceedException(string.Format("DataExceedError{0}", maxRowCount));
            }
            return true;
        }

        /// <summary>
        /// To Add Error Columns in Existing Uploaded data table
        /// </summary>
        /// <param name="fileData">Uploaded File Data</param>
        private void SetInvalidDataSet(DataTable fileData)
        {
            DataTable invalidDataTable = fileData.Copy();
            invalidDataTable.Clear();
            invalidDataTable.Columns.Add(ErrorColumn);
            invalidDataSet.Tables.Add(invalidDataTable);
        }

        /// <summary>
        /// To Set the Unique dataSet to be passed on to Stored Procudure for all import type
        /// </summary>
        /// <returns>Return Uploaded File Data in DataTable Format</returns>
        private DataTable SetValidDataSet()
        {
            DataTable validData = new DataTable();
            validData.Columns.Add(recordIdColumn);
            for (int columnIndex = 1; columnIndex <= noOfColumn; columnIndex++)
            {
                validData.Columns.Add(col + columnIndex);
            }
            return validData;
        }

        /// <summary>
        /// To Set the Extra column Details required for Import.
        /// </summary>
        /// <param name="extraColumns">Dictionary of Extra Columns</param>
        /// <param name="mappingKey">Key for the Mapping</param>
        /// <returns>Return the Data in DataTable Format.</returns>
        private DataTable SetExtraColumns(Dictionary<string, object> extraColumns, string mappingKey)
        {
            DataTable extraParmaterDatatable = new DataTable();
            try
            {
                IEnumerable<DataRow> rows = GetExtraColumnsBySp(mappingKey).Tables[0].AsEnumerable();
                extraColumns = ValidateExtraColumns(extraColumns, rows);

                extraParmaterDatatable.Columns.Add(columnName);// Select Column Sequence on the basis of columnSession
                extraParmaterDatatable.Columns.Add(columnValue); // Assign Value to Column Sequence on the basis of columnSession
                if (extraColumns != null)
                {
                    DataRow row = null;

                    foreach (string key in extraColumns.Keys)
                    {
                        row = extraParmaterDatatable.NewRow();
                        row[0] = key;
                        row[1] = extraColumns[key];
                        extraParmaterDatatable.Rows.Add(row);
                    }
                }
            }
            catch (Exception ex)
            {
                ZNodeLogging.LogMessage("ImportExportService In Engine.Service - UploadData() - exception in SetExtraColumns() ex = "+ex.ToString());
            }
            return extraParmaterDatatable;
        }

        /// <summary>
        /// To Get Extra Column Details based on Mapping Key.
        /// </summary>
        /// <param name="mappingKey">Key for the Mapping</param>
        /// <returns>Return the Extra column details in DataSet Format.</returns>
        private DataSet GetExtraColumnsBySp(string mappingKey)
        {
            ExecuteSpHelper executeStoredProcedure = new ExecuteSpHelper();
            executeStoredProcedure.GetParameter(excelSheetNameParameter, mappingKey, ParameterDirection.Input, SqlDbType.NVarChar);
            executeStoredProcedure.GetParameter(processFlagParameter, columnSession, ParameterDirection.Input, SqlDbType.NVarChar);
            return executeStoredProcedure.GetSPResultInDataSet(getTableColumnDetailsSp);
        }

        /// <summary>
        /// To Validate Extra Column Details.
        /// </summary>
        /// <param name="extraColumns">Dictionary of Extra Columns</param>
        /// <param name="rows"></param>
        /// <returns>Return the Dectionary of Extra Columns</returns>
        private Dictionary<string, object> ValidateExtraColumns(Dictionary<string, object> extraColumns, IEnumerable<DataRow> rows)
        {
            string missingcolumnNames = string.Empty;
            Dictionary<string, object> validExtraColumns = new Dictionary<string, object>();
            string[] columns = rows.Where(s => s.Field<bool>(allowNull).Equals(false)).Select(s => s.Field<string>(columnName)).ToArray<string>();
            string[] optionalColumns = rows.Where(s => s.Field<bool>(allowNull).Equals(true)).Select(s => s.Field<string>(columnName)).ToArray<string>();

            foreach (string column in columns)
            {
                if (extraColumns.Keys.Contains(column))
                {
                    validExtraColumns.Add(column, extraColumns[column]);
                    extraColumns.Remove(column);
                }
                else
                {
                    missingcolumnNames += string.Concat(column, ",");
                }
            }
            string[] keys = extraColumns.Keys.ToArray();
            foreach (string key in keys)
            {
                if (optionalColumns.Contains(key))
                {
                    validExtraColumns.Add(key, extraColumns[key]);
                    extraColumns.Remove(key);
                }
            }
            if (!string.IsNullOrEmpty(missingcolumnNames))
            {
                throw new ExtraColumnException(string.Format("ExtraColumnMissingError{0}", missingcolumnNames));
            }
            return validExtraColumns;
        }

        /// <summary>
        /// To Validate & Save the Uploaded File Data.
        /// </summary>
        /// <param name="fileDataTable">Uploaded File Data</param>
        /// <param name="validData">Uploaded File Data</param>
        /// <param name="mappingKey">Key of the Mapping</param>
        /// <param name="extraColumns">Dictionaty for the Extra Columns</param>
        /// <param name="rulesList">Mapping Details</param>
        /// <param name="processFlag">Key for the Process</param>
        /// <returns>Return the Error List in DataTable Format.</returns>
        private DataTable ValidateAndSave(DataTable fileDataTable, DataTable validData, string mappingKey, DataTable extraColumns, List<ValidationRulesModel> rulesList, string processFlag)
        {
            DataTable errorLogData = new DataTable();
            //ZNodeLogging.LogMessage("Step16 - ImportExportService In Engine.Service - UploadData() - ValidateAndSave() -  ValidateData() start");
            validData = ValidateData(fileDataTable, validData, rulesList, processFlag);
            //ZNodeLogging.LogMessage("Step17 - ImportExportService In Engine.Service - UploadData() - ValidateAndSave() -  ValidateData() end");
            if (!stopProcessingData)
            {
                if (validData.Rows.Count >= 1)
                {
                    validData.TableName = fileDataTable.TableName;
                    //ZNodeLogging.LogMessage("Step18 - ImportExportService In Engine.Service - UploadData() - ValidateAndSave() -  SaveDataToDatabase() called");
                    errorLogData = SaveDataToDatabase(validData, mappingKey, processFlag);
                    //ZNodeLogging.LogMessage("Step21 - ImportExportService In Engine.Service - UploadData() - ValidateAndSave() -  SaveDataToDatabase() returned");
                }
            }
            return errorLogData;
        }

        /// <summary>
        /// To Set the Error Model.
        /// </summary>
        /// <param name="fileData">Uploaded File Data</param>
        /// <param name="recordNo">Id of the Record</param>
        /// <param name="rowIndex">Index of the Row</param>
        /// <param name="rowErrorList">List of the Errors</param>
        private void SetError(DataTable fileData, int recordNo, int rowIndex, List<RecordErrorModel> rowErrorList)
        {
            foreach (RecordErrorModel model in rowErrorList)
            {
                model.RecordNo = recordNo;
                model.TableName = fileData.TableName;
            }
            if (excelSetting.ErrorInformation)
            {
                errorList = errorList.Union(rowErrorList).ToList();
            }
            if (excelSetting.ErrorRecordNumber)
            {
                errorInRecordIdString += string.Concat(recordNo, ",");
            }
            if ((excelSetting.InvalidDataSet) || (excelSetting.ErrorExcelSheet))
            {
                //DataRow invalidDataRow = invalidDataSet.Tables[fileData.TableName].NewRow();
                DataRow invalidDataRow = invalidDataSet.Tables[0].NewRow();
                for (int i = 0; i < fileData.Columns.Count; i++)
                {
                    invalidDataRow[i] = fileData.Rows[rowIndex][i];
                }
                invalidDataRow[errorMessageColumn] = GetRowErrorMessage(rowErrorList);
                //invalidDataSet.Tables[fileData.TableName].Rows.Add(invalidDataRow);
                invalidDataSet.Tables[0].Rows.Add(invalidDataRow);
            }
        }

        /// <summary>
        /// To Get Error Row Message.
        /// </summary>
        /// <param name="errorList">List of the Errors</param>
        /// <returns>Return Error Details in string format.</returns>
        private string GetRowErrorMessage(List<RecordErrorModel> errorList)
        {
            string errorMessage = string.Empty;
            foreach (RecordErrorModel error in errorList)
            {
                errorMessage += error.ColumnName + ":" + error.ErrorMessage + "; ";
            }
            return errorMessage;
        }

        /// <summary>
        /// To Validate the Data.
        /// </summary>
        /// <param name="fileData">Uploaded File Data</param>
        /// <param name="validData">Uploaded File Data</param>
        /// <param name="rulesList">Mapping Details</param>
        /// <param name="processFlag">Key for the Process</param>
        /// <returns>Return the Uplaoded File Data in DataTable Format</returns>
        private DataTable ValidateData(DataTable fileData, DataTable validData, List<ValidationRulesModel> rulesList, string processFlag)
        {
            string[] columnNamesInFile = (from column in fileData.Columns.Cast<DataColumn>()
                                          select column.ColumnName).ToArray();

            for (int rowIndex = 0; rowIndex < fileData.Rows.Count; rowIndex++)
            {
                if (fileData.Rows.Count > rowIndex)
                {
                    validData = SetValidRow(fileData, validData, recordId, fileData.Rows[rowIndex], processFlag);
                }
                else
                {
                    break;
                }
                recordId++;
            }
            return validData;
        }
        /// <summary>
        /// To Set Valid Row Record
        /// </summary>
        /// <param name="fileData">Uploaded File Data</param>
        /// <param name="fileDataToSave">Uploaded File Data</param>
        /// <param name="recordId">Id of the Record</param>
        /// <param name="dataRow">Row of the Data</param>
        /// <param name="processFlag">Key of the Process flag.</param>
        /// <returns>Return the Uplaoded File Data in DataTable Format</returns>
        private DataTable SetValidRow(DataTable fileData, DataTable fileDataToSave, int recordId, DataRow dataRow, string processFlag)
        {
            DataRow row = fileDataToSave.NewRow();
            row[0] = (processFlag == addProcess) ? recordId : dataRow[0];
            int k = 1;
            for (int j = 0; j < fileData.Columns.Count; j++)
            {
                if (fileData.Columns[j].ColumnName != idColumn)
                {
                    row[k] = dataRow[j];
                    k++;
                }
            }
            fileDataToSave.Rows.Add(row);
            return fileDataToSave;
        }

        /// <summary>
        /// To Save the Uploaded Data in the DataBase.
        /// </summary>
        /// <param name="fileData">Uploaded File Data</param>
        /// <param name="mappingKey">Key for the mapping</param>
        /// <param name="processFlag">key for the process flag</param>
        /// <returns>Return the Error Details in DataTable Format</returns>
        private DataTable SaveDataToDatabase(DataTable fileData, string mappingKey, string processFlag)
        {
            DataTable errorData = null;
            try
            {
                //ZNodeLogging.LogMessage("Step19 - ImportExportService In Engine.Service - UploadData() - SaveDataToDatabase() starts");
                ExecuteSpHelper executeStoredProcedure = new ExecuteSpHelper();
                errorData = executeStoredProcedure.SaveDataToDatabase(fileData, mappingKey, processFlag, InsertImportDataSPName);
                //ZNodeLogging.LogMessage("Step20 - ImportExportService In Engine.Service - UploadData() - SaveDataToDatabase() complete");
            }
            catch (Exception ex)
            {
                ZNodeLogging.LogMessage("Exception in Engine.Service - UploadData() - SaveDataToDatabase() EX = "+ ex.ToString());
                ZNodeLoggingBase.LogMessage(string.Format("Import Error {0} occurs while Insert DataBase Operation for file type {1}", ex.Message, mappingKey));
                throw ex;
            }
            return errorData;
        }

        private void ErrorHandling(Exception ex)
        {
            if (!Equals(ex, null))
            {
                string errorType = ex.GetType().Name.ToString();
                switch (errorType)
                {
                    case "ColumnLengthException":
                        throw new ZnodeException(ErrorCodes.ImportError, "Please upload valid file.");
                    case "ColumnMismatchException":
                        throw new ZnodeException(ErrorCodes.ImportError, "Please verify columns of excel.");
                    case "InvalidMappingKeyException":
                        throw new ZnodeException(ErrorCodes.ImportError, "Invalid Mapping Key Error");
                }
            }
            else
            {
                throw ex;
            }
        }

        #endregion
        #endregion
    }
}
