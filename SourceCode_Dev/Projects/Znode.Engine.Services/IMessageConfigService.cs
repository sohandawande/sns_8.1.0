﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    /// <summary>
    /// Message Config Service Interface
    /// </summary>
	public interface IMessageConfigService
	{
        /// <summary>
        /// Get Message Config
        /// </summary>
        /// <param name="messageConfigId">messageConfigId</param>
        /// <param name="expands">expands</param>
        /// <returns>Returns MessageConfigModel</returns>
		MessageConfigModel GetMessageConfig(int messageConfigId, NameValueCollection expands);

        /// <summary>
        /// Get Message Configs
        /// </summary>
        /// <param name="expands">expands</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <param name="page">page</param>
        /// <returns>Returns MessageConfigListModel</returns>
		MessageConfigListModel GetMessageConfigs(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
		
        /// <summary>
        /// Get Message Configs By Keys
        /// </summary>
        /// <param name="keys">keys</param>
        /// <param name="expands">expands</param>
        /// <param name="sorts">sorts</param>
        /// <returns>Returns MessageConfigListModel</returns>
        MessageConfigListModel GetMessageConfigsByKeys(string keys, NameValueCollection expands, NameValueCollection sorts);

        /// <summary>
        /// Get Message Config
        /// </summary>
        /// <param name="key">key</param>
        /// <param name="portalId">portalId</param>
        /// <param name="language">language</param>
        /// <param name="messageType">messageType</param>
        /// <returns>Returns MessageConfigModel</returns>
	    MessageConfigModel GetMessageConfig(string key, int portalId, int language, int messageType);

        /// <summary>
        /// Create Message Config
        /// </summary>
        /// <param name="model">MessageConfigModel</param>
        /// <returns>Returns MessageConfigModel</returns>
		MessageConfigModel CreateMessageConfig(MessageConfigModel model);

        /// <summary>
        /// Update Message Config
        /// </summary>
        /// <param name="messageConfigId">messageConfigId</param>
        /// <param name="model">MessageConfigModel</param>
        /// <returns>Returns MessageConfigModel</returns>
		MessageConfigModel UpdateMessageConfig(int messageConfigId, MessageConfigModel model);

        /// <summary>
        /// Delete Message Config
        /// </summary>
        /// <param name="messageConfigId">messageConfigId</param>
        /// <returns>Returns true/false</returns>
		bool DeleteMessageConfig(int messageConfigId);
	}
}
