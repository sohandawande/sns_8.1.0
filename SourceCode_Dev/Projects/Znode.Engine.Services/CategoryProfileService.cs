﻿using Znode.Engine.Api.Models;
using Znode.Engine.Services.Maps;
using CategoryAdminRepository = ZNode.Libraries.Admin.CategoryAdmin;
using CategoryProfileRepository = ZNode.Libraries.DataAccess.Data.DataRepository;

namespace Znode.Engine.Services
{
    public class CategoryProfileService : BaseService, ICategoryProfileService
    {
        #region Private Variables
        private readonly CategoryAdminRepository _catgoryAdminRepository;
        private readonly CategoryProfileRepository _categoryProfileRepository;
        #endregion

        #region Constructor
        public CategoryProfileService()
        {
            _catgoryAdminRepository = new CategoryAdminRepository();            
        }
        #endregion

        #region Public Methods

        public CategoryProfileModel GetCategoryProfileByCategoryProfileId(int categoryProfileId)
        {
            return CategoryProfileMap.ToModel(CategoryProfileRepository.CategoryProfileProvider.GetByCategoryProfileID(categoryProfileId));
        }

        public CategoryProfileListModel GetCategoryProfileByCategoryId(int categoryId)
        {
            return CategoryProfileMap.ToModelList(_catgoryAdminRepository.GetCategoryProfileByCategoryID(categoryId));
        }

        public CategoryProfileModel Create(CategoryProfileModel model)
        {
            return CategoryProfileMap.ToModel(CategoryProfileRepository.CategoryProfileProvider.Save(CategoryProfileMap.ToEntity(model)));
        }

        public bool UpdateCategoryProfile(int categoryProfileId, CategoryProfileModel model)
        {
            return CategoryProfileRepository.CategoryProfileProvider.Update(CategoryProfileMap.ToEntity(model));
        }

        public bool DeleteCategoryProfile(int categoryProfileId)
        {
            return CategoryProfileRepository.CategoryProfileProvider.Delete(categoryProfileId);
        }

        #endregion
    }
}
