﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using CSSRepository = ZNode.Libraries.DataAccess.Service.CSSService;
using ThemeRepository = ZNode.Libraries.DataAccess.Service.ThemeService;


namespace Znode.Engine.Services
{
    public class CSSService : BaseService, ICSSService
    {
        #region Private Variables
        private readonly CSSRepository _cssRepository;
        private readonly ThemeRepository _themeRepository;
        #endregion

        #region Public Constructor
        public CSSService()
        {
            _cssRepository = new CSSRepository();
            _themeRepository = new ThemeRepository();
        } 
        #endregion

        #region Public Methods
        public CSSListModel GetCSSs(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new CSSListModel();
            var cssList = new TList<CSS>();
            var tempList = new TList<CSS>();

            var query = new CSSQuery();
            var sortBuilder = new CSSSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            tempList = _cssRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (pagingLength.Equals(int.MaxValue))
            {
                model.PageSize = model.TotalResults;
            }

            // Now go through the list and get any expands
            foreach (var css in tempList)
            {
                GetExpands(expands, css);
                cssList.Add(css);
            }

            // Map each item and add to the list
            foreach (var a in cssList)
            {
                model.CSSs.Add(CSSMap.ToModel(a));
            }

            return model;
        }

        public CSSModel CreateCSS(CSSModel model)
        {
            if (Equals(model, null))
            {
                throw new Exception("CSS model can not be null");
            }

            var entity = CSSMap.ToEntity(model);

            if (this.IsDuplicateCSS(model))
            {
                throw new Exception("This CSS already exists.");
            }

            var css = _cssRepository.Save(entity);
            return CSSMap.ToModel(css);
        }

        public CSSModel GetCSS(int cssId)
        {
            var css = _cssRepository.GetByCSSID(cssId);
            return !Equals(css, null) ? CSSMap.ToModel(css) : null;
        }

        public CSSModel UpdateCSS(int cssId, CSSModel model)
        {
            if (cssId < 1)
            {
                throw new Exception("CSS ID cannot be less than 1.");
            }

            if (Equals(model, null))
            {
                throw new Exception("CSS model cannot be null.");
            }
            var css = _cssRepository.GetByCSSID(cssId);
            if (!Equals(css, null))
            {
                // Set the promotion ID and map
                model.CSSID = cssId;
                var cssToUpdate = CSSMap.ToEntity(model);

                var updated = _cssRepository.Update(cssToUpdate);
                if (updated)
                {
                    css = _cssRepository.GetByCSSID(cssId);
                    return CSSMap.ToModel(css);
                }
            }
            return null;
        }

        public bool DeleteCSS(int cssId)
        {
            bool isAssociate = false;
            CSSHelper cssHelper = new CSSHelper();
            if (cssId < 1)
            {
                throw new Exception("CSS ID cannot be less than 1.");
            }
            else
            {
                if (!cssHelper.DeleteCSSByID(cssId))
                {
                    throw new Exception("An error occurred and the CSS could not be deleted. Please ensure that this CSS  is not in use.");
                }
                else
                {
                    isAssociate = true;
                }
            }
            return isAssociate;
        }
        #endregion

        #region Private Methods
        private void SetFiltering(List<Tuple<string, string, string>> filters, CSSQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;

                if (filterKey.Equals(FilterKeys.CSSName)) { SetQueryParameter(CSSColumn.Name, filterOperator, filterValue, query); }
                if (filterKey.Equals(FilterKeys.CSSThemeId)) { SetQueryParameter(CSSColumn.ThemeID, filterOperator, filterValue, query); }
                if (filterKey.Equals(FilterKeys.CSSId)) { SetQueryParameter(CSSColumn.CSSID, filterOperator, filterValue, query); }
            }
        }

        private void SetSorting(NameValueCollection sorts, CSSSortBuilder sortBuilder)
        {
            if (sorts.HasKeys())
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);

                    if (key.Equals(SortKeys.CSSName)) { SetSortDirection(CSSColumn.Name, value, sortBuilder); }
                }
            }
        }

        private void GetExpands(NameValueCollection expands, CSS css)
        {
            if (expands.HasKeys())
            {
                ExpandTheme(expands, css);
            }
        }

        private void ExpandTheme(NameValueCollection expands, CSS css)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Theme)))
            {
                if (css.ThemeID > 0)
                {
                    var themes = _themeRepository.GetByThemeID(css.ThemeID);
                    if (!Equals(themes, null))
                    {
                        css.ThemeIDSource = themes;
                    }
                }
            }
        }

        private bool IsDuplicateCSS(CSSModel model)
        {
            if (!Equals(model, null))
            {
                var query = new CSSQuery();

                query.AppendEquals(CSSColumn.Name, model.Name);
                query.AppendEquals(CSSColumn.ThemeID, model.ThemeID.ToString());
                var css = _cssRepository.Find(query);
                if (!Equals(css, null) && css.Count > 0)
                {
                    return true;
                }
            }
            return false;
        }
        #endregion
    }
}
