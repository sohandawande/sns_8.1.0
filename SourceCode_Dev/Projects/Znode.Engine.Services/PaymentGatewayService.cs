﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using PaymentGatewayRepository = ZNode.Libraries.DataAccess.Service.GatewayService;

namespace Znode.Engine.Services
{
	public class PaymentGatewayService : BaseService, IPaymentGatewayService
	{
		private readonly PaymentGatewayRepository _paymentGatewayRepository;

		public PaymentGatewayService()
		{
			_paymentGatewayRepository = new PaymentGatewayRepository();
		}

		public PaymentGatewayModel GetPaymentGateway(int paymentGatewayId)
		{
			var paymentGateway = _paymentGatewayRepository.GetByGatewayTypeID(paymentGatewayId);
			return PaymentGatewayMap.ToModel(paymentGateway);
		}

		public PaymentGatewayListModel GetPaymentGateways(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
		{
			var model = new PaymentGatewayListModel();
			var paymentGateways = new TList<Gateway>();

			var query = new GatewayQuery();
			var sortBuilder = new GatewaySortBuilder();
			var pagingStart = 0;
			var pagingLength = 0;

			SetFiltering(filters, query);
			SetSorting(sorts, sortBuilder);
			SetPaging(page, model, out pagingStart, out pagingLength);

			// Get the initial set
			var totalResults = 0;
			paymentGateways = _paymentGatewayRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
			model.TotalResults = totalResults;

			// Check to set page size equal to the total number of results
			if (pagingLength == int.MaxValue)
			{
				model.PageSize = model.TotalResults;
			}

			// Map each item and add to the list
			foreach (var p in paymentGateways)
			{
				model.PaymentGateways.Add(PaymentGatewayMap.ToModel(p));
			}

			return model;
		}

		public PaymentGatewayModel CreatePaymentGateway(PaymentGatewayModel model)
		{
			if (model == null)
			{
				throw new Exception("Payment gateway model cannot be null.");
			}

			var entity = PaymentGatewayMap.ToEntity(model);
			var paymentGateway = _paymentGatewayRepository.Save(entity);
			return PaymentGatewayMap.ToModel(paymentGateway);
		}

		public PaymentGatewayModel UpdatePaymentGateway(int paymentGatewayId, PaymentGatewayModel model)
		{
			if (paymentGatewayId < 1)
			{
				throw new Exception("Payment gateway ID cannot be less than 1.");
			}

			if (model == null)
			{
				throw new Exception("Payment gateway model cannot be null.");
			}

			var paymentGateway = _paymentGatewayRepository.GetByGatewayTypeID(paymentGatewayId);
			if (paymentGateway != null)
			{
				// Set the payment gateway ID
				model.PaymentGatewayId = paymentGatewayId;

				var paymentGatewayToUpdate = PaymentGatewayMap.ToEntity(model);

				var updated = _paymentGatewayRepository.Update(paymentGatewayToUpdate);
				if (updated)
				{
					paymentGateway = _paymentGatewayRepository.GetByGatewayTypeID(paymentGatewayId);
					return PaymentGatewayMap.ToModel(paymentGateway);
				}
			}

			return null;
		}

		public bool DeletePaymentGateway(int paymentGatewayId)
		{
			if (paymentGatewayId < 1)
			{
				throw new Exception("Payment gateway ID cannot be less than 1.");
			}

			var paymentGateway = _paymentGatewayRepository.GetByGatewayTypeID(paymentGatewayId);
			if (paymentGateway != null)
			{
				return _paymentGatewayRepository.Delete(paymentGateway);
			}

			return false;
		}

		private void SetFiltering(List<Tuple<string, string, string>> filters, GatewayQuery query)
		{
			foreach (var tuple in filters)
			{
				var filterKey = tuple.Item1;
				var filterOperator = tuple.Item2;
				var filterValue = tuple.Item3;

				if (filterKey == FilterKeys.Name) SetQueryParameter(GatewayColumn.GatewayName, filterOperator, filterValue, query);
			}
		}

		private void SetSorting(NameValueCollection sorts, GatewaySortBuilder sortBuilder)
		{
			if (sorts.HasKeys())
			{
				foreach (var key in sorts.AllKeys)
				{
					var value = sorts.Get(key);

					if (key == SortKeys.Name) SetSortDirection(GatewayColumn.GatewayName, value, sortBuilder);
					if (key == SortKeys.PaymentGatewayId) SetSortDirection(GatewayColumn.GatewayTypeID, value, sortBuilder);
				}
			}
		}

		private void SetQueryParameter(GatewayColumn column, string filterOperator, string filterValue, GatewayQuery query)
		{
			base.SetQueryParameter(column, filterOperator, filterValue, query);
		}

		private void SetSortDirection(GatewayColumn column, string value, GatewaySortBuilder sortBuilder)
		{
			base.SetSortDirection(column, value, sortBuilder);
		}
	}
}
