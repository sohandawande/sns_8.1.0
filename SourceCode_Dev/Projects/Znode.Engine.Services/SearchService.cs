﻿using StructureMap;
using System;
using System.Collections.Specialized;
using System.Data;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Helpers.Extensions;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Search.Interfaces;
using ZNode.Libraries.Search.LuceneSearchProvider.Search;

namespace Znode.Engine.Services
{
	public class SearchService : BaseService, ISearchService
	{
		public SearchService()
		{
			ObjectFactory.Initialize(x =>
			{
				x.For<IZnodeSearchProvider>().Use<LuceneSearchProvider>();
				x.For<IZNodeSearchRequest>().Use<LuceneSearchRequest>();
			});
		}

		public KeywordSearchModel GetKeywordSearch(KeywordSearchModel model, NameValueCollection expands, NameValueCollection sorts, NameValueCollection page)
		{
			var searchProvider = ObjectFactory.GetInstance<IZnodeSearchProvider>();

			var searchRequest = KeywordSearchMap.ToZnodeRequest(model);
			searchRequest.PortalCatalogID = ZNodeCatalogManager.CatalogConfig.PortalCatalogID;

			GetExpands(expands, searchRequest);
			SetSorting(sorts, searchRequest);

			var response = searchProvider.Search(searchRequest);

            if (!string.IsNullOrEmpty(model.Keyword))
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.KeywordSearch, model.Keyword.ToLower());
            }

			return KeywordSearchMap.ToModel(response);
		}

		public SuggestedSearchListModel GetSearchSuggestions(SuggestedSearchModel model)
		{
			var searchProvider = ObjectFactory.GetInstance<IZnodeSearchProvider>();

			var response = searchProvider.SuggestTermsFor(model.Term, model.Category, ZNodeCatalogManager.CatalogConfig.PortalCatalogID,model.ExternalIdNullCheck);

			if (response != null && response.Count > 0)
			{
				return SuggestedSearchMap.ToModel(response);
			}

			return null;
		}

		private void GetExpands(NameValueCollection expands, IZNodeSearchRequest request)
		{
			ExpandCategories(expands, request);
			ExpandFacets(expands, request);
		}

		private void ExpandCategories(NameValueCollection expands, IZNodeSearchRequest request)
		{
			if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Categories)))
			{
				request.GetCategoriesHeirarchy = true;
			}
		}

		private void ExpandFacets(NameValueCollection expands, IZNodeSearchRequest request)
		{
			if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Facets)))
			{
				request.GetFacets = true;
			}
		}

		private void SetSorting(NameValueCollection expands, IZNodeSearchRequest request)
		{
			if (expands.HasKeys())
			{
				foreach (var key in expands.AllKeys)
				{
					var value = expands.Get(key);

					if (key == SortKeys.Name)
					{
						request.SortOrder = value.ToLower() == "desc" ? 5 : 4;
					}

					if (key == SortKeys.Rating)
					{
						request.SortOrder = 6;
					}
				}
			}
		}

		public void ReloadIndex()
		{
			var searchProvider = new LuceneSearchProvider();
			searchProvider.ReloadIndex();
		}

        /// <summary>
        /// To Get Seo Url Details.
        /// </summary>
        /// <param name="seoUrl">string seoUrl</param>
        /// <returns>Returns Seo Url Details having SEOUrlModel format.</returns>
        public SEOUrlModel GetSeoUrlDetail(string seoUrl)
        {
            SEOUrlModel urlModel = new SEOUrlModel();
            if (!string.IsNullOrEmpty(seoUrl))
            {
                SEOUrlHelper seoUrlHelper = new SEOUrlHelper();
                DataSet seoDetail = seoUrlHelper.GetSEOUrlDetail(seoUrl);
                if (!Equals(seoDetail, null) && !Equals(seoDetail.Tables[0], null) && seoDetail.Tables[0].Rows.Count > 0)
                {
                    urlModel = seoDetail.Tables[0].Rows[0].ToSingleRow<SEOUrlModel>();
                }
            }
            return urlModel;
        }

        /// <summary>
        /// To Check whether provided Seo Url is restricted or not.
        /// </summary>
        /// <param name="seoUrl">string seoUrl</param>
        /// <returns>Return true or false.</returns>
        public bool IsRestrictedSeoUrl(string seoUrl)
        {
            bool isSeoUrlRestricted = false;
            if (!string.IsNullOrEmpty(seoUrl))
            {
                SEOUrlHelper seoUrlHelper = new SEOUrlHelper();
                isSeoUrlRestricted = seoUrlHelper.IsRestrictedSeoUrl(seoUrl);
            }
            return isSeoUrlRestricted;
        }
	}
}
