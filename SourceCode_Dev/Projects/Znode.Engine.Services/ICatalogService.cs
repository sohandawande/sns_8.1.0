﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
	public interface ICatalogService
	{
        /// <summary>
        /// Gets catalog details by catalog Id.
        /// </summary>
        /// <param name="catalogId">int catalogId</param>
        /// <returns>Returns model of type CatalogModel.</returns>
		CatalogModel GetCatalog(int catalogId);

        /// <summary>
        /// Gets the list of catalogs.
        /// </summary>
        /// <param name="filters">Filter Collection</param>
        /// <param name="sorts">Sort Collection</param>
        /// <param name="page">Page Collection</param>
        /// <returns>Returns model of type CatalogListModel.</returns>
		CatalogListModel GetCatalogs(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Gets the catalogs list by catalog Id
        /// </summary>
        /// <param name="catalogIds">string catalogId</param>
        /// <param name="expands">Expands Collection</param>
        /// <param name="sorts">Sort Collection</param>
        /// <returns>Returns modle of type CatalogListModel.</returns>
	    CatalogListModel GetCatalogsByCatalogIds(string catalogIds, NameValueCollection expands, NameValueCollection sorts);

        /// <summary>
        /// Creats a new catalog
        /// </summary>
        /// <param name="model">Model of type CatalogModel.</param>
        /// <returns>Returns model of tpye CatalogModel.</returns>
		CatalogModel CreateCatalog(CatalogModel model);

        /// <summary>
        /// Updates the catalog
        /// </summary>
        /// <param name="catalogId">int catalogId</param>
        /// <param name="model">Model of type CatalogModel.</param>
        /// <returns>Returns model of type CatalogModel.</returns>
		CatalogModel UpdateCatalog(int catalogId, CatalogModel model);

        /// <summary>
        /// Deletes the catalog.
        /// </summary>
        /// <param name="catalogId">int catalogId</param>
        /// <returns>Returns true/false.</returns>
		bool DeleteCatalog(int catalogId);

        /// <summary>
        /// Znode Version 8.0
        /// Copies an existing catalog.
        /// </summary>
        /// <param name="model">The model of type CatalogModel.</param>
        /// <returns>Returns true or false.</returns>
        bool CopyCatalog(CatalogModel model);

        /// <summary>
        /// Znode Version 8.0
        /// Deletes an existing catalog.
        /// </summary>
        /// <param name="catalogId">The Id of catalog</param>
        /// <param name="preserveCategories">boolean value for preserveCategories</param>
        /// <returns>Returns true or false.</returns>
        bool DeleteCatalogUsingCustomService(int catalogId,bool preserveCategories);

        /// <summary>
        /// Znode Version 8.0
        /// Gets catalog list according to catalog id.
        /// </summary>
        /// <param name="portalId">Patalog id.</param>
        /// <returns>CatalogListModel</returns>
        CatalogListModel GetCatalogListByPortalId(int portalId);
	}
}
