﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    /// <summary>
    /// Shipping Rule Type Service Interface
    /// </summary>
	public interface IShippingRuleTypeService
	{
        /// <summary>
        /// Get Shipping Rule Type on the basis of shipping rule type id
        /// </summary>
        /// <param name="shippingRuleTypeId">shippingRuleTypeId</param>
        /// <returns>Returns ShippingRuleTypeModel</returns>
		ShippingRuleTypeModel GetShippingRuleType(int shippingRuleTypeId);

        /// <summary>
        /// Get list of shipping rule types
        /// </summary>
        /// <param name="filters">filters</param>
        /// <param name="sorts">NameValueCollection sorts</param>
        /// <param name="page">NameValueCollection page</param>
        /// <returns>Returns ShippingRuleTypeListModel</returns>
		ShippingRuleTypeListModel GetShippingRuleTypes(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Create Shipping Rule Type
        /// </summary>
        /// <param name="model">ShippingRuleTypeModel model</param>
        /// <returns>Returns ShippingRuleTypeModel</returns>
		ShippingRuleTypeModel CreateShippingRuleType(ShippingRuleTypeModel model);

        /// <summary>
        /// Update Shipping Rule Type on the basis of shipping rule type id
        /// </summary>
        /// <param name="shippingRuleTypeId">shippingRuleTypeId</param>
        /// <param name="model">ShippingRuleTypeModel model</param>
        /// <returns>Returns ShippingRuleTypeModel</returns>
		ShippingRuleTypeModel UpdateShippingRuleType(int shippingRuleTypeId, ShippingRuleTypeModel model);

        /// <summary>
        /// Delete Shipping Rule Type on the basis of shipping rule type id
        /// </summary>
        /// <param name="shippingRuleTypeId">shippingRuleTypeId</param>
        /// <returns>Returns true/false</returns>
		bool DeleteShippingRuleType(int shippingRuleTypeId);
	}
}
