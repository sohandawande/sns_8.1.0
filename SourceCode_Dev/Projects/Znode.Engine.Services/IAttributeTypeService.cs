﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
	public interface IAttributeTypeService
	{
        /// <summary>
        /// To Get Attribute Type details based on Attribute Type Id.
        /// </summary>
        /// <param name="attributeTypeId">Id of the Attribute Type</param>
        /// <returns>Return Attribute Type Details in AttributeTypeModel format.</returns>
		AttributeTypeModel GetAttributeType(int attributeTypeId);

        /// <summary>
        /// To Get Attribute Types detail.
        /// </summary>
        /// <param name="filters"></param>
        /// <param name="sorts"></param>
        /// <returns>Return Attribute Type Details in AttributeTypeListModel format.</returns>
		AttributeTypeListModel GetAttributeTypes(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// To Create Attribute Type
        /// </summary>
        /// <param name="model">Model of the AttributeTypeModel</param>
        /// <returns>Return Attribute Type Details in AttributeTypeModel format</returns>
		AttributeTypeModel CreateAttributeType(AttributeTypeModel model);

        /// <summary>
        /// To Update Attribute Type
        /// </summary>
        /// <param name="attributeTypeId">Id of the Attribute Type</param>
        /// <param name="model"></param>
        /// <returns>Return Attribute Type Details in AttributeTypeModel format</returns>
		AttributeTypeModel UpdateAttributeType(int attributeTypeId, AttributeTypeModel model);

        /// <summary>
        /// To Delete the Attribute Type
        /// </summary>
        /// <param name="attributeTypeId"></param>
        /// <returns>Return True or false</returns>
		bool DeleteAttributeType(int attributeTypeId);

        /// <summary>
        /// To Get Attribute Types detail based on catalog Id.
        /// </summary>
        /// <param name="catalogId">Id of the Catalog</param>
        /// <returns>Return Attribute Type Details in AttributeTypeListModel format</returns>
        AttributeTypeListModel GetAttributeTypesByCatalogId(int catalogId);
	}
}
