﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    public interface IProductAttributesService
    {
        AttributeModel GetAttribute(int attributeId);
        ProductAttributeListModel GetAttributes(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
        AttributeModel CreateAttributes(AttributeModel model);
        AttributeModel UpdateAttributes(int attributeId, AttributeModel model);
        bool DeleteAttributes(int attributeId);

        /// <summary>
        /// Get all Attributes by AttributetypeId
        /// </summary>
        /// <param name="filters">List filters</param>
        /// <param name="sorts">NameValueCollection sorts</param>
        /// <param name="page">NameValueCollection page</param>
        /// <returns>Returns list of Attributes</returns>
        ProductAttributeListModel GetAttributesByAttributeTypeId(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
    }
}
