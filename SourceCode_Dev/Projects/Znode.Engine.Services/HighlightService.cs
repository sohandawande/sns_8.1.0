﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Helpers.Constants;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using HighlightRepository = ZNode.Libraries.DataAccess.Service.HighlightService;
using HighlightTypeRepository = ZNode.Libraries.DataAccess.Service.HighlightTypeService;

namespace Znode.Engine.Services
{
    public class HighlightService : BaseService, IHighlightService
    {
        #region Private Variables 
        private readonly HighlightRepository _highlightRepository;
        private readonly HighlightTypeRepository _highlightTypeRepository; 
        #endregion

        #region Constructor
        public HighlightService()
        {
            _highlightRepository = new HighlightRepository();
            _highlightTypeRepository = new HighlightTypeRepository();
        } 
        #endregion

        #region Public Methods
        public HighlightModel GetHighlight(int highlightId, NameValueCollection expands)
        {
            var highlight = _highlightRepository.GetByHighlightID(highlightId);
            if (!Equals(highlight, null))
            {
                GetExpands(expands, highlight);
            }

            return HighlightMap.ToModel(highlight);
        }

        public HighlightListModel GetHighlights(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new HighlightListModel();
            var highlights = new TList<Highlight>();
            var tempList = new TList<Highlight>();

            var query = new HighlightQuery();
            var sortBuilder = new HighlightSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            tempList = _highlightRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (pagingLength.Equals(int.MaxValue))
            {
                model.PageSize = model.TotalResults;
            }

            // Now go through the list and get any expands
            foreach (var highlight in tempList)
            {
                GetExpands(expands, highlight);
                highlights.Add(highlight);
            }

            // Map each item and add to the list
            foreach (var h in highlights)
            {
                model.Highlights.Add(HighlightMap.ToModel(h));
            }

            return model;
        }

        public HighlightModel CreateHighlight(HighlightModel model)
        {
            if (Equals(model, null))
            {
                throw new Exception("Highlight model cannot be null.");
            }

            var entity = HighlightMap.ToEntity(model);
            var highlight = _highlightRepository.Save(entity);
            return HighlightMap.ToModel(highlight);
        }

        public HighlightModel UpdateHighlight(int highlightId, HighlightModel model)
        {
            if (highlightId < 1)
            {
                throw new Exception("Highlight ID cannot be less than 1.");
            }

            if (Equals(model, null))
            {
                throw new Exception("Highlight model cannot be null.");
            }

            var highlight = _highlightRepository.GetByHighlightID(highlightId);
            if (!Equals(highlight, null))
            {
                // Set highlight ID
                model.HighlightId = highlightId;

                var highlightToUpdate = HighlightMap.ToEntity(model);

                var updated = _highlightRepository.Update(highlightToUpdate);
                if (updated)
                {
                    highlight = _highlightRepository.GetByHighlightID(highlightId);
                    return HighlightMap.ToModel(highlight);
                }
            }

            return null;
        }

        public bool DeleteHighlight(int highlightId)
        {
            if (highlightId < 1)
            {
                throw new Exception("Highlight ID cannot be less than 1.");
            }

            var highlight = _highlightRepository.GetByHighlightID(highlightId);
            if (!Equals(highlight, null))
            {
                return _highlightRepository.Delete(highlight);
            }

            return false;
        }

        public HighlightModel CheckAssociateProduct(int highlightId)
        {
            HighlightModel model = new HighlightModel();
            HighlightHelper highlightHelper = new HighlightHelper();
            DataSet ds = highlightHelper.GetAssociatedProductByHighlightId(highlightId);

            if (!Equals(ds, null) && !Equals(ds.Tables, null) && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                model.IsAssociatedProduct = true;
            }
            else
            {
                model.IsAssociatedProduct = false;
            }

            return model;
        } 
        #endregion

        #region Private Methods
        private void GetExpands(NameValueCollection expands, Highlight highlight)
        {
            if (expands.HasKeys())
            {
                ExpandHighlightType(expands, highlight);
            }
        }

        private void ExpandHighlightType(NameValueCollection expands, Highlight highlight)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.HighlightType)))
            {
                if (highlight.HighlightTypeID.HasValue)
                {
                    var highlightType = _highlightTypeRepository.GetByHighlightTypeID(highlight.HighlightTypeID.Value);
                    if (!Equals(highlightType, null))
                    {
                        highlight.HighlightTypeIDSource = highlightType;
                    }
                }
            }
        }

        private void SetFiltering(List<Tuple<string, string, string>> filters, HighlightQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;

                if (FilterKeys.HighlightId.Equals(filterKey)) SetQueryParameter(HighlightColumn.HighlightID, filterOperator, filterValue, query);
                if (FilterKeys.HighlightTypeId.Equals(filterKey)) SetQueryParameter(HighlightColumn.HighlightTypeID, filterOperator, filterValue, query);
                if (FilterKeys.IsActive.Equals(filterKey)) SetQueryParameter(HighlightColumn.ActiveInd, filterOperator, filterValue, query);
                if (FilterKeys.LocaleId.Equals(filterKey)) SetQueryParameter(HighlightColumn.LocaleId, filterOperator, filterValue, query);
                if (FilterKeys.Name.Equals(filterKey)) SetQueryParameter(HighlightColumn.Name, filterOperator, filterValue, query);
                if (FilterKeys.PortalId.Equals(filterKey)) SetQueryParameter(HighlightColumn.PortalID, filterOperator, filterValue, query);
                if (FilterKeys.DispalyOrder.Equals(filterKey)) SetQueryParameter(HighlightColumn.DisplayOrder, filterOperator, filterValue, query);
                if (FilterKeys.ShowPopup.Equals(filterKey)) SetQueryParameter(HighlightColumn.DisplayPopup, filterOperator, filterValue, query);
            }
        }

        private void SetSorting(NameValueCollection sorts, HighlightSortBuilder sortBuilder)
        {
            if (sorts.HasKeys())
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);

                    if (value.Equals(SortKeys.DisplayOrder)) { SetSortDirection(HighlightColumn.DisplayOrder, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (value.Equals(SortKeys.HighlightId)) { SetSortDirection(HighlightColumn.HighlightID, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (value.Equals(SortKeys.Name)) { SetSortDirection(HighlightColumn.Name, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (value.Equals(SortKeys.ShowPopup)) { SetSortDirection(HighlightColumn.DisplayPopup, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (value.Equals(SortKeys.HighlightTypeName)) { SetSortDirection(HighlightColumn.HighlightTypeID, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (value.Equals(SortKeys.IsActive)) { SetSortDirection(HighlightColumn.ActiveInd, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                }
            }
            else
            {
                SetSortDirection(HighlightColumn.HighlightID, sorts[StoredProcedureKeys.sortDirKey], sortBuilder);
            }
        }

        private void SetQueryParameter(HighlightColumn column, string filterOperator, string filterValue, HighlightQuery query)
        {
            base.SetQueryParameter(column, filterOperator, filterValue, query);
        }

        private void SetSortDirection(HighlightColumn column, string value, HighlightSortBuilder sortBuilder)
        {
            base.SetSortDirection(column, value, sortBuilder);
        } 
        #endregion
    }
}
