﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    public interface IGiftCardService
    {
        GiftCardModel GetGiftCard(int giftCardId, NameValueCollection expands);
        GiftCardListModel GetGiftCards(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
        GiftCardModel CreateGiftCard(GiftCardModel model);
        GiftCardModel UpdateGiftCard(int giftCardId, GiftCardModel model);
        bool DeleteGiftCard(int giftCardId);

        /// <summary>
        /// Znode Version 8.0
        /// Create GiftCard Number
        /// </summary>
        /// <returns>Returns giftcard model</returns>
        GiftCardModel GetNextGiftCardNumber();

        /// <summary>
        /// To check gift card is valid or not.
        /// </summary>
        /// <param name="giftCardNumber">Gift card number</param>
        /// <param name="accountId">logged in user account id</param>
        /// <returns>true / false</returns>
        bool IsValidGiftCard(string giftCardNumber, int accountId);
    }
}
