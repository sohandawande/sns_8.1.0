﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    public interface ICategoryService
    {
        /// <summary>
        /// Get category based on categoryId
        /// </summary>
        /// <param name="categoryId">int categoryId</param>
        /// <param name="expands">Expands Collection</param>
        /// <returns>Category details</returns>
        CategoryModel GetCategory(int categoryId, NameValueCollection expands);

        /// <summary>
        /// Gets the list of categories.
        /// </summary>
        /// <param name="expands">Expands Collection</param>
        /// <param name="filters">Filter Collection</param>
        /// <param name="sorts">Sort Collection</param>
        /// <param name="page">Page Collection</param>
        /// <returns>Returns model of type CategoryListModel.</returns>
        CategoryListModel GetCategories(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Gets the categories list by catalogId.
        /// </summary>
        /// <param name="catalogId">int catalogId</param>
        /// <param name="expands">Expands Collection</param>
        /// <param name="filters">Filter Collection</param>
        /// <param name="sorts">Sort Collection</param>
        /// <param name="page">Page Collection</param>
        /// <returns>Returns model of type CategoryListModel.</returns>
        CategoryListModel GetCategoriesByCatalog(int catalogId, NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Gets the catgories list by catalogs ids.
        /// </summary>
        /// <param name="catalogIds">string catalogIds</param>
        /// <param name="expands">Expands Collection</param>
        /// <param name="filters">Filter Collection</param>
        /// <param name="sorts">Sort Collection</param>
        /// <param name="page">Page Collection</param>
        /// <returns>Returns model of type CategoryListModel.</returns>
        CategoryListModel GetCategoriesByCatalogIds(string catalogIds, NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Gets catgeories by category ids.
        /// </summary>
        /// <param name="categoryIds">int categoryIds</param>
        /// <param name="expands">Expands Collection</param>
        /// <param name="filters">Filter Collection</param>
        /// <param name="sorts">Sort Collection</param>
        /// <param name="page">Page Collection</param>
        /// <returns>Returns model of type CategoryListModel.</returns>
        CategoryListModel GetCategoriesByCategoryIds(string categoryIds, NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Method Create the Category
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Return category model.</returns>
        CategoryModel CreateCategory(CategoryModel categoryModel);

        /// <summary>
        /// Method Update the category based on profileId.
        /// </summary>
        /// <param name="facetGroupId"></param>
        /// <param name="model"></param>
        /// <returns>Return updated category model.</returns>
        CategoryModel UpdateCategory(int categoryId, CategoryModel categoryModel);

        /// <summary>
        /// Method Delete the category based on categoryId.
        /// </summary>
        /// <param name="categoryId">The id of category</param>
        /// <returns>Return true or false.</returns>
        bool DeleteCategory(int categoryId);

        /// <summary>
        /// Znode Version 8.0
        /// Gets list of categories by catalog Id.   
        /// </summary>
        /// <param name="filters">Filer Collection</param>
        /// <param name="sorts">Sort Collection</param>
        /// <param name="page">Page Index</param>
        /// <param name="totalRowCount">Total result count</param>
        /// /// <param name="catalogId">The Id of catalog</param>
        /// <returns>Returns list of categories by catalog Id.</returns>
        CatalogAssociatedCategoriesListModel GetCategoryByCatalogId(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page, out int totalRowCount, int catalogId = 0);

        /// <summary>
        /// Znode Version 8.0
        /// Gets all categories.
        /// </summary>
        /// <param name="expands">NameValueCollection expands</param>
        /// <param name="filters">List Tuple filters</param>
        /// <param name="sorts">NameValueCollection sorts</param>
        /// <param name="page">NameValueCollection page</param>
        /// <returns>Returns list of all categories</returns>
        CatalogAssociatedCategoriesListModel GetAllCategories(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
        

        /// <summary>
        /// Add product associated with category
        /// </summary>
        /// <param name="categoryId">int category Id</param>
        /// <param name="productIds">string productIds</param>
        /// <returns> bool value</returns>
        bool CategoryAssociatedProducts(int categoryId, string productIds);

        /// <summary>
        /// To update category seo details
        /// </summary>
        /// <param name="categoryId">int categoryId</param>
        /// <param name="categoryModel">CategoryModel categoryModel</param>
        /// <returns>returns CategoryModel</returns>
        CategoryModel UpdateCategorySEODetails(int categoryId, CategoryModel model);

        /// <summary>
        /// Get the list of Category Associated Products
        /// </summary>
        /// <param name="expands">Expand Collection</param>
        /// <param name="filters">Filter Collection</param>
        /// <param name="sorts">Sort Collection</param>
        /// <param name="page">Paging</param>
        /// <returns>ProductListModel</returns>
        ProductListModel GetCategoryAssociatedProducts(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts = null, NameValueCollection page = null);

        /// <summary>
        ///  Get the List of Category Sub-Category & its Products
        /// </summary>
        /// <param name="expands">Expands Collection</param>
        /// <param name="filters">Filter Collection</param>
        /// <param name="sorts">Sort Collection</param>
        /// <param name="page">Page Collection</param>
        /// <returns>Returns model of type CategoryListModel.</returns>
        CategoryListModel GetCategoryTree(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
    }
}
