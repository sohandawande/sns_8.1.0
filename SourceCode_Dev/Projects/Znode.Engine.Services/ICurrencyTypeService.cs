﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    public interface ICurrencyTypeService
    {
        /// <summary>
        /// Gets currency type list.
        /// </summary>
        /// <param name="expands"></param>
        /// <param name="filters"></param>
        /// <param name="sorts"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        CurrencyTypeListModel GetCurrencyTypes(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Gets currency type according to currency type id.
        /// </summary>
        /// <param name="currencyTypeId"></param>
        /// <param name="expands"></param>
        /// <returns></returns>
        CurrencyTypeModel GetCurrencyType(int currencyTypeId, NameValueCollection expands);

        /// <summary>
        /// Update Currency Type.
        /// </summary>
        /// <param name="currencyTypeId">Currency type ID.</param>
        /// <param name="currencyTypeModel">Currency Type Model.</param>
        /// <returns>Currency Type Model.</returns>
        CurrencyTypeModel UpdateCurrencyType(int currencyTypeId, CurrencyTypeModel currencyTypeModel);
    }
}
