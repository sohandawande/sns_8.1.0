﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Helpers;
using Znode.Libraries.Helpers.Constants;
using Znode.Libraries.Helpers.Extensions;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using CountryRepository = ZNode.Libraries.DataAccess.Service.CountryService;
using PortalCountryRepository = ZNode.Libraries.DataAccess.Service.PortalCountryService;

namespace Znode.Engine.Services
{
	public class PortalCountryService : BaseService, IPortalCountryService
	{
		private readonly CountryRepository _countryRepository;
		private readonly PortalCountryRepository _portalCountryRepository;

		public PortalCountryService()
		{
			_countryRepository = new CountryRepository();
			_portalCountryRepository = new PortalCountryRepository();
		}

		public PortalCountryModel GetPortalCountry(int portalCountryId, NameValueCollection expands)
		{
			var portalCountry = _portalCountryRepository.GetByPortalCountryID(portalCountryId);
			if (portalCountry != null)
			{
				GetExpands(expands, portalCountry);
			}

			return PortalCountryMap.ToModel(portalCountry);
		}

		public PortalCountryListModel GetPortalCountries(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
		{
			var model = new PortalCountryListModel();
			var portalCountries = new TList<PortalCountry>();
			var tempList = new TList<PortalCountry>();

			var query = new PortalCountryQuery();
			var sortBuilder = new PortalCountrySortBuilder();
			var pagingStart = 0;
			var pagingLength = 0;

			SetFiltering(filters, query);
			SetSorting(sorts, sortBuilder);
			SetPaging(page, model, out pagingStart, out pagingLength);

			// Get the initial set
			var totalResults = 0;
			tempList = _portalCountryRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
			model.TotalResults = totalResults;

			// Check to set page size equal to the total number of results
			if (pagingLength == int.MaxValue)
			{
				model.PageSize = model.TotalResults;
			}

			// Now go through the list and get any expands
			foreach (var portalCountry in tempList)
			{
				GetExpands(expands, portalCountry);
				portalCountries.Add(portalCountry);
			}

			// Map each item and add to the list
			foreach (var pc in portalCountries)
			{
				model.PortalCountries.Add(PortalCountryMap.ToModel(pc));
			}

			return model;
		}

		public PortalCountryModel CreatePortalCountry(PortalCountryModel model)
		{
			if (model == null)
			{
				throw new Exception("Portal country model cannot be null.");
			}

			var entity = PortalCountryMap.ToEntity(model);
			var portalCountry = _portalCountryRepository.Save(entity);
			return PortalCountryMap.ToModel(portalCountry);
		}

		public PortalCountryModel UpdatePortalCountry(int portalCountryId, PortalCountryModel model)
		{
			if (portalCountryId < 1)
			{
				throw new Exception("Portal country ID cannot be less than 1.");
			}

			if (model == null)
			{
				throw new Exception("Portal country model cannot be null.");
			}

			var portalCountry = _portalCountryRepository.GetByPortalCountryID(portalCountryId);
			if (portalCountry != null)
			{
				// Set portal country ID
				model.PortalCountryId = portalCountryId;

				var portalCountryToUpdate = PortalCountryMap.ToEntity(model);

				var updated = _portalCountryRepository.Update(portalCountryToUpdate);
				if (updated)
				{
					portalCountry = _portalCountryRepository.GetByPortalCountryID(portalCountryId);
					return PortalCountryMap.ToModel(portalCountry);
				}
			}

			return null;
		}

		public bool DeletePortalCountry(int portalCountryId)
		{
			if (portalCountryId < 1)
			{
				throw new Exception("Portal country ID cannot be less than 1.");
			}

			var portalCountry = _portalCountryRepository.GetByPortalCountryID(portalCountryId);
			if (portalCountry != null)
			{
				return _portalCountryRepository.Delete(portalCountry);
			}

			return false;
		}

		private void GetExpands(NameValueCollection expands, PortalCountry portalCountry)
		{
			if (expands.HasKeys())
			{
				ExpandCountry(expands, portalCountry);
			}
		}

		private void ExpandCountry(NameValueCollection expands, PortalCountry portalCountry)
		{
			if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Country)))
			{
				var country = _countryRepository.GetByCode(portalCountry.CountryCode);
				if (country != null)
				{
					portalCountry.CountryCodeSource = country;
				}
			}
		}

		private void SetFiltering(List<Tuple<string, string, string>> filters, PortalCountryQuery query)
		{
			foreach (var tuple in filters)
			{
				var filterKey = tuple.Item1;
				var filterOperator = tuple.Item2;
				var filterValue = tuple.Item3;

				if (filterKey == FilterKeys.CountryCode) SetQueryParameter(PortalCountryColumn.CountryCode, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.IsBillingActive) SetQueryParameter(PortalCountryColumn.BillingActive, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.IsShippingActive) SetQueryParameter(PortalCountryColumn.ShippingActive, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.PortalId) SetQueryParameter(PortalCountryColumn.PortalID, filterOperator, filterValue, query);
			}
		}

		private void SetSorting(NameValueCollection sorts, PortalCountrySortBuilder sortBuilder)
		{
			if (sorts.HasKeys())
			{
				foreach (var key in sorts.AllKeys)
				{
					var value = sorts.Get(key);

					if (key == SortKeys.CountryCode) SetSortDirection(PortalCountryColumn.CountryCode, value, sortBuilder);
					if (key == SortKeys.PortalCountryId) SetSortDirection(PortalCountryColumn.PortalCountryID, value, sortBuilder);
				}
			}
		}

		private void SetQueryParameter(PortalCountryColumn column, string filterOperator, string filterValue, PortalCountryQuery query)
		{
			base.SetQueryParameter(column, filterOperator, filterValue, query);
		}

		private void SetSortDirection(PortalCountryColumn column, string value, PortalCountrySortBuilder sortBuilder)
		{
			base.SetSortDirection(column, value, sortBuilder);
		}

        #region Znode Version 8.0

        public PortalCountryListModel GetAllPortalCountries(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            PortalCountryListModel list = new PortalCountryListModel();
            int totalRowCount = 0;
            var pagingStart = 0;
            var pagingLength = 0;
            string orderBy = StringHelper.GenerateDynamicOrderByClause(sorts); ;
            pagingStart = Convert.ToInt32(page.Get(PageKeys.Index));
            pagingLength = Convert.ToInt32(page.Get(PageKeys.Size));
            string whereClause = StringHelper.GenerateDynamicWhereClause(filters);
            StoredProcedureHelper storedProcedureHelper = new StoredProcedureHelper();

            DataSet resultDataSet = storedProcedureHelper.SP_DataDetailsSpConfig(whereClause, null, StoredProcedureKeys.SpGetAllPortalCountries, orderBy, pagingStart.ToString(), pagingLength.ToString(), out totalRowCount);
            if (!Equals(resultDataSet, null) && resultDataSet.Tables.Count > 0 && !Equals(resultDataSet.Tables[0], null) && resultDataSet.Tables[0].Rows.Count > 0)
            {
                list.PortalCountries = resultDataSet.Tables[0].ToList<PortalCountryModel>().ToCollection();
            }
            list.TotalResults = totalRowCount;
            list.PageSize = pagingLength;
            list.PageIndex = pagingStart;
            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                list.PageSize = list.TotalResults;
            }
            return list;
        }
    
        public bool DeletePortalCountryIfNotAssociated(int portalCountryId)
        {
            if (portalCountryId < 1)
            {
                throw new Exception("Portal country ID cannot be less than 1.");
            }

            var portalCountry = _portalCountryRepository.GetByPortalCountryID(portalCountryId);

            if(Equals(portalCountry,null))
            {
                throw new Exception("Portal country cannot be null.");
            }

            PortalCountryAdmin portalCountryAdmin = new PortalCountryAdmin();

            if (portalCountryAdmin.IsDeletable(portalCountry.CountryCode))
            {
                return _portalCountryRepository.Delete(portalCountry);
            }
            else
            {
                throw new Exception("The country can not be deleted. Delete the associated account with the country, if any.");
            }            
        }

        public bool CreatePortalCountries(int portalId,string billableCountryCodes,string shipableCountryCodes)
        {
            PortalCountryHelper portalCountryHelper = new PortalCountryHelper();

            billableCountryCodes = billableCountryCodes.Equals("Empty") ? string.Empty : billableCountryCodes;
            shipableCountryCodes = shipableCountryCodes.Equals("Empty") ? string.Empty : shipableCountryCodes;


            bool isPortalCountryCreated=portalCountryHelper.CreatePortalCountry(portalId, billableCountryCodes, shipableCountryCodes);
            if(!isPortalCountryCreated)
            {
                throw new Exception("There was an error while creating portal countries.");
            }
            else
            {
                return isPortalCountryCreated;
            }
        }
        #endregion
	}
}
