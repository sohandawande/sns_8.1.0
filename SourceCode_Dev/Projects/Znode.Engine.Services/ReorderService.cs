﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using AccountRepository = ZNode.Libraries.DataAccess.Service.AccountService;
using CategoryRepository = ZNode.Libraries.DataAccess.Service.CategoryService;
using DigitalAssetRepository = ZNode.Libraries.DataAccess.Service.DigitalAssetService;
using OrderLineItemRepository = ZNode.Libraries.DataAccess.Service.OrderLineItemService;
using OrderRepository = ZNode.Libraries.DataAccess.Service.OrderService;
using OrderShipmentRepository = ZNode.Libraries.DataAccess.Service.OrderShipmentService;
using PaymentSettingRepository = ZNode.Libraries.DataAccess.Service.PaymentSettingService;
using PaymentTypeRepository = ZNode.Libraries.DataAccess.Service.PaymentTypeService;
using PortalRepository = ZNode.Libraries.DataAccess.Service.PortalService;
using PortalProfileRepository = ZNode.Libraries.DataAccess.Service.PortalProfileService;
using ProfileRepository = ZNode.Libraries.DataAccess.Service.ProfileService;
using ShippingRepository = ZNode.Libraries.DataAccess.Service.ShippingService;
using ZNodeShoppingCartItem = ZNode.Libraries.ECommerce.ShoppingCart.ZNodeShoppingCartItem;
using AddOnRepository = ZNode.Libraries.DataAccess.Service.ProductAddOnService;
using AddOnValueRepository = ZNode.Libraries.DataAccess.Service.AddOnValueService;
using ParentChildProductRepository = ZNode.Libraries.DataAccess.Service.ParentChildProductService;
using ProductAttributeRepository = ZNode.Libraries.DataAccess.Service.ProductAttributeService;
using SkuRepository = ZNode.Libraries.DataAccess.Service.SKUService;
namespace Znode.Engine.Services
{
    public class ReorderService : BaseService, IReorderService
    {
        private readonly OrderLineItemRepository _orderLineItemRepository;
        private readonly OrderRepository _orderRepository;
        private readonly AddOnRepository _addOnRepository;
        private readonly AddOnValueRepository _addOnValueRepository;
        private readonly ParentChildProductRepository _parentChildProductRepository;
        private readonly ProductAttributeRepository _productAttributeRepository;
        private readonly SkuRepository _skuRepository;
        public ReorderService()
        {
            _orderLineItemRepository = new OrderLineItemRepository();
            _orderRepository = new OrderRepository();
            _addOnRepository = new AddOnRepository();
            _addOnValueRepository = new AddOnValueRepository();
            _parentChildProductRepository = new ParentChildProductRepository();
            _productAttributeRepository = new ProductAttributeRepository();
            _skuRepository = new SkuRepository();
        }


        public CartItemsListModel GetReorderItems(int orderId, NameValueCollection expands)
        {
            //Get order details by orderId
            var orderLineItems = _orderLineItemRepository.GetByOrderID(orderId);

            SkuService objSkuService = new SkuService();
            CartItemsListModel model = new CartItemsListModel();

            orderLineItems.ToList().ForEach(_Items =>
            {
                CartItemsModel CartItemViewModel = new CartItemsModel();
                CartItemViewModel.Sku = _Items.SKU;
                CartItemViewModel.Quantity = (int)_Items.Quantity;

                var skuList = objSkuService.GetSkuList(CartItemViewModel.Sku, expands);
                if (skuList != null)
                {
                    skuList.ForEach(sku =>
                    {
                        CartItemViewModel.ProductId = sku.ProductID;
                    });
                }

                if (CartItemViewModel.ProductId != 0 && Equals(_Items.OrderLineItemRelationshipTypeID, null))
                {
                    var childOrdeLineItems = _orderLineItemRepository.GetByParentOrderLineItemID(_Items.OrderLineItemID);
                    string addOnIds = string.Empty;
                    childOrdeLineItems.ForEach(childItem =>
                    {
                        if (childItem.OrderLineItemRelationshipTypeID.Equals(2))
                        {
                            if (addOnIds.Equals(string.Empty))
                            {
                                addOnIds = GetProductAddons(childItem.SKU);
                            }
                            else
                            {
                                addOnIds += "," + GetProductAddons(childItem.SKU);
                            }
                        }
                        //CartItemViewModel.AddOnValuesCustomText = Equals(CartItemViewModel.AddOnValuesCustomText,null)?childItem.Custom1:CartItemViewModel.AddOnValuesCustomText + "," + childItem.Custom1;                        
                    });

                    CartItemViewModel.AddOnValueIds = addOnIds;
                    CartItemViewModel.SelectedBundles = (GetBundleItems(CartItemViewModel.ProductId, orderId));
                    model.ReorderItems.Add(CartItemViewModel);

                }
            });

            if (model != null)
            {
                return model;
            }

            return null;
        }

        public CartItemsModel GetReorderLineItem(int orderLineItemId, NameValueCollection expands)
        {
            //Get orderLineItems by orderLineItemId.
            var orderLineItems = _orderLineItemRepository.GetByOrderLineItemID(orderLineItemId);

            SkuService objSkuService = new SkuService();

            CartItemsModel CartItemViewModel = new CartItemsModel();
            CartItemViewModel.Sku = orderLineItems.SKU;
            CartItemViewModel.Quantity = (int)orderLineItems.Quantity;
            CartItemViewModel.Description = orderLineItems.Description;

            //Get SKU 
            var SKUList = objSkuService.GetSkuList(CartItemViewModel.Sku, expands);

            //Get product Id
            if (SKUList != null)
            {
                SKUList.ForEach(sku =>
                {
                    CartItemViewModel.ProductId = sku.ProductID;
                });
            }

            //Get product AddOns
            if (CartItemViewModel.ProductId != 0 && Equals(orderLineItems.OrderLineItemRelationshipTypeID, null))
            {
                var childOrdeLineItems = _orderLineItemRepository.GetByParentOrderLineItemID(orderLineItems.OrderLineItemID);
                string addOnIds = string.Empty;
                childOrdeLineItems.ForEach(childItem =>
                {
                    if (childItem.OrderLineItemRelationshipTypeID.Equals(2))
                    {
                        if (addOnIds.Equals(string.Empty))
                        {
                            addOnIds = GetProductAddons(childItem.SKU);
                        }
                        else
                        {
                            addOnIds += "," + GetProductAddons(childItem.SKU);
                        }
                    }
                    //CartItemViewModel.AddOnValuesCustomText = Equals(CartItemViewModel.AddOnValuesCustomText,null)?childItem.Custom1:CartItemViewModel.AddOnValuesCustomText + "," + childItem.Custom1;                        
                });

                CartItemViewModel.AddOnValueIds = addOnIds;
                CartItemViewModel.SelectedBundles = (GetBundleItems(CartItemViewModel.ProductId, orderLineItems.OrderID));
            }


            if (CartItemViewModel != null)
            {
                return CartItemViewModel;
            }

            return null;
        }

        public string GetProductAddons(string addonSku)
        {
            //Get product AddOns
            var addons = _addOnValueRepository.GetBySKU(addonSku);
            string strAddonIds = string.Empty;

            //Get Product Addons Values
            addons.ForEach(addon =>
            {
                if (strAddonIds.Equals(string.Empty))
                    strAddonIds = addon.AddOnValueID.ToString();
                else
                    strAddonIds += "," + addon.AddOnValueID.ToString();
            });
            return strAddonIds;
        }

        /// <summary>
        /// get bundle product with addons
        /// </summary>
        /// <param name="productId">productid</param>
        /// <param name="orderId">orderid</param>
        /// <returns>returns collection of BundleViewModel</returns>
        private Collection<BundleViewModel> GetBundleItems(int productId, int orderId)
        {
            var productBundleList = _parentChildProductRepository.GetByParentProductID(productId);
            Collection<BundleViewModel> bundleItems = new Collection<BundleViewModel>();

            productBundleList.ForEach(item =>
            {
                BundleViewModel _bundle = new BundleViewModel();
                _bundle.ProductId = int.Parse(item.ChildProductID.ToString());
                string sku = _skuRepository.GetByProductID(int.Parse(item.ChildProductID.ToString())).FirstOrDefault().SKU;

                int parentOrderLineId = _orderLineItemRepository.GetByOrderID(orderId).FirstOrDefault(x => x.SKU.Equals(sku)).OrderLineItemID;

                var parentOrder = _orderLineItemRepository.GetByParentOrderLineItemID(parentOrderLineId);
                if (parentOrder.Count > 0)
                {
                    string addOnValueIds = GetProductAddons(parentOrder.FirstOrDefault(y => y.ParentOrderLineItemID.Equals(parentOrderLineId)).SKU);
                    _bundle.AddOnValueIds = (!string.IsNullOrEmpty(addOnValueIds) ? addOnValueIds.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToArray() : null);
                    bundleItems.Add(_bundle);
                }
            });

            return bundleItems;
        }



        //TODO : Function implementation not yet done . using this function we can manage reorder both way ie single item and complete order.
        public CartItemsListModel GetReorderItemsList(int orderId, int orderLineItemId, bool isOrder, NameValueCollection expands)
        {
            TList<OrderLineItem> itemModel = new TList<OrderLineItem>();

            itemModel.Add(_orderLineItemRepository.GetByOrderLineItemID(830));

            SkuService objSkuService = new SkuService();
            CartItemsListModel model = new CartItemsListModel();
            foreach (var m in itemModel)
            {
                CartItemsModel CartItemViewModel = new CartItemsModel();
                CartItemViewModel.Sku = m.SKU;
                CartItemViewModel.Quantity = (int)m.Quantity;

                foreach (var sku in objSkuService.GetSkuList(CartItemViewModel.Sku, expands))
                {
                    CartItemViewModel.ProductId = sku.ProductID;
                }

                model.ReorderItems.Add(CartItemViewModel);
            }

            if (model != null)
            {
                return model;
            }

            return null;
        }


    }
}
