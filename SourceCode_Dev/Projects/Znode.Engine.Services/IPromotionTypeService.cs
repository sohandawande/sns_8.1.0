﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
	public interface IPromotionTypeService
	{
		PromotionTypeModel GetPromotionType(int promotionTypeId);
		PromotionTypeListModel GetPromotionTypes(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
		PromotionTypeModel CreatePromotionType(PromotionTypeModel model);
		PromotionTypeModel UpdatePromotionType(int promotionTypeId, PromotionTypeModel model);
		bool DeletePromotionType(int promotionTypeId);
        
        /// <summary>
        /// ZNode Version 8.0
        /// Get all Promotion Types not present in database.
        /// </summary>
        /// <returns>Return Promotion Type List Model</returns>
        PromotionTypeListModel GetAllPromotionTypesNotInDatabase();
        
        /// <summary>
        /// ZNode Version 8.0
        /// Get Franchise Admin Access Promotion Types.
        /// </summary>
        /// <param name="filters">Filter collection</param>
        /// <param name="sorts">Sort Collection</param>
        /// <param name="page">Paging</param>
        /// <returns>PromotionTypeListModel</returns>
        PromotionTypeListModel GetFranchisePromotionTypes(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
	}
}
