﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.Promotions;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using Znode.Engine.Taxes;
using Znode.Libraries.Helpers;
using Znode.Libraries.Helpers.Constants;
using Znode.Libraries.Helpers.Extensions;
using Znode.Libraries.Helpers.Utility;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;
using ProductReviewHistoryRepository = ZNode.Libraries.DataAccess.Service.ProductReviewHistoryService;

namespace Znode.Engine.Services
{
    public class ProductReviewHistoryService : BaseService, IProductReviewHistoryService
    {
        #region Private Variables
        private readonly ProductReviewHistoryRepository _productReviewHistoryRepository;
        #endregion

        #region Constructor
        public ProductReviewHistoryService()
        {
            _productReviewHistoryRepository = new ProductReviewHistoryRepository();
        }
        #endregion

        #region Public Methods
        public ProductReviewHistoryListModel GetProductReviewHistory(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new ProductReviewHistoryListModel();
            var reviewHistory = new TList<ProductReviewHistory>();
            var tempList = new TList<ProductReviewHistory>();

            var query = new ProductReviewHistoryQuery();
            var sortBuilder = new ProductReviewHistorySortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            tempList = _productReviewHistoryRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                model.PageSize = model.TotalResults;
            }

            // Now go through the list and get any expands
            foreach (var item in tempList)
            {
                GetExpands(expands, item);
                reviewHistory.Add(item);
            }

            // Map each item and add to the list
            foreach (var a in reviewHistory)
            {
                model.ProductReviewHistory.Add(ProductReviewHistoryMap.ToModel(a));
            }

            return model;
        }

        public ProductReviewHistoryModel GetProductReviewHistoryById(int productReviewHistoryID)
        {
            ProductReviewHistoryModel model = new ProductReviewHistoryModel();
            if (productReviewHistoryID > 0)
            {
                ProductReviewHistoryAdmin productReviewHistoryAdmin = new ProductReviewHistoryAdmin();
                ProductReviewHistory productReviewHistory = productReviewHistoryAdmin.GetByReviewHistoryID(productReviewHistoryID);
                string productName = string.Empty;
                string vendorName = string.Empty;
                if (!Equals(productReviewHistory, null))
                {
                    ProductAdmin productAdmin = new ProductAdmin();
                    if (productReviewHistory.ProductID > 0)
                    {
                        Product product = productAdmin.GetByProductId(productReviewHistory.ProductID);
                        int? vendorId = 0;                       
                        if (!Equals(product,null))
                        {
                            productName = product.Name;
                            vendorId = product.AccountID;
                            if (!Equals(vendorId,null))
                            {
                                AccountAdmin accountAdmin = new AccountAdmin();
                                Address address = accountAdmin.GetDefaultBillingAddress(Convert.ToInt32(vendorId));
                                if (address != null)
                                {
                                    vendorName = address.FirstName + " " + address.LastName;
                                }
                            }
                        }
                    }
                }
                model = ProductReviewHistoryMap.ToModel(productReviewHistory);
                model.ProductName = productName;
                model.VendorName = vendorName;
            }
            return model;
        }
        #endregion

        #region Private Methods
        private void SetFiltering(List<Tuple<string, string, string>> filters, ProductReviewHistoryQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;

                if (filterKey == FilterKeys.ProductId) SetQueryParameter(ProductReviewHistoryColumn.ProductID, filterOperator, filterValue, query);
            }
        }

        private void SetSorting(NameValueCollection sorts, ProductReviewHistorySortBuilder sortBuilder)
        {
            if (sorts.HasKeys())
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);
                    if (value.Equals(SortKeys.Status)) { SetSortDirection(ProductReviewHistoryColumn.Status, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (value.Equals(SortKeys.LogDate)) { SetSortDirection(ProductReviewHistoryColumn.LogDate, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (value.Equals(SortKeys.Username)) { SetSortDirection(ProductReviewHistoryColumn.Username, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (value.Equals(SortKeys.Reason)) { SetSortDirection(ProductReviewHistoryColumn.Reason, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (value.Equals(SortKeys.ProductReviewHistoryID)) { SetSortDirection(ProductReviewHistoryColumn.ProductReviewHistoryID, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }  
                }
            }
            else
            {
                SetSortDirection(ProductReviewHistoryColumn.ProductReviewHistoryID, sorts[StoredProcedureKeys.sortDirKey], sortBuilder);
            }
        }
        private void GetExpands(NameValueCollection expands, ProductReviewHistory productReviewHistory)
        {
            if (expands.HasKeys())
            {

            }
        }
        #endregion
    }
}
