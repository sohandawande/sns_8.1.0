﻿using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    public interface ISearchService
    {
        KeywordSearchModel GetKeywordSearch(KeywordSearchModel model, NameValueCollection expands, NameValueCollection sorts, NameValueCollection page);
        SuggestedSearchListModel GetSearchSuggestions(SuggestedSearchModel model);
        void ReloadIndex();

        /// <summary>
        /// To Get Seo Url Details.
        /// </summary>
        /// <param name="seoUrl">string seoUrl</param>
        /// <returns>Returns Seo Url Details having SEOUrlModel format.</returns>
        SEOUrlModel GetSeoUrlDetail(string seoUrl);


        /// <summary>
        /// To Check whether provided Seo Url is restricted or not.
        /// </summary>
        /// <param name="seoUrl">string seoUrl</param>
        /// <returns>Return true or false.</returns>
        bool IsRestrictedSeoUrl(string seoUrl);
    }
}