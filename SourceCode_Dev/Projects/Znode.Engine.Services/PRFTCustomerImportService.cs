﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using WebMatrix.WebData;
using Znode.Engine.Exceptions;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Services
{
    public class PRFTCustomerImportService : BaseService, IPRFTCustomerImportService
    {
        #region private region
        private string apiUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority); //PRFT Custom Code
        #endregion

        public PRFTCustomerImportService()
        {

        }
         
        public void CreateAccount()
        {
            PRFTImportCustomerHelper customerHelper = new PRFTImportCustomerHelper();
            DataSet ds = new DataSet();
            ds = customerHelper.GetUser();
            if (ds != null && ds.Tables.Count > 0)
            {
                DataTable dt = new DataTable();
                if (ds.Tables[0] != null)
                {
                    dt = ds.Tables[0];
                    for (int i = 0; dt.Rows.Count > i; i++)
                    {
                        if (dt.Rows[i]["AccountId"] != null)
                        {
                            string accountId = dt.Rows[i]["AccountId"].ToString();
                            string emailId = dt.Rows[i]["Email"].ToString();
                            
                            if (!WebSecurity.UserExists(accountId))
                            {
                                Guid UserId;
                                string password = string.Empty;
                                string confirmationToken = CreateNewUser(accountId, emailId, out UserId, out password);

                                //Set the confirmation for the user.
                                if (!Equals(confirmationToken, null))
                                {
                                    WebSecurity.ConfirmAccount(confirmationToken);
                                }
                                else
                                {
                                    throw new ZnodeException(ErrorCodes.MembershipError, "Could not create user.");
                                }

                                if (!customerHelper.UpdateUserID(UserId.ToString(), Convert.ToInt32(accountId)))
                                {
                                    throw new ZnodeException(ErrorCodes.MembershipError, "UserId Not Update.");
                                }

                                SendMailUserAccount(accountId, password, emailId);
                            }

                        }
                    }
                }
            }

        }

        private static string CreateNewUser(string accountId, string emailId, out Guid UserId, out string newPassword)
        {
            UserId = Guid.NewGuid();
            string actualUsername = accountId;
            string LoweredUserName = accountId.ToLower();            
            string password = ConfigurationManager.AppSettings["CustomerPassword"];
            string Email = emailId;
            string MobileAlias = string.Empty;
            bool IsAnonymous = false;
            DateTime LastActivityDate = DateTime.Now;
            newPassword = password;

            //Create the new user.
            return WebSecurity.CreateUserAndAccount(actualUsername, password, new
            {
                UserId,
                LoweredUserName,
                Email,
                MobileAlias,
                IsAnonymous,
                LastActivityDate,
            }, true);
        }


        /// <summary>
        /// Znode Version 7.2.2
        ///  This function will send acknowledge mail to new registerd user.
        ///  To inform users login name and password.
        /// </summary>
        /// <param name="loginName">string loginName</param>
        /// <param name="password">string password</param>
        /// <param name="email">string email</param>
        protected void SendMailUserAccount(string loginName, string password, string email)
        {
            string smtpServer = ZNodeConfigManager.SiteConfig.SMTPServer;
            string senderEmail = ZNodeConfigManager.SiteConfig.CustomerServiceEmail;
            string subject = "Welcome to " + ZNodeConfigManager.SiteConfig.StoreName;
            string customerservicephone = ZNodeConfigManager.SiteConfig.CustomerServicePhoneNumber;
            string currentCulture = string.Empty;
            string templatePath = string.Empty;
            string defaultTemplatePath = string.Empty;

            // Template selection
            currentCulture = ZNodeCatalogManager.CultureInfo;

            defaultTemplatePath = HttpContext.Current.Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, "UserRegistrationEmailTemplate.html"));

            templatePath = (!string.IsNullOrEmpty(currentCulture))
                ? HttpContext.Current.Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "UserRegistrationEmailTemplate_" + currentCulture + ".html")
                : HttpContext.Current.Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, "UserRegistrationEmailTemplate.html"));

            templatePath = defaultTemplatePath;

            StreamReader rw = new StreamReader(templatePath);
            string messageText = rw.ReadToEnd();

            Regex rx1 = new Regex("#UserName#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(messageText, loginName);

            Regex rx2 = new Regex("#Password#", RegexOptions.IgnoreCase);
            messageText = rx2.Replace(messageText, password);

            Regex rx3 = new Regex("#UserID#", RegexOptions.IgnoreCase);
            messageText = rx3.Replace(messageText, loginName);

            Regex rx4 = new Regex("#CUSTOMERSERVICEPHONE#", RegexOptions.IgnoreCase);
            messageText = rx4.Replace(messageText, customerservicephone);

            Regex rx5 = new Regex("#CUSTOMERSERVICEMAIL#", RegexOptions.IgnoreCase);
            messageText = rx5.Replace(messageText, senderEmail.Replace(",", ", "));

            Regex RegularExpressionLogo = new Regex("#LOGO#", RegexOptions.IgnoreCase);
            messageText = RegularExpressionLogo.Replace(messageText, ZNodeConfigManager.SiteConfig.StoreName);

            //PRFT Custom Code : Start
            string storeLogoPath = ZNodeConfigManager.SiteConfig.LogoPath.Trim().Replace("~/", "");

            Regex rx6 = new Regex("#StoreLogo#", RegexOptions.IgnoreCase);
            messageText = rx6.Replace(messageText, apiUrl + "/" + storeLogoPath.TrimStart('~'));

            var rx7 = new Regex("#MailingInfo#", RegexOptions.IgnoreCase);
            messageText = rx7.Replace(messageText, ZNodeConfigManager.SiteConfig.CustomerServiceEmail);

            var rx8 = new Regex("#SupportPhone#", RegexOptions.IgnoreCase);
            messageText = rx8.Replace(messageText, ZNodeConfigManager.SiteConfig.CustomerServicePhoneNumber);

            var rx9 = new Regex("#SupportEmail#", RegexOptions.IgnoreCase);
            messageText = rx9.Replace(messageText, ZNodeConfigManager.SiteConfig.SalesEmail);

            string logoLink = ConfigurationSettings.AppSettings["DemoWebsiteUrl"];
            var rx10 = new Regex("#LogoLink#", RegexOptions.IgnoreCase);
            messageText = rx10.Replace(messageText, logoLink);
            //PRFT Custom Code : End

            try
            {
                ZNode.Libraries.Framework.Business.ZNodeEmail.SendEmail(email, senderEmail, string.Empty, subject, messageText, true);
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.LoginCreateSuccess, loginName, "", null, ex.Message, null);
            }
        }
    }
}
