﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    public interface IProductService
    {
        /// <summary>
        /// Get Product
        /// </summary>
        /// <param name="productId">product id</param>
        /// <param name="expands">expands</param>
        /// <returns>ProductModel</returns>
        ProductModel GetProduct(int productId, NameValueCollection expands);

        /// <summary>
        /// Get product with sku
        /// </summary>
        /// <param name="productId">product id</param>
        /// <param name="skuId">sku id</param>
        /// <param name="expands">expands</param>
        /// <returns>ProductModel</returns>
        ProductModel GetProductWithSku(int productId, int skuId, NameValueCollection expands);

        /// <summary>
        /// Get Products list
        /// </summary>
        /// <param name="expands">expands</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <param name="page">page</param>
        /// <returns>ProductListModel</returns>
        ProductListModel GetProducts(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Get Products By Catalog
        /// </summary>
        /// <param name="catalogId">catalogId</param>
        /// <param name="expands">expands</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <param name="page">page</param>
        /// <returns>ProductListModel</returns>
        ProductListModel GetProductsByCatalog(int catalogId, NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Get Products By Catalog Ids
        /// </summary>
        /// <param name="catalogIds">catalog Ids</param>
        /// <param name="expands">expands</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <param name="page">page</param>
        /// <returns>ProductListModel</returns>
        ProductListModel GetProductsByCatalogIds(string catalogIds, NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Get Products By Category
        /// </summary>
        /// <param name="categoryId">category id</param>
        /// <param name="expands">expands</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <param name="page">page</param>
        /// <returns>ProductListModel</returns>
        ProductListModel GetProductsByCategory(int categoryId, NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Get Products By Category By Promotion Type
        /// </summary>
        /// <param name="categoryId">category id</param>
        /// <param name="promotionTypeId"> promotionTypeId</param>
        /// <param name="expands">expands</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <param name="page">page</param>
        /// <returns>ProductListModel</returns>
        ProductListModel GetProductsByCategoryByPromotionType(int categoryId, int promotionTypeId, NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Get Products By External  Ids
        /// </summary>
        /// <param name="externalIds">externalIds</param>
        /// <param name="expands">expands</param>
        /// <param name="sorts">sorts</param>
        /// <returns>ProductListModel</returns>
        ProductListModel GetProductsByExternalIds(string externalIds, NameValueCollection expands, NameValueCollection sorts);

        /// <summary>
        /// Get Products By Home Specials
        /// </summary>
        /// <param name="portalCatalogId">portalCatalogId</param>
        /// <param name="expands">expands</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <param name="page">page</param>
        /// <returns>ProductListModel</returns>
        ProductListModel GetProductsByHomeSpecials(int portalCatalogId, NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Get Products By ProductIds
        /// </summary>
        /// <param name="productIds">product Ids</param>
        /// <param name="expands">expands</param>
        /// <param name="sorts">sorts</param>
        /// <returns>ProductListModel</returns>
        ProductListModel GetProductsByProductIds(string productIds, NameValueCollection expands, NameValueCollection sorts);

        /// <summary>
        /// Get Products By Promotion Type
        /// </summary>
        /// <param name="promotionTypeId">promotion type id</param>
        /// <param name="expands">expands</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <param name="page">page</param>
        /// <returns>ProductListModel</returns>
        ProductListModel GetProductsByPromotionType(int promotionTypeId, NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Create Product
        /// </summary>
        /// <param name="model">model</param>
        /// <returns>ProductModel</returns>
        ProductModel CreateProduct(ProductModel model);

        /// <summary>
        /// Update Product
        /// </summary>
        /// <param name="productId">product id</param>
        /// <param name="model">model</param>
        /// <returns>ProductModel</returns>
        ProductModel UpdateProduct(int productId, ProductModel model);

        /// <summary>
        /// Delete Product
        /// </summary>
        /// <param name="productId">product id</param>
        /// <returns></returns>
        bool DeleteProduct(int productId);

        #region Product Settings
        /// <summary>
        ///  Znode Version 8.0
        ///  To update product setting
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <param name="model">ProductModel model</param>
        /// <returns>returns ProductModel</returns>
        ProductModel UpdateProductSettings(int productId, ProductModel model);

        /// <summary>
        /// To check seo url is exist
        /// </summary>
        /// <param name="seoUrl">string seoUrl</param>
        /// <param name="productId">int productId</param>
        /// <returns>returns true/false</returns>
        bool IsSeoUrlExist(int productId, string seoUrl);
        /// <summary>
        /// Znode Version 8.0
        /// To get product information by productId
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <returns>returns product details </returns>
        ProductModel GetProductDetailsByProductId(int productId);
        #endregion

        #region Product Category
        /// <summary>
        /// To get ProductCategory By ProductId
        /// </summary>  
        /// <param name="expands">NameValueCollection expands</param>
        /// <param name="filters">List<Tuple<string, string, string>> filters</param>
        /// <param name="sorts">NameValueCollection sorts</param>
        /// <param name="page">NameValueCollection page</param>
        /// <returns>returns CategoryListModel</returns>
        CategoryListModel GetProductCategoryByProductId(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        ///  To get unassociated Category By ProductId
        /// </summary>
        /// <param name="expands">NameValueCollection expands</param>
        /// <param name="filters">List<Tuple<string, string, string>> filters</param>
        /// <param name="sorts">NameValueCollection sorts</param>
        /// <param name="page">NameValueCollection page</param>
        /// <returns>returns CategoryListModel</returns>
        CategoryListModel GetProductUnAssociatedCategoryByProductId(int productId, NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// To Associate Product Category
        /// </summary>
        /// <param name="model">CategoryModel</param>
        /// <returns>returns true/false</returns>
        bool AssociateProductCategory(CategoryModel model);

        /// <summary>
        ///  To remove Associated Product Category
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <param name="categoryIds">string categoryIds</param>
        /// <returns>returns true/false</returns>
        bool UnAssociateProductCategory(CategoryModel model);

        #endregion

        #region Product Sku

        /// <summary>
        ///  To get product sku details
        /// </summary>
        /// <param name="expands">expands</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <param name="page">page</param>
        /// <returns>returns sku list</returns>
        ProductSkuListModel GetProductSkuDetails(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// To associate product/sku with multiple facets 
        /// </summary>
        /// <param name="skuId">int? skuId </param>
        /// <param name="associateFacetIds">string associateFacetIds</param>
        /// <param name="unassociateFacetIds">string unassociateFacetIds</param>
        /// <returns></returns>
        bool AssociateSkuFacets(int? skuId, string associateFacetIds, string unassociateFacetIds);

        /// <summary>
        /// To Delete Sku Associated Facets 
        /// </summary>
        /// <param name="skuId">int skuId</param>
        /// <param name="facetGroupId">int facetGroupId</param>
        /// <returns>returns true/false</returns>
        bool DeleteSkuAssociatedFacets(int skuId, int facetGroupId);

        /// <summary>
        /// To Get Sku Associated Facets
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <param name="skuId"> int skuId</param>
        /// <param name="facetGroupId">int facetGroupId</param>
        /// <param name="expands">NameValueCollection expands</param>
        /// <returns>returns associated facets</returns>
        ProductFacetModel GetSkuAssociatedFacets(int productId, int skuId, int facetGroupId, NameValueCollection expands);

        /// <summary>
        /// Get Sku Facets
        /// </summary>
        /// <param name="expands">expands</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <param name="page">page</param>
        /// <returns>ProductFacetListModel</returns>
        ProductFacetListModel GetSkuFacets(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
        #endregion

        #region Product Tags
        /// <summary>
        /// Znode Version 8.0
        /// Gets the Product tags of the product.
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <param name="expands">NameValueCollection expands</param>
        /// <returns></returns>
        ProductTagsModel GetProductTags(int productId, NameValueCollection expands);

        /// <summary>
        /// Znode Version 8.0
        /// Insert the Product Tags for the product
        /// </summary>
        /// <param name="model">ProductTagsModel model</param>
        /// <returns></returns>
        ProductTagsModel CreateProductTag(ProductTagsModel model);

        /// <summary>
        /// Znode Version 8.0
        /// Updates the product tags for the product.
        /// </summary>
        /// <param name="tagId">int tagId</param>
        /// <param name="model">ProductTagsModel model</param>
        /// <returns></returns>
        ProductTagsModel UpdateProductTag(int tagId, ProductTagsModel model);

        /// <summary>
        /// Znode Version 8.0
        /// Deletes the Product Tags based on tag Id.
        /// </summary>
        /// <param name="tagId">int tagId</param>
        /// <returns></returns>
        bool DeleteProductTag(int tagId);

        #endregion

        #region Product Facets

        /// <summary>
        /// Znode Version 8.0
        /// Get Product associated facets.
        /// </summary>
        /// <param name="expands">NameValueCollection expands</param>
        /// <param name="filters">List<Tuple<string, string, string>> filters</param>
        /// <param name="sorts">NameValueCollection sorts</param>
        /// <param name="page">NameValueCollection page</param>
        /// <returns>Return product associated facets.</returns>
        ProductFacetListModel GetProductFacetsList(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Znode Version 8.0
        /// Gets the Product Facets of the product, facetGroupId.
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <param name="facetGroupId">int facetGroupId</param>
        /// <param name="expands">NameValueCollection expands</param>
        /// <returns>ProductFacetModel</returns>
        ProductFacetModel GetProductFacets(int productId, int facetGroupId, NameValueCollection expands);

        /// <summary>
        /// Znode Version 8.0
        /// Binds the Associated facets to the Product.
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <param name="model">ProductFacetModel model)</param>
        /// <returns>ProductFacetModel</returns>
        ProductFacetModel BindProductFacets(int productId, ProductFacetModel model);

        /// <summary>
        /// Znode Version 8.0
        /// Delete the Product associated facets.
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <param name="facetGroupId">int facetGroupId</param>
        /// <returns>return true or false</returns>
        bool DeleteProductAssociatedFacets(int productId, int facetGroupId);

        #endregion

        #region Product Image

        /// <summary>
        /// Znode Version 8.0
        /// Get Product image types.
        /// </summary>
        /// <returns>Return product image types.</returns>
        ProductImageTypeListModel GetProductImageTypes();

        /// <summary>
        /// Znode Version 8.0
        /// Insert Product alternate image.
        /// </summary>
        /// <param name="model">ProductAlternateImageModel model</param>
        /// <returns></returns>
        ProductAlternateImageModel InsertProductAlternateImage(ProductAlternateImageModel model);

        /// <summary>
        /// Znode Version 8.0
        /// Update the Product alternate image.
        /// </summary>
        /// <param name="productImageId">int productImageId</param>
        /// <param name="model">ProductAlternateImageModel model</param>
        /// <returns></returns>
        ProductAlternateImageModel UpdateProductAlternateImage(int productImageId, ProductAlternateImageModel model);

        /// <summary>
        /// Znode Version 8.0
        /// Delete the Product alternate image.
        /// </summary>
        /// <param name="productImageId">int productImageId</param>
        /// <returns></returns>
        bool DeleteProductAlternateImage(int productImageId);

        /// <summary>
        /// Znode Version 8.0
        /// Get All the Product Alternate Images.
        /// </summary>
        /// <param name="expands">NameValueCollection expands</param>
        /// <param name="filters">List<Tuple<string, string, string>> filters</param>
        /// <param name="sorts">NameValueCollection sorts</param>
        /// <param name="page">NameValueCollection page</param>
        /// <returns></returns>
        ProductAlternateImageListModel GetAllProductAlternateImages(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Znode Version 8.0
        /// Get the product alternate image.
        /// </summary>
        /// <param name="productImageId">int productImageId</param>
        /// <param name="expands">NameValueCollection expands</param>
        /// <returns></returns>
        ProductAlternateImageModel GetProductAlternateImageById(int productImageId, NameValueCollection expands);
        #endregion

        #region Product AddOns

        /// <summary>
        /// Znode Version 8.0
        /// Get associated addons for specified product id.
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <param name="filters">List<Tuple<string, string, string>> filters</param>
        /// <param name="sorts">NameValueCollection sorts</param>
        /// <param name="page">NameValueCollection page</param>
        /// <returns>Return List of Addons</returns>
        AddOnListModel GetAssociatedAddOns(int productId, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Znode Version 8.0
        /// Removes the specfied mapping between product and addon. 
        /// </summary>
        /// <param name="productAddOnId"></param>
        /// <returns></returns>
        bool RemoveProductAddOn(int productAddOnId);

        /// <summary>
        /// Znode Version 8.0
        /// Associate specfied addons with the current product. 
        /// </summary>
        /// <param name="models"></param>
        bool AssociateProductAddOn(List<AddOnModel> models);

        /// <summary>
        /// Znode Version 8.0
        /// Return addons which are not associated with specified product.
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <param name="filters">List<Tuple<string, string, string>> filters</param>
        /// <param name="sorts">NameValueCollection sorts</param>
        /// <param name="page">NameValueCollection page</param>
        /// <param name="portalId">int portalId</param>
        /// <returns>Return List of Addons</returns>
        AddOnListModel GetUnassociatedAddOns(int productId, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page, int portalId = 0);
        #endregion

        #region Product Details
        /// <summary>
        /// Copies a product.
        /// </summary>
        /// <param name="productId">Product ID</param>
        /// <returns>Copied product model.</returns>
        ProductModel CopyProduct(int productId);

        /// <summary>
        /// Deletes a product.
        /// </summary>
        /// <param name="productId">Product id of the Product.</param>
        /// <returns>Returns true or false.</returns>
        bool DeleteByProductId(int productId);
        #endregion

        #region Product Highlights

        /// <summary>
        /// Return associated highlights for specified product id.
        /// </summary>
        /// <param name="productId">productId</param>
        /// <param name="sorts">sorts</param>
        /// <param name="page">page</param>
        /// <returns>HighlightListModel</returns>
        HighlightListModel GetAssociatedHighlights(int productId, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Znode Version 8.0
        /// Removes the specified mapping between highlight and product. 
        /// </summary>
        /// <param name="productHighlightId"></param>
        /// <returns></returns>
        bool RemoveProductHighlight(int productHighlightId);

        /// <summary>
        /// Znode Version 8.0
        /// Associate specfied highlights with the current product. 
        /// </summary>
        /// <param name="models"></param>
        /// <returns></returns>
        bool AssociateProductHighlight(List<HighlightModel> models);

        /// <summary>
        /// Znode Version 8.0
        /// Return highlights which are not associated with specified product.
        /// </summary>
        /// <param name="productId">productId</param>
        /// <param name="sorts">sorts</param>
        /// <param name="page">page</param>
        /// <param name="portalId">portalId</param>
        /// <returns>HighlightListModel</returns>
        HighlightListModel GetUnassociatedHighlights(int productId, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page, int portalId = 0);

        #endregion

        /// <summary>
        /// Get the list of products on the basis of product name, product number,
        /// manufacturer name, product type, sku, category
        /// </summary>
        /// <param name="expands"></param>
        /// <param name="filters"></param>
        /// <param name="sorts"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        CategoryAssociatedProductsListModel GetAllProducts(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        #region Product Bundle

        /// <summary>
        /// Znode Version 8.0
        /// Get All the Product Bundle list.
        /// </summary>
        /// <param name="expands">NameValueCollection expands</param>
        /// <param name="filters">List<Tuple<string, string, string>> filters</param>
        /// <param name="sorts">NameValueCollection sorts</param>
        /// <param name="page">NameValueCollection page</param>
        /// <returns></returns>
        ProductBundlesListModel GetProductBundlesList(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Znode Version 8.0
        /// Delete the Product Bundle.
        /// </summary>
        /// <param name="parentChildProductID">int parentChildProductID</param>
        /// <returns>Return true or false</returns>
        bool DeleteBundleProduct(int parentChildProductID);

        /// <summary>
        /// Znode Version 8.0
        /// Get All the Product list.
        /// </summary>
        /// <param name="expands">NameValueCollection expands</param>
        /// <param name="filters">List<Tuple<string, string, string>> filters</param>
        /// <param name="sorts">NameValueCollection sorts</param>
        /// <param name="page">NameValueCollection page</param>
        /// <returns></returns>
        ProductListModel GetProductList(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Znode Version 8.0
        /// To Associate Bundle Product.
        /// </summary>
        /// <param name="model">ProductModel</param>
        /// <returns>returns true/false</returns>
        bool AssociateBundleProduct(ProductModel model);


        #endregion

        #region Product Tier Pricing

        /// <summary>
        /// Znode Version 8.0
        /// Return product tiers for specfied product id.
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="filters"></param>
        /// <param name="sorts"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        TierListModel GetProductTiers(int productId, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Znode Version 8.0
        /// Insert product tier for specified product.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        ProductTierPricingModel CreateProductTier(ProductTierPricingModel model);

        /// <summary>
        /// Znode Version 8.0
        /// Update product tier for specified product.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        bool UpdateProductTier(ProductTierPricingModel model);

        /// <summary>
        /// Znode Version 8.0
        /// Delete product tier for specified product.
        /// </summary>
        /// <param name="productTierId"></param>
        /// <returns></returns>
        bool DeleteProductTier(int productTierId);

        #endregion

        #region Digital Asset

        /// <summary>
        /// Znode Version 8.0
        /// Return digital assets for specified product id.
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="filters"></param>
        /// <param name="sorts"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        DigitalAssetLisModel GetDigitalAssets(int productId, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Znode Version 8.0
        /// Insert digital asset for specified product. 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        DigitalAssetModel CreateDigitalAsset(DigitalAssetModel model);

        /// <summary>
        /// Znode Version 8.0
        /// Delete digital asset. 
        /// </summary>
        /// <param name="digitalAssetId">int digitalAssetId</param>
        /// <returns></returns>
        bool DeleteDigitalAsset(int digitalAssetId);

        #endregion

        #region Product search and list
        /// <summary>
        /// Get List and Search Functionality for product
        /// </summary>
        /// <param name="expands"></param>
        /// <param name="filters"></param>
        /// <param name="sorts"></param>
        /// <param name="page"></param>
        /// <returns>returns ProductListModel</returns>
        ProductListModel GetProductsList(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
        #endregion

        #region Product SEO Information
        /// <summary>
        /// To update seo setting of product
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <param name="model">ProductModel model</param>
        /// <returns>returns ProductModel</returns>
        ProductModel UpdateProductSEOInformation(int productId, ProductModel model);
        #endregion

        #region Update Image

        /// <summary>
        /// To update image
        /// </summary>
        /// <param name="model">UpdateImageModel model</param>
        /// <returns>returns updated model</returns>
        UpdateImageModel UpdateImage(UpdateImageModel model);

        #endregion

        /// <summary>
        /// Search Products
        /// </summary>
        /// <param name="expands">expands</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <param name="page">page</param>
        /// <returns>ProductListModel</returns>
        ProductListModel SearchProducts(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Create Order Product
        /// </summary>
        /// <param name="productId">productId</param>
        /// <returns>ProductModel</returns>
        ProductModel CreateOrderProduct(int productId);

        /// <summary>
        /// Get the sku product list by sku.
        /// </summary>
        /// <param name="filters">Filters.</param>
        /// <returns>Returns sku product list</returns>
        SkuProductListModel GetSkuProductListBySku(List<Tuple<string, string, string>> filters);

        /// <summary>
        /// Send Mail of compared product to friend
        /// </summary>
        /// <param name="compareProduct">Compare Product Model contains productIds</param>
        /// <returns>Boolean value true/false</returns>
        bool SendMailToFriend(CompareProductModel compareProducts);

        /// <summary>
        /// Get Products Inventory list
        /// </summary>
        /// <param name="expands">expands</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <param name="page">page</param>
        /// <returns>PRFTInventoryListModel</returns>
        PRFTInventoryListModel GetInventoryFromErp(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
    }
}
