﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using HighlightTypeRepository = ZNode.Libraries.DataAccess.Service.HighlightTypeService;

namespace Znode.Engine.Services
{
	public class HighlightTypeService : BaseService, IHighlightTypeService
	{
		private readonly HighlightTypeRepository _highlightTypeRepository;

		public HighlightTypeService()
		{
			_highlightTypeRepository = new HighlightTypeRepository();
		}

		public HighlightTypeModel GetHighlightType(int highlightTypeId)
		{
			var highlightType = _highlightTypeRepository.GetByHighlightTypeID(highlightTypeId);
			return HighlightTypeMap.ToModel(highlightType);
		}

		public HighlightTypeListModel GetHighlightTypes(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
		{
			var model = new HighlightTypeListModel();
			var highlightTypes = new TList<HighlightType>();

			var query = new HighlightTypeQuery();
			var sortBuilder = new HighlightTypeSortBuilder();
			var pagingStart = 0;
			var pagingLength = 0;

			SetFiltering(filters, query);
			SetSorting(sorts, sortBuilder);
			SetPaging(page, model, out pagingStart, out pagingLength);

			// Get the initial set
			var totalResults = 0;
			highlightTypes = _highlightTypeRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
			model.TotalResults = totalResults;

			// Check to set page size equal to the total number of results
			if (pagingLength == int.MaxValue)
			{
				model.PageSize = model.TotalResults;
			}

			// Map each item and add to the list
			foreach (var p in highlightTypes)
			{
				model.HighlightTypes.Add(HighlightTypeMap.ToModel(p));
			}

			return model;
		}

		public HighlightTypeModel CreateHighlightType(HighlightTypeModel model)
		{
			if (model == null)
			{
				throw new Exception("Highlight type model cannot be null.");
			}

			var entity = HighlightTypeMap.ToEntity(model);
			var highlightType = _highlightTypeRepository.Save(entity);
			return HighlightTypeMap.ToModel(highlightType);
		}

		public HighlightTypeModel UpdateHighlightType(int highlightTypeId, HighlightTypeModel model)
		{
			if (highlightTypeId < 1)
			{
				throw new Exception("Highlight type ID cannot be less than 1.");
			}

			if (model == null)
			{
				throw new Exception("Highlight type model cannot be null.");
			}

			var highlightType = _highlightTypeRepository.GetByHighlightTypeID(highlightTypeId);
			if (highlightType != null)
			{
				// Set the highlight type ID
				model.HighlightTypeId = highlightTypeId;

				var highlightTypeToUpdate = HighlightTypeMap.ToEntity(model);

				var updated = _highlightTypeRepository.Update(highlightTypeToUpdate);
				if (updated)
				{
					highlightType = _highlightTypeRepository.GetByHighlightTypeID(highlightTypeId);
					return HighlightTypeMap.ToModel(highlightType);
				}
			}

			return null;
		}

		public bool DeleteHighlightType(int highlightTypeId)
		{
			if (highlightTypeId < 1)
			{
				throw new Exception("Highlight type ID cannot be less than 1.");
			}

			var highlightType = _highlightTypeRepository.GetByHighlightTypeID(highlightTypeId);
			if (highlightType != null)
			{
				return _highlightTypeRepository.Delete(highlightType);
			}

			return false;
		}

		private void SetFiltering(List<Tuple<string, string, string>> filters, HighlightTypeQuery query)
		{
			foreach (var tuple in filters)
			{
				var filterKey = tuple.Item1;
				var filterOperator = tuple.Item2;
				var filterValue = tuple.Item3;

				if (filterKey == FilterKeys.Name) SetQueryParameter(HighlightTypeColumn.Name, filterOperator, filterValue, query);
			}
		}

		private void SetSorting(NameValueCollection sorts, HighlightTypeSortBuilder sortBuilder)
		{
			if (sorts.HasKeys())
			{
				foreach (var key in sorts.AllKeys)
				{
					var value = sorts.Get(key);

					if (key == SortKeys.HighlightTypeId) SetSortDirection(HighlightTypeColumn.HighlightTypeID, value, sortBuilder);
					if (key == SortKeys.Name) SetSortDirection(HighlightTypeColumn.Name, value, sortBuilder);
				}
			}
		}

		private void SetQueryParameter(HighlightTypeColumn column, string filterOperator, string filterValue, HighlightTypeQuery query)
		{
			base.SetQueryParameter(column, filterOperator, filterValue, query);
		}

		private void SetSortDirection(HighlightTypeColumn column, string value, HighlightTypeSortBuilder sortBuilder)
		{
			base.SetSortDirection(column, value, sortBuilder);
		}
	}
}
