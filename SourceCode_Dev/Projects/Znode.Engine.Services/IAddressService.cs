﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
	public interface IAddressService
	{
		AddressModel GetAddress(int addressId);
		AddressListModel GetAddresses(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
		AddressModel CreateAddress(AddressModel model);
		AddressModel UpdateAddress(int addressId, AddressModel model);
		bool DeleteAddress(int addressId);
		bool AddDefaultAddresses(int accountId);

        /// <summary>
        /// To set default address.
        /// </summary>
        /// <param name="addressId">Address id </param>
        /// <param name="model">AddressModel</param>
        /// <param name="errorCode">errorCode for catch current error message</param>
        /// <returns>AddressModel</returns>
        AddressModel UpdateAddressDefault(int addressId, AddressModel model, out string errorCode);

        /// <summary>
        /// To update customer address.
        /// </summary>
        /// <param name="accountId">customer account id</param>
        /// <param name="listModel">AddressListModel with address information</param>
        /// <returns>AddressListModel</returns>
        AddressListModel UpdateCustomerAddress(int accountId, AddressListModel listModel);
	}
}