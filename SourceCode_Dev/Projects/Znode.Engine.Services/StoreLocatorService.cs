﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Helpers.Constants;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;
using StoreRepository = ZNode.Libraries.DataAccess.Service.StoreService;

namespace Znode.Engine.Services
{
    public class StoreLocatorService : BaseService, IStoreLocatorService
    {
        #region Private Varibles
        private readonly StoreRepository _storeRepository;
        #endregion

        #region Public Constructor
        public StoreLocatorService()
        {
            _storeRepository = new StoreRepository();
        }
        #endregion

        #region Public Methods


        public StoreModel CreateStoreLocator(StoreModel model)
        {
            if (Equals(model, null))
            {
                throw new Exception("Store model cannot be null.");
            }

            var entity = StoreMap.ToEntity(model);
            var storeLocator = _storeRepository.Save(entity);
            return StoreMap.ToModel(storeLocator);
        }

        public StoreModel GetStoreLocator(int storeId)
        {
            var storeLocator = _storeRepository.GetByStoreID(storeId);
            return StoreMap.ToModel(storeLocator);
        }

        public StoreListModel GetStoreLocators(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new StoreListModel();
            var storeLocator = new TList<Store>();

            var query = new StoreQuery();
            var sort = new StoreSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetFiltering(filters, query);
            SetSorting(sorts, sort);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            storeLocator = _storeRepository.Find(query, sort, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (Equals(pagingLength, int.MaxValue))
            {
                model.PageSize = model.TotalResults;
            }

            // Map each item and add to the list
            foreach (var m in storeLocator)
            {
                model.Stores.Add(StoreMap.ToModel(m));
            }

            return model;
        }

        public bool DeleteStoreLocator(int storeId)
        {
            if (storeId < 1)
            {
                throw new Exception("Store ID cannot be less than 1.");
            }

            var storeLocator = _storeRepository.GetByStoreID(storeId);
            if (!Equals(storeLocator, null))
            {
                return _storeRepository.Delete(storeLocator);
            }

            return false;
        }

        public StoreModel UpdateStoreLocator(int storeId, StoreModel model)
        {
            if (storeId < 1)
            {
                throw new Exception("Store ID cannot be less than 1.");
            }

            if (Equals(model, null))
            {
                throw new Exception("Store model cannot be null.");
            }

            var storeLocator = _storeRepository.GetByStoreID(storeId);
            if (!Equals(storeLocator, null))
            {
                // Set Store ID
                model.StoreID = storeId;

                var storeToUpdate = StoreMap.ToEntity(model);

                var updated = _storeRepository.Update(storeToUpdate);
                if (updated)
                {
                    storeLocator = _storeRepository.GetByStoreID(storeId);
                    return StoreMap.ToModel(storeLocator);
                }
            }

            return null;
        }

        public StoreListModel GetStoresList(StoreModel model)
        {
            ZNodeStoreList _store = new ZNodeStoreList();
            StoreListModel storeList = new StoreListModel();

            int count = _store.GetZipCodeCount();

            if (count > 0)
            {
                ZNodeStoreList storesList = ZNodeStoreList.SearchByZipCodeAndRadius(model.Zip, model.Radius, model.City, model.State, model.AreaCode, ZNodeConfigManager.SiteConfig.PortalID);
                if (!Equals(storesList, null) && !Equals(storesList.StoreCollection, null) && storesList.StoreCollection.Count > 0)
                {
                    foreach (ZNodeStore item in storesList.StoreCollection)
                    {
                        StoreModel store = StoreMap.ToStoreModel(item);
                        storeList.Stores.Add(store);
                    }
                }
                else
                {
                    throw new Exception("No stores were found at the above location. Please enter a different Zip code / Radius");
                }
            }
            else
            {
                throw new Exception("No Zip Code data has been uploaded to the database.");
            }
            return storeList;
        }

        #endregion

        #region Private Methods

        private void SetFiltering(List<Tuple<string, string, string>> filters, StoreQuery query)
        {
            bool isPortalIdPresent = false;
            string UserName = string.Empty;
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;
                if (!Equals(filterKey, FilterKeys.UserName))
                {
                    if (filterKey.Equals(FilterKeys.PortalId))
                    {
                        isPortalIdPresent = true;
                        SetQueryParameter(StoreColumn.PortalID, filterOperator, filterValue, query);
                    }
                    if (Equals(filterKey, FilterKeys.ActiveInd)) { SetQueryParameter(StoreColumn.ActiveInd, filterOperator, filterValue, query); }
                    if (Equals(filterKey, FilterKeys.Name)) { SetQueryParameter(StoreColumn.Name, filterOperator, filterValue, query); }
                    if (Equals(filterKey, FilterKeys.City)) { SetQueryParameter(StoreColumn.City, filterOperator, filterValue, query); }
                    if (Equals(filterKey, FilterKeys.State)) { SetQueryParameter(StoreColumn.State, filterOperator, filterValue, query); }
                    if (Equals(filterKey, FilterKeys.Zip)) { SetQueryParameter(StoreColumn.Zip, filterOperator, filterValue, query); }
                    if (Equals(filterKey, FilterKeys.PortalId)) { SetQueryParameter(StoreColumn.PortalID, filterOperator, filterValue, query); }
                }
                else
                {
                    UserName = filterValue;
                }
            }
            if (!isPortalIdPresent)
            {
                string portalIds = GetAvailablePortals(UserName);
                if (!string.IsNullOrEmpty(portalIds) && !Equals(portalIds, "0"))
                {
                    query.AppendIn(SqlUtil.AND, StoreColumn.PortalID, portalIds.Split(','));
                }
            }
        }

        private void SetSorting(NameValueCollection sorts, StoreSortBuilder sortBuilder)
        {
            if (sorts.HasKeys())
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);

                    if (Equals(value, SortKeys.DisplayOrder)) { SetSortDirection(StoreColumn.DisplayOrder, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (Equals(value, SortKeys.StoreID)) { SetSortDirection(StoreColumn.StoreID, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (Equals(value, SortKeys.Name)) { SetSortDirection(StoreColumn.Name, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (Equals(value, SortKeys.City)) { SetSortDirection(StoreColumn.City, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (Equals(value, SortKeys.State)) { SetSortDirection(StoreColumn.State, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (Equals(value, SortKeys.Zip)) { SetSortDirection(StoreColumn.Zip, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                }
            }
        }

        private void SetQueryParameter(StoreColumn column, string filterOperator, string filterValue, StoreQuery query)
        {
            base.SetQueryParameter(column, filterOperator, filterValue, query);
        }

        private void SetSortDirection(StoreColumn column, string value, StoreSortBuilder sortBuilder)
        {
            base.SetSortDirection(column, value, sortBuilder);
        }

        #endregion
    }
}
