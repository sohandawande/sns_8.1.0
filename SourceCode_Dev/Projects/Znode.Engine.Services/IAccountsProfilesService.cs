﻿
namespace Znode.Engine.Services
{
    public interface IAccountsProfilesService
    {
        /// <summary>
        /// Save Account Associated Profiles.
        /// </summary>
        /// <param name="accountId">account id</param>
        /// <param name="profileIds">profile Ids</param>
        /// <returns>True / false when record is inserted</returns>
        bool AccountAssociatedProfiles(int accountId, string profileIds);

        /// <summary>
        /// Delete Existing Account Profile.
        /// </summary>
        /// <param name="accountProfileId">accountProfileId</param>
        /// <returns>True / False</returns>
        bool DeleteAccountProfile(int accountProfileId);

    }
}
