﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Libraries.Helpers;
using Znode.Libraries.Helpers.Extensions;
using ZNode.Libraries.DataAccess.Custom;

namespace Znode.Engine.Services
{
    public class ApplicationSettingService : BaseService, IApplicationSettingService
    {

        public ApplicationSettingListModel GetApplicationSettings(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            List<Tuple<string, string, string>> filterList = new List<Tuple<string, string, string>>();
            ApplicationSettingListModel list = new ApplicationSettingListModel();
            int totalRowCount = 0;
            int pagingStart = Convert.ToInt32(page.Get(PageKeys.Index));
            int pagingLength = Convert.ToInt32(page.Get(PageKeys.Size));
            string innerWhereClause = string.Empty;

            string orderBy = StringHelper.GenerateDynamicOrderByClause(sorts);
         
            string whereClause = StringHelper.GenerateDynamicWhereClause(filters);

            StoredProcedureHelper storedProcedureHelper = new StoredProcedureHelper();

            DataSet resultDataSet = storedProcedureHelper.SP_DataDetailsSpConfig(whereClause, null, "ZNodeApplicationSetting", orderBy, pagingStart.ToString(), pagingLength.ToString(), out totalRowCount, null, innerWhereClause);

            list.ApplicationSettingList = resultDataSet.Tables[0].ToList<ApplicationSettingDataModel>();
            list.TotalResults = totalRowCount;
            list.PageSize = pagingLength;
            list.PageIndex = pagingStart;

            // Check to set page size equal to the total number of results
            if (pagingLength.Equals(int.MaxValue))
            {
                list.PageSize = list.TotalResults;
            }
            return list;
        }

        public DataSet GetColumnList(string entityType, string entityName)
        {
            entityName = entityName.Equals("Empty") ? null : entityName;
            ApplicationSettingHelper storedProcedureHelper = new ApplicationSettingHelper();
            DataSet resultDataSet = storedProcedureHelper.SP_ApplicationSettingColumnList(entityType, entityName);
            return resultDataSet;
        }

        public bool SaveXmlConfiguration(ApplicationSettingDataModel model)
        {
            ApplicationSettingHelper storedProcedureHelper = new ApplicationSettingHelper();
            bool result = false;

            DataSet resultDataSet = storedProcedureHelper.SP_SaveXml(model.SettingTableName, model.Id, model.GroupName, model.ItemName, model.Setting, model.ViewOptions, model.FrontPageName, model.FrontObjectName, model.IsCompressed, model.ActionMode);

            if (Equals(model.ViewOptions, "INSERT"))
            {
                if (!Equals(resultDataSet, null) && resultDataSet.Tables.Count > 0)
                {
                    int id = Convert.ToInt32(resultDataSet.Tables[0].Rows[0]["Id"]);
                    result = id > 0 ? true : false;
                }
            }
            else
            {
                result = true;
            }
            return result;
        }
    }
}
