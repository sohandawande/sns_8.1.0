﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Helpers;
using Znode.Libraries.Helpers.Constants;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using ProducCrossSellRepository = ZNode.Libraries.DataAccess.Service.ProductCrossSellService;

namespace Znode.Engine.Services
{
    public class PersonalizationService : BaseService, IPersonalizationService
    {
        #region Private Variables
        private readonly ProducCrossSellRepository _producCrossSellRepository;
        #endregion

        #region Constructor
        public PersonalizationService()
        {
            _producCrossSellRepository = new ProducCrossSellRepository();
        }
        #endregion

        #region Public Methods

        public CrossSellListModel GetFrequentlyBoughtProductList(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            CrossSellListModel model = new CrossSellListModel();

            int pagingStart = 0, pagingLength = 0;

            string whereClause = string.Empty, orderbyClause = StringHelper.GenerateDynamicOrderByClause(sorts);
            string storedProcedureName = string.Empty;

            SetPaging(page, model, out pagingStart, out pagingLength);
            pagingStart = Convert.ToInt32(page.Get(PageKeys.Index));

            StoredProcedureHelper storedProcedureHelper = new StoredProcedureHelper();
            int totalRowCount = 0, crossSellType = 0;

            foreach (Tuple<string, string, string> filterItem in filters)
            {
                if (Equals(filterItem.Item1, FilterKeys.CrossSellType))
                {
                    if (Equals(filterItem.Item3, ((int)ProductRelationCode.FrequentlyBoughtTogether).ToString()))
                    {
                        crossSellType = (int)ProductRelationCode.FrequentlyBoughtTogether;
                        storedProcedureName = StoredProcedureKeys.SpGetFrequentlyBoughtProducts;
                        filters.Remove(filterItem);
                        break;
                    }
                    else if (Equals(filterItem.Item3, ((int)ProductRelationCode.YouMayAlsoLike).ToString()))
                    {
                        crossSellType = (int)ProductRelationCode.YouMayAlsoLike;
                        storedProcedureName = StoredProcedureKeys.SpGetYouMayAlsoProductList;
                        filters.Remove(filterItem);
                        break;
                    }
                }
            }
            whereClause = StringHelper.GenerateDynamicWhereClause(filters);

            DataSet resultDataSet = storedProcedureHelper.SP_DataDetailsSpConfig(whereClause, null, storedProcedureName, orderbyClause, pagingStart.ToString(), pagingLength.ToString(), out totalRowCount, null, null);

            if (!Equals(resultDataSet, null) && resultDataSet.Tables.Count > 0 && resultDataSet.Tables[0].Rows.Count > 0)
            {
                if (Equals(crossSellType, (int)ProductRelationCode.FrequentlyBoughtTogether)) { model = CrossSellMap.ToFrequentlyBoughtModelList(resultDataSet); }
                else if (Equals(crossSellType, (int)ProductRelationCode.YouMayAlsoLike)) { model = CrossSellMap.ToAssociatedProductItemModel(resultDataSet); }
            }
            model.TotalResults = totalRowCount;
            model.PageSize = pagingLength;
            model.PageIndex = pagingStart;
            // Check to set page size equal to the total number of results
            if (pagingLength.Equals(int.MaxValue))
            {
                model.PageSize = model.TotalResults;
            }
            return model;
        }

        public CrossSellListModel GetCrossSellProductsByProductId(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new CrossSellListModel();
            var crossSellItems = new TList<ProductCrossSell>();

            var sortBuilder = new ProductCrossSellSortBuilder();
            var query = new ProductCrossSellQuery();
            var pagingStart = 0;
            var pagingLength = 0;

            SetFiltering(filters, query);
            SetPaging(page, model, out pagingStart, out pagingLength);
            // Get the initial set
            var totalResults = 0;
            crossSellItems = _producCrossSellRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (pagingLength.Equals(int.MaxValue))
            {
                model.PageSize = model.TotalResults;
            }

            // Map each item and add to the list
            foreach (var crossSellItem in crossSellItems)
            {
                model.CrossSellProducts.Add(CrossSellMap.ToModel(crossSellItem));
            }
            return model;
        }

        public CrossSellModel CreateCrossSellProduct(CrossSellModel model)
        {
            CrossSellListModel listModel = new CrossSellListModel();

            if (Equals(model, null))
            {
                throw new Exception("CrossSell model cannot be null.");
            }

            listModel.CrossSellProducts.Add(model);

            var entity = CrossSellMap.ToEntity(model);
            var crossSellProduct = _producCrossSellRepository.Save(entity);

            if (!Equals(crossSellProduct, null))
            {
                return CrossSellMap.ToModel(crossSellProduct);
            }
            return null;
        }

        public bool DeleteCrossSellProducts(int productId, int relationTypeId)
        {
            List<Tuple<string, string, string>> filters = new List<Tuple<string, string, string>>();
            Tuple<string, string, string> productidTuple = new Tuple<string, string, string>(FilterKeys.ProductId, FilterOperators.Equals, productId.ToString());
            Tuple<string, string, string> relationTypeIdTuple = new Tuple<string, string, string>(FilterKeys.RelationTypeId, FilterOperators.Equals, relationTypeId.ToString());

            filters.Add(productidTuple);
            filters.Add(relationTypeIdTuple);

            CrossSellListModel listModel = this.GetCrossSellProductsByProductId(filters, null, null);

            if (!Equals(listModel, null) && !Equals(listModel.CrossSellProducts, null))
            {
                if (listModel.CrossSellProducts.Count.Equals(0))
                {
                    return true;
                }
                else
                {
                    foreach (CrossSellModel crossSellItem in listModel.CrossSellProducts)
                    {
                        var crossSellProducts = _producCrossSellRepository.GetByProductId(crossSellItem.ProductId);

                        //var product = _producCrossSellRepository.GetByRelationTypeId()
                        if (!Equals(crossSellProducts, null))
                        {
                            var crossSellProduct = _producCrossSellRepository.Delete(crossSellProducts);
                            return !Equals(crossSellProduct, null);
                        }
                    }
                }
            }
            return false;
        }

        public bool CreateCrossSellProducts(CrossSellListModel model)
        {
            bool isCreated = false;

            bool isDeleted = true;

            foreach (CrossSellModel crossSellProduct in model.CrossSellProducts)
            {
                isDeleted = isDeleted && this.DeleteCrossSellProducts(crossSellProduct.ProductId, crossSellProduct.RelationTypeId);
                if (!isDeleted)
                {
                    break;
                }
            }
            if (isDeleted)
            {
                if (!Equals(model, null) && !Equals(model.CrossSellProducts, null) && model.CrossSellProducts.Count > 0)
                {
                    foreach (CrossSellModel crossSellProduct in model.CrossSellProducts)
                    {
                        CrossSellModel productCrossSell = CreateCrossSellProduct(crossSellProduct);
                        if (!Equals(productCrossSell, null) && productCrossSell.ProductCrossSellTypeId > 0)
                        {
                            isCreated = true;
                        }
                        else
                        {
                            isCreated = false;
                            break;
                        }
                    }
                }
            }
            return isCreated;
        }

        #endregion

        #region Private Methods
        /// <summary>
        /// Sets filtering.
        /// </summary>
        /// <param name="filters">Filters.</param>
        /// <param name="query">Query.</param>
        private void SetFiltering(List<Tuple<string, string, string>> filters, ProductCrossSellQuery query)
        {
            if (!Equals(filters, null))
            {
                foreach (var tuple in filters)
                {
                    var filterKey = tuple.Item1;
                    var filterOperator = tuple.Item2;
                    var filterValue = tuple.Item3;

                    if (filterKey.Equals(FilterKeys.ProductId)) SetQueryParameter(ProductCrossSellColumn.ProductId, filterOperator, filterValue, query);
                    if (filterKey.Equals(FilterKeys.RelationTypeId)) SetQueryParameter(ProductCrossSellColumn.RelationTypeId, filterOperator, filterValue, query);
                }
            }
        }
        #endregion
    }
}

