﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using ShippingServiceCodeRepository = ZNode.Libraries.DataAccess.Service.ShippingServiceCodeService;
using ShippingTypeRepository = ZNode.Libraries.DataAccess.Service.ShippingTypeService;

namespace Znode.Engine.Services
{
    /// <summary>
    /// Shipping Service Code Service
    /// </summary>
	public class ShippingServiceCodeService : BaseService, IShippingServiceCodeService
	{
        #region Private Variables

        private readonly ShippingServiceCodeRepository _shippingServiceCodeRepository;
        private readonly ShippingTypeRepository _shippingTypeRepository; 

        #endregion

        #region Constructor
        public ShippingServiceCodeService()
        {
            _shippingServiceCodeRepository = new ShippingServiceCodeRepository();
            _shippingTypeRepository = new ShippingTypeRepository();
        } 
        #endregion

        #region Public Methods
        public ShippingServiceCodeModel GetShippingServiceCode(int shippingServiceCodeId, NameValueCollection expands)
        {
            var shippingServiceCode = _shippingServiceCodeRepository.GetByShippingServiceCodeID(shippingServiceCodeId);
            if (!Equals(shippingServiceCode, null))
            {
                GetExpands(expands, shippingServiceCode);
            }

            return ShippingServiceCodeMap.ToModel(shippingServiceCode);
        }

        public ShippingServiceCodeListModel GetShippingServiceCodes(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new ShippingServiceCodeListModel();
            var shippingServiceCodes = new TList<ShippingServiceCode>();
            var tempList = new TList<ShippingServiceCode>();

            var query = new ShippingServiceCodeQuery();
            var sortBuilder = new ShippingServiceCodeSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            tempList = _shippingServiceCodeRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (Equals(pagingLength, int.MaxValue))
            {
                model.PageSize = model.TotalResults;
            }

            // Now go through the list and get any expands
            foreach (var shippingServiceCode in tempList)
            {
                GetExpands(expands, shippingServiceCode);
                shippingServiceCodes.Add(shippingServiceCode);
            }

            // Map each item and add to the list
            foreach (var s in shippingServiceCodes)
            {
                model.ShippingServiceCodes.Add(ShippingServiceCodeMap.ToModel(s));
            }

            return model;
        }

        public ShippingServiceCodeModel CreateShippingServiceCode(ShippingServiceCodeModel model)
        {
            if (Equals(model, null))
            {
                throw new Exception("Shipping service code model cannot be null.");
            }

            var entity = ShippingServiceCodeMap.ToEntity(model);
            var shippingServiceCode = _shippingServiceCodeRepository.Save(entity);
            return ShippingServiceCodeMap.ToModel(shippingServiceCode);
        }

        public ShippingServiceCodeModel UpdateShippingServiceCode(int shippingServiceCodeId, ShippingServiceCodeModel model)
        {
            if (shippingServiceCodeId < 1)
            {
                throw new Exception("Shipping service code ID cannot be less than 1.");
            }

            if (Equals(model, null))
            {
                throw new Exception("Shipping service code model cannot be null.");
            }

            var shippingServiceCode = _shippingServiceCodeRepository.GetByShippingServiceCodeID(shippingServiceCodeId);
            if (!Equals(shippingServiceCode, null))
            {
                // Set the shipping service code ID and map
                model.ShippingServiceCodeId = shippingServiceCodeId;
                var shippingServiceCodeToUpdate = ShippingServiceCodeMap.ToEntity(model);

                var updated = _shippingServiceCodeRepository.Update(shippingServiceCodeToUpdate);
                if (updated)
                {
                    shippingServiceCode = _shippingServiceCodeRepository.GetByShippingServiceCodeID(shippingServiceCodeId);
                    return ShippingServiceCodeMap.ToModel(shippingServiceCode);
                }
            }

            return null;
        }

        public bool DeleteShippingServiceCode(int shippingServiceCodeId)
        {
            if (shippingServiceCodeId < 1)
            {
                throw new Exception("Shipping service code ID cannot be less than 1.");
            }

            var shippingServiceCode = _shippingServiceCodeRepository.GetByShippingServiceCodeID(shippingServiceCodeId);
            if (!Equals(shippingServiceCode, null))
            {
                return _shippingServiceCodeRepository.Delete(shippingServiceCode);
            }

            return false;
        } 
        #endregion       

        #region Private Methods
        private void GetExpands(NameValueCollection expands, ShippingServiceCode shippingServiceCode)
        {
            if (expands.HasKeys())
            {
                ExpandShippingType(expands, shippingServiceCode);
            }
        }

        private void ExpandShippingType(NameValueCollection expands, ShippingServiceCode shippingServiceCode)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.ShippingType)))
            {
                var shippingType = _shippingTypeRepository.GetByShippingTypeID(shippingServiceCode.ShippingTypeID);
                if (!Equals(shippingType, null))
                {
                    shippingServiceCode.ShippingTypeIDSource = shippingType;
                }
            }
        }

        private void SetFiltering(List<Tuple<string, string, string>> filters, ShippingServiceCodeQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;

                if (Equals(filterKey, FilterKeys.Code)) SetQueryParameter(ShippingServiceCodeColumn.Code, filterOperator, filterValue, query);
                if (Equals(filterKey, FilterKeys.Description)) SetQueryParameter(ShippingServiceCodeColumn.Description, filterOperator, filterValue, query);
                if (Equals(filterKey, FilterKeys.IsActive)) SetQueryParameter(ShippingServiceCodeColumn.ActiveInd, filterOperator, filterValue, query);
                if (Equals(filterKey, FilterKeys.ShippingTypeId)) SetQueryParameter(ShippingServiceCodeColumn.ShippingTypeID, filterOperator, filterValue, query);
            }
        }

        private void SetSorting(NameValueCollection sorts, ShippingServiceCodeSortBuilder sortBuilder)
        {
            if (sorts.HasKeys())
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);

                    if (Equals(key, SortKeys.Code)) SetSortDirection(ShippingServiceCodeColumn.Code, value, sortBuilder);
                    if (Equals(key, SortKeys.Description)) SetSortDirection(ShippingServiceCodeColumn.Description, value, sortBuilder);
                    if (Equals(key, SortKeys.DisplayOrder)) SetSortDirection(ShippingServiceCodeColumn.DisplayOrder, value, sortBuilder);
                    if (Equals(key, SortKeys.ShippingServiceCodeId)) SetSortDirection(ShippingServiceCodeColumn.ShippingServiceCodeID, value, sortBuilder);
                }
            }
        }

        private void SetQueryParameter(ShippingServiceCodeColumn column, string filterOperator, string filterValue, ShippingServiceCodeQuery query)
        {
            base.SetQueryParameter(column, filterOperator, filterValue, query);
        }

        private void SetSortDirection(ShippingServiceCodeColumn column, string value, ShippingServiceCodeSortBuilder sortBuilder)
        {
            base.SetSortDirection(column, value, sortBuilder);
        } 
        #endregion
	}
}
