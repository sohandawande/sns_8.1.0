﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using MasterPageRepository = ZNode.Libraries.DataAccess.Service.MasterPageServiceBase;

namespace Znode.Engine.Services
{
    public class MasterPageService : BaseService, IMasterPageService
    {
        #region Private Variables
        private readonly MasterPageRepository _masterPageRepository;
        #endregion

        #region Default Constructor
        public MasterPageService()
        {
            _masterPageRepository = new MasterPageRepository();
        }
        #endregion

        #region Public Methods
        public MasterPageListModel GetMasterPages(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new MasterPageListModel();
            var masterPageList = new TList<MasterPage>();
            var tempList = new TList<MasterPage>();

            var query = new MasterPageQuery();
            var sortBuilder = new MasterPageSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            tempList = _masterPageRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (Equals(pagingLength, int.MaxValue))
            {
                model.PageSize = model.TotalResults;
            }

            // Now go through the list and get any expands
            foreach (var masterpage in tempList)
            {
                GetExpands(expands, masterpage);
                masterPageList.Add(masterpage);
            }

            // Map each item and add to the list
            foreach (var a in masterPageList)
            {
                model.MasterPages.Add(MasterPageMap.ToModel(a));
            }

            return model;
        }

        public MasterPageListModel GetMasterPageListByThemeId(int themeId, string pageType)
        {
            PortalAdmin portalAdmin = new PortalAdmin();
            MasterPageListModel list = new MasterPageListModel();
            DataSet dataset = portalAdmin.GetMasterPage(themeId, pageType);
            if (!Equals(dataset, null) && dataset.Tables.Count > 0 && dataset.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in dataset.Tables[0].Rows)
                {
                    MasterPageModel item = new MasterPageModel();
                    item.MasterPageId = Convert.ToInt32(dr[0]);
                    item.ThemeId = Convert.ToInt32(dr[1]);
                    item.Name = dr[2].ToString();
                    list.MasterPages.Add(item);
                }
            }
            return list;
        }

        public MasterPageModel GetMasterPage(int themeId)
        {
            MasterPageKey key = new MasterPageKey();
            key.MasterPageID = themeId;
            var masterpage = _masterPageRepository.Get(key);
            return MasterPageMap.ToModel(masterpage);
        }

        #endregion

        #region Private Methods
        private void SetFiltering(List<Tuple<string, string, string>> filters, MasterPageQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;

                if (Equals(filterKey, FilterKeys.CSSName)) { SetQueryParameter(MasterPageColumn.Name, filterOperator, filterValue, query); }
                if (Equals(filterKey, FilterKeys.CSSThemeId)) { SetQueryParameter(MasterPageColumn.MasterPageID, filterOperator, filterValue, query); }
            }
        }

        private void SetSorting(NameValueCollection sorts, MasterPageSortBuilder sortBuilder)
        {
            if (sorts.HasKeys())
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);

                    if (Equals(key, SortKeys.CSSName)) SetSortDirection(MasterPageColumn.Name, value, sortBuilder);
                }
            }
        }
        private void GetExpands(NameValueCollection expands, MasterPage css)
        {
            if (expands.HasKeys())
            {

            }
        }
        #endregion
    }
}
