﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Helpers.Constants;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using ShippingRepository = ZNode.Libraries.DataAccess.Service.ShippingService;
using ShippingRuleRepository = ZNode.Libraries.DataAccess.Service.ShippingRuleService;
using ShippingRuleTypeRepository = ZNode.Libraries.DataAccess.Service.ShippingRuleTypeService;

namespace Znode.Engine.Services
{
    /// <summary>
    /// Shipping Rule Service
    /// </summary>
    public class ShippingRuleService : BaseService, IShippingRuleService
    {
        #region Private Variables

        private readonly ShippingRepository _shippingRepository;
        private readonly ShippingRuleRepository _shippingRuleRepository;
        private readonly ShippingRuleTypeRepository _shippingRuleTypeRepository;

        #endregion

        #region Constructor

        public ShippingRuleService()
        {
            _shippingRepository = new ShippingRepository();
            _shippingRuleRepository = new ShippingRuleRepository();
            _shippingRuleTypeRepository = new ShippingRuleTypeRepository();
        }

        #endregion

        #region Public Methods

        public ShippingRuleModel GetShippingRule(int shippingRuleId, NameValueCollection expands)
        {
            var shippingRule = _shippingRuleRepository.GetByShippingRuleID(shippingRuleId);
            if (!Equals(shippingRule, null))
            {
                GetExpands(expands, shippingRule);
            }

            return ShippingRuleMap.ToModel(shippingRule);
        }

        public ShippingRuleListModel GetShippingRules(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new ShippingRuleListModel();
            var shippingRules = new TList<ShippingRule>();
            var tempList = new TList<ShippingRule>();

            var query = new ShippingRuleQuery();
            var sortBuilder = new ShippingRuleSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            tempList = _shippingRuleRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (Equals(pagingLength, int.MaxValue))
            {
                model.PageSize = model.TotalResults;
            }

            // Now go through the list and get any expands
            foreach (var shippingRule in tempList)
            {
                GetExpands(expands, shippingRule);
                shippingRules.Add(shippingRule);
            }

            // Map each item and add to the list
            foreach (var s in shippingRules)
            {
                model.ShippingRules.Add(ShippingRuleMap.ToModel(s));
            }

            return model;
        }

        public ShippingRuleModel CreateShippingRule(ShippingRuleModel model)
        {
            if (Equals(model, null))
            {
                throw new Exception("Shipping rule model cannot be null.");
            }

            var shippingRuleCount = _shippingRuleRepository.GetByShippingID(model.ShippingOptionId).Find(x => x.ShippingRuleTypeID.Equals(model.ShippingRuleTypeId));

            if (!Equals(shippingRuleCount, null))
            {
                throw new Exception("Same shipping rule type cannot be added twice.");
            }

            var entity = ShippingRuleMap.ToEntity(model);
            var shippingRule = _shippingRuleRepository.Save(entity);
            return ShippingRuleMap.ToModel(shippingRule);
        }

        public ShippingRuleModel UpdateShippingRule(int shippingRuleId, ShippingRuleModel model)
        {
            var query = new ShippingRuleQuery();
            if (shippingRuleId < 1)
            {
                throw new Exception("Shipping rule ID cannot be less than 1.");
            }

            if (Equals(model, null))
            {
                throw new Exception("Shipping rule model cannot be null.");
            }

            query.AppendEquals(ShippingRuleColumn.ShippingID, model.ShippingOptionId.ToString());
            query.AppendEquals(ShippingRuleColumn.ShippingRuleTypeID, model.ShippingRuleTypeId.ToString());
            query.AppendNotEquals(ShippingRuleColumn.ShippingRuleID, shippingRuleId.ToString());
            TList<ShippingRule> list = _shippingRuleRepository.Find(query);
            if (list.Count > 0)
            {
                // Display error message
                throw new Exception("Same shipping rule type cannot be added twice.");
            }

            var shippingRule = _shippingRuleRepository.GetByShippingRuleID(shippingRuleId);
            if (!Equals(shippingRule, null))
            {
                // Set the shipping rule ID and map
                model.ShippingRuleId = shippingRuleId;
                var shippingRuleToUpdate = ShippingRuleMap.ToEntity(model);

                var updated = _shippingRuleRepository.Update(shippingRuleToUpdate);
                if (updated)
                {
                    shippingRule = _shippingRuleRepository.GetByShippingRuleID(shippingRuleId);
                    return ShippingRuleMap.ToModel(shippingRule);
                }
            }

            var shippingRuleType = _shippingRuleRepository.GetByShippingRuleTypeID(model.ShippingRuleTypeId);

            if (shippingRuleType.Count > 0)
            {
                throw new Exception("Shipping rule type exists.");
            }
            return null;
        }

        public bool DeleteShippingRule(int shippingRuleId)
        {
            if (shippingRuleId < 1)
            {
                throw new Exception("Shipping rule ID cannot be less than 1.");
            }

            var shippingRule = _shippingRuleRepository.GetByShippingRuleID(shippingRuleId);
            if (!Equals(shippingRule, null))
            {
                return _shippingRuleRepository.Delete(shippingRule);
            }

            return false;
        }

        #endregion

        #region Private Methods

        private void GetExpands(NameValueCollection expands, ShippingRule shippingRule)
        {
            if (expands.HasKeys())
            {
                ExpandShippingOption(expands, shippingRule);
                ExpandShippingRuleType(expands, shippingRule);
            }
        }

        private void ExpandShippingOption(NameValueCollection expands, ShippingRule shippingRule)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.ShippingOption)))
            {
                var shippingOption = _shippingRepository.GetByShippingID(shippingRule.ShippingID);
                if (!Equals(shippingOption, null))
                {
                    shippingRule.ShippingIDSource = shippingOption;
                }
            }
        }

        private void ExpandShippingRuleType(NameValueCollection expands, ShippingRule shippingRule)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.ShippingRuleType)))
            {
                var shippingRuleType = _shippingRuleTypeRepository.GetByShippingRuleTypeID(shippingRule.ShippingRuleTypeID);
                if (!Equals(shippingRuleType, null))
                {
                    shippingRule.ShippingRuleTypeIDSource = shippingRuleType;
                }
            }
        }

        private void SetFiltering(List<Tuple<string, string, string>> filters, ShippingRuleQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;

                if (Equals(filterKey, FilterKeys.ExternalId)) { SetQueryParameter(ShippingRuleColumn.ExternalID, filterOperator, filterValue, query); }
                if (Equals(filterKey, FilterKeys.ShippingOptionId)) { SetQueryParameter(ShippingRuleColumn.ShippingID, filterOperator, filterValue, query); }
                if (Equals(filterKey, FilterKeys.ShippingRuleTypeId)) { SetQueryParameter(ShippingRuleColumn.ShippingRuleTypeID, filterOperator, filterValue, query); }
                if (Equals(filterKey, FilterKeys.LowerLimit)) { SetQueryParameter(ShippingRuleColumn.LowerLimit, filterOperator, filterValue, query); }
                if (Equals(filterKey, FilterKeys.BaseCostWithDollar)) { SetQueryParameter(ShippingRuleColumn.BaseCost, filterOperator, filterValue, query); }
                if (Equals(filterKey, FilterKeys.PerItemCostWithDollar)) { SetQueryParameter(ShippingRuleColumn.PerItemCost, filterOperator, filterValue, query); }
                if (Equals(filterKey, FilterKeys.ShippingRuleId)) { SetQueryParameter(ShippingRuleColumn.ShippingRuleID, filterOperator, filterValue, query); }
            }
        }

        private void SetSorting(NameValueCollection sorts, ShippingRuleSortBuilder sortBuilder)
        {
            if (sorts.HasKeys())
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);

                    if (Equals(value, SortKeys.BaseCostWithDollar)) { SetSortDirection(ShippingRuleColumn.BaseCost, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (Equals(value, SortKeys.PerItemCostWithDollar)) { SetSortDirection(ShippingRuleColumn.PerItemCost, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (Equals(value, SortKeys.ShippingRuleId)) { SetSortDirection(ShippingRuleColumn.ShippingRuleID, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (Equals(value, SortKeys.LowerLimit)) { SetSortDirection(ShippingRuleColumn.LowerLimit, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (Equals(value, SortKeys.UpperLimit)) { SetSortDirection(ShippingRuleColumn.UpperLimit, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (Equals(value, SortKeys.RuleTypeName)) { SetSortDirection(ShippingRuleColumn.ShippingRuleTypeID, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                }
            }
        }

        private void SetQueryParameter(ShippingRuleColumn column, string filterOperator, string filterValue, ShippingRuleQuery query)
        {
            base.SetQueryParameter(column, filterOperator, filterValue, query);
        }

        private void SetSortDirection(ShippingRuleColumn column, string value, ShippingRuleSortBuilder sortBuilder)
        {
            base.SetSortDirection(column, value, sortBuilder);
        }

        #endregion
    }
}