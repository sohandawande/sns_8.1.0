﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Helpers;
using Znode.Libraries.Helpers.Constants;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using Znode.Libraries.Helpers.Extensions;
using CreditApplicationRepository = ZNode.Libraries.DataAccess.Service.PRFTCreditApplicationService;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;
using System.IO;
using System.Web;
using System.Text.RegularExpressions;
using System.Configuration;

namespace Znode.Engine.Services
{
    public class PRFTCreditApplicationService : BaseService, IPRFTCreditApplicationService
    {
        private readonly CreditApplicationRepository _creditApplicationRepository;
        private string apiUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);

        public PRFTCreditApplicationService()
        {
            _creditApplicationRepository = new CreditApplicationRepository();
        }

        /// <summary>
        /// Create Credit Application
        /// </summary>
        /// <param name="model">Credit Application Model</param>
        /// <returns></returns>
        public PRFTCreditApplicationModel CreateCreditApplication(PRFTCreditApplicationModel model)
        {
            if (Equals(model, null))
            {
                throw new Exception("Create Application model cannot be null.");
            }

            // Convert to code smith Entity
            var creditApplicationEntity = PRFTCreditApplicationMap.ToEntity(model);

            //Save to Database
            bool isSaved = _creditApplicationRepository.DeepSave(creditApplicationEntity);

            if(isSaved)
            {
                CreditApplicationMail(model);
            }
            else
            {
                return null;
            }
            return new PRFTCreditApplicationModel();
        }


        public PRFTCreditApplicationModel GetCreditApplication(int creditApplicationId, NameValueCollection expands)
        {
            var creditApplication = _creditApplicationRepository.DeepLoadByCreditApplicationID(creditApplicationId, true, DeepLoadType.IncludeChildren, typeof(TList<ZNode.Libraries.DataAccess.Entities.PRFTTradeReferences>));
            PRFTCreditApplicationModel model = PRFTCreditApplicationMap.ToModel(creditApplication);
            return model;
        }

        public PRFTCreditApplicationListModel GetCreditApplications(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            List<Tuple<string, string, string>> filterList = new List<Tuple<string, string, string>>();
            PRFTCreditApplicationListModel list = new PRFTCreditApplicationListModel();

            int totalRowCount = 0;
            var pagingStart = 0;
            var pagingLength = 0;
            string orderBy = StringHelper.GenerateDynamicOrderByClause(sorts);
            string innerWhereClause = string.Empty;
            foreach (var item in filters)
            {
                if (Equals(item.Item1, "username"))
                {
                    filterList.Add(item);
                    innerWhereClause = StringHelper.GenerateDynamicWhereClause(filterList);
                    filters.Remove(item);
                    break;
                }
            }
            string whereClause = StringHelper.GenerateDynamicWhereClause(filters);
            pagingStart = Convert.ToInt32(page.Get(PageKeys.Index));
            pagingLength = Convert.ToInt32(page.Get(PageKeys.Size));

            StoredProcedureHelper storedProcedureHelper = new StoredProcedureHelper();
            DataSet resultDataSet = storedProcedureHelper.SP_DataDetailsSpConfig(whereClause, string.Empty, StoredProcedureKeys.SpGetCreditApplicationList, orderBy, pagingStart.ToString(), pagingLength.ToString(), out totalRowCount, null, innerWhereClause);
            if (resultDataSet.Tables.Count > 0)
            {
                list.CreditApplications = resultDataSet.Tables[0].ToList<PRFTCreditApplicationModel>().ToCollection();
            }
            else
            {
                list.CreditApplications = new System.Collections.ObjectModel.Collection<PRFTCreditApplicationModel>();
            }
            list.TotalResults = totalRowCount;
            list.PageSize = pagingLength;
            list.PageIndex = pagingStart;

            // Check to set page size equal to the total number of results
            if (Equals(pagingLength, int.MaxValue))
            {
                list.PageSize = list.TotalResults;
            }

            return list;
        }

        public PRFTCreditApplicationModel UpdateCreditRequest(int creditApplicationId, PRFTCreditApplicationModel model)
        {
            if (creditApplicationId < 1)
            {
                throw new Exception("Credit Application ID cannot be less than 1.");
            }

            if (Equals(model, null))
            {
                throw new Exception("Credit Application model cannot be null.");
            }

            var creditApplication = _creditApplicationRepository.GetByCreditApplicationID(creditApplicationId);
            if (creditApplication != null)
            {
                // Set case request ID
                model.CreditApplicationID = creditApplicationId;
                var creditApplicationToUpdate = PRFTCreditApplicationMap.ToEntity(model);
                var updated = _creditApplicationRepository.Update(creditApplicationToUpdate);
                if (updated)
                {
                    creditApplication = _creditApplicationRepository.GetByCreditApplicationID(creditApplicationId);
                    return PRFTCreditApplicationMap.ToModel(creditApplication);
                }
            }
            return null;
        }

        public void CreditApplicationMail(PRFTCreditApplicationModel model)
        {

            var defaultTemplatePath = HttpContext.Current.Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, "CreditApplication.html"));

            if (File.Exists(defaultTemplatePath))
            {
                try
                {
                    string subject = ConfigurationManager.AppSettings["CreditApplicationSubject"];

                    StreamReader rw = new StreamReader(defaultTemplatePath);
                    string messageText = rw.ReadToEnd();                                        

                    Regex rx1 = new Regex("#LOGO#", RegexOptions.IgnoreCase);
                    messageText = rx1.Replace(messageText, ZNodeConfigManager.SiteConfig.StoreName);


                    Regex rx2 = new Regex("#StoreLogo#", RegexOptions.IgnoreCase);                                   
                    string LogoPath = ZNodeConfigManager.SiteConfig.LogoPath.Trim().Replace("~/", "");
                    messageText = rx2.Replace(messageText, apiUrl + "/" + LogoPath.TrimStart('~'));

                    var rx3 = new Regex("#MailingInfo#", RegexOptions.IgnoreCase);
                    messageText = rx3.Replace(messageText, ZNodeConfigManager.SiteConfig.CustomerServiceEmail);

                    var rx4 = new Regex("#SupportPhone#", RegexOptions.IgnoreCase);
                    messageText = rx4.Replace(messageText, ZNodeConfigManager.SiteConfig.CustomerServicePhoneNumber);

                    var rx5 = new Regex("#SupportEmail#", RegexOptions.IgnoreCase);
                    messageText = rx5.Replace(messageText, ZNodeConfigManager.SiteConfig.SalesEmail);

                    string logoLink = ConfigurationSettings.AppSettings["DemoWebsiteUrl"];
                    var rx6 = new Regex("#LogoLink#", RegexOptions.IgnoreCase);
                    messageText = rx6.Replace(messageText, logoLink);
                    
                    ZNode.Libraries.Framework.Business.ZNodeEmail.SendEmail(model.BusinessEmail, ZNodeConfigManager.SiteConfig.CustomerServiceEmail, string.Empty, subject, messageText, true);
                    //PRFT Custom Code:End
                }
                catch (Exception ex)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.GeneralError, model.BusinessName, "", null, ex.Message, null);
                }
            }
        }
    }
}
