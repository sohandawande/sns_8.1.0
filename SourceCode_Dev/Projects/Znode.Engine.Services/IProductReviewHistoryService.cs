﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    public interface IProductReviewHistoryService
    {
        /// <summary>
        /// This method will get the product review history list.
        /// </summary>
        /// <param name="expands">Expand Collection expands</param>
        /// <param name="filters">Filter criteria</param>
        /// <param name="sorts">sort criteria</param>
        /// <param name="page">page collection</param>
        /// <returns>Returns all product review  details in ProductReviewHistoryListModel format</returns>
        ProductReviewHistoryListModel GetProductReviewHistory(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// This method will get the product review history details.
        /// </summary>
        /// <param name="productReviewHistoryID">int productReviewHistoryID</param>
        /// <returns>Returns product review  details in ProductReviewHistoryModel format</returns>
        ProductReviewHistoryModel GetProductReviewHistoryById(int productReviewHistoryID);
    }
}
