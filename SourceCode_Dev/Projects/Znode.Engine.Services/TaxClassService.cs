﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using TaxClassRepository = ZNode.Libraries.DataAccess.Service.TaxClassService;
using TaxClassAdminRepository = ZNode.Libraries.Admin.TaxRuleAdmin;
using Znode.Libraries.Helpers.Constants;
using Znode.Libraries.Helpers;
using Znode.Libraries.Helpers.Extensions;
using System.Collections.ObjectModel;

namespace Znode.Engine.Services
{
    /// <summary>
    /// Tax Class Service
    /// </summary>
    public class TaxClassService : BaseService, ITaxClassService
    {
        #region Private Variables

        private readonly TaxClassRepository _taxClassRepository;

        #endregion

        #region Constructor
        public TaxClassService()
        {
            _taxClassRepository = new TaxClassRepository();
        }
        #endregion

        #region Public Methods
        public TaxClassModel GetTaxClass(int taxClassId)
        {
            var taxClass = _taxClassRepository.GetByTaxClassID(taxClassId);
            return TaxClassMap.ToModel(taxClass);
        }

        public TaxClassListModel GetTaxClasses(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new TaxClassListModel();
            var taxClasses = new TList<TaxClass>();

            var query = new TaxClassQuery();
            var sortBuilder = new TaxClassSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            taxClasses = _taxClassRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (Equals(pagingLength, int.MaxValue))
            {
                model.PageSize = model.TotalResults;
            }

            // Map each item and add to the list
            foreach (var t in taxClasses)
            {
                model.TaxClasses.Add(TaxClassMap.ToModel(t));
            }

            return model;
        }

        public TaxClassModel CreateTaxClass(TaxClassModel model)
        {
            var query = new TaxClassQuery();
            if (Equals(model, null))
            {
                throw new Exception("Tax class model cannot be null.");
            }
            query.AppendEquals(TaxClassColumn.Name, model.Name);
            query.AppendNotEquals(TaxClassColumn.TaxClassID, model.TaxClassId.ToString());
            query.AppendEquals(TaxClassColumn.PortalID, model.PortalId.ToString());
            TList<TaxClass> list = _taxClassRepository.Find(query);
            if (list.Count > 0)
            {
                // Display error message
                throw new Exception("An error occurred while saving the tax class, please try again.");
            }
            var entity = TaxClassMap.ToEntity(model);
            var taxClass = _taxClassRepository.Save(entity);
            return TaxClassMap.ToModel(taxClass);
        }

        public TaxClassModel ViewTaxClassDetails(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            TaxClassAdminRepository _taxClassRepository = new TaxClassAdminRepository();
            TaxClassModel model = new TaxClassModel();
            TaxRuleService objService = new TaxRuleService();

            model.TaxRuleList = objService.GetTaxRules(new NameValueCollection(), filters, sorts, page);

            var taxClassModel = _taxClassRepository.GetByTaxClassID(Convert.ToInt32(filters[0].Item3));

            model.DisplayOrder = taxClassModel.DisplayOrder;
            model.Name = taxClassModel.Name;
            model.ActiveInd = taxClassModel.ActiveInd;
            model.TaxClassId = taxClassModel.TaxClassID;
            model.PortalId = taxClassModel.PortalID;

            foreach (var x in model.TaxRuleList.TaxRules)
            {
                if (Equals(x.CountyFips, null))
                    x.CountyFips = string.Empty;
                else
                    x.CountyFips = _taxClassRepository.GetNameByCountyFIPS(x.CountyFips.ToString());
            }

            return model;
        }

        public TaxClassModel UpdateTaxClass(int taxClassId, TaxClassModel model)
        {
            var query = new TaxClassQuery();
            if (taxClassId < 1)
            {
                throw new Exception("Tax class ID cannot be less than 1.");
            }

            if (Equals(model, null))
            {
                throw new Exception("Tax class model cannot be null.");
            }

            query.AppendEquals(TaxClassColumn.Name, model.Name);
            query.AppendNotEquals(TaxClassColumn.TaxClassID, taxClassId.ToString());
            query.AppendEquals(TaxClassColumn.PortalID, model.PortalId.ToString());
            TList<TaxClass> list = _taxClassRepository.Find(query);
            if (list.Count > 0)
            {
                // Display error message
                throw new Exception("An error occurred while saving the tax class, please try again.");
            }

            var taxClass = _taxClassRepository.GetByTaxClassID(taxClassId);
            if (!Equals(taxClass, null))
            {
                // Set the tax class ID and map
                model.TaxClassId = taxClassId;
                var taxClassToUpdate = TaxClassMap.ToEntity(model);

                var updated = _taxClassRepository.Update(taxClassToUpdate);
                if (updated)
                {
                    taxClass = _taxClassRepository.GetByTaxClassID(taxClassId);
                    return TaxClassMap.ToModel(taxClass);
                }
            }

            return null;
        }

        public bool DeleteTaxClass(int taxClassId)
        {
            if (taxClassId < 1)
            {
                throw new Exception("Tax class ID cannot be less than 1.");
            }

            var taxClass = _taxClassRepository.GetByTaxClassID(taxClassId);
            if (!Equals(taxClass, null))
            {
                return _taxClassRepository.Delete(taxClass);
            }

            return false;
        }

        #region Znode Version 8.0
        /// <summary>
        /// To get Active Tax Class by PortalId
        /// </summary>
        /// <param name="portalId">int PortalId</param>
        /// <returns>List of active tax class having portalId equal to supplied portalId or null</returns>
        public TaxClassListModel GetActiveTaxClassByPortalId(int portalId)
        {
            var taxHelper = new TaxHelper();
            TaxClassListModel list = new TaxClassListModel();
            DataSet dataset = taxHelper.GetActiveTaxClassByPortalId(portalId);
            list.TaxClasses = dataset.Tables[0].ToList<TaxClassModel>().ToCollection();
            return list;
        }

        #endregion

        public TaxClassListModel GetTaxClassList(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            List<Tuple<string, string, string>> filterList = new List<Tuple<string, string, string>>();

            TaxClassListModel list = new TaxClassListModel();
            
            int totalRowCount = 0;
            var pagingStart = 0;
            var pagingLength = 0;
            string orderBy = StringHelper.GenerateDynamicOrderByClause(sorts);
            string innerWhereClause = string.Empty;
            foreach (var item in filters)
            {
                if (Equals(item.Item1, "username"))
                {
                    filterList.Add(item);
                    innerWhereClause = StringHelper.GenerateDynamicWhereClause(filterList);
                    filters.Remove(item);
                    break;
                }
            }
            string whereClause = StringHelper.GenerateDynamicWhereClause(filters); 
            pagingStart = Convert.ToInt32(page.Get(PageKeys.Index));
            pagingLength = Convert.ToInt32(page.Get(PageKeys.Size));

            StoredProcedureHelper storedProcedureHelper = new StoredProcedureHelper();
            
            DataSet resultDataSet = storedProcedureHelper.SP_DataDetailsSpConfig(whereClause, string.Empty, StoredProcedureKeys.SpZNodeGetTaxClasses, orderBy, pagingStart.ToString(), pagingLength.ToString(), out totalRowCount,null, innerWhereClause);

            list.TaxClasses = resultDataSet.Tables.Count > 0 ? resultDataSet.Tables[0].ToList<TaxClassModel>().ToCollection() : new Collection<TaxClassModel>();
            list.TotalResults = totalRowCount;
            list.PageSize = pagingLength;
            list.PageIndex = pagingStart;
            // Check to set page size equal to the total number of results
            if (Equals(pagingLength, int.MaxValue))
            {
                list.PageSize = list.TotalResults;
            }

            return list;
        }
        #endregion

        #region Private Methods
        private void SetFiltering(List<Tuple<string, string, string>> filters, TaxClassQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;

                if (Equals(filterKey, FilterKeys.ExternalId)) { SetQueryParameter(TaxClassColumn.ExternalID, filterOperator, filterValue, query); }
                if (Equals(filterKey, FilterKeys.IsActive)) { SetQueryParameter(TaxClassColumn.ActiveInd, filterOperator, filterValue, query); }
                if (Equals(filterKey, FilterKeys.Name)) { SetQueryParameter(TaxClassColumn.Name, filterOperator, filterValue, query); }
                if (Equals(filterKey, FilterKeys.PortalId)) { SetQueryParameter(TaxClassColumn.PortalID, filterOperator, filterValue, query); }
            }
        }

        private void SetSorting(NameValueCollection sorts, TaxClassSortBuilder sortBuilder)
        {
            if (sorts.HasKeys())
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);

                    if (Equals(value, SortKeys.DisplayOrder)) { SetSortDirection(TaxClassColumn.DisplayOrder, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (Equals(value, SortKeys.Name)) { SetSortDirection(TaxClassColumn.Name, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (Equals(value, SortKeys.TaxClassId)) { SetSortDirection(TaxClassColumn.TaxClassID, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                }
            }
        }

        private void SetQueryParameter(TaxClassColumn column, string filterOperator, string filterValue, TaxClassQuery query)
        {
            base.SetQueryParameter(column, filterOperator, filterValue, query);
        }

        private void SetSortDirection(TaxClassColumn column, string value, TaxClassSortBuilder sortBuilder)
        {
            base.SetSortDirection(column, value, sortBuilder);
        }

        #endregion
    }
}
