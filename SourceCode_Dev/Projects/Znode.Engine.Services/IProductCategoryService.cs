﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
	public interface IProductCategoryService
	{
		ProductCategoryModel GetProductCategory(int productCategoryId, NameValueCollection expands);
		ProductCategoryListModel GetProductCategories(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
        ProductCategoryModel CreateProductCategory(ProductCategoryModel productCategoryModel);
		ProductCategoryModel UpdateProductCategory(int productCategoryId, ProductCategoryModel productCategoryModel);
		bool DeleteProductCategory(int productCategoryId);

        /// <summary>
        /// Get Product Category on the basis of product id and category id
        /// </summary>
        /// <param name="productId"> int productId</param>
        /// <param name="categoryId"> int categoryId</param>
        /// <param name="expands"> expand collection</param>
        /// <returns>Product category model</returns>
        ProductCategoryModel GetProductCategories(int productId, int categoryId, NameValueCollection expands);
	}
}
