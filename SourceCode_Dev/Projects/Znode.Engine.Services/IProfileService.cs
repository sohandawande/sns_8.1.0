﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    public interface IProfileService
    {
        /// <summary>
        /// Method Gets profile details.
        /// </summary>
        /// <param name="profileId"></param>
        /// <param name="expands"></param>
        /// <returns></returns>
        ProfileModel GetProfile(int profileId, NameValueCollection expands);

        /// <summary>
        /// Method Gets the profile list.
        /// </summary>
        /// <param name="expands"></param>
        /// <param name="filters"></param>
        /// <param name="sorts"></param>
        /// <param name="page"></param>
        /// <returns>Return profile list.</returns>
        ProfileListModel GetProfiles(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Method Create the Profile.
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Return profile model.</returns>
        ProfileModel CreateProfile(ProfileModel model);

        /// <summary>
        /// Method Update the profile based on profileId.
        /// </summary>
        /// <param name="facetGroupId"></param>
        /// <param name="model"></param>
        /// <returns>Return updated profile model.</returns>
        ProfileModel UpdateProfile(int profileId, ProfileModel model);

        /// <summary>
        /// Method Delete the profile based on profileId.
        /// </summary>
        /// <param name="facetGroupId"></param>
        /// <returns>Return true or false.</returns>
        bool DeleteProfile(int profileId);

        /// <summary>
        /// Gets the profiles list.
        /// </summary>
        /// <returns>Returns model of type ProfileListModel.</returns>
        ProfileListModel GetProfiles();

        /// <summary>
        /// Gets profile list according to profile id.
        /// </summary>
        /// <param name="profileId">Porfile id.</param>
        /// <returns>ProfileListModel</returns>
        ProfileListModel GetProfileListByProfileId(int profileId);

        /// <summary>
        /// Get Account Profile
        /// </summary>
        /// <param name="expands">Expand Collection expands</param>
        /// <param name="filters">Filter criteria</param>
        /// <param name="sorts">sort criteria</param>
        /// <param name="page">page collection</param>
        /// <returns>Returns all account detaisl in ProfileListModel format</returns>
        ProfileListModel GetCustomerProfile(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Get Customer Not Asociated Profiles
        /// </summary>
        /// <param name="expands">Expand Collection expands</param>
        /// <param name="filters">Filter criteria</param>
        /// <param name="sorts">sort criteria</param>
        /// <param name="page">page collection</param>
        /// <returns>Returns all account detaisl in ProfileListModel format</returns>
        ProfileListModel GetAccountNotAssociatedProfiles(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Get the List of Profiles on the basis of portalId
        /// </summary>
        /// <param name="portalId">Portal Id to retrieve Profile List</param>
        /// <returns>ProfileListModel</returns>
        ProfileListModel GetProfileListByPortalId(int portalId);

        /// <summary>
        ///  Get the List of available profiles.
        /// </summary>
        /// <param name="model">Specifies the criteria on which the profiles selected.</param>
        /// <returns>List of available profiles.</returns>
        ProfileListModel GetAvailableProfileListBySkuIdCategoryId(AssociatedSkuCategoryProfileModel model);

        /// <summary>
        /// Get Znode profile Details
        /// </summary>
        /// <returns>Returns profile model</returns>
        ProfileModel GetZNodeProfile();

        /// <summary>
        /// Get Profiles List by userName
        /// </summary>
        /// <param name="userName">string userName</param>
        /// <returns>Return list of profiles</returns>
        ProfileListModel GetProfilesAssociatedWithUsers(string userName);
    }
}
