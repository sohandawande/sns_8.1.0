﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZNode.Libraries.DataAccess.Custom;
using CustomerUserMappingRepository = ZNode.Libraries.DataAccess.Service.PRFTCustomerUserMappingService;

namespace Znode.Engine.Services
{
    public class CustomerUserMappingService : BaseService, ICustomerUserMappingService
    {
        private readonly CustomerUserMappingRepository _customerUserMappingRepository;

        public CustomerUserMappingService()
        {
            _customerUserMappingRepository = new CustomerUserMappingRepository();
        }
        public bool DeleteUserAssociatedCustomer(int customerusermappingId)
        {
            if (customerusermappingId < 1)
            {
                throw new Exception("Account Profile ID cannot be less than 1.");
            }

            var customerUserMapping = _customerUserMappingRepository.GetByCustomerUserMappingId(customerusermappingId);
            if (!Equals(customerUserMapping, null))
            {
                return _customerUserMappingRepository.Delete(customerusermappingId);
            }

            return false;
        }

        public bool InsertUserAssociatedCustomer(int accountId, string customerAccountIds)
        {
            if (accountId < 1)
            {
                throw new Exception("Account ID cannot be less than 1.");
            }

            var customerHelper = new CustomerHelper();
            var isSuccess = customerHelper.InsertUserAssociatedCustomers(accountId, customerAccountIds);

            return isSuccess;
        }

        public bool DeleteAllUserMapping(int userAccountId)
        {
            if (userAccountId < 1)
            {
                throw new Exception("User Account ID cannot be less than 1.");
            }

            var customerHelper = new CustomerHelper();
            var isSuccess = customerHelper.DeleteAllUserMapping(userAccountId);

            return isSuccess;
        }
    }
}
