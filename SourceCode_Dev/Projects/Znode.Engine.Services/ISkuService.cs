﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services
{
    public interface ISkuService
    {
        SkuModel GetSku(int skuId, NameValueCollection expands);
        SkuListModel GetSkus(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
        SkuModel CreateSku(SkuModel model);
        SkuModel UpdateSku(int skuId, SkuModel model);
        bool DeleteSku(int skuId);

        /// <summary>
        /// Znode Version 7.2.2
        /// Get the SKU list on basis of SKU 
        /// </summary>
        /// <param name="strSku">strSku name</param>
        /// <param name="expands"></param>
        /// <returns>returns SKu List</returns>
        TList<SKU> GetSkuList(string strSku, NameValueCollection expands);

        /// <summary>
        /// Znode Version 8.0
        /// To Save Sku Attribute in ZnodeSkuAttribute
        /// </summary>
        /// <param name="skuId">int skuId</param>
        /// <param name="attributeIds">string comma separated attributeId</param>
        /// <returns>return true/false</returns>
        bool AddSkuAttribute(int skuId, string attributeIds);

        /// <summary>
        /// Get the list of sku on the basis of productId
        /// </summary>
        /// <param name="productId">productId to retrieve list of SKU</param>
        /// <returns>list of SKU</returns>
        SkuListModel GetSKUListByProductId(int productId);
    }
}
