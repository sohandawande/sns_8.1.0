﻿using System;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Services
{
	public static class SmtpService
	{
		public static bool SendMail(string email, string messageText)
		{
			var senderEmail = ZNodeConfigManager.SiteConfig.CustomerServiceEmail;
			var subject = ZNodeConfigManager.SiteConfig.StoreName;

			try
			{
				ZNodeEmail.SendEmail(email, senderEmail, String.Empty, subject, messageText, true);
			}
			catch (Exception)
			{
				return false;
			}

			return true;
		}
	}
}
