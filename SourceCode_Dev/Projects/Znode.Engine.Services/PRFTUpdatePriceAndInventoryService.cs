﻿using PRFT.Engine.ERP;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.Services
{
    public class PRFTUpdatePriceAndInventoryService : BaseService, IPRFTUpdatePriceAndInventoryService
    {
        public void UpdatePriceAndInventory()
        {
            PRFTUpdatePriceAndInventoryHelper helper = new PRFTUpdatePriceAndInventoryHelper();
            DataSet ds = helper.GetAllProductsFromZnode();

            DataTable dt = GetPriceAndInventoryFromERP(ds);
            if (dt.Rows.Count > 0)
            {
                helper.UpdatePriceAndInventoryToZnode(dt);
            }

        }

        public DataTable GetPriceAndInventoryFromERP(DataSet ds)
        {
            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[4] { new DataColumn("ProductId", typeof(int)),
                    new DataColumn("ProductNum", typeof(string)),
                    new DataColumn("ERPPrice",typeof(decimal)),
                    new DataColumn("ERPInventory",typeof(int))});

            ERPInventoryService inventoryService = new ERPInventoryService();
            ERPProductService productService = new ERPProductService();

            if (ds != null && ds.Tables.Count > 0)
            {
                DataTable productTable = new DataTable();
                if (ds.Tables[0] != null)
                {
                    productTable = ds.Tables[0];
                    int totalRecord = 10;
                    int i;

                    if (ConfigurationManager.AppSettings["ProcessLimitedRecords"] == "0")
                    {
                        totalRecord = productTable.Rows.Count;
                    }
                    ZNodeLogging.LogMessage("Get ERP Detail of all product-StartTime :: " + DateTime.Now);

                    for (i=0; i < totalRecord; i++)
                    {
                        if (productTable.Rows[i]["ProductNum"] != null)
                        {
                            try
                            {
                                string productId = productTable.Rows[i]["ProductId"].ToString();
                                string productNum = productTable.Rows[i]["ProductNum"].ToString();
                                int inventory = 0;
                                PRFTInventoryModel inventoryModel = inventoryService.GetInventoryDetails(productNum);
                                PRFTERPItemDetailsModel itemDetails = productService.GetProductPriceFromERP(ConfigurationManager.AppSettings["GenericUserExternalID"], productNum, ConfigurationManager.AppSettings["GenericERPCustomerType"]);
                                if (inventoryModel != null && itemDetails != null)
                                {
                                    inventory = inventoryModel.Milwaukee + inventoryModel.Bloomington+inventoryModel.Nebraska+inventoryModel.DesMoines;
                                    dt.Rows.Add(productId, productNum, itemDetails.UnitPrice, inventory);
                                }
                            }
                            catch (Exception ex)
                            {
                                ZNodeLogging.LogMessage("Exception occured for product n0. = " + productTable.Rows[i]["ProductNum"].ToString() + "::" + ex);
                                ZNodeLogging.LogMessage("No. of records get::" + i);
                            }
                        }

                    }
                    ZNodeLogging.LogMessage("Get ERP Detail of all product-EndTime::" + DateTime.Now);
                    ZNodeLogging.LogMessage("No. of records updated::" + i);
                }
            }

            return dt;
        }
    }
}
