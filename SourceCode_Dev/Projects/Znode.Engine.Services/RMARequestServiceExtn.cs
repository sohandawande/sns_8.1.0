﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using ZNode.Libraries.Admin;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Services
{
    public partial class RMARequestService
    {
        //PRFT Custom Code: Start
        public string apiUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
        //PRFT custom Code : End
        public bool SendStatusMailExtn(int RMAItemId)
        {
            string smtpServer = ZNodeConfigManager.SiteConfig.SMTPServer;
            string senderEmail = ZNodeConfigManager.SiteConfig.CustomerServiceEmail;
            string subject = ZNodeConfigManager.SiteConfig.StoreName;
            string ReceiverMailId = "";
            RMARequestAdmin rmaAdmin = new RMARequestAdmin();
            DataSet reportds = rmaAdmin.GetRMARequestReport(RMAItemId);
            if (reportds.Tables[0].Rows.Count > 0)
            {
                StringBuilder sign = new StringBuilder();
                if (Equals(reportds.Tables[0].Rows[0]["EnableEmailNotification"].ToString(), "True") && reportds.Tables[0].Rows[0]["CustomerNotification"].ToString().Length > 0)
                {
                    //sign.Append("Regards,<br />");
                    //sign.Append(ZNodeConfigManager.SiteConfig.StoreName + "<br />");
                    //sign.Append(reportds.Tables[0].Rows[0]["DepartmentName"].ToString() + "<br />");
                    //sign.Append(reportds.Tables[0].Rows[0]["DepartmentAddress"].ToString() + "<br />");
                    //sign.Append(reportds.Tables[0].Rows[0]["DepartmentEmail"].ToString() + "<br />");

                    if (reportds.Tables[0].Rows[0]["DepartmentEmail"].ToString().Length > 0)
                        senderEmail = reportds.Tables[0].Rows[0]["DepartmentEmail"].ToString();
                    ReceiverMailId = reportds.Tables[0].Rows[0]["BillingEmailId"].ToString();

                    if (!string.IsNullOrEmpty(ReceiverMailId) && !string.IsNullOrEmpty(ReceiverMailId))
                    {
                        //string subject = "Track your package";
                        string defaultTemplatePath = HttpContext.Current.Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, "RMANotification.html"));
                        if (File.Exists(defaultTemplatePath))
                        {
                            StreamReader rw = new StreamReader(defaultTemplatePath);
                            string messageText = rw.ReadToEnd();
                            Regex customerName = new Regex("#CustomerName#", RegexOptions.IgnoreCase);
                            messageText = customerName.Replace(messageText, reportds.Tables[0].Rows[0]["CustomerName"].ToString());

                            Regex customerNotification = new Regex("#CustomerNotification#", RegexOptions.IgnoreCase);
                            messageText = customerNotification.Replace(messageText, reportds.Tables[0].Rows[0]["CustomerNotification"].ToString());

                            Regex storeName = new Regex("#StoreName#", RegexOptions.IgnoreCase);
                            messageText = storeName.Replace(messageText, ZNodeConfigManager.SiteConfig.StoreName);

                            Regex departmentName = new Regex("#DepartmentName#", RegexOptions.IgnoreCase);
                            messageText = departmentName.Replace(messageText, reportds.Tables[0].Rows[0]["DepartmentName"].ToString());

                            Regex departmentAddress = new Regex("#DepartmentAddress#", RegexOptions.IgnoreCase);
                            messageText = departmentAddress.Replace(messageText, reportds.Tables[0].Rows[0]["DepartmentAddress"].ToString());

                            Regex departmentEmail = new Regex("#DepartmentEmail#", RegexOptions.IgnoreCase);
                            messageText = departmentEmail.Replace(messageText, reportds.Tables[0].Rows[0]["DepartmentEmail"].ToString());

                            //PRFT Custom Code : Start
                            string storeLogoPath = ZNodeConfigManager.SiteConfig.LogoPath.Trim().Replace("~/", "");

                            Regex rx6 = new Regex("#StoreLogo#", RegexOptions.IgnoreCase);
                            messageText = rx6.Replace(messageText, apiUrl + "/" + storeLogoPath.TrimStart('~'));

                            var rx7 = new Regex("#MailingInfo#", RegexOptions.IgnoreCase);
                            messageText = rx7.Replace(messageText, ZNodeConfigManager.SiteConfig.CustomerServiceEmail);

                            var rx8 = new Regex("#SupportPhone#", RegexOptions.IgnoreCase);
                            messageText = rx8.Replace(messageText, ZNodeConfigManager.SiteConfig.CustomerServicePhoneNumber);

                            var rx9 = new Regex("#SupportEmail#", RegexOptions.IgnoreCase);
                            messageText = rx9.Replace(messageText, ZNodeConfigManager.SiteConfig.SalesEmail);

                            string logoLink = ConfigurationManager.AppSettings["DemoWebsiteUrl"];
                            var rx10 = new Regex("#LogoLink#", RegexOptions.IgnoreCase);
                            messageText = rx10.Replace(messageText, logoLink);
                            //PRFT Custom Code : End


                            //StringBuilder messageText = new StringBuilder();

                            //messageText.Append("Dear " + reportds.Tables[0].Rows[0]["CustomerName"].ToString() + ", <br />");

                            //messageText.Append("<br />");

                            //messageText.Append(reportds.Tables[0].Rows[0]["CustomerNotification"].ToString() + "<br />");

                            //messageText.Append("<br />");
                            //messageText.Append(sign);

                            try
                            {
                                // Send mail
                                ZNode.Libraries.Framework.Business.ZNodeEmail.SendEmail(ReceiverMailId, senderEmail, String.Empty, subject, messageText.ToString(), true);
                                return true;
                            }
                            catch (Exception)
                            {
                                throw new Exception("Unable to send mail. Please check your mail settings.");
                            }
                        }

                    }
                }
            }
            return false;
        }

    }
}
