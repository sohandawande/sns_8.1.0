﻿namespace Znode.Engine.Services
{
    public interface ILicenseService
    {
        bool InstallLicense(string licenseType,string serialNumber,string name, string email,out string errorMessage );
        bool ValidateLicense();
    }
}