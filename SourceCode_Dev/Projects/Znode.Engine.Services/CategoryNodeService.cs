﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Data;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Helpers;
using Znode.Libraries.Helpers.Extensions;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using CatalogRepository = ZNode.Libraries.DataAccess.Service.CatalogService;
using CategoryAdminRepository = ZNode.Libraries.Admin.CategoryAdmin;
using CategoryHelperRepository = ZNode.Libraries.DataAccess.Custom.CategoryHelper;
using CategoryNodeRepository = ZNode.Libraries.DataAccess.Service.CategoryNodeService;
using CategoryRepository = ZNode.Libraries.DataAccess.Service.CategoryService;

namespace Znode.Engine.Services
{
    public class CategoryNodeService : BaseService, ICategoryNodeService
    {
        #region Private Variables
        private readonly CatalogRepository _catalogRepository;
        private readonly CategoryRepository _categoryRepository;
        private readonly CategoryNodeRepository _categoryNodeRepository;
        #endregion

        #region Constructor
        public CategoryNodeService()
        {
            _catalogRepository = new CatalogRepository();
            _categoryRepository = new CategoryRepository();
            _categoryNodeRepository = new CategoryNodeRepository();
        }
        #endregion

        #region Public Methods

        public CategoryNodeModel GetCategoryNode(int categoryNodeId, NameValueCollection expands)
        {
            var categoryNode = _categoryNodeRepository.GetByCategoryNodeID(categoryNodeId);
            if (!Equals(categoryNode, null))
            {
                GetExpands(expands, categoryNode);
            }

            return CategoryNodeMap.ToModel(categoryNode);
        }

        public CategoryNodeListModel GetCategoryNodes(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new CategoryNodeListModel();
            var categoryNodes = new TList<CategoryNode>();
            var tempList = new TList<CategoryNode>();

            var query = new CategoryNodeQuery();
            var sortBuilder = new CategoryNodeSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            tempList = _categoryNodeRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (Equals(pagingLength, int.MaxValue))
            {
                model.PageSize = model.TotalResults;
            }

            // Now go through the list and get any expands
            foreach (var categoryNode in tempList)
            {
                GetExpands(expands, categoryNode);
                categoryNodes.Add(categoryNode);
            }

            // Map each item and add to the list
            foreach (var cn in categoryNodes)
            {
                model.CategoryNodes.Add(CategoryNodeMap.ToModel(cn));
            }

            return model;
        }

        public CategoryNodeModel CreateCategoryNode(CategoryNodeModel model)
        {
            if (Equals(model, null))
            {
                throw new Exception("Category node model cannot be null.");
            }
            CategoryAdminRepository categoryAdminRepository = new CategoryAdminRepository();
            CategoryNodeModel returnModel = new CategoryNodeModel();

            var entity = CategoryNodeMap.ToEntity(model);
            var isCategoryAssociated = categoryAdminRepository.AddNode(entity);

            returnModel.IsCategoryAssociated = isCategoryAssociated;

            return returnModel;
        }

        public CategoryNodeModel UpdateCategoryNode(int categoryNodeId, CategoryNodeModel model)
        {
            if (categoryNodeId < 1)
            {
                throw new Exception("Category node ID cannot be less than 1.");
            }

            if (Equals(model, null))
            {
                throw new Exception("Category node model cannot be null.");
            }

            var categoryNode = _categoryNodeRepository.GetByCategoryNodeID(categoryNodeId);
            if (!Equals(categoryNode, null))
            {
                // Set the category node ID
                model.CategoryNodeId = categoryNodeId;

                var categoryNodeToUpdate = CategoryNodeMap.ToEntity(model);
                CategoryAdminRepository categoryAdminRepository = new CategoryAdminRepository();
                var updated = categoryAdminRepository.UpdateNode(categoryNodeToUpdate);
                if (updated)
                {
                    categoryNode = _categoryNodeRepository.GetByCategoryNodeID(categoryNodeId);
                    if (!Equals(categoryNode, null))
                    {
                        return CategoryNodeMap.ToModel(categoryNode);
                    }
                }
                else
                {
                    throw new Exception("Category cannot be updated, as associated category cannot be assigned as parent.");
                }
            }

            return null;
        }

        public bool DeleteCategoryNode(int categoryNodeId)
        {
            if (categoryNodeId < 1)
            {
                throw new Exception("Category node ID cannot be less than 1.");
            }

            var categoryNode = _categoryNodeRepository.GetByCategoryNodeID(categoryNodeId);
            if (!Equals(categoryNode, null))
            {
                return _categoryNodeRepository.Delete(categoryNode);
            }

            return false;
        }
        #endregion

        #region Private Methods
        private void GetExpands(NameValueCollection expands, CategoryNode categoryNode)
        {
            if (expands.HasKeys())
            {
                ExpandCatalog(expands, categoryNode);
                ExpandCategory(expands, categoryNode);
            }
        }

        private void ExpandCatalog(NameValueCollection expands, CategoryNode categoryNode)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Catalog)))
            {
                if (categoryNode.CatalogID.HasValue)
                {
                    var catalog = _catalogRepository.GetByCatalogID(categoryNode.CatalogID.Value);
                    if (!Equals(catalog, null))
                    {
                        categoryNode.CatalogIDSource = catalog;
                    }
                }
            }
        }

        private void ExpandCategory(NameValueCollection expands, CategoryNode categoryNode)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Category)))
            {
                var category = _categoryRepository.GetByCategoryID(categoryNode.CategoryID);
                if (!Equals(category, null))
                {
                    categoryNode.CategoryIDSource = category;
                }
            }
        }

        private void SetFiltering(List<Tuple<string, string, string>> filters, CategoryNodeQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;

                if (Equals(filterKey, FilterKeys.BeginDate)) { SetQueryParameter(CategoryNodeColumn.BeginDate, filterOperator, filterValue, query); }
                if (Equals(filterKey, FilterKeys.CatalogId)) { SetQueryParameter(CategoryNodeColumn.CatalogID, filterOperator, filterValue, query); }
                if (Equals(filterKey, FilterKeys.CategoryId)) { SetQueryParameter(CategoryNodeColumn.CategoryID, filterOperator, filterValue, query); }
                if (Equals(filterKey, FilterKeys.EndDate)) { SetQueryParameter(CategoryNodeColumn.EndDate, filterOperator, filterValue, query); }
                if (Equals(filterKey, FilterKeys.IsActive)) { SetQueryParameter(CategoryNodeColumn.ActiveInd, filterOperator, filterValue, query); }
                if (Equals(filterKey, FilterKeys.ParentCategoryNodeId)) { SetQueryParameter(CategoryNodeColumn.ParentCategoryNodeID, filterOperator, filterValue, query); }
            }
        }

        private void SetSorting(NameValueCollection sorts, CategoryNodeSortBuilder sortBuilder)
        {
            if (sorts.HasKeys())
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);

                    if (Equals(key, SortKeys.CatalogId)) { SetSortDirection(CategoryNodeColumn.CatalogID, value, sortBuilder); }
                    if (Equals(key, SortKeys.CategoryId)) { SetSortDirection(CategoryNodeColumn.CategoryID, value, sortBuilder); }
                    if (Equals(key, SortKeys.CategoryNodeId)) { SetSortDirection(CategoryNodeColumn.CategoryNodeID, value, sortBuilder); }
                    if (Equals(key, SortKeys.DisplayOrder)) { SetSortDirection(CategoryNodeColumn.DisplayOrder, value, sortBuilder); }
                }
            }
        }

        private void SetQueryParameter(CategoryNodeColumn column, string filterOperator, string filterValue, CategoryNodeQuery query)
        {
            base.SetQueryParameter(column, filterOperator, filterValue, query);
        }

        private void SetSortDirection(CategoryNodeColumn column, string value, CategoryNodeSortBuilder sortBuilder)
        {
            base.SetSortDirection(column, value, sortBuilder);
        }
        #endregion

        #region Znode Version 8.0
        public bool DeleteAssociatedCategoryNode(int categoryNodeId)
        {
            CategoryAdminRepository categoryAdmin = new CategoryAdminRepository();
            CategoryNodeRepository categoryNodeService = new CategoryNodeRepository();
            if (!categoryAdmin.IsChildCategoryExist(categoryNodeId))
            {
                CategoryNode node = new CategoryNode();
                node.CategoryNodeID = categoryNodeId;

                node = categoryNodeService.GetByCategoryNodeID(categoryNodeId);
                categoryNodeService.DeepLoad(node);
                string DepartmentName = node.CategoryIDSource.Name;

                return categoryAdmin.DeleteNode(node);
            }
            else
            {
                throw new Exception("This Category can not be deleted until all child categories are deleted.");
            }
        }

        public CategoryNodeListModel GetParentCategoryNodes(int catalogId, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            CategoryNodeListModel list = new CategoryNodeListModel();
            int totalRowCount = 0;
            var pagingStart = 0;
            var pagingLength = 0;
            string orderBy = StringHelper.GenerateDynamicOrderByClause(sorts);
            CategoryAdminRepository categoryAdmin = new CategoryAdminRepository();
            CategoryHelperRepository categoryHelper = new CategoryHelperRepository();
            DataSet datasetCategory = categoryHelper.GetCategoryNodes(catalogId, !Equals(filters, null) && filters.Count > 0 ? Convert.ToInt32(filters[0].Item3) : 0);

            foreach (System.Data.DataRow dr in datasetCategory.Tables[0].Select())
            {
                dr["Name"] = categoryAdmin.ParsePath(dr["Name"].ToString(), ">");
            }

            list.CategoryNodes = datasetCategory.Tables[0].ToList<CategoryNodeModel>().ToCollection();
            list.TotalResults = totalRowCount;
            list.PageSize = pagingLength;
            list.PageIndex = pagingStart;
            // Check to set page size equal to the total number of results
            if (Equals(pagingLength, int.MaxValue))
            {
                list.PageSize = list.TotalResults;
            }
            return list;

        }

        public BreadCrumbModel GetBreadCrumb(int categoryId, string seoUrl, bool isProductPresent,int catalogId)
        {
            CategoryHelper categoryHelper = new CategoryHelper();
            Collection<BreadCrumbModel> categoryHerarchyList = new Collection<BreadCrumbModel>();
            BreadCrumbModel model = new BreadCrumbModel();
            string categoryIds = (categoryId > 0) ? Convert.ToString(categoryId) : string.Empty;
            DataSet categoryHerarchy = categoryHelper.GetCategoryHerarchyOfCategory(categoryIds, seoUrl, catalogId);
            if (!Equals(categoryHerarchy, null) && categoryHerarchy.Tables.Count > 0 && !Equals(categoryHerarchy.Tables[0], null) && categoryHerarchy.Tables[0].Rows.Count > 0)
            {
                categoryHerarchyList = categoryHerarchy.Tables[0].ToList<BreadCrumbModel>().ToCollection();
            }
            if (!Equals(categoryHerarchyList, null))
            {
                int counter = 0;
                foreach (BreadCrumbModel category in categoryHerarchyList)
                {
                    counter++;
                    string tempCategoryName = System.Text.RegularExpressions.Regex.Replace(category.Name, "[^a-zA-Z0-9]", string.Empty).ToLower();
                    string url = !string.IsNullOrEmpty(category.SEOUrl) ? category.SEOUrl : string.Format("category/{0}/{1}", tempCategoryName, category.CategoryID);
                    model.BreadCrumb += (!isProductPresent && counter.Equals(categoryHerarchyList.Count)) ? string.Format(" \\ <span>{0}</span>", category.Name) : string.Format(" \\ <a href='/{0}' title='{1}'>{2}</a>", url, category.Name, category.Name);
                }
                return model;
            }
            return null;
        }
        #endregion
    }
}
