﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Helpers;
using Znode.Libraries.Helpers.Constants;
using Znode.Libraries.Helpers.Extensions;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using InventoryRepository = ZNode.Libraries.DataAccess.Service.SKUInventoryService;
using ProductAttributeRepository = ZNode.Libraries.DataAccess.Service.ProductAttributeService;
using ProductRepository = ZNode.Libraries.DataAccess.Service.ProductService;
using SkuAdminRepository = ZNode.Libraries.Admin.SKUAdmin;
using SkuAttributeRepository = ZNode.Libraries.DataAccess.Service.SKUAttributeService;
using SkuInventoryRepository = ZNode.Libraries.DataAccess.Service.SKUInventoryService;
using SkuRepository = ZNode.Libraries.DataAccess.Service.SKUService;
using SupplierRepository = ZNode.Libraries.DataAccess.Service.SupplierService;

namespace Znode.Engine.Services
{
    public class SkuService : BaseService, ISkuService
    {
        private readonly InventoryRepository _inventoryRepository;
        private readonly ProductAttributeRepository _productAttributeRepository;
        private readonly SkuRepository _skuRepository;
        private readonly SkuAttributeRepository _skuAttributeRepository;
        private readonly SupplierRepository _supplierRepository;
        private readonly ProductRepository _productRepository;
        private readonly SkuInventoryRepository _skuInventoryRepository;

        public SkuService()
        {
            _inventoryRepository = new InventoryRepository();
            _productAttributeRepository = new ProductAttributeRepository();
            _skuRepository = new SkuRepository();
            _skuAttributeRepository = new SkuAttributeRepository();
            _supplierRepository = new SupplierRepository();
            _productRepository = new ProductRepository();
            _skuInventoryRepository = new SkuInventoryRepository();
        }

        public SkuModel GetSku(int skuId, NameValueCollection expands)
        {
            var sku = _skuRepository.GetBySKUID(skuId);
            if (sku != null)
            {
                GetExpands(expands, sku);
            }

            return SkuMap.ToModel(sku);
        }

        public TList<SKU> GetSkuList(string strSku, NameValueCollection expands)
        {
            var skus = new TList<SKU>();
            skus = _skuRepository.GetBySKU(strSku);
            return skus;
        }

        public SkuListModel GetSkus(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new SkuListModel();
            var skus = new TList<SKU>();
            var tempList = new TList<SKU>();

            var query = new SKUQuery();
            var sortBuilder = new SKUSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            tempList = _skuRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (pagingLength.Equals(int.MaxValue))
            {
                model.PageSize = model.TotalResults;
            }

            // Now go through the list and get any expands
            foreach (var sku in tempList)
            {
                GetExpands(expands, sku);
                skus.Add(sku);
            }

            // Map each item and add to the list
            foreach (var s in skus)
            {
                model.Skus.Add(SkuMap.ToModel(s));
            }

            return model;
        }

        public SkuModel CreateSku(SkuModel model)
        {
            model.Error = "100";
            if (model.Equals(null))
            {
                throw new Exception("SKU model cannot be null.");
            }

            if (model.Inventory.Equals(null))
            {
                throw new Exception("SKU inventory model cannot be null.");
            }
            // First check inventory is exist
            if (IsSkuExist(model.Sku, model.ProductId))
            {
                model.Error = "101";
                return model;
            }

            // To check SKU combination is already exists.
            if (!String.IsNullOrEmpty(model.AttributesIds))
            {
                if (CheckSKUAttributesCombination(model.ProductId, model.SkuId, model.AttributesIds))
                {
                    model.Error = "102";
                    return model;
                }
            }

            // First save the inventory
            var inventoryEntity = InventoryMap.ToEntity(model.Inventory);
            var inventory = _inventoryRepository.Save(inventoryEntity);

            if (inventory != null)
            {
                // Now save the sku itself
                var skuEntity = SkuMap.ToEntity(model);
                var sku = _skuRepository.Save(skuEntity);

                //Checks wheather the Sku save or not
                if (sku.SKUID.Equals(0))
                {
                    model.Error = "101";
                    return model;
                }
                model.SkuId = sku.SKUID;
                if (!String.IsNullOrEmpty(model.AttributesIds))
                {
                    AddSkuAttribute(model.SkuId, model.AttributesIds);
                }
                return SkuMap.ToModel(sku);
            }

            return null;
        }

        private static SKU ToEntity(SKU Sku, SkuModel model)
        {

            Sku.ActiveInd = model.IsActive;
            Sku.Custom1 = model.Custom1;
            Sku.Custom2 = model.Custom2;
            Sku.Custom3 = model.Custom3;
            Sku.DisplayOrder = model.DisplayOrder;
            Sku.ExternalID = model.ExternalId;
            Sku.FinalPrice = model.FinalPrice;
            Sku.ImageAltTag = model.ImageAltTag;
            Sku.Note = model.Note;
            Sku.ProductID = model.ProductId;
            Sku.ProductPrice = model.ProductPrice;
            Sku.PromotionPrice = model.PromotionPrice;
            Sku.RecurringBillingFrequency = model.RecurringBillingFrequency;
            Sku.RecurringBillingInitialAmount = model.RecurringBillingInitialAmount;
            Sku.RecurringBillingPeriod = model.RecurringBillingPeriod;
            Sku.RecurringBillingTotalCycles = model.RecurringBillingTotalCycles;
            Sku.RetailPriceOverride = model.RetailPriceOverride;
            Sku.SalePriceOverride = model.SalePriceOverride;
            Sku.SKU = model.Sku;
            Sku.SKUID = model.SkuId;
            Sku.SKUPicturePath = model.ImageFile;
            Sku.SupplierID = model.SupplierId;
            Sku.UpdateDte = model.UpdateDate;
            Sku.WebServiceDownloadDte = model.WebServiceDownloadDate;
            Sku.WeightAdditional = model.WeightAdditional;
            Sku.WholesalePriceOverride = model.WholesalePriceOverride;

            return Sku;
        }

        public SkuModel UpdateSku(int skuId, SkuModel model)
        {
            model.Error = "100";
            if (skuId < 1)
            {
                throw new Exception("SKU ID cannot be less than 1.");
            }

            if (Equals(model,null))
            {
                throw new Exception("SKU model cannot be null.");
            }

            // First check inventory is exist
            if (IsSkuExist(model.Sku, model.ProductId))
            {
                model.Error = "101";
                return model;
            }

            // To check SKU combination is already exists.
            if (!String.IsNullOrEmpty(model.AttributesIds))
            {
                if (CheckSKUAttributesCombination(model.ProductId, model.SkuId, model.AttributesIds))
                {
                    model.Error = "102";
                    return model;
                }
                else
                {
                    SKUHelper skuService = new SKUHelper();
                    skuService.AddSkuAttribute(model.SkuId, model.AttributesIds);
                }
            }

            var sku = _skuRepository.GetBySKUID(skuId);
            if (sku != null)
            {
                string skuValue = sku.SKU;
                // Set the sku ID and map
                model.SkuId = skuId;
                var skuToUpdate = ToEntity(sku ,model);

                int quantity = !Equals(model.Inventory, null) && !Equals(model.Inventory.QuantityOnHand, null) ? model.Inventory.QuantityOnHand.Value : 999;
                int reorderLevel = !Equals(model.Inventory, null) && !Equals(model.Inventory.ReorderLevel, null) ? model.Inventory.ReorderLevel.Value : 0;
                
                //To get inventory
                var inventory = _skuInventoryRepository.GetBySKU(skuValue);

                //To update new inventory
                if (!Equals(inventory, null))
                {
                    inventory.SKU = model.Sku;
                    inventory.QuantityOnHand = quantity;
                    inventory.ReOrderLevel = reorderLevel;
                    sku.SKU = model.Sku;
                    SKUAdmin.UpdateQuantity(sku, quantity, reorderLevel);
                }
                else
                {
                    var entity = new SKUInventory();

                    entity.SKU = model.Sku;
                    entity.QuantityOnHand = quantity;
                    entity.ReOrderLevel = reorderLevel;
                    _inventoryRepository.Save(entity);
                }

                if (!string.IsNullOrEmpty(model.UserName))
                {
                    AccountInfoModel accountInforModel = new AccountInfoModel();
                    AccountHelper accountHelper = new AccountHelper();
                    DataSet accountDataSet = accountHelper.GetAccountInfo(model.UserName);
                    if (!Equals(accountDataSet.Tables[0], null) && accountDataSet.Tables[0].Rows.Count > 0)
                    {
                        accountInforModel = accountDataSet.Tables[0].Rows[0].ToSingleRow<AccountInfoModel>();
                        if (!Equals(accountInforModel, null) && (Equals(accountInforModel.RoleName, Constant.RoleFranchise) || Equals(accountInforModel.RoleName, Constant.RoleVendor)))
                        {
                            ProductAdmin prodAdminAccess = new ProductAdmin();
                            Product product = prodAdminAccess.GetByProductId(model.ProductId);
                            product.ReviewStateID = prodAdminAccess.UpdateReviewStateID(skuToUpdate, GetAuthorizedPortalId(model.UserName), model.ProductId);
                            prodAdminAccess.Update(product);
                            prodAdminAccess = null;
                        }
                    }
                }

                // Set update date for web service download
                skuToUpdate.UpdateDte = System.DateTime.Now;

                // Update Product SKU
                SKUAdmin skuAdmin = new SKUAdmin();
                bool check = skuAdmin.Update(skuToUpdate);
                model.Error = string.Empty;

                return model;
            }

            return null;
        }

        public bool DeleteSku(int skuId)
        {
            if (skuId < 1)
            {
                throw new Exception("SKU ID cannot be less than 1.");
            }

            //Get product type id from SKUId
            SKU sku = _skuRepository.GetBySKUID(skuId);

            if (!Equals(sku, null))
            {
                SKUAdmin skuAdmin = new SKUAdmin();
                TList<SKU> skuList = skuAdmin.GetByProductID(sku.ProductID);
                if (skuList.Count.Equals(1))
                {
                    throw new Exception("You are trying to delete the default SKU. The default SKU cannot be deleted.");
                }
                else
                {
                    //check if we have access to delete the SKU
                    bool check = skuAdmin.Delete(skuId);
                    if (check)
                    {
                        skuAdmin.DeleteBySKUId(skuId);
                        return true;
                    }
                    else
                    {
                        throw new Exception("You are trying to delete the default SKU. The default SKU cannot be deleted.");
                    }
                }
            }         

            return false;
        }

        private void GetExpands(NameValueCollection expands, SKU sku)
        {
            if (expands.HasKeys())
            {
                ExpandAttributes(expands, sku);
                ExpandInventory(expands, sku);
                ExpandSupplier(expands, sku);
            }
        }

        private void ExpandAttributes(NameValueCollection expands, SKU sku)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Attributes)))
            {
                var skuAttribs = _skuAttributeRepository.GetBySKUID(sku.SKUID);
                foreach (var sa in skuAttribs)
                {
                    var prodAttrib = _productAttributeRepository.GetByAttributeId(sa.AttributeId);
                    var item = new SKUAttribute
                    {
                        AttributeId = sa.AttributeId,
                        AttributeIdSource = prodAttrib,
                        SKUAttributeID = sa.SKUAttributeID,
                        SKUID = sku.SKUID,
                        SKUIDSource = sku
                    };

                    sku.SKUAttributeCollection.Add(item);
                }
            }
        }

        private void ExpandInventory(NameValueCollection expands, SKU sku)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Inventory)))
            {
                var inventory = _inventoryRepository.GetBySKU(sku.SKU);
                if (inventory != null)
                {
                    sku.Inventory = inventory;
                }
            }
        }

        private void ExpandSupplier(NameValueCollection expands, SKU sku)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Supplier)))
            {
                if (sku.SupplierID.HasValue)
                {
                    var supplier = _supplierRepository.GetBySupplierID(sku.SupplierID.Value);
                    if (supplier != null)
                    {
                        sku.SupplierIDSource = supplier;
                    }
                }
            }
        }

        private void SetFiltering(List<Tuple<string, string, string>> filters, SKUQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;

                if (filterKey.Equals(FilterKeys.ExternalId)) { SetQueryParameter(SKUColumn.ExternalID, filterOperator, filterValue, query); }
                if (filterKey.Equals(FilterKeys.IsActive)) {SetQueryParameter(SKUColumn.ActiveInd, filterOperator, filterValue, query);}
                if (filterKey.Equals(FilterKeys.ProductId)) {SetQueryParameter(SKUColumn.ProductID, filterOperator, filterValue, query);}
                if (filterKey.Equals(FilterKeys.Sku)) {SetQueryParameter(SKUColumn.SKU, filterOperator, filterValue, query);}
                if (filterKey.Equals(FilterKeys.SupplierId)) {SetQueryParameter(SKUColumn.SupplierID, filterOperator, filterValue, query);}
            }
        }

        private void SetSorting(NameValueCollection sorts, SKUSortBuilder sortBuilder)
        {
            if (sorts.HasKeys())
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);

                    if (key.Equals(SortKeys.DisplayOrder)) {SetSortDirection(SKUColumn.DisplayOrder, value, sortBuilder);}
                    if (key.Equals(SortKeys.ProductId))  {SetSortDirection(SKUColumn.ProductID, value, sortBuilder);}
                    if (key.Equals(SortKeys.Sku))  {SetSortDirection(SKUColumn.SKU, value, sortBuilder);}
                    if (key.Equals(SortKeys.SkuId)) { SetSortDirection(SKUColumn.SKUID, value, sortBuilder); }
                }
            }
        }

        private void SetQueryParameter(SKUColumn column, string filterOperator, string filterValue, SKUQuery query)
        {
            base.SetQueryParameter(column, filterOperator, filterValue, query);
        }

        private void SetSortDirection(SKUColumn column, string value, SKUSortBuilder sortBuilder)
        {
            base.SetSortDirection(column, value, sortBuilder);
        }

        /// <summary>
        /// Znode Version 8.0
        /// To Save Sku Attribute in ZnodeSkuAttribute
        /// </summary>
        /// <param name="skuId">int skuId</param>
        /// <param name="attributeIds">string comma separated attributeId</param>
        /// <returns>return true/false</returns>
        public bool AddSkuAttribute(int skuId, string attributeIds)
        {
            if (skuId < 1)
            {
                throw new Exception("SKU ID cannot be less than 1.");
            }
            var skuHelper = new SKUHelper();
            var status = skuHelper.AddSkuAttribute(skuId, attributeIds);
            return status;
        }

        #region Znode Version 8.0

        public SkuListModel GetSKUListByProductId(int productId)
        {
            SkuListModel model = new SkuListModel();
            var productSkus = new TList<SKU>();           

            productSkus = _skuRepository.GetByProductID(productId);

            // Map each item and add to the list
            foreach (var productSku in productSkus)
            {
                model.Skus.Add(SkuMap.ToModel(productSku));
            }
            return model;
        }

        /// <summary>
        /// To check sku is exist
        /// </summary>
        /// <param name="sku">string sku</param>
        /// <param name="productId"> int productId</param>
        /// <returns>returns true/ false</returns>
        private bool IsSkuExist(string sku, int productId)
        {
            SkuAdminRepository _adminSku = new SkuAdminRepository();
            bool isSkuExist = _adminSku.IsSkuExist(sku, productId);
            return isSkuExist;
        }

        /// <summary>
        /// To check SKU combination is already exists.
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <param name="skuId">int skuId</param>
        /// <param name="selectAttributes">string selectAttributes</param>
        /// <returns>returns true/ false</returns>
        private bool CheckSKUAttributesCombination(int productId, int skuId, string selectAttributes)
        {
            SKUHelper skuService = new SKUHelper();
            bool isExist = skuService.GetSKUAttributes(productId, skuId, selectAttributes);

            return isExist;
        }
        #endregion

    }
}
