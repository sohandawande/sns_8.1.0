﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Libraries.Helpers;
using Znode.Libraries.Helpers.Constants;
using Znode.Libraries.Helpers.Extensions;
using ZNode.Libraries.DataAccess.Custom;

namespace Znode.Engine.Services
{
    /// <summary>
    /// Service for Rejection Message.
    /// </summary>
    public class RejectionMessageService : BaseService, IRejectionMessageService
    {
        #region Private Variables
        private RejectionMessageHelper rejectionMessageHelper;
        #endregion

        #region Constructor
        public RejectionMessageService()
        {
            rejectionMessageHelper = new RejectionMessageHelper();
        }
        #endregion

        #region Public Methods

        public RejectionMessageModel GetRejectionMessage(int rejectionMessageId)
        {
            RejectionMessageModel model = new RejectionMessageModel();
            if (rejectionMessageId < 1)
            {
                throw new Exception("Rejection  Id cannot be less than 1.");
            }

            DataSet resultDataSet = rejectionMessageHelper.GetRejectionMessage(rejectionMessageId);

            if (!Equals(resultDataSet, null) && !Equals(resultDataSet.Tables, null) && resultDataSet.Tables.Count > 0 && resultDataSet.Tables[0].Rows.Count > 0)
            {
                model = resultDataSet.Tables[0].Rows[0].ToSingleRow<RejectionMessageModel>();
            }

            return model;
        }

        public RejectionMessageListModel GetRejectionMessages(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            RejectionMessageListModel list = new RejectionMessageListModel();
            List<Tuple<string, string, string>> filterList = new List<Tuple<string, string, string>>();
            int totalRowCount = 0;
            var pagingStart = 0;
            var pagingLength = 0;
            string orderBy = string.Empty;
            string whereClause = string.Empty;
            string innerWhereClause = string.Empty;
            if (!Equals(filters, null))
            {
                foreach (var item in filters)
                {
                    if (Equals(item.Item1, "username"))
                    {
                        filterList.Add(item);
                        innerWhereClause = StringHelper.GenerateDynamicWhereClause(filterList);
                        filters.Remove(item);
                        break;
                    }
                }
            }
            if (!Equals(sorts, null))
            {
                orderBy = StringHelper.GenerateDynamicOrderByClause(sorts);
            }
            if (!Equals(page, null))
            {
                pagingStart = Convert.ToInt32(page.Get(PageKeys.Index));
                pagingLength = Convert.ToInt32(page.Get(PageKeys.Size));
            }
            if (!Equals(filters, null))
            {
                whereClause = StringHelper.GenerateDynamicWhereClause(filters);
            }
            StoredProcedureHelper storedProcedureHelper = new StoredProcedureHelper();

            DataSet resultDataSet = storedProcedureHelper.SP_DataDetailsSpConfig(whereClause, string.Empty, StoredProcedureKeys.SpGetRejectionMessages, orderBy, pagingStart.ToString(), pagingLength.ToString(), out totalRowCount, null, innerWhereClause);
            if (!Equals(resultDataSet, null) && resultDataSet.Tables.Count > 0 && !Equals(resultDataSet.Tables[0], null) && resultDataSet.Tables[0].Rows.Count > 0)
            {
                list.RejectionMessages = resultDataSet.Tables[0].ToList<RejectionMessageModel>().ToCollection();
            }
            list.TotalResults = totalRowCount;
            list.PageSize = pagingLength;
            list.PageIndex = pagingStart;
            // Check to set page size equal to the total number of results
            if (pagingLength.Equals(int.MaxValue))
            {
                list.PageSize = list.TotalResults;
            }
            return list;
        }

        public RejectionMessageModel CreateRejectionMessage(RejectionMessageModel model)
        {
            RejectionMessageModel rejectionMessageModel = new RejectionMessageModel();
            if (Equals(model, null))
            {
                throw new Exception("Rejection Message model cannot be null.");
            }

            if (this.IsMessageKeyAvailable(model.MessageKey))
            {
                throw new Exception("Message key already exist.");
            }

            DataSet resultDataSet = rejectionMessageHelper.CreateUpdateRejectionMessage(model.RejectionMessagesId, model.MessageKey, model.MessageValue, model.PortalId, model.LocaleId, DatabaseOperation.INSERT.ToString());

            if (!Equals(resultDataSet, null) && !Equals(resultDataSet.Tables, null) && resultDataSet.Tables.Count > 0 && resultDataSet.Tables[0].Rows.Count > 0)
            {
                rejectionMessageModel = resultDataSet.Tables[0].Rows[0].ToSingleRow<RejectionMessageModel>();
            }

            return rejectionMessageModel;
        }

        public RejectionMessageModel UpdateRejectionMessage(int rejectionMessageId, RejectionMessageModel model)
        {
            RejectionMessageModel updatedModel = new RejectionMessageModel();
            if (rejectionMessageId < 1)
            {
                throw new Exception("Rejection Message ID cannot be less than 1.");
            }

            if (Equals(model, null))
            {
                throw new Exception("Rejection Message model cannot be null.");
            }

            DataSet resultDataSet = rejectionMessageHelper.CreateUpdateRejectionMessage(model.RejectionMessagesId, model.MessageKey, model.MessageValue, model.PortalId, model.LocaleId, DatabaseOperation.UPDATE.ToString());

            if (!Equals(resultDataSet, null) && !Equals(resultDataSet.Tables, null) && resultDataSet.Tables.Count > 0 && resultDataSet.Tables[0].Rows.Count > 0)
            {
                updatedModel = resultDataSet.Tables[0].Rows[0].ToSingleRow<RejectionMessageModel>();
            }

            return updatedModel;
        }

        public bool DeleteRejectionMessage(int rejectionMessageId)
        {
            if (rejectionMessageId < 1)
            {
                throw new Exception("Gift card ID cannot be less than 1.");
            }

            var rejectionMessage = rejectionMessageHelper.GetRejectionMessage(rejectionMessageId);
            if (!Equals(rejectionMessage, null))
            {
                return rejectionMessageHelper.DeleteRejectionMessage(rejectionMessageId);
            }

            return false;
        }

        #endregion

        #region Private Methods
        private bool IsMessageKeyAvailable(string messageKey)
        {
            RejectionMessageListModel list = this.GetRejectionMessages(null, null, null, null);
            if (!Equals(list, null) && !Equals(list.RejectionMessages, null))
            {
                foreach (RejectionMessageModel item in list.RejectionMessages)
                {
                    if (Equals(item.MessageKey, messageKey))
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        #endregion

        #region Private Enum

        private enum DatabaseOperation
        {
            INSERT,
            UPDATE,
            DELETE,
            GET
        }

        #endregion
    }
}
