﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using ThemeRepository = ZNode.Libraries.DataAccess.Service.ThemeService;

namespace Znode.Engine.Services
{
    public class ThemeService : BaseService, IThemeService
    {
        #region Private Variables
        private readonly ThemeRepository _themeRepository;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor for ThemeService
        /// </summary>
        public ThemeService()
        {
            _themeRepository = new ThemeRepository();
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Gets the list of themes according to filters and sorts.
        /// </summary>
        /// <param name="filters">Filters to be applied.</param>
        /// <param name="sorts">Sorting of the list.</param>
        /// <returns>List of themes</returns>
        public ThemeListModel GetThemes(List<Tuple<string, string, string>> filters, NameValueCollection sorts)
        {
            var model = new ThemeListModel();
            var themes = new TList<Theme>();
            var tempList = new TList<Theme>();


            var query = new ThemeQuery();
            var sortBuilder = new ThemeSortBuilder();

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);

            // Get the initial set           
            tempList = _themeRepository.Find(query, sortBuilder);
            model.TotalResults = tempList.Count();

            // Now go through the list and get any expands
            foreach (var theme in tempList)
            {
                themes.Add(theme);
            }

            // Map each item and add to the list
            foreach (var a in themes)
            {
                model.Themes.Add(ThemeMap.ToModel(a));
            }
            return model;
        }

        public ThemeListModel GetThemes(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            ThemeListModel model = new ThemeListModel();
            TList<Theme> tempList = new TList<Theme>();


            ThemeQuery query = new ThemeQuery();
            ThemeSortBuilder sortBuilder = new ThemeSortBuilder();
            int pagingStart = 0;
            int pagingLength = 0;

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);
            // Get the initial set 
            int totalResults = 0;

            tempList = _themeRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
            model.TotalResults = tempList.Count();

            // Map each item and add to the list
            foreach (Theme theme in tempList)
            {
                model.Themes.Add(ThemeMap.ToModel(theme));
            }
            return model;
        }

        public ThemeModel CreateTheme(ThemeModel model)
        {   
            if (Equals(model,null))
            {
                throw new Exception("Theme model can not be null");
            }

            var entity = ThemeMap.ToEntity(model);

            if (this.IsDuplicateTheme(model))
            {
                throw new Exception("This theme is already exists.");
            }

            var theme = _themeRepository.Save(entity);
            return ThemeMap.ToModel(theme);
        }

        public ThemeModel GetTheme(int themeId)
        {
            var theme = _themeRepository.GetByThemeID(themeId);
            return !Equals(theme, null) ? ThemeMap.ToModel(theme) : null;
        }

        public ThemeModel UpdateTheme(int themeId, ThemeModel model)
        {
            if (themeId < 1)
            {
                throw new Exception("Theme ID cannot be less than 1.");
            }

            if (Equals(model,null))
            {
                throw new Exception("Theme model cannot be null.");
            }
            else if (this.IsDuplicateTheme(model))
            {
                throw new Exception("This theme is already exists.");
            }
            var theme = _themeRepository.GetByThemeID(themeId);
            if (!Equals(theme,null))
            {
                // Set the promotion ID and map
                model.ThemeID = themeId;
                var themeToUpdate = ThemeMap.ToEntity(model);

                var updated = _themeRepository.Update(themeToUpdate);
                if (updated)
                {
                    theme = _themeRepository.GetByThemeID(themeId);
                    return ThemeMap.ToModel(theme);
                }
            }
            return null;
        }

        public bool DeleteTheme(int themeId)
        {
            bool isAssociate = false;
            ThemeHelper themeHelper = new ThemeHelper();
            if (themeId < 1)
            {
                throw new Exception("Theme ID cannot be less than 1.");
            }
            else
            {
                if (!themeHelper.DeleteThemeByID(themeId))
                {                    
                    throw new Exception("An error occurred and the CSS could not be deleted. Please ensure that this CSS  is not in use.");
                }
                else
                {
                    isAssociate = true;
                }
            }
            return isAssociate;
        }

        #endregion

        #region Private Methods
        /// <summary>
        /// Sets the filter on Theme List.
        /// </summary>
        /// <param name="filters">filter tuple containg key, operator and value.</param>
        /// <param name="query">Query releated to filter tuple.</param>
        private void SetFiltering(List<Tuple<string, string, string>> filters, ThemeQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;

                if (filterKey == FilterKeys.ThemeName) SetQueryParameter(ThemeColumn.Name, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.ThemeId) SetQueryParameter(ThemeColumn.ThemeID, filterOperator, filterValue, query);
            }
        }

        /// <summary>
        /// Sorts the Theme List.
        /// </summary>
        /// <param name="sorts">Sort collection consisting of key and directions.</param>
        /// <param name="sortBuilder">Creates sort on the basis of sort collections.</param>
        private void SetSorting(NameValueCollection sorts, ThemeSortBuilder sortBuilder)
        {
            if (sorts.HasKeys())
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);

                    if (key == SortKeys.ThemeId) SetSortDirection(ThemeColumn.ThemeID, value, sortBuilder);
                    if (key == SortKeys.ThemeName) SetSortDirection(ThemeColumn.Name, value, sortBuilder);
                }
            }
        }

        private bool IsDuplicateTheme(ThemeModel model)
        {
            if (!Equals(model,null))
            {
                var query = new ThemeQuery();

                query.AppendEquals(ThemeColumn.Name, model.Name);
                var themes = _themeRepository.Find(query);
                if (!Equals(themes, null) && themes.Count > 0)
                {
                    return true;
                }
            }
            return false;
        }
        #endregion

    }
}
