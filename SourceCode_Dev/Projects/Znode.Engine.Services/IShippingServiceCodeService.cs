﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    /// <summary>
    /// Shipping Service Code Service Interface
    /// </summary>
	public interface IShippingServiceCodeService
	{
        /// <summary>
        /// Get Shipping Service Code on the basis of shipping service code id
        /// </summary>
        /// <param name="shippingServiceCodeId">shippingServiceCodeId</param>
        /// <param name="expands">NameValueCollection expands</param>
        /// <returns>Returns ShippingServiceCodeModel</returns>
		ShippingServiceCodeModel GetShippingServiceCode(int shippingServiceCodeId, NameValueCollection expands);

        /// <summary>
        /// Get list of shipping service codes
        /// </summary>
        /// <param name="expands">NameValueCollection expands</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">NameValueCollection sorts</param>
        /// <param name="page">NameValueCollection page</param>
        /// <returns>Returns ShippingServiceCodeListModel</returns>
		ShippingServiceCodeListModel GetShippingServiceCodes(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Create Shipping Service Code
        /// </summary>
        /// <param name="model">ShippingServiceCodeModel model</param>
        /// <returns>Returns ShippingServiceCodeModel</returns>
		ShippingServiceCodeModel CreateShippingServiceCode(ShippingServiceCodeModel model);

        /// <summary>
        /// Update Shipping Service Code
        /// </summary>
        /// <param name="shippingServiceCodeId">shippingServiceCodeId</param>
        /// <param name="model">ShippingServiceCodeModel model</param>
        /// <returns>Returns ShippingServiceCodeModel</returns>
		ShippingServiceCodeModel UpdateShippingServiceCode(int shippingServiceCodeId, ShippingServiceCodeModel model);

        /// <summary>
        /// Delete Shipping Service Code on the basis of shippingServiceCodeId
        /// </summary>
        /// <param name="shippingServiceCodeId">shippingServiceCodeId</param>
        /// <returns>Returns true/false</returns>
		bool DeleteShippingServiceCode(int shippingServiceCodeId);
	}
}
