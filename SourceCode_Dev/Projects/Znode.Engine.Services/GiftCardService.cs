﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Helpers.Constants;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using GiftCardHistoryRepository = ZNode.Libraries.DataAccess.Service.GiftCardHistoryService;
using GiftCardRepository = ZNode.Libraries.DataAccess.Service.GiftCardService;
using OrderLineItemRepository = ZNode.Libraries.DataAccess.Service.OrderLineItemService;

namespace Znode.Engine.Services
{
    public class GiftCardService : BaseService, IGiftCardService
    {
        private readonly GiftCardRepository _giftCardRepository;
        private readonly GiftCardHistoryRepository _giftCardHistoryRepository;
        private readonly OrderLineItemRepository _orderLineItemRepository;

        public GiftCardService()
        {
            _giftCardRepository = new GiftCardRepository();
            _giftCardHistoryRepository = new GiftCardHistoryRepository();
            _orderLineItemRepository = new OrderLineItemRepository();
        }

        public GiftCardModel GetGiftCard(int giftCardId, NameValueCollection expands)
        {
            var giftCard = _giftCardRepository.GetByGiftCardId(giftCardId);
            if (giftCard != null)
            {
                GetExpands(expands, giftCard);
            }

            return GiftCardMap.ToModel(giftCard);
        }

        public GiftCardListModel GetGiftCards(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new GiftCardListModel();
            var giftCards = new TList<GiftCard>();
            var tempList = new TList<GiftCard>();

            var query = new GiftCardQuery();
            var sortBuilder = new GiftCardSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            tempList = _giftCardRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                model.PageSize = model.TotalResults;
            }

            // Now go through the list and get any expands
            foreach (var giftCard in tempList)
            {
                GetExpands(expands, giftCard);
                giftCards.Add(giftCard);
            }

            // Map each item and add to the list
            foreach (var r in giftCards)
            {
                model.GiftCards.Add(GiftCardMap.ToModel(r));
            }

            return model;
        }

        public GiftCardModel CreateGiftCard(GiftCardModel model)
        {
            if (model == null)
            {
                throw new Exception("Gift card model cannot be null.");
            }

            // Set the create date and generate new card number
            model.CreateDate = DateTime.Now;

            if (string.IsNullOrEmpty(model.CardNumber))
            {
                model.CardNumber = new ZNodeProduct().GetNextGiftCardNumber();
            }

            var entity = GiftCardMap.ToEntity(model);
            var giftCard = _giftCardRepository.Save(entity);
            return GiftCardMap.ToModel(giftCard);
        }

        public GiftCardModel UpdateGiftCard(int giftCardId, GiftCardModel model)
        {
            if (giftCardId < 1)
            {
                throw new Exception("Gift card ID cannot be less than 1.");
            }

            if (model == null)
            {
                throw new Exception("Gift card model cannot be null.");
            }

            var giftCard = _giftCardRepository.GetByGiftCardId(giftCardId);
            if (giftCard != null)
            {
                // Set gift card ID
                model.GiftCardId = giftCardId;

                // Get create date and card number from existing entity
                model.CreateDate = giftCard.CreateDate;
                model.CardNumber = giftCard.CardNumber;

                var giftCardToUpdate = GiftCardMap.ToEntity(model);

                var updated = _giftCardRepository.Update(giftCardToUpdate);
                if (updated)
                {
                    giftCard = _giftCardRepository.GetByGiftCardId(giftCardId);
                    return GiftCardMap.ToModel(giftCard);
                }
            }

            return null;
        }

        public bool DeleteGiftCard(int giftCardId)
        {
            if (giftCardId < 1)
            {
                throw new Exception("Gift card ID cannot be less than 1.");
            }

            var giftCard = _giftCardRepository.GetByGiftCardId(giftCardId);
            if (giftCard != null)
            {
                return _giftCardRepository.Delete(giftCard);
            }

            return false;
        }

        private void GetExpands(NameValueCollection expands, GiftCard giftCard)
        {
            if (expands.HasKeys())
            {
                ExpandGiftCardHistory(expands, giftCard);
                ExpandOrderLineItem(expands, giftCard);
            }
        }

        private void ExpandGiftCardHistory(NameValueCollection expands, GiftCard giftCard)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.GiftCardHistory)))
            {
                var history = _giftCardHistoryRepository.GetByGiftCardID(giftCard.GiftCardId);
                foreach (var h in history)
                {
                    giftCard.GiftCardHistoryCollection.Add(h);
                }
            }
        }

        private void ExpandOrderLineItem(NameValueCollection expands, GiftCard giftCard)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.OrderLineItem)))
            {
                if (giftCard.OrderItemId.HasValue)
                {
                    var orderLineItem = _orderLineItemRepository.GetByOrderLineItemID(giftCard.OrderItemId.Value);
                    if (orderLineItem != null)
                    {
                        giftCard.OrderItemIdSource = orderLineItem;
                    }
                }
            }
        }

        private void SetFiltering(List<Tuple<string, string, string>> filters, GiftCardQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;

                if (filterKey == FilterKeys.GiftCardId) SetQueryParameter(GiftCardColumn.GiftCardId, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.AccountId) SetQueryParameter(GiftCardColumn.AccountId, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.Amount) SetQueryParameter(GiftCardColumn.Amount, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.CardNumber) SetQueryParameter(GiftCardColumn.CardNumber, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.CreateDate) SetQueryParameter(GiftCardColumn.CreateDate, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.CreatedBy) SetQueryParameter(GiftCardColumn.CreatedBy, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.ExpirationDate) SetQueryParameter(GiftCardColumn.ExpirationDate, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.Name) SetQueryParameter(GiftCardColumn.Name, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.OrderLineItemId) SetQueryParameter(GiftCardColumn.OrderItemId, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.PortalId) SetQueryParameter(GiftCardColumn.PortalId, filterOperator, filterValue, query);
            }
        }

        private void SetSorting(NameValueCollection sorts, GiftCardSortBuilder sortBuilder)
        {
            if (sorts.HasKeys())
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);

                    if (value == SortKeys.Amount) SetSortDirection(GiftCardColumn.Amount, sorts[StoredProcedureKeys.sortDirKey], sortBuilder);
                    if (value == SortKeys.CardNumber) SetSortDirection(GiftCardColumn.CardNumber, sorts[StoredProcedureKeys.sortDirKey], sortBuilder);
                    if (value == SortKeys.CreateDate) SetSortDirection(GiftCardColumn.CreateDate, sorts[StoredProcedureKeys.sortDirKey], sortBuilder);
                    if (value == SortKeys.ExpirationDate) SetSortDirection(GiftCardColumn.ExpirationDate, sorts[StoredProcedureKeys.sortDirKey], sortBuilder);
                    if (value == SortKeys.GiftCardId) SetSortDirection(GiftCardColumn.GiftCardId, sorts[StoredProcedureKeys.sortDirKey], sortBuilder);
                    if (value == SortKeys.Name) SetSortDirection(GiftCardColumn.Name, sorts[StoredProcedureKeys.sortDirKey], sortBuilder);

                }
            }
        }

        private void SetQueryParameter(GiftCardColumn column, string filterOperator, string filterValue, GiftCardQuery query)
        {
            base.SetQueryParameter(column, filterOperator, filterValue, query);
        }

        private void SetSortDirection(GiftCardColumn column, string value, GiftCardSortBuilder sortBuilder)
        {
            base.SetSortDirection(column, value, sortBuilder);
        }

        #region Znode Version 8.0
        public GiftCardModel GetNextGiftCardNumber()
        {
            GiftCardModel giftCardNumber = new GiftCardModel();
            giftCardNumber.CardNumber = new ZNodeProduct().GetNextGiftCardNumber();
            return giftCardNumber;
        }

        public bool IsValidGiftCard(string giftCardNumber, int accountId)
        {
            var giftCard = _giftCardRepository.GetByCardNumber(giftCardNumber);

            if (!Equals(giftCard, null))
            {
                // Check for specific account Gift Card.
                if (!Equals(giftCard.AccountId, null) && !Equals(giftCard.AccountId, accountId))
                {
                    return false;
                }
                else
                {
                    var availableBalance = Convert.ToDecimal(giftCard.Amount);
                    if (availableBalance > 0)
                    {
                        // Validate the giftcard expiration date.
                        if (Equals(giftCard.ExpirationDate, null) || giftCard.ExpirationDate.Value < DateTime.Today.Date)
                        {
                            return false;
                        }

                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            else
            {
                return false;
            }
        }
        #endregion
    }
}
