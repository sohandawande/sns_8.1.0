﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    public interface ICSSService
    {
        /// <summary>
        /// Get the list of all csss with pagination
        /// </summary>
        /// <param name="expands">Collection of expands</param>
        /// <param name="filters">List of filter tuples</param>
        /// <param name="sorts">Collection of sorting parameters</param>
        /// <param name="page">Collection of paging parameters</param>
        /// <returns>List of CSSs</returns>
        CSSListModel GetCSSs(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Create new css
        /// </summary>
        /// <param name="model">CSS model</param>
        /// <returns>CSS Model</returns>
        CSSModel CreateCSS(CSSModel model);

        /// <summary>
        /// Get theme by cssId
        /// </summary>
        /// <param name="cssId">Get theme through cssId</param>
        /// <returns>CSS Model</returns>
        CSSModel GetCSS(int cssId);

        /// <summary>
        /// Update the css
        /// </summary>
        /// <param name="cssId">cssId to get theme</param>
        /// <param name="model">CSS Model</param>
        /// <returns>CSS Model</returns>
        CSSModel UpdateCSS(int cssId, CSSModel model);

        /// <summary>
        /// Delete CSS
        /// </summary>
        /// <param name="cssId">CSS ID to delete CSS</param>
        /// <returns>boolean value true/false</returns>
        bool DeleteCSS(int cssId);
    }
}
