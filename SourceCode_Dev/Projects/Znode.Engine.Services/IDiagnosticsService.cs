﻿using Znode.Engine.Api.Models;
namespace Znode.Engine.Services
{
    public interface IDiagnosticsService
    {
        /// <summary>
        /// This method checks SMTP Account
        /// </summary>
        /// <returns>status of SMPT account</returns>
        bool CheckEmailAccount();

        /// <summary>
        /// This method gets Version details of product from database
        /// </summary>
        /// <returns>Returns the version details</returns>
        string GetProductVersionDetails();

        /// <summary>
        /// This method sends the diagnostics email
        /// </summary>
        /// <param name="model">DiagnosticsEmailModel which should contain Case number for diagnostics</param>
        /// <returns>Returns true the email sent status otherwise false</returns>
        bool EmailDiagnostics(DiagnosticsEmailModel model);
    }
}
