﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using CategoryRepository = ZNode.Libraries.DataAccess.Service.CategoryService;
using ProductRepository = ZNode.Libraries.DataAccess.Service.ProductService;
using ProductCategoryRepository = ZNode.Libraries.DataAccess.Service.ProductCategoryService;
using ZNode.Libraries.Admin;

namespace Znode.Engine.Services
{
	public class ProductCategoryService : BaseService, IProductCategoryService
	{
        #region Private variables
        private readonly CategoryRepository _categoryRepository;
        private readonly ProductRepository _productRepository;
        private readonly ProductCategoryRepository _productCategoryRepository; 
        #endregion

        #region Constructor
        public ProductCategoryService()
        {
            _categoryRepository = new CategoryRepository();
            _productRepository = new ProductRepository();
            _productCategoryRepository = new ProductCategoryRepository();
        } 
        #endregion

        #region Public methods
        public ProductCategoryModel GetProductCategory(int productCategoryId, NameValueCollection expands)
		{
			var productCategory = _productCategoryRepository.GetByProductCategoryID(productCategoryId);
			if (productCategory != null)
			{
				GetExpands(expands, productCategory);
			}

			return ProductCategoryMap.ToModel(productCategory);
		}

        public ProductCategoryModel GetProductCategories(int productId, int categoryId, NameValueCollection expands)
        {
            var productCategory = _productCategoryRepository.GetByProductIDCategoryID(productId, categoryId);
            if (!Equals(productCategory,null))
            {
                GetExpands(expands, productCategory);
            }

            return ProductCategoryMap.ToModel(productCategory);
        }

        
        public ProductCategoryListModel GetProductCategories(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new ProductCategoryListModel();
            var productCategories = new TList<ProductCategory>();
            var tempList = new TList<ProductCategory>();

            var query = new ProductCategoryQuery();
            var sortBuilder = new ProductCategorySortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            tempList = _productCategoryRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                model.PageSize = model.TotalResults;
            }

            // Now go through the list and get any expands
            foreach (var productCategory in tempList)
            {
                GetExpands(expands, productCategory);
                productCategories.Add(productCategory);
            }

            // Map each item and add to the list
            foreach (var pc in productCategories)
            {
                model.ProductCategories.Add(ProductCategoryMap.ToModel(pc));
            }

            return model;
        }

        public ProductCategoryModel CreateProductCategory(ProductCategoryModel model)
        {
            ProductCategoryAdmin productcategory = new ProductCategoryAdmin();
            if (model == null)
            {
                throw new Exception("Product category model cannot be null.");
            }

            var entity = ProductCategoryMap.ToEntity(model);

            var productCategory = _productCategoryRepository.Save(entity);
            if (productCategory != null)
            {
                return ProductCategoryMap.ToModel(productCategory);
            }

            return null;
        }

        public ProductCategoryModel UpdateProductCategory(int productCategoryId, ProductCategoryModel model)
        {
            if (productCategoryId < 1)
            {
                throw new Exception("Product category ID cannot be less than 1.");
            }

            if (model == null)
            {
                throw new Exception("Product category model cannot be null.");
            }

            var productCategory = _productCategoryRepository.GetByProductCategoryID(productCategoryId);
            if (productCategory != null)
            {
                // Set the product category ID
                model.ProductCategoryId = productCategoryId;

                var productCategoryToUpdate = ProductCategoryMap.ToEntity(model);

                var updated = _productCategoryRepository.Update(productCategoryToUpdate);
                if (updated)
                {
                    productCategory = _productCategoryRepository.GetByProductCategoryID(productCategoryId);
                    if (productCategory != null)
                    {
                        return ProductCategoryMap.ToModel(productCategory);
                    }
                }
            }

            return null;
        }

        public bool DeleteProductCategory(int productCategoryId)
        {
            if (productCategoryId < 1)
            {
                throw new Exception("Product category ID cannot be less than 1.");
            }

            var productCategory = _productCategoryRepository.GetByProductCategoryID(productCategoryId);
            if (productCategory != null)
            {
                return _productCategoryRepository.Delete(productCategory);
            }

            return false;
        } 
        #endregion

        #region Private Methods
        private void GetExpands(NameValueCollection expands, ProductCategory productCategory)
        {
            if (expands.HasKeys())
            {
                ExpandCategory(expands, productCategory);
                ExpandProduct(expands, productCategory);
            }
        }

        private void ExpandCategory(NameValueCollection expands, ProductCategory productCategory)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Category)))
            {
                var category = _categoryRepository.GetByCategoryID(productCategory.CategoryID);
                if (category != null)
                {
                    productCategory.CategoryIDSource = category;
                }
            }
        }

        private void ExpandProduct(NameValueCollection expands, ProductCategory productCategory)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Product)))
            {
                var product = _productRepository.GetByProductID(productCategory.ProductID);
                if (product != null)
                {
                    productCategory.ProductIDSource = product;
                }
            }
        }

        private void SetFiltering(List<Tuple<string, string, string>> filters, ProductCategoryQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;

                if (filterKey == FilterKeys.BeginDate) SetQueryParameter(ProductCategoryColumn.BeginDate, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.CategoryId) SetQueryParameter(ProductCategoryColumn.CategoryID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.EndDate) SetQueryParameter(ProductCategoryColumn.EndDate, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.IsActive) SetQueryParameter(ProductCategoryColumn.ActiveInd, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.ProductId) SetQueryParameter(ProductCategoryColumn.ProductID, filterOperator, filterValue, query);
            }
        }

        private void SetSorting(NameValueCollection sorts, ProductCategorySortBuilder sortBuilder)
        {
            if (sorts.HasKeys())
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);

                    if (key == SortKeys.CategoryId) SetSortDirection(ProductCategoryColumn.CategoryID, value, sortBuilder);
                    if (key == SortKeys.DisplayOrder) SetSortDirection(ProductCategoryColumn.DisplayOrder, value, sortBuilder);
                    if (key == SortKeys.ProductCategoryId) SetSortDirection(ProductCategoryColumn.ProductCategoryID, value, sortBuilder);
                    if (key == SortKeys.ProductId) SetSortDirection(ProductCategoryColumn.ProductID, value, sortBuilder);
                }
            }
        }

        private void SetQueryParameter(ProductCategoryColumn column, string filterOperator, string filterValue, ProductCategoryQuery query)
        {
            base.SetQueryParameter(column, filterOperator, filterValue, query);
        }

        private void SetSortDirection(ProductCategoryColumn column, string value, ProductCategorySortBuilder sortBuilder)
        {
            base.SetSortDirection(column, value, sortBuilder);
        } 
        #endregion
	}
}
