﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Services.Exceptions
{
    public class SqlProcessException : zUploadException
    {
        public SqlProcessException(string errorMessage)
            : base(errorMessage)
        {
        }
        public SqlProcessException(string errorMessage, System.Exception ex)
            : base(errorMessage, ex)
        {
        }
    }
}
