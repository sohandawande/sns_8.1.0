﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Services.Model;

namespace Znode.Engine.Services.Exceptions
{
    public class FileContentException : zUploadException
    {
        #region constructor

        public FileContentException(List<RecordErrorModel> errorList)
            : base(errorList)
        {
        }

        public FileContentException(string Message, List<RecordErrorModel> errorList)
            : base(Message, errorList)
        {
        }

        public FileContentException(string Message, System.Exception ex)
            : base(Message, ex)
        {
        }
        
        #endregion
    }
}
