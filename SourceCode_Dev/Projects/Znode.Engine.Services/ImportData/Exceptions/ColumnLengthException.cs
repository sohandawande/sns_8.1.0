﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Services.Exceptions
{
    public class ColumnLengthException : zUploadException
    {
        public ColumnLengthException(string errorMessage)
            : base(errorMessage)
        {
        }

        public ColumnLengthException(string errorMessage, System.Exception ex)
            : base(errorMessage, ex)
        {
        }
    }
}
