﻿
namespace Znode.Engine.Services.Exceptions
{
    public class ExcelColumnException : zUploadException
    {
        public ExcelColumnException(string errorMessage)
            : base(errorMessage)
        {
        }

        public ExcelColumnException(string errorMessage, System.Exception ex)
            : base(errorMessage, ex)
        {
        }
    }
}
