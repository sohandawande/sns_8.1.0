﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Services.Exceptions
{
    public class ColumnMismatchException : zUploadException
    {
        public ColumnMismatchException(string errorMessage)
            : base(errorMessage)
        {
        }

        public ColumnMismatchException(string errorMessage, System.Exception ex)
            : base(errorMessage, ex)
        {
        }
    }
}
 