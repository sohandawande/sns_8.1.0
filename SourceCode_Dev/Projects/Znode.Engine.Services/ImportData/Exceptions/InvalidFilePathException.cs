﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Services.Exceptions
{
    public class InvalidFilePathException: zUploadException
    {
        public InvalidFilePathException(string errorMessage)
            : base(errorMessage)
        {
        }

        public InvalidFilePathException(string errorMessage, System.Exception ex)
            : base(errorMessage, ex)
        {
        }
    }
}