﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Services.Exceptions
{
    public class InsufficientDataException: zUploadException
    {
        public InsufficientDataException(string errorMessage)
            : base(errorMessage)
        {
        }

        public InsufficientDataException(string errorMessage, System.Exception ex)
            : base(errorMessage, ex)
        {
        }
    }
}
