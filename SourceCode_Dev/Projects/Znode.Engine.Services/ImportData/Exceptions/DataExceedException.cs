﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Services.Exceptions
{
    public class DataExceedException : zUploadException
    {
        public DataExceedException(string errorMessage)
            : base(errorMessage)
        {
        }

        public DataExceedException(string errorMessage, System.Exception ex)
            : base(errorMessage, ex)
        {
        }
    }
}
