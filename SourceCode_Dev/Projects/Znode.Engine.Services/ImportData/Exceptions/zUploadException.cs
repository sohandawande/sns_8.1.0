﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Services.Model;

namespace Znode.Engine.Services.Exceptions
{
    public class zUploadException: System.Exception
    {
        public List<RecordErrorModel> ErrorList { get; protected set; }

        public zUploadException()
        {
        }

        public zUploadException(string message)
            : base(message)
        {
        }

        public zUploadException(List<RecordErrorModel> ErrorList)
        {
            this.ErrorList = ErrorList;
        }

        public zUploadException(string message, System.Exception ex)
            : base(message, ex)
        {
        }

        public zUploadException(string message, List<RecordErrorModel> ErrorList)
            : base(message)
        {
            this.ErrorList = ErrorList;
        }

    }
}
