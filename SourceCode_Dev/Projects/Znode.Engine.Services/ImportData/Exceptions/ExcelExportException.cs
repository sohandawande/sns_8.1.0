﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Services.Exceptions
{
    public class ExcelExportException: zUploadException
    {
        public ExcelExportException(string errorMessage)
            : base(errorMessage)
        {
        }

        public ExcelExportException(string errorMessage, System.Exception ex)
            : base(errorMessage, ex)
        {
        }
    }
}