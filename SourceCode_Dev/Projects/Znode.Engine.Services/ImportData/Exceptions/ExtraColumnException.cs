﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Services.Exceptions
{
    public class ExtraColumnException : zUploadException
    {
        public ExtraColumnException(string errorMessage)
            : base(errorMessage)
        {
        }

        public ExtraColumnException(string errorMessage, System.Exception ex)
            : base(errorMessage, ex)
        {
        }
    }
}
