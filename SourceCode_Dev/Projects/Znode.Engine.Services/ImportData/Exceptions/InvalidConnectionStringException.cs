﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Services.Exceptions
{
    public class InvalidConnectionStringException: zUploadException
    {
        public InvalidConnectionStringException(string errorMessage)
            : base(errorMessage)
        {
        }

        public InvalidConnectionStringException(string errorMessage, System.Exception ex)
            : base(errorMessage, ex)
        {
        }
    }
}