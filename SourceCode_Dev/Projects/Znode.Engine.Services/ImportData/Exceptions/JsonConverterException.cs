﻿
namespace Znode.Engine.Services.Exceptions
{
    public class JsonConverterException: zUploadException
    {
        public JsonConverterException(string errorMessage)
            : base(errorMessage)
        {
        }

        public JsonConverterException(string errorMessage, System.Exception ex)
            : base(errorMessage, ex)
        {
        }
    }
}
