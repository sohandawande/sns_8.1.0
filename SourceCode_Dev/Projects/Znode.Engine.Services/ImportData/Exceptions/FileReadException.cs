﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Services.Exceptions
{
    public class FileReadException : zUploadException
    {
        public FileReadException(string errorMessage)
            : base(errorMessage)
        {
        }
        public FileReadException(string errorMessage, System.Exception ex)
            : base(errorMessage, ex)
        {
        }
    }
}
