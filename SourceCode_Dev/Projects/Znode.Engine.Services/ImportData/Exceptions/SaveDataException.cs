﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Services.Exceptions
{
    public class SaveDataException : zUploadException
    {
        public SaveDataException(string errorMessage) : base(errorMessage) { }

        public SaveDataException(string errorMessage, System.Exception ex) : base(errorMessage, ex) { }
    }
}
