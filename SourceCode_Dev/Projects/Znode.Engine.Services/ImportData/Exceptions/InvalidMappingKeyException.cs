﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Services.Exceptions
{
    public class InvalidMappingKeyException: zUploadException
    {
        public InvalidMappingKeyException(string errorMessage)
            : base(errorMessage)
        {
        }

        public InvalidMappingKeyException(string errorMessage, System.Exception ex)
            : base(errorMessage, ex)
        {
        }
    }
}