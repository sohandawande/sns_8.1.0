﻿using System.Configuration;

namespace Znode.Engine.Services.Configuration
{
    [ConfigurationCollection(typeof(ExcelSettings))]
    public class ExcelSettingCollection : ConfigurationElementCollection
    {
        #region ConfigurationElementCollection Methods

        /// <summary>
        /// creates a new System.Configuration.ConfigurationElement.
        /// </summary>
        /// <returns>Return ExcelSettings </returns>
        protected override ConfigurationElement CreateNewElement()
        {
            return new ExcelSettings();
        }

        /// <summary>
        /// Gets the element key for a specified configuration element when overridden
        /// </summary>
        /// <returns>Return ExcelSettings element key</returns>
        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((ExcelSettings)element).MappingName;
        }

        #endregion
    }
}