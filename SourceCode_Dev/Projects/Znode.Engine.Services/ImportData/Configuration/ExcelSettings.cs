﻿using System;
using System.Configuration;

namespace Znode.Engine.Services.Configuration
{
    public class ExcelSettings : ConfigurationElement
    {
        #region ConfigurationProperties

        [ConfigurationProperty("MappingName", IsRequired = true, IsKey = true)]
        public String MappingName
        {
            get { return (String)this["MappingName"]; }
            set { this["MappingName"] = value; }
        }
        [ConfigurationProperty("Name")]
        public String Name
        {
            get { return (String)this["Name"]; }
            set { this["Name"] = value; }
        }
        [ConfigurationProperty("Value")]
        public String Value
        {
            get { return (String)this["Value"]; }
            set { this["Value"] = value; }
        }
        [ConfigurationProperty("CustomSPForImport")]
        public String CustomSPForImport
        {
            get { return (String)this["CustomSPForImport"]; }
            set { this["CustomSPForImport"] = value; }
        }
        [ConfigurationProperty("CustomSPForExport")]
        public String CustomSPForExport
        {
            get { return (String)this["CustomSPForExport"]; }
            set { this["CustomSPForExport"] = value; }
        }
        [ConfigurationProperty("CustomSPForEdit")]
        public String CustomSPForEdit
        {
            get { return (String)this["CustomSPForEdit"]; }
            set { this["CustomSPForEdit"] = value; }
        }
        [ConfigurationProperty("MappingId")]
        public int MappingId
        {
            get { return (int)this["MappingId"]; }
            set { this["MappingId"] = value; }
        }
        [ConfigurationProperty("CommandTimeout")]
        public int CommandTimeout
        {
            get { return (int)this["CommandTimeout"]; }
            set { this["CommandTimeout"] = value; }
        }
        [ConfigurationProperty("MaxRowAllowed")]
        public int MaxRowAllowed
        {
            get { return (int)this["MaxRowAllowed"]; }
            set { this["MaxRowAllowed"] = value; }
        }
        [ConfigurationProperty("ChunkSize")]
        public int ChunkSize
        {
            get { return (int)this["ChunkSize"]; }
            set { this["ChunkSize"] = value; }
        }
        [ConfigurationProperty("StopOnError")]
        public bool StopOnError
        {
            get { return (bool)this["StopOnError"]; }
            set { this["StopOnError"] = value; }
        }
        [ConfigurationProperty("ErrorExcelSheet")]
        public bool ErrorExcelSheet
        {
            get { return (bool)this["ErrorExcelSheet"]; }
            set { this["ErrorExcelSheet"] = value; }
        }
        [ConfigurationProperty("ErrorRecordNumber")]
        public bool ErrorRecordNumber
        {
            get { return (bool)this["ErrorRecordNumber"]; }
            set { this["ErrorRecordNumber"] = value; }
        }
        [ConfigurationProperty("InvalidDataSet")]
        public bool InvalidDataSet
        {
            get { return (bool)this["InvalidDataSet"]; }
            set { this["InvalidDataSet"] = value; }
        }
        [ConfigurationProperty("ErrorInformation")]
        public bool ErrorInformation
        {
            get { return (bool)this["ErrorInformation"]; }
            set { this["ErrorInformation"] = value; }
        }
        [ConfigurationProperty("validateDataKeyOnServer")]
        public bool validateDataKeyOnServer
        {
            get { return (bool)this["validateDataKeyOnServer"]; }
            set { this["validateDataKeyOnServer"] = value; }
        }
        [ConfigurationProperty("ExportDataFolderPath")]
        public String ExportDataFolderPath
        {
            get { return (String)this["ExportDataFolderPath"]; }
            set { this["ExportDataFolderPath"] = value; }
        }

        #endregion

        #region ExcelSettings HelperMethod
        /// <summary>
        /// Get Excel Settings defined in web.config file for given key
        /// </summary>
        /// <returns>Return Excel </returns>
        public static ExcelSettings GetExcelSettingsByMappingName(string Key)
        {
            ExcelSettings excelSettings = null;

            ExcelSection excelSection = ConfigurationManager.GetSection("ExcelSection") as ExcelSection;

            if (excelSection != null)
            {
                foreach (ExcelSettings excelSettingsCollection in excelSection.ExcelSettings)
                {
                    if (excelSettingsCollection.MappingName == Key)
                    {
                        excelSettings = excelSettingsCollection;
                        break;
                    }
                }
            }
            return excelSettings;
        }
        #endregion
    }
}