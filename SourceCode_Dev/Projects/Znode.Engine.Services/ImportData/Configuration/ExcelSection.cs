﻿using System;
using System.Configuration;

namespace Znode.Engine.Services.Configuration
{
    public class ExcelSection : ConfigurationSection
    {
        #region ExcelSettings
        [ConfigurationProperty("ExcelSettings", IsDefaultCollection = true)]
        public ExcelSettingCollection ExcelSettings
        {
            get { return (ExcelSettingCollection)this["ExcelSettings"]; }
            set { this["ExcelSettings"] = value; }
        }
        #endregion
    }
}