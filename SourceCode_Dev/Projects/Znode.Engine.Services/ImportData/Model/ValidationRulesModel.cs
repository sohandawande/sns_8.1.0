﻿
namespace Znode.Engine.Services.Model
{
    public class ValidationRulesModel
    {
        public string ColumnName { get; set; }
        public string DetailType { get; set; }
        public string ExcelColumnName { get; set; }
        public string DataType { get; set; }
        public int MinLength { get; set; }
        public int MaxLength { get; set; }
        public bool AllowNull { get; set; }
        public string OtherRequiredField { get; set; }
    }
}
