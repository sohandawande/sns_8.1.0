﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Services.Model
{
    public class RecordErrorModel
    {
        public int RecordNo { get; set; }
        public string TableName { get; set; }
        public string ColumnName { get; set; }
        public string ErrorMessage { get; set; }
        public object Value { get; set; }
    }
}
