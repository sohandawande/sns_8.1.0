﻿using System.ComponentModel.DataAnnotations;

namespace Znode.Engine.Services.Validations
{
    public class zUploadValidation : ValidationAttribute, IzUploadValidation
    {
        public void SetError(string ErrorMessage)
        {
            base.ErrorMessage = ErrorMessage;
        }

        public override bool IsValid(object value)
        {
            return base.IsValid(value);
        }
    }
}
