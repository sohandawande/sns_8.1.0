﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Services.Validations
{
    public interface IzUploadValidation
    {
        /// <summary>
        ///  Validate the value
        /// </summary>
        /// <returns>Returns true or false stating wheather value is valid or not</returns>
        bool IsValid(object value);

        string FormatErrorMessage(string key);
    }
}
