﻿using System;
using Znode.Engine.Services.Model;
using Resources;

namespace Znode.Engine.Services.Validations
{
    public class DatatypeValidation :zUploadValidation
    {
        #region variables

        private ValidationRulesModel ruleModel;

        #endregion

        #region constructor

        public DatatypeValidation(ValidationRulesModel ruleModel)
        {
            this.ruleModel = ruleModel;
            SetError("Invalid datatype.");
        }

        #endregion

        public override bool IsValid(object value)
        {
            RequiredValidation requiredValidation = new RequiredValidation(ruleModel);
            if (!requiredValidation.IsValid(value))
            {
                return true;
            }
            string datatype = ruleModel.DataType.ToLower();
            Type valueDataType = value.GetType();
            if (datatype == DatatypeConstants.bitType)
            {
                return valueDataType == typeof(bool);
            }
            if (datatype == DatatypeConstants.nvarcharType)
            {
                return valueDataType == typeof(string) || valueDataType == typeof(double);
            }
            if (datatype == DatatypeConstants.intType)
            {
                return valueDataType == typeof(double);
            }
            return valueDataType.Name.ToString().ToLower() == datatype.ToLower();
        }
    }
}
