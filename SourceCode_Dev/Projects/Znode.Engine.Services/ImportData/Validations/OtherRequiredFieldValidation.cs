﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Services.Model;
using Resources;

namespace Znode.Engine.Services.Validations
{
    public class OtherRequiredFieldValidation : zUploadValidation
    {
        #region variables
        #endregion

        #region constructor

        public OtherRequiredFieldValidation(ValidationRulesModel ruleModel)
        {
            SetError("Dependent fields are required");
        }

        #endregion

        public override bool IsValid(object value)
        {
            bool status = true;
            //if (!string.IsNullOrEmpty(value.ToString()))
            //{
            //    string[] requiredFieldArray = value.ToString().Split(',');

            //    foreach (string requiredField in requiredFieldArray)
            //    {
            //        if (!ValidateRequiredField(row[requiredField]))
            //        {
            //            status = false;
            //        }
            //    }
            //}
            return status;
        }
    }
}
