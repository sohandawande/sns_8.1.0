﻿
namespace Znode.Engine.Services.Validations
{
    public class DatatypeConstants
    {
        public const string nvarcharType = "nvarchar";
        public const string intType = "int";
        public const string bitType = "bit";
        public const string datetimeType = "datetime";
    }
}
