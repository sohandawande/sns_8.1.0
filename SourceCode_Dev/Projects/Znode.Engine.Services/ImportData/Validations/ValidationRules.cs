﻿using System;
using System.Collections.Generic;

namespace Znode.Engine.Services.Validations
{
    public class ValidationRules
    {
        #region variables

        private string datatype = "DataType";
        private string minLength = "MinLength";
       // private string maxLength = "MaxLength";
        private string allowNull = "AllowNull";
        //private string otherRequiredField = "OtherRequiredField";

        #endregion

        #region Get validation rules

        public Dictionary<string, Type> GetRules()
        {
            Dictionary<string, Type> rules = new Dictionary<string, Type>();
            rules.Add(datatype, typeof(DatatypeValidation));
            rules.Add(minLength, typeof(LengthValidation));
            //rules.Add(maxLength, typeof(LengthValidation));
            rules.Add(allowNull, typeof(RequiredValidation));
            //rules.Add(otherRequiredField, typeof(OtherRequiredFieldValidation));
            return rules;
        }

        #endregion
    }
}
