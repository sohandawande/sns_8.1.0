﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using Znode.Engine.Services.Model;
using Resources;

namespace Znode.Engine.Services.Validations
{
    public class ValidationService
    {
        #region variables

        private List<RecordErrorModel> errorList;
        private ValidationRules rulesService;
        private Dictionary<string, Type> rules;
        private PropertyInfo[] validations;
        private ValidationRulesModel model;
        private string fileName = string.Empty;
        private string isValidMethod = "IsValid";
        private string formatErrorMessageMethod = "FormatErrorMessage";

        #endregion

        #region validate row data

        public List<RecordErrorModel> Validate(string fileName, DataRow row, List<ValidationRulesModel> rulesModelList, string[] columnNamesInFile)
        {
            errorList = new List<RecordErrorModel>();
            this.fileName = fileName;
            CheckForRules(row, rulesModelList, columnNamesInFile);

            return errorList;
        }

        #endregion

        #region Get Error Message

        public string GetErrorMessage(List<RecordErrorModel> errorList)
        {
            string errorMessage = string.Empty;
            foreach (RecordErrorModel error in errorList)
            {
                errorMessage += string.Format("File name:- {0}. Record no.:- {1}. Column Name:- {2}. Error Message:- {3}", error.TableName, error.RecordNo, error.ColumnName, error.ErrorMessage);
            }
            return errorMessage;
        }

        #endregion

        #region Helper methods

        private void CheckForRules(DataRow row, List<ValidationRulesModel> rulesModelList, string[] columnNamesInFile)
        {
            rulesService = new ValidationRules();
            rules = rulesService.GetRules();

                foreach (string columnName in columnNamesInFile)
                {
                    model = rulesModelList.Where(x => x.ExcelColumnName == columnName && x.DetailType=="Column").FirstOrDefault();
                    string[] columnsToExclude = { "ColumnIdentity" };
                    if (Array.IndexOf(columnsToExclude, columnName) <= -1)
                    {
                        ValidateCellData(row[columnName], rules, model, columnName);
                    }
                }
        }

        private void ValidateCellData(object value, Dictionary<string, Type> rules, ValidationRulesModel model, string columnName)
        {
            if (model != null)
            {
                validations = model.GetType().GetProperties();

                foreach (PropertyInfo validation in validations)
                {
                    if (!ValidateData(value, rules, model, columnName, validation))
                    {
                        break;
                    }
                }
            }
        }

        private bool ValidateData(object value, Dictionary<string, Type> rules, ValidationRulesModel model, string columnName, PropertyInfo validation)
        {
            bool status = true;
            if (rules.ContainsKey(validation.Name))
            {
                if (Equals(validation.Name, "AllowNull") && model.AllowNull)
                {   
                    return true;
                }
                object validationInstance = Activator.CreateInstance(rules[validation.Name], model);

                bool valid = (bool)rules[validation.Name].InvokeMember(isValidMethod,
                    BindingFlags.InvokeMethod | BindingFlags.Instance | BindingFlags.Public,
                    null,
                    validationInstance,
                    new Object[] { value });

                string errorMessage = (string)rules[validation.Name].InvokeMember(formatErrorMessageMethod,
                    BindingFlags.InvokeMethod | BindingFlags.Instance | BindingFlags.Public,
                    null,
                    validationInstance,
                    new Object[] { columnName });

                if (!valid)
                {
                    errorList.Add(new RecordErrorModel { TableName = fileName, ColumnName = columnName, ErrorMessage = errorMessage, Value = value });
                    status = false;
                }
            }
            return status;
        }

        
        #endregion
    }
}
