﻿using Znode.Engine.Services.Model;
using Resources;

namespace Znode.Engine.Services.Validations
{
    public class LengthValidation : zUploadValidation
    {
        #region variables

        private ValidationRulesModel ruleModel;

        #endregion

        #region constructor

        public LengthValidation(ValidationRulesModel ruleModel)
        {
            this.ruleModel = ruleModel;
            SetError(string.Format("Length must be between {0}-{1}.", ruleModel.MinLength, ruleModel.MaxLength));
        }

        #endregion

        public override bool IsValid(object value)
        {
            if (value.GetType() != typeof(string))
            {
                return true;
            }
            RequiredValidation requiredValidation = new RequiredValidation(ruleModel);

            if (requiredValidation.IsValid(value))
            {
                if (ruleModel.MaxLength > 0)
                {
                    return !(value.ToString().Length < ruleModel.MinLength || value.ToString().Length > ruleModel.MaxLength);
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return true;
            }
        }
    }
}
