﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Services.Model;
using Resources;

namespace Znode.Engine.Services.Validations
{
    public class RequiredValidation : zUploadValidation
    {
        public RequiredValidation(ValidationRulesModel ruleModel)
        {
            SetError("This field is required.");
        }

        public override bool IsValid(object value)
        {
            value = value.ToString().Trim();
            if (string.IsNullOrEmpty(Convert.ToString(value)))
            {
                return false;
            }
            else
            {
                return true;
            } 
        }
    }
}
