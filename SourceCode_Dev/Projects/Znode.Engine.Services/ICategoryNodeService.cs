﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    public interface ICategoryNodeService
    {
        /// <summary>
        /// Gets the category node by category node id and expands collection.
        /// </summary>
        /// <param name="categoryNodeId">The id of category node</param>
        /// <param name="expands">Expands collection</param>
        /// <returns>Return the model of type CategoryNodeModel.</returns>
        CategoryNodeModel GetCategoryNode(int categoryNodeId, NameValueCollection expands);

        /// <summary>
        /// Gets the list of category nodes.
        /// </summary>
        /// <param name="expands">Expands collection</param>
        /// <param name="filters">Filter collection</param>
        /// <param name="sorts">Sort collection</param>
        /// <param name="page">Page collection</param>
        /// <returns>Return the model of type CategoryNodeListModel.</returns>
        CategoryNodeListModel GetCategoryNodes(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Creates the category node.
        /// </summary>
        /// <param name="categoryNodeModel">The model of type CategoryNodeModel</param>
        /// <returns>Returns model of type CategoryNodeModel</returns>
        CategoryNodeModel CreateCategoryNode(CategoryNodeModel categoryNodeModel);

        /// <summary>
        /// Updates category node.
        /// </summary>
        /// <param name="categoryNodeId">The id of category node</param>
        /// <param name="categoryNodeModel">The model of type CategoryNodeModel</param>
        /// <returns>Returns model of type CategoryNodeModel</returns>
        CategoryNodeModel UpdateCategoryNode(int categoryNodeId, CategoryNodeModel categoryNodeModel);

        /// <summary>
        /// Deletes the category node.
        /// </summary>
        /// <param name="categoryNodeId">The id of category node</param>
        /// <returns>Returns true or false.</returns>
        bool DeleteCategoryNode(int categoryNodeId);

        /// <summary>
        /// Checks child associated categories and deletes category node accordingly.
        /// </summary>
        /// <param name="categoryId">The id of category</param>
        /// <returns>Returns true or false.</returns>
        bool DeleteAssociatedCategoryNode(int categoryId);

        /// <summary>
        /// Gets parent category nodes for specific catalog.
        /// </summary>
        /// <param name="catalogId">The id of catalog</param>
        /// <param name="filters">Filter Collection</param>
        /// <param name="sorts">Sort Collection</param>
        /// <param name="pageIndex">Index of page</param>
        /// <param name="pageSize">Size of page</param>
        CategoryNodeListModel GetParentCategoryNodes(int catalogId, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
        
        /// <summary>
        /// Get BreadCrumb
        /// </summary>
        /// <param name="catrgoryId">The id of Catrgory</param>
        /// <param name="SEOTitle">SEOTitle</param>
        /// <param name="isProductPresent">isProductPresent</param>
        /// <param name="catalogId">Catalog Id for whick category hierarchy will be retrieved.</param>
        /// <returns>Returns model of type BreadCrumbModel</returns>
        BreadCrumbModel GetBreadCrumb(int catrgoryId, string SEOTitle, bool isProductPresent, int catalogId);
    }
}
