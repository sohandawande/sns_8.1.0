﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services
{
    public interface IImportExportService
    {
        /// <summary>
        /// To Get the Export Details.
        /// </summary>
        /// <param name="model">Model of the Export Model</param>
        /// <returns>Return the Export Details in ExportModel format</returns>
        ExportModel GetExports(ExportModel model);

        /// <summary>
        /// To upload the Import Details.
        /// </summary>
        /// <param name="model">Model of the Import Model</param>
        /// <returns>Return the Import Details in ImportModel format</returns>
        ImportModel ImportData(ImportModel model);

        /// <summary>
        /// To Get Import Type Details based on template type.
        /// </summary>
        /// <param name="type">Type of the import Template</param>
        /// <returns>Return Import Type Details in ImportModel Format</returns>
        ImportModel GetImportDetails(string type);
    }
}
