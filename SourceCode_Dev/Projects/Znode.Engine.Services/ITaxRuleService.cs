﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    /// <summary>
    /// Tax Rule Service Interface
    /// </summary>
	public interface ITaxRuleService
	{
        /// <summary>
        /// Get Tax Rule on the basis of tax rule id
        /// </summary>
        /// <param name="taxRuleId">Tax rule id</param>
        /// <param name="expands">expands</param>
        /// <returns>Returns Tax Rule</returns>
		TaxRuleModel GetTaxRule(int taxRuleId, NameValueCollection expands);

        /// <summary>
        /// Gets list of Tax Rules
        /// </summary>
        /// <param name="expands">expands</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <param name="page">page</param>
        /// <returns>Returns list of tax rules</returns>
		TaxRuleListModel GetTaxRules(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Create tax rule
        /// </summary>
        /// <param name="model">Tax Rule Model</param>
        /// <returns>Returns created tax rule</returns>
		TaxRuleModel CreateTaxRule(TaxRuleModel model);

        /// <summary>
        /// Updates Tax Rule on the basis of tax rule id
        /// </summary>
        /// <param name="taxRuleId">Tax rule id</param>
        /// <param name="model">Tax Rule Model</param>
        /// <returns>Returns updated tax rule</returns>
		TaxRuleModel UpdateTaxRule(int taxRuleId, TaxRuleModel model);

        /// <summary>
        /// Delete Tax Rule on the basis of tax rule id
        /// </summary>
        /// <param name="taxRuleId">Tax rule id</param>
        /// <returns>Returns true/false</returns>
		bool DeleteTaxRule(int taxRuleId);
	}
}
