﻿using System;
using System.Configuration;
using System.IO;
using System.Web;
using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Services
{
    public class DiagnosticsService : IDiagnosticsService
    {
        /// <summary>
        /// This method checks SMTP Account
        /// </summary>
        /// <returns>status of SMPT account</returns>
        public bool CheckEmailAccount()
        {
            try
            {
                ZNodeEmailBase.SendEmail("noreply@WebApp.com", "noreply@WebApp.com", string.Empty, "Diagnostic Test", "SMTP Account Test", false);
                return true;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// This method gets Version details of product from database
        /// </summary>
        /// <returns>Returns the version details</returns>
        public string GetProductVersionDetails()
        {
            try
            {
                MultifrontService storefrontservice = new MultifrontService();
                string productVersion = string.Empty;
                TList<Multifront> baseversion = storefrontservice.GetAll();
                if (baseversion.Count > 0)
                {
                    baseversion.Sort("ID Asc");
                    productVersion = string.Format(" Base Version: v{0}.{1}.{2}", baseversion[0].MajorVersion.ToString(), baseversion[0].MinorVersion.ToString(), baseversion[0].Build.ToString());
                }
                //ActivityLogService versions = new ActivityLogService();
                //int logTypeId = 100; // Activity log type id 100 is for patches
                //TList<ActivityLog> versionlist = versions.GetByActivityLogTypeID(logTypeId);
                //versionlist.Sort("ActivityLogID Asc");

                //if (versionlist.Count > 0)
                //{
                //    VersionDetails.DataSource = versionlist;
                //    VersionDetails.DataBind();
                //}
                return productVersion;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// This method sends the diagnostics email
        /// </summary>
        /// <param name="model">DiagnosticsEmailModel which should contain Case number for diagnostics</param>
        /// <returns>Returns true the email sent status otherwise false</returns>
        public bool EmailDiagnostics(DiagnosticsEmailModel model)
        {
            try
            {
                string senderEmail = ZNodeConfigManager.SiteConfig.CustomerServiceEmail;
                string recepientEmail = Convert.ToString(ConfigurationManager.AppSettings["ZnodeSupportEmail"]);
                string subject = string.Format("Diagnostics for Case {0}", model.CaseNumber);

                //// If the ZnodeLog.txt file exists send it along with the diagnostics page
                string filepath = string.Concat(ZNodeConfigManager.EnvironmentConfig.DataPath , "/Logs//ZNodeLog.txt");
                if (File.Exists(HttpContext.Current.Server.MapPath(filepath)))
                {
                    string logfile = string.Empty;
                    FileInfo file = new FileInfo(HttpContext.Current.Server.MapPath(filepath));

                    // If we have a small ZNodeLog.txt file under 250kb send it in, otherwise just send diagnostics
                    if (file.Length < 250000)
                    {
                        StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath(filepath));
                        logfile = reader.ReadToEnd();
                        reader.Close();
                        ZNodeEmail.SendEmail(recepientEmail, senderEmail, senderEmail, subject, logfile, false);
                    }
                }
                ZNodeEmail.SendEmail(recepientEmail, senderEmail, senderEmail, subject, model.MergedText, true);
                return true;
            }
            catch
            {
                throw;
            }
        }
    }
}
