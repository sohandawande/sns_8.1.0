﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Helpers;
using Znode.Libraries.Helpers.Constants;
using Znode.Libraries.Helpers.Extensions;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;
using AccountProfileRepositry = ZNode.Libraries.DataAccess.Service.AccountProfileService;
using AccountRepository = ZNode.Libraries.DataAccess.Service.AccountService;
using AddressRepositry = ZNode.Libraries.DataAccess.Service.AddressService;
using ReferralCommissionTypeRepositry = ZNode.Libraries.DataAccess.Service.ReferralCommissionTypeService;
using ReferralCommissionRepositry = ZNode.Libraries.DataAccess.Service.ReferralCommissionService;

namespace Znode.Engine.Services
{
    public partial class CustomerService : BaseService, ICustomerService
    {
        #region Private Variables
        private readonly AccountProfileRepositry _accountProfileRepository;
        private readonly AccountRepository _accountRepository;
        private readonly AddressRepositry _addressRepository;
        private readonly ReferralCommissionTypeRepositry _referralCommissionTypeRepository;
        private readonly ReferralCommissionRepositry _referralCommissionRepository;
        #endregion

        #region Constructor
        public CustomerService()
        {
            _accountProfileRepository = new AccountProfileRepositry();
            _accountRepository = new AccountRepository();
            _addressRepository = new AddressRepositry();
            _referralCommissionTypeRepository = new ReferralCommissionTypeRepositry();
            _referralCommissionRepository = new ReferralCommissionRepositry();
        }
        #endregion

        public AddressListModel AddCustomer(AddressListModel listModel)
        {
            ProfileAdmin prfAdmin = new ProfileAdmin();
            Profile profile = prfAdmin.GetDefaultProfileByPortalID(listModel.PortalId);

            int profileId = 0;

            if (!Equals(profile, null))
            {
                profileId = profile.ProfileID;
            }
            else
            {
                throw new Exception("Profile cannot be null.");
            }

            AddressModel shippingAddressModel = listModel.Addresses[0];
            AddressModel billingAddressModel = listModel.Addresses[1];

            ZNodeUserAccount _userAccount = new ZNodeUserAccount();
            _userAccount.EmailID = listModel.Email;
            _userAccount.BillingAddress = AddressMap.ToEntity(billingAddressModel);
            _userAccount.ShippingAddress = AddressMap.ToEntity(shippingAddressModel);

            Account account = new Account();
            account.ActiveInd = true;
            account.CreateDte = System.DateTime.Now;
            account.UpdateDte = System.DateTime.Now;
            account.ProfileID = profile.ProfileID;
            account.Email = listModel.Email;

            if (profile.DefaultExternalAccountNo != null)
            {
                account.ExternalAccountNo = profile.DefaultExternalAccountNo;
            }

            account.ParentAccountID = null;
            ZNode.Libraries.DataAccess.Service.AccountService acctServ = new ZNode.Libraries.DataAccess.Service.AccountService();
            acctServ.Insert(account);

            ZNode.Libraries.DataAccess.Service.AddressService addressService = new ZNode.Libraries.DataAccess.Service.AddressService();

            if (listModel.IsSameAsBillingAddress)
            {
                _userAccount.BillingAddress.Name = "Default Address";
                _userAccount.BillingAddress.IsDefaultBilling = true;
                _userAccount.BillingAddress.IsDefaultShipping = true;
                _userAccount.BillingAddress.AccountID = account.AccountID;
                listModel.Addresses[0] = AddressMap.ToModel(addressService.Save(_userAccount.BillingAddress));
            }
            else
            {
                _userAccount.BillingAddress.Name = "Default Billing Address";
                _userAccount.BillingAddress.IsDefaultBilling = true;
                _userAccount.BillingAddress.AccountID = account.AccountID;
                listModel.Addresses[0] = AddressMap.ToModel(addressService.Save(_userAccount.BillingAddress));

                _userAccount.ShippingAddress.Name = "Default Shipping Address";
                _userAccount.ShippingAddress.IsDefaultShipping = true;
                _userAccount.ShippingAddress.AccountID = account.AccountID;
                listModel.Addresses[1] = AddressMap.ToModel(addressService.Save(_userAccount.ShippingAddress));
            }

            AccountProfile accountProfile = new AccountProfile();
            accountProfile.AccountID = account.AccountID;
            accountProfile.ProfileID = profileId;

            AccountProfileService serv = new AccountProfileService();
            serv.Insert(accountProfile);

            return listModel;
        }

        /// <summary>
        /// Gets the Turnkey Store Profile ID
        /// </summary>
        private int GetTurnkeyStoreProfileID(string userName)
        {
            PortalProfileService postalProfileService = new PortalProfileService();
            string storeList = GetAvailablePortals(userName);
            if (string.Compare(storeList, "0") == 0)
            {
                return 0;
            }
            string[] stores = storeList.Split(',');
            if (string.IsNullOrEmpty(storeList.Trim()))
            {
                return 0;
            }
            TList<PortalProfile> profiles = postalProfileService.GetByPortalID(Convert.ToInt32(stores[0]));
            if (profiles != null && profiles.Count > 0)
            {
                return profiles[0].ProfileID;
            }
            return 0;
        }

        public CustomerListModel GetCustomerList(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            List<Tuple<string, string, string>> filterList = new List<Tuple<string, string, string>>();
            CustomerListModel list = new CustomerListModel();
            string innerWhereClause = string.Empty;

            foreach (var item in filters)
            {
                if (Equals(item.Item1, "username"))
                {
                    filterList.Add(item);
                    innerWhereClause = StringHelper.GenerateDynamicWhereClause(filterList);
                    filters.Remove(item);
                    break;
                }
            }

            string whereClause = StringHelper.GenerateDynamicWhereClause(filters);

            int totalRowCount = 0;
            var pagingStart = 0;
            var pagingLength = 0;
            string orderBy = StringHelper.GenerateDynamicOrderByClause(sorts);

            pagingStart = Convert.ToInt32(page.Get(PageKeys.Index));
            pagingLength = Convert.ToInt32(page.Get(PageKeys.Size));

            StoredProcedureHelper storedProcedureHelper = new StoredProcedureHelper();
            DataSet resultDataSet = storedProcedureHelper.SP_DataDetailsSpConfig(whereClause, string.Empty, StoredProcedureKeys.SpGetCustomerList, orderBy, pagingStart.ToString(), pagingLength.ToString(), out totalRowCount, null, innerWhereClause);

            list.CustomerList = resultDataSet.Tables[0].ToList<CustomerModel>().ToCollection();
            list.TotalResults = totalRowCount;
            list.PageSize = pagingLength;
            list.PageIndex = pagingStart;
            // Check to set page size equal to the total number of results
            if (Equals(pagingLength, int.MaxValue))
            {
                list.PageSize = list.TotalResults;
            }

            return list;
        }

        #region Customer Affiliate
        public CustomerAffiliateModel GetCustomerAffiliate(int accountId, NameValueCollection expands)
        {
            AccountService accountService = new AccountService();
            CustomerAffiliateModel customerAffiliateModel = new CustomerAffiliateModel();
            var helperAccess = new AccountHelper();
            AccountModel account = new AccountModel();
            account = accountService.GetAccount(accountId, expands);

            var accountProfile = new TList<AccountProfile>();
            var portalProfile = new TList<PortalProfile>();
            var domainURL = new TList<Domain>();

            string domainName = string.Empty;

            accountProfile = _accountProfileRepository.GetByAccountID(accountId);

            if (!Equals(accountProfile, null) && accountProfile.Any())
            {
                var portalProfileService = new PortalProfileService();
                portalProfile = portalProfileService.GetByProfileID(accountProfile[0].ProfileID.GetValueOrDefault(0));
            }

            if (!Equals(portalProfile, null) && portalProfile.Any())
            {
                var domainAdmin = new DomainAdmin();
                domainURL = domainAdmin.GetDomainByPortalID(portalProfile[0].PortalID);
            }

            if (!Equals(domainURL, null) && domainURL.Any())
            {
                domainName = "http://" + domainURL.FirstOrDefault().DomainName;
                customerAffiliateModel.AffiliateLink = domainName;
            }

            if (!Equals(customerAffiliateModel.AffiliateLink, null))
            {
                account = accountService.GetAccount(accountId, expands);
                customerAffiliateModel.AccountInformation = account;
            }

            customerAffiliateModel.AccountInformation = account;

            var commisionAmountDataSet = helperAccess.GetCommisionAmount(ZNodeConfigManager.SiteConfig.PortalID, accountId.ToString());

            if (!Equals(commisionAmountDataSet, null) && commisionAmountDataSet.Tables.Count > 0 && !Equals(commisionAmountDataSet.Tables[0], null) && commisionAmountDataSet.Tables[0].Rows.Count > 0)
            {
                customerAffiliateModel.AmountOwed = string.IsNullOrEmpty((commisionAmountDataSet.Tables[0].Rows[0]["CommissionOwed"]).ToString()) ? (decimal?)null : Convert.ToDecimal(commisionAmountDataSet.Tables[0].Rows[0]["CommissionOwed"]);
            }

            return customerAffiliateModel;
        }

        public ReferralCommissionTypeListModel GetReferralCommissionTypeList(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new ReferralCommissionTypeListModel();
            var referralCommissionTypes = new TList<ReferralCommissionType>();
            var tempList = new TList<ReferralCommissionType>();

            var query = new ReferralCommissionTypeQuery();
            var sortBuilder = new ReferralCommissionTypeSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;


            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            tempList = _referralCommissionTypeRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                model.PageSize = model.TotalResults;
            }

            // Now go through the list and get any expands
            foreach (var referralCommissionType in tempList)
            {
                referralCommissionTypes.Add(referralCommissionType);
            }

            // Map each item and add to the list
            foreach (var referralCommissionType in referralCommissionTypes)
            {
                model.ReferralCommissionTypes.Add(ReferralCommissionTypeMap.ToModel(referralCommissionType));
            }
            return model;
        }

        public ReferralCommissionListModel GetReferralCommissionList(int accountId, NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new ReferralCommissionListModel();
            var referralCommissionAdmin = new ReferralCommissionAdmin();
            var referralCommissionList = new TList<ReferralCommission>();
            var referralCommissions = new TList<ReferralCommission>();

            // Gets the referral commission details
            if (accountId > 0)
            {
                referralCommissionList = referralCommissionAdmin.GetReferralCommissionByAccountID(accountId);
            }

            foreach (var referralCommission in referralCommissionList)
            {
                GetExpands(expands, referralCommission);
                referralCommissions.Add(referralCommission);
            }

            foreach (var referralCommission in referralCommissions)
            {
                model.ReferralCommissions.Add(ReferralCommissionMap.ToModel(referralCommission));
            }
            return model;
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Get the Expands.
        /// </summary>
        /// <param name="expands">NameValueCollection expands</param>
        /// <param name="referralCommission">ReferralCommission referralCommission</param>
        private void GetExpands(NameValueCollection expands, ReferralCommission referralCommission)
        {
            if (expands.HasKeys())
            {
                ExpandReferralCommissionType(expands, referralCommission);
            }
        }

        /// <summary>
        /// This method gives the expand for referral commission type.
        /// </summary>
        /// <param name="expands">expands</param>
        /// <param name="referralCommission">Referral Commission</param>
        private void ExpandReferralCommissionType(NameValueCollection expands, ReferralCommission referralCommission)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.ReferralCommissionType)) && referralCommission.ReferralCommissionTypeID > 0)
            {
                var referralCommissionType = _referralCommissionTypeRepository.GetByReferralCommissionTypeID(referralCommission.ReferralCommissionTypeID);
                if (!Equals(referralCommissionType, null))
                {
                    referralCommission.ReferralCommissionTypeIDSource = referralCommissionType;
                }
            }
        }
        #endregion
    }
}
