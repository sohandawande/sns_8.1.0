﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using ZNode.Libraries.DataAccess.Entities;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    public interface IFacetGroupService
    {
        /// <summary>
        /// Method Gets facet group details.
        /// </summary>
        /// <param name="facetGroupId"></param>
        /// <param name="expands"></param>
        /// <returns>Return facet group details.</returns>
        FacetGroupModel GetFacetGroup(int facetGroupId, NameValueCollection expands);

        /// <summary>
        /// Method Gets the facet group list.
        /// </summary>
        /// <param name="expands"></param>
        /// <param name="filters"></param>
        /// <param name="sorts"></param>
        /// <param name="page"></param>
        /// <returns>Return facet group list.</returns>
        FacetGroupListModel GetFacetGroups(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Method Gets facet control types list.
        /// </summary>
        /// <param name="expands"></param>
        /// <param name="filters"></param>
        /// <param name="sorts"></param>
        /// <param name="page"></param>
        /// <returns>Return facet control types.</returns>
        FacetControlTypeListModel GetFacetControlTypes(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Method Create the facet group.
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Return facet group model.</returns>
        FacetGroupModel CreateFacetGroup(FacetGroupModel model);

        /// <summary>
        /// Method Update the facet group based on facetgroupId.
        /// </summary>
        /// <param name="facetGroupId"></param>
        /// <param name="model"></param>
        /// <returns>Return updated facet group model.</returns>
        FacetGroupModel UpdateFacetGroup(int facetGroupId, FacetGroupModel model);

        /// <summary>
        /// Method Delete the facet group based on facetgroupId.
        /// </summary>
        /// <param name="facetGroupId"></param>
        /// <returns>Return true or false.</returns>
        bool DeleteFacetGroup(int facetGroupId);

        /// <summary>
        /// Method Creates the Facet group Categories.
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Return the facet group categories.</returns>
        FacetGroupCategoryListModel CreateFacetGroupCategory(FacetGroupCategoryListModel model);

        /// <summary>
        /// Method deletes the facet group categories based on facet group id.
        /// </summary>
        /// <param name="facetGroupId"></param>
        /// <returns>Return true or false</returns>
        bool DeleteFacetGroupCategory(int facetGroupId);

        /// <summary>
        /// Method Create the facet.
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Return facet model</returns>
        FacetModel CreateFacet(FacetModel model);

        /// <summary>
        /// Method Update the facet based on facetId.
        /// </summary>
        /// <param name="facetId"></param>
        /// <param name="model"></param>
        /// <returns>Return updated facet model.</returns>
        FacetModel UpdateFacet(int facetId, FacetModel model);

        /// <summary>
        /// Method Delete the facet based on facetId.
        /// </summary>
        /// <param name="facetId"></param>
        /// <returns>Return true or false.</returns>
        bool DeleteFacet(int facetId);


        /// <summary>
        /// Method Gets facet details.
        /// </summary>
        /// <param name="facetId"></param>
        /// <param name="expands"></param>
        /// <returns>Return facet details.</returns>
        FacetModel GetFacet(int facetId, NameValueCollection expands);


        /// <summary>
        /// Method Gets the facet list.
        /// </summary>
        /// <param name="expands"></param>
        /// <param name="filters"></param>
        /// <param name="sorts"></param>
        /// <param name="page"></param>
        /// <returns>Return facet list.</returns>
        FacetListModel GetFacetList(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

    }
}
