﻿
namespace Znode.Engine.Services
{
    public interface IRotateKeyService
    {
        /// <summary>
        /// Method Generates a Ratation Key
        /// </summary>
        /// <returns>Returns Boolean Value tru/False</returns>
        bool GenerateNewRotateKey();
    }
}
