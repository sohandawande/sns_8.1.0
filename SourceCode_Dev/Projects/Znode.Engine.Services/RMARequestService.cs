﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Text;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Helpers;
using Znode.Libraries.Helpers.Constants;
using Znode.Libraries.Helpers.Extensions;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;
using RMARequestRepository = ZNode.Libraries.DataAccess.Service.RMARequestService;

namespace Znode.Engine.Services
{
    public partial class RMARequestService : BaseService, IRMARequestService
    {
        private readonly RMARequestRepository _rmaRequestRepository;

        public RMARequestService()
        {
            _rmaRequestRepository = new RMARequestRepository();
        }

        public RMARequestModel GetRMARequest(int rmaRequestId)
        {
            RMARequestAdmin rmaRequestAdmin = new RMARequestAdmin();
            RMARequest rmaRequest = rmaRequestAdmin.GetByRMARequestID(rmaRequestId);
            RMARequestModel rmaRequestModel = new RMARequestModel();
            rmaRequestModel = RMARequestMap.ToModel(rmaRequest);
            return rmaRequestModel;
        }

        public bool GetOrderRMAFlag(int orderId)
        {
            if (orderId < 1)
            {
                return false;
            }
            RMAHelper rmaHelper = new RMAHelper();
            return rmaHelper.GetOrderRMAFlag(orderId);
        }

        public RMARequestModel UpdateRMARequest(int rmaRequestId, RMARequestModel model)
        {
            if (rmaRequestId < 1)
            {
                throw new Exception("RMA Request ID cannot be less than 1.");
            }

            if (Equals(model, null))
            {
                throw new Exception("RMA Request model cannot be null.");
            }

            var rmaRequest = _rmaRequestRepository.GetByRMARequestID(rmaRequestId);
            if (rmaRequest != null)
            {
                // Set portal ID
                model.RMARequestID = rmaRequest.RMARequestID;
                model.RequestDate = rmaRequest.RequestDate;

                var rmaRequestToUpdate = RMARequestMap.ToEntity(model);

                var updated = _rmaRequestRepository.Update(rmaRequestToUpdate);
                if (updated)
                {
                    rmaRequest = _rmaRequestRepository.GetByRMARequestID(rmaRequestId);
                    return RMARequestMap.ToModel(rmaRequest);
                }
            }

            return null;
        }

        public RMARequestListModel GetRMARequestList(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            List<Tuple<string, string, string>> filterList = new List<Tuple<string, string, string>>();
            var model = new RMARequestListModel();

            string orderByClause = StringHelper.GenerateDynamicOrderByClause(sorts);
            string innerWhereClause = string.Empty;
            foreach (var item in filters)
            {
                if (Equals(item.Item1, "username"))
                {
                    filterList.Add(item);
                    innerWhereClause = StringHelper.GenerateDynamicWhereClause(filterList);
                    filters.Remove(item);
                    break;
                }
            }

            var pagingStart = 0;
            var pagingLength = 0;
            SetPaging(page, model, out pagingStart, out pagingLength);
            pagingStart = Convert.ToInt32(page.Get(PageKeys.Index));
            string whereClause = StringHelper.GenerateDynamicWhereClause(filters);
            StoredProcedureHelper storedProcedureHelper = new StoredProcedureHelper();
            int totalRowCount = 0;
            DataSet resultDataSet = storedProcedureHelper.SP_DataDetailsSpConfig(whereClause, null, StoredProcedureKeys.SpZNodeSearchRMA, orderByClause, pagingStart.ToString(), pagingLength.ToString(), out totalRowCount, null, innerWhereClause);
            model.RMARequests = resultDataSet.Tables[0].ToList<RMARequestModel>().ToCollection();
            model.TotalResults = totalRowCount;
            model.PageSize = pagingLength;
            model.PageIndex = pagingStart;
            // Check to set page size equal to the total number of results
            if (pagingLength.Equals(int.MaxValue))
            {
                model.PageSize = model.TotalResults;
            }

            return model;
        }

        public bool DeleteRMARequest(int rmaRequestId)
        {
            if (rmaRequestId < 1)
            {
                throw new Exception("RMA request ID cannot be less than 1.");
            }

            var rmaRequest = _rmaRequestRepository.GetByRMARequestID(rmaRequestId);

            if (!Equals(rmaRequest, null))
            {
                return _rmaRequestRepository.Delete(rmaRequest);
            }
            return false;
        }

        public IssuedGiftCardListModel GetRMAGiftCardDetails(int rmaRequestId)
        {
            RMARequestAdmin rmaRequestAdmin = new RMARequestAdmin();
            DataSet giftDS = rmaRequestAdmin.GetGiftCardByRMARequest(rmaRequestId);
            IssuedGiftCardListModel issuedGiftCards = new IssuedGiftCardListModel();
            if (!Equals(giftDS, null) && !Equals(giftDS.Tables[0], null) && giftDS.Tables[0].Rows.Count > 0)
            {
                issuedGiftCards.IssuedGiftCardModels = giftDS.Tables[0].ToList<IssuedGiftCardModel>().ToCollection();
            }
            return issuedGiftCards;
        }

        public RMARequestModel CreateRMARequest(RMARequestModel rmaRequestModel)
        {
            if (Equals(rmaRequestModel, null))
            {
                throw new Exception("RMA request model cannot be null.");
            }

            if (Equals(rmaRequestModel.RMARequestItems, null) || rmaRequestModel.RMARequestItems.Count < 1)
            {
                throw new Exception("RMA request Items model cannot be null.");
            }


            RMARequestAdmin rmaRequestAdmin = new RMARequestAdmin();

            RMARequest rmaRequest = RMARequestMap.ToEntity(rmaRequestModel);
            int rmaRequestID = 0;
            bool status = false;
            status = rmaRequestAdmin.Add(rmaRequest, out rmaRequestID);
            rmaRequestModel.RMARequestID = rmaRequestID;

            if (status && !Equals(rmaRequestModel.RMARequestItems, null) && rmaRequestModel.RMARequestItems.Count > 0)
            {
                foreach (RMARequestItemModel items in rmaRequestModel.RMARequestItems)
                {
                    RMARequestItem item = new RMARequestItem();
                    item.RMARequestID = rmaRequestID;
                    item.OrderLineItemID = items.OrderLineItemID;
                    item.Quantity = Equals(items.Quantity, null) ? 0 : items.Quantity;
                    item.Price = Equals(items.Price, null) ? 0 : Convert.ToDecimal(items.Price);
                    item.ReasonForReturnID = Equals(items.ReasonForReturnId, null) ? 0 : items.ReasonForReturnId;
                    rmaRequestAdmin.Add(item);
                }
            }

            //SendStatusMail(rmaRequestID);
            SendStatusMailExtn(rmaRequestID);

            return rmaRequestModel;
        }

        public bool SendStatusMail(int RMAItemId)
        {
            string smtpServer = ZNodeConfigManager.SiteConfig.SMTPServer;
            string senderEmail = ZNodeConfigManager.SiteConfig.CustomerServiceEmail;
            string subject = ZNodeConfigManager.SiteConfig.StoreName;
            string ReceiverMailId = "";
            RMARequestAdmin rmaAdmin = new RMARequestAdmin();
            DataSet reportds = rmaAdmin.GetRMARequestReport(RMAItemId);
            if (reportds.Tables[0].Rows.Count > 0)
            {
                StringBuilder sign = new StringBuilder();
                if (Equals(reportds.Tables[0].Rows[0]["EnableEmailNotification"].ToString(), "True") && reportds.Tables[0].Rows[0]["CustomerNotification"].ToString().Length > 0)
                {
                    sign.Append("Regards,<br />");
                    sign.Append(ZNodeConfigManager.SiteConfig.StoreName + "<br />");
                    sign.Append(reportds.Tables[0].Rows[0]["DepartmentName"].ToString() + "<br />");
                    sign.Append(reportds.Tables[0].Rows[0]["DepartmentAddress"].ToString() + "<br />");
                    sign.Append(reportds.Tables[0].Rows[0]["DepartmentEmail"].ToString() + "<br />");

                    if (reportds.Tables[0].Rows[0]["DepartmentEmail"].ToString().Length > 0)
                        senderEmail = reportds.Tables[0].Rows[0]["DepartmentEmail"].ToString();
                    ReceiverMailId = reportds.Tables[0].Rows[0]["BillingEmailId"].ToString();

                    StringBuilder messageText = new StringBuilder();

                    messageText.Append("Dear " + reportds.Tables[0].Rows[0]["CustomerName"].ToString() + ", <br />");

                    messageText.Append("<br />");

                    messageText.Append(reportds.Tables[0].Rows[0]["CustomerNotification"].ToString() + "<br />");

                    messageText.Append("<br />");
                    messageText.Append(sign);

                    try
                    {
                        // Send mail
                        ZNode.Libraries.Framework.Business.ZNodeEmail.SendEmail(ReceiverMailId, senderEmail, String.Empty, subject, messageText.ToString(), true);
                        return true;
                    }
                    catch (Exception)
                    {
                        throw new Exception("Unable to send mail. Please check your mail settings.");
                    }
                }
            }
            return false;
        }

        public bool SendMail_Giftcard(GiftCardModel model)
        {
            string smtpServer = ZNodeConfigManager.SiteConfig.SMTPServer;
            string senderEmail = ZNodeConfigManager.SiteConfig.CustomerServiceEmail;
            string subject = ZNodeConfigManager.SiteConfig.StoreName;

            OrderAdmin _OrderAdmin = new OrderAdmin();
            Order orderObject = _OrderAdmin.GetOrderByOrderID(model.OrderId);

            RMAConfigurationAdmin rmaConfigAdmin = new RMAConfigurationAdmin();
            TList<ZNode.Libraries.DataAccess.Entities.RMAConfiguration> rmaConfigs = rmaConfigAdmin.GetAllRMAConfiguration();
            ZNode.Libraries.DataAccess.Entities.RMAConfiguration rmaconfiguration = rmaConfigs[0];

            StringBuilder messageText = new StringBuilder();

            if (rmaconfiguration.EnableEmailNotification == true)
            {
                messageText.Append("Dear " + orderObject.BillingFirstName + " " + orderObject.BillingLastName + ",");
                if (rmaConfigs.Count > 0)
                {
                    messageText.Append("<br /><br />");
                    messageText.Append(rmaConfigs[0].GCNotification);
                    messageText.Append("<br />");
                }

                messageText.Append("<br />");

                messageText.Append("Card Number:" + model.CardNumber + "<br />");
                messageText.Append("Amount:" + model.DisplayAmount + "<br />");
                messageText.Append("Expiration Date:" + model.ExpirationDate.GetValueOrDefault().ToString("MM/dd/yyyy") + "<br />");

                messageText.Append("<br />");
                if (rmaConfigs.Count > 0)
                {

                    if (rmaconfiguration.EnableEmailNotification.HasValue && rmaconfiguration.EnableEmailNotification.Value &&
                        rmaconfiguration.Email.Length > 0)
                    {
                        senderEmail = rmaconfiguration.Email;
                    }
                    messageText.Append("Regards" + "<br />");
                    messageText.Append(ZNodeConfigManager.SiteConfig.StoreName + "<br />");
                    messageText.Append(rmaconfiguration.DisplayName + "<br />");
                    messageText.Append(rmaconfiguration.Address + "<br />");
                    messageText.Append(rmaconfiguration.Email + "<br />");
                }
                try
                {
                    // Send mail
                    ZNode.Libraries.Framework.Business.ZNodeEmail.SendEmail(orderObject.BillingEmailId, senderEmail, String.Empty, subject, messageText.ToString(), true);
                    return true;
                }
                catch (Exception)
                {
                    throw new Exception("Unable to send mail. Please check your mail settings.");
                }
            }
            return false;
        }

    }
}
