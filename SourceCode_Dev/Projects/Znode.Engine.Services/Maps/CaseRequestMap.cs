﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
    public static class CaseRequestMap
    {
        public static CaseRequestModel ToModel(CaseRequest entity)
        {
            if (entity == null)
            {
                return null;
            }

            var model = new CaseRequestModel
            {
                AccountId = entity.AccountID,
                CaseOrigin = entity.CaseOrigin,
                CasePriority = CasePriorityMap.ToModel(entity.CasePriorityIDSource),
                CasePriorityId = entity.CasePriorityID,
                CaseRequestId = entity.CaseID,
                CaseStatus = CaseStatusMap.ToModel(entity.CaseStatusIDSource),
                CaseStatusId = entity.CaseStatusID,
                CaseType = CaseTypeMap.ToModel(entity.CaseTypeIDSource),
                CaseTypeId = entity.CaseTypeID,
                CompanyName = entity.CompanyName,
                CreateDate = entity.CreateDte,
                CreateUser = entity.CreateUser,
                Description = entity.Description,
                Email = entity.EmailID,
                FirstName = entity.FirstName,
                LastName = entity.LastName,
                OwnerAccountId = entity.OwnerAccountID,
                PhoneNumber = entity.PhoneNumber,
                PortalId = entity.PortalID,
                Title = entity.Title
            };

            return model;
        }

        public static CaseRequest ToEntity(CaseRequestModel model)
        {
            if (model == null)
            {
                return null;
            }

            var entity = new CaseRequest
            {
                AccountID = model.AccountId,
                CaseID = model.CaseRequestId,
                CaseOrigin = model.CaseOrigin,
                CasePriorityID = model.CasePriorityId,
                CaseStatusID = model.CaseStatusId,
                CaseTypeID = model.CaseTypeId,
                CompanyName = model.CompanyName,
                CreateDte = model.CreateDate,
                CreateUser = model.CreateUser,
                Description = model.Description,
                EmailID = model.Email,
                FirstName = model.FirstName,
                LastName = model.LastName,
                PhoneNumber = model.PhoneNumber,
                PortalID = model.PortalId,
                OwnerAccountID = model.OwnerAccountId,
                Title = model.Title
            };

            return entity;
        }

        #region Znode Version 8.0

        public static Note ToEntity(NoteModel model)
        {
            if (model == null)
            {
                return null;
            }

            var entity = new Note
            {
                NoteID = model.NoteId,
                AccountID = model.AccountId,
                CaseID = model.CaseId,
                CreateDte = model.CreateDte,
                CreateUser = model.CreateUser,
                NoteTitle = model.NoteTitle,
                NoteBody = model.NoteBody
            };

            return entity;
        }

        public static NoteModel ToModel(Note entity)
        {
            if (entity == null)
            {
                return null;
            }

            var model = new NoteModel
            {
                NoteId = entity.NoteID,
                AccountId = entity.AccountID,
                CaseId = entity.CaseID,
                CreateDte = entity.CreateDte,
                CreateUser = entity.CreateUser,
                NoteTitle = entity.NoteTitle,
                NoteBody = entity.NoteBody
            };

            return model;
        }
        #endregion
    }
}
