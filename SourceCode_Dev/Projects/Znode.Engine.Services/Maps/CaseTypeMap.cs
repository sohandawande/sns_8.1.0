﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
	public static class CaseTypeMap
	{
		public static CaseTypeModel ToModel(CaseType entity)
		{
			if (entity == null)
			{
				return null;
			}

			var model = new CaseTypeModel
			{
				CaseTypeId = entity.CaseTypeID,
				Name = entity.CaseTypeNme
			};

			return model;
		}

		public static CaseType ToEntity(CaseTypeModel model)
		{
			if (model == null)
			{
				return null;
			}

			var entity = new CaseType
			{
				CaseTypeID = model.CaseTypeId,
				CaseTypeNme = model.Name,
			};

			return entity;
		}
	}
}
