﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
	public static class TaxClassMap
	{
		public static TaxClassModel ToModel(TaxClass entity)
		{
			if (entity == null)
			{
				return null;
			}

			var model = new TaxClassModel
			{
				DisplayOrder = entity.DisplayOrder,
				ExternalId = entity.ExternalID,
                ActiveInd = entity.ActiveInd,
				Name = entity.Name,
				PortalId = entity.PortalID,
				TaxClassId = entity.TaxClassID,
                TaxRuleList= TaxRuleMap.ToListModel(entity.TaxRuleCollection),               
			};

			return model;
		}

		public static TaxClass ToEntity(TaxClassModel model)
		{
			if (model == null)
			{
				return null;
			}

			var entity = new TaxClass
			{
                ActiveInd = model.ActiveInd,
				DisplayOrder = model.DisplayOrder,
				ExternalID = model.ExternalId,
				Name = model.Name,
				PortalID = model.PortalId,
				TaxClassID = model.TaxClassId
			};

			return entity;
		}
	}
}
