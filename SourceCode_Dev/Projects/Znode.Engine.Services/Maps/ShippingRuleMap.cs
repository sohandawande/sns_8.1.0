﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
	public static class ShippingRuleMap
	{
		public static ShippingRuleModel ToModel(ShippingRule entity)
		{
			if (entity == null)
			{
				return null;
			}

			var model = new ShippingRuleModel
			{
				BaseCost = entity.BaseCost,
				ClassName = entity.ClassName,
				Custom1 = entity.Custom1,
				Custom2 = entity.Custom2,
				Custom3 = entity.Custom3,
				ExternalId = entity.ExternalID,
				LowerLimit = entity.LowerLimit,
				PerItemCost = entity.PerItemCost,
				ShippingOption = ShippingOptionMap.ToModel(entity.ShippingIDSource),
				ShippingOptionId = entity.ShippingID,
				ShippingRuleId = entity.ShippingRuleID,
				ShippingRuleType = ShippingRuleTypeMap.ToModel(entity.ShippingRuleTypeIDSource),
				ShippingRuleTypeId = entity.ShippingRuleTypeID,
				UpperLimit = entity.UpperLimit
			};

			return model;
		}

		public static ShippingRule ToEntity(ShippingRuleModel model)
		{
			if (model == null)
			{
				return null;
			}

			var entity = new ShippingRule
			{
				BaseCost = model.BaseCost,
				ClassName = model.ClassName,
				Custom1 = model.Custom1,
				Custom2 = model.Custom2,
				Custom3 = model.Custom3,
				ExternalID = model.ExternalId,
				LowerLimit = model.LowerLimit,
				PerItemCost = model.PerItemCost,
				ShippingID = model.ShippingOptionId,
				ShippingRuleID = model.ShippingRuleId,
				ShippingRuleTypeID = model.ShippingRuleTypeId,
				UpperLimit = model.UpperLimit
			};

			return entity;
		}
	}
}
