﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
	public static class MessageTypeMap
	{
		public static MessageTypeModel ToModel(MessageType entity)
		{
			if (entity == null)
			{
				return null;
			}

			var model = new MessageTypeModel
			{
				DisplayOrder = entity.DisplayOrder,
				IsActive = entity.IsActive,
				MessageTypeId = entity.MessageTypeID,
				Name = entity.Name
			};

			return model;
		}

		public static MessageType ToEntity(MessageTypeModel model)
		{
			if (model == null)
			{
				return null;
			}

			var entity = new MessageType
			{
				DisplayOrder = model.DisplayOrder,
				IsActive = model.IsActive,
				MessageTypeID = model.MessageTypeId,
				Name = model.Name
			};

			return entity;
		}
	}
}
