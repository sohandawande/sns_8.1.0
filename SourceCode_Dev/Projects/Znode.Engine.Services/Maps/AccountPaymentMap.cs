﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
    public static class AccountPaymentMap
    {
        /// <summary>
        /// Map AccountPayment Entity to AccountPayment Model
        /// </summary>
        /// <param name="entity">AccountPayment Entity</param>
        /// <returns>Returns AccountPayment Model</returns>
        public static AccountPaymentModel ToModel(AccountPayment entity)
        {
            if (Equals(entity, null))
            {
                return null;
            }

            var model = new AccountPaymentModel
            {
                AccountPaymentId = entity.AccountPaymentID,
                AccountId = entity.AccountID,
                OrderId = entity.OrderID,
                TransactionId = entity.TransactionID,
                PaymentDate = entity.ReceivedDate,
                Description = entity.Description,
                Amount = entity.Amount
            };
            return model;
        }

        /// <summary>
        /// Map AccountPayment Entity to AccountPayment Model
        /// </summary>
        /// <param name="entity">AccountPayment Entity</param>
        /// <returns>Returns AccountPayment Model</returns>
        public static AccountPayment ToEntity(AccountPaymentModel model)
        {
            if (Equals(model, null))
            {
                return null;
            }

            var entity = new AccountPayment
            {
                AccountPaymentID = model.AccountPaymentId,
                AccountID = model.AccountId,
                OrderID = model.OrderId,
                TransactionID = model.TransactionId,
                ReceivedDate = model.PaymentDate,
                Description = model.Description,
                Amount = model.Amount
            };
            return entity;
        }
    }
}
