﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
	public static class AuditStatusMap
	{
		public static AuditStatusModel ToModel(SourceModificationStatus entity)
		{
			if (entity == null)
			{
				return null;
			}

			var model = new AuditStatusModel
			{
				AuditStatusId = entity.Id,
				CreateDate = entity.CreatedDate,
				CreatedBy = entity.CreatedBy,
				Description = entity.StatusDescription,
				UpdateDate = entity.UpdatedDate,
				UpdatedBy = entity.UpdatedBy
			};

			return model;
		}

		public static SourceModificationStatus ToEntity(AuditStatusModel model)
		{
			if (model == null)
			{
				return null;
			}

			var entity = new SourceModificationStatus
			{
				CreatedDate = model.CreateDate,
				CreatedBy = model.CreatedBy,
				Id = model.AuditStatusId,
				StatusDescription = model.Description,
				UpdatedDate = model.UpdateDate,
				UpdatedBy = model.UpdatedBy
			};

			return entity;
		}
	}
}
