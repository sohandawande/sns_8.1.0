﻿using ZNode.Libraries.ECommerce.Entities;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services.Maps
{
	public static class PaymentMap
	{
		public static PaymentModel ToModel(ZNodePayment entity)
		{
			if (entity == null)
			{
				return null;
			}
				
			var model = new PaymentModel
			{
				AuthorizationCode = entity.AuthCode,
				BillingAddress = AddressMap.ToModel(entity.BillingAddress),
				CreditCard = CreditCardMap.ToModel(entity.CreditCard),
				GatewayToken = GatewayTokenMap.ToModel(entity.GatewayToken),
				NonRecurringItemsTotalAmount = entity.NonRecurringItemsTotalAmount,
				PaymentName = entity.PaymentName,
				PaymentOption = PaymentOptionMap.ToModel(entity.PaymentSetting),
				RecurringBillingExists = entity.IsRecurringBillingExists,
				SaveCardData = entity.SaveCardData,
				Secure3D = entity.Is3DSecure,
				SessionId = entity.SessionId,
                SubscriptionId = entity.SubscriptionID,
				TokenId = entity.TokenId,
				TransactionId = entity.TransactionID,
				UseToken = entity.UseToken,
				WorldPayEchoData = entity.WorldPayEchodata,
				WorldPayHeaderCookie = entity.WorldPayHeaderCookie,
				WorldPayPostData = entity.WorldPayPostData
			};

			return model;
		}

		public static ZNodePayment ToZnodePayment(PaymentModel model,AddressModel shippingAddress)
		{
			if (model == null)
			{
				return new ZNode.Libraries.ECommerce.PaymentHelper.ZNodePaymentHelper { BillingAddress = new ZNode.Libraries.DataAccess.Entities.Address { CountryCode = "" } };
			}

			var znodePayment = new ZNode.Libraries.ECommerce.PaymentHelper.ZNodePaymentHelper
			{
				AuthCode = model.AuthorizationCode,
				BillingAddress = AddressMap.ToEntity(model.BillingAddress),
				CreditCard = CreditCardMap.ToEntity(model.CreditCard),
				GatewayToken = GatewayTokenMap.ToEntity(model.GatewayToken),
				Is3DSecure = model.Secure3D,
				IsRecurringBillingExists = model.RecurringBillingExists,
				PaymentName = model.PaymentName,
				PaymentSetting = PaymentOptionMap.ToEntity(model.PaymentOption),
				SaveCardData = model.SaveCardData,
				SessionId = model.SessionId,
                ShippingAddress = AddressMap.ToEntity(shippingAddress),
				SubscriptionID = model.SubscriptionId,
				TokenId = model.TokenId,
				TransactionID = model.TransactionId,
				UseToken = model.UseToken,
				WorldPayEchodata = model.WorldPayEchoData,
				WorldPayHeaderCookie = model.WorldPayHeaderCookie,
				WorldPayPostData = model.WorldPayPostData
			};

			return znodePayment;
		}
	}
}
