﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Services.Maps
{
    public static class PaymentOptionMap
    {

        public static PaymentOptionModel ToModel(PaymentSetting entity)
        {
            ZNodeEncryption encryption = new ZNodeEncryption();

            if (entity == null)
            {
                return null;
            }

            var model = new PaymentOptionModel
            {
                DisplayOrder = entity.DisplayOrder,
                EnableAmericanExpress = entity.EnableAmex,
                EnableDiscover = entity.EnableDiscover,
                EnableMasterCard = entity.EnableMasterCard,
                EnableRecurringPayments = entity.EnableRecurringPayments,
                EnableVault = entity.EnableVault,
                EnableVisa = entity.EnableVisa,
                IsActive = entity.ActiveInd,
                IsRmaCompatible = entity.IsRMACompatible,
                Partner = entity.Partner,
                PaymentGateway = PaymentGatewayMap.ToModel(entity.GatewayTypeIDSource),
                PaymentGatewayId = entity.GatewayTypeID,
                PaymentGatewayPassword = !Equals(entity.GatewayPassword, null) ? encryption.DecryptData(entity.GatewayPassword) : entity.GatewayPassword,
                PaymentGatewayUsername = !Equals(entity.GatewayUsername, null) ? encryption.DecryptData(entity.GatewayUsername) : entity.GatewayUsername,
                PaymentOptionId = entity.PaymentSettingID,
                PaymentType = PaymentTypeMap.ToModel(entity.PaymentTypeIDSource),
                PaymentTypeId = entity.PaymentTypeID,
                PreAuthorize = entity.PreAuthorize,
                ProfileId = entity.ProfileID,
                TestMode = entity.TestMode,
                TransactionKey = !Equals(entity.TransactionKey, null) ? encryption.DecryptData(entity.TransactionKey) : entity.TransactionKey,
                Vendor = !Equals(entity.Vendor, null) ? encryption.DecryptData(entity.Vendor) : entity.Vendor
            };

            return model;
        }

        public static PaymentSetting ToEntity(PaymentOptionModel model)
        {
            ZNodeEncryption encryption = new ZNodeEncryption();
            if (model == null)
            {
                return null;
            }

            var entity = new PaymentSetting
            {
                ActiveInd = model.IsActive,
                DisplayOrder = model.DisplayOrder,
                EnableAmex = model.EnableAmericanExpress,
                EnableDiscover = model.EnableDiscover,
                EnableMasterCard = model.EnableMasterCard,
                EnableRecurringPayments = model.EnableRecurringPayments,
                EnableVault = model.EnableVault,
                EnableVisa = model.EnableVisa,
                GatewayPassword = !Equals(model.PaymentGatewayPassword, null) ? encryption.EncryptData(model.PaymentGatewayPassword) : model.PaymentGatewayPassword,
                GatewayTypeID = model.PaymentGatewayId,
                GatewayUsername = !Equals(model.PaymentGatewayUsername, null) ? encryption.EncryptData(model.PaymentGatewayUsername) : model.PaymentGatewayUsername,
                IsRMACompatible = model.IsRmaCompatible,
                Partner = model.Partner,
                PaymentSettingID = model.PaymentOptionId,
                PaymentTypeID = model.PaymentTypeId,
                PreAuthorize = model.PreAuthorize,
                ProfileID = model.ProfileId,
                TestMode = model.TestMode,
                TransactionKey = !Equals(model.TransactionKey, null) ? encryption.EncryptData(model.TransactionKey) : model.TransactionKey,
                Vendor = !Equals(model.Vendor, null) ? encryption.EncryptData(model.Vendor) : model.Vendor,
            };

            return entity;
        }
    }
}
