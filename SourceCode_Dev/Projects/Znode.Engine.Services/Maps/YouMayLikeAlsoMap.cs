﻿using System;
using System.Data;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services.Maps
{
    public static class YouMayLikeAlsoMap
    {
        /// <summary>
        /// Maps Dataset to list of type AssociatedProductItemModel.
        /// </summary>
        /// <param name="result">Dataset</param>
        /// <returns>Returns mapped list of type AssociatedProductItemModel..</returns>
        public static YouMayLikeAlsoListModel ToAssociatedProductItemModel(DataSet result)
        {
            if (Equals(result, null))
            {
                return null;
            }

            var modelList = new YouMayLikeAlsoListModel();
            foreach (DataRow dr in result.Tables[0].Rows)
            {
                YouMayLikeAlsoModel model = new YouMayLikeAlsoModel
                {
                    ProductId = dr.Table.Columns.Contains("ProductId") ? Convert.ToInt32(dr["ProductId"]) : 0,
                    ProductName = (dr["NAME"] != DBNull.Value) ? dr["NAME"].ToString() : string.Empty,
                    CrossSellProducts = (dr["CrossSellProdName"] != DBNull.Value) ? dr["CrossSellProdName"].ToString() : string.Empty
                };
                modelList.AssociatedProductItems.Add(model);
            }
            return modelList;
        }
    }
}
