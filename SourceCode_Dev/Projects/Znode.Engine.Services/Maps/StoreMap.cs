﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Services.Maps
{
    public static class StoreMap
    {
        public static StoreModel ToModel(Store entity)
        {
            if (entity == null)
            {
                return null;
            }

            var imageUtility = new ZNodeImage();

            var model = new StoreModel
            {
                StoreID = entity.StoreID,
                PortalID = entity.PortalID,
                AccountID = entity.AccountID,
                Name = entity.Name,
                Address1 = entity.Address1,
                Address2 = entity.Address2,
                Address3 = entity.Address3,
                City = entity.City,
                ContactName = entity.ContactName,
                Custom1 = entity.Custom1,
                Custom2 = entity.Custom2,
                Custom3 = entity.Custom3,
                DisplayOrder = entity.DisplayOrder,
                Fax = entity.Fax,
                Phone = entity.Phone,
                State = entity.State,
                Zip = entity.Zip,
                ActiveInd = entity.ActiveInd,
                ImageFile = entity.ImageFile,
                ImageMediumPath = imageUtility.GetImageHttpPathMedium(entity.ImageFile),
                ImageLargePath = imageUtility.GetImageHttpPathLarge(entity.ImageFile),
                ImageSmallPath = imageUtility.GetImageHttpPathSmall(entity.ImageFile),
                ImageSmallThumbnailPath = imageUtility.GetImageHttpPathSmallThumbnail(entity.ImageFile),
                ImageThumbnailPath = imageUtility.GetImageHttpPathThumbnail(entity.ImageFile),
            };

            return model;
        }

        public static Store ToEntity(StoreModel model)
        {
            if (model == null)
            {
                return null;
            }

            var entity = new Store
            {
                StoreID = model.StoreID,
                PortalID = model.PortalID,
                AccountID = model.AccountID,
                Name = model.Name,
                Address1 = model.Address1,
                Address2 = model.Address2,
                Address3 = model.Address3,
                City = model.City,
                ContactName = model.ContactName,
                Custom1 = model.Custom1,
                Custom2 = model.Custom2,
                Custom3 = model.Custom3,
                DisplayOrder = model.DisplayOrder,
                Fax = model.Fax,
                Phone = model.Phone,
                State = model.State,
                Zip = model.Zip,
                ActiveInd = model.ActiveInd,
                ImageFile = model.ImageFile
            };

            return entity;
        }

        /// <summary>
        /// Map ZnodeStore to Store Model
        /// </summary>
        /// <param name="store">ZNodeStore</param>
        /// <returns>Store Model</returns>
        public static StoreModel ToStoreModel(ZNodeStore store)
        {
            if (!Equals(store,null))
            {
                return new StoreModel()
                {
                    Zip = store.Zip,
                    City = store.City,
                    State = store.State,
                    Address1 = store.Address1,
                    Address2 = store.Address2,
                    Address3 = store.Address3,
                    ContactName = store.Name,
                    Phone = store.Phone,
                    Fax = store.Fax,
                    Custom1 = store.Custom1,
                    Custom2 = store.Custom2,
                    Custom3 = store.Custom3,
                    MapQuestURL = store.MapQuestURL
                };    
            }
            else
            {
                return new StoreModel();
            }
        }
    }
}
