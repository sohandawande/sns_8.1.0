﻿using ZNode.Libraries.ECommerce.Entities;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services.Maps
{
	public static class CreditCardMap
	{
		public static CreditCardModel ToModel(CreditCard entity)
		{
			if (entity == null)
			{
				return null;
			}

			var model = new CreditCardModel
			{
				Amount = entity.Amount,
				AuthorizationCode = entity.AuthCode,
				CardDataToken = entity.CardDataToken,
				CardExpiration = entity.CreditCardExp,
				CardHolderName = entity.CardHolderName,
				CardNumber = entity.CardNumber,
				CardSecurityCode = entity.CardSecurityCode,
				CardType = CreditCardTypeMap.ToModel(entity.CreditCardNumberType),
				Description = entity.Description,
				OrderId = entity.OrderID,
				ShippingCharge = entity.ShippingCharge,
				SubTotal = entity.SubTotal,
				TaxCost = entity.TaxCost,
				TransactionId = entity.TransactionID
			};

			return model;
		}

		public static CreditCard ToEntity(CreditCardModel model)
		{
			if (model == null)
			{
				return null;
			}

			var entity = new CreditCard
			{
				Amount = model.Amount,
				AuthCode = model.AuthorizationCode,
				CardDataToken = model.CardDataToken,
				CardHolderName = model.CardHolderName,
				CardNumber = model.CardNumber,
				CardSecurityCode = model.CardSecurityCode,
				CreditCardExp = model.CardExpiration,
				Description = model.Description,
				OrderID = model.OrderId,
				ShippingCharge = model.ShippingCharge,
				SubTotal = model.SubTotal,
				TaxCost = model.TaxCost,
				TransactionID = model.TransactionId
			};

			return entity;
		}
	}
}
