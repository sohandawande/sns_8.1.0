﻿using System;
using System.Collections.Generic;
using System.Web;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.ECommerce.SEO;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;
using Znode.Engine.Api.Models;
using System.Linq;
namespace Znode.Engine.Services.Maps
{
    public static class ProductAlternateImageMap
    {
        /// <summary>
        /// Convert Product image model to Product image entity.
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Product Entity</returns>
        public static ProductImage ToEntity(ProductAlternateImageModel model)
        {
            if (Equals(model, null))
            {
                return null;
            }

            var entity = new ProductImage
            {
                ActiveInd = model.ActiveInd,
                ProductID = model.ProductID,
                AlternateThumbnailImageFile = model.AlternateThumbnailImageFile,
                DisplayOrder = model.DisplayOrder,
                ImageAltTag = model.ImageAltTag,
                ImageFile = model.ImageFile,
                ProductImageTypeID = model.ProductImageTypeID,
                ReviewStateID = model.ReviewStateID,
                ShowOnCategoryPage = model.ShowOnCategoryPage,
                Name=model.Name,
                ProductImageID=model.ProductImageID,
            };

            return entity;
        }

        /// <summary>
        /// Convert Product image entity to product alternate image model.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>Product alternate image model</returns>
        public static ProductAlternateImageModel ToModel(ProductImage entity)
        {
            if (Equals(entity, null))
            {
                return null;
            }
            var model = new ProductAlternateImageModel()
            {
                ActiveInd = entity.ActiveInd,
                ProductID = entity.ProductID,
                AlternateThumbnailImageFile = entity.AlternateThumbnailImageFile,
                DisplayOrder = entity.DisplayOrder,
                ImageAltTag = entity.ImageAltTag,
                ImageFile = entity.ImageFile,
                ProductImageTypeID = entity.ProductImageTypeID,
                ReviewStateID = entity.ReviewStateID,
                ShowOnCategoryPage = entity.ShowOnCategoryPage,
                Name = entity.Name,
                ProductImageID=entity.ProductImageID
            };

            return model;
        }
    }
}
