﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
	public static class PortalCatalogMap
	{
		public static PortalCatalogModel ToModel(PortalCatalog entity)
		{
			if (entity == null)
			{
				return null;
			}

			var model = new PortalCatalogModel
			{
				Catalog = CatalogMap.ToModel(entity.CatalogIDSource),
				CatalogId = entity.CatalogID,
				CssId = entity.CSSID,
				LocaleId = entity.LocaleID,
				PortalCatalogId = entity.PortalCatalogID,
				Portal = PortalMap.ToModel(entity.PortalIDSource),
				PortalId = entity.PortalID,
				ThemeId = entity.ThemeID,
                ThemeName = Equals(entity.ThemeIDSource, null) ? "Default" : entity.ThemeIDSource.Name,
                CSSName = Equals(entity.CSSIDSource, null) ? "Default" : entity.CSSIDSource.Name
			};

			return model;
		}

		public static PortalCatalog ToEntity(PortalCatalogModel model)
		{
			if (model == null)
			{
				return null;
			}

			var entity = new PortalCatalog
			{
				CatalogID = model.CatalogId,
				CSSID = model.CssId,
				LocaleID = model.LocaleId,
				PortalCatalogID = model.PortalCatalogId,
				PortalID = model.PortalId,
				ThemeID = model.ThemeId
			};

			return entity;
		}
	}
}
