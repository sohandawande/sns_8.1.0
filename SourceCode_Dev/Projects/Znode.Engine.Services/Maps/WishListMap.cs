﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
	public static class WishListMap
	{
		public static WishListModel ToModel(WishList entity)
		{
			if (entity == null)
			{
				return null;
			}

			var model = new WishListModel
			{
				AccountId = entity.AccountID,
				CreateDate = entity.CreateDte,
				Custom = entity.Custom,
				ProductId = entity.ProductID,
				WishListId = entity.WishListID
			};

			return model;
		}

		public static WishList ToEntity(WishListModel model)
		{
			if (model == null)
			{
				return null;
			}

			var entity = new WishList
			{
				AccountID = model.AccountId,
				CreateDte = model.CreateDate,
				Custom = model.Custom,
				ProductID = model.ProductId,
				WishListID = model.WishListId
			};

			return entity;
		}
	}
}
