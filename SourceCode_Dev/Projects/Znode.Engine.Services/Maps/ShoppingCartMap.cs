﻿using System;
using System.Linq;
using Znode.Engine.Api.Models;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.ECommerce.ShoppingCart;

namespace Znode.Engine.Services.Maps
{
	public static class ShoppingCartMap
	{
        public static ShoppingCartModel ToModel(ZNode.Libraries.ECommerce.ShoppingCart.ZNodeShoppingCart znodeCart)
		{
            if (znodeCart == null)
			{
				return null;
			}
            
			var model = new ShoppingCartModel
			{
				Discount = znodeCart.Discount,
				GiftCardAmount = znodeCart.GiftCardAmount,
				GiftCardApplied = znodeCart.IsGiftCardApplied,
				GiftCardMessage = znodeCart.GiftCardMessage,
				GiftCardNumber = znodeCart.GiftCardNumber,
				GiftCardValid = znodeCart.IsGiftCardValid,
                GiftCardBalance = znodeCart.GiftCardBalance,
                MultipleShipToEnabled = znodeCart.IsMultipleShipToAddress,
				OrderLevelDiscount = znodeCart.OrderLevelDiscount,
				OrderLevelShipping = znodeCart.OrderLevelShipping,
				OrderLevelTaxes = znodeCart.OrderLevelTaxes,
				Payment = PaymentMap.ToModel(znodeCart.Payment),
				ProfileId = znodeCart.ProfileId,
				SalesTax = znodeCart.SalesTax,
				Shipping = ShippingMap.ToModel(znodeCart.Shipping),
                ShippingCost = znodeCart.Shipping != null ? znodeCart.ShippingCost : 0,
				SubTotal = znodeCart.SubTotal,
				TaxCost = znodeCart.TaxCost,
				TaxRate = znodeCart.TaxRate,
				Total = znodeCart.Total,
                AdditionalInstructions = znodeCart.AdditionalInstructions, //PRFT Custom Code
                CustomerExternalAccountId=znodeCart.CustomerExternalId
            };

            foreach (ZNode.Libraries.ECommerce.ShoppingCart.ZNodeShoppingCartItem item in znodeCart.ShoppingCartItems)
			{
                model.ShoppingCartItems.Add(ShoppingCartItemMap.ToModel(item, znodeCart));
			}

            foreach(ZNodeCoupon coupon in znodeCart.Coupons)
            {
                model.Coupons.Add(CouponMap.ToModel(coupon));
            }

			return model;
		}

        public static ShoppingCartModel ToModel(ZNodePortalCart znodeCart)
        {
            if (znodeCart == null)
            {
                return null;
            }

            var model = new ShoppingCartModel
            {
                //Coupon = znodeCart.Coupon,
                //CouponMessage = znodeCart.CouponMessage,
                //CouponApplied = znodeCart.CouponApplied,
                //CouponValid = znodeCart.CouponValid,
                Discount = znodeCart.Discount,
                GiftCardAmount = znodeCart.GiftCardAmount,
                GiftCardApplied = znodeCart.IsGiftCardApplied,
                GiftCardMessage = znodeCart.GiftCardMessage,
                GiftCardNumber = znodeCart.GiftCardNumber,
                GiftCardValid = znodeCart.IsGiftCardValid,
                GiftCardBalance = znodeCart.GiftCardBalance,
                MultipleShipToEnabled = znodeCart.IsMultipleShipToAddress,
                OrderLevelDiscount = znodeCart.OrderLevelDiscount,
                OrderLevelShipping = znodeCart.OrderLevelShipping,
                OrderLevelTaxes = znodeCart.OrderLevelTaxes,
                Payment = PaymentMap.ToModel(znodeCart.Payment),
                ProfileId = znodeCart.ProfileId,
                SalesTax = znodeCart.SalesTax,
                Shipping = ShippingMap.ToModel(znodeCart.Shipping),
                ShippingCost = znodeCart.Shipping != null ? znodeCart.ShippingCost : 0,
                SubTotal = znodeCart.SubTotal,
                TaxCost = znodeCart.TaxCost,
                TaxRate = znodeCart.TaxRate,
                Total = znodeCart.Total
            };

            foreach (ZNode.Libraries.ECommerce.ShoppingCart.ZNodeShoppingCartItem item in znodeCart.ShoppingCartItems)
            {
                model.ShoppingCartItems.Add(ShoppingCartItemMap.ToModel(item, znodeCart));
            }

            foreach(ZNodeCoupon coupon in znodeCart.Coupons)
            {
                model.Coupons.Add(CouponMap.ToModel(coupon));
            }

            return model;
        }

        public static ZNode.Libraries.ECommerce.ShoppingCart.ZNodeShoppingCart ToZnodeShoppingCart(ShoppingCartModel model)
		{
			if (model == null)
			{
				return null;
			}

            var znodeCart = new ZNode.Libraries.ECommerce.ShoppingCart.ZNodeShoppingCart
			{
                //Coupon = model.Coupon,
                //CouponApplied = model.CouponApplied,
                //CouponValid = model.CouponValid,
				GiftCardAmount = model.GiftCardAmount,
				GiftCardMessage = model.GiftCardMessage,
				GiftCardNumber = model.GiftCardNumber ?? String.Empty,
				IsGiftCardApplied = model.GiftCardApplied,
				IsGiftCardValid = model.GiftCardValid,
				OrderLevelDiscount = model.OrderLevelDiscount,
				OrderLevelShipping = model.OrderLevelShipping,
				OrderLevelTaxes = model.OrderLevelTaxes,
                Payerid = model.Payerid,    
				Payment = PaymentMap.ToZnodePayment(model.Payment, (model.MultipleShipToEnabled) ? model.ShoppingCartItems.FirstOrDefault().ShippingAddress : model.ShippingAddress),                
				ProfileId = model.ProfileId,
				SalesTax = model.SalesTax,
				Shipping = ShippingMap.ToZnodeShipping(model.Shipping),                
				TaxRate = model.TaxRate,
                Token = model.Token,  
                PortalId = model.PortalId,
                AdditionalInstructions = model.AdditionalInstructions, //PRFT Custom Code    
                CustomerExternalId = model.CustomerExternalAccountId //PRFT Custom Code 
                 
            };

			foreach (var item in model.ShoppingCartItems)
			{
				znodeCart.AddToCart(ShoppingCartItemMap.ToZnodeShoppingCartItem(item, model.ShippingAddress,model.Account.ExternalId));
			}

            foreach(CouponModel coupon in model.Coupons)
            {
                znodeCart.Coupons.Add(CouponMap.ToZnodeCoupon(coupon));
            }

			return znodeCart;
		}
	}
}
