﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
    public static class RMAConfigurationMap
    {
        /// <summary>
        /// Mapping RMAConfiguration Model to RMAConfiguration Entity
        /// </summary>
        /// <param name="entity">RMAConfiguration Entity</param>
        /// <returns>RMAConfiguration Model</returns>
        public static RMAConfigurationModel ToModel(RMAConfiguration entity)
		{
			if (Equals(entity,null))
			{
				return null;
			}

            var model = new RMAConfigurationModel
			{
				RMAConfigId = entity.RMAConfigID,
                MaxDays = entity.MaxDays,
                DisplayName = entity.DisplayName,
                EmailId = entity.Email,
                Address = entity.Address,
                ShippingDirections = entity.ShippingDirections,
                EnableEmailNotification = entity.EnableEmailNotification,
                GiftCardExpirationPeriod = entity.GCExpirationPeriod,
                GiftCardNotification = entity.GCNotification
			};
			return model;
		}

        /// <summary>
        /// Mapping RMAConfiguration Entity to RMAConfiguration Model
        /// </summary>
        /// <param name="entity">RMAConfiguration Model</param>
        /// <returns>RMAConfiguration Entity</returns>
        public static RMAConfiguration ToEntity(RMAConfigurationModel model)
        {
            if (Equals(model, null))
            {
                return null;
            }

            var entity = new RMAConfiguration
            {
                RMAConfigID = model.RMAConfigId,
                MaxDays = model.MaxDays,
                DisplayName = model.DisplayName,
                Email = model.EmailId,
                Address = model.Address,
                ShippingDirections = model.ShippingDirections,
                EnableEmailNotification = model.EnableEmailNotification,
                GCExpirationPeriod = model.GiftCardExpirationPeriod,
                GCNotification = model.GiftCardNotification
            };
            return entity;
        }
    }
}
