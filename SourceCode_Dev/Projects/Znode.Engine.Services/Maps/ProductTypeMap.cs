﻿using System;
using System.Data;
using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
    public static class ProductTypeMap
    {
        public static ProductTypeModel ToModel(ProductType entity)
        {
            if (entity == null)
            {
                return null;
            }

            var model = new ProductTypeModel
            {
                Description = entity.Description,
                DisplayOrder = entity.DisplayOrder,
                IsFranchiseable = entity.Franchisable,
                IsGiftCard = entity.IsGiftCard,
                Name = entity.Name,
                PortalId = entity.PortalID,
                ProductTypeId = entity.ProductTypeId
            };

            return model;
        }

        public static ProductType ToEntity(ProductTypeModel model)
        {
            if (model == null)
            {
                return null;
            }

            var entity = new ProductType
            {
                Description = model.Description,
                DisplayOrder = model.DisplayOrder,
                Franchisable = model.IsFranchiseable,
                IsGiftCard = model.IsGiftCard,
                Name = model.Name,
                PortalID = model.PortalId,
                ProductTypeId = model.ProductTypeId
            };

            return entity;
        }
    }
}
