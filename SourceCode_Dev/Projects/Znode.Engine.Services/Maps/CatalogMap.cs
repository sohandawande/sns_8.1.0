﻿using System;
using System.Data;
using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
	public static class CatalogMap
	{
        /// <summary>
        /// Map Catalog entity to Catalog Model
        /// </summary>
        /// <param name="entity">Catalog entity</param>
        /// <param name="isDefault">Boolean value IsActive</param>
        /// <returns></returns>
        public static CatalogModel ToModel(Catalog entity, bool isDefault = false)
		{
            if (!Equals(entity,null))
            {
                var model = new CatalogModel
                {
                    CatalogId = entity.CatalogID,
                    ExternalId = entity.ExternalID,
                    IsActive = entity.IsActive,
                    Name = entity.Name,
                    PortalId = entity.PortalID,
                    IsDefault = isDefault
                };
                return model;
            }
            else
            {
                return new CatalogModel();
            }
		}

        /// <summary>
        /// Map Catalog model to Catalog Entity
        /// </summary>
        /// <param name="model">CatalogModel</param>
        /// <returns>Returns Catalog Entity</returns>
		public static Catalog ToEntity(CatalogModel model)
		{
			if (Equals(model,null))
			{
				return null;
			}
            
			var entity = new Catalog
			{
				CatalogID = model.CatalogId,
				ExternalID = model.ExternalId,
				IsActive = model.IsActive,
				Name = model.Name,
				PortalID = model.PortalId
			};

			return entity;
		}

        /// <summary>
        /// Maps the dataset of Profile to ProfileList model.
        /// </summary>
        /// <param name="profiles">List of type Profile</param>
        /// <returns>Retuns the mapped list of Profile to ProfileList model.</returns>
        public static CatalogListModel ToModelList(DataSet catalogs)
        {
            if (!Equals(catalogs, null) && catalogs.Tables.Count > 0)
            {
                DataTable catalogTable = catalogs.Tables[0];
                if (catalogTable.Rows.Count > 0)
                {
                    CatalogListModel model = new CatalogListModel();
                    foreach (DataRow row in catalogTable.Rows)
                    {
                        CatalogModel catalogModel = new CatalogModel();
                        catalogModel.Name = row["Name"].ToString();
                        catalogModel.CatalogId = Convert.ToInt32(row["CatalogID"]);
                        model.Catalogs.Add(catalogModel);
                    }
                    return model;
                }
            }
            return new CatalogListModel();
        }
	}
}
