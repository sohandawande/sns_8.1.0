﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
    public static class AddressExtnMap
    {
        public static PRFTAddressExtnModel ToModel(AddressExtn entity)
        {
            if (entity == null)
            {
                return null;
            }

            var model = new PRFTAddressExtnModel
            {
                AddressExtnID = entity.AddressExtnID,
                ERPAddressID = entity.ERPAddressID,
                ModifiedDate = (entity.ModifiedDate == null) ? DateTime.Now : (DateTime)entity.ModifiedDate,
                CreatedDate = (entity.CreatedDate == null) ? DateTime.Now : (DateTime)entity.CreatedDate,
                AddressID = (entity.AddressID == null) ? 0 : (int)entity.AddressID,
                Custom1 = entity.Custom1,
                Custom2 = entity.Custom2,
                Custom3 = entity.Custom3,
                Custom4 = entity.Custom4,
                Custom5 = entity.Custom5,
            };
            return model;
        }

        public static AddressExtn ToEntity(PRFTAddressExtnModel model)
        {
            if (model == null)
            {
                return null;
            }

            var address = new AddressExtn
            {
                AddressExtnID = model.AddressExtnID,
                ERPAddressID = model.ERPAddressID,
                ModifiedDate = model.ModifiedDate,
                CreatedDate = model.CreatedDate,
                AddressID = model.AddressID,
                Custom1 = model.Custom1,
                Custom2 = model.Custom2,
                Custom3 = model.Custom3,
                Custom4 = model.Custom4,
                Custom5 = model.Custom5,
            };
            return address;
        }

        public static AddressExtn ToEntity(PRFTAddressExtnModel model, AddressExtn entity)
        {
            if (model == null || entity == null)
            {
                return entity;
            }

            if (entity.AddressExtnID == model.AddressExtnID)
            {
                entity.AddressExtnID = model.AddressExtnID;
                entity.ERPAddressID = model.ERPAddressID;
                entity.ModifiedDate = model.ModifiedDate;
                entity.CreatedDate = model.CreatedDate;
                entity.AddressID = model.AddressID;
                entity.Custom1 = model.Custom1;
                entity.Custom2 = model.Custom2;
                entity.Custom3 = model.Custom3;
                entity.Custom4 = model.Custom4;
                entity.Custom5 = model.Custom5;
            }

            return entity;
        }

    }
}
