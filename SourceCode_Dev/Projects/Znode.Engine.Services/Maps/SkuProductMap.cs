﻿using System.Linq;
using Znode.Engine.Api.Models;
using ZNode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.Services.Maps
{
    /// <summary>
    /// Sku Product map
    /// </summary>
    public static class SkuProductMap
    {
        /// <summary>
        /// Set skuProducts's small image path.
        /// </summary>
        /// <param name="skuProducts">To set small image path ofSku product list model.</param>
        public static void SetSmallImagePath(SkuProductListModel skuProducts)
        {
            if (!Equals(skuProducts, null) && !Equals(skuProducts.SkuProductList, null) && skuProducts.SkuProductList.Count > 0)
            {
                ZNodeImage imageUtility = new ZNodeImage();
                skuProducts.SkuProductList.Select(model => { model.ImageSmallPath = imageUtility.GetImageHttpPathSmall(model.ImageFile); return model; }).ToList();
            }
        }
    }
}
