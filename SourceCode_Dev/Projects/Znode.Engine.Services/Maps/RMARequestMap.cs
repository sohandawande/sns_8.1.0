﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
   public class RMARequestMap
    {
       public static RMARequestModel ToModel(RMARequest entity)
       {
           if (Equals(entity,null))
           {
               return null;
           }

           var model = new RMARequestModel()
           {
               RMARequestID=entity.RMARequestID,
               RequestDate=entity.RequestDate,
               Comments=entity.Comments,
               RequestStatusId=entity.RequestStatusID,
               UpdatedDate=entity.UpdatedDate,
               RequestNumber=entity.RequestNumber,
               TaxCost=entity.TaxCost,
               Discount=entity.Discount,
               SubTotal=entity.SubTotal,
               Total=entity.Total               
           };

           return model;
       }

       public static RMARequest ToEntity(RMARequestModel model)
       {
           if (Equals(model, null))
           {
               return null;
           }

           var entity = new RMARequest()
           {
               RMARequestID = model.RMARequestID,
               RequestDate = model.RequestDate,
               Comments = model.Comments,
               RequestStatusID = model.RequestStatusId,
               UpdatedDate = model.UpdatedDate,
               RequestNumber = model.RequestNumber,
               TaxCost = model.TaxCost,
               Discount = model.Discount,
               SubTotal = model.SubTotal,
               Total = model.Total
           };
           return entity;
       }
    }
}
