﻿using System;
using System.Collections.Generic;
using System.Web;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.ECommerce.SEO;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;
using Znode.Engine.Api.Models;
using System.Linq;

namespace Znode.Engine.Services.Maps
{
    public static class ProductBundlesMap
    {
        /// <summary>
        /// Convert Parent Child Product entity to product bundles model.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>Product bundles model</returns>
        public static ProductBundlesModel ToModel(ParentChildProduct entity, ProductModel productModel)
        {
            if (Equals(entity, null))
            {
                return null;
            }
            var model = new ProductBundlesModel()
            {
                ChildProductID=entity.ChildProductID,
                ParentChildProductID=entity.ParentChildProductID,
                ParentProductID=entity.ParentProductID,
                ProductName = (!Equals(productModel, null)) ? productModel.Name : string.Empty,
                IsActive = (!Equals(productModel, null)) ? productModel.IsActive : false,
            };

            return model;
        }
    }
}
