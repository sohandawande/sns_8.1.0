﻿using System;
using ZNode.Libraries.DataAccess.Entities;
using Znode.Engine.Api.Models;
using System.Data;


namespace Znode.Engine.Services.Maps
{
    class AddOnValueMap
    {
        public static AddOnValueModel ToModel(AddOnValue entity)
        {
            if (entity == null)
            {
                return null;
            }

            var model = new AddOnValueModel()
                    {
                        AddOnValueId = entity.AddOnValueID,
                        AddOnId = entity.AddOnID,
                        Name = entity.Name,
                        Description = entity.Description,
                        SKU = entity.SKU,
                        IsDefault = entity.DefaultInd,
                        DisplayOrder = entity.DisplayOrder,
                        RetailPrice = entity.RetailPrice,
                        SalePrice = entity.SalePrice,
                        WholesalePrice = entity.WholesalePrice,
                        Weight = entity.Weight,
                        Height = !Equals(entity.Height, null) ? entity.Height.Value : 0,
                        Width = !Equals(entity.Width, null) ? entity.Width.Value : 0,
                        Length = !Equals(entity.Length, null) ? entity.Length.Value : 0,
                        ShippingRuleTypeId = entity.ShippingRuleTypeID,
                        FreeShippingInd = entity.FreeShippingInd != null && Convert.ToBoolean(entity.FreeShippingInd),
                        SupplierId = !Equals(entity.SupplierID, null) ? entity.SupplierID.Value : 0,
                        TaxClassId = !Equals(entity.TaxClassID, null) ? entity.TaxClassID.Value : 0,
                        RecurringBillingInd = entity.RecurringBillingInd,
                        RecurringBillingInstallmentInd = entity.RecurringBillingInstallmentInd,
                        RecurringBillingPeriod = entity.RecurringBillingPeriod,
                        RecurringBillingFrequency = entity.RecurringBillingFrequency,
                        RecurringBillingTotalCycles = !Equals(entity.RecurringBillingTotalCycles, null) ? entity.RecurringBillingTotalCycles.Value : 0,
                        RecurringBillingInitialAmount = !Equals(entity.RecurringBillingInitialAmount, null) ? entity.RecurringBillingInitialAmount.Value : 0,
                        UpdateDate = entity.UpdateDte,
                        QuantityOnHand = (Equals(entity.Inventory, null) || Equals(entity.Inventory.QuantityOnHand, null)) ? 0 : entity.Inventory.QuantityOnHand.Value
                    };

            return model;

        }


        public static AddOnValue ToEntity(AddOnValueModel model)
        {
            if (model == null)
            {
                return null;
            }

            var entity = new AddOnValue()
                {
                    AddOnID = model.AddOnId,
                    AddOnValueID = model.AddOnValueId,
                    Name = model.Name,
                    Description = model.Description,
                    SKU = model.SKU,
                    DefaultInd = model.IsDefault,
                    DisplayOrder = model.DisplayOrder,
                    RetailPrice = model.RetailPrice,
                    SalePrice = Equals(model.SalePrice, null) ? null : model.SalePrice,
                    WholesalePrice = Equals(model.WholesalePrice, null) ? null : model.WholesalePrice,
                    RecurringBillingInd = model.RecurringBillingInd,
                    RecurringBillingInstallmentInd = model.RecurringBillingInstallmentInd,
                    RecurringBillingPeriod = model.RecurringBillingPeriod,
                    RecurringBillingFrequency = model.RecurringBillingFrequency,
                    RecurringBillingTotalCycles = model.RecurringBillingTotalCycles,
                    RecurringBillingInitialAmount = Equals(model.RecurringBillingInitialAmount, null) ? null : model.RecurringBillingInitialAmount,
                    Weight = model.Weight,
                    Length = Equals(model.Length, null) ? null : model.Length,
                    Height = Equals(model.Height, null) ? null : model.Height,
                    Width = Equals(model.Width, null) ? null : model.Width,
                    ShippingRuleTypeID = model.ShippingRuleTypeId,
                    FreeShippingInd = model.FreeShippingInd,
                    SupplierID = model.SupplierId,
                    TaxClassID = model.TaxClassId,
                    UpdateDte = model.UpdateDate,
                };

            return entity;
        }
    }
}

