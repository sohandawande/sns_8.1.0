﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.Services.Maps
{
	public static class SkuMap
	{
		public static SkuModel ToModel(SKU entity)
		{
			if (entity == null)
			{
				return null;
			}

			var imageUtility = new ZNodeImage();

			var model = new SkuModel
			{
				Custom1 = entity.Custom1,
				Custom2 = entity.Custom2,
				Custom3 = entity.Custom3,
				DisplayOrder = entity.DisplayOrder,
				ExternalId = entity.ExternalID,
				FinalPrice = entity.FinalPrice,
				ImageAltTag = entity.ImageAltTag,
				ImageFile = entity.SKUPicturePath,
				ImageMediumPath = imageUtility.GetImageHttpPathMedium(entity.SKUPicturePath),
				ImageLargePath = imageUtility.GetImageHttpPathLarge(entity.SKUPicturePath),
				ImageSmallPath = imageUtility.GetImageHttpPathSmall(entity.SKUPicturePath),
				ImageSmallThumbnailPath = imageUtility.GetImageHttpPathSmallThumbnail(entity.SKUPicturePath),
				ImageThumbnailPath = imageUtility.GetImageHttpPathThumbnail(entity.SKUPicturePath),
				Inventory = InventoryMap.ToModel(entity.Inventory),
				IsActive = entity.ActiveInd,
				Note = entity.Note,
				ProductId = entity.ProductID,
				ProductPrice = entity.ProductPrice,
				PromotionPrice = entity.PromotionPrice,
				RecurringBillingFrequency = entity.RecurringBillingFrequency,
				RecurringBillingInitialAmount = entity.RecurringBillingInitialAmount,
				RecurringBillingPeriod = entity.RecurringBillingPeriod,
				RecurringBillingTotalCycles = entity.RecurringBillingTotalCycles,
				RetailPriceOverride = entity.RetailPriceOverride,
				SalePriceOverride = entity.SalePriceOverride,
				Sku = entity.SKU,
				SkuId = entity.SKUID,
				Supplier = SupplierMap.ToModel(entity.SupplierIDSource),
				SupplierId = entity.SupplierID,
				UpdateDate = entity.UpdateDte,
				WebServiceDownloadDate = entity.WebServiceDownloadDte,
				WeightAdditional = entity.WeightAdditional,
				WholesalePriceOverride = entity.WholesalePriceOverride
			};

			foreach (var a in entity.SKUAttributeCollection)
			{
				model.Attributes.Add(AttributeMap.ToModel(a.AttributeIdSource));
			}

            foreach (var customerPricingEntity in entity.CustomerPricing)
            {
                model.CustomerPricingSku.Add(CustomerPricingMap.ToModel(customerPricingEntity));
            }

			return model;
		}

		public static SKU ToEntity(SkuModel model)
		{
			if (model == null)
			{
				return null;
			}

			var entity = new SKU
			{
				ActiveInd = model.IsActive,
				Custom1 = model.Custom1,
				Custom2 = model.Custom2,
				Custom3 = model.Custom3,
				DisplayOrder = model.DisplayOrder,
				ExternalID = model.ExternalId,
				FinalPrice = model.FinalPrice,
				ImageAltTag = model.ImageAltTag,
				Note = model.Note,
				ProductID = model.ProductId,
				ProductPrice = model.ProductPrice,
				PromotionPrice = model.PromotionPrice,
				RecurringBillingFrequency = model.RecurringBillingFrequency,
				RecurringBillingInitialAmount = model.RecurringBillingInitialAmount,
				RecurringBillingPeriod = model.RecurringBillingPeriod,
				RecurringBillingTotalCycles = model.RecurringBillingTotalCycles,
				RetailPriceOverride = model.RetailPriceOverride,
				SalePriceOverride = model.SalePriceOverride,
				SKU = model.Sku,
				SKUID = model.SkuId,
				SKUPicturePath = model.ImageFile,
				SupplierID = model.SupplierId,
				UpdateDte = model.UpdateDate,
				WebServiceDownloadDte = model.WebServiceDownloadDate,
				WeightAdditional = model.WeightAdditional,
				WholesalePriceOverride = model.WholesalePriceOverride
			};

			return entity;
		}
	}
}
