﻿using System;
using System.Data;
using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
    public static class ProfileMap
    {
        #region Public Methods

        /// <summary>
        /// Maps the Profile entity to ProfileModel model.
        /// </summary>
        /// <param name="entity">Entity of type Category</param>
        /// <returns>Retuns the mapped Profile entity to ProfileModel model.</returns>
        public static ProfileModel ToModel(Profile entity)
        {
            if (entity == null)
            {
                return null;
            }

            var model = new ProfileModel
            {
                EmailList = entity.EmailList,
                ExternalId = entity.DefaultExternalAccountNo,
                Name = entity.Name,
                ProfileId = entity.ProfileID,
                ShowOnPartnerSignup = entity.ShowOnPartnerSignup,
                ShowPricing = entity.ShowPricing,
                TaxClassId = entity.TaxClassID,
                TaxExempt = entity.TaxExempt,
                UseWholesalePricing = entity.UseWholesalePricing,
                Weighting = entity.Weighting
            };

            return model;
        }

        /// <summary>
        /// Maps the Profile model to Profile entity.
        /// </summary>
        /// <param name="model">Model of type ProfileModel</param>
        /// <returns>Retuns the mapped Profile model to Profile entity.</returns>
        public static Profile ToEntity(ProfileModel model)
        {
            if (model == null)
            {
                return null;
            }

            var entity = new Profile
            {
                DefaultExternalAccountNo = model.ExternalId,
                EmailList = model.EmailList,
                Name = model.Name,
                ProfileID = model.ProfileId,
                ShowOnPartnerSignup = model.ShowOnPartnerSignup,
                ShowPricing = model.ShowPricing,
                TaxClassID = model.TaxClassId,
                TaxExempt = model.TaxExempt,
                UseWholesalePricing = model.UseWholesalePricing,
                Weighting = model.Weighting
            };

            return entity;
        }

        /// <summary>
        /// Maps the list of Profile to ProfileList model.
        /// </summary>
        /// <param name="profiles">List of type Profile</param>
        /// <returns>Retuns the mapped list of Profile to ProfileList model.</returns>
        public static ProfileListModel ToModelList(TList<Profile> profiles)
        {
            if (Equals(profiles, null))
            {
                return null;
            }
            var model = new ProfileListModel();
            foreach (var item in profiles)
            {
                model.Profiles.Add(ProfileMap.ToModel(item));
            }
            return model;
        }

        /// <summary>
        /// Maps the dataset of Profile to ProfileList model.
        /// </summary>
        /// <param name="profiles">List of type Profile</param>
        /// <returns>Retuns the mapped list of Profile to ProfileList model.</returns>
        public static ProfileListModel ToModelList(DataSet profiles)
        {
            if (!Equals(profiles, null) && profiles.Tables.Count > 0)
            {
                DataTable profileTable = profiles.Tables[0];
                if (profileTable.Rows.Count > 0)
                {
                    ProfileListModel model = new ProfileListModel();                   
                    foreach (DataRow row in profileTable.Rows)
                    {
                        ProfileModel profileModel = new ProfileModel();
                        profileModel.Name = row["Name"].ToString();
                        profileModel.ProfileId = Convert.ToInt32(row["ProfileID"]);
                        model.Profiles.Add(profileModel);
                    }
                    return model;
                }
            }
            return new ProfileListModel();
        }

        #endregion

    }
}
