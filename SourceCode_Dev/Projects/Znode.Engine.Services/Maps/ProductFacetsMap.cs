﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;
using System.Collections.ObjectModel;
using System.Linq;
using System.Collections.Generic;

namespace Znode.Engine.Services.Maps
{
    public static class ProductFacetsMap
    {
        public static Collection<FacetProductSKUModel> ToFacetSkuModel(TList<FacetProductSKU> entity)
        {
            Collection<FacetProductSKUModel> lstSkuModel = new Collection<FacetProductSKUModel>();

            if (Equals(entity, null) || Equals(entity.Count, 0))
            {
                return lstSkuModel;
            }
            foreach (var item in entity)
            {
                lstSkuModel.Add(new FacetProductSKUModel { FacetID = item.FacetID, FacetProductSKUID = item.FacetProductSKUID, ProductID = item.ProductID, SKUID = item.SKUID });
            }
            return lstSkuModel;
        }
    }
}
