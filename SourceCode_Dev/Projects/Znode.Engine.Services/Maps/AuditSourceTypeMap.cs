﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
	public static class AuditSourceTypeMap
	{
		public static AuditSourceTypeModel ToModel(SourceType entity)
		{
			if (entity == null)
			{
				return null;
			}

			var model = new AuditSourceTypeModel
			{
				AuditSourceTypeId = entity.Id,
				CreateDate = entity.CreatedDate,
				CreatedBy = entity.CreatedBy,
				SourceType = entity.SourceType,
				UpdateDate = entity.UpdatedDate,
				UpdatedBy = entity.UpdatedBy
			};

			return model;
		}

		public static SourceType ToEntity(AuditSourceTypeModel model)
		{
			if (model == null)
			{
				return null;
			}

			var entity = new SourceType
			{
				CreatedDate = model.CreateDate,
				CreatedBy = model.CreatedBy,
				Id = model.AuditSourceTypeId,
				SourceType = model.SourceType,
				UpdatedDate = model.UpdateDate,
				UpdatedBy = model.UpdatedBy
			};

			return entity;
		}
	}
}
