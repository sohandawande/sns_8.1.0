﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
	public static class AuditTypeMap
	{
		public static AuditTypeModel ToModel(SourceModificationType entity)
		{
			if (entity == null)
			{
				return null;
			}

			var model = new AuditTypeModel
			{
				AuditTypeId = entity.Id,
				CreateDate = entity.CreatedDate,
				CreatedBy = entity.CreatedBy,
				ModificationType = entity.ModificationType,
				UpdateDate = entity.UpdatedDate,
				UpdatedBy = entity.UpdatedBy
			};

			return model;
		}

		public static SourceModificationType ToEntity(AuditTypeModel model)
		{
			if (model == null)
			{
				return null;
			}

			var entity = new SourceModificationType
			{
				CreatedDate = model.CreateDate,
				CreatedBy = model.CreatedBy,
				Id = model.AuditTypeId,
				ModificationType = model.ModificationType,
				UpdatedDate = model.UpdateDate,
				UpdatedBy = model.UpdatedBy
			};

			return entity;
		}
	}
}
