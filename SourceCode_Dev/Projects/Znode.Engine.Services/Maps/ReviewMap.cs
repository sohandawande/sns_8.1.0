﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
	public static class ReviewMap
	{
		public static ReviewModel ToModel(Review entity)
		{
			if (entity == null)
			{
				return null;
			}

			var model = new ReviewModel
			{
				AccountId = entity.AccountID,
				Comments = entity.Comments,
				Cons = entity.Cons,
				CreateDate = entity.CreateDate,
				CreateUser = entity.CreateUser,
				Custom1 = entity.Custom1,
				Custom2 = entity.Custom2,
				Custom3 = entity.Custom3,
				Product = ProductMap.ToModel(entity.ProductIDSource),
				ProductId = entity.ProductID,
				Pros = entity.Pros,
				Rating = entity.Rating,
				ReviewId = entity.ReviewID,
				Status = entity.Status,
				Subject = entity.Subject,
				UserLocation = entity.UserLocation
			};

			return model;
		}

		public static Review ToEntity(ReviewModel model)
		{
			if (model == null)
			{
				return null;
			}

			var entity = new Review
			{
				AccountID = model.AccountId,
				Comments = model.Comments,
				Cons = model.Cons,
				CreateDate = model.CreateDate,
				CreateUser = model.CreateUser,
				Custom1 = model.Custom1,
				Custom2 = model.Custom2,
				Custom3 = model.Custom3,
				ProductID = model.ProductId,
				Pros = model.Pros,
				Rating = model.Rating,
				ReviewID = model.ReviewId,
				Status = model.Status,
				Subject = model.Subject,
				UserLocation = model.UserLocation
			};

			return entity;
		}
	}
}
