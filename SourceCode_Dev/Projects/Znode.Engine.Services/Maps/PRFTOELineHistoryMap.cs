﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PRFT.Engine.ERP.OrderService;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services.Maps
{
    public static class PRFTOELineHistoryMap
    {
        public static PRFTOELineHistoryModel ToModel(ERPLineHistory entity)
        {
            if (entity == null)
            {
                return null;
            }

            var model = new PRFTOELineHistoryModel
            {
                ItemNo = entity.ItemNo,
                ItemDescription1 = entity.ItemDescription1,
                ItemDescription2 = entity.ItemDescription2,
                QuantityOrdered = entity.QuantityOrdered,
                QuantityToShip = entity.QuantityToShip,
                QuantityReturnedtoStock = entity.QuantityReturnedtoStock,
                QuantityBackordered = entity.QuantityBackordered,
                UnitPrice = entity.UnitPrice,
                DiscountPercent = entity.DiscountPercent,
                UnitOfMeasure = entity.UnitOfMeasure,
                SalesAmount = entity.SalesAmount
            };
            return model;
        }
    }
}
