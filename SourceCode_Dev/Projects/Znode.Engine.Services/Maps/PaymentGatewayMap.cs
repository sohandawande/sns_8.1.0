﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
	public static class PaymentGatewayMap
	{
		public static PaymentGatewayModel ToModel(Gateway entity)
		{
			if (entity == null)
			{
				return null;
			}

			var model = new PaymentGatewayModel
			{
				Name = entity.GatewayName,
				PaymentGatewayId = entity.GatewayTypeID,
				Url = entity.WebsiteURL
			};

			return model;
		}

		public static Gateway ToEntity(PaymentGatewayModel model)
		{
			if (model == null)
			{
				return null;
			}

			var entity = new Gateway
			{
				GatewayName = model.Name,
				GatewayTypeID = model.PaymentGatewayId,
				WebsiteURL = model.Url
			};

			return entity;
		}
	}
}
