﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using StructureMap;
using Znode.Engine.Api.Models;
using ZNode.Libraries.Search.Interfaces;

namespace Znode.Engine.Services.Maps
{
	public static class KeywordSearchMap
	{
		public static IZNodeSearchRequest ToZnodeRequest(KeywordSearchModel model)
		{
			if (model == null)
			{
				return null;
			}

			var searchRequest = ObjectFactory.GetInstance<IZNodeSearchRequest>();

			searchRequest.Category = model.Category;
			searchRequest.SearchText = model.Keyword;
			searchRequest.InnerSearchKeywords = (model.InnerSearchKeywords != null) ? model.InnerSearchKeywords.ToList() : null;
			searchRequest.Facets = (model.RefineBy != null) ? model.RefineBy.ToList() : null;
			searchRequest.ExternalIdEnabled = model.ExternalIdEnabled;
		    searchRequest.ExternalIdNotNull = model.ExternalIdNotNullCheck;

			return searchRequest;
		}

		public static KeywordSearchModel ToModel(IZNodeSearchResponse response)
		{
			if (response == null)
			{
				return null;
			}

			var model = new KeywordSearchModel();

			if (response.Products != null)
			{
				model.Products = new Collection<SearchProductModel>();
				foreach (var product in response.Products)
				{
					model.Products.Add(new SearchProductModel { Id = product.Id });
				}
			}

			if (response.Facets != null)
			{
				model.Facets = new Collection<SearchFacetModel>(response.Facets.Select(x => new SearchFacetModel { AttributeName = x.AttributeName, ControlTypeId = x.ControlTypeID, AttributeValues = new Collection<SearchFacetValueModel>(x.AttributeValues.Select(y => new SearchFacetValueModel { AttributeValue = y.AttributeValue, FacetCount = y.FacetCount }).ToList()) }).ToList());
			}

			if (response.CategoryItems != null && response.CategoryItems.Count > 0)
			{
				model.Categories = GetCategoryHierarchy(response.CategoryItems);
			}

			return model;
		}

		private static Collection<SearchCategoryModel> GetCategoryHierarchy(List<IZNodeSearchCategoryItem> znodeCategoryHeirarchy)
		{
			var categoryModel = new Collection<SearchCategoryModel>(
				znodeCategoryHeirarchy.Select(
					x =>
					new SearchCategoryModel
						{
							Count = x.Count,
							Name = x.Name,
							Title = x.Title,
							Hierarchy = GetCategoryHierarchy(x.Hierarchy)
						}).ToList());

			return categoryModel;
		}
	}
}
