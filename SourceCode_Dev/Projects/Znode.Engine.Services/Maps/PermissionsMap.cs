﻿using System;
using System.Collections.Generic;
using System.Data;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services.Maps
{
    /// <summary>
    /// This class will map data set to model
    /// </summary>
    public static class PermissionsMap
    {
        /// <summary>
        /// This method will map the DB records to model.
        /// </summary>
        /// <param name="ds">DataSet ds</param>
        /// <param name="userId">GUID user id</param>
        /// <param name="accountId">int Account Id</param>
        /// <param name="userName">string User Name</param>
        /// <returns></returns>
        public static PermissionsModel ToPermissionModel(DataSet ds, Guid userId, int accountId, string userName)
        {
            if (Equals(ds, null) && Equals(ds.Tables, null) && ds.Tables.Count.Equals(0))
            {
                return null;
            }

            var model = new PermissionsModel();
            model.UserId = userId;
            model.AccountId = accountId;
            model.UserName = userName;

            List<Tuple<String, int, bool>> rolesList = new List<Tuple<string, int, bool>>();
            List<Tuple<String, int, bool>> storesList = new List<Tuple<string, int, bool>>();

            foreach (DataRow drRoles in ds.Tables[0].Rows)
            {
                rolesList.Add(new Tuple<string, int, bool>(drRoles["RoleName"].ToString(), Convert.ToInt32(drRoles["RoleId"]), Convert.ToBoolean(drRoles["IsSelected"])));
            }

            foreach (DataRow drRoles in ds.Tables[1].Rows)
            {
                storesList.Add(new Tuple<string, int, bool>(drRoles["StoreName"].ToString(), Convert.ToInt32(drRoles["PortalId"]), Convert.ToBoolean(drRoles["IsSelected"])));
            }

            model.roleModel = new RolesModel();
            model.storeModel = new StoresModel();

            model.roleModel.Permissions = rolesList;
            model.storeModel.Stores = storesList;

            return model;
        }
    }
}
