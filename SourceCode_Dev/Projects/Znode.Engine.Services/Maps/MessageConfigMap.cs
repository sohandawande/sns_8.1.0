﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{

	public static class MessageConfigMap
	{
		public static MessageConfigModel ToModel(MessageConfig entity)
		{
			if (entity == null)
			{
				return null;
			}

			var model = new MessageConfigModel
			{
				Description = entity.Description,
				Key = entity.Key,
				LocaleId = entity.LocaleID,
				MessageConfigId = entity.MessageID,
				MessageType = MessageTypeMap.ToModel(entity.MessageTypeIDSource),
				MessageTypeId = entity.MessageTypeID,
				PageSeoName = entity.PageSEOName,
				PortalId = entity.PortalID,
				Value = entity.Value,
                PortalName = (!Equals(entity.PortalIDSource, null) && !Equals(entity.PortalIDSource, null)) ? entity.PortalIDSource.StoreName : string.Empty,
			};

			return model;
		}

		public static MessageConfig ToEntity(MessageConfigModel model)
		{
			if (model == null)
			{
				return null;
			}

			var entity = new MessageConfig
			{
				Description = model.Description,
				Key = model.Key,
				LocaleID = model.LocaleId,
				MessageID = model.MessageConfigId,
				MessageTypeID = model.MessageTypeId,
				PageSEOName = model.PageSeoName,
				PortalID = model.PortalId,
				Value = model.Value
			};

			return entity;
		}
	}
}
