﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
    public static class ReasonForReturnMap
    {
        /// <summary>
        /// Mapping ReasonForReturn Model to ReasonForReturn Entity
        /// </summary>
        /// <param name="entity">ReasonForReturn Entity</param>
        /// <returns>ReasonForReturn Model</returns>
        public static ReasonForReturnModel ToModel(ReasonForReturn entity)
        {
            if (Equals(entity, null))
            {
                return null;
            }

            var model = new ReasonForReturnModel
            {
                ReasonForReturnId = entity.ReasonForReturnID,
                Name = entity.Name,
                IsEnabled = entity.IsEnabled,                
            };
            return model;
        }

        /// <summary>
        /// Mapping ReasonForReturn Entity to ReasonForReturn Model
        /// </summary>
        /// <param name="entity">ReasonForReturn Model</param>
        /// <returns>ReasonForReturn Entity</returns>
        public static ReasonForReturn ToEntity(ReasonForReturnModel model)
        {
            if (Equals(model, null))
            {
                return null;
            }

            var entity = new ReasonForReturn
            {
                ReasonForReturnID = model.ReasonForReturnId,
                Name = model.Name,
                IsEnabled = model.IsEnabled,
            };
            return entity;
        }
    }
}
