﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
    public static class AccountProfileMap
    {
        /// <summary>
        /// Maps the Account Profile entity to AccountProfileModel model.
        /// </summary>
        /// <param name="entity">Entity of type Category</param>
        /// <returns>Retuns the mapped Profile entity to ProfileModel model.</returns>
        public static AccountProfileModel ToModel(AccountProfile entity)
        {
            if (entity == null)
            {
                return null;
            }

            var model = new AccountProfileModel
            {
                AccountProfileID = entity.AccountProfileID,
                AccountID = entity.AccountID,
                ProfileID = entity.ProfileID
            };

            return model;
        }

        /// <summary>
        /// Maps the Account Profile model to Profile entity.
        /// </summary>
        /// <param name="model">Model of type AccountProfileModel</param>
        /// <returns>Retuns the mapped Account Profile model to Account Profile entity.</returns>
        public static AccountProfile ToEntity(AccountProfileModel model)
        {
            if (!Equals(model , null))
            {
                return null;
            }

            var entity = new AccountProfile
            {
                AccountID = model.AccountID,
                ProfileID = model.ProfileID,
                AccountProfileID = model.AccountProfileID
            };

            return entity;
        }
    }
}
