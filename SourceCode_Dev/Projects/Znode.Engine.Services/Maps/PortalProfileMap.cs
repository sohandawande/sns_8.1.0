﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
    public class PortalProfileMap
    {

        public static PortalProfileModel ToModel(PortalProfile entity)
        {
            if (Equals(entity, null))
            {
                return null;
            }
            var model = new PortalProfileModel
            {
                PortalProfileID=entity.PortalProfileID,
                ProfileID=entity.ProfileID,
                PortalID=entity.PortalID
            };

            return model;
        }

        public static PortalProfile ToEntity(PortalProfileModel model)
        {
            if (Equals(model, null))
            {
                return null;
            }
            var entity = new PortalProfile
            {
                PortalProfileID = model.PortalID,
                ProfileID = model.ProfileID,
                PortalID = model.PortalID
            };

            return entity;
        }
    }
}
