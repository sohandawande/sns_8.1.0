﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
    public static class ProductSearchSettingsMap
    {
        /// <summary>
        /// Convert LuceneDocumentMapping to FieldLevelSettingModel
        /// </summary>
        /// <param name="entity">LuceneDocumentMapping entity</param>
        /// <returns>FieldLevelSettingModel</returns>
        public static FieldLevelSettingModel ToModel(LuceneDocumentMapping entity)
        {
            if (entity == null)
            {
                return null;
            }

            var model = new FieldLevelSettingModel
            {
                Id=entity.LuceneDocumentMappingID,
                DocumentName = entity.DocumentName,
                Boost = entity.Boost
            };
            return model;
        }
    }
}
