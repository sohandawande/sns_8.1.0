﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
    public static class ContentPageRevisionMap
    {
        public static ContentPageRevisionModel ToModel(ContentPageRevision entity)
        {
            if (Equals(entity, null))
            {
                return null;
            }

            var model = new ContentPageRevisionModel
            {
               ContentPageID=entity.ContentPageID,
               Description=entity.Description,
               HtmlText=entity.HtmlText,
               RevisionID=entity.RevisionID,
               UpdateDate=entity.UpdateDate,
               UpdatedUser=entity.UpdateUser
            };

            return model;
        }

        public static ContentPageRevision ToEntity(ContentPageRevisionModel model)
        {
            if (Equals(model, null))
            {
                return null;
            }

            var entity = new ContentPageRevision
            {
                ContentPageID = model.ContentPageID,
                Description = model.Description,
                HtmlText = model.HtmlText,
                RevisionID = model.RevisionID,
                UpdateDate = model.UpdateDate,
                UpdateUser = model.UpdatedUser
            };

            return entity;
        }

        public static ContentPageRevisionListModel ToListModel(TList<ContentPageRevision> entities)
        {
            if (Equals(entities, null))
            {
                return null;
            }
            var model = new ContentPageRevisionListModel();
            foreach (var item in entities)
            {
                model.ContentPageRevisions.Add(ToModel(item));
            }
            return model;
        }
    }
}
