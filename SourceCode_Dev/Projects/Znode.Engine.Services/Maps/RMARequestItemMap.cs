﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services
{
    public class RMARequestItemMap
    {
        public static RMARequestItemModel ToModel(RMARequestItem entity)
        {
            if (Equals(entity,null))
            {
                return null;
            }
            var model = new RMARequestItemModel
            {
                RMARequestItemID=entity.RMARequestItemID,
                RMARequestId=entity.RMARequestID,
                OrderLineItemID=entity.OrderLineItemID,
                IsReceived=entity.IsReceived,
                IsReturnable=entity.IsReturnable,
                GiftCardId=entity.GiftCardId,
                Quantity=entity.Quantity,
                Price=entity.Price,
                ReasonForReturnId=entity.ReasonForReturnID,
                TransactionId=entity.TansactionId
            };
            return model;
        }

        public static RMARequestItem ToEntity(RMARequestItemModel model)
        {
            if (Equals(model, null))
            {
                return null;
            }
            var entity = new RMARequestItem
            {
                RMARequestItemID = model.RMARequestItemID,
                RMARequestID = model.RMARequestId,
                OrderLineItemID = model.OrderLineItemID,
                IsReceived = model.IsReceived,
                IsReturnable = model.IsReturnable,
                GiftCardId = model.GiftCardId,
                Quantity = model.Quantity,
                Price = model.Price,
                ReasonForReturnID = model.ReasonForReturnId,
                TansactionId = model.TransactionId
            };
            return entity;
        }
    }
}
