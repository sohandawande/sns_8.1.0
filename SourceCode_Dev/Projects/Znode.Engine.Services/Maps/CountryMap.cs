﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
	public static class CountryMap
	{
		public static CountryModel ToModel(Country entity)
		{
			if (entity == null)
			{
				return null;
			}

			var model = new CountryModel
			{
				Code = entity.Code,
				DisplayOrder = entity.DisplayOrder,
				IsActive = entity.ActiveInd,
				Name = entity.Name
			};

			foreach (var s in entity.States)
			{
				model.States.Add(StateMap.ToModel(s));
			}

			return model;
		}

		public static Country ToEntity(CountryModel model)
		{
			if (model == null)
			{
				return null;
			}

			var entity = new Country
			{
				ActiveInd = model.IsActive,
				Code = model.Code,
				DisplayOrder = model.DisplayOrder,
				Name = model.Name
			};

			return entity;
		}
	}
}
