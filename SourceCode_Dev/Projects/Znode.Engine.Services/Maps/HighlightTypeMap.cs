﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
	public static class HighlightTypeMap
	{
		public static HighlightTypeModel ToModel(HighlightType entity)
		{
			if (entity == null)
			{
				return null;
			}

			var model = new HighlightTypeModel
			{
				Description = entity.Description,
				HighlightTypeId = entity.HighlightTypeID,
				Name = entity.Name
			};

			return model;
		}

		public static HighlightType ToEntity(HighlightTypeModel model)
		{
			if (model == null)
			{
				return null;
			}

			var entity = new HighlightType
			{
				Description = model.Description,
				HighlightTypeID = model.HighlightTypeId,
				Name = model.Name
			};

			return entity;
		}
	}
}
