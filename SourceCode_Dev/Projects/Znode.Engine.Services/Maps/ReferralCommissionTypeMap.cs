﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
    public static class ReferralCommissionTypeMap
    {
        /// <summary>
        /// Maps the ReferralCommissionType entity to ReferralCommissionType model.
        /// </summary>
        /// <param name="entity">Entity of type ReferralCommissionType</param>
        /// <returns>Retuns the mapped ReferralCommissionType entity to ReferralCommissionType model.</returns>
        public static ReferralCommissionTypeModel ToModel(ReferralCommissionType entity)
        {
            if (Equals(entity, null))
            {
                return null;
            }

            var model = new ReferralCommissionTypeModel
            {
                ReferralCommissionTypeId = entity.ReferralCommissionTypeID,
                ReferralCommissionTypeName = entity.Name
            };
            return model;
        }

        /// <summary>
        /// Maps the ReferralCommissionType model to ReferralCommissionType entity.
        /// </summary>
        /// <param name="model">ReferralCommissionType Model</param>
        /// <returns>Retuns the mapped ReferralCommissionType model to ReferralCommissionType entity.</returns>
        public static ReferralCommissionType ToEntity(ReferralCommissionTypeModel model)
        {
            if (Equals(model, null))
            {
                return null;
            }

            var entity = new ReferralCommissionType
            {
                ReferralCommissionTypeID = model.ReferralCommissionTypeId,
                Name = model.ReferralCommissionTypeName
            };
            return entity;
        }
    }
}
