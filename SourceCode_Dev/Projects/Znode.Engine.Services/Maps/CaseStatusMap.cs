﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
	public static class CaseStatusMap
	{
		public static CaseStatusModel ToModel(CaseStatus entity)
		{
			if (entity == null)
			{
				return null;
			}

			var model = new CaseStatusModel
			{
				CaseStatusId = entity.CaseStatusID,
				DisplayOrder = entity.ViewOrder,
				Name = entity.CaseStatusNme
			};

			return model;
		}

		public static CaseStatus ToEntity(CaseStatusModel model)
		{
			if (model == null)
			{
				return null;
			}

			var entity = new CaseStatus
			{
				CaseStatusID = model.CaseStatusId,
				CaseStatusNme = model.Name,
				ViewOrder = model.DisplayOrder
			};

			return entity;
		}
	}
}
