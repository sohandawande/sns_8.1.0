﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
	public static class GiftCardMap
	{
		public static GiftCardModel ToModel(GiftCard entity)
		{
			if (entity == null)
			{
				return null;
			}

			var model = new GiftCardModel
			{
				AccountId = entity.AccountId,
				Amount = entity.Amount,
				CardNumber = entity.CardNumber,
				CreateDate = entity.CreateDate,
				CreatedBy = entity.CreatedBy,
				ExpirationDate = entity.ExpirationDate,
				GiftCardId = entity.GiftCardId,
				Name = entity.Name,
				OrderLineItem = OrderLineItemMap.ToModel(entity.OrderItemIdSource),
				OrderLineItemId = entity.OrderItemId,
				PortalId = entity.PortalId
			};

			foreach (var h in entity.GiftCardHistoryCollection)
			{
				model.History.Add(GiftCardHistoryMap.ToModel(h));
			}

			return model;
		}

		public static GiftCard ToEntity(GiftCardModel model)
		{
			if (model == null)
			{
				return null;
			}

			var entity = new GiftCard
			{
				AccountId = model.AccountId,
				Amount = model.Amount,
				CardNumber = model.CardNumber,
				CreateDate = model.CreateDate,
				CreatedBy = model.CreatedBy,
				ExpirationDate = model.ExpirationDate,
				GiftCardId = model.GiftCardId,
				Name = model.Name,
				OrderItemId = model.OrderLineItemId,
				PortalId = model.PortalId
			};

			return entity;
		}
	}
}
