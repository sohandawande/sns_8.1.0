﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
    public class ProductReviewStateMap
    {
        public static ProductReviewStateModel ToModel(ProductReviewState entity)
        {
            if (entity == null)
            {
                return null;
            }

            var model = new ProductReviewStateModel
            {
                ReviewStateID=entity.ReviewStateID,
                ReviewStateName=entity.ReviewStateName,
                Description=entity.Description
            };

            return model;
        }
    }
}
