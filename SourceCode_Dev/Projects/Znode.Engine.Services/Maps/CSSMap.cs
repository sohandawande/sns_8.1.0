﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
    public class CSSMap
    {
        public static CSSModel ToModel(CSS entity)
        {
            if (Equals(entity,null))
            {
                return new CSSModel();
            }
            CSSModel model = new CSSModel
                {
                    CSSID = entity.CSSID,
                    ThemeID = entity.ThemeID,
                    Name = entity.Name,
                    Theme = ThemeMap.ToModel(entity.ThemeIDSource),
                };
            return model;
        }

        public static CSS ToEntity(CSSModel model)
        {
            if (Equals(model, null))
            {
                return null;
            }
            CSS enity = new CSS
            {
                CSSID = model.CSSID,
                ThemeID = model.ThemeID,
                Name = model.Name,
            };
            return enity;
        }
    }
}
