﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
    public static class CustomerPricingMap
    {
        public static CustomerPricingModel ToModel(CustomerPricing entity)
        {
            if (Equals(entity, null))
            {
                return null;
            }

            CustomerPricingModel model = new CustomerPricingModel
            {
                CustomerPricingId = entity.CustomerPricingID,
                ExternalAccountNo = entity.ExternalAccountNo,
                NegotiatedPrice = entity.NegotiatedPrice,
                SKUExternalId = entity.SKUExternalID
            };

            return model;
        }

        public static CustomerPricing ToEntity(CustomerPricingModel model)
        {
            if (Equals(model, null))
            {
                return null;
            }

            CustomerPricing entity = new CustomerPricing
            {
                CustomerPricingID = model.CustomerPricingId,
                ExternalAccountNo = model.ExternalAccountNo,
                NegotiatedPrice = model.NegotiatedPrice,
                SKUExternalID = model.SKUExternalId
            };

            return entity;
        }

        
    }
}
