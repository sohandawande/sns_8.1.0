﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;
using System.Collections.ObjectModel;

namespace Znode.Engine.Services.Maps
{
    public class FacetMap
    {
        public static FacetModel ToModel(Facet entity)
        {
            if (Equals(entity, null))
            {
                return null;
            }
            var model = new FacetModel
            {
                FacetGroupID = entity.FacetGroupID,
                FacetDisplayOrder=entity.FacetDisplayOrder,
                FacetID=entity.FacetID,
                FacetName=entity.FacetName,
            };
            return model;
        }
    }
}
