﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
    public static class ShippingOptionMap
    {
        public static ShippingOptionModel ToModel(ZNode.Libraries.DataAccess.Entities.Shipping entity)
        {
            if (entity == null)
            {
                return null;
            }

            var model = new ShippingOptionModel
            {
                CountryCode = entity.DestinationCountryCode,
                Description = entity.Description,
                DisplayOrder = entity.DisplayOrder,
                ExternalId = entity.ExternalID,
                HandlingCharge = entity.HandlingCharge,
                ActiveInd = entity.ActiveInd,
                ProfileId = entity.ProfileID,
                ShippingCode = entity.ShippingCode,
                ShippingId = entity.ShippingID,
                ShippingType = ShippingTypeMap.ToModel(entity.ShippingTypeIDSource),
                ShippingTypeId = entity.ShippingTypeID,
                ProfileName = (!Equals(entity.ProfileIDSource, null) && !Equals(entity.ProfileIDSource.Name, null)) ? entity.ProfileIDSource.Name : string.Empty,
                ShippingtypeName = (!Equals(entity.ShippingTypeIDSource, null) && !Equals(entity.ShippingTypeIDSource.Name, null)) ? entity.ShippingTypeIDSource.Name : string.Empty,   
            };

            return model;
        }

        public static ZNode.Libraries.DataAccess.Entities.Shipping ToEntity(ShippingOptionModel model)
        {
            if (model == null)
            {
                return null;
            }

            var entity = new ZNode.Libraries.DataAccess.Entities.Shipping
            {
                ActiveInd = model.ActiveInd,
                Description = model.Description,
                DestinationCountryCode = model.CountryCode,
                DisplayOrder = model.DisplayOrder,
                ExternalID = model.ExternalId,
                HandlingCharge = model.HandlingCharge,
                ProfileID = (Equals(model.ProfileId,-1))?null:model.ProfileId,
                ShippingCode = model.ShippingCode,
                ShippingID = model.ShippingId,
                ShippingTypeID = model.ShippingTypeId,                
            };

            return entity;
        }
    }
}
