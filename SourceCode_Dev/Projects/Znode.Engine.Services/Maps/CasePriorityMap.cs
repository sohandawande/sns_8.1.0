﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
	public static class CasePriorityMap
	{
		public static CasePriorityModel ToModel(CasePriority entity)
		{
			if (entity == null)
			{
				return null;
			}

			var model = new CasePriorityModel
			{
				CasePriorityId = entity.CasePriorityID,
				DisplayOrder = entity.ViewOrder,
				Name = entity.CasePriorityNme
			};

			return model;
		}

		public static CasePriority ToEntity(CasePriorityModel model)
		{
			if (model == null)
			{
				return null;
			}

			var entity = new CasePriority
			{
				CasePriorityID = model.CasePriorityId,
				CasePriorityNme = model.Name,
				ViewOrder = model.DisplayOrder
			};

			return entity;
		}
	}
}
