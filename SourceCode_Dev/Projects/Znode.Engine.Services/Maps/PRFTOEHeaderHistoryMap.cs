﻿using PRFT.Engine.ERP.OrderService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services.Maps
{
    public static class PRFTOEHeaderHistoryMap
    {
        public static PRFTOEHeaderHistoryModel ToModel(OrderInvoice entity)
        {
            if (entity == null)
            {
                return null;
            }

            var model = new PRFTOEHeaderHistoryModel
            {
                InvoiceNumber = entity.InvoiceHeaderDetails.InvoiceNumber,
                InvoiceDate = entity.InvoiceHeaderDetails.InvoiceDate,
                BillToCustomerName = entity.InvoiceHeaderDetails.BillToCustomerName,
                BillToAddressLine1 = entity.InvoiceHeaderDetails.BillToAddressLine1,
                BillToAddressLine2 = entity.InvoiceHeaderDetails.BillToAddressLine2,
                BillToAddressLine3 = entity.InvoiceHeaderDetails.BillToAddressLine3,
                BillToAddressLine4 = entity.InvoiceHeaderDetails.BillToAddressLine4,
                BillToCity = entity.InvoiceHeaderDetails.BillToCity,
                BillToState = entity.InvoiceHeaderDetails.BillToState,
                BillToZip = entity.InvoiceHeaderDetails.BillToZip,
                BillToCountry = entity.InvoiceHeaderDetails.BillToCountry,
                ShipToName = entity.InvoiceHeaderDetails.ShipToName,
                ShipToAddressLine1 = entity.InvoiceHeaderDetails.ShipToAddressLine1,
                ShipToAddressLine2 = entity.InvoiceHeaderDetails.ShipToAddressLine2,
                ShipToAddressLine3 = entity.InvoiceHeaderDetails.ShipToAddressLine3,
                ShipToAddressLine4 = entity.InvoiceHeaderDetails.ShipToAddressLine4,
                ShipToCity = entity.InvoiceHeaderDetails.ShipToCity,
                ShipToState = entity.InvoiceHeaderDetails.ShipToState,
                ShipToZip = entity.InvoiceHeaderDetails.ShipToZip,
                ShipToCountry = entity.InvoiceHeaderDetails.ShipToCountry,
                OrderNumber = entity.InvoiceHeaderDetails.OrderNumber,
                OrderDate = entity.InvoiceHeaderDetails.OrderDate,
                CustomerNumber = entity.InvoiceHeaderDetails.CustomerNumber,
                ManufacturingLocation = entity.InvoiceHeaderDetails.ManufacturingLocation,
                SalesPersonNumber = entity.InvoiceHeaderDetails.SalespersonNumber,
                PurchaseOrderNumber = entity.InvoiceHeaderDetails.PurchaseOrderNumber,
                JobNumber = entity.InvoiceHeaderDetails.JobNumber,
                ShipViaCode = entity.InvoiceHeaderDetails.ShipViaCode,
                FreightPaymentCode = entity.InvoiceHeaderDetails.FreightPaymentCode,
                TotalSaleAmt = entity.InvoiceHeaderDetails.TotalSaleAmt,
                MiscellaneousAmount = entity.InvoiceHeaderDetails.MiscellaneousAmount,
                FreightAmount = entity.InvoiceHeaderDetails.FreightAmount,
                SalesTaxAmount1 = entity.InvoiceHeaderDetails.SalesTaxAmount1,
                SalesTaxAmount2 = entity.InvoiceHeaderDetails.SalesTaxAmount2,
                SalesTaxAmount3 = entity.InvoiceHeaderDetails.SalesTaxAmount3,
                PaymentAmount = entity.InvoiceHeaderDetails.PaymentAmount,
                PaymentDiscountAmount = entity.InvoiceHeaderDetails.PaymentDiscountAmount,
                PaymentTerms = entity.InvoiceHeaderDetails.AccountsReceivableTermsCode,
                Comments = string.Format("{0} {1} {2}", entity.InvoiceHeaderDetails.CommentLine1, entity.InvoiceHeaderDetails.CommentLine2, entity.InvoiceHeaderDetails.CommentLine3),
            };

            foreach (var item in entity.InvoiceItems)
            {
                model.OELineHistoryLists.Add(PRFTOELineHistoryMap.ToModel(item));
            }

            return model;
        }
    }
}
