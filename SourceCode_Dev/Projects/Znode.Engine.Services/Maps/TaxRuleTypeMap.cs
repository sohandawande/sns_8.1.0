﻿using Znode.Engine.Api.Models;
using Znode.Engine.Taxes;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
    public static class TaxRuleTypeMap
    {
        public static TaxRuleTypeModel ToModel(TaxRuleType entity)
        {
            if (entity == null)
            {
                return null;
            }

            var model = new TaxRuleTypeModel
            {
                ClassName = entity.ClassName,
                Description = entity.Description,
                IsActive = entity.ActiveInd,
                Name = entity.Name,
                PortalId = entity.PortalID,
                TaxRuleTypeId = entity.TaxRuleTypeID
            };

            return model;
        }

        public static TaxRuleType ToEntity(TaxRuleTypeModel model)
        {
            if (model == null)
            {
                return null;
            }

            var entity = new TaxRuleType
            {
                ActiveInd = model.IsActive,
                ClassName = model.ClassName,
                Description = model.Description,
                Name = model.Name,
                PortalID = model.PortalId,
                TaxRuleTypeID = model.TaxRuleTypeId
            };

            return entity;
        }

        /// <summary>
        /// Znode version 8.0
        /// Convert IZnodeTaxType to TaxRuleTypeModel
        /// </summary>
        /// <param name="znodeTaxType">IZnodeTaxType znodeTaxType</param>
        /// <returns>Returns TaxRuleTypeModel</returns>
        public static TaxRuleTypeModel ToModel(IZnodeTaxType znodeTaxType)
        {
            if (Equals(znodeTaxType, null))
            {
                return null;
            }
            var model = new TaxRuleTypeModel
            {
                ClassName = znodeTaxType.ClassName,
                Description = znodeTaxType.Description,
                Name = znodeTaxType.Name,
            };
            return model;
        }
    }
}
