﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
    public class ProductReviewHistoryMap
    {
        /// <summary>
        /// Convert Product Review Model to Product Review View Model
        /// </summary>
        /// <param name="entity">ProductReviewHistory entity</param>
        /// <returns>Return Product Review History details in ProductReviewHistoryModel Model </returns>
        public static ProductReviewHistoryModel ToModel(ProductReviewHistory entity)
        {
            if (Equals(entity, null))
            {
                return null;
            }

            var model = new ProductReviewHistoryModel
            {
                ProductReviewHistoryID=entity.ProductReviewHistoryID,
                ProductID=entity.ProductID,
                VendorID=entity.VendorID,
                Status=entity.Status,
                Reason=entity.Reason,
                Description = entity.Description,
                Username=entity.Username,
                LogDate=entity.LogDate,
                NotificationDate=entity.NotificationDate,
            };

            return model;
        }
    }
}
