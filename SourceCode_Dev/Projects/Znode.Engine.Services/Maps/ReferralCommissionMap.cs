﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
    public static class ReferralCommissionMap
    {
        /// <summary>
        /// Maps the ReferralCommissionType entity to ReferralCommissionType model.
        /// </summary>
        /// <param name="entity">Entity of type ReferralCommissionType</param>
        /// <returns>Retuns the mapped ReferralCommissionType entity to ReferralCommissionType model.</returns>
        public static ReferralCommissionModel ToModel(ReferralCommission entity)
        {
            if (Equals(entity, null))
            {
                return null;
            }

            var model = new ReferralCommissionModel
            {
                ReferralCommissionId = entity.ReferralCommissionID,
                ReferralCommissionTypeId = entity.ReferralCommissionTypeID,
                ReferralCommission = entity.ReferralCommission,
                ReferralAccountId = entity.ReferralAccountID,
                OrderId = entity.OrderID,
                TransactionId = entity.TransactionID,
                Description = entity.Description,
                CommissionAmount = entity.CommissionAmount,
                ReferralCommissionType = (!Equals(entity.ReferralCommissionTypeIDSource, null) && !Equals(entity.ReferralCommissionTypeIDSource.Name, null)) ? entity.ReferralCommissionTypeIDSource.Name : string.Empty,
            };
            return model;
        }

        /// <summary>
        /// Maps the ReferralCommissionType model to ReferralCommissionType entity.
        /// </summary>
        /// <param name="model">ReferralCommissionType Model</param>
        /// <returns>Retuns the mapped ReferralCommissionType model to ReferralCommissionType entity.</returns>
        public static ReferralCommission ToEntity(ReferralCommissionModel model)
        {
            if (Equals(model, null))
            {
                return null;
            }

            var entity = new ReferralCommission
            {
                ReferralCommissionID = model.ReferralCommissionId,
                ReferralCommissionTypeID = model.ReferralCommissionTypeId,
                ReferralCommission = model.ReferralCommission,
                ReferralAccountID = model.ReferralAccountId,
                OrderID = model.OrderId,
                TransactionID = model.TransactionId,
                Description = model.Description,
                CommissionAmount = model.CommissionAmount
            };
            return entity;
        }
    }
}
