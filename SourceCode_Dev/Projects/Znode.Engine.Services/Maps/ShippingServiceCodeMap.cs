﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
	public static class ShippingServiceCodeMap
	{
		public static ShippingServiceCodeModel ToModel(ShippingServiceCode entity)
		{
			if (entity == null)
			{
				return null;
			}

			var model = new ShippingServiceCodeModel
			{
				Code = entity.Code,
				Description = entity.Description,
				DisplayOrder = entity.DisplayOrder,
				IsActive = entity.ActiveInd,
				ShippingServiceCodeId = entity.ShippingServiceCodeID,
				ShippingType = ShippingTypeMap.ToModel(entity.ShippingTypeIDSource),
				ShippingTypeId = entity.ShippingTypeID
			};

			return model;
		}

		public static ShippingServiceCode ToEntity(ShippingServiceCodeModel model)
		{
			if (model == null)
			{
				return null;
			}

			var entity = new ShippingServiceCode
			{
				ActiveInd = model.IsActive,
				Code = model.Code,
				Description = model.Description,
				DisplayOrder = model.DisplayOrder,
				ShippingServiceCodeID = model.ShippingServiceCodeId,
				ShippingTypeID = model.ShippingTypeId
			};

			return entity;
		}
	}
}
