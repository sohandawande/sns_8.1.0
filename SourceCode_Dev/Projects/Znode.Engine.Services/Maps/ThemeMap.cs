﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
    public static class ThemeMap
    {
        public static ThemeModel ToModel(Theme entity)
        {
            if (Equals(entity,null))
            {
                return new ThemeModel();
            }

            ThemeModel model = new ThemeModel
            {
               ThemeID=entity.ThemeID,
               Name=entity.Name
            };

            return model;
        }

        public static Theme ToEntity(ThemeModel model)
        {
            if (Equals(model, null))
            {
                return null;
            }

            Theme entity = new Theme
            {
                ThemeID = model.ThemeID,
                Name = model.Name
            };
            return entity;
        }
    }
}
