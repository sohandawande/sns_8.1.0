﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.ECommerce.SEO;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;
namespace Znode.Engine.Services.Maps
{
    public static class ProductMap
    {
        public static ProductModel ToModel(Product entity)
        {
            if (entity == null)
            {
                return null;
            }

            var imageUtility = new ZNodeImage();

            var model = new ProductModel
            {
                AccountId = entity.AccountID,
                AdditionalInfo = entity.AdditionalInformation,
                AdditionalInfoLink = entity.AdditionalInfoLink,
                AdditionalInfoLinkLabel = entity.AdditionalInfoLinkLabel,
                AffiliateUrl = entity.AffiliateUrl,
                AllowBackOrder = entity.AllowBackOrder,
                AllowDropShip = entity.DropShipInd,
                AllowFreeShipping = entity.FreeShippingInd,
                AllowRecurringBilling = entity.RecurringBillingInd,
                AllowRecurringBillingInstallment = entity.RecurringBillingInstallmentInd,
                BackOrderMessage = entity.BackOrderMsg,
                BeginActiveDate = entity.BeginActiveDate,
                CallForPricing = entity.CallForPricing,
                CreateDate = entity.CreateDate,
                Custom1 = entity.Custom1,
                Custom2 = entity.Custom2,
                Custom3 = entity.Custom3,
                Description = entity.Description,
                DisplayOrder = entity.DisplayOrder,
                DownloadLink = entity.DownloadLink,
                DropShipEmailAddress = entity.DropShipEmailID,
                EndActiveDate = entity.EndActiveDate,
                ExpirationFrequency = entity.ExpirationFrequency,
                ExpirationPeriod = entity.ExpirationPeriod,
                ExternalId = entity.ExternalID,
                FeaturesDescription = entity.FeaturesDesc,
                Height = entity.Height,
                ImageAltTag = entity.ImageAltTag,
                ImageFile = entity.ImageFile,
                ImageMediumPath = imageUtility.GetImageHttpPathMedium(entity.ImageFile),
                ImageLargePath = imageUtility.GetImageHttpPathLarge(entity.ImageFile),
                ImageSmallPath = imageUtility.GetImageHttpPathSmall(entity.ImageFile),
                ImageSmallThumbnailPath = imageUtility.GetImageHttpPathSmallThumbnail(entity.ImageFile),
                ImageThumbnailPath = imageUtility.GetImageHttpPathThumbnail(entity.ImageFile),
                InStockMessage = entity.InStockMsg,
                InventoryDisplay = entity.InventoryDisplay,
                IsActive = entity.ActiveInd,
                IsCategorySpecial = entity.CategorySpecial,
                IsFeatured = entity.FeaturedInd,
                IsFranchisable = entity.Franchisable,
                IsHomepageSpecial = entity.HomepageSpecial,
                IsNewProduct = entity.NewProductInd,
                IsShippable = entity.IsShippable,
                Keywords = entity.Keywords,
                Length = entity.Length,
                Manufacturer = ManufacturerMap.ToModel(entity.ManufacturerIDSource),
                ManufacturerId = entity.ManufacturerID,
                MaxSelectableQuantity = entity.MaxQty,
                MinSelectableQuantity = entity.MinQty,
                Name = HttpUtility.HtmlDecode(entity.Name),
                OutOfStockMessage = entity.OutOfStockMsg,
                PortalId = entity.PortalID,
                ProductId = entity.ProductID,
                ProductNumber = entity.ProductNum,
                ProductType = ProductTypeMap.ToModel(entity.ProductTypeIDSource),
                ProductTypeId = entity.ProductTypeID,
                ProductUrl = ZNodeSEOUrl.MakeUrlForMvc(entity.ProductID.ToString(), SEOUrlType.Product, entity.SEOURL),
                PromotionPrice = entity.PromotionPrice,
                RecurringBillingFrequency = entity.RecurringBillingFrequency,
                RecurringBillingInitialAmount = entity.RecurringBillingInitialAmount,
                RecurringBillingPeriod = entity.RecurringBillingPeriod,
                RecurringBillingTotalCycles = entity.RecurringBillingTotalCycles,
                RetailPrice = entity.RetailPrice,
                ReviewStateId = entity.ReviewStateID,
                SalePrice = entity.SalePrice,
                SelectedSku = SkuMap.ToModel(entity.SelectedSku),
                SeoDescription = string.IsNullOrEmpty(entity.SEODescription) ? ZNodeConfigManager.SiteConfig.SeoDefaultProductDescription : entity.SEODescription,
                SeoKeywords = string.IsNullOrEmpty(entity.SEOKeywords) ? ZNodeConfigManager.SiteConfig.SeoDefaultProductKeyword : entity.SEOKeywords,
                SeoTitle = string.IsNullOrEmpty(entity.SEOTitle) ? ZNodeConfigManager.SiteConfig.SeoDefaultProductTitle : entity.SEOTitle,
                SeoUrl = entity.SEOURL,
                ShipEachItemSeparately = entity.ShipEachItemSeparately,
                ShippingRate = entity.ShippingRate,
                ShippingRuleTypeId = entity.ShippingRuleTypeID,
                ShipSeparately = entity.ShipSeparately,
                ShortDescription = entity.ShortDescription,
                Specifications = entity.Specifications,
                SupplierId = entity.SupplierID,
                TaxClassId = entity.TaxClassID,
                TieredPrice = entity.TieredPrice,
                TrackInventory = entity.TrackInventoryInd,
                UpdateDate = entity.UpdateDte,
                WebServiceDownloadDate = entity.WebServiceDownloadDte,
                Weight = entity.Weight,
                Width = entity.Width,
                WholesalePrice = entity.WholesalePrice
            };

            if (!Equals(entity.ProductAddOnCollection, null))
            {
                foreach (var a in entity.ProductAddOnCollection)
                {
                    model.AddOns.Add(AddOnMap.ToModel(a.AddOnIDSource, a.ProductAddOnID));
                }
            }

            if (!Equals(entity.Attributes, null))
            {
                foreach (var a in entity.Attributes)
                {
                    model.Attributes.Add(AttributeMap.ToModel(a));
                }
            }

            model.BundledProductIds = String.Join(",", entity.ParentChildProductCollectionGetByChildProductID.Select(bundle => bundle.ChildProductID.ToString()));

            if (!Equals(entity.ProductFrquentlyBoughtTogetherCollection, null))
            {
                foreach (var t in entity.ProductFrquentlyBoughtTogetherCollection)
                {
                    model.FrequentlyBoughtTogether.Add(CrossSellMap.ToModel(t));
                }
            }

            if (!Equals(entity.ProductCategoryCollection, null))
            {
                foreach (var productCategory in entity.ProductCategoryCollection)
                {
                    if (!Equals(productCategory, null))
                    {
                        model.Categories.Add(CategoryMap.ToModel(productCategory.CategoryIDSource));
                        model.ProductCategories.Add(ProductCategoryMap.ToModel(productCategory));
                    }
                }
            }

            if (!Equals(entity.ProductCrossSellCollection, null))
            {
                foreach (var c in entity.ProductCrossSellCollection)
                {
                    model.CrossSells.Add(CrossSellMap.ToModel(c));
                }
            }

            if (!Equals(entity.ProductHighlightCollection, null))
            {
                foreach (var h in entity.ProductHighlightCollection)
                {
                    model.Highlights.Add(HighlightMap.ToModel(h.HighlightIDSource));
                }
            }

            if (!Equals(entity.ProductImageCollection, null))
            {
                foreach (var i in entity.ProductImageCollection)
                {
                    model.Images.Add(ImageMap.ToModel(i));
                }
            }

            if (!Equals(entity.PromotionCollection, null))
            {
                foreach (var p in entity.PromotionCollection)
                {
                    model.Promotions.Add(PromotionMap.ToModel(p));
                }
            }

            if (!Equals(entity.ReviewCollection, null))
            {
                foreach (var r in entity.ReviewCollection)
                {
                    model.Reviews.Add(ReviewMap.ToModel(r));
                }
            }

            if (!Equals(entity.SKUCollection, null))
            {
                foreach (var s in entity.SKUCollection)
                {
                    model.Skus.Add(SkuMap.ToModel(s));
                }
            }

            if (!Equals(entity.ProductTierCollection, null))
            {
                foreach (var t in entity.ProductTierCollection)
                {
                    model.Tiers.Add(TierMap.ToModel(t));
                }
            }

            if (!Equals(entity.ProductYouMayAlsoLikeCollection, null))
            {
                foreach (var t in entity.ProductYouMayAlsoLikeCollection)
                {
                    model.YouMayAlsoLike.Add(CrossSellMap.ToModel(t));
                }
            }

            // If we have a selected SKU then create a simple base product so that
            // we can call ApplyPromotion() on it to set call for pricing properties
            if (model.SelectedSku != null)
            {
                var znodeProductBase = new ZNodeProductBase();
                znodeProductBase.ProductID = model.ProductId;
                znodeProductBase.IsPromotionApplied = false;
                znodeProductBase.SelectedSKUvalue = new ZNodeSKUEntity();
                znodeProductBase.SelectedSKUvalue.SKUID = model.SelectedSku.SkuId;
                znodeProductBase.ApplyPromotion();

                model.CallForPricing = znodeProductBase.CallForPricing;
                model.PromotionMessage = znodeProductBase.CallMessage;
            }

            return model;
        }

        public static Product ToEntity(ProductModel model)
        {
            if (model == null)
            {
                return null;
            }

            var entity = new Product
            {
                AccountID = model.AccountId,
                ActiveInd = model.IsActive,
                AdditionalInformation = model.AdditionalInfo,
                AdditionalInfoLink = model.AdditionalInfoLink,
                AdditionalInfoLinkLabel = model.AdditionalInfoLinkLabel,
                AffiliateUrl = model.AffiliateUrl,
                AllowBackOrder = model.AllowBackOrder,
                BackOrderMsg = model.BackOrderMessage,
                BeginActiveDate = model.BeginActiveDate,
                CallForPricing = model.CallForPricing,
                CategorySpecial = model.IsCategorySpecial,
                CreateDate = model.CreateDate,
                Custom1 = model.Custom1,
                Custom2 = model.Custom2,
                Custom3 = model.Custom3,
                Description = model.Description,
                DisplayOrder = model.DisplayOrder,
                DownloadLink = model.DownloadLink,
                DropShipEmailID = model.DropShipEmailAddress,
                DropShipInd = model.AllowDropShip,
                EndActiveDate = model.EndActiveDate,
                ExpirationFrequency = model.ExpirationFrequency,
                ExpirationPeriod = model.ExpirationPeriod,
                ExternalID = model.ExternalId,
                FeaturedInd = model.IsFeatured,
                FeaturesDesc = model.FeaturesDescription,
                Franchisable = model.IsFranchisable,
                FreeShippingInd = model.AllowFreeShipping,
                Height = model.Height,
                HomepageSpecial = model.IsHomepageSpecial,
                ImageAltTag = model.ImageAltTag,
                ImageFile = model.ImageFile,
                InStockMsg = model.InStockMessage,
                InventoryDisplay = model.InventoryDisplay,
                IsShippable = model.IsShippable,
                Keywords = model.Keywords,
                Length = model.Length,
                ManufacturerID = model.ManufacturerId,
                MaxQty = model.MaxSelectableQuantity,
                MinQty = model.MinSelectableQuantity,
                Name = model.Name,
                NewProductInd = model.IsNewProduct,
                OutOfStockMsg = model.OutOfStockMessage,
                PortalID = model.PortalId,
                ProductID = model.ProductId,
                ProductNum = model.ProductNumber,
                ProductTypeID = model.ProductTypeId,
                PromotionPrice = model.PromotionPrice,
                RecurringBillingFrequency = model.RecurringBillingFrequency,
                RecurringBillingInd = model.AllowRecurringBilling,
                RecurringBillingInitialAmount = model.RecurringBillingInitialAmount,
                RecurringBillingInstallmentInd = model.AllowRecurringBillingInstallment,
                RecurringBillingPeriod = model.RecurringBillingPeriod,
                RecurringBillingTotalCycles = model.RecurringBillingTotalCycles,
                RetailPrice = model.RetailPrice,
                ReviewStateID = model.ReviewStateId,
                SalePrice = model.SalePrice,
                SEODescription = model.SeoDescription,
                SEOKeywords = model.SeoKeywords,
                SEOTitle = model.SeoTitle,
                SEOURL = model.SeoUrl,
                ShipEachItemSeparately = model.ShipEachItemSeparately,
                ShippingRate = model.ShippingRate,
                ShippingRuleTypeID = model.ShippingRuleTypeId,
                ShipSeparately = model.ShipSeparately,
                ShortDescription = model.ShortDescription,
                Specifications = model.Specifications,
                SupplierID = model.SupplierId,
                TaxClassID = model.TaxClassId,
                TieredPrice = model.TieredPrice,
                TrackInventoryInd = model.TrackInventory,
                UpdateDte = model.UpdateDate,
                WebServiceDownloadDte = model.WebServiceDownloadDate,
                Weight = model.Weight,
                Width = model.Width,
                WholesalePrice = model.WholesalePrice
            };

            return entity;
        }

        // Use a database product because our cookie product being submitted only has data needed for display fullblown product
        public static ZNodeProductBase ToZnodeProductBase(int productId, string selectedSku, int[] selectedAddOnValueIds = null, Dictionary<int, string> addOnValuesCustomText = null, string externalAccountId = null, decimal productDiscountAmount = 0)
        {
            var znodeProduct = ZNodeProductBase.Create(productId, 0, 0, true, externalAccountId, false);

            if (!String.IsNullOrEmpty(selectedSku))
            {
                znodeProduct.SelectedSKU = ZNodeSKU.CreateBySKU(selectedSku, externalAccountId);
            }

            if (selectedAddOnValueIds != null)
            {
                znodeProduct.SelectedAddOns = ZNodeAddOnList.CreateByProductAndAddOns(productId, string.Join(",", selectedAddOnValueIds));
                znodeProduct.SelectedAddOns.SelectedAddOnValueIds = string.Join(",", selectedAddOnValueIds);
                znodeProduct.SelectedAddOns.SelectedAddOnValueCustomText = addOnValuesCustomText;

                if (addOnValuesCustomText != null)
                {
                    foreach (var addOnValueEntity in znodeProduct.SelectedAddOns.AddOnCollection.Cast<ZNodeAddOnEntity>()
                                                                 .Where(addOnEntity => addOnEntity.DisplayType.Equals("TextBox", StringComparison.OrdinalIgnoreCase))
                                                                 .SelectMany(addOnEntity => addOnEntity.AddOnValueCollection.Cast<ZNodeAddOnValueEntity>())
                                                                 .Where(addOnValueEntity => addOnValuesCustomText.ContainsKey(addOnValueEntity.AddOnValueID)))
                    {
                        addOnValueEntity.CustomText = addOnValuesCustomText[addOnValueEntity.AddOnValueID];
                        addOnValueEntity.DiscountAmount = productDiscountAmount;
                    }
                    foreach (var addOnValueEntity in znodeProduct.SelectedAddOns.AddOnCollection.Cast<ZNodeAddOnEntity>()
                                                                 .SelectMany(addOnEntity => addOnEntity.AddOnValueCollection.Cast<ZNodeAddOnValueEntity>())
                                                                 .Where(addOnValueEntity => addOnValuesCustomText.ContainsKey(addOnValueEntity.AddOnValueID)))
                    {
                        addOnValueEntity.DiscountAmount = productDiscountAmount;
                    }
                }
            }

            znodeProduct.ApplyPromotion();

            return znodeProduct;
        }

        public static Product ToEntity(Product model)
        {
            if (model == null)
            {
                return null;
            }

            var entity = new Product
            {
                AccountID = model.AccountID,
                ActiveInd = model.ActiveInd,
                AdditionalInformation = model.AdditionalInformation,
                AdditionalInfoLink = model.AdditionalInfoLink,
                AdditionalInfoLinkLabel = model.AdditionalInfoLinkLabel,
                AffiliateUrl = model.AffiliateUrl,
                AllowBackOrder = model.AllowBackOrder,
                BackOrderMsg = model.BackOrderMsg,
                BeginActiveDate = model.BeginActiveDate,
                CallForPricing = model.CallForPricing,
                CategorySpecial = model.CategorySpecial,
                CreateDate = model.CreateDate,
                Custom1 = model.Custom1,
                Custom2 = model.Custom2,
                Custom3 = model.Custom3,
                Description = model.Description,
                DisplayOrder = model.DisplayOrder,
                DownloadLink = model.DownloadLink,
                DropShipEmailID = model.DropShipEmailID,
                DropShipInd = model.DropShipInd,
                EndActiveDate = model.EndActiveDate,
                ExpirationFrequency = model.ExpirationFrequency,
                ExpirationPeriod = model.ExpirationPeriod,
                ExternalID = model.ExternalID,
                FeaturedInd = model.FeaturedInd,
                FeaturesDesc = model.FeaturesDesc,
                Franchisable = model.Franchisable,
                FreeShippingInd = model.FreeShippingInd,
                Height = model.Height,
                HomepageSpecial = model.HomepageSpecial,
                ImageAltTag = model.ImageAltTag,
                ImageFile = model.ImageFile,
                InStockMsg = model.InStockMsg,
                InventoryDisplay = model.InventoryDisplay,
                IsShippable = model.IsShippable,
                Keywords = model.Keywords,
                Length = model.Length,
                ManufacturerID = model.ManufacturerID,
                MaxQty = model.MaxQty,
                MinQty = model.MinQty,
                Name = model.Name,
                NewProductInd = model.NewProductInd,
                OutOfStockMsg = model.OutOfStockMsg,
                PortalID = model.PortalID,
                ProductID = model.ProductID,
                ProductNum = model.ProductNum,
                ProductTypeID = model.ProductTypeID,
                PromotionPrice = model.PromotionPrice,
                RecurringBillingFrequency = model.RecurringBillingFrequency,
                RecurringBillingInd = model.RecurringBillingInd,
                RecurringBillingInitialAmount = model.RecurringBillingInitialAmount,
                RecurringBillingInstallmentInd = model.RecurringBillingInstallmentInd,
                RecurringBillingPeriod = model.RecurringBillingPeriod,
                RecurringBillingTotalCycles = model.RecurringBillingTotalCycles,
                RetailPrice = model.RetailPrice,
                ReviewStateID = model.ReviewStateID,
                SalePrice = model.SalePrice,
                SEODescription = model.SEODescription,
                SEOKeywords = model.SEOKeywords,
                SEOTitle = model.SEOTitle,
                SEOURL = model.SEOURL,
                ShipEachItemSeparately = model.ShipEachItemSeparately,
                ShippingRate = model.ShippingRate,
                ShippingRuleTypeID = model.ShippingRuleTypeID,
                ShipSeparately = model.ShipSeparately,
                ShortDescription = model.ShortDescription,
                Specifications = model.Specifications,
                SupplierID = model.SupplierID,
                TaxClassID = model.TaxClassID,
                TieredPrice = model.TieredPrice,
                TrackInventoryInd = model.TrackInventoryInd,
                UpdateDte = model.UpdateDte,
                WebServiceDownloadDte = model.WebServiceDownloadDte,
                Weight = model.Weight,
                Width = model.Width,
                WholesalePrice = model.WholesalePrice
            };

            return entity;
        }

        public static ProductModel ToModel(DataSet dataset)
        {
            ProductModel item = new ProductModel();
            if (dataset != null && dataset.Tables.Count > 0 && dataset.Tables[0].Rows.Count > 0)
            {
                DataRow dr = dataset.Tables[0].Rows[0];
                item.ProductId = (dr["ProductID"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["ProductID"]);
                item.Name = dr["Name"].ToString();
                item.ShortDescription = dr["ShortDescription"].ToString();
                item.Description = dr["Description"].ToString();
                item.FeaturesDescription = dr["FeaturesDesc"].ToString();
                item.Specifications = dr["Specifications"].ToString();
                item.AdditionalInfo = dr["AdditionalInformation"].ToString();
                item.ProductNumber = dr["ProductNum"].ToString();
                item.ProductTypeId = (dr["ProductID"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["ProductTypeID"]);
                item.RetailPrice = (dr["RetailPrice"] == DBNull.Value) ? 0 : Convert.ToDecimal(dr["RetailPrice"]);
                item.SalePrice = (dr["SalePrice"] == DBNull.Value) ? null : (decimal?)(dr["SalePrice"]);
                item.WholesalePrice = (dr["WholesalePrice"] == DBNull.Value) ? null : (decimal?)(dr["WholesalePrice"]);
                item.ImageFile = dr["ImageFile"].ToString();
                item.ImageAltTag = dr["ImageAltTag"].ToString();
                item.Weight = (dr["Weight"] == DBNull.Value) ? null : (decimal?)(dr["Weight"]);
                item.Length = (dr["Length"] == DBNull.Value) ? null : (decimal?)(dr["Length"]);
                item.Width = (dr["Width"] == DBNull.Value) ? null : (decimal?)(dr["Width"]);
                item.Height = (dr["Height"] == DBNull.Value) ? null : (decimal?)(dr["Height"]);
                item.DisplayOrder = (dr["DisplayOrder"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["DisplayOrder"]);
                item.IsActive = (dr["ActiveInd"] == DBNull.Value) ? false : Convert.ToBoolean(dr["ActiveInd"]);
                item.CallForPricing = (dr["CallForPricing"] == DBNull.Value) ? false : Convert.ToBoolean(dr["CallForPricing"]);
                item.IsHomepageSpecial = (dr["HomepageSpecial"] == DBNull.Value) ? false : Convert.ToBoolean(dr["HomepageSpecial"]);
                item.IsCategorySpecial = (dr["CategorySpecial"] == DBNull.Value) ? false : Convert.ToBoolean(dr["CategorySpecial"]);
                item.ManufacturerId = (dr["ManufacturerID"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["ManufacturerID"]);
                item.AdditionalInfoLink = dr["AdditionalInfoLink"].ToString();
                item.AdditionalInfoLinkLabel = dr["AdditionalInfoLinkLabel"].ToString();
                item.ShippingRuleTypeId = (dr["ShippingRuleTypeID"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["ShippingRuleTypeID"]);
                item.ShippingRate = (dr["ShippingRate"] == DBNull.Value) ? null : (decimal?)(dr["ShippingRate"]);
                item.DownloadLink = dr["DownloadLink"].ToString();
                item.AllowFreeShipping = (dr["FreeShippingInd"] == DBNull.Value) ? false : Convert.ToBoolean(dr["FreeShippingInd"]);
                item.MaxSelectableQuantity = (dr["MaxQty"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["MaxQty"]);
                item.SupplierId = (dr["SupplierID"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["SupplierID"]);
                item.TaxClassId = (dr["TaxClassID"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["TaxClassID"]);
                item.MinSelectableQuantity = (dr["MinQty"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["MinQty"]);
                item.ReviewStateId = (dr["ReviewStateID"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["ReviewStateID"]);
                item.PortalId = (dr["PortalID"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["PortalID"]);
                item.AccountId = (dr["AccountID"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["AccountID"]);
                item.ExpirationPeriod = (dr["ExpirationPeriod"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["ExpirationPeriod"]);
                item.ExpirationFrequency = (dr["ExpirationFrequency"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["ExpirationFrequency"]);
                item.ExternalId = dr["ExternalID"].ToString();
                item.SupplierName = dr["SUPPLIER NAME"].ToString();
                item.SupplierTypeName = dr["SupplierTypeName"].ToString();
                item.ManufacturerName = dr["MANUFACTURER NAME"].ToString();
                item.ProductTypeName = dr["PRODUCTTYPE NAME"].ToString();
                item.SkuName = dr["SKU"].ToString();
                item.SkuId = (dr["SKUID"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["SKUID"]);
                item.TaxClassName = dr["TaxClassName"].ToString();
                item.ShippingRule = dr["ShippingRuleType"].ToString();
                item.AttributeIds = dr["AttributeId"].ToString();
                item.QuantityOnHand = (dr["QuantityOnHand"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["QuantityOnHand"]);
                item.ReorderLevel = (dr["ReorderLevel"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["ReorderLevel"]);
                item.IsNewProduct = (dr["NewProductInd"] == DBNull.Value) ? false : Convert.ToBoolean(dr["NewProductInd"]);
                item.IsFeatured = (dr["FeaturedInd"] == DBNull.Value) ? false : Convert.ToBoolean(dr["FeaturedInd"]);
                item.IsFranchisable = (dr["Franchisable"] == DBNull.Value) ? false : Convert.ToBoolean(dr["Franchisable"]);
                item.TrackInventory = (dr["TrackInventoryInd"] == DBNull.Value) ? false : Convert.ToBoolean(dr["TrackInventoryInd"]);
                item.AllowBackOrder = (dr["AllowBackOrder"] == DBNull.Value) ? false : Convert.ToBoolean(dr["AllowBackOrder"]);
                item.OutOfStockMessage = dr["OutOfStockMsg"].ToString();
                item.BackOrderMessage = dr["BackOrderMsg"].ToString();
                item.AllowRecurringBilling = (dr["RecurringBillingInd"] == DBNull.Value) ? false : Convert.ToBoolean(dr["RecurringBillingInd"]);
                item.RecurringBillingInitialAmount = (dr["RecurringBillingInitialAmount"] == DBNull.Value) ? 0 : Convert.ToDecimal(dr["RecurringBillingInitialAmount"]);
                item.RecurringBillingPeriod = dr["RecurringBillingFrequency"].ToString();
                item.SeoTitle = dr["SEOTitle"].ToString();
                item.SeoKeywords = dr["SEOKeywords"].ToString();
                item.SeoDescription = dr["SEODescription"].ToString();
                item.SeoUrl = dr["SEOURL"].ToString();
                item.AffiliateUrl = dr["AffiliateUrl"].ToString();
                item.IsGiftCard = (dr["IsGiftCard"] == DBNull.Value) ? false : Convert.ToBoolean(dr["IsGiftCard"]);
                item.AttributeCount = (dr["AttributeCount"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["AttributeCount"]);
                item.IsChildProduct = (dr["IsChildProduct"] == DBNull.Value) ? false : Convert.ToBoolean(dr["IsChildProduct"]);
                item.IsParentProductId = (dr["IsParentProduct"] == DBNull.Value) ? false : Convert.ToBoolean(dr["IsParentProduct"]);
            }
            return item;
        }

        public static ProductTier ToProductTierEntity(ProductTierPricingModel model)
        {
            if (Equals(model, null))
            {
                return null;
            }
            ProductTier entity = new ProductTier();
            entity.ProductID = model.ProductId;
            entity.ProfileID = model.ProfileID;
            entity.TierStart = model.TierStart;
            entity.TierEnd = model.TierEnd;
            entity.ProductTierID = model.ProductTierID;
            entity.Price = model.Price;
            return entity;
        }

        public static ProductTierPricingModel ToProductTierModel(ProductTier entity)
        {
            if (Equals(entity, null))
            {
                return null;
            }

            ProductTierPricingModel model = new ProductTierPricingModel();
            model.ProductTierID = entity.ProductTierID;
            entity.ProductID = entity.ProductID;
            entity.ProfileID = entity.ProfileID;
            entity.TierStart = entity.TierStart;
            entity.TierEnd = entity.TierEnd;
            return model; ;
        }

        public static DigitalAsset ToDigitalAssetEntity(DigitalAssetModel model)
        {
            if (Equals(model, null))
            {
                return null;
            }
            DigitalAsset entity = new DigitalAsset();
            entity.ProductID = model.ProductID;
            entity.DigitalAsset = model.DigitalAsset;
            entity.DigitalAssetID = model.DigitalAssetID;
            return entity;
        }

        public static DigitalAssetModel ToDigitalAssetModel(DigitalAsset entity)
        {
            if (Equals(entity, null))
            {
                return null;
            }
            DigitalAssetModel model = new DigitalAssetModel();
            model.ProductID = entity.ProductID;
            model.DigitalAsset = entity.DigitalAsset;
            model.DigitalAssetID = entity.DigitalAssetID;
            return model;
        }

        public static ProductModel ToModel(ZNode.Libraries.ECommerce.Catalog.ZNodeProduct entity)
        {
            if (entity == null)
            {
                return null;
            }

            var imageUtility = new ZNodeImage();

            var model = new ProductModel
            {
                AccountId = entity.AccountID,
                AdditionalInfo = entity.AdditionalInformation,
                AdditionalInfoLink = entity.AdditionalInfoLink,
                AdditionalInfoLinkLabel = entity.AdditionalInfoLinkLabel,
                AffiliateUrl = entity.AffiliateUrl,
                AllowBackOrder = entity.AllowBackOrder,
                AllowFreeShipping = entity.FreeShippingInd,
                AllowRecurringBilling = entity.RecurringBillingInd,
                AllowRecurringBillingInstallment = entity.RecurringBillingInstallmentInd,
                BackOrderMessage = entity.BackOrderMsg,
                CallForPricing = entity.CallForPricing,
                Custom1 = entity.Custom1,
                Custom2 = entity.Custom2,
                Custom3 = entity.Custom3,
                Description = entity.Description,
                DisplayOrder = entity.DisplayOrder,
                DownloadLink = entity.DownloadLink,
                ExpirationFrequency = entity.ExpirationFrequency,
                ExpirationPeriod = entity.ExpirationPeriod,
                FeaturesDescription = entity.FeaturesDesc,
                Height = entity.Height,
                ImageAltTag = entity.ImageAltTag,
                ImageFile = entity.ImageFile,
                ImageMediumPath = imageUtility.GetImageHttpPathMedium(entity.ImageFile),
                ImageLargePath = imageUtility.GetImageHttpPathLarge(entity.ImageFile),
                ImageSmallPath = imageUtility.GetImageHttpPathSmall(entity.ImageFile),
                ImageSmallThumbnailPath = imageUtility.GetImageHttpPathSmallThumbnail(entity.ImageFile),
                ImageThumbnailPath = imageUtility.GetImageHttpPathThumbnail(entity.ImageFile),
                InStockMessage = entity.InStockMsg,
                IsActive = entity.IsActive,
                IsCategorySpecial = entity.CategorySpecial,
                IsFeatured = entity.FeaturedInd,
                IsFranchisable = entity.Franchisable,
                IsHomepageSpecial = entity.HomepageSpecial,
                IsNewProduct = entity.NewProductInd,
                Length = entity.Length,
                ManufacturerId = entity.ManufacturerID,
                MaxSelectableQuantity = entity.MaxQty,
                MinSelectableQuantity = entity.MinQty,
                Name = HttpUtility.HtmlDecode(entity.Name),
                OutOfStockMessage = entity.OutOfStockMsg,
                PortalId = entity.PortalID,
                ProductId = entity.ProductID,
                ProductNumber = entity.ProductNum,
                ProductTypeId = entity.ProductTypeID,
                ProductUrl = ZNodeSEOUrl.MakeUrlForMvc(entity.ProductID.ToString(), SEOUrlType.Product, entity.SEOURL),
                RecurringBillingFrequency = entity.RecurringBillingFrequency,
                RecurringBillingInitialAmount = entity.RecurringBillingInitialAmount,
                RecurringBillingPeriod = entity.RecurringBillingPeriod,
                RecurringBillingTotalCycles = entity.RecurringBillingTotalCycles,
                RetailPrice = entity.RetailPrice,
                SalePrice = entity.SalePrice,
                SeoDescription = string.IsNullOrEmpty(entity.SEODescription) ? ZNodeConfigManager.SiteConfig.SeoDefaultProductDescription : entity.SEODescription,
                SeoKeywords = string.IsNullOrEmpty(entity.SEOKeywords) ? ZNodeConfigManager.SiteConfig.SeoDefaultProductKeyword : entity.SEOKeywords,
                SeoTitle = string.IsNullOrEmpty(entity.SEOTitle) ? ZNodeConfigManager.SiteConfig.SeoDefaultProductTitle : entity.SEOTitle,
                SeoUrl = entity.SEOURL,
                ShippingRate = entity.ShippingRate,
                ShippingRuleTypeId = entity.ShippingRuleTypeID,
                ShipSeparately = entity.ShipSeparately,
                ShortDescription = entity.ShortDescription,
                Specifications = entity.Specifications,
                SupplierId = entity.SupplierID,
                TaxClassId = entity.TaxClassID,
                TieredPrice = entity.TieredPrice,
                TrackInventory = entity.TrackInventoryInd,
                Weight = entity.Weight,
                WholesalePrice = entity.WholesalePrice
            };
            // If we have a selected SKU then create a simple base product so that
            // we can call ApplyPromotion() on it to set call for pricing properties
            if (model.SelectedSku != null)
            {
                var znodeProductBase = new ZNodeProductBase();
                znodeProductBase.ProductID = model.ProductId;
                znodeProductBase.IsPromotionApplied = false;
                znodeProductBase.SelectedSKUvalue = new ZNodeSKUEntity();
                znodeProductBase.SelectedSKUvalue.SKUID = model.SelectedSku.SkuId;
                znodeProductBase.ApplyPromotion();

                model.CallForPricing = znodeProductBase.CallForPricing;
                model.PromotionMessage = znodeProductBase.CallMessage;
            }

            return model;
        }
    }


}
