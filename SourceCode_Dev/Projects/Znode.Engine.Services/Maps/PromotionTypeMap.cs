﻿using System;
using Znode.Engine.Api.Models;
using Znode.Engine.Promotions;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
    public static class PromotionTypeMap
    {
        public static PromotionTypeModel ToModel(DiscountType entity)
        {
            if (entity == null)
            {
                return null;
            }

            var model = new PromotionTypeModel
            {
                ClassName = entity.ClassName,
                ClassType = entity.ClassType,
                Description = entity.Description,
                IsActive = entity.ActiveInd,
                Name = entity.Name,
                PromotionTypeId = entity.DiscountTypeID
            };

            return model;
        }

        public static DiscountType ToEntity(PromotionTypeModel model)
        {
            if (model == null)
            {
                return null;
            }

            var entity = new DiscountType
            {
                ActiveInd = model.IsActive,
                ClassName = model.ClassName,
                ClassType = model.ClassType,
                Description = model.Description,
                DiscountTypeID = model.PromotionTypeId,
                Name = model.Name
            };

            return entity;
        }

        /// <summary>
        /// Znode version 8.0
        /// Convert IZnodePromotionType to PromotionTypeModel
        /// </summary>
        /// <param name="znodePromotionType">IZnodePromotionType znodePromotionType</param>
        /// <returns>Returns PromotionTypeModel</returns>
        public static PromotionTypeModel ToModel(IZnodePromotionType znodePromotionType)
        {
            if (Equals(znodePromotionType, null))
            {
                return null;
            }
            var model = new PromotionTypeModel
            {
                ClassName = znodePromotionType.ClassName,
                Description = znodePromotionType.Description,
                Name = znodePromotionType.Name,
                ClassType = GetClassTypeByClassName(znodePromotionType.ClassName),
            };
            return model;
        }

        /// <summary>
        /// Get class type by class name 
        /// </summary>
        /// <param name="className">string class name</param>
        /// <returns>Return class type of class name</returns>
        private static string GetClassTypeByClassName(string className)
        {
            var classType = String.Empty;
            var promotionTypes = ZnodePromotionManager.GetAvailablePromotionTypes();
            foreach (var t in promotionTypes)
            {
                if (Equals(t.ClassName, className))
                {
                    if (Equals(t.GetType().BaseType, typeof(ZnodeCartPromotionType))) { classType = "CART"; }
                    if (Equals(t.GetType().BaseType, typeof(ZnodePricePromotionType))) { classType = "PRICE"; }
                    if (Equals(t.GetType().BaseType, typeof(ZnodeProductPromotionType))) { classType = "PRODUCT"; }
                }
            }
            return classType;
        }
    }
}
