﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Services.Maps
{
    public static class PortalMap
    {
        /// <summary>
        /// Converts Portal Entity to Portal Model.
        /// </summary>
        /// <param name="entity">Portal Entity.</param>
        /// <returns>Portal Model.</returns>
        public static PortalModel ToModel(Portal entity)
        {
            if (Equals(entity, null))
            {
                return null;
            }

            var model = new PortalModel
            {
                AdminEmail = entity.AdminEmail,
                CompanyName = entity.CompanyName,
                CurrencyTypeId = entity.CurrencyTypeID,
                CustomerServiceEmail = entity.CustomerServiceEmail,
                CustomerServicePhoneNumber = entity.CustomerServicePhoneNumber,
                DefaultAnonymousProfileId = entity.DefaultAnonymousProfileID,
                DefaultOrderStateId = entity.DefaultOrderStateID,
                DefaultProductReviewStateId = entity.DefaultProductReviewStateID,
                DefaultRegisteredProfileId = entity.DefaultRegisteredProfileID,
                DefaultReviewStatus = entity.DefaultReviewStatus,
                DimensionUnit = entity.DimensionUnit,
                EmailListDefaultList = entity.EmailListDefaultList,
                EmailListPassword = entity.EmailListPassword,
                EmailListUsername = entity.EmailListLogin,
                EnableAddressValidation = entity.EnableAddressValidation,
                EnableCustomerPricing = entity.EnableCustomerPricing,
                EnablePims = entity.EnablePIMS,
                ExternalId = entity.ExternalID,
                FedExAccountNumber = entity.FedExAccountNumber,
                FedExAddInsurance = entity.FedExAddInsurance,
                FedExClientProductId = entity.FedExClientProductId,
                FedExClientProductVersion = entity.FedExClientProductVersion,
                FedExCspKey = entity.FedExCSPKey,
                FedExCspPassword = entity.FedExCSPPassword,
                FedExDropoffType = entity.FedExDropoffType,
                FedExMeterNumber = entity.FedExMeterNumber,
                FedExPackagingType = entity.FedExPackagingType,
                FedExProductionKey = entity.FedExProductionKey,
                FedExSecurityCode = entity.FedExSecurityCode,
                FedExUseDiscountRate = entity.FedExUseDiscountRate,
                GoogleAnalyticsCode = entity.GoogleAnalyticsCode,
                ImageNotAvailablePath = entity.ImageNotAvailablePath,
                InclusiveTax = entity.InclusiveTax,
                IsActive = entity.ActiveInd,
                IsShippingTaxable = entity.ShippingTaxable,
                LocaleId = entity.LocaleID,
                LogoPath = entity.LogoPath,
                MasterPage = entity.MasterPage,
                MaxCatalogCategoryDisplayThumbnails = entity.MaxCatalogCategoryDisplayThumbnails,
                MaxCatalogDisplayColumns = entity.MaxCatalogDisplayColumns,
                MaxCatalogDisplayItems = entity.MaxCatalogDisplayItems,
                MaxCatalogItemCrossSellWidth = entity.MaxCatalogItemCrossSellWidth,
                MaxCatalogItemLargeWidth = entity.MaxCatalogItemLargeWidth,
                MaxCatalogItemMediumWidth = entity.MaxCatalogItemMediumWidth,
                MaxCatalogItemSmallThumbnailWidth = entity.MaxCatalogItemSmallThumbnailWidth,
                MaxCatalogItemSmallWidth = entity.MaxCatalogItemSmallWidth,
                MaxCatalogItemThumbnailWidth = entity.MaxCatalogItemThumbnailWidth,
                MobileTheme = entity.MobileTheme,
                OrderReceiptAffiliateJavascript = entity.OrderReceiptAffiliateJavascript,
                PersistentCartEnabled = entity.PersistentCartEnabled,
                PortalId = entity.PortalID,
                RequireValidatedAddress = entity.RequireValidatedAddress,
                SalesEmail = entity.SalesEmail,
                SalesPhoneNumber = entity.SalesPhoneNumber,
                SeoDefaultCategoryDescription = entity.SeoDefaultCategoryDescription,
                SeoDefaultCategoryKeyword = entity.SeoDefaultCategoryKeyword,
                SeoDefaultCategoryTitle = entity.SeoDefaultCategoryTitle,
                SeoDefaultContentDescription = entity.SeoDefaultContentDescription,
                SeoDefaultContentKeyword = entity.SeoDefaultContentKeyword,
                SeoDefaultContentTitle = entity.SeoDefaultContentTitle,
                SeoDefaultProductDescription = entity.SeoDefaultProductDescription,
                SeoDefaultProductKeyword = entity.SeoDefaultProductKeyword,
                SeoDefaultProductTitle = entity.SeoDefaultProductTitle,
                ShippingOriginAddress1 = entity.ShippingOriginAddress1,
                ShippingOriginAddress2 = entity.ShippingOriginAddress2,
                ShippingOriginCity = entity.ShippingOriginCity,
                ShippingOriginCountryCode = entity.ShippingOriginCountryCode,
                ShippingOriginPhone = entity.ShippingOriginPhone,
                ShippingOriginStateCode = entity.ShippingOriginStateCode,
                ShippingOriginZipCode = entity.ShippingOriginZipCode,
                ShopByPriceIncrement = entity.ShopByPriceIncrement,
                ShopByPriceMax = entity.ShopByPriceMax,
                ShopByPriceMin = entity.ShopByPriceMin,
                ShowAlternateImageInCategory = entity.ShowAlternateImageInCategory,
                ShowSwatchInCategory = entity.ShowSwatchInCategory,
                SiteWideAnalyticsJavascript = entity.SiteWideAnalyticsJavascript,
                SiteWideBottomJavascript = entity.SiteWideBottomJavascript,
                SiteWideTopJavascript = entity.SiteWideTopJavascript,
                SmtpPassword = entity.SMTPPassword,
                SmtpPort = entity.SMTPPort,
                SmtpServer = entity.SMTPServer,
                SmtpUsername = entity.SMTPUserName,
                SplashCategoryId = entity.SplashCategoryID,
                SplashImageFile = entity.SplashImageFile,
                StoreName = entity.StoreName,
                TimeZoneOffset = entity.TimeZoneOffset,
                UpsKey = entity.UPSKey,
                UpsPassword = entity.UPSPassword,
                UpsUsername = entity.UPSUserName,
                UseDynamicDisplayOrder = entity.UseDynamicDisplayOrder,
                UseSsl = entity.UseSSL,
                WeightUnit = entity.WeightUnit,
                IsMultipleCouponAllowed = entity.IsMultipleCouponAllowed,
                IsEnableCompare = entity.IsEnableCompare,
                CompareType = entity.CompareType,
                IsEnableSinglePageCheckout = entity.IsEnableSinglePageCheckout,
                MobileLogoPath = entity.MobileLogoPath,
            };

            if (!Equals(entity.CurrencyTypeIDSource, null))
            {
                model.CurrencyTypeModel = CurrencyTypeMap.ToModel(entity.CurrencyTypeIDSource);
            }

            foreach (var catalog in entity.CatalogCollection)
            {
                model.Catalogs.Add(CatalogMap.ToModel(catalog));
            }

            foreach (var portalCatalog in entity.PortalCatalogCollection)
            {
                model.PortalCatalogs.Add(PortalCatalogMap.ToModel(portalCatalog));
            }

            foreach (var pc in entity.PortalCountryCollection)
            {
                model.PortalCountries.Add(PortalCountryMap.ToModel(pc));
            }

            model.ThemeName = !Equals(model.PortalCatalogs, null) && model.PortalCatalogs.Count > 0 && !Equals(model.PortalCatalogs[0], null) ? model.PortalCatalogs[0].ThemeName : string.Empty;
            model.CssName = !Equals(model.PortalCatalogs, null) && model.PortalCatalogs.Count > 0 && !Equals(model.PortalCatalogs[0], null) ? model.PortalCatalogs[0].CSSName : string.Empty;

            return model;
        }

        /// <summary>
        /// Converts Portal Model To Portal Entiy.
        /// </summary>
        /// <param name="model">Portal Model.</param>
        /// <returns>Portal Entity.</returns>
        public static Portal ToEntity(PortalModel model)
        {
            if (model == null)
            {
                return null;
            }

            ZNodeEncryption encryption = new ZNodeEncryption();
            model.FedExAccountNumber = !string.IsNullOrEmpty(model.FedExAccountNumber) ? encryption.EncryptData(model.FedExAccountNumber) : string.Empty;
            model.FedExMeterNumber = !string.IsNullOrEmpty(model.FedExMeterNumber) ? encryption.EncryptData(model.FedExMeterNumber) : string.Empty;
            model.FedExProductionKey = !string.IsNullOrEmpty(model.FedExProductionKey) ? encryption.EncryptData(model.FedExProductionKey) : string.Empty;
            model.FedExSecurityCode = !string.IsNullOrEmpty(model.FedExSecurityCode) ? encryption.EncryptData(model.FedExSecurityCode) : string.Empty;
            model.UpsUsername = !string.IsNullOrEmpty(model.UpsUsername) ? encryption.EncryptData(model.UpsUsername) : string.Empty;
            model.UpsPassword = !string.IsNullOrEmpty(model.UpsPassword) ? encryption.EncryptData(model.UpsPassword) : string.Empty;
            model.UpsKey = !string.IsNullOrEmpty(model.UpsKey) ? encryption.EncryptData(model.UpsKey) : string.Empty;
            model.SmtpPassword = !string.IsNullOrEmpty(model.SmtpPassword) ? encryption.EncryptData(model.SmtpPassword) : string.Empty;
            model.SmtpUsername = !string.IsNullOrEmpty(model.SmtpUsername) ? encryption.EncryptData(model.SmtpUsername) : string.Empty;

            var entity = new Portal
            {
                ActiveInd = model.IsActive,
                AdminEmail = model.AdminEmail,
                CompanyName = model.CompanyName,
                CurrencyTypeID = model.CurrencyTypeId,
                CustomerServiceEmail = model.CustomerServiceEmail,
                CustomerServicePhoneNumber = model.CustomerServicePhoneNumber,
                DefaultAnonymousProfileID = model.DefaultAnonymousProfileId,
                DefaultOrderStateID = model.DefaultOrderStateId,
                DefaultProductReviewStateID = model.DefaultProductReviewStateId,
                DefaultRegisteredProfileID = model.DefaultRegisteredProfileId,
                DefaultReviewStatus = model.DefaultReviewStatus,
                DimensionUnit = model.DimensionUnit,
                EmailListDefaultList = model.EmailListDefaultList,
                EmailListLogin = model.EmailListUsername,
                EmailListPassword = model.EmailListPassword,
                EnableAddressValidation = model.EnableAddressValidation,
                EnableCustomerPricing = model.EnableCustomerPricing,
                EnablePIMS = model.EnablePims,
                ExternalID = model.ExternalId,
                FedExAccountNumber = model.FedExAccountNumber,
                FedExAddInsurance = model.FedExAddInsurance,
                FedExCSPKey = model.FedExCspKey,
                FedExCSPPassword = model.FedExCspPassword,
                FedExClientProductId = model.FedExClientProductId,
                FedExClientProductVersion = model.FedExClientProductVersion,
                FedExDropoffType = model.FedExDropoffType,
                FedExMeterNumber = model.FedExMeterNumber,
                FedExPackagingType = model.FedExPackagingType,
                FedExProductionKey = model.FedExProductionKey,
                FedExSecurityCode = model.FedExSecurityCode,
                FedExUseDiscountRate = model.FedExUseDiscountRate,
                GoogleAnalyticsCode = model.GoogleAnalyticsCode,
                ImageNotAvailablePath = model.ImageNotAvailablePath,
                InclusiveTax = model.InclusiveTax,
                LocaleID = model.LocaleId,
                LogoPath = model.LogoPath,
                MasterPage = model.MasterPage,
                MaxCatalogCategoryDisplayThumbnails = model.MaxCatalogCategoryDisplayThumbnails,
                MaxCatalogDisplayColumns = model.MaxCatalogDisplayColumns,
                MaxCatalogDisplayItems = model.MaxCatalogDisplayItems,
                MaxCatalogItemCrossSellWidth = model.MaxCatalogItemCrossSellWidth,
                MaxCatalogItemLargeWidth = model.MaxCatalogItemLargeWidth,
                MaxCatalogItemMediumWidth = model.MaxCatalogItemMediumWidth,
                MaxCatalogItemSmallThumbnailWidth = model.MaxCatalogItemSmallThumbnailWidth,
                MaxCatalogItemSmallWidth = model.MaxCatalogItemSmallWidth,
                MaxCatalogItemThumbnailWidth = model.MaxCatalogItemThumbnailWidth,
                MobileTheme = model.MobileTheme,
                OrderReceiptAffiliateJavascript = model.OrderReceiptAffiliateJavascript,
                PersistentCartEnabled = model.PersistentCartEnabled,
                PortalID = model.PortalId,
                RequireValidatedAddress = model.RequireValidatedAddress,
                SMTPPassword = model.SmtpPassword,
                SMTPPort = model.SmtpPort,
                SMTPServer = model.SmtpServer,
                SMTPUserName = model.SmtpUsername,
                SalesEmail = model.SalesEmail,
                SalesPhoneNumber = model.SalesPhoneNumber,
                SeoDefaultCategoryDescription = model.SeoDefaultCategoryDescription,
                SeoDefaultCategoryKeyword = model.SeoDefaultCategoryKeyword,
                SeoDefaultCategoryTitle = model.SeoDefaultCategoryTitle,
                SeoDefaultContentDescription = model.SeoDefaultContentDescription,
                SeoDefaultContentKeyword = model.SeoDefaultContentKeyword,
                SeoDefaultContentTitle = model.SeoDefaultContentTitle,
                SeoDefaultProductDescription = model.SeoDefaultProductDescription,
                SeoDefaultProductKeyword = model.SeoDefaultProductKeyword,
                SeoDefaultProductTitle = model.SeoDefaultProductTitle,
                ShippingOriginAddress1 = model.ShippingOriginAddress1,
                ShippingOriginAddress2 = model.ShippingOriginAddress2,
                ShippingOriginCity = model.ShippingOriginCity,
                ShippingOriginCountryCode = model.ShippingOriginCountryCode,
                ShippingOriginPhone = model.ShippingOriginPhone,
                ShippingOriginStateCode = model.ShippingOriginStateCode,
                ShippingOriginZipCode = model.ShippingOriginZipCode,
                ShippingTaxable = model.IsShippingTaxable,
                ShopByPriceIncrement = model.ShopByPriceIncrement,
                ShopByPriceMax = model.ShopByPriceMax,
                ShopByPriceMin = model.ShopByPriceMin,
                ShowAlternateImageInCategory = model.ShowAlternateImageInCategory,
                ShowSwatchInCategory = model.ShowSwatchInCategory,
                SiteWideAnalyticsJavascript = model.SiteWideAnalyticsJavascript,
                SiteWideBottomJavascript = model.SiteWideBottomJavascript,
                SiteWideTopJavascript = model.SiteWideTopJavascript,
                SplashCategoryID = model.SplashCategoryId,
                SplashImageFile = model.SplashImageFile,
                StoreName = model.StoreName,
                TimeZoneOffset = model.TimeZoneOffset,
                UPSKey = model.UpsKey,
                UPSPassword = model.UpsPassword,
                UPSUserName = model.UpsUsername,
                UseDynamicDisplayOrder = model.UseDynamicDisplayOrder,
                UseSSL = model.UseSsl,

                IsMultipleCouponAllowed = model.IsMultipleCouponAllowed,
                WeightUnit = model.WeightUnit,
                IsEnableCompare = model.IsEnableCompare,
                CompareType = model.CompareType,
                IsEnableSinglePageCheckout = model.IsEnableSinglePageCheckout,
                MobileLogoPath = model.MobileLogoPath,
                EnableSSLForSMTP=model.EnableSslForSmtp,
            };

            return entity;
        }

        /// <summary>
        /// Converts dataset to portal model.
        /// </summary>
        /// <param name="ds">Dataset.</param>
        /// <returns>Portal Model.</returns>
        public static void ToDecryptedData(PortalModel portalModel)
        {
            ZNodeEncryption encryption = new ZNodeEncryption();

            portalModel.FedExAccountNumber = !Equals(portalModel.FedExAccountNumber, null) ? encryption.DecryptData(portalModel.FedExAccountNumber) : string.Empty;
            portalModel.FedExMeterNumber = !Equals(portalModel.FedExMeterNumber, null) ? encryption.DecryptData(portalModel.FedExMeterNumber) : string.Empty;
            portalModel.FedExProductionKey = !Equals(portalModel.FedExProductionKey, null) ? encryption.DecryptData(portalModel.FedExProductionKey) : string.Empty;
            portalModel.FedExSecurityCode = !Equals(portalModel.FedExSecurityCode, null) ? encryption.DecryptData(portalModel.FedExSecurityCode) : string.Empty;
            portalModel.UpsUsername = !Equals(portalModel.UpsUsername, null) ? encryption.DecryptData(portalModel.UpsUsername) : string.Empty;
            portalModel.UpsPassword = !Equals(portalModel.UpsPassword, null) ? encryption.DecryptData(portalModel.UpsPassword) : string.Empty;
            portalModel.UpsKey = !Equals(portalModel.UpsKey, null) ? encryption.DecryptData(portalModel.UpsKey) : string.Empty;
            portalModel.SmtpPassword = !Equals(portalModel.SmtpPassword, null) ? encryption.DecryptData(portalModel.SmtpPassword) : string.Empty;
            portalModel.SmtpUsername = !Equals(portalModel.SmtpUsername, null) ? encryption.DecryptData(portalModel.SmtpUsername) : string.Empty;
        }
    }
}
