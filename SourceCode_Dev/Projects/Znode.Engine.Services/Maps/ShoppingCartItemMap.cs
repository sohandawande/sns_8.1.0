﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;
using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.ECommerce.Entities;
using ZNodeShoppingCartItem = ZNode.Libraries.ECommerce.ShoppingCart.ZNodeShoppingCartItem;
using ZNodeShoppingCart = ZNode.Libraries.ECommerce.ShoppingCart.ZNodeShoppingCart;
using ZNodePortalCart = ZNode.Libraries.ECommerce.ShoppingCart.ZNodePortalCart;
using ZNode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.Services.Maps
{
    public static class ShoppingCartItemMap
    {
        public static ShoppingCartItemModel ToModel(ZNodeShoppingCartItem znodeCartItem, ZNodeShoppingCart znodeCart)
        {
            if (Equals(znodeCartItem, null))
            {
                return null;
            }
            //ZNode Version 7.2.2 - Add ImageUtility to Get Image Path
            ZNodeImage imageUtility = new ZNodeImage();
            ShoppingCartItemModel model = new ShoppingCartItemModel
            {
                AddOnValuesCustomText = znodeCartItem.AddOnValuesCustomText,
                AddOnValueIds = znodeCartItem.AddOnValueIds,
                AttributeIds = znodeCartItem.AttributeIds,
                Description = znodeCartItem.PromoDescription,
                ExtendedPrice = znodeCartItem.ExtendedPrice,
                ExternalId = znodeCartItem.ExternalId,
                ProductId = znodeCartItem.Product.ProductID,
                Quantity = znodeCartItem.Quantity,
                ShippingCost = znodeCartItem.ShippingCost,
                ShippingOptionId = znodeCartItem.ShippingOptionId,
                Sku = znodeCartItem.Product.SelectedSKU.SKU,
                SkuId = znodeCartItem.Product.SelectedSKU.SKUID,
                CartDescription = znodeCartItem.Product.ShoppingCartDescription,
                UnitPrice = znodeCartItem.UnitPrice,
                BundleItems = ToBundleItemModel(znodeCartItem),
                MultipleShipToAddress = GetOrderShipmentForModel(znodeCartItem, znodeCart),
                TaxCost = znodeCartItem.TaxCost,
                //ZNode Version 7.2.2 - Bind SKU Image Path Url
                ImagePath = !string.IsNullOrEmpty(znodeCartItem.Product.SelectedSKU.SKUPicturePath) ? imageUtility.GetImageHttpPathLarge(znodeCartItem.Product.SelectedSKU.SKUPicturePath) : imageUtility.GetImageHttpPathLarge(znodeCartItem.Product.ImageFile),
                ImageMediumPath = znodeCartItem.Product.ImageFile,
                ProductName = znodeCartItem.Product.Name,
                RecurringBillingInd = znodeCartItem.Product.RecurringBillingInd,
                RecurringBillingInitialAmount = znodeCartItem.Product.RecurringBillingInitialAmount,
                RecurringBillingFrequency = znodeCartItem.Product.RecurringBillingFrequency,
                ProductDiscountAmount = znodeCartItem.Product.DiscountAmount,
                //PRFT Custom Code: Start
                ERPExtendedPrice = znodeCartItem.ERPExtendedPrice,
                ERPUnitPrice = znodeCartItem.ERPUnitPrice,
                AllowBackOrder = znodeCartItem.AllowBackOrder,
                BackOrderMessage = znodeCartItem.BackOrderMessage
                //PRFT Custom Code: End
            };

            return model;
        }

        public static ZNodeShoppingCartItem ToZnodeShoppingCartItem(ShoppingCartItemModel model, AddressModel shippingAddress, string externalAccountId = null)
        {
            if (Equals(model, null))
            {
                return null;
            }

            ZNodeShoppingCartItem znodeCartItem = new ZNodeShoppingCartItem
            {
                AddOnValuesCustomText = model.AddOnValuesCustomText,
                AddOnValueIds = model.AddOnValueIds,
                AttributeIds = model.AttributeIds,
                ExternalId = model.ExternalId,
                Quantity = model.Quantity,
                ShippingCost = model.ShippingCost,
                ShippingOptionId = model.ShippingOptionId,

                //PRFT Custom Code: Start
                ERPExtendedPrice = model.ERPExtendedPrice,
                ERPUnitPrice = model.ERPUnitPrice,
                AllowBackOrder=model.AllowBackOrder,
                BackOrderMessage=model.BackOrderMessage
                //PRFT Custom Code: End
            };

            int addressId = 0;

            // Cart level shipping address
            if (!Equals(shippingAddress, null))
            {
                addressId = shippingAddress.AddressId;
            }

            if (!Equals(model.MultipleShipToAddress, null) && model.MultipleShipToAddress.Any())
            {
                foreach (OrderShipmentModel shipToAddress in model.MultipleShipToAddress)
                {
                    if (shipToAddress.AddressId.Equals(0))
                    {
                        shipToAddress.AddressId = addressId;
                    }

                    ZNodeOrderShipment znodeOrderShipment = new ZNodeOrderShipment(shipToAddress.AddressId, shipToAddress.Quantity, znodeCartItem.GUID, shipToAddress.ShippingOptionId.GetValueOrDefault(0), shipToAddress.ShippingName);
                    znodeCartItem.OrderShipments.Add(znodeOrderShipment);
                }
            }
            else
            {
                // Cart item level shipping address
                if (!Equals(model.ShippingAddress, null))
                {
                    addressId = model.ShippingAddress.AddressId;
                }

                ZNodeOrderShipment orderShipment = new ZNodeOrderShipment(addressId, model.Quantity, znodeCartItem.GUID);

                znodeCartItem.OrderShipments.Add(orderShipment);
            }

            var productHelper = new ProductHelper();

            // Try to get by external ID
            if (model.ProductId > 0)
            {
                znodeCartItem.Product = ProductMap.ToZnodeProductBase(model.ProductId, model.Sku, model.AddOnValueIds, model.AddOnValuesCustomText, externalAccountId, model.ProductDiscountAmount);

                znodeCartItem.Product.ZNodeBundleProductCollection = GetZnodeProductBundles(model.BundleItems);

                znodeCartItem.Product.DiscountAmount = model.ProductDiscountAmount;
            }
            else if (string.IsNullOrEmpty(model.ExternalId))
            {
                znodeCartItem.Product = ProductMap.ToZnodeProductBase(Convert.ToInt32(productHelper.GetProductIDByExternalID(model.ExternalId)), model.Sku, model.AddOnValueIds, model.AddOnValuesCustomText);

                if (!Equals(model.BundleItems, null))
                {
                    foreach (BundleItemModel bundleItem in model.BundleItems)
                    {
                        znodeCartItem.Product.ZNodeBundleProductCollection.Add(
                            new ZNodeBundleProductEntity(
                                ProductMap.ToZnodeProductBase(
                                    Convert.ToInt32(productHelper.GetProductIDByExternalID(bundleItem.ExternalId)),
                                    bundleItem.Sku,
                                    bundleItem.AddOnValueIds,
                                    bundleItem.AddOnValuesCustomText)));
                    }
                }
            }

            return znodeCartItem;
        }

        private static ZNodeGenericCollection<ZNodeBundleProductEntity> GetZnodeProductBundles(IEnumerable<BundleItemModel> bundles)
        {
            ZNodeGenericCollection<ZNodeBundleProductEntity> bundleCollection = new ZNodeGenericCollection<ZNodeBundleProductEntity>();
            if (!Equals(bundles, null))
            {
                foreach (var bundleItem in bundles)
                {
                    bundleCollection.Add(
                        new ZNodeBundleProductEntity(ProductMap.ToZnodeProductBase(bundleItem.ProductId, bundleItem.Sku,
                                                                                   bundleItem.AddOnValueIds,
                                                                                   bundleItem.AddOnValuesCustomText)));
                }
            }
            return bundleCollection;
        }

        public static Collection<BundleItemModel> ToBundleItemModel(ZNodeShoppingCartItem model)
        {
            if (Equals(model.Product.ZNodeBundleProductCollection, null))
            {
                return null;
            }

            Collection<BundleItemModel> items = new Collection<BundleItemModel>();

            foreach (ZNodeBundleProductEntity bundleItem in model.Product.ZNodeBundleProductCollection)
            {
                BundleItemModel itemModel = new BundleItemModel()
                    {
                        AddOnValueIds = ToAddOnValueIdArray(bundleItem.SelectedAddOns.SelectedAddOnValueIds),
                        ProductId = bundleItem.ProductID,
                        Sku = bundleItem.SelectedSKUvalue.SKU,
                        AddOnValuesCustomText = ToAddOnValueCustomTextDictionary(bundleItem.SelectedAddOns.AddOnCollection)
                    };

                items.Add(itemModel);
            }

            return items;
        }

        private static int[] ToAddOnValueIdArray(string addOnValueIdString)
        {
            return !string.IsNullOrEmpty(addOnValueIdString)
                       ? addOnValueIdString.Split(',').Select(int.Parse).ToArray()
                       : new int[0];
        }

        private static Dictionary<int, string> ToAddOnValueCustomTextDictionary(ZNodeGenericCollection<ZNodeAddOnEntity> addOnCollection)
        {
            var textboxAddOns = addOnCollection.Cast<ZNodeAddOnEntity>()
                                              .Where(addOnEntity => addOnEntity.DisplayType.Equals("TextBox", StringComparison.OrdinalIgnoreCase)).ToList();

            if (textboxAddOns.Any())
            {
                return textboxAddOns.SelectMany(addOnEntity => addOnEntity.AddOnValueCollection.Cast<ZNodeAddOnValueEntity>())
                                        .ToDictionary(addOnValueEntity => addOnValueEntity.AddOnValueID, addOnValueEntity => addOnValueEntity.CustomText);
            }

            return null;
        }

        // For Multiple Ship to address mapping
        private static List<OrderShipmentModel> GetOrderShipmentForModel(ZNodeShoppingCartItem item, ZNodeShoppingCart znodeCart)
        {
            // Return to handle Multiple ship to address on client side.                        
            var resultItems = item.OrderShipments.GroupBy(x => new { x.AddressID, x.ShippingID })
                    .Select(x => new OrderShipmentModel() { AddressId = x.Key.AddressID, ShippingOptionId = x.Key.ShippingID, Quantity = x.Sum(y => y.Quantity) })
                    .ToList();

            return resultItems;
        }

        public static ShoppingCartItemModel ToModel(ZNodeShoppingCartItem znodeCartItem, ZNodePortalCart znodeCart)
        {
            if (!Equals(znodeCartItem, null))
            {
                return null;
            }

            ShoppingCartItemModel model = new ShoppingCartItemModel
            {
                AddOnValuesCustomText = znodeCartItem.AddOnValuesCustomText,
                AddOnValueIds = znodeCartItem.AddOnValueIds,
                AttributeIds = znodeCartItem.AttributeIds,
                Description = znodeCartItem.PromoDescription,
                ExtendedPrice = znodeCartItem.ExtendedPrice,
                ExternalId = znodeCartItem.ExternalId,
                ProductId = znodeCartItem.Product.ProductID,
                Quantity = znodeCartItem.Quantity,
                ShippingCost = znodeCartItem.ShippingCost,
                ShippingOptionId = znodeCartItem.ShippingOptionId,
                Sku = znodeCartItem.Product.SelectedSKU.SKU,
                SkuId = znodeCartItem.Product.SelectedSKU.SKUID,
                CartDescription = znodeCartItem.Product.ShoppingCartDescription,
                UnitPrice = znodeCartItem.UnitPrice,
                BundleItems = ToBundleItemModel(znodeCartItem),
                MultipleShipToAddress = GetOrderShipmentForModel(znodeCartItem, znodeCart),
                TaxCost = znodeCartItem.TaxCost,
                ProductDiscountAmount = znodeCartItem.Product.DiscountAmount,

                //PRFT Custom Code: Start
                ERPExtendedPrice = znodeCartItem.ERPExtendedPrice,
                ERPUnitPrice = znodeCartItem.ERPUnitPrice,
                //PRFT Custom Code: End
            };

            return model;
        }

        private static List<OrderShipmentModel> GetOrderShipmentForModel(ZNodeShoppingCartItem item, ZNodePortalCart znodeCart)
        {
            // Return to handle Multiple ship to address on client side.                        
            var orderShipmentModels = item.OrderShipments.GroupBy(x => new { x.AddressID, x.ShippingID, x.ShippingName })
                    .Select(x => new OrderShipmentModel() { AddressId = x.Key.AddressID, ShippingOptionId = x.Key.ShippingID, ShippingName = x.Key.ShippingName, Quantity = x.Sum(y => y.Quantity) })
                    .ToList();

            var addressCarts = znodeCart.AddressCarts.ToList();

            foreach (OrderShipmentModel orderShipmentModel in orderShipmentModels)
            {
                var addressItem = addressCarts.FirstOrDefault(y => y.AddressID == orderShipmentModel.AddressId);
                if (Equals(addressItem, null))
                {
                    continue;
                }
                orderShipmentModel.ShippingCost = addressItem.ShippingCost;
                orderShipmentModel.TaxCost = addressItem.TaxCost;
            }

            return orderShipmentModels;
        }
    }
}