﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
	public static class StateMap
	{
		public static StateModel ToModel(State entity)
		{
			if (entity == null)
			{
				return null;
			}

			var model = new StateModel
			{
				Code = entity.Code,
				CountryCode = entity.CountryCode,
				Name = entity.Name
			};

			return model;
		}

		public static State ToEntity(StateModel model)
		{
			if (model == null)
			{
				return null;
			}

			var entity = new State
			{
				Code = model.Code,
				CountryCode = model.CountryCode,
				Name = model.Name
			};

			return entity;
		}
	}
}
