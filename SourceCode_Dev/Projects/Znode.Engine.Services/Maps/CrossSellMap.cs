﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
	public static class CrossSellMap
	{
		public static CrossSellModel ToModel(ProductCrossSell entity)
		{
            if (Equals(entity, null))
			{
				return null;
			}

			var model = new CrossSellModel
			{
				DisplayOrder = entity.DisplayOrder,
				ProductCrossSellTypeId = entity.ProductCrossSellTypeId,
				ProductId = entity.ProductId,
				RelatedProductId = entity.RelatedProductId,
				RelationTypeId = entity.RelationTypeId,
                ProductRelation = (ProductRelationCode)Enum.ToObject(typeof(ProductRelationCode), entity.RelationTypeId)
			};

			return model;
		}

		public static ProductCrossSell ToEntity(CrossSellModel model)
		{
			if (Equals(model,null))
			{
				return null;
			}

			var entity = new ProductCrossSell
			{
				DisplayOrder = model.DisplayOrder,
				ProductCrossSellTypeId = model.ProductCrossSellTypeId,
				ProductId = model.ProductId,
				RelatedProductId = model.RelatedProductId,
				RelationTypeId = model.RelationTypeId
			};

			return entity;
		}

        /// <summary>
        /// Maps Dataset to list of type CrossSellModel.
        /// </summary>
        /// <param name="result">Dataset</param>
        /// <returns>Returns mapped list of type CrossSellModel..</returns>
        public static CrossSellListModel ToAssociatedProductItemModel(DataSet result)
        {
            if (Equals(result, null))
            {
                return null;
            }

            var modelList = new CrossSellListModel();
            foreach (DataRow dr in result.Tables[0].Rows)
            {
                string productName = (dr["CrossSellProducts"] != DBNull.Value) ? dr["CrossSellProducts"].ToString() : string.Empty;
                productName = productName.Replace(",", ", ");
                if (productName.Contains(", "))
                {
                    productName = productName.Remove(productName.LastIndexOf(", "));
                }
                CrossSellModel model = new CrossSellModel
                {
                    ProductId = dr.Table.Columns.Contains("ProductId") ? Convert.ToInt32(dr["ProductId"]) : 0,
                    ProductName = (dr["ProductName"] != DBNull.Value) ? dr["ProductName"].ToString() : string.Empty,
                    CrossSellProducts = productName
                };
                modelList.CrossSellProducts.Add(model);
            }
            return modelList;
        }

        /// <summary>
        /// Maps Dataset to list of type FrequentlyBoughtModel.
        /// </summary>
        /// <param name="result">DataSet result</param>
        /// <returns>returns CategoryListModel</returns>
        public static CrossSellListModel ToFrequentlyBoughtModelList(DataSet result)
        {
            if (Equals(result, null))
            {
                return null;
            }
            var modelList = new CrossSellListModel();
            foreach (DataRow dr in result.Tables[0].Rows)
            {
                CrossSellModel model = new CrossSellModel
                {
                    ProductId = (dr["ProductID"] != DBNull.Value) ? Convert.ToInt32(dr["ProductID"]) : 0,
                    ProductName = (dr["ProductName"] != DBNull.Value) ? dr["ProductName"].ToString() : string.Empty,
                    FrequentlyBoughtProduct1 = (dr["FrequentlyBoughtProduct1"] != DBNull.Value) ? dr["FrequentlyBoughtProduct1"].ToString() : string.Empty,
                    FrequentlyBoughtProduct2 = (dr["FrequentlyBoughtProduct2"] != DBNull.Value) ? dr["FrequentlyBoughtProduct2"].ToString() : string.Empty,
                };
                modelList.CrossSellProducts.Add(model);
            }

            return modelList;
        }

        public static TList<ProductCrossSell> ToEntityList(Collection<CrossSellModel> model)
        {
            if (Equals(model, null))
            {
                return null;
            }
            TList<ProductCrossSell> groupProducts = new TList<ProductCrossSell>();
            foreach (var item in model)
            {
                groupProducts.Add(new ProductCrossSell { ProductId = item.ProductId, RelatedProductId = item.RelatedProductId, RelationTypeId = item.RelationTypeId, DisplayOrder = item.DisplayOrder});
            }
            return groupProducts;
        }
	}
}
