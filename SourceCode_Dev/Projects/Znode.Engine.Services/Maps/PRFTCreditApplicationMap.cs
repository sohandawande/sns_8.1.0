﻿using System;
using ZNode.Libraries.DataAccess.Entities;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services.Maps
{
    public static class PRFTCreditApplicationMap
    {
        public static PRFTCreditApplicationModel ToModel(PRFTCreditApplication entity)
        {
            if (entity == null)
            {
                return null;
            }

            var model = new PRFTCreditApplicationModel
            {
                CreditApplicationID = entity.CreditApplicationID,
                BusinessName = entity.BusinessName,
                BusinessStreet = entity.BusinessStreet,
                BusinessStreet1 = entity.BusinessStreet1,
                BusinessCity = entity.BusinessCity,
                BusinessStateCode = entity.BusinessStateCode,
                BusinessPostalCode = entity.BusinessPostalCode,
                BusinessCountryCode = entity.BusinessCountryCode,
                BusinessPhoneNumber = entity.BusinessPhoneNumber,
                BusinessFaxNumber = entity.BusinessFaxNumber,
                BusinessEmail = entity.BusinessEmail,
                BusinessYearEstablished = entity.BusinessYearEstablished,
                BusinessType = entity.BusinessType,
                IsTaxExempt = entity.IsTaxExempt,
                TaxExemptCertificate = entity.TaxExemptCertificate,
                BillingStreet = entity.BillingStreet,
                BillingStreet1 = entity.BillingStreet1,
                BillingCity = entity.BillingCity,
                BillingStateCode = entity.BillingStateCode,
                BillingPostalCode = entity.BillingPostalCode,
                BillingCountryCode = entity.BillingCountryCode,
                BankName = entity.BankName,
                BankAddress = entity.BankAddress,
                BankPhoneNumber = entity.BankPhoneNumber,
                BankAccountNumber = entity.BankAccountNumber,
                RequestDate = entity.RequestDate,
                IsCreditApproved = entity.IsCreditApproved,
                CreditDays = entity.CreditDays,
                CreditApprovedDate = entity.CreditApprovedDate,
                Custom1 = entity.Custom1,
                Custom2 = entity.Custom2,
                Custom3 = entity.Custom3,
                Custom4 = entity.Custom4,
                Custom5 = entity.Custom5
            };

            foreach (var item in entity.PRFTTradeReferencesCollection)
            {
                model.TradeReferences.Add(PRFTTradeReferencesMap.ToModel(item));
            }

            return model;
        }

        public static PRFTCreditApplication ToEntity(PRFTCreditApplicationModel model)
        {
            if (model == null)
            {
                return null;
            }

            var entity = new PRFTCreditApplication
            {
                CreditApplicationID = model.CreditApplicationID,
                BusinessName = model.BusinessName,
                BusinessStreet = model.BusinessStreet,
                BusinessStreet1 = model.BusinessStreet1,
                BusinessCity = model.BusinessCity,
                BusinessStateCode = model.BusinessStateCode,
                BusinessPostalCode = model.BusinessPostalCode,
                BusinessCountryCode = model.BusinessCountryCode,
                BusinessPhoneNumber = model.BusinessPhoneNumber,
                BusinessFaxNumber = model.BusinessFaxNumber,
                BusinessEmail = model.BusinessEmail,
                BusinessYearEstablished = model.BusinessYearEstablished,
                BusinessType = model.BusinessType,
                IsTaxExempt = model.IsTaxExempt,
                TaxExemptCertificate = model.TaxExemptCertificate,
                BillingStreet = model.BillingStreet,
                BillingStreet1 = model.BillingStreet1,
                BillingCity = model.BillingCity,
                BillingStateCode = model.BillingStateCode,
                BillingPostalCode = model.BillingPostalCode,
                BillingCountryCode = model.BillingCountryCode,
                BankName = model.BankName,
                BankAddress = model.BankAddress,
                BankPhoneNumber = model.BankPhoneNumber,
                BankAccountNumber = model.BankAccountNumber,
                RequestDate = model.RequestDate,
                IsCreditApproved = model.IsCreditApproved,
                CreditDays = model.CreditDays,
                CreditApprovedDate = model.CreditApprovedDate,
                Custom1 = model.Custom1,
                Custom2 = model.Custom2,
                Custom3 = model.Custom3,
                Custom4 = model.Custom4,
                Custom5 = model.Custom5
            };

            foreach (var item in model.TradeReferences)
            {
                entity.PRFTTradeReferencesCollection.Add(PRFTTradeReferencesMap.ToEntity(item));
            }

            return entity;
        }
    }
}
