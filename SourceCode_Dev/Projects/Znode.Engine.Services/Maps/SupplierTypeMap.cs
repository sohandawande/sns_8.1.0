﻿using Znode.Engine.Api.Models;
using Znode.Engine.Suppliers;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
    public static class SupplierTypeMap
    {
        public static SupplierTypeModel ToModel(SupplierType entity)
        {
            if (entity == null)
            {
                return null;
            }

            var model = new SupplierTypeModel
            {
                ClassName = entity.ClassName,
                Description = entity.Description,
                IsActive = entity.ActiveInd,
                Name = entity.Name,
                SupplierTypeId = entity.SupplierTypeID
            };

            return model;
        }

        public static SupplierType ToEntity(SupplierTypeModel model)
        {
            if (model == null)
            {
                return null;
            }

            var entity = new SupplierType
            {
                ActiveInd = model.IsActive,
                ClassName = model.ClassName,
                Description = model.Description,
                Name = model.Name,
                SupplierTypeID = model.SupplierTypeId
            };

            return entity;
        }

        /// <summary>
        /// Znode version 8.0
        /// Convert IZnodeSupplierType to SupplierTypeModel
        /// </summary>
        /// <param name="znodeSupplierType">IZnodeSupplierType znodeSupplierType</param>
        /// <returns>Returns SupplierTypeModel</returns>
        public static SupplierTypeModel ToModel(IZnodeSupplierType znodeSupplierType)
        {
            if (Equals(znodeSupplierType, null))
            {
                return null;
            }
            var model = new SupplierTypeModel
            {
                ClassName = znodeSupplierType.ClassName,
                Description = znodeSupplierType.Description,
                Name = znodeSupplierType.Name,
            };
            return model;
        }
    }
}
