﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;
using System.Collections.ObjectModel;
namespace Znode.Engine.Services.Maps
{
    public static class FacetGroupMap
    {
        public static FacetGroupModel ToModel(FacetGroup entity)
        {
            if (Equals(entity, null))
            {
                return null;
            }

            var model = new FacetGroupModel
                {
                    CatalogID = entity.CatalogID,
                    ControlTypeID = entity.ControlTypeID,
                    DisplayOrder = entity.DisplayOrder,
                    FacetGroupID = entity.FacetGroupID,
                    FacetGroupLabel = entity.FacetGroupLabel,
                };
            foreach (var a in entity.FacetGroupCategoryCollection)
            {
                model.FacetGroupCategories.Add(FacetGroupCategoryMap.ToModel(a));
            }

            foreach (var item in entity.FacetCollection)
            {
                model.FacetGroupFacets.Add(FacetMap.ToModel(item));
            }
            return model;
        }

        public static FacetGroup ToEntity(FacetGroupModel model)
        {
            if (Equals(model, null))
            {
                return null;
            }

            var entity = new FacetGroup
            {
               CatalogID=model.CatalogID,
               ControlTypeID=model.ControlTypeID,
               DisplayOrder=model.DisplayOrder,
               FacetGroupID=model.FacetGroupID,
               FacetGroupLabel=model.FacetGroupLabel,

            };

            return entity;
        }

        public static TList<FacetGroupCategory> ToEntityList(Collection<FacetGroupCategoryModel> model)
        {
            if (Equals(model,null))
            {
                return null;
            }
            TList<FacetGroupCategory> groupCategoryList = new TList<FacetGroupCategory>();
            foreach (var item in model)
	        {
                groupCategoryList.Add(new FacetGroupCategory { CategoryID = item.CategoryID, FacetGroupID = item.FacetGroupID, CategoryDisplayOrder = item.CategoryDisplayOrder });
            }
            return groupCategoryList;
        }

        public static Facet ToEntity(FacetModel model)
        {
            if (Equals(model, null))
            {
                return null;
            }

            var entity = new Facet
            {
                FacetGroupID = model.FacetGroupID,
                FacetName = model.FacetName,
                IconPath = model.IconPath,
                FacetDisplayOrder = model.FacetDisplayOrder,
                FacetID=model.FacetID,
            };

            return entity;
        }

        public static FacetModel ToModel(Facet entity)
        {
            if (Equals(entity, null))
            {
                return null;
            }

            var model = new FacetModel
            {
                FacetGroupID = entity.FacetGroupID,
                FacetName = entity.FacetName,
                IconPath = entity.IconPath,
                FacetDisplayOrder = entity.FacetDisplayOrder,
                FacetID=entity.FacetID,
            };
            return model;
        }

    }
}
