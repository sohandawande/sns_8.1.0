﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
	public static class GiftCardHistoryMap
	{
		public static GiftCardHistoryModel ToModel(GiftCardHistory entity)
		{
			if (entity == null)
			{
				return null;
			}

			var model = new GiftCardHistoryModel
			{
				GiftCardHistoryId = entity.GiftCardHistoryID,
				GiftCardId = entity.GiftCardID,
				OrderId = entity.OrderID,
				TransactionAmount = entity.TransactionAmount,
				TransactionDate = entity.TransactionDate
			};

			return model;
		}

		public static GiftCardHistory ToEntity(GiftCardHistoryModel model)
		{
			if (model == null)
			{
				return null;
			}

			var entity = new GiftCardHistory
			{
				GiftCardHistoryID = model.GiftCardHistoryId,
				GiftCardID = model.GiftCardId,
				OrderID = model.OrderId,
				TransactionAmount = model.TransactionAmount,
				TransactionDate = model.TransactionDate
			};

			return entity;
		}
	}
}
