﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using ZNode.Libraries.ECommerce.Entities;

namespace Znode.Engine.Services.Maps
{
    public static class CouponMap
    {
        public static CouponModel ToModel(ZNodeCoupon znodeCoupon)
        {
            if (znodeCoupon == null)
            {
                return null;
            }

            var model = new CouponModel
            {
                Coupon = znodeCoupon.Coupon,
                CouponMessage = znodeCoupon.CouponMessage,
                CouponApplied = znodeCoupon.CouponApplied,
                CouponValid = znodeCoupon.CouponValid,
               
            };
            return model;
        }

        public static ZNodeCoupon ToZnodeCoupon(CouponModel couponModel)
        {
            if (couponModel == null)
            {
                return null;
            }

            var znodeCoupon = new ZNodeCoupon
            {
                Coupon = couponModel.Coupon,
                CouponMessage = couponModel.CouponMessage,
                CouponApplied = couponModel.CouponApplied,
                CouponValid = couponModel.CouponValid,

            };
            return znodeCoupon;
        }
    }
}
