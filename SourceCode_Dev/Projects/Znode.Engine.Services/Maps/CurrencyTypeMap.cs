﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
    public class CurrencyTypeMap
    {
        public static CurrencyTypeModel ToModel(CurrencyType entity)
        {
            if (Equals(entity,null))
            {
                return null;
            }

            var model = new CurrencyTypeModel
            {
                CurrencyTypeID = entity.CurrencyTypeID,
                CurrencySuffix = entity.CurrencySuffix,
                Description = entity.Description,
                Name = entity.Name
            };
            return model;
        }

        public static CurrencyType ToEntity(CurrencyTypeModel model)
        {
            if (Equals(model, null))
            {
                return null;
            }

            var entity = new CurrencyType
            {
                CurrencyTypeID = model.CurrencyTypeID,
                CurrencySuffix = model.CurrencySuffix,
                Description = model.Description,
                Name = model.Name,
                Symbol=model.Symbol,
            };

            return entity;
        }
    }
}
