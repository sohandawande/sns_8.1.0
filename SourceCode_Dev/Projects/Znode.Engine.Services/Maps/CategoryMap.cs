﻿using System.Web;
using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.SEO;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Services.Maps
{
    public static class CategoryMap
    {
        #region Public Methods

        /// <summary>
        /// Maps the Category entity to Category model.
        /// </summary>
        /// <param name="entity">Entity of type Category</param>
        /// <returns>Retuns the mapped Category entity to Category model.</returns>
        public static CategoryModel ToModel(Category entity)
        {
            if (Equals(entity, null))
            {
                return null;
            }

            var imageUtility = new ZNodeImage();

            var model = new CategoryModel
            {
                AlternateDescription = entity.AlternateDescription,
                CategoryId = entity.CategoryID,
                CategoryUrl = ZNodeSEOUrl.MakeUrlForMvc(entity.CategoryID.ToString(), SEOUrlType.Category, entity.SEOURL),
                CreateDate = entity.CreateDate,
                Custom1 = entity.Custom1,
                Custom2 = entity.Custom2,
                Custom3 = entity.Custom3,
                Description = entity.Description,
                DisplayOrder = entity.DisplayOrder,
                ExternalId = entity.ExternalID,
                ImageAltTag = entity.ImageAltTag,
                ImageFile = entity.ImageFile,
                ImageMediumPath = imageUtility.GetImageHttpPathMedium(entity.ImageFile),
                ImageLargePath = imageUtility.GetImageHttpPathLarge(entity.ImageFile),
                ImageSmallPath = imageUtility.GetImageHttpPathSmall(entity.ImageFile),
                ImageSmallThumbnailPath = imageUtility.GetImageHttpPathSmallThumbnail(entity.ImageFile),
                ImageThumbnailPath = imageUtility.GetImageHttpPathThumbnail(entity.ImageFile),
                IsVisible = entity.VisibleInd,
                Name = HttpUtility.HtmlDecode(entity.Name),
                ProductIds = entity.ProductIds,
                SeoDescription = string.IsNullOrEmpty(entity.SEODescription) ? ZNodeConfigManager.SiteConfig.SeoDefaultCategoryDescription : entity.SEODescription,
                SeoKeywords = string.IsNullOrEmpty(entity.SEOKeywords) ? ZNodeConfigManager.SiteConfig.SeoDefaultCategoryKeyword : entity.SEOKeywords,
                SeoTitle = string.IsNullOrEmpty(entity.SEOTitle) ? ZNodeConfigManager.SiteConfig.SeoDefaultCategoryTitle : entity.SEOTitle,
                SeoUrl = entity.SEOURL,
                ShortDescription = entity.ShortDescription,
                SubcategoryGridIsVisible = entity.SubCategoryGridVisibleInd,
                Title = HttpUtility.HtmlDecode(entity.Title),
                UpdateDate = entity.UpdateDate,
                //Znode version 7.2.2 To map Category Banner
                CategoryBanner = entity.CategoryBanner,
                PortalID = entity.PortalID
            };

            foreach (var node in entity.CategoryNodeCollection)
            {
                // Add to category nodes list
                model.CategoryNodes.Add(CategoryNodeMap.ToModel(node));

                // Also add to subcategories list
                if (!Equals(node.CategoryIDSource, null))
                {
                    model.Subcategories.Add(ToModel(node.CategoryIDSource));
                }
            }

            foreach (var profile in entity.CategoryProfileCollection)
            {
                model.CategoryProfiles.Add(CategoryProfileMap.ToModel(profile));
            }

            return model;
        }

        /// <summary>
        /// Maps the Category model to Category entity.
        /// </summary>
        /// <param name="model">Model of type CategoryModel</param>
        /// <returns>Retuns the mapped Category model to Category entity.</returns>
        public static Category ToEntity(CategoryModel model)
        {
            if (Equals(model, null))
            {
                return null;
            }

            var entity = new Category
            {
                AlternateDescription = model.AlternateDescription,
                CategoryBanner = model.CategoryBanner,
                CategoryID = model.CategoryId,
                CategoryNodeCollection = new TList<CategoryNode>(),
                CreateDate = model.CreateDate,
                Description = model.Description,
                Custom1 = model.Custom1,
                Custom2 = model.Custom2,
                Custom3 = model.Custom3,
                DisplayOrder = model.DisplayOrder,
                ExternalID = model.ExternalId,
                ImageAltTag = model.ImageAltTag,
                ImageFile = model.ImageFile,
                Name = model.Name,
                SEODescription = model.SeoDescription,
                SEOKeywords = model.SeoKeywords,
                SEOTitle = model.SeoTitle,
                SEOURL = model.SeoUrl,
                ShortDescription = model.ShortDescription,
                SubCategoryGridVisibleInd = model.SubcategoryGridIsVisible,
                Title = model.Title,
                UpdateDate = model.UpdateDate,
                VisibleInd = model.IsVisible,
                PortalID = model.PortalID
            };

            foreach (var node in model.CategoryNodes)
            {
                entity.CategoryNodeCollection.Add(CategoryNodeMap.ToEntity(node));
            }

            return entity;
        }

        #endregion
    }
}
