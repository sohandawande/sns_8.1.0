﻿using System;
using ZNode.Libraries.DataAccess.Entities;
using Znode.Engine.Api.Models;
using System.Data;

namespace Znode.Engine.Services.Maps
{
    public static class CategoryProfileMap
    {
        #region Public Methods

        /// <summary>
        /// Maps the CategoryProfile entity to CategoryProfile model.
        /// </summary>
        /// <param name="entity">Entity of type Category</param>
        /// <returns>Retuns the mapped CategoryProfile entity to CategoryProfile model.</returns>
        public static CategoryProfileModel ToModel(CategoryProfile entity)
        {
            if (entity == null)
            {
                return null;
            }

            var model = new CategoryProfileModel
            {
                CategoryProfileID = entity.CategoryProfileID,
                CategoryID = entity.CategoryID,
                ProfileID = entity.ProfileID,
                EffectiveDate = entity.EffectiveDate
            };

            return model;
        }

        /// <summary>
        /// Maps the DataSet to CategoryProfileList model.
        /// </summary>
        /// <param name="entity">Entity of type DataSet</param>
        /// <returns>Retuns the mapped DataSet to CategoryProfileList model.</returns>
        public static CategoryProfileListModel ToModelList(DataSet entity)
        {
            if (entity == null)
            {
                return null;
            }


            var modelList = new CategoryProfileListModel();
            foreach (DataRow dr in entity.Tables[0].Rows)
            {
                CategoryProfileModel model = new CategoryProfileModel
                {
                    CategoryProfileID = dr.Table.Columns.Contains("CategoryProfileID") ? Convert.ToInt32(dr["CategoryProfileID"]) : 0,
                    CategoryID = dr.Table.Columns.Contains("CategoryID") ? Convert.ToInt32(dr["CategoryID"]) : 0,
                    ProfileID = dr.Table.Columns.Contains("ProfileID") ? Convert.ToInt32(dr["ProfileID"]) : 0,
                    EffectiveDate = dr.Table.Columns.Contains("EffectiveDate") ? Convert.ToDateTime(dr["EffectiveDate"]) : DateTime.Now,
                    ProfileName = dr.Table.Columns.Contains("Name") ? dr["Name"].ToString() : string.Empty

                };
                modelList.CategoryProfiles.Add(model);

            }


            return modelList;
        }

        /// <summary>
        ///  Maps the CategoryProfile model to CategoryProfile entity.
        /// </summary>
        /// <param name="model">Modelof type CategoryProfile</param>
        /// <returns>Retuns the mapped CategoryProfile model to CategoryProfile entity.</returns>
        public static CategoryProfile ToEntity(CategoryProfileModel model)
        {
            if (model == null)
            {
                return null;
            }

            var entity = new CategoryProfile
            {
                CategoryProfileID = model.CategoryProfileID,
                CategoryID = model.CategoryID,
                ProfileID = model.ProfileID,
                EffectiveDate = model.EffectiveDate
            };

            return entity;
        } 

        #endregion

    }
}
