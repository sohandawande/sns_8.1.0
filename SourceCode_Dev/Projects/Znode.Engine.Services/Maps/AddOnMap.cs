﻿using System;
using System.Data;
using System.Linq;
using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
    public static class AddOnMap
    {
        public static AddOnModel ToModel(AddOn entity, int productAddOnId = 0)
        {
            if (entity == null)
            {
                return null;
            }

            var model = new AddOnModel
            {
                AccountId = entity.AccountID,
                AddOnId = entity.AddOnID,
                AllowBackOrder = entity.AllowBackOrder,
                BackOrderMessage = entity.BackOrderMsg,
                Description = entity.Description,
                DisplayOrder = entity.DisplayOrder,
                DisplayType = entity.DisplayType,
                ExternalApiId = entity.ExternalAPIID,
                ExternalId = entity.ExternalID,
                InStockMessage = entity.InStockMsg,
                IsOptional = entity.OptionalInd,
                LocaleId = entity.LocaleId,
                Name = entity.Name,
                OutOfStockMessage = entity.OutOfStockMsg,
                PortalId = entity.PortalID,
                ProductId = entity.ProductID,
                PromptMessage = entity.PromptMsg,
                Title = entity.Title,
                TrackInventory = entity.TrackInventoryInd,
                ProductAddOnId = productAddOnId
            };

            foreach (var a in entity.AddOnValueCollection)
            {
                model.AddOnValues.Add(AddOnValueMap.ToModel(a));
            }

            return model;
        }

        public static AddOn ToEntity(AddOnModel model)
        {
            if (model == null)
            {
                return null;
            }

            var entity = new AddOn
            {
                AccountID = model.AccountId,
                AddOnID = model.AddOnId,
                AllowBackOrder = model.AllowBackOrder,
                BackOrderMsg = model.BackOrderMessage,
                Description = model.Description,
                DisplayOrder = model.DisplayOrder,
                DisplayType = model.DisplayType,
                ExternalAPIID = model.ExternalApiId,
                ExternalID = model.ExternalId,
                InStockMsg = model.InStockMessage,
                LocaleId = model.LocaleId,
                Name = model.Name,
                OptionalInd = model.IsOptional,
                OutOfStockMsg = model.OutOfStockMessage,
                PortalID = model.PortalId,
                ProductID = model.ProductId,
                PromptMsg = model.PromptMessage,
                Title = model.Title,
                TrackInventoryInd = model.TrackInventory
            };


            foreach (var addOnValueModel in model.AddOnValues)
            {
                entity.AddOnValueCollection.Add(AddOnValueMap.ToEntity(addOnValueModel));
            }


            return entity;
        }

        public static AddOnListModel ToModelList(DataSet result)
        {
            if (result == null)
            {
                return null;
            }

            var modelList = new AddOnListModel();
            foreach (DataRow dr in result.Tables[0].Rows)
            {
                AddOnModel model = new AddOnModel
                {
                    AddOnId = dr.Table.Columns.Contains("AddOnID") ? Convert.ToInt32(dr["AddOnID"]) : 0,
                    ProductId = dr.Table.Columns.Contains("ProductID") ? Convert.ToInt32(dr["ProductID"]) : 0,
                    Name = (dr["Name"] != DBNull.Value) ? dr["Name"].ToString() : string.Empty,
                    DisplayOrder = (dr["DisplayOrder"] != DBNull.Value) ? Convert.ToInt32(dr["DisplayOrder"]) : 99,
                    Title = dr.Table.Columns.Contains("Title") ? dr["Title"].ToString() : string.Empty
                };
                modelList.AddOns.Add(model);
            }

            return modelList;
        }
    }


}
