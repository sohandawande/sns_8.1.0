﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
    public class RequestStatusMap
    {
        /// <summary>
        /// Converts entity to Model.
        /// </summary>
        /// <param name="entity">Request Status entity</param>
        /// <returns>Request status model.</returns>
        public static RequestStatusModel ToModel(RequestStatus entity)
        {
            if (Equals(entity,null))
            {
                return null;
            }
            var model = new RequestStatusModel
            {
                AdminNotification=entity.AdminNotification,
                CustomerNotification=entity.CustomerNotification,
                IsEnabled=entity.IsEnabled,
                Name=entity.Name,
                RequestStatusID=entity.RequestStatusID
            };
            return model;
        }

        /// <summary>
        /// Mapping ReasonForReturn Entity to ReasonForReturn Model
        /// </summary>
        /// <param name="entity">ReasonForReturn Model</param>
        /// <returns>ReasonForReturn Entity</returns>
        public static RequestStatus ToEntity(RequestStatusModel model)
        {
            if (Equals(model, null))
            {
                return null;
            }

            var entity = new RequestStatus
            {
                RequestStatusID = model.RequestStatusID,
                AdminNotification = model.AdminNotification,
                CustomerNotification = model.CustomerNotification,
                Name = model.Name,
                IsEnabled = model.IsEnabled,
            };
            return entity;
        }
    }
}
