﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
    /// <summary>
    /// Map for Sku Profile Effective.
    /// </summary>
    public static class SkuProfileEffectiveMap
    {
        /// <summary>
        /// Convert entity to model
        /// </summary>
        /// <param name="entity">To convert to model</param>
        /// <returns>SkuProfileEffectiveModel</returns>
        public static SkuProfileEffectiveModel ToModel(SKUProfileEffective entity)
        {
            if (Equals(entity, null))
            {
                return null;
            }

            var model = new SkuProfileEffectiveModel
            {
                EffectiveDate = entity.EffectiveDate,
                ProfileId = entity.ProfileId,
                SkuId = entity.SkuId,
                SkuProfileEffectiveID = entity.SkuProfileEffectiveID
            };

            return model;
        }

        /// <summary>
        /// Convert model to entity 
        /// </summary>
        /// <param name="model">To convert to entity</param>
        /// <returns>SKUProfileEffective</returns>
        public static SKUProfileEffective ToEntity(SkuProfileEffectiveModel model)
        {
            if (Equals(model, null))
            {
                return null;
            }

            var entity = new SKUProfileEffective
            {
                EffectiveDate = model.EffectiveDate,
                ProfileId = model.ProfileId,
                SkuId = model.SkuId,
                SkuProfileEffectiveID = model.SkuProfileEffectiveID
            };

            return entity;
        }

        /// <summary>
        /// Convert dateset to list model 
        /// </summary>
        /// <param name="result">to convert to list model</param>
        /// <returns>SkuProfileEffectiveListModel</returns>
        public static SkuProfileEffectiveListModel ToModelList(DataSet result)
        {
            if (Equals(result, null))
            {
                return null;
            }

            var modelList = new SkuProfileEffectiveListModel();
            foreach (DataRow dr in result.Tables[0].Rows)
            {
                SkuProfileEffectiveModel model = new SkuProfileEffectiveModel
                {
                    ProfileId = dr.Table.Columns.Contains("ProfileId") ? Convert.ToInt32(dr["ProfileId"]) : 0,
                    SkuId = dr.Table.Columns.Contains("SkuId") ? Convert.ToInt32(dr["SkuId"]) : 0,
                    SkuProfileEffectiveID = dr.Table.Columns.Contains("SkuProfileEffectiveID") ? Convert.ToInt32(dr["SkuProfileEffectiveID"]) : 0,
                    EffectiveDate = (dr["EffectiveDate"] != DBNull.Value) ? Convert.ToDateTime(dr["EffectiveDate"]) : DateTime.Now,

                };
                modelList.SkuProfileEffectives.Add(model);
            }

            return modelList;
        }
    }
}
