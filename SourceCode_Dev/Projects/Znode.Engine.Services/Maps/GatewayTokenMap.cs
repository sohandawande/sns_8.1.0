﻿using ZNode.Libraries.ECommerce.Entities;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services.Maps
{
	public static class GatewayTokenMap
	{
		public static GatewayTokenModel ToModel(GatewayToken entity)
		{
			if (entity == null)
			{
				return null;
			}

			var model = new GatewayTokenModel
			{
				AuthorizationCode = entity.AuthCode,
				CardExpiration = entity.CreditCardExp,
				CardSecurityCode = entity.CardSecurityCode,
				TransactionId = entity.TransactionID
			};

			return model;
		}

		public static GatewayToken ToEntity(GatewayTokenModel model)
		{
			if (model == null)
			{
				return null;
			}

			var entity = new GatewayToken
			{
				AuthCode = model.AuthorizationCode,
				CardSecurityCode = model.CardSecurityCode,
				CreditCardExp = model.CardExpiration,
				TransactionID = model.TransactionId
			};

			return entity;
		}
	}
}
