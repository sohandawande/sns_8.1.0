﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
    public static class ProductImageTypeMap
    {
        /// <summary>
        /// Convert Product image type entity to product image type model.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>Product image model</returns>
        public static ProductImageTypeModel ToModel(ProductImageType entity)
        {
            if (Equals(entity,null))
            {
                return null;
            }
            var model = new ProductImageTypeModel()
           {
               Description = entity.Description,
               Name = entity.Name,
               ProductImageTypeID = entity.ProductImageTypeID,
           };
            return model;
        }
    }
}
