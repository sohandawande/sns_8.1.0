﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
    /// <summary>
    /// Map for Url Redirect.
    /// </summary>
    public static class UrlRedirectMap
    {
        /// <summary>
        /// Convert Entity to model.
        /// </summary>
        /// <param name="entity">Entity to convert.</param>
        /// <returns>UrlRedirectModel</returns>
        public static UrlRedirectModel ToModel(UrlRedirect entity)
        {
            if (Equals(entity, null))
            {
                return null;
            }

            var model = new UrlRedirectModel
                {
                    IsActive = entity.IsActive,
                    NewUrl = entity.NewUrl,
                    OldUrl = entity.OldUrl,
                    UrlRedirectId = entity.UrlRedirectID

                };
            return model;
        }

        /// <summary>
        ///  Convert model to Entity.
        /// </summary>
        /// <param name="model">Model To convert Entity.</param>
        /// <returns>UrlRedirect</returns>
        public static UrlRedirect ToEntity(UrlRedirectModel model)
        {
            if (Equals(model, null))
            {
                return null;
            }

            var entity = new UrlRedirect
            {
                IsActive = model.IsActive,
                NewUrl = model.NewUrl,
                OldUrl = model.OldUrl,
                UrlRedirectID = model.UrlRedirectId
            };

            return entity;
        }
    }
}
