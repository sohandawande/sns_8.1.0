﻿using System;
using ZNode.Libraries.DataAccess.Entities;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services.Maps
{
	public static class AddressMap
	{
		public static AddressModel ToModel(Address entity)
		{
			if (entity == null)
			{
				return null;
			}

			var model = new AddressModel
			{
				AccountId = entity.AccountID,
				AddressId = entity.AddressID,
				City = entity.City,
				CompanyName = entity.CompanyName,
				CountryCode = entity.CountryCode,
				FirstName = entity.FirstName,
				IsDefaultBilling = entity.IsDefaultBilling,
				IsDefaultShipping = entity.IsDefaultShipping,
				LastName = entity.LastName,
				MiddleName = entity.MiddleName,
				Name = entity.Name,
				PhoneNumber = entity.PhoneNumber,
				PostalCode = entity.PostalCode,
				StateCode = entity.StateCode,
				StreetAddress1 = entity.Street,
				StreetAddress2 = entity.Street1,
			};

			return model;
		}

		public static Address ToEntity(AddressModel model)
		{
			if (model == null)
			{
				return null;
			}

			var address = new Address
			{
				AccountID = model.AccountId,
				AddressID = model.AddressId,
				City = model.City,
				CompanyName = model.CompanyName,
				CountryCode = model.CountryCode,
				FirstName = model.FirstName,
				IsDefaultBilling = model.IsDefaultBilling,
				IsDefaultShipping = model.IsDefaultShipping,
				LastName = model.LastName,
				MiddleName = model.MiddleName,
				Name = model.Name,
				PhoneNumber = model.PhoneNumber,
				PostalCode = model.PostalCode,
				StateCode = model.StateCode,
				Street = model.StreetAddress1,
				Street1 = model.StreetAddress2
			};

			return address;
		}

		public static Address ToEntity(AddressModel model, Address entity)
		{
			if (model == null || entity == null)
			{
				return entity;
			}

			if (entity.AddressID == model.AddressId)
			{
				entity.AccountID = model.AccountId;
				entity.City = model.City;
				entity.CompanyName = model.CompanyName;
				entity.CountryCode = model.CountryCode;
				entity.FirstName = model.FirstName;
				entity.IsDefaultBilling = model.IsDefaultBilling;
				entity.IsDefaultShipping = model.IsDefaultShipping;
				entity.LastName = model.LastName;
				entity.MiddleName = model.MiddleName;
				entity.Name = model.Name;
				entity.PhoneNumber = model.PhoneNumber;
				entity.PostalCode = model.PostalCode;
				entity.StateCode = model.StateCode;
				entity.Street = model.StreetAddress1;
				entity.Street1 = model.StreetAddress2;
			}

			return entity;
		}

		public static Address GetDefaultBillingAddress(int accountId)
		{
			var billingAddress = GetDefaultAddress(accountId);
			billingAddress.IsDefaultBilling = true;
			billingAddress.Name = "Default Billing";

			return billingAddress;
		}

		public static Address GetDefaultShippingAddress(int accountId)
		{
			var shippingAddress = GetDefaultAddress(accountId);
			shippingAddress.IsDefaultShipping = true;
			shippingAddress.Name = "Default Shipping";

			return shippingAddress;
		}

		private static Address GetDefaultAddress(int accountId)
		{
			var address = new Address
			{
				AccountID = accountId,
				City = String.Empty,
				CompanyName = String.Empty,
				CountryCode = String.Empty,
				FirstName = String.Empty,
				LastName = String.Empty,
				MiddleName = String.Empty,
				PhoneNumber = String.Empty,
				PostalCode = String.Empty,
				StateCode = String.Empty,
				Street = String.Empty,
				Street1 = String.Empty
			};

			return address;
		}
	}
}
