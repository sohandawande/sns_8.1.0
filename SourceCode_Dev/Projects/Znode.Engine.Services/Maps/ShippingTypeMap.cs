﻿using Znode.Engine.Api.Models;
using Znode.Engine.Shipping;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
	public static class ShippingTypeMap
	{
		public static ShippingTypeModel ToModel(ShippingType entity)
		{
			if (entity == null)
			{
				return null;
			}

			var model = new ShippingTypeModel
			{
				ClassName = entity.ClassName,
				Description = entity.Description,
				IsActive = entity.IsActive,
				Name = entity.Name,
				ShippingTypeId = entity.ShippingTypeID
			};

			return model;
		}

		public static ShippingType ToEntity(ShippingTypeModel model)
		{
			if (model == null)
			{
				return null;
			}

			var entity = new ShippingType
			{
				ClassName = model.ClassName,
				Description = model.Description,
				IsActive = model.IsActive,
				Name = model.Name,
				ShippingTypeID = model.ShippingTypeId
			};

			return entity;
		}

        // <summary>
        /// Znode version 8.0
        /// Convert IZnodeShippingType to ShippingTypeModel
        /// </summary>
        /// <param name="znodeShippingType">IZnodeShippingType znodeShippingType</param>
        /// <returns>Returns ShippingTypeModel</returns>
        public static ShippingTypeModel ToModel(IZnodeShippingType znodeShippingType)
        {
            if (Equals(znodeShippingType, null))
            {
                return null;
            }
            var model = new ShippingTypeModel
            {
                ClassName = znodeShippingType.ClassName,
                Description = znodeShippingType.Description,
                Name = znodeShippingType.Name,
            };
            return model;
        }
	}
}
