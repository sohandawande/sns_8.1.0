﻿using System;
using System.Data;
using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
    public static class AttributeMap
    {
        public static AttributeModel ToModel(ProductAttribute entity)
        {
            if (entity == null)
            {
                return null;
            }

            var model = new AttributeModel
            {
                AttributeId = entity.AttributeId,
                AttributeType = AttributeTypeMap.ToModel(entity.AttributeType),
                AttributeTypeId = entity.AttributeTypeId,
                DisplayOrder = entity.DisplayOrder,
                ExternalId = entity.ExternalId,
                IsActive = entity.IsActive,
                Name = entity.Name,
                OldAttributeId = entity.OldAttributeId
            };

            return model;
        }

        //public static ProductAttribute ToEntity(AttributeModel model)
        public static ProductAttribute ToEntity(AttributeModel model, ProductAttribute attributes = null)
        {
            if (Equals(model, null))
            {
                return null;
            }

            var entity = new ProductAttribute
            {
                AttributeId = model.AttributeId,
                AttributeTypeId = model.AttributeTypeId,
                DisplayOrder = model.DisplayOrder,
                //ExternalId = model.ExternalId,
                ExternalId = Equals(attributes, null) ? string.Empty : attributes.ExternalId,
                //IsActive = model.IsActive,
                IsActive = Equals(attributes, null) ? true : attributes.IsActive,
                Name = model.Name,
                OldAttributeId = model.OldAttributeId
            };

            return entity;
        }

        public static ProductAttributeListModel ToModelList(DataSet result)
        {
            if (Equals(result, null))
            {
                return null;
            }

            var modelList = new ProductAttributeListModel();
            foreach (DataRow dr in result.Tables[0].Rows)
            {
                AttributeModel model = new AttributeModel
                {
                    AttributeId = Convert.ToInt32(dr["AttributeId"]),
                    AttributeTypeId = Convert.ToInt32(dr["AttributeTypeId"]),
                    DisplayOrder = Convert.ToInt32(dr["DisplayOrder"]),
                    ExternalId = dr["AttributeExternalId"].ToString(),
                    IsActive = Convert.ToBoolean(dr["AttributeIsActive"]),
                    Name = dr["AttributeName"].ToString(),
                    AttributeTypeName = dr["Name"].ToString()
                };
                modelList.Attributes.Add(model);
            }

            return modelList;
        }
    }
}
