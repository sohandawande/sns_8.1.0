﻿using ZNode.Libraries.DataAccess.Entities;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services.Maps
{
	public static class OrderMap
	{
		public static OrderModel ToModel(Order entity,string message=null)
		{
			if (entity == null)
			{
				return null;
			}

			var model = new OrderModel
			{
				Account = AccountMap.ToModel(entity.AccountIDSource),
				AccountId = entity.AccountID,
				AdditionalInstructions = entity.AdditionalInstructions,
				BillingCity = entity.BillingCity,
				BillingCompanyName = entity.BillingCompanyName,
				BillingCountryCode = entity.BillingCountry,
				BillingEmail = entity.BillingEmailId,
				BillingFirstName = entity.BillingFirstName,
				BillingLastName = entity.BillingLastName,
				BillingPhoneNumber = entity.BillingPhoneNumber,
				BillingPostalCode = entity.BillingPostalCode,
				BillingStateCode = entity.BillingStateCode,
				BillingStreetAddress1 = entity.BillingStreet,
				BillingStreetAddress2 = entity.BillingStreet1,
				CardAuthorizationCode = entity.CardAuthCode,
				CardExpiration = entity.CardExp,
				CardTransactionId = entity.CardTransactionID,
				CardTypeId = entity.CardTypeId,
				CouponCode = entity.CouponCode,
				CreateDate = entity.Created,
				CreatedBy = entity.CreatedBy,
				Custom1 = entity.Custom1,
				Custom2 = entity.Custom2,
				Custom3 = entity.Custom3,
				DiscountAmount = entity.DiscountAmount,
				ExternalId = entity.ExternalID,
				Gst = entity.GST,
				Hst = entity.HST,
				ModifiedBy = entity.ModifiedBy,
				ModifiedDate = entity.Modified,
				OrderDate = entity.OrderDate,
				OrderId = entity.OrderID,
				OrderStateId = entity.OrderStateID,
				PaymentOption = PaymentOptionMap.ToModel(entity.PaymentSettingIDSource),
				PaymentOptionId = entity.PaymentSettingID,
				PaymentStatusId = entity.PaymentStatusID,
				PaymentType = PaymentTypeMap.ToModel(entity.PaymentType),
				PaymentTypeId = entity.PaymentTypeId,
				PortalId = entity.PortalId,
				PromotionDescription = entity.PromoDescription,
				Pst = entity.PST,
				PurchaseOrderNumber = entity.PurchaseOrderNumber,
				ReferralAccountId = entity.ReferralAccountID,
				ReturnDate = entity.ReturnDate,
				SalesTax = entity.SalesTax,
				ShipDate = entity.ShipDate,
				ShippingCost = entity.ShippingCost,
				ShippingOption = ShippingOptionMap.ToModel(entity.ShippingIDSource),
				ShippingOptionId = entity.ShippingID,
				SubTotal = entity.SubTotal,
				TaxCost = entity.TaxCost,
				Total = entity.Total,
				TrackingNumber = entity.TrackingNumber,
				Vat = entity.VAT,
				WebServiceDownloadDate = entity.WebServiceDownloadDate,
                TrackingText=message
			};

			foreach (var item in entity.OrderLineItemCollection)
			{
				model.OrderLineItems.Add(OrderLineItemMap.ToModel(item));
			}

			return model;
		}

		public static Order ToEntity(OrderModel model)
		{
			if (model == null)
			{
				return null;
			}

			var entity = new Order
			{
				AccountID = model.AccountId,
				AdditionalInstructions = model.AdditionalInstructions,
				BillingCity = model.BillingCity,
				BillingCompanyName = model.BillingCompanyName,
				BillingCountry = model.BillingCountryCode,
				BillingEmailId = model.BillingEmail,
				BillingFirstName = model.BillingFirstName,
				BillingLastName = model.BillingLastName,
				BillingPhoneNumber = model.BillingPhoneNumber,
				BillingPostalCode = model.BillingPostalCode,
				BillingStateCode = model.BillingStateCode,
				BillingStreet = model.BillingStreetAddress1,
				BillingStreet1 = model.BillingStreetAddress2,
				CardAuthCode = model.CardAuthorizationCode,
				CardExp = model.CardExpiration,
				CardTransactionID = model.CardTransactionId,
				CardTypeId = model.CardTypeId,
				CouponCode = model.CouponCode,
				Created = model.CreateDate,
				CreatedBy = model.CreatedBy,
				Custom1 = model.Custom1,
				Custom2 = model.Custom2,
				Custom3 = model.Custom3,
				DiscountAmount = model.DiscountAmount,
				ExternalID = model.ExternalId,
				GST = model.Gst,
				HST = model.Hst,
				ModifiedBy = model.ModifiedBy,
				Modified = model.ModifiedDate,
				OrderDate = model.OrderDate,
				OrderID = model.OrderId,
				OrderStateID = model.OrderStateId,
				PaymentSettingID = model.PaymentOptionId,
				PaymentStatusID = model.PaymentStatusId,
				PaymentTypeId = model.PaymentTypeId,
				PortalId = model.PortalId,
				PromoDescription = model.PromotionDescription,
				PST = model.Pst,
				PurchaseOrderNumber = model.PurchaseOrderNumber,
				ReferralAccountID = model.ReferralAccountId,
				ReturnDate = model.ReturnDate,
				SalesTax = model.SalesTax,
				ShipDate = model.ShipDate,
				ShippingCost = model.ShippingCost,
				ShippingID = model.ShippingOptionId,
				SubTotal = model.SubTotal,
				TaxCost = model.TaxCost,
				Total = model.Total,
				TrackingNumber = model.TrackingNumber,
				VAT = model.Vat,
				WebServiceDownloadDate = model.WebServiceDownloadDate
			};

			foreach (var item in model.OrderLineItems)
			{
				entity.OrderLineItemCollection.Add(OrderLineItemMap.ToEntity(item));
			}

			return entity;
		}
	}
}
