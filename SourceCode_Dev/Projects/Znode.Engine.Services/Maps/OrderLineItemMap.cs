﻿using ZNode.Libraries.DataAccess.Entities;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services.Maps
{
	public static class OrderLineItemMap
	{
		public static OrderLineItemModel ToModel(OrderLineItem entity)
		{
			if (entity == null)
			{
				return null;
			}

			var model = new OrderLineItemModel
			{
				AppliedPromotion = entity.AppliedPromo,
				AutoGeneratedTracking = entity.AutoGeneratedTracking,
				CouponsApplied = entity.CouponsApplied,
				CreateDate = entity.Created,
				CreatedBy = entity.CreatedBy,
				Custom1 = entity.Custom1,
				Custom2 = entity.Custom2,
				Custom3 = entity.Custom3,
				Description = entity.Description,
				DiscountAmount = entity.DiscountAmount,
				DownloadLink = entity.DownloadLink,
				ExternalId = entity.ExternalID,
				Gst = entity.GST,
				Hst = entity.HST,
				IsRecurringBilling = entity.IsRecurringBilling,
				ModifiedBy = entity.ModifiedBy,
				ModifiedDate = entity.Modified,
				Name = entity.Name,
				OrderId = entity.OrderID,
				OrderLineItemId = entity.OrderLineItemID,
				OrderLineItemRelationshipTypeId = entity.OrderLineItemRelationshipTypeID,
				OrderLineItemStateId = entity.OrderLineItemStateID,
				OrderShipment = OrderShipmentMap.ToModel(entity.OrderShipmentIDSource),
				OrderShipmentId = entity.OrderShipmentID,
				ParentOrderLineItemId = entity.ParentOrderLineItemID,
				PaymentStatusId = entity.PaymentStatusID,
				Price = entity.Price,
				ProductNumber = entity.ProductNum,
				PromotionDescription = entity.PromoDescription,
				Pst = entity.PST,
				Quantity = entity.Quantity,
				RecurringBillingAmount = entity.RecurringBillingAmount,
				RecurringBillingCycles = entity.RecurringBillingCycles,
				RecurringBillingFrequency = entity.RecurringBillingFrequency,
				RecurringBillingPeriod = entity.RecurringBillingPeriod,
				ReturnDate = entity.ReturnDate,
				SalesTax = entity.SalesTax,
				ShipDate = entity.ShipDate,
				ShipSeparately = entity.ShipSeparately,
				ShippingCost = entity.ShippingCost,
				Sku = entity.SKU,
				TrackingNumber = entity.TrackingNumber,
				TransactionNumber = entity.TransactionNumber,
				Vat = entity.VAT,
				Weight = entity.Weight
			};

			return model;
		}

		public static OrderLineItem ToEntity(OrderLineItemModel model)
		{
			if (model == null)
			{
				return null;
			}

			var entity = new OrderLineItem
			{
				AppliedPromo = model.AppliedPromotion,
				AutoGeneratedTracking = model.AutoGeneratedTracking,
				CouponsApplied = model.CouponsApplied,
				Created = model.CreateDate,
				CreatedBy = model.CreatedBy,
				Custom1 = model.Custom1,
				Custom2 = model.Custom2,
				Custom3 = model.Custom3,
				Description = model.Description,
				DiscountAmount = model.DiscountAmount,
				DownloadLink = model.DownloadLink,
				ExternalID = model.ExternalId,
				GST = model.Gst,
				HST = model.Hst,
				IsRecurringBilling = model.IsRecurringBilling,
				ModifiedBy = model.ModifiedBy,
				Modified = model.ModifiedDate,
				Name = model.Name,
				OrderID = model.OrderId,
				OrderLineItemID = model.OrderLineItemId,
				OrderLineItemRelationshipTypeID = model.OrderLineItemRelationshipTypeId,
				OrderLineItemStateID = model.OrderLineItemStateId,
				OrderShipmentID = model.OrderShipmentId,
				ParentOrderLineItemID = model.ParentOrderLineItemId,
				PaymentStatusID = model.PaymentStatusId,
				Price = model.Price,
				ProductNum = model.ProductNumber,
				PromoDescription = model.PromotionDescription,
				PST = model.Pst,
				Quantity = model.Quantity,
				RecurringBillingAmount = model.RecurringBillingAmount,
				RecurringBillingCycles = model.RecurringBillingCycles,
				RecurringBillingFrequency = model.RecurringBillingFrequency,
				RecurringBillingPeriod = model.RecurringBillingPeriod,
				ReturnDate = model.ReturnDate,
				SalesTax = model.SalesTax,
				ShipDate = model.ShipDate,
				ShipSeparately = model.ShipSeparately,
				ShippingCost = model.ShippingCost,
				SKU = model.Sku,
				TrackingNumber = model.TrackingNumber,
				TransactionNumber = model.TransactionNumber,
				VAT = model.Vat,
				Weight = model.Weight
			};

			return entity;
		}
	}
}
