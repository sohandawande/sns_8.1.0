﻿using System;
using System.Data;
using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
    /// <summary>
    /// Mapper for Tax Rule
    /// </summary>
	public static class TaxRuleMap
	{
		public static TaxRuleModel ToModel(TaxRule entity)
		{
			if (entity == null)
			{
				return null;
			}

			var model = new TaxRuleModel
			{
				CountryCode = entity.DestinationCountryCode,
				CountyFips = entity.CountyFIPS,
				Custom1 = entity.Custom1,
				Custom2 = entity.Custom2,
				Custom3 = entity.Custom3,
				ExternalId = entity.ExternalID,
				Gst = entity.GST,
				Hst = entity.HST,
				IsInclusive = entity.InclusiveInd,
				PortalId = entity.PortalID,
				Precedence = entity.Precedence,
				Pst = entity.PST,
				SalesTax = entity.SalesTax,
				StateCode = entity.DestinationStateCode,
				TaxClass = TaxClassMap.ToModel(entity.TaxClassIDSource),
				TaxClassId = entity.TaxClassID,
				TaxRuleId = entity.TaxRuleID,
				TaxRuleType = TaxRuleTypeMap.ToModel(entity.TaxRuleTypeIDSource),
				TaxRuleTypeId = entity.TaxRuleTypeID,
				TaxShipping = entity.TaxShipping,
				Vat = entity.VAT
			};

			return model;
		}

		public static TaxRule ToEntity(TaxRuleModel model)
		{
			if (model == null)
			{
				return null;
			}

			var entity = new TaxRule
			{
				CountyFIPS = model.CountyFips,
				Custom1 = model.Custom1,
				Custom2 = model.Custom2,
				Custom3 = model.Custom3,
				DestinationCountryCode = model.CountryCode,
				DestinationStateCode = model.StateCode,
				ExternalID = model.ExternalId,
				GST = model.Gst,
				HST = model.Hst,
				InclusiveInd = model.IsInclusive,
		        PortalID = model.PortalId,
				Precedence = model.Precedence,
				PST = model.Pst,
				SalesTax = model.SalesTax,
				TaxClassID = model.TaxClassId,
				TaxRuleID = model.TaxRuleId,
				TaxRuleTypeID = model.TaxRuleTypeId,
				TaxShipping = model.TaxShipping,
				VAT = model.Vat
			};

			return entity;
		}        

        public static TaxRuleListModel ToListModel(TList<TaxRule> entity)
        {
            if (Equals(entity, null))
            {
                return null;
            }
            var model = new TaxRuleListModel();
            foreach (var item in entity)
            {
                model.TaxRules.Add(ToModel(item));
            }
            return model;
        }
	}
}
