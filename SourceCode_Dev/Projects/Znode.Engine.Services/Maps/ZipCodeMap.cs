﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
    public static class ZipCodeMap
   {
		public static ZipCodeModel ToModel(ZipCode entity)
		{
			if (entity == null)
			{
				return null;
			}

            var model = new ZipCodeModel
			{
				CountyFips = entity.CountyFIPS,
				CountyName = entity.CountyName,				
			};

			return model;
		}

        public static ZipCode ToEntity(ZipCodeModel model)
		{
			if (model == null)
			{
				return null;
			}

            var entity = new ZipCode
			{
                CountyFIPS = model.CountyFips,
                CountyName = model.CountyName,				
			};

			return entity;
		}
	}
}
