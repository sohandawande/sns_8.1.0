﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
    public static class PromotionMap
    {
        public static PromotionModel ToModel(Promotion entity)
        {
            if (Equals(entity, null))
            {
                return null;
            }

            var model = new PromotionModel
            {
                AccountId = entity.AccountID,
                AddOnValueId = entity.AddOnValueID,
                CatalogId = entity.CatalogID,
                CategoryId = entity.CategoryID,
                CouponCode = entity.CouponCode,
                CouponQuantityAvailable = entity.CouponQuantityAvailable,
                Custom1 = entity.Custom1,
                Custom2 = entity.Custom2,
                Custom3 = entity.Custom3,
                Description = entity.Description,
                Discount = entity.Discount,
                DiscountedProductId = entity.PromotionProductID,
                DiscountedProductQuantity = entity.PromotionProductQty,
                DisplayOrder = entity.DisplayOrder,
                EnableCouponUrl = entity.EnableCouponUrl,
                EndDate = entity.EndDate,
                ExternalId = entity.ExternalID,
                HasCoupon = entity.CouponInd,
                ManufacturerId = entity.ManufacturerID,
                MinimumOrderAmount = entity.OrderMinimum,
                Name = entity.Name,
                PortalId = entity.PortalID,
                Profile = (!Equals(entity.ProfileIDSource, null) && !Equals(entity.ProfileIDSource.Name, null)) ? entity.ProfileIDSource.Name : "All Profiles",
                StoreName = (!Equals(entity.PortalIDSource, null) && !Equals(entity.PortalIDSource.StoreName, null)) ? entity.PortalIDSource.StoreName : "All Stores",
                ProfileId = entity.ProfileID,
                PromoCode = entity.PromoCode,
                PromotionId = entity.PromotionID,
                PromotionMessage = entity.PromotionMessage,
                PromotionType = PromotionTypeMap.ToModel(entity.DiscountTypeIDSource),
                PromotionTypeId = entity.DiscountTypeID,
                RequiredProductId = entity.ProductID,
                RequiredProductMinimumQuantity = entity.QuantityMinimum,
                SkuId = entity.SKUID,
                StartDate = entity.StartDate,
                IsCouponAllowedWithOtherCoupons = entity.IsCouponAllowedWithOtherCoupons
            };

            return model;
        }

        public static Promotion ToEntity(PromotionModel model)
        {
            if (Equals(model, null))
            {
                return null;
            }

            var entity = new Promotion
            {
                AccountID = model.AccountId,
                AddOnValueID = model.AddOnValueId,
                CatalogID = model.CatalogId,
                CategoryID = model.CategoryId,
                CouponCode = model.CouponCode,
                CouponQuantityAvailable = model.CouponQuantityAvailable,
                CouponInd = model.HasCoupon,
                Custom1 = model.Custom1,
                Custom2 = model.Custom2,
                Custom3 = model.Custom3,
                Description = model.Description,
                Discount = model.Discount,
                DiscountTypeID = model.PromotionTypeId,
                DisplayOrder = model.DisplayOrder,
                EnableCouponUrl = model.EnableCouponUrl,
                EndDate = model.EndDate,
                ExternalID = model.ExternalId,
                ManufacturerID = model.ManufacturerId,
                OrderMinimum = model.MinimumOrderAmount,
                Name = model.Name,
                PortalID = model.PortalId,
                ProductID = model.RequiredProductId,
                ProfileID = model.ProfileId,
                PromoCode = model.PromoCode,
                PromotionID = model.PromotionId,
                PromotionMessage = model.PromotionMessage,
                PromotionProductID = model.DiscountedProductId,
                PromotionProductQty = model.DiscountedProductQuantity,
                QuantityMinimum = model.RequiredProductMinimumQuantity,
                SKUID = model.SkuId,
                StartDate = model.StartDate,
                IsCouponAllowedWithOtherCoupons=model.IsCouponAllowedWithOtherCoupons
            };

            return entity;
        }
    }
}
