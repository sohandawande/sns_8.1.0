﻿using ZNode.Libraries.ECommerce.Entities;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services.Maps
{
	public static class ShippingMap
	{
		public static ShippingModel ToModel(ZNodeShipping znodeShipping)
		{
			if (znodeShipping == null)
			{
				return null;
			}

			var model = new ShippingModel
			{
				ResponseCode = znodeShipping.ResponseCode,
				ResponseMessage = znodeShipping.ResponseMessage,
				ShippingDiscount = znodeShipping.ShippingDiscount,
				ShippingHandlingCharge = znodeShipping.ShippingHandlingCharge,
				ShippingName = znodeShipping.ShippingName,
				ShippingOptionId = znodeShipping.ShippingID
			};

			return model;
		}

		public static ZNodeShipping ToZnodeShipping(ShippingModel model)
		{
			if (model == null)
			{
				// Cannot be null, so create new one with shipping ID of 0
				return new ZNodeShipping { ShippingID = 0 };
			}

			var znodeShipping = new ZNodeShipping
			{
				ResponseCode = model.ResponseCode,
				ResponseMessage = model.ResponseMessage,
				ShippingDiscount = model.ShippingDiscount,
				ShippingHandlingCharge = model.ShippingHandlingCharge,
				ShippingID = model.ShippingOptionId,
				ShippingName = model.ShippingName
			};

			return znodeShipping;
		}
	}
}
