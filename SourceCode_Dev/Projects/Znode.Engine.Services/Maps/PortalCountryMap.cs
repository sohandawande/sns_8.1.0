﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
	public static class PortalCountryMap
	{
		public static PortalCountryModel ToModel(PortalCountry entity)
		{
			if (entity == null)
			{
				return null;
			}

			var model = new PortalCountryModel
			{
				Country = CountryMap.ToModel(entity.CountryCodeSource),
				CountryCode = entity.CountryCode,
				IsBillingActive = entity.BillingActive,
				IsShippingActive = entity.ShippingActive,
				PortalCountryId = entity.PortalCountryID,
				PortalId = entity.PortalID
			};

			return model;
		}

		public static PortalCountry ToEntity(PortalCountryModel model)
		{
			if (model == null)
			{
				return null;
			}

			var entity = new PortalCountry
			{
				BillingActive = model.IsBillingActive,
				CountryCode = model.CountryCode,
				PortalCountryID = model.PortalCountryId,
				PortalID = model.PortalId,
				ShippingActive = model.IsShippingActive
			};

			return entity;
		}
	}
}
