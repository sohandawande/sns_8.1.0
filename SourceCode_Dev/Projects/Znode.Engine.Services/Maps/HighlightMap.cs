﻿using System;
using System.Data;
using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.Services.Maps
{
	public static class HighlightMap
	{
		public static HighlightModel ToModel(Highlight entity)
		{
			if (Equals(entity, null))
			{
				return null;
			}

			var imageUtility = new ZNodeImage();

			var model = new HighlightModel
			{
				Description = entity.Description,
				DisplayOrder = entity.DisplayOrder,
				HighlightId = entity.HighlightID,
				HighlightType = HighlightTypeMap.ToModel(entity.HighlightTypeIDSource),
				HighlightTypeId = entity.HighlightTypeID,
				Hyperlink = entity.Hyperlink,
				HyperlinkNewWindow = entity.HyperlinkNewWinInd,
				ImageAltTag = entity.ImageAltTag,
				ImageFile = !Equals(entity.ImageFile,null)?entity.ImageFile:string.Empty,
                ImageMediumPath = imageUtility.GetImageHttpPathMedium(!Equals(entity.ImageFile, null) ? entity.ImageFile : string.Empty),
                ImageLargePath = imageUtility.GetImageHttpPathLarge(!Equals(entity.ImageFile, null) ? entity.ImageFile : string.Empty),
                ImageSmallPath = imageUtility.GetImageHttpPathSmall(!Equals(entity.ImageFile, null) ? entity.ImageFile : string.Empty),
                ImageSmallThumbnailPath = imageUtility.GetImageHttpPathSmallThumbnail(!Equals(entity.ImageFile, null) ? entity.ImageFile : string.Empty),
                ImageThumbnailPath = imageUtility.GetImageHttpPathThumbnail(!Equals(entity.ImageFile, null) ? entity.ImageFile : string.Empty),
				IsActive = entity.ActiveInd,
				LocaleId = entity.LocaleId,
				Name = entity.Name,
				PortalId = entity.PortalID,
				ShortDescription = entity.ShortDescription,
                DisplayPopup = entity.DisplayPopup
			};

			return model;
		}

		public static Highlight ToEntity(HighlightModel model)
		{
            if (Equals(model, null))
			{
				return null;
			}

			var entity = new Highlight
			{
				ActiveInd = model.IsActive,
				Description = model.Description,
				DisplayOrder = model.DisplayOrder,
                DisplayPopup = model.DisplayPopup,
				HighlightID = model.HighlightId,
				HighlightTypeID = model.HighlightTypeId,
				Hyperlink = model.Hyperlink,
				HyperlinkNewWinInd = model.HyperlinkNewWindow,
				ImageAltTag = model.ImageAltTag,
				ImageFile = model.ImageFile,
				LocaleId = model.LocaleId,
				Name = model.Name,
				PortalID = model.PortalId,
				ShortDescription = model.ShortDescription
			};

			return entity;
		}

        public static HighlightListModel ToModelList(DataSet result)
        {
            if (Equals(result, null))
            {
                return null;
            }

            var modelList = new HighlightListModel();
            foreach (DataRow dr in result.Tables[0].Rows)
            {
                HighlightModel model = new HighlightModel
                {
                    HighlightId = dr.Table.Columns.Contains("HighlightId") ? Convert.ToInt32(dr["HighlightId"]) : 0,
                    ProductId = dr.Table.Columns.Contains("ProductID") ? Convert.ToInt32(dr["ProductID"]) : 0,
                    Name = (!Equals(dr["Name"], DBNull.Value)) ? dr["Name"].ToString() : string.Empty,
                    DisplayOrder = (!Equals(dr["DisplayOrder"], DBNull.Value)) ? Convert.ToInt32(dr["DisplayOrder"]) : 99,
                };
                modelList.Highlights.Add(model);
            }

            return modelList;
        }
	}
}
