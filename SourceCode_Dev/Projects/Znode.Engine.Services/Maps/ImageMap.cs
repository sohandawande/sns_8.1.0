﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.Services.Maps
{
	public static class ImageMap
	{
		public static ImageModel ToModel(ProductImage entity)
		{
			if (entity == null)
			{
				return null;
			}

			var imageUtility = new ZNodeImage();

			var model = new ImageModel
			{
				AlternateThumbnailImageFile = entity.AlternateThumbnailImageFile,
				DisplayOrder = entity.DisplayOrder,
				ImageAltTag = entity.ImageAltTag,
				ImageFile = entity.ImageFile,
				ImageMediumPath = imageUtility.GetImageHttpPathMedium(entity.ImageFile),
				ImageLargePath = imageUtility.GetImageHttpPathLarge(entity.ImageFile),
				ImageSmallPath = imageUtility.GetImageHttpPathSmall(entity.ImageFile),
				ImageSmallThumbnailPath = imageUtility.GetImageHttpPathSmallThumbnail(entity.ImageFile),
				ImageThumbnailPath = imageUtility.GetImageHttpPathThumbnail(entity.ImageFile),
				IsActive = entity.ActiveInd,
				Name = entity.Name,
				ProductId = entity.ProductID,
				ProductImageId = entity.ProductImageID,
				ProductImageTypeId = entity.ProductImageTypeID,
				ReviewStateId = entity.ReviewStateID,
				ShowOnCategoryPage = entity.ShowOnCategoryPage
			};

			return model;
		}

		public static ProductImage ToEntity(ImageModel model)
		{
			if (model == null)
			{
				return null;
			}

			var entity = new ProductImage
			{
				ActiveInd = model.IsActive,
				AlternateThumbnailImageFile = model.AlternateThumbnailImageFile,
				DisplayOrder = model.DisplayOrder,
				ImageAltTag = model.ImageAltTag,
				ImageFile = model.ImageFile,
				Name = model.Name,
				ProductID = model.ProductId,
				ProductImageID = model.ProductImageId,
				ProductImageTypeID = model.ProductImageTypeId,
				ReviewStateID = model.ReviewStateId,
				ShowOnCategoryPage = model.ShowOnCategoryPage
			};

			return entity;
		}
	}
}
