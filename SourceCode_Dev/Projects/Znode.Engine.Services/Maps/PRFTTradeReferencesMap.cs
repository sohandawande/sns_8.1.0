﻿using ZNode.Libraries.DataAccess.Entities;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services.Maps
{
    public static class PRFTTradeReferencesMap
    {
        public static PRFTTradeReferencesModel ToModel(PRFTTradeReferences entity)
        {
            if (entity == null)
            {
                return null;
            }

            var model = new PRFTTradeReferencesModel
            {
                TradeRefID = entity.TradeRefID,
                CreditApplicationID = entity.CreditApplicationID,
                ReferenceName = entity.ReferenceName,
                BusinessName = entity.BusinessName,
                Street = entity.Street,
                Street1 = entity.Street1,
                City = entity.City,
                StateCode = entity.StateCode,
                CountryCode = entity.CountryCode,
                PostalCode = entity.PostalCode,
                PhoneNumber = entity.PhoneNumber,
                FaxNumber = entity.FaxNumber,
                Custom1 = entity.Custom1,
                Custom2 = entity.Custom2,
                Custom3 = entity.Custom3,
                Custom4 = entity.Custom4,
                Custom5 = entity.Custom5
            };

            return model;
        }

        public static PRFTTradeReferences ToEntity(PRFTTradeReferencesModel model)
        {
            if (model == null)
            {
                return null;
            }

            var entity = new PRFTTradeReferences
            {
                TradeRefID = model.TradeRefID,
                CreditApplicationID = model.CreditApplicationID,
                ReferenceName = model.ReferenceName,
                BusinessName = model.BusinessName,
                Street = model.Street,
                Street1 = model.Street1,
                City = model.City,
                StateCode = model.StateCode,
                CountryCode = model.CountryCode,
                PostalCode = model.PostalCode,
                PhoneNumber = model.PhoneNumber,
                FaxNumber = model.FaxNumber,
                Custom1 = model.Custom1,
                Custom2 = model.Custom2,
                Custom3 = model.Custom3,
                Custom4 = model.Custom4,
                Custom5 = model.Custom5
            };

            return entity;
        }
    }
}
