﻿using System;
using System.Collections.Generic;
using System.Data;
using DataManagerAdminRepository = ZNode.Libraries.Admin.DataManagerAdmin;

namespace Znode.Engine.Services
{
    public class AffiliateTrackingService : BaseService, IAffiliateTrackingService
    {
        #region Private Variables
        private readonly DataManagerAdminRepository _dataManagerAdminRepository;
        #endregion

        #region Constructor
        public AffiliateTrackingService()
        {
            _dataManagerAdminRepository = new DataManagerAdminRepository();
        }
        #endregion

        #region Public Methods

        public DataSet GetAffiliateTrackingData(List<Tuple<string, string, string>> filters)
        {            
            DataSet trackingData = _dataManagerAdminRepository.GetDownloadTrackingData(filters[0].Item3.ToString(), filters[1].Item3.ToString());

            return !Equals(trackingData, null) ? trackingData : null;
        }

        #endregion
    }
}
