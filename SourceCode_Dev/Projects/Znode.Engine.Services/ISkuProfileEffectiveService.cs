﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    public interface ISkuProfileEffectiveService
    {
        /// <summary>
        /// Get existing Sku Profile Effective.
        /// </summary>
        /// <param name="skuProfileEffective">Id of the sku Profile Effective.</param>
        /// <param name="expands">NameValueCollection expands</param>
        /// <returns>SkuProfileEffectiveModel</returns>
        SkuProfileEffectiveModel GetSkuProfileEffective(int skuProfileEffective, NameValueCollection expands);

        /// <summary>
        /// Get the Sku Profile Effective list.
        /// </summary>
        /// <param name="expands">NameValueCollection expands</param>
        /// <param name="filters">List<Tuple<string, string, string>> filters</param>
        /// <param name="sorts">NameValueCollection sorts</param>
        /// <param name="page">NameValueCollection page</param>
        /// <returns>SkuProfileEffectiveListModel</returns>
        SkuProfileEffectiveListModel GetSkuProfileEffectives(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Save the Sku Profile Effective in databse.
        /// </summary>
        /// <param name="model">model to save.</param>
        /// <returns>Returns SkuProfileEffectiveModel</returns>
        SkuProfileEffectiveModel CreateSkuProfileEffective(SkuProfileEffectiveModel model);

        /// <summary>
        /// Update the  Sku Profile Effective.
        /// </summary>
        /// <param name="skuProfileEffectiveId">Id of the sku Profile Effective.</param>
        /// <param name="model">model to update.</param>
        /// <returns>Returns SkuProfileEffectiveModel</returns>
        SkuProfileEffectiveModel UpdateSkuProfileEffective(int skuProfileEffectiveId, SkuProfileEffectiveModel model);

        /// <summary>
        /// Deletes the existing Sku Profile Effective.
        /// </summary>
        /// <param name="skuProfileEffectiveId">Id of the sku Profile Effective.</param>
        /// <returns>Returns true or false.</returns>
        bool DeleteSkuProfileEffective(int skuProfileEffectiveId);

        /// <summary>
        /// Get Sku Profile EffectiveBy Sku Id.
        /// </summary>
        /// <param name="skuId">Id of the Sku.</param>
        /// <returns>SkuProfileEffectiveListModel</returns>
        SkuProfileEffectiveListModel GetSkuProfileEffectiveBySkuId(int skuId);

        /// <summary>
        /// Get Sku Profile Effective By Sku ID.
        /// </summary>
        /// <param name="filters">List<Tuple<string, string, string>> filters</param>
        /// <param name="sorts">NameValueCollection sorts</param>
        /// <param name="page">NameValueCollection page</param>
        /// <returns>SkuProfileEffectiveListModel</returns>
        SkuProfileEffectiveListModel GetSkuProfileEffectivesBySkuId(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
    }
}
