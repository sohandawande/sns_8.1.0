﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    public interface IProductSearchSettingService
    {
        /// <summary>
        /// Get all Product for Product level settings
        /// </summary>
        /// <param name="expands">NameValueCollection expands</param>
        /// <param name="filters">List Tuple filters</param>
        /// <param name="sorts">NameValueCollection sorts</param>
        /// <param name="page">NameValueCollection page</param>
        /// <returns>Returns Products list</returns>
        ProductLevelSettingListModel GetProductLevelSettings(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Get all Product and Categories for Category level settings
        /// </summary>
        /// <param name="expands">NameValueCollection expands</param>
        /// <param name="filters">List Tuple filters</param>
        /// <param name="sorts">NameValueCollection sorts</param>
        /// <param name="page">NameValueCollection page</param>
        /// <returns>Returns Products and associated Categories list</returns>
        CategoryLevelSettingListModel GetCategoryLevelSettings(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Get all field level settings
        /// </summary>
        /// <param name="expands">NameValueCollection expands</param>
        /// <param name="filters">List Tuple filters</param>
        /// <param name="sorts">NameValueCollection sorts</param>
        /// <param name="page">NameValueCollection page</param>
        /// <returns>Returns fields and boost values</returns>
        FieldLevelSettingListModel GetFieldLevelSettings(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        ///Save product search boost values for all sections like Category, Product and Field.
        /// </summary>
        /// <param name="model">BoostSearchSetting</param>
        /// <returns>true/false</returns>
        bool SaveBoostValues(BoostSearchSettingModel model);
    }
}
