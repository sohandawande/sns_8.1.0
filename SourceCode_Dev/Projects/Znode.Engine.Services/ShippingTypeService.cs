﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using Znode.Engine.Shipping;
using Znode.Libraries.Helpers.Constants;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using ShippingTypeRepository = ZNode.Libraries.DataAccess.Service.ShippingTypeService;

namespace Znode.Engine.Services
{
    /// <summary>
    /// Shipping Type Service
    /// </summary>
    public class ShippingTypeService : BaseService, IShippingTypeService
    {
        #region Private Variables

        private readonly ShippingTypeRepository _shippingTypeRepository;

        #endregion

        #region Constructor

        public ShippingTypeService()
        {
            _shippingTypeRepository = new ShippingTypeRepository();
        }

        #endregion

        #region Public Methods
        public ShippingTypeModel GetShippingType(int shippingTypeId)
        {
            var shippingType = _shippingTypeRepository.GetByShippingTypeID(shippingTypeId);
            return ShippingTypeMap.ToModel(shippingType);
        }

        public ShippingTypeListModel GetShippingTypes(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new ShippingTypeListModel();
            var shippingTypes = new TList<ShippingType>();

            var query = new ShippingTypeQuery();
            var sortBuilder = new ShippingTypeSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            shippingTypes = _shippingTypeRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (Equals(pagingLength, int.MaxValue))
            {
                model.PageSize = model.TotalResults;
            }

            // Map each item and add to the list
            foreach (var s in shippingTypes)
            {
                model.ShippingTypes.Add(ShippingTypeMap.ToModel(s));
            }

            return model;
        }

        public ShippingTypeModel CreateShippingType(ShippingTypeModel model)
        {
            if (Equals(model, null))
            {
                throw new Exception("Shipping type model cannot be null.");
            }

            var entity = ShippingTypeMap.ToEntity(model);
            var shippingType = _shippingTypeRepository.Save(entity);
            return ShippingTypeMap.ToModel(shippingType);
        }

        public ShippingTypeModel UpdateShippingType(int shippingTypeId, ShippingTypeModel model)
        {
            if (shippingTypeId < 1)
            {
                throw new Exception("Shipping type ID cannot be less than 1.");
            }

            if (Equals(model, null))
            {
                throw new Exception("Shipping type model cannot be null.");
            }

            var shippingType = _shippingTypeRepository.GetByShippingTypeID(shippingTypeId);
            if (!Equals(shippingType, null))
            {
                // Set the shipping type ID and map
                model.ShippingTypeId = shippingTypeId;
                var shippingTypeToUpdate = ShippingTypeMap.ToEntity(model);

                var updated = _shippingTypeRepository.Update(shippingTypeToUpdate);
                if (updated)
                {
                    shippingType = _shippingTypeRepository.GetByShippingTypeID(shippingTypeId);
                    return ShippingTypeMap.ToModel(shippingType);
                }
            }

            return null;
        }

        public bool DeleteShippingType(int shippingTypeId)
        {
            if (shippingTypeId < 1)
            {
                throw new Exception("Shipping type ID cannot be less than 1.");
            }

            var shippingType = _shippingTypeRepository.GetByShippingTypeID(shippingTypeId);

            if (this.CheckAssociatedShipping(shippingTypeId))
            {
                throw new Exception("Not able to delete shipping type " + shippingType.ClassName + ". Ensure all associated shippers are deleted first.");
            }

            if (!Equals(shippingType, null))
            {
                return _shippingTypeRepository.Delete(shippingType);
            }

            return false;
        }


        #endregion

        #region Znode Version 8.0
        /// <summary>
        /// Check Shipping Type associated with Shipping 
        /// </summary>
        /// <param name="ShippingTypeId">int ShippingTypeId</param>
        /// <returns>Return true is associate with else return fale</returns>
        private bool CheckAssociatedShipping(int ShippingTypeId)
        {
            ShippingHelper shippingHelper = new ShippingHelper();
            if (ShippingTypeId > 0)
            {
                return shippingHelper.CheckAssociatedShipping(ShippingTypeId);
            }
            else
            {
                return false;
            }
        }

        public ShippingTypeListModel GetAllShippingTypesNotInDatabase()
        {
            ShippingTypeListModel model = new ShippingTypeListModel();
            ShippingTypeListModel list = new ShippingTypeListModel();

            var shippingTypes = ZnodeShippingManager.GetAvailableShippingTypes();

            foreach (var p in shippingTypes)
            {
                model.ShippingTypes.Add(ShippingTypeMap.ToModel(p));
            }
            ShippingTypeListModel availableShippingTypes = this.GetShippingTypes(new List<Tuple<string, string, string>>(), new NameValueCollection(), new NameValueCollection());
            if (availableShippingTypes.ShippingTypes.Count < model.ShippingTypes.Count)
            {
                foreach (var x in model.ShippingTypes)
                {
                    var found = false;
                    foreach (var y in availableShippingTypes.ShippingTypes)
                    {
                        if (!found)
                        {
                            if (Equals(x.ClassName, y.ClassName))
                            {
                                found = true;
                            }
                        }
                    }

                    if (!found)
                    {
                        list.ShippingTypes.Add(x);
                    }
                }
            }
            return list;
        }
        #endregion

        #region Private Methodss
        private void SetFiltering(List<Tuple<string, string, string>> filters, ShippingTypeQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;

                if (String.Equals(filterKey, FilterKeys.ClassName)) SetQueryParameter(ShippingTypeColumn.ClassName, filterOperator, filterValue, query);
                if (String.Equals(filterKey, FilterKeys.IsActive)) SetQueryParameter(ShippingTypeColumn.IsActive, filterOperator, filterValue, query);
                if (String.Equals(filterKey, FilterKeys.Name)) SetQueryParameter(ShippingTypeColumn.Name, filterOperator, filterValue, query);
                if (String.Equals(filterKey, FilterKeys.ShippingTypeId)) SetQueryParameter(ShippingTypeColumn.ShippingTypeID, filterOperator, filterValue, query);
            }
        }

        private void SetSorting(NameValueCollection sorts, ShippingTypeSortBuilder sortBuilder)
        {
            if (sorts.HasKeys())
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);

                    if (String.Equals(value, SortKeys.ClassName)) SetSortDirection(ShippingTypeColumn.ClassName, sorts[StoredProcedureKeys.sortDirKey], sortBuilder);
                    if (String.Equals(value, SortKeys.Name)) SetSortDirection(ShippingTypeColumn.Name, sorts[StoredProcedureKeys.sortDirKey], sortBuilder);
                    if (String.Equals(value, SortKeys.ShippingTypeId)) SetSortDirection(ShippingTypeColumn.ShippingTypeID, sorts[StoredProcedureKeys.sortDirKey], sortBuilder);
                }
            }
        }

        private void SetQueryParameter(ShippingTypeColumn column, string filterOperator, string filterValue, ShippingTypeQuery query)
        {
            base.SetQueryParameter(column, filterOperator, filterValue, query);
        }

        private void SetSortDirection(ShippingTypeColumn column, string value, ShippingTypeSortBuilder sortBuilder)
        {
            base.SetSortDirection(column, value, sortBuilder);
        }

        #endregion
    }
}
