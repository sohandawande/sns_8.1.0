﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Helpers.Constants;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using ContentPageRevisionRepository = ZNode.Libraries.DataAccess.Service.ContentPageRevisionService;

namespace Znode.Engine.Services
{
    public class ContentPageRevisionService : BaseService, IContentPageRevisionService
    {
        #region Private Variables
        private readonly ContentPageRevisionRepository _contentPageRevisionRepository;
        private readonly ContentPageAdmin contentPageAdmin;
        #endregion

        #region Default Constructor
        public ContentPageRevisionService()
        {
            _contentPageRevisionRepository = new ContentPageRevisionRepository();
            contentPageAdmin = new ContentPageAdmin();
        }
        #endregion

        #region Public Methods

        public ContentPageRevisionListModel GetContentPageRevisions(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new ContentPageRevisionListModel();
            var contentPageRevisions = new TList<ContentPageRevision>();

            var query = new ContentPageRevisionQuery();
            var sortBuilder = new ContentPageRevisionSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            contentPageRevisions = _contentPageRevisionRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (pagingLength.Equals(int.MaxValue))
            {
                model.PageSize = model.TotalResults;
            }

            // Map each item and add to the list
            foreach (var c in contentPageRevisions)
            {
                model.ContentPageRevisions.Add(ContentPageRevisionMap.ToModel(c));
            }

            return model;
        }

        public bool RevertRevision(int pageRevisionId, ContentPageRevisionModel model)
        {
            return contentPageAdmin.RevertToRevision(pageRevisionId, model.UpdatedUser, model.OldHtml);
        }

        public ContentPageRevisionListModel GetContentPageRevisionsById(int contentPageId)
        {
            if (contentPageId < 1)
            {
                throw new Exception("Content Page Revision ID cannot be less than 1.");
            }

            return ContentPageRevisionMap.ToListModel(contentPageAdmin.GetPageRevisions(contentPageId));
        }

        public ContentPageRevisionListModel GetContentPageRevisionsById(int contentPageId, NameValueCollection sorts, NameValueCollection page, out int totalRowCount)
        {
            var contentpagerevisionlistmodel = GetContentPageRevisionsById(contentPageId);
            var sortBuilder = new ContentPageRevisionSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetPaging(page, contentpagerevisionlistmodel, out pagingStart, out pagingLength);
            SetSorting(sorts, sortBuilder);
            pagingStart = Convert.ToInt32(page.Get(PageKeys.Index));

            totalRowCount = contentpagerevisionlistmodel.ContentPageRevisions.Count;

            contentpagerevisionlistmodel.TotalResults = totalRowCount;
            contentpagerevisionlistmodel.PageSize = pagingLength;
            contentpagerevisionlistmodel.PageIndex = pagingStart;
            // Check to set page size equal to the total number of results
            if (pagingLength.Equals(int.MaxValue))
            {
                contentpagerevisionlistmodel.PageSize = contentpagerevisionlistmodel.TotalResults;
            }

            return contentpagerevisionlistmodel;
        }


        #endregion

        #region private Methods
        private void SetFiltering(List<Tuple<string, string, string>> filters, ContentPageRevisionQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;

                if (filterKey.Equals(FilterKeys.RevisionId)) { SetQueryParameter(ContentPageRevisionColumn.RevisionID, filterOperator, filterValue, query); }
                if (filterKey.Equals(FilterKeys.ContentPageID)) { SetQueryParameter(ContentPageRevisionColumn.ContentPageID, filterOperator, filterValue, query); }
                if (filterKey.Equals(FilterKeys.Description)) { SetQueryParameter(ContentPageRevisionColumn.Description, filterOperator, filterValue, query); }
                if (filterKey.Equals(FilterKeys.UpdateDate)) { SetQueryParameter(ContentPageRevisionColumn.UpdateDate, filterOperator, filterValue, query); }
                if (filterKey.Equals(FilterKeys.UpdatedUser)) { SetQueryParameter(ContentPageRevisionColumn.UpdateUser, filterOperator, filterValue, query); }
            }
        }

        private void SetSorting(NameValueCollection sorts, ContentPageRevisionSortBuilder sortBuilder)
        {
            if (sorts.HasKeys() && sorts.AllKeys.Length > 1)
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);

                    if (value.Equals(SortKeys.ContentPageID)) { SetSortDirection(ContentPageRevisionColumn.ContentPageID, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (value.Equals(SortKeys.RevisionId)) { SetSortDirection(ContentPageRevisionColumn.RevisionID, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (value.Equals(SortKeys.UpdatedUser)) { SetSortDirection(ContentPageRevisionColumn.UpdateUser, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (value.Equals(SortKeys.Description)) { SetSortDirection(ContentPageRevisionColumn.Description, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (value.Equals(SortKeys.UpdateDate)) { SetSortDirection(ContentPageRevisionColumn.UpdateDate, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                }
            }
            else
            {
                SetSortDirection(ContentPageRevisionColumn.RevisionID, "DESC", sortBuilder);
            }
        }

        private void SetQueryParameter(ContentPageRevisionColumn column, string filterOperator, string filterValue, ContentPageRevisionQuery query)
        {
            base.SetQueryParameter(column, filterOperator, filterValue, query);
        }

        private void SetSortDirection(ContentPageRevisionColumn column, string value, ContentPageRevisionSortBuilder sortBuilder)
        {
            base.SetSortDirection(column, value, sortBuilder);
        }
        #endregion
    }
}
