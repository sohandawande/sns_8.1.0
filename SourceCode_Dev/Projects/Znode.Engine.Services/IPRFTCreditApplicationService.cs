﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    public interface IPRFTCreditApplicationService
    {
        PRFTCreditApplicationModel CreateCreditApplication(PRFTCreditApplicationModel model);
        PRFTCreditApplicationModel GetCreditApplication(int creditApplicationId, NameValueCollection expands);
        PRFTCreditApplicationListModel GetCreditApplications(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
        PRFTCreditApplicationModel UpdateCreditRequest(int creditApplicationId, PRFTCreditApplicationModel model);
    }
}
