﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
	public interface IPortalService
	{
        /// <summary>
        /// Get portal
        /// </summary>
        /// <param name="portalId">portal id</param>
        /// <param name="expands">expands</param>
        /// <returns>PortalModel</returns>
		PortalModel GetPortal(int portalId, NameValueCollection expands);

        /// <summary>
        /// Get portal list
        /// </summary>
        /// <param name="expands">expands</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <param name="page">page</param>
        /// <returns>Portal List Model </returns>
		PortalListModel GetPortals(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Get portals by portalIds
        /// </summary>
        /// <param name="portalIds">portal ids</param>
        /// <param name="expands">expands</param>
        /// <param name="sorts">sorts</param>
        /// <returns>Portal List Model</returns>
		PortalListModel GetPortalsByPortalIds(string portalIds, NameValueCollection expands, NameValueCollection sorts);

        /// <summary>
        /// Create Portal
        /// </summary>
        /// <param name="model">model to create</param>
        /// <returns>created model</returns>
		PortalModel CreatePortal(PortalModel model);

        /// <summary>
        /// Update Portal
        /// </summary>
        /// <param name="portalId">portal id</param>
        /// <param name="model">updated model.</param>
        /// <returns>Portal Model</returns>
		PortalModel UpdatePortal(int portalId, PortalModel model);

      bool DeletePortal(int portalId);
        /// <summary>
        /// Gets the Fedex keys.
        /// </summary>
        /// <returns>dataset of fedex key.</returns>
        DataSet GetFedexKey();
        /// <summary>
        /// Creates Messages for the new store.
        /// </summary>
        /// <param name="portalId">Portal Id of the new store.</param>
        /// <param name="localeId">Locale id of the new store.</param>
        /// <returns>Bool value if the messages are created or not.</returns>
        bool CreateMessages(int portalId, int localeId);

        /// <summary>
        /// Copies the store.
        /// </summary>
        /// <param name="portalId">Portal id of the store to be copied.</param>
        /// <returns>Bool value if the store is copied or not.</returns>
        bool CopyStore(int portalId);

        /// <summary>
        /// Deletes a portal by Portal Id.
        /// </summary>
        /// <param name="portalId">Id of the portal to be deleted.</param>
        /// <returns>Bool value whether the portal is deleted or not.</returns>
        bool DeletePortalByPortalId(int portalId);

        /// <summary>
        /// Gets Portal Information according to portal id.
        /// </summary>
        /// <param name="portalId">Portal id for which information is to be retrieved.</param>
        /// <returns>Portal Model.</returns>
        PortalModel GetPortalInformationByPortalId(int portalId);

        /// <summary>
        /// Gets list of portal after checking the profile access permissio of the user.
        /// </summary>
        /// <param name="expands">Expand collection of portal list.</param>
        /// <param name="filters">Filter list of portal list.</param>
        /// <param name="sorts">Sort collection for portal list.</param>
        /// <param name="page">Page collection of portal list.</param>
        /// <returns>List of portals.</returns>
        PortalListModel GetPortalsByProfileAccess(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
	}
}
