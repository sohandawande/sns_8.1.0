﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using CatalogRepository = ZNode.Libraries.DataAccess.Service.CatalogService;
using PortalRepository = ZNode.Libraries.DataAccess.Service.PortalService;
using PortalCatalogRepository = ZNode.Libraries.DataAccess.Service.PortalCatalogService;
using CSSRepository = ZNode.Libraries.DataAccess.Service.CSSService;
using ThemeRepository = ZNode.Libraries.DataAccess.Service.ThemeService;

namespace Znode.Engine.Services
{
    public class PortalCatalogService : BaseService, IPortalCatalogService
    {
        private readonly CatalogRepository _catalogRepository;
        private readonly PortalRepository _portalRepository;
        private readonly PortalCatalogRepository _portalCatalogRepository;

        public PortalCatalogService()
        {
            _catalogRepository = new CatalogRepository();
            _portalRepository = new PortalRepository();
            _portalCatalogRepository = new PortalCatalogRepository();
        }

        public PortalCatalogModel GetPortalCatalog(int portalCatalogId, NameValueCollection expands)
        {
            var portalCatalog = _portalCatalogRepository.GetByPortalCatalogID(portalCatalogId);
            if (portalCatalog != null)
            {
                GetExpands(expands, portalCatalog);
            }

            return PortalCatalogMap.ToModel(portalCatalog);
        }

        public PortalCatalogListModel GetPortalCatalogs(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new PortalCatalogListModel();
            var portalCatalogs = new TList<PortalCatalog>();
            var tempList = new TList<PortalCatalog>();

            var query = new PortalCatalogQuery();
            var sortBuilder = new PortalCatalogSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            tempList = _portalCatalogRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (pagingLength.Equals(int.MaxValue))
            {
                model.PageSize = model.TotalResults;
            }

            // Now go through the list and get any expands
            foreach (var portalCatalog in tempList)
            {
                GetExpands(expands, portalCatalog);
                portalCatalogs.Add(portalCatalog);
            }

            // Map each item and add to the list
            CSSRepository css = new CSSRepository();
            ThemeRepository theme = new ThemeRepository();
            PortalCatalogModel portalCatalogModel = new PortalCatalogModel();
            foreach (var pc in portalCatalogs)
            {
                int cssId = pc.CSSID ?? 0;
                int themeId = pc.ThemeID ?? 0;
                portalCatalogModel = PortalCatalogMap.ToModel(pc);
                if (!Equals(portalCatalogModel.Catalog, null))
                {
                    portalCatalogModel.CatalogName = portalCatalogModel.Catalog.Name;
                }
                portalCatalogModel.CSSName = (cssId > 0) ? css.GetByCSSID(cssId).Name : string.Empty;
                portalCatalogModel.ThemeName = (themeId > 0) ? theme.GetByThemeID(themeId).Name : string.Empty;
                model.PortalCatalogs.Add(portalCatalogModel);
            }

            return model;
        }

        public PortalCatalogModel CreatePortalCatalog(PortalCatalogModel model)
        {
            if (model == null)
            {
                throw new Exception("Portal catalog model cannot be null.");
            }

            var entity = PortalCatalogMap.ToEntity(model);
            var portalCatalog = _portalCatalogRepository.Save(entity);
            return PortalCatalogMap.ToModel(portalCatalog);
        }

        public PortalCatalogModel UpdatePortalCatalog(int portalCatalogId, PortalCatalogModel model)
        {
            if (portalCatalogId < 1)
            {
                throw new Exception("Portal catalog ID cannot be less than 1.");
            }

            if (model == null)
            {
                throw new Exception("Portal catalog model cannot be null.");
            }

            var portalCatalog = _portalCatalogRepository.GetByPortalCatalogID(portalCatalogId);
            if (portalCatalog != null)
            {
                // Set portal catalog ID
                model.PortalCatalogId = portalCatalogId;

                var portalCatalogToUpdate = PortalCatalogMap.ToEntity(model);

                var updated = _portalCatalogRepository.Update(portalCatalogToUpdate);
                if (updated)
                {
                    portalCatalog = _portalCatalogRepository.GetByPortalCatalogID(portalCatalogId);
                    return PortalCatalogMap.ToModel(portalCatalog);
                }
            }

            return null;
        }

        public PortalCatalogListModel GetPortalCatalogByPortalId(int portalId)
        {
            PortalCatalogListModel portalCatalogListModel = new PortalCatalogListModel();
            var portalCatalogs = _portalCatalogRepository.GetByPortalID(portalId);
            if (!Equals(portalCatalogs, null) && portalCatalogs.Count > 0)
            {
                foreach (var portalCatalog in portalCatalogs)
                {
                    portalCatalogListModel.PortalCatalogs.Add(PortalCatalogMap.ToModel(portalCatalog));
                }
            }

            return portalCatalogListModel;
        }

        public bool DeletePortalCatalog(int portalCatalogId)
        {
            if (portalCatalogId < 1)
            {
                throw new Exception("Portal catalog ID cannot be less than 1.");
            }

            var portalCatalog = _portalCatalogRepository.GetByPortalCatalogID(portalCatalogId);
            if (portalCatalog != null)
            {
                return _portalCatalogRepository.Delete(portalCatalog);
            }

            return false;
        }

        private void GetExpands(NameValueCollection expands, PortalCatalog portalCatalog)
        {
            if (expands != null && expands.HasKeys())
            {
                ExpandCatalog(expands, portalCatalog);
                ExpandPortal(expands, portalCatalog);
            }
        }

        private void ExpandCatalog(NameValueCollection expands, PortalCatalog portalCatalog)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Catalog)))
            {
                var catalog = _catalogRepository.GetByCatalogID(portalCatalog.CatalogID);
                if (catalog != null)
                {
                    portalCatalog.CatalogIDSource = catalog;
                }
            }
        }

        private void ExpandPortal(NameValueCollection expands, PortalCatalog portalCatalog)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Portal)))
            {
                var portal = _portalRepository.GetByPortalID(portalCatalog.PortalID);
                if (portal != null)
                {
                    portalCatalog.PortalIDSource = portal;
                }
            }
        }

        private void SetFiltering(List<Tuple<string, string, string>> filters, PortalCatalogQuery query)
        {
            bool isPortalIdPresent = false;
            string UserName = string.Empty;

            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;
                if (!Equals(filterKey, FilterKeys.UserName))
                {
                    if (filterKey == FilterKeys.CatalogId) SetQueryParameter(PortalCatalogColumn.CatalogID, filterOperator, filterValue, query);
                    if (filterKey == FilterKeys.LocaleId) SetQueryParameter(PortalCatalogColumn.LocaleID, filterOperator, filterValue, query);
                    if (filterKey == FilterKeys.PortalCatalogId) SetQueryParameter(PortalCatalogColumn.PortalCatalogID, filterOperator, filterValue, query);
                    if (filterKey == FilterKeys.PortalId)
                    {
                        isPortalIdPresent = true;
                        SetQueryParameter(PortalCatalogColumn.PortalID, filterOperator, filterValue, query);
                    }
                }
                else
                {
                    UserName = filterValue;
                }
            }
            if (!isPortalIdPresent)
            {
                string portalIds = GetAvailablePortals(UserName);
                if (!string.IsNullOrEmpty(portalIds) && !Equals(portalIds, "0"))
                {
                    query.AppendIn(SqlUtil.AND, PortalCatalogColumn.PortalID, portalIds.Split(','));
                }
            }
        }

        private void SetSorting(NameValueCollection sorts, PortalCatalogSortBuilder sortBuilder)
        {
            if (sorts.HasKeys())
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);

                    if (key == SortKeys.CatalogId) SetSortDirection(PortalCatalogColumn.CatalogID, value, sortBuilder);
                    if (key == SortKeys.PortalCatalogId) SetSortDirection(PortalCatalogColumn.PortalCatalogID, value, sortBuilder);
                    if (key == SortKeys.PortalId) SetSortDirection(PortalCatalogColumn.PortalID, value, sortBuilder);
                }
            }
        }

        private void SetQueryParameter(PortalCatalogColumn column, string filterOperator, string filterValue, PortalCatalogQuery query)
        {
            base.SetQueryParameter(column, filterOperator, filterValue, query);
        }

        private void SetSortDirection(PortalCatalogColumn column, string value, PortalCatalogSortBuilder sortBuilder)
        {
            base.SetSortDirection(column, value, sortBuilder);
        }
    }
}
