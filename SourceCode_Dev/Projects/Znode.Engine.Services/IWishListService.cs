﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    public interface IWishListService
    {
        WishListModel Create(WishListModel model);
        WishListModel GetWishList(int id);
        WishListListModel GetWishLists(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
        WishListModel UpdateWishlist(int wishlistId, WishListModel model);
        bool DeleteWishlist(int wishlistId);
    }
}