﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    public interface ICountryService
    {
        /// <summary>
        /// Get Country on the basis of country code
        /// </summary>
        /// <param name="countryCode">Country Code</param>
        /// <param name="expands">expands</param>
        /// <returns>Returns country name on the basis of country code</returns>
        CountryModel GetCountry(string countryCode, NameValueCollection expands);

        /// <summary>
        /// Gets the list of Countries
        /// </summary>
        /// <param name="expands">expands</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <param name="page">page</param>
        /// <returns>Returns the list of countries</returns>
        CountryListModel GetCountries(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Create Country
        /// </summary>
        /// <param name="model">Country Model</param>
        /// <returns>Returns created country</returns>
        CountryModel CreateCountry(CountryModel model);

        /// <summary>
        /// Updates country on the basis of country code
        /// </summary>
        /// <param name="countryCode">Country Code</param>
        /// <param name="model">Country Model</param>
        /// <returns>Returns updated country</returns>
        CountryModel UpdateCountry(string countryCode, CountryModel model);

        /// <summary>
        /// Delete Country on the basis of country code
        /// </summary>
        /// <param name="countryCode">Country Code</param>
        /// <returns>Returns true/false</returns>
        bool DeleteCountry(string countryCode);

        /// <summary>
        /// Znode Version 7.2.2
        /// This function Get all active shipping and billing countries for portalId.
        /// </summary>
        /// <param name="model">int portalId</param>
        /// <param name="billingShippingFlag">int billingShippingFlag</param>
        /// <returns>Returns CountryListModel</returns>
        CountryListModel GetActiveCountryByPortalId(int portalId, int billingShippingFlag = 0);

        /// <summary>
        /// This function Get all countries for portalId.
        /// </summary>
        /// <param name="portalId">int portalId</param>
        /// <returns>Returns CountryListModel</returns>
        CountryListModel GetCountryByPortalId(int portalId);

        /// <summary>
        /// This function Get Shipping Active countries for portalId.
        /// </summary>
        /// <param name="portalId">int portalId to get countries related to perticular portal</param>
        /// <returns>Returns CountryListModel</returns>
        CountryListModel GetShippingActiveCountryByPortalId(int portalId);
    }
}
