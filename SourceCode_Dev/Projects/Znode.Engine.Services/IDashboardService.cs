﻿using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    public interface IDashboardService 
    {
        /// <summary>
        /// Gets the Dashboard Items by Portal Id.
        /// </summary>
        /// <param name="portalId">Id of the portal.</param>
        /// <returns>DashboardModel</returns>
        DashboardModel GetDashboardItemsByPortalId(int portalId);
    }
}
