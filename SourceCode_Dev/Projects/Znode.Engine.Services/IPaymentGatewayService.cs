﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
	public interface IPaymentGatewayService
	{
		PaymentGatewayModel GetPaymentGateway(int paymentGatewayId);
		PaymentGatewayListModel GetPaymentGateways(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
		PaymentGatewayModel CreatePaymentGateway(PaymentGatewayModel model);
		PaymentGatewayModel UpdatePaymentGateway(int paymentGatewayId, PaymentGatewayModel model);
		bool DeletePaymentGateway(int paymentGatewayId);
	}
}
