﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using OrderStateRepository = ZNode.Libraries.DataAccess.Service.OrderStateService;


namespace Znode.Engine.Services
{
    public class OrderStateService : BaseService, IOrderStateService
    {
        #region Private Variables
        private readonly OrderStateRepository _orderStateRepository;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor for OrderStateService
        /// </summary>
        public OrderStateService()
        {
            _orderStateRepository = new OrderStateRepository();
        }
        #endregion

        #region Public Methods
        // <summary>
        /// Gets the list of order status according to filters and sorts.
        /// </summary>
        /// <param name="filters">Filters to be applied.</param>
        /// <param name="sorts">Sorting of the list.</param>
        /// <returns>List of Order States</returns>
        public OrderStateListModel GetOrderStates(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new OrderStateListModel();
            var orderStates = new TList<OrderState>();
            var tempList = new TList<OrderState>();


            var query = new OrderStateQuery();
            var sortBuilder = new OrderStateSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            tempList = _orderStateRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                model.PageSize = model.TotalResults;
            }

            // Now go through the list and get any expands
            foreach (var orderState in tempList)
            {
                GetExpands(expands, orderState);
                orderStates.Add(orderState);
            }

            // Map each item and add to the list
            foreach (var a in orderStates)
            {
                model.OrderStates.Add(OrderStateMap.ToModel(a));
            }

            return model;

        }
        #endregion

        private void SetFiltering(List<Tuple<string, string, string>> filters, OrderStateQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;

                if (filterKey == FilterKeys.OrderStateName) SetQueryParameter(OrderStateColumn.OrderStateName, filterOperator, filterValue, query);
            }
        }

        private void SetSorting(NameValueCollection sorts, OrderStateSortBuilder sortBuilder)
        {
            if (sorts.HasKeys())
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);

                    if (key == SortKeys.OrderStateId) SetSortDirection(OrderStateColumn.OrderStateID, value, sortBuilder);
                    if (key == SortKeys.OrderStateName) SetSortDirection(OrderStateColumn.OrderStateName, value, sortBuilder);
                }
            }
        }

        private void GetExpands(NameValueCollection expands, OrderState orderState)
        {
            if (expands.HasKeys())
            {

            }
        }
    }
}
