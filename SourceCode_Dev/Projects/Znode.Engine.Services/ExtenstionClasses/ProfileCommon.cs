﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Profile;

namespace Znode.Engine.Services.ExtenstionClasses
{
    /// <summary>
    /// Extending the properties of the ProfileCommon base class
    /// </summary>
    public class ProfileCommon : Znode.Engine.Common.ProfileCommon
    {
        public static new ProfileCommon Create(string username, bool isAuthenticated)
        {
            ProfileCommon profileCommon = new ProfileCommon();
            profileCommon.profile = ProfileBase.Create(username, isAuthenticated);
            return profileCommon;
        }
    }
}
