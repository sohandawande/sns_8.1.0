﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    /// <summary>
    /// Interface for Product Type Service
    /// </summary>
    public interface IProductTypeService 
    {
        /// <summary>
        /// To Get Product Type by Product Type Id.
        /// </summary>
        /// <param name="productTypeId">int</param>
        /// <returns>Returns ProductTypeModel</returns>
        ProductTypeModel GetProductType(int productTypeId);

        /// <summary>
        /// To Get Product Types.
        /// </summary>
        /// <param name="filters">List</param>
        /// <param name="sorts">NameValueCollection</param>
        /// <param name="page">NameValueCollection</param>
        /// <returns></returns>
        ProductTypeListModel GetProductTypes(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// To Create Product Type.
        /// </summary>
        /// <param name="model">ProductTypeModel</param>
        /// <returns>Returns ProductTypeModel</returns>
        ProductTypeModel CreateProductType(ProductTypeModel model);

        /// <summary>
        /// To Update existing Product Type by product Type Id.
        /// </summary>
        /// <param name="productTypeId"></param>
        /// <param name="model">ProductTypeModel</param>
        /// <returns>Returns ProductTypeModel.</returns>
        ProductTypeModel UpdateProductType(int productTypeId, ProductTypeModel model);

        /// <summary>
        /// To Delete Existing Product Type.
        /// </summary>
        /// <param name="productTypeId">int product Type Id</param>
        /// <returns>Returns bool</returns>
        bool DeleteProductType(int productTypeId);

        /// <summary>
        ///  To get products and its value by productTypeId
        /// </summary>
        /// <param name="expands"></param>
        /// <param name="filters"></param>
        /// <param name="sorts"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        AttributeTypeValueListModel GetProductAttributesByProductTypeId(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
    }
}
