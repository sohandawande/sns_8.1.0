﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Helpers.Constants;
using Znode.Libraries.Helpers.Extensions;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using AttributeTypeRepository = ZNode.Libraries.DataAccess.Service.AttributeTypeService;
using ProductTypeAttributeRepository = ZNode.Libraries.DataAccess.Service.ProductTypeAttributeService;

namespace Znode.Engine.Services
{
	public class AttributeTypeService : BaseService, IAttributeTypeService
	{
		private readonly AttributeTypeRepository _attributeTypeRepository;
        private readonly ProductTypeAttributeRepository _productTypeAttributeRepository;

		public AttributeTypeService()
		{
			_attributeTypeRepository = new AttributeTypeRepository();
            _productTypeAttributeRepository = new ProductTypeAttributeRepository();
		}

		public AttributeTypeModel GetAttributeType(int attributeTypeId)
		{
			var attributeType = _attributeTypeRepository.GetByAttributeTypeId(attributeTypeId);
			return AttributeTypeMap.ToModel(attributeType);
		}

		public AttributeTypeListModel GetAttributeTypes(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
		{
			var model = new AttributeTypeListModel();
			var attributeTypes = new TList<AttributeType>();

			var query = new AttributeTypeQuery();
			var sortBuilder = new AttributeTypeSortBuilder();
			var pagingStart = 0;
			var pagingLength = 0;

			SetFiltering(filters, query);
			SetSorting(sorts, sortBuilder);
			SetPaging(page, model, out pagingStart, out pagingLength);

			// Get the initial set
			var totalResults = 0;
			attributeTypes = _attributeTypeRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
			model.TotalResults = totalResults;

			// Check to set page size equal to the total number of results
			if (pagingLength == int.MaxValue)
			{
				model.PageSize = model.TotalResults;
			}

			// Map each item and add to the list
			foreach (var a in attributeTypes)
			{
				model.AttributeTypes.Add(AttributeTypeMap.ToModel(a));
			}

			return model;
		}

		public AttributeTypeModel CreateAttributeType(AttributeTypeModel model)
		{
			if (model == null)
			{
				throw new Exception("Attribute type model cannot be null.");
			}

			var entity = AttributeTypeMap.ToEntity(model);
			var attributeType = _attributeTypeRepository.Save(entity);
			return AttributeTypeMap.ToModel(attributeType);
		}

		public AttributeTypeModel UpdateAttributeType(int attributeTypeId, AttributeTypeModel model)
		{
			if (attributeTypeId < 1)
			{
				throw new Exception("Attribute type ID cannot be less than 1.");
			}

			if (model == null)
			{
				throw new Exception("Attribute type model cannot be null.");
			}

			var attributeType = _attributeTypeRepository.GetByAttributeTypeId(attributeTypeId);
			if (attributeType != null)
			{
				// Set attributeType ID
				model.AttributeTypeId = attributeTypeId;

				var attributeTypeToUpdate = AttributeTypeMap.ToEntity(model);

				var updated = _attributeTypeRepository.Update(attributeTypeToUpdate);
				if (updated)
				{
					attributeType = _attributeTypeRepository.GetByAttributeTypeId(attributeTypeId);
					return AttributeTypeMap.ToModel(attributeType);
				}
			}

			return null;
		}

		public bool DeleteAttributeType(int attributeTypeId)
		{
            var query = new ProductTypeAttributeQuery();
			if (attributeTypeId < 1)
			{
				throw new Exception("Attribute type ID cannot be less than 1.");
			}

            query.AppendEquals(ProductTypeAttributeColumn.AttributeTypeId, attributeTypeId.ToString());

            TList<ProductTypeAttribute> list = _productTypeAttributeRepository.Find(query);
            if (list.Count > 0)
            {
                throw new Exception("Delete action could not be completed because the Product Attribute Type is in use.");
            }

			var attributeType = _attributeTypeRepository.GetByAttributeTypeId(attributeTypeId);
			if (attributeType != null)
			{
				return _attributeTypeRepository.Delete(attributeType);
			}

			return false;
		}

        public AttributeTypeListModel GetAttributeTypesByCatalogId(int catalogId)
        {
            AttributeTypeListModel model = new AttributeTypeListModel();
            AttributeTypeHelper helper = new AttributeTypeHelper();
            DataSet resultDataSet = helper.GetAttributesByCatalogID(catalogId);
            model.AttributeTypes = resultDataSet.Tables[0].ToList<AttributeTypeModel>().ToCollection();
            return model;
        }

		private void SetFiltering(List<Tuple<string, string, string>> filters, AttributeTypeQuery query)
		{
			foreach (var tuple in filters)
			{
				var filterKey = tuple.Item1;
				var filterOperator = tuple.Item2;
				var filterValue = tuple.Item3;

                if (filterKey == FilterKeys.DispalyOrder) SetQueryParameter(AttributeTypeColumn.DisplayOrder, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.AttributeTypeId) SetQueryParameter(AttributeTypeColumn.AttributeTypeId, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.IsPrivate) SetQueryParameter(AttributeTypeColumn.IsPrivate, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.LocaleId) SetQueryParameter(AttributeTypeColumn.LocaleId, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.Name) SetQueryParameter(AttributeTypeColumn.Name, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.PortalId) SetQueryParameter(AttributeTypeColumn.PortalID, filterOperator, filterValue, query);
			}
		}

		private void SetSorting(NameValueCollection sorts, AttributeTypeSortBuilder sortBuilder)
		{
			if (sorts.HasKeys())
			{
				foreach (var key in sorts.AllKeys)
				{
					var value = sorts.Get(key);

                    if (value == SortKeys.AttributeTypeId) SetSortDirection(AttributeTypeColumn.AttributeTypeId, sorts[StoredProcedureKeys.sortDirKey], sortBuilder);
                    if (value == SortKeys.DisplayOrder) SetSortDirection(AttributeTypeColumn.DisplayOrder, sorts[StoredProcedureKeys.sortDirKey], sortBuilder);
                    if (value == SortKeys.Name) SetSortDirection(AttributeTypeColumn.Name, sorts[StoredProcedureKeys.sortDirKey], sortBuilder);
				}
			}
		}

		private void SetQueryParameter(AttributeTypeColumn column, string filterOperator, string filterValue, AttributeTypeQuery query)
		{
			base.SetQueryParameter(column, filterOperator, filterValue, query);
		}

		private void SetSortDirection(AttributeTypeColumn column, string value, AttributeTypeSortBuilder sortBuilder)
		{
			base.SetSortDirection(column, value, sortBuilder);
		}

	}
}
