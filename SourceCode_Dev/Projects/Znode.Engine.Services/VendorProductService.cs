﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.Promotions;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using Znode.Engine.Taxes;
using Znode.Libraries.Helpers;
using Znode.Libraries.Helpers.Constants;
using Znode.Libraries.Helpers.Extensions;
using Znode.Libraries.Helpers.Utility;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;
using ProductRepository = ZNode.Libraries.DataAccess.Service.ProductService;
using SkuInventoryRepository = ZNode.Libraries.DataAccess.Service.SKUInventoryService;
using SkuRepository = ZNode.Libraries.DataAccess.Service.SKUService;
using SkuAdminRepository = ZNode.Libraries.Admin.SKUAdmin;

namespace Znode.Engine.Services
{
    public class VendorProductService : BaseService, IVendorProductService
    {

        #region Private Variables
        private readonly ProductRepository _productRepository;
        private readonly SkuRepository _skuRepository;
        private readonly SkuInventoryRepository _skuInventoryRepository;
        #endregion

        #region Constructor
        public VendorProductService()
        {
            _productRepository = new ProductRepository();
            _skuRepository = new SkuRepository();
            _skuInventoryRepository = new SkuInventoryRepository();
        }
        #endregion

        #region Public Methods
        public VendorProductListModel GetVendorProductList(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            List<Tuple<string, string, string>> filterList = new List<Tuple<string, string, string>>();

            VendorProductListModel list = new VendorProductListModel();            
            int totalRowCount = 0;
            var pagingStart = 0;
            var pagingLength = 0;
            string orderBy = StringHelper.GenerateDynamicOrderByClause(sorts);
            pagingStart = Convert.ToInt32(page.Get(PageKeys.Index));
            pagingLength = Convert.ToInt32(page.Get(PageKeys.Size));
            string innerWhereClause = string.Empty;

            foreach (var item in filters)
            {
                if (Equals(item.Item1, "username"))
                {
                    filterList.Add(item);
                    innerWhereClause = StringHelper.GenerateDynamicWhereClause(filterList);
                    filters.Remove(item);
                    break;
                }
            }
            string whereClause = StringHelper.GenerateDynamicWhereClause(filters);
            StoredProcedureHelper storedProcedureHelper = new StoredProcedureHelper();
            DataSet resultDataSet = storedProcedureHelper.SP_DataDetailsSpConfig(whereClause, string.Empty, StoredProcedureKeys.SpGetVendorProductList, orderBy, pagingStart.ToString(), pagingLength.ToString(), out totalRowCount,null,innerWhereClause);
            if (!Equals(resultDataSet, null) && resultDataSet.Tables.Count > 0 && !Equals(resultDataSet.Tables[0], null) && resultDataSet.Tables[0].Rows.Count > 0)
            {
                list.Products = resultDataSet.Tables[0].ToList<ProductModel>().ToCollection();
            }
            list.TotalResults = totalRowCount;
            list.PageSize = pagingLength;
            list.PageIndex = pagingStart;
            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                list.PageSize = list.TotalResults;
            }

            return list;
        }

        public VendorProductListModel GetMallAdminProductList(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            VendorProductListModel list = new VendorProductListModel();
            string whereClause = StringHelper.GenerateDynamicWhereClause(filters);
            int totalRowCount = 0;
            var pagingStart = 0;
            var pagingLength = 0;
            string orderBy = StringHelper.GenerateDynamicOrderByClause(sorts);
            pagingStart = Convert.ToInt32(page.Get(PageKeys.Index));
            pagingLength = Convert.ToInt32(page.Get(PageKeys.Size));

            StoredProcedureHelper storedProcedureHelper = new StoredProcedureHelper();
            DataSet resultDataSet = storedProcedureHelper.SP_DataDetailsSpConfig(whereClause, string.Empty, StoredProcedureKeys.SpGetMallAdminProductList, orderBy, pagingStart.ToString(), pagingLength.ToString(), out totalRowCount);

            list.Products = resultDataSet.Tables[0].ToList<ProductModel>().ToCollection();
            list.TotalResults = totalRowCount;
            list.PageSize = pagingLength;
            list.PageIndex = pagingStart;
            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                list.PageSize = list.TotalResults;
            }

            return list;
        }

        public bool ChangeProductStatus(RejectProductModel model, string state)
        {
            bool status = false;

            if (!string.IsNullOrEmpty(model.SelectedProductIds) && !string.IsNullOrEmpty(state))
            {
                ProductReviewStateAdmin reviewstateAdmin = new ProductReviewStateAdmin();
                ProductReviewState reviewState = reviewstateAdmin.GetProductReviewStateByName(state.ToString());
                List<int> vendorProductIds = model.SelectedProductIds.Split(',').Select(int.Parse).ToList();
                List<SupplierAdmin> listProductDetails = new List<SupplierAdmin>();
                AccountService accountService = new AccountService();
                ProductAdmin AdminAccess = new ProductAdmin();

                if (Equals(state, ZNodeProductReviewState.Approved.ToString()))
                {
                    if (!Equals(reviewState, null))
                    {
                        //Get all product details 
                        foreach (int id in vendorProductIds)
                        {
                            Product entity = AdminAccess.GetByProductId(id);
                            // Update only if existing status is not same as current status
                            if (entity.ReviewStateID != reviewState.ReviewStateID)
                            {
                                if (entity.AccountID > 0)
                                {
                                    //Get productId, productName, vendorEmail
                                    string vendorEmail = accountService.GetAccount((int)entity.AccountID, new NameValueCollection()).Email;
                                    listProductDetails.Add(new SupplierAdmin { AccountId = (int)entity.AccountID, ProductId = id, ProductName = entity.Name, VendorEmail = vendorEmail });
                                }
                                entity.ReviewStateID = reviewState.ReviewStateID;
                                status = AdminAccess.Update(entity);
                            }
                            UpdateReviewHistory(id, (ZNodeProductReviewState)entity.ReviewStateID, Convert.ToInt32(entity.AccountID), model.UserName);
                        }

                        //Send Mail 
                        if (listProductDetails.Count > 0)
                        {
                            this.SendMailBySiteAdmin(listProductDetails, state);
                        }
                        return true;
                    }
                }
                else if (Equals(state, ZNodeProductReviewState.Declined.ToString()))
                {
                    foreach (int id in vendorProductIds)
                    {
                        Product entity = AdminAccess.GetByProductId(id);
                        //Checking for franchise Admin AccountId
                        if (entity.AccountID == null)
                        {

                            //Method To Get Franchise AccountID
                            ProductReviewHistoryAdmin productReviewAdmin = new ProductReviewHistoryAdmin();
                            int accountID = productReviewAdmin.GetFranchiseAccountIDByPortalID(entity.PortalID.GetValueOrDefault(0));
                            entity.AccountID = accountID;
                        }
                        // Update only if existing status is not same as current status
                        if (entity.ReviewStateID != reviewState.ReviewStateID)
                        {
                            if (entity.AccountID > 0)
                            {
                                //Get productId, productName, vendorEmail
                                string vendorEmail = accountService.GetAccount((int)entity.AccountID, new NameValueCollection()).Email;
                                listProductDetails.Add(new SupplierAdmin { AccountId = (int)entity.AccountID, ProductId = id, ProductName = entity.Name, VendorEmail = vendorEmail });
                            }
                            entity.ReviewStateID = reviewState.ReviewStateID;
                            status = AdminAccess.Update(entity);
                        }

                        var isSuccess = SaveProductHistory(id, Convert.ToInt32(entity.AccountID), model);
                    }
                    //Send Mail 
                    if (listProductDetails.Count > 0)
                    {
                        this.SendMailBySiteAdmin(listProductDetails, state);
                    }
                }

            }
            return status;
        }

        public RejectProductModel BindRejectProduct(string productIds)
        {
            RejectProductModel model = new RejectProductModel();
            if (!string.IsNullOrEmpty(productIds))
            {
                string whereClause = string.Format(StoredProcedureKeys.RejectionMessageFilterCondition, ZNodeConfigManager.SiteConfig.PortalID, ZNodeConfigManager.SiteConfig.LocaleID);
                int totalRowCount = 0;
                StoredProcedureHelper storedProcedureHelper = new StoredProcedureHelper();
                DataSet resultDataSet = storedProcedureHelper.SP_DataDetailsSpConfig(whereClause, string.Empty, StoredProcedureKeys.SpGetRejectionMessages, string.Empty, "0", "0", out totalRowCount);
                List<RejectionMessageModel> lstRejectionMessages = resultDataSet.Tables[0].ToList<RejectionMessageModel>();

                model.RejectionResons = new Dictionary<string, string>();
                if (!Equals(lstRejectionMessages, null) && lstRejectionMessages.Count > 0)
                {
                    foreach (RejectionMessageModel item in lstRejectionMessages)
                    {
                        model.RejectionResons.Add(item.MessageKey, item.MessageKey);
                    }
                }
                ProductAdmin ProdAdmin = new ProductAdmin();
                AccountAdmin accountAdmin = new AccountAdmin();
                PortalAdmin portalAdmin = new PortalAdmin();
                Product product = null;
                List<int> vendorProductIds = productIds.Split(',').Select(int.Parse).ToList();
                List<int> distinctSupplierIdList = new List<int>();
                StringBuilder vendorList = new StringBuilder();
                foreach (int id in vendorProductIds)
                {
                    product = ProdAdmin.GetByProductId(id);
                    if (!Equals(product, null) && !Equals(product.AccountID, null))
                    {
                        // If only one product then display the name
                        if (Equals(vendorProductIds.Count, 1))
                        {
                            model.ProductName = product.Name;
                        }

                        int vendorId = Convert.ToInt32(product.AccountID);
                        if (!distinctSupplierIdList.Contains(vendorId))
                        {
                            distinctSupplierIdList.Add(vendorId);
                        }
                    }
                }
                foreach (int vendorId in distinctSupplierIdList)
                {
                    Address address = accountAdmin.GetDefaultBillingAddress(vendorId);
                    if (address != null)
                    {
                        vendorList.Append(address.FirstName + " " + address.LastName + ", ");
                    }
                }
                if (vendorList.ToString().IndexOf(",") > 1)
                {
                    model.VendorName = vendorList.ToString().Substring(0, vendorList.ToString().LastIndexOf(","));
                }
                else if (!string.IsNullOrEmpty(product.PortalID.ToString()))
                {
                    Portal portal = portalAdmin.GetByPortalId(Convert.ToInt32(product.PortalID));
                    model.VendorName = string.Format((string.IsNullOrEmpty(model.VendorName)) ? string.Empty : model.VendorName, (string.IsNullOrEmpty(portal.CompanyName)) ? string.Empty : portal.CompanyName);
                }
            }
            return model;
        }

        public ProductAlternateImageListModel GetReviewImageList(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            ProductAlternateImageListModel list = new ProductAlternateImageListModel();
            string whereClause = StringHelper.GenerateDynamicWhereClause(filters);
            int totalRowCount = 0;
            var pagingStart = 0;
            var pagingLength = 0;
            string orderBy = StringHelper.GenerateDynamicOrderByClause(sorts);

            pagingStart = Convert.ToInt32(page.Get(PageKeys.Index));
            pagingLength = Convert.ToInt32(page.Get(PageKeys.Size));

            StoredProcedureHelper storedProcedureHelper = new StoredProcedureHelper();
            DataSet resultDataSet = storedProcedureHelper.SP_DataDetailsSpConfig(whereClause, string.Empty, "ZNodeGetImageType", orderBy, pagingStart.ToString(), pagingLength.ToString(), out totalRowCount);
            list.ProductImages = resultDataSet.Tables[0].ToList<ProductAlternateImageModel>().ToCollection();
            list.TotalResults = totalRowCount;
            list.PageSize = pagingLength;
            list.PageIndex = pagingStart;
            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                list.PageSize = list.TotalResults;
            }

            return list;
        }

        public bool UpdateProductImageStatus(string productIds, string state)
        {
            bool status = false;
            if (!string.IsNullOrEmpty(productIds) && !string.IsNullOrEmpty(state))
            {
                List<string> selectedProductImagesIdList = new List<string>();
                List<int> vendorProductIds = productIds.Split(',').Select(int.Parse).ToList();

                ProductViewAdmin productAdmin = new ProductViewAdmin();
                foreach (int id in vendorProductIds)
                {
                    ProductImage productImage = productAdmin.GetByProductImageID(id);
                    int reviewStateId = 10;
                    reviewStateId = (Equals(state, "true")) ? Convert.ToInt32(ZNodeProductReviewState.Approved) : Convert.ToInt32(ZNodeProductReviewState.Declined);

                    // Update review state only if existing state is not same as current state.
                    if (productImage != null && (productImage.ReviewStateID != reviewStateId))
                    {
                        // Set the review state ID from enumeration
                        productImage.ReviewStateID = reviewStateId;

                        // Set the product image as rejected.
                        status = productAdmin.Update(productImage);
                    }
                }
            }
            return status;
        }

        public ProductModel UpdateVendorProduct(int productId, ProductModel model)
        {
            if (productId < 1)
            {
                throw new Exception("Product ID cannot be less than 1.");
            }

            if (Equals(model, null))
            {
                throw new Exception("Product model cannot be null.");
            }

            if (model.SkuName == null)
            {
                throw new Exception("SKU cannot be null.");
            }

            if (IsSkuExist(model.SkuName, model.ProductId))
            {
                throw new Exception("SKU or Part# already exists. Please try with different SKU or Part#.");
            }

            var product = _productRepository.GetByProductID(productId);
            if (product != null)
            {
                // Set product ID and update date
                model.ProductId = productId;
                model.UpdateDate = DateTime.Now;

                product.ProductID = productId;
                product.Name = model.Name;
                product.ProductNum = model.ProductNumber;
                product.ImageFile = model.ImageFile;
                product.AffiliateUrl = model.AffiliateUrl;
                product.Description = model.Description;
                product.ShortDescription = model.ShortDescription;
                product.AdditionalInformation = model.AdditionalInfo;
                product.FeaturesDesc = model.FeaturesDescription;
                product.RetailPrice = model.RetailPrice;
                product.InStockMsg = model.InStockMessage;
                product.OutOfStockMsg = model.OutOfStockMessage;
                product.ReviewStateID = Convert.ToInt32(ZNodeProductReviewState.Approved);
                product.IsShippable = model.IsShippable;
                product.Weight = model.Weight;
                product.Height = model.Height;
                product.Width = model.Width;
                product.Length = model.Length;
                product.TrackInventoryInd = model.TrackInventory;
                product.AllowBackOrder = model.AllowBackOrder;
                product.ActiveInd = model.IsActive;
                product.BackOrderMsg = model.BackOrderMessage;
                product.RecurringBillingInd = model.AllowRecurringBilling;
                product.RecurringBillingInstallmentInd = model.AllowRecurringBillingInstallment;
                product.RecurringBillingInitialAmount = model.RecurringBillingInitialAmount;
                product.RecurringBillingFrequency = model.RecurringBillingFrequency;
                product.SalePrice = model.SalePrice;

                var updated = _productRepository.Update(product);
                if (updated)
                {
                    product = _productRepository.GetByProductID(productId);

                    //To update sku
                    var skuEntity = _skuRepository.GetBySKUID(model.SkuId);
                    if (!Equals(skuEntity, null))
                    {
                        skuEntity.ProductID = productId;
                        skuEntity.SKU = model.SkuName;
                        skuEntity.ActiveInd = true;
                        SKUAdmin.UpdateQuantity(skuEntity, Convert.ToInt32(model.QuantityOnHand));
                    }
                    ProductAdmin productAdmin = new ProductAdmin();
                    productAdmin.DeleteProductCategories(productId);
                    if (!string.IsNullOrEmpty(model.CategoryIds))
                    {
                        List<int> vendorCategoryIds = model.CategoryIds.Split(',').Select(int.Parse).ToList();
                        // Add Product Categories
                        foreach (int item in vendorCategoryIds)
                        {
                            ProductCategory prodCategory = new ProductCategory();
                            ProductAdmin prodAdmin = new ProductAdmin();
                            prodCategory.ActiveInd = true;
                            prodCategory.CategoryID = item;
                            prodCategory.ProductID = productId;
                            prodAdmin.AddProductCategory(prodCategory);
                        }
                    }

                    return ProductMap.ToModel(product);
                }
            }

            return null;
        }

        public ProductModel UpdateMarketingDetails(int productId, ProductModel model, out string errorCode)
        {
            errorCode = "0";
            if (productId < 1)
            {
                throw new Exception("Product ID cannot be less than 1.");
            }

            if (Equals(model, null))
            {
                throw new Exception("Product model cannot be null.");
            }

            UrlRedirectAdmin urlRedirectAdmin = new UrlRedirectAdmin();
            ProductAdmin productAdmin = new ProductAdmin();
            Product product = new Product();
            string mappedSEOUrl = string.Empty;

            product = productAdmin.GetByProductId(productId);
            if (product.SEOURL != null)
            {
                mappedSEOUrl = product.SEOURL;
            }

            if (!Equals(product, null))
            {
                // Set product ID and update details
                product.ProductID = productId;
                product.DisplayOrder = model.DisplayOrder;
                product.HomepageSpecial = model.IsHomepageSpecial;
                product.FeaturedInd = model.IsFeatured;
                product.NewProductInd = model.IsNewProduct;
                product.SEOTitle = model.SeoTitle;
                product.SEODescription = model.SeoDescription;
                product.SEOKeywords = model.SeoKeywords;
                product.SEOURL = null;
                if (!string.IsNullOrEmpty(model.SeoUrl))
                {
                    product.SEOURL = model.SeoUrl.Trim().Replace(" ", "-");
                }

                if (string.Compare(product.SEOURL, mappedSEOUrl, true) != 0)
                {
                    if (urlRedirectAdmin.SeoUrlExists(product.SEOURL, product.ProductID))
                    {
                        errorCode = ErrorCodes.SEOUrlAlreadExists.ToString();
                        return new ProductModel();
                    }
                }
                bool status = false;

                if (!string.IsNullOrEmpty(model.UserName))
                {
                    AccountHelper accountHelper = new AccountHelper();
                    DataSet accountDataSet = accountHelper.GetAccountInfo(model.UserName);
                    AccountInfoModel accountInforModel = new AccountInfoModel();

                    if (!Equals(accountDataSet.Tables[0], null) && accountDataSet.Tables[0].Rows.Count > 0)
                    {
                        accountInforModel = accountDataSet.Tables[0].Rows[0].ToSingleRow<AccountInfoModel>();
                        if (!Equals(accountInforModel, null) && (Equals(accountInforModel.RoleName, Constant.RoleFranchise) || Equals(accountInforModel.RoleName, Constant.RoleVendor)))
                        {
                            product.ReviewStateID = productAdmin.UpdateReviewStateID(product, accountInforModel.PortalId, product.ProductID, accountInforModel.AccountId, model.UserName);
                        }
                    }
                }

                // PRODUCT UPDATE
                status = productAdmin.Update(product);

                if (status)
                {
                    urlRedirectAdmin.UpdateUrlRedirect(mappedSEOUrl);
                    product = _productRepository.GetByProductID(productId);
                    return ProductMap.ToModel(product);
                }
            }
            return null;
        }

        public CategoryNodeListModel GetCategoryNode(int productId)
        {
            CategoryNodeListModel model = new CategoryNodeListModel();
            if (productId > 0)
            {
                CategoryHelper categoryHelper = new CategoryHelper();
                ProductAdmin productAdmin = new ProductAdmin();
                Product product = new Product();
                product = productAdmin.GetByProductId(productId);
                int portalId = product.PortalID == null ? ZNodeConfigManager.SiteConfig.PortalID : Convert.ToInt32(product.PortalID);
                DataSet resultDataSet = categoryHelper.GetNavigationItems(portalId, ZNodeConfigManager.SiteConfig.LocaleID);
                model.CategoryNodes = resultDataSet.Tables[0].ToList<CategoryNodeModel>().ToCollection();
            }
            else
            {

                CategoryHelper categoryHelper = new CategoryHelper();
                int portalId = ZNodeConfigManager.SiteConfig.PortalID;
                DataSet resultDataSet = categoryHelper.GetNavigationItems(portalId, ZNodeConfigManager.SiteConfig.LocaleID);
                model.CategoryNodes = resultDataSet.Tables[0].ToList<CategoryNodeModel>().ToCollection();
            }
            return model;
        }

        public bool DeleteProduct(int productId)
        {
            if (productId < 1)
            {
                throw new Exception("Product ID cannot be less than 1.");
            }

            ProductAdmin _ProductAdmin = new ProductAdmin();
            return _ProductAdmin.DeleteByProductID(productId);
        }
        #endregion

        #region Private Methods

        /// <summary>
        /// To check sku is exist
        /// </summary>
        /// <param name="sku">string sku</param>
        /// <param name="productId"> int productId</param>
        /// <returns>returns true/ false</returns>
        private bool IsSkuExist(string sku, int productId)
        {
            SkuAdminRepository _adminSku = new SkuAdminRepository();
            bool isSkuExist = _adminSku.IsSkuExist(sku, productId);
            return isSkuExist;
        }

        /// <summary>
        /// Update Review History Method
        /// </summary>
        /// <param name="itemID">The value of ItemID</param>
        /// <param name="state">The value of Product Review State</param>
        /// <param name="vendorId">The value of Vendor Id</param>
        private void UpdateReviewHistory(int itemID, ZNodeProductReviewState state, int vendorId, string userName)
        {
            // Add entry to Product Review history table.
            ProductReviewHistoryAdmin productReviewHistoryAdmin = new ProductReviewHistoryAdmin();
            ProductReviewHistory productReviewHistory = new ProductReviewHistory();
            productReviewHistory.ProductID = itemID;
            productReviewHistory.VendorID = vendorId;
            productReviewHistory.LogDate = DateTime.Now;
            productReviewHistory.Username = (string.IsNullOrEmpty(userName)) ? string.Empty : userName;
            productReviewHistory.Status = state.ToString();
            productReviewHistory.Reason = state.ToString();
            productReviewHistory.Description = state.ToString();
            bool isSuccess = productReviewHistoryAdmin.Add(productReviewHistory);
        }

        /// <summary>
        /// This function will send  mail notification to vendor.
        /// To inform product is approved by Site Admin.
        /// </summary>
        /// <param name="productList">productList</param>
        private void SendMailBySiteAdmin(List<SupplierAdmin> productList, string state)
        {
            string smtpServer = ZNodeConfigManager.SiteConfig.SMTPServer;
            string senderEmail = ZNodeConfigManager.SiteConfig.AdminEmail;
            string siteAdmin = ZNodeConfigManager.SiteConfig.StoreName;
            string currentCulture = string.Empty;
            string templatePath = string.Empty;
            string defaultTemplatePath = string.Empty;

            int productId = 0;
            string productName = string.Empty;
            string vendorEmail = string.Empty;

            foreach (SupplierAdmin pair in productList)
            {
                productId = pair.ProductId;
                productName = pair.ProductName;
                string productId2 = productId.ToString();

                //Get Vendor Email Id
                string receiverEmail = pair.VendorEmail;

                //Subject is set.
                string subject = (Equals(state, ZNodeProductReviewState.Declined.ToString())) ? string.Format("{0} Product is declined", productName) : string.Format("{0} Product is approved", productName);

                // Template selection
                currentCulture = ZNodeCatalogManager.CultureInfo;

                if (Equals(state, ZNodeProductReviewState.Declined.ToString()))
                {
                    currentCulture = ZNodeCatalogManager.CultureInfo;
                    defaultTemplatePath = HttpContext.Current.Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, "RejectProductBySiteAdmin.html"));

                    templatePath = (!string.IsNullOrEmpty(currentCulture))
                        ? HttpContext.Current.Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "RejectProductBySiteAdmin_" + currentCulture + ".html")
                        : HttpContext.Current.Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, "RejectProductBySiteAdmin.html"));
                }
                else
                {
                    defaultTemplatePath = HttpContext.Current.Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, "ApproveMultipleProducts.html"));
                    templatePath = (!string.IsNullOrEmpty(currentCulture))
                        ? HttpContext.Current.Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "ApproveMultipleProducts_" + currentCulture + ".html")
                        : HttpContext.Current.Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, "ApproveMultipleProducts.html"));
                }
                // Check the default template file existence.
                if (!File.Exists(defaultTemplatePath))
                {
                    return;
                }
                else
                {
                    // If language specific template not available then load default template.
                    templatePath = defaultTemplatePath;
                }

                StreamReader rw = new StreamReader(templatePath);
                string messageText = rw.ReadToEnd();

                Regex rx1 = new Regex("#PRODUCTID#", RegexOptions.IgnoreCase);
                messageText = rx1.Replace(messageText, productId2);

                Regex rx2 = new Regex("#PRODUCTNAME#", RegexOptions.IgnoreCase);
                messageText = rx2.Replace(messageText, productName);

                Regex rx3 = new Regex("#SITEADMIN#", RegexOptions.IgnoreCase);
                messageText = rx3.Replace(messageText, siteAdmin);

                senderEmail = senderEmail.Replace(",", ", ");
                Regex rx4 = new Regex("#SITEADMINEMAIL#", RegexOptions.IgnoreCase);
                messageText = rx4.Replace(messageText, senderEmail);

                Regex RegularExpressionLogo = new Regex("#LOGO#", RegexOptions.IgnoreCase);
                messageText = RegularExpressionLogo.Replace(messageText, ZNodeConfigManager.SiteConfig.StoreName);

                try
                {
                    // Send mail
                    ZNode.Libraries.Framework.Business.ZNodeEmail.SendEmail(receiverEmail, senderEmail, string.Empty, subject, messageText, true);

                }
                catch (Exception ex)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.GeneralMessage, receiverEmail, HttpContext.Current.Request.UserHostAddress.ToString(), null, ex.Message, null);
                }
            }
        }

        /// <summary>
        /// Save the entry to product history table.
        /// </summary>
        /// <param name="productId">The value of Product ID</param>
        /// <param name="vendorId">The value of Vendor ID</param>
        /// <returns>Returns true if success else False.</returns>
        private bool SaveProductHistory(int productId, int vendorId, RejectProductModel model)
        {
            // Add entry to Product Review history table.
            ProductReviewHistoryAdmin productReviewHistoryAdmin = new ProductReviewHistoryAdmin();
            ProductReviewHistory productReviewHistory = new ProductReviewHistory();
            productReviewHistory.ProductID = productId;
            productReviewHistory.VendorID = vendorId;
            productReviewHistory.LogDate = DateTime.Now;
            productReviewHistory.Username = (string.IsNullOrEmpty(model.UserName)) ? string.Empty : model.UserName;
            productReviewHistory.Status = ZNodeProductReviewState.Declined.ToString();
            if (!string.IsNullOrEmpty(model.SelectedRejectionReason))
            {
                productReviewHistory.Reason = model.SelectedRejectionReason;
            }

            productReviewHistory.Description = HttpUtility.HtmlEncode(model.DetailReason);
            return productReviewHistoryAdmin.Add(productReviewHistory);
        }
        #endregion
    }
}
