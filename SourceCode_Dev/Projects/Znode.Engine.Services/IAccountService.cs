﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services
{
    public partial interface IAccountService
    {
        /// <summary>
        /// This method will get the account by Account ID
        /// </summary>
        /// <param name="accountId">integer Account ID</param>
        /// <param name="expands">Expand collection</param>
        /// <returns>Returns the account details in AccountModel format</returns>
        AccountModel GetAccount(int accountId, NameValueCollection expands);

        /// <summary>
        /// This method will get the account details by user id
        /// </summary>
        /// <param name="userId">Guid User Id</param>
        /// <param name="expands">Expand collection</param>
        /// <returns>Returns the account details in AccountModel format</returns>
        AccountModel GetAccountByUserId(Guid userId, NameValueCollection expands);

        /// <summary>
        /// This method will Get the accound details based on Username
        /// </summary>
        /// <param name="username">string Username</param>
        /// <param name="expands">Expand Collection</param>
        /// <returns>Returns the Account details in AccountModel format</returns>
        AccountModel GetAccountByUsername(string username, NameValueCollection expands);

        /// <summary>
        /// This method will get the details of the Accounts
        /// </summary>
        /// <param name="expands">Expand Collection expands</param>
        /// <param name="filters">Filter criteria</param>
        /// <param name="sorts">sort criteria</param>
        /// <param name="page">page collection</param>
        /// <returns>Returns all account detaisl in AccountListModel format</returns>
        AccountListModel GetAccounts(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// This method will create the Account for the user
        /// </summary>
        /// <param name="portalId">integer portal id</param>
        /// <param name="model">AccountModel model</param>
        /// <returns>Returns the new details in AccountModel format</returns>
        AccountModel CreateAccount(int portalId, AccountModel model);

        /// <summary>
        /// This method will update the account details based on account id
        /// </summary>
        /// <param name="accountId">integer Account ID</param>
        /// <param name="model">AccountModel model</param>
        /// <returns>Returns the updated details</returns>
        AccountModel UpdateAccount(int accountId, AccountModel model);

        /// <summary>
        /// This method is used to delete the account
        /// </summary>
        /// <param name="accountId">integer Account ID</param>
        /// <returns>Returns true if Account deleted</returns>
        bool DeleteAccount(int accountId);

        /// <summary>
        /// This method is used to logged in to the site
        /// </summary>
        /// <param name="portalId">integer Portal ID</param>
        /// <param name="model">AccountModel model</param>
        /// <param name="errorCode">Out param string error code</param>
        /// <param name="expand">Expand collection</param>
        /// <returns>Returns the data for the logged in</returns>
        AccountModel Login(int portalId, AccountModel model, out string errorCode, NameValueCollection expand);

        /// <summary>
        /// This method is used to change the password
        /// </summary>
        /// <param name="portalId">integer Portal ID</param>
        /// <param name="model">AccountModel model</param>
        /// <returns>Returns changed data in AccountModel format</returns>
        AccountModel ChangePassword(int portalId, AccountModel model);

        /// <summary>
        /// This method will reset the password
        /// </summary>
        /// <param name="model">AccountModel model</param>
        /// <returns>returns all the account model data</returns>
        AccountModel ResetPassword(AccountModel model);

        /// <summary>
        /// This method will get the Customer profile based on the Account ID and portal ID
        /// </summary>
        /// <param name="accountId">integer Account ID</param>
        /// <param name="portalId">integer Portal ID</param>
        /// <returns>Returns the profile data</returns>
        Profile GetCustomerProfile(int accountId, int portalId);

        /// <summary>
        /// Znode Version 7.2.2 
        /// This function will check the Reset Password Link current status.
        /// </summary>
        /// <param name="model">AccountModel</param>
        /// <param name="errorCode"></param>
        /// <returns>Returns Reset Password Link Status i.e. Error codes</returns>
        AccountModel CheckResetPasswordLinkStatus(AccountModel model, out string errorCode);

        /// <summary>
        /// Znode Version 8.0
        /// Method Check for the user role based on role name
        /// </summary>
        /// <param name="userName">string userName</param>
        /// <param name="roleName">string roleName</param>
        /// <returns>Returns status for the user based on role name</returns>
        AccountModel CheckUserRole(string userName, string roleName);

        /// <summary>
        ///  To get Valid Address
        /// </summary>
        /// <param name="model">AddressModel model</param>
        /// <returns>returns valid AddressModel</returns>
        AddressModel IsAddressValid(AddressModel model);

        /// <summary>
        /// Znode Version 8.0
        /// Method used to reset Admin details in case the first default login.
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Return the Reset admin details</returns>
        AccountModel ResetAdminDetails(AccountModel model);

        /// <summary>
        /// Znode Version 8.0
        /// This method will create the Admin account
        /// </summary>
        /// <param name="model">AccountModel model</param>
        /// <returns>return all the account details</returns>
        AccountModel CreateAdminAccount(int portalId, AccountModel model);

        /// <summary>
        /// This method will return all the accouts with the same role name
        /// </summary>
        /// <param name="roleName">string Role Name</param>
        /// <returns>Returns the Account Details</returns>
        AccountListModel GetAccountDetailsByRoleName(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page, out int totalRowCount);

        /// <summary>
        /// Create Vendor Account.
        /// </summary>
        /// <param name="portalId">portal Id</param>
        /// <param name="model">AccountModel</param>
        /// <returns>AccountModel</returns>
        AccountModel CreateVendorAccount(int portalId, AccountModel model);

        /// <summary>
        /// This function is used to check Role Exist For Profile
        /// </summary>
        /// <param name="porfileId">int porfileId - default porfileId</param>
        /// <param name="roleName">string roleName</param>
        /// <returns>returns true/false</returns>
        bool IsRoleExistForProfile(int porfileId, string roleName);

        /// <summary>
        /// This method will disable the admin account
        /// </summary>
        /// <param name="accountId"></param>
        /// <returns></returns>
        bool EnableDisableAccount(int accountId);

        /// <summary>
        /// Create Franchise Account.
        /// </summary>
        /// <param name="portalId">portal Id</param>
        /// <param name="model">AccountModel</param>
        /// <returns>AccountModel</returns>
        AccountModel CreateFranchiseAccount(int portalId, AccountModel model);

        /// <summary>
        /// To Update Vendor Account Details
        /// </summary>
        /// <param name="accountId">Id of the Vendor Account</param>
        /// <param name="model">AccountModel model</param>
        /// <returns>Return Updated Model in AccountModel format.</returns>
        AccountModel UpdateVendorAccount(int accountId, AccountModel model);

        /// <summary>
        /// To Update Customer Account Details
        /// </summary>
        /// <param name="accountId">Id of the Customer Account</param>
        /// <param name="model">AccountModel model</param>
        /// <returns>Return Updated Model in AccountModel format & Errorcode.</returns>
        AccountModel UpdateCustomerAccount(int accountId, AccountModel model, out string errorCode);

        /// <summary>
        /// Create Customer Account.
        /// </summary>
        /// <param name="portalId">Portal Id</param>
        /// <param name="model">AccountModel</param>
        /// <param name="errorCode">hold error message</param>
        /// <returns></returns>
        AccountModel CreateCustomerAccount(int portalId, AccountModel model, out string errorCode);

        /// <summary>
        /// To send email with attachment
        /// </summary>
        /// <param name="model">SendMailModel model</param>
        /// <returns>returns SendMailModel</returns>
        SendMailModel SendEmail(SendMailModel model);

        /// <summary>
        /// Get the list of Customer Account Payment
        /// </summary>
        /// <param name="filters">Filter criteria</param>
        /// <param name="sorts">sort criteria</param>
        /// <param name="page">page collection</param>
        /// <returns>Returns all Customer Account Payments in AccountPaymentListModel format</returns>
        AccountPaymentListModel GetAccountPaymentList(int accountId, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Create Customer Account Payment
        /// </summary>
        /// <param name="model">AccountPaymentModel</param>
        /// <returns>AccountPayment Model</returns>
        AccountPaymentModel CreateAccountPayment(AccountPaymentModel model);

        /// <summary>
        /// Get list of Role Permissions based on userName.
        /// </summary>
        /// <param name="userName">UserName for the user</param>
        /// <returns>Return the Role Permission list in RolePermissionListModel format</returns>
        RolePermissionListModel GetRolePermission(string userName);

        /// <summary>
        /// Get list of Role Menu based on userName.
        /// </summary>
        /// <param name="model">Model of type AccountModel</param>
        /// <returns>Return the Role Menu list in RoleMenuListModel format</returns>
        RoleMenuListModel GetRoleMenuList(AccountModel model);

        /// <summary>
        /// Sign Up the User for NewsLetters
        /// </summary>
        /// <param name="model">NewsLetterSignUpModel</param>
        /// <returns>returns true/false</returns>
        bool SignUpForNewsLetter(NewsLetterSignUpModel model);

        /// <summary>
        /// Validate the Social User Login
        /// </summary>
        /// <param name="portalId">integer Portal ID</param>
        /// <param name="model">SocialLoginModel</param>
        /// <param name="expands">Expand collection</param>
        /// <returns>return details in AccountModelFormat</returns>
        AccountModel SocialUserLogin(int portalId, SocialLoginModel model, NameValueCollection expand);

        /// <summary>
        /// Creates/Updated the Social User Account
        /// </summary>
        /// <param name="model">SocialLoginModel</param>
        /// <returns>Return true or false</returns>
        bool SocialUserCreateOrUpdateAccount(SocialLoginModel model);

        /// <summary>
        /// To Get All Registered Social Client Details List
        /// </summary>
        /// <returns>Return Details in RegisteredSocialClientListModel format</returns>
        RegisteredSocialClientListModel GetRegisteredSocialClientDetailsList();
    }
}
