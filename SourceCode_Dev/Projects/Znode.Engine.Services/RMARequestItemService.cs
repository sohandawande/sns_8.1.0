﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Libraries.Helpers.Extensions;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using RMARequestItemRepository = ZNode.Libraries.DataAccess.Service.RMARequestItemService;

namespace Znode.Engine.Services
{
    public class RMARequestItemService : BaseService, IRMARequestItemService
    {
        private readonly RMARequestItemRepository _rmaRequestItemRepository;

        public RMARequestItemService()
        {
            _rmaRequestItemRepository = new RMARequestItemRepository();
        }

        public RMARequestItemModel CreateRMARequest(RMARequestItemModel model)
        {
            if (Equals(model, null))
            {
                throw new Exception("RMA requestItem model cannot be null.");
            }

            RMARequestItem entity = RMARequestItemMap.ToEntity(model);
            RMARequestItemModel rmaRequestItemModel = RMARequestItemMap.ToModel(_rmaRequestItemRepository.Save(entity));
            return rmaRequestItemModel;
        }

        public RMARequestItemListModel GetRMARequestItemList(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            RMARequestItemListModel model = new RMARequestItemListModel();

            RMARequestAdmin rmaRequestAdmin = new RMARequestAdmin();

            string flag = string.Empty;
            int orderId = 0, rmaId = 0;

            foreach (Tuple<string, string, string> filterItem in filters)
            {
                if (filterItem.Item1.Equals(FilterKeys.OrderId, StringComparison.InvariantCultureIgnoreCase))
                {
                    orderId = Convert.ToInt32(filterItem.Item3);
                }
                else if (filterItem.Item1.Equals(FilterKeys.RMARequestId, StringComparison.InvariantCultureIgnoreCase))
                {
                    rmaId = Convert.ToInt32(filterItem.Item3);
                }
                else if (filterItem.Item1.Equals(FilterKeys.Flag, StringComparison.InvariantCultureIgnoreCase))
                {
                    flag = filterItem.Item3;
                }
            }

            flag = flag.Equals("Empty", StringComparison.CurrentCultureIgnoreCase) ? string.Empty : flag;

            int pagingStart = 0;
            int pagingLength = 0;
            SetPaging(page, model, out pagingStart, out pagingLength);

            DataSet resultDataSet = rmaRequestAdmin.GetRMAOrderLineItemsByOrderID(orderId, rmaId, 0, flag);

            if (!Equals(resultDataSet, null) && resultDataSet.Tables.Count > 0)
            {
                model.RMARequestItemList = resultDataSet.Tables[0].ToList<RMARequestItemModel>().ToCollection();
            }

            model.TotalResults = model.RMARequestItemList.Count();
            return model;
        }

        public RMARequestItemListModel GetRMARequestItemsForGiftCard(string orderLineItemList)
        {
            RMARequestItemListModel model = new RMARequestItemListModel();

            RMARequestAdmin rmaRequestAdmin = new RMARequestAdmin();

            DataSet resultDataSet = rmaRequestAdmin.GetRMARequestItem(orderLineItemList);

            if (!Equals(resultDataSet, null) && !Equals(resultDataSet.Tables[0], null) && resultDataSet.Tables[0].Rows.Count > 0)
            {
                model.RMARequestItemList = resultDataSet.Tables[0].ToList<RMARequestItemModel>().ToCollection();
            }
            return model;
        }
    }
}
