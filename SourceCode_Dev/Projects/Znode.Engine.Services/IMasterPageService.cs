﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
   public interface IMasterPageService
    {
       /// <summary>
       /// Get master pages. 
       /// </summary>
        /// <param name="expands">NameValueCollection expands</param>
        /// <param name="filters">List<Tuple<string, string, string>> filters</param>
        /// <param name="sorts">NameValueCollection sorts</param>
        /// <param name="page">NameValueCollection page</param>
        /// <returns>MasterPageListModel</returns>
       MasterPageListModel GetMasterPages(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

       /// <summary>
       /// Get master page list by Css theme id.
       /// </summary>
       /// <param name="themeId">Id of the Css theme.</param>
       /// <param name="pageType">Paget type</param>
       /// <returns>MasterPageListModel</returns>
       MasterPageListModel GetMasterPageListByThemeId(int themeId, string pageType);

       /// <summary>
       /// Get master page.
       /// </summary>
       /// <param name="themeId">Id of the Css theme.</param>
       /// <returns>MasterPageModel</returns>
       MasterPageModel GetMasterPage(int themeId);
    }
}
