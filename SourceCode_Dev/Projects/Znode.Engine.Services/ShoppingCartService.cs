﻿using System;
using System.Linq;
using ZNode.Libraries.ECommerce.Entities;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Maps;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNodeShoppingCart = ZNode.Libraries.ECommerce.ShoppingCart.ZNodeShoppingCart;
using ZNodeShoppingCartItem = ZNode.Libraries.ECommerce.ShoppingCart.ZNodeShoppingCartItem;

namespace Znode.Engine.Services
{
    public class ShoppingCartService : IShoppingCartService
    {
        public ShoppingCartModel GetCartByAccount(int accountId, int portalId)
        {
            var znodeShoppingCart = ZNodeShoppingCart.LoadFromDatabase(accountId, portalId);
            var shoppingCartModel = ShoppingCartMap.ToModel(znodeShoppingCart);

            return shoppingCartModel;
        }

        public ShoppingCartModel GetCartByCookie(int cookieId)
        {
            var znodeShoppingCart = ZNodeShoppingCart.LoadFromDatabase(cookieId);
            var shoppingCartModel = ShoppingCartMap.ToModel(znodeShoppingCart);
            if (shoppingCartModel != null)
            {
                shoppingCartModel.CookieId = cookieId;
            }


            return shoppingCartModel;
        }

        public ShoppingCartModel CreateCart(int portalId, ShoppingCartModel model)
        {
            int? accountId = null;
            if (model == null)
            {
                throw new Exception("Shopping cart model cannot be null.");
            }

            var znodeShoppingCart = ShoppingCartMap.ToZnodeShoppingCart(model);
            if (model.Account != null && model.Account.AccountId > 0)
            {
                accountId = model.Account.AccountId;
            }

            var cookieMappingId = znodeShoppingCart.Save(accountId, portalId, model.CookieId);

            if (cookieMappingId > 0)
            {
                ShoppingCartModel shoppingCart = GetCartByCookie(cookieMappingId);
                if (!Equals(shoppingCart, null) && !Equals(shoppingCart.ShoppingCartItems, null))
                {
                    foreach (ShoppingCartItemModel shoppingCartItem in shoppingCart.ShoppingCartItems)
                    {
                        shoppingCartItem.ProductDiscountAmount = model.ShoppingCartItems.Where(product => Equals(product.ProductId, shoppingCartItem.ProductId)).FirstOrDefault().ProductDiscountAmount;
                        shoppingCartItem.ERPUnitPrice = model.ShoppingCartItems.Where(product=>Equals(product.ProductId,shoppingCartItem.ProductId)).FirstOrDefault().ERPUnitPrice;
                        shoppingCartItem.ERPExtendedPrice = model.ShoppingCartItems.Where(product => Equals(product.ProductId, shoppingCartItem.ProductId)).FirstOrDefault().ERPExtendedPrice;
                        shoppingCartItem.AllowBackOrder=model.ShoppingCartItems.Where(product => Equals(product.ProductId, shoppingCartItem.ProductId)).FirstOrDefault().AllowBackOrder;
                        shoppingCartItem.BackOrderMessage = model.ShoppingCartItems.Where(product => Equals(product.ProductId, shoppingCartItem.ProductId)).FirstOrDefault().BackOrderMessage;

                    }
                }
                return shoppingCart;
            }

            return null;
        }

        public ShoppingCartModel Calculate(int profileId, ShoppingCartModel model)
        {
            if (model == null)
            {
                throw new Exception("Shopping cart model cannot be null.");
            }

            var znodeShoppingCart = ShoppingCartMap.ToZnodeShoppingCart(model);

            if (profileId == 0)
            {
                profileId = model.ProfileId.GetValueOrDefault(0);
            }

            if (model.Account != null && model.Account.AccountId > 0 && znodeShoppingCart.IsMultipleShipToAddress)
                return Calculate(profileId, znodeShoppingCart);

            znodeShoppingCart.Calculate(model.ProfileId);

            var calculatedModel = ShoppingCartMap.ToModel(znodeShoppingCart);

            calculatedModel.ShippingAddress = model.ShippingAddress;

            if (!Equals(calculatedModel, null) && !Equals(calculatedModel.ShoppingCartItems, null))
            {
                foreach (ShoppingCartItemModel shoppingCartItem in calculatedModel.ShoppingCartItems)
                {
                    decimal modelCartItemDiscountAmount = model.ShoppingCartItems.Where(product => Equals(product.ProductId, shoppingCartItem.ProductId)).FirstOrDefault().ProductDiscountAmount;
                    if (shoppingCartItem.ProductDiscountAmount <= 0.0M && modelCartItemDiscountAmount > 0.0M)
                    {
                        shoppingCartItem.ProductDiscountAmount = model.ShoppingCartItems.Where(product => Equals(product.ProductId, shoppingCartItem.ProductId)).FirstOrDefault().ProductDiscountAmount;
                        shoppingCartItem.ERPUnitPrice = model.ShoppingCartItems.Where(product => Equals(product.ProductId, shoppingCartItem.ProductId)).FirstOrDefault().ERPUnitPrice;
                        shoppingCartItem.ERPExtendedPrice = model.ShoppingCartItems.Where(product => Equals(product.ProductId, shoppingCartItem.ProductId)).FirstOrDefault().ERPExtendedPrice;
                    }
                }
            }

            return calculatedModel;
        }

        private ShoppingCartModel Calculate(int profileId, ZNodeShoppingCart znodeCart)
        {
            // Return to handle Multiple ship to address on client side.                        
            var resultItems =
                znodeCart.ShoppingCartItems.OfType<ZNodeShoppingCartItem>()
                         .SelectMany(y => y.OrderShipments)
                         .GroupBy(x => new { x.AddressID, x.ShippingID })
                         .Select(
                             x =>
                             new OrderShipmentModel()
                                 {
                                     AddressId = x.Key.AddressID,
                                     ShippingOptionId = x.Key.ShippingID,
                                     Quantity = x.Sum(y => y.Quantity)
                                 })
                         .ToList();

            var portalCarts = znodeCart.PortalCarts.ToList();

            portalCarts.ForEach(x =>
                {
                    x.AddressCarts.ForEach(
                        y =>
                        {
                            var item = resultItems.FirstOrDefault(z => z.AddressId == y.AddressID);
                            y.Shipping = new ZNodeShipping()
                                {
                                    ShippingID = item.ShippingOptionId.GetValueOrDefault(0),
                                    ShippingName = item.ShippingName,
                                };

                            if (znodeCart.Payment != null && y.ShoppingCartItems.Count > 0)
                            {
                                y.Payment = new ZNodePayment()
                                    {
                                        BillingAddress = znodeCart.Payment.BillingAddress,
                                        ShippingAddress = y.ShoppingCartItems[0].Product.AddressToShip
                                    };
                            }
                        });

                    x.GiftCardNumber = portalCarts.Count == 1 ? znodeCart.GiftCardNumber : string.Empty;
                    x.Calculate(znodeCart.ProfileId);
                });

            var calculatedModel = ShoppingCartMap.ToModel(znodeCart.PortalCarts.FirstOrDefault());
            return calculatedModel;
        }
    }
}
