﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    public interface IPersonalizationService
    {
        /// <summary>
        /// Gets Product list with Frequently bought products.
        /// </summary>
        /// <param name="expands">Expands for product list</param>
        /// <param name="filters">Filters for product list.</param>
        /// <param name="sorts">Sorting of Product list.</param>
        /// <param name="page">Paging of product list.</param>
        /// <returns>Returns list of products with frequently bought products.</returns>
        CrossSellListModel GetFrequentlyBoughtProductList(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Gets Cross Sell products by product id.
        /// </summary>
        /// <param name="filters">Filters</param>
        /// <param name="sorts">Sorts</param>
        /// <param name="page">Paginations.</param>
        /// <returns>CrossSellListModel</returns>
        CrossSellListModel GetCrossSellProductsByProductId(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Remove Cross Sell Products 
        /// </summary>
        /// <param name="listModel">List model of CrossSell</param>
        /// <returns></returns>
        bool DeleteCrossSellProducts(int productId, int relationTypeId);

        /// <summary>
        /// Add Cross Sell Products
        /// </summary>
        /// <param name="model">CrossSell Model</param>
        /// <returns>CrossSellModel</returns>
        CrossSellModel CreateCrossSellProduct(CrossSellModel model);

        /// <summary>
        /// Creates list of cross sell model.
        /// </summary>
        /// <param name="model">Cross sell list model.</param>
        /// <returns>Boolean value depicting the cross sell list is created or not.</returns>
        bool CreateCrossSellProducts(CrossSellListModel model);
    }
}
