﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    public partial interface IOrderService
    {
        OrderModel GetOrder(int orderId, NameValueCollection expands);
        OrderListModel GetOrders(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
        OrderModel CreateOrder(int portalId, ShoppingCartModel model);
        OrderModel UpdateOrder(int orderId, OrderModel model);

        /// <summary>
        /// Gets Order list from database using stored procedure.
        /// ZNode Version 8.0
        /// </summary>
        /// <param name="expands">NameValueCollection of expands</param>
        /// <param name="filters">List<Tuple<string, string, string>> of filters</param>
        /// <param name="sorts">NameValueCollection of sorts</param>
        /// <param name="page">NameValueCollection of page</param>
        /// <returns>Returns model of type AdminOrderList model</returns>
        AdminOrderListModel GetOrderList(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Gets Order details from database using stored procedure.
        /// ZNode Version 8.0
        /// </summary>
        /// <param name="expands">NameValueCollection of expands</param>
        /// <param name="filters">List<Tuple<string, string, string>> of filters</param>
        /// <param name="sorts">NameValueCollection of sorts</param>
        /// <param name="page">NameValueCollection of page</param>
        /// <returns>Returns model of type Order model</returns>
        OrderModel GetOrderDetails(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Updates existing order status.
        /// ZNode Version 8.0
        /// </summary>
        /// <param name="orderId">Order Id</param>
        /// <param name="model">Model of type Order model</param>
        /// <returns>Returns OrderModel</returns>
        OrderModel UpdateOrderStatus(int orderId, OrderModel model);

        /// <summary>
        /// Updates order with void payment.
        /// ZNode Version 8.0
        /// </summary>
        /// <param name="orderId">Order Id</param>
        /// <param name="model">Model of type Order model</param>
        /// <returns>Returns OrderModel</returns>
        OrderModel RefundPayment(int orderId, OrderModel model);

        /// <summary>
        /// Updates order with refund payment.
        /// ZNode Version 8.0
        /// </summary>
        /// <param name="orderId">Order Id</param>
        /// <param name="model">Model of type Order model</param>
        /// <returns>Returns OrderModel</returns>
        OrderModel VoidPayment(int orderId, OrderModel model);

        /// <summary>
        /// Downloads order data from db using sp.
        /// ZNode Version 8.0
        /// </summary>
        /// <param name="filters">List<Tuple<string, string, string>> of filters</param>
        /// <returns>Returns Dataset.</returns>
        DataSet DownloadOrderData(List<Tuple<string, string, string>> filters);

        /// <summary>
        /// Downloads order line item data from db using sp.
        /// ZNode Version 8.0
        /// </summary>
        /// <param name="filters">List<Tuple<string, string, string>> of filters</param>
        /// <returns>Returns Dataset.</returns>
        DataSet DownloadOrderLineItemsData(List<Tuple<string, string, string>> filters);

        /// <summary>
        /// Get order line item on the basis of order line item id.
        /// </summary>
        /// <param name="orderLineItemId">int id of order line item</param>
        /// <param name="expands">NameValueCollection of expands</param>
        /// <returns>Returns order line item on the basis of order line item id.</returns>
        OrderLineItemModel GetOrderLineItem(int orderLineItemId, NameValueCollection expands);

        /// <summary>
        /// Update order line item status on the basis of order line item id.
        /// </summary>
        /// <param name="orderLineItemId">int id of order line item</param>
        /// <param name="model">Model of type OrderLineItemModel</param>
        /// <returns>Returns updated order line item model.</returns>
        OrderLineItemModel UpdateOrderLineItem(int orderLineItemId, OrderLineItemModel model);

        /// <summary>
        /// This method will update the order with Payment status id
        /// </summary>
        /// <param name="orderId">int order id</param>
        /// <param name="paymentStatus">string payment status</param>
        /// <returns>true if updated</returns>
        bool updateorderpaymentstatus(int orderId, string paymentStatus);

        /// <summary>
        /// Sends email for the shipped status change and also tracking number of order line item.
        /// </summary>
        /// <param name="filters">List<Tuple<string, string, string>> of filters containing order related data</param>
        /// <returns>Returns string message whether the email was sent or not.</returns>
        string SendEmail(List<Tuple<string, string, string>> filters);
    }
}
