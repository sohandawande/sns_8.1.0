﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
	public interface IInventoryService
	{
		InventoryModel GetInventory(int inventoryId, NameValueCollection expands);
		InventoryListModel GetInventories(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
		InventoryModel CreateInventory(InventoryModel model);
		InventoryModel UpdateInventory(int inventoryId, InventoryModel model);
		bool DeleteInventory(int inventoryId);
	}
}
