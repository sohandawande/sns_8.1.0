﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Helpers;
using Znode.Libraries.Helpers.Constants;
using Znode.Libraries.Helpers.Extensions;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using PortalProfileRepository = ZNode.Libraries.DataAccess.Service.PortalProfileService;

namespace Znode.Engine.Services
{
    public class PortalProfilesService : BaseService, IPortalProfileService
    {
        #region Private Variables
        private readonly PortalProfileRepository _portalProfileRepository;
        #endregion

        #region Constructor
        public PortalProfilesService()
        {
            _portalProfileRepository = new PortalProfileRepository();
        } 
        #endregion

        #region Public Methods 

        public PortalProfileModel CreatePortalProfile(PortalProfileModel model)
        {
            if (Equals(model, null))
            {
                throw new Exception("Attributes model cannot be null.");
            }

            var entity = PortalProfileMap.ToEntity(model);
            var portalProfile = _portalProfileRepository.Save(entity);
            return PortalProfileMap.ToModel(portalProfile);
        }

        public PortalProfileModel GetPortalProfile(int protalProfileId, NameValueCollection expands)
        {
            var portalProfile = _portalProfileRepository.GetByPortalProfileID(protalProfileId);
            if (!Equals(portalProfile, null))
            {               
                return PortalProfileMap.ToModel(portalProfile);
            }

            return null;
        }

        public PortalProfileListModel GetPortalProfiles(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new PortalProfileListModel();
            var portalProfiles = new TList<PortalProfile>();
            var tempList = new TList<PortalProfile>();

            var query = new PortalProfileQuery();
            var sortBuilder = new PortalProfileSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            tempList = _portalProfileRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;
            
            // Check to set page size equal to the total number of results
            if (pagingLength.Equals(int.MaxValue))
            {
                model.PageSize = model.TotalResults;
            }

            // Now go through the list and get any expands
            foreach (var portalProfile in tempList)
            {               
                portalProfiles.Add(portalProfile);
            }

            // Map each item and add to the list
            foreach (var portalProfile in portalProfiles)
            {
                model.PortalProfiles.Add(PortalProfileMap.ToModel(portalProfile));
            }

            return model;
        }

        public bool DeletePortalProfile(int portalProfileId)
        {
            if (portalProfileId < 1)
            {
                throw new Exception("Portal profile ID cannot be less than 1.");
            }

            var portalProfile = _portalProfileRepository.GetByPortalProfileID(portalProfileId);
            if (!Equals(portalProfile, null))
            {
                bool isDeleted = false;
                isDeleted = _portalProfileRepository.Delete(portalProfile.PortalProfileID);
                if (isDeleted)
                {
                    ResetDefaultProfile(portalProfile.PortalID, portalProfile.ProfileID);
                }
                return isDeleted;
            }

            return false;
        }

        public PortalProfileModel UpdatePortalProfile(int portalProfileId, PortalProfileModel portalProfileModel)
        {
            if (portalProfileId < 1)
            {
                throw new Exception("Portal profile ID cannot be less than 1.");
            }

            if (Equals(portalProfileModel, null))
            {
                throw new Exception("Portal profile model cannot be null.");
            }

            var portalProfile = _portalProfileRepository.GetByPortalProfileID(portalProfileId);
            if (!Equals(portalProfile, null))
            {
                // Set the order ID
                portalProfileModel.PortalProfileID = portalProfileId;

                var portalProfileToUpdate = PortalProfileMap.ToEntity(portalProfileModel);

                var updated = _portalProfileRepository.Update(portalProfileToUpdate);
                if (updated)
                {
                    portalProfile = _portalProfileRepository.GetByPortalProfileID(portalProfileId);
                    return PortalProfileMap.ToModel(portalProfile);
                }
            }

            return null;
        }
        #endregion

        #region Private Methods  

        private void SetFiltering(List<Tuple<string, string, string>> filters, PortalProfileQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;

                if (filterKey.Equals(FilterKeys.PortalId)) { SetQueryParameter(PortalProfileColumn.PortalID, filterOperator, filterValue, query); }
            }
        }

        private void SetSorting(NameValueCollection sorts, PortalProfileSortBuilder sortBuilder)
        {
            if (sorts.HasKeys())
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);

                    if (value.Equals(SortKeys.PortalProfileId)) { SetSortDirection(PortalProfileColumn.PortalProfileID, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                }
            }
            else
            {
                SetSortDirection(PortalProfileColumn.PortalProfileID, sorts[StoredProcedureKeys.sortDirKey], sortBuilder);
            }
        }

        /// <summary>
        /// Reset the default profile
        /// </summary>
        /// <param name="PortalID">Portal Id for the profile</param>
        /// <param name="ProfileID">Profile Id for the profile</param>
        private void ResetDefaultProfile(int PortalID, int ProfileID)
        {
            PortalAdmin portalAdmin = new PortalAdmin();
            ZNode.Libraries.DataAccess.Service.PortalService portalService = new ZNode.Libraries.DataAccess.Service.PortalService();
            Portal portal = portalAdmin.GetByPortalId(PortalID);

            if (Equals(portal.DefaultAnonymousProfileID, ProfileID))
            {
                portal.DefaultAnonymousProfileID = null;
            }

            if (Equals(portal.DefaultRegisteredProfileID, ProfileID))
            {
                portal.DefaultRegisteredProfileID = null;
            }

            portalService.Update(portal);
        }
        #endregion

        #region Znode Version 8.0
        public PortalProfileListModel GetPortalProfileListDetails(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            PortalProfileListModel list = new PortalProfileListModel();
            int totalRowCount = 0;
            var pagingStart = 0;
            var pagingLength = 0;
            string orderBy = StringHelper.GenerateDynamicOrderByClause(sorts);
            pagingStart = Convert.ToInt32(page.Get(PageKeys.Index));
            pagingLength = Convert.ToInt32(page.Get(PageKeys.Size));
            string whereClause = StringHelper.GenerateDynamicWhereClause(filters);

            StoredProcedureHelper storedProcedureHelper = new StoredProcedureHelper();

            DataSet resultDataSet = storedProcedureHelper.SP_DataDetailsSpConfig(whereClause, null, StoredProcedureKeys.SPGetProfilewithSerialNumber, orderBy, pagingStart.ToString(), pagingLength.ToString(), out totalRowCount);

            list.PortalProfiles = resultDataSet.Tables[0].ToList<PortalProfileModel>().ToCollection();
            list.TotalResults = totalRowCount;
            list.PageSize = pagingLength;
            list.PageIndex = pagingStart;
            // Check to set page size equal to the total number of results
            if (pagingLength.Equals(int.MaxValue))
            {
                list.PageSize = list.TotalResults;
            }
            return list;
        }

        public PortalProfileListModel GetPortalProfilesByPortalId(int portalId)
        {
            var model = new PortalProfileListModel();
            var portalProfiles = new TList<PortalProfile>();
            var tempList = new TList<PortalProfile>();                        

            // Get the initial set
            var totalResults = 0;
            tempList = _portalProfileRepository.GetByPortalID(portalId);
            model.TotalResults = totalResults;            

            // Now go through the list and get any expands
            foreach (var portalProfile in tempList)
            {
                portalProfiles.Add(portalProfile);
            }

            // Map each item and add to the list
            foreach (var portalProfile in portalProfiles)
            {
                model.PortalProfiles.Add(PortalProfileMap.ToModel(portalProfile));
            }

            return model;
        }
        #endregion
    }
}
