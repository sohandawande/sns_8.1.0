﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    /// <summary>
    /// This is the interface for the Permissions service
    /// </summary>
    public interface IPermissionsService
    {
        /// <summary>
        /// This method will fetch the Roles and Permissions from DB for the specific Account Id
        /// </summary>
        /// <param name="accountId">int Account Id</param>
        /// <param name="filters"></param>
        /// <param name="expand"></param>
        /// <returns></returns>
        PermissionsModel GetRolesAndPerminssions(int accountId, List<Tuple<string, string, string>> filters, NameValueCollection expand);

        /// <summary>
        /// This method will update the Roles and permissions for relevant Account Id
        /// </summary>
        /// <param name="model">PermissionsModel model</param>
        /// <returns>True if the records updated</returns>
        bool UpdateRolesAndPermissions(PermissionsModel model);
    }
}
