﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using RequestStatusRepository = ZNode.Libraries.DataAccess.Service.RequestStatusService;

namespace Znode.Engine.Services
{
    public class RequestStatusService : BaseService, IRequestStatusService
    {
        #region Private Variables
        private readonly RequestStatusRepository _requestStatusRepository; 
        #endregion

        #region Constructor
        public RequestStatusService()
        {
            _requestStatusRepository = new RequestStatusRepository();
        } 
        #endregion

        #region Public Methods
        public RequestStatusListModel GetRequestStatusList(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new RequestStatusListModel();
            var requestStatusList = new TList<RequestStatus>();

            var query = new RequestStatusQuery();
            var sortBuilder = new RequestStatusSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            requestStatusList = _requestStatusRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (pagingLength.Equals(int.MaxValue))
            {
                model.PageSize = model.TotalResults;
            }

            // Map each item and add to the list
            foreach (var requestStatus in requestStatusList)
            {
                model.RequestStatusList.Add(RequestStatusMap.ToModel(requestStatus));
            }

            return model;
        }

        public RequestStatusModel GetRequestStatus(int requestStatusId, NameValueCollection expands)
        {
            var requestStatus = _requestStatusRepository.GetByRequestStatusID(requestStatusId);

            if (!Equals(requestStatus, null))
            {
                return RequestStatusMap.ToModel(requestStatus);
            }
            return null;
        }

        public RequestStatusModel CreateRequestStatus(RequestStatusModel model)
        {
            if (Equals(model, null))
            {
                throw new Exception("RequestStatus model cannot be null.");
            }

            var entity = RequestStatusMap.ToEntity(model);
            var requestStatus = _requestStatusRepository.Save(entity);
            return RequestStatusMap.ToModel(requestStatus);
        }

        public RequestStatusModel UpdateRequestStatus(int requestStatusId, RequestStatusModel model)
        {
            if (requestStatusId < 1)
            {
                throw new Exception("RequestStatus ID cannot be less than 1.");
            }

            if (Equals(model, null))
            {
                throw new Exception("ReasonForReturn model cannot be null.");
            }

            var requestStatus = _requestStatusRepository.GetByRequestStatusID(requestStatusId);
            if (!Equals(requestStatus, null))
            {
                // Set profileId and update details.
                model.RequestStatusID = requestStatusId;
                var reasonForReturnToUpdate = RequestStatusMap.ToEntity(model);

                var updated = _requestStatusRepository.Update(reasonForReturnToUpdate);
                if (updated)
                {
                    requestStatus = _requestStatusRepository.GetByRequestStatusID(requestStatusId);
                    return RequestStatusMap.ToModel(requestStatus);
                }
            }
            return null;
        }

        public bool DeleteRequestStatus(int requestStatusId)
        {
            if (requestStatusId < 1)
            {
                throw new Exception("RequestStatus ID cannot be less than 1.");
            }

            var requestStatus = _requestStatusRepository.GetByRequestStatusID(requestStatusId);
            if (!Equals(requestStatus, null))
            {
                return _requestStatusRepository.Delete(requestStatus);
            }
            return false;
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Sets filter for Request status.
        /// </summary>
        /// <param name="filters">Filter parameters.</param>
        /// <param name="query">Request status query.</param>
        private void SetFiltering(List<Tuple<string, string, string>> filters, RequestStatusQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;
                if (filterKey.Equals(FilterKeys.IsActive)) { SetQueryParameter(RequestStatusColumn.IsEnabled, filterOperator, filterValue, query); }
                if (filterKey.Equals(FilterKeys.RequestStatusId)) { SetQueryParameter(RequestStatusColumn.RequestStatusID, filterOperator, filterValue, query); }
                if (filterKey.Equals(FilterKeys.Name)) { SetQueryParameter(RequestStatusColumn.Name, filterOperator, filterValue, query); }
            }
        }

        /// <summary>
        /// Sets sorting of request status list.
        /// </summary>
        /// <param name="sorts">Sorts the request status list.</param>
        /// <param name="sortBuilder">Sort builder for Request status entities.</param>
        private void SetSorting(NameValueCollection sorts, RequestStatusSortBuilder sortBuilder)
        {
            if (sorts.HasKeys())
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);
                    if (key.Equals(SortKeys.Name)) { SetSortDirection(RequestStatusColumn.Name, value, sortBuilder); }
                }
            }
        } 
        #endregion
    }
}
