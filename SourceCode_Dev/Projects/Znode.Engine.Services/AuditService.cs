﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using AuditRepository = ZNode.Libraries.DataAccess.Service.SourceModificationAuditService;
using AuditSourceTypeRepository = ZNode.Libraries.DataAccess.Service.SourceTypeService;
using AuditStatusRepository = ZNode.Libraries.DataAccess.Service.SourceModificationStatusService;
using AuditTypeRepository = ZNode.Libraries.DataAccess.Service.SourceModificationTypeService;
using ProductRepository = ZNode.Libraries.DataAccess.Service.ProductService;
using CategoryRepository = ZNode.Libraries.DataAccess.Service.CategoryService;
using CatalogRepository = ZNode.Libraries.DataAccess.Service.CatalogService;
using PortalRepository = ZNode.Libraries.DataAccess.Service.PortalService;

namespace Znode.Engine.Services
{
	public class AuditService : BaseService, IAuditService
	{
		private readonly AuditRepository _auditRepository;
		private readonly AuditSourceTypeRepository _auditSourceTypeRepository;
		private readonly AuditStatusRepository _auditStatusRepository;
		private readonly AuditTypeRepository _auditTypeRepository;
        private readonly ProductRepository _productRepository;
        private readonly CategoryRepository _categoryRepository;
        private readonly CatalogRepository _catalogRepository;
        private readonly PortalRepository _portalRepository;

		public AuditService()
		{
			_auditRepository = new AuditRepository();
			_auditSourceTypeRepository = new AuditSourceTypeRepository();
			_auditStatusRepository = new AuditStatusRepository();
			_auditTypeRepository = new AuditTypeRepository();
		    _productRepository = new ProductRepository();
            _categoryRepository = new CategoryRepository();
            _catalogRepository = new CatalogRepository();
            _portalRepository = new PortalRepository();
        }

		public AuditModel GetAudit(long auditId, NameValueCollection expands)
		{
			var audit = _auditRepository.GetById(auditId);
			if (audit != null)
			{
				GetExpands(expands, audit);
			}

			return AuditMap.ToModel(audit);
		}

		public AuditListModel GetAudits(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
		{
			var model = new AuditListModel();
			var audits = new TList<SourceModificationAudit>();
			var tempList = new TList<SourceModificationAudit>();

			var query = new SourceModificationAuditQuery();
			var sortBuilder = new SourceModificationAuditSortBuilder();
			var pagingStart = 0;
			var pagingLength = 0;

			SetFiltering(filters, query);
			SetSorting(sorts, sortBuilder);
			SetPaging(page, model, out pagingStart, out pagingLength);

			// Get the initial set
			var totalResults = 0;
			tempList = _auditRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
			model.TotalResults = totalResults;

			// Check to set page size equal to the total number of results
			if (pagingLength == int.MaxValue)
			{
				model.PageSize = model.TotalResults;
			}

			// Now go through the list and get any expands
			foreach (var audit in tempList)
			{
				GetExpands(expands, audit);
				audits.Add(audit);
			}

			// Map each item and add to the list
			foreach (var a in audits)
			{
				model.Audits.Add(AuditMap.ToModel(a));
			}

			return model;
		}

		public AuditModel UpdateAudit(long auditId, AuditModel model)
		{
			if (auditId < 1)
			{
				throw new Exception("Audit ID cannot be less than 1.");
			}

			if (model == null)
			{
				throw new Exception("Audit model cannot be null.");
			}

			var audit = _auditRepository.GetById(auditId);
			if (audit != null)
			{
				// Set audit ID
				model.AuditId = auditId;

				var auditToUpdate = AuditMap.ToEntity(model);

				var updated = _auditRepository.Update(auditToUpdate);
				if (updated)
				{
					audit = _auditRepository.GetById(auditId);
					return AuditMap.ToModel(audit);
				}
			}

			return null;
		}

		public bool DeleteAudit(long auditId)
		{
			if (auditId < 1)
			{
				throw new Exception("Audit ID cannot be less than 1.");
			}

			var audit = _auditRepository.GetById(auditId);
			if (audit != null)
			{
				return _auditRepository.Delete(audit);
			}

			return false;
		}

		private void GetExpands(NameValueCollection expands, SourceModificationAudit audit)
		{
			if (expands.HasKeys())
			{
				ExpandAuditSourceType(expands, audit);
				ExpandAuditStatus(expands, audit);
				ExpandAuditType(expands, audit);
                ExpandAuditProduct(expands, audit);
                ExpandAuditCategory(expands, audit);
                ExpandAuditCatalog(expands, audit);
                ExpandAuditPortal(expands, audit);
            }
		}

		private void ExpandAuditSourceType(NameValueCollection expands, SourceModificationAudit audit)
		{
			if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.AuditSourceType)))
			{
				var sourceType = _auditSourceTypeRepository.GetById(audit.SourceTypeId);
				if (sourceType != null)
				{
					audit.SourceTypeIdSource = sourceType;
				}
			}
		}

		private void ExpandAuditStatus(NameValueCollection expands, SourceModificationAudit audit)
		{
			if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.AuditStatus)))
			{
				var auditStatus = _auditStatusRepository.GetById(audit.StatusId);
				if (auditStatus != null)
				{
					audit.StatusIdSource = auditStatus;
				}
			}
		}

		private void ExpandAuditType(NameValueCollection expands, SourceModificationAudit audit)
		{
			if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.AuditType)))
			{
				var auditType = _auditTypeRepository.GetById(audit.ModificationTypeId);
				if (auditType != null)
				{
					audit.ModificationTypeIdSource = auditType;
				}
			}
		}

        private void ExpandAuditProduct(NameValueCollection expands, SourceModificationAudit audit)
        {
            //audit.SourceTypeId==1 means product
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Product)) && audit.SourceTypeId==1)
            {
                var product = _productRepository.GetByProductID((int)audit.SourceId);
                if (product != null)
                {
                    audit.Product = product;
                }
            }
        }

        private void ExpandAuditCategory(NameValueCollection expands, SourceModificationAudit audit)
        {
            //audit.SourceTypeId==2 means category
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Categories)) && audit.SourceTypeId == 2)
            {
                var category = _categoryRepository.GetByCategoryID((int)audit.SourceId);
                if (category != null)
                {
                    audit.Category = category;
                }
            }
        }

        private void ExpandAuditPortal(NameValueCollection expands, SourceModificationAudit audit)
        {
            //audit.SourceTypeId==3 means portal
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Portal)) && audit.SourceTypeId == 3)
            {
                var portal = _portalRepository.GetByPortalID((int)audit.SourceId);
                if (portal != null)
                {
                    audit.Portal = portal;
                }
            }
        }

        private void ExpandAuditCatalog(NameValueCollection expands, SourceModificationAudit audit)
        {
            //audit.SourceTypeId==4 means catalog
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Catalogs)) && audit.SourceTypeId == 4)
            {
                var catalog = _catalogRepository.GetByCatalogID((int)audit.SourceId);
                if (catalog != null)
                {
                    audit.Catalog = catalog;
                }
            }
        }

        private void SetFiltering(List<Tuple<string, string, string>> filters, SourceModificationAuditQuery query)
		{
			if (filters != null)
			{
				foreach (var tuple in filters)
				{
					var filterKey = tuple.Item1;
					var filterOperator = tuple.Item2;
					var filterValue = tuple.Item3;

					if (filterKey == FilterKeys.CreateDate) SetQueryParameter(SourceModificationAuditColumn.CreatedDate, filterOperator, filterValue, query);
					if (filterKey == FilterKeys.CreatedBy) SetQueryParameter(SourceModificationAuditColumn.CreatedBy, filterOperator, filterValue, query);
					if (filterKey == FilterKeys.ExternalId) SetQueryParameter(SourceModificationAuditColumn.ExternalId, filterOperator, filterValue, query);
					if (filterKey == FilterKeys.ModificationTypeId) SetQueryParameter(SourceModificationAuditColumn.ModificationTypeId, filterOperator, filterValue, query);
					if (filterKey == FilterKeys.ProcessedDate) SetQueryParameter(SourceModificationAuditColumn.ProcessedDate, filterOperator, filterValue, query);
					if (filterKey == FilterKeys.SourceId) SetQueryParameter(SourceModificationAuditColumn.SourceId, filterOperator, filterValue, query);
					if (filterKey == FilterKeys.SourceTypeId) SetQueryParameter(SourceModificationAuditColumn.SourceTypeId, filterOperator, filterValue, query);
					if (filterKey == FilterKeys.StatusId) SetQueryParameter(SourceModificationAuditColumn.StatusId, filterOperator, filterValue, query);
				}
			}
		}

		private void SetSorting(NameValueCollection sorts, SourceModificationAuditSortBuilder sortBuilder)
		{
			if (sorts.HasKeys())
			{
				foreach (var key in sorts.AllKeys)
				{
					var value = sorts.Get(key);

					if (key == SortKeys.AuditId) SetSortDirection(SourceModificationAuditColumn.Id, value, sortBuilder);
					if (key == SortKeys.CreateDate) SetSortDirection(SourceModificationAuditColumn.CreatedDate, value, sortBuilder);
					if (key == SortKeys.ExternalId) SetSortDirection(SourceModificationAuditColumn.ExternalId, value, sortBuilder);
					if (key == SortKeys.ModificationTypeId) SetSortDirection(SourceModificationAuditColumn.ModificationTypeId, value, sortBuilder);
					if (key == SortKeys.ProcessedDate) SetSortDirection(SourceModificationAuditColumn.ProcessedDate, value, sortBuilder);
					if (key == SortKeys.SourceId) SetSortDirection(SourceModificationAuditColumn.SourceId, value, sortBuilder);
					if (key == SortKeys.SourceTypeId) SetSortDirection(SourceModificationAuditColumn.SourceTypeId, value, sortBuilder);
				}
			}
		}

		private void SetQueryParameter(SourceModificationAuditColumn column, string filterOperator, string filterValue, SourceModificationAuditQuery query)
		{
			base.SetQueryParameter(column, filterOperator, filterValue, query);
		}

		private void SetSortDirection(SourceModificationAuditColumn column, string value, SourceModificationAuditSortBuilder sortBuilder)
		{
			base.SetSortDirection(column, value, sortBuilder);
		}
	}
}
