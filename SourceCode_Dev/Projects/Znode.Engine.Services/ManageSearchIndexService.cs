﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Search.LuceneSearchProvider.Indexer.Services;

namespace Znode.Engine.Services
{
    public class ManageSearchIndexService : BaseService, IManageSearchIndexService
    {

        public LuceneIndexListModel GetLuceneIndexStatus(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new LuceneIndexListModel();
            int totalCount = 0;
            var pagingStart = 0;
            var pagingLength = 0;
            pagingStart = Convert.ToInt32(page.Get(PageKeys.Index));
            pagingLength = Convert.ToInt32(page.Get(PageKeys.Size));
            LuceneIndexMonitorService luceneIndexerStatusService = new LuceneIndexMonitorService();

            var Tlist = luceneIndexerStatusService.GetPaged("", "LuceneIndexMonitorID DESC", pagingStart, pagingLength, out totalCount);

            model.LuceneIndexList = ToListModel(Tlist).LuceneIndexList;
           
            


            //Get States of service and trigger
            LuceneIndexService luceneIndexService = new LuceneIndexService();
            if (!Equals(model, null) && model.LuceneIndexList.Count > 0)
            {

                model.LuceneIndexList[0].PageIndex = pagingStart;
                model.LuceneIndexList[0].PageSize = pagingLength;
                model.LuceneIndexList[0].TotalResults = totalCount;
                
                // check to set page size equal to the total number of results
                if (pagingLength == int.MaxValue)
                {
                    model.LuceneIndexList[0].PageSize = model.TotalResults;
                }

                model.LuceneIndexList[0].TriggerFlag = luceneIndexService.IsLuceneTriggersDisabled();

                model.LuceneIndexList[0].ServiceFlag = luceneIndexService.IsLucceneWinserviceDisable();
            }
            return model;
        }

        public bool CreateIndex()
        {
            LuceneIndexService luceneIndexService = new LuceneIndexService();
            luceneIndexService.CreateLuceneIndexButton(0);
            return true;
        }

        public int DisableTriggers(int flag)
        {
            LuceneIndexService luceneIndexService = new LuceneIndexService();
            luceneIndexService.SwitchLuceneTrigger(flag);
            return flag;
        }

        public int DisableWinService(int flag)
        {
            int intFlag = 0;
            var luceneIndexService = new LuceneIndexService();

            intFlag = luceneIndexService.IsLucceneWinserviceDisable();
            luceneIndexService.SwitchWindowsService(intFlag);
            return intFlag;
        }

        public LuceneIndexServerListModel GetIndexServerStatus(int indexId)
        {
            LuceneIndexService luceneIndexService = new LuceneIndexService();

            var model = luceneIndexService.GetIndexServerStatus(indexId);

            return ToServerListModel(model);
        }

        #region Convert To model

        private LuceneIndexListModel ToListModel(TList<LuceneIndexMonitor> model)
        {
            LuceneIndexListModel indexModel = new LuceneIndexListModel();

            model.ForEach(x =>
            {
                indexModel.LuceneIndexList.Add(new LuceneIndexModel()
                {
                    LuceneIndexMonitorId = x.LuceneIndexMonitorID,
                    SourceId = x.SourceID,
                    SourceType = x.SourceType,
                    SourceTransationType = x.SourceTransactionType,
                    TransationDateTime = x.TransactionDateTime,
                    IsDuplicate = !Equals(x.IsDuplicate, null) ? (bool)x.IsDuplicate : false,
                    AffectedType = x.AffectedType,
                    IndexerStatusChangedBy = !Equals(x.IndexerStatusChangedBy) ? x.IndexerStatusChangedBy : null,
                });
            });
            return indexModel;
        }

        private LuceneIndexServerListModel ToServerListModel(List<LuceneIndexServerStatus> model)
        {
            LuceneIndexServerListModel listModel = new LuceneIndexServerListModel();
            if (!Equals(model, null))
            {

                model.ForEach(x =>
                {
                    listModel.IndexServerStatusList.Add(new LuceneIndexServerModel()
                    {
                        ServerName = x.ServerName,
                        Status = Convert.ToInt32(x.Status),
                        StartTime = Convert.ToDateTime(x.StartTime),
                        EndTime = Convert.ToDateTime(x.EndTime)
                    });
                });

            }
            return listModel;
        }
        #endregion
    }
}
