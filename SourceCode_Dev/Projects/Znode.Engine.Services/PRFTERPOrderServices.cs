﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Znode.Engine.Api.Models;
using ZNode.Libraries.ECommerce.Fulfillment;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.ECommerce.Utilities;
using PRFT.Engine.ERP.OrderService;
using System.Configuration;
using Znode.Engine.Services.Maps;
using ZNode.Libraries.DataAccess.Custom;

namespace Znode.Engine.Services
{
    public class PRFTERPOrderServices : IPRFTERPOrderServices
    {
        private readonly ERPOrderService service;

        public PRFTERPOrderServices()
        {
            service = new ERPOrderService();
        }

        public PRFTERPOrderResponseModel SubmitOrderToERP(ZNodeOrderFulfillment order, ZNodePortalCart shoppingCart)
        {
            try
            {
                PRFTERPOrderResponseModel model = new PRFTERPOrderResponseModel();

                //Get the Request Object
                ERPOrderServicesRequest request = GetOrderRequestValues(order); 
                
                //Call ERP system to submit order
                ERPOrderServicesResponse response = service.SubmitOrderToERP(request);

                model.StatusDescription =  response.StatusDescription;
                model.StatusCode = response.StatusCode;
                model.HasError = response.HasError;

                if (response.HasError == false && !string.IsNullOrEmpty(response.NewOrderID))
                {
                    model.NewOrderID = response.NewOrderID;
                    Order newOrderDetails = GetOrderDetailsByID(response.NewOrderID);
                    if(newOrderDetails!=null && !string.IsNullOrEmpty(newOrderDetails.OrderNumber))
                    {
                        model.NewOrderID = newOrderDetails.OrderNumber;
                    }
                }
                else
                {
                    string logErrorMessae = string.Format("Order ID: {0} Message : ", order.OrderID.ToString(), model.StatusDescription);
                    ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.OrderSubmissionFailed, "ERP ORDER SUBMISSION FAILD", string.Empty, null, logErrorMessae, null);

                    StringBuilder sbError = new StringBuilder();
                    sbError.Append("Error on PRFTERPOrderService ==> SubmitOrderToERP ");
                    sbError.Append("OrderID=");
                    sbError.Append(order.OrderID);
                    sbError.Append(" ");
                    sbError.Append("MessageText=");
                    sbError.Append(model.StatusDescription);
                    sbError.Append(" ");
                    sbError.Append(" ");
                    ZNodeLogging.LogMessage(sbError.ToString());
                }
                return model;
            }
            catch (Exception ex)
            {
                StringBuilder sbError = new StringBuilder();
                sbError.Append("Error on PRFTERPOrderService ==> SubmitOrderToERP ");
                sbError.Append("OrderID=");
                sbError.Append(order.OrderID);
                sbError.Append(" ");
                ZNodeLogging.LogMessage(sbError.ToString() + ex.ToString()+"\n\rStack Trace=>"+ex.StackTrace.ToString());
            }
            return null;
        }

        private Order GetOrderDetailsByID(string newOrderID)
        {
            try
            {
                Order newOrderDetails = service.GetOrderDetailsByID(newOrderID);

                if (newOrderDetails.ServiceResponse.HasError == true || string.IsNullOrEmpty(newOrderDetails.OrderNumber))
                {
                    
                    string logErrorMessae = string.Format("Order ID: {0} Message : ", newOrderID, newOrderDetails.ServiceResponse.StatusDescription);
                    ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.GeneralError, "Getting Order Details from ERP FAILD", string.Empty, null, logErrorMessae, null);

                    StringBuilder sbError = new StringBuilder();
                    sbError.Append("Error on PRFTERPOrderService ==> GetOrderDetailsByID ");
                    sbError.Append("OrderID=");
                    sbError.Append(newOrderID);
                    sbError.Append(" ");
                    sbError.Append("MessageText=");
                    sbError.Append(newOrderDetails.ServiceResponse.StatusDescription);
                    sbError.Append(" ");
                    sbError.Append(" ");
                    ZNodeLogging.LogMessage(sbError.ToString());
                }
                return newOrderDetails;
            }
            catch (Exception ex)
            {
                StringBuilder sbError = new StringBuilder();
                sbError.Append("Error on PRFTERPOrderService ==> GetOrderDetailsByID ");
                sbError.Append("OrderID=");
                sbError.Append(newOrderID);
                sbError.Append(" ");
                ZNodeLogging.LogMessage(sbError.ToString() + ex.ToString());
            }
            return null;
        }

        private ERPOrderServicesRequest GetOrderRequestValues(ZNodeOrderFulfillment order)
        {
            ERPOrderServicesRequest request = new ERPOrderServicesRequest();

            //Order Header
            OEOrderHeader header = new OEOrderHeader();

            string billToName = string.Format("{0} {1}", order.BillingAddress.FirstName, order.BillingAddress.LastName);
            billToName = billToName.ToUpper();

            header.BillToAddressLine1 = string.IsNullOrEmpty(order.BillingAddress.Street) ? "." : order.BillingAddress.Street;
            header.BillToAddressLine1 = header.BillToAddressLine1.ToUpper();

            header.BillToAddressLine2 = string.IsNullOrEmpty(order.BillingAddress.Street1)?".": order.BillingAddress.Street1;
            header.BillToAddressLine2 = header.BillToAddressLine2.ToUpper();

            header.BillToAddressLine3 = string.Empty;
            header.BillToAddressLine4 = string.Format("{0}, {1} {2}",order.BillingAddress.City, order.BillingAddress.StateCode, order.BillingAddress.PostalCode);
            header.BillToAddressLine4 = header.BillToAddressLine4.ToUpper();

            header.BillToCountry = order.BillingAddress.CountryCode;

            header.BillToCustomerName = (string.IsNullOrEmpty(billToName.Trim()))?order.BillingAddress.CompanyName:billToName;
            header.BillToCustomerName = header.BillToCustomerName.ToUpper();

            header.CustomerNumber = order.Custom1;
            header.OrderDate = order.OrderDate.ToString();
            header.OrderType = Convert.ToString(ConfigurationManager.AppSettings["ERPOrderType"]);
            header.PurchaseOrderNumber = order.PurchaseOrderNumber;
            header.ShipInstruction1 = order.AdditionalInstructions;

            header.ShipToAddressLine1 = string.IsNullOrEmpty(order.ShippingAddress.Street)?".": order.ShippingAddress.Street;
            header.ShipToAddressLine1 = header.ShipToAddressLine1.ToUpper();

            header.ShipToAddressLine2 = string.IsNullOrEmpty(order.ShippingAddress.Street1) ? "." : order.ShippingAddress.Street1;
            header.ShipToAddressLine2 = header.ShipToAddressLine2.ToUpper();

            header.ShipToAddressLine3 = string.Empty;
            header.ShipToAddressLine4 = string.Format("{0}, {1} {2}", order.ShippingAddress.City, order.ShippingAddress.StateCode, order.ShippingAddress.PostalCode);
            header.ShipToAddressLine4 = header.ShipToAddressLine4.ToUpper();

            header.ShipToCountry = order.ShippingAddress.CountryCode;

            string shipToName = string.Format("{0} {1}", order.ShippingAddress.FirstName, order.ShippingAddress.LastName);
            header.ShipToName = (string.IsNullOrEmpty(shipToName.Trim())) ? order.ShippingAddress.CompanyName : shipToName;
            header.ShipToName = header.ShipToName.ToUpper();

            header.UserDefinedField1 = string.Format("{0}{1}", Convert.ToString(ConfigurationManager.AppSettings["ERPOrderPrefix"]),order.OrderID);
            header.UserDefinedField2 = Convert.ToString(order.ShippingCost);
            header.UserDefinedField3 = Convert.ToString(order.TaxCost);

            //Order Line Items
            OEOrderLines items = new OEOrderLines();
            items.OrderLineItems = new List<OEOrderLine>();
            OEOrderLine item;
            int lineItemsCount = order.OrderLineItems.Count;
            for(int i=0;i< lineItemsCount; i++) 
            {
                var tmpItem = order.OrderLineItems[i];
                item = new OEOrderLine();
                item.ItemNo = tmpItem.ProductNum;
                //item.ItemDescription1 = tmpItem.Name;
                item.QuantityOrdered = Convert.ToString(tmpItem.Quantity);
                item.UnitPrice = Convert.ToString(tmpItem.Price);
                item.UserDefinedField1 = string.Format("{0}{1}", Convert.ToString(ConfigurationManager.AppSettings["ERPOrderPrefix"]), order.OrderID);
                item._OrderComplete = (i < lineItemsCount - 1) ? "false" : "true"; //Set true for the last items

                items.OrderLineItems.Add(item);
            }

            request.OrderHeader = header;
            request.LineItems = items;

            return request;
        }


        #region Order Invoice
        /// <summary>
        /// Get the order history details from ERP
        /// </summary>
        /// <param name="erpOrderNumber"> ERP Order number</param>
        /// <returns></returns>
        public PRFTOEHeaderHistoryModel GetOrderHistory(string erpOrderNumber)
        {
            try
            {
                PRFTOEHeaderHistoryModel orderInvoiceModel = null;
                OrderInvoice orderInvoice = service.GetOrderHistory(erpOrderNumber);

                if (orderInvoice!=null && orderInvoice.HasError == false && orderInvoice.InvoiceHeaderDetails!=null && !string.IsNullOrEmpty(orderInvoice.InvoiceHeaderDetails.InvoiceNumber))
                {
                    //Get Invoice list items
                    orderInvoice.InvoiceItems = GetOrderInvoiceItems(orderInvoice.InvoiceHeaderDetails.InvoiceNumber);

                    PRFTInvoiceHelper invoiceHelper = new PRFTInvoiceHelper();
                    if (!string.IsNullOrEmpty(orderInvoice.InvoiceHeaderDetails.ShipViaCode))
                    {
                        string shipviaDescription = invoiceHelper.GetShipviaDetails(orderInvoice.InvoiceHeaderDetails.ShipViaCode);
                        if (!string.IsNullOrEmpty(shipviaDescription))
                        {
                            orderInvoice.InvoiceHeaderDetails.ShipViaCode = shipviaDescription;
                        }
                    }

                    if (!string.IsNullOrEmpty(orderInvoice.InvoiceHeaderDetails.AccountsReceivableTermsCode))
                    {
                        string paymentTerms = invoiceHelper.GetPaymentTermsDetails(orderInvoice.InvoiceHeaderDetails.AccountsReceivableTermsCode);
                        if (!string.IsNullOrEmpty(paymentTerms))
                        {
                            orderInvoice.InvoiceHeaderDetails.AccountsReceivableTermsCode = paymentTerms;
                        }
                    }
                    
                    //Map the data into required fields
                    orderInvoiceModel = PRFTOEHeaderHistoryMap.ToModel(orderInvoice);

                    return orderInvoiceModel;
                }
                else
                {
                    string logErrorMessae = string.Format("ERP Order ID: {0} Message : ", erpOrderNumber, orderInvoice.StatusDescription);
                    ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.GeneralError, "Getting Order Invoice Details from ERP FAILD", string.Empty, null, logErrorMessae, null);

                    StringBuilder sbError = new StringBuilder();
                    sbError.Append("Error on PRFTERPOrderService ==> GetOrderHistory ");
                    sbError.Append("ERP OrderID=");
                    sbError.Append(erpOrderNumber);
                    sbError.Append(" ");
                    sbError.Append("MessageText=");
                    sbError.Append(orderInvoice.StatusDescription);
                    sbError.Append(" ");
                    sbError.Append(" ");
                    ZNodeLogging.LogMessage(sbError.ToString());
                }
                
            }
            catch (Exception ex)
            {
                StringBuilder sbError = new StringBuilder();
                sbError.Append("Error on PRFTERPOrderService ==> GetOrderHistory ");
                sbError.Append("ERP OrderID=");
                sbError.Append(erpOrderNumber);
                sbError.Append(" ");
                ZNodeLogging.LogMessage(sbError.ToString() + ex.ToString());
            }
            return null;
        }
        

        /// <summary>
        /// Get the order history details from ERP
        /// </summary>
        /// <param name="erpOrderNumber"> ERP Order number</param>
        /// <returns></returns>
        public List<ERPLineHistory> GetOrderInvoiceItems(string invoiceNumber)
        {
            try
            {
                List<ERPLineHistory> items = new List<ERPLineHistory>();
                
                OrderInvoiceLineItems orderInvoiceLineItems = service.GetOrderLineHistory(invoiceNumber);

                if (orderInvoiceLineItems!=null && orderInvoiceLineItems.HasError == false && orderInvoiceLineItems.InvoiceItems!=null && orderInvoiceLineItems.InvoiceItems.Count>0)
                {
                    //Map the data into required fields
                    items = orderInvoiceLineItems.InvoiceItems;
                    return items;
                }
                else
                {
                    string logErrorMessae = string.Format("ERP Invoice ID: {0} Message : ", invoiceNumber, orderInvoiceLineItems.StatusDescription);
                    ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.GeneralError, "Getting Order Invoice line item Details from ERP FAILD", string.Empty, null, logErrorMessae, null);

                    StringBuilder sbError = new StringBuilder();
                    sbError.Append("Error on PRFTERPOrderService ==> GetOrderLineHistory ");
                    sbError.Append("ERP Invoice Number=");
                    sbError.Append(invoiceNumber);
                    sbError.Append(" ");
                    sbError.Append("MessageText=");
                    sbError.Append(orderInvoiceLineItems.StatusDescription);
                    sbError.Append(" ");
                    sbError.Append(" ");
                    ZNodeLogging.LogMessage(sbError.ToString());
                }

            }
            catch (Exception ex)
            {
                StringBuilder sbError = new StringBuilder();
                sbError.Append("Error on PRFTERPOrderService ==> GetOrderLineHistory ");
                sbError.Append("ERP OrderID=");
                sbError.Append(invoiceNumber);
                sbError.Append(" ");
                ZNodeLogging.LogMessage(sbError.ToString() + ex.ToString());
            }
            return null;
        }

        #endregion

        #region PRFT Commented Code
        
        
        public PRFTERPOrderResponseModel ReSubmitOrderToERP(OrderModel order)
        {
            try
            {
                PRFTERPOrderResponseModel model = new PRFTERPOrderResponseModel();

                //Get the Request Object
                ERPOrderServicesRequest request = GetOrderRequestValues(order);

                //Call ERP system to submit order
                ERPOrderServicesResponse response = service.SubmitOrderToERP(request);

                model.StatusDescription = response.StatusDescription;

                string[] requiredMessage = response.StatusDescription.Split(new string[] { "<br/>" }, StringSplitOptions.None);
                if(requiredMessage.Length>0)
                {
                    model.StatusDescription = requiredMessage[0];
                }
                
                model.StatusCode = response.StatusCode;
                model.HasError = response.HasError;

                if (response.HasError == false && !string.IsNullOrEmpty(response.NewOrderID))
                {
                    model.NewOrderID = response.NewOrderID;
                    Order newOrderDetails = GetOrderDetailsByID(response.NewOrderID);
                    if (newOrderDetails != null && !string.IsNullOrEmpty(newOrderDetails.OrderNumber))
                    {
                        model.NewOrderID = newOrderDetails.OrderNumber;
                    }
                }
                else
                {
                    string logErrorMessae = string.Format("Order ID: {0} Message : ", order.OrderId.ToString(), response.StatusDescription);
                    ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.OrderSubmissionFailed, "ERP ORDER SUBMISSION FAILD", string.Empty, null, logErrorMessae, null);

                    StringBuilder sbError = new StringBuilder();
                    sbError.Append("Error on PRFTERPOrderService ==> ReSubmitOrderToERP ");
                    sbError.Append("OrderID=");
                    sbError.Append(order.OrderId);
                    sbError.Append(" ");
                    sbError.Append("MessageText=");
                    sbError.Append(response.StatusDescription);
                    sbError.Append(" ");
                    sbError.Append(" ");
                    ZNodeLogging.LogMessage(sbError.ToString());
                }
                return model;
            }
            catch (Exception ex)
            {
                StringBuilder sbError = new StringBuilder();
                sbError.Append("Error on PRFTERPOrderService ==> ReSubmitOrderToERP ");
                sbError.Append("OrderID=");
                sbError.Append(order.OrderId);
                sbError.Append(" ");
                ZNodeLogging.LogMessage(sbError.ToString() + ex.ToString());
            }
            return null;
        }

        private ERPOrderServicesRequest GetOrderRequestValues(OrderModel order)
        {
            ERPOrderServicesRequest request = new ERPOrderServicesRequest();

            //Order Header
            OEOrderHeader header = new OEOrderHeader();

            string billToName = string.Format("{0} {1}", order.BillingFirstName, order.BillingLastName);
            header.BillToAddressLine1 = string.IsNullOrEmpty(order.BillingStreet) ? "." : order.BillingStreet;
            header.BillToAddressLine2 = string.IsNullOrEmpty(order.BillingStreet1) ? "." : order.BillingStreet1;
            header.BillToAddressLine3 = string.Empty;
            header.BillToAddressLine4 = string.Format("{0}, {1} {2}", order.BillingCity, order.BillingStateCode, order.BillingPostalCode);
            header.BillToCountry = order.BillingCountryCode;
            header.BillToCustomerName = (string.IsNullOrEmpty(billToName.Trim())) ? order.BillingCompanyName : billToName;
            header.CustomerNumber = order.Custom1;
            header.OrderDate = order.OrderDate.ToString();
            header.OrderType = Convert.ToString(ConfigurationManager.AppSettings["ERPOrderType"]);
            header.PurchaseOrderNumber = order.PurchaseOrderNumber;
            header.ShipInstruction1 = order.AdditionalInstructions;

            header.ShipToAddressLine1 = string.IsNullOrEmpty(order.ShipToStreet) ? "." : order.ShipToStreet;
            header.ShipToAddressLine2 = string.IsNullOrEmpty(order.ShipToStreet1) ? "." : order.ShipToStreet1;
            header.ShipToAddressLine3 = string.Empty;
            header.ShipToAddressLine4 = string.Format("{0}, {1} {2}", order.ShipToCity, order.ShipToStateCode, order.ShipToPostalCode);
            header.ShipToCountry = order.ShipToCountryCode;

            string shipToName = string.Format("{0} {1}", order.ShipToFirstName, order.ShipToLastName);
            header.ShipToName = (string.IsNullOrEmpty(shipToName.Trim())) ? order.ShipToCompanyName : shipToName;

            header.UserDefinedField1 = string.Format("{0}{1}", Convert.ToString(ConfigurationManager.AppSettings["ERPOrderPrefix"]), order.OrderId);
            header.UserDefinedField2 = Convert.ToString(order.ShippingCost);
            header.UserDefinedField3 = Convert.ToString(order.TaxCost);

            //Order Line Items
            OEOrderLines items = new OEOrderLines();
            items.OrderLineItems = new List<OEOrderLine>();
            OEOrderLine item;
            int lineItemsCount = order.OrderLineItems.Count;
            for (int i = 0; i < lineItemsCount; i++)
            {
                var tmpItem = order.OrderLineItems[i];
                item = new OEOrderLine();
                item.ItemNo = tmpItem.ProductNum;
                //item.ItemDescription1 = tmpItem.Name;
                item.QuantityOrdered = Convert.ToString(tmpItem.Quantity);
                item.UnitPrice = Convert.ToString(tmpItem.Price);
                item.UserDefinedField1 = string.Format("{0}{1}", Convert.ToString(ConfigurationManager.AppSettings["ERPOrderPrefix"]), order.OrderId);
                item._OrderComplete = (i < lineItemsCount - 1) ? "false" : "true"; //Set true for the last items

                items.OrderLineItems.Add(item);
            }

            request.OrderHeader = header;
            request.LineItems = items;

            return request;
        }

        /*
        public PRFTOrderStatusResponseModel GetOrderStatus(string orderId)
        {
            PRFTOrderStatusResponseModel model = new PRFTOrderStatusResponseModel();

            var response = service.GetOrderStatus(orderId);

            model.OrderStatus = response.OrderStatus;
            model.OrderTrakingNumber = response.OrderTrakingNumber;
            model.ReturnCode = response.ReturnCode;
            model.MessageText = response.MessageText;

            return model;
        }
        */
        #endregion
    }
}
