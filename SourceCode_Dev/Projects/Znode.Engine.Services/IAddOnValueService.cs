﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    /// <summary>
    /// AddOn Value Service
    /// </summary>
    public interface IAddOnValueService
    {
        /// <summary>
        /// Create AddOn value
        /// </summary>
        /// <param name="model">AddOnValueModel model</param>
        /// <returns>Returns the model of AddOn value</returns>
        AddOnValueModel CreateAddOnValue(AddOnValueModel model);

        /// <summary>
        /// Delete AddOn value
        /// </summary>
        /// <param name="addOnValueId">int addOnValueId</param>
        /// <returns>Returns true or false</returns>
        bool DeleteAddOnValue(int addOnValueId);

        /// <summary>
        /// Update AddOn value
        /// </summary>
        /// <param name="addOnValueId">int addOnValueId</param>
        /// <param name="model">AddOnValueModel model</param>
        /// <returns>Returns the model of AddOn value</returns>
        AddOnValueModel UpdateAddOnValue(int addOnValueId, AddOnValueModel model);

        /// <summary>
        /// Get AddOn value By AddOn value id and expand
        /// </summary>
        /// <param name="AddOnValueId">int addOnValueId</param>
        /// <param name="expands">NameValueCollection expands</param>
        /// <returns>Returns the model of AddOn value</returns>
        AddOnValueModel GetAddOnValue(int addOnValueId, NameValueCollection expands);

        /// <summary>
        /// Get AddOn values associated with AddOn id
        /// </summary>
        /// <param name="expands">NameValueCollection expands</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">NameValueCollection sorts</param>
        /// <param name="page">NameValueCollection page</param>
        /// <returns>Returns list of AddOn value list model</returns>
        AddOnValueListModel GetAddOnValueByAddOnId(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
    }
}
