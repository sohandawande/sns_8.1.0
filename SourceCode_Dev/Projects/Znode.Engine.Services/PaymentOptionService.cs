﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using GatewayRepository = ZNode.Libraries.DataAccess.Service.GatewayService;
using PaymentSettingRepository = ZNode.Libraries.DataAccess.Service.PaymentSettingService;
using PaymentTypeRepository = ZNode.Libraries.DataAccess.Service.PaymentTypeService;
using ProfilesRepository = ZNode.Libraries.DataAccess.Service.ProfileService;
namespace Znode.Engine.Services
{
    public class PaymentOptionService : BaseService, IPaymentOptionService
    {
        private readonly GatewayRepository _gatewayRepository;
        private readonly PaymentSettingRepository _paymentSettingRepository;
        private readonly PaymentTypeRepository _paymentTypeRepository;
        private readonly ProfilesRepository _profilesRepository;
        public PaymentOptionService()
        {
            _gatewayRepository = new GatewayRepository();
            _paymentSettingRepository = new PaymentSettingRepository();
            _paymentTypeRepository = new PaymentTypeRepository();
            _profilesRepository = new ProfilesRepository();
        }

        public PaymentOptionModel GetPaymentOption(int paymentOptionId, NameValueCollection expands)
        {
            var paymentOption = _paymentSettingRepository.GetByPaymentSettingID(paymentOptionId);
            if (paymentOption != null)
            {
                GetExpands(expands, paymentOption);
            }

            return PaymentOptionMap.ToModel(paymentOption);
        }

        public PaymentOptionListModel GetPaymentOptions(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new PaymentOptionListModel();
            var paymentOptions = new TList<PaymentSetting>();
            var tempList = new TList<PaymentSetting>();

            var query = new PaymentSettingQuery();
            var sortBuilder = new PaymentSettingSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);
            if (query.Length > 0)
            {
                // Get the initial set
                var totalResults = 0;
                tempList = _paymentSettingRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
                model.TotalResults = totalResults;

                // Check to set page size equal to the total number of results
                if (pagingLength == int.MaxValue)
                {
                    model.PageSize = model.TotalResults;
                }

                // Now go through the list and get any expands
                foreach (var paymentOption in tempList)
                {
                    GetExpands(expands, paymentOption);
                    paymentOptions.Add(paymentOption);
                }

                // Gets the list of all profiles
                var profiles = _profilesRepository.GetAll();

                // Map each item and add to the list
                foreach (var p in paymentOptions)
                {
                    var payment = PaymentOptionMap.ToModel(p);
                    payment.ProfileName = Equals(p.ProfileID, null) ? "All Profiles" : profiles.Find(x => x.ProfileID.Equals(p.ProfileID)).Name;
                    model.PaymentOptions.Add(payment);
                }
            }
            else
            {
                model.PaymentOptions = new System.Collections.ObjectModel.Collection<PaymentOptionModel>();
            }
            return model;
        }


        public PaymentOptionModel CreatePaymentOption(PaymentOptionModel model)
        {
            if (model == null)
            {
                throw new Exception("Payment option model cannot be null.");
            }

            var paymenttypepeModel = _paymentSettingRepository.GetByPaymentTypeID(model.PaymentTypeId).Find(x => x.ProfileID.Equals(model.ProfileId) && x.GatewayTypeID.Equals(model.PaymentGatewayId));

            if (!Equals(paymenttypepeModel, null))
            {
                return new PaymentOptionModel() { IsExists = true };
            }
            var entity = PaymentOptionMap.ToEntity(model);
            var paymentOption = _paymentSettingRepository.Save(entity);

            //update status 
            if (!Equals(model.PaymentGatewayId, null))
            {
                var updatemodel = _paymentSettingRepository.GetByPaymentTypeID(model.PaymentTypeId);

                foreach (var x in updatemodel)
                {
                    if (x.ProfileID.Equals(model.ProfileId) && !Equals(x.PaymentSettingID,paymentOption.PaymentSettingID))
                    {
                        var optionModel = PaymentOptionMap.ToModel(x);
                        optionModel.IsActive = false;
                        UpdatePaymentOption(optionModel.PaymentOptionId, optionModel);
                    }
                }
            }

            return PaymentOptionMap.ToModel(paymentOption);


        }

        public PaymentOptionModel UpdatePaymentOption(int paymentOptionId, PaymentOptionModel model)
        {
            if (paymentOptionId < 1)
            {
                throw new Exception("Payment option ID cannot be less than 1.");
            }

            if (model == null)
            {
                throw new Exception("Payment option model cannot be null.");
            }

            var paymentOption = _paymentSettingRepository.GetByPaymentSettingID(paymentOptionId);
            if (paymentOption != null)
            {
                // Set the payment option ID
                model.PaymentOptionId = paymentOptionId;

                var paymentOptionToUpdate = PaymentOptionMap.ToEntity(model);

                var updated = _paymentSettingRepository.Update(paymentOptionToUpdate);
                if (updated)
                {
                    paymentOption = _paymentSettingRepository.GetByPaymentSettingID(paymentOptionId);

                    //update status 
                    if (model.IsActive)
                    {
                        if (!Equals(model.PaymentGatewayId, null))
                        {
                            var updatemodel = _paymentSettingRepository.GetByPaymentTypeID(model.PaymentTypeId);

                            foreach (var x in updatemodel)
                            {
                                if (x.ProfileID.Equals(model.ProfileId) && !Equals(x.PaymentSettingID, paymentOption.PaymentSettingID))
                                {
                                    var optionModel = PaymentOptionMap.ToModel(x);
                                    optionModel.IsActive = false;
                                    UpdatePaymentOption(optionModel.PaymentOptionId, optionModel);
                                }
                            }
                        }
                    }
                    return PaymentOptionMap.ToModel(paymentOption);
                }
            }

            return null;
        }

        public bool DeletePaymentOption(int paymentOptionId)
        {
            if (paymentOptionId < 1)
            {
                throw new Exception("Payment option ID cannot be less than 1.");
            }

            var paymentOption = _paymentSettingRepository.GetByPaymentSettingID(paymentOptionId);
            if (paymentOption != null)
            {
                return _paymentSettingRepository.Delete(paymentOption);
            }

            return false;
        }

        private void GetExpands(NameValueCollection expands, PaymentSetting paymentOption)
        {
            if (expands.HasKeys())
            {
                ExpandPaymentGateway(expands, paymentOption);
                ExpandPaymentType(expands, paymentOption);
            }
        }

        private void ExpandPaymentGateway(NameValueCollection expands, PaymentSetting paymentOption)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.PaymentGateway)))
            {
                if (paymentOption.GatewayTypeID.HasValue)
                {
                    var paymentGateway = _gatewayRepository.GetByGatewayTypeID(paymentOption.GatewayTypeID.Value);
                    if (paymentGateway != null)
                    {
                        paymentOption.GatewayTypeIDSource = paymentGateway;
                    }
                }
            }
        }

        private void ExpandPaymentType(NameValueCollection expands, PaymentSetting paymentOption)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.PaymentType)))
            {
                var paymentType = _paymentTypeRepository.GetByPaymentTypeID(paymentOption.PaymentTypeID);
                if (paymentType != null)
                {
                    paymentOption.PaymentTypeIDSource = paymentType;
                }
            }
        }

        private void SetFiltering(List<Tuple<string, string, string>> filters, PaymentSettingQuery query)
        {
            bool isProfileIdPresent = false;
            string UserName = string.Empty;
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;
                if (!Equals(filterKey, FilterKeys.UserName))
                {
                    if (filterKey.Equals(FilterKeys.ProfileId))
                    {
                        isProfileIdPresent = true;
                        SetQueryParameter(PaymentSettingColumn.ProfileID, filterOperator, filterValue, query);
                    }
                    if (filterKey == FilterKeys.IsActive) SetQueryParameter(PaymentSettingColumn.ActiveInd, filterOperator, filterValue, query);
                    if (filterKey == FilterKeys.Partner) SetQueryParameter(PaymentSettingColumn.Partner, filterOperator, filterValue, query);
                    if (filterKey == FilterKeys.PaymentGatewayId) SetQueryParameter(PaymentSettingColumn.PaymentSettingID, filterOperator, filterValue, query);
                    if (filterKey == FilterKeys.PaymentTypeId) SetQueryParameter(PaymentSettingColumn.PaymentTypeID, filterOperator, filterValue, query);
                    if (filterKey == FilterKeys.ProfileId) SetQueryParameter(PaymentSettingColumn.ProfileID, filterOperator, filterValue, query);
                    if (filterKey == FilterKeys.Vendor) SetQueryParameter(PaymentSettingColumn.Vendor, filterOperator, filterValue, query);
                    if (filterKey == FilterKeys.PaymentSettingID) SetQueryParameter(PaymentSettingColumn.PaymentSettingID, filterOperator, filterValue, query);
                }                
                else
                {
                    UserName = filterValue;
                }
            }
            if (!isProfileIdPresent)
            {
                string profileIds = GetAvailableProfiles(UserName);
                if (!string.IsNullOrEmpty(profileIds) && !Equals(profileIds, "0"))
                {
                    query.AppendIn(SqlUtil.AND, PaymentSettingColumn.ProfileID, profileIds.Split(','));
                    query.AppendIsNull(SqlUtil.OR, PaymentSettingColumn.ProfileID);
                }
            }
        }

        private void SetSorting(NameValueCollection sorts, PaymentSettingSortBuilder sortBuilder)
        {
            if (sorts.HasKeys() && sorts.Count > 1)
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);


                    if (value == SortKeys.DisplayOrder) SetSortDirection(PaymentSettingColumn.DisplayOrder, sorts["sortDir"], sortBuilder);
                    if (value == SortKeys.PaymentOptionId) SetSortDirection(PaymentSettingColumn.PaymentSettingID, sorts["sortDir"], sortBuilder);
                    if (value == "paymenttypename") SetSortDirection(PaymentSettingColumn.PaymentSettingID, sorts["sortDir"], sortBuilder);
                }
            }
            else
            {
                SetSortDirection(PaymentSettingColumn.PaymentSettingID, "asc", sortBuilder);
            }
        }




        private void SetQueryParameter(PaymentSettingColumn column, string filterOperator, string filterValue, PaymentSettingQuery query)
        {
            base.SetQueryParameter(column, filterOperator, filterValue, query);
        }

        private void SetSortDirection(PaymentSettingColumn column, string value, PaymentSettingSortBuilder sortBuilder)
        {
            base.SetSortDirection(column, value, sortBuilder);
        }
    }
}
