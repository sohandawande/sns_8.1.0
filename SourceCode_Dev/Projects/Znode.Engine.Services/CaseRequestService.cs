﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Helpers;
using Znode.Libraries.Helpers.Constants;
using Znode.Libraries.Helpers.Extensions;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;
using CaseNoteRepository = ZNode.Libraries.DataAccess.Service.NoteService;
using CasePriorityRepository = ZNode.Libraries.DataAccess.Service.CasePriorityService;
using CaseRequestRepository = ZNode.Libraries.DataAccess.Service.CaseRequestService;
using CaseStatusRepository = ZNode.Libraries.DataAccess.Service.CaseStatusService;
using CaseTypeRepository = ZNode.Libraries.DataAccess.Service.CaseTypeService;
using NoteAdminRepository = ZNode.Libraries.Admin.NoteAdmin;

namespace Znode.Engine.Services
{
    public class CaseRequestService : BaseService, ICaseRequestService
    {
        private readonly CasePriorityRepository _casePriorityRepository;
        private readonly CaseRequestRepository _caseRequestRepository;
        private readonly CaseStatusRepository _caseStatusRepository;
        private readonly CaseTypeRepository _caseTypeRepository;
        private readonly CaseNoteRepository _caseNoteRepository;        

        //PRFT Custom Code: Start
        public string apiUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);        
        //PRFT custom Code : End

        public CaseRequestService()
        {
            _casePriorityRepository = new CasePriorityRepository();
            _caseRequestRepository = new CaseRequestRepository();
            _caseStatusRepository = new CaseStatusRepository();
            _caseTypeRepository = new CaseTypeRepository();
            _caseNoteRepository = new CaseNoteRepository();            
        }

        public CaseRequestModel GetCaseRequest(int caseRequestId, NameValueCollection expands)
        {
            var caseHelper = new CaseHelper();
            var caseRequest = caseHelper.GetCaseRequestById(caseRequestId.ToString());

            CaseRequestModel model = caseRequest.Tables[0].Rows[0].ToSingleRow<CaseRequestModel>();
            return model;
        }

        public CaseRequestListModel GetCaseRequests(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new CaseRequestListModel();
            var caseRequests = new TList<CaseRequest>();
            var tempList = new TList<CaseRequest>();

            var query = new CaseRequestQuery();
            var sortBuilder = new CaseRequestSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            tempList = _caseRequestRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (Equals(pagingLength, int.MaxValue))
            {
                model.PageSize = model.TotalResults;
            }

            // Now go through the list and get any expands
            foreach (var caseRequest in tempList)
            {
                GetExpands(expands, caseRequest);
                caseRequests.Add(caseRequest);
            }

            // Map each item and add to the list
            foreach (var c in caseRequests)
            {
                model.CaseRequests.Add(CaseRequestMap.ToModel(c));
            }

            return model;
        }

        public CaseRequestModel CreateCaseRequest(CaseRequestModel model)
        {
            if (Equals(model, null))
            {
                throw new Exception("Case request model cannot be null.");
            }

            // Set the create date
            model.CreateDate = System.DateTime.Now;
            var entity = CaseRequestMap.ToEntity(model);

            var caseRequest = _caseRequestRepository.Save(entity);
            bool isInserted = false;//PRFT Custom Code : Start
            //To add note
            if (caseRequest.CaseID > 0)
            {
                NoteAdminRepository _noteAdminRepository = new NoteAdminRepository();
                Note _NoteAccess = new Note();

                _NoteAccess.CaseID = caseRequest.CaseID;
                _NoteAccess.AccountID = null;
                _NoteAccess.CreateDte = System.DateTime.Now;
                _NoteAccess.CreateUser = caseRequest.CreateUser;
                if (!Equals(model.CaseNote, null))
                {
                    _NoteAccess.NoteTitle = model.CaseNote.NoteTitle;
                    _NoteAccess.NoteBody = model.CaseNote.NoteBody;

                    bool Check = _noteAdminRepository.Insert(_NoteAccess);
                }
                //PRFT Custom Code : Start
                if (Equals(model.CaseOrigin, "Contact Us Form") || Equals(model.CaseOrigin, "Request A Quote Form"))
                {
                    isInserted = true;
                    //SendContactUsEmail(model.FirstName, model.LastName, model.CompanyName, model.PhoneNumber, model.Description, model.Email);
                    SendContactUsEmail(model);
                    //PRFT Custom Code : End
                }
                else if(Equals(model.CaseOrigin,"Feedback Form"))
                {
                    string desc = model.Description.Replace("<br/>", ",");
                    string[] message = desc.Split(new char[] { ':', ',' });
                    SendCustomerFeedbackEmail(message, model.FirstName, model.LastName, model.Email);
                }
            }
            //end
            //PRFT Custom Code : Start
            CaseRequestModel caseRequestModel = CaseRequestMap.ToModel(caseRequest);
            if (isInserted)
            {
                caseRequestModel.CountryCode = model.CountryCode;
                caseRequestModel.StateCode = model.StateCode;
                caseRequestModel.City = model.City;
                caseRequestModel.SalesRepId = model.SalesRepId;
            }
            return caseRequestModel;
            //return CaseRequestMap.ToModel(caseRequest);
            //PRFT Custom Code : End           
        }

        public CaseRequestModel UpdateCaseRequest(int caseRequestId, CaseRequestModel model)
        {
            if (caseRequestId < 1)
            {
                throw new Exception("Case request ID cannot be less than 1.");
            }

            if (Equals(model, null))
            {
                throw new Exception("Case request model cannot be null.");
            }

            var caseRequest = _caseRequestRepository.GetByCaseID(caseRequestId);
            if (caseRequest != null)
            {
                int oldStatus = 0;
                int newStatus = 0;
                // Set case request ID
                model.CaseRequestId = caseRequestId;
                model.CreateUser = caseRequest.CreateUser;
                model.CreateDate = caseRequest.CreateDte;
                oldStatus = caseRequest.CaseStatusID;
                newStatus = model.CaseStatusId;
                var caseRequestToUpdate = CaseRequestMap.ToEntity(model);

                var updated = _caseRequestRepository.Update(caseRequestToUpdate);
                if (updated)
                {
                    //To add note
                    if (oldStatus != newStatus)
                    {
                        NoteAdminRepository _noteAdminRepository = new NoteAdminRepository();
                        Note _NoteAccess = new Note();

                        _NoteAccess.CaseID = caseRequest.CaseID;
                        _NoteAccess.AccountID = null;
                        _NoteAccess.CreateDte = System.DateTime.Now;
                        _NoteAccess.CreateUser = caseRequest.CreateUser;
                        _NoteAccess.NoteTitle = model.CaseNote.NoteTitle;
                        _NoteAccess.NoteBody = model.CaseNote.NoteBody;

                        bool Check = _noteAdminRepository.Insert(_NoteAccess);
                    }
                    //end
                    caseRequest = _caseRequestRepository.GetByCaseID(caseRequestId);
                    return CaseRequestMap.ToModel(caseRequest);
                }
            }

            return null;
        }

        public bool DeleteCaseRequest(int caseRequestId)
        {
            if (caseRequestId < 1)
            {
                throw new Exception("Case request ID cannot be less than 1.");
            }

            var caseRequest = _caseRequestRepository.GetByCaseID(caseRequestId);
            if (caseRequest != null)
            {
                return _caseRequestRepository.Delete(caseRequest);
            }

            return false;
        }

        private void GetExpands(NameValueCollection expands, CaseRequest caseRequest)
        {
            if (expands.HasKeys())
            {
                ExpandCasePriority(expands, caseRequest);
                ExpandCaseStatus(expands, caseRequest);
                ExpandCaseType(expands, caseRequest);
            }
        }

        private void ExpandCasePriority(NameValueCollection expands, CaseRequest caseRequest)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.CasePriority)))
            {
                var casePriority = _casePriorityRepository.GetByCasePriorityID(caseRequest.CasePriorityID);
                if (casePriority != null)
                {
                    caseRequest.CasePriorityIDSource = casePriority;
                }
            }
        }

        private void ExpandCaseStatus(NameValueCollection expands, CaseRequest caseRequest)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.CaseStatus)))
            {
                var caseStatus = _caseStatusRepository.GetByCaseStatusID(caseRequest.CaseStatusID);
                if (caseStatus != null)
                {
                    caseRequest.CaseStatusIDSource = caseStatus;
                }
            }
        }

        private void ExpandCaseType(NameValueCollection expands, CaseRequest caseRequest)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.CaseType)))
            {
                var caseType = _caseTypeRepository.GetByCaseTypeID(caseRequest.CaseTypeID);
                if (caseType != null)
                {
                    caseRequest.CaseTypeIDSource = caseType;
                }
            }
        }

        private void SetFiltering(List<Tuple<string, string, string>> filters, CaseRequestQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;

                if (Equals(filterKey, FilterKeys.AccountId)) { SetQueryParameter(CaseRequestColumn.AccountID, filterOperator, filterValue, query); }
                if (Equals(filterKey, FilterKeys.CasePriorityId)) { SetQueryParameter(CaseRequestColumn.CasePriorityID, filterOperator, filterValue, query); }
                if (Equals(filterKey, FilterKeys.CaseStatusId)) { SetQueryParameter(CaseRequestColumn.CaseStatusID, filterOperator, filterValue, query); }
                if (Equals(filterKey, FilterKeys.CaseTypeId)) { SetQueryParameter(CaseRequestColumn.CaseTypeID, filterOperator, filterValue, query); }
                if (Equals(filterKey, FilterKeys.CreateDate)) { SetQueryParameter(CaseRequestColumn.CreateDte, filterOperator, filterValue, query); }
                if (Equals(filterKey, FilterKeys.Email)) { SetQueryParameter(CaseRequestColumn.EmailID, filterOperator, filterValue, query); }
                if (Equals(filterKey, FilterKeys.LastName)) { SetQueryParameter(CaseRequestColumn.LastName, filterOperator, filterValue, query); }
                if (Equals(filterKey, FilterKeys.OwnerAccountId)) { SetQueryParameter(CaseRequestColumn.OwnerAccountID, filterOperator, filterValue, query); }
                if (Equals(filterKey, FilterKeys.PortalId)) { SetQueryParameter(CaseRequestColumn.PortalID, filterOperator, filterValue, query); }
            }
        }

        private void SetFiltering(List<Tuple<string, string, string>> filters, CaseStatusQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;

                if (Equals(filterKey, FilterKeys.CaseStatusId)) { SetQueryParameter(CaseStatusColumn.CaseStatusID, filterOperator, filterValue, query); }
                if (Equals(filterKey, FilterKeys.CaseStatusName)) { SetQueryParameter(CaseStatusColumn.CaseStatusNme, filterOperator, filterValue, query); }
                if (Equals(filterKey, FilterKeys.ViewOrder)) { SetQueryParameter(CaseStatusColumn.ViewOrder, filterOperator, filterValue, query); }
            }
        }

        private void SetFiltering(List<Tuple<string, string, string>> filters, CasePriorityQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;

                if (Equals(filterKey, FilterKeys.CasePriorityID)) { SetQueryParameter(CasePriorityColumn.CasePriorityID, filterOperator, filterValue, query); }
                if (Equals(filterKey, FilterKeys.CasePriorityName)) { SetQueryParameter(CasePriorityColumn.CasePriorityNme, filterOperator, filterValue, query); }
                if (Equals(filterKey, FilterKeys.ViewOrder)) { SetQueryParameter(CasePriorityColumn.ViewOrder, filterOperator, filterValue, query); }
            }
        }

        private void SetFiltering(List<Tuple<string, string, string>> filters, NoteQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;

                if (Equals(filterKey, FilterKeys.NoteId)) { SetQueryParameter(NoteColumn.NoteID, filterOperator, filterValue, query); }
                if (Equals(filterKey, FilterKeys.CaseId)) { SetQueryParameter(NoteColumn.CaseID, filterOperator, filterValue, query); }
                if (Equals(filterKey, FilterKeys.AccountId)) { SetQueryParameter(NoteColumn.AccountID, filterOperator, filterValue, query); }
                if (Equals(filterKey, FilterKeys.NoteTitle)) { SetQueryParameter(NoteColumn.NoteTitle, filterOperator, filterValue, query); }
                if (Equals(filterKey, FilterKeys.NoteBody)) { SetQueryParameter(NoteColumn.NoteBody, filterOperator, filterValue, query); }
                if (Equals(filterKey, FilterKeys.CreateUser)) { SetQueryParameter(NoteColumn.CreateUser, filterOperator, filterValue, query); }
            }
        }

        private void SetSorting(NameValueCollection sorts, CaseRequestSortBuilder sortBuilder)
        {
            if (sorts.HasKeys())
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);

                    if (Equals(key, SortKeys.CasePriorityId)) { SetSortDirection(CaseRequestColumn.CasePriorityID, value, sortBuilder); }
                    if (Equals(key, SortKeys.CaseRequestId)) { SetSortDirection(CaseRequestColumn.CaseID, value, sortBuilder); }
                    if (Equals(key, SortKeys.CaseStatusId)) { SetSortDirection(CaseRequestColumn.CaseStatusID, value, sortBuilder); }
                    if (Equals(key, SortKeys.CaseTypeId)) { SetSortDirection(CaseRequestColumn.CaseTypeID, value, sortBuilder); }
                    if (Equals(key, SortKeys.CreateDate)) { SetSortDirection(CaseRequestColumn.CreateDte, value, sortBuilder); }
                    if (Equals(key, SortKeys.LastName)) { SetSortDirection(CaseRequestColumn.LastName, value, sortBuilder); }
                }
            }
        }

        private void SetSorting(NameValueCollection sorts, CaseStatusSortBuilder sortBuilder)
        {
            if (sorts.HasKeys())
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);

                    if (Equals(key, SortKeys.CaseStatusId)) { SetSortDirection(CaseStatusColumn.CaseStatusID, value, sortBuilder); }
                    if (Equals(key, SortKeys.CaseStatusName)) { SetSortDirection(CaseStatusColumn.CaseStatusNme, value, sortBuilder); }
                    if (Equals(key, SortKeys.ViewOrder)) { SetSortDirection(CaseStatusColumn.ViewOrder, value, sortBuilder); }
                }
            }
        }

        private void SetSorting(NameValueCollection sorts, CasePrioritySortBuilder sortBuilder)
        {
            if (sorts.HasKeys())
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);

                    if (Equals(key, SortKeys.CasePriorityID)) { SetSortDirection(CasePriorityColumn.CasePriorityID, value, sortBuilder); }
                    if (Equals(key, SortKeys.CasePriorityName)) { SetSortDirection(CasePriorityColumn.CasePriorityNme, value, sortBuilder); }
                    if (Equals(key, SortKeys.ViewOrder)) { SetSortDirection(CasePriorityColumn.ViewOrder, value, sortBuilder); }
                }
            }
        }

        private void SetSorting(NameValueCollection sorts, NoteSortBuilder sortBuilder)
        {
            if (sorts.HasKeys())
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);

                    if (Equals(key, SortKeys.NoteId)) { SetSortDirection(NoteColumn.NoteID, value, sortBuilder); }
                    if (Equals(key, SortKeys.CaseId)) { SetSortDirection(NoteColumn.CaseID, value, sortBuilder); }
                    if (Equals(key, SortKeys.AccountId)) { SetSortDirection(NoteColumn.AccountID, value, sortBuilder); }
                    if (Equals(key, SortKeys.NoteTitle)) { SetSortDirection(NoteColumn.NoteTitle, value, sortBuilder); }
                    if (Equals(key, SortKeys.NoteBody)) { SetSortDirection(NoteColumn.NoteBody, value, sortBuilder); }
                    if (Equals(key, SortKeys.CreateUser)) { SetSortDirection(NoteColumn.CreateUser, value, sortBuilder); }
                    if (Equals(key, SortKeys.CreateDate)) { SetSortDirection(NoteColumn.CreateDte, value, sortBuilder); }
                }
            }
        }

        private void SetQueryParameter(CaseRequestColumn column, string filterOperator, string filterValue, CaseRequestQuery query)
        {
            base.SetQueryParameter(column, filterOperator, filterValue, query);
        }

        private void SetSortDirection(CaseRequestColumn column, string value, CaseRequestSortBuilder sortBuilder)
        {
            base.SetSortDirection(column, value, sortBuilder);
        }

        #region Znode Version 8.0

        /// <summary>
        /// To create CaseNote
        /// </summary>
        /// <param name="model">NoteModel model</param>
        /// <returns>returns NoteModel</returns>
        public NoteModel CreateCaseNote(NoteModel model)
        {
            var entity = CaseRequestMap.ToEntity(model);

            var caseNote = _caseNoteRepository.Save(entity);

            return CaseRequestMap.ToModel(caseNote);
        }

        /// <summary>
        /// To GetCaseRequestList
        /// </summary>
        /// <param name="expands"></param>
        /// <param name="filters"></param>
        /// <param name="sorts"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        public CaseRequestListModel GetCaseRequestList(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            List<Tuple<string, string, string>> filterList = new List<Tuple<string, string, string>>();
            CaseRequestListModel list = new CaseRequestListModel();

            int totalRowCount = 0;
            var pagingStart = 0;
            var pagingLength = 0;
            string orderBy = StringHelper.GenerateDynamicOrderByClause(sorts);
            string innerWhereClause = string.Empty;
            foreach (var item in filters)
            {
                if (Equals(item.Item1, "username"))
                {
                    filterList.Add(item);
                    innerWhereClause = StringHelper.GenerateDynamicWhereClause(filterList);
                    filters.Remove(item);
                    break;
                }
            }
            string whereClause = StringHelper.GenerateDynamicWhereClause(filters);
            pagingStart = Convert.ToInt32(page.Get(PageKeys.Index));
            pagingLength = Convert.ToInt32(page.Get(PageKeys.Size));

            StoredProcedureHelper storedProcedureHelper = new StoredProcedureHelper();
            DataSet resultDataSet = storedProcedureHelper.SP_DataDetailsSpConfig(whereClause, string.Empty, StoredProcedureKeys.SpGetCaseRequestsLis, orderBy, pagingStart.ToString(), pagingLength.ToString(), out totalRowCount, null, innerWhereClause);

            list.CaseRequests = resultDataSet.Tables[0].ToList<CaseRequestModel>().ToCollection();
            list.TotalResults = totalRowCount;
            list.PageSize = pagingLength;
            list.PageIndex = pagingStart;
            // Check to set page size equal to the total number of results
            if (Equals(pagingLength, int.MaxValue))
            {
                list.PageSize = list.TotalResults;
            }

            return list;

        }

        /// <summary>
        /// To GetCaseStatus
        /// </summary>
        /// <param name="expands"></param>
        /// <param name="filters"></param>
        /// <param name="sorts"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        public CaseStatusListModel GetCaseStatus(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new CaseStatusListModel();
            var caseStatus = new TList<CaseStatus>();
            var tempList = new TList<CaseStatus>();

            var query = new CaseStatusQuery();
            var sortBuilder = new CaseStatusSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            tempList = _caseStatusRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (Equals(pagingLength, int.MaxValue))
            {
                model.PageSize = model.TotalResults;
            }

            // Now go through the list and get any expands
            foreach (var status in tempList)
            {
                caseStatus.Add(status);
            }

            // Map each item and add to the list
            foreach (var c in caseStatus)
            {
                model.CaseStatuses.Add(CaseStatusMap.ToModel(c));
            }

            return model;
        }

        /// <summary>
        /// To GetCasePriority
        /// </summary>
        /// <param name="expands"></param>
        /// <param name="filters"></param>
        /// <param name="sorts"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        public CasePriorityListModel GetCasePriority(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new CasePriorityListModel();
            var casePriority = new TList<CasePriority>();
            var tempList = new TList<CasePriority>();

            var query = new CasePriorityQuery();
            var sortBuilder = new CasePrioritySortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            tempList = _casePriorityRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (Equals(pagingLength, int.MaxValue))
            {
                model.PageSize = model.TotalResults;
            }

            // Now go through the list and get any expands
            foreach (var priority in tempList)
            {
                casePriority.Add(priority);
            }

            // Map each item and add to the list
            foreach (var c in casePriority)
            {
                model.CasePriorities.Add(CasePriorityMap.ToModel(c));
            }

            return model;
        }

        /// <summary>
        /// To GetCaseNote
        /// </summary>
        /// <param name="expands"></param>
        /// <param name="filters"></param>
        /// <param name="sorts"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        public NoteListModel GetCaseNote(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new NoteListModel();
            var caseNote = new TList<Note>();
            var tempList = new TList<Note>();

            var query = new NoteQuery();
            var sortBuilder = new NoteSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            tempList = _caseNoteRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (Equals(pagingLength, int.MaxValue))
            {
                model.PageSize = model.TotalResults;
            }

            // Now go through the list and get any expands
            foreach (var note in tempList)
            {
                caseNote.Add(note);
            }

            // Map each item and add to the list
            foreach (var c in caseNote)
            {
                model.Notes.Add(CaseRequestMap.ToModel(c));
            }

            return model;
        }

        #endregion

        #region Send Email for Contact Us
        private void SendContactUsEmail(CaseRequestModel model)
        {
            var defaultTemplatePath = (model.IsRequestAQuoteForm) ? HttpContext.Current.Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, "RequestAQuote.html")) : HttpContext.Current.Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, "ContactUs.html"));

            if (File.Exists(defaultTemplatePath))
            {
                try
                {
                    string subject = (model.IsRequestAQuoteForm) ? ConfigurationManager.AppSettings["RequestAQuoteNotificationSubject"] : ConfigurationManager.AppSettings["ContactUsNotificationSubject"];

                    StreamReader rw = new StreamReader(defaultTemplatePath);
                    string messageText = rw.ReadToEnd();

                    Regex rx1 = new Regex("#FirstName#", RegexOptions.IgnoreCase);
                    messageText = rx1.Replace(messageText, model.FirstName);

                    Regex rx2 = new Regex("#LastName#", RegexOptions.IgnoreCase);
                    messageText = rx2.Replace(messageText, model.LastName);

                    Regex rx3 = new Regex("#CompanyName#", RegexOptions.IgnoreCase);
                    messageText = rx3.Replace(messageText, model.CompanyName);

                    Regex rx4 = new Regex("#PhoneNumber#", RegexOptions.IgnoreCase);
                    messageText = rx4.Replace(messageText, model.PhoneNumber);

                    Regex rx5 = new Regex("#Message#", RegexOptions.IgnoreCase);
                    messageText = rx5.Replace(messageText, model.Description);

                    //PRFT Custom Code:Start
                    if (string.IsNullOrEmpty(model.SalesRepName))
                    {
                        Regex rx6 = new Regex("#SalesRepresentative#", RegexOptions.IgnoreCase);
                        messageText = rx6.Replace(messageText, string.Empty);
                    }
                    else
                    {
                        Regex rx6 = new Regex("#SalesRepresentative#", RegexOptions.IgnoreCase);
                        messageText = rx6.Replace(messageText, model.SalesRepName);
                    }

                    if (model.IsRequestAQuoteForm)
                    {
                        Regex rx12 = new Regex("#DueDate#", RegexOptions.IgnoreCase);
                        messageText = rx12.Replace(messageText, model.DueDate.ToString("dd/MM/yyyy"));

                        Regex rx13 = new Regex("#Fax#", RegexOptions.IgnoreCase);
                        string faxNumber = string.IsNullOrEmpty(model.FaxNumber) ? string.Empty : model.FaxNumber;
                        messageText = rx13.Replace(messageText, faxNumber);

                        Regex rx14 = new Regex("#PMOContact#", RegexOptions.IgnoreCase);
                        messageText = rx14.Replace(messageText, model.ContactChoiceSelected);
                    }
                    else
                    {
                        Regex rx7 = new Regex("#Country#", RegexOptions.IgnoreCase);
                        messageText = rx7.Replace(messageText, model.CountryCode);

                        Regex rx8 = new Regex("#State#", RegexOptions.IgnoreCase);
                        messageText = rx8.Replace(messageText, model.StateCode);

                        Regex rx9 = new Regex("#City#", RegexOptions.IgnoreCase);
                        messageText = rx9.Replace(messageText, model.City);
                    }

                    Regex rx10 = new Regex("#Email#", RegexOptions.IgnoreCase);
                    messageText = rx10.Replace(messageText, model.Email);

                    Regex rx11 = new Regex("#LOGO#", RegexOptions.IgnoreCase);
                    messageText = rx11.Replace(messageText, ZNodeConfigManager.SiteConfig.StoreName);


                    //PRFT CUstom COde : Start
                   
                    Regex rx15 = new Regex("#StoreLogo#", RegexOptions.IgnoreCase);
                    //string LogoPath = HttpContext.GetGlobalResourceObject("CommonCaption", "ApiRootUrl").ToString() + "/" + ZNodeConfigManager.SiteConfig.LogoPath.Trim().Replace("~/", "");
                    //messageText = rx15.Replace(messageText, ZNodeConfigManager.SiteConfig.LogoPath.Trim().Replace("~/", ""));

                    string LogoPath = ZNodeConfigManager.SiteConfig.LogoPath.Trim().Replace("~/", "");
                    messageText = rx15.Replace(messageText, apiUrl + "/" + LogoPath.TrimStart('~'));

                    var rx16 = new Regex("#MailingInfo#", RegexOptions.IgnoreCase);
                    messageText = rx16.Replace(messageText, ZNodeConfigManager.SiteConfig.CustomerServiceEmail);

                    var rx17 = new Regex("#SupportPhone#", RegexOptions.IgnoreCase);
                    messageText = rx17.Replace(messageText, ZNodeConfigManager.SiteConfig.CustomerServicePhoneNumber);

                    var rx18 = new Regex("#SupportEmail#", RegexOptions.IgnoreCase);
                    messageText = rx18.Replace(messageText, ZNodeConfigManager.SiteConfig.SalesEmail);

                    string logoLink = ConfigurationManager.AppSettings["DemoWebsiteUrl"];
                    var rx19 = new Regex("#LogoLink#", RegexOptions.IgnoreCase);
                    messageText = rx19.Replace(messageText, logoLink);
                    //PRFT Custom Code : End

                    var salesRepEmail = string.IsNullOrEmpty(model.SalesRepEmail) ? string.Empty : model.SalesRepEmail;
                    ZNode.Libraries.Framework.Business.ZNodeEmail.SendEmail(ZNodeConfigManager.SiteConfig.CustomerServiceEmail, model.Email, salesRepEmail, subject, messageText, true);
                    //PRFT Custom Code:End
                }
                catch (Exception ex)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.GeneralError, model.FirstName, "", null, ex.Message, null);
                }
            }
        }
        #endregion

        #region Send Email for Customer Feedback
        private void SendCustomerFeedbackEmail(string[] message, string firstName, string lastName, string email)
        {
            var defaultTemplatePath = HttpContext.Current.Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, "CustomerFeedback.html"));
            if (File.Exists(defaultTemplatePath))
            {
                string portalName = ZNodeConfigManager.SiteConfig.StoreName;
                string smtpServer = ZNodeConfigManager.SiteConfig.SMTPServer;
                string senderEmail = email;

                string subject = " Feedback from Website Visitor : User Feedback ";

                StreamReader rw = new StreamReader(defaultTemplatePath);
                string messageText = rw.ReadToEnd();

                Regex rx1 = new Regex("#Message#", RegexOptions.IgnoreCase);
                messageText = rx1.Replace(messageText, message[1]);

                Regex rx2 = new Regex("#FirstName#", RegexOptions.IgnoreCase);
                messageText = rx2.Replace(messageText, firstName);

                Regex rx3 = new Regex("#LastName#", RegexOptions.IgnoreCase);
                messageText = rx3.Replace(messageText, lastName);

                Regex rx4 = new Regex("#City#", RegexOptions.IgnoreCase);
                messageText = rx4.Replace(messageText, message[3]);

                Regex rx5 = new Regex("#State#", RegexOptions.IgnoreCase);
                messageText = rx5.Replace(messageText, message[5]);

                Regex rx6 = new Regex("#Email Address#", RegexOptions.IgnoreCase);
                messageText = rx6.Replace(messageText, email);

                Regex rx7 = new Regex("#ShareFeedback#", RegexOptions.IgnoreCase);
                messageText = rx7.Replace(messageText, message[6]);

                try
                {
                    ZNode.Libraries.Framework.Business.ZNodeEmail.SendEmail(ZNodeConfigManager.SiteConfig.AdminEmail, senderEmail, string.Empty, subject, messageText, true);
                }
                catch (Exception ex)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.GeneralError, firstName, "", null, ex.Message, null);
                }
            }
        }
        #endregion
    }
}
