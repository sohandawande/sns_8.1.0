﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    public interface IRejectionMessageService
    {
        /// <summary>
        /// Get Rejection Message by rejection message Id.
        /// </summary>
        /// <param name="rejectionMessageId">Id of the rejection message.</param>
        /// <returns>RejectionMessageModel</returns>
        RejectionMessageModel GetRejectionMessage(int rejectionMessageId);

        /// <summary>
        /// Get Rejection Message list.
        /// </summary>
        /// <param name="expands">Name Value Collection.</param>
        /// <param name="filters">filters for Rejection Message.</param>
        /// <param name="sorts">sorts for rejection message.</param>
        /// <param name="page">page for rejection message.</param>
        /// <returns>RejectionMessageListModel</returns>
        RejectionMessageListModel GetRejectionMessages(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Create Rejection Message.
        /// </summary>
        /// <param name="model">model to create.</param>
        /// <returns>RejectionMessageModel</returns>
        RejectionMessageModel CreateRejectionMessage(RejectionMessageModel model);

        /// <summary>
        /// Update existing Rejection Message
        /// </summary>
        /// <param name="rejectionMessageId">Id of the rejection message.</param>
        /// <param name="model">model to update.</param>
        /// <returns>RejectionMessageModel</returns>
        RejectionMessageModel UpdateRejectionMessage(int rejectionMessageId, RejectionMessageModel model);

        /// <summary>
        /// Delete the existing rejection message.
        /// </summary>
        /// <param name="rejectionMessageId">Id of the rejection message.</param>
        /// <returns>Returns true or false.</returns>
        bool DeleteRejectionMessage(int rejectionMessageId);
    }
}
