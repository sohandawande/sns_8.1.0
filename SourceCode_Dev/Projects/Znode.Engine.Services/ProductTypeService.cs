﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Helpers;
using Znode.Libraries.Helpers.Constants;
using Znode.Libraries.Helpers.Extensions;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using ProductTypeRepository = ZNode.Libraries.DataAccess.Service.ProductTypeService;
using ProductRepository = ZNode.Libraries.DataAccess.Service.ProductService;

namespace Znode.Engine.Services
{
    public class ProductTypeService : BaseService, IProductTypeService
    {

        #region Private Variable
        private readonly ProductTypeRepository _productTypeRepository;
        private readonly ProductRepository _productRepository;
        #endregion

        #region Public Constructor
        public ProductTypeService()
        {
            _productTypeRepository = new ProductTypeRepository();
            _productRepository = new ProductRepository();
        }
        #endregion

        #region Public Methods
        public ProductTypeModel GetProductType(int productTypeId)
        {
            var ProductType = _productTypeRepository.GetByProductTypeId(productTypeId);
            return ProductTypeMap.ToModel(ProductType);
        }

        public ProductTypeListModel GetProductTypes(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new ProductTypeListModel();
            var productTypes = new TList<ProductType>();

            var query = new ProductTypeQuery();
            var sort = new ProductTypeSortBuilder();

            var pagingStart = 0;
            var pagingLength = 0;

            SetFiltering(filters, query);
            SetSorting(sorts, sort);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            productTypes = _productTypeRepository.Find(query, sort, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                model.PageSize = model.TotalResults;
            }

            // Map each item and add to the list
            foreach (var m in productTypes)
            {
                model.ProductTypes.Add(ProductTypeMap.ToModel(m));
            }

            return model;
        }

        public ProductTypeModel CreateProductType(ProductTypeModel model)
        {
            var query = new ProductTypeQuery();

            if (Equals(model, null))
            {
                throw new Exception("Product Type model cannot be null.");
            }
            query.AppendEquals(ProductTypeColumn.Name, model.Name);
            query.AppendNotEquals(ProductTypeColumn.ProductTypeId, model.ProductTypeId.ToString());
            TList<ProductType> list = _productTypeRepository.Find(query);
            if (list.Count > 0)
            {
                // Display error message
                throw new Exception("Product type name already exists. Please enter some other name.");
            }

            var entity = ProductTypeMap.ToEntity(model);
            var productType = _productTypeRepository.Save(entity);
            return ProductTypeMap.ToModel(productType);
        }

        public ProductTypeModel UpdateProductType(int productTypeId, ProductTypeModel model)
        {

            var query = new ProductTypeQuery();

            if (productTypeId < 1)
            {
                throw new Exception("ProductType ID cannot be less than 1.");
            }

            if (model == null)
            {
                throw new Exception("ProductType model cannot be null.");
            }

            query.AppendEquals(ProductTypeColumn.Name, model.Name);
            query.AppendNotEquals(ProductTypeColumn.ProductTypeId, productTypeId.ToString());
            TList<ProductType> list = _productTypeRepository.Find(query);
            if (list.Count > 0)
            {
                // Display error message
                throw new Exception("Product type name already exists. Please enter some other name.");
            }

            var productType = _productTypeRepository.GetByProductTypeId(productTypeId);
            if (!Equals(productType, null))
            {
                // Set Product Type Id
                model.ProductTypeId = productTypeId;

                var productTypeToUpdate = ProductTypeMap.ToEntity(model);

                if (_productTypeRepository.Update(productTypeToUpdate))
                {
                    productType = _productTypeRepository.GetByProductTypeId(productTypeId);
                    return ProductTypeMap.ToModel(productType);
                }
            }

            return null;
        }

        public bool DeleteProductType(int productTypeId)
        {
            var query = new ProductQuery();
            if (productTypeId < 1)
            {
                throw new Exception("productType ID cannot be less than 1.");
            }

            query.AppendEquals(ProductColumn.ProductTypeID, productTypeId.ToString());

            TList<Product> list = _productRepository.Find(query);
            if (list.Count > 0)
            {
                throw new Exception("Delete action could not be completed because the Product Type is in use.");
            }
            var productType = _productTypeRepository.GetByProductTypeId(productTypeId);
            if (!Equals(productType, null))
            {
                return _productTypeRepository.Delete(productType);
            }

            return false;
        }

        public AttributeTypeValueListModel GetProductAttributesByProductTypeId(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new AttributeTypeValueListModel();
            string whereClause = StringHelper.GenerateDynamicWhereClause(filters);
            string orderBy = StringHelper.GenerateDynamicOrderByClause(sorts);
            int totalRowCount = 0;
            var pagingStart = 0;
            var pagingLength = 0;
            SetPaging(page, model, out pagingStart, out pagingLength);
            pagingStart = Convert.ToInt32(page.Get(PageKeys.Index));

            StoredProcedureHelper storedProcedureHelper = new StoredProcedureHelper();

            DataSet resultDataSet = storedProcedureHelper.SP_DataDetailsSpConfig(whereClause, StoredProcedureKeys.SpProductTypeWiseAttributes);
            model.AttributeTypeValueList = resultDataSet.Tables[0].ToList<AttributeTypeValueModel>().ToCollection();
            model.TotalResults = totalRowCount;
            model.PageSize = pagingLength;
            model.PageIndex = pagingStart;
            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                model.PageSize = model.TotalResults;
            }

            return model;
        }

        #endregion

        #region Private Methods
        private void SetFiltering(List<Tuple<string, string, string>> filters, ProductTypeQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;

                if (filterKey == FilterKeys.Name) SetQueryParameter(ProductTypeColumn.Name, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.Description) SetQueryParameter(ProductTypeColumn.Description, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.PortalId) SetQueryParameter(ProductTypeColumn.PortalID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.ProductTypeId) SetQueryParameter(ProductTypeColumn.ProductTypeId, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.Franchisable) SetQueryParameter(ProductTypeColumn.Franchisable, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.DispalyOrder) SetQueryParameter(ProductTypeColumn.DisplayOrder, filterOperator, filterValue, query);
            }
        }

        private void SetSorting(NameValueCollection sorts, ProductTypeSortBuilder sortBuilder)
        {
            if (sorts.HasKeys())
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);

                    if (value == SortKeys.DisplayOrder) SetSortDirection(ProductTypeColumn.DisplayOrder, sorts[StoredProcedureKeys.sortDirKey], sortBuilder);
                    if (value == SortKeys.ProductTypeId) SetSortDirection(ProductTypeColumn.ProductTypeId, sorts[StoredProcedureKeys.sortDirKey], sortBuilder);
                    if (value == SortKeys.Name) SetSortDirection(ProductTypeColumn.Name, sorts[StoredProcedureKeys.sortDirKey], sortBuilder);
                    if (value == SortKeys.Description) SetSortDirection(ProductTypeColumn.Description, sorts[StoredProcedureKeys.sortDirKey], sortBuilder);
                }
            }
        }

        private void SetQueryParameter(ProductTypeColumn column, string filterOperator, string filterValue, ProductTypeQuery query)
        {
            base.SetQueryParameter(column, filterOperator, filterValue, query);
        }

        private void SetSortDirection(ProductTypeColumn column, string value, ProductTypeSortBuilder sortBuilder)
        {
            base.SetSortDirection(column, value, sortBuilder);
        }
        #endregion

    }
}
