﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    public interface IPRFTERPServices
    {
        PRFTERPItemDetailsModel GetERPItemDetails(string associatedCustomerExternalId, string productNum, string customerType);
        PRFTInventoryModel GetInventoryDetails(string productNum);
    }
}
