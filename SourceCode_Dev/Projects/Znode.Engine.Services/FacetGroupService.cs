﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using WebMatrix.WebData;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;
using FacetGroupRepository = ZNode.Libraries.DataAccess.Service.FacetGroupService;
using FacetControlTypeRepository = ZNode.Libraries.DataAccess.Service.FacetControlTypeService;
using FacetGroupCategoryRepository = ZNode.Libraries.DataAccess.Service.FacetGroupCategoryService;
using FacetsRepository = ZNode.Libraries.DataAccess.Service.FacetService;
using Znode.Libraries.Helpers.Constants;
namespace Znode.Engine.Services
{
    public class FacetGroupService : BaseService, IFacetGroupService
    {
        #region Private Variables
        private readonly FacetGroupRepository _facetGroupRepository;
        private readonly FacetControlTypeRepository _facetControlTypeRepository;
        private readonly FacetGroupCategoryRepository _facetGroupCategoryRepository;
        private readonly FacetsRepository _facetsRepository;

        #endregion

        #region Constructor
        public FacetGroupService()
        {
            _facetGroupRepository = new FacetGroupRepository();
            _facetControlTypeRepository = new FacetControlTypeRepository();
            _facetGroupCategoryRepository = new FacetGroupCategoryRepository();
            _facetsRepository = new FacetsRepository();
        }
        #endregion

        #region Public Methods

        public FacetGroupModel GetFacetGroup(int facetGroupId, NameValueCollection expands)
        {
            var facetGroup = _facetGroupRepository.GetByFacetGroupID(facetGroupId);
            if (!Equals(facetGroup, null))
            {
                GetExpands(expands, facetGroup);
            }

            return FacetGroupMap.ToModel(facetGroup);
        }

        public FacetGroupListModel GetFacetGroups(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new FacetGroupListModel();
            var facetGroups = new TList<FacetGroup>();
            var tempList = new TList<FacetGroup>();

            var query = new FacetGroupQuery();
            var sortBuilder = new FacetGroupSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            tempList = _facetGroupRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                model.PageSize = model.TotalResults;
            }

            // Now go through the list and get any expands
            foreach (var facetGroup in tempList)
            {
                GetExpands(expands, facetGroup);
                facetGroups.Add(facetGroup);
            }

            // Map each item and add to the list
            foreach (var a in facetGroups)
            {
                model.FacetGroups.Add(FacetGroupMap.ToModel(a));
            }

            return model;
        }

        public FacetControlTypeListModel GetFacetControlTypes(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new FacetControlTypeListModel();
            var facetGroups = new TList<FacetControlType>();
            var tempList = new TList<FacetControlType>();

            var query = new FacetControlTypeQuery();
            var sortBuilder = new FacetControlTypeSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;
            var totalResults = 0;
            SetPaging(page, model, out pagingStart, out pagingLength);
            tempList = _facetControlTypeRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Map each item and add to the list
            foreach (var a in tempList)
            {
                model.FacetControlTypes.Add(FacetControlTypeMap.ToModel(a));
            }

            return model;
        }

        public FacetGroupModel CreateFacetGroup(FacetGroupModel model)
        {
            if (Equals(model, null))
            {
                throw new Exception("Facet group model cannot be null.");
            }

            var entity = FacetGroupMap.ToEntity(model);
            var facetGroup = _facetGroupRepository.Save(entity);
            return FacetGroupMap.ToModel(facetGroup);
        }

        public FacetGroupModel UpdateFacetGroup(int facetGroupId, FacetGroupModel model)
        {
            if (facetGroupId < 1)
            {
                throw new Exception("Facet Group ID cannot be less than 1.");
            }

            if (Equals(model, null))
            {
                throw new Exception("Facet Group model cannot be null.");
            }

            var facetGroup = _facetGroupRepository.GetByFacetGroupID(facetGroupId);
            if (!Equals(facetGroup, null))
            {
                // Set facetGroupId and update details.
                model.FacetGroupID = facetGroupId;
                var facetGroupToUpdate = FacetGroupMap.ToEntity(model);

                var updated = _facetGroupRepository.Update(facetGroupToUpdate);
                if (updated)
                {
                    facetGroup = _facetGroupRepository.GetByFacetGroupID(facetGroupId);
                    return FacetGroupMap.ToModel(facetGroup);
                }
            }

            return null;
        }

        public bool DeleteFacetGroup(int facetGroupId)
        {
            if (facetGroupId < 1)
            {
                throw new Exception("Facet Group ID cannot be less than 1.");
            }

            var facetGroupCategories = _facetGroupCategoryRepository.GetByFacetGroupID(facetGroupId);
            if (!Equals(facetGroupCategories, null))
            {
                var facetgroup = _facetGroupCategoryRepository.Delete(facetGroupCategories);
            }

            var facetGroup = _facetGroupRepository.GetByFacetGroupID(facetGroupId);
            if (!Equals(facetGroup, null))
            {
                return _facetGroupRepository.Delete(facetGroup);
            }
            return false;
        }

        public FacetGroupCategoryListModel CreateFacetGroupCategory(FacetGroupCategoryListModel model)
        {
            if (Equals(model, null))
            {
                throw new Exception("Facet group category model cannot be null.");
            }

            //var datqa = _facetGroupCategoryRepository.DeepSave(
            var entity = FacetGroupMap.ToEntityList(model.FacetGroupCategoryList);
            var status = _facetGroupCategoryRepository.DeepSave(entity);
            if (status)
            {
                var facetGroupCategories = _facetGroupCategoryRepository.GetByFacetGroupID(model.FacetGroupCategoryList[0].FacetGroupID);
                return FacetGroupCategoryMap.ToModel(facetGroupCategories);
            }
            return null;

        }

        public bool DeleteFacetGroupCategory(int facetGroupId)
        {
            if (facetGroupId < 1)
            {
                throw new Exception("Facet Group ID cannot be less than 1.");
            }
            var facetGroupCategories = _facetGroupCategoryRepository.GetByFacetGroupID(facetGroupId);
            if (!Equals(facetGroupCategories, null))
            {
                var facetgroup = _facetGroupCategoryRepository.Delete(facetGroupCategories);
                return !Equals(facetgroup, null);
            }

            return false;
        }

        public FacetModel CreateFacet(FacetModel model)
        {
            if (Equals(model, null))
            {
                throw new Exception("Facet model cannot be null.");
            }

            var entity = FacetGroupMap.ToEntity(model);
            var facet = _facetsRepository.Save(entity);
            return FacetGroupMap.ToModel(facet);
        }

        public FacetModel UpdateFacet(int facetId, FacetModel model)
        {
            if (facetId < 1)
            {
                throw new Exception("Facet ID cannot be less than 1.");
            }

            if (Equals(model, null))
            {
                throw new Exception("Facet model cannot be null.");
            }

            var facet = _facetsRepository.GetByFacetID(facetId);
            if (!Equals(facet, null))
            {
                // Set facetId and update details.
                model.FacetID = facetId;
                var facetToUpdate = FacetGroupMap.ToEntity(model);

                var updated = _facetsRepository.Update(facetToUpdate);
                if (updated)
                {
                    facet = _facetsRepository.GetByFacetID(facetId);
                    return FacetGroupMap.ToModel(facet);
                }
            }
            return null;
        }

        public bool DeleteFacet(int facetId)
        {
            if (facetId < 1)
            {
                throw new Exception("Facet ID cannot be less than 1.");
            }

            var facet = _facetsRepository.GetByFacetID(facetId);
            if (!Equals(facet, null))
            {
                return _facetsRepository.Delete(facet);
            }
            return false;
        }

        public FacetModel GetFacet(int facetId, NameValueCollection expands)
        {
            var facet = _facetsRepository.GetByFacetID(facetId);
            return FacetMap.ToModel(facet);
        }

        public FacetListModel GetFacetList(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new FacetListModel();
            var facets = new TList<Facet>();
            var tempList = new TList<Facet>();

            var query = new FacetQuery();
            var sortBuilder = new FacetSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetFacetFiltering(filters, query);
            SetFacetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            tempList = _facetsRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                model.PageSize = model.TotalResults;
            }

            // Now go through the list and get any expands
            foreach (var facet in tempList)
            {
                facets.Add(facet);
            }

            // Map each item and add to the list
            foreach (var a in facets)
            {
                model.Facets.Add(FacetMap.ToModel(a));
            }

            return model;
        }

        #endregion

        #region Private Methods

        private void SetFiltering(List<Tuple<string, string, string>> filters, FacetGroupQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;

                if (filterKey == FilterKeys.FacetGroupLabel) SetQueryParameter(FacetGroupColumn.FacetGroupLabel, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.CatalogId) SetQueryParameter(FacetGroupColumn.CatalogID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.FacetGroupId) SetQueryParameter(FacetGroupColumn.FacetGroupID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.DispalyOrder) SetQueryParameter(FacetGroupColumn.DisplayOrder, filterOperator, filterValue, query);
                if (filterKey.Equals(FilterKeys.UserName))
                {
                    string data = this.GetCatalogIdByUserName(filters);
                    if (!string.IsNullOrEmpty(data))
                    {
                        query.AppendIn(SqlUtil.AND, FacetGroupColumn.CatalogID, data.Split(','));
                    }
                }
            }
        }

        private void SetSorting(NameValueCollection sorts, FacetGroupSortBuilder sortBuilder)
        {
            if (sorts.HasKeys())
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);
                    if (Equals(value, SortKeys.FacetGroupID)) { SetSortDirection(FacetGroupColumn.FacetGroupID, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (Equals(value, SortKeys.DisplayOrder)) { SetSortDirection(FacetGroupColumn.DisplayOrder, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (Equals(value, SortKeys.FacetGroupLabel)) { SetSortDirection(FacetGroupColumn.FacetGroupLabel, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                }
            }
        }

        private void GetExpands(NameValueCollection expands, FacetGroup facetGroup)
        {
            if (expands.HasKeys())
            {
                ExpandFacetGroupCategories(expands, facetGroup);
                ExpandFacetGroupFacets(expands, facetGroup);
            }
        }

        private void ExpandFacetGroupCategories(NameValueCollection expands, FacetGroup facetGroup)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Categories)))
            {
                // get all facet group categries.
                var facetGroupCategories = _facetGroupCategoryRepository.GetByFacetGroupID(facetGroup.FacetGroupID);
                _facetGroupCategoryRepository.DeepLoad(facetGroupCategories, true, DeepLoadType.IncludeChildren, typeof(FacetGroupCategory));

                foreach (var item in facetGroupCategories)
                {
                    facetGroup.FacetGroupCategoryCollection.Add(item);
                }
            }
        }

        private void ExpandFacetGroupFacets(NameValueCollection expands, FacetGroup facetGroup)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Facets)))
            {
                // get all associated facets.
                var facetGroupFacets = _facetsRepository.GetByFacetGroupID(facetGroup.FacetGroupID);
                _facetsRepository.DeepLoad(facetGroupFacets, true, DeepLoadType.IncludeChildren, typeof(Facet));

                foreach (var item in facetGroupFacets)
                {
                    facetGroup.FacetCollection.Add(item);
                }
            }
        }


        private void SetFacetFiltering(List<Tuple<string, string, string>> filters, FacetQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;

                if (Equals(filterKey, FilterKeys.FacetName)) SetQueryParameter(FacetColumn.FacetName, filterOperator, filterValue, query);
                if (Equals(filterKey, FilterKeys.FacetId)) SetQueryParameter(FacetColumn.FacetID, filterOperator, filterValue, query);
                if (Equals(filterKey, FilterKeys.FacetGroupId)) SetQueryParameter(FacetColumn.FacetGroupID, filterOperator, filterValue, query);
                if (Equals(filterKey, FilterKeys.FacetDisplayOrder)) SetQueryParameter(FacetColumn.FacetDisplayOrder, filterOperator, filterValue, query);
            }
        }

        private void SetFacetSorting(NameValueCollection sorts, FacetSortBuilder sortBuilder)
        {
            if (sorts.HasKeys())
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);
                    if (Equals(value, SortKeys.FacetGroupID)) { SetSortDirection(FacetColumn.FacetGroupID, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (Equals(value, SortKeys.FacetName)) { SetSortDirection(FacetColumn.FacetName, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (Equals(value, SortKeys.FacetId)) { SetSortDirection(FacetColumn.FacetID, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (Equals(value, SortKeys.FacetDisplayOrder)) { SetSortDirection(FacetColumn.FacetDisplayOrder, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                }
            }
        }

        private string GetCatalogIdByUserName(List<Tuple<string, string, string>> filters)
        {
            string catalogIds = string.Empty;
            PortalCatalogService service = new PortalCatalogService();
            if (!Equals(filters, null) && filters.Count > 0)
            {
                foreach (var tuple in filters)
                {
                    if (!Equals(filters, null) && Equals(filters.FirstOrDefault().Item1, "username"))
                    {
                        var list = service.GetPortalCatalogs(null, filters, new NameValueCollection(), new NameValueCollection());
                        if (!Equals(list, null) && !Equals(list.PortalCatalogs, null) && list.PortalCatalogs.Count > 0)
                        {
                            catalogIds = string.Join(",", list.PortalCatalogs.ToList().Select(x => x.CatalogId.ToString()));
                        }
                    }
                }
            }
            return catalogIds;
        }

        #endregion
    }

}
