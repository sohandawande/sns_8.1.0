﻿using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    public interface ICategoryProfileService
    {
        /// <summary>
        /// Gets the profile details by category profile id.
        /// </summary>
        /// <param name="categoryProfileId">The id of category profile</param>
        /// <returns>Returns model of type CategoryProfileModel.</returns>
        CategoryProfileModel GetCategoryProfileByCategoryProfileId(int categoryProfileId);

        /// <summary>
        /// Creates the profile associated to category.
        /// </summary>
        /// <param name="model">Model of type CategoryProfileModel.</param>
        /// <returns>Returns model of type CategoryProfileModel.</returns>
        CategoryProfileModel Create(CategoryProfileModel model);

        /// <summary>
        /// Gets category profile by category id.
        /// </summary>
        /// <param name="categoryId">The id of the category</param>
        /// <returns>Returns the mdoel of type CategoryProfileListModel.</returns>
        CategoryProfileListModel GetCategoryProfileByCategoryId(int categoryId);

        /// <summary>
        /// This method will update the category profile data
        /// </summary>
        /// <param name="categoryProfileId">int categoryProfileId</param>
        /// <param name="model">CategoryProfileModel model</param>
        /// <returns>Returns true if data updated</returns>
        bool UpdateCategoryProfile(int categoryProfileId, CategoryProfileModel model);

        /// <summary>
        /// This method will delete the record for the specific category profile Id
        /// </summary>
        /// <param name="categoryProfileId">int categoryProfileId</param>
        /// <returns>True if records deleted</returns>
        bool DeleteCategoryProfile(int categoryProfileId);
    }
}
