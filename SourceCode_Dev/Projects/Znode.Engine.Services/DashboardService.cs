﻿using System;
using System.Data;
using Znode.Engine.Api.Models;
using Znode.Libraries.Helpers.Extensions;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;

namespace Znode.Engine.Services
{
    public class DashboardService : BaseService, IDashboardService
    {
        #region Private Variables
        private readonly DashboardAdmin dashboardAdmin;
        #endregion

        #region Default Constructor
        public DashboardService()
        {
            dashboardAdmin = new DashboardAdmin();
        }
        #endregion

        #region Public Methods
        public DashboardModel GetDashboardItemsByPortalId(int portalId)
        {
            if (portalId < 1)
            {
                throw new Exception("Portal ID cannot be less than 1.");
            }

            DashboardModel model = new DashboardModel();

            DashboardHelper dash = new DashboardHelper();
            DataSet resultDataSet = dash.GetDashboardItemsByPortal(portalId);

            if (!Equals(resultDataSet, null) && resultDataSet.Tables.Count > 0 && !Equals(resultDataSet.Tables[0], null) && resultDataSet.Tables[0].Rows.Count > 0)
            {
                model = resultDataSet.Tables[0].Rows[0].ToSingleRow<DashboardModel>();
            }

            return model;
        } 
        #endregion
    }
}
