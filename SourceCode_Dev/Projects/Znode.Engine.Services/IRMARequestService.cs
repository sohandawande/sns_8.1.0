﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    public interface IRMARequestService
    {
        /// <summary>
        /// Gets list of RMA request.
        /// </summary>
        /// <param name="expands">Expands for RMA request list.</param>
        /// <param name="filters">Filters for RMA request list.</param>
        /// <param name="sorts">Sorting of RMA request list.</param>
        /// <param name="page">Paging of RMA request.</param>
        /// <returns>List of RMA request.</returns>
        RMARequestListModel GetRMARequestList(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Updates an RMA request.
        /// </summary>
        /// <param name="rmaRequestId">RMA request ID.</param>
        /// <param name="model">RMA Request model.</param>
        /// <returns>Returns RMARequestModel</returns>
        RMARequestModel UpdateRMARequest(int rmaRequestId, RMARequestModel model);

        /// <summary>
        /// Gets an RMA request model.
        /// </summary>
        /// <param name="rmaRequestId">RMA Request Id.</param>
        /// <returns></returns>
        RMARequestModel GetRMARequest(int rmaRequestId);

        /// <summary>
        /// To Get Order RMA Display Flag by Order Id.
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        bool GetOrderRMAFlag(int orderId);

        /// <summary>
        /// Deletes an RMA Request.
        /// </summary>
        /// <param name="rmaRequestId">RMA Request ID.</param>
        /// <returns>Returns true or false.</returns>
        bool DeleteRMARequest(int rmaRequestId);

        /// <summary>
        /// Return RMA Gift Card details.
        /// </summary>
        /// <param name="rmaRequestId">RMA Request ID.</param>
        /// <returns>List of IssuedGiftCardModel.</returns>
        IssuedGiftCardListModel GetRMAGiftCardDetails(int rmaRequestId);

        /// <summary>
        /// Creates an RMA request.
        /// </summary>
        /// <param name="rmaRequestModel">RMA Request model.</param>
        /// <returns>RMA Request model.</returns>
        RMARequestModel CreateRMARequest(RMARequestModel rmaRequestModel);

        /// <summary>
        /// Sends email to customer.
        /// </summary>
        /// <param name="RMAItemId">Rma Item Id.</param>
        /// <returns>Returns true or false value showing if the mail is sent or not.</returns>
        bool SendStatusMail(int RMAItemId);

        /// <summary>
        /// Send Gift card mail.
        /// </summary>
        /// <param name="model">Gift card model from where email data will be retrieved.</param>
        /// <returns>Returns true or false value according to email sent status.</returns>
        bool SendMail_Giftcard(GiftCardModel model);
    }
}
