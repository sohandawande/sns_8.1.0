﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    /// <summary>
    /// Interface for Tax Class Service
    /// </summary>
	public interface ITaxClassService
	{
        /// <summary>
        /// Get Tax Class
        /// </summary>
        /// <param name="taxClassId">taxClassId</param>
        /// <returns>Returns TaxClassModel</returns>
		TaxClassModel GetTaxClass(int taxClassId);

        /// <summary>
        /// Get Tax Classes
        /// </summary>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts for NameValueCollection</param>
        /// <param name="page">page for NameValueCollection</param>
        /// <returns>Returns TaxClassListModel</returns>
		TaxClassListModel GetTaxClasses(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// CreateTaxClass
        /// </summary>
        /// <param name="model">TaxClassModel</param>
        /// <returns>Returns TaxClassModel</returns>
		TaxClassModel CreateTaxClass(TaxClassModel model);

        /// <summary>
        /// Update Tax Class
        /// </summary>
        /// <param name="taxClassId">taxClassId</param>
        /// <param name="model">TaxClassModel</param>
        /// <returns>Returns TaxClassModel</returns>
		TaxClassModel UpdateTaxClass(int taxClassId, TaxClassModel model);

        /// <summary>
        /// Delete Tax Class
        /// </summary>
        /// <param name="taxClassId">taxClassId</param>
        /// <returns>Returns True/False</returns>
		bool DeleteTaxClass(int taxClassId);
        /// <summary>
        /// To get Active Tax Class by PortalId
        /// </summary>
        /// <param name="portalId">int PortalId</param>
        /// <returns>List of active tax class having portalId equal to supplied portalId or null</returns>
        TaxClassListModel GetActiveTaxClassByPortalId(int portalId);

        /// <summary>
        /// View Tax Class Details
        /// </summary>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts for NameValueCollection</param>
        /// <param name="page">page for NameValueCollection</param>
        /// <returns>Returns TaxClassModel</returns>
        TaxClassModel ViewTaxClassDetails(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Gets the list of all tax classes
        /// </summary>
        /// <param name="expands">NameValueCollection expands</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">NameValueCollection sorts</param>
        /// <param name="page"> NameValueCollection page</param>
        /// <returns>Returns list of tax classes</returns>
        TaxClassListModel GetTaxClassList(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
	}
}
