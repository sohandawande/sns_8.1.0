﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Helpers;
using Znode.Libraries.Helpers.Constants;
using Znode.Libraries.Helpers.Extensions;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using SkuProfileEffectiveRepository = ZNode.Libraries.DataAccess.Service.SKUProfileEffectiveService;

namespace Znode.Engine.Services
{
    public class SkuProfileEffectiveService : BaseService, ISkuProfileEffectiveService
    {
        #region Private Variables

        private readonly SkuProfileEffectiveRepository _skuProfileEffectiveRepository;
        private readonly SKUAdmin skuAdmin;
        private readonly ProfileAdmin profileAdmin;

        #endregion

        #region Constructor

        public SkuProfileEffectiveService()
        {
            _skuProfileEffectiveRepository = new SkuProfileEffectiveRepository();
            skuAdmin = new SKUAdmin();
            profileAdmin = new ProfileAdmin();
        }

        #endregion

        #region Public Methods

        public SkuProfileEffectiveModel GetSkuProfileEffective(int skuProfileEffective, NameValueCollection expands)
        {
            var model = _skuProfileEffectiveRepository.GetBySkuProfileEffectiveID(skuProfileEffective);
            if (!Equals(model, null))
            {
                GetExpands(expands, model);
            }

            return SkuProfileEffectiveMap.ToModel(model);
        }

        public SkuProfileEffectiveListModel GetSkuProfileEffectives(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new SkuProfileEffectiveListModel();
            var SkuProfileEffectives = new TList<SKUProfileEffective>();
            var tempList = new TList<SKUProfileEffective>();

            var query = new SKUProfileEffectiveQuery();
            var sortBuilder = new SKUProfileEffectiveSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            tempList = _skuProfileEffectiveRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (pagingLength.Equals(int.MaxValue))
            {
                model.PageSize = model.TotalResults;
            }

            // Now go through the list and get any expands
            foreach (var skuProfileEffective in tempList)
            {
                GetExpands(expands, skuProfileEffective);
                SkuProfileEffectives.Add(skuProfileEffective);
            }

            // Map each item and add to the list
            foreach (var h in SkuProfileEffectives)
            {
                model.SkuProfileEffectives.Add(SkuProfileEffectiveMap.ToModel(h));
            }

            return model;
        }

        public SkuProfileEffectiveListModel GetSkuProfileEffectiveBySkuId(int skuId)
        {
            SkuProfileEffectiveListModel listModel = new SkuProfileEffectiveListModel();
            DataSet list = skuAdmin.GetSkuProfileEffectiveBySkuID(skuId);
            listModel.SkuProfileEffectives = list.Tables[0].ToList<SkuProfileEffectiveModel>().ToCollection();

            foreach (var item in listModel.SkuProfileEffectives)
            {
                item.Name = this.GetProfileNameById(item.ProfileId);
            }

            return listModel;
        }

        public SkuProfileEffectiveListModel GetSkuProfileEffectivesBySkuId(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            SkuProfileEffectiveListModel model = new SkuProfileEffectiveListModel();
            var sortBuilder = new SKUProfileEffectiveSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;
            int totalRowCount = 0;
            string whereClause = StringHelper.GenerateDynamicWhereClause(filters);
            
            SetSorting(sorts, sortBuilder);
            string orderBy = sortBuilder.ToString();

            StoredProcedureHelper storedProcedureHelper = new StoredProcedureHelper();
            DataSet resultDataSet = storedProcedureHelper.SP_DataDetailsSpConfig(whereClause, string.Empty/*HavingClause*/, StoredProcedureKeys.spGetSKUProfileEffective, orderBy, pagingStart.ToString(), pagingLength.ToString(), out totalRowCount);

            model.SkuProfileEffectives = resultDataSet.Tables[0].ToList<SkuProfileEffectiveModel>().ToCollection();
            model.TotalResults = totalRowCount;
            model.PageSize = pagingLength;
            model.PageIndex = pagingStart;
            // Check to set page size equal to the total number of results
            if (pagingLength.Equals(int.MaxValue))
            {
                model.PageSize = model.TotalResults;
            }

            return model;
        }

        public SkuProfileEffectiveModel CreateSkuProfileEffective(SkuProfileEffectiveModel model)
        {
            if (Equals(model, null))
            {
                throw new Exception("Sku Profile Effective model cannot be null.");
            }

            var entity = SkuProfileEffectiveMap.ToEntity(model);
            var skuProfileEffective = _skuProfileEffectiveRepository.Save(entity);
            return SkuProfileEffectiveMap.ToModel(skuProfileEffective);
        }

        public SkuProfileEffectiveModel UpdateSkuProfileEffective(int skuProfileEffectiveId, SkuProfileEffectiveModel model)
        {
            if (skuProfileEffectiveId < 1)
            {
                throw new Exception("Sku Profile Effective ID cannot be less than 1.");
            }

            if (Equals(model, null))
            {
                throw new Exception("Sku Profile Effective model cannot be null.");
            }

            var skuProfileEffective = _skuProfileEffectiveRepository.GetBySkuProfileEffectiveID(skuProfileEffectiveId);
            if (!Equals(skuProfileEffective, null))
            {
                // Set Sku Profile Effective ID
                model.SkuProfileEffectiveID = skuProfileEffectiveId;

                var skuProfileEffectiveToUpdate = SkuProfileEffectiveMap.ToEntity(model);

                var updated = _skuProfileEffectiveRepository.Update(skuProfileEffectiveToUpdate);
                if (updated)
                {
                    skuProfileEffective = _skuProfileEffectiveRepository.GetBySkuProfileEffectiveID(skuProfileEffectiveId);
                    return SkuProfileEffectiveMap.ToModel(skuProfileEffective);
                }
            }
            return null;
        }

        public bool DeleteSkuProfileEffective(int skuProfileEffectiveId)
        {
            if (skuProfileEffectiveId < 1)
            {
                throw new Exception("skuProfileEffective ID cannot be less than 1.");
            }

            var skuProfileEffective = _skuProfileEffectiveRepository.GetBySkuProfileEffectiveID(skuProfileEffectiveId);

            if (!Equals(skuProfileEffective, null))
            {
                return _skuProfileEffectiveRepository.Delete(skuProfileEffectiveId);
            }

            return false;
        }

        #endregion

        #region Private Methods

        private void GetExpands(NameValueCollection expands, SKUProfileEffective skuProfileEffective)
        {
            if (expands.HasKeys())
            {
            }
        }

        private void SetFiltering(List<Tuple<string, string, string>> filters, SKUProfileEffectiveQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;

                if (FilterKeys.SkuProfileEffectiveID.Equals(filterKey)) { SetQueryParameter(SKUProfileEffectiveColumn.SkuProfileEffectiveID, filterOperator, filterValue, query); }
                if (FilterKeys.SkuId.Equals(filterKey)) SetQueryParameter(SKUProfileEffectiveColumn.SkuId, filterOperator, filterValue, query);
                if (FilterKeys.ProfileId.Equals(filterKey)) SetQueryParameter(SKUProfileEffectiveColumn.ProfileId, filterOperator, filterValue, query);
            }
        }

        private void SetSorting(NameValueCollection sorts, SKUProfileEffectiveSortBuilder sortBuilder)
        {
            if (sorts.HasKeys())
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);

                    if (key.Equals(SortKeys.SkuProfileEffectiveID)) { SetSortDirection(SKUProfileEffectiveColumn.SkuProfileEffectiveID, value, sortBuilder); }
                    if (key.Equals(SortKeys.SkuId)) { SetSortDirection(SKUProfileEffectiveColumn.SkuId, value, sortBuilder); }
                    if (key.Equals(SortKeys.ProfileId)) { SetSortDirection(SKUProfileEffectiveColumn.ProfileId, value, sortBuilder); }
                }
            }
        }

        private void SetQueryParameter(SKUProfileEffectiveColumn column, string filterOperator, string filterValue, SKUProfileEffectiveQuery query)
        {
            base.SetQueryParameter(column, filterOperator, filterValue, query);
        }

        private void SetSortDirection(SKUProfileEffectiveColumn column, string value, SKUProfileEffectiveSortBuilder sortBuilder)
        {
            base.SetSortDirection(column, value, sortBuilder);
        }

        /// <summary>
        /// Get profile name by profile id.
        /// </summary>
        /// <param name="profileId">Id of the profile.</param>
        /// <returns>Name of the profile.</returns>
        private string GetProfileNameById(int profileId)
        {
            return profileAdmin.GetProfileNameByProfileID(profileId);
        }

        #endregion
    }
}
