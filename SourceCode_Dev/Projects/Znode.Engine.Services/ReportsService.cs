﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Dynamic;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Libraries.Helpers;
using ZNode.Libraries.DataAccess.Custom;

namespace Znode.Engine.Services
{
    public class ReportsService : BaseService, IReportsService
    {
        #region Public Methods
        public ReportsDataModel GetReportDataSet(string reportName, NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            List<Tuple<string, string, string>> filterList = new List<Tuple<string, string, string>>();
            ReportsDataModel model = new ReportsDataModel();

            int totalRowCount = 0;
            int pagingStart = 0;
            int pagingLength = 0;
            string orderBy = string.Empty;
            string innerWhereClause = string.Empty;
            foreach (var item in filters)
            {
                if (Equals(item.Item1, "username"))
                {
                    filterList.Add(item);
                    innerWhereClause = StringHelper.GenerateDynamicWhereClause(filterList);
                    filters.Remove(item);
                    break;
                }
            }
            pagingStart = Convert.ToInt32(page.Get(PageKeys.Index));
            pagingLength = Convert.ToInt32(page.Get(PageKeys.Size));

            orderBy = string.Format("{0} {1}", sorts["sort"], sorts["sortDir"]);

            StoredProcedureHelper storedProcedureHelper = new StoredProcedureHelper();
            if (Equals(reportName, "ZNode_ReportsProductSoldOnVendorSites"))
            {
                foreach (Tuple<string, string, string> tuple in filters)
                {
                    if (tuple.Item1.Equals("accountid", StringComparison.InvariantCultureIgnoreCase))
                    {
                        string filterValue = tuple.Item3;
                        innerWhereClause = string.Format("{0}{1}", "~~accountid~~ = ", filterValue);
                        filters.Remove(tuple);
                        break;
                    }
                }
            }

            reportName = Equals(reportName, "ZNode_ReportsTopEarningProduct") ? "ZNode_ReportsPopularFiltered" : reportName;
            var _isChart = filters.Find(x => x.Item1.Equals("ischart"));
            if (!Equals(_isChart, null))
            {
                reportName = string.Format("{0}{1}", "@", reportName);
                filters.Remove(_isChart);
            }
            RemoveFilterKey(filters, "durationid");

            string whereClause = StringHelper.GenerateDynamicWhereClause(filters);

            DataSet resultDataSet = storedProcedureHelper.SP_DataDetailsSpConfig(whereClause, null, reportName, orderBy, pagingStart.ToString(), pagingLength.ToString(), out totalRowCount, null, innerWhereClause);

            if (!Equals(resultDataSet, null) && !Equals(resultDataSet.Tables.Count, 0))
                model.RecordSet = DataTableToList(resultDataSet.Tables[0]);

            model.TotalResults = totalRowCount;
            model.PageSize = pagingLength;
            model.PageIndex = pagingStart;

            // check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                model.PageSize = model.TotalResults;
            }
            return model;
        }

        public ReportsDataModel GetOrderDetails(int orderId, string reportName)
        {
            ReportsDataModel model = new ReportsDataModel();
            ReportHelper storedProcedureHelper = new ReportHelper();

            DataSet resultDataSet = storedProcedureHelper.GetOrderDetails(orderId, reportName);

            if (!Equals(resultDataSet, null) && !Equals(resultDataSet.Tables.Count, 0))
                model.RecordSet = DataTableToList(resultDataSet.Tables[0]);

            return model;
        }

        /// <summary>
        /// Get column table format to dynamic list with dateFormat
        /// </summary>
        /// <param name="dataTable">DataTable</param>
        /// <returns>Returns dynamic list</returns>
        public static List<dynamic> DataTableToList(DataTable dataTable)
        {
            var result = new List<dynamic>();
            try
            {
                if (!Equals(dataTable, null) && dataTable.Rows.Count > 0)
                {
                    foreach (DataRow row in dataTable.Rows)
                    {
                        var obj = (IDictionary<string, object>)new ExpandoObject();
                        foreach (DataColumn col in dataTable.Columns)
                        {
                            obj.Add(col.ColumnName, row[col.ColumnName]);
                        }
                        result.Add(obj);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            finally
            {
                dataTable.Dispose();
            }

            return result;
        }
        #endregion

        #region Private Methods

        /// <summary>
        /// Remove extra filter from filter collection
        /// </summary>
        /// <param name="filters">Filter collection</param>
        /// <param name="keyName">Key name</param>
        private void RemoveFilterKey(List<Tuple<string, string, string>> filters, string keyName)
        {
            foreach (Tuple<string, string, string> tuple in filters)
            {
                if (tuple.Item1.Equals(keyName, StringComparison.InvariantCultureIgnoreCase))
                {
                    filters.Remove(tuple);
                    break;
                }
            }
        }
        #endregion
    }
}



