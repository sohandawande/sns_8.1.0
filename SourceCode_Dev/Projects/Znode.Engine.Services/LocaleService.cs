﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using LocaleRepository = ZNode.Libraries.DataAccess.Service.LocaleService;

namespace Znode.Engine.Services
{
    /// <summary>
    /// Locale Service
    /// </summary>
    public class LocaleService : BaseService, ILocaleService
    {
        #region Private Variables
        private readonly LocaleRepository _localeRepository;
        #endregion

        #region Constructor
        public LocaleService()
        {
            _localeRepository = new LocaleRepository();

        }
        
        #endregion

        #region Public Methods
        public LocaleListModel GetLocales(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new LocaleListModel();
            var locales = new TList<Locale>();
            var tempList = new TList<Locale>();

            var query = new LocaleQuery();
            var sortBuilder = new LocaleSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            tempList = _localeRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (Equals(pagingLength, int.MaxValue))
            {
                model.PageSize = model.TotalResults;
            }

            // Now go through the list and get any expands
            foreach (var locale in tempList)
            {               
                locales.Add(locale);
            }

            // Map each item and add to the list
            foreach (var a in locales)
            {
                model.Locales.Add(LocaleMap.ToModel(a));
            }

            return model;
        } 
        #endregion

        #region Private Methods
        private void SetFiltering(List<Tuple<string, string, string>> filters, LocaleQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;

                if (Equals(filterKey, FilterKeys.LocaleCode)) { SetQueryParameter(LocaleColumn.LocaleCode, filterOperator, filterValue, query); }
                if (Equals(filterKey, FilterKeys.LocaleId)) { SetQueryParameter(LocaleColumn.LocaleID, filterOperator, filterValue, query); }
            }
        }
        private void SetSorting(NameValueCollection sorts, LocaleSortBuilder sortBuilder)
        {
            if (sorts.HasKeys())
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);

                    if (Equals(key, SortKeys.LocaleCode)) { SetSortDirection(LocaleColumn.LocaleCode, value, sortBuilder); }
                }
            }
        }
      
        #endregion
    }
}

