﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using Znode.Engine.Suppliers;
using Znode.Libraries.Helpers;
using Znode.Libraries.Helpers.Constants;
using Znode.Libraries.Helpers.Extensions;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Analytics;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.ECommerce.Fulfillment;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.Google;
using AccountRepository = ZNode.Libraries.DataAccess.Service.AccountService;
using AddressRepository = ZNode.Libraries.DataAccess.Service.AddressService;
using CategoryRepository = ZNode.Libraries.DataAccess.Service.CategoryService;
using DigitalAssetRepository = ZNode.Libraries.DataAccess.Service.DigitalAssetService;
using OrderAdminRepository = ZNode.Libraries.Admin.OrderAdmin;
using OrderHelperRepository = ZNode.Libraries.DataAccess.Custom.OrderHelper;
using OrderLineItemRepository = ZNode.Libraries.DataAccess.Service.OrderLineItemService;
using OrderRepository = ZNode.Libraries.DataAccess.Service.OrderService;
using OrderShipmentRepository = ZNode.Libraries.DataAccess.Service.OrderShipmentService;
using PaymentSettingRepository = ZNode.Libraries.DataAccess.Service.PaymentSettingService;
using PaymentTypeRepository = ZNode.Libraries.DataAccess.Service.PaymentTypeService;
using PortalProfileRepository = ZNode.Libraries.DataAccess.Service.PortalProfileService;
using PortalRepository = ZNode.Libraries.DataAccess.Service.PortalService;
using ProfileRepository = ZNode.Libraries.DataAccess.Service.ProfileService;
using ReferralCommissionAdminRepository = ZNode.Libraries.Admin.ReferralCommissionAdmin;
using RMAAdminRepository = ZNode.Libraries.DataAccess.Custom.RMAHelper;
using ShippingRepository = ZNode.Libraries.DataAccess.Service.ShippingService;
using ZNodeShoppingCartItem = ZNode.Libraries.ECommerce.ShoppingCart.ZNodeShoppingCartItem;

namespace Znode.Engine.Services
{
    public partial class OrderService : BaseService, IOrderService
    {
        #region Private Variables
        private readonly AccountRepository _accountRepository;
        private readonly CategoryRepository _categoryRepository;
        private readonly DigitalAssetRepository _digitalAssetRepository;
        private readonly OrderLineItemRepository _orderLineItemRepository;
        private readonly OrderRepository _orderRepository;
        private readonly OrderShipmentRepository _orderShipmentRepository;
        private readonly PaymentSettingRepository _paymentSettingRepository;
        private readonly PaymentTypeRepository _paymentTypeRepository;
        private readonly PortalRepository _portalRepository;
        private readonly PortalProfileRepository _portalProfileRepository;
        private readonly ProfileRepository _profileRepository;
        private readonly ShippingRepository _shippingRepository;
        private readonly AddressRepository _addressRepository;
        private readonly RMAAdminRepository _rMAAdminRepository;
        private int orderShippingTypeIdWithValue2 = 2;
        private int orderShippingTypeIdWithValue3 = 3;
        #endregion

        #region Constructor
        public OrderService()
        {
            _accountRepository = new AccountRepository();
            _categoryRepository = new CategoryRepository();
            _digitalAssetRepository = new DigitalAssetService();
            _orderLineItemRepository = new OrderLineItemRepository();
            _orderRepository = new OrderRepository();
            _orderShipmentRepository = new OrderShipmentRepository();
            _paymentSettingRepository = new PaymentSettingService();
            _paymentTypeRepository = new PaymentTypeRepository();
            _portalRepository = new PortalRepository();
            _portalProfileRepository = new PortalProfileService();
            _profileRepository = new ProfileRepository();
            _shippingRepository = new ShippingRepository();
            _addressRepository = new AddressRepository();
            _rMAAdminRepository = new RMAAdminRepository();
        }
        #endregion

        #region Public Methods
        public OrderModel GetOrder(int orderId, NameValueCollection expands)
        {
            var order = _orderRepository.GetByOrderID(orderId);
            if (!Equals(order, null))
            {
                GetExpands(expands, order);
                return OrderMap.ToModel(order);
            }
            return null;
        }

        public OrderListModel GetOrders(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new OrderListModel();
            var orders = new TList<Order>();
            var tempList = new TList<Order>();

            var query = new OrderQuery();
            var sortBuilder = new OrderSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            tempList = _orderRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (Equals(pagingLength, int.MaxValue))
            {
                model.PageSize = model.TotalResults;
            }

            // Now go through the list and get any expands
            foreach (var order in tempList)
            {
                GetExpands(expands, order);
                orders.Add(order);
            }

            // Map each item and add to the list
            foreach (var o in orders)
            {
                model.Orders.Add(OrderMap.ToModel(o));
            }

            return model;
        }

        public OrderModel CreateOrder(int portalId, ShoppingCartModel model)
        {
            if (Equals(model, null))
            {
                throw new Exception("Shopping cart model cannot be null.");
            }

            var log = new ZNodeLogging();

            ZNodeUserAccount userAccount;

            var shippingAddress = (model.MultipleShipToEnabled) ? model.ShoppingCartItems.First().ShippingAddress : model.ShippingAddress;

            // Check to create user account from model or with the default for anonymous users
            if (!Equals(model.Account, null))
            {
                userAccount = new ZNodeUserAccount(AccountMap.ToEntity(model.Account));
                userAccount.SetBillingAddress(AddressMap.ToEntity(model.Payment.BillingAddress));
                userAccount.SetShippingAddress(AddressMap.ToEntity(shippingAddress));
            }
            else
            {
                userAccount = new ZNodeUserAccount();
                userAccount.AddUserAccount();
                userAccount.SetBillingAddress(AddressMap.ToEntity(model.Payment.BillingAddress));
                userAccount.SetShippingAddress(AddressMap.ToEntity(shippingAddress));
                userAccount.EmailID = model.BillingEmail;
            }

            // Add the user account to session so that it's there when ZNodeCheckout is created
            userAccount.AddToSession(ZNodeSessionKeyType.UserAccount);

            var profileId = GetProfileIdForCheckout(portalId, userAccount);
            var isDefaultPortal = IsDefaultPortal(portalId);

            //Znode Version 7.2.2 Multi cart shipping - Start
            if (!Equals(model.OrderShipment, null))
            {
                var multiShipModel = model.OrderShipment;
                //Multiship 
                model.ShoppingCartItems.ToList().ForEach(x =>
                {
                    x.MultipleShipToAddress.Clear();
                    int count = 1;
                    string cartAddOnValueIds = string.Empty;
                    multiShipModel.ForEach(shippingModel =>
                    {                        
                        if (x.Sku.Equals(shippingModel.SKU))
                        {
                            if (!Equals(x.AddOnValueIds, null))
                            {
                                for (int index = 0; index < x.AddOnValueIds.Length; index++)
                                {
                                    cartAddOnValueIds = string.IsNullOrEmpty(cartAddOnValueIds)?x.AddOnValueIds[index].ToString():cartAddOnValueIds + "," + x.AddOnValueIds[index].ToString();                                                                        
                                }
                                if (Equals(shippingModel.AddOnValueIds,cartAddOnValueIds))
                                {
                                    x.MultipleShipToAddress.Add(new OrderShipmentModel() { AddressId = int.Parse(shippingModel.addressid), Quantity = shippingModel.quantity, OrderShipmentId = count, ShippingOptionId = shippingModel.ShippingId, ShippingName = shippingModel.ShippingCode });
                                    count++;
                                }
                                cartAddOnValueIds = string.Empty;                              
                            }
                            else
                            {
                                x.MultipleShipToAddress.Add(new OrderShipmentModel() { AddressId = int.Parse(shippingModel.addressid), Quantity = shippingModel.quantity, OrderShipmentId = count, ShippingOptionId = shippingModel.ShippingId, ShippingName = shippingModel.ShippingCode });
                                count++;
                            }
                        }
                    });


                });

                model.MultipleShipToEnabled = true;
            }
            //Multi cart shipping - End 

            // Create the shopping cart and add to session so that it's there when ZNodeCheckout is created

            var znodeShoppingCart = ShoppingCartMap.ToZnodeShoppingCart(model);

            znodeShoppingCart.AddToSession(ZNodeSessionKeyType.ShoppingCart);

            // Create the checkout object
            var checkout = CheckoutMap.ToZnodeCheckout(userAccount, znodeShoppingCart);

            if (Equals(checkout, null) || Equals(checkout.ShoppingCart, null))
            {
                log.LogActivityTimerEnd((int)ZNodeLogging.ErrorNum.OrderSubmissionFailed, null);
                ZNodeLogging.LogMessage("Unable to process your order.");
                throw new Exception("Unable to process your order.");
            }
            // assign gift card values
            checkout.ShoppingCart.GiftCardAmount = model.GiftCardAmount;
            checkout.ShoppingCart.GiftCardMessage = model.GiftCardMessage;
            checkout.ShoppingCart.GiftCardNumber = model.GiftCardNumber;
            checkout.ShoppingCart.IsGiftCardApplied = model.GiftCardApplied;
            checkout.ShoppingCart.IsGiftCardValid = model.GiftCardValid;

            //checkout.ShoppingCart.Coupon = model.Coupon;
            foreach (CouponModel coupon in model.Coupons)
            {
                checkout.ShoppingCart.Coupons.Add(CouponMap.ToZnodeCoupon(coupon));
            }

            checkout.ShoppingCart.PortalID = portalId;
            checkout.ShoppingCart.VAT = model.Vat.GetValueOrDefault();
            checkout.ShoppingCart.HST = model.Hst.GetValueOrDefault();
            checkout.ShoppingCart.GST = model.Gst.GetValueOrDefault();
            checkout.ShoppingCart.PST = model.Pst.GetValueOrDefault();
            checkout.ShoppingCart.Payment = PaymentMap.ToZnodePayment(model.Payment, shippingAddress);
            checkout.AdditionalInstructions = model.AdditionalInstructions;
            checkout.ShoppingCart.AdditionalInstructions = znodeShoppingCart.AdditionalInstructions; //PRFT Custom Code
            checkout.PurchaseOrderNumber = model.PurchaseOrderNumber;
            checkout.PortalID = portalId;
            checkout.ShoppingCart.Shipping = ShippingMap.ToZnodeShipping(model.Shipping);
            checkout.ShippingID = checkout.ShoppingCart.Shipping.ShippingID;
            checkout.CustomerExternalId = model.CustomerExternalAccountId;//PRFT Custom Code
            if (model.Total > 0)
            {
                checkout.PaymentSettingID = model.Payment.PaymentOption.PaymentOptionId;
                GetPaymentOptionForCheckout(checkout, model, profileId, isDefaultPortal, log);
            }
            else
            {
                checkout.PaymentSettingID = 0;
                checkout.ShoppingCart.Payment.PaymentName = String.Empty;
                checkout.ShoppingCart.Payment.PaymentSetting = null;
            }

            //Check order is multiple shipping - Start
            if (!Equals(model.OrderShipment, null))
            {
                // Do the cart calculation			
                checkout.ShoppingCart.AddressCarts.ForEach(x =>
                    {
                        x.Shipping = string.IsNullOrEmpty(x.Shipping.ShippingName) ? new ZNodeShipping
                            {
                                ShippingID = checkout.ShoppingCart.Shipping.ShippingID,
                                ShippingName = checkout.ShoppingCart.Shipping.ShippingName
                            } : x.Shipping;
                        var address = _addressRepository.GetByAddressID(x.AddressID);
                        checkout.ShoppingCart.Payment = PaymentMap.ToZnodePayment(model.Payment, AddressMap.ToModel(address));
                        x.Payment = checkout.ShoppingCart.Payment;
                        //PRFT Custom Code:Start
                        x.CustomerExternalId = checkout.ShoppingCart.CustomerExternalId;
                        //PRFT Custom Code:End
                        x.Calculate();
                    });
            }
            else
            {
                // Do the cart calculation			
                checkout.ShoppingCart.AddressCarts.ForEach(x =>
                {
                    x.Shipping = string.IsNullOrEmpty(x.Shipping.ShippingName) ? new ZNodeShipping
                    {
                        ShippingID = checkout.ShoppingCart.Shipping.ShippingID,
                        ShippingName = checkout.ShoppingCart.Shipping.ShippingName
                    } : x.Shipping;
                    x.Payment = checkout.ShoppingCart.Payment;
                    //PRFT Custom Code:Start
                    x.CustomerExternalId = checkout.ShoppingCart.CustomerExternalId;
                    //PRFT Custom Code:End

                    x.Calculate();
                });
            }
            //Check order is multiple shipping - End
            if (!Equals(model.PortalId, null))
            {
                checkout.ShoppingCart.PortalId = model.PortalId;
            }

            checkout.ShoppingCart.Calculate();

            // Perform validation and start the timer
            ValidateCheckout(checkout);
            log.LogActivityTimerStart();

            // Instantiate the order fullfillment
            var order = new ZNodeOrderFulfillment();

            try
            {
                // Do pre-submit processing
                var preSubmitOrderSuccess = checkout.ShoppingCart.PreSubmitOrderProcess();
                if (preSubmitOrderSuccess)
                {
                    //PRFT Custom Code : Start
                    order.Custom1 = model.CustomerExternalAccountId;// saving customer external accountid
                    //PRFT Custom Code : End

                    // Now submit order to add to our system, passing true flag indicating this is from the API
                    order = checkout.SubmitOrder(true);
                }
                else
                {
                    log.LogActivityTimerEnd((int)ZNodeLogging.ErrorNum.OrderSubmissionFailed, null);
                    ZNodeLogging.LogMessage("Pre-submit order processing failed.");
                    throw new Exception("Pre-submit order processing failed.");
                }
            }
            catch (Exception ex)
            {
                log.LogActivityTimerEnd((int)ZNodeLogging.ErrorNum.OrderSubmissionFailed, null);
                ZNodeLogging.LogMessage("General submit order error: " + ex.Message);
                throw new Exception("General submit order error: " + ex.Message);
            }

            // If checkout successful then do post-submit processing
            if (checkout.IsSuccess)
            {
                if (!Equals(model.Account.ReferralAccountId, null) && model.Account.ReferralAccountId > 0)
                {
                    HttpContext.Current.Session.Add("ZnodeStorefrontAffiliateId", model.Account.ReferralAccountId);
                }

                order.Custom3 = model.CurrentURL;
                // TODO: Supposedly WorldPay logic went here, but we're not supporting it in API 2.0

                // Check to create tracking event if affiliate exists
                DoAffiliateTracking(order);

                //PRFT Custom Code : Start
                //Submit Order To ERP
                PRFTERPOrderServices _orderERPService = new PRFTERPOrderServices();
                var response = _orderERPService.SubmitOrderToERP(order, checkout.ShoppingCart);

                if (response.HasError == false && !string.IsNullOrEmpty(response.NewOrderID))
                {
                    UpdateOrderExternalId(order.OrderID, response.NewOrderID);
                    order.ExternalId = response.NewOrderID;
                }
                else
                {
                    try
                    {
                        //Send Email to Admin
                        SendFailuerEmail(order, checkout.ShoppingCart, response.StatusDescription);
                    }
                    catch (Exception ex) { }
                }
                //PRFT Custom Code : END

                // Do post submit processing
                PostSubmitOrder(order, checkout, model.FeedbackUrl);
                log.LogActivityTimerEnd((int)ZNodeLogging.ErrorNum.OrderSubmissionSuccess, order.OrderID.ToString());
            }
            else
            {
                log.LogActivityTimerEnd((int)ZNodeLogging.ErrorNum.OrderSubmissionFailed, null, null, null, null, checkout.PaymentResponseText);
                throw new Exception(checkout.PaymentResponseText);
            }

            // If we get to here we're good, so setup the expands to retrieve the newly created order
            var expands = new NameValueCollection
			{
				{ ExpandKeys.Account, ExpandKeys.Account },
				{ ExpandKeys.OrderLineItems, ExpandKeys.OrderLineItems },
				{ ExpandKeys.PaymentOption, ExpandKeys.PaymentOption },
				{ ExpandKeys.PaymentType, ExpandKeys.PaymentType },
				{ ExpandKeys.ShippingOption, ExpandKeys.ShippingOption }
			};

            // Now get the newly created order
            var newlyCreatedOrder = GetOrder(order.OrderID, expands);

            // And finally attach the receipt HTML to the order and return
            var receipt = new ZnodeReceipt(order, checkout.ShoppingCart) { FromApi = true, ApiShoppingCart = checkout.ShoppingCart, FeedbackUrl = model.FeedbackUrl };
            #region PRFT Custom Code
            // newlyCreatedOrder.ReceiptHtml = receipt.GetHtmlReceiptForUI();
            string ReceiptHtml = receipt.GetHtmlReceiptForUI();
            ZnodeSupplierEmail znodeSupplierEmail = new ZnodeSupplierEmail();
            newlyCreatedOrder.ReceiptHtml = znodeSupplierEmail.GetOrderReceiptTemplate(ReceiptHtml);
            #endregion
           

            return newlyCreatedOrder;
        }

        public OrderModel UpdateOrder(int orderId, OrderModel model)
        {
            if (orderId < 1)
            {
                throw new Exception("Order ID cannot be less than 1.");
            }

            if (Equals(model, null))
            {
                throw new Exception("Order model cannot be null.");
            }

            var order = _orderRepository.GetByOrderID(orderId);
            if (!Equals(order, null))
            {
                // Set the order ID
                model.OrderId = orderId;

                var orderToUpdate = OrderMap.ToEntity(model);

                var updated = _orderRepository.Update(orderToUpdate);
                if (updated)
                {
                    // Update order line items
                    foreach (var item in model.OrderLineItems)
                    {
                        _orderLineItemRepository.Update(OrderLineItemMap.ToEntity(item));
                    }

                    order = _orderRepository.GetByOrderID(orderId);
                    return OrderMap.ToModel(order);
                }
            }

            return null;
        }
        #endregion

        #region Znode Version 8.0
        public AdminOrderListModel GetOrderList(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            List<Tuple<string, string, string>> filterList = new List<Tuple<string, string, string>>();
            AdminOrderListModel list = new AdminOrderListModel();
            int totalRowCount = 0;
            var pagingStart = 0;
            var pagingLength = 0;
            string orderBy = StringHelper.GenerateDynamicOrderByClause(sorts);
            string innerWhereClause = string.Empty;
            foreach (var item in filters)
            {
                if (Equals(item.Item1, "username"))
                {
                    filterList.Add(item);
                    innerWhereClause = StringHelper.GenerateDynamicWhereClause(filterList);
                    filters.Remove(item);
                    break;
                }
            }
            pagingStart = Convert.ToInt32(page.Get(PageKeys.Index));
            pagingLength = Convert.ToInt32(page.Get(PageKeys.Size));
            string whereClause = StringHelper.GenerateDynamicWhereClause(filters);
            StoredProcedureHelper storedProcedureHelper = new StoredProcedureHelper();

            string spName = StoredProcedureKeys.SpGetAllOrders;

            bool isVendor = false;
            IsVendorOrderAccount(filters, out isVendor);

            if (isVendor)
            {
                spName = StoredProcedureKeys.SpGetVendorOrders;
            }


            DataSet resultDataSet = storedProcedureHelper.SP_DataDetailsSpConfig(whereClause, null, spName, orderBy, pagingStart.ToString(), pagingLength.ToString(), out totalRowCount, null, innerWhereClause);

            list.OrderList = resultDataSet.Tables[0].ToList<AdminOrderModel>().ToCollection();
            list.TotalResults = totalRowCount;
            list.PageSize = pagingLength;
            list.PageIndex = pagingStart;
            // Check to set page size equal to the total number of results
            if (Equals(pagingLength, int.MaxValue))
            {
                list.PageSize = list.TotalResults;
            }
            return list;
        }

        public OrderModel GetOrderDetails(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            OrderModel model = new OrderModel();
            string whereClause = StringHelper.GenerateDynamicWhereClause(filters);
            OrderHelperRepository orderHelper = new OrderHelperRepository();
            DataSet resultDataSet = null;
            if (!Equals(filters, null) && filters.Count > 0)
            {
                if (filters.Count > 1)
                {
                    string portalid = GetAvailablePortals(filters[1].Item3);
                    resultDataSet = orderHelper.GetOrderDetailsByOrderId(Convert.ToInt32(filters[0].Item3));
                    model = resultDataSet.Tables[0].Rows[0].ToSingleRow<OrderModel>();
                    model.OrderLineItemList = new OrderLineItemListModel();
                    model.OrderLineItemList.OrdersLineItems = resultDataSet.Tables[1].ToList<OrderLineItemModel>().ToCollection();
                    DataSet rmaDataSet = _rMAAdminRepository.SearchRMARequest(null, Convert.ToInt32(filters[0].Item3), string.Empty, string.Empty, null, null, null, null, portalid);
                    model.rmaRequestList.RMARequests = rmaDataSet.Tables[0].ToList<RMARequestModel>().ToCollection();
                }
                else
                {
                    resultDataSet = orderHelper.GetOrderDetailsByOrderId(Convert.ToInt32(filters[0].Item3));
                    model = resultDataSet.Tables[0].Rows[0].ToSingleRow<OrderModel>();
                    model.OrderLineItemList = new OrderLineItemListModel();
                    model.OrderLineItemList.OrdersLineItems = resultDataSet.Tables[1].ToList<OrderLineItemModel>().ToCollection();
                }
            }
            return model;
        }

        public OrderModel UpdateOrderStatus(int orderId, OrderModel model)
        {
            OrderAdminRepository orderAdmin = new OrderAdminRepository();
            if (orderId < 1)
            {
                throw new Exception("Order ID cannot be less than 1.");
            }

            if (Equals(model, null))
            {
                throw new Exception("Order model cannot be null.");
            }

            var order = orderAdmin.GetOrderByOrderID(orderId);
            if (!Equals(order, null))
            {
                // Set the order ID
                model.OrderId = orderId;

                order.OrderStateID = model.OrderStateId;
                order.TrackingNumber = model.TrackingNumber;
                if (Equals(order.OrderStateID, 20))
                {
                    order.ShipDate = DateTime.Now.Date + DateTime.Now.TimeOfDay;
                }

                if (Equals(order.OrderStateID, 30))
                {
                    order.ReturnDate = DateTime.Now.Date + DateTime.Now.TimeOfDay;
                }

                var updated = orderAdmin.Update(order);
                if (updated)
                {
                    bool isReferralCommissionDeleted = true;

                    if (Equals(order.OrderStateID, 30) || Equals(order.OrderStateID, 40))
                    {
                        ReferralCommissionAdminRepository referralCommissionAdmin = new ReferralCommissionAdminRepository();
                        if (referralCommissionAdmin.GetReferralCommissionByOrderID(orderId))
                        {
                            referralCommissionAdmin.GetReferralCommissionByOrderID(orderId);
                            isReferralCommissionDeleted = referralCommissionAdmin.DeleteByOrderId(orderId);
                        }
                    }

                    if (updated && isReferralCommissionDeleted)
                    {
                        //If Selected index changed is selected as Shipped then Send the mail
                        if (Equals(order.OrderStateID, 20))
                        {
                            OrderAdminRepository orderAdminRepository = new OrderAdminRepository();
                            Order _OrderList = orderAdminRepository.DeepLoadByOrderID(orderId);
                            //commented below line as no code for email sending.
                            model = this.SendEmailReceipt(_OrderList, model);
                        }
                    }

                    // Update order line items
                    foreach (var item in model.OrderLineItems)
                    {
                        _orderLineItemRepository.Update(OrderLineItemMap.ToEntity(item));
                    }

                    order = _orderRepository.GetByOrderID(orderId);
                    if (!string.IsNullOrEmpty(model.TrackingText))
                    {
                        return OrderMap.ToModel(order, model.TrackingText);
                    }
                    return OrderMap.ToModel(order);
                }
            }

            return null;
        }

        public OrderModel VoidPayment(int orderId, OrderModel model)
        {
            // Retrieve order
            OrderAdminRepository orderAdminRepository = new OrderAdminRepository();
            var order = orderAdminRepository.GetOrderByOrderID(orderId);

            ZNode.Libraries.Framework.Business.ZNodeEncryption enc = new ZNode.Libraries.Framework.Business.ZNodeEncryption();

            // Get payment settings
            int paymentSettingID = (int)order.PaymentSettingID;
            PaymentSettingService pss = new PaymentSettingService();
            PaymentSetting ps = pss.GetByPaymentSettingID(paymentSettingID);
            try
            {

                string creditCardExp = Convert.ToString(order.CardExp);
                if (Equals(creditCardExp, null))
                {
                    creditCardExp = string.Empty;
                }

                // Set credit card
                CreditCard cc = new CreditCard();
                cc.Amount = model.RefundAmount;
                cc.CardNumber = model.RefundCardNumber.Trim();
                cc.CreditCardExp = creditCardExp;
                cc.OrderID = order.OrderID;
                cc.TransactionID = order.CardTransactionID;
                cc.ProcTxnId = order.CardTransactionID;

                // update order status
                // cancelled status
                order.OrderStateID = 40;

                // Refund status
                order.PaymentStatusID = 4;

                _orderRepository.Update(order);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: Credit card refunds are not supported for your gateway.");
            }
            return OrderMap.ToModel(order);
        }

        public OrderModel RefundPayment(int orderId, OrderModel model)
        {
            // Retrieve order
            OrderAdminRepository orderAdminRepository = new OrderAdminRepository();
            var order = orderAdminRepository.GetOrderByOrderID(orderId);
            ZNode.Libraries.Framework.Business.ZNodeEncryption enc = new ZNode.Libraries.Framework.Business.ZNodeEncryption();

            // Get payment settings
            int paymentSettingID = (int)order.PaymentSettingID;
            PaymentSettingService pss = new PaymentSettingService();
            PaymentSetting ps = pss.GetByPaymentSettingID(paymentSettingID);

            // Set gateway info
            try
            {

                string creditCardExp = Convert.ToString(order.CardExp);
                if (Equals(creditCardExp, null))
                {
                    creditCardExp = string.Empty;
                }

                // Set credit card
                CreditCard cc = new CreditCard();
                cc.Amount = model.RefundAmount;
                cc.CardNumber = model.RefundCardNumber.Trim();
                cc.CreditCardExp = creditCardExp;
                cc.OrderID = order.OrderID;
                cc.TransactionID = order.CardTransactionID;

                // Update order status
                // Returned status
                order.OrderStateID = 30;

                // Refund status
                order.PaymentStatusID = 3;

                _orderRepository.Update(order);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: Credit card refunds are not supported for your gateway.");
            }

            return OrderMap.ToModel(order);
        }

        public DataSet DownloadOrderData(List<Tuple<string, string, string>> filters)
        {
            DataSet ordersDownload = null;
            OrderAdminRepository orderAdminRepository = new OrderAdminRepository();
            bool isVendor = false;
            string orderId = string.Empty;
            string loggedAccountId = string.Empty;
            GetVendorOrderAccountDetails(filters, out orderId, out loggedAccountId, out isVendor);

            if (isVendor)
            {
                ordersDownload = orderAdminRepository.GetVendorOrdersByOrderId(orderId, loggedAccountId);
            }
            else
            {
                ordersDownload = orderAdminRepository.GetOrdersByOrderId(orderId);
            }
            return ordersDownload;
        }

        public DataSet DownloadOrderLineItemsData(List<Tuple<string, string, string>> filters)
        {
            DataTable dt = new DataTable();
            DataSet orderLineItemDownload = new DataSet();
            OrderAdminRepository orderAdminRepository = new OrderAdminRepository();

            bool isVendor = false;
            string orderId = string.Empty;
            string loggedAccountId = string.Empty;
            GetVendorOrderAccountDetails(filters, out orderId, out loggedAccountId, out isVendor);

            if (isVendor)
            {
                dt = orderAdminRepository.GetVendorOrderLineItemsByOrderId(orderId, loggedAccountId).Tables[0];
            }
            else
            {
                dt = orderAdminRepository.GetOrderLineItemsByOrderId(orderId).Tables[0];
            }

            dt.Columns.Remove("PortalId");
            foreach (DataRow dr in dt.Rows)
            {
                dr["description"] = StripHTML(dr["description"].ToString());
            }
            orderLineItemDownload.Tables.Add(dt.Copy());
            return orderLineItemDownload;
        }

        public OrderLineItemModel GetOrderLineItem(int orderLineItemId, NameValueCollection expands)
        {
            var orderLineItem = _orderLineItemRepository.GetByOrderLineItemID(orderLineItemId);
            var model = new OrderModel();
            OrderLineItemModel orderLineItemModel = new OrderLineItemModel();

            if (!Equals(orderLineItem, null))
            {
                model = this.GetOrder(orderLineItem.OrderID, new NameValueCollection());
                orderLineItemModel = OrderLineItemMap.ToModel(orderLineItem);
                orderLineItemModel.Order = model;
                return orderLineItemModel;
            }

            return null;
        }

        public OrderLineItemModel UpdateOrderLineItem(int orderLineItemId, OrderLineItemModel model)
        {
            if (orderLineItemId < 1)
            {
                throw new Exception("Order Line Item ID cannot be less than 1.");
            }

            if (Equals(model, null))
            {
                throw new Exception("Order Line Item model cannot be null.");
            }

            OrderAdminRepository orderAdmin = new OrderAdminRepository();
            Order order = orderAdmin.GetOrderByOrderID(model.OrderId);
            OrderLineItemRepository _orderLineItemRepository = new OrderLineItemRepository();
            OrderLineItem orderLineItem = _orderLineItemRepository.GetByOrderLineItemID(orderLineItemId);
            if (!Equals(order, null))
            {
                order.OrderID = model.OrderId;
                orderLineItem.OrderLineItemStateID = model.OrderLineItemStateId;
                orderLineItem.TrackingNumber = model.TrackingNumber;
                order.TrackingNumber = model.TrackingNumber;
                if (Equals(order.OrderStateID, 20))
                {
                    order.ShipDate = DateTime.Now.Date + DateTime.Now.TimeOfDay;
                    orderLineItem.ShipDate = DateTime.Now.Date + DateTime.Now.TimeOfDay;
                }

                if (Equals(order.OrderStateID, 30))
                {
                    order.ReturnDate = DateTime.Now.Date + DateTime.Now.TimeOfDay;
                    orderLineItem.ReturnDate = DateTime.Now.Date + DateTime.Now.TimeOfDay;
                }

                bool orderCheck = orderAdmin.Update(order);
                bool orderLineItemCheck = _orderLineItemRepository.Update(orderLineItem);
                bool isReferralCommissionDeleted = true;
                if (order.OrderStateID == 30 || order.OrderStateID == 40)
                {
                    ReferralCommissionAdmin referralCommissionAdmin = new ReferralCommissionAdmin();
                    if (referralCommissionAdmin.GetReferralCommissionByOrderID(model.OrderLineItemId))
                    {
                        referralCommissionAdmin.GetReferralCommissionByOrderID(model.OrderLineItemId);
                        isReferralCommissionDeleted = referralCommissionAdmin.DeleteByOrderId(model.OrderLineItemId);
                    }
                }
            }
            return OrderLineItemMap.ToModel(orderLineItem);
        }

        public string SendEmail(List<Tuple<string, string, string>> filters)
        {
            int orderId = 0;
            int orderLineItemId = 0;
            string trackingNumber = string.Empty;
            if (!Equals(filters, null))
            {
                foreach (Tuple<string, string, string> tuple in filters)
                {
                    if (Equals(FilterKeys.OrderId, tuple.Item1))
                    {
                        orderId = Convert.ToInt32(tuple.Item3);
                    }
                    else if (Equals(FilterKeys.OrderLineItemId, tuple.Item1))
                    {
                        orderLineItemId = Convert.ToInt32(tuple.Item3);
                    }
                    else if (Equals(FilterKeys.TrackingNumber, tuple.Item1))
                    {
                        trackingNumber = tuple.Item3;
                    }
                }
            }

            OrderAdmin _orderAdmin = new OrderAdmin();
            Order order = _orderAdmin.DeepLoadByOrderID(orderId);
            order.TrackingNumber = trackingNumber;
            return this.SendEmailReceipt(order, orderLineItemId);
        }

        public bool updateorderpaymentstatus(int orderId, string paymentStatus)
        {
            if (orderId < 1)
            {
                throw new Exception("Order ID cannot be less than 1.");
            }

            OrderHelper helper = new OrderHelper();
            return helper.UpdateOrderPaymentStatus(orderId, paymentStatus);
        }

        #endregion

        #region Private Methods
        private void ValidateCheckout(ZNodeCheckout checkout)
        {
            if (Equals(checkout.ShoppingCart, null))
            {
                throw new Exception("Shopping cart cannot be null.");
            }

            if (Equals(checkout.ShoppingCart.ShoppingCartItems.Count, 0))
            {
                throw new Exception("There are no items in the shopping cart.");
            }

            if (!ProfileHasPaymentOptions(checkout))
            {
                throw new Exception("Profile does not have any payment options.");
            }

            // Check for any error message at the portal cart level
            if (checkout.ShoppingCart.PortalCarts.Select(cart => cart.AddressCarts.Count(x => !Equals(x.ErrorMessage, String.Empty))).Any(errorMessageCount => errorMessageCount > 0))
            {
                throw new Exception("Shopping cart contains errors.");
            }

            // Don't validate payment if cart total is 0
            if (Equals(checkout.ShoppingCart.Total, 0))
            {
                return;
            }

            // Check for any error message at the Address cart level
            if (checkout.ShoppingCart.AddressCarts.Select(addressCart => !Equals(addressCart.ErrorMessage, string.Empty)).Any(errorMessageCount => errorMessageCount))
            {
                throw new Exception("Invalid shipping address.");
            }

            if (Equals(checkout.ShoppingCart.Payment, null))
            {
                throw new Exception("Shopping cart payment cannot be null.");
            }
        }

        private bool ProfileHasPaymentOptions(ZNodeCheckout checkout)
        {
            var profileId = !Equals(checkout.UserAccount, null) ? ZNodeProfile.CurrentUserProfileId : ZNodeProfile.DefaultRegisteredProfileId;

            var paymentOptions = _paymentSettingRepository.GetAll();

            var total = 0;
            var isDefaultPortal = !DataRepository.CategoryProvider.GetByPortalID(checkout.PortalID, 0, 1, out total).Any();
            var franchisePortal = _portalRepository.GetByPortalID(checkout.PortalID);
            profileId = franchisePortal.DefaultRegisteredProfileID.GetValueOrDefault(0);
            return paymentOptions.Any(p => ((Equals(p.ProfileID, null) && isDefaultPortal) || Equals(p.ProfileID, profileId)) && p.ActiveInd);
        }

        private bool CheckoutHasValidAddress(ZNodeCheckout checkout)
        {
            var isValidAddress = !checkout.ShoppingCart.IsMultipleShipToAddress ||
                                 checkout.UserAccount.CheckValidAddress(checkout.UserAccount.BillingAddress) &&
                                 checkout.ShoppingCart.PortalCarts.All(x => x.AddressCarts.All(
                                    y =>
                                    checkout.UserAccount.CheckValidAddress(checkout.UserAccount.Addresses.FirstOrDefault(z => Equals(z.AddressID, y.AddressID)))
                                 ));

            return isValidAddress;
        }

        private int? GetProfileIdForCheckout(int portalId, ZNodeUserAccount userAccount)
        {
            int? profileId = null;

            if (Equals(portalId, ZNodeConfigManager.SiteConfig.PortalID))
            {
                profileId = userAccount.ProfileID;
            }
            else
            {
                var portalProfiles = _portalProfileRepository.GetByPortalID(portalId);
                var portalProfile = portalProfiles.Find(PortalProfileColumn.ProfileID, userAccount.ProfileID);

                if (!Equals(portalProfile, null))
                {
                    // Use the profile ID on the user account
                    profileId = userAccount.ProfileID;
                }
                else
                {
                    // Use default registered profile ID associated with the franchise portal
                    var franchisePortal = _portalRepository.GetByPortalID(portalId);
                    if (!Equals(franchisePortal, null))
                    {
                        profileId = franchisePortal.DefaultRegisteredProfileID;
                    }
                }
            }

            HttpContext.Current.Session["ProfileCache"] = _profileRepository.GetByProfileID(profileId.GetValueOrDefault(0));

            return profileId;
        }

        private bool IsDefaultPortal(int portalId)
        {
            // Check categories to determine if default portal
            int total;
            var categoryList = _categoryRepository.GetByPortalID(portalId, 0, 1, out total);
            return Equals(categoryList.Count, 0);
        }

        /// <summary>
        /// Strip HTML method
        /// </summary>
        /// <param name="description">The value of description</param>
        /// <returns>Returns the HTMl string</returns>
        private string StripHTML(string description)
        {
            return System.Text.RegularExpressions.Regex.Replace(description, "<[^>]*>", " ");
        }

        private void CheckoutWithPayPalExpress(ZNodeCheckout checkout, ShoppingCartModel model, int? profileId, ZNodeLogging log)
        {
            // Get payment option to give to PayPal
            var paymentOption = GetPaymentOption(profileId.GetValueOrDefault(), model.Payment.PaymentOption.PaymentType.PaymentTypeId);

        }

        private void CheckoutWithGoogleCheckout(ZNodeCheckout checkout, ShoppingCartModel model, int? profileId, ZNodeLogging log)
        {
            // Get payment option to give to Google
            var paymentOption = GetPaymentOption(profileId.GetValueOrDefault(), model.Payment.PaymentOption.PaymentTypeId);

            // Create the Google gateway
            var google = new GoogleCheckout(model.ReturnUrl, model.CancelUrl)
            {
                PaymentSetting = paymentOption
            };

            // Call out to Google and check the response
            var response = google.SendRequestToGoogle();
            if (!Equals(response.ResponseCode, "0"))
            {
                log.LogActivityTimerEnd((int)ZNodeLogging.ErrorNum.SubmitPaymentFailed, response.ResponseText);
                ZNodeLogging.LogMessage("Google Checkout error: " + response.ResponseText);
                throw new Exception("Google Checkout error: " + response.ResponseText);
            }

            // Get the response key
            var key = response.ResponseText;

            checkout.ShoppingCart.Payment.PaymentSetting = paymentOption;
            HttpRuntime.Cache.Add(key + "cart", checkout.ShoppingCart, null, DateTime.Now.AddHours(1), TimeSpan.FromMinutes(0), CacheItemPriority.Normal, null);

            // Check to create user account
            if (Equals(checkout.UserAccount, null))
            {
                checkout.UserAccount = new ZNodeUserAccount();
                checkout.UserAccount.AddUserAccount();
            }

            // Add user account to cache
            HttpRuntime.Cache.Add(key + "user", checkout.UserAccount, null, DateTime.Now.AddHours(1), TimeSpan.FromMinutes(0), CacheItemPriority.Normal, null);

            // Add portal ID to cache
            HttpRuntime.Cache.Add(key + "portalid", ZNodeConfigManager.SiteConfig.PortalID, null, DateTime.Now.AddHours(1), TimeSpan.FromMinutes(0), CacheItemPriority.Normal, null);

            // Pre-submit order
            checkout.ShoppingCart.PreSubmitOrderProcess();

            // If successful, set the redirect URL for clients to use
            model.RedirectUrl = response.ECRedirectURL;
        }

        private void PostSubmitOrder(ZNodeOrderFulfillment order, ZNodeCheckout checkout, string feedbackUrl)
        {
            checkout.ShoppingCart.Payment.TransactionID = order.CardTransactionID;
            checkout.ShoppingCart.Payment.AuthCode = order.CardAuthCode;
            checkout.ShoppingCart.Payment.SubscriptionID = order.Custom2;

            // Do post submit processing
            //Znode Version 7.2.2 
            //Code commented for update Inventory- Start
            //Removed this code block to update Inventory just after order submitted in database
            //checkout.ShoppingCart.PostSubmitOrderProcess();
            //Code commented for update Inventory- End

            // Invoke any supplier web services
            var supplierWebService = new ZnodeSupplierWebServiceManager(order, checkout.ShoppingCart);
            supplierWebService.InvokeWebService();

            try
            {
                // Send email receipts to the suppliers
                var supplierEmail = new ZnodeSupplierEmailManager(order, checkout.ShoppingCart) { FromApi = true, FeedbackUrl = feedbackUrl };
                supplierEmail.SendEmailReceipt();
            }
            catch (Exception ex) { }
            // Clear the saved cart info
            var savedCart = new ZNodeSavedCart();
            savedCart.RemoveSavedCart();

            var counter = 0;

            // Loop through the order line items
            foreach (var item in order.OrderLineItems)
            {
                var shoppingCartItem = checkout.ShoppingCart.AddressCarts.SelectMany(x => x.ShoppingCartItems.Cast<ZNodeShoppingCartItem>()).ElementAt(counter++);

                // Set product ID and quantity
                var productId = shoppingCartItem.Product.ProductID;
                var quantity = shoppingCartItem.Quantity;

                // Get digital assets and loop through them
                var digitalAssets = ZNodeDigitalAssetList.CreateByProductIdAndQuantity(productId, quantity).DigitalAssetCollection;
                foreach (var da in digitalAssets.Cast<ZNodeDigitalAsset>())
                {
                    // Get asset, set order line item ID, then update
                    var asset = _digitalAssetRepository.GetByDigitalAssetID(da.DigitalAssetID);
                    if (!Equals(asset, null))
                    {
                        asset.OrderLineItemID = item.OrderLineItemID;
                        _digitalAssetRepository.Update(asset);
                    }
                }
            }
        }

        private PaymentSetting GetPaymentOption(int profileId, int paymentTypeId)
        {
            var all = _paymentSettingRepository.GetAll();

            var list = all.FindAll(x => Equals(x.PaymentTypeID, paymentTypeId));
            list.Filter = "ActiveInd = true AND ProfileID = " + profileId;

            if (Equals(list.Count, 0))
            {
                // If nothing in the list with profile ID, try filtering without it
                list.Filter = "ActiveInd = true";
            }

            // Now sort and return the first one in the list
            list.Sort("ProfileID DESC");
            return list[0];
        }

        private void GetPaymentOptionForCheckout(ZNodeCheckout checkout, ShoppingCartModel model, int? profileId, bool isDefaultPortal, ZNodeLogging log)
        {
            // Get all payment options
            var allPaymentOptions = _paymentSettingRepository.GetAll();

            // Get active payment options for this profile ID
            var activeProfilePaymentOptions = allPaymentOptions.FindAll(pmt => Equals(pmt.ProfileID, profileId) && pmt.ActiveInd);

            // Get all active payment options with no profile ID and default portal
            var allActivePaymentOptions = allPaymentOptions.FindAll(pmt => Equals(pmt.ProfileID, null) && isDefaultPortal && pmt.ActiveInd);

            // Merge them together
            if (allActivePaymentOptions.Any())
            {
                activeProfilePaymentOptions.AddRange(allActivePaymentOptions.Where(x => activeProfilePaymentOptions.All(y => !Equals(y.PaymentTypeID, x.PaymentTypeID))).ToList());
            }

            // Get active payment options with payment type ID
            var paymentTypeId = model.Payment.PaymentOption.PaymentType.PaymentTypeId;
            var activePaymentOptions = activeProfilePaymentOptions.FindAll(pmt => Equals(pmt.PaymentTypeID, paymentTypeId) && pmt.ActiveInd);

            if (!Equals(activePaymentOptions, null) && activePaymentOptions.Count > 0)
            {
                checkout.ShoppingCart.Payment.PaymentSetting = activePaymentOptions[0];
                checkout.PaymentSettingID = activePaymentOptions[0].PaymentSettingID;
            }
            else if (model.Payment.PaymentOption.PaymentType.PaymentTypeId == (int)ZNode.Libraries.ECommerce.Entities.PaymentType.COD || model.Payment.PaymentOption.PaymentType.PaymentTypeId == (int)ZNode.Libraries.ECommerce.Entities.PaymentType.PURCHASE_ORDER)
            {
                checkout.ShoppingCart.Payment.PaymentSetting = checkout.ShoppingCart.Payment.PaymentSetting;
                checkout.PaymentSettingID = checkout.PaymentSettingID;
            }
            else
            {
                log.LogActivityTimerEnd((int)ZNodeLogging.ErrorNum.OrderSubmissionFailed, null);
                ZNodeLogging.LogMessage("Multiple vendor checkout not supported, site not configured for credit card payments.");
                throw new Exception("Multiple vendor checkout not supported, site not configured for credit card payments.");
            }
        }

        private void DoAffiliateTracking(ZNodeOrderFulfillment order)
        {
            var tracking = new ZNodeTracking();
            if (tracking.AffiliateId.Length > 0)
            {
                tracking.AccountID = order.AccountID;
                tracking.OrderID = order.OrderID;
                tracking.LogTrackingEvent("Placed an order", order.Custom3);
            }
        }

        private void GetExpands(NameValueCollection expands, Order order)
        {
            if (expands.HasKeys())
            {
                ExpandAccount(expands, order);
                ExpandOrderLineItems(expands, order);
                ExpandPaymentOption(expands, order);
                ExpandPaymentType(expands, order);
                ExpandShippingOption(expands, order);
            }
        }

        private void ExpandAccount(NameValueCollection expands, Order order)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Account)))
            {
                if (order.AccountID.HasValue)
                {
                    var account = _accountRepository.GetByAccountID(order.AccountID.Value);
                    if (!Equals(account, null))
                    {
                        order.AccountIDSource = account;
                    }
                }
            }
        }

        private void ExpandOrderLineItems(NameValueCollection expands, Order order)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.OrderLineItems)))
            {
                var orderLineItems = _orderLineItemRepository.DeepLoadByOrderID(order.OrderID, true, DeepLoadType.IncludeChildren, typeof(OrderLineItem));
                foreach (var item in orderLineItems)
                {
                    // Check to get the order shipment for the item
                    if (item.OrderShipmentID.HasValue)
                    {
                        var orderShipment = _orderShipmentRepository.GetByOrderShipmentID(item.OrderShipmentID.Value);
                        if (!Equals(orderShipment, null))
                        {
                            item.OrderShipmentIDSource = orderShipment;
                        }
                    }
                }

                order.OrderLineItemCollection = orderLineItems;
            }
        }

        private void ExpandPaymentOption(NameValueCollection expands, Order order)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.PaymentOption)))
            {
                if (order.PaymentSettingID.HasValue)
                {
                    var paymentOption = _paymentSettingRepository.GetByPaymentSettingID(order.PaymentSettingID.Value);
                    if (!Equals(paymentOption, null))
                    {
                        order.PaymentSettingIDSource = paymentOption;
                    }
                }
            }
        }

        private void ExpandPaymentType(NameValueCollection expands, Order order)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.PaymentType)))
            {
                if (order.PaymentTypeId.HasValue)
                {
                    var paymentType = _paymentTypeRepository.GetByPaymentTypeID(order.PaymentTypeId.Value);
                    if (!Equals(paymentType, null))
                    {
                        order.PaymentType = paymentType;
                    }
                }
            }
        }

        private void ExpandShippingOption(NameValueCollection expands, Order order)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.ShippingOption)))
            {
                if (order.ShippingID.HasValue)
                {
                    var shippingOption = _shippingRepository.GetByShippingID(order.ShippingID.Value);
                    if (!Equals(shippingOption, null))
                    {
                        order.ShippingIDSource = shippingOption;
                    }
                }
            }
        }

        private void SetFiltering(List<Tuple<string, string, string>> filters, OrderQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;

                if (Equals(filterKey, FilterKeys.AccountId)) SetQueryParameter(OrderColumn.AccountID, filterOperator, filterValue, query);
                if (Equals(filterKey, FilterKeys.CompanyName)) SetQueryParameter(OrderColumn.BillingCompanyName, filterOperator, filterValue, query);
                if (Equals(filterKey, FilterKeys.CountryCode)) SetQueryParameter(OrderColumn.BillingCountry, filterOperator, filterValue, query);
                if (Equals(filterKey, FilterKeys.Email)) SetQueryParameter(OrderColumn.BillingEmailId, filterOperator, filterValue, query);
                if (Equals(filterKey, FilterKeys.ExternalId)) SetQueryParameter(OrderColumn.ExternalID, filterOperator, filterValue, query);
                if (Equals(filterKey, FilterKeys.LastName)) SetQueryParameter(OrderColumn.BillingLastName, filterOperator, filterValue, query);
                if (Equals(filterKey, FilterKeys.OrderDate)) SetQueryParameter(OrderColumn.OrderDate, filterOperator, filterValue, query);
                if (Equals(filterKey, FilterKeys.OrderStateId)) SetQueryParameter(OrderColumn.OrderStateID, filterOperator, filterValue, query);
                if (Equals(filterKey, FilterKeys.PortalId)) SetQueryParameter(OrderColumn.PortalId, filterOperator, filterValue, query);
                if (Equals(filterKey, FilterKeys.PostalCode)) SetQueryParameter(OrderColumn.BillingPostalCode, filterOperator, filterValue, query);
                if (Equals(filterKey, FilterKeys.PurchaseOrderNumber)) SetQueryParameter(OrderColumn.PurchaseOrderNumber, filterOperator, filterValue, query);
                if (Equals(filterKey, FilterKeys.StateCode)) SetQueryParameter(OrderColumn.BillingStateCode, filterOperator, filterValue, query);
                if (Equals(filterKey, FilterKeys.Total)) SetQueryParameter(OrderColumn.Total, filterOperator, filterValue, query);
                if (Equals(filterKey, FilterKeys.TrackingNumber)) SetQueryParameter(OrderColumn.TrackingNumber, filterOperator, filterValue, query);
                if (Equals(filterKey, FilterKeys.TransactionId)) SetQueryParameter(OrderColumn.CardTransactionID, filterOperator, filterValue, query);
            }
        }

        private void SetSorting(NameValueCollection sorts, OrderSortBuilder sortBuilder)
        {
            if (sorts.HasKeys() && sorts.Count > 1)
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);

                    if (Equals(value, SortKeys.City)) SetSortDirection(OrderColumn.BillingCity, sorts[StoredProcedureKeys.sortDirKey], sortBuilder);
                    if (Equals(value, SortKeys.CompanyName)) SetSortDirection(OrderColumn.BillingCompanyName, sorts[StoredProcedureKeys.sortDirKey], sortBuilder);
                    if (Equals(value, SortKeys.CountryCode)) SetSortDirection(OrderColumn.BillingCountry, sorts[StoredProcedureKeys.sortDirKey], sortBuilder);
                    if (Equals(value, SortKeys.LastName)) SetSortDirection(OrderColumn.BillingLastName, sorts[StoredProcedureKeys.sortDirKey], sortBuilder);
                    if (Equals(value, SortKeys.OrderDate)) SetSortDirection(OrderColumn.OrderDate, sorts[StoredProcedureKeys.sortDirKey], sortBuilder);
                    if (Equals(value, SortKeys.OrderId)) SetSortDirection(OrderColumn.OrderID, sorts[StoredProcedureKeys.sortDirKey], sortBuilder);
                    if (Equals(value, SortKeys.PostalCode)) SetSortDirection(OrderColumn.BillingPostalCode, sorts[StoredProcedureKeys.sortDirKey], sortBuilder);
                    if (Equals(value, SortKeys.StateCode)) SetSortDirection(OrderColumn.BillingStateCode, sorts[StoredProcedureKeys.sortDirKey], sortBuilder);
                    if (Equals(value, SortKeys.Total)) SetSortDirection(OrderColumn.Total, sorts[StoredProcedureKeys.sortDirKey], sortBuilder);
                }
            }
            else
            {
                SetSortDirection(OrderColumn.OrderID, sorts[StoredProcedureKeys.sortDirKey], sortBuilder);
            }
        }

        private void SetQueryParameter(OrderColumn column, string filterOperator, string filterValue, OrderQuery query)
        {
            base.SetQueryParameter(column, filterOperator, filterValue, query);
        }

        private void SetSortDirection(OrderColumn column, string value, OrderSortBuilder sortBuilder)
        {
            base.SetSortDirection(column, value, sortBuilder);
        }

        /// <summary>
        /// Send the order status email to customer.
        /// </summary>
        /// <param name="order">Order object</param>
        /// <param name="model">Order Model</param>
        /// <returns>Returns OrderModel</returns>
        private OrderModel SendEmailReceipt(Order order, OrderModel model)
        {
            string recepientEmail = string.Empty;

            try
            {
                recepientEmail = order.BillingEmailId;
                string senderEmail = ZNodeConfigManager.SiteConfig.CustomerServiceEmail;
                if (!string.IsNullOrEmpty(recepientEmail) && !string.IsNullOrEmpty(recepientEmail))
                {
                    string subject = "Track your package";
                    string defaultTemplatePath = HttpContext.Current.Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, "TrackingNumber.htm"));
                    if (File.Exists(defaultTemplatePath))
                    {
                        StreamReader rw = new StreamReader(defaultTemplatePath);
                        string messageText = rw.ReadToEnd();
                        Regex firstName = new Regex("#BillingFirstName#", RegexOptions.IgnoreCase);
                        messageText = firstName.Replace(messageText, order.BillingFirstName);

                        Regex lastName = new Regex("#BillingLastName#", RegexOptions.IgnoreCase);
                        messageText = lastName.Replace(messageText, order.BillingLastName);

                        Regex custom1 = new Regex("#Custom1#", RegexOptions.IgnoreCase);
                        messageText = custom1.Replace(messageText, model.TrackingNumber.ToString());

                        //PRFT Custom code:Start
                        Regex trackingMessage = new Regex("#TrackingMessage#", RegexOptions.IgnoreCase);
                        if (Equals(order.ShippingIDSource.ShippingTypeID, orderShippingTypeIdWithValue3))
                        {
                            //messageText = trackingMessage.Replace(messageText, "Your FedEx Tracking Number : ");
                            messageText = trackingMessage.Replace(messageText, "Your UPS Tracking Number : ");
                        }
                        else if (Equals(order.ShippingIDSource.ShippingTypeID, orderShippingTypeIdWithValue2))
                        {
                            //messageText = trackingMessage.Replace(messageText, "Your UPS Tracking Number : ");
                            messageText = trackingMessage.Replace(messageText, "Your FedEx Tracking Number : ");
                        }
                        else
                        {
                            messageText = trackingMessage.Replace(messageText, string.Empty);
                        }
                        //PRFT Custom code:End
                        Regex message = new Regex("#Message#", RegexOptions.IgnoreCase);
                        if (Equals(order.ShippingIDSource.ShippingTypeID, orderShippingTypeIdWithValue3))
                        {
                            messageText = message.Replace(messageText, "We would like to inform you that your order #"+ order.OrderID + " has shipped. You can track your package using the tracking number given below:");
                        }
                        else if (Equals(order.ShippingIDSource.ShippingTypeID, orderShippingTypeIdWithValue2))
                        {
                            messageText = message.Replace(messageText, "We would like to inform you that your order #" + order.OrderID + " has shipped. You can track your package using the tracking number given below:");
                        }
                        else
                        {
                            messageText = message.Replace(messageText, "We would like to inform you that your order #" + order.OrderID + " has shipped.");
                        }

                        //PRFT Custom Code : Start
                        string storeLogoPath = ZNodeConfigManager.SiteConfig.LogoPath.Trim().Replace("~/", "");

                        Regex rx6 = new Regex("#StoreLogo#", RegexOptions.IgnoreCase);
                        messageText = rx6.Replace(messageText, apiUrl + "/" + storeLogoPath.TrimStart('~'));

                        var rx7 = new Regex("#MailingInfo#", RegexOptions.IgnoreCase);
                        messageText = rx7.Replace(messageText, ZNodeConfigManager.SiteConfig.CustomerServiceEmail);

                        var rx8 = new Regex("#SupportPhone#", RegexOptions.IgnoreCase);
                        messageText = rx8.Replace(messageText, ZNodeConfigManager.SiteConfig.CustomerServicePhoneNumber);

                        var rx9 = new Regex("#SupportEmail#", RegexOptions.IgnoreCase);
                        messageText = rx9.Replace(messageText, ZNodeConfigManager.SiteConfig.SalesEmail);

                        string logoLink = ConfigurationManager.AppSettings["DemoWebsiteUrl"];
                        var rx10 = new Regex("#LogoLink#", RegexOptions.IgnoreCase);
                        messageText = rx10.Replace(messageText, logoLink);
                        //PRFT Custom Code : End

                        ZNode.Libraries.Framework.Business.ZNodeEmail.SendEmail(recepientEmail, senderEmail, senderEmail, subject, messageText, true);

                        model.TrackingText = string.Format("Tracking Number Email Sent to <a href=mailto: {0}> {1} </a>", recepientEmail.ToString(), recepientEmail);

                    }
                }

            }
            catch (Exception)
            {
                throw new Exception("There was a problem sending the email to " + recepientEmail);
            }
            return model;
        }

        /// <summary>
        /// To get order Id , loggedAccountId & check is request is from mall admin
        /// </summary>
        /// <param name="filters">List<Tuple<string, string, string>> filters</param>
        /// <param name="orderId">out string orderId</param>
        /// <param name="loggedAccountId">out string loggedAccountId</param>
        /// <param name="isVendor">out bool isVendor</param>
        private void GetVendorOrderAccountDetails(List<Tuple<string, string, string>> filters, out string orderId, out string loggedAccountId, out bool isVendor)
        {
            orderId = string.Empty;
            loggedAccountId = string.Empty;
            isVendor = false;
            foreach (var tuple in filters)
            {
                if (Equals(tuple.Item1.ToLower(), FilterKeys.UserType))
                {
                    if (Equals(tuple.Item3.Trim().ToLower(), "malladmin"))
                    {
                        isVendor = true;
                    }
                }
                else if (Equals(tuple.Item1.ToLower(), FilterKeys.OrderId))
                {
                    orderId = tuple.Item3.Trim();
                }
                else if (Equals(tuple.Item1.ToLower(), FilterKeys.AccountId))
                {
                    loggedAccountId = tuple.Item3.Trim();
                }
            }
        }


        /// <summary>
        /// Sends the order status changed to shipped email to the customer.
        /// </summary>
        /// <param name="order">Order entity fetched on the basis of order id</param>
        /// <param name="ordLineItemID">int orderLineItemId to fetch the order shipping id</param>
        /// <returns>Returns string message if email gets sent successfully.</returns>
        private string SendEmailReceipt(Order order, int ordLineItemID)
        {
            string recepientEmail = string.Empty;
            try
            {
                recepientEmail = order.BillingEmailId;
                string senderEmail = ZNodeConfigManager.SiteConfig.CustomerServiceEmail;

                if (string.IsNullOrEmpty(senderEmail))
                {
                    throw new Exception("Sender email address (Customer service email) is empty. ");
                }

                if (string.IsNullOrEmpty(recepientEmail))
                {
                    throw new Exception("Recepient email address is empty.");
                }

                string subject = "Track your package";
                string defaultTemplatePath = HttpContext.Current.Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, "TrackingNumber.htm"));
                if (File.Exists(defaultTemplatePath))
                {
                    // Get message text.
                    StreamReader rw = new StreamReader(defaultTemplatePath);
                    string messageText = rw.ReadToEnd();
                    Regex rx1 = new Regex("#BillingFirstName#", RegexOptions.IgnoreCase);
                    messageText = rx1.Replace(messageText, order.BillingFirstName);

                    Regex rx2 = new Regex("#BillingLastName#", RegexOptions.IgnoreCase);
                    messageText = rx2.Replace(messageText, order.BillingLastName);

                    Regex rx3 = new Regex("#Custom1#", RegexOptions.IgnoreCase);
                    messageText = rx3.Replace(messageText, order.TrackingNumber);

                    Regex rx4 = new Regex("#TrackingMessage#", RegexOptions.IgnoreCase);

                    //PRFT Custom Code : Start
                    string storeLogoPath = ZNodeConfigManager.SiteConfig.LogoPath.Trim().Replace("~/", "");

                    Regex rx6 = new Regex("#StoreLogo#", RegexOptions.IgnoreCase);
                    messageText = rx6.Replace(messageText, apiUrl + "/" + storeLogoPath.TrimStart('~'));

                    var rx7 = new Regex("#MailingInfo#", RegexOptions.IgnoreCase);
                    messageText = rx7.Replace(messageText, ZNodeConfigManager.SiteConfig.CustomerServiceEmail);

                    var rx8 = new Regex("#SupportPhone#", RegexOptions.IgnoreCase);
                    messageText = rx8.Replace(messageText, ZNodeConfigManager.SiteConfig.CustomerServicePhoneNumber);

                    var rx9 = new Regex("#SupportEmail#", RegexOptions.IgnoreCase);
                    messageText = rx9.Replace(messageText, ZNodeConfigManager.SiteConfig.SalesEmail);

                    string logoLink = ConfigurationSettings.AppSettings["DemoWebsiteUrl"];
                    var rx10 = new Regex("#LogoLink#", RegexOptions.IgnoreCase);
                    messageText = rx10.Replace(messageText, logoLink);
                    //PRFT Custom Code : End


                    OrderAdmin _orderAdmin = new OrderAdmin();
                    int shippingTypeID = 0;

                    shippingTypeID = order.ShippingIDSource == null ? _orderAdmin.GetOrderShippingTypeID(ordLineItemID) : int.Parse(order.ShippingIDSource.ShippingTypeID.ToString());

                    if (shippingTypeID.Equals(orderShippingTypeIdWithValue3))
                    {
                        messageText = rx4.Replace(messageText, "We would like to inform you that your order has shipped. You can track your package using the tracking number given below:");
                    }
                    else if (shippingTypeID.Equals(orderShippingTypeIdWithValue2))
                    {
                        messageText = rx4.Replace(messageText, "We would like to inform you that your order has shipped. You can track your package using the tracking number given below:");
                    }
                    else
                    {
                        messageText = rx4.Replace(messageText, string.Empty);
                    }

                    Regex rx5 = new Regex("#Message#", RegexOptions.IgnoreCase);
                    if (shippingTypeID.Equals(orderShippingTypeIdWithValue3))
                    {
                        messageText = rx5.Replace(messageText, "We would like to inform you that your order has shipped. You can track your package using the tracking number given below:");
                    }
                    else if (shippingTypeID.Equals(orderShippingTypeIdWithValue2))
                    {
                        messageText = rx5.Replace(messageText, "We would like to inform you that your order has shipped. You can track your package using the tracking number given below:");
                    }
                    else
                    {
                        messageText = rx5.Replace(messageText, "We would like to inform you that your order has shipped.");
                    }
                    //For fetching  shippingTypeID on the basis of ordLineItemID - End

                    ZNode.Libraries.Framework.Business.ZNodeEmail.SendEmail(recepientEmail, senderEmail, senderEmail, subject, messageText, true);

                    return string.Format("Tracking Number Email Sent to <a href=mailto: {0}> {1} </a>", recepientEmail.ToString(), recepientEmail);
                }
            }
            catch (Exception)
            {
                throw new Exception("There was a problem sending the email to " + recepientEmail);
            }
            return string.Empty;
        }

        /// <summary>
        /// To check  is request is from mall admin
        /// </summary>
        /// <param name="filters"></param>
        /// <param name="isVendor"></param>
        private void IsVendorOrderAccount(List<Tuple<string, string, string>> filters, out bool isVendor)
        {
            isVendor = false;
            foreach (var tuple in filters)
            {
                if (Equals(tuple.Item1.ToLower(), FilterKeys.UserType))
                {
                    if (Equals(tuple.Item3.Trim().ToLower(), "malladmin"))
                    {
                        isVendor = true;
                    }
                }
            }
        }
        #endregion

        #region PRFT Custom Method
        public void UpdateOrderExternalId(int OrderId, string ExternalID)
        {
            if (OrderId < 1)
            {
                throw new Exception("Order ID cannot be less than 1.");
            }

            var order = _orderRepository.GetByOrderID(OrderId);
            if (!Equals(order, null))
            {
                order.ExternalID = ExternalID;
                //For updating the order state as SUBMITTED in ZNodeOrder table When order is submitted to ERP.
                //order.OrderStateID = Convert.ToInt32(ZNodeOrderState.SUBMITTED);
                var updated = _orderRepository.Update(order);
            }
        }
        #endregion
    }
}
