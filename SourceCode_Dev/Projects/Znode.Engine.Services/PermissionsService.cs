﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Data;
using WebMatrix.WebData;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Maps;
using ZNode.Libraries.DataAccess.Custom;
using AccountRepository = ZNode.Libraries.DataAccess.Service.AccountService;

namespace Znode.Engine.Services
{
    /// <summary>
    /// This class will act as a service to interact with the roles and permissions data
    /// </summary>
    public class PermissionsService : BaseService, IPermissionsService
    {
        #region Private Variables
        private readonly AccountRepository _accountRepository;
        private ProfileHelper _profileHelper;
        #endregion

        #region Private Constant Variables
        private const string DefaultProviderUserId = "00000000-0000-0000-0000-000000000000";
        #endregion

        #region Constructors
        /// <summary>
        /// This is the default constructor
        /// </summary>
        public PermissionsService()
        {
            _accountRepository = new AccountRepository();
            _profileHelper = new ProfileHelper();
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// This method will fetch the DB records for roles and permissions for relevant user id
        /// </summary>
        /// <param name="accountId">int account id</param>
        /// <param name="filters"></param>
        /// <param name="expand"></param>
        /// <returns></returns>
        public PermissionsModel GetRolesAndPerminssions(int accountId, List<Tuple<string, string, string>> filters, System.Collections.Specialized.NameValueCollection expand)
        {
            if (!Equals(accountId, 0))
            {
                var account = _accountRepository.GetByAccountID(accountId);
                if (!Equals(account, null))
                {
                    // First try getting the user by their guid
                    if (account.UserID.HasValue)
                    {
                        var user = WebSecurity.GetUser(account.UserID.Value);

                        if (Equals(user, null))
                        {
                            throw new Exception("User not found.");
                        }

                        var model = new PermissionsModel();
                        DataSet resultDataSet = _profileHelper.GetRolesAndPermissions(account.UserID.Value);
                        model = PermissionsMap.ToPermissionModel(resultDataSet, account.UserID.Value, accountId, user.UserName);

                        return model;
                    }
                    else
                    {
                        throw new Exception("User not exists.");
                    }
                }
            }
            else
            {
                var model = new PermissionsModel();
                DataSet resultDataSet = _profileHelper.GetRolesAndPermissions(Guid.Parse(DefaultProviderUserId));
                model = PermissionsMap.ToPermissionModel(resultDataSet, Guid.Parse(DefaultProviderUserId), 0, string.Empty);

                return model;
            }
            return null;
        }
        
        /// <summary>
        /// This method will update the Roles and permissions for relevant Account Id
        /// </summary>
        /// <param name="model">PermissionsModel model</param>
        /// <returns>True if the records updated</returns>
        public bool UpdateRolesAndPermissions(PermissionsModel model)
        {
            if (Equals(model, null))
            {
                throw new Exception("Permission model cannot be null.");
            }

            bool recordsUpdated = false;
            var account = _accountRepository.GetByAccountID(model.AccountId);
            if (!Equals(account, null))
            {
                if (account.UserID.HasValue)
                {
                    var user = WebSecurity.GetUser(account.UserID.Value);

                    if (Equals(user, null))
                    {
                        throw new Exception("User not found.");
                    }

                    recordsUpdated = _profileHelper.UpdateRolesAndPermissions((Guid)user.ProviderUserKey, model.SelectedStoreIds,  model.SelectedRoleIds);
                }
                else
                { 
                    throw new Exception("User not exists."); 
                }
            }

            return recordsUpdated;
        }
        #endregion        
    }
}