﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
	public interface ICaseRequestService
	{
		CaseRequestModel GetCaseRequest(int caseRequestId, NameValueCollection expands);
		CaseRequestListModel GetCaseRequests(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
		CaseRequestModel CreateCaseRequest(CaseRequestModel model);
		CaseRequestModel UpdateCaseRequest(int caseRequestId, CaseRequestModel model);
		bool DeleteCaseRequest(int caseRequestId);

        /// <summary>
        /// Znode Version 8.0
        /// To Create CaseNote
        /// </summary>
        /// <param name="model">NoteModel model</param>
        /// <returns>returns NoteModel</returns>
        NoteModel CreateCaseNote(NoteModel model);

        /// <summary>
        /// To get CaseRequest list with paging
        /// </summary>
        /// <param name="expands">NameValueCollection expands </param>
        /// <param name="filters">List<Tuple<string, string, string>> filters</param>
        /// <param name="sorts">NameValueCollection sorts</param>
        /// <param name="page">NameValueCollection page</param>
        /// <returns>returns CaseRequestListModel</returns>
        CaseRequestListModel GetCaseRequestList(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// To GetCaseStatus
        /// </summary>
        /// <param name="expands"></param>
        /// <param name="filters"></param>
        /// <param name="sorts"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        CaseStatusListModel GetCaseStatus(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// To GetCasePriority
        /// </summary>
        /// <param name="expands"></param>
        /// <param name="filters"></param>
        /// <param name="sorts"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        CasePriorityListModel GetCasePriority(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// To GetCaseNote
        /// </summary>
        /// <param name="expands"></param>
        /// <param name="filters"></param>
        /// <param name="sorts"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        NoteListModel GetCaseNote(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

	}
}
