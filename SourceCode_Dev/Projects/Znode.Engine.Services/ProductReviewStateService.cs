﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using ProductReviewStateRepository = ZNode.Libraries.DataAccess.Service.ProductReviewStateService;

namespace Znode.Engine.Services
{
    public class ProductReviewStateService : BaseService, IProductReviewStateService
    {
        #region Private Variables
        private readonly ProductReviewStateRepository _productReviewStateRepository;
        #endregion

        public ProductReviewStateService()
        {
            _productReviewStateRepository = new ProductReviewStateRepository();
        }

        public ProductReviewStateListModel GetProductReviewStates(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new ProductReviewStateListModel();
            var productReviewStates = new TList<ProductReviewState>();
            var tempList = new TList<ProductReviewState>();

            var query = new ProductReviewStateQuery();
            var sortBuilder = new ProductReviewStateSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            tempList = _productReviewStateRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                model.PageSize = model.TotalResults;
            }

            // Now go through the list and get any expands
            foreach (var productReviewState in tempList)
            {
                GetExpands(expands, productReviewState);
                productReviewStates.Add(productReviewState);
            }

            // Map each item and add to the list
            foreach (var a in productReviewStates)
            {
                model.ProductReviewStates.Add(ProductReviewStateMap.ToModel(a));
            }

            return model;
        }
        private void SetFiltering(List<Tuple<string, string, string>> filters, ProductReviewStateQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;

                if (filterKey == FilterKeys.ProductReviewStateName) SetQueryParameter(ProductReviewStateColumn.ReviewStateName, filterOperator, filterValue, query);
            }
        }

        private void SetSorting(NameValueCollection sorts, ProductReviewStateSortBuilder sortBuilder)
        {
            if (sorts.HasKeys())
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);

                    if (key == SortKeys.ProductReviewStateName) SetSortDirection(ProductReviewStateColumn.ReviewStateName, value, sortBuilder);
                }
            }
        }
        private void GetExpands(NameValueCollection expands, ProductReviewState productReviewState)
        {
            if (expands.HasKeys())
            {

            }
        }
    }
}
