﻿using System.Web;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.Services
{
	public class LicenseService : ILicenseService
	{
		protected string DomainName = "";
		private string _customerIpAddress = HttpContext.Current.Request.UserHostAddress;

		public bool InstallLicense(string licenseType, string serialNumber, string name, string email, out string errorMessage)
		{
			DomainName = HttpContext.Current.Request.Url.Host + HttpContext.Current.Request.ApplicationPath;

			// Install the license
			var licenseMgr = new ZNodeLicenseManager();
			var licenseInstalled = licenseMgr.InstallLicense(GetLicenseType(licenseType), serialNumber, name, email, out errorMessage);

			if (licenseInstalled)
			{
				ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.ActivationSuccess, _customerIpAddress, DomainName, name, email, "Your Znode license has been successfully activated.");
			}
			else
			{
				ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.ActivationFailed, _customerIpAddress, DomainName, name, email, "Failed to activate license.");
			}

			return licenseInstalled;
		}

		private static ZNodeLicenseType GetLicenseType(string licenseType)
		{
			var trialLicense = ZNodeLicenseType.Trial; // Default

			if (licenseType == "Api")
			{
				trialLicense = ZNodeLicenseType.Api;
			}

			return trialLicense;
		}

		public bool ValidateLicense()
		{
			var licenseMgr = new ZNodeLicenseManager();
			var licenseType = licenseMgr.Validate();

			if (licenseType == ZNodeLicenseType.Invalid)
			{
				return false;
			}

			return true;
		}
	}
}
