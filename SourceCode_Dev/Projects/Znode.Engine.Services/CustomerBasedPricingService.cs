﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Libraries.Helpers;
using Znode.Libraries.Helpers.Constants;
using Znode.Libraries.Helpers.Extensions;
using ZNode.Libraries.DataAccess.Custom;

namespace Znode.Engine.Services
{
    public class CustomerBasedPricingService : BaseService, ICustomerBasedPricingService
    {
        #region public methods
        #region Get Customer Pricing List & Search method
        public CustomerBasedPricingListModel GetCustomerBasedPricing(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            CustomerBasedPricingListModel list = new CustomerBasedPricingListModel();
            string whereClause = StringHelper.GenerateDynamicWhereClause(filters);
            int totalRowCount = 0;
            var pagingStart = 1;
            var pagingLength = 0;
            string orderBy = string.Empty;

            pagingLength = Convert.ToInt32(page.Get(PageKeys.Size));

            StoredProcedureHelper storedProcedureHelper = new StoredProcedureHelper();
            DataSet resultDataSet = storedProcedureHelper.SP_DataDetailsSpConfig(whereClause, string.Empty, StoredProcedureKeys.SpGetCustomerPricing, orderBy, pagingStart.ToString(), pagingLength.ToString(), out totalRowCount);

            list.CustomerBasedPricing = resultDataSet.Tables[0].ToList<CustomerBasedPricingModel>().ToCollection();
            list.TotalResults = totalRowCount;
            list.PageSize = pagingLength;
            list.PageIndex = pagingStart;
            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                list.PageSize = list.TotalResults;
            }

            return list;
        }
        #endregion 

        #region Get Customer Pricing List with poduct in Orders
        public CustomerBasedPricingListModel GetCustomerPricingProduct(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            CustomerBasedPricingListModel list = new CustomerBasedPricingListModel();
            string whereClause = StringHelper.GenerateDynamicWhereClause(filters);
            int totalRowCount = 0;
            var pagingStart = 1;
            var pagingLength = 0;
            string orderBy = string.Empty;
            //string innerWhereClause = ;

            pagingLength = Convert.ToInt32(page.Get(PageKeys.Size));

            StoredProcedureHelper storedProcedureHelper = new StoredProcedureHelper();
            DataSet resultDataSet = storedProcedureHelper.SP_DataDetailsSpConfig(whereClause, string.Empty, StoredProcedureKeys.SPGetCustomerPricingProduct, orderBy, pagingStart.ToString(), pagingLength.ToString(), out totalRowCount,null,null);

            list.CustomerBasedPricing = resultDataSet.Tables[0].ToList<CustomerBasedPricingModel>().ToCollection();
            list.TotalResults = totalRowCount;
            list.PageSize = pagingLength;
            list.PageIndex = pagingStart;
            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                list.PageSize = list.TotalResults;
            }

            return list;
        } 
        #endregion
        #endregion
    }
}
