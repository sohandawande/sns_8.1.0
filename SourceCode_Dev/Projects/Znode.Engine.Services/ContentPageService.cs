﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Helpers.Constants;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;
using ContentPageRepository = ZNode.Libraries.DataAccess.Service.ContentPageService;
using MasterPageRepository = ZNode.Libraries.DataAccess.Service.MasterPageService;
using PortalRepository = ZNode.Libraries.DataAccess.Service.PortalService;

namespace Znode.Engine.Services
{
    public class ContentPageService : BaseService, IContentPageService
    {
        #region Private Variables

        private readonly ContentPageRepository _contentPageRepository;
        private readonly ContentPageAdmin contentPageAdmin;
        private readonly PortalRepository _portalTypeRepository;
        private readonly MasterPageRepository _masterPageRepository;

        #endregion

        #region Default Constructor

        public ContentPageService()
        {
            _contentPageRepository = new ContentPageRepository();
            contentPageAdmin = new ContentPageAdmin();
            _portalTypeRepository = new PortalRepository();
            _masterPageRepository = new MasterPageRepository();
        }

        #endregion

        #region Public Methods

        public ContentPageListModel GetContentPages(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new ContentPageListModel();
            var contentPages = new TList<ContentPage>();
            var tempList = new TList<ContentPage>();
            var query = new ContentPageQuery();
            var sortBuilder = new ContentPageSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            tempList = _contentPageRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (pagingLength.Equals(int.MaxValue))
            {
                model.PageSize = model.TotalResults;
            }

            // Now go through the list and get any expands
            foreach (var contentPage in tempList)
            {
                GetExpands(expands, contentPage);
                contentPages.Add(contentPage);
            }

            // Map each item and add to the list
            foreach (var a in contentPages)
            {
                model.ContentPages.Add(ContentPageMap.ToModel(a));
            }

            return model;
        }

        public ContentPageModel CreateContentPage(ContentPageModel model)
        {
            if (Equals(model, null))
            {
                throw new Exception("Content page model cannot be null.");
            }

            if (!contentPageAdmin.IsNameAvailable(model.Name, model.PortalID, model.LocaleId))
            {
                throw new Exception("A page with this name already exists in the database. Please enter a different name.");
            }
            else
            {
                return this.AddPage(model) ? model : null;
            }
        }

        public ContentPageModel Clone(ContentPageModel contentPageModel)
        {
            ContentPage page = ContentPageMap.ToEntity(contentPageModel);
            ContentPageModel model = ContentPageMap.ToModel(page.Clone() as ContentPage);
            return model;
        }

        public bool AddPage(ContentPageModel contentPageModel)
        {
            ContentPage contentPage = ContentPageMap.ToEntity(contentPageModel);
            ContentPageAdmin contentPageAdmin = new ContentPageAdmin();
            return contentPageAdmin.AddPage(contentPage, contentPageModel.Html, contentPageModel.PortalID, contentPageModel.LocaleId.ToString(), contentPageModel.UpdatedUser, contentPageModel.MappedSeoUrl, contentPageModel.IsUrlRedirectEnabled, true);
        }

        public ContentPageModel GetContentPage(int contentPageId, NameValueCollection expands)
        {
            var contentPage = _contentPageRepository.GetByContentPageID(contentPageId);
            ContentPageModel contentPageModel = ContentPageMap.ToModel(contentPage);
            contentPageModel.Html = GetPageHTMLByName(contentPageModel.Name, contentPageModel.PortalID, contentPageModel.LocaleId.ToString());
            return contentPageModel;
        }

        public bool DeleteContentPage(int contentPageId)
        {
            if (contentPageId < 1)
            {
                throw new Exception("Catalog ID cannot be less than 1.");
            }

            var contentPage = ContentPageMap.ToEntity(GetContentPage(contentPageId, null));

            if (!contentPage.AllowDelete)
            {
                throw new Exception("This page is a reserved page and cannot be deleted.");
            }

            return contentPageAdmin.DeletePage(contentPage);
        }

        public bool UpdateContentPage(int contentPageId, ContentPageModel model)
        {
            if (contentPageId < 1)
            {
                throw new Exception("Content Page ID cannot be less than 1.");
            }

            if (Equals(model, null))
            {
                throw new Exception("Content Page model cannot be null.");
            }

            ContentPage contentPage = ContentPageMap.ToEntity(model);

            return contentPageAdmin.UpdatePage(contentPage, model.Html, model.OldHtml, model.PortalID, model.LocaleId.ToString(), model.UpdatedUser, model.MappedSeoUrl, model.IsUrlRedirectEnabled, true);
        }

        public bool RevertRevision(int pageRevisionId, string updatedUser, string oldHtml)
        {
            return contentPageAdmin.RevertToRevision(pageRevisionId, updatedUser, oldHtml);
        }

        public string GetPageHTMLByName(string pageName, int portalId, string localeId)
        {
            return contentPageAdmin.GetPageHTMLByName(pageName, portalId, localeId);
        }

        public string GetContentPageByName(string contentPageName, string extension)
        {
            ContentPageModel model = new ContentPageModel();
            string filePath = string.Format("{0}{1}{2}{3}", ZNodeConfigManager.EnvironmentConfig.ContentPath, contentPageName.Replace("'", string.Empty), ".", extension.Replace("'", string.Empty));

            if (ZNodeStorageManager.Exists(filePath))
            {
                try
                {
                    model.Html = ZNodeStorageManager.ReadTextStorage(filePath);
                    return model.Html;
                }
                catch
                {
                    return model.Html;
                }
            }
            return model.Html;
        }
        #endregion

        #region Private Methods

        /// <summary>
        /// Set the Filter.
        /// </summary>
        /// <param name="filters">List<Tuple<string, string, string>> filters</param>
        /// <param name="query">ContentPageQuery query</param>
        private void SetFiltering(List<Tuple<string, string, string>> filters, ContentPageQuery query)
        {
            bool isPortalIdPresent = false;
            string UserName = string.Empty;
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;
                if (!Equals(filterKey, FilterKeys.UserName))
                {
                    if (filterKey.Equals(FilterKeys.PortalId))
                    {
                        isPortalIdPresent = true;
                        SetQueryParameter(ContentPageColumn.PortalID, filterOperator, filterValue, query);
                    }

                    if (filterKey.Equals(FilterKeys.ContentPageID)) { SetQueryParameter(ContentPageColumn.ContentPageID, filterOperator, filterValue, query); }
                    if (filterKey.Equals(FilterKeys.LocaleId)) { SetQueryParameter(ContentPageColumn.LocaleId, filterOperator, filterValue, query); }
                    if (filterKey.Equals(FilterKeys.PageName)) { SetQueryParameter(ContentPageColumn.Name, filterOperator, filterValue, query); }
                }
                else
                {
                    UserName = filterValue;
                }
            }
            if (!isPortalIdPresent)
            {
                string portalIds = GetAvailablePortals(UserName);
                if (!string.IsNullOrEmpty(portalIds) && !Equals(portalIds, "0"))
                {
                    query.AppendIn(SqlUtil.AND, ContentPageColumn.PortalID, portalIds.Split(','));
                }
            }
        }

        /// <summary>
        /// Set the Sort.
        /// </summary>
        /// <param name="sorts">NameValueCollection sorts</param>
        /// <param name="sortBuilder">ContentPageSortBuilder sortBuilder</param>
        private void SetSorting(NameValueCollection sorts, ContentPageSortBuilder sortBuilder)
        {
            if (sorts.HasKeys())
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key).ToLower();

                    if (value.Equals(SortKeys.StoreName.ToLower())) { SetSortDirection(ContentPageColumn.PortalID, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (value.Equals(SortKeys.ContentPageID.ToLower())) { SetSortDirection(ContentPageColumn.ContentPageID, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (value.Equals(SortKeys.MasterPageName.ToLower())) { SetSortDirection(ContentPageColumn.MasterPageID, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (value.Equals(SortKeys.PageName.ToLower())) { SetSortDirection(ContentPageColumn.Name, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                }
            }
            else
            {
                SetSortDirection(ContentPageColumn.ContentPageID, sorts[StoredProcedureKeys.sortDirKey], sortBuilder);
            }
        }

        /// <summary>
        /// Gets the Expands.
        /// </summary>
        /// <param name="expands">NameValueCollection expands</param>
        /// <param name="contentPage">ContentPage contentPage</param>
        private void GetExpands(NameValueCollection expands, ContentPage contentPage)
        {
            if (expands.HasKeys())
            {
                ExpandPortal(expands, contentPage);
                ExpandMasterPage(expands, contentPage);
            }
        }

        /// <summary>
        /// This method gives the expand for portal
        /// </summary>
        /// <param name="expands">expands</param>
        /// <param name="ContentPage">contentPage</param>
        private void ExpandPortal(NameValueCollection expands, ContentPage contentPage)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Portal)))
            {
                if (contentPage.PortalID > 0)
                {
                    var portals = _portalTypeRepository.GetByPortalID(contentPage.PortalID);
                    if (!Equals(portals, null))
                    {
                        contentPage.PortalIDSource = portals;
                    }
                }
            }
        }

        /// <summary>
        /// This method gives the expand for master page
        /// </summary>
        /// <param name="expands">expands</param>
        /// <param name="ContentPage">contentPage</param>
        private void ExpandMasterPage(NameValueCollection expands, ContentPage contentPage)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.MasterPage)))
            {
                if (!Equals(contentPage.ThemeID, null))
                {
                    MasterPageKey key = new MasterPageKey();
                    key.MasterPageID = contentPage.ThemeID.Value;
                    var masterPage = _masterPageRepository.Get(key);
                    if (!Equals(masterPage, null))
                    {
                        contentPage.MasterPageIDSource = masterPage;
                    }
                }
            }
        }

        #endregion
    }
}
