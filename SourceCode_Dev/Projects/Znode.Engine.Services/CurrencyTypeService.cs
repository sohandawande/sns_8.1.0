﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Maps;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using CurrencyTypeRepository = ZNode.Libraries.DataAccess.Service.CurrencyTypeService;

namespace Znode.Engine.Services
{
    public class CurrencyTypeService : BaseService, ICurrencyTypeService
    {
        #region Private Variables
        private readonly CurrencyTypeRepository _currencyTypeRepository;
        #endregion

        public CurrencyTypeService()
        {
            _currencyTypeRepository = new CurrencyTypeRepository();
        }

        public CurrencyTypeModel GetCurrencyType(int currencyTypeId, NameValueCollection expands)
        {
            return CurrencyTypeMap.ToModel(_currencyTypeRepository.GetByCurrencyTypeID(currencyTypeId));
        }

        public CurrencyTypeListModel GetCurrencyTypes(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new CurrencyTypeListModel();
            var currencyTypeList = new TList<CurrencyType>();
            var tempList = new TList<CurrencyType>();

            var query = new CurrencyTypeQuery();
            var sortBuilder = new CurrencyTypeSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            tempList = _currencyTypeRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                model.PageSize = model.TotalResults;
            }

            // Now go through the list and get any expands
            foreach (var currencyType in tempList)
            {
               currencyTypeList.Add(currencyType);
            }

            // Map each item and add to the list
            foreach (var a in currencyTypeList)
            {
                model.Currency.Add(CurrencyTypeMap.ToModel(a));
            }

            return model;
        }       

        public CurrencyTypeModel UpdateCurrencyType(int currencyTypeId,CurrencyTypeModel currencyTypeModel)
        {
            if (currencyTypeId < 1)
            {
                throw new Exception("Currency Type ID cannot be less than 1.");
            }

            if (Equals(currencyTypeModel, null))
            {
                throw new Exception("Currency Type model cannot be null.");
            }

            var currencyType = _currencyTypeRepository.GetByCurrencyTypeID(currencyTypeId);
            if (!Equals(currencyType,null))
            {
                // Set portal ID
                currencyType.CurrencyTypeID = currencyTypeId;
                if(!string.IsNullOrEmpty(currencyType.Symbol))
                {
                    currencyTypeModel.Symbol = currencyType.Symbol;
                }
                var currrencyTypeToUpdate = CurrencyTypeMap.ToEntity(currencyTypeModel);
                currrencyTypeToUpdate.OriginalCurrencyTypeID = currencyTypeModel.CurrencyTypeID;

                var updated = _currencyTypeRepository.Update(currrencyTypeToUpdate);
                if (updated)
                {
                    currencyType = _currencyTypeRepository.GetByCurrencyTypeID(currencyTypeId);
                    return CurrencyTypeMap.ToModel(currencyType);
                }
            }

            return null;
        }

    }
}
