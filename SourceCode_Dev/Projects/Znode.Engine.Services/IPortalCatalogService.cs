﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
	public interface IPortalCatalogService
	{
		PortalCatalogModel GetPortalCatalog(int portalCatalogId, NameValueCollection expands);
		PortalCatalogListModel GetPortalCatalogs(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
		PortalCatalogModel CreatePortalCatalog(PortalCatalogModel model);
		PortalCatalogModel UpdatePortalCatalog(int portalCatalogId, PortalCatalogModel model);
		bool DeletePortalCatalog(int portalCatalogId);

        // <summary>
        /// Znode 8.0 Version
        /// Gets PortalcatlogListModel according to portal id.
        /// </summary>
        /// <param name="portalId">Portal Id according to which PortalCatalogListModel will be returns</param>
        /// <param name="expands">Expands to be sent with PortalCatalogListModel</param>
        /// <returns></returns>
        PortalCatalogListModel GetPortalCatalogByPortalId(int portalId);
	}
}
