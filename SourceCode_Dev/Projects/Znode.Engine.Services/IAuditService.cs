﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
	public interface IAuditService
	{
		AuditModel GetAudit(long auditId, NameValueCollection expands);
		AuditListModel GetAudits(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
		AuditModel UpdateAudit(long auditId, AuditModel model);
		bool DeleteAudit(long auditId);
	}
}
