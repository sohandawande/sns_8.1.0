﻿using StructureMap;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;
using Znode.Engine.Promotions;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using DiscountTypeRepository = ZNode.Libraries.DataAccess.Service.DiscountTypeService;
using ZNode.Libraries.Framework.Business;
using Znode.Libraries.Helpers.Constants;
using ZNode.Libraries.Admin;

namespace Znode.Engine.Services
{
    public class PromotionTypeService : BaseService, IPromotionTypeService
    {
        private readonly DiscountTypeRepository _discountTypeRepository;

        public PromotionTypeService()
        {
            _discountTypeRepository = new DiscountTypeRepository();
        }

        public PromotionTypeModel GetPromotionType(int promotionTypeId)
        {
            var promotionType = _discountTypeRepository.GetByDiscountTypeID(promotionTypeId);
            return PromotionTypeMap.ToModel(promotionType);
        }

        public PromotionTypeListModel GetPromotionTypes(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new PromotionTypeListModel();
            var promotionTypes = new TList<DiscountType>();

            var query = new DiscountTypeQuery();
            var sortBuilder = new DiscountTypeSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            promotionTypes = _discountTypeRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                model.PageSize = model.TotalResults;
            }

            // Map each item and add to the list
            foreach (var p in promotionTypes)
            {
                model.PromotionTypes.Add(PromotionTypeMap.ToModel(p));
            }

            return model;
        }

        public PromotionTypeModel CreatePromotionType(PromotionTypeModel model)
        {
            if (model == null)
            {
                throw new Exception("Promotion type model cannot be null.");
            }

            var entity = PromotionTypeMap.ToEntity(model);
            var promotionType = _discountTypeRepository.Save(entity);
            return PromotionTypeMap.ToModel(promotionType);
        }

        public PromotionTypeModel UpdatePromotionType(int promotionTypeId, PromotionTypeModel model)
        {
            if (promotionTypeId < 1)
            {
                throw new Exception("Promotion type ID cannot be less than 1.");
            }

            if (model == null)
            {
                throw new Exception("Promotion type model cannot be null.");
            }

            var promotionType = _discountTypeRepository.GetByDiscountTypeID(promotionTypeId);
            if (promotionType != null)
            {
                // Set the promotion type ID and map
                model.PromotionTypeId = promotionTypeId;
                var promotionTypeToUpdate = PromotionTypeMap.ToEntity(model);

                var updated = _discountTypeRepository.Update(promotionTypeToUpdate);
                if (updated)
                {
                    promotionType = _discountTypeRepository.GetByDiscountTypeID(promotionTypeId);
                    return PromotionTypeMap.ToModel(promotionType);
                }
            }

            return null;
        }

        public bool DeletePromotionType(int promotionTypeId)
        {
            if (promotionTypeId < 1)
            {
                throw new Exception("Promotion type ID cannot be less than 1.");
            }

            var promotionType = _discountTypeRepository.GetByDiscountTypeID(promotionTypeId);

            if (this.CheckAssociatedPromtions(promotionTypeId))
            {
                throw new Exception("Not able to delete promotion type " + promotionType.ClassName + ". Ensure all associated promotions are deleted first.");
            }
            if (promotionType != null)
            {
                return _discountTypeRepository.Delete(promotionType);
            }

            return false;
        }

        private void SetFiltering(List<Tuple<string, string, string>> filters, DiscountTypeQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;

                if (String.Equals(filterKey, FilterKeys.ClassName)) SetQueryParameter(DiscountTypeColumn.ClassName, filterOperator, filterValue, query);
                if (String.Equals(filterKey, FilterKeys.ClassType)) SetQueryParameter(DiscountTypeColumn.ClassType, filterOperator, filterValue, query);
                if (String.Equals(filterKey, FilterKeys.IsActive)) SetQueryParameter(DiscountTypeColumn.ActiveInd, filterOperator, filterValue, query);
                if (String.Equals(filterKey, FilterKeys.Name)) SetQueryParameter(DiscountTypeColumn.Name, filterOperator, filterValue, query);
                if (String.Equals(filterKey, FilterKeys.PromotionTypeId)) SetQueryParameter(DiscountTypeColumn.DiscountTypeID, filterOperator, filterValue, query);
            }
        }

        private void SetSorting(NameValueCollection sorts, DiscountTypeSortBuilder sortBuilder)
        {
            if (sorts.HasKeys())
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);
                    if (value.Equals(SortKeys.ClassName)) { SetSortDirection(DiscountTypeColumn.ClassName, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (value.Equals(SortKeys.Name)) { SetSortDirection(DiscountTypeColumn.Name, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (value.Equals(SortKeys.PromotionTypeId)) { SetSortDirection(DiscountTypeColumn.DiscountTypeID, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }

                }
            }
        }

        private void SetQueryParameter(DiscountTypeColumn column, string filterOperator, string filterValue, DiscountTypeQuery query)
        {
            base.SetQueryParameter(column, filterOperator, filterValue, query);
        }

        private void SetSortDirection(DiscountTypeColumn column, string value, DiscountTypeSortBuilder sortBuilder)
        {
            base.SetSortDirection(column, value, sortBuilder);
        }

        #region Znode Version 8.0
        /// <summary>
        /// Check PromotionType associated with Promotions
        /// </summary>
        /// <param name="PromotionTypeId">int PromotionTypeId</param>
        /// <returns>Return true is associate with else return fale</returns>
        private bool CheckAssociatedPromtions(int PromotionTypeId)
        {
            PromotionHelper promotionHelper = new PromotionHelper();
            if (PromotionTypeId > 0)
            {
                return promotionHelper.CheckAssociatedPromtions(PromotionTypeId);
            }
            else
            {
                return false;
            }
        }

        public PromotionTypeListModel GetAllPromotionTypesNotInDatabase()
        {
            PromotionTypeListModel model = new PromotionTypeListModel();
            PromotionTypeListModel list = new PromotionTypeListModel();


            var promotionTypes = ZnodePromotionManager.GetAvailablePromotionTypes();

            foreach (var p in promotionTypes)
            {
                model.PromotionTypes.Add(PromotionTypeMap.ToModel(p));
            }

            PromotionTypeListModel availablePromotions = this.GetPromotionTypes(new List<Tuple<string, string, string>>(), new NameValueCollection(), new NameValueCollection());
            if (availablePromotions.PromotionTypes.Count < model.PromotionTypes.Count)
            {
                foreach (var x in model.PromotionTypes)
                {
                    var found = false;
                    foreach (var y in availablePromotions.PromotionTypes)
                    {
                        if (!found)
                        {
                            if (string.Equals(x.ClassName, y.ClassName))
                            {
                                found = true;
                            }
                        }
                    }

                    if (!found)
                    {
                        list.PromotionTypes.Add(x);
                    }
                }
            }
            return list;
        }

        public PromotionTypeListModel GetFranchisePromotionTypes(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            PromotionTypeListModel promotionList = new PromotionTypeListModel();
            // First get types from the database
            var promoAdmin = new PromotionAdmin();
            var discountTypes = promoAdmin.GetAllDiscountTypes().FindAll(DiscountTypeColumn.ActiveInd, true);

            var availablePromoTypes = ZnodePromotionManager.GetAvailablePromotionTypes();
            foreach (var p in availablePromoTypes)
            {
                foreach (var item in discountTypes)
                {
                    if (Equals(p.ClassName, item.ClassName) && p.AvailableForFranchise)
                    {
                        promotionList.PromotionTypes.Add(new PromotionTypeModel()
                        {
                            ClassName = item.ClassName,
                            ClassType = item.ClassType,
                            Name = item.Name,
                            PromotionTypeId = item.DiscountTypeID
                        });
                    }
                }
            }

            return promotionList;
        }
        #endregion
    }
}
