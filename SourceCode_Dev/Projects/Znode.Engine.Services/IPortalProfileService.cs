﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    public interface IPortalProfileService
    {
        /// <summary>
        /// Creates a portal profile.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        PortalProfileModel CreatePortalProfile(PortalProfileModel model);

        /// <summary>
        /// Deletes portal profile.
        /// </summary>
        /// <param name="portalProfileId"></param>
        /// <returns></returns>
        bool DeletePortalProfile(int portalProfileId);

        /// <summary>
        /// Gets a portal portfile according portal profile id.
        /// </summary>
        /// <param name="protalProfileId"></param>
        /// <param name="expands"></param>
        /// <returns></returns>
        PortalProfileModel GetPortalProfile(int protalProfileId, NameValueCollection expands);

        /// <summary>
        /// Gets the list of portal profiles.
        /// </summary>
        /// <param name="expands"></param>
        /// <param name="filters"></param>
        /// <param name="sorts"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        PortalProfileListModel GetPortalProfiles(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Updates a portal profile.
        /// </summary>
        /// <param name="portalProfileId"></param>
        /// <param name="portalProfileModel"></param>
        /// <returns></returns>
        PortalProfileModel UpdatePortalProfile(int portalProfileId, PortalProfileModel portalProfileModel);

        /// <summary>
        /// Get portal profiles.
        /// </summary>
        /// <param name="expands">Expands for portal profiles.</param>
        /// <param name="filters">Filters for portal profiles.</param>
        /// <param name="sorts">Sorting of portal profiles.</param>
        /// <param name="page">Paging information for portal profiles.</param>
        /// <returns>Portal profilt list model.</returns>
        PortalProfileListModel GetPortalProfileListDetails(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Get the List of PortalProfiles by portalId
        /// </summary>
        /// <param name="portalId">portalId to retrive portalProfileList</param>
        /// <returns>PortalProfileListModel</returns>
        PortalProfileListModel GetPortalProfilesByPortalId(int portalId);
    }
}
