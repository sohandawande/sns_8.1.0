﻿using Microsoft.Web.WebPages.OAuth;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using WebMatrix.WebData;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Helpers;
using Znode.Libraries.Helpers.Constants;
using Znode.Libraries.Helpers.Extensions;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;
using AccountAdminRepository = ZNode.Libraries.Admin.AccountAdmin;
using AccountPaymentRepository = ZNode.Libraries.DataAccess.Service.AccountPaymentService;
using AccountProfileRepositry = ZNode.Libraries.DataAccess.Service.AccountProfileService;
using AccountRepository = ZNode.Libraries.DataAccess.Service.AccountService;
using AccountTypeRepository = ZNode.Libraries.DataAccess.Service.AccountTypeService;
using AddressRepositry = ZNode.Libraries.DataAccess.Service.AddressService;
using GiftCardRepository = ZNode.Libraries.DataAccess.Service.GiftCardService;
using OrderLineItemRepository = ZNode.Libraries.DataAccess.Service.OrderLineItemService;
using OrderRepositry = ZNode.Libraries.DataAccess.Service.OrderService;
using OrderShipmentRepository = ZNode.Libraries.DataAccess.Service.OrderShipmentService;
using PortalProfileRepository = ZNode.Libraries.DataAccess.Service.PortalProfileService;
using PortalRepository = ZNode.Libraries.DataAccess.Service.PortalService;
using ProfileRepositry = ZNode.Libraries.DataAccess.Service.ProfileService;
using ReferralCommissionTypeRepository = ZNode.Libraries.DataAccess.Service.ReferralCommissionTypeService;
using WishListRepository = ZNode.Libraries.DataAccess.Service.WishListService;

namespace Znode.Engine.Services
{
    public partial class AccountService : BaseService, IAccountService
    {
        #region Private Variables
        private readonly AccountProfileRepositry _accountProfileRepository;
        private readonly AccountRepository _accountRepository;
        private readonly AccountTypeRepository _accountTypeRepository;
        private readonly AddressRepositry _addressRepository;
        private readonly GiftCardRepository _giftCardRepository;
        private readonly OrderRepositry _orderRepository;
        private readonly OrderLineItemRepository _orderLineItemRepository;
        private readonly OrderShipmentRepository _orderShipmentRepository;
        private readonly PortalRepository _portalRepository;
        private readonly PortalProfileRepository _portalProfileRepository;
        private readonly ProfileRepositry _profileRepository;
        private readonly ZNodeUserAccountBase _userAccountBase;
        private readonly WishListRepository _wishlistRepository;
        private readonly ReferralCommissionTypeRepository _referralCommissionTypeRepository;
        private readonly AccountPaymentRepository _accountPaymentRepository;
        private ProfileHelper _profileHelper;
        private string apiUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority); //PRFT Custom Code
        #endregion



        #region Private Consts Variables
        private const string AccountActivationHTMLTemplateName = "AccountActivatiedNotification.htm";
        private const string FranchiseAccountActivationHTMLTemplateName = "NewUserAccount_en.htm";
        //PRFT Custom Code : Start
        private const int SuperUserProfileId = 5;
        //PRFT Custom Code : End
        #endregion

        public AccountService()
        {
            _accountProfileRepository = new AccountProfileRepositry();
            _accountRepository = new AccountRepository();
            _accountTypeRepository = new AccountTypeRepository();
            _addressRepository = new AddressRepositry();
            _giftCardRepository = new GiftCardRepository();
            _orderRepository = new OrderRepositry();
            _orderLineItemRepository = new OrderLineItemRepository();
            _orderShipmentRepository = new OrderShipmentRepository();
            _portalRepository = new PortalRepository();
            _portalProfileRepository = new PortalProfileRepository();
            _profileRepository = new ProfileRepositry();
            _userAccountBase = new ZNodeUserAccountBase();
            _wishlistRepository = new WishListRepository();
            _referralCommissionTypeRepository = new ReferralCommissionTypeRepository();
            _profileHelper = new ProfileHelper();
            _accountPaymentRepository = new AccountPaymentRepository();
        }

        public AccountModel GetAccount(int accountId, NameValueCollection expands)
        {
            AccountModel accountModel = null;
            var account = _accountRepository.GetByAccountID(accountId);            
            if (!Equals(account, null))
            {
                GetExpands(expands, account);
                accountModel = AccountMap.ToModel(account);

                if (!Equals(accountModel, null))
                {
                    //PRFT Custom Code:Start
                    UpdateAddressExtensionDetails(accountModel);
                    //PRFT Custom Code:End

                    accountModel.TrackingLink = GetTrackingLink(accountId);


                    return accountModel;
                }
            }
            return accountModel;
        }

        public AccountModel GetAccountByUserId(Guid userId, NameValueCollection expands)
        {
            var account = _accountRepository.GetByUserID(userId);
            if (account[0] != null)
            {
                GetExpands(expands, account[0]);
                //PRFT Custom Code:Start
                AccountModel accountModel = AccountMap.ToModel(account[0]);
                if (!Equals(accountModel, null))
                {
                    UpdateAddressExtensionDetails(accountModel);
                }
                return accountModel;
                //PRFT Custom Code:End
            }
            return AccountMap.ToModel(account[0]);
        }

        public AccountModel GetAccountByUsername(string username, NameValueCollection expands)
        {
            var user = Membership.GetUser(username);
            if (user == null)
            {
                throw new Exception("User " + username + " not found.");
            }

            var account = _accountRepository.GetByUserID((Guid)user.ProviderUserKey);
            if (account[0] != null)
            {
                GetExpands(expands, account[0]);
            }

            return AccountMap.ToModel(account[0]);
        }

        public AccountListModel GetAccounts(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new AccountListModel();
            var accounts = new TList<Account>();
            var tempList = new TList<Account>();

            var query = new AccountQuery();
            var sortBuilder = new AccountSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            tempList = _accountRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                model.PageSize = model.TotalResults;
            }

            // Now go through the list and get any expands
            foreach (var account in tempList)
            {
                GetExpands(expands, account);
                accounts.Add(account);
            }

            // Map each item and add to the list
            foreach (var a in accounts)
            {
                model.Accounts.Add(AccountMap.ToModel(a));
            }

            return model;
        }

        public AccountModel CreateAccount(int portalId, AccountModel model)
        {
            bool isGuestUser = false;

            if (Equals(model, null))
            {
                throw new Exception("Account model cannot be null.");
            }

            if (Equals(GetDefaultRegisteredProfile(portalId), null))
            {
                throw new ZnodeException(ErrorCodes.ProfileNotPresent, "You can not register new account, please contact administrator.");
            }

            var account = AccountMap.ToEntity(model);

            var expand = new NameValueCollection { { ExpandKeys.Addresses, ExpandKeys.Addresses }, { ExpandKeys.Profiles, ExpandKeys.Profiles } };

            if (!Equals(model.User, null) && !String.IsNullOrEmpty(model.User.Username))
            {
                isGuestUser = model.IsSinglePageCheckoutUser ? false : true;

                // Check if username is available                 
                //var usernameAvailable = IsUsernameAvailable(0, model.User.Username);
                if (!WebSecurity.UserExists(model.User.Username))
                {
                    Guid UserId;
                    string password = string.Empty;
                    string confirmationToken = CreateNewUser(model, out UserId, out password);

                    //Set the confirmation for the user.
                    if (!Equals(confirmationToken, null))
                    {
                        WebSecurity.ConfirmAccount(confirmationToken);
                    }
                    else
                    {
                        throw new ZnodeException(ErrorCodes.MembershipError, "Could not create user.");
                    }
                    account.Email = model.User.Email;
                    account.UserID = UserId;

                    //PRFT Custom Code : Start
                    //account.ParentAccountID = model.User.ParentAccountId;
                    //PRFT Custom Code : Start

                    // Add registered profile information and other details 
                    UpdateRegisterdProfile(account, portalId);

                    //TODO// Log the password
                    ZNodeUserAccountBase.LogPassword((Guid)account.UserID, model.User.Password);

                    //Send Mail
                    SendMailUserAccount(model.User.Username, model.User.Password, model.User.Email);
                }
                else
                {
                    throw new ZnodeException(ErrorCodes.UserNameUnavailable, "Username " + model.User.Username + " is not available.");
                }

                expand.Add(new NameValueCollection() { { ExpandKeys.User, ExpandKeys.User } });
            }
            else
            {
                // Add anonmyous profile information
                UpdateAnonmyousProfile(account, portalId);
            }

            // Set the dates and save account
            account.CreateDte = DateTime.Now;
            account.UpdateDte = DateTime.Now;
            var savedAccount = _accountRepository.Save(account);

            // Add account profile
            var accountProfile = new AccountProfile { AccountID = savedAccount.AccountID, ProfileID = savedAccount.ProfileID };
            _accountProfileRepository.Save(accountProfile);

            //if it is guest user then unable to create default address entry.
            if (isGuestUser)
            {
                SaveAdminDefaultAddress(savedAccount.AccountID, model);
            }

            // Now get back to newly created account and expand addresses and profiles by default
            var newlyCreatedAccount = _accountRepository.GetByAccountID(savedAccount.AccountID);

            GetExpands(expand, newlyCreatedAccount);

            return AccountMap.ToModel(newlyCreatedAccount);
        }

        public AccountModel UpdateAccount(int accountId, AccountModel model)
        {
            if (accountId < 1)
            {
                throw new Exception("Account ID cannot be less than 1.");
            }

            if (model == null)
            {
                throw new Exception("Account model cannot be null.");
            }

            var account = _accountRepository.GetByAccountID(accountId);
            if (account != null)
            {
                // Check to update the user's email address
                if (model.User != null && account.Email != model.User.Email)
                {
                    // Update the email on the account
                    account.Email = model.User.Email;

                    // Update the email on the membership user
                    var user = Membership.GetUser(model.User.Username);
                    if (user != null)
                    {
                        user.Email = model.User.Email;
                        Membership.UpdateUser(user);
                    }
                }

                // Set the account ID
                model.AccountId = accountId;

                var accountToUpdate = AccountMap.ToEntity(model, account);

                var updated = _accountRepository.Update(accountToUpdate);
                if (updated)
                {
                    account = _accountRepository.GetByAccountID(accountId);
                    return AccountMap.ToModel(account);
                }
            }

            return null;
        }

        public bool DeleteAccount(int accountId)
        {
            if (accountId < 1)
            {
                throw new Exception("Account ID cannot be less than 1.");
            }

            var account = _accountRepository.GetByAccountID(accountId);
            if (account != null)
            {
                AccountHelper AccountHelper = new AccountHelper();
                if (AccountHelper.DeleteAccountDetails(accountId))
                {
                    // Delete the membership user
                    if (!Equals(account.UserID, null))
                    {
                        var user = WebSecurity.GetUser((Guid)account.UserID);
                        WebSecurity.DeleteUser(user.UserName);
                    }
                    // Now delete the account itself
                    return _accountRepository.Delete(account);
                }
                else
                {
                    throw new Exception("Unable to delete this account. Please remove any child relationship with this account");
                }
            }

            return false;
        }

        public AccountModel Login(int portalId, AccountModel model, out string errorCode, NameValueCollection expand)
        {
            string logErrorMessae = string.Empty;
            errorCode = "0";

            if (Equals(model, null))
            {
                throw new Exception("Account model cannot be null.");
            }

            if (Equals(model.User, null))
            {
                throw new Exception("Account user model cannot be null.");
            }

            if (!WebSecurity.UserExists(model.User.Username))
            {
                logErrorMessae = string.Format("Account user {0} is not exists.", model.User.Username);
                ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.LoginFailed, portalId, model.User.Username, string.Empty, null, logErrorMessae, null,null,null);
                throw new Exception(logErrorMessae);
            }
            //Verify username & Password combination for the user.
            bool successful = WebSecurity.Login(model.User.Username, model.User.Password, persistCookie: false);
            ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.LoginSuccess, portalId, model.User.Username, string.Empty, null, logErrorMessae, null,null,null);

            //Gets the user details based on username.
            var membershipUser = Membership.GetUser(model.User.Username);
            if (!successful)
            {
                //Znode Version 7.2.2
                //MVC User Section, Error message for account gets locked - Start 
                //Set the Error messages in case of Account gets locked. And Also display warning messages before the final last two attempts.
                CheckForLockedAccount(membershipUser);
                //MVC User Section, Error message for account gets locked - End 
            }

            if (!Equals(membershipUser, null))
            {
                var helperAccess = new AccountHelper();
                //Gets the Details of Reset Password based on User Id
                DataSet dataset = helperAccess.GetMembershipDetailsByUserId(((Guid)membershipUser.ProviderUserKey));
                if (dataset != null && dataset.Tables.Count > 0 && dataset.Tables[0].Rows.Count > 0)
                {
                    //Check for the Password Token.
                    if (!string.IsNullOrEmpty(dataset.Tables[0].Rows[0]["PASSWORDVERIFICATIONTOKEN"].ToString()))
                    {
                        model.User.PasswordToken = dataset.Tables[0].Rows[0]["PASSWORDVERIFICATIONTOKEN"].ToString();
                        errorCode = "0";
                        return model;
                    }
                }

                var accountModel = GetAccountByUserId((Guid)membershipUser.ProviderUserKey, expand);

                // filter current portal related profiles only as to identify this account related to current store.
                var protalProfiles = _portalProfileRepository.GetByPortalID(portalId);
                accountModel.Profiles = new Collection<ProfileModel>(accountModel.Profiles.Where(x => protalProfiles.Any(y => y.ProfileID == x.ProfileId)).ToList());

                var isError = ZNodeUserAccountBase.CheckLastPasswordChangeDate((Guid)membershipUser.ProviderUserKey, out errorCode);

                if (portalId > 0)
                {
                    accountModel.PortalId = portalId;
                }
                if (errorCode.Equals("0"))
                {
                    logErrorMessae = "Login successful";
                    ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.LoginSuccess, portalId, model.User.Username, string.Empty, null, logErrorMessae, null,null,null);
                }
                return accountModel;
            }

            return null;
        }

        public AccountModel ChangePassword(int portalId, AccountModel model)
        {
            if (Equals(model, null))
            {
                throw new Exception("Account model cannot be null.");
            }
            if (Equals(model.User, null))
            {
                throw new Exception("Account user model cannot be null.");
            }
            var user = Membership.GetUser(model.User.Username);
            if (Equals(user, null))
            {
                throw new Exception("User " + model.User.Username + " not found.");
            }
            bool isResetPassword = (string.IsNullOrEmpty(model.User.PasswordToken)) ? false : true;

            if (!isResetPassword && !Membership.ValidateUser(model.User.Username, model.User.Password))
            {
                throw new Exception("Current Password Incorrect.");
            }
            if (isResetPassword && !WebSecurity.IsResetPasswordTokenValid(model.User.PasswordToken))
            {
                throw new Exception("The reset link you clicked has expired. Please request a new one.");
            }

            // Verify if the new password specified by the user is in the list of the last 4 passwords used
            var verified = (isResetPassword) ? true : ZNodeUserAccountBase.VerifyNewPassword((Guid)user.ProviderUserKey, model.User.NewPassword);
            if (verified)
            {
                // Update/Reset the password for this user
                var passwordChanged = (isResetPassword)
                    ? WebSecurity.ResetPassword(model.User.PasswordToken, model.User.NewPassword)
                    : user.ChangePassword(model.User.Password, model.User.NewPassword);
                if (passwordChanged)
                {
                    // Log password
                    ZNodeUserAccountBase.LogPassword((Guid)user.ProviderUserKey, model.User.NewPassword);
                    model.User.Password = model.User.NewPassword;
                    var errorCode = String.Empty;
                    return Login(portalId, model, out errorCode, new NameValueCollection());
                }
            }
            // Otherwise
            throw new Exception("Your password matches a previously used password. You should select a password that is different than the previous 4 passwords.");
        }

        public AccountModel ResetPassword(AccountModel model)
        {
            if (Equals(model, null))
            {
                throw new Exception("Account model cannot be null.");
            }

            if (Equals(model.User, null))
            {
                throw new Exception("Account user model cannot be null.");
            }

            var membershipUser = Membership.GetUser(model.User.Username);
            if (Equals(membershipUser, null))
            {
                throw new Exception("User " + model.User.Username + " not found.");
            }

            if (!WebSecurity.GetIsConfirmedStatusByUserId((Guid)membershipUser.ProviderUserKey))
            {
                throw new Exception("Your account has been locked. Please contact site administrator.");
            }


            // Do the reset
            //var newPassword = membershipUser.ResetPassword(model.User.PasswordAnswer);
            string passwordResetToken = WebSecurity.GeneratePasswordResetToken(model.User.Username);

            //TODO// delete existing passwords as user needs to force the reset password at next login
            var helperAccess = new AccountHelper();
            helperAccess.DeletePasswordLogByUserId(((Guid)membershipUser.ProviderUserKey).ToString());

            //Znode Version 8.0
            //Reset Password Link in Email - Start
            string passwordResetUrl = string.Format("{0}/Account/ResetPassword?passwordToken={1}&userName={2}", model.BaseUrl, WebUtility.UrlEncode(passwordResetToken), WebUtility.UrlEncode(model.User.Username));
            string passwordResetLink = string.Format("<a href=\"{0}\"> here</a>", passwordResetUrl);
            //Reset Password Link in Email - End
            // Send email to the user

            var messageText = string.Empty;
            messageText += "Username: " + model.User.Username + Environment.NewLine;
            messageText += "Password Reset Link: " + passwordResetUrl + Environment.NewLine;

            var defaultTemplatePath = HttpContext.Current.Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, "ResetPasswordLink_en.htm"));
            if (File.Exists(defaultTemplatePath))
            {
                var rw = new StreamReader(defaultTemplatePath);
                messageText = rw.ReadToEnd();

                var rx1 = new Regex("#UserName#", RegexOptions.IgnoreCase);
                messageText = rx1.Replace(messageText, model.User.Username);

                var rx2 = new Regex("#Link#", RegexOptions.IgnoreCase);
                messageText = rx2.Replace(messageText, passwordResetLink);

                var rx3 = new Regex("#Url#", RegexOptions.IgnoreCase);
                messageText = rx3.Replace(messageText, passwordResetUrl);
            }

            //PRFT Custom Code : Start
            string storeLogoPath = ZNodeConfigManager.SiteConfig.LogoPath.Trim().Replace("~/", "");

            Regex rx6 = new Regex("#StoreLogo#", RegexOptions.IgnoreCase);
            messageText = rx6.Replace(messageText, apiUrl + "/" + storeLogoPath.TrimStart('~'));

            var rx7 = new Regex("#MailingInfo#", RegexOptions.IgnoreCase);
            messageText = rx7.Replace(messageText, ZNodeConfigManager.SiteConfig.CustomerServiceEmail);

            var rx8 = new Regex("#SupportPhone#", RegexOptions.IgnoreCase);
            messageText = rx8.Replace(messageText, ZNodeConfigManager.SiteConfig.CustomerServicePhoneNumber);

            var rx9 = new Regex("#SupportEmail#", RegexOptions.IgnoreCase);
            messageText = rx9.Replace(messageText, ZNodeConfigManager.SiteConfig.SalesEmail);

            string logoLink = ConfigurationSettings.AppSettings["DemoWebsiteUrl"];
            var rx10 = new Regex("#LogoLink#", RegexOptions.IgnoreCase);
            messageText = rx10.Replace(messageText, logoLink);
            //PRFT Custom Code : End

            var senderEmail = ZNodeConfigManager.SiteConfig.CustomerServiceEmail;
            var subject = string.Format("{0}: Password Reset Confirmation", ZNodeConfigManager.SiteConfig.StoreName);
            try
            {
                ZNode.Libraries.Framework.Business.ZNodeEmail.SendEmail(model.User.Email, senderEmail, string.Empty, subject, messageText, true);
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.LoginCreateSuccess, "", "", null, ex.Message, null);
                model.IsEmailSentFailed = true;
            }
            return model;
        }

        private bool IsUsernameAvailable(int portalId, string userName)
        {
            return _userAccountBase.IsLoginNameAvailable(portalId, userName);
        }

        private Profile GetDefaultRegisteredProfile(int portalId)
        {
            var portal = _portalRepository.GetByPortalID(portalId);
            return _profileRepository.GetByProfileID(portal.DefaultRegisteredProfileID.GetValueOrDefault());
        }

        public Profile GetCustomerProfile(int accountId, int portalId)
        {
            if (accountId == 0)
            {
                return GetDefaultAnonymousProfile(portalId);
            }

            // Account profile
            var accountHelper = new AccountHelper();
            //Code commented for getting profileId of login user by accountId
            //var profileId = accountHelper.GetCustomerProfile(accountId, portalId);
            int profileId = accountHelper.GetCustomerProfileByAccountId(accountId);

            return _profileRepository.GetByProfileID(profileId);
        }

        private Profile GetDefaultAnonymousProfile(int portalId)
        {
            var portal = _portalRepository.GetByPortalID(portalId);
            return _profileRepository.GetByProfileID(portal.DefaultAnonymousProfileID.GetValueOrDefault());
        }

        private void UpdateRegisterdProfile(Account account, int portalId)
        {
            var profile = GetDefaultRegisteredProfile(portalId);
            if (profile != null)
            {
                account.ProfileID = profile.ProfileID;
            }
        }

        private void UpdateAnonmyousProfile(Account account, int portalId)
        {
            var profile = GetDefaultAnonymousProfile(portalId);
            if (profile != null)
            {
                account.ProfileID = profile.ProfileID;
            }
        }

        private void GetExpands(NameValueCollection expands, Account account)
        {
            if (expands.HasKeys())
            {
                ExpandAccountType(expands, account);
                ExpandAddresses(expands, account);
                ExpandOrders(expands, account);
                ExpandProfiles(expands, account);
                ExpandUser(expands, account);
                ExpandWishList(expands, account);
                ExpandGiftCardHistory(expands, account);
                ExpandReferralCommissionType(expands, account);
            }
        }

        private void ExpandAccountType(NameValueCollection expands, Account account)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.AccountType)))
            {
                if (account.AccountTypeID.HasValue)
                {
                    var accountType = _accountTypeRepository.GetByAccountTypeID(account.AccountTypeID.Value);
                    if (accountType != null)
                    {
                        account.AccountTypeIDSource = accountType;
                    }
                }
            }
        }

        private void ExpandAddresses(NameValueCollection expands, Account account)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Addresses)))
            {
                var addresses = _addressRepository.GetByAccountID(account.AccountID);
                foreach (var a in addresses)
                {
                    account.AddressCollection.Add(a);
                }
            }
        }

        private void ExpandOrders(NameValueCollection expands, Account account)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Orders)))
            {
                var orders = _orderRepository.GetByAccountID(account.AccountID);
                foreach (var o in orders)
                {
                    account.OrderCollectionGetByAccountID.Add(o);

                    ExpandOrderLineItems(expands, o);
                }
            }
        }

        private void ExpandOrderLineItems(NameValueCollection expands, Order order)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.OrderLineItems)))
            {
                var orderLineItems = _orderLineItemRepository.DeepLoadByOrderID(order.OrderID, true, DeepLoadType.IncludeChildren, typeof(OrderLineItem));
                foreach (var item in orderLineItems)
                {
                    // Check to get the order shipment for the item
                    if (item.OrderShipmentID.HasValue)
                    {
                        var orderShipment = _orderShipmentRepository.GetByOrderShipmentID(item.OrderShipmentID.Value);
                        if (orderShipment != null)
                        {
                            item.OrderShipmentIDSource = orderShipment;
                        }
                    }
                }

                order.OrderLineItemCollection = orderLineItems;
            }
        }

        private void ExpandProfiles(NameValueCollection expands, Account account)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Profiles)))
            {
                // get all account profiles, later we filter by portal if needed.
                var accountProfiles = _accountProfileRepository.GetByAccountID(account.AccountID);
                _accountProfileRepository.DeepLoad(accountProfiles, true, DeepLoadType.IncludeChildren, typeof(Profile));

                foreach (var a in accountProfiles)
                {
                    account.AccountProfileCollection.Add(a);
                }
            }
        }

        private void ExpandUser(NameValueCollection expands, Account account)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.User)))
            {
                MembershipUser user = null;

                // First try getting the user by their guid
                if (account.UserID.HasValue)
                {
                    user = WebSecurity.GetUser(account.UserID.Value);
                }
                account.User = user;
            }
        }

        private void ExpandWishList(NameValueCollection expands, Account account)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.WishList)))
            {
                var wishlist = _wishlistRepository.GetByAccountID(account.AccountID);

                foreach (var wish in wishlist)
                {
                    account.WishListCollection.Add(wish);
                }
            }
        }

        private void ExpandGiftCardHistory(NameValueCollection expands, Account account)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.GiftCardHistory)))
            {
                GiftCardAdmin giftCardAdmin = new GiftCardAdmin();
                TList<ZNode.Libraries.DataAccess.Entities.GiftCardHistory> giftCardHistoryList = giftCardAdmin.GetGiftCardHistoryByAccountID(account.AccountID);
                if (!Equals(giftCardHistoryList, null))
                {
                    giftCardHistoryList.Sort("OrderID DESC");
                    var giftCardIds = string.Join(",", giftCardHistoryList.Select(x => Convert.ToString(x.GiftCardID)).ToArray());
                    var query = new GiftCardQuery { Junction = String.Empty };
                    var sortBuilder = new GiftCardSortBuilder();

                    var list = giftCardIds.Split(',');
                    foreach (var productId in list.Where(productId => !String.IsNullOrEmpty(productId)))
                    {
                        query.BeginGroup("OR");
                        query.AppendEquals(GiftCardColumn.GiftCardId, productId);
                        query.EndGroup();
                    }

                    account.GiftCardCollection = _giftCardRepository.Find(query, sortBuilder);
                    foreach (var item in account.GiftCardCollection)
                    {
                        var giftCardHistoryCollection = giftCardHistoryList.FindAll(x => Equals(x.GiftCardID, item.GiftCardId)).ToArray();
                        if (!Equals(giftCardHistoryCollection, null) && giftCardHistoryCollection.Count() > 0)
                        {
                            item.GiftCardHistoryCollection.AddRange(giftCardHistoryCollection);
                        }
                    }
                }
            }
        }

        private void ExpandReferralCommissionType(NameValueCollection expands, Account account)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.ReferralCommissionType)))
            {
                if (account.ReferralCommissionTypeID.HasValue)
                {
                    var referralcommissiontypes = _referralCommissionTypeRepository.GetByReferralCommissionTypeID(account.ReferralCommissionTypeID.Value);
                    if (!Equals(referralcommissiontypes, null))
                    {
                        account.ReferralCommissionTypeIDSource = referralcommissiontypes;
                    }
                }
            }
        }

        private void SetFiltering(List<Tuple<string, string, string>> filters, AccountQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;

                if (filterKey == FilterKeys.AccountTypeId) SetQueryParameter(AccountColumn.AccountTypeID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.CompanyName) SetQueryParameter(AccountColumn.CompanyName, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.Email) SetQueryParameter(AccountColumn.Email, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.EmailOptIn) SetQueryParameter(AccountColumn.EmailOptIn, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.EnableCustomerPricing) SetQueryParameter(AccountColumn.EnableCustomerPricing, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.ExternalId) SetQueryParameter(AccountColumn.ExternalAccountNo, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.IsActive) SetQueryParameter(AccountColumn.ActiveInd, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.ParentAccountId) SetQueryParameter(AccountColumn.ParentAccountID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.ProfileId) SetQueryParameter(AccountColumn.ProfileID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.UserId) SetQueryParameter(AccountColumn.UserID, filterOperator, filterValue, query);                
            }
        }

        private void SetSorting(NameValueCollection sorts, AccountSortBuilder sortBuilder)
        {
            if (sorts.HasKeys())
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);

                    if (key == SortKeys.AccountId) SetSortDirection(AccountColumn.AccountID, value, sortBuilder);
                    if (key == SortKeys.CompanyName) SetSortDirection(AccountColumn.CompanyName, value, sortBuilder);
                    if (key == SortKeys.CreateDate) SetSortDirection(AccountColumn.CreateDte, value, sortBuilder);
                }
            }
        }

        private void SetQueryParameter(AccountColumn column, string filterOperator, string filterValue, AccountQuery query)
        {
            base.SetQueryParameter(column, filterOperator, filterValue, query);
        }

        private void SetSortDirection(AccountColumn column, string value, AccountSortBuilder sortBuilder)
        {
            base.SetSortDirection(column, value, sortBuilder);
        }

        #region Znode Version 7.2.2
        // check the Reset Password Link current status.
        public AccountModel CheckResetPasswordLinkStatus(AccountModel model, out string errorCode)
        {
            var membershipUser = Membership.GetUser(model.User.Username);
            errorCode = string.Empty;
            if (membershipUser != null)
            {
                errorCode = (WebSecurity.IsResetPasswordTokenValid(model.User.PasswordToken)) ? ErrorCodes.ResetPasswordContinue.ToString() : ErrorCodes.ResetPasswordLinkExpired.ToString();
                return new AccountModel();
            }
            errorCode = ErrorCodes.ResetPasswordNoRecord.ToString();
            return null;
        }

        //Method to check whether the user account gets locked or not & also sets the Warning message before final last two attempts.
        private void CheckForLockedAccount(MembershipUser membershipUser)
        {
            if (!Equals(membershipUser, null))
            {
                //Check whether user account is locked or not.
                if (!WebSecurity.GetIsConfirmedStatusByUserId((Guid)membershipUser.ProviderUserKey))
                {
                    ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.LoginFailed, string.Empty, string.Empty, null, "Login failed", null);
                    throw new ZnodeUnauthorizedException(ErrorCodes.AccountLocked, "Your account has been locked. Please contact site administrator.", HttpStatusCode.Unauthorized);
                }
                else
                {
                    //Gets current failed password atttemt count for setting the message.
                    AccountHelper accountHelper = new AccountHelper();
                    int inValidAttemtCount = accountHelper.GetUserFailedPasswordAttemptCount((Guid)membershipUser.ProviderUserKey);

                    //Gets Maximum failed password atttemt count from web.config
                    int maxInvalidPasswordAttemptCount = Convert.ToInt32(ConfigurationManager.AppSettings["MaxInvalidPasswordAttempts"]);
                    if (inValidAttemtCount > 0 && maxInvalidPasswordAttemptCount > 0)
                    {
                        if (maxInvalidPasswordAttemptCount <= inValidAttemtCount)
                        {
                            WebSecurity.EnableDisableUser((Guid)membershipUser.ProviderUserKey, false);
                            ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.LoginFailed, string.Empty, string.Empty, null, "Login failed", null);
                            throw new ZnodeUnauthorizedException(ErrorCodes.AccountLocked, "Your account has been locked due to maximum invalid login attempt. Please contact site administrator.", HttpStatusCode.Unauthorized);
                        }


                        //Set warning error at (MaxFailureAttemptCount - 2) & (MaxFailureAttemptCount - 1) attempt.
                        if (Equals((maxInvalidPasswordAttemptCount - inValidAttemtCount), 2))
                        {
                            ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.LoginFailed, string.Empty, string.Empty, null, "Login failed", null);
                            throw new ZnodeUnauthorizedException(ErrorCodes.AccountLocked, "You are left with TWO more attempts. If failed, your account will get locked.", HttpStatusCode.Unauthorized);
                        }
                        else if (Equals((maxInvalidPasswordAttemptCount - inValidAttemtCount), 1))
                        {
                            ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.LoginFailed, string.Empty, string.Empty, null, "Login failed", null);
                            throw new ZnodeUnauthorizedException(ErrorCodes.AccountLocked, "You are left with ONE more attempt. If failed, your account will get locked.", HttpStatusCode.Unauthorized);
                        }
                        else
                        {
                            ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.LoginFailed, string.Empty, string.Empty, null, "Invalid Username or Password", null);
                            throw new ZnodeUnauthorizedException(ErrorCodes.LoginFailed, "Invalid Username or Password", HttpStatusCode.Unauthorized);
                        }
                    }
                    else
                    {
                        ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.LoginFailed, string.Empty, string.Empty, null, "Invalid Username or Password", null);
                        throw new ZnodeUnauthorizedException(ErrorCodes.LoginFailed, "Invalid Username or Password", HttpStatusCode.Unauthorized);
                    }
                }
            }
            else
            {
                ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.LoginFailed, string.Empty, string.Empty, null, "Invalid Username or Password", null);
                throw new ZnodeUnauthorizedException(ErrorCodes.LoginFailed, "Invalid Username or Password", HttpStatusCode.Unauthorized);
            }
        }

        /// <summary>
        /// This function will send acknowledge mail to new registerd Vendor.
        /// </summary>
        /// <param name="userName">String userName</param>
        /// <param name="newPassword">string newPassword</param>
        /// <param name="email">string email</param>


        protected bool SendMailStoreAdministrator(string userName, string newPassword, string email)
        {
            var defaultTemplatePath = HttpContext.Current.Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, FranchiseAccountActivationHTMLTemplateName));
            if (File.Exists(defaultTemplatePath))
            {
                string portalName = ZNodeConfigManager.SiteConfig.StoreName;
                string smtpServer = ZNodeConfigManager.SiteConfig.SMTPServer;
                string senderEmail = ZNodeConfigManager.SiteConfig.CustomerServiceEmail;

                string subject = string.Format("{0} - New User Creation", portalName);

                StreamReader rw = new StreamReader(defaultTemplatePath);
                string messageText = rw.ReadToEnd();

                Regex userNames = new Regex("#UserName#", RegexOptions.IgnoreCase);
                messageText = userNames.Replace(messageText, userName);

                var vendorLoginUrl = ConfigurationManager.AppSettings["AdminWebsiteUrl"];

                if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["AdminWebsiteUrl"]))
                {
                    vendorLoginUrl = "http://" + ZNodeConfigManager.DomainConfig.DomainName;
                }

                Regex Url = new Regex("#Url#", RegexOptions.IgnoreCase);
                messageText = Url.Replace(messageText, vendorLoginUrl);

                Regex password = new Regex("#Password#", RegexOptions.IgnoreCase);
                messageText = password.Replace(messageText, newPassword);

                try
                {
                    ZNode.Libraries.Framework.Business.ZNodeEmail.SendEmail(email, senderEmail, string.Empty, subject, messageText, true);
                }
                catch (Exception ex)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.LoginCreateSuccess, userName, "", null, ex.Message, null);
                    return false;
                }
                return true;
            }
            return false;
        }

        /// <summary>
        /// Znode Version 7.2.2
        ///  This function will send acknowledge mail to new registerd user.
        ///  To inform users login name and password.
        /// </summary>
        /// <param name="loginName">string loginName</param>
        /// <param name="password">string password</param>
        /// <param name="email">string email</param>
        protected void SendMailUserAccount(string loginName, string password, string email)
        {
            string smtpServer = ZNodeConfigManager.SiteConfig.SMTPServer;
            string senderEmail = ZNodeConfigManager.SiteConfig.CustomerServiceEmail;
            string subject = "Welcome to " + ZNodeConfigManager.SiteConfig.StoreName;
            string customerservicephone = ZNodeConfigManager.SiteConfig.CustomerServicePhoneNumber;
            string currentCulture = string.Empty;
            string templatePath = string.Empty;
            string defaultTemplatePath = string.Empty;            

            // Template selection
            currentCulture = ZNodeCatalogManager.CultureInfo;

            defaultTemplatePath = HttpContext.Current.Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, "UserRegistrationEmailTemplate.html"));

            templatePath = (!string.IsNullOrEmpty(currentCulture))
                ? HttpContext.Current.Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "UserRegistrationEmailTemplate_" + currentCulture + ".html")
                : HttpContext.Current.Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, "UserRegistrationEmailTemplate.html"));

            templatePath = defaultTemplatePath;

            StreamReader rw = new StreamReader(templatePath);
            string messageText = rw.ReadToEnd();

            Regex rx1 = new Regex("#UserName#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(messageText, loginName);

            Regex rx2 = new Regex("#Password#", RegexOptions.IgnoreCase);
            messageText = rx2.Replace(messageText, password);

            Regex rx3 = new Regex("#UserID#", RegexOptions.IgnoreCase);
            messageText = rx3.Replace(messageText, loginName);

            Regex rx4 = new Regex("#CUSTOMERSERVICEPHONE#", RegexOptions.IgnoreCase);
            messageText = rx4.Replace(messageText, customerservicephone);

            Regex rx5 = new Regex("#CUSTOMERSERVICEMAIL#", RegexOptions.IgnoreCase);
            messageText = rx5.Replace(messageText, senderEmail.Replace(",", ", "));

            Regex RegularExpressionLogo = new Regex("#LOGO#", RegexOptions.IgnoreCase);
            messageText = RegularExpressionLogo.Replace(messageText, ZNodeConfigManager.SiteConfig.StoreName);

            //PRFT Custom Code : Start
            string storeLogoPath = ZNodeConfigManager.SiteConfig.LogoPath.Trim().Replace("~/", "");

            Regex rx6 = new Regex("#StoreLogo#", RegexOptions.IgnoreCase);
            messageText = rx6.Replace(messageText, apiUrl + "/" + storeLogoPath.TrimStart('~'));

            var rx7 = new Regex("#MailingInfo#", RegexOptions.IgnoreCase);
            messageText = rx7.Replace(messageText, ZNodeConfigManager.SiteConfig.CustomerServiceEmail);

            var rx8 = new Regex("#SupportPhone#", RegexOptions.IgnoreCase);
            messageText = rx8.Replace(messageText, ZNodeConfigManager.SiteConfig.CustomerServicePhoneNumber);

            var rx9 = new Regex("#SupportEmail#", RegexOptions.IgnoreCase);
            messageText = rx9.Replace(messageText, ZNodeConfigManager.SiteConfig.SalesEmail);

            string logoLink = ConfigurationSettings.AppSettings["DemoWebsiteUrl"];
            var rx10 = new Regex("#LogoLink#", RegexOptions.IgnoreCase);
            messageText = rx10.Replace(messageText, logoLink);
            //PRFT Custom Code : End

            try
            {
                ZNode.Libraries.Framework.Business.ZNodeEmail.SendEmail(email, senderEmail, string.Empty, subject, messageText, true);
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.LoginCreateSuccess, loginName, "", null, ex.Message, null);
            }
        }

        #endregion

        #region Znode Version 8.0
        /// <summary>
        /// Method check for User Role
        /// </summary>
        /// <param name="userName">string userName</param>
        /// <param name="roleName">string roleName</param>
        /// <returns>Returns true or false</returns>
        public AccountModel CheckUserRole(string userName, string roleName)
        {
            AccountModel model = new AccountModel();
            model.IsUserInRole = Roles.IsUserInRole(userName, roleName);
            return model;
        }

        /// <summary>
        /// To get Valid Address
        /// </summary>
        /// <param name="model">AccountModel model</param>
        /// <returns>returns valid AddressModel</returns>
        public AddressModel IsAddressValid(AddressModel model)
        {
            AccountAdminRepository _accountAdmin = new AccountAdminRepository();
            model.ValidAddress = _accountAdmin.IsAddressValid(AddressMap.ToEntity(model));
            return model;
        }

        /// <summary>
        /// Method resets the admin details for the first default login.
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Returns reset details.</returns>
        public AccountModel ResetAdminDetails(AccountModel model)
        {

            var membershipUser = Membership.GetUser(model.UserId);

            if (Equals(membershipUser, null))
            {
                throw new Exception("Account model cannot be null.");
            }
            if (Equals(model.ErrorCode, "1"))
            {
                var account = _accountRepository.GetByAccountID(model.AccountId);
                string strEmail = membershipUser.Email;
                if (Roles.IsUserInRole(membershipUser.UserName, "admin"))
                {
                    strEmail = model.User.Email.Trim();
                    account.Email = strEmail; // Set Email address
                }
                model.User.Email = strEmail;

                if (!WebSecurity.UserExists(model.User.Username))
                {
                    if (CheckPasswordEncryption(model.User.Password))
                    {
                        Guid oldUserId = (Guid)model.UserId;
                        Guid userId = Guid.Empty;
                        string password = string.Empty;
                        string confirmationToken = CreateNewUser(model, out userId, out password);
                        if (!Equals(confirmationToken, null))
                        {
                            WebSecurity.ConfirmAccount(confirmationToken);
                            string[] roles = Roles.GetRolesForUser(membershipUser.UserName);
                            if (roles.Length > 0)
                            {
                                // Associate the new user with the roles list
                                Roles.AddUsersToRoles(new string[] { model.User.Username }, roles);
                            }
                            ZNodeUserAccountBase.LogPassword(userId, model.User.Password.Trim());
                            account.UserID = userId;
                            model.UserId = userId;
                            var accountToUpdate = AccountMap.ToEntity(model, account);
                            var updated = _accountRepository.Update(accountToUpdate);

                            //Save the Profile Access for the selected user.
                            _profileHelper.UpdateProfileUserId(oldUserId, userId);
                            WebSecurity.DeleteUser(membershipUser.UserName);
                        }
                        else
                        {
                            throw new ZnodeException(ErrorCodes.MembershipError, "Could not create user.");
                        }
                    }
                    else
                    {
                        throw new ZnodeException(ErrorCodes.ZnodeEncryptionError, "An error occured while reading the Znode Certificate.");
                    }

                }
                else
                {
                    throw new ZnodeException(ErrorCodes.UserNameUnavailable, "Username " + model.User.Username + " is not available.");
                }
            }

            return new AccountModel();
        }

        /// <summary>
        /// This method will create the Admin account
        /// </summary>
        /// <param name="model">AccountModel model</param>
        /// <returns>return all the account details</returns>
        public AccountModel CreateAdminAccount(int portalId, AccountModel model)
        {
            try
            {
                ZNodeUserAccount znodeAccount = new ZNodeUserAccount();

                Guid adminGuid = new Guid();
                string newPassword = string.Empty;

                int profileId = GetCurrentProfileId(portalId);
                if (Equals(profileId, 0))
                {
                    throw new Exception("Could not create user account because the system was unable to find a Default Profile for the store. Please configure a default profile for Anonymous and registered users.");
                }

                if (WebSecurity.UserExists(model.UserName))
                {
                    throw new Exception("This login name already exists. Please choose a different name for your UserID.");
                }

                string confirmationToken = CreateNewUser(model, out adminGuid, out newPassword);

                if (!String.IsNullOrEmpty(confirmationToken))
                {
                    WebSecurity.ConfirmAccount(model.UserName, confirmationToken);
                    Roles.AddUserToRole(model.UserName, model.RoleName);
                    //Add Profile Store Access
                    _profileHelper.UpdateRolesAndPermissions(adminGuid, "0", null);

                    Account account = SaveAccountDetails(model, adminGuid, profileId);

                    bool isAdminAddressSaved = SaveAdminDefaultAddress(account.AccountID, model);
                    bool isAccountProfileSaved = SaveAccountProfileDetails(account.AccountID, profileId);

                    if (!SendMailStoreAdministrator(model.UserName, newPassword, account.Email))
                    {
                        AccountModel accountModel = AccountMap.ToModel(account);
                        accountModel.IsEmailSentFailed = true;
                        return accountModel;
                    }
                    return AccountMap.ToModel(account);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return new AccountModel();
        }

        /// <summary>
        /// This method will return the Profile Id on the basis of portal id.
        /// </summary>
        /// <param name="portalId">integer Portal Id</param>
        /// <returns>Returns the profile id</returns>
        private int GetCurrentProfileId(int portalId)
        {
            int profileID = 0;
            Profile profile = null;

            Portal currentPortal = _portalRepository.GetByPortalID(portalId);
            profile = _profileRepository.GetByProfileID(currentPortal.DefaultRegisteredProfileID.GetValueOrDefault());

            if (profile != null)
            {
                profileID = profile.ProfileID;
            }

            return profileID;
        }

        /// <summary>
        /// This method will return the Registered Profile Id on the basis of portal id.
        /// </summary>
        /// <param name="portalId">integer Portal Id</param>
        /// <returns>Returns the profile id</returns>
        private int GetRegisteredProfileId(int portalId)
        {
            int profileID = 0;
            Profile profile = null;

            Portal currentPortal = _portalRepository.GetByPortalID(portalId);
            profile = _profileRepository.GetByProfileID(currentPortal.DefaultRegisteredProfileID.GetValueOrDefault());

            if (!Equals(profile, null))
            {
                profileID = profile.ProfileID;
            }

            return profileID;
        }
        /// <summary>
        /// This method will save the Account details
        /// </summary>
        /// <param name="model">AccountModel model</param>
        /// <param name="guid">GUID</param>
        /// <param name="profileId">integer Profile id</param>
        /// <returns>Returns the updated account details</returns>
        private Account SaveAccountDetails(AccountModel model, Guid guid, int profileId)
        {
            model.UserId = guid;
            model.CreateDate = DateTime.Now;
            model.UpdateDate = DateTime.Now;
            model.ProfileId = profileId;
            return _accountRepository.Save(AccountMap.ToEntity(model));
        }

        /// <summary>
        /// This method will save the default address for the Admin account
        /// </summary>
        /// <param name="accountId">integer Account id</param>
        /// <returns>Returns true if the data saved in table</returns>
        private bool SaveAdminDefaultAddress(int accountId, AccountModel model)
        {
            AccountAdmin accountAdmin = new AccountAdmin();

            var adminAddess = accountAdmin.GetDefaultBillingAddress(accountId) ??
                               new ZNode.Libraries.DataAccess.Entities.Address
                               {
                                   IsDefaultBilling = true,
                                   IsDefaultShipping = true,
                                   AccountID = accountId
                               };
            //Set Defaults              

            adminAddess.FirstName = !Equals(model, null) ? !string.IsNullOrEmpty(model.FirstName) ? model.FirstName : String.Empty : String.Empty;
            adminAddess.LastName = !Equals(model, null) ? !string.IsNullOrEmpty(model.LastName) ? model.LastName : String.Empty : String.Empty;
            adminAddess.PhoneNumber = !Equals(model, null) ? !string.IsNullOrEmpty(model.PhoneNumber) ? model.PhoneNumber : String.Empty : String.Empty;
            adminAddess.Street = String.Empty;
            adminAddess.Street1 = String.Empty;
            adminAddess.City = String.Empty;
            adminAddess.CompanyName = !Equals(model, null) ? !string.IsNullOrEmpty(model.CompanyName) ? model.CompanyName : String.Empty : String.Empty;
            adminAddess.StateCode = String.Empty;
            adminAddess.PostalCode = String.Empty;
            //PRFT Custom Code : Start
            adminAddess.MiddleName= !Equals(model, null) ? !string.IsNullOrEmpty(model.MiddleName) ? model.MiddleName : String.Empty : String.Empty;
            //PRFT Custom Code : End

            //Default country code for United States
            adminAddess.CountryCode = "US";
            adminAddess.Name = "Default Address";

            return _addressRepository.Insert(adminAddess);
        }

        private bool SaveVendorDefaultAddress(int accountId, AccountModel model)
        {
            AccountAdmin accountAdmin = new AccountAdmin();

            var adminAddess = accountAdmin.GetDefaultBillingAddress(accountId) ??
                               new ZNode.Libraries.DataAccess.Entities.Address
                               {
                                   IsDefaultBilling = true,
                                   IsDefaultShipping = true,
                                   AccountID = accountId
                               };
            //Set Defaults              
            adminAddess.FirstName = model.FirstName;
            adminAddess.LastName = model.LastName;
            adminAddess.PhoneNumber = model.PhoneNumber;
            adminAddess.Street = model.AddressModel.StreetAddress1;
            adminAddess.Street1 = model.AddressModel.StreetAddress2;
            adminAddess.City = model.AddressModel.City;
            adminAddess.CompanyName = model.AddressModel.CompanyName;
            adminAddess.StateCode = model.AddressModel.StateCode;
            adminAddess.PostalCode = model.AddressModel.PostalCode;

            //Default country code for United States
            adminAddess.CountryCode = "US";
            adminAddess.Name = "Default Address";

            return _addressRepository.Insert(adminAddess);
        }

        /// <summary>
        /// This method will save the profile and account details in ZnodeAccountProfile table
        /// </summary>
        /// <param name="accountID">integer Account id</param>
        /// <param name="profileId">integer profile id</param>
        /// <returns>returns true if data get saved in table</returns>
        private bool SaveAccountProfileDetails(int accountID, int profileId)
        {
            var adminAccountProfile = new AccountProfile { AccountID = accountID, ProfileID = profileId };

            return _accountProfileRepository.Insert(adminAccountProfile);
        }

        /// <summary>
        /// Function Create new user.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="UserId"></param>
        /// <returns>Returns the confirmation token in case user created successfully.</returns>
        private static string CreateNewUser(AccountModel model, out Guid UserId, out string newPassword)
        {
            UserId = Guid.NewGuid();
            string actualUsername = Equals(model.User, null) ? model.UserName : model.User.Username;
            string LoweredUserName = Equals(model.User, null) ? model.UserName.ToLower() : model.User.Username.ToLower();
            string password = Equals(model.User, null) ? GenerateNewPassword() : (string.IsNullOrEmpty(model.User.Password)) ? GenerateNewPassword() : model.User.Password;
            string Email = Equals(model.User, null) ? model.Email : model.User.Email;
            string MobileAlias = string.Empty;
            bool IsAnonymous = false;
            DateTime LastActivityDate = DateTime.Now;
            newPassword = password;

            //Create the new user.
            return WebSecurity.CreateUserAndAccount(actualUsername, password, new
            {
                UserId,
                LoweredUserName,
                Email,
                MobileAlias,
                IsAnonymous,
                LastActivityDate,
            }, true);
        }

        /// <summary>
        /// This method will generate the new password for the admin which we need to send it vai email
        /// </summary>
        /// <returns>Returns the new password</returns>
        public static string GenerateNewPassword()
        {
            return GetUniqueKey(8);
        }

        /// <summary>
        /// This method will fetch the account details by role name.
        /// </summary>
        /// <param name="filters">list of filters</param>
        /// <param name="sorts">sort collection</param>
        /// <param name="page"></param>
        /// <param name="totalRowCount"></param>
        /// <returns></returns>
        public AccountListModel GetAccountDetailsByRoleName(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page, out int totalRowCount)
        {
            List<Tuple<string, string, string>> filterList = new List<Tuple<string, string, string>>();
            var model = new AccountListModel();
            string innerWhereClause = string.Empty;

            string orderBy = StringHelper.GenerateDynamicOrderByClause(sorts);
            foreach (var item in filters)
            {
                if (Equals(item.Item1, "username"))
                {
                    filterList.Add(item);
                    innerWhereClause = StringHelper.GenerateDynamicWhereClause(filterList);
                    filters.Remove(item);
                    break;
                }
            }
            var pagingStart = 0;
            var pagingLength = 0;
            SetPaging(page, model, out pagingStart, out pagingLength);
            pagingStart = Convert.ToInt32(page.Get(PageKeys.Index));
            string whereClause = StringHelper.GenerateDynamicWhereClause(filters);
            StoredProcedureHelper storedProcedureHelper = new StoredProcedureHelper();
            //This sp will only be used for Store admin list
            DataSet resultDataSet = storedProcedureHelper.SP_DataDetailsSpConfig(whereClause, null, StoredProcedureKeys.ZNodeAdminAccounts, orderBy, pagingStart.ToString(), pagingLength.ToString(), out totalRowCount, null, innerWhereClause);
            if (!Equals(resultDataSet, null) && resultDataSet.Tables.Count > 0 && !Equals(resultDataSet.Tables[0], null) && resultDataSet.Tables[0].Rows.Count > 0)
            {
                model = AccountMap.ToAccountModelList(resultDataSet);
            }
            model.TotalResults = totalRowCount;
            model.PageSize = pagingLength;
            model.PageIndex = pagingStart;

            // Check to set page size equal to the total number of results
            if (Equals(pagingLength, int.MaxValue))
            {
                model.PageSize = model.TotalResults;
            }

            return model;
        }

        /// <summary>
        /// This function is used to check Role Exist For Profile
        /// </summary>
        /// <param name="porfileId">int porfileId - default porfileId</param>
        /// <param name="roleName">string roleName</param>
        /// <returns>returns true/false</returns>
        public bool IsRoleExistForProfile(int porfileId, string roleName)
        {
            if (porfileId > 0 && !string.IsNullOrEmpty(roleName))
            {
                AccountHelper accountHelper = new AccountHelper();
                return accountHelper.IsRoleExistForProfile(porfileId, roleName);
            }
            return false;
        }

        /// <summary>
        /// This method will disable the admin account
        /// </summary>
        /// <param name="accountId">int account id</param>
        /// <returns></returns>
        public bool EnableDisableAccount(int accountId)
        {
            if (accountId < 1)
            {
                throw new Exception("Account ID cannot be less than 1.");
            }

            var account = _accountRepository.GetByAccountID(accountId);
            if (!Equals(account, null))
            {
                var user = WebSecurity.GetUser((Guid)account.UserID);
                if (!Equals(user, null))
                {
                    bool isConfirmed = WebSecurity.GetIsConfirmedStatusByUserId((Guid)account.UserID);
                    if (isConfirmed)
                    {
                        return WebSecurity.EnableDisableUser((Guid)user.ProviderUserKey, false);
                    }
                    else
                    {
                        if (WebSecurity.EnableDisableUser((Guid)user.ProviderUserKey, true))
                        {
                            SendAccountActivationEmail(user.UserName, user.Email);
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
                return false;
            }

            return false;
        }

        /// <summary>
        /// To send email with attachment
        /// </summary>
        /// <param name="email"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        /// <param name="attactment"></param>
        public SendMailModel SendEmail(SendMailModel model)
        {
            ZNodeEncryption encrypt = new ZNodeEncryption();
            string portalName = ZNodeConfigManager.SiteConfig.StoreName;
            string smtpServer = ZNodeConfigManager.SiteConfig.SMTPServer;
            string senderEmail = encrypt.DecryptData(ZNodeConfigManager.SiteConfig.SMTPUserName);

            try
            {
                if (Equals(model.Attachment, null))
                {
                    ZNode.Libraries.Framework.Business.ZNodeEmail.SendEmail(model.EmailId, senderEmail, string.Empty, model.Subject, model.Body, true);
                }
                else
                {
                    ZNode.Libraries.Framework.Business.ZNodeEmail.SendEmail(model.EmailId, senderEmail, string.Empty, model.Subject, model.Body, model.Attachment, true);
                }
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.LoginCreateSuccess, model.EmailId, "", null, ex.Message, null);
            }
            return model;
        }

        /// <summary>
        /// Get list of Role Permissions based on userName.
        /// </summary>
        /// <param name="userName">UserName for the user</param>
        /// <returns>Return the Role Permission list in RolePermissionListModel format</returns>
        public RolePermissionListModel GetRolePermission(string userName)
        {
            RolePermissionListModel model = new RolePermissionListModel();
            if (!string.IsNullOrEmpty(userName))
            {
                AccountHelper helperAccess = new AccountHelper();
                DataSet dataset = helperAccess.GetRolePermissionByUserName(userName);
                model.UserPermissionList = dataset.Tables[0].ToList<RolePermissionModel>().ToCollection();
            }
            return model;
        }

        /// <summary>
        /// Get list of Role Menu based on userName.
        /// </summary>
        /// <param name="userName">UserName for the user</param>
        /// <returns>Return the Role Menu list in RoleMenuListModel format</returns>
        public RoleMenuListModel GetRoleMenuList(AccountModel accountModel)
        {
            RoleMenuListModel model = new RoleMenuListModel();
            if (!Equals(accountModel, null) && !string.IsNullOrEmpty(accountModel.UserName))
            {
                AccountHelper helperAccess = new AccountHelper();
                DataSet dataset = helperAccess.GetRoleMenuListByUserName(accountModel.UserName);
                model.RoleMenuList = dataset.Tables[0].ToList<RoleMenuModel>().ToCollection();
            }
            return model;
        }


        #region Create Vendor Account
        /// <summary>
        /// This method will create the Vendor account
        /// </summary>
        /// <param name="model">AccountModel model</param>
        /// <returns>return all the account details</returns>
        public AccountModel CreateVendorAccount(int portalId, AccountModel model)
        {
            try
            {
                Guid adminGuid = new Guid();
                string newPassword = string.Empty;

                int profileId = GetCurrentProfileId(portalId);

                if (Equals(profileId, 0))
                {
                    throw new Exception("Could not create user account because the system was unable to find a Default Profile for the store. Please configure a default profile for Anonymous and registered users.");
                }

                if (WebSecurity.UserExists(model.UserName))
                {
                    throw new Exception("This login name already exists. Please choose a different name for your UserID.");
                }

                if (!Equals(model.ExternalAccountNum, null) && !Equals(model.ExternalAccountNum, string.Empty))
                {
                    //TODO: Database can have empty string for ExternalAccountNo, so adding length check, then proceeding
                    TList<Account> accounts = _accountRepository.Find(String.Format("ExternalAccountNo='{0}'", model.ExternalAccountNum));
                    if (!Equals(accounts, null) && accounts.Count > 0)
                    {
                        if (!Equals(accounts[0].UserID, null))
                        {
                            throw new Exception("This External Number \"" + model.ExternalAccountNum + "\" already has an account associated with it. Please contact our support staff at test@znode.com for assistance");
                        }
                    }

                }
                string confirmationToken = CreateNewUser(model, out adminGuid, out newPassword);

                if (!String.IsNullOrEmpty(confirmationToken))
                {


                    if (Equals(model.UserType, null) && !Equals(model.UserType, Convert.ToString(UserTypes.MallAdmin)))
                    {
                        WebSecurity.ConfirmAccount(model.UserName, confirmationToken);
                    }
                    Roles.AddUserToRole(model.UserName, model.RoleName);

                    //Add Profile Store Access
                    _profileHelper.UpdateRolesAndPermissions(adminGuid, portalId.ToString(), null);

                    Account account = SaveAccountDetails(model, adminGuid, profileId);
                    SupplierModel supplierModel = new SupplierModel();
                    SupplierService supplierService = new SupplierService();

                    SaveVendorDefaultAddress(account.AccountID, model);
                    SaveAccountProfileDetails(account.AccountID, profileId);

                    supplierModel = this.SetSupplierModel(model);

                    supplierService.CreateSupplier(supplierModel);

                    if (Equals(model.UserType, null) && !Equals(model.UserType, Convert.ToString(UserTypes.MallAdmin)))
                    {
                        if (!SendMailVendorAccount(model.UserName, newPassword, account.Email))
                        {
                            AccountModel accountModel = AccountMap.ToModel(account);
                            accountModel.IsEmailSentFailed = true;
                            return accountModel;
                        }
                    }
                    return AccountMap.ToModel(account);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return new AccountModel();
        }

        public AccountModel UpdateVendorAccount(int accountId, AccountModel model)
        {
            if (accountId < 1)
            {
                throw new Exception("Account ID cannot be less than 1.");
            }

            if (Equals(model, null))
            {
                throw new Exception("Account model cannot be null.");
            }

            Account account = _accountRepository.GetByAccountID(accountId);
            if (!Equals(account, null))
            {
                // Check to update the user's email address
                if (!Equals(model.User, null) && !Equals(account.Email, model.User.Email))
                {
                    // Update the email on the account
                    account.Email = model.User.Email;

                    // Update the email on the membership user
                    MembershipUser user = Membership.GetUser(model.User.Username);
                    if (!Equals(user, null))
                    {
                        user.Email = model.User.Email;
                        Membership.UpdateUser(user);
                    }
                }

                // Set the account ID
                model.AccountId = accountId;

                Account accountToUpdate = AccountMap.ToEntity(model, account);
                UpdateVendorAccountAddress(account.AccountID, model);
                bool updated = _accountRepository.Update(accountToUpdate);

                if (updated)
                {
                    account = _accountRepository.GetByAccountID(accountId);
                    return AccountMap.ToModel(account);
                }
            }

            return null;
        }

        // new method
        private bool UpdateVendorAccountAddress(int accountId, AccountModel model)
        {
            AccountAdmin accountAdmin = new AccountAdmin();

            Address adminAddess = accountAdmin.GetDefaultBillingAddress(accountId) ??
                               new ZNode.Libraries.DataAccess.Entities.Address
                               {
                                   IsDefaultBilling = true,
                                   IsDefaultShipping = true,
                                   AccountID = accountId
                               };
            //Set Defaults              
            adminAddess.FirstName = model.FirstName;
            adminAddess.LastName = model.LastName;
            adminAddess.PhoneNumber = model.PhoneNumber;
            adminAddess.Street = model.AddressModel.StreetAddress1;
            adminAddess.Street1 = model.AddressModel.StreetAddress2;
            adminAddess.City = model.AddressModel.City;
            adminAddess.CompanyName = model.AddressModel.CompanyName;
            adminAddess.StateCode = model.AddressModel.StateCode;
            adminAddess.PostalCode = model.AddressModel.PostalCode;

            //Default country code for United States
            adminAddess.CountryCode = model.AddressModel.CountryCode;
            adminAddess.Name = "Default Address";

            return _addressRepository.Update(adminAddess);
        }

        private bool SendMailVendorAccount(string userName, string newPassword, string email)
        {
            var defaultTemplatePath = HttpContext.Current.Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, FranchiseAccountActivationHTMLTemplateName));
            if (File.Exists(defaultTemplatePath))
            {
                string portalName = ZNodeConfigManager.SiteConfig.StoreName;
                string smtpServer = ZNodeConfigManager.SiteConfig.SMTPServer;
                string senderEmail = ZNodeConfigManager.SiteConfig.CustomerServiceEmail;

                string subject = string.Format("{0} - New User Creation", portalName);


                StreamReader rw = new StreamReader(defaultTemplatePath);
                string messageText = rw.ReadToEnd();

                Regex userNames = new Regex("#UserName#", RegexOptions.IgnoreCase);
                messageText = userNames.Replace(messageText, userName);

                var vendorLoginUrl = ConfigurationManager.AppSettings["AdminWebsiteUrl"] + "/malladmin";

                if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["AdminWebsiteUrl"]))
                {
                    vendorLoginUrl = "http://" + ZNodeConfigManager.DomainConfig.DomainName + "/malladmin";
                }

                Regex Url = new Regex("#Url#", RegexOptions.IgnoreCase);
                messageText = Url.Replace(messageText, vendorLoginUrl);

                Regex password = new Regex("#Password#", RegexOptions.IgnoreCase);
                messageText = password.Replace(messageText, newPassword);

                try
                {
                    ZNode.Libraries.Framework.Business.ZNodeEmail.SendEmail(email, senderEmail, string.Empty, subject, messageText, true);
                }
                catch (Exception ex)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.LoginCreateSuccess, userName, "", null, ex.Message, null);
                    return false;
                }
                return true;
            }
            return false;
        }
        #endregion

        #region Create Franchise Account
        public AccountModel CreateFranchiseAccount(int portalId, AccountModel model)
        {
            try
            {
                Guid adminGuid = new Guid();
                string newPassword = string.Empty;

                int profileId = GetCurrentProfileId(model.PortalId.Value);

                if (Equals(profileId, 0))
                {
                    throw new Exception("Could not create user account because the system was unable to find a Default Profile for the store. Please configure a default profile for Anonymous and registered users.");
                }

                if (WebSecurity.UserExists(model.UserName))
                {
                    throw new Exception("This login name already exists. Please choose a different name for your UserID.");
                }

                TList<Account> accounts = _accountRepository.Find(String.Format("ExternalAccountNo='{0}'", model.ExternalAccountNum));
                if (!Equals(accounts, null) && accounts.Count > 0)
                {
                    if (!Equals(accounts[0].UserID, null))
                    {
                        throw new Exception("This Franchise Number \"" + model.ExternalAccountNum + "\" already has an account associated with it. Please contact our support staff at " + ZNodeConfigManager.SiteConfig.CustomerServiceEmail + "for assistance");
                    }
                }

                string confirmationToken = CreateNewUser(model, out adminGuid, out newPassword);

                if (!String.IsNullOrEmpty(confirmationToken))
                {
                    if (Equals(model.UserType, null) && !Equals(model.UserType, Convert.ToString(UserTypes.FranchiseAdmin)))
                    {
                        WebSecurity.ConfirmAccount(model.UserName, confirmationToken);
                    }
                    Roles.AddUserToRole(model.UserName, model.RoleName);

                    //Add Profile Store Access
                    _profileHelper.UpdateRolesAndPermissions(adminGuid, model.PortalId.ToString(), null);

                    Account account = SaveAccountDetails(model, adminGuid, profileId);

                    SaveVendorDefaultAddress(account.AccountID, model);
                    SaveAccountProfileDetails(account.AccountID, profileId);

                    if (Equals(model.UserType, null) && !Equals(model.UserType, Convert.ToString(UserTypes.FranchiseAdmin)))
                    {
                        if (!(SendFranchiseAccountActivationEmail(model.UserName, newPassword, account.Email)))
                        {
                            AccountModel accountModel = AccountMap.ToModel(account);
                            accountModel.IsEmailSentFailed = true;
                            return accountModel;
                        }
                    }
                    return AccountMap.ToModel(account);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return new AccountModel();
        }

        private bool SendFranchiseAccountActivationEmail(string userName, string newPassword, string email)
        {
            var defaultTemplatePath = HttpContext.Current.Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, FranchiseAccountActivationHTMLTemplateName));
            if (File.Exists(defaultTemplatePath))
            {
                string portalName = ZNodeConfigManager.SiteConfig.StoreName;
                string smtpServer = ZNodeConfigManager.SiteConfig.SMTPServer;
                string senderEmail = ZNodeConfigManager.SiteConfig.CustomerServiceEmail;

                string subject = string.Format("{0} - New User Creation", portalName);

                StreamReader rw = new StreamReader(defaultTemplatePath);
                string messageText = rw.ReadToEnd();

                Regex userNames = new Regex("#UserName#", RegexOptions.IgnoreCase);
                messageText = userNames.Replace(messageText, userName);

                var franchiseLoginUrl = ConfigurationManager.AppSettings["AdminWebsiteUrl"] + "/franchiseadmin";

                if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["AdminWebsiteUrl"]))
                {
                    franchiseLoginUrl = "http://" + ZNodeConfigManager.DomainConfig.DomainName + "/franchiseadmin";
                }

                Regex Url = new Regex("#Url#", RegexOptions.IgnoreCase);
                messageText = Url.Replace(messageText, franchiseLoginUrl);

                Regex password = new Regex("#Password#", RegexOptions.IgnoreCase);
                messageText = password.Replace(messageText, newPassword);

                try
                {
                    ZNode.Libraries.Framework.Business.ZNodeEmail.SendEmail(email, senderEmail, string.Empty, subject, messageText, true);
                }
                catch (Exception ex)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.LoginCreateSuccess, userName, "", null, ex.Message, null);
                    return false;
                }
                return true;
            }
            return false;
        }


        #endregion

        #region Customer Account
        public AccountModel UpdateCustomerAccount(int accountId, AccountModel model, out string errorCode)
        {
            int portalId = ZNodeConfigManager.SiteConfig.PortalID;
            errorCode = "0";
            if (accountId < 1)
            {
                throw new Exception("Account ID cannot be less than 1.");
            }
            if (Equals(model, null))
            {
                throw new Exception("Account model cannot be null.");
            }
            Account account = _accountRepository.GetByAccountID(accountId);
            if (!Equals(account, null))
            {
                if (account.UserID.HasValue)
                {
                    CheckCustomerExists(account);
                }
                else
                {
                    CheckUserName(model);
                    Guid userId = CreateNewCustomer(model);
                    LogCustomerPassword(model, userId);
                    model.UserId = userId;
                }
                // Check to update the user's email address
                UpdateUserDetails(model, account);

                //PRFT Custom Code : Start
                if (!(string.IsNullOrEmpty(model.Custom2)))
                {
                    model.ProfileId = model.Custom2.Equals("1") ? SuperUserProfileId : GetRegisteredProfileId(portalId);
                }
                //PRFT Custom Code : End

                // Set the account ID
                model.AccountId = accountId;
                Account accountToUpdate = AccountMap.ToEntity(model, account);
                bool updated = _accountRepository.Update(accountToUpdate);
                if (updated)
                {
                    //PRFT Custom Code : Start
                    AccountHelper accHelperObj = new AccountHelper();
                    accHelperObj.UpdateAcountProfileTable(model.AccountId, model.ProfileId);
                    //PRFT Custom Code : End

                    account = _accountRepository.GetByAccountID(accountId);
                    MembershipUser user = null;
                    // First try getting the user by their guid
                    if (account.UserID.HasValue)
                    {
                        user = WebSecurity.GetUser(account.UserID.Value);
                        account.User = user;
                    }

                    //PRFT Custom Code: Start -- Set IsConfirm field for webpages_Membership table for Enable / Disable user
                    var Webuser = WebSecurity.GetUser((Guid)account.UserID);
                    if(!(string.IsNullOrEmpty(model.IsActive.ToString())))
                    {
                        bool isConfirmed = model.IsActive.Value;
                        if (!Equals(Webuser, null))
                        {
                            bool isCurrentStatus = WebSecurity.GetIsConfirmedStatusByUserId((Guid)account.UserID);
                            if (!isConfirmed)
                            {
                                WebSecurity.EnableDisableUser((Guid)Webuser.ProviderUserKey, false);
                            }
                            else
                            {
                                if (WebSecurity.EnableDisableUser((Guid)Webuser.ProviderUserKey, true))
                                {
                                    if (isCurrentStatus != model.IsActive.Value)
                                    {
                                        SendAccountActivationEmail(Webuser.UserName, Webuser.Email);
                                    }
                                }
                            }
                        }
                    }
                   
                    //PRFT Custom Code: End

                    return AccountMap.ToModel(account);
                }
            }
            return null;
        }

        private static void CheckCustomerExists(Account account)
        {
            var user = Membership.GetUser(account.UserID);
            if (Equals(user, null))
            {
                throw new ZnodeException(ErrorCodes.CustomerAccountError, "Unable to validate the current membership. Please try again, or contact your Administrator.");
            }
        }

        private static void LogCustomerPassword(AccountModel model, Guid userId)
        {
            try
            {
                //Remove password log
                new AccountHelper().DeletePasswordLogByUserId(userId.ToString());
                //Add password log
                ZNodeUserAccount.LogPassword(userId, model.User.Password);
            }
            catch (Exception ex)
            {
                throw new ZnodeException(ErrorCodes.CustomerAccountError, string.Format("Unable to log the new password for the user. Please try again, or contact your Administrator. Error: {0}", ex.Message));
            }
        }

        private static void UpdateUserDetails(AccountModel model, Account account)
        {
            if (!Equals(model.User, null) && !Equals(account.Email, model.User.Email))
            {
                // Update the email on the account
                account.Email = model.User.Email;

                // Update the email on the membership user
                var user = Membership.GetUser(model.User.Username);
                if (!Equals(user, null))
                {
                    user.Email = model.User.Email;
                    Membership.UpdateUser(user);
                }
            }
        }

        private static Guid CreateNewCustomer(AccountModel model)
        {
            Guid userId = Guid.Empty;
            string newPassword = string.Empty;
            model.User.Password = GenerateNewPassword();
            string confirmationToken = CreateNewUser(model, out userId, out newPassword);

            if (!String.IsNullOrEmpty(confirmationToken))
            {
                WebSecurity.ConfirmAccount(model.UserName, confirmationToken);
            }
            else
            {
                throw new ZnodeException(ErrorCodes.CustomerAccountError, "Unable to successfully create a Membership for the current user. Please try again, or contact your Administrator.");
            }
            return userId;
        }

        private static void CheckUserName(AccountModel model)
        {
            if (string.IsNullOrEmpty(model.User.Username))
            {
                throw new ZnodeException(ErrorCodes.CustomerAccountError, "User Name is required.");
            }

            // Check if loginName already exists
            if (WebSecurity.UserExists(model.User.Username.Trim()))
            {
                throw new ZnodeException(ErrorCodes.CustomerAccountError, "Username " + model.User.Username + " is not available.");
            }
        }

        public AccountModel CreateCustomerAccount(int portalId, AccountModel model, out string errorCode)
        {
            errorCode = "0";
            try
            {

                Guid adminGuid = new Guid();
                string newPassword = string.Empty;
                int profileId = 0;

                if (!Equals(model.ProfileId, null))
                {
                    profileId = model.ProfileId.Value;
                    portalId = model.PortalId.Value;
                }
                //PRFT Custom Code : Start
                else if (model.Custom2.Equals("1"))
                {
                    profileId = SuperUserProfileId;
                }
                //PRFT Custom Code : End
                else
                {
                    profileId = GetRegisteredProfileId(portalId);

                }

                if (WebSecurity.UserExists(model.User.Username))
                {
                    throw new Exception("This login name already exists. Please choose a different name for your UserID.");
                }

                string confirmationToken = CreateNewUser(model, out adminGuid, out newPassword);

                if (!String.IsNullOrEmpty(confirmationToken))
                {
                    WebSecurity.ConfirmAccount(model.UserName, confirmationToken);

                    if (Equals(profileId, 0))
                    {
                        throw new Exception("Could not create user account because the system was unable to find a Default Profile for the store. Please configure a default profile for Anonymous and registered users.");
                    }

                    //Add Profile Store Access
                    _profileHelper.UpdateRolesAndPermissions(adminGuid, "0", null);

                    Account account = SaveAccountDetails(model, adminGuid, profileId);
                    ZNodeUserAccountBase.LogPassword((Guid)account.UserID, newPassword);

                    SaveAdminDefaultAddress(account.AccountID, model);
                    SaveAccountProfileDetails(account.AccountID, profileId);

                    Account accountToUpdate = AccountMap.ToEntity(model, account);

                    SendMailUserAccount(model.User.Username, newPassword, account.Email);

                    return AccountMap.ToModel(account);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return new AccountModel();
        }

        #endregion

        #region Customer Account Payment

        #region Public Methods
        public AccountPaymentListModel GetAccountPaymentList(int accountId, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new AccountPaymentListModel();
            var accountPayments = new TList<AccountPayment>();
            var tempList = new TList<AccountPayment>();

            var query = new AccountPaymentQuery();
            var sortBuilder = new AccountPaymentSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetPaging(page, model, out pagingStart, out pagingLength);
            SetSorting(sorts, sortBuilder);
            // Get the initial set
            var totalResults = 0;
            tempList = _accountPaymentRepository.GetByAccountID(accountId, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (Equals(pagingLength, int.MaxValue))
            {
                model.PageSize = model.TotalResults;
            }

            // Now go through the list and get any expands
            foreach (var accountPayment in tempList)
            {
                accountPayments.Add(accountPayment);
            }

            // Map each item and add to the list
            foreach (var accountPayment in accountPayments)
            {
                model.AccountPayments.Add(AccountPaymentMap.ToModel(accountPayment));
            }
            return model;
        }

        public AccountPaymentModel CreateAccountPayment(AccountPaymentModel model)
        {
            if (Equals(model, null))
            {
                throw new Exception("AccountPayment model cannot be null.");
            }

            var entity = AccountPaymentMap.ToEntity(model);

            var accountPayment = _accountPaymentRepository.Save(entity);
            if (!Equals(accountPayment, null))
            {
                return AccountPaymentMap.ToModel(accountPayment);
            }

            return null;
        }
        #endregion

        #region Private Methods
        private void SetSorting(NameValueCollection sorts, AccountPaymentSortBuilder sortBuilder)
        {
            if (sorts.HasKeys())
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);
                    if (value.Equals(SortKeys.Description)) { SetSortDirection(AccountPaymentColumn.Description, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (value.Equals(SortKeys.PaymentDate)) { SetSortDirection(AccountPaymentColumn.ReceivedDate, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (value.Equals(SortKeys.Amount)) { SetSortDirection(AccountPaymentColumn.Amount, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                }
            }
            else
            {
                SetSortDirection(AccountPaymentColumn.AccountPaymentID, sorts[StoredProcedureKeys.sortDirKey], sortBuilder);
            }
        }
        #endregion
        #endregion


        /// <summary>
        /// Sign Up the User for NewsLetters
        /// </summary>
        /// <param name="model">NewsLetterSignUpModel</param>
        /// <returns>returns true/false</returns>
        public bool SignUpForNewsLetter(NewsLetterSignUpModel model)
        {
            AccountAdmin _UserAccountAdmin = new AccountAdmin();
            Account _UserAccount = new Account();
            Address address = new Address();
            bool status = false;
            // Check if email already exists in DB
            if (!IsUserAlreadyExist(model.Email.Trim()))
            {
                _UserAccount.EmailOptIn = true;
                _UserAccount.Email = model.Email;
                _UserAccount.Source = string.Empty;

                // Set to empty values to other billing address properties            
                address.Name = "Default Address";
                address.MiddleName = string.Empty;

                // Set to empty values to other billing address properties
                address.PhoneNumber = string.Empty;
                address.CompanyName = string.Empty;
                address.Street = string.Empty;
                address.Street1 = string.Empty;
                address.City = string.Empty;
                address.StateCode = string.Empty;
                address.PostalCode = string.Empty;
                address.CountryCode = string.Empty;

                _UserAccount.ProfileID = ZNodeConfigManager.SiteConfig.DefaultAnonymousProfileID;

                // Pre-set properties
                _UserAccount.UserID = null;
                _UserAccount.ActiveInd = true;
                _UserAccount.ParentAccountID = null;
                _UserAccount.AccountTypeID = 0;
                _UserAccount.CreateDte = System.DateTime.Now;
                _UserAccount.UpdateDte = System.DateTime.Now;
                _UserAccount.CreateUser = null;

                // Add New Contact
                status = _UserAccountAdmin.Add(_UserAccount);

                if (status)
                {
                    // Add New Address
                    ZNode.Libraries.DataAccess.Service.AddressService addressService = new ZNode.Libraries.DataAccess.Service.AddressService();
                    address.AccountID = _UserAccount.AccountID;
                    address.IsDefaultBilling = true;
                    address.IsDefaultShipping = true;
                    addressService.Insert(address);

                    // Add and entry to AccountProfile.
                    AccountProfileService accountProfileService = new AccountProfileService();
                    AccountProfile accountProfile = new AccountProfile();
                    accountProfile.ProfileID = _UserAccount.ProfileID;
                    accountProfile.AccountID = _UserAccount.AccountID;
                    accountProfileService.Insert(accountProfile);
                }
            }
            else
            {
                throw new ZnodeException(ErrorCodes.UserNameUnavailable, "Email address already exist.");
            }
            return status;
        }

        /// <summary>
        /// Sign Up the User for NewsLetters
        /// </summary>
        /// <param name="model">NewsLetterSignUpModel</param>
        /// <returns>returns true/false</returns>
        public AccountModel SocialUserLogin(int portalId, SocialLoginModel model, NameValueCollection expand)
        {
            if (!string.IsNullOrEmpty(model.Provider) && !string.IsNullOrEmpty(model.ProviderUserId) && !Equals(model.CreatePersistentCookie, null))
            {
                string logErrorMessae = string.Empty;
                    if (OAuthWebSecurity.Login(model.Provider, model.ProviderUserId, model.CreatePersistentCookie))
                    {
                        string userName = OAuthWebSecurity.GetUserName(model.Provider, model.ProviderUserId);
                        if (!string.IsNullOrEmpty(userName))
                        {
                            MembershipUser membershipUser = Membership.GetUser(userName);
                            if (!Equals(membershipUser, null))
                            {
                            if (!WebSecurity.GetIsConfirmedStatusByUserId((Guid)membershipUser.ProviderUserKey))
                            {
                                ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.LoginFailed, string.Empty, string.Empty, null, "Social Login failed", null);
                                throw new ZnodeUnauthorizedException(ErrorCodes.AccountLocked, "Your account has been locked. Please contact site administrator.", HttpStatusCode.Unauthorized);
                            }
                                var accountModel = GetAccountByUserId((Guid)membershipUser.ProviderUserKey, expand);

                                // filter current portal related profiles only as to identify this account related to current store.
                                var protalProfiles = _portalProfileRepository.GetByPortalID(portalId);
                                accountModel.Profiles = new Collection<ProfileModel>(accountModel.Profiles.Where(x => protalProfiles.Any(y => y.ProfileID == x.ProfileId)).ToList());
                                if (portalId > 0)
                                {
                                    accountModel.PortalId = portalId;
                                }
                                logErrorMessae = "Social Login successful";
                                ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.LoginSuccess, string.Empty, string.Empty, null, logErrorMessae, null);
                                return accountModel;
                            }
                        }
                    }
                    logErrorMessae = "Social Login Failed";
                    ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.LoginFailed, string.Empty, string.Empty, null, logErrorMessae, null);
                }
            return null;
        }

        public bool SocialUserCreateOrUpdateAccount(SocialLoginModel model)
        {
            if (!string.IsNullOrEmpty(model.Provider) && !string.IsNullOrEmpty(model.ProviderUserId) && !string.IsNullOrEmpty(model.UserName))
            {
                try
                {
                    OAuthWebSecurity.CreateOrUpdateAccount(model.Provider, model.ProviderUserId, model.UserName);
                    return true;
                }
                catch { }
            }
            return false;
        }

        public RegisteredSocialClientListModel GetRegisteredSocialClientDetailsList()
        {
            RegisteredSocialClientListModel model = new RegisteredSocialClientListModel();
            var authenticateItemData = OAuthWebSecurity.RegisteredClientData;
            foreach (AuthenticationClientData item in authenticateItemData)
            {
                if (!Equals(item, null))
                {
                    switch (item.AuthenticationClient.ProviderName)
                    {
                        case "facebook":
                            model.SocialClients.Add(new RegisteredSocialClientModel { ProviderName = item.AuthenticationClient.ProviderName, ProviderClientId = Convert.ToString(ConfigurationManager.AppSettings["FacebookAppId"]), ProviderClientSecret = Convert.ToString(ConfigurationManager.AppSettings["FacebookAppSecret"]) });
                            break;
                        case "google":
                            model.SocialClients.Add(new RegisteredSocialClientModel { ProviderName = item.AuthenticationClient.ProviderName, ProviderClientId = Convert.ToString(ConfigurationManager.AppSettings["GoogleClientId"]), ProviderClientSecret = Convert.ToString(ConfigurationManager.AppSettings["GoogleClientSecret"]) });
                            break;
                        default:
                            break;
                    }
                }
            }
            return model;
        }


        #endregion

        #region Private Methods

        /// <summary>
        /// Sets the supplier model.
        /// </summary>
        /// <param name="model">Model having the properties.</param>
        /// <returns>Model sets the value.</returns>
        private SupplierModel SetSupplierModel(AccountModel model)
        {
            SupplierModel supplierModel = new SupplierModel();
            if (!Equals(model, null))
            {
                supplierModel.Name = model.CompanyName;
                supplierModel.Description = string.Empty;
                supplierModel.ContactFirstName = model.FirstName;
                supplierModel.ContactLastName = model.LastName;
                supplierModel.ContactPhone = model.PhoneNumber;
                supplierModel.DisplayOrder = 500;
                supplierModel.IsActive = true;
                supplierModel.Custom1 = string.Empty;
                supplierModel.Custom2 = string.Empty;
                supplierModel.Custom3 = string.Empty;
                supplierModel.Custom4 = string.Empty;
                supplierModel.Custom5 = string.Empty;
                supplierModel.NotificationEmail = model.Email;
                supplierModel.ContactEmail = model.Email;
                supplierModel.EmailNotificationTemplate = string.Empty;
                supplierModel.EnableEmailNotification = true;
                supplierModel.ExternalSupplierNumber = model.ExternalAccountNum;
                supplierModel.SupplierTypeId = 1; // Assign Default Supplier Type Id
            }
            return supplierModel;
        }

        /// <summary>
        /// This method will send account activation email to relevant user
        /// </summary>
        /// <param name="userName">string user name</param>
        /// <param name="email">string email address</param>
        private void SendAccountActivationEmail(string userName, string email)
        {
            var defaultTemplatePath = HttpContext.Current.Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, AccountActivationHTMLTemplateName));
            if (File.Exists(defaultTemplatePath))
            {
                string portalName = ZNodeConfigManager.SiteConfig.StoreName;
                string smtpServer = ZNodeConfigManager.SiteConfig.SMTPServer;
                string senderEmail = ZNodeConfigManager.SiteConfig.CustomerServiceEmail;

                string subject = string.Format("{0} - Account Activation", portalName);

                StreamReader rw = new StreamReader(defaultTemplatePath);
                string messageText = rw.ReadToEnd();

                Regex rx1 = new Regex("#BillingFirstName#", RegexOptions.IgnoreCase);
                messageText = rx1.Replace(messageText, userName);

                //PRFT Custom COde : Start
                Regex rx2 = new Regex("#StoreLogo#", RegexOptions.IgnoreCase);
                string LogoPath = ZNodeConfigManager.SiteConfig.LogoPath.Trim().Replace("~/", "");
                messageText = rx2.Replace(messageText, apiUrl + "/" + LogoPath.TrimStart('~'));

                var rx3 = new Regex("#MailingInfo#", RegexOptions.IgnoreCase);
                messageText = rx3.Replace(messageText, ZNodeConfigManager.SiteConfig.CustomerServiceEmail);

                var rx4 = new Regex("#SupportPhone#", RegexOptions.IgnoreCase);
                messageText = rx4.Replace(messageText, ZNodeConfigManager.SiteConfig.CustomerServicePhoneNumber);

                var rx5 = new Regex("#SupportEmail#", RegexOptions.IgnoreCase);
                messageText = rx5.Replace(messageText, ZNodeConfigManager.SiteConfig.SalesEmail);

                string logoLink = ConfigurationSettings.AppSettings["DemoWebsiteUrl"];
                var rx6 = new Regex("#LogoLink#", RegexOptions.IgnoreCase);
                messageText = rx6.Replace(messageText, logoLink);
                //PRFT Custom Code : End

                try
                {
                    ZNode.Libraries.Framework.Business.ZNodeEmail.SendEmail(email, senderEmail, string.Empty, subject, messageText, true);
                }
                catch (Exception ex)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.LoginCreateSuccess, userName, "", null, ex.Message, null);
                }
            }
        }

        /// <summary>
        /// Generated the unique Password string
        /// </summary>
        /// <param name="maxSize">Length for Max password size</param>
        /// <returns>Return the Unique key combination</returns>
        private static string GetUniqueKey(int maxSize)
        {
            char[] chars = new char[62];
            string strCharacters;
            strCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            chars = strCharacters.ToCharArray();
            int size = maxSize;
            byte[] data = new byte[1];
            RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
            crypto.GetNonZeroBytes(data);
            size = maxSize;
            data = new byte[size];
            crypto.GetNonZeroBytes(data);
            StringBuilder result = new StringBuilder(size);
            foreach (byte bytedata in data)
            {
                result.Append(chars[bytedata % (chars.Length - 1)]);
            }
            SetNumberInUniqueKey(result);
            return result.ToString();
        }

        /// <summary>
        /// Set the Number at the end of the autogenerated unique key.
        /// As for User, it is mandatory to have atlease one number in the password
        /// </summary>
        /// <param name="result"></param>
        private static void SetNumberInUniqueKey(StringBuilder result)
        {
             Random rnd = new Random();
             int dice = rnd.Next(1, 9);
             result.Append(Convert.ToString(dice));
        }


        /// <summary>
        /// To check where user exists or not.
        /// </summary>
        /// <param name="email">string Email for the user</param>
        /// <returns>Return true or false</returns>
        private bool IsUserAlreadyExist(string email)
        {
            AccountAdmin _UserAccounts = new AccountAdmin();
            // To get all customer list
            TList<Account> customerList = _UserAccounts.GetAllCustomer();
            //To find specific email exist or not.
            Account account = customerList.Find("Email", email);
            return (Equals(account, null)) ? false : true;
        }

        /// <summary>
        /// Gets the tracking link.
        /// </summary>
        /// <param name="accountId">account Id.</param>
        /// <returns>Returns the tracking link.</returns>
        private string GetTrackingLink(int accountId)
        {
            TList<PortalProfile> portalProfile = new TList<PortalProfile>();
            TList<Domain> domainURL = new TList<Domain>();
            string domainName = string.Empty;

            var accountProfile = _accountProfileRepository.GetByAccountID(accountId);

            if (!Equals(accountProfile, null) && accountProfile.Any())
            {
                PortalProfileService portalProfileService = new PortalProfileService();
                portalProfile = portalProfileService.GetByProfileID(accountProfile[0].ProfileID.GetValueOrDefault(0));
            }

            if (!Equals(portalProfile, null) && portalProfile.Any())
            {
                DomainAdmin domainAdmin = new DomainAdmin();
                domainURL = domainAdmin.GetDomainByPortalID(portalProfile[0].PortalID);
            }

            if (!Equals(domainURL, null) && domainURL.Any())
            {
                domainName = "http://" + domainURL.FirstOrDefault().DomainName;
            }
            return domainName;
        }

        /// <summary>
        /// To Check whether the Znode encryption is able to encrypt the password or not.
        /// </summary>
        /// <param name="password">string Password</param>
        /// <returns>Return true or false</returns>
        private bool CheckPasswordEncryption(string password)
        {
            try
            {
                ZNodeEncryption encrypt = new ZNodeEncryption();
                encrypt.EncryptData(password);
                return true;
            }
            catch (Exception ex)
            {
                ZNodeLogging.LogMessage("An error occured while reading the Znode Certificate. " + ex.Message);
                return false;
            }

        }
        #endregion
       
    }
}