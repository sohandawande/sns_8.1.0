﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using Znode.Engine.Api.Models;
using Znode.Engine.Promotions;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using Znode.Engine.Taxes;
using Znode.Libraries.Helpers;
using Znode.Libraries.Helpers.Constants;
using Znode.Libraries.Helpers.Extensions;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;
using System.Configuration;
using PRFT.Engine.ERP;

using AddOnRepository = ZNode.Libraries.DataAccess.Service.ProductAddOnService;
using AddOnValueRepository = ZNode.Libraries.DataAccess.Service.AddOnValueService;
using AttributeTypeRepository = ZNode.Libraries.DataAccess.Service.AttributeTypeService;
using CategoryRepository = ZNode.Libraries.DataAccess.Service.ProductCategoryService;
using CrossSellRepository = ZNode.Libraries.DataAccess.Service.ProductCrossSellService;
using CustomerPricingRepository = ZNode.Libraries.DataAccess.Service.CustomerPricingService;
using FacetGroupRepository = ZNode.Libraries.DataAccess.Service.FacetGroupService;
using FacetProductSKURepository = ZNode.Libraries.DataAccess.Service.FacetProductSKUService;
using FacetsRepository = ZNode.Libraries.DataAccess.Service.FacetService;
using HighlightRepository = ZNode.Libraries.DataAccess.Service.ProductHighlightService;
using ImageRepository = ZNode.Libraries.DataAccess.Service.ProductImageService;
using ManufacturerRepository = ZNode.Libraries.DataAccess.Service.ManufacturerService;
using ParentChildProductRepository = ZNode.Libraries.DataAccess.Service.ParentChildProductService;
using ProductAttributeRepository = ZNode.Libraries.DataAccess.Service.ProductAttributeService;
using ProductBundlesRepository = ZNode.Libraries.DataAccess.Service.ParentChildProductService;
using ProductImageRepository = ZNode.Libraries.DataAccess.Service.ProductImageService;
using ProductRepository = ZNode.Libraries.DataAccess.Service.ProductService;
using ProductsCatalogsRepository = ZNode.Libraries.DataAccess.Service.VwZnodeProductsCatalogsService;
using ProductsCategoriesPromotionsRepository = ZNode.Libraries.DataAccess.Service.VwZnodeProductsCategoriesPromotionsService;
using ProductsCategoriesRepository = ZNode.Libraries.DataAccess.Service.VwZnodeProductsCategoriesService;
using ProductsPortalsRepository = ZNode.Libraries.DataAccess.Service.VwZnodeProductsPortalsService;
using ProductsPromotionsRepository = ZNode.Libraries.DataAccess.Service.VwZnodeProductsPromotionsService;
using ProductTierRepository = ZNode.Libraries.DataAccess.Service.ProductTierService;
using ProductTypeRepository = ZNode.Libraries.DataAccess.Service.ProductTypeService;
using PromotionRepository = ZNode.Libraries.DataAccess.Service.PromotionService;
using ReviewRepository = ZNode.Libraries.DataAccess.Service.ReviewService;
using SkuAdminRepository = ZNode.Libraries.Admin.SKUAdmin;
using SkuAttributeRepository = ZNode.Libraries.DataAccess.Service.SKUAttributeService;
using SkuInventoryRepository = ZNode.Libraries.DataAccess.Service.SKUInventoryService;
using SkuRepository = ZNode.Libraries.DataAccess.Service.SKUService;
using TagsRepository = ZNode.Libraries.DataAccess.Service.TagsService;
using TierRepository = ZNode.Libraries.DataAccess.Service.ProductTierService;
using UrlRedirectAdminRepository = ZNode.Libraries.Admin.UrlRedirectAdmin;
using ZnodeProductCatalogRepository = ZNode.Libraries.ECommerce.Catalog.ZNodeProduct;
using ZnodeProductDigitalAssetRepository = ZNode.Libraries.DataAccess.Service.DigitalAssetService;

namespace Znode.Engine.Services
{
    public class ProductService : BaseService, IProductService
    {
        #region Private Variables
        private readonly AddOnRepository _addOnRepository;
        private readonly AddOnValueRepository _addOnValueRepository;
        private readonly AttributeTypeRepository _attributeTypeRepository;
        private readonly CategoryRepository _categoryRepository;
        private readonly CrossSellRepository _crossSellRepository;
        private readonly CustomerPricingRepository _customerPricingRepository;
        private readonly HighlightRepository _highlightRepository;
        private readonly ImageRepository _imageRepository;
        private readonly ManufacturerRepository _manufacturerRepository;
        private readonly ParentChildProductRepository _parentChildProductRepository;
        private readonly ProductAttributeRepository _productAttributeRepository;
        private readonly ProductRepository _productRepository;
        private readonly ProductTypeRepository _productTypeRepository;
        private readonly ProductsCatalogsRepository _productsCatalogsRepository;
        private readonly ProductsCategoriesRepository _productsCategoriesRepository;
        private readonly ProductsCategoriesPromotionsRepository _productsCategoriesPromotionsRepository;
        private readonly ProductsPortalsRepository _productsPortalsRepository;
        private readonly ProductsPromotionsRepository _productsPromotionsRepository;
        private readonly PromotionRepository _promotionRepository;
        private readonly ReviewRepository _reviewRepository;
        private readonly SkuRepository _skuRepository;
        private readonly SkuInventoryRepository _skuInventoryRepository;
        private readonly SkuAttributeRepository _skuAttributeRepository;
        private readonly TierRepository _tierRepository;
        private readonly TagsRepository _tagsRepository;
        private readonly FacetGroupRepository _facetGroupRepository;
        private readonly FacetsRepository _facetsRepository;
        private readonly FacetProductSKURepository _facetProductSKURepository;
        private readonly ProductImageRepository _productImageRepository;
        private readonly ProductBundlesRepository _productBundlesRepository;
        private readonly ProductTierRepository _productTierRepository;
        private readonly ZnodeProductDigitalAssetRepository _productDigitalAssetRepository;
        #endregion

        #region Public Methods
        public ProductService()
        {
            _addOnRepository = new AddOnRepository();
            _addOnValueRepository = new AddOnValueRepository();
            _attributeTypeRepository = new AttributeTypeRepository();
            _categoryRepository = new CategoryRepository();
            _crossSellRepository = new CrossSellRepository();
            _customerPricingRepository = new CustomerPricingRepository();
            _highlightRepository = new HighlightRepository();
            _imageRepository = new ImageRepository();
            _manufacturerRepository = new ManufacturerRepository();
            _parentChildProductRepository = new ParentChildProductRepository();
            _productAttributeRepository = new ProductAttributeRepository();
            _productRepository = new ProductRepository();
            _productTypeRepository = new ProductTypeRepository();
            _productsCatalogsRepository = new ProductsCatalogsRepository();
            _productsCategoriesRepository = new ProductsCategoriesRepository();
            _productsCategoriesPromotionsRepository = new ProductsCategoriesPromotionsRepository();
            _productsPortalsRepository = new ProductsPortalsRepository();
            _productsPromotionsRepository = new ProductsPromotionsRepository();
            _promotionRepository = new PromotionRepository();
            _reviewRepository = new ReviewRepository();
            _skuRepository = new SkuRepository();
            _skuInventoryRepository = new SkuInventoryRepository();
            _skuAttributeRepository = new SkuAttributeRepository();
            _tierRepository = new TierRepository();
            _tagsRepository = new TagsRepository();
            _facetGroupRepository = new FacetGroupRepository();
            _facetsRepository = new FacetsRepository();
            _facetProductSKURepository = new FacetProductSKURepository();
            _productImageRepository = new ProductImageRepository();
            _productBundlesRepository = new ProductBundlesRepository();
            _productTierRepository = new ProductTierRepository();
            _productDigitalAssetRepository = new ZnodeProductDigitalAssetRepository();
        }

        public ProductModel GetProduct(int productId, NameValueCollection expands)
        {
            var product = _productRepository.GetByProductID(productId);
            if (product != null)
            {
                GetExpands(expands, product);
            }

            return ProductMap.ToModel(product);
        }

        public ProductModel GetProductWithSku(int productId, int skuId, NameValueCollection expands)
        {
            var product = _productRepository.GetByProductID(productId);
            if (product != null)
            {
                var sku = _skuRepository.GetBySKUID(skuId);
                if (sku != null)
                {
                    // Set selected SKU and get pricing
                    product.SelectedSku = sku;
                    GetPricing(product, sku);
                }
                else
                {
                    throw new Exception("SKU ID " + skuId + " does not exist.");
                }

                GetExpands(expands, product);
            }

            return ProductMap.ToModel(product);
        }

        public ProductListModel GetProducts(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new ProductListModel();
            var products = new TList<Product>();
            var tempList = new TList<Product>();

            var query = new ProductQuery { Junction = String.Empty };
            var sortBuilder = new ProductSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            tempList = _productRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                model.PageSize = model.TotalResults;
            }

            // Now go through the list and get any expands
            foreach (var product in tempList)
            {
                GetExpands(expands, product);

                product.RetailPrice = GetFinalPrice(product.TaxClassID.GetValueOrDefault(), product.RetailPrice.GetValueOrDefault());

                if (product.SalePrice.HasValue)
                {
                    product.SalePrice = GetFinalPrice(product.TaxClassID.GetValueOrDefault(), product.SalePrice.GetValueOrDefault());
                }

                products.Add(product);
            }

            // Map each item and add to the list
            foreach (var p in products)
            {
                model.Products.Add(ProductMap.ToModel(p));
            }

            return model;
        }

        public ProductListModel GetProductsByCatalog(int catalogId, NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new ProductListModel();
            var products = new TList<Product>();
            var tempList = new VList<VwZnodeProductsCatalogs>();

            var query = new VwZnodeProductsCatalogsQuery { Junction = String.Empty };
            var sortBuilder = new VwZnodeProductsCatalogsSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetQueryParameter(VwZnodeProductsCatalogsColumn.CatalogID, FilterOperators.Equals, catalogId.ToString(), query);

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            tempList = _productsCatalogsRepository.Find(query, sortBuilder.ToString(), pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                model.PageSize = model.TotalResults;
            }

            // Now go through the list, get the full product, and get any expands
            foreach (var item in tempList)
            {
                var product = _productRepository.GetByProductID(item.ProductID);
                GetExpands(expands, product);
                products.Add(product);
            }

            // Map each item and add to the list
            foreach (var p in products)
            {
                model.Products.Add(ProductMap.ToModel(p));
            }

            return model;
        }

        public ProductListModel GetProductsByCatalogIds(string catalogIds, NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {

            if (String.IsNullOrEmpty(catalogIds))
            {
                throw new Exception("List of catalog IDs cannot be null or empty.");
            }

            var model = new ProductListModel();
            var products = new TList<Product>();
            var tempList = new VList<VwZnodeProductsCatalogs>();

            var query = new VwZnodeProductsCatalogsQuery { Junction = String.Empty };
            var sortBuilder = new VwZnodeProductsCatalogsSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            var list = catalogIds.Split(',');
            foreach (var catalogId in list.Where(catalogId => !String.IsNullOrEmpty(catalogId)))
            {
                query.BeginGroup("OR");
                query.AppendEquals(VwZnodeProductsCatalogsColumn.CatalogID, catalogId);
                query.EndGroup();
            }

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            tempList = _productsCatalogsRepository.Find(query, sortBuilder.ToString(), pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                model.PageSize = model.TotalResults;
            }

            // Now go through the list, get the full product, and get any expands
            foreach (var item in tempList)
            {
                var product = _productRepository.GetByProductID(item.ProductID);
                GetExpands(expands, product);
                products.Add(product);
            }

            // Map each item and add to the list
            foreach (var p in products)
            {
                model.Products.Add(ProductMap.ToModel(p));
            }

            return model;
        }

        public ProductListModel GetProductsByCategory(int categoryId, NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new ProductListModel();
            var products = new TList<Product>();
            var tempList = new VList<VwZnodeProductsCategories>();

            var query = new VwZnodeProductsCategoriesQuery { Junction = String.Empty };
            var sortBuilder = new VwZnodeProductsCategoriesSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetQueryParameter(VwZnodeProductsCategoriesColumn.CategoryID, FilterOperators.Equals, categoryId.ToString(), query);

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            tempList = _productsCategoriesRepository.Find(query, sortBuilder.ToString(), pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                model.PageSize = model.TotalResults;
            }

            // Now go through the list, get the full product, and get any expands
            foreach (var item in tempList)
            {
                var product = _productRepository.GetByProductID(item.ProductID);
                GetExpands(expands, product);
                products.Add(product);
            }

            // Map each item and add to the list
            foreach (var p in products)
            {
                model.Products.Add(ProductMap.ToModel(p));
            }

            return model;
        }

        public ProductListModel GetProductsByCategoryByPromotionType(int categoryId, int promotionTypeId, NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new ProductListModel();
            var products = new TList<Product>();
            var tempList = new VList<VwZnodeProductsCategoriesPromotions>();

            var query = new VwZnodeProductsCategoriesPromotionsQuery { Junction = String.Empty };
            var sortBuilder = new VwZnodeProductsCategoriesPromotionsSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetQueryParameter(VwZnodeProductsCategoriesPromotionsColumn.CategoryID, FilterOperators.Equals, categoryId.ToString(), query);
            SetQueryParameter(VwZnodeProductsCategoriesPromotionsColumn.DiscountTypeID, FilterOperators.Equals, promotionTypeId.ToString(), query);

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            tempList = _productsCategoriesPromotionsRepository.Find(query, sortBuilder.ToString(), pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                model.PageSize = model.TotalResults;
            }

            // Now go through the list, get the full product, and get any expands
            foreach (var item in tempList)
            {
                var product = _productRepository.GetByProductID(item.ProductID);
                GetExpands(expands, product);
                products.Add(product);
            }

            // Map each item and add to the list
            foreach (var p in products)
            {
                model.Products.Add(ProductMap.ToModel(p));
            }

            return model;
        }

        public ProductListModel GetProductsByExternalIds(string externalIds, NameValueCollection expands, NameValueCollection sorts)
        {
            if (String.IsNullOrEmpty(externalIds))
            {
                throw new Exception("List of product IDs cannot be null or empty.");
            }

            var model = new ProductListModel();
            var products = new TList<Product>();
            var tempList = new TList<Product>();

            var query = new ProductQuery { Junction = String.Empty };
            var sortBuilder = new ProductSortBuilder();

            var list = externalIds.Split(',');
            foreach (var externalId in list)
            {
                query.BeginGroup("OR");
                query.AppendEquals(ProductColumn.ExternalID, externalId);
                query.EndGroup();
            }

            SetSorting(sorts, sortBuilder);

            // Get the initial set
            tempList = _productRepository.Find(query, sortBuilder);

            // Now go through the list and get any expands
            foreach (var product in tempList)
            {
                GetExpands(expands, product);
                products.Add(product);
            }

            // Map each item and add to the list
            foreach (var p in products)
            {
                model.Products.Add(ProductMap.ToModel(p));
            }

            return model;
        }

        public ProductListModel GetProductsByHomeSpecials(int portalId, NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new ProductListModel();
            var products = new TList<Product>();
            var tempList = new VList<VwZnodeProductsPortals>();

            var query = new VwZnodeProductsPortalsQuery { Junction = String.Empty };
            var sortBuilder = new VwZnodeProductsCatalogsSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetQueryParameter(VwZnodeProductsPortalsColumn.PortalID, FilterOperators.Equals, portalId.ToString(), query);
            SetQueryParameter(VwZnodeProductsPortalsColumn.HomepageSpecial, FilterOperators.Equals, true.ToString(), query);

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            tempList = _productsPortalsRepository.Find(query, sortBuilder.ToString(), pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                model.PageSize = model.TotalResults;
            }

            // Now go through the list, get the full product, and get any expands
            foreach (var item in tempList)
            {
                var product = _productRepository.GetByProductID(item.ProductID);
                GetExpands(expands, product);
                products.Add(product);
            }

            // Map each item and add to the list
            foreach (var p in products)
            {
                model.Products.Add(ProductMap.ToModel(p));
            }

            return model;
        }

        public ProductListModel GetProductsByProductIds(string productIds, NameValueCollection expands, NameValueCollection sorts)
        {
            if (String.IsNullOrEmpty(productIds))
            {
                throw new Exception("List of product IDs cannot be null or empty.");
            }

            var model = new ProductListModel();
            var products = new TList<Product>();
            var tempList = new TList<Product>();

            var query = new ProductQuery { Junction = String.Empty };
            var sortBuilder = new ProductSortBuilder();

            var list = productIds.Split(',');
            foreach (var productId in list.Where(productId => !String.IsNullOrEmpty(productId)))
            {
                query.BeginGroup("OR");
                query.AppendEquals(ProductColumn.ProductID, productId);
                query.EndGroup();
            }

            SetSorting(sorts, sortBuilder);

            // Get the initial set
            tempList = _productRepository.Find(query, sortBuilder);

            // Now go through the list and get any expands
            foreach (var product in tempList)
            {
                GetExpands(expands, product);
                products.Add(product);
            }

            // Map each item and add to the list
            foreach (var p in products)
            {
                model.Products.Add(ProductMap.ToModel(p));
            }

            return model;
        }

        public ProductListModel GetProductsByPromotionType(int promotionTypeId, NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new ProductListModel();
            var products = new TList<Product>();
            var tempList = new VList<VwZnodeProductsPromotions>();

            var query = new VwZnodeProductsPromotionsQuery { Junction = String.Empty };
            var sortBuilder = new VwZnodeProductsPromotionsSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetQueryParameter(VwZnodeProductsPromotionsColumn.DiscountTypeID, FilterOperators.Equals, promotionTypeId.ToString(), query);

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            tempList = _productsPromotionsRepository.Find(query, sortBuilder.ToString(), pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                model.PageSize = model.TotalResults;
            }

            // Now go through the list, get the full product, and get any expands
            foreach (var item in tempList)
            {
                var product = _productRepository.GetByProductID(item.ProductID);
                GetExpands(expands, product);
                products.Add(product);
            }

            // Map each item and add to the list
            foreach (var p in products)
            {
                model.Products.Add(ProductMap.ToModel(p));
            }

            return model;
        }

        public ProductModel CreateProduct(ProductModel model)
        {
            bool isAdminUser = true;
            string userRole = string.Empty;
            AccountInfoModel accountInforModel = new AccountInfoModel();
            if (Equals(model, null))
            {
                throw new Exception("Product model cannot be null.");
            }
            if (IsSkuExist(model.SkuName, model.ProductId))
            {
                throw new Exception("SKU or Part# already exists. Please try with different SKU or Part#.");
            }

            Product entity = ProductMap.ToEntity(model);

            if (entity.ProductTypeID < 1)
            {
                // Attach the default product type
                entity.ProductTypeID = _productTypeRepository.GetByName("Default").ProductTypeId;
            }
            if (!string.IsNullOrEmpty(model.UserName))
            {
                AccountHelper accountHelper = new AccountHelper();
                DataSet accountDataSet = accountHelper.GetAccountInfo(model.UserName);
                if (!Equals(accountDataSet.Tables[0], null) && accountDataSet.Tables[0].Rows.Count > 0)
                {
                    accountInforModel = accountDataSet.Tables[0].Rows[0].ToSingleRow<AccountInfoModel>();
                    if (!Equals(accountInforModel, null) && (Equals(accountInforModel.RoleName, Constant.RoleFranchise) || Equals(accountInforModel.RoleName, Constant.RoleVendor)))
                    {
                        entity.DisplayOrder = (Equals(entity.DisplayOrder, null)) ? 500 : entity.DisplayOrder;
                        ProductAdmin productAdmin = new ProductAdmin();
                        isAdminUser = false;
                        userRole = accountInforModel.RoleName;
                        entity.ReviewStateID = productAdmin.UpdateReviewStateID(entity, accountInforModel.PortalId, entity.ProductID, accountInforModel.AccountId, model.UserName);
                        productAdmin = null;
                    }
                }
                if (!Equals(accountInforModel, null) && (Equals(accountInforModel.RoleName, Constant.RoleVendor)))
                {
                    MembershipUser user = Membership.GetUser(model.UserName);
                    if (!Equals(user, null))
                    {
                        ZNode.Libraries.DataAccess.Service.AccountService accountService = new ZNode.Libraries.DataAccess.Service.AccountService();
                        TList<Account> accountList = accountService.GetByUserID((Guid)user.ProviderUserKey);
                        if (!Equals(accountList, null) && accountList.Count > 0)
                        {
                            entity.AccountID = accountList[0].AccountID;
                            TList<Supplier> supplier = DataRepository.SupplierProvider.GetByExternalSupplierNoName(accountList[0].ExternalAccountNo, accountList[0].CompanyName);
                            if (!Equals(supplier, null) && supplier.Count > 0)
                            {
                                entity.SupplierID = supplier[0].SupplierID;
                            }
                        }
                    }
                }

            }
            // Set the dates
            entity.CreateDate = DateTime.Now;
            entity.UpdateDte = DateTime.Now;

            Product product = _productRepository.Save(entity);
            if (product.ProductID > 0)
            {
                //To add inventory
                InventoryModel inventorymodel = new InventoryModel();
                inventorymodel.Sku = model.SkuName;
                inventorymodel.QuantityOnHand = model.QuantityOnHand;
                inventorymodel.ReorderLevel = model.ReorderLevel;
                var inventoryEntity = InventoryMap.ToEntity(inventorymodel);
                _skuInventoryRepository.Save(inventoryEntity);

                //To add SKU
                SkuModel skumodel = new SkuModel();
                skumodel.ProductId = product.ProductID;
                skumodel.Sku = model.SkuName;
                skumodel.IsActive = model.IsActive;
                SKU skuEntity = SkuMap.ToEntity(skumodel);
                SKU sku = _skuRepository.Save(skuEntity);
                model.SkuId = sku.SKUID;


                //To add product category in case of mall admin
                if (!Equals(model.AccountId, null) && !Equals(model.AccountId, 0))
                {
                    ProductAdmin productAdmin = new ProductAdmin();
                    if (!string.IsNullOrEmpty(model.CategoryIds))
                    {
                        List<int> vendorCategoryIds = model.CategoryIds.Split(',').Select(int.Parse).ToList();
                        // Add Product Categories
                        foreach (int item in vendorCategoryIds)
                        {
                            ProductCategory prodCategory = new ProductCategory();
                            ProductAdmin prodAdmin = new ProductAdmin();
                            prodCategory.ActiveInd = true;
                            prodCategory.CategoryID = item;
                            prodCategory.ProductID = product.ProductID;
                            prodAdmin.AddProductCategory(prodCategory);
                        }
                    }
                }
                //To save sku attributes
                if (!String.IsNullOrEmpty(model.AttributeIds))
                {
                    SKUHelper skuHelper = new SKUHelper();
                    skuHelper.AddSkuAttribute(model.SkuId, model.AttributeIds);
                }
                if (!isAdminUser)
                {
                    SendMailByVendorOrFranchise(product.ProductID, product.Name, accountInforModel, product.ShortDescription);
                }

            }
            return ProductMap.ToModel(product);
        }

        public ProductModel UpdateProduct(int productId, ProductModel model)
        {
            if (productId < 1)
            {
                throw new Exception("Product ID cannot be less than 1.");
            }

            if (Equals(model, null))
            {
                throw new Exception("Product model cannot be null.");
            }

            //IsFromSitecore condition is added to check request is from SiteCore for Znode Sitecore Connector
            //If request is from Sitecore then ZnodeProduct table data need to update on impact on SKU & Inventory table
            if (Equals(model.IsFromSitecore, true))
            {
                var product = _productRepository.GetByProductID(productId);
                if (product != null)
                {
                    // Set product ID and update date
                    model.ProductId = productId;
                    model.UpdateDate = DateTime.Now;

                    var productToUpdate = ProductMap.ToEntity(model);

                    var updated = _productRepository.Update(productToUpdate);
                    if (updated)
                    {
                        product = _productRepository.GetByProductID(productId);
                        return ProductMap.ToModel(product);
                    }
                }
            }
            else
            {
                if (Equals(model.SkuName, null))
                {
                    throw new Exception("SKU cannot be null.");
                }

                if (IsSkuExist(model.SkuName, model.ProductId))
                {
                    throw new Exception("SKU or Part# already exists. Please try with different SKU or Part#.");
                }


                SKUAdmin skuAdminAccess = new SKUAdmin();
                bool RetValue = skuAdminAccess.CheckSKUAttributes(productId, model.SkuId, model.AttributeIds);
                skuAdminAccess = null;
                if (RetValue)
                {
                    throw new Exception("This Attribute combination already exists for this product. Please select different combination.");
                }
                var product = _productRepository.GetByProductID(productId);
                if (!Equals(product, null))
                {
                    // Set product ID and update date
                    model.ProductId = productId;
                    model.UpdateDate = DateTime.Now;
                    MapProductDetailsToEntity(model, product);
                    if (!string.IsNullOrEmpty(model.UserName))
                    {
                        AccountHelper accountHelper = new AccountHelper();
                        DataSet accountDataSet = accountHelper.GetAccountInfo(model.UserName);
                        AccountInfoModel accountInforModel = new AccountInfoModel();
                        ProductAdmin productAdmin = new ProductAdmin();
                        if (!Equals(accountDataSet.Tables[0], null) && accountDataSet.Tables[0].Rows.Count > 0)
                        {
                            accountInforModel = accountDataSet.Tables[0].Rows[0].ToSingleRow<AccountInfoModel>();
                            if (!Equals(accountInforModel, null) && (Equals(accountInforModel.RoleName, Constant.RoleFranchise) || Equals(accountInforModel.RoleName, Constant.RoleVendor)))
                            {
                                product.ReviewStateID = productAdmin.UpdateReviewStateID(product, accountInforModel.PortalId, product.ProductID, accountInforModel.AccountId, model.UserName);
                            }
                        }
                        if (!Equals(accountInforModel, null) && (Equals(accountInforModel.RoleName, Constant.RoleVendor)))
                        {
                            product.TrackInventoryInd = model.TrackInventory;
                            product.AllowBackOrder = model.AllowBackOrder;
                            product.BackOrderMsg = model.BackOrderMessage;
                            product.OutOfStockMsg = model.OutOfStockMessage;
                            product.ActiveInd = model.IsActive;
                            productAdmin.DeleteProductCategories(productId);

                            if (!string.IsNullOrEmpty(model.CategoryIds))
                            {
                                List<int> vendorCategoryIds = model.CategoryIds.Split(',').Select(int.Parse).ToList();
                                // Add Product Categories
                                foreach (int item in vendorCategoryIds)
                                {
                                    ProductCategory prodCategory = new ProductCategory();
                                    ProductAdmin prodAdmin = new ProductAdmin();
                                    prodCategory.ActiveInd = true;
                                    prodCategory.CategoryID = item;
                                    prodCategory.ProductID = productId;
                                    prodAdmin.AddProductCategory(prodCategory);
                                }
                            }
                        }
                    }
                    var updated = _productRepository.Update(product);
                    if (updated)
                    {
                        product = _productRepository.GetByProductID(productId);

                        //To update sku
                        var skuEntity = _skuRepository.GetBySKUID(model.SkuId);

                        //To get inventory
                        var inventory = _skuInventoryRepository.GetBySKU(skuEntity.SKU);

                        //To update new inventory
                        inventory.SKU = model.SkuName;
                        inventory.QuantityOnHand = model.QuantityOnHand;
                        inventory.ReOrderLevel = model.ReorderLevel;
                        _skuInventoryRepository.Update(inventory);

                        skuEntity.SKU = model.SkuName;
                        skuEntity.ActiveInd = model.IsActive;
                        _skuRepository.Update(skuEntity);

                        //To update sku attributes                   
                        var skuHelper = new SKUHelper();
                        skuHelper.AddSkuAttribute(model.SkuId, model.AttributeIds);
                        return ProductMap.ToModel(product);
                    }
                }
            }
            return null;
        }

        public bool DeleteProduct(int productId)
        {
            if (productId < 1)
            {
                throw new Exception("Product ID cannot be less than 1.");
            }

            var product = _productRepository.GetByProductID(productId);
            try
            {
                if (product != null)
                {
                    return _productRepository.Delete(product);
                }
            }
            catch
            {
                throw new Exception("The product can not be deleted until all associated items are removed. Please ensure that this product does not contain cross-sell items, product images, promotions or skus. If it does, then delete these Items first.");
            }

            return false;
        }

        public SkuProductListModel GetSkuProductListBySku(List<Tuple<string, string, string>> filters)
        {
            int portalId = 0;
            string sku = string.Empty;
            if (!Equals(filters, null) && filters.Count > 0)
            {
                foreach (Tuple<string, string, string> filterItem in filters)
                {
                    if (Equals(filterItem.Item1, FilterKeys.Sku))
                    {
                        sku = filterItem.Item3;
                    }
                    else if (Equals(filterItem.Item1, FilterKeys.PortalId))
                    {
                        portalId = Convert.ToInt32(filterItem.Item3);
                    }
                }
            }
            SkuProductListModel skuProductList = new SkuProductListModel();
            if (!string.IsNullOrEmpty(sku) && portalId > 0)
            {
                ProductHelper productHelper = new ProductHelper();
                DataSet resultDataSet = productHelper.GetProductListBySku(sku, portalId);
                if (!Equals(resultDataSet, null) && resultDataSet.Tables.Count > 0 && !Equals(resultDataSet.Tables[0], null) && resultDataSet.Tables[0].Rows.Count > 0)
                {
                    skuProductList.SkuProductList = resultDataSet.Tables[0].ToList<SkuProductModel>().ToCollection();
                }
                SkuProductMap.SetSmallImagePath(skuProductList);
            }
            return skuProductList;
        }
        
        //PRFT Custom Code:Start
        public PRFTInventoryListModel GetInventoryFromErp(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            List<Tuple<string, string, string>> filterList = new List<Tuple<string, string, string>>();
            PRFTInventoryListModel list = new PRFTInventoryListModel();

            int totalRowCount = 0;
            var pagingStart = 0;
            var pagingLength = 0;
            string orderBy = StringHelper.GenerateDynamicOrderByClause(sorts);
            string innerWhereClause = string.Empty;
            foreach (var item in filters)
            {
                if (Equals(item.Item1, "username"))
                {
                    filterList.Add(item);
                    innerWhereClause = StringHelper.GenerateDynamicWhereClause(filterList);
                    filters.Remove(item);
                    break;
                }
            }
            string whereClause = StringHelper.GenerateDynamicWhereClause(filters);
            pagingStart = Convert.ToInt32(page.Get(PageKeys.Index));
            pagingLength = Convert.ToInt32(page.Get(PageKeys.Size));

            StoredProcedureHelper storedProcedureHelper = new StoredProcedureHelper();
            DataSet resultDataSet = storedProcedureHelper.SP_DataDetailsSpConfig(whereClause, string.Empty, StoredProcedureKeys.SpGetInventoryList, orderBy, pagingStart.ToString(), pagingLength.ToString(), out totalRowCount, null, innerWhereClause);
            if (resultDataSet.Tables.Count > 0)
            {
                list.Inventories = resultDataSet.Tables[0].ToList<PRFTInventoryModel>().ToCollection();

                //Update Inventory details from ERP
                if (list.Inventories != null && list.Inventories.Count > 0)
                {
                    new ERPInventoryService().UpdateInventoryDetailsFromERP(list);
                }
            }
            else
            {
                list.Inventories = new System.Collections.ObjectModel.Collection<PRFTInventoryModel>();
            }
            list.TotalResults = totalRowCount;
            list.PageSize = pagingLength;
            list.PageIndex = pagingStart;

            // Check to set page size equal to the total number of results
            if (Equals(pagingLength, int.MaxValue))
            {
                list.PageSize = list.TotalResults;
            }

            return list;
        }
        //PRFT Custom Code:End
        #endregion

        #region Znode Version 8.0

        #region Product Settings

        public ProductModel UpdateProductSettings(int productId, ProductModel model)
        {
            if (productId < 1)
            {
                throw new Exception("Product ID cannot be less than 1.");
            }

            if (model == null)
            {
                throw new Exception("Product model cannot be null.");
            }


            var product = _productRepository.GetByProductID(productId);
            if (!Equals(product, null))
            {
                model.SeoUrl = model.SeoUrl.Replace(" ", "-");
                // Set product ID and update date
                string oldSeoUrl = product.SEOURL;
                model.ProductId = productId;
                model.UpdateDate = DateTime.Now;
                MapProductSettingToEntity(model, product);

                if (!string.IsNullOrEmpty(model.UserName))
                {
                    AccountInfoModel accountInforModel = new AccountInfoModel();
                    AccountHelper accountHelper = new AccountHelper();
                    DataSet accountDataSet = accountHelper.GetAccountInfo(model.UserName);
                    if (!Equals(accountDataSet.Tables[0], null) && accountDataSet.Tables[0].Rows.Count > 0)
                    {
                        accountInforModel = accountDataSet.Tables[0].Rows[0].ToSingleRow<AccountInfoModel>();
                        if (!Equals(accountInforModel, null) && (Equals(accountInforModel.RoleName, Constant.RoleFranchise) || Equals(accountInforModel.RoleName, Constant.RoleVendor)))
                        {
                            ProductAdmin productAdmin = new ProductAdmin();
                            product.ReviewStateID = productAdmin.UpdateReviewStateID(product, accountInforModel.PortalId, productId, accountInforModel.AccountId, model.UserName);
                            productAdmin = null;
                        }
                    }
                }

                var updated = _productRepository.Update(product);
                if (Equals(model.IsFranchisable, false))
                {
                    var productCategoryHelper = new ProductCategoryHelper();
                    productCategoryHelper.RemoveProductCategory(productId);
                }

                if (Equals(model.RedirectUrlInd, true))
                {
                    string newSeoUrl = model.SeoUrl;
                    UrlRedirectAdminRepository _urlRedirectAdminRepository = new UrlRedirectAdminRepository();
                    _urlRedirectAdminRepository.AddUrlRedirectTableForMVCAdmin(SEOUrlType.Product, oldSeoUrl, newSeoUrl, model.ProductId.ToString());
                }

                if (updated)
                {
                    return ProductMap.ToModel(product);
                }
            }

            return null;
        }

        public bool IsSeoUrlExist(int productId, string seoUrl)
        {
            if (productId < 1)
            {
                throw new Exception("Product ID cannot be less than 1.");
            }
            seoUrl = seoUrl.Replace(" ", "-");
            var productHelper = new ProductHelper();
            var isExist = productHelper.IsSeoUrlExist(productId, seoUrl);

            return isExist;
        }

        public ProductModel GetProductDetailsByProductId(int productId)
        {
            var productHelper = new ProductHelper();
            ProductModel model = new ProductModel();
            DataSet dataset = productHelper.GetProductDetailsByProductId(productId);
            if (dataset != null && dataset.Tables.Count > 0 && dataset.Tables[0].Rows.Count > 0)
            {
                model = ProductMap.ToModel(dataset);
            }
            return model;
        }

        #endregion

        #region Product Category

        public CategoryListModel GetProductCategoryByProductId(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new CategoryListModel();
            string whereClause = StringHelper.GenerateDynamicWhereClause(filters);
            string orderBy = StringHelper.GenerateDynamicOrderByClause(sorts);
            int totalRowCount = 0;
            var pagingStart = 0;
            var pagingLength = 0;
            SetPaging(page, model, out pagingStart, out pagingLength);
            pagingStart = Convert.ToInt32(page.Get(PageKeys.Index));

            StoredProcedureHelper storedProcedureHelper = new StoredProcedureHelper();

            DataSet resultDataSet = storedProcedureHelper.SP_DataDetailsSpConfig(whereClause, null, StoredProcedureKeys.SpGetProductCategoryByProductId, orderBy, pagingStart.ToString(), pagingLength.ToString(), out totalRowCount);
            model.Categories = resultDataSet.Tables[0].ToList<CategoryModel>().ToCollection();
            model.TotalResults = totalRowCount;
            model.PageSize = pagingLength;
            model.PageIndex = pagingStart;
            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                model.PageSize = model.TotalResults;
            }

            return model;
        }

        public CategoryListModel GetProductUnAssociatedCategoryByProductId(int productId, NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new CategoryListModel();
            string whereClause = StringHelper.GenerateDynamicWhereClause(filters);
            string orderBy = StringHelper.GenerateDynamicOrderByClause(sorts);
            int totalRowCount = 0;
            var pagingStart = 0;
            var pagingLength = 0;
            SetPaging(page, model, out pagingStart, out pagingLength);
            pagingStart = Convert.ToInt32(page.Get(PageKeys.Index));

            List<Tuple<string, string, string>> filterList = new List<Tuple<string, string, string>>();
            Tuple<string, string, string> productIdTuple = new Tuple<string, string, string>(FilterKeys.ProductId, FilterOperators.Equals, productId.ToString());
            filterList.Add(productIdTuple);

            string innerwhereClause = StringHelper.GenerateDynamicWhereClause(filterList);
            StoredProcedureHelper storedProcedureHelper = new StoredProcedureHelper();

            DataSet resultDataSet = storedProcedureHelper.SP_DataDetailsSpConfig(whereClause, null, StoredProcedureKeys.SpGetUnAssociatedCategoryByProductId, orderBy, pagingStart.ToString(), pagingLength.ToString(), out totalRowCount, null, innerwhereClause);
            model.Categories = resultDataSet.Tables[0].ToList<CategoryModel>().ToCollection();
            model.TotalResults = totalRowCount;
            model.PageSize = pagingLength;
            model.PageIndex = pagingStart;
            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                model.PageSize = model.TotalResults;
            }

            return model;
        }

        public bool AssociateProductCategory(CategoryModel model)
        {
            if (model.ProductId < 1)
            {
                throw new Exception("Product ID cannot be less than 1.");
            }

            ProductHelper productHelper = new ProductHelper();
            bool isExist = productHelper.AssociateProductCategory(model.ProductId, model.CategoryIds);

            if (isExist)
            {
                if (!string.IsNullOrEmpty(model.UserName))
                {

                    AccountInfoModel accountInforModel = new AccountInfoModel();
                    AccountHelper accountHelper = new AccountHelper();
                    DataSet accountDataSet = accountHelper.GetAccountInfo(model.UserName);
                    if (!Equals(accountDataSet.Tables[0], null) && accountDataSet.Tables[0].Rows.Count > 0)
                    {
                        accountInforModel = accountDataSet.Tables[0].Rows[0].ToSingleRow<AccountInfoModel>();
                        if (!Equals(accountInforModel, null) && (Equals(accountInforModel.RoleName, Constant.RoleFranchise)))
                        {
                            ProductAdmin productAdmin = new ProductAdmin();
                            Product product = _productRepository.GetByProductID(model.ProductId);
                            product.ReviewStateID = productAdmin.UpdateReviewStateID(accountInforModel.PortalId, product.ProductID, "Category Association Change", accountInforModel.AccountId, model.UserName);
                            productAdmin.Update(product);
                        }
                    }
                }
            }

            return isExist;
        }

        public bool UnAssociateProductCategory(CategoryModel model)
        {
            if (model.ProductId < 1)
            {
                throw new Exception("Product ID cannot be less than 1.");
            }

            var productHelper = new ProductHelper();
            var isExist = productHelper.UnAssociateProductCategory(model.ProductId, model.CategoryId);

            return isExist;
        }
        #endregion

        #region Product Sku

        public ProductSkuListModel GetProductSkuDetails(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {            
            ProductSkuListModel list = new ProductSkuListModel();
            string whereClause = StringHelper.GenerateDynamicWhereClause(filters);
            int totalRowCount = 0;
            var pagingStart = 0;
            var pagingLength = 0;
            string orderBy = StringHelper.GenerateDynamicOrderByClause(sorts);

            pagingStart = Convert.ToInt32(page.Get(PageKeys.Index));
            pagingLength = Convert.ToInt32(page.Get(PageKeys.Size));

            StoredProcedureHelper storedProcedureHelper = new StoredProcedureHelper();
            DataSet resultDataSet = storedProcedureHelper.SP_DataDetailsSpConfig(whereClause, string.Empty, StoredProcedureKeys.SpGetProductSKUDetails, orderBy, pagingStart.ToString(), pagingLength.ToString(), out totalRowCount);

            list.ProductSkus = resultDataSet.Tables[0].ToList<ProductSkuModel>().ToCollection();
            list.TotalResults = totalRowCount;
            list.PageSize = pagingLength;
            list.PageIndex = pagingStart;

            //PRFT CUstom Code : Start
            ZNodeSKU skuAttributes = new ZNodeSKU();
            foreach (var productSku in list.ProductSkus)
            {
                if (!String.IsNullOrEmpty(productSku.SKU))
                {
                    skuAttributes = ZNodeSKU.CreateBySKU(productSku.SKU, null);
                    productSku.AttributeDescription = skuAttributes.AttributesDescription;
                }
            }
            //PRFT CUstom Code : Start
            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                list.PageSize = list.TotalResults;
            }
            return list;
        }

        public bool AssociateSkuFacets(int? skuId, string associateFacetIds, string unassociateFacetIds)
        {
            if (Equals(skuId, null))
            {
                throw new Exception("Sku ID cannot be less than 1.");
            }

            var productHelper = new ProductHelper();
            var isExist = productHelper.AssociateSkuFacets(skuId, associateFacetIds, unassociateFacetIds);

            return isExist;
        }

        public bool DeleteSkuAssociatedFacets(int skuId, int facetGroupId)
        {
            if (Equals(skuId, null))
            {
                throw new Exception("Sku ID cannot be less than 1.");
            }

            var productHelper = new ProductHelper();
            var isExist = productHelper.DeleteSkuAssociatedFacets(skuId, facetGroupId);

            return isExist;
        }

        public ProductFacetModel GetSkuAssociatedFacets(int productId, int skuId, int facetGroupId, NameValueCollection expands)
        {
            ProductFacetModel model = new ProductFacetModel();
            ProductHelper productHelper = new ProductHelper();
            DataSet facetData = productHelper.GetSkuAssociatedFacets(productId, skuId, facetGroupId);
            model.FacetGroups = facetData.Tables[0].ToList<FacetGroupModel>().ToCollection();
            model.AssociatedFacets = facetData.Tables[1].ToList<FacetModel>().ToCollection();
            model.AssociatedFacetsSkus = facetData.Tables[2].ToList<FacetProductSKUModel>().ToCollection();
            return model;
        }

        public ProductFacetListModel GetSkuFacets(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            ProductFacetListModel list = new ProductFacetListModel();
            string whereClause = StringHelper.GenerateDynamicWhereClause(filters);
            int totalRowCount = 0;
            var pagingStart = 0;
            var pagingLength = 0;
            string orderBy = string.Empty;

            pagingStart = Convert.ToInt32(page.Get(PageKeys.Index));
            pagingLength = Convert.ToInt32(page.Get(PageKeys.Size));

            StoredProcedureHelper storedProcedureHelper = new StoredProcedureHelper();
            DataSet resultDataSet = storedProcedureHelper.SP_DataDetailsSpConfig(whereClause, null, StoredProcedureKeys.SpGetSKUFacetGroup, orderBy, pagingStart.ToString(), pagingLength.ToString(), out totalRowCount);

            list.FacetGroups = resultDataSet.Tables[0].ToList<FacetGroupModel>().ToCollection();
            list.TotalResults = totalRowCount;
            list.PageSize = pagingLength;
            list.PageIndex = pagingStart;
            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                list.PageSize = list.TotalResults;
            }

            return list;
        }

        #endregion

        #region Product Tags

        public ProductTagsModel GetProductTags(int productId, NameValueCollection expands)
        {
            var productTags = _tagsRepository.GetByProductID(productId);
            return ProductTagMap.ToModel(productTags);
        }

        public ProductTagsModel CreateProductTag(ProductTagsModel model)
        {
            if (Equals(model, null))
            {
                throw new Exception("Product Tag model cannot be null.");
            }

            var entity = ProductTagMap.ToEntity(model);
            var tags = _tagsRepository.Save(entity);
            return ProductTagMap.ToModel(tags);
        }

        public ProductTagsModel UpdateProductTag(int tagId, ProductTagsModel model)
        {
            if (tagId < 1)
            {
                throw new Exception("Tag ID cannot be less than 1.");
            }

            if (Equals(model, null))
            {
                throw new Exception("Product Tag model cannot be null.");
            }

            var productTags = _tagsRepository.GetByProductID(model.ProductId);
            if (!Equals(productTags, null))
            {
                // Set tagId and update details.
                model.TagId = productTags.FirstOrDefault().TagID;
                var tagsToUpdate = ProductTagMap.ToEntity(model);

                var updated = _tagsRepository.Update(tagsToUpdate);
                if (updated)
                {
                    var tags = _tagsRepository.GetByTagID(productTags.FirstOrDefault().TagID);
                    return ProductTagMap.ToModel(tags);
                }
            }
            return null;
        }

        public bool DeleteProductTag(int tagId)
        {
            if (tagId < 1)
            {
                throw new Exception("Tag ID cannot be less than 1.");
            }

            var tags = _tagsRepository.GetByTagID(tagId);
            if (!Equals(tags, null))
            {
                return _tagsRepository.Delete(tags);
            }
            return false;
        }

        #endregion

        #region Product Facets
        public ProductFacetListModel GetProductFacetsList(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            ProductFacetListModel list = new ProductFacetListModel();
            string whereClause = StringHelper.GenerateDynamicWhereClause(filters);
            int totalRowCount = 0;
            var pagingStart = 0;
            var pagingLength = 0;
            string orderBy = StringHelper.GenerateDynamicOrderByClause(sorts);

            pagingStart = Convert.ToInt32(page.Get(PageKeys.Index));
            pagingLength = Convert.ToInt32(page.Get(PageKeys.Size));

            StoredProcedureHelper storedProcedureHelper = new StoredProcedureHelper();
            DataSet resultDataSet = storedProcedureHelper.SP_DataDetailsSpConfig(whereClause, string.Empty/*HavingClause*/, "ZNodeGetProductFacetGroup", orderBy, pagingStart.ToString(), pagingLength.ToString(), out totalRowCount);

            list.FacetGroups = resultDataSet.Tables[0].ToList<FacetGroupModel>().ToCollection();
            list.TotalResults = totalRowCount;
            list.PageSize = pagingLength;
            list.PageIndex = pagingStart;
            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                list.PageSize = list.TotalResults;
            }

            return list;
        }

        public ProductFacetModel GetProductFacets(int productId, int facetGroupId, NameValueCollection expands)
        {
            ProductFacetModel model = new ProductFacetModel();
            ProductHelper productHelper = new ProductHelper();
            DataSet facetData = productHelper.GetProductFacetsDetails(productId, facetGroupId);

            model.FacetGroups = facetData.Tables[0].ToList<FacetGroupModel>().ToCollection();
            model.AssociatedFacets = facetData.Tables[1].ToList<FacetModel>().ToCollection();
            model.AssociatedFacetsSkus = facetData.Tables[2].ToList<FacetProductSKUModel>().ToCollection();
            return model;
        }

        public ProductFacetModel BindProductFacets(int productId, ProductFacetModel model)
        {
            if (productId < 1)
            {
                throw new Exception("Product ID cannot be less than 1.");
            }

            if (Equals(model, null))
            {
                throw new Exception("Product Facet model cannot be null.");
            }
            TList<FacetProductSKU> tpsList = _facetProductSKURepository.GetByProductID(productId);

            TList<Facet> facetList = _facetsRepository.GetByFacetGroupID(model.FacetGroupId);
            var selectedFacetIds = (Equals(model.FacetIds, null)) ? new List<int>() : model.FacetIds.Select(x => Int32.Parse(x)).ToList();

            foreach (var li in facetList)
            {
                if (selectedFacetIds.Contains(li.FacetID))
                {
                    if (tpsList.Find("FacetID", Convert.ToInt32(li.FacetID)) == null)
                    {
                        FacetProductSKU tps = new FacetProductSKU();
                        tps.FacetID = Convert.ToInt32(li.FacetID);
                        tps.ProductID = productId;
                        _facetProductSKURepository.Save(tps);
                    }
                }
                else
                {
                    FacetProductSKU tp;
                    if ((tp = tpsList.Find("FacetID", Convert.ToInt32(li.FacetID))) != null)
                    {
                        _facetProductSKURepository.Delete(tp);
                    }
                }
            }
            //TO DO, return the list of facets
            return new ProductFacetModel();
        }

        public bool DeleteProductAssociatedFacets(int productId, int facetGroupId)
        {
            bool status = false;
            FacetService service = new FacetService();
            TList<Facet> facetList = service.GetByFacetGroupID(facetGroupId);
            foreach (Facet facet in facetList)
            {
                FacetProductSKUService skuService = new FacetProductSKUService();
                TList<FacetProductSKU> tps = skuService.GetByFacetID(facet.FacetID);
                foreach (FacetProductSKU tp in tps)
                {
                    if (Equals(tp.ProductID, productId))
                    {
                        status = skuService.Delete(tp);
                    }
                }
            }
            return status;
        }

        #endregion

        #region Product Images
        public ProductImageTypeListModel GetProductImageTypes()
        {
            var model = new ProductImageTypeListModel();
            //Get all image types.
            ZNode.Libraries.Admin.ProductViewAdmin imageadmin = new ProductViewAdmin();
            var imageTypes = imageadmin.GetImageType();
            foreach (var p in imageTypes)
            {
                model.ProductImageTypes.Add(ProductImageTypeMap.ToModel(p));
            }
            return model;
        }

        public ProductAlternateImageModel InsertProductAlternateImage(ProductAlternateImageModel model)
        {
            if (Equals(model, null))
            {
                throw new Exception("Product Alternate Image model cannot be null.");
            }

            ProductImage entity = ProductAlternateImageMap.ToEntity(model);
            ProductImage productImage = _productImageRepository.Save(entity);

            if (productImage.ProductImageID > 0)
            {
                if (!string.IsNullOrEmpty(model.UserName))
                {
                    AccountInfoModel accountInforModel = new AccountInfoModel();
                    AccountHelper accountHelper = new AccountHelper();
                    DataSet accountDataSet = accountHelper.GetAccountInfo(model.UserName);
                    if (!Equals(accountDataSet.Tables[0], null) && accountDataSet.Tables[0].Rows.Count > 0)
                    {
                        accountInforModel = accountDataSet.Tables[0].Rows[0].ToSingleRow<AccountInfoModel>();
                        if (!Equals(accountInforModel, null) && (Equals(accountInforModel.RoleName, Constant.RoleFranchise)))
                        {
                            ProductAdmin productAdmin = new ProductAdmin();
                            Product product = _productRepository.GetByProductID(model.ProductID);

                            product.ReviewStateID = productAdmin.UpdateReviewStateID(accountInforModel.PortalId, product.ProductID, "Alternate / Swatch Image Changed", accountInforModel.AccountId, model.UserName);
                            productAdmin.Update(product);

                        }
                    }
                }
            }
            return ProductAlternateImageMap.ToModel(productImage);
        }

        public ProductAlternateImageModel UpdateProductAlternateImage(int productImageId, ProductAlternateImageModel model)
        {
            if (productImageId < 1)
            {
                throw new Exception("Product Image ID cannot be less than 1.");
            }

            if (Equals(model, null))
            {
                throw new Exception("Product Alternate Image model cannot be null.");
            }

            var productImages = _productImageRepository.GetByProductImageID(productImageId);
            if (!Equals(productImages, null))
            {
                // Set productImageId and update details.
                model.ProductImageID = productImages.ProductImageID;
                var imagesToUpdate = ProductAlternateImageMap.ToEntity(model);

                var updated = _productImageRepository.Update(imagesToUpdate);
                if (updated)
                {
                    if (!string.IsNullOrEmpty(model.UserName))
                    {

                        AccountInfoModel accountInforModel = new AccountInfoModel();
                        AccountHelper accountHelper = new AccountHelper();
                        DataSet accountDataSet = accountHelper.GetAccountInfo(model.UserName);
                        if (!Equals(accountDataSet.Tables[0], null) && accountDataSet.Tables[0].Rows.Count > 0)
                        {
                            accountInforModel = accountDataSet.Tables[0].Rows[0].ToSingleRow<AccountInfoModel>();
                            if (!Equals(accountInforModel, null) && (Equals(accountInforModel.RoleName, Constant.RoleFranchise)))
                            {
                                ProductAdmin productAdmin = new ProductAdmin();
                                var product = _productRepository.GetByProductID(model.ProductID);

                                product.ReviewStateID = productAdmin.UpdateReviewStateID(accountInforModel.PortalId, product.ProductID, "Alternate / Swatch Image Changed", accountInforModel.AccountId, model.UserName);
                                productAdmin.Update(product);

                            }
                        }
                    }

                    var images = _productImageRepository.GetByProductImageID(model.ProductImageID);
                    return ProductAlternateImageMap.ToModel(images);
                }
            }
            return new ProductAlternateImageModel();
        }

        public bool DeleteProductAlternateImage(int productImageId)
        {
            if (productImageId < 1)
            {
                throw new Exception("Product Image Id ID cannot be less than 1.");
            }

            var images = _productImageRepository.GetByProductImageID(productImageId);
            if (!Equals(images, null))
            {
                return _productImageRepository.Delete(images);
            }
            return false;
        }

        public ProductAlternateImageListModel GetAllProductAlternateImages(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new ProductAlternateImageListModel();
            var productImages = new TList<ProductImage>();
            var tempList = new TList<ProductImage>();

            var query = new ProductImageQuery { Junction = String.Empty };
            var sortBuilder = new ProductImageSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetAlternateImageFiltering(filters, query);
            SetAlternateImageSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            tempList = _productImageRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                model.PageSize = model.TotalResults;
            }

            // Now go through the list and get any expands
            foreach (var images in tempList)
            {
                productImages.Add(images);
            }

            // Map each item and add to the list
            foreach (var p in productImages)
            {
                model.ProductImages.Add(ProductAlternateImageMap.ToModel(p));
            }

            return model;
        }

        public ProductAlternateImageModel GetProductAlternateImageById(int productImageId, NameValueCollection expands)
        {
            var productImage = _productImageRepository.GetByProductImageID(productImageId);
            return ProductAlternateImageMap.ToModel(productImage);
        }

        private void SetAlternateImageFiltering(List<Tuple<string, string, string>> filters, ProductImageQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;
                if (Equals(filterKey, FilterKeys.ProductId)) SetQueryParameter(ProductImageColumn.ProductID, filterOperator, filterValue, query);
                if (Equals(filterKey, FilterKeys.ProductImageTypeId)) SetQueryParameter(ProductImageColumn.ProductImageTypeID, filterOperator, filterValue, query);
                if (Equals(filterKey, FilterKeys.ProductImageId)) SetQueryParameter(ProductImageColumn.ProductImageID, filterOperator, filterValue, query);
                if (Equals(filterKey, FilterKeys.Name)) SetQueryParameter(ProductImageColumn.Name, filterOperator, filterValue, query);
                if (Equals(filterKey, FilterKeys.DispalyOrder)) SetQueryParameter(ProductImageColumn.DisplayOrder, filterOperator, filterValue, query);
                if (Equals(filterKey, FilterKeys.ActiveInd)) SetQueryParameter(ProductImageColumn.ActiveInd, filterOperator, filterValue, query);
                if (Equals(filterKey, FilterKeys.ShowOnCategoryPage)) SetQueryParameter(ProductImageColumn.ShowOnCategoryPage, filterOperator, filterValue, query);
            }
        }

        private void SetAlternateImageSorting(NameValueCollection expands, ProductImageSortBuilder sortBuilder)
        {
            if (expands != null && expands.HasKeys())
            {
                foreach (var key in expands.AllKeys)
                {
                    var value = expands.Get(key);
                    if (value.Equals(SortKeys.DisplayOrder)) { SetSortDirection(ProductImageColumn.DisplayOrder, expands[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (value.Equals(SortKeys.Name)) { SetSortDirection(ProductImageColumn.Name, expands[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (value.Equals(SortKeys.ProductImageID)) { SetSortDirection(ProductImageColumn.ProductImageID, expands[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (value.Equals(SortKeys.ProductId)) { SetSortDirection(ProductImageColumn.ProductID, expands[StoredProcedureKeys.sortDirKey], sortBuilder); }
                }
            }
        }

        #endregion

        #region Product AddOn

        public AddOnListModel GetAssociatedAddOns(int productId, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            AddOnListModel list = new AddOnListModel();
            var sortBuilder = new AddOnSortBuilder();
            string whereClause = StringHelper.GenerateDynamicWhereClause(filters);

            int totalRowCount = 0;
            var pagingStart = 0;
            var pagingLength = 0;
            pagingStart = Convert.ToInt32(page.Get(PageKeys.Index));
            pagingLength = Convert.ToInt32(page.Get(PageKeys.Size));

            string orderBy = sortBuilder.ToString();
            string spName = StoredProcedureKeys.SPZNode_GetAssociatedAddOn;

            StoredProcedureHelper storedProcedureHelper = new StoredProcedureHelper();

            if (sorts.HasKeys())
            {
                var isVendor = sorts.GetValues(SortKeys.IsVendor);
                if (!Equals(isVendor, null))
                {
                    spName = StoredProcedureKeys.SPZNode_GetAssociatedAddOn_2;
                    sorts.Remove(SortKeys.IsVendor);
                }
            }
            SetSorting(sorts, sortBuilder);

            DataSet resultDataSet = storedProcedureHelper.SP_DataDetailsSpConfig(whereClause, string.Empty/*HavingClause*/, spName, orderBy, pagingStart.ToString(), pagingLength.ToString(), out totalRowCount);

            list.AddOns = resultDataSet.Tables[0].ToList<AddOnModel>().ToCollection();
            list.TotalResults = totalRowCount;
            list.PageSize = pagingLength;
            list.PageIndex = pagingStart;
            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                list.PageSize = list.TotalResults;
            }

            return list;
        }

        public bool RemoveProductAddOn(int productAddOnId)
        {
            if (productAddOnId < 1)
            {
                throw new Exception("ProductAddOnId cannot be less than 1.");
            }

            bool status = false;
            var entity = _addOnRepository.GetByProductAddOnID(productAddOnId);

            if (!Equals(entity, null))
            {
                status = _addOnRepository.Delete(entity);
            }
            return status;
        }

        public bool AssociateProductAddOn(List<AddOnModel> models)
        {
            if (Equals(models, null))
            {
                throw new Exception("Addon models cannot be null.");
            }

            ProductAddOnAdmin AdminAccess = new ProductAddOnAdmin();
            bool status = false;
            foreach (var item in models)
            {
                ProductAddOn productAddOnEntity = new ProductAddOn();
                productAddOnEntity.AddOnID = item.AddOnId;
                productAddOnEntity.ProductID = item.ProductId;

                if (AdminAccess.IsAddOnExists(productAddOnEntity))
                {
                    status = AdminAccess.AddNewProductAddOn(productAddOnEntity);
                }

                if (status && !Equals(models[0], null) && !string.IsNullOrEmpty(models[0].UserName))
                {
                    AccountInfoModel accountInforModel = new AccountInfoModel();
                    AccountHelper accountHelper = new AccountHelper();
                    DataSet accountDataSet = accountHelper.GetAccountInfo(models[0].UserName);
                    if (!Equals(accountDataSet.Tables[0], null) && accountDataSet.Tables[0].Rows.Count > 0)
                    {
                        accountInforModel = accountDataSet.Tables[0].Rows[0].ToSingleRow<AccountInfoModel>();
                        if (!Equals(accountInforModel, null) && (Equals(accountInforModel.RoleName, Constant.RoleFranchise) || Equals(accountInforModel.RoleName, Constant.RoleVendor)))
                        {
                            accountInforModel = accountDataSet.Tables[0].Rows[0].ToSingleRow<AccountInfoModel>();
                            ProductAdmin productAddon = new ProductAdmin();
                            Product product = productAddon.GetByProductId(models[0].ProductId);
                            product.ReviewStateID = productAddon.UpdateReviewStateID(GetAuthorizedPortalId(models[0].UserName), models[0].ProductId, "AddOns Changed", accountInforModel.AccountId, models[0].UserName);
                            productAddon.Update(product);
                        }
                    }
                }

            }
            return status;
        }

        public AddOnListModel GetUnassociatedAddOns(int productId, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page, int portalId = 0)
        {
            AddOnListModel list = new AddOnListModel();
            var sortBuilder = new AddOnSortBuilder();
            string whereClause = StringHelper.GenerateDynamicWhereClause(filters);
            string innerWhereClause = string.Format("~~ProductID~~={0}", productId);
            int totalRowCount = 0;
            var pagingStart = 0;
            var pagingLength = 0;
            pagingStart = Convert.ToInt32(page.Get(PageKeys.Index));
            pagingLength = Convert.ToInt32(page.Get(PageKeys.Size));
            string orderBy = StringHelper.GenerateDynamicOrderByClause(sorts);

            StoredProcedureHelper storedProcedureHelper = new StoredProcedureHelper();
            DataSet resultDataSet = storedProcedureHelper.SP_DataDetailsSpConfig(whereClause, string.Empty/*HavingClause*/, StoredProcedureKeys.SpZNodeGetAvailableAddOn, orderBy, pagingStart.ToString(), pagingLength.ToString(), out totalRowCount, null, innerWhereClause);
            list.AddOns = resultDataSet.Tables[0].ToList<AddOnModel>().ToCollection();
            list.TotalResults = totalRowCount;
            list.PageSize = pagingLength;
            list.PageIndex = pagingStart;
            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                list.PageSize = list.TotalResults;
            }

            return list;
        }

        private void SetSorting(NameValueCollection sorts, AddOnSortBuilder sortBuilder)
        {
            if (sorts.HasKeys())
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);
                    if (value.Equals(SortKeys.ProductAddonId)) { SetSortDirection(AddOnColumn.AddOnID, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (value.Equals(SortKeys.DisplayOrder)) { SetSortDirection(AddOnColumn.DisplayOrder, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (value.Equals(SortKeys.Name)) { SetSortDirection(AddOnColumn.Name, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (value.Equals(SortKeys.Title)) { SetSortDirection(AddOnColumn.Title, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                }
            }
        }

        #endregion

        #region Product Details
        public ProductModel CopyProduct(int productId)
        {
            ProductAdmin productAdmin = new ProductAdmin();
            if (productId < 1)
            {
                throw new Exception("Product Id cannot be less than 1.");
            }
            int copiedProductid = productAdmin.CopyProduct(productId);

            ProductModel copiedProduct = this.GetProductDetailsByProductId(copiedProductid);

            return copiedProduct;
        }


        public bool DeleteByProductId(int productId)
        {
            ProductAdmin productAdmin = new ProductAdmin();
            bool isProductDeleted = false;

            if (productId < 1)
            {
                throw new Exception("Product Id cannot be less than 1.");
            }
            isProductDeleted = productAdmin.DeleteByProductID(productId);

            if (!isProductDeleted)
            {
                throw new Exception("The product can not be deleted until all associated items are removed. Please ensure that this product does not contain cross-sell items, product images, promotions or skus. If it does, then delete these Items first.");
            }
            return isProductDeleted;
        }

        #endregion

        #region Product Highlights

        public HighlightListModel GetAssociatedHighlights(int productId, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            HighlightListModel list = new HighlightListModel();
            string whereClause = StringHelper.GenerateDynamicWhereClause(filters);
            int totalRowCount = 0;
            var pagingStart = 0;
            var pagingLength = 0;

            pagingStart = Convert.ToInt32(page.Get(PageKeys.Index));
            pagingLength = Convert.ToInt32(page.Get(PageKeys.Size));
            string orderBy = StringHelper.GenerateDynamicOrderByClause(sorts);

            StoredProcedureHelper storedProcedureHelper = new StoredProcedureHelper();
            DataSet resultDataSet = storedProcedureHelper.SP_DataDetailsSpConfig(whereClause, string.Empty/*HavingClause*/, StoredProcedureKeys.SpZNodeGetAssocitedHighLights, orderBy, pagingStart.ToString(), pagingLength.ToString(), out totalRowCount);

            list.Highlights = resultDataSet.Tables[0].ToList<HighlightModel>().ToCollection();
            SetSmallImagePathForHighlight(list);
            list.TotalResults = totalRowCount;
            list.PageSize = pagingLength;
            list.PageIndex = pagingStart;
            // Check to set page size equal to the total number of results
            if (pagingLength.Equals(int.MaxValue))
            {
                list.PageSize = list.TotalResults;
            }

            return list;
        }

        public bool RemoveProductHighlight(int productHighlightId)
        {
            if (productHighlightId < 1)
            {
                throw new Exception("ProductHighlightId cannot be less than 1.");
            }
            ProductAdmin productAdmin = new ProductAdmin();
            bool status = productAdmin.DeleteProductHighlight(productHighlightId);
            return status;
        }

        public bool AssociateProductHighlight(List<HighlightModel> models)
        {
            if (Equals(models, null))
            {
                throw new Exception("Highlight models cannot be null.");
            }

            ProductAdmin AdminAccess = new ProductAdmin();
            bool status = false;
            foreach (var item in models)
            {
                ProductHighlight productHighlight = new ProductHighlight();
                productHighlight.HighlightID = item.HighlightId;
                productHighlight.ProductID = item.ProductId;

                if (!AdminAccess.IsHighlightExists(item.ProductId, item.HighlightId))
                {
                    status = AdminAccess.AddProductHighlight(productHighlight);
                }
            }
            if (status && !Equals(models[0], null) && !string.IsNullOrEmpty(models[0].UserType) && Equals(models[0].UserType, Convert.ToString(UserTypes.FranchiseAdmin)))
            {
                AccountInfoModel accountInforModel = new AccountInfoModel();
                AccountHelper accountHelper = new AccountHelper();
                DataSet accountDataSet = accountHelper.GetAccountInfo(models[0].UserName);
                if (!Equals(accountDataSet.Tables[0], null) && accountDataSet.Tables[0].Rows.Count > 0)
                {
                    accountInforModel = accountDataSet.Tables[0].Rows[0].ToSingleRow<AccountInfoModel>();
                    if (!Equals(accountInforModel, null) && (Equals(accountInforModel.RoleName, Constant.RoleFranchise)))
                    {
                        ProductAdmin productTierAdmin = new ProductAdmin();
                        Product product = productTierAdmin.GetByProductId(models[0].ProductId);
                        product.ReviewStateID = productTierAdmin.UpdateReviewStateID(GetAuthorizedPortalId(models[0].UserName), models[0].ProductId, "Highlights Changed", accountInforModel.AccountId, models[0].UserName);
                        productTierAdmin.Update(product);
                    }
                }
            }
            return status;
        }

        public HighlightListModel GetUnassociatedHighlights(int productId, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page, int portalId = 0)
        {
            HighlightListModel list = new HighlightListModel();
            var sortBuilder = new HighlightSortBuilder();

            string whereClause = StringHelper.GenerateDynamicWhereClause(filters);
            string innerWhereClause = string.Format("~~ProductID~~={0}", productId);
            int totalRowCount = 0;
            var pagingStart = 0;
            var pagingLength = 0;
            string orderBy = StringHelper.GenerateDynamicOrderByClause(sorts);
            pagingStart = Convert.ToInt32(page.Get(PageKeys.Index));
            pagingLength = Convert.ToInt32(page.Get(PageKeys.Size));
            StoredProcedureHelper storedProcedureHelper = new StoredProcedureHelper();
            DataSet resultDataSet = storedProcedureHelper.SP_DataDetailsSpConfig(whereClause, string.Empty/*HavingClause*/, StoredProcedureKeys.SpZNodeGetAvailableHighlights, orderBy, pagingStart.ToString(), pagingLength.ToString(), out totalRowCount, null, innerWhereClause);

            list.Highlights = resultDataSet.Tables[0].ToList<HighlightModel>().ToCollection();
            list.TotalResults = totalRowCount;
            list.PageSize = pagingLength;
            list.PageIndex = pagingStart;
            // Check to set page size equal to the total number of results
            if (pagingLength.Equals(int.MaxValue))
            {
                list.PageSize = list.TotalResults;
            }

            return list;
        }

        #endregion

        #region Product Bundles
        public ProductBundlesListModel GetProductBundlesList(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            ProductBundlesListModel list = new ProductBundlesListModel();
            string whereClause = StringHelper.GenerateDynamicWhereClause(filters);
            int totalRowCount = 0;
            var pagingStart = 0;
            var pagingLength = 0;
            string orderBy = StringHelper.GenerateDynamicOrderByClause(sorts);

            pagingStart = Convert.ToInt32(page.Get(PageKeys.Index));
            pagingLength = Convert.ToInt32(page.Get(PageKeys.Size));

            StoredProcedureHelper storedProcedureHelper = new StoredProcedureHelper();
            DataSet resultDataSet = storedProcedureHelper.SP_DataDetailsSpConfig(whereClause, string.Empty/*HavingClause*/, StoredProcedureKeys.SPGetChildProduct, orderBy, pagingStart.ToString(), pagingLength.ToString(), out totalRowCount);

            list.ProductBundles = resultDataSet.Tables[0].ToList<ProductBundlesModel>().ToCollection();
            list.TotalResults = totalRowCount;
            list.PageSize = pagingLength;
            list.PageIndex = pagingStart;
            // Check to set page size equal to the total number of results
            if (pagingLength.Equals(int.MaxValue))
            {
                list.PageSize = list.TotalResults;
            }

            return list;
        }

        private void SetProductBundlesFiltering(List<Tuple<string, string, string>> filters, ParentChildProductQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;
                if (filterKey == FilterKeys.ProductId) SetQueryParameter(ParentChildProductColumn.ParentProductID, filterOperator, filterValue, query);
            }
        }

        public bool DeleteBundleProduct(int parentChildProductID)
        {
            if (parentChildProductID < 1)
            {
                throw new Exception("Parent Child Product ID cannot be less than 1.");
            }

            var productBundle = _productBundlesRepository.GetByParentChildProductID(parentChildProductID);
            if (!Equals(productBundle, null))
            {
                return _productBundlesRepository.Delete(productBundle);
            }
            return false;
        }

        public ProductListModel GetProductList(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            ProductListModel list = new ProductListModel();
            string whereClause = StringHelper.GenerateDynamicWhereClause(filters);
            int totalRowCount = 0;
            var pagingStart = 0;
            var pagingLength = 0;
            string orderBy = string.Empty;

            pagingStart = Convert.ToInt32(page.Get(PageKeys.Index));
            pagingLength = Convert.ToInt32(page.Get(PageKeys.Size));

            StoredProcedureHelper storedProcedureHelper = new StoredProcedureHelper();
            DataSet resultDataSet = storedProcedureHelper.SP_DataDetailsSpConfig(whereClause, string.Empty, StoredProcedureKeys.SpGetProductsList, orderBy, pagingStart.ToString(), pagingLength.ToString(), out totalRowCount);

            list.Products = resultDataSet.Tables[0].ToList<ProductModel>().ToCollection();
            list.TotalResults = totalRowCount;
            list.PageSize = pagingLength;
            list.PageIndex = pagingStart;
            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                list.PageSize = list.TotalResults;
            }

            return list;
        }

        public bool AssociateBundleProduct(ProductModel model)
        {
            bool status = false;
            if (model.ProductId < 1)
            {
                throw new Exception("Product ID cannot be less than 1.");
            }
            var existingProductList = _productBundlesRepository.GetByParentProductID(model.ProductId);

            List<int> childProductIds = model.ProductIds.Split(',').Select(int.Parse).ToList();
            foreach (int item in childProductIds)
            {
                var existingChildProduct = existingProductList.FindAll(x => Equals(x.ChildProductID, item)).Count;
                if (Equals(existingChildProduct, 0) && !Equals(item, model.ProductId))
                {
                    ParentChildProductAdmin _ParentChildProductAdmin = new ParentChildProductAdmin();

                    status = _ParentChildProductAdmin.Insert(model.ProductId, item);
                }
            }

            if (status && !string.IsNullOrEmpty(model.UserName))
            {

                AccountInfoModel accountInforModel = new AccountInfoModel();
                AccountHelper accountHelper = new AccountHelper();
                DataSet accountDataSet = accountHelper.GetAccountInfo(model.UserName);
                if (!Equals(accountDataSet.Tables[0], null) && accountDataSet.Tables[0].Rows.Count > 0)
                {
                    accountInforModel = accountDataSet.Tables[0].Rows[0].ToSingleRow<AccountInfoModel>();
                    if (!Equals(accountInforModel, null) && (Equals(accountInforModel.RoleName, Constant.RoleFranchise) || Equals(accountInforModel.RoleName, Constant.RoleVendor)))
                    {
                        ProductAdmin productAdmin = new ProductAdmin();
                        var product = _productRepository.GetByProductID(model.ProductId);

                        product.ReviewStateID = productAdmin.UpdateReviewStateID(accountInforModel.PortalId, product.ProductID, "You are trying to add same product(s) as bundle product.", accountInforModel.AccountId, model.UserName);
                        productAdmin.Update(product);
                        productAdmin = null;
                    }
                }
            }
            return status;
        }
        #endregion

        #region Category Associated Products
        public CategoryAssociatedProductsListModel GetAllProducts(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new CategoryAssociatedProductsListModel();
            Dictionary<string, string> StoredProc = new Dictionary<string, string>();

            string whereClause = StringHelper.GenerateDynamicWhereClause(filters);
            string orderBy = StringHelper.GenerateDynamicOrderByClause(sorts);
            var pagingStart = 0;
            var pagingLength = 0;
            var totalRowCount = 0;
            SetPaging(page, model, out pagingStart, out pagingLength);
            pagingStart = Convert.ToInt32(page.Get(PageKeys.Index));

            StoredProcedureHelper storedProcedureHelper = new StoredProcedureHelper();

            DataSet resultDataSet = storedProcedureHelper.SP_DataDetailsSpConfig(whereClause, null, StoredProcedureKeys.SpGetProductsList, orderBy, pagingStart.ToString(), pagingLength.ToString(), out totalRowCount);
            model.TotalResults = totalRowCount;
            model.PageSize = pagingLength;
            model.PageIndex = pagingStart;
            model.ProductList = resultDataSet.Tables[0].ToList<CategoryAssociatedProductsModel>().ToCollection();
            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                model.PageSize = model.TotalResults;
            }
            return model;
        }
        #endregion

        #region Private Method

        private void MapProductSettingToEntity(ProductModel model, Product entity)
        {
            if (!Equals(model, null) && !Equals(entity, null))
            {
                entity.TrackInventoryInd = model.TrackInventory;
                entity.AllowBackOrder = model.AllowBackOrder;
                entity.ActiveInd = model.IsActive;
                entity.HomepageSpecial = model.IsHomepageSpecial;
                entity.NewProductInd = model.IsNewProduct;
                entity.FeaturedInd = model.IsFeatured;
                entity.CallForPricing = model.CallForPricing;
                entity.Franchisable = model.IsFranchisable;
                entity.InStockMsg = model.InStockMessage;
                entity.OutOfStockMsg = model.OutOfStockMessage;
                entity.BackOrderMsg = model.BackOrderMessage;
                entity.RecurringBillingInd = model.AllowRecurringBilling;
                entity.RecurringBillingInstallmentInd = model.AllowRecurringBillingInstallment;
                entity.RecurringBillingInitialAmount = model.RecurringBillingInitialAmount;
                entity.RecurringBillingFrequency = model.RecurringBillingFrequency;
                entity.SEOTitle = model.SeoTitle;
                entity.SEOKeywords = model.SeoKeywords;
                entity.SEODescription = model.SeoDescription;
                entity.SEOURL = model.SeoUrl;
                entity.DropShipInd = model.AllowDropShip;
                entity.DropShipEmailID = model.DropShipEmailAddress;
            }
        }

        private void MapProductDetailsToEntity(ProductModel model, Product entity)
        {
            if (!Equals(model, null) && !Equals(entity, null))
            {
                entity.Name = model.Name;
                entity.ManufacturerID = model.ManufacturerId;
                entity.ProductNum = model.ProductNumber;
                entity.MinQty = model.MinSelectableQuantity;
                entity.MaxQty = model.MaxSelectableQuantity;
                entity.ProductTypeID = model.ProductTypeId;
                entity.SupplierID = model.SupplierId;
                entity.RetailPrice = model.RetailPrice;
                entity.SalePrice = model.SalePrice;
                entity.WholesalePrice = model.WholesalePrice;
                entity.TaxClassID = model.TaxClassId;
                entity.DownloadLink = model.DownloadLink;
                entity.DisplayOrder = model.DisplayOrder;
                entity.FreeShippingInd = model.AllowFreeShipping;
                entity.ShippingRate = model.ShippingRate;
                entity.Weight = model.Weight;
                entity.Height = model.Height;
                entity.Width = model.Width;
                entity.Length = model.Length;
                entity.ImageAltTag = model.ImageAltTag;
                entity.ImageFile = model.ImageFile;
                entity.ShortDescription = model.ShortDescription;
                entity.Description = model.Description;
                entity.FeaturesDesc = model.FeaturesDescription;
                entity.Specifications = model.Specifications;
                entity.AdditionalInformation = model.AdditionalInfo;
                entity.ReviewStateID = model.ReviewStateId;
                entity.ExpirationPeriod = Equals(model.ExpirationPeriod, null) ? null : model.ExpirationPeriod;
                entity.ExpirationFrequency = Equals(model.ExpirationFrequency, null) ? null : model.ExpirationFrequency;
                entity.ShippingRuleTypeID = Equals(model.ShippingRuleTypeId, null) ? null : model.ShippingRuleTypeId;
            }
        }

        #endregion

        #region Product SEO Information

        public ProductModel UpdateProductSEOInformation(int productId, ProductModel model)
        {
            if (productId < 1)
            {
                throw new Exception("Product ID cannot be less than 1.");
            }

            if (model == null)
            {
                throw new Exception("Product model cannot be null.");
            }

            var product = _productRepository.GetByProductID(productId);
            if (product != null)
            {
                // Set product ID and update date
                model.ProductId = productId;
                model.UpdateDate = DateTime.Now;

                string oldSeoUrl = product.SEOURL;

                var productToUpdate = ProductMap.ToEntity(model);
                var updated = _productRepository.Update(productToUpdate);
                if (updated)
                {
                    if (Equals(model.RedirectUrlInd, true))
                    {
                        string newSeoUrl = model.SeoUrl;
                        UrlRedirectAdminRepository _urlRedirectAdminRepository = new UrlRedirectAdminRepository();
                        _urlRedirectAdminRepository.AddUrlRedirectTableForMVCAdmin(SEOUrlType.Product, oldSeoUrl, newSeoUrl, model.ProductId.ToString());
                    }

                    return ProductMap.ToModel(product);
                }
            }
            return null;
        }
        #endregion

        public ProductListModel SearchProducts(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            ProductListModel list = new ProductListModel();
            var sortBuilder = new HighlightSortBuilder();

            string whereClause = StringHelper.GenerateDynamicWhereClause(filters);
            int totalRowCount = 0;
            var pagingStart = 0;
            var pagingLength = 0;
            string orderBy = StringHelper.GenerateDynamicOrderByClause(sorts);
            pagingStart = Convert.ToInt32(page.Get(PageKeys.Index));
            pagingLength = Convert.ToInt32(page.Get(PageKeys.Size));
            StoredProcedureHelper storedProcedureHelper = new StoredProcedureHelper();
            DataSet resultDataSet = storedProcedureHelper.SP_DataDetailsSpConfig(whereClause, string.Empty, StoredProcedureKeys.SpZNodeGetProductPriceListByPortalID, orderBy, pagingStart.ToString(), pagingLength.ToString(), out totalRowCount, null, null);
            list.Products = resultDataSet.Tables[0].ToList<ProductModel>().ToCollection();
            list.TotalResults = totalRowCount;
            list.PageSize = pagingLength;
            list.PageIndex = pagingStart;
            // Check to set page size equal to the total number of results
            if (pagingLength.Equals(int.MaxValue))
            {
                list.PageSize = list.TotalResults;
            }

            return list;
        }

        public ProductModel CreateOrderProduct(int productId)
        {
            ProductModel productModel = new ProductModel();
            ZNodeProduct product = ZnodeProductCatalogRepository.Create(productId, ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId, true, "");
            Dictionary<string, Dictionary<string, string>> productAttributes = new Dictionary<string, Dictionary<string, string>>();
            Dictionary<string, Dictionary<string, string>> productAddOns = new Dictionary<string, Dictionary<string, string>>();
            Dictionary<string, Dictionary<string, Dictionary<string, string>>> bundleProductDict = new Dictionary<string, Dictionary<string, Dictionary<string, string>>>();
            MapProductAttributeTypes(product, productAttributes);

            MapProductAddOnCollection(product, productAddOns);
            Dictionary<string, Dictionary<string, string>> bundleAttribute = new Dictionary<string, Dictionary<string, string>>();
            MapBundleProduct(product, bundleProductDict, bundleAttribute);
            // Local Variables
            ValidateAddons(product);

            productModel.ProductAttributes = productAttributes;
            productModel.ProductAddOns = productAddOns;
            productModel.BundleProductDict = bundleProductDict;
            decimal unitPrice = 0M;
            if (product.ProductID > 0)
            {
                unitPrice = product.FinalPrice + product.AddOnPrice;
            }

            if (product.ZNodeBundleProductCollection.Count > 0)
            {
                for (int idx = 0; idx < product.ZNodeBundleProductCollection.Count; idx++)
                {
                    unitPrice += product.ZNodeBundleProductCollection[idx].AddOnPrice;
                }
            }
            productModel.RetailPrice = unitPrice;
            productModel.ImageFile = product.ImageFile;
            productModel.Description = product.Description;
            productModel.Name = product.Name;
            productModel.MinSelectableQuantity = product.MinQty;
            productModel.InStockMessage = product.InStockMsg;
            productModel.ProductId = product.ProductID;
            return productModel;
        }

        private void MapBundleProduct(ZNodeProduct product, Dictionary<string, Dictionary<string, Dictionary<string, string>>> bundleProductDict, Dictionary<string, Dictionary<string, string>> bundleAttribute)
        {
            Dictionary<string, string> values = new Dictionary<string, string>();
            if (product.ZNodeBundleProductCollection.Count > 0)
            {
                foreach (ZNode.Libraries.ECommerce.Entities.ZNodeProductBaseEntity _bundleProduct in product.ZNodeBundleProductCollection)
                {
                    ZNodeProduct _znodeBundleProduct = ZNodeProduct.Create(_bundleProduct.ProductID);
                    if (_znodeBundleProduct.ZNodeAttributeTypeCollection.Count > 0)
                    {
                        foreach (ZNodeAttributeType BundleAttributeType in _znodeBundleProduct.ZNodeAttributeTypeCollection)
                        {
                            foreach (ZNodeAttribute item in BundleAttributeType.ZNodeAttributeCollection)
                            {
                                values.Add(item.Name, item.AttributeId.ToString());
                            }
                            bundleAttribute.Add(BundleAttributeType.Name, values);
                        }
                        bundleProductDict.Add(_bundleProduct.Name + "_BundleAttribute", bundleAttribute);
                    }
                    else
                    {
                        ZNodeSKU SKU = ZNodeSKU.CreateByProductDefault(_znodeBundleProduct.ProductID, "");
                        _znodeBundleProduct.SelectedSKU = SKU;
                    }

                    if (_znodeBundleProduct.ZNodeAddOnCollection.Count > 0)
                    {
                        values = new Dictionary<string, string>();
                        foreach (ZNodeAddOn BundleAddon in _znodeBundleProduct.ZNodeAddOnCollection)
                        {
                            // Don't display list box if there is no add-on values for AddOns
                            if (BundleAddon.ZNodeAddOnValueCollection.Count > 0)
                            {
                                foreach (ZNodeAddOnValue AddOnValue in BundleAddon.ZNodeAddOnValueCollection)
                                {
                                    string AddOnValueName = AddOnValue.Name;
                                    decimal decRetailPrice = AddOnValue.FinalPrice;

                                    if (decRetailPrice < 0)
                                    {
                                        // Price format
                                        string priceformat = "-" + ZNodeCurrencyManager.GetCurrencyPrefix() + "{0:0.00}" + ZNodeCurrencyManager.GetCurrencySuffix();
                                        AddOnValueName += " : " + String.Format(priceformat, System.Math.Abs(decRetailPrice)) + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
                                    }
                                    else if (decRetailPrice > 0)
                                    {
                                        AddOnValueName += " : " + decRetailPrice.ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
                                    }

                                    // Added Inventory Message with the BundleAddon Value Name in the dropdownlist
                                    AddOnValueName += " " + this.BindStatusMsg(BundleAddon, AddOnValue);
                                    AddOnValue.Name = AddOnValueName;
                                    values.Add(AddOnValue.Name, AddOnValue.AddOnValueID.ToString());
                                }

                            }
                            bundleAttribute.Add(BundleAddon.Name, values);
                        }
                        bundleProductDict.Add(_bundleProduct.Name + "_BundleAddon", bundleAttribute);
                    }
                }
            }
        }

        private static void ValidateAddons(ZNodeProduct product)
        {
            System.Text.StringBuilder _addonValues = new System.Text.StringBuilder();

            foreach (ZNodeAddOn AddOn in product.ZNodeAddOnCollection)
            {

                if (_addonValues.Length > 0)
                {
                    _addonValues.Append(",");
                }

                // Loop through the Add-on values for each Add-on
                foreach (ZNodeAddOnValue AddOnValue in AddOn.ZNodeAddOnValueCollection)
                {
                    // Optional Addons are not selected,then leave those addons 
                    // If optinal Addons are selected, it should add with the Selected item 
                    // Check for Selected Addon value 
                    if (AddOnValue.AddOnValueID.ToString() == AddOn.AddOnID.ToString())
                    {
                        // Add to Selected Addon list for this product
                        _addonValues.Append(AddOnValue.AddOnValueID.ToString());
                    }
                }

            }

            ZNodeAddOnList SelectedAddOn = new ZNodeAddOnList();

            if (!_addonValues.ToString().Contains("0"))
            {
                // Get a sku based on Add-ons selected
                SelectedAddOn = ZNodeAddOnList.CreateByProductAndAddOns(product.ProductID, _addonValues.ToString());

                SelectedAddOn.SelectedAddOnValueIds = _addonValues.ToString();
            }
            else
            {
                SelectedAddOn.SelectedAddOnValueIds = _addonValues.ToString();
            }


            // Set Selected Add-on 
            product.SelectedAddOns = SelectedAddOn;

            // Validate Bundle Addons
            foreach (ZNode.Libraries.ECommerce.Entities.ZNodeProductBaseEntity bundleProduct in product.ZNodeBundleProductCollection)
            {
                _addonValues = new System.Text.StringBuilder();
                ZNodeProduct znodeBundleProduct = ZNodeProduct.Create(bundleProduct.ProductID);

                if (znodeBundleProduct.ZNodeAddOnCollection.Count > 0)
                {
                    foreach (ZNodeAddOn BundleAddon in znodeBundleProduct.ZNodeAddOnCollection)
                    {
                        if (_addonValues.Length > 0)
                        {
                            _addonValues.Append(",");
                        }

                        // Loop through the Add-on values for each Add-on
                        foreach (ZNodeAddOnValue AddOnValue in BundleAddon.ZNodeAddOnValueCollection)
                        {
                            // Optional Addons are not selected,then leave those addons 
                            // If optinal Addons are selected, it should add with the Selected item 
                            // Check for Selected Addon value 
                            if (AddOnValue.AddOnValueID.ToString() == BundleAddon.AddOnID.ToString())
                            {
                                // Add to Selected Addon list for this product
                                _addonValues.Append(AddOnValue.AddOnValueID.ToString());
                            }
                        }

                    }

                    ZNodeAddOnList bundleSelectedAddOn = new ZNodeAddOnList();

                    if (!_addonValues.ToString().Contains("0"))
                    {
                        // Get a sku based on Add-ons selected
                        bundleSelectedAddOn = ZNodeAddOnList.CreateByProductAndAddOns(znodeBundleProduct.ProductID, _addonValues.ToString());

                        bundleSelectedAddOn.SelectedAddOnValueIds = _addonValues.ToString();
                    }
                    else
                    {
                        bundleSelectedAddOn.SelectedAddOnValueIds = _addonValues.ToString();
                    }

                    // Set Selected Add-on 
                    int idx = product.ZNodeBundleProductCollection.IndexOf((ZNode.Libraries.ECommerce.Entities.ZNodeBundleProductEntity)bundleProduct);
                    product.ZNodeBundleProductCollection[idx].SelectedAddOns = bundleSelectedAddOn;
                }
            }
        }

        private void MapProductAddOnCollection(ZNodeProduct product, Dictionary<string, Dictionary<string, string>> productAddOns)
        {
            Dictionary<string, string> values = new Dictionary<string, string>();
            if (product.ZNodeAddOnCollection.Count > 0)
            {
                foreach (ZNodeAddOn AddOn in product.ZNodeAddOnCollection)
                {

                    if (AddOn.ZNodeAddOnValueCollection.Count > 0)
                    {
                        foreach (ZNodeAddOnValue AddOnValue in AddOn.ZNodeAddOnValueCollection)
                        {
                            string AddOnValueName = AddOnValue.Name;
                            decimal decRetailPrice = AddOnValue.FinalPrice;

                            if (decRetailPrice < 0)
                            {
                                // Price format
                                string priceformat = "-" + ZNodeCurrencyManager.GetCurrencyPrefix() + "{0:0.00}" + ZNodeCurrencyManager.GetCurrencySuffix();
                                AddOnValueName += " : " + String.Format(priceformat, System.Math.Abs(decRetailPrice)) + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
                            }
                            else if (decRetailPrice > 0)
                            {
                                AddOnValueName += " : " + decRetailPrice.ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
                            }

                            // Added Inventory Message with the Addon Value Name in the dropdownlist
                            AddOnValueName += " " + this.BindStatusMsg(AddOn, AddOnValue);
                            AddOnValue.Name = AddOnValueName;
                            values.Add(AddOnValue.Name, AddOnValue.AddOnValueID.ToString());
                        }
                        productAddOns.Add(AddOn.Name, values);
                    }
                }
            }
        }

        private static void MapProductAttributeTypes(ZNodeProduct product, Dictionary<string, Dictionary<string, string>> productAttributes)
        {
            Dictionary<string, string> values = new Dictionary<string, string>();
            if (product.ZNodeAttributeTypeCollection.Count > 0)
            {
                foreach (ZNodeAttributeType AttributeType in product.ZNodeAttributeTypeCollection)
                {
                    foreach (ZNodeAttribute item in AttributeType.ZNodeAttributeCollection)
                    {
                        values.Add(item.Name, item.AttributeId.ToString());
                    }
                    productAttributes.Add(AttributeType.Name, values);
                }
            }
            else
            {
                ZNodeSKU SKU = ZNodeSKU.CreateByProductDefault(product.ProductID, "");
                product.SelectedSKU = SKU;
            }
        }

        /// <summary>
        /// Validate selected addons and returns the Inventory related messages
        /// </summary>
        /// <param name="addOn">The ZNodeAddon object</param>
        /// <param name="addOnValue">The ZNodeAddonValue object</param>
        /// <returns>Returs the Status Message</returns>
        private string BindStatusMsg(ZNodeAddOn addOn, ZNodeAddOnValue addOnValue)
        {
            // If quantity available is less and track inventory is enabled
            AddOnValueRepository avs = new AddOnValueRepository();
            addOnValue.QuantityOnHand = ProductAdmin.GetQuantity(avs.GetByAddOnValueID(addOnValue.AddOnValueID));
            if (addOnValue.QuantityOnHand <= 0 && addOn.AllowBackOrder == false && addOn.TrackInventoryInd)
            {
                return addOn.OutOfStockMsg;
            }
            else if (addOnValue.QuantityOnHand <= 0 && addOn.AllowBackOrder == true && addOn.TrackInventoryInd)
            {
                return addOn.BackOrderMsg;
            }
            else if (addOn.TrackInventoryInd && addOnValue.QuantityOnHand > 0)
            {
                return addOn.InStockMsg;
            }
            else if (addOn.AllowBackOrder == false && addOn.TrackInventoryInd == false)
            {
                // Don't track Inventory
                // Items can always be added to the cart,TrackInventory is disabled
                return addOn.InStockMsg;
            }

            return string.Empty;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// To check sku is exist
        /// </summary>
        /// <param name="sku">string sku</param>
        /// <param name="productId"> int productId</param>
        /// <returns>returns true/ false</returns>
        private bool IsSkuExist(string sku, int productId)
        {
            SkuAdminRepository _adminSku = new SkuAdminRepository();
            bool isSkuExist = _adminSku.IsSkuExist(sku, productId);
            return isSkuExist;
        }

        private decimal? GetFinalPrice(int taxClassId, decimal price)
        {
            var finalPrice = GetTaxedPrice(taxClassId, price);
            return finalPrice;
        }

        private void GetPricing(Product product, SKU sku)
        {
            // Do pricing calculations with selected SKU
            var retailPrice = GetRetailPrice(product, sku);
            var salePrice = GetSalePrice(product, sku);
            var wholesalePrice = GetWholesalePrice(product, sku);

            // Tiered pricing doesn't require a SKU
            var tieredPrice = GetTieredPrice(product);

            if (sku.RetailPriceOverride.HasValue)
            {
                sku.RetailPriceOverride = GetFinalPrice(product.TaxClassID.GetValueOrDefault(), sku.RetailPriceOverride.GetValueOrDefault());
            }

            if (sku.SalePriceOverride.HasValue)
            {
                sku.SalePriceOverride = GetFinalPrice(product.TaxClassID.GetValueOrDefault(), sku.SalePriceOverride.GetValueOrDefault());
            }

            // Once above calculations are done, product price can be calculated with SKU
            var productPrice = GetProductPrice(retailPrice, salePrice, wholesalePrice, tieredPrice, sku);

            // Now set product, final, and promotion price on the SKU
            sku.ProductPrice = productPrice;
            sku.FinalPrice = sku.ProductPrice; // GetFinalPrice(product.TaxClassID.GetValueOrDefault(), sku.ProductPrice.GetValueOrDefault());
            sku.PromotionPrice = GetPromotionPrice(product.ProductID, productPrice);

            // And finally get product price without SKU and set promotion and tiered price on the product
            productPrice = GetProductPrice(product.RetailPrice, product.SalePrice, product.WholesalePrice, tieredPrice);
            productPrice = GetFinalPrice(product.TaxClassID.GetValueOrDefault(), productPrice.GetValueOrDefault());
            product.PromotionPrice = GetPromotionPrice(product.ProductID, productPrice);
            product.TieredPrice = tieredPrice;

            if (!product.SKUCollection.Any())
            {
                product.RetailPrice = retailPrice;
                product.SalePrice = salePrice;
            }
        }

        private decimal? GetProductPrice(decimal? productRetailPrice, decimal? productSalePrice, decimal? productWholesalePrice, decimal? productTieredPrice)
        {
            var useWholesalePrice = UseWholesalePrice();

            var basePrice = productRetailPrice;

            if (productSalePrice.HasValue && !useWholesalePrice)
            {
                basePrice = productSalePrice.Value;
            }
            else
            {
                if (productWholesalePrice.HasValue && useWholesalePrice)
                {
                    basePrice = productWholesalePrice.Value;
                }
                else if (productSalePrice.HasValue)
                {
                    basePrice = productSalePrice.Value;
                }
            }

            // Check tiered price
            return productTieredPrice > 0 ? productTieredPrice : basePrice;
        }

        private decimal? GetProductPrice(decimal? productRetailPrice, decimal? productSalePrice, decimal? productWholesalePrice, decimal? productTieredPrice, SKU sku)
        {
            var useWholesalePrice = UseWholesalePrice();

            // Base/retail price
            var basePrice = productRetailPrice;
            var retailPriceOverride = sku.RetailPriceOverride;
            if (retailPriceOverride.HasValue && retailPriceOverride.Value >= 0)
            {
                basePrice = retailPriceOverride.Value;
            }

            // Sale price
            var salePrice = productSalePrice;
            var salePriceOverride = sku.SalePriceOverride;
            if (salePriceOverride.HasValue && salePriceOverride.Value >= 0)
            {
                salePrice = salePriceOverride.Value;
            }

            // Wholesale price
            var wholesalePrice = productWholesalePrice;
            var wholesalePriceOverride = sku.WholesalePriceOverride;
            if (wholesalePriceOverride.HasValue && wholesalePriceOverride.Value >= 0)
            {
                wholesalePrice = wholesalePriceOverride.Value;
            }

            // Compare sale price, wholesale price, and negotiated price
            // commented CBP price calculation 
            sku.NegotiatedPrice = null;//GetSkuNegotiatedPrice(sku);
            if (!salePrice.HasValue && !useWholesalePrice && sku.NegotiatedPrice.HasValue)
            {
                salePrice = sku.NegotiatedPrice.GetValueOrDefault();
                basePrice = salePrice.Value;
            }
            else if (salePrice.HasValue && !useWholesalePrice)
            {
                basePrice = salePrice.Value;
            }
            else
            {
                if (useWholesalePrice && wholesalePrice.HasValue)
                {
                    if (sku.NegotiatedPrice.HasValue)
                    {
                        salePrice = sku.NegotiatedPrice.GetValueOrDefault();
                        basePrice = salePrice.GetValueOrDefault();
                    }
                    else
                    {
                        basePrice = wholesalePrice.Value;
                    }
                }
                else if (salePrice.HasValue)
                {
                    basePrice = salePrice.Value;
                }
            }

            // Check tiered price
            return productTieredPrice > 0 ? productTieredPrice : basePrice;
        }

        private decimal? GetPromotionPrice(int productId, decimal? productPrice)
        {
            if (productPrice.HasValue)
            {
                var pricePromoManager = new ZnodePricePromotionManager();
                return pricePromoManager.PromotionalPrice(productId, productPrice.Value);
            }

            return null;
        }

        private decimal? GetRetailPrice(Product product, SKU sku)
        {
            var retailPrice = product.RetailPrice;
            var retailPriceOverride = sku.RetailPriceOverride;

            if (retailPriceOverride.HasValue && retailPriceOverride.Value >= 0)
            {
                retailPrice = retailPriceOverride.Value;
            }

            if (retailPrice.HasValue)
            {
                retailPrice = GetTaxedPrice(product.TaxClassID.GetValueOrDefault(), retailPrice.Value);
            }

            return retailPrice;
        }

        private decimal? GetSalePrice(Product product, SKU sku)
        {
            var salePrice = product.SalePrice;
            var salePriceOverride = sku.SalePriceOverride;

            if (salePriceOverride.HasValue)
            {
                salePrice = salePriceOverride.Value;
            }

            if (!salePrice.HasValue)
            {
                salePrice = GetSkuNegotiatedPrice(sku);
            }

            if (salePrice.HasValue)
            {
                salePrice = GetTaxedPrice(product.TaxClassID.GetValueOrDefault(), salePrice.Value);
            }

            return salePrice;
        }

        private decimal? GetSkuNegotiatedPrice(SKU sku)
        {
            if (Equals(sku.ExternalID, null))
            {
                return null;
            }

            var query = new CustomerPricingQuery();
            query.AppendEquals(CustomerPricingColumn.SKUExternalID, sku.ExternalID);

            var customerPricing = _customerPricingRepository.Find(query);
            if (customerPricing != null && customerPricing.Count > 0)
            {
                return customerPricing[0].NegotiatedPrice;
            }

            return null;
        }

        private decimal? GetTaxedPrice(int taxClassId, decimal price)
        {
            // We're leaving address null so that underneath it defaults to the shipping address on the account
            var tax = new ZnodeInclusiveTax();
            return tax.GetInclusivePrice(taxClassId, price, null);
        }

        private decimal? GetTieredPrice(Product product)
        {
            var profileId = 0;
            var tieredPrice = 0.0m;

            // Get product tiers
            var tiers = _tierRepository.GetByProductID(product.ProductID);
            if (tiers != null && tiers.Count > 0)
            {
                // Get profile
                if (HttpContext.Current.Session["ProfileCache"] != null)
                {
                    var profile = (Profile)HttpContext.Current.Session["ProfileCache"];
                    if (profile != null)
                    {
                        profileId = profile.ProfileID;
                    }
                }

                // Go through each tier to set the price
                foreach (var tier in tiers)
                {
                    if (1 >= tier.TierStart && tier.TierEnd >= 1)
                    {
                        if (tier.ProfileID == 0 || tier.ProfileID == profileId)
                        {
                            tieredPrice = tier.Price;
                            break;
                        }
                    }
                }
            }

            return tieredPrice;
        }

        private decimal? GetWholesalePrice(Product product, SKU sku)
        {
            var wholesalePrice = product.WholesalePrice;
            var wholesalePriceOverride = sku.WholesalePriceOverride;

            if (wholesalePriceOverride.HasValue && wholesalePriceOverride.Value >= 0)
            {
                wholesalePrice = wholesalePriceOverride.Value;
            }

            if (wholesalePrice.HasValue)
            {
                wholesalePrice = GetTaxedPrice(product.TaxClassID.GetValueOrDefault(), wholesalePrice.Value);
            }

            return wholesalePrice;
        }

        private bool UseWholesalePrice()
        {
            if (HttpContext.Current.Session["ProfileCache"] != null)
            {
                var profile = (Profile)HttpContext.Current.Session["ProfileCache"];
                if (profile != null)
                {
                    return profile.UseWholesalePricing.GetValueOrDefault(false);
                }
            }

            return false;
        }

        private void GetExpands(NameValueCollection expands, Product product)
        {
            if (expands != null && expands.HasKeys())
            {
                ExpandAddOns(expands, product);
                ExpandAttributes(expands, product);
                ExpandBundleItems(expands, product);
                ExpandCategories(expands, product);
                ExpandCrossSells(expands, product);
                ExpandFrequentlyBoughtTogether(expands, product);
                ExpandHighlights(expands, product);
                ExpandImages(expands, product);
                ExpandManufacturer(expands, product);
                ExpandProductType(expands, product);
                ExpandPromotions(expands, product);
                ExpandReviews(expands, product);
                ExpandSkus(expands, product);
                ExpandTiers(expands, product);
                ExpandYouMayAlsoLike(expands, product);
                ExpandProductTags(expands, product);
            }
        }

        private void ExpandAddOns(NameValueCollection expands, Product product)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.AddOns)))
            {
                var productAddOnList = _addOnRepository.GetByProductID(product.ProductID);
                foreach (var productAddOn in productAddOnList)
                {
                    var productAddOnLoaded = _addOnRepository.DeepLoadByProductIDAddOnID(productAddOn.ProductID, productAddOn.AddOnID, true, DeepLoadType.IncludeChildren,
                        new[] { typeof(AddOn) });

                    var addOnValueList = _addOnValueRepository.GetByAddOnID(productAddOn.AddOnID);
                    foreach (AddOnValue addOnValue in addOnValueList)
                    {
                        var addOnValueLoaded = _addOnValueRepository.DeepLoadByAddOnValueID(addOnValue.AddOnValueID, true, DeepLoadType.IncludeChildren, new[] { typeof(SKUInventory) });
                        addOnValueLoaded.Inventory = _skuInventoryRepository.GetBySKU(addOnValueLoaded.SKU);
                        productAddOnLoaded.AddOnIDSource.AddOnValueCollection.Add(addOnValueLoaded);
                    }

                    product.ProductAddOnCollection.Add(productAddOnLoaded);
                }
            }
        }

        private void ExpandAttributes(NameValueCollection expands, Product product)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Attributes)))
            {
                // First get the SKUs
                var skus = _skuRepository.GetByProductID(product.ProductID);
                foreach (var s in skus)
                {
                    // Then get the attributes
                    var skuAttribs = _skuAttributeRepository.GetBySKUID(s.SKUID);
                    foreach (var sa in skuAttribs)
                    {
                        var prodAttrib = _productAttributeRepository.GetByAttributeId(sa.AttributeId);
                        if (!Equals(product.Attributes, null))
                        {
                            if (!Equals(prodAttrib, null) && !product.Attributes.Contains(prodAttrib))
                            {
                                // Go ahead and expand the attribute type as well
                                var attribType = _attributeTypeRepository.GetByAttributeTypeId(prodAttrib.AttributeTypeId);
                                if (!Equals(attribType, null))
                                {
                                    prodAttrib.AttributeType = attribType;
                                }

                                product.Attributes.Add(prodAttrib);
                            }
                        }
                    }
                }
            }
        }

        private void ExpandAttributes(NameValueCollection expands, SKU sku)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Attributes)))
            {
                var skuAttribs = _skuAttributeRepository.GetBySKUID(sku.SKUID);
                foreach (var sa in skuAttribs)
                {
                    var prodAttrib = _productAttributeRepository.GetByAttributeId(sa.AttributeId);
                    var item = new SKUAttribute
                    {
                        AttributeId = sa.AttributeId,
                        AttributeIdSource = prodAttrib,
                        SKUAttributeID = sa.SKUAttributeID,
                        SKUID = sku.SKUID,
                        SKUIDSource = sku
                    };

                    sku.SKUAttributeCollection.Add(item);
                }
            }
        }

        private void ExpandCustomerPricing(NameValueCollection expands, SKU customerPricingSku)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Skus)))
            {
                TList<CustomerPricing> customerPricing = _customerPricingRepository.GetBySKUExternalID(customerPricingSku.ExternalID);
                foreach (var sa in customerPricing)
                {
                    var item = new CustomerPricing
                    {
                        CustomerPricingID = sa.CustomerPricingID,
                        ExternalAccountNo = sa.ExternalAccountNo,
                        NegotiatedPrice = sa.NegotiatedPrice,
                        SKUExternalID = sa.SKUExternalID,

                    };
                    customerPricingSku.CustomerPricing.Add(item);
                }
            }
        }

        private void ExpandBundleItems(NameValueCollection expands, Product product)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.BundleItems)))
            {
                var productBundleList = _parentChildProductRepository.GetByParentProductID(product.ProductID);

                product.ParentChildProductCollectionGetByChildProductID = productBundleList;
            }
        }

        private void ExpandCategories(NameValueCollection expands, Product product)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Categories)))
            {
                var categories = _categoryRepository.DeepLoadByProductID(product.ProductID, true, DeepLoadType.IncludeChildren, new[] { typeof(Category) });
                foreach (var c in categories)
                {
                    product.ProductCategoryCollection.Add(c);
                }
            }
        }

        private void ExpandCrossSells(NameValueCollection expands, Product product)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.CrossSells)))
            {
                var crossSells = _crossSellRepository.GetByProductId(product.ProductID);
                foreach (var c in crossSells)
                {
                    product.ProductCrossSellCollection.Add(c);
                }
            }
        }

        private void ExpandFrequentlyBoughtTogether(NameValueCollection expands, Product product)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.FrequentlyBoughtTogether)))
            {
                var crossSells = _crossSellRepository.GetByProductIdRelationTypeId(product.ProductID, 4);
                foreach (var c in crossSells)
                {
                    product.ProductFrquentlyBoughtTogetherCollection.Add(c);
                }
            }
        }

        private void ExpandHighlights(NameValueCollection expands, Product product)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Highlights)))
            {
                var highlights = _highlightRepository.DeepLoadByProductID(product.ProductID, true, DeepLoadType.IncludeChildren, new[] { typeof(Highlight) });
                foreach (var h in highlights)
                {
                    product.ProductHighlightCollection.Add(h);
                }
            }
        }

        private void ExpandImages(NameValueCollection expands, Product product)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Images)))
            {
                var images = _imageRepository.GetByProductID(product.ProductID);
                foreach (var i in images)
                {
                    product.ProductImageCollection.Add(i);
                }
            }
        }

        private void ExpandManufacturer(NameValueCollection expands, Product product)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Manufacturer)))
            {
                if (product.ManufacturerID.HasValue)
                {
                    var manufacturer = _manufacturerRepository.GetByManufacturerID(product.ManufacturerID.Value);
                    if (manufacturer != null)
                    {
                        product.ManufacturerIDSource = manufacturer;
                    }
                }
            }
        }

        private void ExpandProductType(NameValueCollection expands, Product product)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.ProductType)))
            {
                var type = _productTypeRepository.GetByProductTypeId(product.ProductTypeID);
                if (type != null)
                {
                    product.ProductTypeIDSource = type;
                }
            }
        }

        private void ExpandPromotions(NameValueCollection expands, Product product)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Promotions)))
            {
                var promos = _promotionRepository.GetByProductID(product.ProductID);
                foreach (var p in promos)
                {
                    product.PromotionCollection.Add(p);
                }
            }
        }

        private void ExpandReviews(NameValueCollection expands, Product product)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Reviews)))
            {
                var reviews = _reviewRepository.GetByProductID(product.ProductID);
                foreach (var r in reviews)
                {
                    product.ReviewCollection.Add(r);
                }
            }
        }

        private void ExpandSkus(NameValueCollection expands, Product product)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Skus)))
            {
                var skus = _skuRepository.GetByProductID(product.ProductID);
                foreach (var s in skus)
                {
                    ExpandAttributes(expands, s);
                    ExpandCustomerPricing(expands, s);
                    // Calculate prices and add to list
                    GetPricing(product, s);
                    product.SKUCollection.Add(s);
                }
            }
        }

        private void ExpandTiers(NameValueCollection expands, Product product)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Tiers)))
            {
                var tiers = _tierRepository.GetByProductID(product.ProductID);
                foreach (var t in tiers)
                {
                    product.ProductTierCollection.Add(t);
                }
            }
        }

        private void ExpandYouMayAlsoLike(NameValueCollection expands, Product product)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.YouMayAlsoLike)))
            {
                var crossSells = _crossSellRepository.GetByProductIdRelationTypeId(product.ProductID, 5);
                foreach (var c in crossSells)
                {
                    product.ProductYouMayAlsoLikeCollection.Add(c);
                }
            }
        }

        private void ExpandProductTags(NameValueCollection expands, Product product)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.ProductTags)))
            {
                TagsService tagService = new TagsService();
                TList<ZNode.Libraries.DataAccess.Entities.Tags> tagList = tagService.GetByProductID(product.ProductID);
                foreach (var c in tagList)
                {
                    product.TagsCollection.Add(c);
                }
            }
        }

        private void SetFiltering(List<Tuple<string, string, string>> filters, ProductQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;
                if (String.Equals(filterKey, "producttypeid") || String.Equals(filterKey, "manufacturerid"))
                {
                    filterOperator = "eq";
                }

                if (filterKey == FilterKeys.AccountId) SetQueryParameter(ProductColumn.AccountID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.ExternalId) SetQueryParameter(ProductColumn.ExternalID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.IsActive) SetQueryParameter(ProductColumn.ActiveInd, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.IsFeatured) SetQueryParameter(ProductColumn.FeaturedInd, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.IsNew) SetQueryParameter(ProductColumn.NewProductInd, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.ManufacturerId) SetQueryParameter(ProductColumn.ManufacturerID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.Name) SetQueryParameter(ProductColumn.Name, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.ProductNum) SetQueryParameter(ProductColumn.ProductNum, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.PortalId) SetQueryParameter(ProductColumn.PortalID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.ProductTypeId) SetQueryParameter(ProductColumn.ProductTypeID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.RetailPrice) SetQueryParameter(ProductColumn.RetailPrice, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.SalePrice) SetQueryParameter(ProductColumn.SalePrice, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.ShippingRuleTypeId) SetQueryParameter(ProductColumn.ShippingRuleTypeID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.SupplierId) SetQueryParameter(ProductColumn.SupplierID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.TaxClassId) SetQueryParameter(ProductColumn.TaxClassID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.WholesalePrice) SetQueryParameter(ProductColumn.WholesalePrice, filterOperator, filterValue, query);
            }
        }

        private void SetFiltering(List<Tuple<string, string, string>> filters, VwZnodeProductsCatalogsQuery query)
        {
            if (filters != null)
            {
                foreach (var tuple in filters)
                {
                    var filterKey = tuple.Item1;
                    var filterOperator = tuple.Item2;
                    var filterValue = tuple.Item3;

                    if (filterKey == FilterKeys.AccountId) SetQueryParameter(VwZnodeProductsCatalogsColumn.AccountID, filterOperator, filterValue, query);
                    if (filterKey == FilterKeys.ExternalId) SetQueryParameter(VwZnodeProductsCatalogsColumn.ExternalID, filterOperator, filterValue, query);
                    if (filterKey == FilterKeys.IsActive) SetQueryParameter(VwZnodeProductsCatalogsColumn.ActiveInd, filterOperator, filterValue, query);
                    if (filterKey == FilterKeys.IsFeatured) SetQueryParameter(VwZnodeProductsCatalogsColumn.FeaturedInd, filterOperator, filterValue, query);
                    if (filterKey == FilterKeys.IsNew) SetQueryParameter(VwZnodeProductsCatalogsColumn.NewProductInd, filterOperator, filterValue, query);
                    if (filterKey == FilterKeys.ManufacturerId) SetQueryParameter(VwZnodeProductsCatalogsColumn.ManufacturerID, filterOperator, filterValue, query);
                    if (filterKey == FilterKeys.Name) SetQueryParameter(VwZnodeProductsCatalogsColumn.Name, filterOperator, filterValue, query);
                    if (filterKey == FilterKeys.Number) SetQueryParameter(VwZnodeProductsCatalogsColumn.ProductNum, filterOperator, filterValue, query);
                    if (filterKey == FilterKeys.PortalId) SetQueryParameter(VwZnodeProductsCatalogsColumn.PortalID, filterOperator, filterValue, query);
                    if (filterKey == FilterKeys.ProductTypeId) SetQueryParameter(VwZnodeProductsCatalogsColumn.ProductTypeID, filterOperator, filterValue, query);
                    if (filterKey == FilterKeys.RetailPrice) SetQueryParameter(VwZnodeProductsCatalogsColumn.RetailPrice, filterOperator, filterValue, query);
                    if (filterKey == FilterKeys.SalePrice) SetQueryParameter(VwZnodeProductsCatalogsColumn.SalePrice, filterOperator, filterValue, query);
                    if (filterKey == FilterKeys.ShippingRuleTypeId) SetQueryParameter(VwZnodeProductsCatalogsColumn.ShippingRuleTypeID, filterOperator, filterValue, query);
                    if (filterKey == FilterKeys.SupplierId) SetQueryParameter(VwZnodeProductsCatalogsColumn.SupplierID, filterOperator, filterValue, query);
                    if (filterKey == FilterKeys.TaxClassId) SetQueryParameter(VwZnodeProductsCatalogsColumn.TaxClassID, filterOperator, filterValue, query);
                    if (filterKey == FilterKeys.WholesalePrice) SetQueryParameter(VwZnodeProductsCatalogsColumn.WholesalePrice, filterOperator, filterValue, query);
                }
            }
        }

        private void SetFiltering(List<Tuple<string, string, string>> filters, VwZnodeProductsCategoriesQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;

                if (filterKey == FilterKeys.AccountId) SetQueryParameter(VwZnodeProductsCategoriesColumn.AccountID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.ExternalId) SetQueryParameter(VwZnodeProductsCategoriesColumn.ExternalID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.IsActive) SetQueryParameter(VwZnodeProductsCategoriesColumn.ActiveInd, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.IsFeatured) SetQueryParameter(VwZnodeProductsCategoriesColumn.FeaturedInd, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.IsNew) SetQueryParameter(VwZnodeProductsCategoriesColumn.NewProductInd, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.ManufacturerId) SetQueryParameter(VwZnodeProductsCategoriesColumn.ManufacturerID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.Name) SetQueryParameter(VwZnodeProductsCategoriesColumn.Name, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.Number) SetQueryParameter(VwZnodeProductsCategoriesColumn.ProductNum, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.PortalId) SetQueryParameter(VwZnodeProductsCategoriesColumn.PortalID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.ProductTypeId) SetQueryParameter(VwZnodeProductsCategoriesColumn.ProductTypeID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.RetailPrice) SetQueryParameter(VwZnodeProductsCategoriesColumn.RetailPrice, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.SalePrice) SetQueryParameter(VwZnodeProductsCategoriesColumn.SalePrice, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.ShippingRuleTypeId) SetQueryParameter(VwZnodeProductsCategoriesColumn.ShippingRuleTypeID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.SupplierId) SetQueryParameter(VwZnodeProductsCategoriesColumn.SupplierID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.TaxClassId) SetQueryParameter(VwZnodeProductsCategoriesColumn.TaxClassID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.WholesalePrice) SetQueryParameter(VwZnodeProductsCategoriesColumn.WholesalePrice, filterOperator, filterValue, query);
            }
        }

        private void SetFiltering(List<Tuple<string, string, string>> filters, VwZnodeProductsCategoriesPromotionsQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;

                if (filterKey == FilterKeys.AccountId) SetQueryParameter(VwZnodeProductsCategoriesPromotionsColumn.AccountID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.ExternalId) SetQueryParameter(VwZnodeProductsCategoriesPromotionsColumn.ExternalID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.IsActive) SetQueryParameter(VwZnodeProductsCategoriesPromotionsColumn.ActiveInd, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.IsFeatured) SetQueryParameter(VwZnodeProductsCategoriesPromotionsColumn.FeaturedInd, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.IsNew) SetQueryParameter(VwZnodeProductsCategoriesPromotionsColumn.NewProductInd, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.ManufacturerId) SetQueryParameter(VwZnodeProductsCategoriesPromotionsColumn.ManufacturerID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.Name) SetQueryParameter(VwZnodeProductsCategoriesPromotionsColumn.Name, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.Number) SetQueryParameter(VwZnodeProductsCategoriesPromotionsColumn.ProductNum, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.PortalId) SetQueryParameter(VwZnodeProductsCategoriesPromotionsColumn.PortalID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.ProductTypeId) SetQueryParameter(VwZnodeProductsCategoriesPromotionsColumn.ProductTypeID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.RetailPrice) SetQueryParameter(VwZnodeProductsCategoriesPromotionsColumn.RetailPrice, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.SalePrice) SetQueryParameter(VwZnodeProductsCategoriesPromotionsColumn.SalePrice, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.ShippingRuleTypeId) SetQueryParameter(VwZnodeProductsCategoriesPromotionsColumn.ShippingRuleTypeID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.SupplierId) SetQueryParameter(VwZnodeProductsCategoriesPromotionsColumn.SupplierID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.TaxClassId) SetQueryParameter(VwZnodeProductsCategoriesPromotionsColumn.TaxClassID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.WholesalePrice) SetQueryParameter(VwZnodeProductsCategoriesPromotionsColumn.WholesalePrice, filterOperator, filterValue, query);
            }
        }

        private void SetFiltering(List<Tuple<string, string, string>> filters, VwZnodeProductsPortalsQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;

                if (filterKey == FilterKeys.AccountId) SetQueryParameter(VwZnodeProductsPortalsColumn.AccountID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.ExternalId) SetQueryParameter(VwZnodeProductsPortalsColumn.ExternalID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.IsActive) SetQueryParameter(VwZnodeProductsPortalsColumn.ActiveInd, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.IsFeatured) SetQueryParameter(VwZnodeProductsPortalsColumn.FeaturedInd, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.IsNew) SetQueryParameter(VwZnodeProductsPortalsColumn.NewProductInd, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.ManufacturerId) SetQueryParameter(VwZnodeProductsPortalsColumn.ManufacturerID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.Name) SetQueryParameter(VwZnodeProductsPortalsColumn.Name, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.Number) SetQueryParameter(VwZnodeProductsPortalsColumn.ProductNum, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.PortalId) SetQueryParameter(VwZnodeProductsPortalsColumn.PortalID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.ProductTypeId) SetQueryParameter(VwZnodeProductsPortalsColumn.ProductTypeID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.RetailPrice) SetQueryParameter(VwZnodeProductsPortalsColumn.RetailPrice, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.SalePrice) SetQueryParameter(VwZnodeProductsPortalsColumn.SalePrice, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.ShippingRuleTypeId) SetQueryParameter(VwZnodeProductsPortalsColumn.ShippingRuleTypeID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.SupplierId) SetQueryParameter(VwZnodeProductsPortalsColumn.SupplierID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.TaxClassId) SetQueryParameter(VwZnodeProductsPortalsColumn.TaxClassID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.WholesalePrice) SetQueryParameter(VwZnodeProductsPortalsColumn.WholesalePrice, filterOperator, filterValue, query);
            }
        }

        private void SetFiltering(List<Tuple<string, string, string>> filters, VwZnodeProductsPromotionsQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;

                if (filterKey == FilterKeys.AccountId) SetQueryParameter(VwZnodeProductsPromotionsColumn.AccountID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.ExternalId) SetQueryParameter(VwZnodeProductsPromotionsColumn.ExternalID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.IsActive) SetQueryParameter(VwZnodeProductsPromotionsColumn.ActiveInd, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.IsFeatured) SetQueryParameter(VwZnodeProductsPromotionsColumn.FeaturedInd, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.IsNew) SetQueryParameter(VwZnodeProductsPromotionsColumn.NewProductInd, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.ManufacturerId) SetQueryParameter(VwZnodeProductsPromotionsColumn.ManufacturerID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.Name) SetQueryParameter(VwZnodeProductsPromotionsColumn.Name, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.Number) SetQueryParameter(VwZnodeProductsPromotionsColumn.ProductNum, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.PortalId) SetQueryParameter(VwZnodeProductsPromotionsColumn.PortalID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.ProductTypeId) SetQueryParameter(VwZnodeProductsPromotionsColumn.ProductTypeID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.RetailPrice) SetQueryParameter(VwZnodeProductsPromotionsColumn.RetailPrice, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.SalePrice) SetQueryParameter(VwZnodeProductsPromotionsColumn.SalePrice, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.ShippingRuleTypeId) SetQueryParameter(VwZnodeProductsPromotionsColumn.ShippingRuleTypeID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.SupplierId) SetQueryParameter(VwZnodeProductsPromotionsColumn.SupplierID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.TaxClassId) SetQueryParameter(VwZnodeProductsPromotionsColumn.TaxClassID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.WholesalePrice) SetQueryParameter(VwZnodeProductsPromotionsColumn.WholesalePrice, filterOperator, filterValue, query);
            }
        }

        private void SetSorting(NameValueCollection expands, ProductSortBuilder sortBuilder)
        {
            if (expands != null && expands.HasKeys())
            {
                foreach (var key in expands.AllKeys)
                {
                    var value = expands.Get(key);

                    if (key == SortKeys.DisplayOrder) SetSortDirection(ProductColumn.DisplayOrder, value, sortBuilder);
                    if (key == SortKeys.Name) SetSortDirection(ProductColumn.Name, value, sortBuilder);
                    if (key == SortKeys.Number) SetSortDirection(ProductColumn.ProductNum, value, sortBuilder);
                    if (key == SortKeys.ProductId) SetSortDirection(ProductColumn.ProductID, value, sortBuilder);
                    if (key == SortKeys.RetailPrice) SetSortDirection(ProductColumn.RetailPrice, value, sortBuilder);
                    if (key == SortKeys.SalePrice) SetSortDirection(ProductColumn.SalePrice, value, sortBuilder);
                    if (key == SortKeys.WholesalePrice) SetSortDirection(ProductColumn.WholesalePrice, value, sortBuilder);
                }
            }
        }

        private void SetSorting(NameValueCollection expands, VwZnodeProductsCatalogsSortBuilder sortBuilder)
        {
            if (expands != null && expands.HasKeys())
            {
                foreach (var key in expands.AllKeys)
                {
                    var value = expands.Get(key);

                    if (key == SortKeys.DisplayOrder) SetSortDirection(VwZnodeProductsCatalogsColumn.DisplayOrder, value, sortBuilder);
                    if (key == SortKeys.Name) SetSortDirection(VwZnodeProductsCatalogsColumn.Name, value, sortBuilder);
                    if (key == SortKeys.Number) SetSortDirection(VwZnodeProductsCatalogsColumn.ProductNum, value, sortBuilder);
                    if (key == SortKeys.ProductId) SetSortDirection(VwZnodeProductsCatalogsColumn.ProductID, value, sortBuilder);
                    if (key == SortKeys.RetailPrice) SetSortDirection(VwZnodeProductsCatalogsColumn.RetailPrice, value, sortBuilder);
                    if (key == SortKeys.SalePrice) SetSortDirection(VwZnodeProductsCatalogsColumn.SalePrice, value, sortBuilder);
                    if (key == SortKeys.WholesalePrice) SetSortDirection(VwZnodeProductsCatalogsColumn.WholesalePrice, value, sortBuilder);
                }
            }
        }

        private void SetSorting(NameValueCollection sorts, VwZnodeProductsCategoriesSortBuilder sortBuilder)
        {
            if (sorts.HasKeys())
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);
                    if (value.Equals(SortKeys.ProductId)) { SetSortDirection(VwZnodeProductsCategoriesColumn.ProductID, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (value.Equals(SortKeys.Name)) { SetSortDirection(VwZnodeProductsCategoriesColumn.Name, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (value.Equals(SortKeys.Number)) { SetSortDirection(VwZnodeProductsCategoriesColumn.ProductNum, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (value.Equals(SortKeys.RetailPrice)) { SetSortDirection(VwZnodeProductsCategoriesColumn.RetailPrice, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (value.Equals(SortKeys.SalePrice)) { SetSortDirection(VwZnodeProductsCategoriesColumn.SalePrice, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (value.Equals(SortKeys.WholesalePrice)) { SetSortDirection(VwZnodeProductsCategoriesColumn.WholesalePrice, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (value.Equals(SortKeys.DisplayOrder)) { SetSortDirection(VwZnodeProductsCategoriesColumn.DisplayOrder, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                }
            }
        }

        private void SetSorting(NameValueCollection expands, VwZnodeProductsCategoriesPromotionsSortBuilder sortBuilder)
        {
            if (expands != null && expands.HasKeys())
            {
                foreach (var key in expands.AllKeys)
                {
                    var value = expands.Get(key);

                    if (key == SortKeys.DisplayOrder) SetSortDirection(VwZnodeProductsCategoriesPromotionsColumn.DisplayOrder, value, sortBuilder);
                    if (key == SortKeys.Name) SetSortDirection(VwZnodeProductsCategoriesPromotionsColumn.Name, value, sortBuilder);
                    if (key == SortKeys.Number) SetSortDirection(VwZnodeProductsCategoriesPromotionsColumn.ProductNum, value, sortBuilder);
                    if (key == SortKeys.ProductId) SetSortDirection(VwZnodeProductsCategoriesPromotionsColumn.ProductID, value, sortBuilder);
                    if (key == SortKeys.RetailPrice) SetSortDirection(VwZnodeProductsCategoriesPromotionsColumn.RetailPrice, value, sortBuilder);
                    if (key == SortKeys.SalePrice) SetSortDirection(VwZnodeProductsCategoriesPromotionsColumn.SalePrice, value, sortBuilder);
                    if (key == SortKeys.WholesalePrice) SetSortDirection(VwZnodeProductsCategoriesPromotionsColumn.WholesalePrice, value, sortBuilder);
                }
            }
        }

        private void SetSorting(NameValueCollection expands, VwZnodeProductsPromotionsSortBuilder sortBuilder)
        {
            if (expands != null && expands.HasKeys())
            {
                foreach (var key in expands.AllKeys)
                {
                    var value = expands.Get(key);

                    if (key == SortKeys.DisplayOrder) SetSortDirection(VwZnodeProductsPromotionsColumn.DisplayOrder, value, sortBuilder);
                    if (key == SortKeys.Name) SetSortDirection(VwZnodeProductsPromotionsColumn.Name, value, sortBuilder);
                    if (key == SortKeys.Number) SetSortDirection(VwZnodeProductsPromotionsColumn.ProductNum, value, sortBuilder);
                    if (key == SortKeys.ProductId) SetSortDirection(VwZnodeProductsPromotionsColumn.ProductID, value, sortBuilder);
                    if (key == SortKeys.RetailPrice) SetSortDirection(VwZnodeProductsPromotionsColumn.RetailPrice, value, sortBuilder);
                    if (key == SortKeys.SalePrice) SetSortDirection(VwZnodeProductsPromotionsColumn.SalePrice, value, sortBuilder);
                    if (key == SortKeys.WholesalePrice) SetSortDirection(VwZnodeProductsPromotionsColumn.WholesalePrice, value, sortBuilder);
                }
            }
        }


        private void SetQueryParameter(ProductColumn column, string filterOperator, string filterValue, ProductQuery query)
        {
            base.SetQueryParameter(column, filterOperator, filterValue, query);
        }

        private void SetQueryParameter(VwZnodeProductsPortalsColumn column, string filterOperator, string filterValue, VwZnodeProductsPortalsQuery query)
        {
            base.SetQueryParameter(column, filterOperator, filterValue, query);
        }

        private void SetSortDirection(ProductColumn column, string value, ProductSortBuilder sortBuilder)
        {
            base.SetSortDirection(column, value, sortBuilder);
        }

        private void SendMailByVendorOrFranchise(int productId, string productName, AccountInfoModel model, string shortDescription)
        {
            if (Equals(model.RoleName, Constant.RoleVendor))
            {
                this.SendMailByVendor(productId, productName, shortDescription, model.AccountId);
            }
            else if (Equals(model.RoleName, Constant.RoleFranchise))
            {
                SendMailByFranchiseAdmin(productId, productName, shortDescription, model.AccountId);
            }
        }

        /// <summary>
        /// This function will return vendor name for sending mail to vendor.
        /// </summary>
        /// <returns>vendorName for mail notification</returns>      
        private string GetUserFullName(int accountID)
        {
            string vendorName = string.Empty;
            AccountService accountService = new AccountService();
            if (accountID > 0)
            {
                ZNode.Libraries.DataAccess.Service.AddressService addressService = new ZNode.Libraries.DataAccess.Service.AddressService();
                TList<Address> addressList = addressService.GetByAccountID(accountID);
                if (!addressList.Equals(null) && addressList.Count > 0)
                {
                    vendorName = addressList[0].FirstName + " " + addressList[0].LastName;
                }
            }
            return vendorName;
        }

        /// <summary>
        /// This function will return vendor EmailId for sending mail to vendor.
        /// </summary>
        /// <returns>EmailId for mail notification</returns>
        private string GetVendorEmailId(int productId)
        {
            string vendorEmailId = string.Empty;
            ZNode.Libraries.DataAccess.Service.SupplierService supplierService = new ZNode.Libraries.DataAccess.Service.SupplierService();
            ZNode.Libraries.DataAccess.Service.ProductService productService = new ZNode.Libraries.DataAccess.Service.ProductService();
            Product product = productService.GetByProductID(productId);

            if (!product.Equals(null) && !product.SupplierID.Equals(null) && product.SupplierID > 0)
            {
                vendorEmailId = supplierService.GetBySupplierID(int.Parse(product.SupplierID.ToString())).NotificationEmailID;
            }

            return vendorEmailId;
        }

        /// <summary>
        /// This function will send  mail notification to site admin.
        /// For approval of new added product by Mall Admin.
        /// </summary>
        /// <param name="productId">string productId to send productId in email</param>
        /// <param name="productName">string productName to send productName in email</param>
        private void SendMailByVendor(int productId, string productName, string shortDescription, int accountId)
        {
            try
            {
                //Get vendor Name
                string vendorName = GetUserFullName(accountId);

                string smtpServer = ZNodeConfigManager.SiteConfig.SMTPServer;

                //Get Vendor EmailId
                string senderEmail = GetVendorEmailId(productId);

                //Subject is set from global resources of admin.
                string subject = productName + " " + HttpContext.GetGlobalResourceObject("CommonCaption", "SubjectProductAddedByVendor");
                string receiverEmail = ZNodeConfigManager.SiteConfig.AdminEmail;
                string currentCulture = string.Empty;
                string templatePath = string.Empty;
                string defaultTemplatePath = string.Empty;
                string shortDesc = shortDescription;

                // Template selection
                currentCulture = ZNodeCatalogManager.CultureInfo;
                defaultTemplatePath = HttpContext.Current.Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, "ProductAddedByVendor.html"));

                templatePath = (!string.IsNullOrEmpty(currentCulture))
                    ? HttpContext.Current.Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "ProductAddedByVendor_" + currentCulture + ".html")
                    : HttpContext.Current.Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, "ProductAddedByVendor.html"));

                // Check the default template file existence.
                if (!File.Exists(defaultTemplatePath))
                {
                    return;
                }
                else
                {
                    // If language specific template not available then load default template.
                    templatePath = defaultTemplatePath;
                }

                StreamReader rw = new StreamReader(templatePath);
                string messageText = rw.ReadToEnd();

                Regex rx1 = new Regex("#PRODUCTID#", RegexOptions.IgnoreCase);
                messageText = rx1.Replace(messageText, Convert.ToString(productId));

                Regex rx2 = new Regex("#PRODUCTNAME#", RegexOptions.IgnoreCase);
                messageText = rx2.Replace(messageText, productName);

                Regex rx3 = new Regex("#SHORTDESCRIPTION#", RegexOptions.IgnoreCase);
                messageText = rx3.Replace(messageText, shortDesc);

                Regex rx4 = new Regex("#VENDORNAME#", RegexOptions.IgnoreCase);
                messageText = rx4.Replace(messageText, vendorName);

                senderEmail = senderEmail.Replace(",", ", ");
                Regex rx5 = new Regex("#VENDOREMAIL#", RegexOptions.IgnoreCase);
                messageText = rx5.Replace(messageText, senderEmail);

                Regex RegularExpressionLogo = new Regex("#LOGO#", RegexOptions.IgnoreCase);
                messageText = RegularExpressionLogo.Replace(messageText, ZNodeConfigManager.SiteConfig.StoreName);

                try
                {
                    // Send mail
                    ZNode.Libraries.Framework.Business.ZNodeEmail.SendEmail(receiverEmail, senderEmail, string.Empty, subject, messageText, true);
                }
                catch (Exception ex)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.GeneralMessage, receiverEmail, HttpContext.Current.Request.UserHostAddress.ToString(), null, ex.Message, null);
                }
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.GeneralMessage, "", HttpContext.Current.Request.UserHostAddress.ToString(), null, ex.Message, null);
            }
        }

        /// <summary>
        /// This function will send notification mail to site admin.
        /// For approval of new added product by Franchise Admin.
        /// </summary>
        /// <param name="productId">string productId to send productId in email</param>
        /// <param name="productName">string productName to send productName in email</param>
        private void SendMailByFranchiseAdmin(int productId, string productName, string shortDescription, int accountId)
        {
            try
            {
                //Get franchise Name
                string franchiseName = GetUserFullName(accountId);

                string smtpServer = ZNodeConfigManager.SiteConfig.SMTPServer;

                //Get franchise Email
                string senderEmail = GetFranchiseEmail(accountId);

                //Subject is set from global resources of admin.
                string subject = productName + " " + HttpContext.GetGlobalResourceObject("CommonCaption", "SubjectProductAddedByVendor");
                string receiverEmail = ZNodeConfigManager.SiteConfig.AdminEmail;
                string currentCulture = string.Empty;
                string templatePath = string.Empty;
                string defaultTemplatePath = string.Empty;
                string shortDesc = shortDescription;

                // Template selection
                currentCulture = ZNodeCatalogManager.CultureInfo;
                defaultTemplatePath = HttpContext.Current.Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, "ProductsAddedByFranchisee.html"));

                templatePath = (!string.IsNullOrEmpty(currentCulture))
                    ? HttpContext.Current.Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "ProductsAddedByFranchisee_" + currentCulture + ".html")
                    : HttpContext.Current.Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, "ProductsAddedByFranchisee.html"));

                // Check the default template file existence.
                if (!File.Exists(defaultTemplatePath))
                {
                    return;
                }
                else
                {
                    // If language specific template not available then load default template.
                    templatePath = defaultTemplatePath;
                }

                StreamReader rw = new StreamReader(templatePath);
                string messageText = rw.ReadToEnd();

                Regex rx1 = new Regex("#PRODUCTID#", RegexOptions.IgnoreCase);
                messageText = rx1.Replace(messageText, Convert.ToString(productId));

                Regex rx2 = new Regex("#PRODUCTNAME#", RegexOptions.IgnoreCase);
                messageText = rx2.Replace(messageText, productName);

                Regex rx3 = new Regex("#SHORTDESCRIPTION#", RegexOptions.IgnoreCase);
                messageText = rx3.Replace(messageText, shortDesc);

                Regex rx4 = new Regex("#FRANCHISEENAME#", RegexOptions.IgnoreCase);
                messageText = rx4.Replace(messageText, franchiseName);

                senderEmail = senderEmail.Replace(",", ", ");
                Regex rx5 = new Regex("#FRANCHISEEMAIL#", RegexOptions.IgnoreCase);
                messageText = rx5.Replace(messageText, senderEmail);

                Regex RegularExpressionLogo = new Regex("#LOGO#", RegexOptions.IgnoreCase);
                messageText = RegularExpressionLogo.Replace(messageText, ZNodeConfigManager.SiteConfig.StoreName);

                try
                {
                    // Send mail
                    ZNode.Libraries.Framework.Business.ZNodeEmail.SendEmail(receiverEmail, senderEmail, string.Empty, subject, messageText, true);
                }
                catch (Exception ex)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.GeneralMessage, receiverEmail, HttpContext.Current.Request.UserHostAddress.ToString(), null, ex.Message, null);
                }
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.GeneralError, "", HttpContext.Current.Request.UserHostAddress.ToString(), null, ex.Message, null);
            }
        }

        /// <summary>
        /// Znode version 7.2.2 
        /// This function will return franchise Email id for sending mail to Site Admin.
        /// </summary>
        /// <returns>franchiseEmail for mail notification</returns>
        private string GetFranchiseEmail(int accountID)
        {
            string franchiseEmail = string.Empty;
            if (accountID > 0)
            {
                ZNode.Libraries.DataAccess.Service.AccountService accountService = new ZNode.Libraries.DataAccess.Service.AccountService();
                Account account = accountService.GetByAccountID(accountID);
                franchiseEmail = account.Email;
            }
            return franchiseEmail;
        }
        #endregion

        #region Get Product List & Search method
        public ProductListModel GetProductsList(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            ProductListModel list = new ProductListModel();
            List<Tuple<string, string, string>> filterList = new List<Tuple<string, string, string>>();
            string innerWhereClause = string.Empty;
            if (!Equals(filters, null))
                foreach (var item in filters)
                {
                    if (Equals(item.Item1, "username"))
                    {
                        filterList.Add(item);
                        innerWhereClause = StringHelper.GenerateDynamicWhereClause(filterList);
                        filters.Remove(item);
                        break;
                    }
                }
            string whereClause = StringHelper.GenerateDynamicWhereClause(filters);
            int totalRowCount = 0;
            var pagingStart = 0;
            var pagingLength = 0;
            string orderBy = (Equals(sorts.AllKeys[0], string.Empty) || Equals(sorts.AllKeys[0], null)) ? StoredProcedureKeys.defaultSortKey : StringHelper.GenerateDynamicOrderByClause(sorts);

            pagingStart = Convert.ToInt32(page.Get(PageKeys.Index));
            pagingLength = Convert.ToInt32(page.Get(PageKeys.Size));

            StoredProcedureHelper storedProcedureHelper = new StoredProcedureHelper();
            DataSet resultDataSet = storedProcedureHelper.SP_DataDetailsSpConfig(whereClause, string.Empty, StoredProcedureKeys.SpGetProductsList, orderBy, pagingStart.ToString(), pagingLength.ToString(), out totalRowCount, null, innerWhereClause);

            list.Products = resultDataSet.Tables[0].ToList<ProductModel>().ToCollection();
            list.TotalResults = totalRowCount;
            list.PageSize = pagingLength;
            list.PageIndex = pagingStart;
            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                list.PageSize = list.TotalResults;
            }


            return list;
        }
        #endregion

        #region Product Tiers

        public TierListModel GetProductTiers(int productId, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            TierListModel list = new TierListModel();
            string whereClause = StringHelper.GenerateDynamicWhereClause(filters);

            int totalRowCount = 0;
            var pagingStart = 1;
            var pagingLength = 0;
            string orderBy = StringHelper.GenerateDynamicOrderByClause(sorts);

            pagingLength = Convert.ToInt32(page.Get(PageKeys.Size));

            StoredProcedureHelper storedProcedureHelper = new StoredProcedureHelper();
            DataSet resultDataSet = storedProcedureHelper.SP_DataDetailsSpConfig(whereClause, string.Empty/*HavingClause*/, "ZnodeProductTier", orderBy, pagingStart.ToString(), pagingLength.ToString(), out totalRowCount);

            list.Tiers = resultDataSet.Tables[0].ToList<TierModel>().ToCollection();
            list.TotalResults = totalRowCount;
            list.PageSize = pagingLength;
            list.PageIndex = pagingStart;
            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                list.PageSize = list.TotalResults;
            }

            return list;
        }

        public ProductTierPricingModel CreateProductTier(ProductTierPricingModel model)
        {

            if (model == null)
            {
                throw new Exception("ProductTier model cannot be null.");
            }

            if (this.IsTieredPricingExists(model))
            {
                throw new Exception("Could not update the product Tiered pricing. Please try again.");
            }
            if (!string.IsNullOrEmpty(model.UserType) && Equals(model.UserType, Convert.ToString(UserTypes.FranchiseAdmin)))
            {
                model.ProfileID = GetTurnkeyStoreProfileID(model.UserName);
            }
            var entity = ProductMap.ToProductTierEntity(model);
            ProductTierService productTierService = new ProductTierService();
            var productTier = productTierService.Save(entity);


            if (!string.IsNullOrEmpty(model.UserName) && !Equals(productTier, null) && productTier.ProductTierID > 0)
            {
                AccountInfoModel accountInforModel = new AccountInfoModel();
                AccountHelper accountHelper = new AccountHelper();
                DataSet accountDataSet = accountHelper.GetAccountInfo(model.UserName);
                if (!Equals(accountDataSet.Tables[0], null) && accountDataSet.Tables[0].Rows.Count > 0)
                {
                    accountInforModel = accountDataSet.Tables[0].Rows[0].ToSingleRow<AccountInfoModel>();
                    if (!Equals(accountInforModel, null) && (Equals(accountInforModel.RoleName, Constant.RoleFranchise)))
                    {
                        ProductAdmin productTierAdmin = new ProductAdmin();
                        Product product = productTierAdmin.GetByProductId(model.ProductId);
                        product.ReviewStateID = productTierAdmin.UpdateReviewStateID(GetAuthorizedPortalId(model.UserName), model.ProductId, "Tiered Pricing", accountInforModel.AccountId, model.UserName);
                        productTierAdmin.Update(product);
                    }
                }

            }

            return ProductMap.ToProductTierModel(productTier);
        }

        /// <summary>
        /// To check whether the tiered pricing already exists for this product
        /// </summary>
        /// <param name="productTier">ProductTier instance</param>
        /// <returns>Returns a bool value Tiered Pricing exists or not</returns>
        private bool IsTieredPricingExists(ProductTierPricingModel productTier)
        {
            TList<ProductTier> list;
            ProductTierService productTierService = new ProductTierService();
            ProductTierQuery query = new ProductTierQuery();
            query.Append(ProductTierColumn.ProductID, productTier.ProductId.ToString());
            if (productTier.ProductTierID > 0)
            {
                query.AppendNotEquals(ProductTierColumn.ProductTierID, productTier.ProductTierID.ToString());
            }

            TList<ProductTier> Mainlist = productTierService.Find(query.GetParameters());

            if (productTier.ProfileID.HasValue)
            {
                list = Mainlist.FindAll(delegate(ProductTier p) { return p.ProfileID == null || p.ProfileID == productTier.ProfileID; });
            }
            else
            {
                list = Mainlist;
            }

            if (list.Count > 0)
            {
                foreach (ProductTier listitem in list)
                {
                    if ((productTier.TierStart >= listitem.TierStart) && (productTier.TierStart <= listitem.TierEnd))
                    {
                        return true;
                    }

                    if ((productTier.TierEnd >= listitem.TierStart) && (productTier.TierEnd <= listitem.TierEnd))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public bool UpdateProductTier(ProductTierPricingModel model)
        {
            if (model == null)
            {
                throw new Exception("Producttier model cannot be null.");
            }
            if (!string.IsNullOrEmpty(model.UserType) && Equals(model.UserType, Convert.ToString(UserTypes.FranchiseAdmin)))
            {
                model.ProfileID = GetTurnkeyStoreProfileID(model.UserName);
            }
            var entity = ProductMap.ToProductTierEntity(model);
            ProductTierService productTierService = new ProductTierService();
            var isUpdated = productTierService.Update(entity);
            if (!string.IsNullOrEmpty(model.UserName) && isUpdated)
            {
                AccountInfoModel accountInforModel = new AccountInfoModel();
                AccountHelper accountHelper = new AccountHelper();
                DataSet accountDataSet = accountHelper.GetAccountInfo(model.UserName);
                if (!Equals(accountDataSet.Tables[0], null) && accountDataSet.Tables[0].Rows.Count > 0)
                {
                    accountInforModel = accountDataSet.Tables[0].Rows[0].ToSingleRow<AccountInfoModel>();
                    if (!Equals(accountInforModel, null) && (Equals(accountInforModel.RoleName, Constant.RoleFranchise)))
                    {
                        ProductAdmin productTierAdmin = new ProductAdmin();
                        Product product = productTierAdmin.GetByProductId(model.ProductId);
                        product.ReviewStateID = productTierAdmin.UpdateReviewStateID(GetAuthorizedPortalId(model.UserName), model.ProductId, "Tiered Pricing Updated", accountInforModel.AccountId, model.UserName);
                        productTierAdmin.Update(product);
                    }
                }
            }
            return isUpdated;
        }

        public bool DeleteProductTier(int productTierId)
        {
            if (productTierId < 1)
            {
                throw new Exception("Producttier id cannot be less than 1.");
            }

            ProductTierService productTierService = new ProductTierService();
            var isDeleted = productTierService.Delete(productTierId);
            return isDeleted;
        }

        #endregion

        #region Digital Assest

        public DigitalAssetLisModel GetDigitalAssets(int productId, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            DigitalAssetLisModel list = new DigitalAssetLisModel();
            string whereClause = StringHelper.GenerateDynamicWhereClause(filters);
            int totalRowCount = 0;
            var pagingStart = 1;
            var pagingLength = 0;
            string orderBy = StringHelper.GenerateDynamicOrderByClause(sorts);

            pagingLength = Convert.ToInt32(page.Get(PageKeys.Size));
            pagingStart = Convert.ToInt32(page.Get(PageKeys.Index));

            StoredProcedureHelper storedProcedureHelper = new StoredProcedureHelper();
            DataSet resultDataSet = storedProcedureHelper.SP_DataDetailsSpConfig(whereClause, string.Empty/*HavingClause*/, StoredProcedureKeys.SpZNodeGetDigitalAsset, orderBy, pagingStart.ToString(), pagingLength.ToString(), out totalRowCount);

            list.DigitalAssets = resultDataSet.Tables[0].ToList<DigitalAssetModel>().ToCollection();
            list.TotalResults = totalRowCount;
            list.PageSize = pagingLength;
            list.PageIndex = pagingStart;
            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                list.PageSize = list.TotalResults;
            }

            return list;
        }

        public DigitalAssetModel CreateDigitalAsset(DigitalAssetModel model)
        {
            if (model == null)
            {
                throw new Exception("DigitalAssetModel model cannot be null.");
            }

            var entity = ProductMap.ToDigitalAssetEntity(model);
            var digitalAsset = _productDigitalAssetRepository.Save(entity);
            if (!Equals(digitalAsset, null) && digitalAsset.DigitalAssetID > 0 && !string.IsNullOrEmpty(model.UserName))
            {
                AccountInfoModel accountInforModel = new AccountInfoModel();
                AccountHelper accountHelper = new AccountHelper();
                DataSet accountDataSet = accountHelper.GetAccountInfo(model.UserName);
                if (!Equals(accountDataSet.Tables[0], null) && accountDataSet.Tables[0].Rows.Count > 0)
                {
                    accountInforModel = accountDataSet.Tables[0].Rows[0].ToSingleRow<AccountInfoModel>();
                    if (!Equals(accountInforModel, null) && (Equals(accountInforModel.RoleName, Constant.RoleFranchise)))
                    {

                        ProductAdmin productTierAdmin = new ProductAdmin();
                        Product product = productTierAdmin.GetByProductId(model.ProductID);
                        product.ReviewStateID = productTierAdmin.UpdateReviewStateID(GetAuthorizedPortalId(model.UserName), model.ProductID, "Digital Asset Added", accountInforModel.AccountId, model.UserName);
                        productTierAdmin.Update(product);
                    }
                }
            }
            return ProductMap.ToDigitalAssetModel(digitalAsset);
        }


        public bool DeleteDigitalAsset(int digitalAssetId)
        {

            if (digitalAssetId < 1)
            {
                throw new Exception("DigitalAsset ID cannot be less than 1.");
            }

            var digitalAsset = _productDigitalAssetRepository.GetByDigitalAssetID(digitalAssetId);
            if (!Equals(digitalAsset, null) && (!Equals(digitalAsset.OrderLineItemID, null)))
            {
                throw new Exception("Assigned Digital Assets cannot be Deleted.");
            }

            if (!Equals(digitalAsset, null))
            {
                return _productDigitalAssetRepository.Delete(digitalAssetId);
            }

            return false;
        }

        #endregion

        #region UpdateImage

        public UpdateImageModel UpdateImage(UpdateImageModel model)
        {
            if (model == null)
            {
                throw new Exception("Product model cannot be null.");
            }
            if (!Equals(model, null))
            {
                var productHelper = new ProductHelper();
                productHelper.UpdateImage(model.Id, model.ImagePath, model.EntityName);
                return model;
            }
            return null;
        }

        #endregion

        #region Product Compare

        public bool SendMailToFriend(CompareProductModel compareProducts)
        {
            string templatePath = string.Empty;
            string defaultTemplatePath = string.Empty;
            string smtpServer = ZNodeConfigManager.SiteConfig.SMTPServer;
            //string senderEmail = compareProducts.SenderEmailAddress;
            
            string receiverEmail = compareProducts.RecieverEmailAddress;

            string apiUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            PortalService portalService = new PortalService();
            PortalModel portal = portalService.GetPortal(compareProducts.PortalId, new NameValueCollection());

            //PRFT Custom Code: Start
            string senderEmail = portal.CustomerServiceEmail;
            //PRFT Custom Code: End
            string emailId = string.Empty;
            //string subject = string.Format("{0}  wants you to see this item at {1}", senderEmail.Split('@').FirstOrDefault(), portal.StoreName);
            string subject = string.Format("{0}  wants you to see this item at {1}", compareProducts.SenderEmailAddress, portal.StoreName);
            string[] recieverEmailIds = receiverEmail.Split(',');
            if (!Equals(recieverEmailIds, null) && recieverEmailIds.Count() > 0)
            {
                for (int index = 0; index < recieverEmailIds.Length; index++)
                {
                    if (!string.IsNullOrEmpty(recieverEmailIds[index]))
                    {
                        emailId = Equals(emailId, string.Empty) ? recieverEmailIds[index] : emailId + ',' + recieverEmailIds[index];
                    }
                }
            }
            if (!compareProducts.IsProductDetails)
            {
                defaultTemplatePath = HttpContext.Current.Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, "ProductCompare.html"));

                compareProducts.ProductList = GetProductsByProductIds(compareProducts.ProductIds, null, null).Products;

                DataTable productDetailItems = new DataTable();
                productDetailItems.Columns.Add("Image");
                productDetailItems.Columns.Add("ProductName");
                productDetailItems.Columns.Add("Price");
                productDetailItems.Columns.Add("BrandName");
                productDetailItems.Columns.Add("Weight");
                productDetailItems.Columns.Add("Length");
                productDetailItems.Columns.Add("Width");
                productDetailItems.Columns.Add("Height");
                productDetailItems.Columns.Add("Description");
                productDetailItems.Columns.Add("FeatureDescription");
                productDetailItems.Columns.Add("ProductSpecification");
                productDetailItems.Columns.Add("MaxQuantity");

                foreach (ProductModel product in compareProducts.ProductList)
                {
                    if (!Equals(productDetailItems, null))
                    {
                        DataRow productImageRow = productDetailItems.NewRow();
                        productImageRow["Image"] = "<img src=" + apiUrl + product.ImageSmallPath.TrimStart('~') + ">";
                        productImageRow["ProductName"] = (!Equals(product.Name, null) || !Equals(product.Name, string.Empty)) ? "<a href='" + compareProducts.BaseUrl + '/' + product.SeoUrl + "'>" + product.Name + "</a>" : "-";
                        productImageRow["Price"] = product.RetailPrice.Value.ToString("c");
                        productImageRow["BrandName"] = !Equals(product.ManufacturerId, null) ? _manufacturerRepository.GetByManufacturerID(product.ManufacturerId.Value).Name : "-";
                        productImageRow["Weight"] = product.Weight;
                        productImageRow["Length"] = product.Length;
                        productImageRow["Width"] = product.Width;
                        productImageRow["Height"] = product.Height;
                        productImageRow["Description"] = (!Equals(product.Description, null) || !Equals(product.Description, string.Empty)) ? HttpUtility.HtmlDecode(product.Description) : "-";
                        productImageRow["FeatureDescription"] = (!Equals(product.FeaturesDescription, null) || !Equals(product.FeaturesDescription, string.Empty)) ? HttpUtility.HtmlDecode(product.FeaturesDescription) : "-";
                        productImageRow["ProductSpecification"] = (!Equals(product.Specifications, null) || !Equals(product.Specifications, string.Empty)) ? HttpUtility.HtmlDecode(product.Specifications) : "-";
                        productImageRow["MaxQuantity"] = product.MaxSelectableQuantity;
                        foreach (AttributeModel attribute in product.Attributes)
                        {
                            productImageRow["Attribute"] = attribute.Name;
                        }
                        productDetailItems.Rows.Add(productImageRow);
                    }
                }

                ZNodeHtmlTemplate template = new ZNodeHtmlTemplate();
                template.Path = defaultTemplatePath;
                StreamReader rw = new StreamReader(template.Path);
                string messageText = rw.ReadToEnd();

                template.Parse("ComparedProducts", productDetailItems.CreateDataReader());

                // Return the HTML output
                messageText = template.Output;

                portal.CustomerServiceEmail = portal.CustomerServiceEmail.Replace(",", ", ");
                Regex rx1 = new Regex("#CustomerServiceEmail#", RegexOptions.IgnoreCase);
                messageText = rx1.Replace(messageText, portal.CustomerServiceEmail);

                Regex rx2 = new Regex("#CustomerServicePhoneNumber#", RegexOptions.IgnoreCase);
                messageText = rx2.Replace(messageText, portal.CustomerServicePhoneNumber);

                string storeLogoPath = portal.LogoPath;

                Regex rx3 = new Regex("#StoreLogo#", RegexOptions.IgnoreCase);
                messageText = rx3.Replace(messageText, apiUrl + storeLogoPath.TrimStart('~'));
                if (File.Exists(defaultTemplatePath))
                {
                    ZNode.Libraries.Framework.Business.ZNodeEmail.SendEmail(emailId, senderEmail, string.Empty, subject, messageText, true);
                }
                return true;
            }
            else
            {
                defaultTemplatePath = HttpContext.Current.Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, "Product_Details.html"));

                ProductModel product = GetProduct(compareProducts.ProductId, null);

                string productLink = string.Format("<a href={0}/{1}>{2} </a> ", compareProducts.BaseUrl, product.SeoUrl, product.Name);
                string storeLink = string.Format("<a href={0}>{1} </a>", compareProducts.BaseUrl, portal.CompanyName);

                StreamReader rw = new StreamReader(defaultTemplatePath);
                string messageText = rw.ReadToEnd();

                Regex rx1 = new Regex("#ProductLink#", RegexOptions.IgnoreCase);
                messageText = rx1.Replace(messageText, productLink);

                Regex rx2 = new Regex("#StoreLink#", RegexOptions.IgnoreCase);
                messageText = rx2.Replace(messageText, storeLink);

                //PRFT Custom COde : Start
                string storeLogoPath = portal.LogoPath;

                Regex rx3 = new Regex("#StoreLogo#", RegexOptions.IgnoreCase);
                messageText = rx3.Replace(messageText, apiUrl + storeLogoPath.TrimStart('~'));

                var rx4 = new Regex("#MailingInfo#", RegexOptions.IgnoreCase);
                messageText = rx4.Replace(messageText, ZNodeConfigManager.SiteConfig.CustomerServiceEmail);

                var rx5 = new Regex("#SupportPhone#", RegexOptions.IgnoreCase);
                messageText = rx5.Replace(messageText, ZNodeConfigManager.SiteConfig.CustomerServicePhoneNumber);

                var rx6 = new Regex("#SupportEmail#", RegexOptions.IgnoreCase);
                messageText = rx6.Replace(messageText, ZNodeConfigManager.SiteConfig.SalesEmail);

                string logoLink = ConfigurationManager.AppSettings["DemoWebsiteUrl"];
                var rx7 = new Regex("#LogoLink#", RegexOptions.IgnoreCase);
                messageText = rx7.Replace(messageText, logoLink);

                var rx8 = new Regex("#YourName#", RegexOptions.IgnoreCase);
                messageText = rx8.Replace(messageText, compareProducts.SenderEmailAddress);
                //PRFT Custom Code : End

                if (File.Exists(defaultTemplatePath))
                {
                    ZNode.Libraries.Framework.Business.ZNodeEmail.SendEmail(emailId, senderEmail, string.Empty, subject, messageText, true);
                    return true;
                }
                return false;
            }
            return false;
        }

        #endregion

        /// <summary>
        /// Gets the Turnkey Store Profile ID
        /// </summary>
        private int GetTurnkeyStoreProfileID(string userName)
        {
            PortalProfileService postalProfileService = new PortalProfileService();
            string storeList = GetAvailablePortals(userName);
            if (string.Compare(storeList, "0") == 0)
            {
                return 0;
            }
            string[] stores = storeList.Split(',');
            if (string.IsNullOrEmpty(storeList.Trim()))
            {
                return 0;
            }
            TList<PortalProfile> profiles = postalProfileService.GetByPortalID(Convert.ToInt32(stores[0]));
            if (profiles != null && profiles.Count > 0)
            {
                return profiles[0].ProfileID;
            }
            return 0;
        }

        /// <summary>
        /// Set Small Image Path For Highlight
        /// </summary>
        /// <param name="highlightList">highlightList</param>
        public static void SetSmallImagePathForHighlight(HighlightListModel highlightList)
        {
            if (!Equals(highlightList, null) && !Equals(highlightList.Highlights, null) && highlightList.Highlights.Count > 0)
            {
                ZNodeImage imageUtility = new ZNodeImage();
                highlightList.Highlights.Select(model => { model.ImageSmallPath = imageUtility.GetImageHttpPathSmall(model.ImageFile); return model; }).ToList();
            }
        }
    }
}

