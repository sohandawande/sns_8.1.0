﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    public interface IRequestStatusService
    {
        /// <summary>
        /// Gets request status list.
        /// </summary>
        /// <param name="filters">Filters for request status list.</param>
        /// <param name="sorts">Sorting of request status list.</param>
        /// <param name="page">Page information for request status list.</param>
        /// <returns>Request status list model.</returns>
        RequestStatusListModel GetRequestStatusList(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Method Gets RequestStatus details.
        /// </summary>
        /// <param name="requestStatusId">RequestStatus Id for which RequestStatus list is to be retrieved.</param>
        /// <param name="expands">Expands for RequestStatus</param>
        /// <returns></returns>
        RequestStatusModel GetRequestStatus(int requestStatusId, NameValueCollection expands);

        /// <summary>
        /// Method Create the RequestStatus.
        /// </summary>
        /// <param name="model">Model of RequestStatus</param>
        /// <returns>Return RequestStatus model.</returns>
        RequestStatusModel CreateRequestStatus(RequestStatusModel model);

        /// <summary>
        /// Method Update the RequestStatus based on RequestStatusId.
        /// </summary>
        /// <param name="requestStatusId">RequestStatus Id for which existing RequestStatus will update</param>
        /// <param name="model">Model of RequestStatus</param>
        /// <returns>Return updated RequestStatus model.</returns>
        RequestStatusModel UpdateRequestStatus(int requestStatusId, RequestStatusModel model);

        /// <summary>
        /// Method Delete the RequestStatus based on RequestStatusId.
        /// </summary>
        /// <param name="requestStatusId">RequestStatus Id for which existing RequestStatus will delete</param>
        /// <returns>Return true or false.</returns>
        bool DeleteRequestStatus(int requestStatusId);

    }
}
