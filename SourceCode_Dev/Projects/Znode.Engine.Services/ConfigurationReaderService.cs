﻿using System.Linq;
using ApplicationSettingRepository = ZNode.Libraries.DataAccess.Service.ApplicationSettingService;
namespace Znode.Engine.Services
{
    public class ConfigurationReaderService : BaseService, IConfigurationReaderService
    {
        #region Private Variables
        private readonly ApplicationSettingRepository _dynamicGridConfig;
        #endregion

        #region Constructor
        public ConfigurationReaderService()
        {
            _dynamicGridConfig = new ApplicationSettingRepository();
        }
        #endregion

        #region Public Methods
        public string GetFilterConfigurationXML(string itemName)
        {
            var configData = _dynamicGridConfig.GetAll().ToList().FirstOrDefault(x => x.ItemName.Equals(itemName) || x.Id.ToString().Equals(itemName));
            return Equals(configData, null) ? string.Empty : configData.Setting;
        }
        #endregion
    }
}
