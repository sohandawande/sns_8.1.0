﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    public interface IContentPageService
    {
        /// <summary>
        /// Gets List of content Pages.
        /// </summary>
        /// <param name="expands">Expands for the content pages.</param>
        /// <param name="filters">Filters for the content page list.</param>
        /// <param name="sorts">Sorts for content page list.</param>
        /// <param name="page">Paging informtion for content page list.</param>
        /// <returns></returns>
        ContentPageListModel GetContentPages(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Creates a content page.
        /// </summary>
        /// <param name="model">Content page model to be created.</param>
        /// <returns>Content page model.</returns>
        ContentPageModel CreateContentPage(ContentPageModel model);

        /// <summary>
        /// Clones a content page.
        /// </summary>
        /// <param name="contentPageModel">Content page model to be cloned.</param>
        /// <returns>Content Page Model.</returns>
        ContentPageModel Clone(ContentPageModel contentPageModel);

        /// <summary>
        /// Adds a content page with specified parameters.
        /// </summary>
        /// <param name="contentPageModel">Content page model to be added.</param>
        /// <returns></returns>
        bool AddPage(ContentPageModel contentPageModel);

        /// <summary>
        /// Deletes the content page.
        /// </summary>
        /// <param name="contentPageId">Id of the content page.</param>
        /// <returns>Returns True or False</returns>
        bool DeleteContentPage(int contentPageId);

        /// <summary>
        /// Update the Content Page.
        /// </summary>
        /// <param name="contentPageId">int contentPageId</param>
        /// <param name="model">ContentPageModel model</param>
        /// <returns>Returns True or False</returns>
        bool UpdateContentPage(int contentPageId, ContentPageModel model);

        /// <summary>
        /// Gets the Content Page.
        /// </summary>
        /// <param name="contentPageId">int contentPageId</param>
        /// <param name="expands">NameValueCollection expands</param>
        /// <returns>Returns True or False</returns>
        ContentPageModel GetContentPage(int contentPageId, NameValueCollection expands);

        /// <summary>
        /// Revert the revision.
        /// </summary>
        /// <param name="pageRevisionId">Id of the revision</param>
        /// <param name="updatedUser">Upadted user</param>
        /// <param name="oldHtml">old html</param>
        /// <returns>Returns True or False</returns>
        bool RevertRevision(int pageRevisionId, string updatedUser, string oldHtml);

        /// <summary>
        /// Get content page data on the basis of content page name and its file extension.
        /// </summary>
        /// <param name="contentPageName">Name of the content page.</param>
        /// <param name="extension">Extension of content page file.</param>
        /// <returns>Returns string of content page data.</returns>
        string GetContentPageByName(string contentPageName, string extension);
    }
}
