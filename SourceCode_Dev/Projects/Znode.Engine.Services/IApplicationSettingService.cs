﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
   public interface IApplicationSettingService
    {
       /// <summary>
       /// Get Xml configuration list from database.
       /// </summary>
        /// <param name="expands">Expands collections</param>
       /// <param name="filters">Filters collection</param>
       /// <param name="sorts">Sort collection</param>
       /// <param name="page">Page Number</param>
        /// <returns>Returns ApplicationSettingListModel</returns>
       ApplicationSettingListModel GetApplicationSettings(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

       /// <summary>
       /// Get Column list from database.
       /// </summary>
       /// <param name="entityType">Type of object</param>
       /// <param name="entityName">Name of Object</param>
       /// <returns></returns>
       DataSet GetColumnList(string entityType, string entityName);

       /// <summary>
       /// Save Xml configuration in ApplicationSetting Table.
       /// </summary>
       /// <param name="model">ApplicationSettingDataModel</param>
       /// <returns>Returns true/false</returns>
       bool SaveXmlConfiguration(ApplicationSettingDataModel model);
    }
}
