﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    public interface IManageSearchIndexService
    {
        /// <summary>
        /// Get lucene search index status list
        /// </summary>
        /// <param name="filters">filter collection</param>
        /// <param name="sorts">sort collection</param>
        /// <param name="page">page number</param>
        /// <returns>Returns LuceneIndexListModel</returns>
        LuceneIndexListModel GetLuceneIndexStatus(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Create lucene search index
        /// </summary>
        /// <returns>true/false</returns>
        bool CreateIndex();

        /// <summary>
        /// Disable/Enable trigger
        /// </summary>
        /// <param name="flag"></param>
        /// <returns></returns>
        int DisableTriggers(int flag);

        /// <summary>
        /// Disable/Enable window service
        /// </summary>
        /// <param name="flag">int flag</param>
        /// <returns>int flag</returns>
        int DisableWinService(int flag);

        /// <summary>
        /// Get lucene index server status log
        /// </summary>
        /// <param name="indexId">index id</param>
        /// <returns>Returns LuceneIndexServerListModel</returns>
        LuceneIndexServerListModel GetIndexServerStatus(int indexId);
    }
}
