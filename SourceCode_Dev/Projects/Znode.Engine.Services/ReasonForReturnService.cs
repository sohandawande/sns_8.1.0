﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using ReasonForReturnRepository = ZNode.Libraries.DataAccess.Service.ReasonForReturnService;
namespace Znode.Engine.Services
{
    public class ReasonForReturnService : BaseService, IReasonForReturnService
    {
        #region Private Variables
        private readonly ReasonForReturnRepository _reasonForReturnRepository;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor for ReasonForReturn service
        /// </summary>
        public ReasonForReturnService()
        {
            _reasonForReturnRepository = new ReasonForReturnRepository();
        }
        #endregion

        #region Public Methods
        public ReasonForReturnListModel GetListOfReasonForReturn(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new ReasonForReturnListModel();
            var reasonsForReturn = new TList<ReasonForReturn>();
            var tempList = new TList<ReasonForReturn>();

            var query = new ReasonForReturnQuery();
            var sortBuilder = new ReasonForReturnSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            tempList = _reasonForReturnRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (Equals(pagingLength, int.MaxValue))
            {
                model.PageSize = model.TotalResults;
            }

            // Now go through the list and get any expands
            foreach (var reasonForReturn in tempList)
            {
                reasonsForReturn.Add(reasonForReturn);
            }

            // Map each item and add to the list
            foreach (var item in reasonsForReturn)
            {
                model.ReasonsForReturn.Add(ReasonForReturnMap.ToModel(item));
            }
            return model;
        }

        public ReasonForReturnModel GetReasonForReturn(int reasonForReturnId, NameValueCollection expands)
        {
            var reasonForReturn = _reasonForReturnRepository.GetByReasonForReturnID(reasonForReturnId);

            if (!Equals(reasonForReturn,null))
            {
                return ReasonForReturnMap.ToModel(reasonForReturn);
            }
            return null;
        }

        public ReasonForReturnModel CreateReasonForReturn(ReasonForReturnModel model)
        {
            if (Equals(model, null))
            {
                throw new Exception("ReasonForReturn model cannot be null.");
            }

            var entity = ReasonForReturnMap.ToEntity(model);
            var reasonForReturn = _reasonForReturnRepository.Save(entity);
            return ReasonForReturnMap.ToModel(reasonForReturn);
        }

        public ReasonForReturnModel UpdateReasonForReturn(int reasonForReturnId, ReasonForReturnModel model)
        {
            if (reasonForReturnId < 1)
            {
                throw new Exception("ReasonForReturn ID cannot be less than 1.");
            }

            if (Equals(model, null))
            {
                throw new Exception("ReasonForReturn model cannot be null.");
            }

            var reasonForReturn = _reasonForReturnRepository.GetByReasonForReturnID(reasonForReturnId);
            if (!Equals(reasonForReturn, null))
            {
                // Set profileId and update details.
                model.ReasonForReturnId = reasonForReturnId;
                var reasonForReturnToUpdate = ReasonForReturnMap.ToEntity(model);

                var updated = _reasonForReturnRepository.Update(reasonForReturnToUpdate);
                if (updated)
                {
                    reasonForReturn = _reasonForReturnRepository.GetByReasonForReturnID(reasonForReturnId);
                    return ReasonForReturnMap.ToModel(reasonForReturn);
                }
            }
            return null;
        }

        public bool DeleteReasonForReturn(int reasonForReturnId)
        {
            if (reasonForReturnId < 1)
            {
                throw new Exception("ReasonForReturn ID cannot be less than 1.");
            }

            var reasonForReturn = _reasonForReturnRepository.GetByReasonForReturnID(reasonForReturnId);
            if (!Equals(reasonForReturn,null))
            {
                return _reasonForReturnRepository.Delete(reasonForReturn);
            }
            return false;
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Method Add filters by which Get filtered list
        /// </summary>
        /// <param name="filters">Filter collection</param>
        /// <param name="query">Query to execute</param>
        private void SetFiltering(List<Tuple<string, string, string>> filters, ReasonForReturnQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;

                if (Equals(filterKey, FilterKeys.IsActive)) { SetQueryParameter(ReasonForReturnColumn.IsEnabled, filterOperator, filterValue, query); }
                if (Equals(filterKey,FilterKeys.Name)) { SetQueryParameter(ReasonForReturnColumn.Name, filterOperator, filterValue, query); }
                if (Equals(filterKey, FilterKeys.ReasonForReturnId)) { SetQueryParameter(ReasonForReturnColumn.ReasonForReturnID, filterOperator, filterValue, query); }
            }
        }

        /// <summary>
        /// Method Add Sorts by which Get sorted list
        /// </summary>
        /// <param name="sorts">Sort Collection</param>
        /// <param name="sortBuilder">SortBuilder</param>
        private void SetSorting(NameValueCollection sorts, ReasonForReturnSortBuilder sortBuilder)
        {
            if (sorts.HasKeys())
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);

                    if (Equals(key, SortKeys.Name)) { SetSortDirection(ReasonForReturnColumn.Name, value, sortBuilder); }
                }
            }
        }
        #endregion
    }
}
