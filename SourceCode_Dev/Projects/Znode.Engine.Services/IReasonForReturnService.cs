﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    public interface IReasonForReturnService
    {
        /// <summary>
        /// Method Gets ReasonForReturn details.
        /// </summary>
        /// <param name="reasonForReturnId">ReasonForReturn Id for which ReasonForReturn list is to be retrieved.</param>
        /// <param name="expands">Expands for ReasonForReturn</param>
        /// <returns></returns>
        ReasonForReturnModel GetReasonForReturn(int reasonForReturnId, NameValueCollection expands);

        /// <summary>
        /// Method Gets the ReasonForReturn list.
        /// </summary>
        /// <param name="expands">Expands for ReasonForReturn list.</param>
        /// <param name="filters">Filters for ReasonForReturn list.</param>
        /// <param name="sorts">Sorting of ReasonForReturn list.</param>
        /// <param name="page">Paging information for ReasonForReturn list.</param>
        /// <returns>Return ReasonForReturn list.</returns>
        ReasonForReturnListModel GetListOfReasonForReturn(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Method Create the ReasonForReturn.
        /// </summary>
        /// <param name="model">Model of ReasonForReturn</param>
        /// <returns>Return ReasonForReturn model.</returns>
        ReasonForReturnModel CreateReasonForReturn(ReasonForReturnModel model);

        /// <summary>
        /// Method Update the ReasonForReturn based on reasonForReturnId.
        /// </summary>
        /// <param name="reasonForReturnId">ReasonForReturn Id for which existing ReasonForReturn will update</param>
        /// <param name="model">Model of ReasonForReturn</param>
        /// <returns>Return updated ReasonForReturn model.</returns>
        ReasonForReturnModel UpdateReasonForReturn(int reasonForReturnId, ReasonForReturnModel model);

        /// <summary>
        /// Method Delete the ReasonForReturn based on reasonForReturnId.
        /// </summary>
        /// <param name="reasonForReturnId">ReasonForReturn Id for which existing ReasonForReturn will delete</param>
        /// <returns>Return true or false.</returns>
        bool DeleteReasonForReturn(int reasonForReturnId);
    }
}
