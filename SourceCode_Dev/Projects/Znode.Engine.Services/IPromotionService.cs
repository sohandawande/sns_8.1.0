﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
	public interface IPromotionService
	{
		PromotionModel GetPromotion(int promotionId, NameValueCollection expands);
		PromotionListModel GetPromotions(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
        ProductListModel GetProductsList(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
		PromotionModel CreatePromotion(PromotionModel model);
		PromotionModel UpdatePromotion(int promotionId, PromotionModel model);
		bool DeletePromotion(int promotionId);
	}
}
