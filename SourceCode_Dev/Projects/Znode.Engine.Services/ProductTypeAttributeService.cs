﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Text;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Helpers;
using Znode.Libraries.Helpers.Constants;
using Znode.Libraries.Helpers.Extensions;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using ProductTypeAttributeRepository = ZNode.Libraries.DataAccess.Service.ProductTypeAttributeService;

namespace Znode.Engine.Services
{
    public class ProductTypeAttributeService : BaseService, IProductTypeAttributeService
    {
        #region Public Variables
        private readonly ProductTypeAttributeRepository _productTypeAttributeRepository;
        #endregion

        #region Public Constructor
        public ProductTypeAttributeService()
        {
            _productTypeAttributeRepository = new ProductTypeAttributeRepository();
        }
        #endregion

        #region Public Methods
        public ProductTypeAttributeModel GetAttributeType(int productTypeId)
        {
            var ProductTypeAttribute = _productTypeAttributeRepository.GetByProductAttributeTypeID(productTypeId);
            return ProductTypeAttributeMap.ToModel(ProductTypeAttribute);
        }


        public ProductTypeAttributeListModel GetAttributeTypes(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new ProductTypeAttributeListModel();
            var productTypes = new TList<ProductTypeAttribute>();

            var query = new ProductTypeAttributeQuery();
            var sort = new ProductTypeAttributeSortBuilder();

            var pagingStart = 0;
            var pagingLength = 0;

            SetFiltering(filters, query);
            SetSorting(sorts, sort);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            int productTypeId = Convert.ToInt32(filters[0].Item3);
            productTypes = _productTypeAttributeRepository.GetByProductTypeId(productTypeId);
            //productTypes = _productTypeAttributeRepository.Find(query, sort, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                model.PageSize = model.TotalResults;
            }

            // Map each item and add to the list
            foreach (var m in productTypes)
            {
                model.ProductTypeAttribute.Add(ProductTypeAttributeMap.ToModel(m));
            }

            return model;
        }

        public ProductTypeAttributeModel CreateAttributeType(ProductTypeAttributeModel model)
        {
            var productTypeAttributes = new TList<ProductTypeAttribute>();
            var query = new ProductTypeAttributeQuery();

            if (Equals(model , null))
            {
                throw new Exception("Product Type Attribute model cannot be null.");
            }

            query.AppendEquals(ProductTypeAttributeColumn.AttributeTypeId, model.AttributeTypeId.ToString());
            query.AppendEquals(ProductTypeAttributeColumn.ProductTypeId, model.ProductTypeId.ToString());
            TList<ProductTypeAttribute> list = _productTypeAttributeRepository.Find(query);
            if (list.Count > 0)
            {
                // Display error message
                throw new Exception("Same attribute can't be added twice.");
            }
            var entity = ProductTypeAttributeMap.ToEntity(model);
            var productType = _productTypeAttributeRepository.Save(entity);
            return ProductTypeAttributeMap.ToModel(productType);
        }

        public ProductTypeAttributeModel UpdateAttributeType(int productTypeAttributeId, ProductTypeAttributeModel model)
        {
            if (productTypeAttributeId < 1)
            {
                throw new Exception("ProductAttributeType ID cannot be less than 1.");
            }

            if (Equals(model , null))
            {
                throw new Exception("ProductTypeAttribute model cannot be null.");
            }

            var productAttributeType = _productTypeAttributeRepository.GetByProductAttributeTypeID(productTypeAttributeId);
            if (!Equals(productAttributeType , null))
            {
                // Set Product Attribute Type ID
                model.ProductAttributeTypeID = productTypeAttributeId;

                var productTypeToUpdate = ProductTypeAttributeMap.ToEntity(model);

                if (_productTypeAttributeRepository.Update(productTypeToUpdate))
                {
                    productAttributeType = _productTypeAttributeRepository.GetByProductAttributeTypeID(productTypeAttributeId);

                    return ProductTypeAttributeMap.ToModel(productAttributeType);
                }
            }

            return null;
        }

        public bool DeleteAttributeType(int productTypeId)
        {
            if (productTypeId < 1)
            {
                throw new Exception("productType ID cannot be less than 1.");
            }

            var productTypeAttribute = _productTypeAttributeRepository.GetByProductAttributeTypeID(productTypeId);
            if (!Equals(productTypeAttribute , null))
            {
                return _productTypeAttributeRepository.Delete(productTypeAttribute);
            }

            return false;
        }

        public ProductTypeAssociatedAttributeTypesListModel GetAttributeTypesByProductTypeId(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            ProductTypeAssociatedAttributeTypesListModel model = new ProductTypeAssociatedAttributeTypesListModel();
            Dictionary<string, string> StoredProc = new Dictionary<string, string>();
            
            string whereClause = StringHelper.GenerateDynamicWhereClause(filters);
            
            var pagingStart = 0;
            var pagingLength = 0;
            var totalRowCount = 0;
            SetPaging(page, model, out pagingStart, out pagingLength);
            pagingStart = Convert.ToInt32(page.Get(PageKeys.Index));
            string orderBy = StringHelper.GenerateDynamicOrderByClause(sorts); 

            pagingStart = Convert.ToInt32(page.Get(PageKeys.Index));
            pagingLength = Convert.ToInt32(page.Get(PageKeys.Size));

            StoredProcedureHelper storedProcedureHelper = new StoredProcedureHelper();

            DataSet resultDataSet = storedProcedureHelper.SP_DataDetailsSpConfig(whereClause, string.Empty/*HavingClause*/, StoredProcedureKeys.SpGetAttributeTypesBy, orderBy, pagingStart.ToString(), pagingLength.ToString(), out totalRowCount, null, whereClause);
            model.TotalResults = totalRowCount;
            model.PageSize = pagingLength;
            model.PageIndex = pagingStart;
            model.AttributeList = resultDataSet.Tables[0].ToList<ProductTypeAssociatedAttributeTypesModel>().ToCollection();

            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                model.PageSize = model.TotalResults;
            }

            return model;
        }

        public bool IsProductAssociatedProductType(int productTypeId)
        {
            ProductTypeHelper productTypeHelper = new ProductTypeHelper();
            return productTypeHelper.GetProductsByProductTypeId(productTypeId) > 0 ? true : false;
        }

        #endregion

        #region Private Methods
        private void SetFiltering(List<Tuple<string, string, string>> filters, ProductTypeAttributeQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;

                if (filterKey == FilterKeys.ProductAttributeTypeID) { SetQueryParameter(ProductTypeAttributeColumn.ProductAttributeTypeID, filterOperator, filterValue, query); }

            }
        }

        private void SetSorting(NameValueCollection sorts, ProductTypeAttributeSortBuilder sortBuilder)
        {
            if (sorts.HasKeys())
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);

                    if (key == SortKeys.AttributeTypeId) { SetSortDirection(ProductTypeAttributeColumn.AttributeTypeId, value, sortBuilder); }
                    if (key == SortKeys.ProductTypeId) { SetSortDirection(ProductTypeAttributeColumn.ProductTypeId, value, sortBuilder); }
                    if (key == SortKeys.ProductAttributeTypeID) { SetSortDirection(ProductTypeAttributeColumn.ProductAttributeTypeID, value, sortBuilder); }
                }
            }
        }

        private void SetQueryParameter(ProductTypeAttributeColumn column, string filterOperator, string filterValue, ProductTypeAttributeQuery query)
        {
            base.SetQueryParameter(column, filterOperator, filterValue, query);
        }

        private void SetSortDirection(ProductTypeAttributeColumn column, string value, ProductTypeAttributeSortBuilder sortBuilder)
        {
            base.SetSortDirection(column, value, sortBuilder);
        }

        /// <summary>
        /// Returns a bread crumb path from a delimited string
        /// </summary>
        /// <param name="delimitedString">Delimitted string</param>
        /// <param name="separator">Path seperator.</param>
        /// <returns>Returns the parsed category path.</returns>
        private string ParsePath(string delimitedString, string separator)
        {
            StringBuilder categoryPath = new StringBuilder();

            string[] delim1 = { "||" };
            string[] pathItems = delimitedString.Split(delim1, StringSplitOptions.None);

            foreach (string pathItem in pathItems)
            {
                string[] delim2 = { "%%" };
                string[] items = pathItem.Split(delim2, StringSplitOptions.None);

                string categoryId = items.GetValue(0).ToString();
                string categoryName = items.GetValue(1).ToString();
                categoryPath.Append(categoryName);
                categoryPath.Append(string.Format(" {0} ", separator));
            }

            if (categoryPath.Length > 0)
            {
                if (categoryPath.ToString().LastIndexOf(string.Format(" {0} ", separator)) == categoryPath.ToString().Length - 3)
                {
                    categoryPath = categoryPath.Remove(categoryPath.ToString().Length - 3, 3);
                }
            }

            return categoryPath.ToString();
        }
        #endregion
        
    }
}
