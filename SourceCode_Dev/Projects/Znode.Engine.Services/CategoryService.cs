﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Text;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Helpers;
using Znode.Libraries.Helpers.Constants;
using Znode.Libraries.Helpers.Extensions;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;
using CategoriesCatalogsRepository = ZNode.Libraries.DataAccess.Service.VwZnodeCategoriesCatalogsService;
using CategoryNodeRepository = ZNode.Libraries.DataAccess.Service.CategoryNodeService;
using CategoryProfileRepository = ZNode.Libraries.DataAccess.Service.CategoryProfileService;
using CategoryRepository = ZNode.Libraries.DataAccess.Service.CategoryService;
using ProductCategoryAdminRepository = ZNode.Libraries.Admin.ProductCategoryAdmin;
using ProductCategoryRepository = ZNode.Libraries.DataAccess.Service.ProductCategoryService;
using UrlRedirectAdminRepository = ZNode.Libraries.Admin.UrlRedirectAdmin;

namespace Znode.Engine.Services
{
    public class CategoryService : BaseService, ICategoryService
    {
        #region Private Variables
        private readonly CategoriesCatalogsRepository _categoriesCatalogsRepository;
        private readonly CategoryNodeRepository _categoryNodeRepository;
        private readonly CategoryProfileRepository _categoryProfileRepository;
        private readonly CategoryRepository _categoryRepository;
        private readonly ProductCategoryRepository _productCategoryRepository;
        private readonly UrlRedirectAdminRepository _urlRedirectAdminRepository;
        private readonly ProductCategoryAdminRepository _productCategoryAdminRepository;
        #endregion

        #region Constructor
        public CategoryService()
        {
            _categoriesCatalogsRepository = new CategoriesCatalogsRepository();
            _categoryNodeRepository = new CategoryNodeRepository();
            _categoryProfileRepository = new CategoryProfileRepository();
            _categoryRepository = new CategoryRepository();
            _productCategoryRepository = new ProductCategoryRepository();
            _urlRedirectAdminRepository = new UrlRedirectAdminRepository();
            _productCategoryAdminRepository = new ProductCategoryAdminRepository();
        }
        #endregion

        #region Public Methods
        public CategoryModel GetCategory(int categoryId, NameValueCollection expands)
        {
            var category = _categoryRepository.GetByCategoryID(categoryId);
            if (!Equals(category, null))
            {
                GetExpands(expands, category);
            }

            return CategoryMap.ToModel(category);
        }

        public CategoryListModel GetCategories(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new CategoryListModel();
            var categories = new TList<Category>();
            var tempList = new TList<Category>();

            var query = new CategoryQuery();
            var sortBuilder = new CategorySortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            tempList = _categoryRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (Equals(pagingLength, int.MaxValue))
            {
                model.PageSize = model.TotalResults;
            }

            // Now go through the list and get any expands
            foreach (var category in tempList)
            {
                GetExpands(expands, category);
                categories.Add(category);
            }

            // Map each item and add to the list
            foreach (var c in categories)
            {
                model.Categories.Add(CategoryMap.ToModel(c));
            }

            return model;
        }

        public CategoryListModel GetCategoryTree(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var tempModel = new CategoryListModel();
            string orderBy = StringHelper.GenerateDynamicOrderByClause(sorts);
            CategoryHelper categoryHelper = new CategoryHelper();
            int catalogId = !Equals(filters, null) && filters.Count > 0 ? Convert.ToInt32(filters[0].Item3) : 0;
            string categoryName = !Equals(filters, null) && filters.Count > 0 ? Convert.ToString(filters[1].Item3) : string.Empty;
            DataSet datasetCategory = categoryHelper.GetCategoryTree(catalogId, categoryName);
            tempModel.Categories = datasetCategory.Tables[0].ToList<CategoryModel>().ToCollection();
            var categories = new TList<Category>();
            // Now go through the list, get the full category, and get any expands
            foreach (var item in tempModel.Categories)
            {
                var category = _categoryRepository.GetByCategoryID(item.CategoryId);

                GetExpands(expands, category);
                categories.Add(category);
            }

            //// Map each item and add to the list
            var model = new CategoryListModel();
            foreach (var c in categories)
            {
                CategoryModel categoryModel = CategoryMap.ToModel(c);
                categoryModel.ProductCount = tempModel.Categories.Where(category => Equals(category.CategoryId, c.CategoryID)).FirstOrDefault().ProductCount;
                categoryModel.ActiveProductCount = tempModel.Categories.Where(category => Equals(category.CategoryId, c.CategoryID)).FirstOrDefault().ActiveProductCount;
                categoryModel.ParentCategoryNodeId = tempModel.Categories.Where(category => Equals(category.CategoryId, c.CategoryID)).FirstOrDefault().ParentCategoryNodeId;
                categoryModel.IsActive = tempModel.Categories.Where(category => Equals(category.CategoryId, c.CategoryID)).FirstOrDefault().IsActive;
                model.Categories.Add(categoryModel);
            }

            return model;
        }
        public CategoryListModel GetCategoriesByCatalog(int catalogId, NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new CategoryListModel();
            var categories = new TList<Category>();
            var tempList = new VList<VwZnodeCategoriesCatalogs>();

            var query = new VwZnodeCategoriesCatalogsQuery { Junction = String.Empty };
            var sortBuilder = new VwZnodeCategoriesCatalogsSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            // Manually set the catalog ID parameter
            SetQueryParameter(VwZnodeCategoriesCatalogsColumn.CatalogID, FilterOperators.Equals, catalogId.ToString(), query);

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            tempList = _categoriesCatalogsRepository.Find(query, sortBuilder.ToString(), pagingStart, pagingLength, out totalResults);
            if (Equals(tempList, null))
            { return model; }
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (Equals(pagingLength, int.MaxValue))
            {
                model.PageSize = model.TotalResults;
            }

            // Now go through the list, get the full category, and get any expands
            foreach (var item in tempList)
            {
                var category = _categoryRepository.GetByCategoryID(item.CategoryID);
                GetExpands(expands, category);
                categories.Add(category);
            }

            // Map each item and add to the list
            foreach (var c in categories)
            {
                model.Categories.Add(CategoryMap.ToModel(c));
            }

            return model;
        }

        public CategoryListModel GetCategoriesByCatalogIds(string catalogIds, NameValueCollection expands, List<Tuple<string, string, string>> filters,
                                                           NameValueCollection sorts, NameValueCollection page)
        {
            if (String.IsNullOrEmpty(catalogIds))
            {
                throw new Exception("List of catalog IDs cannot be null or empty.");
            }

            var model = new CategoryListModel();
            var categories = new TList<Category>();
            var tempList = new VList<VwZnodeCategoriesCatalogs>();

            var query = new VwZnodeCategoriesCatalogsQuery { Junction = String.Empty };
            var sortBuilder = new VwZnodeCategoriesCatalogsSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            var list = catalogIds.Split(',');
            foreach (var catalogId in list.Where(catalogId => !String.IsNullOrEmpty(catalogId)))
            {
                query.BeginGroup("OR");
                query.AppendEquals(VwZnodeCategoriesCatalogsColumn.CatalogID, catalogId);
                query.EndGroup();
            }

            // Manually set the catalog ID parameter
            //SetQueryParameter(VwZnodeCategoriesCatalogsColumn.CatalogID, FilterOperators.Contains, catalogIds, query);

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            tempList = _categoriesCatalogsRepository.Find(query, sortBuilder.ToString(), pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (Equals(pagingLength, int.MaxValue))
            {
                model.PageSize = model.TotalResults;
            }

            // Now go through the list, get the full category, and get any expands
            foreach (var item in tempList)
            {
                var category = _categoryRepository.GetByCategoryID(item.CategoryID);
                GetExpands(expands, category);
                categories.Add(category);
            }

            // Map each item and add to the list
            foreach (var c in categories)
            {
                model.Categories.Add(CategoryMap.ToModel(c));
            }

            return model;
        }

        public CategoryListModel GetCategoriesByCategoryIds(string categoryIds, NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            if (String.IsNullOrEmpty(categoryIds))
            {
                throw new Exception("List of category IDs cannot be null or empty.");
            }

            var model = new CategoryListModel();
            var categories = new TList<Category>();
            var tempList = new TList<Category>();

            var query = new CategoryQuery { Junction = String.Empty };
            var sortBuilder = new CategorySortBuilder();

            var list = categoryIds.Split(',');
            foreach (var categoryId in list.Where(categoryId => !String.IsNullOrEmpty(categoryId)))
            {
                query.BeginGroup("OR");
                query.AppendEquals(CategoryColumn.CategoryID, categoryId);
                query.EndGroup();
            }

            SetSorting(sorts, sortBuilder);

            // Get the initial set
            tempList = _categoryRepository.Find(query, sortBuilder);

            // Now go through the list and get any expands
            foreach (var category in tempList)
            {
                GetExpands(expands, category);
                categories.Add(category);
            }

            // Map each item and add to the list
            foreach (var c in categories)
            {
                model.Categories.Add(CategoryMap.ToModel(c));
            }

            return model;
        }

        public CategoryModel CreateCategory(CategoryModel model)
        {
            if (Equals(model, null))
            {
                throw new Exception("Category model cannot be null.");
            }

            if (!string.IsNullOrEmpty(model.SeoUrl))
            {
                if (_urlRedirectAdminRepository.SeoUrlExists(model.SeoUrl, model.CategoryId))
                {
                    throw new Exception("The SEO Friendly URL you entered is already in use on another page. Please select another name for your URL.");
                }
            }

            var entity = CategoryMap.ToEntity(model);

            var category = _categoryRepository.Save(entity);
            if (!Equals(category, null))
            {
                return CategoryMap.ToModel(category);
            }

            return null;
        }

        public CategoryModel UpdateCategory(int categoryId, CategoryModel model)
        {
            if (categoryId < 1)
            {
                throw new Exception("Category ID cannot be less than 1.");
            }

            if (Equals(model, null))
            {
                throw new Exception("Category model cannot be null.");
            }

            //IsFromSitecore condition is added to check request is from SiteCore for Znode Sitecore Connector
            //If request is from Sitecore then no need to check SeoUrl Exists
            if (!string.IsNullOrEmpty(model.SeoUrl) && Equals(model.IsFromSitecore, false))
            {
                if (_urlRedirectAdminRepository.SeoUrlExists(model.SeoUrl, categoryId))
                {
                    throw new Exception("The SEO Friendly URL you entered is already in use on another page. Please select another name for your URL.");
                }
            }

            var category = _categoryRepository.GetByCategoryID(categoryId);
            if (!Equals(category, null))
            {
                // Set the category ID
                model.CategoryId = categoryId;

                var categoryToUpdate = CategoryMap.ToEntity(model);

                var updated = _categoryRepository.Update(categoryToUpdate);
                if (updated)
                {
                    category = _categoryRepository.GetByCategoryID(categoryId);
                    if (!Equals(category, null))
                    {
                        return CategoryMap.ToModel(category);
                    }
                }
            }

            return null;
        }

        public bool DeleteCategory(int categoryId)
        {
            bool isAssociate = false;

            CategoryHelper categoryHelper = new CategoryHelper();

            if (categoryId < 1)
            {
                throw new Exception("Category ID cannot be less than 1.");
            }

            if (!Equals(categoryId, null))
            {
                if (!categoryHelper.DeleteCategoryByID(categoryId))
                {
                    isAssociate = false;
                    throw new Exception("An error occurred and the category could not be deleted. Please ensure that this category does not contain child categories or products. If it does, then delete the child categories and products first.");
                }
                else
                {
                    isAssociate = true;
                }
            }
            return isAssociate;
        }

        public bool CategoryAssociatedProducts(int categoryId, string productIds)
        {
            if (categoryId < 1)
            {
                throw new Exception("Category ID cannot be less than 1.");
            }

            var categoryHelper = new CategoryHelper();
            if (!_productCategoryAdminRepository.ProductExists(productIds, categoryId))
            {
                return categoryHelper.AddCategoryAssociateProducts(categoryId, productIds);
            }
            else
            {
                throw new Exception("The selected product(s) are already associated with this category.");
            }
        }
        #endregion

        #region Private Methods
        private void GetExpands(NameValueCollection expands, Category category)
        {
            if (expands.HasKeys())
            {
                ExpandCategoryNodes(expands, category);
                ExpandProductIds(expands, category);
                ExpandSubcategories(expands, category);
                ExpanCategoryProfiles(expands, category);
            }
        }

        private void ExpanCategoryProfiles(NameValueCollection expands, Category category)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.CategoryProfiles)))
            {
                var categoryProfiles = _categoryProfileRepository.GetByCategoryID(category.CategoryID);

                foreach (var profile in categoryProfiles)
                {
                    category.CategoryProfileCollection.Add(profile);
                }
            }
        }

        private void ExpandCategoryNodes(NameValueCollection expands, Category category)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.CategoryNodes)))
            {
                var categoryNodes = _categoryNodeRepository.GetByCategoryID(category.CategoryID);

                foreach (var node in categoryNodes)
                {
                    category.CategoryNodeCollection.Add(node);
                }
            }
        }

        private void ExpandProductIds(NameValueCollection expands, Category category)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.ProductIds)))
            {
                var productIds = String.Empty;
                var productCategories = _productCategoryRepository.GetByCategoryID(category.CategoryID);
                foreach (var pc in productCategories)
                {
                    productIds += pc.ProductID + ",";
                }

                productIds = productIds.TrimEnd(',');
                category.ProductIds = productIds;
            }
        }

        private void ExpandSubcategories(NameValueCollection expands, Category category)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Subcategories)))
            {
                var categoryNodes = _categoryNodeRepository.GetByCategoryID(category.CategoryID);

                foreach (var node in categoryNodes)
                {
                    var nodes = _categoryNodeRepository.DeepLoadByParentCategoryNodeID(node.CategoryNodeID, true, DeepLoadType.IncludeChildren, typeof(Category));

                    foreach (var catnode in nodes)
                    {
                        _categoryNodeRepository.DeepLoad(catnode, true, DeepLoadType.IncludeChildren, typeof(Category));
                        category.CategoryNodeCollection.Add(catnode);
                    }
                }
            }
        }

        private void SetFiltering(List<Tuple<string, string, string>> filters, CategoryQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;

                if (Equals(filterKey, FilterKeys.ExternalId)) { SetQueryParameter(CategoryColumn.ExternalID, filterOperator, filterValue, query); }
                if (Equals(filterKey, FilterKeys.IsVisible)) { SetQueryParameter(CategoryColumn.VisibleInd, filterOperator, filterValue, query); }
                if (Equals(filterKey, FilterKeys.Name)) { SetQueryParameter(CategoryColumn.Name, filterOperator, filterValue, query); }
                if (Equals(filterKey, FilterKeys.PortalId)) { SetQueryParameter(CategoryColumn.PortalID, filterOperator, filterValue, query); }
                if (Equals(filterKey, FilterKeys.Title)) { SetQueryParameter(CategoryColumn.Title, filterOperator, filterValue, query); }
                if (Equals(filterKey, FilterKeys.CategoryId)) { SetQueryParameter(CategoryColumn.CategoryID, filterOperator, filterValue, query); }
            }
        }

        private void SetFiltering(List<Tuple<string, string, string>> filters, VwZnodeCategoriesCatalogsQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;

                if (Equals(filterKey, FilterKeys.ExternalId)) { SetQueryParameter(VwZnodeCategoriesCatalogsColumn.ExternalID, filterOperator, filterValue, query); }
                if (Equals(filterKey, FilterKeys.IsVisible)) { SetQueryParameter(VwZnodeCategoriesCatalogsColumn.VisibleInd, filterOperator, filterValue, query); }
                if (Equals(filterKey, FilterKeys.Name)) { SetQueryParameter(VwZnodeCategoriesCatalogsColumn.Name, filterOperator, filterValue, query); }
                if (Equals(filterKey, FilterKeys.PortalId)) { SetQueryParameter(VwZnodeCategoriesCatalogsColumn.PortalID, filterOperator, filterValue, query); }
                if (Equals(filterKey, FilterKeys.Title)) { SetQueryParameter(VwZnodeCategoriesCatalogsColumn.Title, filterOperator, filterValue, query); }
            }
        }

        private void SetSorting(NameValueCollection sorts, CategorySortBuilder sortBuilder)
        {
            if (sorts.HasKeys() && sorts.Count > 1)
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);

                    if (Equals(value, SortKeys.CategoryId)) { SetSortDirection(CategoryColumn.CategoryID, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (Equals(value, SortKeys.DisplayOrder)) { SetSortDirection(CategoryColumn.DisplayOrder, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (Equals(value, SortKeys.Name)) { SetSortDirection(CategoryColumn.Name, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (Equals(value, SortKeys.Title)) { SetSortDirection(CategoryColumn.Title, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                }
            }
            else
            {
                SetSortDirection(VwZnodeCategoriesCatalogsColumn.CategoryID, sorts[StoredProcedureKeys.sortDirKey], sortBuilder);
            }
        }

        private void SetSorting(NameValueCollection sorts, VwZnodeCategoriesCatalogsSortBuilder sortBuilder)
        {
            if (sorts.HasKeys() && sorts.Count > 1)
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);

                    if (Equals(value, SortKeys.CategoryId)) { SetSortDirection(VwZnodeCategoriesCatalogsColumn.CategoryID, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (Equals(value, SortKeys.DisplayOrder)) { SetSortDirection(VwZnodeCategoriesCatalogsColumn.DisplayOrder, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (Equals(value, SortKeys.Name)) { SetSortDirection(VwZnodeCategoriesCatalogsColumn.Name, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (Equals(value, SortKeys.Title)) { SetSortDirection(VwZnodeCategoriesCatalogsColumn.Title, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                }
            }
            else
            {
                SetSortDirection(VwZnodeCategoriesCatalogsColumn.CategoryID, sorts[StoredProcedureKeys.sortDirKey], sortBuilder);
            }
        }

        private void SetQueryParameter(CategoryColumn column, string filterOperator, string filterValue, CategoryQuery query)
        {
            base.SetQueryParameter(column, filterOperator, filterValue, query);
        }

        private void SetSortDirection(CategoryColumn column, string value, CategorySortBuilder sortBuilder)
        {
            base.SetSortDirection(column, value, sortBuilder);
        }
        #endregion

        #region Znode Version 8.0

        /// <summary>
        /// Gets list of categories by catalog Id.
        /// </summary>
        /// <param name="catalogId">The Id of catalog.</param>
        /// <returns>Returns list of categories by catalog Id.</returns>
        public CatalogAssociatedCategoriesListModel GetCategoryByCatalogId(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page, out int totalRowCount, int catalogId = 0)
        {
            var model = new CatalogAssociatedCategoriesListModel();
            var sortBuilder = new VwZnodeCategoriesCatalogsSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            string whereClause = filters.Count > 0 ? StringHelper.GenerateDynamicWhereClause(filters) : string.Format("~~CatalogId~~={0}", catalogId);

            SetPaging(page, model, out pagingStart, out pagingLength);
            string orderBy = StringHelper.GenerateDynamicOrderByClause(sorts);
            pagingStart = Convert.ToInt32(page.Get(PageKeys.Index));


            StoredProcedureHelper storedProcedureHelper = new StoredProcedureHelper();
            DataSet resultDataSet = storedProcedureHelper.SP_DataDetailsSpConfig(whereClause, string.Empty/*HavingClause*/, StoredProcedureKeys.SpZNodeCatalogAssociatedCategory, orderBy, pagingStart.ToString(), pagingLength.ToString(), out totalRowCount);

            foreach (DataRow dr in resultDataSet.Tables[0].Rows)
            {
                dr["Name"] = ParsePath(dr["Name"].ToString(), ">");
            }
            model.CategoryList = resultDataSet.Tables[0].ToList<CatalogAssociatedCategoriesModel>().ToCollection();
            model.TotalResults = totalRowCount;
            model.PageSize = pagingLength;
            model.PageIndex = pagingStart;
            // Check to set page size equal to the total number of results
            if (Equals(pagingLength, int.MaxValue))
            {
                model.PageSize = model.TotalResults;
            }

            return model;
        }

        /// <summary>
        /// Returns a bread crumb path from a delimited string
        /// </summary>
        /// <param name="delimitedString">Delimitted string</param>
        /// <param name="separator">Path seperator.</param>
        /// <returns>Returns the parsed category path.</returns>
        private string ParsePath(string delimitedString, string separator)
        {
            StringBuilder categoryPath = new StringBuilder();

            string[] delim1 = { "||" };
            string[] pathItems = delimitedString.Split(delim1, StringSplitOptions.None);

            foreach (string pathItem in pathItems)
            {
                string[] delim2 = { "%%" };
                string[] items = pathItem.Split(delim2, StringSplitOptions.None);

                string categoryId = items.GetValue(0).ToString();
                string categoryName = items.GetValue(1).ToString();
                categoryPath.Append(categoryName);
                categoryPath.Append(string.Format(" {0} ", separator));
            }

            if (categoryPath.Length > 0)
            {
                if (Equals(categoryPath.ToString().LastIndexOf(string.Format(" {0} ", separator)), categoryPath.ToString().Length - 3))
                {
                    categoryPath = categoryPath.Remove(categoryPath.ToString().Length - 3, 3);
                }
            }

            return categoryPath.ToString();
        }

        public CatalogAssociatedCategoriesListModel GetAllCategories(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            List<Tuple<string, string, string>> filterList = new List<Tuple<string, string, string>>();
            bool isManageSSEO = false;
            var model = new CatalogAssociatedCategoriesListModel();
            var sortBuilder = new VwZnodeCategoriesCatalogsSortBuilder();
            int totalRowCount = 0;
            var pagingStart = 0;
            var pagingLength = 0;
            pagingStart = Convert.ToInt32(page.Get(PageKeys.Index));
            pagingLength = Convert.ToInt32(page.Get(PageKeys.Size));
            string orderBy = StringHelper.GenerateDynamicOrderByClause(sorts);
            string innerWhereClause = string.Empty;

            foreach (var item in filters)
            {
                if (Equals(item.Item1, "ismanageseo"))
                {
                    isManageSSEO = true;
                    filters.Remove(item);
                    break;
                }

            }
            foreach (var item in filters)
            {
                if (Equals(item.Item1, "username"))
                {
                    filterList.Add(item);
                    innerWhereClause = StringHelper.GenerateDynamicWhereClause(filterList);
                    filters.Remove(item);
                    break;
                }
            }

            //PRFT Custom code: Start
            foreach (var item in filters)
            {
                if (Equals(item.Item1, "ismanufacturer"))
                {
                    if (item.Item3.Contains("true"))
                    {
                        filters.Add(new Tuple<string, string, string>(FilterKeys.Custom1, FilterOperators.NotEquals, FilterKeys.Null));
                        filters.Remove(item);
                        break;
                    }
                    else
                    {
                        filters.Remove(item);
                        break;
                    }

                }
            }
            //PRFT Custom code: End

            string whereClause = StringHelper.GenerateDynamicWhereClause(filters);
            string spName = isManageSSEO ? StoredProcedureKeys.SPZNode_SearchCategoriesByPortalCatalog : StoredProcedureKeys.SpSearchCategories;
            StoredProcedureHelper storedProcedureHelper = new StoredProcedureHelper();
            DataSet resultDataSet = storedProcedureHelper.SP_DataDetailsSpConfig(whereClause, string.Empty/*HavingClause*/, spName, orderBy, pagingStart.ToString(), pagingLength.ToString(), out totalRowCount, null, innerWhereClause);
            if (!Equals(resultDataSet, null) && resultDataSet.Tables.Count > 0 && !Equals(resultDataSet.Tables[0], null) && resultDataSet.Tables[0].Rows.Count > 0)
            {
                model.CategoryList = resultDataSet.Tables[0].ToList<CatalogAssociatedCategoriesModel>().ToCollection();
            }
            model.TotalResults = totalRowCount;
            model.PageSize = pagingLength;
            model.PageIndex = pagingStart;
            // Check to set page size equal to the total number of results
            if (Equals(pagingLength, int.MaxValue))
            {
                model.PageSize = model.TotalResults;
            }

            return model;
        }

        public CategoryModel UpdateCategorySEODetails(int categoryId, CategoryModel model)
        {
            if (categoryId < 1)
            {
                throw new Exception("Category ID cannot be less than 1.");
            }

            if (Equals(model, null))
            {
                throw new Exception("Category model cannot be null.");
            }

            var category = _categoryRepository.GetByCategoryID(categoryId);
            if (!Equals(category, null))
            {
                // Set the category ID
                model.CategoryId = categoryId;

                model.SeoUrl = model.SeoUrl.Replace(" ", "-");
                string oldSeoUrl = category.SEOURL;
                string newSeoUrl = model.SeoUrl;

                if (!Equals(string.Compare(newSeoUrl, oldSeoUrl, true), 0))
                {
                    if (_urlRedirectAdminRepository.SeoUrlExists(newSeoUrl, category.CategoryID))
                    {
                        model.Custom1 = "103";
                        return model;
                    }
                }

                category.Title = model.Title;
                category.Description = model.Description;
                category.ShortDescription = model.ShortDescription;
                category.SEODescription = model.SeoDescription;
                category.SEOKeywords = model.SeoKeywords;
                category.SEOTitle = model.SeoTitle;
                category.SEOURL = model.SeoUrl;

                var updated = _categoryRepository.Update(category);
                if (updated)
                {
                    //For url redirect - start                    
                    if (Equals(model.RedirectUrlInd, true))
                    {
                        _urlRedirectAdminRepository.AddUrlRedirectTableForMVCAdmin(SEOUrlType.Category, oldSeoUrl, newSeoUrl, model.CategoryId.ToString(), category.Name);
                    }

                    if (!Equals(category, null))
                    {
                        return CategoryMap.ToModel(category);
                    }
                }
            }

            return null;
        }

        public ProductListModel GetCategoryAssociatedProducts(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts = null, NameValueCollection page = null)
        {
            var model = new ProductListModel();
            string whereClause = StringHelper.GenerateDynamicWhereClause(filters);
            string orderBy = string.Empty;
            if (!Equals(sorts, null))
            {
                orderBy = StringHelper.GenerateDynamicOrderByClause(sorts);
            }
            int totalRowCount = 0;
            int pagingStart = 0;

            var pagingLength = 0;
            SetPaging(page, model, out pagingStart, out pagingLength);
            pagingStart = Convert.ToInt32(page.Get(PageKeys.Index));
            if (Equals(pagingStart, 0))
            {
                pagingStart = 1;
            }
            StoredProcedureHelper storedProcedureHelper = new StoredProcedureHelper();
            DataSet resultDataSet = storedProcedureHelper.SP_DataDetailsSpConfig(whereClause, null, StoredProcedureKeys.SpGetCategoryProducts, orderBy, pagingStart.ToString(), pagingLength.ToString(), out totalRowCount);
            model.Products = resultDataSet.Tables[0].ToList<ProductModel>().ToCollection();
            model.TotalResults = totalRowCount;
            model.PageSize = pagingLength;
            model.PageIndex = pagingStart;
            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                model.PageSize = model.TotalResults;
            }
            return model;
        }

        #endregion
    }
}
