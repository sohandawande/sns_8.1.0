﻿namespace Znode.Engine.Services.Constants
{
	public static class PageKeys
	{
		public static string Index = "index";
		public static string Size = "size";
	}
}
