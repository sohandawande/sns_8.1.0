﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using PaymentTypeRepository = ZNode.Libraries.DataAccess.Service.PaymentTypeService;

namespace Znode.Engine.Services
{
	public class PaymentTypeService : BaseService, IPaymentTypeService
	{
		private readonly PaymentTypeRepository _paymentTypeRepository;

		public PaymentTypeService()
		{
			_paymentTypeRepository = new PaymentTypeRepository();
		}

		public PaymentTypeModel GetPaymentType(int paymentTypeId)
		{
			var paymentType = _paymentTypeRepository.GetByPaymentTypeID(paymentTypeId);
			return PaymentTypeMap.ToModel(paymentType);
		}

		public PaymentTypeListModel GetPaymentTypes(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
		{
			var model = new PaymentTypeListModel();
			var paymentTypes = new TList<PaymentType>();

			var query = new PaymentTypeQuery();
			var sortBuilder = new PaymentTypeSortBuilder();
			var pagingStart = 0;
			var pagingLength = 0;

			SetFiltering(filters, query);
			SetSorting(sorts, sortBuilder);
			SetPaging(page, model, out pagingStart, out pagingLength);

			// Get the initial set
			var totalResults = 0;
			paymentTypes = _paymentTypeRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
			model.TotalResults = totalResults;

			// Check to set page size equal to the total number of results
			if (pagingLength == int.MaxValue)
			{
				model.PageSize = model.TotalResults;
			}

			// Map each item and add to the list
			foreach (var p in paymentTypes)
			{
				model.PaymentTypes.Add(PaymentTypeMap.ToModel(p));
			}

			return model;
		}

		public PaymentTypeModel CreatePaymentType(PaymentTypeModel model)
		{
			if (model == null)
			{
				throw new Exception("Payment type model cannot be null.");
			}

			var entity = PaymentTypeMap.ToEntity(model);
			var paymentType = _paymentTypeRepository.Save(entity);
			return PaymentTypeMap.ToModel(paymentType);
		}

		public PaymentTypeModel UpdatePaymentType(int paymentTypeId, PaymentTypeModel model)
		{
			if (paymentTypeId < 1)
			{
				throw new Exception("Payment type ID cannot be less than 1.");
			}

			if (model == null)
			{
				throw new Exception("Payment type model cannot be null.");
			}

			var paymentType = _paymentTypeRepository.GetByPaymentTypeID(paymentTypeId);
			if (paymentType != null)
			{
				// Set the payment type ID
				model.PaymentTypeId = paymentTypeId;

				var paymentTypeToUpdate = PaymentTypeMap.ToEntity(model);

				var updated = _paymentTypeRepository.Update(paymentTypeToUpdate);
				if (updated)
				{
					paymentType = _paymentTypeRepository.GetByPaymentTypeID(paymentTypeId);
					return PaymentTypeMap.ToModel(paymentType);
				}
			}

			return null;
		}

		public bool DeletePaymentType(int paymentTypeId)
		{
			if (paymentTypeId < 1)
			{
				throw new Exception("Payment type ID cannot be less than 1.");
			}

			var paymentType = _paymentTypeRepository.GetByPaymentTypeID(paymentTypeId);
			if (paymentType != null)
			{
				return _paymentTypeRepository.Delete(paymentType);
			}

			return false;
		}

		private void SetFiltering(List<Tuple<string, string, string>> filters, PaymentTypeQuery query)
		{
			foreach (var tuple in filters)
			{
				var filterKey = tuple.Item1;
				var filterOperator = tuple.Item2;
				var filterValue = tuple.Item3;

				if (filterKey == FilterKeys.IsActive) SetQueryParameter(PaymentTypeColumn.ActiveInd, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.Name) SetQueryParameter(PaymentTypeColumn.Name, filterOperator, filterValue, query);
			}
		}

		private void SetSorting(NameValueCollection sorts, PaymentTypeSortBuilder sortBuilder)
		{
			if (sorts.HasKeys())
			{
				foreach (var key in sorts.AllKeys)
				{
					var value = sorts.Get(key);

					if (key == SortKeys.Name) SetSortDirection(PaymentTypeColumn.Name, value, sortBuilder);
					if (key == SortKeys.PaymentTypeId) SetSortDirection(PaymentTypeColumn.PaymentTypeID, value, sortBuilder);
				}
			}
		}

		private void SetQueryParameter(PaymentTypeColumn column, string filterOperator, string filterValue, PaymentTypeQuery query)
		{
			base.SetQueryParameter(column, filterOperator, filterValue, query);
		}

		private void SetSortDirection(PaymentTypeColumn column, string value, PaymentTypeSortBuilder sortBuilder)
		{
			base.SetSortDirection(column, value, sortBuilder);
		}
	}
}
