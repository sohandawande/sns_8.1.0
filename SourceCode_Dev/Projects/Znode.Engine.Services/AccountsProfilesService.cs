﻿using System;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Maps;
using ZNode.Libraries.DataAccess.Custom;
using AccountProfileRepository = ZNode.Libraries.DataAccess.Service.AccountProfileService;

namespace Znode.Engine.Services
{
    public class AccountsProfilesService : BaseService , IAccountsProfilesService
    {
        #region Private Variables

        private readonly AccountProfileRepository _accountProfileRepository;
        #endregion

        #region Public Methods

        public AccountsProfilesService()
        {
            _accountProfileRepository = new AccountProfileRepository();
        }

        public bool AccountAssociatedProfiles(int accountId, string profileIds)
        {
            if (accountId < 1)
            {
                throw new Exception("Category ID cannot be less than 1.");
            }

            var customerHelper = new CustomerHelper();
            var isSuccess = customerHelper.InsertAccountAssociatedProfiles(accountId, profileIds);

            return isSuccess;
        }

        public bool DeleteAccountProfile(int accountProfileId)
        {
            if (accountProfileId < 1)
            {
                throw new Exception("Account Profile ID cannot be less than 1.");
            }

            var accountProfile = _accountProfileRepository.GetByAccountProfileID(accountProfileId);
            if (!Equals(accountProfile , null))
            {
                return _accountProfileRepository.Delete(accountProfile);
            }

            return false;
        }
        #endregion
    }
}
