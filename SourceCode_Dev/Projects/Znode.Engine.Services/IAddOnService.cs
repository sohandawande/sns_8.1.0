﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    /// <summary>
    /// AddOn Service Interface
    /// </summary>
    public interface IAddOnService
    {
        /// <summary>
        /// Get AddOn By AddOn id and expand
        /// </summary>
        /// <param name="AddOnId">int addOnId</param>
        /// <param name="expands">NameValueCollection expands</param>
        /// <returns>Returns the model of AddOn</returns>
        AddOnModel GetAddOn(int addOnId, NameValueCollection expands);

        /// <summary>
        /// Get list of Addons
        /// </summary>
        /// <param name="expands">NameValueCollection expands</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">NameValueCollection sorts</param>
        /// <param name="page">NameValueCollection page</param>
        /// <returns>Returns list of AddOns</returns>
        AddOnListModel GetAddOns(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Create AddOns
        /// </summary>
        /// <param name="model">AddOnModel model</param>
        /// <returns>Returns the model of AddOn</returns>
        AddOnModel CreateAddOn(AddOnModel model);

        /// <summary>
        /// Update AddOn
        /// </summary>
        /// <param name="addOnId">int addOnId</param>
        /// <param name="model">AddOnModel model</param>
        /// <returns>Returns the model of AddOn</returns>
        AddOnModel UpdateAddOn(int addOnId, AddOnModel model);

        /// <summary>
        /// Delete AddOn
        /// </summary>
        /// <param name="addOnId">int addOnId</param>
        /// <returns>Returns true or false</returns>
        bool DeleteAddOn(int addOnId);
    }
}
