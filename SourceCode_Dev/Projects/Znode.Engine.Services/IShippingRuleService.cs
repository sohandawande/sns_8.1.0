﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    /// <summary>
    /// Shipping Rule Service Interface
    /// </summary>
	public interface IShippingRuleService
	{
        /// <summary>
        /// Get Shipping Rule
        /// </summary>
        /// <param name="shippingRuleId">shippingRuleId</param>
        /// <param name="expands">expands</param>
        /// <returns>Returns ShippingRuleModel</returns>
		ShippingRuleModel GetShippingRule(int shippingRuleId, NameValueCollection expands);
		
        /// <summary>
        /// Get Shipping Rules
        /// </summary>
        /// <param name="expands">expands</param>
        /// <param name="filters">filters</param>
        /// <param name="sorts">sorts</param>
        /// <param name="page">page</param>
        /// <returns>Returns ShippingRuleListModel</returns>
        ShippingRuleListModel GetShippingRules(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
		
        /// <summary>
        /// Create Shipping Rule
        /// </summary>
        /// <param name="model">ShippingRuleModel</param>
        /// <returns>Returns ShippingRuleModel</returns>
        ShippingRuleModel CreateShippingRule(ShippingRuleModel model);

        /// <summary>
        /// Update Shipping Rule
        /// </summary>
        /// <param name="shippingRuleId">shippingRuleId</param>
        /// <param name="model">ShippingRuleModel</param>
        /// <returns>Returns ShippingRuleModel</returns>
		ShippingRuleModel UpdateShippingRule(int shippingRuleId, ShippingRuleModel model);

        /// <summary>
        /// Delete Shipping Rule
        /// </summary>
        /// <param name="shippingRuleId">shippingRuleId</param>
        /// <returns>Returns True/False</returns>
		bool DeleteShippingRule(int shippingRuleId);
	}
}
