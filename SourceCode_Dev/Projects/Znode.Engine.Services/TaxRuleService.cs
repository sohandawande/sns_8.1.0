﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Helpers.Constants;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using TaxClassRepository = ZNode.Libraries.DataAccess.Service.TaxClassService;
using TaxRuleRepository = ZNode.Libraries.DataAccess.Service.TaxRuleService;
using TaxRuleTypeRepository = ZNode.Libraries.DataAccess.Service.TaxRuleTypeService;

namespace Znode.Engine.Services
{
    /// <summary>
    /// Tax Rule Service
    /// </summary>
    public class TaxRuleService : BaseService, ITaxRuleService
    {
        #region Private Variables
        private readonly TaxClassRepository _taxClassRepository;
        private readonly TaxRuleRepository _taxRuleRepository;
        private readonly TaxRuleTypeRepository _taxRuleTypeRepository;
        #endregion

        #region Constructor
        public TaxRuleService()
        {
            _taxClassRepository = new TaxClassRepository();
            _taxRuleRepository = new TaxRuleRepository();
            _taxRuleTypeRepository = new TaxRuleTypeRepository();
        }
        #endregion

        #region Public Methods
        public TaxRuleModel GetTaxRule(int taxRuleId, NameValueCollection expands)
        {
            var taxRule = _taxRuleRepository.GetByTaxRuleID(taxRuleId);
            if (!Equals(taxRule, null))
            {
                GetExpands(expands, taxRule);
            }

            return TaxRuleMap.ToModel(taxRule);
        }

        public TaxRuleListModel GetTaxRules(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new TaxRuleListModel();
            var taxRules = new TList<TaxRule>();
            var tempList = new TList<TaxRule>();

            var query = new TaxRuleQuery();
            var sortBuilder = new TaxRuleSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            tempList = _taxRuleRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (Equals(pagingLength, int.MaxValue))
            {
                model.PageSize = model.TotalResults;
            }

            // Now go through the list and get any expands
            foreach (var taxRule in tempList)
            {
                GetExpands(expands, taxRule);
                taxRules.Add(taxRule);
            }

            // Map each item and add to the list
            foreach (var t in taxRules)
            {
                model.TaxRules.Add(TaxRuleMap.ToModel(t));
            }

            return model;
        }

        public TaxRuleModel CreateTaxRule(TaxRuleModel model)
        {
            if (Equals(model, null))
            {
                throw new Exception("Tax rule model cannot be null.");
            }

            var entity = TaxRuleMap.ToEntity(model);
            var taxRule = _taxRuleRepository.Save(entity);
            return TaxRuleMap.ToModel(taxRule);
        }

        public TaxRuleModel UpdateTaxRule(int taxRuleId, TaxRuleModel model)
        {
            if (taxRuleId < 1)
            {
                throw new Exception("Tax rule ID cannot be less than 1.");
            }

            if (Equals(model, null))
            {
                throw new Exception("Tax rule model cannot be null.");
            }

            var taxRule = _taxRuleRepository.GetByTaxRuleID(taxRuleId);
            if (!Equals(taxRule, null))
            {
                // Set the tax rule ID and map
                model.TaxRuleId = taxRuleId;
                var taxRuleToUpdate = TaxRuleMap.ToEntity(model);

                var updated = _taxRuleRepository.Update(taxRuleToUpdate);
                if (updated)
                {
                    taxRule = _taxRuleRepository.GetByTaxRuleID(taxRuleId);
                    return TaxRuleMap.ToModel(taxRule);
                }
            }

            return null;
        }

        public bool DeleteTaxRule(int taxRuleId)
        {
            if (taxRuleId < 1)
            {
                throw new Exception("Tax rule ID cannot be less than 1.");
            }

            var taxRule = _taxRuleRepository.GetByTaxRuleID(taxRuleId);
            if (!Equals(taxRule, null))
            {
                return _taxRuleRepository.Delete(taxRule);
            }

            return false;
        }
        #endregion

        #region Private Methods
        private void GetExpands(NameValueCollection expands, TaxRule taxRule)
        {
            if (expands.HasKeys())
            {
                ExpandTaxClass(expands, taxRule);
                ExpandTaxRuleType(expands, taxRule);
            }
        }

        private void ExpandTaxClass(NameValueCollection expands, TaxRule taxRule)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.TaxClass)))
            {
                if (taxRule.TaxClassID.HasValue)
                {
                    var taxClass = _taxClassRepository.GetByTaxClassID(taxRule.TaxClassID.Value);
                    if (!Equals(taxClass, null))
                    {
                        taxRule.TaxClassIDSource = taxClass;
                    }
                }
            }
        }

        private void ExpandTaxRuleType(NameValueCollection expands, TaxRule taxRule)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.TaxRuleType)))
            {
                if (taxRule.TaxRuleTypeID.HasValue)
                {
                    var taxRuleType = _taxRuleTypeRepository.GetByTaxRuleTypeID(taxRule.TaxRuleTypeID.Value);
                    if (!Equals(taxRuleType, null))
                    {
                        taxRule.TaxRuleTypeIDSource = taxRuleType;
                    }
                }
            }
        }

        private void SetFiltering(List<Tuple<string, string, string>> filters, TaxRuleQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;

                if (Equals(filterKey, FilterKeys.CountryCode)) { SetQueryParameter(TaxRuleColumn.DestinationCountryCode, filterOperator, filterValue, query); }
                if (Equals(filterKey, FilterKeys.ExternalId)) { SetQueryParameter(TaxRuleColumn.ExternalID, filterOperator, filterValue, query); }
                if (Equals(filterKey, FilterKeys.PortalId)) { SetQueryParameter(TaxRuleColumn.PortalID, filterOperator, filterValue, query); }
                if (Equals(filterKey, FilterKeys.StateCode)) { SetQueryParameter(TaxRuleColumn.DestinationStateCode, filterOperator, filterValue, query); }
                if (Equals(filterKey, FilterKeys.TaxClassId)) { SetQueryParameter(TaxRuleColumn.TaxClassID, filterOperator, filterValue, query); }
                if (Equals(filterKey, FilterKeys.TaxRuleTypeId)) { SetQueryParameter(TaxRuleColumn.TaxRuleTypeID, filterOperator, filterValue, query); }
            }
        }

        private void SetSorting(NameValueCollection sorts, TaxRuleSortBuilder sortBuilder)
        {
            if (sorts.HasKeys())
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);

                    if (string.IsNullOrEmpty(value)){SetSortDirection(TaxRuleColumn.Precedence, "asc", sortBuilder);}

                    if (Equals(value, SortKeys.CountryCode)) { SetSortDirection(TaxRuleColumn.DestinationCountryCode, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (Equals(value, SortKeys.Gst)) { SetSortDirection(TaxRuleColumn.GST, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (Equals(value, SortKeys.Hst)) { SetSortDirection(TaxRuleColumn.HST, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (Equals(value, SortKeys.Precedence)) { SetSortDirection(TaxRuleColumn.Precedence, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (Equals(value, SortKeys.Pst)) { SetSortDirection(TaxRuleColumn.PST, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (Equals(value, SortKeys.SalesTax)) { SetSortDirection(TaxRuleColumn.SalesTax, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (Equals(value, SortKeys.StateCode)) { SetSortDirection(TaxRuleColumn.DestinationStateCode, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (Equals(value, SortKeys.TaxRuleId)) { SetSortDirection(TaxRuleColumn.TaxRuleID, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (Equals(value, SortKeys.Vat)) { SetSortDirection(TaxRuleColumn.VAT, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (Equals(value, SortKeys.CountyFips)) { SetSortDirection(TaxRuleColumn.CountyFIPS, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                }
            }
        }

        private void SetQueryParameter(TaxRuleColumn column, string filterOperator, string filterValue, TaxRuleQuery query)
        {
            base.SetQueryParameter(column, filterOperator, filterValue, query);
        }

        private void SetSortDirection(TaxRuleColumn column, string value, TaxRuleSortBuilder sortBuilder)
        {
            base.SetSortDirection(column, value, sortBuilder);
        }
        #endregion
    }
}
