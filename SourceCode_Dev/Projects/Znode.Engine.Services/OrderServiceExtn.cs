﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Libraries.Helpers.Extensions;
using ZNode.Libraries.ECommerce.Analytics;
using ZNode.Libraries.ECommerce.Fulfillment;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.Framework.Business;
using OrderHelperRepository = ZNode.Libraries.DataAccess.Custom.OrderHelper;

namespace Znode.Engine.Services
{
    public partial class OrderService
    {
        //PRFT Custom Code: Start
        public string apiUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
        //PRFT custom Code : End
        public string GetEcommerceTrackingOrderReceiptJavascript(OrderModel orderModel)
        {
            string orderReceiptJavascript = "";

            var googleAnalytics = new ZNodeGoogleAnalyticsService();

            var orderEntity = Maps.OrderMap.ToEntity(orderModel);

            orderReceiptJavascript = googleAnalytics.GetOrderReceiptEcommerceTrackingJavascriptCode(orderEntity);

            return orderReceiptJavascript;
        }

        public PRFTOrderSubmissionModel ReSubmitOrderToERP(int OrderID)
        {
            var OrderSubmissionModel = new PRFTOrderSubmissionModel();
            DataSet resultDataSet = null;
            OrderModel model = new OrderModel();
            OrderHelperRepository orderHelper = new OrderHelperRepository();
            resultDataSet = orderHelper.GetOrderDetailsByOrderId(OrderID);
            model = resultDataSet.Tables[0].Rows[0].ToSingleRow<OrderModel>();
            model.OrderLineItemList = new OrderLineItemListModel();
            model.OrderLineItemList.OrdersLineItems = resultDataSet.Tables[1].ToList<OrderLineItemModel>().ToCollection();


            PRFTERPOrderServices _orderERPService = new PRFTERPOrderServices();
            var response = _orderERPService.ReSubmitOrderToERP(model);

            if (response.HasError == false && !string.IsNullOrEmpty(response.NewOrderID))
            {
                UpdateOrderExternalId(model.OrderId, response.NewOrderID.ToString());
                OrderSubmissionModel.IsSubmitted = true;
            }
            else
            {
                OrderSubmissionModel.IsSubmitted = false;
                OrderSubmissionModel.ErrorMessage = response.StatusDescription;
                //Send Email to Admin
                SendFailuerEmail(model, response.StatusDescription);
            }
            return OrderSubmissionModel;
        }

        public void SendFailuerEmail(ZNodeOrderFulfillment order, ZNodePortalCart shoppingCart, string errorMessage)
        {
            string recepientEmail = string.Empty;
            try
            {
                recepientEmail = ZNodeConfigManager.SiteConfig.AdminEmail;
                string senderEmail = ZNodeConfigManager.SiteConfig.CustomerServiceEmail;

                if (string.IsNullOrEmpty(senderEmail))
                {
                    throw new Exception("Sender email address (Customer service email) is empty. ");
                }

                if (string.IsNullOrEmpty(recepientEmail))
                {
                    throw new Exception("Recepient email address is empty.");
                }

                //string subject = "Track your package";
                string subject = HttpContext.GetGlobalResourceObject("CommonCaption", "ERPOrderSubmissionFailedSubject").ToString();
                string defaultTemplatePath = HttpContext.Current.Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, "ERPOrderFailedTemplate.html"));
                if (File.Exists(defaultTemplatePath))
                {
                    // Get message text.
                    StreamReader rw = new StreamReader(defaultTemplatePath);
                    string messageText = rw.ReadToEnd();

                    messageText = GetOrderToERPFailedTemplate(messageText, order, shoppingCart, errorMessage);

                    ZNode.Libraries.Framework.Business.ZNodeEmail.SendEmail(recepientEmail, senderEmail, senderEmail, subject, messageText, true);
                }
            }
            catch (Exception)
            {
                throw new Exception("There was a problem sending the email to " + recepientEmail);
            }
        }

        public void SendFailuerEmail(OrderModel order, string errorMessage)
        {
            string recepientEmail = string.Empty;
            try
            {
                recepientEmail = ZNodeConfigManager.SiteConfig.AdminEmail;
                string senderEmail = ZNodeConfigManager.SiteConfig.CustomerServiceEmail;

                if (string.IsNullOrEmpty(senderEmail))
                {
                    throw new Exception("Sender email address (Customer service email) is empty. ");
                }

                if (string.IsNullOrEmpty(recepientEmail))
                {
                    throw new Exception("Recepient email address is empty.");
                }

                //string subject = "Track your package";
                string subject = HttpContext.GetGlobalResourceObject("CommonCaption", "ERPOrderSubmissionFailedSubject").ToString();
                string defaultTemplatePath = HttpContext.Current.Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, "ERPOrderFailedTemplate.html"));
                if (File.Exists(defaultTemplatePath))
                {
                    // Get message text.
                    StreamReader rw = new StreamReader(defaultTemplatePath);
                    string messageText = rw.ReadToEnd();

                    messageText = GetOrderToERPFailedTemplate(messageText, order, errorMessage);
                    //For fetching  shippingTypeID on the basis of ordLineItemID - End

                    ZNode.Libraries.Framework.Business.ZNodeEmail.SendEmail(recepientEmail, senderEmail, senderEmail, subject, messageText, true);

                }
            }
            catch (Exception)
            {
                throw new Exception("There was a problem sending the email to " + recepientEmail);
            }

        }
        private string GetOrderToERPFailedTemplate(string messageText, ZNodeOrderFulfillment order, ZNodePortalCart shoppingCart, string errorMessage)
        {

            string logoLink = ConfigurationManager.AppSettings["DemoWebsiteUrl"];
            Regex _logoLink = new Regex("#LogoLink#", RegexOptions.IgnoreCase);
            messageText = _logoLink.Replace(messageText, logoLink);

            string LogoPath = ZNodeConfigManager.SiteConfig.LogoPath.Trim().Replace("~/", "");
            Regex storeLogo = new Regex("#StoreLogo#", RegexOptions.IgnoreCase);
            messageText = storeLogo.Replace(messageText, apiUrl + "/" + LogoPath.TrimStart('~'));

            Regex logo = new Regex("#LOGO#", RegexOptions.IgnoreCase);
            messageText = logo.Replace(messageText, ZNodeConfigManager.SiteConfig.StoreName);

            Regex _order = new Regex("#Order#", RegexOptions.IgnoreCase);
            messageText = _order.Replace(messageText, order.OrderID.ToString());

            Regex customerid = new Regex("#ZNodeCustomerid#", RegexOptions.IgnoreCase);
            messageText = customerid.Replace(messageText, order.AccountID.ToString());

            Regex erpCustomerid = new Regex("#ERPCustomerid#", RegexOptions.IgnoreCase);
            messageText = erpCustomerid.Replace(messageText, order.Custom1);

            Regex emailId = new Regex("#EmailAddress#", RegexOptions.IgnoreCase);
            messageText = emailId.Replace(messageText, order.Email);

            Regex phoneNumber = new Regex("#PhoneNumber#", RegexOptions.IgnoreCase);
            messageText = phoneNumber.Replace(messageText, string.IsNullOrEmpty(order.ShippingAddress.PhoneNumber) ? string.Empty : order.ShippingAddress.PhoneNumber);

            Regex total = new Regex("#Total#", RegexOptions.IgnoreCase);
            messageText = total.Replace(messageText, order.Total.ToString("N2"));

            Regex errorMessageRegex = new Regex("#ErrorMessage#", RegexOptions.IgnoreCase);
            messageText = errorMessageRegex.Replace(messageText, errorMessage);

            Regex mailingInfo = new Regex("#MailingInfo#", RegexOptions.IgnoreCase);
            messageText = mailingInfo.Replace(messageText, ZNodeConfigManager.SiteConfig.CustomerServiceEmail);

            Regex supportPhone = new Regex("#SupportPhone#", RegexOptions.IgnoreCase);
            messageText = supportPhone.Replace(messageText, ZNodeConfigManager.SiteConfig.CustomerServicePhoneNumber);

            Regex supportEmail = new Regex("#SupportEmail#", RegexOptions.IgnoreCase);
            messageText = supportEmail.Replace(messageText, ZNodeConfigManager.SiteConfig.SalesEmail);
            return messageText;
        }

        private string GetOrderToERPFailedTemplate(string messageText, OrderModel order, string errorMessage)
        {

            string logoLink = ConfigurationManager.AppSettings["DemoWebsiteUrl"];
            Regex _logoLink = new Regex("#LogoLink#", RegexOptions.IgnoreCase);
            messageText = _logoLink.Replace(messageText, logoLink);

            string LogoPath = ZNodeConfigManager.SiteConfig.LogoPath.Trim().Replace("~/", "");
            Regex storeLogo = new Regex("#StoreLogo#", RegexOptions.IgnoreCase);
            messageText = storeLogo.Replace(messageText, apiUrl + "/" + LogoPath.TrimStart('~'));

            Regex logo = new Regex("#LOGO#", RegexOptions.IgnoreCase);
            messageText = logo.Replace(messageText, ZNodeConfigManager.SiteConfig.StoreName);

            Regex _order = new Regex("#Order#", RegexOptions.IgnoreCase);
            messageText = _order.Replace(messageText, order.OrderId.ToString());

            Regex customerid = new Regex("#ZNodeCustomerid#", RegexOptions.IgnoreCase);
            messageText = customerid.Replace(messageText, order.AccountId.ToString());

            Regex erpCustomerid = new Regex("#ERPCustomerid#", RegexOptions.IgnoreCase);
            messageText = erpCustomerid.Replace(messageText, order.Custom1);

            var account = _accountRepository.GetByAccountID(order.AccountId.Value);

            Regex emailId = new Regex("#EmailAddress#", RegexOptions.IgnoreCase);
            messageText = emailId.Replace(messageText, (account != null && !string.IsNullOrEmpty(account.Email)) ? account.Email : string.Empty);

            Regex phoneNumber = new Regex("#PhoneNumber#", RegexOptions.IgnoreCase);
            messageText = phoneNumber.Replace(messageText, string.IsNullOrEmpty(order.ShipToPhoneNumber) ? string.Empty : order.ShipToPhoneNumber);

            Regex total = new Regex("#Total#", RegexOptions.IgnoreCase);
            messageText = total.Replace(messageText, ((decimal)order.Total).ToString("N2"));

            Regex errorMessageRegex = new Regex("#ErrorMessage#", RegexOptions.IgnoreCase);
            messageText = errorMessageRegex.Replace(messageText, errorMessage);

            Regex mailingInfo = new Regex("#MailingInfo#", RegexOptions.IgnoreCase);
            messageText = mailingInfo.Replace(messageText, ZNodeConfigManager.SiteConfig.CustomerServiceEmail);

            Regex supportPhone = new Regex("#SupportPhone#", RegexOptions.IgnoreCase);
            messageText = supportPhone.Replace(messageText, ZNodeConfigManager.SiteConfig.CustomerServicePhoneNumber);

            Regex supportEmail = new Regex("#SupportEmail#", RegexOptions.IgnoreCase);
            messageText = supportEmail.Replace(messageText, ZNodeConfigManager.SiteConfig.SalesEmail);
            return messageText;
        }
    }
}
