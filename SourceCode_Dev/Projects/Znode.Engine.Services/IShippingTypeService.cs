﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    /// <summary>
    /// Shipping Type Service Interface
    /// </summary>
	public interface IShippingTypeService
	{
        /// <summary>
        /// Get Shipping Type on the basis of shipping type id
        /// </summary>
        /// <param name="shippingTypeId">shippingTypeId</param>
        /// <returns>Returns ShippingTypeModel</returns>
		ShippingTypeModel GetShippingType(int shippingTypeId);

        /// <summary>
        /// Get list of shipping types
        /// </summary>
        /// <param name="filters">filters</param>
        /// <param name="sorts">NameValueCollection sorts</param>
        /// <param name="page">NameValueCollection page</param>
        /// <returns>Returns ShippingTypeListModel</returns>
		ShippingTypeListModel GetShippingTypes(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Create Shipping Type
        /// </summary>
        /// <param name="model">ShippingTypeModel model</param>
        /// <returns>Returns ShippingTypeModel</returns>
		ShippingTypeModel CreateShippingType(ShippingTypeModel model);

        /// <summary>
        /// Update Shipping Type on the basis of shipping type id
        /// </summary>
        /// <param name="shippingTypeId">shippingTypeId</param>
        /// <param name="model">ShippingTypeModel model</param>
        /// <returns>Returns ShippingTypeModel</returns>
		ShippingTypeModel UpdateShippingType(int shippingTypeId, ShippingTypeModel model);

        /// <summary>
        /// Delete Shipping Type on the basis of shipping type id
        /// </summary>
        /// <param name="shippingTypeId">shippingTypeId</param>
        /// <returns>Returns true/false</returns>
		bool DeleteShippingType(int shippingTypeId);
        
        /// <summary>
        /// ZNode Version 8.0
        /// Get all Shipping Types not present in database.
        /// </summary>
        /// <returns>Return Shipping Type List Model</returns>
        ShippingTypeListModel GetAllShippingTypesNotInDatabase();
	}
}
