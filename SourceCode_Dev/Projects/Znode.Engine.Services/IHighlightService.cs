﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
	public interface IHighlightService
	{
        /// <summary>
        /// Get highlight by highlight id.
        /// </summary>
        /// <param name="highlightId">Id of the highlight.</param>
        /// <param name="expands">Expands for highlight.</param>
        /// <returns>Highlight model</returns>
		HighlightModel GetHighlight(int highlightId, NameValueCollection expands);

        /// <summary>
        /// Get the highlight list.
        /// </summary>
        /// <param name="expands">Expands for highlight.</param>
        /// <param name="filters">Filters for highlight.</param>
        /// <param name="sorts">Sorts for highlight.</param>
        /// <param name="page">Page for highlight.</param>
        /// <returns>List of highlight.</returns>
		HighlightListModel GetHighlights(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Create the highlight.
        /// </summary>
        /// <param name="model">Model to create.</param>
        /// <returns>Created model</returns>
		HighlightModel CreateHighlight(HighlightModel model);

        /// <summary>
        /// Update the existing highlight.
        /// </summary>
        /// <param name="highlightId">Id of the highlight.</param>
        /// <param name="model">Updated model.</param>
        /// <returns>Updated model</returns>
		HighlightModel UpdateHighlight(int highlightId, HighlightModel model);

        /// <summary>
        /// Delete the existing highlight.
        /// </summary>
        /// <param name="highlightId">Id of the highlight.</param>
        /// <returns>true or false.</returns>
		bool DeleteHighlight(int highlightId);

        /// <summary>
        /// Check the Associated product with highlight and sets the property of it.
        /// </summary>
        /// <param name="highlightId">int highlightId</param>
        /// <returns>HighlightModel</returns>
        HighlightModel CheckAssociateProduct(int highlightId);
	}
}
