﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using EnvironmentConfigRepository = ZNode.Libraries.Framework.Business.ZNodeEnvironmentConfig;

namespace Znode.Engine.Services
{
    public class EnvironmentConfigService:BaseService,IEnvironmentConfigService
    {
        #region Private Variables
        private readonly EnvironmentConfigRepository _environmentConfigRepository;
        #endregion

        public EnvironmentConfigService()
        {
            _environmentConfigRepository = new EnvironmentConfigRepository();
        }

        public EnvironmentConfigModel GetEnvironmentConfig()
        {
            EnvironmentConfigModel environmentConfigModel = new EnvironmentConfigModel();
            environmentConfigModel.DataPath = _environmentConfigRepository.DataPath;
            environmentConfigModel.ApplicationPath = _environmentConfigRepository.ApplicationPath;
            environmentConfigModel.ConfigPath = _environmentConfigRepository.ConfigPath;
            environmentConfigModel.ContentPath = _environmentConfigRepository.ContentPath;
            environmentConfigModel.CrossSellImagePath = _environmentConfigRepository.CrossSellImagePath;
            environmentConfigModel.HostPrefix = _environmentConfigRepository.HostPrefix;
            environmentConfigModel.ImagePath = _environmentConfigRepository.ImagePath;
            environmentConfigModel.LargeImagePath = _environmentConfigRepository.LargeImagePath;
            environmentConfigModel.MediumImagePath = _environmentConfigRepository.MediumImagePath;
            environmentConfigModel.MultiStoreAdminEnabled = _environmentConfigRepository.MultiStoreAdminEnabled;
            environmentConfigModel.OriginalImagePath = _environmentConfigRepository.OriginalImagePath;
            environmentConfigModel.SmallImagePath = _environmentConfigRepository.SmallImagePath;
            environmentConfigModel.SwatchImagePath = _environmentConfigRepository.SwatchImagePath;
            environmentConfigModel.ThumbnailImagePath = _environmentConfigRepository.ThumbnailImagePath;

            return environmentConfigModel;
        }
    }
}
