﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
	public interface ISupplierService
	{
        /// <summary>
        /// Get the supplier.
        /// </summary>
        /// <param name="supplierId">Id of the Supplier.</param>
        /// <param name="expands">Expands for supplier.</param>
        /// <returns>Supplier model.</returns>
		SupplierModel GetSupplier(int supplierId, NameValueCollection expands);

        /// <summary>
        /// Get the supplier list.
        /// </summary>
        /// <param name="expands">expands for supplier.</param>
        /// <param name="filters">filters for supplier.</param>
        /// <param name="sorts">sorts for supplier.</param>
        /// <param name="page">page for supplier.</param>
        /// <returns>list of suppliers.</returns>
		SupplierListModel GetSuppliers(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Create the Supplier.
        /// </summary>
        /// <param name="model">model to create.</param>
        /// <returns>returns Supplier model.</returns>
		SupplierModel CreateSupplier(SupplierModel model);

        /// <summary>
        /// Update the existing supplier.
        /// </summary>
        /// <param name="supplierId">Id of the Supplier.</param>
        /// <param name="model">updated model.</param>
        /// <returns>updated model.</returns>
		SupplierModel UpdateSupplier(int supplierId, SupplierModel model);

        /// <summary>
        /// Delete the Existing supplier and outs the error code if it is associated with any product or sku.
        /// </summary>
        /// <param name="supplierId">Id of the supplier.</param>
        /// <param name="errorCode">error code.</param>
        /// <returns></returns>
        bool DeleteSupplier(int supplierId);
	}
}
