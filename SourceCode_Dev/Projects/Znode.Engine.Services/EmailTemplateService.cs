﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Libraries.Helpers.Extensions;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Services
{
    public class EmailTemplateService : BaseService, IEmailTemplateService
    {
        string[] extensions = !Equals(ConfigurationManager.AppSettings["TemplateExtensions"], null) ? (ConfigurationManager.AppSettings["TemplateExtensions"].Split(new char[] { ';' })) : null;

        #region Private variables
        private const string DeletedTemplateName = "_deleted";
        private const string TemplateNameColumn = "TemplateName";
        private const string DeletedTemplateColumn = "DeleteTemplate";
        private const string ExtensionName = ".html";
        private const string Dot = ".";
        private const string AllTypeOfFile = "*.*";
        private const string AllFile = "*";
        #endregion

        #region Public methods
        public EmailTemplateListModel GetEmailTemplates(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            EmailTemplateListModel model = new EmailTemplateListModel();
            string searchTemplateName = string.Empty;
            if (filters.Count > 0)
            {
                foreach (var item in filters)
                {
                    searchTemplateName = item.Item3;
                }
            }

            DataSet templatelist = GetFileNames(HttpContext.Current.Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath), searchTemplateName);
            if (!Equals(templatelist, null) && !Equals(templatelist.Tables, null) && templatelist.Tables.Count > 0)
            {
                model.EmailTemplates = templatelist.Tables[0].ToList<EmailTemplateModel>().ToCollection();
            }
            return model;
        }

        public EmailTemplateListModel GetDeletedTemplates(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            EmailTemplateListModel model = new EmailTemplateListModel();

            DataSet templatelist = GetDeletedFileNames(HttpContext.Current.Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath));
            if (!Equals(templatelist, null) && !Equals(templatelist.Tables, null) && templatelist.Tables.Count > 0)
            {
                model.EmailTemplates = templatelist.Tables[0].ToList<EmailTemplateModel>().ToCollection();
            }
            return model;
        }

        private DataSet GetDeletedFileNames(string path)
        {
            try
            {
                string[] files = Equals(extensions, null)
                    ? Directory.GetFiles(path, AllTypeOfFile)
                    : Directory.GetFiles(path, AllTypeOfFile).Where(f => extensions.Contains(System.IO.Path.GetExtension(f).ToLower())).ToArray();

                DataSet ds = new DataSet();
                DataTable dt = new DataTable();

                dt.Columns.Add(TemplateNameColumn);
                dt.Columns.Add(DeletedTemplateColumn);
                for (int i = 0; i < files.Length; i++)
                {
                    DataRow dr;
                    files[i] = Path.GetFileName(files[i]);
                    if (Path.GetFileNameWithoutExtension(files[i]).Contains(DeletedTemplateName))
                    {
                        dr = dt.NewRow();
                        dr[0] = files[i].Replace(DeletedTemplateName, string.Empty);
                        dr[1] = files[i];
                        dt.Rows.Add(dr);
                    }
                }
                ds.Tables.Add(dt);
                return ds;
            }
            catch (ArgumentException)
            {
                // Path functions will throw this 
                // if path contains invalid chars
                return null;
            }
        }

        public EmailTemplateModel CreateTemplatePage(EmailTemplateModel model)
        {

            if (Equals(model, null))
            {
                throw new Exception("Template page model cannot be null.");
            }
            if (model.IsNewAdd)
            {
                if (model.TemplateName.Length > DeletedTemplateName.Length)
                {
                    //Create new if file name contains "_deleted" at last then unable to create this file.
                    if (Equals(model.TemplateName.Substring(model.TemplateName.Length - 8), DeletedTemplateName))
                    {
                        throw new Exception("Can not create this file. Please enter a different name.");
                    }
                }
                //if file is already deleted then try to create file with same name then unable to create this file.
                if (!IsTemplateExist(string.Format("{0}{1}{2}", model.TemplateName, DeletedTemplateName, ExtensionName)))
                {
                    throw new Exception("Already in use. Please enter a different name.");
                }
                model.TemplateName = model.TemplateName + ExtensionName;
            }

            if (!IsTemplateExist(model.TemplateName))
            {
                if (model.TemplateName.Contains(DeletedTemplateName))
                {
                    ResetDeletedTemplate(model);
                    return new EmailTemplateModel();
                }

                throw new Exception("A page with this name already exists in the Data folder. Please enter a different name.");
            }
            else
            {
                return this.AddTemplate(model) ? model : null;
            }
        }

        public bool IsTemplateExist(string pageName)
        {
            string filePath = ZNodeConfigManager.EnvironmentConfig.ConfigPath + pageName;
            return (!ZNodeStorageManager.Exists(filePath)) ? true : false;
        }

        public bool AddTemplate(EmailTemplateModel model)
        {
            string filePath = string.Format("{0}{1}", ZNodeConfigManager.EnvironmentConfig.ConfigPath, model.TemplateName);

            if (ZNodeStorageManager.Exists(filePath))
            {
                throw new Exception("A page with this name already exists in the Data folder. Please enter a different name.");
            }

            ZNodeStorageManager.WriteTextStorage(model.Html, filePath);
            return true;
        }

        public EmailTemplateModel GetTemplatePageByName(string templateName, string extension, NameValueCollection expands)
        {
            EmailTemplateModel model = new EmailTemplateModel();
            string filePath = string.Format("{0}{1}{2}{3}", ZNodeConfigManager.EnvironmentConfig.ConfigPath, templateName, Dot, extension);

            if (ZNodeStorageManager.Exists(filePath))
            {
                try
                {
                    model.TemplateName = templateName;
                    model.Html = ZNodeStorageManager.ReadTextStorage(filePath);

                    return model;
                }
                catch
                {
                    return model;
                }
            }
            return model;
        }

        public bool UpdateTemplatePage(EmailTemplateModel model)
        {
            if (Equals(model, null))
            {
                throw new Exception("Template Page model cannot be null.");
            }
            this.UpdateHtmlFile(model);
            return true;
        }

        public bool DeleteTemplatePage(string templateName, string extension)
        {
            string fromFile = string.Format("{0}{1}{2}", templateName, Dot, extension);
            string toFile = string.Format("{0}{1}{2}{3}", templateName, DeletedTemplateName, Dot, extension);
            string fromFileLocation = HttpContext.Current.Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, fromFile));

            string toFileLocation = HttpContext.Current.Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, toFile));

            File.Move(fromFileLocation, toFileLocation);

            return true;
        }
        #endregion

        #region Private methods

        private DataSet GetFileNames(string path, string searchTemplateName)
        {
            try
            {
                string[] files = Equals(extensions, null)
                    ? Directory.GetFiles(path, string.IsNullOrEmpty(searchTemplateName) ? AllTypeOfFile : string.Format("{0}{1}{2}", AllFile, searchTemplateName, AllTypeOfFile))
                    : Directory.GetFiles(path, string.IsNullOrEmpty(searchTemplateName) ? AllTypeOfFile : string.Format("{0}{1}{2}", AllFile, searchTemplateName, AllTypeOfFile)).Where(f => extensions.Contains(System.IO.Path.GetExtension(f).ToLower())).ToArray();

                DataSet ds = new DataSet();
                DataTable dt = new DataTable();

                dt.Columns.Add(TemplateNameColumn);

                for (int i = 0; i < files.Length; i++)
                {
                    DataRow dr;
                    files[i] = Path.GetFileName(files[i]);
                    if (!Path.GetFileNameWithoutExtension(files[i]).Contains(DeletedTemplateName))
                    {
                        dr = dt.NewRow();
                        dr[0] = files[i];
                        dt.Rows.Add(dr);
                    }
                }
                ds.Tables.Add(dt);
                return ds;
            }
            catch (ArgumentException)
            {
                // Path functions will throw this 
                // if path contains invalid chars
                return null;
            }
        }

        private static void ResetDeletedTemplate(EmailTemplateModel model)
        {
            string fromFile = model.TemplateName;
            string toFile = model.TemplateName.Replace(DeletedTemplateName, string.Empty);
            string fromFileLocation = HttpContext.Current.Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, fromFile));

            string toFileLocation = HttpContext.Current.Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, toFile));
            File.Move(fromFileLocation, toFileLocation);
            ZNodeStorageManager.WriteTextStorage(model.Html, Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, toFile));
        }

        /// <summary>
        /// Creates or updates an HTML file
        /// </summary>
        /// <param name="pageName">Page name.</param>
        /// <param name="html">Html content of the page</param>
        private void UpdateHtmlFile(EmailTemplateModel model)
        {
            if (Equals(ZNode.Libraries.Framework.Business.ZNodeConfigManager.SetSiteConfig(), false))
            {
                return;
            }

            string filePath = string.Format("{0}{1}{2}{3}", ZNodeConfigManager.EnvironmentConfig.ConfigPath, model.TemplateName, Dot, model.Extension);
            ZNodeStorageManager.WriteTextStorage(model.Html, filePath);
        }
        #endregion
    }
}
