﻿using PRFT.Engine.ERP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    public class PRFTERPServices : IPRFTERPServices
    {
        
        private readonly ERPProductService service;
        private readonly ERPInventoryService inventoryService;

        public PRFTERPServices()
        {
            service = new ERPProductService();
            inventoryService = new ERPInventoryService();
        }
        public PRFTERPItemDetailsModel GetERPItemDetails(string associatedCustomerExternalId, string productNum, string customerType)
        {
            PRFTERPItemDetailsModel itemDetails = service.GetProductPriceFromERP(associatedCustomerExternalId, productNum, customerType);
            return itemDetails;
        }

        public PRFTInventoryModel GetInventoryDetails(string productNum)
        {
            PRFTInventoryModel inventoryDetails = inventoryService.GetInventoryDetails(productNum);
            return inventoryDetails;
        }
    }
}
