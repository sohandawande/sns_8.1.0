﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services
{
    public interface ICommonProfileService
    {
        /// <summary>
        /// To Get Store Access list based on user name.
        /// </summary>
        /// <param name="userName">User name of the user.</param>
        /// <returns>Return Stored Access List.</returns>
        ProfileCommonListModel GetProfileStoreAccess(string userName);
    }
}
