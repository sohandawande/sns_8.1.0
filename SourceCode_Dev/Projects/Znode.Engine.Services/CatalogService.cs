﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Helpers.Constants;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using CatalogManagerRepository = ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager;
using CatalogRepository = ZNode.Libraries.DataAccess.Service.CatalogService;
using PortalCatalogRepository = ZNode.Libraries.DataAccess.Service.PortalCatalogService;
using PromotionRepository = ZNode.Libraries.DataAccess.Service.PromotionService;

namespace Znode.Engine.Services
{
    public class CatalogService : BaseService, ICatalogService
    {
        #region Private Variables
        private readonly CatalogRepository _catalogRepository;
        private readonly PortalCatalogRepository _portalCatalogRepository;
        private readonly PromotionRepository _promotionRepository;
        #endregion

        #region Constructor
        public CatalogService()
        {
            _catalogRepository = new CatalogRepository();
            _portalCatalogRepository = new PortalCatalogRepository();
            _promotionRepository = new PromotionRepository();
        }
        #endregion

        #region Public Methods
        public CatalogModel GetCatalog(int catalogId)
        {
            var catalog = _catalogRepository.GetByCatalogID(catalogId);
            bool isDefault = Equals(CatalogManagerRepository.CatalogConfig.CatalogID, int.Parse(catalog.CatalogID.ToString()));
            return CatalogMap.ToModel(catalog, isDefault);
        }

        public CatalogListModel GetCatalogs(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new CatalogListModel();
            var catalogs = new TList<Catalog>();

            var query = new CatalogQuery();
            var sortBuilder = new CatalogSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            catalogs = _catalogRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (Equals(pagingLength, int.MaxValue))
            {
                model.PageSize = model.TotalResults;
            }

            // Map each item and add to the list
            foreach (var c in catalogs)
            {
                model.Catalogs.Add(CatalogMap.ToModel(c));
            }

            return model;
        }

        public CatalogListModel GetCatalogsByCatalogIds(string catalogIds, NameValueCollection expands, NameValueCollection sorts)
        {
            if (String.IsNullOrEmpty(catalogIds))
            {
                throw new Exception("List of catalog IDs cannot be null or empty.");
            }

            var model = new CatalogListModel();
            var catalogs = new TList<Catalog>();
            var tempList = new TList<Catalog>();

            var query = new CatalogQuery { Junction = String.Empty };
            var sortBuilder = new CatalogSortBuilder();

            var list = catalogIds.Split(',');
            foreach (var catalogId in list.Where(catalogId => !String.IsNullOrEmpty(catalogId)))
            {
                query.BeginGroup("OR");
                query.AppendEquals(CatalogColumn.CatalogID, catalogId);
                query.EndGroup();
            }

            SetSorting(sorts, sortBuilder);

            // Get the initial set
            tempList = _catalogRepository.Find(query, sortBuilder);

            // Now go through the list and get any expands
            foreach (var catalog in tempList)
            {
                catalogs.Add(catalog);
            }

            // Map each item and add to the list
            foreach (var c in catalogs)
            {
                model.Catalogs.Add(CatalogMap.ToModel(c));
            }

            return model;
        }

        public CatalogModel CreateCatalog(CatalogModel model)
        {
            if (Equals(model, null))
            {
                throw new Exception("Catalog model cannot be null.");
            }

            var entity = CatalogMap.ToEntity(model);
            var catalog = _catalogRepository.Save(entity);
            return CatalogMap.ToModel(catalog);
        }

        public CatalogModel UpdateCatalog(int catalogId, CatalogModel model)
        {
            if (catalogId < 1)
            {
                throw new Exception("Catalog ID cannot be less than 1.");
            }

            if (Equals(model, null))
            {
                throw new Exception("Catalog model cannot be null.");
            }

            var catalog = _catalogRepository.GetByCatalogID(catalogId);
            if (!Equals(catalog, null))
            {
                // Set catalog ID
                model.CatalogId = catalogId;

                var catalogToUpdate = CatalogMap.ToEntity(model);

                var updated = _catalogRepository.Update(catalogToUpdate);
                if (updated)
                {
                    catalog = _catalogRepository.GetByCatalogID(catalogId);
                    return CatalogMap.ToModel(catalog);
                }
            }

            return null;
        }

        public bool DeleteCatalog(int catalogId)
        {


            if (catalogId < 1)
            {
                throw new Exception("Catalog ID cannot be less than 1.");
            }

            var catalog = _catalogRepository.GetByCatalogID(catalogId);
            if (!Equals(catalog, null))
            {
                return _catalogRepository.Delete(catalog);
            }

            return false;
        }
        #endregion

        #region Private Methods
        private void SetFiltering(List<Tuple<string, string, string>> filters, CatalogQuery query)
        {
            bool isPortalIdPresent = false;
            string userName = string.Empty;
            bool isNullToBeChecked = false;
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;
                if (!Equals(filterKey, FilterKeys.UserName))
                {
                    if (filterKey.Equals(FilterKeys.PortalId))
                    {
                        isPortalIdPresent = true;
                        SetQueryParameter(CatalogColumn.PortalID, filterOperator, filterValue, query);
                    }
                    if (Equals(filterKey, FilterKeys.CatalogId)) { SetQueryParameter(CatalogColumn.CatalogID, filterOperator, filterValue, query); }
                    if (Equals(filterKey, FilterKeys.ExternalId)) { SetQueryParameter(CatalogColumn.ExternalID, filterOperator, filterValue, query); }
                    if (Equals(filterKey, FilterKeys.IsActive)) { SetQueryParameter(CatalogColumn.IsActive, filterOperator, filterValue, query); }
                    if (Equals(filterKey, FilterKeys.Name)) { SetQueryParameter(CatalogColumn.Name, filterOperator, filterValue, query); }
                    if (Equals(filterKey, FilterKeys.IsCategory))
                    {
                        isNullToBeChecked = Convert.ToBoolean(filterValue);
                    }
                }
                else
                {
                    userName = filterValue; 
                }
            }
            if (!isPortalIdPresent)
            {
                string portalId = GetAvailablePortals(userName);
                if (!string.IsNullOrEmpty(portalId) && !Equals(portalId, "0"))
                {
                    query.AppendIn(SqlUtil.AND, CatalogColumn.PortalID, portalId.Split(','));
                    if (!isNullToBeChecked)
                    {
                        query.AppendIsNull(SqlUtil.OR, CatalogColumn.PortalID);
                    }
                }
            }
        }

        private void SetSorting(NameValueCollection sorts, CatalogSortBuilder sortBuilder)
        {
            if (sorts.HasKeys() && sorts.Count > 1)
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);

                    if (Equals(value, SortKeys.CatalogId)) { SetSortDirection(CatalogColumn.CatalogID, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                    if (Equals(value, SortKeys.Name)) { SetSortDirection(CatalogColumn.Name, sorts[StoredProcedureKeys.sortDirKey], sortBuilder); }
                }
            }
            else
            {
                SetSortDirection(CatalogColumn.CatalogID, sorts[StoredProcedureKeys.sortDirKey], sortBuilder);
            }
        }

        private void SetQueryParameter(CatalogColumn column, string filterOperator, string filterValue, CatalogQuery query)
        {
            base.SetQueryParameter(column, filterOperator, filterValue, query);
        }

        private void SetSortDirection(CatalogColumn column, string value, CatalogSortBuilder sortBuilder)
        {
            base.SetSortDirection(column, value, sortBuilder);
        }
        #endregion

        #region Znode Version 8.0
        public bool CopyCatalog(CatalogModel model)
        {
            int localeId = 43;
            CatalogHelper catalogHelper = new CatalogHelper();
            return catalogHelper.CopyCatalog(model.CatalogId, model.Name, localeId);
        }

        public bool DeleteCatalogUsingCustomService(int catalogId, bool preserveCategories)
        {
            var query = new PromotionQuery();
            CatalogHelper catalogHelper = new CatalogHelper();
            TList<PortalCatalog> portalCatalogList = _portalCatalogRepository.GetByCatalogID(catalogId);
            query.AppendEquals(PromotionColumn.CatalogID, catalogId.ToString());
            TList<Promotion> list = _promotionRepository.Find(query);

            if (!Equals(portalCatalogList.Count, 0) || list.Count > 0)
            {
                throw new Exception("The catalog can not be deleted. Please delete the association with category, Store, if any.");
            }
            else
            {
                return catalogHelper.DeleteCatalog(catalogId, preserveCategories);
            }
            //return false;
        }

        public CatalogListModel GetCatalogListByPortalId(int portalId)
        {
            CatalogHelper catalogHelper = new CatalogHelper();
            DataSet catalogs = catalogHelper.GetCatalogsByPortalId(portalId);
            return CatalogMap.ToModelList(catalogs);
        }
        #endregion
    }
}
