﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
	public interface IPaymentOptionService
	{
		PaymentOptionModel GetPaymentOption(int paymentOptionId, NameValueCollection expands);
		PaymentOptionListModel GetPaymentOptions(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
		PaymentOptionModel CreatePaymentOption(PaymentOptionModel model);
		PaymentOptionModel UpdatePaymentOption(int paymentOptionId, PaymentOptionModel model);
		bool DeletePaymentOption(int paymentOptionId);
	}
}
