using System;

namespace Znode.Framework.Api.HelpPage.SampleGeneration
{
	/// <summary>
	/// This represents a preformatted JSON sample on the help page. There's a display template named JsonSample associated with this class.
	/// </summary>
	public class JsonSample
	{
		public JsonSample(string json)
		{
			if (json == null)
			{
				throw new ArgumentNullException("json");
			}

			Json = json;
		}

		public string Json { get; private set; }

		public override bool Equals(object obj)
		{
			var other = obj as JsonSample;
			return other != null && Json == other.Json;
		}

		public override int GetHashCode()
		{
			return Json.GetHashCode();
		}

		public override string ToString()
		{
			return Json;
		}
	}
}