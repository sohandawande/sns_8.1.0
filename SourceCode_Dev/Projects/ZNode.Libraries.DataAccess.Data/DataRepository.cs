﻿#region Using directives

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Configuration.Provider;
using System.Web.Configuration;
using System.Web;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;

#endregion

namespace ZNode.Libraries.DataAccess.Data
{
	/// <summary>
	/// This class represents the Data source repository and gives access to all the underlying providers.
	/// </summary>
	[CLSCompliant(true)]
	public sealed class DataRepository 
	{
		private static volatile NetTiersProvider _provider = null;
        private static volatile NetTiersProviderCollection _providers = null;
		private static volatile NetTiersServiceSection _section = null;
        
        private static object SyncRoot = new object();
				
		private DataRepository()
		{
		}
		
		#region Public LoadProvider
		/// <summary>
        /// Enables the DataRepository to programatically create and 
        /// pass in a <c>NetTiersProvider</c> during runtime.
        /// </summary>
        /// <param name="provider">An instatiated NetTiersProvider.</param>
        public static void LoadProvider(NetTiersProvider provider)
        {
			LoadProvider(provider, false);
        }
		
		/// <summary>
        /// Enables the DataRepository to programatically create and 
        /// pass in a <c>NetTiersProvider</c> during runtime.
        /// </summary>
        /// <param name="provider">An instatiated NetTiersProvider.</param>
        /// <param name="setAsDefault">ability to set any valid provider as the default provider for the DataRepository.</param>
		public static void LoadProvider(NetTiersProvider provider, bool setAsDefault)
        {
            if (provider == null)
                throw new ArgumentNullException("provider");

            if (_providers == null)
			{
				lock(SyncRoot)
				{
            		if (_providers == null)
						_providers = new NetTiersProviderCollection();
				}
			}
			
            if (_providers[provider.Name] == null)
            {
                lock (_providers.SyncRoot)
                {
                    _providers.Add(provider);
                }
            }

            if (_provider == null || setAsDefault)
            {
                lock (SyncRoot)
                {
                    if(_provider == null || setAsDefault)
                         _provider = provider;
                }
            }
        }
		#endregion 
		
		///<summary>
		/// Configuration based provider loading, will load the providers on first call.
		///</summary>
		private static void LoadProviders()
        {
            // Avoid claiming lock if providers are already loaded
            if (_provider == null)
            {
                lock (SyncRoot)
                {
                    // Do this again to make sure _provider is still null
                    if (_provider == null)
                    {
                        // Load registered providers and point _provider to the default provider
                        _providers = new NetTiersProviderCollection();

                        ProvidersHelper.InstantiateProviders(NetTiersSection.Providers, _providers, typeof(NetTiersProvider));
						_provider = _providers[NetTiersSection.DefaultProvider];

                        if (_provider == null)
                        {
                            throw new ProviderException("Unable to load default NetTiersProvider");
                        }
                    }
                }
            }
        }

		/// <summary>
        /// Gets the provider.
        /// </summary>
        /// <value>The provider.</value>
        public static NetTiersProvider Provider
        {
            get { LoadProviders(); return _provider; }
        }

		/// <summary>
        /// Gets the provider collection.
        /// </summary>
        /// <value>The providers.</value>
        public static NetTiersProviderCollection Providers
        {
            get { LoadProviders(); return _providers; }
        }
		
		/// <summary>
		/// Creates a new <see cref="TransactionManager"/> instance from the current datasource.
		/// </summary>
		/// <returns></returns>
		public TransactionManager CreateTransaction()
		{
			return _provider.CreateTransaction();
		}

		#region Configuration

		/// <summary>
		/// Gets a reference to the configured NetTiersServiceSection object.
		/// </summary>
		public static NetTiersServiceSection NetTiersSection
		{
			get
			{
				// Try to get a reference to the default <netTiersService> section
				_section = WebConfigurationManager.GetSection("netTiersService") as NetTiersServiceSection;

				if ( _section == null )
				{
					// otherwise look for section based on the assembly name
					_section = WebConfigurationManager.GetSection("ZNode.Libraries.DataAccess.Data") as NetTiersServiceSection;
				}

				if ( _section == null )
				{
					throw new ProviderException("Unable to load NetTiersServiceSection");
				}

				return _section;
			}
		}

		#endregion Configuration

		#region Connections

		/// <summary>
		/// Gets a reference to the ConnectionStringSettings collection.
		/// </summary>
		public static ConnectionStringSettingsCollection ConnectionStrings
		{
			get
			{
					return WebConfigurationManager.ConnectionStrings;
			}
		}

		// dictionary of connection providers
		private static Dictionary<String, ConnectionProvider> _connections;

		/// <summary>
		/// Gets the dictionary of connection providers.
		/// </summary>
		public static Dictionary<String, ConnectionProvider> Connections
		{
			get
			{
				if ( _connections == null )
				{
					lock (SyncRoot)
                	{
						if (_connections == null)
						{
							_connections = new Dictionary<String, ConnectionProvider>();
		
							// add a connection provider for each configured connection string
							foreach ( ConnectionStringSettings conn in ConnectionStrings )
							{
								_connections.Add(conn.Name, new ConnectionProvider(conn.Name, conn.ConnectionString));
							}
						}
					}
				}

				return _connections;
			}
		}

		/// <summary>
		/// Adds the specified connection string to the map of connection strings.
		/// </summary>
		/// <param name="connectionStringName">The connection string name.</param>
		/// <param name="connectionString">The provider specific connection information.</param>
		public static void AddConnection(String connectionStringName, String connectionString)
		{
			lock (SyncRoot)
            {
				Connections.Remove(connectionStringName);
				ConnectionProvider connection = new ConnectionProvider(connectionStringName, connectionString);
				Connections.Add(connectionStringName, connection);
			}
		}

		/// <summary>
		/// Provides ability to switch connection string at runtime.
		/// </summary>
		public sealed class ConnectionProvider
		{
			private NetTiersProvider _provider;
			private NetTiersProviderCollection _providers;
			private String _connectionStringName;
			private String _connectionString;


			/// <summary>
			/// Initializes a new instance of the ConnectionProvider class.
			/// </summary>
			/// <param name="connectionStringName">The connection string name.</param>
			/// <param name="connectionString">The provider specific connection information.</param>
			public ConnectionProvider(String connectionStringName, String connectionString)
			{
				_connectionString = connectionString;
				_connectionStringName = connectionStringName;
			}

			/// <summary>
			/// Gets the provider.
			/// </summary>
			public NetTiersProvider Provider
			{
				get { LoadProviders(); return _provider; }
			}

			/// <summary>
			/// Gets the provider collection.
			/// </summary>
			public NetTiersProviderCollection Providers
			{
				get { LoadProviders(); return _providers; }
			}

			/// <summary>
			/// Instantiates the configured providers based on the supplied connection string.
			/// </summary>
			private void LoadProviders()
			{
				DataRepository.LoadProviders();

				// Avoid claiming lock if providers are already loaded
				if ( _providers == null )
				{
					lock ( SyncRoot )
					{
						// Do this again to make sure _provider is still null
						if ( _providers == null )
						{
							// apply connection information to each provider
							for ( int i = 0; i < NetTiersSection.Providers.Count; i++ )
							{
								NetTiersSection.Providers[i].Parameters["connectionStringName"] = _connectionStringName;
								// remove previous connection string, if any
								NetTiersSection.Providers[i].Parameters.Remove("connectionString");

								if ( !String.IsNullOrEmpty(_connectionString) )
								{
									NetTiersSection.Providers[i].Parameters["connectionString"] = _connectionString;
								}
							}

							// Load registered providers and point _provider to the default provider
							_providers = new NetTiersProviderCollection();

							ProvidersHelper.InstantiateProviders(NetTiersSection.Providers, _providers, typeof(NetTiersProvider));
							_provider = _providers[NetTiersSection.DefaultProvider];
						}
					}
				}
			}
		}

		#endregion Connections

		#region Static properties
		
		#region SKUProfileEffectiveProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="SKUProfileEffective"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static SKUProfileEffectiveProviderBase SKUProfileEffectiveProvider
		{
			get 
			{
				LoadProviders();
				return _provider.SKUProfileEffectiveProvider;
			}
		}
		
		#endregion
		
		#region TagsProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="Tags"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static TagsProviderBase TagsProvider
		{
			get 
			{
				LoadProviders();
				return _provider.TagsProvider;
			}
		}
		
		#endregion
		
		#region ProductTypeAttributeProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ProductTypeAttribute"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ProductTypeAttributeProviderBase ProductTypeAttributeProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ProductTypeAttributeProvider;
			}
		}
		
		#endregion
		
		#region ProductImageProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ProductImage"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ProductImageProviderBase ProductImageProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ProductImageProvider;
			}
		}
		
		#endregion
		
		#region AddOnProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="AddOn"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static AddOnProviderBase AddOnProvider
		{
			get 
			{
				LoadProviders();
				return _provider.AddOnProvider;
			}
		}
		
		#endregion
		
		#region CSSProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="CSS"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static CSSProviderBase CSSProvider
		{
			get 
			{
				LoadProviders();
				return _provider.CSSProvider;
			}
		}
		
		#endregion
		
		#region PriceListProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="PriceList"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static PriceListProviderBase PriceListProvider
		{
			get 
			{
				LoadProviders();
				return _provider.PriceListProvider;
			}
		}
		
		#endregion
		
		#region ProductHighlightProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ProductHighlight"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ProductHighlightProviderBase ProductHighlightProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ProductHighlightProvider;
			}
		}
		
		#endregion
		
		#region CategoryProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="Category"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static CategoryProviderBase CategoryProvider
		{
			get 
			{
				LoadProviders();
				return _provider.CategoryProvider;
			}
		}
		
		#endregion
		
		#region CaseTypeProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="CaseType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static CaseTypeProviderBase CaseTypeProvider
		{
			get 
			{
				LoadProviders();
				return _provider.CaseTypeProvider;
			}
		}
		
		#endregion
		
		#region ResetPasswordProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ResetPassword"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ResetPasswordProviderBase ResetPasswordProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ResetPasswordProvider;
			}
		}
		
		#endregion
		
		#region CategoryProfileProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="CategoryProfile"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static CategoryProfileProviderBase CategoryProfileProvider
		{
			get 
			{
				LoadProviders();
				return _provider.CategoryProfileProvider;
			}
		}
		
		#endregion
		
		#region AccountProfileProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="AccountProfile"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static AccountProfileProviderBase AccountProfileProvider
		{
			get 
			{
				LoadProviders();
				return _provider.AccountProfileProvider;
			}
		}
		
		#endregion
		
		#region ProductReviewHistoryProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ProductReviewHistory"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ProductReviewHistoryProviderBase ProductReviewHistoryProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ProductReviewHistoryProvider;
			}
		}
		
		#endregion
		
		#region GatewayProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="Gateway"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static GatewayProviderBase GatewayProvider
		{
			get 
			{
				LoadProviders();
				return _provider.GatewayProvider;
			}
		}
		
		#endregion
		
		#region SavedCartLineItemProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="SavedCartLineItem"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static SavedCartLineItemProviderBase SavedCartLineItemProvider
		{
			get 
			{
				LoadProviders();
				return _provider.SavedCartLineItemProvider;
			}
		}
		
		#endregion
		
		#region SKUInventoryProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="SKUInventory"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static SKUInventoryProviderBase SKUInventoryProvider
		{
			get 
			{
				LoadProviders();
				return _provider.SKUInventoryProvider;
			}
		}
		
		#endregion
		
		#region WishListProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="WishList"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static WishListProviderBase WishListProvider
		{
			get 
			{
				LoadProviders();
				return _provider.WishListProvider;
			}
		}
		
		#endregion
		
		#region ProductCategoryProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ProductCategory"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ProductCategoryProviderBase ProductCategoryProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ProductCategoryProvider;
			}
		}
		
		#endregion
		
		#region ContentPageProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ContentPage"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ContentPageProviderBase ContentPageProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ContentPageProvider;
			}
		}
		
		#endregion
		
		#region ProductAttributeProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ProductAttribute"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ProductAttributeProviderBase ProductAttributeProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ProductAttributeProvider;
			}
		}
		
		#endregion
		
		#region PortalCatalogProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="PortalCatalog"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static PortalCatalogProviderBase PortalCatalogProvider
		{
			get 
			{
				LoadProviders();
				return _provider.PortalCatalogProvider;
			}
		}
		
		#endregion
		
		#region ContentPageRevisionProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ContentPageRevision"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ContentPageRevisionProviderBase ContentPageRevisionProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ContentPageRevisionProvider;
			}
		}
		
		#endregion
		
		#region SKUAttributeProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="SKUAttribute"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static SKUAttributeProviderBase SKUAttributeProvider
		{
			get 
			{
				LoadProviders();
				return _provider.SKUAttributeProvider;
			}
		}
		
		#endregion
		
		#region ShippingProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="Shipping"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ShippingProviderBase ShippingProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ShippingProvider;
			}
		}
		
		#endregion
		
		#region ProductAddOnProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ProductAddOn"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ProductAddOnProviderBase ProductAddOnProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ProductAddOnProvider;
			}
		}
		
		#endregion
		
		#region HighlightProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="Highlight"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static HighlightProviderBase HighlightProvider
		{
			get 
			{
				LoadProviders();
				return _provider.HighlightProvider;
			}
		}
		
		#endregion
		
		#region CategoryNodeProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="CategoryNode"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static CategoryNodeProviderBase CategoryNodeProvider
		{
			get 
			{
				LoadProviders();
				return _provider.CategoryNodeProvider;
			}
		}
		
		#endregion
		
		#region AddressProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="Address"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static AddressProviderBase AddressProvider
		{
			get 
			{
				LoadProviders();
				return _provider.AddressProvider;
			}
		}
		
		#endregion
		
		#region ActivityLogProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ActivityLog"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ActivityLogProviderBase ActivityLogProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ActivityLogProvider;
			}
		}
		
		#endregion
		
		#region FacetProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="Facet"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static FacetProviderBase FacetProvider
		{
			get 
			{
				LoadProviders();
				return _provider.FacetProvider;
			}
		}
		
		#endregion
		
		#region HighlightTypeProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="HighlightType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static HighlightTypeProviderBase HighlightTypeProvider
		{
			get 
			{
				LoadProviders();
				return _provider.HighlightTypeProvider;
			}
		}
		
		#endregion
		
		#region ProductProfileProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ProductProfile"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ProductProfileProviderBase ProductProfileProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ProductProfileProvider;
			}
		}
		
		#endregion
		
		#region CaseRequestProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="CaseRequest"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static CaseRequestProviderBase CaseRequestProvider
		{
			get 
			{
				LoadProviders();
				return _provider.CaseRequestProvider;
			}
		}
		
		#endregion
		
		#region ReviewProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="Review"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ReviewProviderBase ReviewProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ReviewProvider;
			}
		}
		
		#endregion
		
		#region TrackingOutboundProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="TrackingOutbound"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static TrackingOutboundProviderBase TrackingOutboundProvider
		{
			get 
			{
				LoadProviders();
				return _provider.TrackingOutboundProvider;
			}
		}
		
		#endregion
		
		#region ActivityLogTypeProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ActivityLogType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ActivityLogTypeProviderBase ActivityLogTypeProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ActivityLogTypeProvider;
			}
		}
		
		#endregion
		
		#region PortalProfileProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="PortalProfile"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static PortalProfileProviderBase PortalProfileProvider
		{
			get 
			{
				LoadProviders();
				return _provider.PortalProfileProvider;
			}
		}
		
		#endregion
		
		#region FacetGroupProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="FacetGroup"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static FacetGroupProviderBase FacetGroupProvider
		{
			get 
			{
				LoadProviders();
				return _provider.FacetGroupProvider;
			}
		}
		
		#endregion
		
		#region LuceneDocumentMappingProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="LuceneDocumentMapping"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static LuceneDocumentMappingProviderBase LuceneDocumentMappingProvider
		{
			get 
			{
				LoadProviders();
				return _provider.LuceneDocumentMappingProvider;
			}
		}
		
		#endregion
		
		#region FacetControlTypeProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="FacetControlType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static FacetControlTypeProviderBase FacetControlTypeProvider
		{
			get 
			{
				LoadProviders();
				return _provider.FacetControlTypeProvider;
			}
		}
		
		#endregion
		
		#region DomainProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="Domain"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static DomainProviderBase DomainProvider
		{
			get 
			{
				LoadProviders();
				return _provider.DomainProvider;
			}
		}
		
		#endregion
		
		#region FacetProductSKUProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="FacetProductSKU"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static FacetProductSKUProviderBase FacetProductSKUProvider
		{
			get 
			{
				LoadProviders();
				return _provider.FacetProductSKUProvider;
			}
		}
		
		#endregion
		
		#region DiscountTypeProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="DiscountType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static DiscountTypeProviderBase DiscountTypeProvider
		{
			get 
			{
				LoadProviders();
				return _provider.DiscountTypeProvider;
			}
		}
		
		#endregion
		
		#region PortalCountryProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="PortalCountry"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static PortalCountryProviderBase PortalCountryProvider
		{
			get 
			{
				LoadProviders();
				return _provider.PortalCountryProvider;
			}
		}
		
		#endregion
		
		#region ZipCodeProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ZipCode"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ZipCodeProviderBase ZipCodeProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ZipCodeProvider;
			}
		}
		
		#endregion
		
		#region SKUProfileProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="SKUProfile"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static SKUProfileProviderBase SKUProfileProvider
		{
			get 
			{
				LoadProviders();
				return _provider.SKUProfileProvider;
			}
		}
		
		#endregion
		
		#region TrackingProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="Tracking"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static TrackingProviderBase TrackingProvider
		{
			get 
			{
				LoadProviders();
				return _provider.TrackingProvider;
			}
		}
		
		#endregion
		
		#region LuceneGlobalProductBoostProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="LuceneGlobalProductBoost"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static LuceneGlobalProductBoostProviderBase LuceneGlobalProductBoostProvider
		{
			get 
			{
				LoadProviders();
				return _provider.LuceneGlobalProductBoostProvider;
			}
		}
		
		#endregion
		
		#region CasePriorityProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="CasePriority"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static CasePriorityProviderBase CasePriorityProvider
		{
			get 
			{
				LoadProviders();
				return _provider.CasePriorityProvider;
			}
		}
		
		#endregion
		
		#region AddOnValueProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="AddOnValue"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static AddOnValueProviderBase AddOnValueProvider
		{
			get 
			{
				LoadProviders();
				return _provider.AddOnValueProvider;
			}
		}
		
		#endregion
		
		#region PromotionProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="Promotion"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static PromotionProviderBase PromotionProvider
		{
			get 
			{
				LoadProviders();
				return _provider.PromotionProvider;
			}
		}
		
		#endregion
		
		#region ProductTierProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ProductTier"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ProductTierProviderBase ProductTierProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ProductTierProvider;
			}
		}
		
		#endregion
		
		#region TaxRuleProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="TaxRule"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static TaxRuleProviderBase TaxRuleProvider
		{
			get 
			{
				LoadProviders();
				return _provider.TaxRuleProvider;
			}
		}
		
		#endregion
		
		#region ShippingRuleProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ShippingRule"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ShippingRuleProviderBase ShippingRuleProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ShippingRuleProvider;
			}
		}
		
		#endregion
		
		#region LuceneGlobalProductCategoryBoostProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="LuceneGlobalProductCategoryBoost"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static LuceneGlobalProductCategoryBoostProviderBase LuceneGlobalProductCategoryBoostProvider
		{
			get 
			{
				LoadProviders();
				return _provider.LuceneGlobalProductCategoryBoostProvider;
			}
		}
		
		#endregion
		
		#region SavedCartProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="SavedCart"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static SavedCartProviderBase SavedCartProvider
		{
			get 
			{
				LoadProviders();
				return _provider.SavedCartProvider;
			}
		}
		
		#endregion
		
		#region ProductCrossSellProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ProductCrossSell"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ProductCrossSellProviderBase ProductCrossSellProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ProductCrossSellProvider;
			}
		}
		
		#endregion
		
		#region LuceneGlobalProductBrandBoostProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="LuceneGlobalProductBrandBoost"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static LuceneGlobalProductBrandBoostProviderBase LuceneGlobalProductBrandBoostProvider
		{
			get 
			{
				LoadProviders();
				return _provider.LuceneGlobalProductBrandBoostProvider;
			}
		}
		
		#endregion
		
		#region TaxRuleTypeProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="TaxRuleType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static TaxRuleTypeProviderBase TaxRuleTypeProvider
		{
			get 
			{
				LoadProviders();
				return _provider.TaxRuleTypeProvider;
			}
		}
		
		#endregion
		
		#region FacetGroupCategoryProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="FacetGroupCategory"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static FacetGroupCategoryProviderBase FacetGroupCategoryProvider
		{
			get 
			{
				LoadProviders();
				return _provider.FacetGroupCategoryProvider;
			}
		}
		
		#endregion
		
		#region AttributeTypeProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="AttributeType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static AttributeTypeProviderBase AttributeTypeProvider
		{
			get 
			{
				LoadProviders();
				return _provider.AttributeTypeProvider;
			}
		}
		
		#endregion
		
		#region NoteProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="Note"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static NoteProviderBase NoteProvider
		{
			get 
			{
				LoadProviders();
				return _provider.NoteProvider;
			}
		}
		
		#endregion
		
		#region TrackingEventProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="TrackingEvent"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static TrackingEventProviderBase TrackingEventProvider
		{
			get 
			{
				LoadProviders();
				return _provider.TrackingEventProvider;
			}
		}
		
		#endregion
		
		#region LuceneGlobalProductCatalogBoostProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="LuceneGlobalProductCatalogBoost"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static LuceneGlobalProductCatalogBoostProviderBase LuceneGlobalProductCatalogBoostProvider
		{
			get 
			{
				LoadProviders();
				return _provider.LuceneGlobalProductCatalogBoostProvider;
			}
		}
		
		#endregion
		
		#region OrderStateProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="OrderState"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static OrderStateProviderBase OrderStateProvider
		{
			get 
			{
				LoadProviders();
				return _provider.OrderStateProvider;
			}
		}
		
		#endregion
		
		#region OrderProcessingTypeProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="OrderProcessingType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static OrderProcessingTypeProviderBase OrderProcessingTypeProvider
		{
			get 
			{
				LoadProviders();
				return _provider.OrderProcessingTypeProvider;
			}
		}
		
		#endregion
		
		#region StoreProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="Store"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static StoreProviderBase StoreProvider
		{
			get 
			{
				LoadProviders();
				return _provider.StoreProvider;
			}
		}
		
		#endregion
		
		#region LocaleProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="Locale"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static LocaleProviderBase LocaleProvider
		{
			get 
			{
				LoadProviders();
				return _provider.LocaleProvider;
			}
		}
		
		#endregion
		
		#region ProductReviewStateProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ProductReviewState"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ProductReviewStateProviderBase ProductReviewStateProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ProductReviewStateProvider;
			}
		}
		
		#endregion
		
		#region AccountTypeProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="AccountType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static AccountTypeProviderBase AccountTypeProvider
		{
			get 
			{
				LoadProviders();
				return _provider.AccountTypeProvider;
			}
		}
		
		#endregion
		
		#region ShippingTypeProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ShippingType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ShippingTypeProviderBase ShippingTypeProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ShippingTypeProvider;
			}
		}
		
		#endregion
		
		#region StateProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="State"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static StateProviderBase StateProvider
		{
			get 
			{
				LoadProviders();
				return _provider.StateProvider;
			}
		}
		
		#endregion
		
		#region CurrencyTypeProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="CurrencyType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static CurrencyTypeProviderBase CurrencyTypeProvider
		{
			get 
			{
				LoadProviders();
				return _provider.CurrencyTypeProvider;
			}
		}
		
		#endregion
		
		#region ProductRelationTypeProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ProductRelationType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ProductRelationTypeProviderBase ProductRelationTypeProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ProductRelationTypeProvider;
			}
		}
		
		#endregion
		
		#region ShippingRuleTypeProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ShippingRuleType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ShippingRuleTypeProviderBase ShippingRuleTypeProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ShippingRuleTypeProvider;
			}
		}
		
		#endregion
		
		#region PortalProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="Portal"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static PortalProviderBase PortalProvider
		{
			get 
			{
				LoadProviders();
				return _provider.PortalProvider;
			}
		}
		
		#endregion
		
		#region ProductExtensionOptionProductProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ProductExtensionOptionProduct"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ProductExtensionOptionProductProviderBase ProductExtensionOptionProductProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ProductExtensionOptionProductProvider;
			}
		}
		
		#endregion
		
		#region ReferralCommissionTypeProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ReferralCommissionType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ReferralCommissionTypeProviderBase ReferralCommissionTypeProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ReferralCommissionTypeProvider;
			}
		}
		
		#endregion
		
		#region TaxClassProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="TaxClass"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static TaxClassProviderBase TaxClassProvider
		{
			get 
			{
				LoadProviders();
				return _provider.TaxClassProvider;
			}
		}
		
		#endregion
		
		#region ProductExtensionRelationProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ProductExtensionRelation"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ProductExtensionRelationProviderBase ProductExtensionRelationProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ProductExtensionRelationProvider;
			}
		}
		
		#endregion
		
		#region ProfileProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="Profile"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ProfileProviderBase ProfileProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ProfileProvider;
			}
		}
		
		#endregion
		
		#region RMARequestProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="RMARequest"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static RMARequestProviderBase RMARequestProvider
		{
			get 
			{
				LoadProviders();
				return _provider.RMARequestProvider;
			}
		}
		
		#endregion
		
		#region PaymentSettingProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="PaymentSetting"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static PaymentSettingProviderBase PaymentSettingProvider
		{
			get 
			{
				LoadProviders();
				return _provider.PaymentSettingProvider;
			}
		}
		
		#endregion
		
		#region OrderShipmentProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="OrderShipment"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static OrderShipmentProviderBase OrderShipmentProvider
		{
			get 
			{
				LoadProviders();
				return _provider.OrderShipmentProvider;
			}
		}
		
		#endregion
		
		#region ProductExtensionOptionProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ProductExtensionOption"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ProductExtensionOptionProviderBase ProductExtensionOptionProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ProductExtensionOptionProvider;
			}
		}
		
		#endregion
		
		#region OrderLineItemProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="OrderLineItem"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static OrderLineItemProviderBase OrderLineItemProvider
		{
			get 
			{
				LoadProviders();
				return _provider.OrderLineItemProvider;
			}
		}
		
		#endregion
		
		#region ReasonForReturnProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ReasonForReturn"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ReasonForReturnProviderBase ReasonForReturnProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ReasonForReturnProvider;
			}
		}
		
		#endregion
		
		#region SourceModificationAuditProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="SourceModificationAudit"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static SourceModificationAuditProviderBase SourceModificationAuditProvider
		{
			get 
			{
				LoadProviders();
				return _provider.SourceModificationAuditProvider;
			}
		}
		
		#endregion
		
		#region UserDefinedViewProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="UserDefinedView"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static UserDefinedViewProviderBase UserDefinedViewProvider
		{
			get 
			{
				LoadProviders();
				return _provider.UserDefinedViewProvider;
			}
		}
		
		#endregion
		
		#region RMARequestItemProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="RMARequestItem"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static RMARequestItemProviderBase RMARequestItemProvider
		{
			get 
			{
				LoadProviders();
				return _provider.RMARequestItemProvider;
			}
		}
		
		#endregion
		
		#region WorkflowProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="Workflow"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static WorkflowProviderBase WorkflowProvider
		{
			get 
			{
				LoadProviders();
				return _provider.WorkflowProvider;
			}
		}
		
		#endregion
		
		#region CustomerPricingProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="CustomerPricing"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static CustomerPricingProviderBase CustomerPricingProvider
		{
			get 
			{
				LoadProviders();
				return _provider.CustomerPricingProvider;
			}
		}
		
		#endregion
		
		#region SourceModificationTypeProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="SourceModificationType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static SourceModificationTypeProviderBase SourceModificationTypeProvider
		{
			get 
			{
				LoadProviders();
				return _provider.SourceModificationTypeProvider;
			}
		}
		
		#endregion
		
		#region ApplicationSettingProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ApplicationSetting"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ApplicationSettingProviderBase ApplicationSettingProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ApplicationSettingProvider;
			}
		}
		
		#endregion
		
		#region PaymentTokenProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="PaymentToken"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static PaymentTokenProviderBase PaymentTokenProvider
		{
			get 
			{
				LoadProviders();
				return _provider.PaymentTokenProvider;
			}
		}
		
		#endregion
		
		#region RequestStatusProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="RequestStatus"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static RequestStatusProviderBase RequestStatusProvider
		{
			get 
			{
				LoadProviders();
				return _provider.RequestStatusProvider;
			}
		}
		
		#endregion
		
		#region UrlRedirectProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="UrlRedirect"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static UrlRedirectProviderBase UrlRedirectProvider
		{
			get 
			{
				LoadProviders();
				return _provider.UrlRedirectProvider;
			}
		}
		
		#endregion
		
		#region OrderDetailProductExtensionOptionProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="OrderDetailProductExtensionOption"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static OrderDetailProductExtensionOptionProviderBase OrderDetailProductExtensionOptionProvider
		{
			get 
			{
				LoadProviders();
				return _provider.OrderDetailProductExtensionOptionProvider;
			}
		}
		
		#endregion
		
		#region AccountProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="Account"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static AccountProviderBase AccountProvider
		{
			get 
			{
				LoadProviders();
				return _provider.AccountProvider;
			}
		}
		
		#endregion
		
		#region ThemeProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="Theme"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ThemeProviderBase ThemeProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ThemeProvider;
			}
		}
		
		#endregion
		
		#region OrderLineItemRelationshipTypeProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="OrderLineItemRelationshipType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static OrderLineItemRelationshipTypeProviderBase OrderLineItemRelationshipTypeProvider
		{
			get 
			{
				LoadProviders();
				return _provider.OrderLineItemRelationshipTypeProvider;
			}
		}
		
		#endregion
		
		#region SourceModificationStatusProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="SourceModificationStatus"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static SourceModificationStatusProviderBase SourceModificationStatusProvider
		{
			get 
			{
				LoadProviders();
				return _provider.SourceModificationStatusProvider;
			}
		}
		
		#endregion
		
		#region LuceneIndexStatusNamesProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="LuceneIndexStatusNames"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static LuceneIndexStatusNamesProviderBase LuceneIndexStatusNamesProvider
		{
			get 
			{
				LoadProviders();
				return _provider.LuceneIndexStatusNamesProvider;
			}
		}
		
		#endregion
		
		#region DigitalAssetProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="DigitalAsset"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static DigitalAssetProviderBase DigitalAssetProvider
		{
			get 
			{
				LoadProviders();
				return _provider.DigitalAssetProvider;
			}
		}
		
		#endregion
		
		#region OrderProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="Order"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static OrderProviderBase OrderProvider
		{
			get 
			{
				LoadProviders();
				return _provider.OrderProvider;
			}
		}
		
		#endregion
		
		#region SourceTypeProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="SourceType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static SourceTypeProviderBase SourceTypeProvider
		{
			get 
			{
				LoadProviders();
				return _provider.SourceTypeProvider;
			}
		}
		
		#endregion
		
		#region PasswordLogProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="PasswordLog"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static PasswordLogProviderBase PasswordLogProvider
		{
			get 
			{
				LoadProviders();
				return _provider.PasswordLogProvider;
			}
		}
		
		#endregion
		
		#region LuceneIndexMonitorProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="LuceneIndexMonitor"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static LuceneIndexMonitorProviderBase LuceneIndexMonitorProvider
		{
			get 
			{
				LoadProviders();
				return _provider.LuceneIndexMonitorProvider;
			}
		}
		
		#endregion
		
		#region MultifrontProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="Multifront"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static MultifrontProviderBase MultifrontProvider
		{
			get 
			{
				LoadProviders();
				return _provider.MultifrontProvider;
			}
		}
		
		#endregion
		
		#region GiftCardProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="GiftCard"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static GiftCardProviderBase GiftCardProvider
		{
			get 
			{
				LoadProviders();
				return _provider.GiftCardProvider;
			}
		}
		
		#endregion
		
		#region GiftCardHistoryProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="GiftCardHistory"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static GiftCardHistoryProviderBase GiftCardHistoryProvider
		{
			get 
			{
				LoadProviders();
				return _provider.GiftCardHistoryProvider;
			}
		}
		
		#endregion
		
		#region MessageTypeProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="MessageType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static MessageTypeProviderBase MessageTypeProvider
		{
			get 
			{
				LoadProviders();
				return _provider.MessageTypeProvider;
			}
		}
		
		#endregion
		
		#region MessageConfigProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="MessageConfig"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static MessageConfigProviderBase MessageConfigProvider
		{
			get 
			{
				LoadProviders();
				return _provider.MessageConfigProvider;
			}
		}
		
		#endregion
		
		#region ProductTypeProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ProductType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ProductTypeProviderBase ProductTypeProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ProductTypeProvider;
			}
		}
		
		#endregion
		
		#region CatalogProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="Catalog"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static CatalogProviderBase CatalogProvider
		{
			get 
			{
				LoadProviders();
				return _provider.CatalogProvider;
			}
		}
		
		#endregion
		
		#region ManufacturerProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="Manufacturer"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ManufacturerProviderBase ManufacturerProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ManufacturerProvider;
			}
		}
		
		#endregion
		
		#region SupplierProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="Supplier"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static SupplierProviderBase SupplierProvider
		{
			get 
			{
				LoadProviders();
				return _provider.SupplierProvider;
			}
		}
		
		#endregion
		
		#region ProductProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="Product"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ProductProviderBase ProductProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ProductProvider;
			}
		}
		
		#endregion
		
		#region PaymentStatusProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="PaymentStatus"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static PaymentStatusProviderBase PaymentStatusProvider
		{
			get 
			{
				LoadProviders();
				return _provider.PaymentStatusProvider;
			}
		}
		
		#endregion
		
		#region SKUProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="SKU"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static SKUProviderBase SKUProvider
		{
			get 
			{
				LoadProviders();
				return _provider.SKUProvider;
			}
		}
		
		#endregion
		
		#region CookieMappingProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="CookieMapping"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static CookieMappingProviderBase CookieMappingProvider
		{
			get 
			{
				LoadProviders();
				return _provider.CookieMappingProvider;
			}
		}
		
		#endregion
		
		#region ReferralCommissionProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ReferralCommission"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ReferralCommissionProviderBase ReferralCommissionProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ReferralCommissionProvider;
			}
		}
		
		#endregion
		
		#region MasterPageProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="MasterPage"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static MasterPageProviderBase MasterPageProvider
		{
			get 
			{
				LoadProviders();
				return _provider.MasterPageProvider;
			}
		}
		
		#endregion
		
		#region CountryProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="Country"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static CountryProviderBase CountryProvider
		{
			get 
			{
				LoadProviders();
				return _provider.CountryProvider;
			}
		}
		
		#endregion
		
		#region SupplierTypeProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="SupplierType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static SupplierTypeProviderBase SupplierTypeProvider
		{
			get 
			{
				LoadProviders();
				return _provider.SupplierTypeProvider;
			}
		}
		
		#endregion
		
		#region CaseStatusProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="CaseStatus"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static CaseStatusProviderBase CaseStatusProvider
		{
			get 
			{
				LoadProviders();
				return _provider.CaseStatusProvider;
			}
		}
		
		#endregion
		
		#region ShippingServiceCodeProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ShippingServiceCode"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ShippingServiceCodeProviderBase ShippingServiceCodeProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ShippingServiceCodeProvider;
			}
		}
		
		#endregion
		
		#region PaymentTokenAuthorizeProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="PaymentTokenAuthorize"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static PaymentTokenAuthorizeProviderBase PaymentTokenAuthorizeProvider
		{
			get 
			{
				LoadProviders();
				return _provider.PaymentTokenAuthorizeProvider;
			}
		}
		
		#endregion
		
		#region RMAConfigurationProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="RMAConfiguration"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static RMAConfigurationProviderBase RMAConfigurationProvider
		{
			get 
			{
				LoadProviders();
				return _provider.RMAConfigurationProvider;
			}
		}
		
		#endregion
		
		#region LuceneServerConfigurationStatusProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="LuceneServerConfigurationStatus"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static LuceneServerConfigurationStatusProviderBase LuceneServerConfigurationStatusProvider
		{
			get 
			{
				LoadProviders();
				return _provider.LuceneServerConfigurationStatusProvider;
			}
		}
		
		#endregion
		
		#region PortalWorkflowProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="PortalWorkflow"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static PortalWorkflowProviderBase PortalWorkflowProvider
		{
			get 
			{
				LoadProviders();
				return _provider.PortalWorkflowProvider;
			}
		}
		
		#endregion
		
		#region AccountPaymentProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="AccountPayment"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static AccountPaymentProviderBase AccountPaymentProvider
		{
			get 
			{
				LoadProviders();
				return _provider.AccountPaymentProvider;
			}
		}
		
		#endregion
		
		#region ProductImageTypeProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ProductImageType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ProductImageTypeProviderBase ProductImageTypeProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ProductImageTypeProvider;
			}
		}
		
		#endregion
		
		#region ProductExtensionProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ProductExtension"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ProductExtensionProviderBase ProductExtensionProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ProductExtensionProvider;
			}
		}
		
		#endregion
		
		#region SavedCartLineItemProductExtensionOptionProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="SavedCartLineItemProductExtensionOption"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static SavedCartLineItemProductExtensionOptionProviderBase SavedCartLineItemProductExtensionOptionProvider
		{
			get 
			{
				LoadProviders();
				return _provider.SavedCartLineItemProductExtensionOptionProvider;
			}
		}
		
		#endregion
		
		#region PaymentTypeProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="PaymentType"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static PaymentTypeProviderBase PaymentTypeProvider
		{
			get 
			{
				LoadProviders();
				return _provider.PaymentTypeProvider;
			}
		}
		
		#endregion
		
		#region ParentChildProductProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="ParentChildProduct"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static ParentChildProductProviderBase ParentChildProductProvider
		{
			get 
			{
				LoadProviders();
				return _provider.ParentChildProductProvider;
			}
		}
		
		#endregion
		
		#region LuceneIndexServerStatusProvider

		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="LuceneIndexServerStatus"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static LuceneIndexServerStatusProviderBase LuceneIndexServerStatusProvider
		{
			get 
			{
				LoadProviders();
				return _provider.LuceneIndexServerStatusProvider;
			}
		}
		
		#endregion
		
		
		#region VwZnodeCategoriesCatalogsProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="VwZnodeCategoriesCatalogs"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static VwZnodeCategoriesCatalogsProviderBase VwZnodeCategoriesCatalogsProvider
		{
			get 
			{
				LoadProviders();
				return _provider.VwZnodeCategoriesCatalogsProvider;
			}
		}
		
		#endregion
		
		#region VwZnodeCategoriesPortalsProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="VwZnodeCategoriesPortals"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static VwZnodeCategoriesPortalsProviderBase VwZnodeCategoriesPortalsProvider
		{
			get 
			{
				LoadProviders();
				return _provider.VwZnodeCategoriesPortalsProvider;
			}
		}
		
		#endregion
		
		#region VwZnodeProductsCatalogsProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="VwZnodeProductsCatalogs"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static VwZnodeProductsCatalogsProviderBase VwZnodeProductsCatalogsProvider
		{
			get 
			{
				LoadProviders();
				return _provider.VwZnodeProductsCatalogsProvider;
			}
		}
		
		#endregion
		
		#region VwZnodeProductsCategoriesProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="VwZnodeProductsCategories"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static VwZnodeProductsCategoriesProviderBase VwZnodeProductsCategoriesProvider
		{
			get 
			{
				LoadProviders();
				return _provider.VwZnodeProductsCategoriesProvider;
			}
		}
		
		#endregion
		
		#region VwZnodeProductsCategoriesPromotionsProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="VwZnodeProductsCategoriesPromotions"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static VwZnodeProductsCategoriesPromotionsProviderBase VwZnodeProductsCategoriesPromotionsProvider
		{
			get 
			{
				LoadProviders();
				return _provider.VwZnodeProductsCategoriesPromotionsProvider;
			}
		}
		
		#endregion
		
		#region VwZnodeProductsPortalsProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="VwZnodeProductsPortals"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static VwZnodeProductsPortalsProviderBase VwZnodeProductsPortalsProvider
		{
			get 
			{
				LoadProviders();
				return _provider.VwZnodeProductsPortalsProvider;
			}
		}
		
		#endregion
		
		#region VwZnodeProductsPromotionsProvider
		
		///<summary>
		/// Gets the current instance of the Data Access Logic Component for the <see cref="VwZnodeProductsPromotions"/> business entity.
		/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
		///</summary>
		public static VwZnodeProductsPromotionsProviderBase VwZnodeProductsPromotionsProvider
		{
			get 
			{
				LoadProviders();
				return _provider.VwZnodeProductsPromotionsProvider;
			}
		}
		
		#endregion
		
		#endregion
	}
	
	#region Query/Filters
		
	#region SKUProfileEffectiveFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SKUProfileEffective"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SKUProfileEffectiveFilters : SKUProfileEffectiveFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SKUProfileEffectiveFilters class.
		/// </summary>
		public SKUProfileEffectiveFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the SKUProfileEffectiveFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SKUProfileEffectiveFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SKUProfileEffectiveFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SKUProfileEffectiveFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SKUProfileEffectiveFilters
	
	#region SKUProfileEffectiveQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SKUProfileEffectiveParameterBuilder"/> class
	/// that is used exclusively with a <see cref="SKUProfileEffective"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SKUProfileEffectiveQuery : SKUProfileEffectiveParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SKUProfileEffectiveQuery class.
		/// </summary>
		public SKUProfileEffectiveQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the SKUProfileEffectiveQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SKUProfileEffectiveQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SKUProfileEffectiveQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SKUProfileEffectiveQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SKUProfileEffectiveQuery
		
	#region TagsFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Tags"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TagsFilters : TagsFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TagsFilters class.
		/// </summary>
		public TagsFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the TagsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TagsFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TagsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TagsFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TagsFilters
	
	#region TagsQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="TagsParameterBuilder"/> class
	/// that is used exclusively with a <see cref="Tags"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TagsQuery : TagsParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TagsQuery class.
		/// </summary>
		public TagsQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the TagsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TagsQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TagsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TagsQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TagsQuery
		
	#region ProductTypeAttributeFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ProductTypeAttribute"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductTypeAttributeFilters : ProductTypeAttributeFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductTypeAttributeFilters class.
		/// </summary>
		public ProductTypeAttributeFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductTypeAttributeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductTypeAttributeFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductTypeAttributeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductTypeAttributeFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductTypeAttributeFilters
	
	#region ProductTypeAttributeQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ProductTypeAttributeParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ProductTypeAttribute"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductTypeAttributeQuery : ProductTypeAttributeParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductTypeAttributeQuery class.
		/// </summary>
		public ProductTypeAttributeQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductTypeAttributeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductTypeAttributeQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductTypeAttributeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductTypeAttributeQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductTypeAttributeQuery
		
	#region ProductImageFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ProductImage"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductImageFilters : ProductImageFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductImageFilters class.
		/// </summary>
		public ProductImageFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductImageFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductImageFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductImageFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductImageFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductImageFilters
	
	#region ProductImageQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ProductImageParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ProductImage"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductImageQuery : ProductImageParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductImageQuery class.
		/// </summary>
		public ProductImageQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductImageQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductImageQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductImageQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductImageQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductImageQuery
		
	#region AddOnFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AddOn"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AddOnFilters : AddOnFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AddOnFilters class.
		/// </summary>
		public AddOnFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the AddOnFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AddOnFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AddOnFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AddOnFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AddOnFilters
	
	#region AddOnQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="AddOnParameterBuilder"/> class
	/// that is used exclusively with a <see cref="AddOn"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AddOnQuery : AddOnParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AddOnQuery class.
		/// </summary>
		public AddOnQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the AddOnQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AddOnQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AddOnQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AddOnQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AddOnQuery
		
	#region CSSFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CSS"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CSSFilters : CSSFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CSSFilters class.
		/// </summary>
		public CSSFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the CSSFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CSSFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CSSFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CSSFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CSSFilters
	
	#region CSSQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="CSSParameterBuilder"/> class
	/// that is used exclusively with a <see cref="CSS"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CSSQuery : CSSParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CSSQuery class.
		/// </summary>
		public CSSQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the CSSQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CSSQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CSSQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CSSQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CSSQuery
		
	#region PriceListFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PriceList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PriceListFilters : PriceListFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PriceListFilters class.
		/// </summary>
		public PriceListFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the PriceListFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PriceListFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PriceListFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PriceListFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PriceListFilters
	
	#region PriceListQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="PriceListParameterBuilder"/> class
	/// that is used exclusively with a <see cref="PriceList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PriceListQuery : PriceListParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PriceListQuery class.
		/// </summary>
		public PriceListQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the PriceListQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PriceListQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PriceListQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PriceListQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PriceListQuery
		
	#region ProductHighlightFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ProductHighlight"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductHighlightFilters : ProductHighlightFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductHighlightFilters class.
		/// </summary>
		public ProductHighlightFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductHighlightFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductHighlightFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductHighlightFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductHighlightFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductHighlightFilters
	
	#region ProductHighlightQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ProductHighlightParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ProductHighlight"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductHighlightQuery : ProductHighlightParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductHighlightQuery class.
		/// </summary>
		public ProductHighlightQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductHighlightQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductHighlightQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductHighlightQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductHighlightQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductHighlightQuery
		
	#region CategoryFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Category"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CategoryFilters : CategoryFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CategoryFilters class.
		/// </summary>
		public CategoryFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the CategoryFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CategoryFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CategoryFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CategoryFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CategoryFilters
	
	#region CategoryQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="CategoryParameterBuilder"/> class
	/// that is used exclusively with a <see cref="Category"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CategoryQuery : CategoryParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CategoryQuery class.
		/// </summary>
		public CategoryQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the CategoryQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CategoryQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CategoryQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CategoryQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CategoryQuery
		
	#region CaseTypeFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CaseType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CaseTypeFilters : CaseTypeFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CaseTypeFilters class.
		/// </summary>
		public CaseTypeFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the CaseTypeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CaseTypeFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CaseTypeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CaseTypeFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CaseTypeFilters
	
	#region CaseTypeQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="CaseTypeParameterBuilder"/> class
	/// that is used exclusively with a <see cref="CaseType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CaseTypeQuery : CaseTypeParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CaseTypeQuery class.
		/// </summary>
		public CaseTypeQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the CaseTypeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CaseTypeQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CaseTypeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CaseTypeQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CaseTypeQuery
		
	#region ResetPasswordFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ResetPassword"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ResetPasswordFilters : ResetPasswordFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ResetPasswordFilters class.
		/// </summary>
		public ResetPasswordFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ResetPasswordFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ResetPasswordFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ResetPasswordFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ResetPasswordFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ResetPasswordFilters
	
	#region ResetPasswordQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ResetPasswordParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ResetPassword"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ResetPasswordQuery : ResetPasswordParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ResetPasswordQuery class.
		/// </summary>
		public ResetPasswordQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ResetPasswordQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ResetPasswordQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ResetPasswordQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ResetPasswordQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ResetPasswordQuery
		
	#region CategoryProfileFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CategoryProfile"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CategoryProfileFilters : CategoryProfileFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CategoryProfileFilters class.
		/// </summary>
		public CategoryProfileFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the CategoryProfileFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CategoryProfileFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CategoryProfileFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CategoryProfileFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CategoryProfileFilters
	
	#region CategoryProfileQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="CategoryProfileParameterBuilder"/> class
	/// that is used exclusively with a <see cref="CategoryProfile"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CategoryProfileQuery : CategoryProfileParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CategoryProfileQuery class.
		/// </summary>
		public CategoryProfileQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the CategoryProfileQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CategoryProfileQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CategoryProfileQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CategoryProfileQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CategoryProfileQuery
		
	#region AccountProfileFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AccountProfile"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AccountProfileFilters : AccountProfileFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AccountProfileFilters class.
		/// </summary>
		public AccountProfileFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the AccountProfileFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AccountProfileFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AccountProfileFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AccountProfileFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AccountProfileFilters
	
	#region AccountProfileQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="AccountProfileParameterBuilder"/> class
	/// that is used exclusively with a <see cref="AccountProfile"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AccountProfileQuery : AccountProfileParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AccountProfileQuery class.
		/// </summary>
		public AccountProfileQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the AccountProfileQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AccountProfileQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AccountProfileQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AccountProfileQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AccountProfileQuery
		
	#region ProductReviewHistoryFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ProductReviewHistory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductReviewHistoryFilters : ProductReviewHistoryFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductReviewHistoryFilters class.
		/// </summary>
		public ProductReviewHistoryFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductReviewHistoryFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductReviewHistoryFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductReviewHistoryFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductReviewHistoryFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductReviewHistoryFilters
	
	#region ProductReviewHistoryQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ProductReviewHistoryParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ProductReviewHistory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductReviewHistoryQuery : ProductReviewHistoryParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductReviewHistoryQuery class.
		/// </summary>
		public ProductReviewHistoryQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductReviewHistoryQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductReviewHistoryQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductReviewHistoryQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductReviewHistoryQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductReviewHistoryQuery
		
	#region GatewayFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Gateway"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class GatewayFilters : GatewayFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the GatewayFilters class.
		/// </summary>
		public GatewayFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the GatewayFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public GatewayFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the GatewayFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public GatewayFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion GatewayFilters
	
	#region GatewayQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="GatewayParameterBuilder"/> class
	/// that is used exclusively with a <see cref="Gateway"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class GatewayQuery : GatewayParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the GatewayQuery class.
		/// </summary>
		public GatewayQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the GatewayQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public GatewayQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the GatewayQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public GatewayQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion GatewayQuery
		
	#region SavedCartLineItemFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SavedCartLineItem"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SavedCartLineItemFilters : SavedCartLineItemFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SavedCartLineItemFilters class.
		/// </summary>
		public SavedCartLineItemFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the SavedCartLineItemFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SavedCartLineItemFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SavedCartLineItemFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SavedCartLineItemFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SavedCartLineItemFilters
	
	#region SavedCartLineItemQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SavedCartLineItemParameterBuilder"/> class
	/// that is used exclusively with a <see cref="SavedCartLineItem"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SavedCartLineItemQuery : SavedCartLineItemParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SavedCartLineItemQuery class.
		/// </summary>
		public SavedCartLineItemQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the SavedCartLineItemQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SavedCartLineItemQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SavedCartLineItemQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SavedCartLineItemQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SavedCartLineItemQuery
		
	#region SKUInventoryFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SKUInventory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SKUInventoryFilters : SKUInventoryFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SKUInventoryFilters class.
		/// </summary>
		public SKUInventoryFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the SKUInventoryFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SKUInventoryFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SKUInventoryFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SKUInventoryFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SKUInventoryFilters
	
	#region SKUInventoryQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SKUInventoryParameterBuilder"/> class
	/// that is used exclusively with a <see cref="SKUInventory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SKUInventoryQuery : SKUInventoryParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SKUInventoryQuery class.
		/// </summary>
		public SKUInventoryQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the SKUInventoryQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SKUInventoryQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SKUInventoryQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SKUInventoryQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SKUInventoryQuery
		
	#region WishListFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="WishList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class WishListFilters : WishListFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the WishListFilters class.
		/// </summary>
		public WishListFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the WishListFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public WishListFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the WishListFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public WishListFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion WishListFilters
	
	#region WishListQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="WishListParameterBuilder"/> class
	/// that is used exclusively with a <see cref="WishList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class WishListQuery : WishListParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the WishListQuery class.
		/// </summary>
		public WishListQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the WishListQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public WishListQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the WishListQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public WishListQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion WishListQuery
		
	#region ProductCategoryFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ProductCategory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductCategoryFilters : ProductCategoryFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductCategoryFilters class.
		/// </summary>
		public ProductCategoryFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductCategoryFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductCategoryFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductCategoryFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductCategoryFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductCategoryFilters
	
	#region ProductCategoryQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ProductCategoryParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ProductCategory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductCategoryQuery : ProductCategoryParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductCategoryQuery class.
		/// </summary>
		public ProductCategoryQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductCategoryQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductCategoryQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductCategoryQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductCategoryQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductCategoryQuery
		
	#region ContentPageFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ContentPage"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ContentPageFilters : ContentPageFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ContentPageFilters class.
		/// </summary>
		public ContentPageFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ContentPageFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ContentPageFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ContentPageFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ContentPageFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ContentPageFilters
	
	#region ContentPageQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ContentPageParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ContentPage"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ContentPageQuery : ContentPageParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ContentPageQuery class.
		/// </summary>
		public ContentPageQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ContentPageQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ContentPageQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ContentPageQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ContentPageQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ContentPageQuery
		
	#region ProductAttributeFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ProductAttribute"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductAttributeFilters : ProductAttributeFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductAttributeFilters class.
		/// </summary>
		public ProductAttributeFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductAttributeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductAttributeFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductAttributeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductAttributeFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductAttributeFilters
	
	#region ProductAttributeQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ProductAttributeParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ProductAttribute"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductAttributeQuery : ProductAttributeParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductAttributeQuery class.
		/// </summary>
		public ProductAttributeQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductAttributeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductAttributeQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductAttributeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductAttributeQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductAttributeQuery
		
	#region PortalCatalogFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PortalCatalog"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PortalCatalogFilters : PortalCatalogFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PortalCatalogFilters class.
		/// </summary>
		public PortalCatalogFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the PortalCatalogFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PortalCatalogFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PortalCatalogFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PortalCatalogFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PortalCatalogFilters
	
	#region PortalCatalogQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="PortalCatalogParameterBuilder"/> class
	/// that is used exclusively with a <see cref="PortalCatalog"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PortalCatalogQuery : PortalCatalogParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PortalCatalogQuery class.
		/// </summary>
		public PortalCatalogQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the PortalCatalogQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PortalCatalogQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PortalCatalogQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PortalCatalogQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PortalCatalogQuery
		
	#region ContentPageRevisionFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ContentPageRevision"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ContentPageRevisionFilters : ContentPageRevisionFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ContentPageRevisionFilters class.
		/// </summary>
		public ContentPageRevisionFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ContentPageRevisionFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ContentPageRevisionFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ContentPageRevisionFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ContentPageRevisionFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ContentPageRevisionFilters
	
	#region ContentPageRevisionQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ContentPageRevisionParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ContentPageRevision"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ContentPageRevisionQuery : ContentPageRevisionParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ContentPageRevisionQuery class.
		/// </summary>
		public ContentPageRevisionQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ContentPageRevisionQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ContentPageRevisionQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ContentPageRevisionQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ContentPageRevisionQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ContentPageRevisionQuery
		
	#region SKUAttributeFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SKUAttribute"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SKUAttributeFilters : SKUAttributeFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SKUAttributeFilters class.
		/// </summary>
		public SKUAttributeFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the SKUAttributeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SKUAttributeFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SKUAttributeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SKUAttributeFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SKUAttributeFilters
	
	#region SKUAttributeQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SKUAttributeParameterBuilder"/> class
	/// that is used exclusively with a <see cref="SKUAttribute"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SKUAttributeQuery : SKUAttributeParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SKUAttributeQuery class.
		/// </summary>
		public SKUAttributeQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the SKUAttributeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SKUAttributeQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SKUAttributeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SKUAttributeQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SKUAttributeQuery
		
	#region ShippingFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Shipping"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ShippingFilters : ShippingFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ShippingFilters class.
		/// </summary>
		public ShippingFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ShippingFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ShippingFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ShippingFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ShippingFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ShippingFilters
	
	#region ShippingQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ShippingParameterBuilder"/> class
	/// that is used exclusively with a <see cref="Shipping"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ShippingQuery : ShippingParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ShippingQuery class.
		/// </summary>
		public ShippingQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ShippingQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ShippingQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ShippingQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ShippingQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ShippingQuery
		
	#region ProductAddOnFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ProductAddOn"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductAddOnFilters : ProductAddOnFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductAddOnFilters class.
		/// </summary>
		public ProductAddOnFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductAddOnFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductAddOnFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductAddOnFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductAddOnFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductAddOnFilters
	
	#region ProductAddOnQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ProductAddOnParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ProductAddOn"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductAddOnQuery : ProductAddOnParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductAddOnQuery class.
		/// </summary>
		public ProductAddOnQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductAddOnQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductAddOnQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductAddOnQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductAddOnQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductAddOnQuery
		
	#region HighlightFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Highlight"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class HighlightFilters : HighlightFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the HighlightFilters class.
		/// </summary>
		public HighlightFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the HighlightFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public HighlightFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the HighlightFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public HighlightFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion HighlightFilters
	
	#region HighlightQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="HighlightParameterBuilder"/> class
	/// that is used exclusively with a <see cref="Highlight"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class HighlightQuery : HighlightParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the HighlightQuery class.
		/// </summary>
		public HighlightQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the HighlightQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public HighlightQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the HighlightQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public HighlightQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion HighlightQuery
		
	#region CategoryNodeFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CategoryNode"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CategoryNodeFilters : CategoryNodeFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CategoryNodeFilters class.
		/// </summary>
		public CategoryNodeFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the CategoryNodeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CategoryNodeFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CategoryNodeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CategoryNodeFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CategoryNodeFilters
	
	#region CategoryNodeQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="CategoryNodeParameterBuilder"/> class
	/// that is used exclusively with a <see cref="CategoryNode"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CategoryNodeQuery : CategoryNodeParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CategoryNodeQuery class.
		/// </summary>
		public CategoryNodeQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the CategoryNodeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CategoryNodeQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CategoryNodeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CategoryNodeQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CategoryNodeQuery
		
	#region AddressFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Address"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AddressFilters : AddressFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AddressFilters class.
		/// </summary>
		public AddressFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the AddressFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AddressFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AddressFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AddressFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AddressFilters
	
	#region AddressQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="AddressParameterBuilder"/> class
	/// that is used exclusively with a <see cref="Address"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AddressQuery : AddressParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AddressQuery class.
		/// </summary>
		public AddressQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the AddressQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AddressQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AddressQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AddressQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AddressQuery
		
	#region ActivityLogFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ActivityLog"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ActivityLogFilters : ActivityLogFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ActivityLogFilters class.
		/// </summary>
		public ActivityLogFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ActivityLogFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ActivityLogFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ActivityLogFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ActivityLogFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ActivityLogFilters
	
	#region ActivityLogQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ActivityLogParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ActivityLog"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ActivityLogQuery : ActivityLogParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ActivityLogQuery class.
		/// </summary>
		public ActivityLogQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ActivityLogQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ActivityLogQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ActivityLogQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ActivityLogQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ActivityLogQuery
		
	#region FacetFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Facet"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FacetFilters : FacetFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FacetFilters class.
		/// </summary>
		public FacetFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the FacetFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FacetFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FacetFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FacetFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FacetFilters
	
	#region FacetQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="FacetParameterBuilder"/> class
	/// that is used exclusively with a <see cref="Facet"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FacetQuery : FacetParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FacetQuery class.
		/// </summary>
		public FacetQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the FacetQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FacetQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FacetQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FacetQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FacetQuery
		
	#region HighlightTypeFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="HighlightType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class HighlightTypeFilters : HighlightTypeFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the HighlightTypeFilters class.
		/// </summary>
		public HighlightTypeFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the HighlightTypeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public HighlightTypeFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the HighlightTypeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public HighlightTypeFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion HighlightTypeFilters
	
	#region HighlightTypeQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="HighlightTypeParameterBuilder"/> class
	/// that is used exclusively with a <see cref="HighlightType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class HighlightTypeQuery : HighlightTypeParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the HighlightTypeQuery class.
		/// </summary>
		public HighlightTypeQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the HighlightTypeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public HighlightTypeQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the HighlightTypeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public HighlightTypeQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion HighlightTypeQuery
		
	#region ProductProfileFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ProductProfile"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductProfileFilters : ProductProfileFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductProfileFilters class.
		/// </summary>
		public ProductProfileFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductProfileFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductProfileFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductProfileFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductProfileFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductProfileFilters
	
	#region ProductProfileQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ProductProfileParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ProductProfile"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductProfileQuery : ProductProfileParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductProfileQuery class.
		/// </summary>
		public ProductProfileQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductProfileQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductProfileQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductProfileQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductProfileQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductProfileQuery
		
	#region CaseRequestFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CaseRequest"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CaseRequestFilters : CaseRequestFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CaseRequestFilters class.
		/// </summary>
		public CaseRequestFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the CaseRequestFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CaseRequestFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CaseRequestFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CaseRequestFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CaseRequestFilters
	
	#region CaseRequestQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="CaseRequestParameterBuilder"/> class
	/// that is used exclusively with a <see cref="CaseRequest"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CaseRequestQuery : CaseRequestParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CaseRequestQuery class.
		/// </summary>
		public CaseRequestQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the CaseRequestQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CaseRequestQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CaseRequestQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CaseRequestQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CaseRequestQuery
		
	#region ReviewFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Review"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ReviewFilters : ReviewFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ReviewFilters class.
		/// </summary>
		public ReviewFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ReviewFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ReviewFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ReviewFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ReviewFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ReviewFilters
	
	#region ReviewQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ReviewParameterBuilder"/> class
	/// that is used exclusively with a <see cref="Review"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ReviewQuery : ReviewParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ReviewQuery class.
		/// </summary>
		public ReviewQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ReviewQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ReviewQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ReviewQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ReviewQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ReviewQuery
		
	#region TrackingOutboundFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TrackingOutbound"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TrackingOutboundFilters : TrackingOutboundFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TrackingOutboundFilters class.
		/// </summary>
		public TrackingOutboundFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the TrackingOutboundFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TrackingOutboundFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TrackingOutboundFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TrackingOutboundFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TrackingOutboundFilters
	
	#region TrackingOutboundQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="TrackingOutboundParameterBuilder"/> class
	/// that is used exclusively with a <see cref="TrackingOutbound"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TrackingOutboundQuery : TrackingOutboundParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TrackingOutboundQuery class.
		/// </summary>
		public TrackingOutboundQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the TrackingOutboundQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TrackingOutboundQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TrackingOutboundQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TrackingOutboundQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TrackingOutboundQuery
		
	#region ActivityLogTypeFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ActivityLogType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ActivityLogTypeFilters : ActivityLogTypeFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ActivityLogTypeFilters class.
		/// </summary>
		public ActivityLogTypeFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ActivityLogTypeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ActivityLogTypeFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ActivityLogTypeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ActivityLogTypeFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ActivityLogTypeFilters
	
	#region ActivityLogTypeQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ActivityLogTypeParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ActivityLogType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ActivityLogTypeQuery : ActivityLogTypeParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ActivityLogTypeQuery class.
		/// </summary>
		public ActivityLogTypeQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ActivityLogTypeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ActivityLogTypeQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ActivityLogTypeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ActivityLogTypeQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ActivityLogTypeQuery
		
	#region PortalProfileFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PortalProfile"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PortalProfileFilters : PortalProfileFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PortalProfileFilters class.
		/// </summary>
		public PortalProfileFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the PortalProfileFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PortalProfileFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PortalProfileFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PortalProfileFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PortalProfileFilters
	
	#region PortalProfileQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="PortalProfileParameterBuilder"/> class
	/// that is used exclusively with a <see cref="PortalProfile"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PortalProfileQuery : PortalProfileParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PortalProfileQuery class.
		/// </summary>
		public PortalProfileQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the PortalProfileQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PortalProfileQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PortalProfileQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PortalProfileQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PortalProfileQuery
		
	#region FacetGroupFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FacetGroup"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FacetGroupFilters : FacetGroupFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FacetGroupFilters class.
		/// </summary>
		public FacetGroupFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the FacetGroupFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FacetGroupFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FacetGroupFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FacetGroupFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FacetGroupFilters
	
	#region FacetGroupQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="FacetGroupParameterBuilder"/> class
	/// that is used exclusively with a <see cref="FacetGroup"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FacetGroupQuery : FacetGroupParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FacetGroupQuery class.
		/// </summary>
		public FacetGroupQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the FacetGroupQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FacetGroupQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FacetGroupQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FacetGroupQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FacetGroupQuery
		
	#region LuceneDocumentMappingFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="LuceneDocumentMapping"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class LuceneDocumentMappingFilters : LuceneDocumentMappingFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the LuceneDocumentMappingFilters class.
		/// </summary>
		public LuceneDocumentMappingFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the LuceneDocumentMappingFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public LuceneDocumentMappingFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the LuceneDocumentMappingFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public LuceneDocumentMappingFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion LuceneDocumentMappingFilters
	
	#region LuceneDocumentMappingQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="LuceneDocumentMappingParameterBuilder"/> class
	/// that is used exclusively with a <see cref="LuceneDocumentMapping"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class LuceneDocumentMappingQuery : LuceneDocumentMappingParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the LuceneDocumentMappingQuery class.
		/// </summary>
		public LuceneDocumentMappingQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the LuceneDocumentMappingQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public LuceneDocumentMappingQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the LuceneDocumentMappingQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public LuceneDocumentMappingQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion LuceneDocumentMappingQuery
		
	#region FacetControlTypeFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FacetControlType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FacetControlTypeFilters : FacetControlTypeFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FacetControlTypeFilters class.
		/// </summary>
		public FacetControlTypeFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the FacetControlTypeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FacetControlTypeFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FacetControlTypeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FacetControlTypeFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FacetControlTypeFilters
	
	#region FacetControlTypeQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="FacetControlTypeParameterBuilder"/> class
	/// that is used exclusively with a <see cref="FacetControlType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FacetControlTypeQuery : FacetControlTypeParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FacetControlTypeQuery class.
		/// </summary>
		public FacetControlTypeQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the FacetControlTypeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FacetControlTypeQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FacetControlTypeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FacetControlTypeQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FacetControlTypeQuery
		
	#region DomainFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Domain"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class DomainFilters : DomainFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the DomainFilters class.
		/// </summary>
		public DomainFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the DomainFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public DomainFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the DomainFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public DomainFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion DomainFilters
	
	#region DomainQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="DomainParameterBuilder"/> class
	/// that is used exclusively with a <see cref="Domain"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class DomainQuery : DomainParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the DomainQuery class.
		/// </summary>
		public DomainQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the DomainQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public DomainQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the DomainQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public DomainQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion DomainQuery
		
	#region FacetProductSKUFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FacetProductSKU"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FacetProductSKUFilters : FacetProductSKUFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FacetProductSKUFilters class.
		/// </summary>
		public FacetProductSKUFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the FacetProductSKUFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FacetProductSKUFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FacetProductSKUFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FacetProductSKUFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FacetProductSKUFilters
	
	#region FacetProductSKUQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="FacetProductSKUParameterBuilder"/> class
	/// that is used exclusively with a <see cref="FacetProductSKU"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FacetProductSKUQuery : FacetProductSKUParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FacetProductSKUQuery class.
		/// </summary>
		public FacetProductSKUQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the FacetProductSKUQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FacetProductSKUQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FacetProductSKUQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FacetProductSKUQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FacetProductSKUQuery
		
	#region DiscountTypeFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="DiscountType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class DiscountTypeFilters : DiscountTypeFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the DiscountTypeFilters class.
		/// </summary>
		public DiscountTypeFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the DiscountTypeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public DiscountTypeFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the DiscountTypeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public DiscountTypeFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion DiscountTypeFilters
	
	#region DiscountTypeQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="DiscountTypeParameterBuilder"/> class
	/// that is used exclusively with a <see cref="DiscountType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class DiscountTypeQuery : DiscountTypeParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the DiscountTypeQuery class.
		/// </summary>
		public DiscountTypeQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the DiscountTypeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public DiscountTypeQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the DiscountTypeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public DiscountTypeQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion DiscountTypeQuery
		
	#region PortalCountryFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PortalCountry"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PortalCountryFilters : PortalCountryFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PortalCountryFilters class.
		/// </summary>
		public PortalCountryFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the PortalCountryFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PortalCountryFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PortalCountryFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PortalCountryFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PortalCountryFilters
	
	#region PortalCountryQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="PortalCountryParameterBuilder"/> class
	/// that is used exclusively with a <see cref="PortalCountry"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PortalCountryQuery : PortalCountryParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PortalCountryQuery class.
		/// </summary>
		public PortalCountryQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the PortalCountryQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PortalCountryQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PortalCountryQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PortalCountryQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PortalCountryQuery
		
	#region ZipCodeFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ZipCode"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ZipCodeFilters : ZipCodeFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ZipCodeFilters class.
		/// </summary>
		public ZipCodeFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ZipCodeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ZipCodeFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ZipCodeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ZipCodeFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ZipCodeFilters
	
	#region ZipCodeQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ZipCodeParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ZipCode"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ZipCodeQuery : ZipCodeParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ZipCodeQuery class.
		/// </summary>
		public ZipCodeQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ZipCodeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ZipCodeQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ZipCodeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ZipCodeQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ZipCodeQuery
		
	#region SKUProfileFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SKUProfile"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SKUProfileFilters : SKUProfileFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SKUProfileFilters class.
		/// </summary>
		public SKUProfileFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the SKUProfileFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SKUProfileFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SKUProfileFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SKUProfileFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SKUProfileFilters
	
	#region SKUProfileQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SKUProfileParameterBuilder"/> class
	/// that is used exclusively with a <see cref="SKUProfile"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SKUProfileQuery : SKUProfileParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SKUProfileQuery class.
		/// </summary>
		public SKUProfileQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the SKUProfileQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SKUProfileQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SKUProfileQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SKUProfileQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SKUProfileQuery
		
	#region TrackingFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Tracking"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TrackingFilters : TrackingFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TrackingFilters class.
		/// </summary>
		public TrackingFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the TrackingFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TrackingFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TrackingFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TrackingFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TrackingFilters
	
	#region TrackingQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="TrackingParameterBuilder"/> class
	/// that is used exclusively with a <see cref="Tracking"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TrackingQuery : TrackingParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TrackingQuery class.
		/// </summary>
		public TrackingQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the TrackingQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TrackingQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TrackingQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TrackingQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TrackingQuery
		
	#region LuceneGlobalProductBoostFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="LuceneGlobalProductBoost"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class LuceneGlobalProductBoostFilters : LuceneGlobalProductBoostFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the LuceneGlobalProductBoostFilters class.
		/// </summary>
		public LuceneGlobalProductBoostFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the LuceneGlobalProductBoostFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public LuceneGlobalProductBoostFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the LuceneGlobalProductBoostFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public LuceneGlobalProductBoostFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion LuceneGlobalProductBoostFilters
	
	#region LuceneGlobalProductBoostQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="LuceneGlobalProductBoostParameterBuilder"/> class
	/// that is used exclusively with a <see cref="LuceneGlobalProductBoost"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class LuceneGlobalProductBoostQuery : LuceneGlobalProductBoostParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the LuceneGlobalProductBoostQuery class.
		/// </summary>
		public LuceneGlobalProductBoostQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the LuceneGlobalProductBoostQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public LuceneGlobalProductBoostQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the LuceneGlobalProductBoostQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public LuceneGlobalProductBoostQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion LuceneGlobalProductBoostQuery
		
	#region CasePriorityFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CasePriority"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CasePriorityFilters : CasePriorityFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CasePriorityFilters class.
		/// </summary>
		public CasePriorityFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the CasePriorityFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CasePriorityFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CasePriorityFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CasePriorityFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CasePriorityFilters
	
	#region CasePriorityQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="CasePriorityParameterBuilder"/> class
	/// that is used exclusively with a <see cref="CasePriority"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CasePriorityQuery : CasePriorityParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CasePriorityQuery class.
		/// </summary>
		public CasePriorityQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the CasePriorityQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CasePriorityQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CasePriorityQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CasePriorityQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CasePriorityQuery
		
	#region AddOnValueFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AddOnValue"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AddOnValueFilters : AddOnValueFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AddOnValueFilters class.
		/// </summary>
		public AddOnValueFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the AddOnValueFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AddOnValueFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AddOnValueFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AddOnValueFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AddOnValueFilters
	
	#region AddOnValueQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="AddOnValueParameterBuilder"/> class
	/// that is used exclusively with a <see cref="AddOnValue"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AddOnValueQuery : AddOnValueParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AddOnValueQuery class.
		/// </summary>
		public AddOnValueQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the AddOnValueQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AddOnValueQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AddOnValueQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AddOnValueQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AddOnValueQuery
		
	#region PromotionFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Promotion"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PromotionFilters : PromotionFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PromotionFilters class.
		/// </summary>
		public PromotionFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the PromotionFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PromotionFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PromotionFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PromotionFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PromotionFilters
	
	#region PromotionQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="PromotionParameterBuilder"/> class
	/// that is used exclusively with a <see cref="Promotion"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PromotionQuery : PromotionParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PromotionQuery class.
		/// </summary>
		public PromotionQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the PromotionQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PromotionQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PromotionQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PromotionQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PromotionQuery
		
	#region ProductTierFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ProductTier"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductTierFilters : ProductTierFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductTierFilters class.
		/// </summary>
		public ProductTierFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductTierFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductTierFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductTierFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductTierFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductTierFilters
	
	#region ProductTierQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ProductTierParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ProductTier"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductTierQuery : ProductTierParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductTierQuery class.
		/// </summary>
		public ProductTierQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductTierQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductTierQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductTierQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductTierQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductTierQuery
		
	#region TaxRuleFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TaxRule"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TaxRuleFilters : TaxRuleFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TaxRuleFilters class.
		/// </summary>
		public TaxRuleFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the TaxRuleFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TaxRuleFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TaxRuleFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TaxRuleFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TaxRuleFilters
	
	#region TaxRuleQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="TaxRuleParameterBuilder"/> class
	/// that is used exclusively with a <see cref="TaxRule"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TaxRuleQuery : TaxRuleParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TaxRuleQuery class.
		/// </summary>
		public TaxRuleQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the TaxRuleQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TaxRuleQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TaxRuleQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TaxRuleQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TaxRuleQuery
		
	#region ShippingRuleFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ShippingRule"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ShippingRuleFilters : ShippingRuleFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ShippingRuleFilters class.
		/// </summary>
		public ShippingRuleFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ShippingRuleFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ShippingRuleFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ShippingRuleFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ShippingRuleFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ShippingRuleFilters
	
	#region ShippingRuleQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ShippingRuleParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ShippingRule"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ShippingRuleQuery : ShippingRuleParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ShippingRuleQuery class.
		/// </summary>
		public ShippingRuleQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ShippingRuleQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ShippingRuleQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ShippingRuleQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ShippingRuleQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ShippingRuleQuery
		
	#region LuceneGlobalProductCategoryBoostFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="LuceneGlobalProductCategoryBoost"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class LuceneGlobalProductCategoryBoostFilters : LuceneGlobalProductCategoryBoostFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the LuceneGlobalProductCategoryBoostFilters class.
		/// </summary>
		public LuceneGlobalProductCategoryBoostFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the LuceneGlobalProductCategoryBoostFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public LuceneGlobalProductCategoryBoostFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the LuceneGlobalProductCategoryBoostFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public LuceneGlobalProductCategoryBoostFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion LuceneGlobalProductCategoryBoostFilters
	
	#region LuceneGlobalProductCategoryBoostQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="LuceneGlobalProductCategoryBoostParameterBuilder"/> class
	/// that is used exclusively with a <see cref="LuceneGlobalProductCategoryBoost"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class LuceneGlobalProductCategoryBoostQuery : LuceneGlobalProductCategoryBoostParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the LuceneGlobalProductCategoryBoostQuery class.
		/// </summary>
		public LuceneGlobalProductCategoryBoostQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the LuceneGlobalProductCategoryBoostQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public LuceneGlobalProductCategoryBoostQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the LuceneGlobalProductCategoryBoostQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public LuceneGlobalProductCategoryBoostQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion LuceneGlobalProductCategoryBoostQuery
		
	#region SavedCartFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SavedCart"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SavedCartFilters : SavedCartFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SavedCartFilters class.
		/// </summary>
		public SavedCartFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the SavedCartFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SavedCartFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SavedCartFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SavedCartFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SavedCartFilters
	
	#region SavedCartQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SavedCartParameterBuilder"/> class
	/// that is used exclusively with a <see cref="SavedCart"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SavedCartQuery : SavedCartParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SavedCartQuery class.
		/// </summary>
		public SavedCartQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the SavedCartQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SavedCartQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SavedCartQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SavedCartQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SavedCartQuery
		
	#region ProductCrossSellFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ProductCrossSell"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductCrossSellFilters : ProductCrossSellFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductCrossSellFilters class.
		/// </summary>
		public ProductCrossSellFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductCrossSellFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductCrossSellFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductCrossSellFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductCrossSellFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductCrossSellFilters
	
	#region ProductCrossSellQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ProductCrossSellParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ProductCrossSell"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductCrossSellQuery : ProductCrossSellParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductCrossSellQuery class.
		/// </summary>
		public ProductCrossSellQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductCrossSellQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductCrossSellQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductCrossSellQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductCrossSellQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductCrossSellQuery
		
	#region LuceneGlobalProductBrandBoostFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="LuceneGlobalProductBrandBoost"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class LuceneGlobalProductBrandBoostFilters : LuceneGlobalProductBrandBoostFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the LuceneGlobalProductBrandBoostFilters class.
		/// </summary>
		public LuceneGlobalProductBrandBoostFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the LuceneGlobalProductBrandBoostFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public LuceneGlobalProductBrandBoostFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the LuceneGlobalProductBrandBoostFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public LuceneGlobalProductBrandBoostFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion LuceneGlobalProductBrandBoostFilters
	
	#region LuceneGlobalProductBrandBoostQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="LuceneGlobalProductBrandBoostParameterBuilder"/> class
	/// that is used exclusively with a <see cref="LuceneGlobalProductBrandBoost"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class LuceneGlobalProductBrandBoostQuery : LuceneGlobalProductBrandBoostParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the LuceneGlobalProductBrandBoostQuery class.
		/// </summary>
		public LuceneGlobalProductBrandBoostQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the LuceneGlobalProductBrandBoostQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public LuceneGlobalProductBrandBoostQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the LuceneGlobalProductBrandBoostQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public LuceneGlobalProductBrandBoostQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion LuceneGlobalProductBrandBoostQuery
		
	#region TaxRuleTypeFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TaxRuleType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TaxRuleTypeFilters : TaxRuleTypeFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TaxRuleTypeFilters class.
		/// </summary>
		public TaxRuleTypeFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the TaxRuleTypeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TaxRuleTypeFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TaxRuleTypeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TaxRuleTypeFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TaxRuleTypeFilters
	
	#region TaxRuleTypeQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="TaxRuleTypeParameterBuilder"/> class
	/// that is used exclusively with a <see cref="TaxRuleType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TaxRuleTypeQuery : TaxRuleTypeParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TaxRuleTypeQuery class.
		/// </summary>
		public TaxRuleTypeQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the TaxRuleTypeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TaxRuleTypeQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TaxRuleTypeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TaxRuleTypeQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TaxRuleTypeQuery
		
	#region FacetGroupCategoryFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FacetGroupCategory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FacetGroupCategoryFilters : FacetGroupCategoryFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FacetGroupCategoryFilters class.
		/// </summary>
		public FacetGroupCategoryFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the FacetGroupCategoryFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FacetGroupCategoryFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FacetGroupCategoryFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FacetGroupCategoryFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FacetGroupCategoryFilters
	
	#region FacetGroupCategoryQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="FacetGroupCategoryParameterBuilder"/> class
	/// that is used exclusively with a <see cref="FacetGroupCategory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FacetGroupCategoryQuery : FacetGroupCategoryParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FacetGroupCategoryQuery class.
		/// </summary>
		public FacetGroupCategoryQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the FacetGroupCategoryQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FacetGroupCategoryQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FacetGroupCategoryQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FacetGroupCategoryQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FacetGroupCategoryQuery
		
	#region AttributeTypeFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AttributeType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AttributeTypeFilters : AttributeTypeFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AttributeTypeFilters class.
		/// </summary>
		public AttributeTypeFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the AttributeTypeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AttributeTypeFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AttributeTypeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AttributeTypeFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AttributeTypeFilters
	
	#region AttributeTypeQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="AttributeTypeParameterBuilder"/> class
	/// that is used exclusively with a <see cref="AttributeType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AttributeTypeQuery : AttributeTypeParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AttributeTypeQuery class.
		/// </summary>
		public AttributeTypeQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the AttributeTypeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AttributeTypeQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AttributeTypeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AttributeTypeQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AttributeTypeQuery
		
	#region NoteFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Note"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class NoteFilters : NoteFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the NoteFilters class.
		/// </summary>
		public NoteFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the NoteFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public NoteFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the NoteFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public NoteFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion NoteFilters
	
	#region NoteQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="NoteParameterBuilder"/> class
	/// that is used exclusively with a <see cref="Note"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class NoteQuery : NoteParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the NoteQuery class.
		/// </summary>
		public NoteQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the NoteQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public NoteQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the NoteQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public NoteQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion NoteQuery
		
	#region TrackingEventFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TrackingEvent"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TrackingEventFilters : TrackingEventFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TrackingEventFilters class.
		/// </summary>
		public TrackingEventFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the TrackingEventFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TrackingEventFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TrackingEventFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TrackingEventFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TrackingEventFilters
	
	#region TrackingEventQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="TrackingEventParameterBuilder"/> class
	/// that is used exclusively with a <see cref="TrackingEvent"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TrackingEventQuery : TrackingEventParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TrackingEventQuery class.
		/// </summary>
		public TrackingEventQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the TrackingEventQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TrackingEventQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TrackingEventQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TrackingEventQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TrackingEventQuery
		
	#region LuceneGlobalProductCatalogBoostFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="LuceneGlobalProductCatalogBoost"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class LuceneGlobalProductCatalogBoostFilters : LuceneGlobalProductCatalogBoostFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the LuceneGlobalProductCatalogBoostFilters class.
		/// </summary>
		public LuceneGlobalProductCatalogBoostFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the LuceneGlobalProductCatalogBoostFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public LuceneGlobalProductCatalogBoostFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the LuceneGlobalProductCatalogBoostFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public LuceneGlobalProductCatalogBoostFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion LuceneGlobalProductCatalogBoostFilters
	
	#region LuceneGlobalProductCatalogBoostQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="LuceneGlobalProductCatalogBoostParameterBuilder"/> class
	/// that is used exclusively with a <see cref="LuceneGlobalProductCatalogBoost"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class LuceneGlobalProductCatalogBoostQuery : LuceneGlobalProductCatalogBoostParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the LuceneGlobalProductCatalogBoostQuery class.
		/// </summary>
		public LuceneGlobalProductCatalogBoostQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the LuceneGlobalProductCatalogBoostQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public LuceneGlobalProductCatalogBoostQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the LuceneGlobalProductCatalogBoostQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public LuceneGlobalProductCatalogBoostQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion LuceneGlobalProductCatalogBoostQuery
		
	#region OrderStateFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="OrderState"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class OrderStateFilters : OrderStateFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OrderStateFilters class.
		/// </summary>
		public OrderStateFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the OrderStateFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public OrderStateFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the OrderStateFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public OrderStateFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion OrderStateFilters
	
	#region OrderStateQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="OrderStateParameterBuilder"/> class
	/// that is used exclusively with a <see cref="OrderState"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class OrderStateQuery : OrderStateParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OrderStateQuery class.
		/// </summary>
		public OrderStateQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the OrderStateQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public OrderStateQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the OrderStateQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public OrderStateQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion OrderStateQuery
		
	#region OrderProcessingTypeFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="OrderProcessingType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class OrderProcessingTypeFilters : OrderProcessingTypeFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OrderProcessingTypeFilters class.
		/// </summary>
		public OrderProcessingTypeFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the OrderProcessingTypeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public OrderProcessingTypeFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the OrderProcessingTypeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public OrderProcessingTypeFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion OrderProcessingTypeFilters
	
	#region OrderProcessingTypeQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="OrderProcessingTypeParameterBuilder"/> class
	/// that is used exclusively with a <see cref="OrderProcessingType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class OrderProcessingTypeQuery : OrderProcessingTypeParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OrderProcessingTypeQuery class.
		/// </summary>
		public OrderProcessingTypeQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the OrderProcessingTypeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public OrderProcessingTypeQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the OrderProcessingTypeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public OrderProcessingTypeQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion OrderProcessingTypeQuery
		
	#region StoreFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Store"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class StoreFilters : StoreFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the StoreFilters class.
		/// </summary>
		public StoreFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the StoreFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public StoreFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the StoreFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public StoreFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion StoreFilters
	
	#region StoreQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="StoreParameterBuilder"/> class
	/// that is used exclusively with a <see cref="Store"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class StoreQuery : StoreParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the StoreQuery class.
		/// </summary>
		public StoreQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the StoreQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public StoreQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the StoreQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public StoreQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion StoreQuery
		
	#region LocaleFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Locale"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class LocaleFilters : LocaleFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the LocaleFilters class.
		/// </summary>
		public LocaleFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the LocaleFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public LocaleFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the LocaleFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public LocaleFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion LocaleFilters
	
	#region LocaleQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="LocaleParameterBuilder"/> class
	/// that is used exclusively with a <see cref="Locale"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class LocaleQuery : LocaleParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the LocaleQuery class.
		/// </summary>
		public LocaleQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the LocaleQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public LocaleQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the LocaleQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public LocaleQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion LocaleQuery
		
	#region ProductReviewStateFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ProductReviewState"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductReviewStateFilters : ProductReviewStateFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductReviewStateFilters class.
		/// </summary>
		public ProductReviewStateFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductReviewStateFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductReviewStateFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductReviewStateFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductReviewStateFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductReviewStateFilters
	
	#region ProductReviewStateQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ProductReviewStateParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ProductReviewState"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductReviewStateQuery : ProductReviewStateParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductReviewStateQuery class.
		/// </summary>
		public ProductReviewStateQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductReviewStateQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductReviewStateQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductReviewStateQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductReviewStateQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductReviewStateQuery
		
	#region AccountTypeFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AccountType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AccountTypeFilters : AccountTypeFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AccountTypeFilters class.
		/// </summary>
		public AccountTypeFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the AccountTypeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AccountTypeFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AccountTypeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AccountTypeFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AccountTypeFilters
	
	#region AccountTypeQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="AccountTypeParameterBuilder"/> class
	/// that is used exclusively with a <see cref="AccountType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AccountTypeQuery : AccountTypeParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AccountTypeQuery class.
		/// </summary>
		public AccountTypeQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the AccountTypeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AccountTypeQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AccountTypeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AccountTypeQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AccountTypeQuery
		
	#region ShippingTypeFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ShippingType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ShippingTypeFilters : ShippingTypeFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ShippingTypeFilters class.
		/// </summary>
		public ShippingTypeFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ShippingTypeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ShippingTypeFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ShippingTypeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ShippingTypeFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ShippingTypeFilters
	
	#region ShippingTypeQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ShippingTypeParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ShippingType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ShippingTypeQuery : ShippingTypeParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ShippingTypeQuery class.
		/// </summary>
		public ShippingTypeQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ShippingTypeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ShippingTypeQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ShippingTypeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ShippingTypeQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ShippingTypeQuery
		
	#region StateFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="State"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class StateFilters : StateFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the StateFilters class.
		/// </summary>
		public StateFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the StateFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public StateFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the StateFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public StateFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion StateFilters
	
	#region StateQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="StateParameterBuilder"/> class
	/// that is used exclusively with a <see cref="State"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class StateQuery : StateParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the StateQuery class.
		/// </summary>
		public StateQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the StateQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public StateQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the StateQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public StateQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion StateQuery
		
	#region CurrencyTypeFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CurrencyType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CurrencyTypeFilters : CurrencyTypeFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CurrencyTypeFilters class.
		/// </summary>
		public CurrencyTypeFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the CurrencyTypeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CurrencyTypeFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CurrencyTypeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CurrencyTypeFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CurrencyTypeFilters
	
	#region CurrencyTypeQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="CurrencyTypeParameterBuilder"/> class
	/// that is used exclusively with a <see cref="CurrencyType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CurrencyTypeQuery : CurrencyTypeParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CurrencyTypeQuery class.
		/// </summary>
		public CurrencyTypeQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the CurrencyTypeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CurrencyTypeQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CurrencyTypeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CurrencyTypeQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CurrencyTypeQuery
		
	#region ProductRelationTypeFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ProductRelationType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductRelationTypeFilters : ProductRelationTypeFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductRelationTypeFilters class.
		/// </summary>
		public ProductRelationTypeFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductRelationTypeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductRelationTypeFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductRelationTypeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductRelationTypeFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductRelationTypeFilters
	
	#region ProductRelationTypeQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ProductRelationTypeParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ProductRelationType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductRelationTypeQuery : ProductRelationTypeParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductRelationTypeQuery class.
		/// </summary>
		public ProductRelationTypeQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductRelationTypeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductRelationTypeQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductRelationTypeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductRelationTypeQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductRelationTypeQuery
		
	#region ShippingRuleTypeFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ShippingRuleType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ShippingRuleTypeFilters : ShippingRuleTypeFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ShippingRuleTypeFilters class.
		/// </summary>
		public ShippingRuleTypeFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ShippingRuleTypeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ShippingRuleTypeFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ShippingRuleTypeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ShippingRuleTypeFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ShippingRuleTypeFilters
	
	#region ShippingRuleTypeQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ShippingRuleTypeParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ShippingRuleType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ShippingRuleTypeQuery : ShippingRuleTypeParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ShippingRuleTypeQuery class.
		/// </summary>
		public ShippingRuleTypeQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ShippingRuleTypeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ShippingRuleTypeQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ShippingRuleTypeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ShippingRuleTypeQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ShippingRuleTypeQuery
		
	#region PortalFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Portal"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PortalFilters : PortalFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PortalFilters class.
		/// </summary>
		public PortalFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the PortalFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PortalFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PortalFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PortalFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PortalFilters
	
	#region PortalQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="PortalParameterBuilder"/> class
	/// that is used exclusively with a <see cref="Portal"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PortalQuery : PortalParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PortalQuery class.
		/// </summary>
		public PortalQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the PortalQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PortalQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PortalQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PortalQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PortalQuery
		
	#region ProductExtensionOptionProductFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ProductExtensionOptionProduct"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductExtensionOptionProductFilters : ProductExtensionOptionProductFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductExtensionOptionProductFilters class.
		/// </summary>
		public ProductExtensionOptionProductFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductExtensionOptionProductFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductExtensionOptionProductFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductExtensionOptionProductFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductExtensionOptionProductFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductExtensionOptionProductFilters
	
	#region ProductExtensionOptionProductQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ProductExtensionOptionProductParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ProductExtensionOptionProduct"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductExtensionOptionProductQuery : ProductExtensionOptionProductParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductExtensionOptionProductQuery class.
		/// </summary>
		public ProductExtensionOptionProductQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductExtensionOptionProductQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductExtensionOptionProductQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductExtensionOptionProductQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductExtensionOptionProductQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductExtensionOptionProductQuery
		
	#region ReferralCommissionTypeFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ReferralCommissionType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ReferralCommissionTypeFilters : ReferralCommissionTypeFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ReferralCommissionTypeFilters class.
		/// </summary>
		public ReferralCommissionTypeFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ReferralCommissionTypeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ReferralCommissionTypeFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ReferralCommissionTypeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ReferralCommissionTypeFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ReferralCommissionTypeFilters
	
	#region ReferralCommissionTypeQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ReferralCommissionTypeParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ReferralCommissionType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ReferralCommissionTypeQuery : ReferralCommissionTypeParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ReferralCommissionTypeQuery class.
		/// </summary>
		public ReferralCommissionTypeQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ReferralCommissionTypeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ReferralCommissionTypeQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ReferralCommissionTypeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ReferralCommissionTypeQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ReferralCommissionTypeQuery
		
	#region TaxClassFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TaxClass"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TaxClassFilters : TaxClassFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TaxClassFilters class.
		/// </summary>
		public TaxClassFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the TaxClassFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TaxClassFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TaxClassFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TaxClassFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TaxClassFilters
	
	#region TaxClassQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="TaxClassParameterBuilder"/> class
	/// that is used exclusively with a <see cref="TaxClass"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TaxClassQuery : TaxClassParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TaxClassQuery class.
		/// </summary>
		public TaxClassQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the TaxClassQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TaxClassQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TaxClassQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TaxClassQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TaxClassQuery
		
	#region ProductExtensionRelationFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ProductExtensionRelation"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductExtensionRelationFilters : ProductExtensionRelationFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductExtensionRelationFilters class.
		/// </summary>
		public ProductExtensionRelationFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductExtensionRelationFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductExtensionRelationFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductExtensionRelationFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductExtensionRelationFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductExtensionRelationFilters
	
	#region ProductExtensionRelationQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ProductExtensionRelationParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ProductExtensionRelation"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductExtensionRelationQuery : ProductExtensionRelationParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductExtensionRelationQuery class.
		/// </summary>
		public ProductExtensionRelationQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductExtensionRelationQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductExtensionRelationQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductExtensionRelationQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductExtensionRelationQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductExtensionRelationQuery
		
	#region ProfileFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Profile"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProfileFilters : ProfileFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProfileFilters class.
		/// </summary>
		public ProfileFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProfileFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProfileFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProfileFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProfileFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProfileFilters
	
	#region ProfileQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ProfileParameterBuilder"/> class
	/// that is used exclusively with a <see cref="Profile"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProfileQuery : ProfileParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProfileQuery class.
		/// </summary>
		public ProfileQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProfileQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProfileQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProfileQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProfileQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProfileQuery
		
	#region RMARequestFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="RMARequest"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class RMARequestFilters : RMARequestFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the RMARequestFilters class.
		/// </summary>
		public RMARequestFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the RMARequestFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public RMARequestFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the RMARequestFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public RMARequestFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion RMARequestFilters
	
	#region RMARequestQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="RMARequestParameterBuilder"/> class
	/// that is used exclusively with a <see cref="RMARequest"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class RMARequestQuery : RMARequestParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the RMARequestQuery class.
		/// </summary>
		public RMARequestQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the RMARequestQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public RMARequestQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the RMARequestQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public RMARequestQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion RMARequestQuery
		
	#region PaymentSettingFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PaymentSetting"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PaymentSettingFilters : PaymentSettingFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PaymentSettingFilters class.
		/// </summary>
		public PaymentSettingFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the PaymentSettingFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PaymentSettingFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PaymentSettingFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PaymentSettingFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PaymentSettingFilters
	
	#region PaymentSettingQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="PaymentSettingParameterBuilder"/> class
	/// that is used exclusively with a <see cref="PaymentSetting"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PaymentSettingQuery : PaymentSettingParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PaymentSettingQuery class.
		/// </summary>
		public PaymentSettingQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the PaymentSettingQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PaymentSettingQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PaymentSettingQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PaymentSettingQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PaymentSettingQuery
		
	#region OrderShipmentFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="OrderShipment"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class OrderShipmentFilters : OrderShipmentFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OrderShipmentFilters class.
		/// </summary>
		public OrderShipmentFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the OrderShipmentFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public OrderShipmentFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the OrderShipmentFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public OrderShipmentFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion OrderShipmentFilters
	
	#region OrderShipmentQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="OrderShipmentParameterBuilder"/> class
	/// that is used exclusively with a <see cref="OrderShipment"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class OrderShipmentQuery : OrderShipmentParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OrderShipmentQuery class.
		/// </summary>
		public OrderShipmentQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the OrderShipmentQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public OrderShipmentQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the OrderShipmentQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public OrderShipmentQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion OrderShipmentQuery
		
	#region ProductExtensionOptionFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ProductExtensionOption"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductExtensionOptionFilters : ProductExtensionOptionFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductExtensionOptionFilters class.
		/// </summary>
		public ProductExtensionOptionFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductExtensionOptionFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductExtensionOptionFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductExtensionOptionFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductExtensionOptionFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductExtensionOptionFilters
	
	#region ProductExtensionOptionQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ProductExtensionOptionParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ProductExtensionOption"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductExtensionOptionQuery : ProductExtensionOptionParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductExtensionOptionQuery class.
		/// </summary>
		public ProductExtensionOptionQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductExtensionOptionQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductExtensionOptionQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductExtensionOptionQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductExtensionOptionQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductExtensionOptionQuery
		
	#region OrderLineItemFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="OrderLineItem"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class OrderLineItemFilters : OrderLineItemFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OrderLineItemFilters class.
		/// </summary>
		public OrderLineItemFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the OrderLineItemFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public OrderLineItemFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the OrderLineItemFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public OrderLineItemFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion OrderLineItemFilters
	
	#region OrderLineItemQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="OrderLineItemParameterBuilder"/> class
	/// that is used exclusively with a <see cref="OrderLineItem"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class OrderLineItemQuery : OrderLineItemParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OrderLineItemQuery class.
		/// </summary>
		public OrderLineItemQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the OrderLineItemQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public OrderLineItemQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the OrderLineItemQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public OrderLineItemQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion OrderLineItemQuery
		
	#region ReasonForReturnFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ReasonForReturn"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ReasonForReturnFilters : ReasonForReturnFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ReasonForReturnFilters class.
		/// </summary>
		public ReasonForReturnFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ReasonForReturnFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ReasonForReturnFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ReasonForReturnFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ReasonForReturnFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ReasonForReturnFilters
	
	#region ReasonForReturnQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ReasonForReturnParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ReasonForReturn"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ReasonForReturnQuery : ReasonForReturnParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ReasonForReturnQuery class.
		/// </summary>
		public ReasonForReturnQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ReasonForReturnQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ReasonForReturnQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ReasonForReturnQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ReasonForReturnQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ReasonForReturnQuery
		
	#region SourceModificationAuditFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SourceModificationAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SourceModificationAuditFilters : SourceModificationAuditFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SourceModificationAuditFilters class.
		/// </summary>
		public SourceModificationAuditFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the SourceModificationAuditFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SourceModificationAuditFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SourceModificationAuditFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SourceModificationAuditFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SourceModificationAuditFilters
	
	#region SourceModificationAuditQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SourceModificationAuditParameterBuilder"/> class
	/// that is used exclusively with a <see cref="SourceModificationAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SourceModificationAuditQuery : SourceModificationAuditParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SourceModificationAuditQuery class.
		/// </summary>
		public SourceModificationAuditQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the SourceModificationAuditQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SourceModificationAuditQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SourceModificationAuditQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SourceModificationAuditQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SourceModificationAuditQuery
		
	#region UserDefinedViewFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UserDefinedView"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UserDefinedViewFilters : UserDefinedViewFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UserDefinedViewFilters class.
		/// </summary>
		public UserDefinedViewFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the UserDefinedViewFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UserDefinedViewFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UserDefinedViewFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UserDefinedViewFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UserDefinedViewFilters
	
	#region UserDefinedViewQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="UserDefinedViewParameterBuilder"/> class
	/// that is used exclusively with a <see cref="UserDefinedView"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UserDefinedViewQuery : UserDefinedViewParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UserDefinedViewQuery class.
		/// </summary>
		public UserDefinedViewQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the UserDefinedViewQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UserDefinedViewQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UserDefinedViewQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UserDefinedViewQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UserDefinedViewQuery
		
	#region RMARequestItemFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="RMARequestItem"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class RMARequestItemFilters : RMARequestItemFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the RMARequestItemFilters class.
		/// </summary>
		public RMARequestItemFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the RMARequestItemFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public RMARequestItemFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the RMARequestItemFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public RMARequestItemFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion RMARequestItemFilters
	
	#region RMARequestItemQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="RMARequestItemParameterBuilder"/> class
	/// that is used exclusively with a <see cref="RMARequestItem"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class RMARequestItemQuery : RMARequestItemParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the RMARequestItemQuery class.
		/// </summary>
		public RMARequestItemQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the RMARequestItemQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public RMARequestItemQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the RMARequestItemQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public RMARequestItemQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion RMARequestItemQuery
		
	#region WorkflowFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Workflow"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class WorkflowFilters : WorkflowFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the WorkflowFilters class.
		/// </summary>
		public WorkflowFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the WorkflowFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public WorkflowFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the WorkflowFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public WorkflowFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion WorkflowFilters
	
	#region WorkflowQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="WorkflowParameterBuilder"/> class
	/// that is used exclusively with a <see cref="Workflow"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class WorkflowQuery : WorkflowParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the WorkflowQuery class.
		/// </summary>
		public WorkflowQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the WorkflowQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public WorkflowQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the WorkflowQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public WorkflowQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion WorkflowQuery
		
	#region CustomerPricingFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CustomerPricing"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CustomerPricingFilters : CustomerPricingFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CustomerPricingFilters class.
		/// </summary>
		public CustomerPricingFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the CustomerPricingFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CustomerPricingFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CustomerPricingFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CustomerPricingFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CustomerPricingFilters
	
	#region CustomerPricingQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="CustomerPricingParameterBuilder"/> class
	/// that is used exclusively with a <see cref="CustomerPricing"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CustomerPricingQuery : CustomerPricingParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CustomerPricingQuery class.
		/// </summary>
		public CustomerPricingQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the CustomerPricingQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CustomerPricingQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CustomerPricingQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CustomerPricingQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CustomerPricingQuery
		
	#region SourceModificationTypeFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SourceModificationType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SourceModificationTypeFilters : SourceModificationTypeFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SourceModificationTypeFilters class.
		/// </summary>
		public SourceModificationTypeFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the SourceModificationTypeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SourceModificationTypeFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SourceModificationTypeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SourceModificationTypeFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SourceModificationTypeFilters
	
	#region SourceModificationTypeQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SourceModificationTypeParameterBuilder"/> class
	/// that is used exclusively with a <see cref="SourceModificationType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SourceModificationTypeQuery : SourceModificationTypeParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SourceModificationTypeQuery class.
		/// </summary>
		public SourceModificationTypeQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the SourceModificationTypeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SourceModificationTypeQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SourceModificationTypeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SourceModificationTypeQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SourceModificationTypeQuery
		
	#region ApplicationSettingFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ApplicationSetting"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ApplicationSettingFilters : ApplicationSettingFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ApplicationSettingFilters class.
		/// </summary>
		public ApplicationSettingFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ApplicationSettingFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ApplicationSettingFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ApplicationSettingFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ApplicationSettingFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ApplicationSettingFilters
	
	#region ApplicationSettingQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ApplicationSettingParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ApplicationSetting"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ApplicationSettingQuery : ApplicationSettingParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ApplicationSettingQuery class.
		/// </summary>
		public ApplicationSettingQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ApplicationSettingQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ApplicationSettingQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ApplicationSettingQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ApplicationSettingQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ApplicationSettingQuery
		
	#region PaymentTokenFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PaymentToken"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PaymentTokenFilters : PaymentTokenFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PaymentTokenFilters class.
		/// </summary>
		public PaymentTokenFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the PaymentTokenFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PaymentTokenFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PaymentTokenFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PaymentTokenFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PaymentTokenFilters
	
	#region PaymentTokenQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="PaymentTokenParameterBuilder"/> class
	/// that is used exclusively with a <see cref="PaymentToken"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PaymentTokenQuery : PaymentTokenParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PaymentTokenQuery class.
		/// </summary>
		public PaymentTokenQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the PaymentTokenQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PaymentTokenQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PaymentTokenQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PaymentTokenQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PaymentTokenQuery
		
	#region RequestStatusFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="RequestStatus"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class RequestStatusFilters : RequestStatusFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the RequestStatusFilters class.
		/// </summary>
		public RequestStatusFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the RequestStatusFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public RequestStatusFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the RequestStatusFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public RequestStatusFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion RequestStatusFilters
	
	#region RequestStatusQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="RequestStatusParameterBuilder"/> class
	/// that is used exclusively with a <see cref="RequestStatus"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class RequestStatusQuery : RequestStatusParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the RequestStatusQuery class.
		/// </summary>
		public RequestStatusQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the RequestStatusQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public RequestStatusQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the RequestStatusQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public RequestStatusQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion RequestStatusQuery
		
	#region UrlRedirectFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UrlRedirect"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UrlRedirectFilters : UrlRedirectFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UrlRedirectFilters class.
		/// </summary>
		public UrlRedirectFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the UrlRedirectFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UrlRedirectFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UrlRedirectFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UrlRedirectFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UrlRedirectFilters
	
	#region UrlRedirectQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="UrlRedirectParameterBuilder"/> class
	/// that is used exclusively with a <see cref="UrlRedirect"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UrlRedirectQuery : UrlRedirectParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UrlRedirectQuery class.
		/// </summary>
		public UrlRedirectQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the UrlRedirectQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UrlRedirectQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UrlRedirectQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UrlRedirectQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UrlRedirectQuery
		
	#region OrderDetailProductExtensionOptionFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="OrderDetailProductExtensionOption"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class OrderDetailProductExtensionOptionFilters : OrderDetailProductExtensionOptionFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OrderDetailProductExtensionOptionFilters class.
		/// </summary>
		public OrderDetailProductExtensionOptionFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the OrderDetailProductExtensionOptionFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public OrderDetailProductExtensionOptionFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the OrderDetailProductExtensionOptionFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public OrderDetailProductExtensionOptionFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion OrderDetailProductExtensionOptionFilters
	
	#region OrderDetailProductExtensionOptionQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="OrderDetailProductExtensionOptionParameterBuilder"/> class
	/// that is used exclusively with a <see cref="OrderDetailProductExtensionOption"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class OrderDetailProductExtensionOptionQuery : OrderDetailProductExtensionOptionParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OrderDetailProductExtensionOptionQuery class.
		/// </summary>
		public OrderDetailProductExtensionOptionQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the OrderDetailProductExtensionOptionQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public OrderDetailProductExtensionOptionQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the OrderDetailProductExtensionOptionQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public OrderDetailProductExtensionOptionQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion OrderDetailProductExtensionOptionQuery
		
	#region AccountFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Account"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AccountFilters : AccountFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AccountFilters class.
		/// </summary>
		public AccountFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the AccountFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AccountFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AccountFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AccountFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AccountFilters
	
	#region AccountQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="AccountParameterBuilder"/> class
	/// that is used exclusively with a <see cref="Account"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AccountQuery : AccountParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AccountQuery class.
		/// </summary>
		public AccountQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the AccountQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AccountQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AccountQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AccountQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AccountQuery
		
	#region ThemeFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Theme"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ThemeFilters : ThemeFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ThemeFilters class.
		/// </summary>
		public ThemeFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ThemeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ThemeFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ThemeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ThemeFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ThemeFilters
	
	#region ThemeQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ThemeParameterBuilder"/> class
	/// that is used exclusively with a <see cref="Theme"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ThemeQuery : ThemeParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ThemeQuery class.
		/// </summary>
		public ThemeQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ThemeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ThemeQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ThemeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ThemeQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ThemeQuery
		
	#region OrderLineItemRelationshipTypeFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="OrderLineItemRelationshipType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class OrderLineItemRelationshipTypeFilters : OrderLineItemRelationshipTypeFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OrderLineItemRelationshipTypeFilters class.
		/// </summary>
		public OrderLineItemRelationshipTypeFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the OrderLineItemRelationshipTypeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public OrderLineItemRelationshipTypeFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the OrderLineItemRelationshipTypeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public OrderLineItemRelationshipTypeFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion OrderLineItemRelationshipTypeFilters
	
	#region OrderLineItemRelationshipTypeQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="OrderLineItemRelationshipTypeParameterBuilder"/> class
	/// that is used exclusively with a <see cref="OrderLineItemRelationshipType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class OrderLineItemRelationshipTypeQuery : OrderLineItemRelationshipTypeParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OrderLineItemRelationshipTypeQuery class.
		/// </summary>
		public OrderLineItemRelationshipTypeQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the OrderLineItemRelationshipTypeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public OrderLineItemRelationshipTypeQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the OrderLineItemRelationshipTypeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public OrderLineItemRelationshipTypeQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion OrderLineItemRelationshipTypeQuery
		
	#region SourceModificationStatusFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SourceModificationStatus"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SourceModificationStatusFilters : SourceModificationStatusFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SourceModificationStatusFilters class.
		/// </summary>
		public SourceModificationStatusFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the SourceModificationStatusFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SourceModificationStatusFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SourceModificationStatusFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SourceModificationStatusFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SourceModificationStatusFilters
	
	#region SourceModificationStatusQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SourceModificationStatusParameterBuilder"/> class
	/// that is used exclusively with a <see cref="SourceModificationStatus"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SourceModificationStatusQuery : SourceModificationStatusParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SourceModificationStatusQuery class.
		/// </summary>
		public SourceModificationStatusQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the SourceModificationStatusQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SourceModificationStatusQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SourceModificationStatusQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SourceModificationStatusQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SourceModificationStatusQuery
		
	#region LuceneIndexStatusNamesFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="LuceneIndexStatusNames"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class LuceneIndexStatusNamesFilters : LuceneIndexStatusNamesFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the LuceneIndexStatusNamesFilters class.
		/// </summary>
		public LuceneIndexStatusNamesFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the LuceneIndexStatusNamesFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public LuceneIndexStatusNamesFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the LuceneIndexStatusNamesFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public LuceneIndexStatusNamesFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion LuceneIndexStatusNamesFilters
	
	#region LuceneIndexStatusNamesQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="LuceneIndexStatusNamesParameterBuilder"/> class
	/// that is used exclusively with a <see cref="LuceneIndexStatusNames"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class LuceneIndexStatusNamesQuery : LuceneIndexStatusNamesParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the LuceneIndexStatusNamesQuery class.
		/// </summary>
		public LuceneIndexStatusNamesQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the LuceneIndexStatusNamesQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public LuceneIndexStatusNamesQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the LuceneIndexStatusNamesQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public LuceneIndexStatusNamesQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion LuceneIndexStatusNamesQuery
		
	#region DigitalAssetFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="DigitalAsset"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class DigitalAssetFilters : DigitalAssetFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the DigitalAssetFilters class.
		/// </summary>
		public DigitalAssetFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the DigitalAssetFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public DigitalAssetFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the DigitalAssetFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public DigitalAssetFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion DigitalAssetFilters
	
	#region DigitalAssetQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="DigitalAssetParameterBuilder"/> class
	/// that is used exclusively with a <see cref="DigitalAsset"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class DigitalAssetQuery : DigitalAssetParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the DigitalAssetQuery class.
		/// </summary>
		public DigitalAssetQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the DigitalAssetQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public DigitalAssetQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the DigitalAssetQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public DigitalAssetQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion DigitalAssetQuery
		
	#region OrderFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Order"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class OrderFilters : OrderFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OrderFilters class.
		/// </summary>
		public OrderFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the OrderFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public OrderFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the OrderFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public OrderFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion OrderFilters
	
	#region OrderQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="OrderParameterBuilder"/> class
	/// that is used exclusively with a <see cref="Order"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class OrderQuery : OrderParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OrderQuery class.
		/// </summary>
		public OrderQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the OrderQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public OrderQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the OrderQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public OrderQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion OrderQuery
		
	#region SourceTypeFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SourceType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SourceTypeFilters : SourceTypeFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SourceTypeFilters class.
		/// </summary>
		public SourceTypeFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the SourceTypeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SourceTypeFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SourceTypeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SourceTypeFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SourceTypeFilters
	
	#region SourceTypeQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SourceTypeParameterBuilder"/> class
	/// that is used exclusively with a <see cref="SourceType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SourceTypeQuery : SourceTypeParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SourceTypeQuery class.
		/// </summary>
		public SourceTypeQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the SourceTypeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SourceTypeQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SourceTypeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SourceTypeQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SourceTypeQuery
		
	#region PasswordLogFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PasswordLog"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PasswordLogFilters : PasswordLogFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PasswordLogFilters class.
		/// </summary>
		public PasswordLogFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the PasswordLogFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PasswordLogFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PasswordLogFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PasswordLogFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PasswordLogFilters
	
	#region PasswordLogQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="PasswordLogParameterBuilder"/> class
	/// that is used exclusively with a <see cref="PasswordLog"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PasswordLogQuery : PasswordLogParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PasswordLogQuery class.
		/// </summary>
		public PasswordLogQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the PasswordLogQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PasswordLogQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PasswordLogQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PasswordLogQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PasswordLogQuery
		
	#region LuceneIndexMonitorFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="LuceneIndexMonitor"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class LuceneIndexMonitorFilters : LuceneIndexMonitorFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the LuceneIndexMonitorFilters class.
		/// </summary>
		public LuceneIndexMonitorFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the LuceneIndexMonitorFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public LuceneIndexMonitorFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the LuceneIndexMonitorFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public LuceneIndexMonitorFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion LuceneIndexMonitorFilters
	
	#region LuceneIndexMonitorQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="LuceneIndexMonitorParameterBuilder"/> class
	/// that is used exclusively with a <see cref="LuceneIndexMonitor"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class LuceneIndexMonitorQuery : LuceneIndexMonitorParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the LuceneIndexMonitorQuery class.
		/// </summary>
		public LuceneIndexMonitorQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the LuceneIndexMonitorQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public LuceneIndexMonitorQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the LuceneIndexMonitorQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public LuceneIndexMonitorQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion LuceneIndexMonitorQuery
		
	#region MultifrontFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Multifront"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class MultifrontFilters : MultifrontFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MultifrontFilters class.
		/// </summary>
		public MultifrontFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the MultifrontFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public MultifrontFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the MultifrontFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public MultifrontFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion MultifrontFilters
	
	#region MultifrontQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="MultifrontParameterBuilder"/> class
	/// that is used exclusively with a <see cref="Multifront"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class MultifrontQuery : MultifrontParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MultifrontQuery class.
		/// </summary>
		public MultifrontQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the MultifrontQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public MultifrontQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the MultifrontQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public MultifrontQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion MultifrontQuery
		
	#region GiftCardFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="GiftCard"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class GiftCardFilters : GiftCardFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the GiftCardFilters class.
		/// </summary>
		public GiftCardFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the GiftCardFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public GiftCardFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the GiftCardFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public GiftCardFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion GiftCardFilters
	
	#region GiftCardQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="GiftCardParameterBuilder"/> class
	/// that is used exclusively with a <see cref="GiftCard"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class GiftCardQuery : GiftCardParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the GiftCardQuery class.
		/// </summary>
		public GiftCardQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the GiftCardQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public GiftCardQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the GiftCardQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public GiftCardQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion GiftCardQuery
		
	#region GiftCardHistoryFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="GiftCardHistory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class GiftCardHistoryFilters : GiftCardHistoryFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the GiftCardHistoryFilters class.
		/// </summary>
		public GiftCardHistoryFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the GiftCardHistoryFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public GiftCardHistoryFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the GiftCardHistoryFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public GiftCardHistoryFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion GiftCardHistoryFilters
	
	#region GiftCardHistoryQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="GiftCardHistoryParameterBuilder"/> class
	/// that is used exclusively with a <see cref="GiftCardHistory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class GiftCardHistoryQuery : GiftCardHistoryParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the GiftCardHistoryQuery class.
		/// </summary>
		public GiftCardHistoryQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the GiftCardHistoryQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public GiftCardHistoryQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the GiftCardHistoryQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public GiftCardHistoryQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion GiftCardHistoryQuery
		
	#region MessageTypeFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="MessageType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class MessageTypeFilters : MessageTypeFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MessageTypeFilters class.
		/// </summary>
		public MessageTypeFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the MessageTypeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public MessageTypeFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the MessageTypeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public MessageTypeFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion MessageTypeFilters
	
	#region MessageTypeQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="MessageTypeParameterBuilder"/> class
	/// that is used exclusively with a <see cref="MessageType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class MessageTypeQuery : MessageTypeParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MessageTypeQuery class.
		/// </summary>
		public MessageTypeQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the MessageTypeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public MessageTypeQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the MessageTypeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public MessageTypeQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion MessageTypeQuery
		
	#region MessageConfigFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="MessageConfig"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class MessageConfigFilters : MessageConfigFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MessageConfigFilters class.
		/// </summary>
		public MessageConfigFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the MessageConfigFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public MessageConfigFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the MessageConfigFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public MessageConfigFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion MessageConfigFilters
	
	#region MessageConfigQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="MessageConfigParameterBuilder"/> class
	/// that is used exclusively with a <see cref="MessageConfig"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class MessageConfigQuery : MessageConfigParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MessageConfigQuery class.
		/// </summary>
		public MessageConfigQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the MessageConfigQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public MessageConfigQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the MessageConfigQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public MessageConfigQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion MessageConfigQuery
		
	#region ProductTypeFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ProductType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductTypeFilters : ProductTypeFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductTypeFilters class.
		/// </summary>
		public ProductTypeFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductTypeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductTypeFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductTypeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductTypeFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductTypeFilters
	
	#region ProductTypeQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ProductTypeParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ProductType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductTypeQuery : ProductTypeParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductTypeQuery class.
		/// </summary>
		public ProductTypeQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductTypeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductTypeQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductTypeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductTypeQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductTypeQuery
		
	#region CatalogFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Catalog"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CatalogFilters : CatalogFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CatalogFilters class.
		/// </summary>
		public CatalogFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the CatalogFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CatalogFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CatalogFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CatalogFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CatalogFilters
	
	#region CatalogQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="CatalogParameterBuilder"/> class
	/// that is used exclusively with a <see cref="Catalog"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CatalogQuery : CatalogParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CatalogQuery class.
		/// </summary>
		public CatalogQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the CatalogQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CatalogQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CatalogQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CatalogQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CatalogQuery
		
	#region ManufacturerFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Manufacturer"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ManufacturerFilters : ManufacturerFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ManufacturerFilters class.
		/// </summary>
		public ManufacturerFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ManufacturerFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ManufacturerFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ManufacturerFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ManufacturerFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ManufacturerFilters
	
	#region ManufacturerQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ManufacturerParameterBuilder"/> class
	/// that is used exclusively with a <see cref="Manufacturer"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ManufacturerQuery : ManufacturerParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ManufacturerQuery class.
		/// </summary>
		public ManufacturerQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ManufacturerQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ManufacturerQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ManufacturerQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ManufacturerQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ManufacturerQuery
		
	#region SupplierFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Supplier"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SupplierFilters : SupplierFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SupplierFilters class.
		/// </summary>
		public SupplierFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the SupplierFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SupplierFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SupplierFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SupplierFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SupplierFilters
	
	#region SupplierQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SupplierParameterBuilder"/> class
	/// that is used exclusively with a <see cref="Supplier"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SupplierQuery : SupplierParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SupplierQuery class.
		/// </summary>
		public SupplierQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the SupplierQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SupplierQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SupplierQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SupplierQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SupplierQuery
		
	#region ProductFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Product"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductFilters : ProductFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductFilters class.
		/// </summary>
		public ProductFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductFilters
	
	#region ProductQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ProductParameterBuilder"/> class
	/// that is used exclusively with a <see cref="Product"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductQuery : ProductParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductQuery class.
		/// </summary>
		public ProductQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductQuery
		
	#region PaymentStatusFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PaymentStatus"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PaymentStatusFilters : PaymentStatusFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PaymentStatusFilters class.
		/// </summary>
		public PaymentStatusFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the PaymentStatusFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PaymentStatusFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PaymentStatusFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PaymentStatusFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PaymentStatusFilters
	
	#region PaymentStatusQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="PaymentStatusParameterBuilder"/> class
	/// that is used exclusively with a <see cref="PaymentStatus"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PaymentStatusQuery : PaymentStatusParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PaymentStatusQuery class.
		/// </summary>
		public PaymentStatusQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the PaymentStatusQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PaymentStatusQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PaymentStatusQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PaymentStatusQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PaymentStatusQuery
		
	#region SKUFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SKU"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SKUFilters : SKUFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SKUFilters class.
		/// </summary>
		public SKUFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the SKUFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SKUFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SKUFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SKUFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SKUFilters
	
	#region SKUQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SKUParameterBuilder"/> class
	/// that is used exclusively with a <see cref="SKU"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SKUQuery : SKUParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SKUQuery class.
		/// </summary>
		public SKUQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the SKUQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SKUQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SKUQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SKUQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SKUQuery
		
	#region CookieMappingFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CookieMapping"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CookieMappingFilters : CookieMappingFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CookieMappingFilters class.
		/// </summary>
		public CookieMappingFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the CookieMappingFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CookieMappingFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CookieMappingFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CookieMappingFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CookieMappingFilters
	
	#region CookieMappingQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="CookieMappingParameterBuilder"/> class
	/// that is used exclusively with a <see cref="CookieMapping"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CookieMappingQuery : CookieMappingParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CookieMappingQuery class.
		/// </summary>
		public CookieMappingQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the CookieMappingQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CookieMappingQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CookieMappingQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CookieMappingQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CookieMappingQuery
		
	#region ReferralCommissionFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ReferralCommission"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ReferralCommissionFilters : ReferralCommissionFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ReferralCommissionFilters class.
		/// </summary>
		public ReferralCommissionFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ReferralCommissionFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ReferralCommissionFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ReferralCommissionFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ReferralCommissionFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ReferralCommissionFilters
	
	#region ReferralCommissionQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ReferralCommissionParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ReferralCommission"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ReferralCommissionQuery : ReferralCommissionParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ReferralCommissionQuery class.
		/// </summary>
		public ReferralCommissionQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ReferralCommissionQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ReferralCommissionQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ReferralCommissionQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ReferralCommissionQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ReferralCommissionQuery
		
	#region MasterPageFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="MasterPage"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class MasterPageFilters : MasterPageFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MasterPageFilters class.
		/// </summary>
		public MasterPageFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the MasterPageFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public MasterPageFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the MasterPageFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public MasterPageFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion MasterPageFilters
	
	#region MasterPageQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="MasterPageParameterBuilder"/> class
	/// that is used exclusively with a <see cref="MasterPage"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class MasterPageQuery : MasterPageParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MasterPageQuery class.
		/// </summary>
		public MasterPageQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the MasterPageQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public MasterPageQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the MasterPageQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public MasterPageQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion MasterPageQuery
		
	#region CountryFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Country"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CountryFilters : CountryFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CountryFilters class.
		/// </summary>
		public CountryFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the CountryFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CountryFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CountryFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CountryFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CountryFilters
	
	#region CountryQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="CountryParameterBuilder"/> class
	/// that is used exclusively with a <see cref="Country"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CountryQuery : CountryParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CountryQuery class.
		/// </summary>
		public CountryQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the CountryQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CountryQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CountryQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CountryQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CountryQuery
		
	#region SupplierTypeFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SupplierType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SupplierTypeFilters : SupplierTypeFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SupplierTypeFilters class.
		/// </summary>
		public SupplierTypeFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the SupplierTypeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SupplierTypeFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SupplierTypeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SupplierTypeFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SupplierTypeFilters
	
	#region SupplierTypeQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SupplierTypeParameterBuilder"/> class
	/// that is used exclusively with a <see cref="SupplierType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SupplierTypeQuery : SupplierTypeParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SupplierTypeQuery class.
		/// </summary>
		public SupplierTypeQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the SupplierTypeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SupplierTypeQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SupplierTypeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SupplierTypeQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SupplierTypeQuery
		
	#region CaseStatusFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CaseStatus"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CaseStatusFilters : CaseStatusFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CaseStatusFilters class.
		/// </summary>
		public CaseStatusFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the CaseStatusFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CaseStatusFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CaseStatusFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CaseStatusFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CaseStatusFilters
	
	#region CaseStatusQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="CaseStatusParameterBuilder"/> class
	/// that is used exclusively with a <see cref="CaseStatus"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CaseStatusQuery : CaseStatusParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CaseStatusQuery class.
		/// </summary>
		public CaseStatusQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the CaseStatusQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CaseStatusQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CaseStatusQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CaseStatusQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CaseStatusQuery
		
	#region ShippingServiceCodeFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ShippingServiceCode"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ShippingServiceCodeFilters : ShippingServiceCodeFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ShippingServiceCodeFilters class.
		/// </summary>
		public ShippingServiceCodeFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ShippingServiceCodeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ShippingServiceCodeFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ShippingServiceCodeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ShippingServiceCodeFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ShippingServiceCodeFilters
	
	#region ShippingServiceCodeQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ShippingServiceCodeParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ShippingServiceCode"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ShippingServiceCodeQuery : ShippingServiceCodeParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ShippingServiceCodeQuery class.
		/// </summary>
		public ShippingServiceCodeQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ShippingServiceCodeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ShippingServiceCodeQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ShippingServiceCodeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ShippingServiceCodeQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ShippingServiceCodeQuery
		
	#region PaymentTokenAuthorizeFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PaymentTokenAuthorize"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PaymentTokenAuthorizeFilters : PaymentTokenAuthorizeFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PaymentTokenAuthorizeFilters class.
		/// </summary>
		public PaymentTokenAuthorizeFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the PaymentTokenAuthorizeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PaymentTokenAuthorizeFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PaymentTokenAuthorizeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PaymentTokenAuthorizeFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PaymentTokenAuthorizeFilters
	
	#region PaymentTokenAuthorizeQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="PaymentTokenAuthorizeParameterBuilder"/> class
	/// that is used exclusively with a <see cref="PaymentTokenAuthorize"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PaymentTokenAuthorizeQuery : PaymentTokenAuthorizeParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PaymentTokenAuthorizeQuery class.
		/// </summary>
		public PaymentTokenAuthorizeQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the PaymentTokenAuthorizeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PaymentTokenAuthorizeQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PaymentTokenAuthorizeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PaymentTokenAuthorizeQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PaymentTokenAuthorizeQuery
		
	#region RMAConfigurationFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="RMAConfiguration"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class RMAConfigurationFilters : RMAConfigurationFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the RMAConfigurationFilters class.
		/// </summary>
		public RMAConfigurationFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the RMAConfigurationFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public RMAConfigurationFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the RMAConfigurationFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public RMAConfigurationFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion RMAConfigurationFilters
	
	#region RMAConfigurationQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="RMAConfigurationParameterBuilder"/> class
	/// that is used exclusively with a <see cref="RMAConfiguration"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class RMAConfigurationQuery : RMAConfigurationParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the RMAConfigurationQuery class.
		/// </summary>
		public RMAConfigurationQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the RMAConfigurationQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public RMAConfigurationQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the RMAConfigurationQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public RMAConfigurationQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion RMAConfigurationQuery
		
	#region LuceneServerConfigurationStatusFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="LuceneServerConfigurationStatus"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class LuceneServerConfigurationStatusFilters : LuceneServerConfigurationStatusFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the LuceneServerConfigurationStatusFilters class.
		/// </summary>
		public LuceneServerConfigurationStatusFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the LuceneServerConfigurationStatusFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public LuceneServerConfigurationStatusFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the LuceneServerConfigurationStatusFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public LuceneServerConfigurationStatusFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion LuceneServerConfigurationStatusFilters
	
	#region LuceneServerConfigurationStatusQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="LuceneServerConfigurationStatusParameterBuilder"/> class
	/// that is used exclusively with a <see cref="LuceneServerConfigurationStatus"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class LuceneServerConfigurationStatusQuery : LuceneServerConfigurationStatusParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the LuceneServerConfigurationStatusQuery class.
		/// </summary>
		public LuceneServerConfigurationStatusQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the LuceneServerConfigurationStatusQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public LuceneServerConfigurationStatusQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the LuceneServerConfigurationStatusQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public LuceneServerConfigurationStatusQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion LuceneServerConfigurationStatusQuery
		
	#region PortalWorkflowFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PortalWorkflow"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PortalWorkflowFilters : PortalWorkflowFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PortalWorkflowFilters class.
		/// </summary>
		public PortalWorkflowFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the PortalWorkflowFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PortalWorkflowFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PortalWorkflowFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PortalWorkflowFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PortalWorkflowFilters
	
	#region PortalWorkflowQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="PortalWorkflowParameterBuilder"/> class
	/// that is used exclusively with a <see cref="PortalWorkflow"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PortalWorkflowQuery : PortalWorkflowParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PortalWorkflowQuery class.
		/// </summary>
		public PortalWorkflowQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the PortalWorkflowQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PortalWorkflowQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PortalWorkflowQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PortalWorkflowQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PortalWorkflowQuery
		
	#region AccountPaymentFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AccountPayment"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AccountPaymentFilters : AccountPaymentFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AccountPaymentFilters class.
		/// </summary>
		public AccountPaymentFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the AccountPaymentFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AccountPaymentFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AccountPaymentFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AccountPaymentFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AccountPaymentFilters
	
	#region AccountPaymentQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="AccountPaymentParameterBuilder"/> class
	/// that is used exclusively with a <see cref="AccountPayment"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AccountPaymentQuery : AccountPaymentParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AccountPaymentQuery class.
		/// </summary>
		public AccountPaymentQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the AccountPaymentQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AccountPaymentQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AccountPaymentQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AccountPaymentQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AccountPaymentQuery
		
	#region ProductImageTypeFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ProductImageType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductImageTypeFilters : ProductImageTypeFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductImageTypeFilters class.
		/// </summary>
		public ProductImageTypeFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductImageTypeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductImageTypeFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductImageTypeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductImageTypeFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductImageTypeFilters
	
	#region ProductImageTypeQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ProductImageTypeParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ProductImageType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductImageTypeQuery : ProductImageTypeParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductImageTypeQuery class.
		/// </summary>
		public ProductImageTypeQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductImageTypeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductImageTypeQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductImageTypeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductImageTypeQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductImageTypeQuery
		
	#region ProductExtensionFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ProductExtension"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductExtensionFilters : ProductExtensionFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductExtensionFilters class.
		/// </summary>
		public ProductExtensionFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductExtensionFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductExtensionFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductExtensionFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductExtensionFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductExtensionFilters
	
	#region ProductExtensionQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ProductExtensionParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ProductExtension"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductExtensionQuery : ProductExtensionParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductExtensionQuery class.
		/// </summary>
		public ProductExtensionQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductExtensionQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductExtensionQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductExtensionQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductExtensionQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductExtensionQuery
		
	#region SavedCartLineItemProductExtensionOptionFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SavedCartLineItemProductExtensionOption"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SavedCartLineItemProductExtensionOptionFilters : SavedCartLineItemProductExtensionOptionFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SavedCartLineItemProductExtensionOptionFilters class.
		/// </summary>
		public SavedCartLineItemProductExtensionOptionFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the SavedCartLineItemProductExtensionOptionFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SavedCartLineItemProductExtensionOptionFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SavedCartLineItemProductExtensionOptionFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SavedCartLineItemProductExtensionOptionFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SavedCartLineItemProductExtensionOptionFilters
	
	#region SavedCartLineItemProductExtensionOptionQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SavedCartLineItemProductExtensionOptionParameterBuilder"/> class
	/// that is used exclusively with a <see cref="SavedCartLineItemProductExtensionOption"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SavedCartLineItemProductExtensionOptionQuery : SavedCartLineItemProductExtensionOptionParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SavedCartLineItemProductExtensionOptionQuery class.
		/// </summary>
		public SavedCartLineItemProductExtensionOptionQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the SavedCartLineItemProductExtensionOptionQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SavedCartLineItemProductExtensionOptionQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SavedCartLineItemProductExtensionOptionQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SavedCartLineItemProductExtensionOptionQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SavedCartLineItemProductExtensionOptionQuery
		
	#region PaymentTypeFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PaymentType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PaymentTypeFilters : PaymentTypeFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PaymentTypeFilters class.
		/// </summary>
		public PaymentTypeFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the PaymentTypeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PaymentTypeFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PaymentTypeFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PaymentTypeFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PaymentTypeFilters
	
	#region PaymentTypeQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="PaymentTypeParameterBuilder"/> class
	/// that is used exclusively with a <see cref="PaymentType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PaymentTypeQuery : PaymentTypeParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PaymentTypeQuery class.
		/// </summary>
		public PaymentTypeQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the PaymentTypeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PaymentTypeQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PaymentTypeQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PaymentTypeQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PaymentTypeQuery
		
	#region ParentChildProductFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ParentChildProduct"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ParentChildProductFilters : ParentChildProductFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ParentChildProductFilters class.
		/// </summary>
		public ParentChildProductFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the ParentChildProductFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ParentChildProductFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ParentChildProductFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ParentChildProductFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ParentChildProductFilters
	
	#region ParentChildProductQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParentChildProductParameterBuilder"/> class
	/// that is used exclusively with a <see cref="ParentChildProduct"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ParentChildProductQuery : ParentChildProductParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ParentChildProductQuery class.
		/// </summary>
		public ParentChildProductQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the ParentChildProductQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ParentChildProductQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ParentChildProductQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ParentChildProductQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ParentChildProductQuery
		
	#region LuceneIndexServerStatusFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="LuceneIndexServerStatus"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class LuceneIndexServerStatusFilters : LuceneIndexServerStatusFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the LuceneIndexServerStatusFilters class.
		/// </summary>
		public LuceneIndexServerStatusFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the LuceneIndexServerStatusFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public LuceneIndexServerStatusFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the LuceneIndexServerStatusFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public LuceneIndexServerStatusFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion LuceneIndexServerStatusFilters
	
	#region LuceneIndexServerStatusQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="LuceneIndexServerStatusParameterBuilder"/> class
	/// that is used exclusively with a <see cref="LuceneIndexServerStatus"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class LuceneIndexServerStatusQuery : LuceneIndexServerStatusParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the LuceneIndexServerStatusQuery class.
		/// </summary>
		public LuceneIndexServerStatusQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the LuceneIndexServerStatusQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public LuceneIndexServerStatusQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the LuceneIndexServerStatusQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public LuceneIndexServerStatusQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion LuceneIndexServerStatusQuery
		
	#region VwZnodeCategoriesCatalogsFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="VwZnodeCategoriesCatalogs"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class VwZnodeCategoriesCatalogsFilters : VwZnodeCategoriesCatalogsFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the VwZnodeCategoriesCatalogsFilters class.
		/// </summary>
		public VwZnodeCategoriesCatalogsFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the VwZnodeCategoriesCatalogsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public VwZnodeCategoriesCatalogsFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the VwZnodeCategoriesCatalogsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public VwZnodeCategoriesCatalogsFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion VwZnodeCategoriesCatalogsFilters
	
	#region VwZnodeCategoriesCatalogsQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="VwZnodeCategoriesCatalogsParameterBuilder"/> class
	/// that is used exclusively with a <see cref="VwZnodeCategoriesCatalogs"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class VwZnodeCategoriesCatalogsQuery : VwZnodeCategoriesCatalogsParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the VwZnodeCategoriesCatalogsQuery class.
		/// </summary>
		public VwZnodeCategoriesCatalogsQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the VwZnodeCategoriesCatalogsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public VwZnodeCategoriesCatalogsQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the VwZnodeCategoriesCatalogsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public VwZnodeCategoriesCatalogsQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion VwZnodeCategoriesCatalogsQuery
		
	#region VwZnodeCategoriesPortalsFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="VwZnodeCategoriesPortals"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class VwZnodeCategoriesPortalsFilters : VwZnodeCategoriesPortalsFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the VwZnodeCategoriesPortalsFilters class.
		/// </summary>
		public VwZnodeCategoriesPortalsFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the VwZnodeCategoriesPortalsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public VwZnodeCategoriesPortalsFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the VwZnodeCategoriesPortalsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public VwZnodeCategoriesPortalsFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion VwZnodeCategoriesPortalsFilters
	
	#region VwZnodeCategoriesPortalsQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="VwZnodeCategoriesPortalsParameterBuilder"/> class
	/// that is used exclusively with a <see cref="VwZnodeCategoriesPortals"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class VwZnodeCategoriesPortalsQuery : VwZnodeCategoriesPortalsParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the VwZnodeCategoriesPortalsQuery class.
		/// </summary>
		public VwZnodeCategoriesPortalsQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the VwZnodeCategoriesPortalsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public VwZnodeCategoriesPortalsQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the VwZnodeCategoriesPortalsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public VwZnodeCategoriesPortalsQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion VwZnodeCategoriesPortalsQuery
		
	#region VwZnodeProductsCatalogsFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="VwZnodeProductsCatalogs"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class VwZnodeProductsCatalogsFilters : VwZnodeProductsCatalogsFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the VwZnodeProductsCatalogsFilters class.
		/// </summary>
		public VwZnodeProductsCatalogsFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the VwZnodeProductsCatalogsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public VwZnodeProductsCatalogsFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the VwZnodeProductsCatalogsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public VwZnodeProductsCatalogsFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion VwZnodeProductsCatalogsFilters
	
	#region VwZnodeProductsCatalogsQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="VwZnodeProductsCatalogsParameterBuilder"/> class
	/// that is used exclusively with a <see cref="VwZnodeProductsCatalogs"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class VwZnodeProductsCatalogsQuery : VwZnodeProductsCatalogsParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the VwZnodeProductsCatalogsQuery class.
		/// </summary>
		public VwZnodeProductsCatalogsQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the VwZnodeProductsCatalogsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public VwZnodeProductsCatalogsQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the VwZnodeProductsCatalogsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public VwZnodeProductsCatalogsQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion VwZnodeProductsCatalogsQuery
		
	#region VwZnodeProductsCategoriesFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="VwZnodeProductsCategories"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class VwZnodeProductsCategoriesFilters : VwZnodeProductsCategoriesFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the VwZnodeProductsCategoriesFilters class.
		/// </summary>
		public VwZnodeProductsCategoriesFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the VwZnodeProductsCategoriesFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public VwZnodeProductsCategoriesFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the VwZnodeProductsCategoriesFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public VwZnodeProductsCategoriesFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion VwZnodeProductsCategoriesFilters
	
	#region VwZnodeProductsCategoriesQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="VwZnodeProductsCategoriesParameterBuilder"/> class
	/// that is used exclusively with a <see cref="VwZnodeProductsCategories"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class VwZnodeProductsCategoriesQuery : VwZnodeProductsCategoriesParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the VwZnodeProductsCategoriesQuery class.
		/// </summary>
		public VwZnodeProductsCategoriesQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the VwZnodeProductsCategoriesQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public VwZnodeProductsCategoriesQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the VwZnodeProductsCategoriesQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public VwZnodeProductsCategoriesQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion VwZnodeProductsCategoriesQuery
		
	#region VwZnodeProductsCategoriesPromotionsFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="VwZnodeProductsCategoriesPromotions"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class VwZnodeProductsCategoriesPromotionsFilters : VwZnodeProductsCategoriesPromotionsFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the VwZnodeProductsCategoriesPromotionsFilters class.
		/// </summary>
		public VwZnodeProductsCategoriesPromotionsFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the VwZnodeProductsCategoriesPromotionsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public VwZnodeProductsCategoriesPromotionsFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the VwZnodeProductsCategoriesPromotionsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public VwZnodeProductsCategoriesPromotionsFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion VwZnodeProductsCategoriesPromotionsFilters
	
	#region VwZnodeProductsCategoriesPromotionsQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="VwZnodeProductsCategoriesPromotionsParameterBuilder"/> class
	/// that is used exclusively with a <see cref="VwZnodeProductsCategoriesPromotions"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class VwZnodeProductsCategoriesPromotionsQuery : VwZnodeProductsCategoriesPromotionsParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the VwZnodeProductsCategoriesPromotionsQuery class.
		/// </summary>
		public VwZnodeProductsCategoriesPromotionsQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the VwZnodeProductsCategoriesPromotionsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public VwZnodeProductsCategoriesPromotionsQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the VwZnodeProductsCategoriesPromotionsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public VwZnodeProductsCategoriesPromotionsQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion VwZnodeProductsCategoriesPromotionsQuery
		
	#region VwZnodeProductsPortalsFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="VwZnodeProductsPortals"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class VwZnodeProductsPortalsFilters : VwZnodeProductsPortalsFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the VwZnodeProductsPortalsFilters class.
		/// </summary>
		public VwZnodeProductsPortalsFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the VwZnodeProductsPortalsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public VwZnodeProductsPortalsFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the VwZnodeProductsPortalsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public VwZnodeProductsPortalsFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion VwZnodeProductsPortalsFilters
	
	#region VwZnodeProductsPortalsQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="VwZnodeProductsPortalsParameterBuilder"/> class
	/// that is used exclusively with a <see cref="VwZnodeProductsPortals"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class VwZnodeProductsPortalsQuery : VwZnodeProductsPortalsParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the VwZnodeProductsPortalsQuery class.
		/// </summary>
		public VwZnodeProductsPortalsQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the VwZnodeProductsPortalsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public VwZnodeProductsPortalsQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the VwZnodeProductsPortalsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public VwZnodeProductsPortalsQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion VwZnodeProductsPortalsQuery
		
	#region VwZnodeProductsPromotionsFilters
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="VwZnodeProductsPromotions"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class VwZnodeProductsPromotionsFilters : VwZnodeProductsPromotionsFilterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the VwZnodeProductsPromotionsFilters class.
		/// </summary>
		public VwZnodeProductsPromotionsFilters() : base() { }

		/// <summary>
		/// Initializes a new instance of the VwZnodeProductsPromotionsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public VwZnodeProductsPromotionsFilters(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the VwZnodeProductsPromotionsFilters class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public VwZnodeProductsPromotionsFilters(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion VwZnodeProductsPromotionsFilters
	
	#region VwZnodeProductsPromotionsQuery
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="VwZnodeProductsPromotionsParameterBuilder"/> class
	/// that is used exclusively with a <see cref="VwZnodeProductsPromotions"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class VwZnodeProductsPromotionsQuery : VwZnodeProductsPromotionsParameterBuilder
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the VwZnodeProductsPromotionsQuery class.
		/// </summary>
		public VwZnodeProductsPromotionsQuery() : base() { }

		/// <summary>
		/// Initializes a new instance of the VwZnodeProductsPromotionsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public VwZnodeProductsPromotionsQuery(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the VwZnodeProductsPromotionsQuery class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public VwZnodeProductsPromotionsQuery(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion VwZnodeProductsPromotionsQuery
	#endregion

	
}
