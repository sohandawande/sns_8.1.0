﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="LuceneGlobalProductBoostProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class LuceneGlobalProductBoostProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBoost, ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBoostKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBoostKey key)
		{
			return Delete(transactionManager, key.LuceneGlobalProductBoostID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_luceneGlobalProductBoostID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _luceneGlobalProductBoostID)
		{
			return Delete(null, _luceneGlobalProductBoostID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_luceneGlobalProductBoostID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _luceneGlobalProductBoostID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBoost Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBoostKey key, int start, int pageLength)
		{
			return GetByLuceneGlobalProductBoostID(transactionManager, key.LuceneGlobalProductBoostID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeLuceneGlobalProductBoost index.
		/// </summary>
		/// <param name="_productID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBoost"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBoost GetByProductID(System.Int32 _productID)
		{
			int count = -1;
			return GetByProductID(null,_productID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeLuceneGlobalProductBoost index.
		/// </summary>
		/// <param name="_productID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBoost"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBoost GetByProductID(System.Int32 _productID, int start, int pageLength)
		{
			int count = -1;
			return GetByProductID(null, _productID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeLuceneGlobalProductBoost index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBoost"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBoost GetByProductID(TransactionManager transactionManager, System.Int32 _productID)
		{
			int count = -1;
			return GetByProductID(transactionManager, _productID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeLuceneGlobalProductBoost index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBoost"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBoost GetByProductID(TransactionManager transactionManager, System.Int32 _productID, int start, int pageLength)
		{
			int count = -1;
			return GetByProductID(transactionManager, _productID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeLuceneGlobalProductBoost index.
		/// </summary>
		/// <param name="_productID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBoost"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBoost GetByProductID(System.Int32 _productID, int start, int pageLength, out int count)
		{
			return GetByProductID(null, _productID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeLuceneGlobalProductBoost index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBoost"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBoost GetByProductID(TransactionManager transactionManager, System.Int32 _productID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodeLuceneGlobalProductBoost index.
		/// </summary>
		/// <param name="_luceneGlobalProductBoostID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBoost"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBoost GetByLuceneGlobalProductBoostID(System.Int32 _luceneGlobalProductBoostID)
		{
			int count = -1;
			return GetByLuceneGlobalProductBoostID(null,_luceneGlobalProductBoostID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeLuceneGlobalProductBoost index.
		/// </summary>
		/// <param name="_luceneGlobalProductBoostID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBoost"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBoost GetByLuceneGlobalProductBoostID(System.Int32 _luceneGlobalProductBoostID, int start, int pageLength)
		{
			int count = -1;
			return GetByLuceneGlobalProductBoostID(null, _luceneGlobalProductBoostID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeLuceneGlobalProductBoost index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_luceneGlobalProductBoostID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBoost"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBoost GetByLuceneGlobalProductBoostID(TransactionManager transactionManager, System.Int32 _luceneGlobalProductBoostID)
		{
			int count = -1;
			return GetByLuceneGlobalProductBoostID(transactionManager, _luceneGlobalProductBoostID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeLuceneGlobalProductBoost index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_luceneGlobalProductBoostID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBoost"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBoost GetByLuceneGlobalProductBoostID(TransactionManager transactionManager, System.Int32 _luceneGlobalProductBoostID, int start, int pageLength)
		{
			int count = -1;
			return GetByLuceneGlobalProductBoostID(transactionManager, _luceneGlobalProductBoostID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeLuceneGlobalProductBoost index.
		/// </summary>
		/// <param name="_luceneGlobalProductBoostID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBoost"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBoost GetByLuceneGlobalProductBoostID(System.Int32 _luceneGlobalProductBoostID, int start, int pageLength, out int count)
		{
			return GetByLuceneGlobalProductBoostID(null, _luceneGlobalProductBoostID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeLuceneGlobalProductBoost index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_luceneGlobalProductBoostID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBoost"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBoost GetByLuceneGlobalProductBoostID(TransactionManager transactionManager, System.Int32 _luceneGlobalProductBoostID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;LuceneGlobalProductBoost&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;LuceneGlobalProductBoost&gt;"/></returns>
		public static TList<LuceneGlobalProductBoost> Fill(IDataReader reader, TList<LuceneGlobalProductBoost> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBoost c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("LuceneGlobalProductBoost")
					.Append("|").Append((System.Int32)reader[((int)LuceneGlobalProductBoostColumn.LuceneGlobalProductBoostID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<LuceneGlobalProductBoost>(
					key.ToString(), // EntityTrackingKey
					"LuceneGlobalProductBoost",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBoost();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.LuceneGlobalProductBoostID = (System.Int32)reader[((int)LuceneGlobalProductBoostColumn.LuceneGlobalProductBoostID - 1)];
					c.ProductID = (System.Int32)reader[((int)LuceneGlobalProductBoostColumn.ProductID - 1)];
					c.Boost = (System.Double)reader[((int)LuceneGlobalProductBoostColumn.Boost - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBoost"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBoost"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBoost entity)
		{
			if (!reader.Read()) return;
			
			entity.LuceneGlobalProductBoostID = (System.Int32)reader[((int)LuceneGlobalProductBoostColumn.LuceneGlobalProductBoostID - 1)];
			entity.ProductID = (System.Int32)reader[((int)LuceneGlobalProductBoostColumn.ProductID - 1)];
			entity.Boost = (System.Double)reader[((int)LuceneGlobalProductBoostColumn.Boost - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBoost"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBoost"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBoost entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.LuceneGlobalProductBoostID = (System.Int32)dataRow["LuceneGlobalProductBoostID"];
			entity.ProductID = (System.Int32)dataRow["ProductID"];
			entity.Boost = (System.Double)dataRow["Boost"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBoost"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBoost Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBoost entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region ProductIDSource	
			if (CanDeepLoad(entity, "Product|ProductIDSource", deepLoadType, innerList) 
				&& entity.ProductIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.ProductID;
				Product tmpEntity = EntityManager.LocateEntity<Product>(EntityLocator.ConstructKeyFromPkItems(typeof(Product), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ProductIDSource = tmpEntity;
				else
					entity.ProductIDSource = DataRepository.ProductProvider.GetByProductID(transactionManager, entity.ProductID);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ProductIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ProductIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.ProductProvider.DeepLoad(transactionManager, entity.ProductIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ProductIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBoost object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBoost instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBoost Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBoost entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region ProductIDSource
			if (CanDeepSave(entity, "Product|ProductIDSource", deepSaveType, innerList) 
				&& entity.ProductIDSource != null)
			{
				DataRepository.ProductProvider.Save(transactionManager, entity.ProductIDSource);
				entity.ProductID = entity.ProductIDSource.ProductID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region LuceneGlobalProductBoostChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBoost</c>
	///</summary>
	public enum LuceneGlobalProductBoostChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Product</c> at ProductIDSource
		///</summary>
		[ChildEntityType(typeof(Product))]
		Product,
	}
	
	#endregion LuceneGlobalProductBoostChildEntityTypes
	
	#region LuceneGlobalProductBoostFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;LuceneGlobalProductBoostColumn&gt;"/> class
	/// that is used exclusively with a <see cref="LuceneGlobalProductBoost"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class LuceneGlobalProductBoostFilterBuilder : SqlFilterBuilder<LuceneGlobalProductBoostColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the LuceneGlobalProductBoostFilterBuilder class.
		/// </summary>
		public LuceneGlobalProductBoostFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the LuceneGlobalProductBoostFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public LuceneGlobalProductBoostFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the LuceneGlobalProductBoostFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public LuceneGlobalProductBoostFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion LuceneGlobalProductBoostFilterBuilder
	
	#region LuceneGlobalProductBoostParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;LuceneGlobalProductBoostColumn&gt;"/> class
	/// that is used exclusively with a <see cref="LuceneGlobalProductBoost"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class LuceneGlobalProductBoostParameterBuilder : ParameterizedSqlFilterBuilder<LuceneGlobalProductBoostColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the LuceneGlobalProductBoostParameterBuilder class.
		/// </summary>
		public LuceneGlobalProductBoostParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the LuceneGlobalProductBoostParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public LuceneGlobalProductBoostParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the LuceneGlobalProductBoostParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public LuceneGlobalProductBoostParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion LuceneGlobalProductBoostParameterBuilder
	
	#region LuceneGlobalProductBoostSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;LuceneGlobalProductBoostColumn&gt;"/> class
	/// that is used exclusively with a <see cref="LuceneGlobalProductBoost"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class LuceneGlobalProductBoostSortBuilder : SqlSortBuilder<LuceneGlobalProductBoostColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the LuceneGlobalProductBoostSqlSortBuilder class.
		/// </summary>
		public LuceneGlobalProductBoostSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion LuceneGlobalProductBoostSortBuilder
	
} // end namespace
