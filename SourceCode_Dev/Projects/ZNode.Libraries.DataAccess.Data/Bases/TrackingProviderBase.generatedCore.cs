﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="TrackingProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class TrackingProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.Tracking, ZNode.Libraries.DataAccess.Entities.TrackingKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.TrackingKey key)
		{
			return Delete(transactionManager, key.TrackingID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_trackingID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _trackingID)
		{
			return Delete(null, _trackingID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_trackingID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _trackingID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeTracking_ZNodePortal key.
		///		FK_ZNodeTracking_ZNodePortal Description: 
		/// </summary>
		/// <param name="_portalID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Tracking objects.</returns>
		public TList<Tracking> GetByPortalID(System.Int32? _portalID)
		{
			int count = -1;
			return GetByPortalID(_portalID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeTracking_ZNodePortal key.
		///		FK_ZNodeTracking_ZNodePortal Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Tracking objects.</returns>
		/// <remarks></remarks>
		public TList<Tracking> GetByPortalID(TransactionManager transactionManager, System.Int32? _portalID)
		{
			int count = -1;
			return GetByPortalID(transactionManager, _portalID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeTracking_ZNodePortal key.
		///		FK_ZNodeTracking_ZNodePortal Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Tracking objects.</returns>
		public TList<Tracking> GetByPortalID(TransactionManager transactionManager, System.Int32? _portalID, int start, int pageLength)
		{
			int count = -1;
			return GetByPortalID(transactionManager, _portalID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeTracking_ZNodePortal key.
		///		fKZNodeTrackingZNodePortal Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_portalID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Tracking objects.</returns>
		public TList<Tracking> GetByPortalID(System.Int32? _portalID, int start, int pageLength)
		{
			int count =  -1;
			return GetByPortalID(null, _portalID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeTracking_ZNodePortal key.
		///		fKZNodeTrackingZNodePortal Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_portalID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Tracking objects.</returns>
		public TList<Tracking> GetByPortalID(System.Int32? _portalID, int start, int pageLength,out int count)
		{
			return GetByPortalID(null, _portalID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeTracking_ZNodePortal key.
		///		FK_ZNodeTracking_ZNodePortal Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Tracking objects.</returns>
		public abstract TList<Tracking> GetByPortalID(TransactionManager transactionManager, System.Int32? _portalID, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.Tracking Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.TrackingKey key, int start, int pageLength)
		{
			return GetByTrackingID(transactionManager, key.TrackingID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodeTracking index.
		/// </summary>
		/// <param name="_trackingID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Tracking"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Tracking GetByTrackingID(System.Int32 _trackingID)
		{
			int count = -1;
			return GetByTrackingID(null,_trackingID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeTracking index.
		/// </summary>
		/// <param name="_trackingID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Tracking"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Tracking GetByTrackingID(System.Int32 _trackingID, int start, int pageLength)
		{
			int count = -1;
			return GetByTrackingID(null, _trackingID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeTracking index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_trackingID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Tracking"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Tracking GetByTrackingID(TransactionManager transactionManager, System.Int32 _trackingID)
		{
			int count = -1;
			return GetByTrackingID(transactionManager, _trackingID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeTracking index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_trackingID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Tracking"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Tracking GetByTrackingID(TransactionManager transactionManager, System.Int32 _trackingID, int start, int pageLength)
		{
			int count = -1;
			return GetByTrackingID(transactionManager, _trackingID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeTracking index.
		/// </summary>
		/// <param name="_trackingID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Tracking"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Tracking GetByTrackingID(System.Int32 _trackingID, int start, int pageLength, out int count)
		{
			return GetByTrackingID(null, _trackingID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeTracking index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_trackingID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Tracking"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.Tracking GetByTrackingID(TransactionManager transactionManager, System.Int32 _trackingID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;Tracking&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;Tracking&gt;"/></returns>
		public static TList<Tracking> Fill(IDataReader reader, TList<Tracking> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.Tracking c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("Tracking")
					.Append("|").Append((System.Int32)reader[((int)TrackingColumn.TrackingID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<Tracking>(
					key.ToString(), // EntityTrackingKey
					"Tracking",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.Tracking();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.TrackingID = (System.Int32)reader[((int)TrackingColumn.TrackingID - 1)];
					c.PortalID = (reader.IsDBNull(((int)TrackingColumn.PortalID - 1)))?null:(System.Int32?)reader[((int)TrackingColumn.PortalID - 1)];
					c.ProfileID = (reader.IsDBNull(((int)TrackingColumn.ProfileID - 1)))?null:(System.Int32?)reader[((int)TrackingColumn.ProfileID - 1)];
					c.Date = (reader.IsDBNull(((int)TrackingColumn.Date - 1)))?null:(System.DateTime?)reader[((int)TrackingColumn.Date - 1)];
					c.DestinationDomain = (reader.IsDBNull(((int)TrackingColumn.DestinationDomain - 1)))?null:(System.String)reader[((int)TrackingColumn.DestinationDomain - 1)];
					c.RefererDomain = (reader.IsDBNull(((int)TrackingColumn.RefererDomain - 1)))?null:(System.String)reader[((int)TrackingColumn.RefererDomain - 1)];
					c.RefererQuery = (reader.IsDBNull(((int)TrackingColumn.RefererQuery - 1)))?null:(System.String)reader[((int)TrackingColumn.RefererQuery - 1)];
					c.AffiliateId = (reader.IsDBNull(((int)TrackingColumn.AffiliateId - 1)))?null:(System.Int32?)reader[((int)TrackingColumn.AffiliateId - 1)];
					c.CampaignSource = (reader.IsDBNull(((int)TrackingColumn.CampaignSource - 1)))?null:(System.String)reader[((int)TrackingColumn.CampaignSource - 1)];
					c.CampaignId = (reader.IsDBNull(((int)TrackingColumn.CampaignId - 1)))?null:(System.String)reader[((int)TrackingColumn.CampaignId - 1)];
					c.CampaignKeyword = (reader.IsDBNull(((int)TrackingColumn.CampaignKeyword - 1)))?null:(System.String)reader[((int)TrackingColumn.CampaignKeyword - 1)];
					c.Custom1 = (reader.IsDBNull(((int)TrackingColumn.Custom1 - 1)))?null:(System.String)reader[((int)TrackingColumn.Custom1 - 1)];
					c.Custom2 = (reader.IsDBNull(((int)TrackingColumn.Custom2 - 1)))?null:(System.String)reader[((int)TrackingColumn.Custom2 - 1)];
					c.Custom3 = (reader.IsDBNull(((int)TrackingColumn.Custom3 - 1)))?null:(System.String)reader[((int)TrackingColumn.Custom3 - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.Tracking"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.Tracking"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.Tracking entity)
		{
			if (!reader.Read()) return;
			
			entity.TrackingID = (System.Int32)reader[((int)TrackingColumn.TrackingID - 1)];
			entity.PortalID = (reader.IsDBNull(((int)TrackingColumn.PortalID - 1)))?null:(System.Int32?)reader[((int)TrackingColumn.PortalID - 1)];
			entity.ProfileID = (reader.IsDBNull(((int)TrackingColumn.ProfileID - 1)))?null:(System.Int32?)reader[((int)TrackingColumn.ProfileID - 1)];
			entity.Date = (reader.IsDBNull(((int)TrackingColumn.Date - 1)))?null:(System.DateTime?)reader[((int)TrackingColumn.Date - 1)];
			entity.DestinationDomain = (reader.IsDBNull(((int)TrackingColumn.DestinationDomain - 1)))?null:(System.String)reader[((int)TrackingColumn.DestinationDomain - 1)];
			entity.RefererDomain = (reader.IsDBNull(((int)TrackingColumn.RefererDomain - 1)))?null:(System.String)reader[((int)TrackingColumn.RefererDomain - 1)];
			entity.RefererQuery = (reader.IsDBNull(((int)TrackingColumn.RefererQuery - 1)))?null:(System.String)reader[((int)TrackingColumn.RefererQuery - 1)];
			entity.AffiliateId = (reader.IsDBNull(((int)TrackingColumn.AffiliateId - 1)))?null:(System.Int32?)reader[((int)TrackingColumn.AffiliateId - 1)];
			entity.CampaignSource = (reader.IsDBNull(((int)TrackingColumn.CampaignSource - 1)))?null:(System.String)reader[((int)TrackingColumn.CampaignSource - 1)];
			entity.CampaignId = (reader.IsDBNull(((int)TrackingColumn.CampaignId - 1)))?null:(System.String)reader[((int)TrackingColumn.CampaignId - 1)];
			entity.CampaignKeyword = (reader.IsDBNull(((int)TrackingColumn.CampaignKeyword - 1)))?null:(System.String)reader[((int)TrackingColumn.CampaignKeyword - 1)];
			entity.Custom1 = (reader.IsDBNull(((int)TrackingColumn.Custom1 - 1)))?null:(System.String)reader[((int)TrackingColumn.Custom1 - 1)];
			entity.Custom2 = (reader.IsDBNull(((int)TrackingColumn.Custom2 - 1)))?null:(System.String)reader[((int)TrackingColumn.Custom2 - 1)];
			entity.Custom3 = (reader.IsDBNull(((int)TrackingColumn.Custom3 - 1)))?null:(System.String)reader[((int)TrackingColumn.Custom3 - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.Tracking"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.Tracking"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.Tracking entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.TrackingID = (System.Int32)dataRow["TrackingID"];
			entity.PortalID = Convert.IsDBNull(dataRow["PortalID"]) ? null : (System.Int32?)dataRow["PortalID"];
			entity.ProfileID = Convert.IsDBNull(dataRow["ProfileID"]) ? null : (System.Int32?)dataRow["ProfileID"];
			entity.Date = Convert.IsDBNull(dataRow["Date"]) ? null : (System.DateTime?)dataRow["Date"];
			entity.DestinationDomain = Convert.IsDBNull(dataRow["DestinationDomain"]) ? null : (System.String)dataRow["DestinationDomain"];
			entity.RefererDomain = Convert.IsDBNull(dataRow["RefererDomain"]) ? null : (System.String)dataRow["RefererDomain"];
			entity.RefererQuery = Convert.IsDBNull(dataRow["RefererQuery"]) ? null : (System.String)dataRow["RefererQuery"];
			entity.AffiliateId = Convert.IsDBNull(dataRow["AffiliateId"]) ? null : (System.Int32?)dataRow["AffiliateId"];
			entity.CampaignSource = Convert.IsDBNull(dataRow["CampaignSource"]) ? null : (System.String)dataRow["CampaignSource"];
			entity.CampaignId = Convert.IsDBNull(dataRow["CampaignId"]) ? null : (System.String)dataRow["CampaignId"];
			entity.CampaignKeyword = Convert.IsDBNull(dataRow["CampaignKeyword"]) ? null : (System.String)dataRow["CampaignKeyword"];
			entity.Custom1 = Convert.IsDBNull(dataRow["Custom1"]) ? null : (System.String)dataRow["Custom1"];
			entity.Custom2 = Convert.IsDBNull(dataRow["Custom2"]) ? null : (System.String)dataRow["Custom2"];
			entity.Custom3 = Convert.IsDBNull(dataRow["Custom3"]) ? null : (System.String)dataRow["Custom3"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.Tracking"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.Tracking Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.Tracking entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region PortalIDSource	
			if (CanDeepLoad(entity, "Portal|PortalIDSource", deepLoadType, innerList) 
				&& entity.PortalIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.PortalID ?? (int)0);
				Portal tmpEntity = EntityManager.LocateEntity<Portal>(EntityLocator.ConstructKeyFromPkItems(typeof(Portal), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.PortalIDSource = tmpEntity;
				else
					entity.PortalIDSource = DataRepository.PortalProvider.GetByPortalID(transactionManager, (entity.PortalID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PortalIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.PortalIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.PortalProvider.DeepLoad(transactionManager, entity.PortalIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion PortalIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByTrackingID methods when available
			
			#region TrackingEventCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<TrackingEvent>|TrackingEventCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'TrackingEventCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.TrackingEventCollection = DataRepository.TrackingEventProvider.GetByTrackingID(transactionManager, entity.TrackingID);

				if (deep && entity.TrackingEventCollection.Count > 0)
				{
					deepHandles.Add("TrackingEventCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<TrackingEvent>) DataRepository.TrackingEventProvider.DeepLoad,
						new object[] { transactionManager, entity.TrackingEventCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.Tracking object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.Tracking instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.Tracking Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.Tracking entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region PortalIDSource
			if (CanDeepSave(entity, "Portal|PortalIDSource", deepSaveType, innerList) 
				&& entity.PortalIDSource != null)
			{
				DataRepository.PortalProvider.Save(transactionManager, entity.PortalIDSource);
				entity.PortalID = entity.PortalIDSource.PortalID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<TrackingEvent>
				if (CanDeepSave(entity.TrackingEventCollection, "List<TrackingEvent>|TrackingEventCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(TrackingEvent child in entity.TrackingEventCollection)
					{
						if(child.TrackingIDSource != null)
						{
							child.TrackingID = child.TrackingIDSource.TrackingID;
						}
						else
						{
							child.TrackingID = entity.TrackingID;
						}

					}

					if (entity.TrackingEventCollection.Count > 0 || entity.TrackingEventCollection.DeletedItems.Count > 0)
					{
						//DataRepository.TrackingEventProvider.Save(transactionManager, entity.TrackingEventCollection);
						
						deepHandles.Add("TrackingEventCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< TrackingEvent >) DataRepository.TrackingEventProvider.DeepSave,
							new object[] { transactionManager, entity.TrackingEventCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region TrackingChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.Tracking</c>
	///</summary>
	public enum TrackingChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Portal</c> at PortalIDSource
		///</summary>
		[ChildEntityType(typeof(Portal))]
		Portal,
		///<summary>
		/// Collection of <c>Tracking</c> as OneToMany for TrackingEventCollection
		///</summary>
		[ChildEntityType(typeof(TList<TrackingEvent>))]
		TrackingEventCollection,
	}
	
	#endregion TrackingChildEntityTypes
	
	#region TrackingFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;TrackingColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Tracking"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TrackingFilterBuilder : SqlFilterBuilder<TrackingColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TrackingFilterBuilder class.
		/// </summary>
		public TrackingFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the TrackingFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TrackingFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TrackingFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TrackingFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TrackingFilterBuilder
	
	#region TrackingParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;TrackingColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Tracking"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TrackingParameterBuilder : ParameterizedSqlFilterBuilder<TrackingColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TrackingParameterBuilder class.
		/// </summary>
		public TrackingParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the TrackingParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TrackingParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TrackingParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TrackingParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TrackingParameterBuilder
	
	#region TrackingSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;TrackingColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Tracking"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class TrackingSortBuilder : SqlSortBuilder<TrackingColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TrackingSqlSortBuilder class.
		/// </summary>
		public TrackingSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion TrackingSortBuilder
	
} // end namespace
