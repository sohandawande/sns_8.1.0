﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="PortalProfileProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class PortalProfileProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.PortalProfile, ZNode.Libraries.DataAccess.Entities.PortalProfileKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.PortalProfileKey key)
		{
			return Delete(transactionManager, key.PortalProfileID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_portalProfileID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _portalProfileID)
		{
			return Delete(null, _portalProfileID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalProfileID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _portalProfileID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePortalProfile_ZNodePortal key.
		///		FK_ZNodePortalProfile_ZNodePortal Description: 
		/// </summary>
		/// <param name="_portalID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.PortalProfile objects.</returns>
		public TList<PortalProfile> GetByPortalID(System.Int32 _portalID)
		{
			int count = -1;
			return GetByPortalID(_portalID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePortalProfile_ZNodePortal key.
		///		FK_ZNodePortalProfile_ZNodePortal Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.PortalProfile objects.</returns>
		/// <remarks></remarks>
		public TList<PortalProfile> GetByPortalID(TransactionManager transactionManager, System.Int32 _portalID)
		{
			int count = -1;
			return GetByPortalID(transactionManager, _portalID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePortalProfile_ZNodePortal key.
		///		FK_ZNodePortalProfile_ZNodePortal Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.PortalProfile objects.</returns>
		public TList<PortalProfile> GetByPortalID(TransactionManager transactionManager, System.Int32 _portalID, int start, int pageLength)
		{
			int count = -1;
			return GetByPortalID(transactionManager, _portalID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePortalProfile_ZNodePortal key.
		///		fKZNodePortalProfileZNodePortal Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_portalID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.PortalProfile objects.</returns>
		public TList<PortalProfile> GetByPortalID(System.Int32 _portalID, int start, int pageLength)
		{
			int count =  -1;
			return GetByPortalID(null, _portalID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePortalProfile_ZNodePortal key.
		///		fKZNodePortalProfileZNodePortal Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_portalID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.PortalProfile objects.</returns>
		public TList<PortalProfile> GetByPortalID(System.Int32 _portalID, int start, int pageLength,out int count)
		{
			return GetByPortalID(null, _portalID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePortalProfile_ZNodePortal key.
		///		FK_ZNodePortalProfile_ZNodePortal Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.PortalProfile objects.</returns>
		public abstract TList<PortalProfile> GetByPortalID(TransactionManager transactionManager, System.Int32 _portalID, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePortalProfile_ZNodeProfile key.
		///		FK_ZNodePortalProfile_ZNodeProfile Description: 
		/// </summary>
		/// <param name="_profileID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.PortalProfile objects.</returns>
		public TList<PortalProfile> GetByProfileID(System.Int32 _profileID)
		{
			int count = -1;
			return GetByProfileID(_profileID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePortalProfile_ZNodeProfile key.
		///		FK_ZNodePortalProfile_ZNodeProfile Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_profileID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.PortalProfile objects.</returns>
		/// <remarks></remarks>
		public TList<PortalProfile> GetByProfileID(TransactionManager transactionManager, System.Int32 _profileID)
		{
			int count = -1;
			return GetByProfileID(transactionManager, _profileID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePortalProfile_ZNodeProfile key.
		///		FK_ZNodePortalProfile_ZNodeProfile Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_profileID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.PortalProfile objects.</returns>
		public TList<PortalProfile> GetByProfileID(TransactionManager transactionManager, System.Int32 _profileID, int start, int pageLength)
		{
			int count = -1;
			return GetByProfileID(transactionManager, _profileID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePortalProfile_ZNodeProfile key.
		///		fKZNodePortalProfileZNodeProfile Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_profileID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.PortalProfile objects.</returns>
		public TList<PortalProfile> GetByProfileID(System.Int32 _profileID, int start, int pageLength)
		{
			int count =  -1;
			return GetByProfileID(null, _profileID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePortalProfile_ZNodeProfile key.
		///		fKZNodePortalProfileZNodeProfile Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_profileID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.PortalProfile objects.</returns>
		public TList<PortalProfile> GetByProfileID(System.Int32 _profileID, int start, int pageLength,out int count)
		{
			return GetByProfileID(null, _profileID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePortalProfile_ZNodeProfile key.
		///		FK_ZNodePortalProfile_ZNodeProfile Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_profileID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.PortalProfile objects.</returns>
		public abstract TList<PortalProfile> GetByProfileID(TransactionManager transactionManager, System.Int32 _profileID, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.PortalProfile Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.PortalProfileKey key, int start, int pageLength)
		{
			return GetByPortalProfileID(transactionManager, key.PortalProfileID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodePortalProfile index.
		/// </summary>
		/// <param name="_portalProfileID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.PortalProfile"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.PortalProfile GetByPortalProfileID(System.Int32 _portalProfileID)
		{
			int count = -1;
			return GetByPortalProfileID(null,_portalProfileID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodePortalProfile index.
		/// </summary>
		/// <param name="_portalProfileID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.PortalProfile"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.PortalProfile GetByPortalProfileID(System.Int32 _portalProfileID, int start, int pageLength)
		{
			int count = -1;
			return GetByPortalProfileID(null, _portalProfileID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodePortalProfile index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalProfileID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.PortalProfile"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.PortalProfile GetByPortalProfileID(TransactionManager transactionManager, System.Int32 _portalProfileID)
		{
			int count = -1;
			return GetByPortalProfileID(transactionManager, _portalProfileID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodePortalProfile index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalProfileID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.PortalProfile"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.PortalProfile GetByPortalProfileID(TransactionManager transactionManager, System.Int32 _portalProfileID, int start, int pageLength)
		{
			int count = -1;
			return GetByPortalProfileID(transactionManager, _portalProfileID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodePortalProfile index.
		/// </summary>
		/// <param name="_portalProfileID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.PortalProfile"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.PortalProfile GetByPortalProfileID(System.Int32 _portalProfileID, int start, int pageLength, out int count)
		{
			return GetByPortalProfileID(null, _portalProfileID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodePortalProfile index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalProfileID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.PortalProfile"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.PortalProfile GetByPortalProfileID(TransactionManager transactionManager, System.Int32 _portalProfileID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;PortalProfile&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;PortalProfile&gt;"/></returns>
		public static TList<PortalProfile> Fill(IDataReader reader, TList<PortalProfile> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.PortalProfile c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("PortalProfile")
					.Append("|").Append((System.Int32)reader[((int)PortalProfileColumn.PortalProfileID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<PortalProfile>(
					key.ToString(), // EntityTrackingKey
					"PortalProfile",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.PortalProfile();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.PortalProfileID = (System.Int32)reader[((int)PortalProfileColumn.PortalProfileID - 1)];
					c.PortalID = (System.Int32)reader[((int)PortalProfileColumn.PortalID - 1)];
					c.ProfileID = (System.Int32)reader[((int)PortalProfileColumn.ProfileID - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.PortalProfile"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.PortalProfile"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.PortalProfile entity)
		{
			if (!reader.Read()) return;
			
			entity.PortalProfileID = (System.Int32)reader[((int)PortalProfileColumn.PortalProfileID - 1)];
			entity.PortalID = (System.Int32)reader[((int)PortalProfileColumn.PortalID - 1)];
			entity.ProfileID = (System.Int32)reader[((int)PortalProfileColumn.ProfileID - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.PortalProfile"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.PortalProfile"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.PortalProfile entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.PortalProfileID = (System.Int32)dataRow["PortalProfileID"];
			entity.PortalID = (System.Int32)dataRow["PortalID"];
			entity.ProfileID = (System.Int32)dataRow["ProfileID"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.PortalProfile"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.PortalProfile Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.PortalProfile entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region PortalIDSource	
			if (CanDeepLoad(entity, "Portal|PortalIDSource", deepLoadType, innerList) 
				&& entity.PortalIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.PortalID;
				Portal tmpEntity = EntityManager.LocateEntity<Portal>(EntityLocator.ConstructKeyFromPkItems(typeof(Portal), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.PortalIDSource = tmpEntity;
				else
					entity.PortalIDSource = DataRepository.PortalProvider.GetByPortalID(transactionManager, entity.PortalID);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PortalIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.PortalIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.PortalProvider.DeepLoad(transactionManager, entity.PortalIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion PortalIDSource

			#region ProfileIDSource	
			if (CanDeepLoad(entity, "Profile|ProfileIDSource", deepLoadType, innerList) 
				&& entity.ProfileIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.ProfileID;
				Profile tmpEntity = EntityManager.LocateEntity<Profile>(EntityLocator.ConstructKeyFromPkItems(typeof(Profile), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ProfileIDSource = tmpEntity;
				else
					entity.ProfileIDSource = DataRepository.ProfileProvider.GetByProfileID(transactionManager, entity.ProfileID);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ProfileIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ProfileIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.ProfileProvider.DeepLoad(transactionManager, entity.ProfileIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ProfileIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.PortalProfile object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.PortalProfile instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.PortalProfile Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.PortalProfile entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region PortalIDSource
			if (CanDeepSave(entity, "Portal|PortalIDSource", deepSaveType, innerList) 
				&& entity.PortalIDSource != null)
			{
				DataRepository.PortalProvider.Save(transactionManager, entity.PortalIDSource);
				entity.PortalID = entity.PortalIDSource.PortalID;
			}
			#endregion 
			
			#region ProfileIDSource
			if (CanDeepSave(entity, "Profile|ProfileIDSource", deepSaveType, innerList) 
				&& entity.ProfileIDSource != null)
			{
				DataRepository.ProfileProvider.Save(transactionManager, entity.ProfileIDSource);
				entity.ProfileID = entity.ProfileIDSource.ProfileID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region PortalProfileChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.PortalProfile</c>
	///</summary>
	public enum PortalProfileChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Portal</c> at PortalIDSource
		///</summary>
		[ChildEntityType(typeof(Portal))]
		Portal,
		
		///<summary>
		/// Composite Property for <c>Profile</c> at ProfileIDSource
		///</summary>
		[ChildEntityType(typeof(Profile))]
		Profile,
	}
	
	#endregion PortalProfileChildEntityTypes
	
	#region PortalProfileFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;PortalProfileColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PortalProfile"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PortalProfileFilterBuilder : SqlFilterBuilder<PortalProfileColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PortalProfileFilterBuilder class.
		/// </summary>
		public PortalProfileFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the PortalProfileFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PortalProfileFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PortalProfileFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PortalProfileFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PortalProfileFilterBuilder
	
	#region PortalProfileParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;PortalProfileColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PortalProfile"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PortalProfileParameterBuilder : ParameterizedSqlFilterBuilder<PortalProfileColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PortalProfileParameterBuilder class.
		/// </summary>
		public PortalProfileParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the PortalProfileParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PortalProfileParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PortalProfileParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PortalProfileParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PortalProfileParameterBuilder
	
	#region PortalProfileSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;PortalProfileColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PortalProfile"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class PortalProfileSortBuilder : SqlSortBuilder<PortalProfileColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PortalProfileSqlSortBuilder class.
		/// </summary>
		public PortalProfileSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion PortalProfileSortBuilder
	
} // end namespace
