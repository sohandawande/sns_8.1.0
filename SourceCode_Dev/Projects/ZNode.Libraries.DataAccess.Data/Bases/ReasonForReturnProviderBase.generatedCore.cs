﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ReasonForReturnProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class ReasonForReturnProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.ReasonForReturn, ZNode.Libraries.DataAccess.Entities.ReasonForReturnKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ReasonForReturnKey key)
		{
			return Delete(transactionManager, key.ReasonForReturnID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_reasonForReturnID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _reasonForReturnID)
		{
			return Delete(null, _reasonForReturnID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_reasonForReturnID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _reasonForReturnID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.ReasonForReturn Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ReasonForReturnKey key, int start, int pageLength)
		{
			return GetByReasonForReturnID(transactionManager, key.ReasonForReturnID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodeRMAReasonForReturn index.
		/// </summary>
		/// <param name="_reasonForReturnID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ReasonForReturn"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ReasonForReturn GetByReasonForReturnID(System.Int32 _reasonForReturnID)
		{
			int count = -1;
			return GetByReasonForReturnID(null,_reasonForReturnID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeRMAReasonForReturn index.
		/// </summary>
		/// <param name="_reasonForReturnID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ReasonForReturn"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ReasonForReturn GetByReasonForReturnID(System.Int32 _reasonForReturnID, int start, int pageLength)
		{
			int count = -1;
			return GetByReasonForReturnID(null, _reasonForReturnID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeRMAReasonForReturn index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_reasonForReturnID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ReasonForReturn"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ReasonForReturn GetByReasonForReturnID(TransactionManager transactionManager, System.Int32 _reasonForReturnID)
		{
			int count = -1;
			return GetByReasonForReturnID(transactionManager, _reasonForReturnID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeRMAReasonForReturn index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_reasonForReturnID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ReasonForReturn"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ReasonForReturn GetByReasonForReturnID(TransactionManager transactionManager, System.Int32 _reasonForReturnID, int start, int pageLength)
		{
			int count = -1;
			return GetByReasonForReturnID(transactionManager, _reasonForReturnID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeRMAReasonForReturn index.
		/// </summary>
		/// <param name="_reasonForReturnID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ReasonForReturn"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ReasonForReturn GetByReasonForReturnID(System.Int32 _reasonForReturnID, int start, int pageLength, out int count)
		{
			return GetByReasonForReturnID(null, _reasonForReturnID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeRMAReasonForReturn index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_reasonForReturnID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ReasonForReturn"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.ReasonForReturn GetByReasonForReturnID(TransactionManager transactionManager, System.Int32 _reasonForReturnID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;ReasonForReturn&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;ReasonForReturn&gt;"/></returns>
		public static TList<ReasonForReturn> Fill(IDataReader reader, TList<ReasonForReturn> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.ReasonForReturn c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("ReasonForReturn")
					.Append("|").Append((System.Int32)reader[((int)ReasonForReturnColumn.ReasonForReturnID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<ReasonForReturn>(
					key.ToString(), // EntityTrackingKey
					"ReasonForReturn",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.ReasonForReturn();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.ReasonForReturnID = (System.Int32)reader[((int)ReasonForReturnColumn.ReasonForReturnID - 1)];
					c.Name = (System.String)reader[((int)ReasonForReturnColumn.Name - 1)];
					c.IsEnabled = (System.Boolean)reader[((int)ReasonForReturnColumn.IsEnabled - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.ReasonForReturn"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ReasonForReturn"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.ReasonForReturn entity)
		{
			if (!reader.Read()) return;
			
			entity.ReasonForReturnID = (System.Int32)reader[((int)ReasonForReturnColumn.ReasonForReturnID - 1)];
			entity.Name = (System.String)reader[((int)ReasonForReturnColumn.Name - 1)];
			entity.IsEnabled = (System.Boolean)reader[((int)ReasonForReturnColumn.IsEnabled - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.ReasonForReturn"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ReasonForReturn"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.ReasonForReturn entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.ReasonForReturnID = (System.Int32)dataRow["ReasonForReturnID"];
			entity.Name = (System.String)dataRow["Name"];
			entity.IsEnabled = (System.Boolean)dataRow["IsEnabled"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ReasonForReturn"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.ReasonForReturn Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ReasonForReturn entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByReasonForReturnID methods when available
			
			#region RMARequestItemCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<RMARequestItem>|RMARequestItemCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'RMARequestItemCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.RMARequestItemCollection = DataRepository.RMARequestItemProvider.GetByReasonForReturnID(transactionManager, entity.ReasonForReturnID);

				if (deep && entity.RMARequestItemCollection.Count > 0)
				{
					deepHandles.Add("RMARequestItemCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<RMARequestItem>) DataRepository.RMARequestItemProvider.DeepLoad,
						new object[] { transactionManager, entity.RMARequestItemCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.ReasonForReturn object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.ReasonForReturn instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.ReasonForReturn Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ReasonForReturn entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<RMARequestItem>
				if (CanDeepSave(entity.RMARequestItemCollection, "List<RMARequestItem>|RMARequestItemCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(RMARequestItem child in entity.RMARequestItemCollection)
					{
						if(child.ReasonForReturnIDSource != null)
						{
							child.ReasonForReturnID = child.ReasonForReturnIDSource.ReasonForReturnID;
						}
						else
						{
							child.ReasonForReturnID = entity.ReasonForReturnID;
						}

					}

					if (entity.RMARequestItemCollection.Count > 0 || entity.RMARequestItemCollection.DeletedItems.Count > 0)
					{
						//DataRepository.RMARequestItemProvider.Save(transactionManager, entity.RMARequestItemCollection);
						
						deepHandles.Add("RMARequestItemCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< RMARequestItem >) DataRepository.RMARequestItemProvider.DeepSave,
							new object[] { transactionManager, entity.RMARequestItemCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region ReasonForReturnChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.ReasonForReturn</c>
	///</summary>
	public enum ReasonForReturnChildEntityTypes
	{
		///<summary>
		/// Collection of <c>ReasonForReturn</c> as OneToMany for RMARequestItemCollection
		///</summary>
		[ChildEntityType(typeof(TList<RMARequestItem>))]
		RMARequestItemCollection,
	}
	
	#endregion ReasonForReturnChildEntityTypes
	
	#region ReasonForReturnFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;ReasonForReturnColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ReasonForReturn"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ReasonForReturnFilterBuilder : SqlFilterBuilder<ReasonForReturnColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ReasonForReturnFilterBuilder class.
		/// </summary>
		public ReasonForReturnFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ReasonForReturnFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ReasonForReturnFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ReasonForReturnFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ReasonForReturnFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ReasonForReturnFilterBuilder
	
	#region ReasonForReturnParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;ReasonForReturnColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ReasonForReturn"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ReasonForReturnParameterBuilder : ParameterizedSqlFilterBuilder<ReasonForReturnColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ReasonForReturnParameterBuilder class.
		/// </summary>
		public ReasonForReturnParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ReasonForReturnParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ReasonForReturnParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ReasonForReturnParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ReasonForReturnParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ReasonForReturnParameterBuilder
	
	#region ReasonForReturnSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;ReasonForReturnColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ReasonForReturn"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ReasonForReturnSortBuilder : SqlSortBuilder<ReasonForReturnColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ReasonForReturnSqlSortBuilder class.
		/// </summary>
		public ReasonForReturnSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ReasonForReturnSortBuilder
	
} // end namespace
