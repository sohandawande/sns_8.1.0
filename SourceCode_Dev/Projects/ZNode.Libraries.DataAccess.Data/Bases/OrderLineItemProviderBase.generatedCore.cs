﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="OrderLineItemProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class OrderLineItemProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.OrderLineItem, ZNode.Libraries.DataAccess.Entities.OrderLineItemKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.OrderLineItemKey key)
		{
			return Delete(transactionManager, key.OrderLineItemID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_orderLineItemID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _orderLineItemID)
		{
			return Delete(null, _orderLineItemID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_orderLineItemID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _orderLineItemID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeOrderLineItem_ZNodeOrderLineItem key.
		///		FK_ZNodeOrderLineItem_ZNodeOrderLineItem Description: 
		/// </summary>
		/// <param name="_parentOrderLineItemID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.OrderLineItem objects.</returns>
		public TList<OrderLineItem> GetByParentOrderLineItemID(System.Int32? _parentOrderLineItemID)
		{
			int count = -1;
			return GetByParentOrderLineItemID(_parentOrderLineItemID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeOrderLineItem_ZNodeOrderLineItem key.
		///		FK_ZNodeOrderLineItem_ZNodeOrderLineItem Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_parentOrderLineItemID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.OrderLineItem objects.</returns>
		/// <remarks></remarks>
		public TList<OrderLineItem> GetByParentOrderLineItemID(TransactionManager transactionManager, System.Int32? _parentOrderLineItemID)
		{
			int count = -1;
			return GetByParentOrderLineItemID(transactionManager, _parentOrderLineItemID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeOrderLineItem_ZNodeOrderLineItem key.
		///		FK_ZNodeOrderLineItem_ZNodeOrderLineItem Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_parentOrderLineItemID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.OrderLineItem objects.</returns>
		public TList<OrderLineItem> GetByParentOrderLineItemID(TransactionManager transactionManager, System.Int32? _parentOrderLineItemID, int start, int pageLength)
		{
			int count = -1;
			return GetByParentOrderLineItemID(transactionManager, _parentOrderLineItemID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeOrderLineItem_ZNodeOrderLineItem key.
		///		fKZNodeOrderLineItemZNodeOrderLineItem Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_parentOrderLineItemID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.OrderLineItem objects.</returns>
		public TList<OrderLineItem> GetByParentOrderLineItemID(System.Int32? _parentOrderLineItemID, int start, int pageLength)
		{
			int count =  -1;
			return GetByParentOrderLineItemID(null, _parentOrderLineItemID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeOrderLineItem_ZNodeOrderLineItem key.
		///		fKZNodeOrderLineItemZNodeOrderLineItem Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_parentOrderLineItemID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.OrderLineItem objects.</returns>
		public TList<OrderLineItem> GetByParentOrderLineItemID(System.Int32? _parentOrderLineItemID, int start, int pageLength,out int count)
		{
			return GetByParentOrderLineItemID(null, _parentOrderLineItemID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeOrderLineItem_ZNodeOrderLineItem key.
		///		FK_ZNodeOrderLineItem_ZNodeOrderLineItem Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_parentOrderLineItemID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.OrderLineItem objects.</returns>
		public abstract TList<OrderLineItem> GetByParentOrderLineItemID(TransactionManager transactionManager, System.Int32? _parentOrderLineItemID, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeOrderLineItem_ZNodeOrderLineItemRelationshipType key.
		///		FK_ZNodeOrderLineItem_ZNodeOrderLineItemRelationshipType Description: 
		/// </summary>
		/// <param name="_orderLineItemRelationshipTypeID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.OrderLineItem objects.</returns>
		public TList<OrderLineItem> GetByOrderLineItemRelationshipTypeID(System.Int32? _orderLineItemRelationshipTypeID)
		{
			int count = -1;
			return GetByOrderLineItemRelationshipTypeID(_orderLineItemRelationshipTypeID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeOrderLineItem_ZNodeOrderLineItemRelationshipType key.
		///		FK_ZNodeOrderLineItem_ZNodeOrderLineItemRelationshipType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_orderLineItemRelationshipTypeID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.OrderLineItem objects.</returns>
		/// <remarks></remarks>
		public TList<OrderLineItem> GetByOrderLineItemRelationshipTypeID(TransactionManager transactionManager, System.Int32? _orderLineItemRelationshipTypeID)
		{
			int count = -1;
			return GetByOrderLineItemRelationshipTypeID(transactionManager, _orderLineItemRelationshipTypeID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeOrderLineItem_ZNodeOrderLineItemRelationshipType key.
		///		FK_ZNodeOrderLineItem_ZNodeOrderLineItemRelationshipType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_orderLineItemRelationshipTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.OrderLineItem objects.</returns>
		public TList<OrderLineItem> GetByOrderLineItemRelationshipTypeID(TransactionManager transactionManager, System.Int32? _orderLineItemRelationshipTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetByOrderLineItemRelationshipTypeID(transactionManager, _orderLineItemRelationshipTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeOrderLineItem_ZNodeOrderLineItemRelationshipType key.
		///		fKZNodeOrderLineItemZNodeOrderLineItemRelationshipType Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_orderLineItemRelationshipTypeID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.OrderLineItem objects.</returns>
		public TList<OrderLineItem> GetByOrderLineItemRelationshipTypeID(System.Int32? _orderLineItemRelationshipTypeID, int start, int pageLength)
		{
			int count =  -1;
			return GetByOrderLineItemRelationshipTypeID(null, _orderLineItemRelationshipTypeID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeOrderLineItem_ZNodeOrderLineItemRelationshipType key.
		///		fKZNodeOrderLineItemZNodeOrderLineItemRelationshipType Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_orderLineItemRelationshipTypeID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.OrderLineItem objects.</returns>
		public TList<OrderLineItem> GetByOrderLineItemRelationshipTypeID(System.Int32? _orderLineItemRelationshipTypeID, int start, int pageLength,out int count)
		{
			return GetByOrderLineItemRelationshipTypeID(null, _orderLineItemRelationshipTypeID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeOrderLineItem_ZNodeOrderLineItemRelationshipType key.
		///		FK_ZNodeOrderLineItem_ZNodeOrderLineItemRelationshipType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_orderLineItemRelationshipTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.OrderLineItem objects.</returns>
		public abstract TList<OrderLineItem> GetByOrderLineItemRelationshipTypeID(TransactionManager transactionManager, System.Int32? _orderLineItemRelationshipTypeID, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeOrderLineItem_ZNodeOrderShipment key.
		///		FK_ZNodeOrderLineItem_ZNodeOrderShipment Description: 
		/// </summary>
		/// <param name="_orderShipmentID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.OrderLineItem objects.</returns>
		public TList<OrderLineItem> GetByOrderShipmentID(System.Int32? _orderShipmentID)
		{
			int count = -1;
			return GetByOrderShipmentID(_orderShipmentID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeOrderLineItem_ZNodeOrderShipment key.
		///		FK_ZNodeOrderLineItem_ZNodeOrderShipment Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_orderShipmentID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.OrderLineItem objects.</returns>
		/// <remarks></remarks>
		public TList<OrderLineItem> GetByOrderShipmentID(TransactionManager transactionManager, System.Int32? _orderShipmentID)
		{
			int count = -1;
			return GetByOrderShipmentID(transactionManager, _orderShipmentID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeOrderLineItem_ZNodeOrderShipment key.
		///		FK_ZNodeOrderLineItem_ZNodeOrderShipment Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_orderShipmentID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.OrderLineItem objects.</returns>
		public TList<OrderLineItem> GetByOrderShipmentID(TransactionManager transactionManager, System.Int32? _orderShipmentID, int start, int pageLength)
		{
			int count = -1;
			return GetByOrderShipmentID(transactionManager, _orderShipmentID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeOrderLineItem_ZNodeOrderShipment key.
		///		fKZNodeOrderLineItemZNodeOrderShipment Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_orderShipmentID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.OrderLineItem objects.</returns>
		public TList<OrderLineItem> GetByOrderShipmentID(System.Int32? _orderShipmentID, int start, int pageLength)
		{
			int count =  -1;
			return GetByOrderShipmentID(null, _orderShipmentID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeOrderLineItem_ZNodeOrderShipment key.
		///		fKZNodeOrderLineItemZNodeOrderShipment Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_orderShipmentID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.OrderLineItem objects.</returns>
		public TList<OrderLineItem> GetByOrderShipmentID(System.Int32? _orderShipmentID, int start, int pageLength,out int count)
		{
			return GetByOrderShipmentID(null, _orderShipmentID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeOrderLineItem_ZNodeOrderShipment key.
		///		FK_ZNodeOrderLineItem_ZNodeOrderShipment Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_orderShipmentID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.OrderLineItem objects.</returns>
		public abstract TList<OrderLineItem> GetByOrderShipmentID(TransactionManager transactionManager, System.Int32? _orderShipmentID, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeOrderLineItem_ZNodeOrderState key.
		///		FK_ZNodeOrderLineItem_ZNodeOrderState Description: 
		/// </summary>
		/// <param name="_orderLineItemStateID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.OrderLineItem objects.</returns>
		public TList<OrderLineItem> GetByOrderLineItemStateID(System.Int32? _orderLineItemStateID)
		{
			int count = -1;
			return GetByOrderLineItemStateID(_orderLineItemStateID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeOrderLineItem_ZNodeOrderState key.
		///		FK_ZNodeOrderLineItem_ZNodeOrderState Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_orderLineItemStateID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.OrderLineItem objects.</returns>
		/// <remarks></remarks>
		public TList<OrderLineItem> GetByOrderLineItemStateID(TransactionManager transactionManager, System.Int32? _orderLineItemStateID)
		{
			int count = -1;
			return GetByOrderLineItemStateID(transactionManager, _orderLineItemStateID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeOrderLineItem_ZNodeOrderState key.
		///		FK_ZNodeOrderLineItem_ZNodeOrderState Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_orderLineItemStateID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.OrderLineItem objects.</returns>
		public TList<OrderLineItem> GetByOrderLineItemStateID(TransactionManager transactionManager, System.Int32? _orderLineItemStateID, int start, int pageLength)
		{
			int count = -1;
			return GetByOrderLineItemStateID(transactionManager, _orderLineItemStateID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeOrderLineItem_ZNodeOrderState key.
		///		fKZNodeOrderLineItemZNodeOrderState Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_orderLineItemStateID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.OrderLineItem objects.</returns>
		public TList<OrderLineItem> GetByOrderLineItemStateID(System.Int32? _orderLineItemStateID, int start, int pageLength)
		{
			int count =  -1;
			return GetByOrderLineItemStateID(null, _orderLineItemStateID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeOrderLineItem_ZNodeOrderState key.
		///		fKZNodeOrderLineItemZNodeOrderState Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_orderLineItemStateID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.OrderLineItem objects.</returns>
		public TList<OrderLineItem> GetByOrderLineItemStateID(System.Int32? _orderLineItemStateID, int start, int pageLength,out int count)
		{
			return GetByOrderLineItemStateID(null, _orderLineItemStateID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeOrderLineItem_ZNodeOrderState key.
		///		FK_ZNodeOrderLineItem_ZNodeOrderState Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_orderLineItemStateID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.OrderLineItem objects.</returns>
		public abstract TList<OrderLineItem> GetByOrderLineItemStateID(TransactionManager transactionManager, System.Int32? _orderLineItemStateID, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeOrderLineItem_ZNodePaymentStatus key.
		///		FK_ZNodeOrderLineItem_ZNodePaymentStatus Description: 
		/// </summary>
		/// <param name="_paymentStatusID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.OrderLineItem objects.</returns>
		public TList<OrderLineItem> GetByPaymentStatusID(System.Int32? _paymentStatusID)
		{
			int count = -1;
			return GetByPaymentStatusID(_paymentStatusID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeOrderLineItem_ZNodePaymentStatus key.
		///		FK_ZNodeOrderLineItem_ZNodePaymentStatus Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_paymentStatusID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.OrderLineItem objects.</returns>
		/// <remarks></remarks>
		public TList<OrderLineItem> GetByPaymentStatusID(TransactionManager transactionManager, System.Int32? _paymentStatusID)
		{
			int count = -1;
			return GetByPaymentStatusID(transactionManager, _paymentStatusID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeOrderLineItem_ZNodePaymentStatus key.
		///		FK_ZNodeOrderLineItem_ZNodePaymentStatus Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_paymentStatusID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.OrderLineItem objects.</returns>
		public TList<OrderLineItem> GetByPaymentStatusID(TransactionManager transactionManager, System.Int32? _paymentStatusID, int start, int pageLength)
		{
			int count = -1;
			return GetByPaymentStatusID(transactionManager, _paymentStatusID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeOrderLineItem_ZNodePaymentStatus key.
		///		fKZNodeOrderLineItemZNodePaymentStatus Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_paymentStatusID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.OrderLineItem objects.</returns>
		public TList<OrderLineItem> GetByPaymentStatusID(System.Int32? _paymentStatusID, int start, int pageLength)
		{
			int count =  -1;
			return GetByPaymentStatusID(null, _paymentStatusID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeOrderLineItem_ZNodePaymentStatus key.
		///		fKZNodeOrderLineItemZNodePaymentStatus Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_paymentStatusID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.OrderLineItem objects.</returns>
		public TList<OrderLineItem> GetByPaymentStatusID(System.Int32? _paymentStatusID, int start, int pageLength,out int count)
		{
			return GetByPaymentStatusID(null, _paymentStatusID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeOrderLineItem_ZNodePaymentStatus key.
		///		FK_ZNodeOrderLineItem_ZNodePaymentStatus Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_paymentStatusID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.OrderLineItem objects.</returns>
		public abstract TList<OrderLineItem> GetByPaymentStatusID(TransactionManager transactionManager, System.Int32? _paymentStatusID, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.OrderLineItem Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.OrderLineItemKey key, int start, int pageLength)
		{
			return GetByOrderLineItemID(transactionManager, key.OrderLineItemID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeOrderLineItem_OrderID index.
		/// </summary>
		/// <param name="_orderID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;OrderLineItem&gt;"/> class.</returns>
		public TList<OrderLineItem> GetByOrderID(System.Int32 _orderID)
		{
			int count = -1;
			return GetByOrderID(null,_orderID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeOrderLineItem_OrderID index.
		/// </summary>
		/// <param name="_orderID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;OrderLineItem&gt;"/> class.</returns>
		public TList<OrderLineItem> GetByOrderID(System.Int32 _orderID, int start, int pageLength)
		{
			int count = -1;
			return GetByOrderID(null, _orderID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeOrderLineItem_OrderID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_orderID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;OrderLineItem&gt;"/> class.</returns>
		public TList<OrderLineItem> GetByOrderID(TransactionManager transactionManager, System.Int32 _orderID)
		{
			int count = -1;
			return GetByOrderID(transactionManager, _orderID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeOrderLineItem_OrderID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_orderID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;OrderLineItem&gt;"/> class.</returns>
		public TList<OrderLineItem> GetByOrderID(TransactionManager transactionManager, System.Int32 _orderID, int start, int pageLength)
		{
			int count = -1;
			return GetByOrderID(transactionManager, _orderID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeOrderLineItem_OrderID index.
		/// </summary>
		/// <param name="_orderID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;OrderLineItem&gt;"/> class.</returns>
		public TList<OrderLineItem> GetByOrderID(System.Int32 _orderID, int start, int pageLength, out int count)
		{
			return GetByOrderID(null, _orderID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeOrderLineItem_OrderID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_orderID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;OrderLineItem&gt;"/> class.</returns>
		public abstract TList<OrderLineItem> GetByOrderID(TransactionManager transactionManager, System.Int32 _orderID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeOrderLineItem_ProductNum index.
		/// </summary>
		/// <param name="_productNum"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;OrderLineItem&gt;"/> class.</returns>
		public TList<OrderLineItem> GetByProductNum(System.String _productNum)
		{
			int count = -1;
			return GetByProductNum(null,_productNum, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeOrderLineItem_ProductNum index.
		/// </summary>
		/// <param name="_productNum"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;OrderLineItem&gt;"/> class.</returns>
		public TList<OrderLineItem> GetByProductNum(System.String _productNum, int start, int pageLength)
		{
			int count = -1;
			return GetByProductNum(null, _productNum, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeOrderLineItem_ProductNum index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productNum"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;OrderLineItem&gt;"/> class.</returns>
		public TList<OrderLineItem> GetByProductNum(TransactionManager transactionManager, System.String _productNum)
		{
			int count = -1;
			return GetByProductNum(transactionManager, _productNum, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeOrderLineItem_ProductNum index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productNum"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;OrderLineItem&gt;"/> class.</returns>
		public TList<OrderLineItem> GetByProductNum(TransactionManager transactionManager, System.String _productNum, int start, int pageLength)
		{
			int count = -1;
			return GetByProductNum(transactionManager, _productNum, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeOrderLineItem_ProductNum index.
		/// </summary>
		/// <param name="_productNum"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;OrderLineItem&gt;"/> class.</returns>
		public TList<OrderLineItem> GetByProductNum(System.String _productNum, int start, int pageLength, out int count)
		{
			return GetByProductNum(null, _productNum, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeOrderLineItem_ProductNum index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productNum"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;OrderLineItem&gt;"/> class.</returns>
		public abstract TList<OrderLineItem> GetByProductNum(TransactionManager transactionManager, System.String _productNum, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key SC_OrderLineItem_PK index.
		/// </summary>
		/// <param name="_orderLineItemID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.OrderLineItem"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.OrderLineItem GetByOrderLineItemID(System.Int32 _orderLineItemID)
		{
			int count = -1;
			return GetByOrderLineItemID(null,_orderLineItemID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the SC_OrderLineItem_PK index.
		/// </summary>
		/// <param name="_orderLineItemID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.OrderLineItem"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.OrderLineItem GetByOrderLineItemID(System.Int32 _orderLineItemID, int start, int pageLength)
		{
			int count = -1;
			return GetByOrderLineItemID(null, _orderLineItemID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the SC_OrderLineItem_PK index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_orderLineItemID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.OrderLineItem"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.OrderLineItem GetByOrderLineItemID(TransactionManager transactionManager, System.Int32 _orderLineItemID)
		{
			int count = -1;
			return GetByOrderLineItemID(transactionManager, _orderLineItemID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the SC_OrderLineItem_PK index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_orderLineItemID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.OrderLineItem"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.OrderLineItem GetByOrderLineItemID(TransactionManager transactionManager, System.Int32 _orderLineItemID, int start, int pageLength)
		{
			int count = -1;
			return GetByOrderLineItemID(transactionManager, _orderLineItemID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the SC_OrderLineItem_PK index.
		/// </summary>
		/// <param name="_orderLineItemID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.OrderLineItem"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.OrderLineItem GetByOrderLineItemID(System.Int32 _orderLineItemID, int start, int pageLength, out int count)
		{
			return GetByOrderLineItemID(null, _orderLineItemID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the SC_OrderLineItem_PK index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_orderLineItemID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.OrderLineItem"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.OrderLineItem GetByOrderLineItemID(TransactionManager transactionManager, System.Int32 _orderLineItemID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;OrderLineItem&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;OrderLineItem&gt;"/></returns>
		public static TList<OrderLineItem> Fill(IDataReader reader, TList<OrderLineItem> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.OrderLineItem c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("OrderLineItem")
					.Append("|").Append((System.Int32)reader[((int)OrderLineItemColumn.OrderLineItemID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<OrderLineItem>(
					key.ToString(), // EntityTrackingKey
					"OrderLineItem",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.OrderLineItem();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.OrderLineItemID = (System.Int32)reader[((int)OrderLineItemColumn.OrderLineItemID - 1)];
					c.OrderID = (System.Int32)reader[((int)OrderLineItemColumn.OrderID - 1)];
					c.OrderShipmentID = (reader.IsDBNull(((int)OrderLineItemColumn.OrderShipmentID - 1)))?null:(System.Int32?)reader[((int)OrderLineItemColumn.OrderShipmentID - 1)];
					c.ProductNum = (reader.IsDBNull(((int)OrderLineItemColumn.ProductNum - 1)))?null:(System.String)reader[((int)OrderLineItemColumn.ProductNum - 1)];
					c.Name = (reader.IsDBNull(((int)OrderLineItemColumn.Name - 1)))?null:(System.String)reader[((int)OrderLineItemColumn.Name - 1)];
					c.Description = (reader.IsDBNull(((int)OrderLineItemColumn.Description - 1)))?null:(System.String)reader[((int)OrderLineItemColumn.Description - 1)];
					c.Quantity = (reader.IsDBNull(((int)OrderLineItemColumn.Quantity - 1)))?null:(System.Int32?)reader[((int)OrderLineItemColumn.Quantity - 1)];
					c.Price = (reader.IsDBNull(((int)OrderLineItemColumn.Price - 1)))?null:(System.Decimal?)reader[((int)OrderLineItemColumn.Price - 1)];
					c.Weight = (reader.IsDBNull(((int)OrderLineItemColumn.Weight - 1)))?null:(System.Decimal?)reader[((int)OrderLineItemColumn.Weight - 1)];
					c.SKU = (reader.IsDBNull(((int)OrderLineItemColumn.SKU - 1)))?null:(System.String)reader[((int)OrderLineItemColumn.SKU - 1)];
					c.ParentOrderLineItemID = (reader.IsDBNull(((int)OrderLineItemColumn.ParentOrderLineItemID - 1)))?null:(System.Int32?)reader[((int)OrderLineItemColumn.ParentOrderLineItemID - 1)];
					c.OrderLineItemRelationshipTypeID = (reader.IsDBNull(((int)OrderLineItemColumn.OrderLineItemRelationshipTypeID - 1)))?null:(System.Int32?)reader[((int)OrderLineItemColumn.OrderLineItemRelationshipTypeID - 1)];
					c.DownloadLink = (reader.IsDBNull(((int)OrderLineItemColumn.DownloadLink - 1)))?null:(System.String)reader[((int)OrderLineItemColumn.DownloadLink - 1)];
					c.DiscountAmount = (reader.IsDBNull(((int)OrderLineItemColumn.DiscountAmount - 1)))?null:(System.Decimal?)reader[((int)OrderLineItemColumn.DiscountAmount - 1)];
					c.ShipSeparately = (reader.IsDBNull(((int)OrderLineItemColumn.ShipSeparately - 1)))?null:(System.Boolean?)reader[((int)OrderLineItemColumn.ShipSeparately - 1)];
					c.ShipDate = (reader.IsDBNull(((int)OrderLineItemColumn.ShipDate - 1)))?null:(System.DateTime?)reader[((int)OrderLineItemColumn.ShipDate - 1)];
					c.ReturnDate = (reader.IsDBNull(((int)OrderLineItemColumn.ReturnDate - 1)))?null:(System.DateTime?)reader[((int)OrderLineItemColumn.ReturnDate - 1)];
					c.ShippingCost = (reader.IsDBNull(((int)OrderLineItemColumn.ShippingCost - 1)))?null:(System.Decimal?)reader[((int)OrderLineItemColumn.ShippingCost - 1)];
					c.PromoDescription = (reader.IsDBNull(((int)OrderLineItemColumn.PromoDescription - 1)))?null:(System.String)reader[((int)OrderLineItemColumn.PromoDescription - 1)];
					c.SalesTax = (reader.IsDBNull(((int)OrderLineItemColumn.SalesTax - 1)))?null:(System.Decimal?)reader[((int)OrderLineItemColumn.SalesTax - 1)];
					c.VAT = (reader.IsDBNull(((int)OrderLineItemColumn.VAT - 1)))?null:(System.Decimal?)reader[((int)OrderLineItemColumn.VAT - 1)];
					c.GST = (reader.IsDBNull(((int)OrderLineItemColumn.GST - 1)))?null:(System.Decimal?)reader[((int)OrderLineItemColumn.GST - 1)];
					c.PST = (reader.IsDBNull(((int)OrderLineItemColumn.PST - 1)))?null:(System.Decimal?)reader[((int)OrderLineItemColumn.PST - 1)];
					c.HST = (reader.IsDBNull(((int)OrderLineItemColumn.HST - 1)))?null:(System.Decimal?)reader[((int)OrderLineItemColumn.HST - 1)];
					c.TransactionNumber = (reader.IsDBNull(((int)OrderLineItemColumn.TransactionNumber - 1)))?null:(System.String)reader[((int)OrderLineItemColumn.TransactionNumber - 1)];
					c.PaymentStatusID = (reader.IsDBNull(((int)OrderLineItemColumn.PaymentStatusID - 1)))?null:(System.Int32?)reader[((int)OrderLineItemColumn.PaymentStatusID - 1)];
					c.TrackingNumber = (reader.IsDBNull(((int)OrderLineItemColumn.TrackingNumber - 1)))?null:(System.String)reader[((int)OrderLineItemColumn.TrackingNumber - 1)];
					c.AutoGeneratedTracking = (System.Boolean)reader[((int)OrderLineItemColumn.AutoGeneratedTracking - 1)];
					c.Custom1 = (reader.IsDBNull(((int)OrderLineItemColumn.Custom1 - 1)))?null:(System.String)reader[((int)OrderLineItemColumn.Custom1 - 1)];
					c.Custom2 = (reader.IsDBNull(((int)OrderLineItemColumn.Custom2 - 1)))?null:(System.String)reader[((int)OrderLineItemColumn.Custom2 - 1)];
					c.Custom3 = (reader.IsDBNull(((int)OrderLineItemColumn.Custom3 - 1)))?null:(System.String)reader[((int)OrderLineItemColumn.Custom3 - 1)];
					c.OrderLineItemStateID = (reader.IsDBNull(((int)OrderLineItemColumn.OrderLineItemStateID - 1)))?null:(System.Int32?)reader[((int)OrderLineItemColumn.OrderLineItemStateID - 1)];
					c.IsRecurringBilling = (reader.IsDBNull(((int)OrderLineItemColumn.IsRecurringBilling - 1)))?null:(System.Boolean?)reader[((int)OrderLineItemColumn.IsRecurringBilling - 1)];
					c.RecurringBillingPeriod = (reader.IsDBNull(((int)OrderLineItemColumn.RecurringBillingPeriod - 1)))?null:(System.String)reader[((int)OrderLineItemColumn.RecurringBillingPeriod - 1)];
					c.RecurringBillingCycles = (reader.IsDBNull(((int)OrderLineItemColumn.RecurringBillingCycles - 1)))?null:(System.Int32?)reader[((int)OrderLineItemColumn.RecurringBillingCycles - 1)];
					c.RecurringBillingFrequency = (reader.IsDBNull(((int)OrderLineItemColumn.RecurringBillingFrequency - 1)))?null:(System.String)reader[((int)OrderLineItemColumn.RecurringBillingFrequency - 1)];
					c.RecurringBillingAmount = (reader.IsDBNull(((int)OrderLineItemColumn.RecurringBillingAmount - 1)))?null:(System.Decimal?)reader[((int)OrderLineItemColumn.RecurringBillingAmount - 1)];
					c.Created = (reader.IsDBNull(((int)OrderLineItemColumn.Created - 1)))?null:(System.DateTime?)reader[((int)OrderLineItemColumn.Created - 1)];
					c.Modified = (reader.IsDBNull(((int)OrderLineItemColumn.Modified - 1)))?null:(System.DateTime?)reader[((int)OrderLineItemColumn.Modified - 1)];
					c.CreatedBy = (reader.IsDBNull(((int)OrderLineItemColumn.CreatedBy - 1)))?null:(System.String)reader[((int)OrderLineItemColumn.CreatedBy - 1)];
					c.ModifiedBy = (reader.IsDBNull(((int)OrderLineItemColumn.ModifiedBy - 1)))?null:(System.String)reader[((int)OrderLineItemColumn.ModifiedBy - 1)];
					c.AppliedPromo = (reader.IsDBNull(((int)OrderLineItemColumn.AppliedPromo - 1)))?null:(System.String)reader[((int)OrderLineItemColumn.AppliedPromo - 1)];
					c.CouponsApplied = (reader.IsDBNull(((int)OrderLineItemColumn.CouponsApplied - 1)))?null:(System.String)reader[((int)OrderLineItemColumn.CouponsApplied - 1)];
					c.ExternalID = (reader.IsDBNull(((int)OrderLineItemColumn.ExternalID - 1)))?null:(System.String)reader[((int)OrderLineItemColumn.ExternalID - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.OrderLineItem"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.OrderLineItem"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.OrderLineItem entity)
		{
			if (!reader.Read()) return;
			
			entity.OrderLineItemID = (System.Int32)reader[((int)OrderLineItemColumn.OrderLineItemID - 1)];
			entity.OrderID = (System.Int32)reader[((int)OrderLineItemColumn.OrderID - 1)];
			entity.OrderShipmentID = (reader.IsDBNull(((int)OrderLineItemColumn.OrderShipmentID - 1)))?null:(System.Int32?)reader[((int)OrderLineItemColumn.OrderShipmentID - 1)];
			entity.ProductNum = (reader.IsDBNull(((int)OrderLineItemColumn.ProductNum - 1)))?null:(System.String)reader[((int)OrderLineItemColumn.ProductNum - 1)];
			entity.Name = (reader.IsDBNull(((int)OrderLineItemColumn.Name - 1)))?null:(System.String)reader[((int)OrderLineItemColumn.Name - 1)];
			entity.Description = (reader.IsDBNull(((int)OrderLineItemColumn.Description - 1)))?null:(System.String)reader[((int)OrderLineItemColumn.Description - 1)];
			entity.Quantity = (reader.IsDBNull(((int)OrderLineItemColumn.Quantity - 1)))?null:(System.Int32?)reader[((int)OrderLineItemColumn.Quantity - 1)];
			entity.Price = (reader.IsDBNull(((int)OrderLineItemColumn.Price - 1)))?null:(System.Decimal?)reader[((int)OrderLineItemColumn.Price - 1)];
			entity.Weight = (reader.IsDBNull(((int)OrderLineItemColumn.Weight - 1)))?null:(System.Decimal?)reader[((int)OrderLineItemColumn.Weight - 1)];
			entity.SKU = (reader.IsDBNull(((int)OrderLineItemColumn.SKU - 1)))?null:(System.String)reader[((int)OrderLineItemColumn.SKU - 1)];
			entity.ParentOrderLineItemID = (reader.IsDBNull(((int)OrderLineItemColumn.ParentOrderLineItemID - 1)))?null:(System.Int32?)reader[((int)OrderLineItemColumn.ParentOrderLineItemID - 1)];
			entity.OrderLineItemRelationshipTypeID = (reader.IsDBNull(((int)OrderLineItemColumn.OrderLineItemRelationshipTypeID - 1)))?null:(System.Int32?)reader[((int)OrderLineItemColumn.OrderLineItemRelationshipTypeID - 1)];
			entity.DownloadLink = (reader.IsDBNull(((int)OrderLineItemColumn.DownloadLink - 1)))?null:(System.String)reader[((int)OrderLineItemColumn.DownloadLink - 1)];
			entity.DiscountAmount = (reader.IsDBNull(((int)OrderLineItemColumn.DiscountAmount - 1)))?null:(System.Decimal?)reader[((int)OrderLineItemColumn.DiscountAmount - 1)];
			entity.ShipSeparately = (reader.IsDBNull(((int)OrderLineItemColumn.ShipSeparately - 1)))?null:(System.Boolean?)reader[((int)OrderLineItemColumn.ShipSeparately - 1)];
			entity.ShipDate = (reader.IsDBNull(((int)OrderLineItemColumn.ShipDate - 1)))?null:(System.DateTime?)reader[((int)OrderLineItemColumn.ShipDate - 1)];
			entity.ReturnDate = (reader.IsDBNull(((int)OrderLineItemColumn.ReturnDate - 1)))?null:(System.DateTime?)reader[((int)OrderLineItemColumn.ReturnDate - 1)];
			entity.ShippingCost = (reader.IsDBNull(((int)OrderLineItemColumn.ShippingCost - 1)))?null:(System.Decimal?)reader[((int)OrderLineItemColumn.ShippingCost - 1)];
			entity.PromoDescription = (reader.IsDBNull(((int)OrderLineItemColumn.PromoDescription - 1)))?null:(System.String)reader[((int)OrderLineItemColumn.PromoDescription - 1)];
			entity.SalesTax = (reader.IsDBNull(((int)OrderLineItemColumn.SalesTax - 1)))?null:(System.Decimal?)reader[((int)OrderLineItemColumn.SalesTax - 1)];
			entity.VAT = (reader.IsDBNull(((int)OrderLineItemColumn.VAT - 1)))?null:(System.Decimal?)reader[((int)OrderLineItemColumn.VAT - 1)];
			entity.GST = (reader.IsDBNull(((int)OrderLineItemColumn.GST - 1)))?null:(System.Decimal?)reader[((int)OrderLineItemColumn.GST - 1)];
			entity.PST = (reader.IsDBNull(((int)OrderLineItemColumn.PST - 1)))?null:(System.Decimal?)reader[((int)OrderLineItemColumn.PST - 1)];
			entity.HST = (reader.IsDBNull(((int)OrderLineItemColumn.HST - 1)))?null:(System.Decimal?)reader[((int)OrderLineItemColumn.HST - 1)];
			entity.TransactionNumber = (reader.IsDBNull(((int)OrderLineItemColumn.TransactionNumber - 1)))?null:(System.String)reader[((int)OrderLineItemColumn.TransactionNumber - 1)];
			entity.PaymentStatusID = (reader.IsDBNull(((int)OrderLineItemColumn.PaymentStatusID - 1)))?null:(System.Int32?)reader[((int)OrderLineItemColumn.PaymentStatusID - 1)];
			entity.TrackingNumber = (reader.IsDBNull(((int)OrderLineItemColumn.TrackingNumber - 1)))?null:(System.String)reader[((int)OrderLineItemColumn.TrackingNumber - 1)];
			entity.AutoGeneratedTracking = (System.Boolean)reader[((int)OrderLineItemColumn.AutoGeneratedTracking - 1)];
			entity.Custom1 = (reader.IsDBNull(((int)OrderLineItemColumn.Custom1 - 1)))?null:(System.String)reader[((int)OrderLineItemColumn.Custom1 - 1)];
			entity.Custom2 = (reader.IsDBNull(((int)OrderLineItemColumn.Custom2 - 1)))?null:(System.String)reader[((int)OrderLineItemColumn.Custom2 - 1)];
			entity.Custom3 = (reader.IsDBNull(((int)OrderLineItemColumn.Custom3 - 1)))?null:(System.String)reader[((int)OrderLineItemColumn.Custom3 - 1)];
			entity.OrderLineItemStateID = (reader.IsDBNull(((int)OrderLineItemColumn.OrderLineItemStateID - 1)))?null:(System.Int32?)reader[((int)OrderLineItemColumn.OrderLineItemStateID - 1)];
			entity.IsRecurringBilling = (reader.IsDBNull(((int)OrderLineItemColumn.IsRecurringBilling - 1)))?null:(System.Boolean?)reader[((int)OrderLineItemColumn.IsRecurringBilling - 1)];
			entity.RecurringBillingPeriod = (reader.IsDBNull(((int)OrderLineItemColumn.RecurringBillingPeriod - 1)))?null:(System.String)reader[((int)OrderLineItemColumn.RecurringBillingPeriod - 1)];
			entity.RecurringBillingCycles = (reader.IsDBNull(((int)OrderLineItemColumn.RecurringBillingCycles - 1)))?null:(System.Int32?)reader[((int)OrderLineItemColumn.RecurringBillingCycles - 1)];
			entity.RecurringBillingFrequency = (reader.IsDBNull(((int)OrderLineItemColumn.RecurringBillingFrequency - 1)))?null:(System.String)reader[((int)OrderLineItemColumn.RecurringBillingFrequency - 1)];
			entity.RecurringBillingAmount = (reader.IsDBNull(((int)OrderLineItemColumn.RecurringBillingAmount - 1)))?null:(System.Decimal?)reader[((int)OrderLineItemColumn.RecurringBillingAmount - 1)];
			entity.Created = (reader.IsDBNull(((int)OrderLineItemColumn.Created - 1)))?null:(System.DateTime?)reader[((int)OrderLineItemColumn.Created - 1)];
			entity.Modified = (reader.IsDBNull(((int)OrderLineItemColumn.Modified - 1)))?null:(System.DateTime?)reader[((int)OrderLineItemColumn.Modified - 1)];
			entity.CreatedBy = (reader.IsDBNull(((int)OrderLineItemColumn.CreatedBy - 1)))?null:(System.String)reader[((int)OrderLineItemColumn.CreatedBy - 1)];
			entity.ModifiedBy = (reader.IsDBNull(((int)OrderLineItemColumn.ModifiedBy - 1)))?null:(System.String)reader[((int)OrderLineItemColumn.ModifiedBy - 1)];
			entity.AppliedPromo = (reader.IsDBNull(((int)OrderLineItemColumn.AppliedPromo - 1)))?null:(System.String)reader[((int)OrderLineItemColumn.AppliedPromo - 1)];
			entity.CouponsApplied = (reader.IsDBNull(((int)OrderLineItemColumn.CouponsApplied - 1)))?null:(System.String)reader[((int)OrderLineItemColumn.CouponsApplied - 1)];
			entity.ExternalID = (reader.IsDBNull(((int)OrderLineItemColumn.ExternalID - 1)))?null:(System.String)reader[((int)OrderLineItemColumn.ExternalID - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.OrderLineItem"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.OrderLineItem"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.OrderLineItem entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.OrderLineItemID = (System.Int32)dataRow["OrderLineItemID"];
			entity.OrderID = (System.Int32)dataRow["OrderID"];
			entity.OrderShipmentID = Convert.IsDBNull(dataRow["OrderShipmentID"]) ? null : (System.Int32?)dataRow["OrderShipmentID"];
			entity.ProductNum = Convert.IsDBNull(dataRow["ProductNum"]) ? null : (System.String)dataRow["ProductNum"];
			entity.Name = Convert.IsDBNull(dataRow["Name"]) ? null : (System.String)dataRow["Name"];
			entity.Description = Convert.IsDBNull(dataRow["Description"]) ? null : (System.String)dataRow["Description"];
			entity.Quantity = Convert.IsDBNull(dataRow["Quantity"]) ? null : (System.Int32?)dataRow["Quantity"];
			entity.Price = Convert.IsDBNull(dataRow["Price"]) ? null : (System.Decimal?)dataRow["Price"];
			entity.Weight = Convert.IsDBNull(dataRow["Weight"]) ? null : (System.Decimal?)dataRow["Weight"];
			entity.SKU = Convert.IsDBNull(dataRow["SKU"]) ? null : (System.String)dataRow["SKU"];
			entity.ParentOrderLineItemID = Convert.IsDBNull(dataRow["ParentOrderLineItemID"]) ? null : (System.Int32?)dataRow["ParentOrderLineItemID"];
			entity.OrderLineItemRelationshipTypeID = Convert.IsDBNull(dataRow["OrderLineItemRelationshipTypeID"]) ? null : (System.Int32?)dataRow["OrderLineItemRelationshipTypeID"];
			entity.DownloadLink = Convert.IsDBNull(dataRow["DownloadLink"]) ? null : (System.String)dataRow["DownloadLink"];
			entity.DiscountAmount = Convert.IsDBNull(dataRow["DiscountAmount"]) ? null : (System.Decimal?)dataRow["DiscountAmount"];
			entity.ShipSeparately = Convert.IsDBNull(dataRow["ShipSeparately"]) ? null : (System.Boolean?)dataRow["ShipSeparately"];
			entity.ShipDate = Convert.IsDBNull(dataRow["ShipDate"]) ? null : (System.DateTime?)dataRow["ShipDate"];
			entity.ReturnDate = Convert.IsDBNull(dataRow["ReturnDate"]) ? null : (System.DateTime?)dataRow["ReturnDate"];
			entity.ShippingCost = Convert.IsDBNull(dataRow["ShippingCost"]) ? null : (System.Decimal?)dataRow["ShippingCost"];
			entity.PromoDescription = Convert.IsDBNull(dataRow["PromoDescription"]) ? null : (System.String)dataRow["PromoDescription"];
			entity.SalesTax = Convert.IsDBNull(dataRow["SalesTax"]) ? null : (System.Decimal?)dataRow["SalesTax"];
			entity.VAT = Convert.IsDBNull(dataRow["VAT"]) ? null : (System.Decimal?)dataRow["VAT"];
			entity.GST = Convert.IsDBNull(dataRow["GST"]) ? null : (System.Decimal?)dataRow["GST"];
			entity.PST = Convert.IsDBNull(dataRow["PST"]) ? null : (System.Decimal?)dataRow["PST"];
			entity.HST = Convert.IsDBNull(dataRow["HST"]) ? null : (System.Decimal?)dataRow["HST"];
			entity.TransactionNumber = Convert.IsDBNull(dataRow["TransactionNumber"]) ? null : (System.String)dataRow["TransactionNumber"];
			entity.PaymentStatusID = Convert.IsDBNull(dataRow["PaymentStatusID"]) ? null : (System.Int32?)dataRow["PaymentStatusID"];
			entity.TrackingNumber = Convert.IsDBNull(dataRow["TrackingNumber"]) ? null : (System.String)dataRow["TrackingNumber"];
			entity.AutoGeneratedTracking = (System.Boolean)dataRow["AutoGeneratedTracking"];
			entity.Custom1 = Convert.IsDBNull(dataRow["Custom1"]) ? null : (System.String)dataRow["Custom1"];
			entity.Custom2 = Convert.IsDBNull(dataRow["Custom2"]) ? null : (System.String)dataRow["Custom2"];
			entity.Custom3 = Convert.IsDBNull(dataRow["Custom3"]) ? null : (System.String)dataRow["Custom3"];
			entity.OrderLineItemStateID = Convert.IsDBNull(dataRow["OrderLineItemStateID"]) ? null : (System.Int32?)dataRow["OrderLineItemStateID"];
			entity.IsRecurringBilling = Convert.IsDBNull(dataRow["IsRecurringBilling"]) ? null : (System.Boolean?)dataRow["IsRecurringBilling"];
			entity.RecurringBillingPeriod = Convert.IsDBNull(dataRow["RecurringBillingPeriod"]) ? null : (System.String)dataRow["RecurringBillingPeriod"];
			entity.RecurringBillingCycles = Convert.IsDBNull(dataRow["RecurringBillingCycles"]) ? null : (System.Int32?)dataRow["RecurringBillingCycles"];
			entity.RecurringBillingFrequency = Convert.IsDBNull(dataRow["RecurringBillingFrequency"]) ? null : (System.String)dataRow["RecurringBillingFrequency"];
			entity.RecurringBillingAmount = Convert.IsDBNull(dataRow["RecurringBillingAmount"]) ? null : (System.Decimal?)dataRow["RecurringBillingAmount"];
			entity.Created = Convert.IsDBNull(dataRow["Created"]) ? null : (System.DateTime?)dataRow["Created"];
			entity.Modified = Convert.IsDBNull(dataRow["Modified"]) ? null : (System.DateTime?)dataRow["Modified"];
			entity.CreatedBy = Convert.IsDBNull(dataRow["CreatedBy"]) ? null : (System.String)dataRow["CreatedBy"];
			entity.ModifiedBy = Convert.IsDBNull(dataRow["ModifiedBy"]) ? null : (System.String)dataRow["ModifiedBy"];
			entity.AppliedPromo = Convert.IsDBNull(dataRow["AppliedPromo"]) ? null : (System.String)dataRow["AppliedPromo"];
			entity.CouponsApplied = Convert.IsDBNull(dataRow["CouponsApplied"]) ? null : (System.String)dataRow["CouponsApplied"];
			entity.ExternalID = Convert.IsDBNull(dataRow["ExternalID"]) ? null : (System.String)dataRow["ExternalID"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.OrderLineItem"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.OrderLineItem Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.OrderLineItem entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region OrderIDSource	
			if (CanDeepLoad(entity, "Order|OrderIDSource", deepLoadType, innerList) 
				&& entity.OrderIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.OrderID;
				Order tmpEntity = EntityManager.LocateEntity<Order>(EntityLocator.ConstructKeyFromPkItems(typeof(Order), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.OrderIDSource = tmpEntity;
				else
					entity.OrderIDSource = DataRepository.OrderProvider.GetByOrderID(transactionManager, entity.OrderID);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'OrderIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.OrderIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.OrderProvider.DeepLoad(transactionManager, entity.OrderIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion OrderIDSource

			#region ParentOrderLineItemIDSource	
			if (CanDeepLoad(entity, "OrderLineItem|ParentOrderLineItemIDSource", deepLoadType, innerList) 
				&& entity.ParentOrderLineItemIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.ParentOrderLineItemID ?? (int)0);
				OrderLineItem tmpEntity = EntityManager.LocateEntity<OrderLineItem>(EntityLocator.ConstructKeyFromPkItems(typeof(OrderLineItem), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ParentOrderLineItemIDSource = tmpEntity;
				else
					entity.ParentOrderLineItemIDSource = DataRepository.OrderLineItemProvider.GetByOrderLineItemID(transactionManager, (entity.ParentOrderLineItemID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ParentOrderLineItemIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ParentOrderLineItemIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.OrderLineItemProvider.DeepLoad(transactionManager, entity.ParentOrderLineItemIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ParentOrderLineItemIDSource

			#region OrderLineItemRelationshipTypeIDSource	
			if (CanDeepLoad(entity, "OrderLineItemRelationshipType|OrderLineItemRelationshipTypeIDSource", deepLoadType, innerList) 
				&& entity.OrderLineItemRelationshipTypeIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.OrderLineItemRelationshipTypeID ?? (int)0);
				OrderLineItemRelationshipType tmpEntity = EntityManager.LocateEntity<OrderLineItemRelationshipType>(EntityLocator.ConstructKeyFromPkItems(typeof(OrderLineItemRelationshipType), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.OrderLineItemRelationshipTypeIDSource = tmpEntity;
				else
					entity.OrderLineItemRelationshipTypeIDSource = DataRepository.OrderLineItemRelationshipTypeProvider.GetByOrderLineItemRelationshipTypeID(transactionManager, (entity.OrderLineItemRelationshipTypeID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'OrderLineItemRelationshipTypeIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.OrderLineItemRelationshipTypeIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.OrderLineItemRelationshipTypeProvider.DeepLoad(transactionManager, entity.OrderLineItemRelationshipTypeIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion OrderLineItemRelationshipTypeIDSource

			#region OrderShipmentIDSource	
			if (CanDeepLoad(entity, "OrderShipment|OrderShipmentIDSource", deepLoadType, innerList) 
				&& entity.OrderShipmentIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.OrderShipmentID ?? (int)0);
				OrderShipment tmpEntity = EntityManager.LocateEntity<OrderShipment>(EntityLocator.ConstructKeyFromPkItems(typeof(OrderShipment), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.OrderShipmentIDSource = tmpEntity;
				else
					entity.OrderShipmentIDSource = DataRepository.OrderShipmentProvider.GetByOrderShipmentID(transactionManager, (entity.OrderShipmentID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'OrderShipmentIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.OrderShipmentIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.OrderShipmentProvider.DeepLoad(transactionManager, entity.OrderShipmentIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion OrderShipmentIDSource

			#region OrderLineItemStateIDSource	
			if (CanDeepLoad(entity, "OrderState|OrderLineItemStateIDSource", deepLoadType, innerList) 
				&& entity.OrderLineItemStateIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.OrderLineItemStateID ?? (int)0);
				OrderState tmpEntity = EntityManager.LocateEntity<OrderState>(EntityLocator.ConstructKeyFromPkItems(typeof(OrderState), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.OrderLineItemStateIDSource = tmpEntity;
				else
					entity.OrderLineItemStateIDSource = DataRepository.OrderStateProvider.GetByOrderStateID(transactionManager, (entity.OrderLineItemStateID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'OrderLineItemStateIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.OrderLineItemStateIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.OrderStateProvider.DeepLoad(transactionManager, entity.OrderLineItemStateIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion OrderLineItemStateIDSource

			#region PaymentStatusIDSource	
			if (CanDeepLoad(entity, "PaymentStatus|PaymentStatusIDSource", deepLoadType, innerList) 
				&& entity.PaymentStatusIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.PaymentStatusID ?? (int)0);
				PaymentStatus tmpEntity = EntityManager.LocateEntity<PaymentStatus>(EntityLocator.ConstructKeyFromPkItems(typeof(PaymentStatus), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.PaymentStatusIDSource = tmpEntity;
				else
					entity.PaymentStatusIDSource = DataRepository.PaymentStatusProvider.GetByPaymentStatusID(transactionManager, (entity.PaymentStatusID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PaymentStatusIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.PaymentStatusIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.PaymentStatusProvider.DeepLoad(transactionManager, entity.PaymentStatusIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion PaymentStatusIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByOrderLineItemID methods when available
			
			#region GiftCardCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<GiftCard>|GiftCardCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'GiftCardCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.GiftCardCollection = DataRepository.GiftCardProvider.GetByOrderItemId(transactionManager, entity.OrderLineItemID);

				if (deep && entity.GiftCardCollection.Count > 0)
				{
					deepHandles.Add("GiftCardCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<GiftCard>) DataRepository.GiftCardProvider.DeepLoad,
						new object[] { transactionManager, entity.GiftCardCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region DigitalAssetCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<DigitalAsset>|DigitalAssetCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'DigitalAssetCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.DigitalAssetCollection = DataRepository.DigitalAssetProvider.GetByOrderLineItemID(transactionManager, entity.OrderLineItemID);

				if (deep && entity.DigitalAssetCollection.Count > 0)
				{
					deepHandles.Add("DigitalAssetCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<DigitalAsset>) DataRepository.DigitalAssetProvider.DeepLoad,
						new object[] { transactionManager, entity.DigitalAssetCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region OrderLineItemCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<OrderLineItem>|OrderLineItemCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'OrderLineItemCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.OrderLineItemCollection = DataRepository.OrderLineItemProvider.GetByParentOrderLineItemID(transactionManager, entity.OrderLineItemID);

				if (deep && entity.OrderLineItemCollection.Count > 0)
				{
					deepHandles.Add("OrderLineItemCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<OrderLineItem>) DataRepository.OrderLineItemProvider.DeepLoad,
						new object[] { transactionManager, entity.OrderLineItemCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region RMARequestItemCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<RMARequestItem>|RMARequestItemCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'RMARequestItemCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.RMARequestItemCollection = DataRepository.RMARequestItemProvider.GetByOrderLineItemID(transactionManager, entity.OrderLineItemID);

				if (deep && entity.RMARequestItemCollection.Count > 0)
				{
					deepHandles.Add("RMARequestItemCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<RMARequestItem>) DataRepository.RMARequestItemProvider.DeepLoad,
						new object[] { transactionManager, entity.RMARequestItemCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.OrderLineItem object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.OrderLineItem instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.OrderLineItem Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.OrderLineItem entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region OrderIDSource
			if (CanDeepSave(entity, "Order|OrderIDSource", deepSaveType, innerList) 
				&& entity.OrderIDSource != null)
			{
				DataRepository.OrderProvider.Save(transactionManager, entity.OrderIDSource);
				entity.OrderID = entity.OrderIDSource.OrderID;
			}
			#endregion 
			
			#region ParentOrderLineItemIDSource
			if (CanDeepSave(entity, "OrderLineItem|ParentOrderLineItemIDSource", deepSaveType, innerList) 
				&& entity.ParentOrderLineItemIDSource != null)
			{
				DataRepository.OrderLineItemProvider.Save(transactionManager, entity.ParentOrderLineItemIDSource);
				entity.ParentOrderLineItemID = entity.ParentOrderLineItemIDSource.OrderLineItemID;
			}
			#endregion 
			
			#region OrderLineItemRelationshipTypeIDSource
			if (CanDeepSave(entity, "OrderLineItemRelationshipType|OrderLineItemRelationshipTypeIDSource", deepSaveType, innerList) 
				&& entity.OrderLineItemRelationshipTypeIDSource != null)
			{
				DataRepository.OrderLineItemRelationshipTypeProvider.Save(transactionManager, entity.OrderLineItemRelationshipTypeIDSource);
				entity.OrderLineItemRelationshipTypeID = entity.OrderLineItemRelationshipTypeIDSource.OrderLineItemRelationshipTypeID;
			}
			#endregion 
			
			#region OrderShipmentIDSource
			if (CanDeepSave(entity, "OrderShipment|OrderShipmentIDSource", deepSaveType, innerList) 
				&& entity.OrderShipmentIDSource != null)
			{
				DataRepository.OrderShipmentProvider.Save(transactionManager, entity.OrderShipmentIDSource);
				entity.OrderShipmentID = entity.OrderShipmentIDSource.OrderShipmentID;
			}
			#endregion 
			
			#region OrderLineItemStateIDSource
			if (CanDeepSave(entity, "OrderState|OrderLineItemStateIDSource", deepSaveType, innerList) 
				&& entity.OrderLineItemStateIDSource != null)
			{
				DataRepository.OrderStateProvider.Save(transactionManager, entity.OrderLineItemStateIDSource);
				entity.OrderLineItemStateID = entity.OrderLineItemStateIDSource.OrderStateID;
			}
			#endregion 
			
			#region PaymentStatusIDSource
			if (CanDeepSave(entity, "PaymentStatus|PaymentStatusIDSource", deepSaveType, innerList) 
				&& entity.PaymentStatusIDSource != null)
			{
				DataRepository.PaymentStatusProvider.Save(transactionManager, entity.PaymentStatusIDSource);
				entity.PaymentStatusID = entity.PaymentStatusIDSource.PaymentStatusID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<GiftCard>
				if (CanDeepSave(entity.GiftCardCollection, "List<GiftCard>|GiftCardCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(GiftCard child in entity.GiftCardCollection)
					{
						if(child.OrderItemIdSource != null)
						{
							child.OrderItemId = child.OrderItemIdSource.OrderLineItemID;
						}
						else
						{
							child.OrderItemId = entity.OrderLineItemID;
						}

					}

					if (entity.GiftCardCollection.Count > 0 || entity.GiftCardCollection.DeletedItems.Count > 0)
					{
						//DataRepository.GiftCardProvider.Save(transactionManager, entity.GiftCardCollection);
						
						deepHandles.Add("GiftCardCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< GiftCard >) DataRepository.GiftCardProvider.DeepSave,
							new object[] { transactionManager, entity.GiftCardCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<DigitalAsset>
				if (CanDeepSave(entity.DigitalAssetCollection, "List<DigitalAsset>|DigitalAssetCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(DigitalAsset child in entity.DigitalAssetCollection)
					{
						if(child.OrderLineItemIDSource != null)
						{
							child.OrderLineItemID = child.OrderLineItemIDSource.OrderLineItemID;
						}
						else
						{
							child.OrderLineItemID = entity.OrderLineItemID;
						}

					}

					if (entity.DigitalAssetCollection.Count > 0 || entity.DigitalAssetCollection.DeletedItems.Count > 0)
					{
						//DataRepository.DigitalAssetProvider.Save(transactionManager, entity.DigitalAssetCollection);
						
						deepHandles.Add("DigitalAssetCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< DigitalAsset >) DataRepository.DigitalAssetProvider.DeepSave,
							new object[] { transactionManager, entity.DigitalAssetCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<OrderLineItem>
				if (CanDeepSave(entity.OrderLineItemCollection, "List<OrderLineItem>|OrderLineItemCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(OrderLineItem child in entity.OrderLineItemCollection)
					{
						if(child.ParentOrderLineItemIDSource != null)
						{
							child.ParentOrderLineItemID = child.ParentOrderLineItemIDSource.OrderLineItemID;
						}
						else
						{
							child.ParentOrderLineItemID = entity.OrderLineItemID;
						}

					}

					if (entity.OrderLineItemCollection.Count > 0 || entity.OrderLineItemCollection.DeletedItems.Count > 0)
					{
						//DataRepository.OrderLineItemProvider.Save(transactionManager, entity.OrderLineItemCollection);
						
						deepHandles.Add("OrderLineItemCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< OrderLineItem >) DataRepository.OrderLineItemProvider.DeepSave,
							new object[] { transactionManager, entity.OrderLineItemCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<RMARequestItem>
				if (CanDeepSave(entity.RMARequestItemCollection, "List<RMARequestItem>|RMARequestItemCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(RMARequestItem child in entity.RMARequestItemCollection)
					{
						if(child.OrderLineItemIDSource != null)
						{
							child.OrderLineItemID = child.OrderLineItemIDSource.OrderLineItemID;
						}
						else
						{
							child.OrderLineItemID = entity.OrderLineItemID;
						}

					}

					if (entity.RMARequestItemCollection.Count > 0 || entity.RMARequestItemCollection.DeletedItems.Count > 0)
					{
						//DataRepository.RMARequestItemProvider.Save(transactionManager, entity.RMARequestItemCollection);
						
						deepHandles.Add("RMARequestItemCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< RMARequestItem >) DataRepository.RMARequestItemProvider.DeepSave,
							new object[] { transactionManager, entity.RMARequestItemCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region OrderLineItemChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.OrderLineItem</c>
	///</summary>
	public enum OrderLineItemChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Order</c> at OrderIDSource
		///</summary>
		[ChildEntityType(typeof(Order))]
		Order,
		
		///<summary>
		/// Composite Property for <c>OrderLineItem</c> at ParentOrderLineItemIDSource
		///</summary>
		[ChildEntityType(typeof(OrderLineItem))]
		OrderLineItem,
		
		///<summary>
		/// Composite Property for <c>OrderLineItemRelationshipType</c> at OrderLineItemRelationshipTypeIDSource
		///</summary>
		[ChildEntityType(typeof(OrderLineItemRelationshipType))]
		OrderLineItemRelationshipType,
		
		///<summary>
		/// Composite Property for <c>OrderShipment</c> at OrderShipmentIDSource
		///</summary>
		[ChildEntityType(typeof(OrderShipment))]
		OrderShipment,
		
		///<summary>
		/// Composite Property for <c>OrderState</c> at OrderLineItemStateIDSource
		///</summary>
		[ChildEntityType(typeof(OrderState))]
		OrderState,
		
		///<summary>
		/// Composite Property for <c>PaymentStatus</c> at PaymentStatusIDSource
		///</summary>
		[ChildEntityType(typeof(PaymentStatus))]
		PaymentStatus,
		///<summary>
		/// Collection of <c>OrderLineItem</c> as OneToMany for GiftCardCollection
		///</summary>
		[ChildEntityType(typeof(TList<GiftCard>))]
		GiftCardCollection,
		///<summary>
		/// Collection of <c>OrderLineItem</c> as OneToMany for DigitalAssetCollection
		///</summary>
		[ChildEntityType(typeof(TList<DigitalAsset>))]
		DigitalAssetCollection,
		///<summary>
		/// Collection of <c>OrderLineItem</c> as OneToMany for OrderLineItemCollection
		///</summary>
		[ChildEntityType(typeof(TList<OrderLineItem>))]
		OrderLineItemCollection,
		///<summary>
		/// Collection of <c>OrderLineItem</c> as OneToMany for RMARequestItemCollection
		///</summary>
		[ChildEntityType(typeof(TList<RMARequestItem>))]
		RMARequestItemCollection,
	}
	
	#endregion OrderLineItemChildEntityTypes
	
	#region OrderLineItemFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;OrderLineItemColumn&gt;"/> class
	/// that is used exclusively with a <see cref="OrderLineItem"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class OrderLineItemFilterBuilder : SqlFilterBuilder<OrderLineItemColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OrderLineItemFilterBuilder class.
		/// </summary>
		public OrderLineItemFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the OrderLineItemFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public OrderLineItemFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the OrderLineItemFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public OrderLineItemFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion OrderLineItemFilterBuilder
	
	#region OrderLineItemParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;OrderLineItemColumn&gt;"/> class
	/// that is used exclusively with a <see cref="OrderLineItem"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class OrderLineItemParameterBuilder : ParameterizedSqlFilterBuilder<OrderLineItemColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OrderLineItemParameterBuilder class.
		/// </summary>
		public OrderLineItemParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the OrderLineItemParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public OrderLineItemParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the OrderLineItemParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public OrderLineItemParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion OrderLineItemParameterBuilder
	
	#region OrderLineItemSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;OrderLineItemColumn&gt;"/> class
	/// that is used exclusively with a <see cref="OrderLineItem"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class OrderLineItemSortBuilder : SqlSortBuilder<OrderLineItemColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OrderLineItemSqlSortBuilder class.
		/// </summary>
		public OrderLineItemSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion OrderLineItemSortBuilder
	
} // end namespace
