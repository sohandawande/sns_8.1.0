﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="SourceModificationTypeProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class SourceModificationTypeProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.SourceModificationType, ZNode.Libraries.DataAccess.Entities.SourceModificationTypeKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.SourceModificationTypeKey key)
		{
			return Delete(transactionManager, key.Id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _id)
		{
			return Delete(null, _id);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _id);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.SourceModificationType Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.SourceModificationTypeKey key, int start, int pageLength)
		{
			return GetById(transactionManager, key.Id, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZnodeSourceModificationType index.
		/// </summary>
		/// <param name="_id"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SourceModificationType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.SourceModificationType GetById(System.Int32 _id)
		{
			int count = -1;
			return GetById(null,_id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZnodeSourceModificationType index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SourceModificationType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.SourceModificationType GetById(System.Int32 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(null, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZnodeSourceModificationType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SourceModificationType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.SourceModificationType GetById(TransactionManager transactionManager, System.Int32 _id)
		{
			int count = -1;
			return GetById(transactionManager, _id, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZnodeSourceModificationType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SourceModificationType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.SourceModificationType GetById(TransactionManager transactionManager, System.Int32 _id, int start, int pageLength)
		{
			int count = -1;
			return GetById(transactionManager, _id, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZnodeSourceModificationType index.
		/// </summary>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SourceModificationType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.SourceModificationType GetById(System.Int32 _id, int start, int pageLength, out int count)
		{
			return GetById(null, _id, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZnodeSourceModificationType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_id"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SourceModificationType"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.SourceModificationType GetById(TransactionManager transactionManager, System.Int32 _id, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;SourceModificationType&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;SourceModificationType&gt;"/></returns>
		public static TList<SourceModificationType> Fill(IDataReader reader, TList<SourceModificationType> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.SourceModificationType c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("SourceModificationType")
					.Append("|").Append((System.Int32)reader[((int)SourceModificationTypeColumn.Id - 1)]).ToString();
					c = EntityManager.LocateOrCreate<SourceModificationType>(
					key.ToString(), // EntityTrackingKey
					"SourceModificationType",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.SourceModificationType();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Id = (System.Int32)reader[((int)SourceModificationTypeColumn.Id - 1)];
					c.ModificationType = (System.String)reader[((int)SourceModificationTypeColumn.ModificationType - 1)];
					c.CreatedDate = (System.DateTime)reader[((int)SourceModificationTypeColumn.CreatedDate - 1)];
					c.CreatedBy = (System.String)reader[((int)SourceModificationTypeColumn.CreatedBy - 1)];
					c.UpdatedDate = (reader.IsDBNull(((int)SourceModificationTypeColumn.UpdatedDate - 1)))?null:(System.DateTime?)reader[((int)SourceModificationTypeColumn.UpdatedDate - 1)];
					c.UpdatedBy = (reader.IsDBNull(((int)SourceModificationTypeColumn.UpdatedBy - 1)))?null:(System.String)reader[((int)SourceModificationTypeColumn.UpdatedBy - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.SourceModificationType"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.SourceModificationType"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.SourceModificationType entity)
		{
			if (!reader.Read()) return;
			
			entity.Id = (System.Int32)reader[((int)SourceModificationTypeColumn.Id - 1)];
			entity.ModificationType = (System.String)reader[((int)SourceModificationTypeColumn.ModificationType - 1)];
			entity.CreatedDate = (System.DateTime)reader[((int)SourceModificationTypeColumn.CreatedDate - 1)];
			entity.CreatedBy = (System.String)reader[((int)SourceModificationTypeColumn.CreatedBy - 1)];
			entity.UpdatedDate = (reader.IsDBNull(((int)SourceModificationTypeColumn.UpdatedDate - 1)))?null:(System.DateTime?)reader[((int)SourceModificationTypeColumn.UpdatedDate - 1)];
			entity.UpdatedBy = (reader.IsDBNull(((int)SourceModificationTypeColumn.UpdatedBy - 1)))?null:(System.String)reader[((int)SourceModificationTypeColumn.UpdatedBy - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.SourceModificationType"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.SourceModificationType"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.SourceModificationType entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Id = (System.Int32)dataRow["Id"];
			entity.ModificationType = (System.String)dataRow["ModificationType"];
			entity.CreatedDate = (System.DateTime)dataRow["CreatedDate"];
			entity.CreatedBy = (System.String)dataRow["CreatedBy"];
			entity.UpdatedDate = Convert.IsDBNull(dataRow["UpdatedDate"]) ? null : (System.DateTime?)dataRow["UpdatedDate"];
			entity.UpdatedBy = Convert.IsDBNull(dataRow["UpdatedBy"]) ? null : (System.String)dataRow["UpdatedBy"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.SourceModificationType"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.SourceModificationType Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.SourceModificationType entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetById methods when available
			
			#region SourceModificationAuditCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<SourceModificationAudit>|SourceModificationAuditCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SourceModificationAuditCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.SourceModificationAuditCollection = DataRepository.SourceModificationAuditProvider.GetByModificationTypeId(transactionManager, entity.Id);

				if (deep && entity.SourceModificationAuditCollection.Count > 0)
				{
					deepHandles.Add("SourceModificationAuditCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<SourceModificationAudit>) DataRepository.SourceModificationAuditProvider.DeepLoad,
						new object[] { transactionManager, entity.SourceModificationAuditCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.SourceModificationType object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.SourceModificationType instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.SourceModificationType Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.SourceModificationType entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<SourceModificationAudit>
				if (CanDeepSave(entity.SourceModificationAuditCollection, "List<SourceModificationAudit>|SourceModificationAuditCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(SourceModificationAudit child in entity.SourceModificationAuditCollection)
					{
						if(child.ModificationTypeIdSource != null)
						{
							child.ModificationTypeId = child.ModificationTypeIdSource.Id;
						}
						else
						{
							child.ModificationTypeId = entity.Id;
						}

					}

					if (entity.SourceModificationAuditCollection.Count > 0 || entity.SourceModificationAuditCollection.DeletedItems.Count > 0)
					{
						//DataRepository.SourceModificationAuditProvider.Save(transactionManager, entity.SourceModificationAuditCollection);
						
						deepHandles.Add("SourceModificationAuditCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< SourceModificationAudit >) DataRepository.SourceModificationAuditProvider.DeepSave,
							new object[] { transactionManager, entity.SourceModificationAuditCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region SourceModificationTypeChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.SourceModificationType</c>
	///</summary>
	public enum SourceModificationTypeChildEntityTypes
	{
		///<summary>
		/// Collection of <c>SourceModificationType</c> as OneToMany for SourceModificationAuditCollection
		///</summary>
		[ChildEntityType(typeof(TList<SourceModificationAudit>))]
		SourceModificationAuditCollection,
	}
	
	#endregion SourceModificationTypeChildEntityTypes
	
	#region SourceModificationTypeFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;SourceModificationTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SourceModificationType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SourceModificationTypeFilterBuilder : SqlFilterBuilder<SourceModificationTypeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SourceModificationTypeFilterBuilder class.
		/// </summary>
		public SourceModificationTypeFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SourceModificationTypeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SourceModificationTypeFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SourceModificationTypeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SourceModificationTypeFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SourceModificationTypeFilterBuilder
	
	#region SourceModificationTypeParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;SourceModificationTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SourceModificationType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SourceModificationTypeParameterBuilder : ParameterizedSqlFilterBuilder<SourceModificationTypeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SourceModificationTypeParameterBuilder class.
		/// </summary>
		public SourceModificationTypeParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SourceModificationTypeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SourceModificationTypeParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SourceModificationTypeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SourceModificationTypeParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SourceModificationTypeParameterBuilder
	
	#region SourceModificationTypeSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;SourceModificationTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SourceModificationType"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class SourceModificationTypeSortBuilder : SqlSortBuilder<SourceModificationTypeColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SourceModificationTypeSqlSortBuilder class.
		/// </summary>
		public SourceModificationTypeSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion SourceModificationTypeSortBuilder
	
} // end namespace
