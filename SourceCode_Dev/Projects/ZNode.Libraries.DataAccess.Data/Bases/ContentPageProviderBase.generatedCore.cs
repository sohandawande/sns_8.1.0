﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ContentPageProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class ContentPageProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.ContentPage, ZNode.Libraries.DataAccess.Entities.ContentPageKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ContentPageKey key)
		{
			return Delete(transactionManager, key.ContentPageID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_contentPageID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _contentPageID)
		{
			return Delete(null, _contentPageID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_contentPageID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _contentPageID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeCont__CSSID__4D2051A6 key.
		///		FK__ZNodeCont__CSSID__4D2051A6 Description: 
		/// </summary>
		/// <param name="_cSSID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ContentPage objects.</returns>
		public TList<ContentPage> GetByCSSID(System.Int32? _cSSID)
		{
			int count = -1;
			return GetByCSSID(_cSSID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeCont__CSSID__4D2051A6 key.
		///		FK__ZNodeCont__CSSID__4D2051A6 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_cSSID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ContentPage objects.</returns>
		/// <remarks></remarks>
		public TList<ContentPage> GetByCSSID(TransactionManager transactionManager, System.Int32? _cSSID)
		{
			int count = -1;
			return GetByCSSID(transactionManager, _cSSID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeCont__CSSID__4D2051A6 key.
		///		FK__ZNodeCont__CSSID__4D2051A6 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_cSSID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ContentPage objects.</returns>
		public TList<ContentPage> GetByCSSID(TransactionManager transactionManager, System.Int32? _cSSID, int start, int pageLength)
		{
			int count = -1;
			return GetByCSSID(transactionManager, _cSSID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeCont__CSSID__4D2051A6 key.
		///		fKZNodeContCSSID4D2051A6 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_cSSID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ContentPage objects.</returns>
		public TList<ContentPage> GetByCSSID(System.Int32? _cSSID, int start, int pageLength)
		{
			int count =  -1;
			return GetByCSSID(null, _cSSID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeCont__CSSID__4D2051A6 key.
		///		fKZNodeContCSSID4D2051A6 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_cSSID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ContentPage objects.</returns>
		public TList<ContentPage> GetByCSSID(System.Int32? _cSSID, int start, int pageLength,out int count)
		{
			return GetByCSSID(null, _cSSID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeCont__CSSID__4D2051A6 key.
		///		FK__ZNodeCont__CSSID__4D2051A6 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_cSSID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ContentPage objects.</returns>
		public abstract TList<ContentPage> GetByCSSID(TransactionManager transactionManager, System.Int32? _cSSID, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeCont__Maste__4E1475DF key.
		///		FK__ZNodeCont__Maste__4E1475DF Description: 
		/// </summary>
		/// <param name="_masterPageID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ContentPage objects.</returns>
		public TList<ContentPage> GetByMasterPageID(System.Int32? _masterPageID)
		{
			int count = -1;
			return GetByMasterPageID(_masterPageID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeCont__Maste__4E1475DF key.
		///		FK__ZNodeCont__Maste__4E1475DF Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_masterPageID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ContentPage objects.</returns>
		/// <remarks></remarks>
		public TList<ContentPage> GetByMasterPageID(TransactionManager transactionManager, System.Int32? _masterPageID)
		{
			int count = -1;
			return GetByMasterPageID(transactionManager, _masterPageID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeCont__Maste__4E1475DF key.
		///		FK__ZNodeCont__Maste__4E1475DF Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_masterPageID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ContentPage objects.</returns>
		public TList<ContentPage> GetByMasterPageID(TransactionManager transactionManager, System.Int32? _masterPageID, int start, int pageLength)
		{
			int count = -1;
			return GetByMasterPageID(transactionManager, _masterPageID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeCont__Maste__4E1475DF key.
		///		fKZNodeContMaste4E1475DF Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_masterPageID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ContentPage objects.</returns>
		public TList<ContentPage> GetByMasterPageID(System.Int32? _masterPageID, int start, int pageLength)
		{
			int count =  -1;
			return GetByMasterPageID(null, _masterPageID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeCont__Maste__4E1475DF key.
		///		fKZNodeContMaste4E1475DF Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_masterPageID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ContentPage objects.</returns>
		public TList<ContentPage> GetByMasterPageID(System.Int32? _masterPageID, int start, int pageLength,out int count)
		{
			return GetByMasterPageID(null, _masterPageID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeCont__Maste__4E1475DF key.
		///		FK__ZNodeCont__Maste__4E1475DF Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_masterPageID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ContentPage objects.</returns>
		public abstract TList<ContentPage> GetByMasterPageID(TransactionManager transactionManager, System.Int32? _masterPageID, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeCont__Theme__4F089A18 key.
		///		FK__ZNodeCont__Theme__4F089A18 Description: 
		/// </summary>
		/// <param name="_themeID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ContentPage objects.</returns>
		public TList<ContentPage> GetByThemeID(System.Int32? _themeID)
		{
			int count = -1;
			return GetByThemeID(_themeID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeCont__Theme__4F089A18 key.
		///		FK__ZNodeCont__Theme__4F089A18 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_themeID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ContentPage objects.</returns>
		/// <remarks></remarks>
		public TList<ContentPage> GetByThemeID(TransactionManager transactionManager, System.Int32? _themeID)
		{
			int count = -1;
			return GetByThemeID(transactionManager, _themeID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeCont__Theme__4F089A18 key.
		///		FK__ZNodeCont__Theme__4F089A18 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_themeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ContentPage objects.</returns>
		public TList<ContentPage> GetByThemeID(TransactionManager transactionManager, System.Int32? _themeID, int start, int pageLength)
		{
			int count = -1;
			return GetByThemeID(transactionManager, _themeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeCont__Theme__4F089A18 key.
		///		fKZNodeContTheme4F089A18 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_themeID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ContentPage objects.</returns>
		public TList<ContentPage> GetByThemeID(System.Int32? _themeID, int start, int pageLength)
		{
			int count =  -1;
			return GetByThemeID(null, _themeID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeCont__Theme__4F089A18 key.
		///		fKZNodeContTheme4F089A18 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_themeID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ContentPage objects.</returns>
		public TList<ContentPage> GetByThemeID(System.Int32? _themeID, int start, int pageLength,out int count)
		{
			return GetByThemeID(null, _themeID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeCont__Theme__4F089A18 key.
		///		FK__ZNodeCont__Theme__4F089A18 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_themeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ContentPage objects.</returns>
		public abstract TList<ContentPage> GetByThemeID(TransactionManager transactionManager, System.Int32? _themeID, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeContentPage_ZNodePortal key.
		///		FK_ZNodeContentPage_ZNodePortal Description: 
		/// </summary>
		/// <param name="_portalID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ContentPage objects.</returns>
		public TList<ContentPage> GetByPortalID(System.Int32 _portalID)
		{
			int count = -1;
			return GetByPortalID(_portalID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeContentPage_ZNodePortal key.
		///		FK_ZNodeContentPage_ZNodePortal Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ContentPage objects.</returns>
		/// <remarks></remarks>
		public TList<ContentPage> GetByPortalID(TransactionManager transactionManager, System.Int32 _portalID)
		{
			int count = -1;
			return GetByPortalID(transactionManager, _portalID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeContentPage_ZNodePortal key.
		///		FK_ZNodeContentPage_ZNodePortal Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ContentPage objects.</returns>
		public TList<ContentPage> GetByPortalID(TransactionManager transactionManager, System.Int32 _portalID, int start, int pageLength)
		{
			int count = -1;
			return GetByPortalID(transactionManager, _portalID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeContentPage_ZNodePortal key.
		///		fKZNodeContentPageZNodePortal Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_portalID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ContentPage objects.</returns>
		public TList<ContentPage> GetByPortalID(System.Int32 _portalID, int start, int pageLength)
		{
			int count =  -1;
			return GetByPortalID(null, _portalID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeContentPage_ZNodePortal key.
		///		fKZNodeContentPageZNodePortal Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_portalID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ContentPage objects.</returns>
		public TList<ContentPage> GetByPortalID(System.Int32 _portalID, int start, int pageLength,out int count)
		{
			return GetByPortalID(null, _portalID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeContentPage_ZNodePortal key.
		///		FK_ZNodeContentPage_ZNodePortal Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ContentPage objects.</returns>
		public abstract TList<ContentPage> GetByPortalID(TransactionManager transactionManager, System.Int32 _portalID, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.ContentPage Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ContentPageKey key, int start, int pageLength)
		{
			return GetByContentPageID(transactionManager, key.ContentPageID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_SEOURL index.
		/// </summary>
		/// <param name="_sEOURL"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;ContentPage&gt;"/> class.</returns>
		public TList<ContentPage> GetBySEOURL(System.String _sEOURL)
		{
			int count = -1;
			return GetBySEOURL(null,_sEOURL, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_SEOURL index.
		/// </summary>
		/// <param name="_sEOURL"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ContentPage&gt;"/> class.</returns>
		public TList<ContentPage> GetBySEOURL(System.String _sEOURL, int start, int pageLength)
		{
			int count = -1;
			return GetBySEOURL(null, _sEOURL, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_SEOURL index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sEOURL"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ContentPage&gt;"/> class.</returns>
		public TList<ContentPage> GetBySEOURL(TransactionManager transactionManager, System.String _sEOURL)
		{
			int count = -1;
			return GetBySEOURL(transactionManager, _sEOURL, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_SEOURL index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sEOURL"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ContentPage&gt;"/> class.</returns>
		public TList<ContentPage> GetBySEOURL(TransactionManager transactionManager, System.String _sEOURL, int start, int pageLength)
		{
			int count = -1;
			return GetBySEOURL(transactionManager, _sEOURL, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_SEOURL index.
		/// </summary>
		/// <param name="_sEOURL"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ContentPage&gt;"/> class.</returns>
		public TList<ContentPage> GetBySEOURL(System.String _sEOURL, int start, int pageLength, out int count)
		{
			return GetBySEOURL(null, _sEOURL, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_SEOURL index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sEOURL"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;ContentPage&gt;"/> class.</returns>
		public abstract TList<ContentPage> GetBySEOURL(TransactionManager transactionManager, System.String _sEOURL, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodePage index.
		/// </summary>
		/// <param name="_name"></param>
		/// <param name="_portalID"></param>
		/// <param name="_localeId"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ContentPage"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ContentPage GetByNamePortalIDLocaleId(System.String _name, System.Int32 _portalID, System.Int32 _localeId)
		{
			int count = -1;
			return GetByNamePortalIDLocaleId(null,_name, _portalID, _localeId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePage index.
		/// </summary>
		/// <param name="_name"></param>
		/// <param name="_portalID"></param>
		/// <param name="_localeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ContentPage"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ContentPage GetByNamePortalIDLocaleId(System.String _name, System.Int32 _portalID, System.Int32 _localeId, int start, int pageLength)
		{
			int count = -1;
			return GetByNamePortalIDLocaleId(null, _name, _portalID, _localeId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePage index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_name"></param>
		/// <param name="_portalID"></param>
		/// <param name="_localeId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ContentPage"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ContentPage GetByNamePortalIDLocaleId(TransactionManager transactionManager, System.String _name, System.Int32 _portalID, System.Int32 _localeId)
		{
			int count = -1;
			return GetByNamePortalIDLocaleId(transactionManager, _name, _portalID, _localeId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePage index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_name"></param>
		/// <param name="_portalID"></param>
		/// <param name="_localeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ContentPage"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ContentPage GetByNamePortalIDLocaleId(TransactionManager transactionManager, System.String _name, System.Int32 _portalID, System.Int32 _localeId, int start, int pageLength)
		{
			int count = -1;
			return GetByNamePortalIDLocaleId(transactionManager, _name, _portalID, _localeId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePage index.
		/// </summary>
		/// <param name="_name"></param>
		/// <param name="_portalID"></param>
		/// <param name="_localeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ContentPage"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ContentPage GetByNamePortalIDLocaleId(System.String _name, System.Int32 _portalID, System.Int32 _localeId, int start, int pageLength, out int count)
		{
			return GetByNamePortalIDLocaleId(null, _name, _portalID, _localeId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePage index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_name"></param>
		/// <param name="_portalID"></param>
		/// <param name="_localeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ContentPage"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.ContentPage GetByNamePortalIDLocaleId(TransactionManager transactionManager, System.String _name, System.Int32 _portalID, System.Int32 _localeId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodePage_1 index.
		/// </summary>
		/// <param name="_contentPageID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ContentPage"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ContentPage GetByContentPageID(System.Int32 _contentPageID)
		{
			int count = -1;
			return GetByContentPageID(null,_contentPageID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodePage_1 index.
		/// </summary>
		/// <param name="_contentPageID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ContentPage"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ContentPage GetByContentPageID(System.Int32 _contentPageID, int start, int pageLength)
		{
			int count = -1;
			return GetByContentPageID(null, _contentPageID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodePage_1 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_contentPageID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ContentPage"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ContentPage GetByContentPageID(TransactionManager transactionManager, System.Int32 _contentPageID)
		{
			int count = -1;
			return GetByContentPageID(transactionManager, _contentPageID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodePage_1 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_contentPageID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ContentPage"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ContentPage GetByContentPageID(TransactionManager transactionManager, System.Int32 _contentPageID, int start, int pageLength)
		{
			int count = -1;
			return GetByContentPageID(transactionManager, _contentPageID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodePage_1 index.
		/// </summary>
		/// <param name="_contentPageID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ContentPage"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ContentPage GetByContentPageID(System.Int32 _contentPageID, int start, int pageLength, out int count)
		{
			return GetByContentPageID(null, _contentPageID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodePage_1 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_contentPageID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ContentPage"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.ContentPage GetByContentPageID(TransactionManager transactionManager, System.Int32 _contentPageID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;ContentPage&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;ContentPage&gt;"/></returns>
		public static TList<ContentPage> Fill(IDataReader reader, TList<ContentPage> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.ContentPage c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("ContentPage")
					.Append("|").Append((System.Int32)reader[((int)ContentPageColumn.ContentPageID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<ContentPage>(
					key.ToString(), // EntityTrackingKey
					"ContentPage",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.ContentPage();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.ContentPageID = (System.Int32)reader[((int)ContentPageColumn.ContentPageID - 1)];
					c.Name = (System.String)reader[((int)ContentPageColumn.Name - 1)];
					c.PortalID = (System.Int32)reader[((int)ContentPageColumn.PortalID - 1)];
					c.Title = (System.String)reader[((int)ContentPageColumn.Title - 1)];
					c.SEOTitle = (reader.IsDBNull(((int)ContentPageColumn.SEOTitle - 1)))?null:(System.String)reader[((int)ContentPageColumn.SEOTitle - 1)];
					c.SEOMetaKeywords = (reader.IsDBNull(((int)ContentPageColumn.SEOMetaKeywords - 1)))?null:(System.String)reader[((int)ContentPageColumn.SEOMetaKeywords - 1)];
					c.SEOMetaDescription = (reader.IsDBNull(((int)ContentPageColumn.SEOMetaDescription - 1)))?null:(System.String)reader[((int)ContentPageColumn.SEOMetaDescription - 1)];
					c.AllowDelete = (System.Boolean)reader[((int)ContentPageColumn.AllowDelete - 1)];
					c.TemplateName = (System.String)reader[((int)ContentPageColumn.TemplateName - 1)];
					c.ActiveInd = (System.Boolean)reader[((int)ContentPageColumn.ActiveInd - 1)];
					c.AnalyticsCode = (reader.IsDBNull(((int)ContentPageColumn.AnalyticsCode - 1)))?null:(System.String)reader[((int)ContentPageColumn.AnalyticsCode - 1)];
					c.Custom1 = (reader.IsDBNull(((int)ContentPageColumn.Custom1 - 1)))?null:(System.String)reader[((int)ContentPageColumn.Custom1 - 1)];
					c.Custom2 = (reader.IsDBNull(((int)ContentPageColumn.Custom2 - 1)))?null:(System.String)reader[((int)ContentPageColumn.Custom2 - 1)];
					c.Custom3 = (reader.IsDBNull(((int)ContentPageColumn.Custom3 - 1)))?null:(System.String)reader[((int)ContentPageColumn.Custom3 - 1)];
					c.SEOURL = (reader.IsDBNull(((int)ContentPageColumn.SEOURL - 1)))?null:(System.String)reader[((int)ContentPageColumn.SEOURL - 1)];
					c.LocaleId = (System.Int32)reader[((int)ContentPageColumn.LocaleId - 1)];
					c.MetaTagAdditional = (reader.IsDBNull(((int)ContentPageColumn.MetaTagAdditional - 1)))?null:(System.String)reader[((int)ContentPageColumn.MetaTagAdditional - 1)];
					c.ThemeID = (reader.IsDBNull(((int)ContentPageColumn.ThemeID - 1)))?null:(System.Int32?)reader[((int)ContentPageColumn.ThemeID - 1)];
					c.CSSID = (reader.IsDBNull(((int)ContentPageColumn.CSSID - 1)))?null:(System.Int32?)reader[((int)ContentPageColumn.CSSID - 1)];
					c.MasterPageID = (reader.IsDBNull(((int)ContentPageColumn.MasterPageID - 1)))?null:(System.Int32?)reader[((int)ContentPageColumn.MasterPageID - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.ContentPage"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ContentPage"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.ContentPage entity)
		{
			if (!reader.Read()) return;
			
			entity.ContentPageID = (System.Int32)reader[((int)ContentPageColumn.ContentPageID - 1)];
			entity.Name = (System.String)reader[((int)ContentPageColumn.Name - 1)];
			entity.PortalID = (System.Int32)reader[((int)ContentPageColumn.PortalID - 1)];
			entity.Title = (System.String)reader[((int)ContentPageColumn.Title - 1)];
			entity.SEOTitle = (reader.IsDBNull(((int)ContentPageColumn.SEOTitle - 1)))?null:(System.String)reader[((int)ContentPageColumn.SEOTitle - 1)];
			entity.SEOMetaKeywords = (reader.IsDBNull(((int)ContentPageColumn.SEOMetaKeywords - 1)))?null:(System.String)reader[((int)ContentPageColumn.SEOMetaKeywords - 1)];
			entity.SEOMetaDescription = (reader.IsDBNull(((int)ContentPageColumn.SEOMetaDescription - 1)))?null:(System.String)reader[((int)ContentPageColumn.SEOMetaDescription - 1)];
			entity.AllowDelete = (System.Boolean)reader[((int)ContentPageColumn.AllowDelete - 1)];
			entity.TemplateName = (System.String)reader[((int)ContentPageColumn.TemplateName - 1)];
			entity.ActiveInd = (System.Boolean)reader[((int)ContentPageColumn.ActiveInd - 1)];
			entity.AnalyticsCode = (reader.IsDBNull(((int)ContentPageColumn.AnalyticsCode - 1)))?null:(System.String)reader[((int)ContentPageColumn.AnalyticsCode - 1)];
			entity.Custom1 = (reader.IsDBNull(((int)ContentPageColumn.Custom1 - 1)))?null:(System.String)reader[((int)ContentPageColumn.Custom1 - 1)];
			entity.Custom2 = (reader.IsDBNull(((int)ContentPageColumn.Custom2 - 1)))?null:(System.String)reader[((int)ContentPageColumn.Custom2 - 1)];
			entity.Custom3 = (reader.IsDBNull(((int)ContentPageColumn.Custom3 - 1)))?null:(System.String)reader[((int)ContentPageColumn.Custom3 - 1)];
			entity.SEOURL = (reader.IsDBNull(((int)ContentPageColumn.SEOURL - 1)))?null:(System.String)reader[((int)ContentPageColumn.SEOURL - 1)];
			entity.LocaleId = (System.Int32)reader[((int)ContentPageColumn.LocaleId - 1)];
			entity.MetaTagAdditional = (reader.IsDBNull(((int)ContentPageColumn.MetaTagAdditional - 1)))?null:(System.String)reader[((int)ContentPageColumn.MetaTagAdditional - 1)];
			entity.ThemeID = (reader.IsDBNull(((int)ContentPageColumn.ThemeID - 1)))?null:(System.Int32?)reader[((int)ContentPageColumn.ThemeID - 1)];
			entity.CSSID = (reader.IsDBNull(((int)ContentPageColumn.CSSID - 1)))?null:(System.Int32?)reader[((int)ContentPageColumn.CSSID - 1)];
			entity.MasterPageID = (reader.IsDBNull(((int)ContentPageColumn.MasterPageID - 1)))?null:(System.Int32?)reader[((int)ContentPageColumn.MasterPageID - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.ContentPage"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ContentPage"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.ContentPage entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.ContentPageID = (System.Int32)dataRow["ContentPageID"];
			entity.Name = (System.String)dataRow["Name"];
			entity.PortalID = (System.Int32)dataRow["PortalID"];
			entity.Title = (System.String)dataRow["Title"];
			entity.SEOTitle = Convert.IsDBNull(dataRow["SEOTitle"]) ? null : (System.String)dataRow["SEOTitle"];
			entity.SEOMetaKeywords = Convert.IsDBNull(dataRow["SEOMetaKeywords"]) ? null : (System.String)dataRow["SEOMetaKeywords"];
			entity.SEOMetaDescription = Convert.IsDBNull(dataRow["SEOMetaDescription"]) ? null : (System.String)dataRow["SEOMetaDescription"];
			entity.AllowDelete = (System.Boolean)dataRow["AllowDelete"];
			entity.TemplateName = (System.String)dataRow["TemplateName"];
			entity.ActiveInd = (System.Boolean)dataRow["ActiveInd"];
			entity.AnalyticsCode = Convert.IsDBNull(dataRow["AnalyticsCode"]) ? null : (System.String)dataRow["AnalyticsCode"];
			entity.Custom1 = Convert.IsDBNull(dataRow["Custom1"]) ? null : (System.String)dataRow["Custom1"];
			entity.Custom2 = Convert.IsDBNull(dataRow["Custom2"]) ? null : (System.String)dataRow["Custom2"];
			entity.Custom3 = Convert.IsDBNull(dataRow["Custom3"]) ? null : (System.String)dataRow["Custom3"];
			entity.SEOURL = Convert.IsDBNull(dataRow["SEOURL"]) ? null : (System.String)dataRow["SEOURL"];
			entity.LocaleId = (System.Int32)dataRow["LocaleId"];
			entity.MetaTagAdditional = Convert.IsDBNull(dataRow["MetaTagAdditional"]) ? null : (System.String)dataRow["MetaTagAdditional"];
			entity.ThemeID = Convert.IsDBNull(dataRow["ThemeID"]) ? null : (System.Int32?)dataRow["ThemeID"];
			entity.CSSID = Convert.IsDBNull(dataRow["CSSID"]) ? null : (System.Int32?)dataRow["CSSID"];
			entity.MasterPageID = Convert.IsDBNull(dataRow["MasterPageID"]) ? null : (System.Int32?)dataRow["MasterPageID"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ContentPage"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.ContentPage Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ContentPage entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region CSSIDSource	
			if (CanDeepLoad(entity, "CSS|CSSIDSource", deepLoadType, innerList) 
				&& entity.CSSIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.CSSID ?? (int)0);
				CSS tmpEntity = EntityManager.LocateEntity<CSS>(EntityLocator.ConstructKeyFromPkItems(typeof(CSS), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CSSIDSource = tmpEntity;
				else
					entity.CSSIDSource = DataRepository.CSSProvider.GetByCSSID(transactionManager, (entity.CSSID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CSSIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CSSIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CSSProvider.DeepLoad(transactionManager, entity.CSSIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CSSIDSource

			#region MasterPageIDSource	
			if (CanDeepLoad(entity, "MasterPage|MasterPageIDSource", deepLoadType, innerList) 
				&& entity.MasterPageIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.MasterPageID ?? (int)0);
				MasterPage tmpEntity = EntityManager.LocateEntity<MasterPage>(EntityLocator.ConstructKeyFromPkItems(typeof(MasterPage), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.MasterPageIDSource = tmpEntity;
				else
					entity.MasterPageIDSource = DataRepository.MasterPageProvider.GetByMasterPageID(transactionManager, (entity.MasterPageID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'MasterPageIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.MasterPageIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.MasterPageProvider.DeepLoad(transactionManager, entity.MasterPageIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion MasterPageIDSource

			#region ThemeIDSource	
			if (CanDeepLoad(entity, "Theme|ThemeIDSource", deepLoadType, innerList) 
				&& entity.ThemeIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.ThemeID ?? (int)0);
				Theme tmpEntity = EntityManager.LocateEntity<Theme>(EntityLocator.ConstructKeyFromPkItems(typeof(Theme), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ThemeIDSource = tmpEntity;
				else
					entity.ThemeIDSource = DataRepository.ThemeProvider.GetByThemeID(transactionManager, (entity.ThemeID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ThemeIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ThemeIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.ThemeProvider.DeepLoad(transactionManager, entity.ThemeIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ThemeIDSource

			#region PortalIDSource	
			if (CanDeepLoad(entity, "Portal|PortalIDSource", deepLoadType, innerList) 
				&& entity.PortalIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.PortalID;
				Portal tmpEntity = EntityManager.LocateEntity<Portal>(EntityLocator.ConstructKeyFromPkItems(typeof(Portal), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.PortalIDSource = tmpEntity;
				else
					entity.PortalIDSource = DataRepository.PortalProvider.GetByPortalID(transactionManager, entity.PortalID);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PortalIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.PortalIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.PortalProvider.DeepLoad(transactionManager, entity.PortalIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion PortalIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByContentPageID methods when available
			
			#region ContentPageRevisionCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<ContentPageRevision>|ContentPageRevisionCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ContentPageRevisionCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ContentPageRevisionCollection = DataRepository.ContentPageRevisionProvider.GetByContentPageID(transactionManager, entity.ContentPageID);

				if (deep && entity.ContentPageRevisionCollection.Count > 0)
				{
					deepHandles.Add("ContentPageRevisionCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<ContentPageRevision>) DataRepository.ContentPageRevisionProvider.DeepLoad,
						new object[] { transactionManager, entity.ContentPageRevisionCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.ContentPage object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.ContentPage instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.ContentPage Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ContentPage entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region CSSIDSource
			if (CanDeepSave(entity, "CSS|CSSIDSource", deepSaveType, innerList) 
				&& entity.CSSIDSource != null)
			{
				DataRepository.CSSProvider.Save(transactionManager, entity.CSSIDSource);
				entity.CSSID = entity.CSSIDSource.CSSID;
			}
			#endregion 
			
			#region MasterPageIDSource
			if (CanDeepSave(entity, "MasterPage|MasterPageIDSource", deepSaveType, innerList) 
				&& entity.MasterPageIDSource != null)
			{
				DataRepository.MasterPageProvider.Save(transactionManager, entity.MasterPageIDSource);
				entity.MasterPageID = entity.MasterPageIDSource.MasterPageID;
			}
			#endregion 
			
			#region ThemeIDSource
			if (CanDeepSave(entity, "Theme|ThemeIDSource", deepSaveType, innerList) 
				&& entity.ThemeIDSource != null)
			{
				DataRepository.ThemeProvider.Save(transactionManager, entity.ThemeIDSource);
				entity.ThemeID = entity.ThemeIDSource.ThemeID;
			}
			#endregion 
			
			#region PortalIDSource
			if (CanDeepSave(entity, "Portal|PortalIDSource", deepSaveType, innerList) 
				&& entity.PortalIDSource != null)
			{
				DataRepository.PortalProvider.Save(transactionManager, entity.PortalIDSource);
				entity.PortalID = entity.PortalIDSource.PortalID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<ContentPageRevision>
				if (CanDeepSave(entity.ContentPageRevisionCollection, "List<ContentPageRevision>|ContentPageRevisionCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(ContentPageRevision child in entity.ContentPageRevisionCollection)
					{
						if(child.ContentPageIDSource != null)
						{
							child.ContentPageID = child.ContentPageIDSource.ContentPageID;
						}
						else
						{
							child.ContentPageID = entity.ContentPageID;
						}

					}

					if (entity.ContentPageRevisionCollection.Count > 0 || entity.ContentPageRevisionCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ContentPageRevisionProvider.Save(transactionManager, entity.ContentPageRevisionCollection);
						
						deepHandles.Add("ContentPageRevisionCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< ContentPageRevision >) DataRepository.ContentPageRevisionProvider.DeepSave,
							new object[] { transactionManager, entity.ContentPageRevisionCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region ContentPageChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.ContentPage</c>
	///</summary>
	public enum ContentPageChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>CSS</c> at CSSIDSource
		///</summary>
		[ChildEntityType(typeof(CSS))]
		CSS,
		
		///<summary>
		/// Composite Property for <c>MasterPage</c> at MasterPageIDSource
		///</summary>
		[ChildEntityType(typeof(MasterPage))]
		MasterPage,
		
		///<summary>
		/// Composite Property for <c>Theme</c> at ThemeIDSource
		///</summary>
		[ChildEntityType(typeof(Theme))]
		Theme,
		
		///<summary>
		/// Composite Property for <c>Portal</c> at PortalIDSource
		///</summary>
		[ChildEntityType(typeof(Portal))]
		Portal,
		///<summary>
		/// Collection of <c>ContentPage</c> as OneToMany for ContentPageRevisionCollection
		///</summary>
		[ChildEntityType(typeof(TList<ContentPageRevision>))]
		ContentPageRevisionCollection,
	}
	
	#endregion ContentPageChildEntityTypes
	
	#region ContentPageFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;ContentPageColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ContentPage"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ContentPageFilterBuilder : SqlFilterBuilder<ContentPageColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ContentPageFilterBuilder class.
		/// </summary>
		public ContentPageFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ContentPageFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ContentPageFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ContentPageFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ContentPageFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ContentPageFilterBuilder
	
	#region ContentPageParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;ContentPageColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ContentPage"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ContentPageParameterBuilder : ParameterizedSqlFilterBuilder<ContentPageColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ContentPageParameterBuilder class.
		/// </summary>
		public ContentPageParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ContentPageParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ContentPageParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ContentPageParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ContentPageParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ContentPageParameterBuilder
	
	#region ContentPageSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;ContentPageColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ContentPage"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ContentPageSortBuilder : SqlSortBuilder<ContentPageColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ContentPageSqlSortBuilder class.
		/// </summary>
		public ContentPageSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ContentPageSortBuilder
	
} // end namespace
