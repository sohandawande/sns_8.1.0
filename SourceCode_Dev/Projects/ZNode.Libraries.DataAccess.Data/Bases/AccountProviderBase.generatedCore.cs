﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="AccountProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class AccountProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.Account, ZNode.Libraries.DataAccess.Entities.AccountKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.AccountKey key)
		{
			return Delete(transactionManager, key.AccountID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_accountID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _accountID)
		{
			return Delete(null, _accountID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _accountID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAccount_ZNodeAccount key.
		///		FK_ZNodeAccount_ZNodeAccount Description: 
		/// </summary>
		/// <param name="_referralAccountID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Account objects.</returns>
		public TList<Account> GetByReferralAccountID(System.Int32? _referralAccountID)
		{
			int count = -1;
			return GetByReferralAccountID(_referralAccountID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAccount_ZNodeAccount key.
		///		FK_ZNodeAccount_ZNodeAccount Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_referralAccountID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Account objects.</returns>
		/// <remarks></remarks>
		public TList<Account> GetByReferralAccountID(TransactionManager transactionManager, System.Int32? _referralAccountID)
		{
			int count = -1;
			return GetByReferralAccountID(transactionManager, _referralAccountID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAccount_ZNodeAccount key.
		///		FK_ZNodeAccount_ZNodeAccount Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_referralAccountID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Account objects.</returns>
		public TList<Account> GetByReferralAccountID(TransactionManager transactionManager, System.Int32? _referralAccountID, int start, int pageLength)
		{
			int count = -1;
			return GetByReferralAccountID(transactionManager, _referralAccountID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAccount_ZNodeAccount key.
		///		fKZNodeAccountZNodeAccount Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_referralAccountID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Account objects.</returns>
		public TList<Account> GetByReferralAccountID(System.Int32? _referralAccountID, int start, int pageLength)
		{
			int count =  -1;
			return GetByReferralAccountID(null, _referralAccountID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAccount_ZNodeAccount key.
		///		fKZNodeAccountZNodeAccount Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_referralAccountID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Account objects.</returns>
		public TList<Account> GetByReferralAccountID(System.Int32? _referralAccountID, int start, int pageLength,out int count)
		{
			return GetByReferralAccountID(null, _referralAccountID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAccount_ZNodeAccount key.
		///		FK_ZNodeAccount_ZNodeAccount Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_referralAccountID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Account objects.</returns>
		public abstract TList<Account> GetByReferralAccountID(TransactionManager transactionManager, System.Int32? _referralAccountID, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAccount_ZNodeReferralCommissionType key.
		///		FK_ZNodeAccount_ZNodeReferralCommissionType Description: 
		/// </summary>
		/// <param name="_referralCommissionTypeID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Account objects.</returns>
		public TList<Account> GetByReferralCommissionTypeID(System.Int32? _referralCommissionTypeID)
		{
			int count = -1;
			return GetByReferralCommissionTypeID(_referralCommissionTypeID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAccount_ZNodeReferralCommissionType key.
		///		FK_ZNodeAccount_ZNodeReferralCommissionType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_referralCommissionTypeID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Account objects.</returns>
		/// <remarks></remarks>
		public TList<Account> GetByReferralCommissionTypeID(TransactionManager transactionManager, System.Int32? _referralCommissionTypeID)
		{
			int count = -1;
			return GetByReferralCommissionTypeID(transactionManager, _referralCommissionTypeID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAccount_ZNodeReferralCommissionType key.
		///		FK_ZNodeAccount_ZNodeReferralCommissionType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_referralCommissionTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Account objects.</returns>
		public TList<Account> GetByReferralCommissionTypeID(TransactionManager transactionManager, System.Int32? _referralCommissionTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetByReferralCommissionTypeID(transactionManager, _referralCommissionTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAccount_ZNodeReferralCommissionType key.
		///		fKZNodeAccountZNodeReferralCommissionType Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_referralCommissionTypeID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Account objects.</returns>
		public TList<Account> GetByReferralCommissionTypeID(System.Int32? _referralCommissionTypeID, int start, int pageLength)
		{
			int count =  -1;
			return GetByReferralCommissionTypeID(null, _referralCommissionTypeID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAccount_ZNodeReferralCommissionType key.
		///		fKZNodeAccountZNodeReferralCommissionType Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_referralCommissionTypeID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Account objects.</returns>
		public TList<Account> GetByReferralCommissionTypeID(System.Int32? _referralCommissionTypeID, int start, int pageLength,out int count)
		{
			return GetByReferralCommissionTypeID(null, _referralCommissionTypeID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAccount_ZNodeReferralCommissionType key.
		///		FK_ZNodeAccount_ZNodeReferralCommissionType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_referralCommissionTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Account objects.</returns>
		public abstract TList<Account> GetByReferralCommissionTypeID(TransactionManager transactionManager, System.Int32? _referralCommissionTypeID, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.Account Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.AccountKey key, int start, int pageLength)
		{
			return GetByAccountID(transactionManager, key.AccountID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeAccount_AccountTypeID index.
		/// </summary>
		/// <param name="_accountTypeID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Account&gt;"/> class.</returns>
		public TList<Account> GetByAccountTypeID(System.Int32? _accountTypeID)
		{
			int count = -1;
			return GetByAccountTypeID(null,_accountTypeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAccount_AccountTypeID index.
		/// </summary>
		/// <param name="_accountTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Account&gt;"/> class.</returns>
		public TList<Account> GetByAccountTypeID(System.Int32? _accountTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetByAccountTypeID(null, _accountTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAccount_AccountTypeID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountTypeID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Account&gt;"/> class.</returns>
		public TList<Account> GetByAccountTypeID(TransactionManager transactionManager, System.Int32? _accountTypeID)
		{
			int count = -1;
			return GetByAccountTypeID(transactionManager, _accountTypeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAccount_AccountTypeID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Account&gt;"/> class.</returns>
		public TList<Account> GetByAccountTypeID(TransactionManager transactionManager, System.Int32? _accountTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetByAccountTypeID(transactionManager, _accountTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAccount_AccountTypeID index.
		/// </summary>
		/// <param name="_accountTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Account&gt;"/> class.</returns>
		public TList<Account> GetByAccountTypeID(System.Int32? _accountTypeID, int start, int pageLength, out int count)
		{
			return GetByAccountTypeID(null, _accountTypeID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAccount_AccountTypeID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Account&gt;"/> class.</returns>
		public abstract TList<Account> GetByAccountTypeID(TransactionManager transactionManager, System.Int32? _accountTypeID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeAccount_ActiveInd index.
		/// </summary>
		/// <param name="_activeInd"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Account&gt;"/> class.</returns>
		public TList<Account> GetByActiveInd(System.Boolean? _activeInd)
		{
			int count = -1;
			return GetByActiveInd(null,_activeInd, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAccount_ActiveInd index.
		/// </summary>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Account&gt;"/> class.</returns>
		public TList<Account> GetByActiveInd(System.Boolean? _activeInd, int start, int pageLength)
		{
			int count = -1;
			return GetByActiveInd(null, _activeInd, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAccount_ActiveInd index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_activeInd"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Account&gt;"/> class.</returns>
		public TList<Account> GetByActiveInd(TransactionManager transactionManager, System.Boolean? _activeInd)
		{
			int count = -1;
			return GetByActiveInd(transactionManager, _activeInd, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAccount_ActiveInd index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Account&gt;"/> class.</returns>
		public TList<Account> GetByActiveInd(TransactionManager transactionManager, System.Boolean? _activeInd, int start, int pageLength)
		{
			int count = -1;
			return GetByActiveInd(transactionManager, _activeInd, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAccount_ActiveInd index.
		/// </summary>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Account&gt;"/> class.</returns>
		public TList<Account> GetByActiveInd(System.Boolean? _activeInd, int start, int pageLength, out int count)
		{
			return GetByActiveInd(null, _activeInd, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAccount_ActiveInd index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Account&gt;"/> class.</returns>
		public abstract TList<Account> GetByActiveInd(TransactionManager transactionManager, System.Boolean? _activeInd, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeAccount_CompanyName index.
		/// </summary>
		/// <param name="_companyName"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Account&gt;"/> class.</returns>
		public TList<Account> GetByCompanyName(System.String _companyName)
		{
			int count = -1;
			return GetByCompanyName(null,_companyName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAccount_CompanyName index.
		/// </summary>
		/// <param name="_companyName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Account&gt;"/> class.</returns>
		public TList<Account> GetByCompanyName(System.String _companyName, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanyName(null, _companyName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAccount_CompanyName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyName"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Account&gt;"/> class.</returns>
		public TList<Account> GetByCompanyName(TransactionManager transactionManager, System.String _companyName)
		{
			int count = -1;
			return GetByCompanyName(transactionManager, _companyName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAccount_CompanyName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Account&gt;"/> class.</returns>
		public TList<Account> GetByCompanyName(TransactionManager transactionManager, System.String _companyName, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanyName(transactionManager, _companyName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAccount_CompanyName index.
		/// </summary>
		/// <param name="_companyName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Account&gt;"/> class.</returns>
		public TList<Account> GetByCompanyName(System.String _companyName, int start, int pageLength, out int count)
		{
			return GetByCompanyName(null, _companyName, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAccount_CompanyName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Account&gt;"/> class.</returns>
		public abstract TList<Account> GetByCompanyName(TransactionManager transactionManager, System.String _companyName, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeAccount_EmailOptIn index.
		/// </summary>
		/// <param name="_emailOptIn"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Account&gt;"/> class.</returns>
		public TList<Account> GetByEmailOptIn(System.Boolean _emailOptIn)
		{
			int count = -1;
			return GetByEmailOptIn(null,_emailOptIn, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAccount_EmailOptIn index.
		/// </summary>
		/// <param name="_emailOptIn"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Account&gt;"/> class.</returns>
		public TList<Account> GetByEmailOptIn(System.Boolean _emailOptIn, int start, int pageLength)
		{
			int count = -1;
			return GetByEmailOptIn(null, _emailOptIn, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAccount_EmailOptIn index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_emailOptIn"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Account&gt;"/> class.</returns>
		public TList<Account> GetByEmailOptIn(TransactionManager transactionManager, System.Boolean _emailOptIn)
		{
			int count = -1;
			return GetByEmailOptIn(transactionManager, _emailOptIn, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAccount_EmailOptIn index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_emailOptIn"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Account&gt;"/> class.</returns>
		public TList<Account> GetByEmailOptIn(TransactionManager transactionManager, System.Boolean _emailOptIn, int start, int pageLength)
		{
			int count = -1;
			return GetByEmailOptIn(transactionManager, _emailOptIn, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAccount_EmailOptIn index.
		/// </summary>
		/// <param name="_emailOptIn"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Account&gt;"/> class.</returns>
		public TList<Account> GetByEmailOptIn(System.Boolean _emailOptIn, int start, int pageLength, out int count)
		{
			return GetByEmailOptIn(null, _emailOptIn, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAccount_EmailOptIn index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_emailOptIn"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Account&gt;"/> class.</returns>
		public abstract TList<Account> GetByEmailOptIn(TransactionManager transactionManager, System.Boolean _emailOptIn, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeAccount_EnableCustomerPricing index.
		/// </summary>
		/// <param name="_enableCustomerPricing"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Account&gt;"/> class.</returns>
		public TList<Account> GetByEnableCustomerPricing(System.Boolean? _enableCustomerPricing)
		{
			int count = -1;
			return GetByEnableCustomerPricing(null,_enableCustomerPricing, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAccount_EnableCustomerPricing index.
		/// </summary>
		/// <param name="_enableCustomerPricing"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Account&gt;"/> class.</returns>
		public TList<Account> GetByEnableCustomerPricing(System.Boolean? _enableCustomerPricing, int start, int pageLength)
		{
			int count = -1;
			return GetByEnableCustomerPricing(null, _enableCustomerPricing, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAccount_EnableCustomerPricing index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_enableCustomerPricing"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Account&gt;"/> class.</returns>
		public TList<Account> GetByEnableCustomerPricing(TransactionManager transactionManager, System.Boolean? _enableCustomerPricing)
		{
			int count = -1;
			return GetByEnableCustomerPricing(transactionManager, _enableCustomerPricing, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAccount_EnableCustomerPricing index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_enableCustomerPricing"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Account&gt;"/> class.</returns>
		public TList<Account> GetByEnableCustomerPricing(TransactionManager transactionManager, System.Boolean? _enableCustomerPricing, int start, int pageLength)
		{
			int count = -1;
			return GetByEnableCustomerPricing(transactionManager, _enableCustomerPricing, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAccount_EnableCustomerPricing index.
		/// </summary>
		/// <param name="_enableCustomerPricing"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Account&gt;"/> class.</returns>
		public TList<Account> GetByEnableCustomerPricing(System.Boolean? _enableCustomerPricing, int start, int pageLength, out int count)
		{
			return GetByEnableCustomerPricing(null, _enableCustomerPricing, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAccount_EnableCustomerPricing index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_enableCustomerPricing"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Account&gt;"/> class.</returns>
		public abstract TList<Account> GetByEnableCustomerPricing(TransactionManager transactionManager, System.Boolean? _enableCustomerPricing, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeAccount_ExternalAccountNo index.
		/// </summary>
		/// <param name="_externalAccountNo"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Account&gt;"/> class.</returns>
		public TList<Account> GetByExternalAccountNo(System.String _externalAccountNo)
		{
			int count = -1;
			return GetByExternalAccountNo(null,_externalAccountNo, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAccount_ExternalAccountNo index.
		/// </summary>
		/// <param name="_externalAccountNo"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Account&gt;"/> class.</returns>
		public TList<Account> GetByExternalAccountNo(System.String _externalAccountNo, int start, int pageLength)
		{
			int count = -1;
			return GetByExternalAccountNo(null, _externalAccountNo, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAccount_ExternalAccountNo index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_externalAccountNo"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Account&gt;"/> class.</returns>
		public TList<Account> GetByExternalAccountNo(TransactionManager transactionManager, System.String _externalAccountNo)
		{
			int count = -1;
			return GetByExternalAccountNo(transactionManager, _externalAccountNo, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAccount_ExternalAccountNo index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_externalAccountNo"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Account&gt;"/> class.</returns>
		public TList<Account> GetByExternalAccountNo(TransactionManager transactionManager, System.String _externalAccountNo, int start, int pageLength)
		{
			int count = -1;
			return GetByExternalAccountNo(transactionManager, _externalAccountNo, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAccount_ExternalAccountNo index.
		/// </summary>
		/// <param name="_externalAccountNo"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Account&gt;"/> class.</returns>
		public TList<Account> GetByExternalAccountNo(System.String _externalAccountNo, int start, int pageLength, out int count)
		{
			return GetByExternalAccountNo(null, _externalAccountNo, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAccount_ExternalAccountNo index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_externalAccountNo"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Account&gt;"/> class.</returns>
		public abstract TList<Account> GetByExternalAccountNo(TransactionManager transactionManager, System.String _externalAccountNo, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeAccount_ParentAccountID index.
		/// </summary>
		/// <param name="_parentAccountID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Account&gt;"/> class.</returns>
		public TList<Account> GetByParentAccountID(System.Int32? _parentAccountID)
		{
			int count = -1;
			return GetByParentAccountID(null,_parentAccountID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAccount_ParentAccountID index.
		/// </summary>
		/// <param name="_parentAccountID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Account&gt;"/> class.</returns>
		public TList<Account> GetByParentAccountID(System.Int32? _parentAccountID, int start, int pageLength)
		{
			int count = -1;
			return GetByParentAccountID(null, _parentAccountID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAccount_ParentAccountID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_parentAccountID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Account&gt;"/> class.</returns>
		public TList<Account> GetByParentAccountID(TransactionManager transactionManager, System.Int32? _parentAccountID)
		{
			int count = -1;
			return GetByParentAccountID(transactionManager, _parentAccountID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAccount_ParentAccountID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_parentAccountID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Account&gt;"/> class.</returns>
		public TList<Account> GetByParentAccountID(TransactionManager transactionManager, System.Int32? _parentAccountID, int start, int pageLength)
		{
			int count = -1;
			return GetByParentAccountID(transactionManager, _parentAccountID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAccount_ParentAccountID index.
		/// </summary>
		/// <param name="_parentAccountID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Account&gt;"/> class.</returns>
		public TList<Account> GetByParentAccountID(System.Int32? _parentAccountID, int start, int pageLength, out int count)
		{
			return GetByParentAccountID(null, _parentAccountID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAccount_ParentAccountID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_parentAccountID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Account&gt;"/> class.</returns>
		public abstract TList<Account> GetByParentAccountID(TransactionManager transactionManager, System.Int32? _parentAccountID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeAccount_ProfileID index.
		/// </summary>
		/// <param name="_profileID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Account&gt;"/> class.</returns>
		public TList<Account> GetByProfileID(System.Int32? _profileID)
		{
			int count = -1;
			return GetByProfileID(null,_profileID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAccount_ProfileID index.
		/// </summary>
		/// <param name="_profileID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Account&gt;"/> class.</returns>
		public TList<Account> GetByProfileID(System.Int32? _profileID, int start, int pageLength)
		{
			int count = -1;
			return GetByProfileID(null, _profileID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAccount_ProfileID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_profileID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Account&gt;"/> class.</returns>
		public TList<Account> GetByProfileID(TransactionManager transactionManager, System.Int32? _profileID)
		{
			int count = -1;
			return GetByProfileID(transactionManager, _profileID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAccount_ProfileID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_profileID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Account&gt;"/> class.</returns>
		public TList<Account> GetByProfileID(TransactionManager transactionManager, System.Int32? _profileID, int start, int pageLength)
		{
			int count = -1;
			return GetByProfileID(transactionManager, _profileID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAccount_ProfileID index.
		/// </summary>
		/// <param name="_profileID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Account&gt;"/> class.</returns>
		public TList<Account> GetByProfileID(System.Int32? _profileID, int start, int pageLength, out int count)
		{
			return GetByProfileID(null, _profileID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAccount_ProfileID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_profileID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Account&gt;"/> class.</returns>
		public abstract TList<Account> GetByProfileID(TransactionManager transactionManager, System.Int32? _profileID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeAccount_UserID index.
		/// </summary>
		/// <param name="_userID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Account&gt;"/> class.</returns>
		public TList<Account> GetByUserID(System.Guid? _userID)
		{
			int count = -1;
			return GetByUserID(null,_userID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAccount_UserID index.
		/// </summary>
		/// <param name="_userID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Account&gt;"/> class.</returns>
		public TList<Account> GetByUserID(System.Guid? _userID, int start, int pageLength)
		{
			int count = -1;
			return GetByUserID(null, _userID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAccount_UserID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Account&gt;"/> class.</returns>
		public TList<Account> GetByUserID(TransactionManager transactionManager, System.Guid? _userID)
		{
			int count = -1;
			return GetByUserID(transactionManager, _userID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAccount_UserID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Account&gt;"/> class.</returns>
		public TList<Account> GetByUserID(TransactionManager transactionManager, System.Guid? _userID, int start, int pageLength)
		{
			int count = -1;
			return GetByUserID(transactionManager, _userID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAccount_UserID index.
		/// </summary>
		/// <param name="_userID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Account&gt;"/> class.</returns>
		public TList<Account> GetByUserID(System.Guid? _userID, int start, int pageLength, out int count)
		{
			return GetByUserID(null, _userID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAccount_UserID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Account&gt;"/> class.</returns>
		public abstract TList<Account> GetByUserID(TransactionManager transactionManager, System.Guid? _userID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_Account index.
		/// </summary>
		/// <param name="_accountID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Account"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Account GetByAccountID(System.Int32 _accountID)
		{
			int count = -1;
			return GetByAccountID(null,_accountID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Account index.
		/// </summary>
		/// <param name="_accountID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Account"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Account GetByAccountID(System.Int32 _accountID, int start, int pageLength)
		{
			int count = -1;
			return GetByAccountID(null, _accountID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Account index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Account"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Account GetByAccountID(TransactionManager transactionManager, System.Int32 _accountID)
		{
			int count = -1;
			return GetByAccountID(transactionManager, _accountID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Account index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Account"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Account GetByAccountID(TransactionManager transactionManager, System.Int32 _accountID, int start, int pageLength)
		{
			int count = -1;
			return GetByAccountID(transactionManager, _accountID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Account index.
		/// </summary>
		/// <param name="_accountID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Account"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Account GetByAccountID(System.Int32 _accountID, int start, int pageLength, out int count)
		{
			return GetByAccountID(null, _accountID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Account index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Account"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.Account GetByAccountID(TransactionManager transactionManager, System.Int32 _accountID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;Account&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;Account&gt;"/></returns>
		public static TList<Account> Fill(IDataReader reader, TList<Account> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.Account c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("Account")
					.Append("|").Append((System.Int32)reader[((int)AccountColumn.AccountID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<Account>(
					key.ToString(), // EntityTrackingKey
					"Account",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.Account();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.AccountID = (System.Int32)reader[((int)AccountColumn.AccountID - 1)];
					c.ParentAccountID = (reader.IsDBNull(((int)AccountColumn.ParentAccountID - 1)))?null:(System.Int32?)reader[((int)AccountColumn.ParentAccountID - 1)];
					c.UserID = (reader.IsDBNull(((int)AccountColumn.UserID - 1)))?null:(System.Guid?)reader[((int)AccountColumn.UserID - 1)];
					c.ExternalAccountNo = (reader.IsDBNull(((int)AccountColumn.ExternalAccountNo - 1)))?null:(System.String)reader[((int)AccountColumn.ExternalAccountNo - 1)];
					c.CompanyName = (reader.IsDBNull(((int)AccountColumn.CompanyName - 1)))?null:(System.String)reader[((int)AccountColumn.CompanyName - 1)];
					c.AccountTypeID = (reader.IsDBNull(((int)AccountColumn.AccountTypeID - 1)))?null:(System.Int32?)reader[((int)AccountColumn.AccountTypeID - 1)];
					c.ProfileID = (reader.IsDBNull(((int)AccountColumn.ProfileID - 1)))?null:(System.Int32?)reader[((int)AccountColumn.ProfileID - 1)];
					c.AccountProfileCode = (reader.IsDBNull(((int)AccountColumn.AccountProfileCode - 1)))?null:(System.String)reader[((int)AccountColumn.AccountProfileCode - 1)];
					c.SubAccountLimit = (reader.IsDBNull(((int)AccountColumn.SubAccountLimit - 1)))?null:(System.Int32?)reader[((int)AccountColumn.SubAccountLimit - 1)];
					c.Description = (reader.IsDBNull(((int)AccountColumn.Description - 1)))?null:(System.String)reader[((int)AccountColumn.Description - 1)];
					c.CreateUser = (reader.IsDBNull(((int)AccountColumn.CreateUser - 1)))?null:(System.String)reader[((int)AccountColumn.CreateUser - 1)];
					c.CreateDte = (System.DateTime)reader[((int)AccountColumn.CreateDte - 1)];
					c.UpdateUser = (reader.IsDBNull(((int)AccountColumn.UpdateUser - 1)))?null:(System.String)reader[((int)AccountColumn.UpdateUser - 1)];
					c.UpdateDte = (reader.IsDBNull(((int)AccountColumn.UpdateDte - 1)))?null:(System.DateTime?)reader[((int)AccountColumn.UpdateDte - 1)];
					c.ActiveInd = (reader.IsDBNull(((int)AccountColumn.ActiveInd - 1)))?null:(System.Boolean?)reader[((int)AccountColumn.ActiveInd - 1)];
					c.Website = (reader.IsDBNull(((int)AccountColumn.Website - 1)))?null:(System.String)reader[((int)AccountColumn.Website - 1)];
					c.Source = (reader.IsDBNull(((int)AccountColumn.Source - 1)))?null:(System.String)reader[((int)AccountColumn.Source - 1)];
					c.ReferralAccountID = (reader.IsDBNull(((int)AccountColumn.ReferralAccountID - 1)))?null:(System.Int32?)reader[((int)AccountColumn.ReferralAccountID - 1)];
					c.ReferralStatus = (reader.IsDBNull(((int)AccountColumn.ReferralStatus - 1)))?null:(System.String)reader[((int)AccountColumn.ReferralStatus - 1)];
					c.Custom1 = (reader.IsDBNull(((int)AccountColumn.Custom1 - 1)))?null:(System.String)reader[((int)AccountColumn.Custom1 - 1)];
					c.Custom2 = (reader.IsDBNull(((int)AccountColumn.Custom2 - 1)))?null:(System.String)reader[((int)AccountColumn.Custom2 - 1)];
					c.Custom3 = (reader.IsDBNull(((int)AccountColumn.Custom3 - 1)))?null:(System.String)reader[((int)AccountColumn.Custom3 - 1)];
					c.EmailOptIn = (System.Boolean)reader[((int)AccountColumn.EmailOptIn - 1)];
					c.WebServiceDownloadDte = (reader.IsDBNull(((int)AccountColumn.WebServiceDownloadDte - 1)))?null:(System.DateTime?)reader[((int)AccountColumn.WebServiceDownloadDte - 1)];
					c.ReferralCommission = (reader.IsDBNull(((int)AccountColumn.ReferralCommission - 1)))?null:(System.Decimal?)reader[((int)AccountColumn.ReferralCommission - 1)];
					c.ReferralCommissionTypeID = (reader.IsDBNull(((int)AccountColumn.ReferralCommissionTypeID - 1)))?null:(System.Int32?)reader[((int)AccountColumn.ReferralCommissionTypeID - 1)];
					c.TaxID = (reader.IsDBNull(((int)AccountColumn.TaxID - 1)))?null:(System.String)reader[((int)AccountColumn.TaxID - 1)];
					c.Email = (reader.IsDBNull(((int)AccountColumn.Email - 1)))?null:(System.String)reader[((int)AccountColumn.Email - 1)];
					c.EnableCustomerPricing = (reader.IsDBNull(((int)AccountColumn.EnableCustomerPricing - 1)))?null:(System.Boolean?)reader[((int)AccountColumn.EnableCustomerPricing - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.Account"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.Account"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.Account entity)
		{
			if (!reader.Read()) return;
			
			entity.AccountID = (System.Int32)reader[((int)AccountColumn.AccountID - 1)];
			entity.ParentAccountID = (reader.IsDBNull(((int)AccountColumn.ParentAccountID - 1)))?null:(System.Int32?)reader[((int)AccountColumn.ParentAccountID - 1)];
			entity.UserID = (reader.IsDBNull(((int)AccountColumn.UserID - 1)))?null:(System.Guid?)reader[((int)AccountColumn.UserID - 1)];
			entity.ExternalAccountNo = (reader.IsDBNull(((int)AccountColumn.ExternalAccountNo - 1)))?null:(System.String)reader[((int)AccountColumn.ExternalAccountNo - 1)];
			entity.CompanyName = (reader.IsDBNull(((int)AccountColumn.CompanyName - 1)))?null:(System.String)reader[((int)AccountColumn.CompanyName - 1)];
			entity.AccountTypeID = (reader.IsDBNull(((int)AccountColumn.AccountTypeID - 1)))?null:(System.Int32?)reader[((int)AccountColumn.AccountTypeID - 1)];
			entity.ProfileID = (reader.IsDBNull(((int)AccountColumn.ProfileID - 1)))?null:(System.Int32?)reader[((int)AccountColumn.ProfileID - 1)];
			entity.AccountProfileCode = (reader.IsDBNull(((int)AccountColumn.AccountProfileCode - 1)))?null:(System.String)reader[((int)AccountColumn.AccountProfileCode - 1)];
			entity.SubAccountLimit = (reader.IsDBNull(((int)AccountColumn.SubAccountLimit - 1)))?null:(System.Int32?)reader[((int)AccountColumn.SubAccountLimit - 1)];
			entity.Description = (reader.IsDBNull(((int)AccountColumn.Description - 1)))?null:(System.String)reader[((int)AccountColumn.Description - 1)];
			entity.CreateUser = (reader.IsDBNull(((int)AccountColumn.CreateUser - 1)))?null:(System.String)reader[((int)AccountColumn.CreateUser - 1)];
			entity.CreateDte = (System.DateTime)reader[((int)AccountColumn.CreateDte - 1)];
			entity.UpdateUser = (reader.IsDBNull(((int)AccountColumn.UpdateUser - 1)))?null:(System.String)reader[((int)AccountColumn.UpdateUser - 1)];
			entity.UpdateDte = (reader.IsDBNull(((int)AccountColumn.UpdateDte - 1)))?null:(System.DateTime?)reader[((int)AccountColumn.UpdateDte - 1)];
			entity.ActiveInd = (reader.IsDBNull(((int)AccountColumn.ActiveInd - 1)))?null:(System.Boolean?)reader[((int)AccountColumn.ActiveInd - 1)];
			entity.Website = (reader.IsDBNull(((int)AccountColumn.Website - 1)))?null:(System.String)reader[((int)AccountColumn.Website - 1)];
			entity.Source = (reader.IsDBNull(((int)AccountColumn.Source - 1)))?null:(System.String)reader[((int)AccountColumn.Source - 1)];
			entity.ReferralAccountID = (reader.IsDBNull(((int)AccountColumn.ReferralAccountID - 1)))?null:(System.Int32?)reader[((int)AccountColumn.ReferralAccountID - 1)];
			entity.ReferralStatus = (reader.IsDBNull(((int)AccountColumn.ReferralStatus - 1)))?null:(System.String)reader[((int)AccountColumn.ReferralStatus - 1)];
			entity.Custom1 = (reader.IsDBNull(((int)AccountColumn.Custom1 - 1)))?null:(System.String)reader[((int)AccountColumn.Custom1 - 1)];
			entity.Custom2 = (reader.IsDBNull(((int)AccountColumn.Custom2 - 1)))?null:(System.String)reader[((int)AccountColumn.Custom2 - 1)];
			entity.Custom3 = (reader.IsDBNull(((int)AccountColumn.Custom3 - 1)))?null:(System.String)reader[((int)AccountColumn.Custom3 - 1)];
			entity.EmailOptIn = (System.Boolean)reader[((int)AccountColumn.EmailOptIn - 1)];
			entity.WebServiceDownloadDte = (reader.IsDBNull(((int)AccountColumn.WebServiceDownloadDte - 1)))?null:(System.DateTime?)reader[((int)AccountColumn.WebServiceDownloadDte - 1)];
			entity.ReferralCommission = (reader.IsDBNull(((int)AccountColumn.ReferralCommission - 1)))?null:(System.Decimal?)reader[((int)AccountColumn.ReferralCommission - 1)];
			entity.ReferralCommissionTypeID = (reader.IsDBNull(((int)AccountColumn.ReferralCommissionTypeID - 1)))?null:(System.Int32?)reader[((int)AccountColumn.ReferralCommissionTypeID - 1)];
			entity.TaxID = (reader.IsDBNull(((int)AccountColumn.TaxID - 1)))?null:(System.String)reader[((int)AccountColumn.TaxID - 1)];
			entity.Email = (reader.IsDBNull(((int)AccountColumn.Email - 1)))?null:(System.String)reader[((int)AccountColumn.Email - 1)];
			entity.EnableCustomerPricing = (reader.IsDBNull(((int)AccountColumn.EnableCustomerPricing - 1)))?null:(System.Boolean?)reader[((int)AccountColumn.EnableCustomerPricing - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.Account"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.Account"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.Account entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.AccountID = (System.Int32)dataRow["AccountID"];
			entity.ParentAccountID = Convert.IsDBNull(dataRow["ParentAccountID"]) ? null : (System.Int32?)dataRow["ParentAccountID"];
			entity.UserID = Convert.IsDBNull(dataRow["UserID"]) ? null : (System.Guid?)dataRow["UserID"];
			entity.ExternalAccountNo = Convert.IsDBNull(dataRow["ExternalAccountNo"]) ? null : (System.String)dataRow["ExternalAccountNo"];
			entity.CompanyName = Convert.IsDBNull(dataRow["CompanyName"]) ? null : (System.String)dataRow["CompanyName"];
			entity.AccountTypeID = Convert.IsDBNull(dataRow["AccountTypeID"]) ? null : (System.Int32?)dataRow["AccountTypeID"];
			entity.ProfileID = Convert.IsDBNull(dataRow["ProfileID"]) ? null : (System.Int32?)dataRow["ProfileID"];
			entity.AccountProfileCode = Convert.IsDBNull(dataRow["AccountProfileCode"]) ? null : (System.String)dataRow["AccountProfileCode"];
			entity.SubAccountLimit = Convert.IsDBNull(dataRow["SubAccountLimit"]) ? null : (System.Int32?)dataRow["SubAccountLimit"];
			entity.Description = Convert.IsDBNull(dataRow["Description"]) ? null : (System.String)dataRow["Description"];
			entity.CreateUser = Convert.IsDBNull(dataRow["CreateUser"]) ? null : (System.String)dataRow["CreateUser"];
			entity.CreateDte = (System.DateTime)dataRow["CreateDte"];
			entity.UpdateUser = Convert.IsDBNull(dataRow["UpdateUser"]) ? null : (System.String)dataRow["UpdateUser"];
			entity.UpdateDte = Convert.IsDBNull(dataRow["UpdateDte"]) ? null : (System.DateTime?)dataRow["UpdateDte"];
			entity.ActiveInd = Convert.IsDBNull(dataRow["ActiveInd"]) ? null : (System.Boolean?)dataRow["ActiveInd"];
			entity.Website = Convert.IsDBNull(dataRow["Website"]) ? null : (System.String)dataRow["Website"];
			entity.Source = Convert.IsDBNull(dataRow["Source"]) ? null : (System.String)dataRow["Source"];
			entity.ReferralAccountID = Convert.IsDBNull(dataRow["ReferralAccountID"]) ? null : (System.Int32?)dataRow["ReferralAccountID"];
			entity.ReferralStatus = Convert.IsDBNull(dataRow["ReferralStatus"]) ? null : (System.String)dataRow["ReferralStatus"];
			entity.Custom1 = Convert.IsDBNull(dataRow["Custom1"]) ? null : (System.String)dataRow["Custom1"];
			entity.Custom2 = Convert.IsDBNull(dataRow["Custom2"]) ? null : (System.String)dataRow["Custom2"];
			entity.Custom3 = Convert.IsDBNull(dataRow["Custom3"]) ? null : (System.String)dataRow["Custom3"];
			entity.EmailOptIn = (System.Boolean)dataRow["EmailOptIn"];
			entity.WebServiceDownloadDte = Convert.IsDBNull(dataRow["WebServiceDownloadDte"]) ? null : (System.DateTime?)dataRow["WebServiceDownloadDte"];
			entity.ReferralCommission = Convert.IsDBNull(dataRow["ReferralCommission"]) ? null : (System.Decimal?)dataRow["ReferralCommission"];
			entity.ReferralCommissionTypeID = Convert.IsDBNull(dataRow["ReferralCommissionTypeID"]) ? null : (System.Int32?)dataRow["ReferralCommissionTypeID"];
			entity.TaxID = Convert.IsDBNull(dataRow["TaxID"]) ? null : (System.String)dataRow["TaxID"];
			entity.Email = Convert.IsDBNull(dataRow["Email"]) ? null : (System.String)dataRow["Email"];
			entity.EnableCustomerPricing = Convert.IsDBNull(dataRow["EnableCustomerPricing"]) ? null : (System.Boolean?)dataRow["EnableCustomerPricing"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.Account"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.Account Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.Account entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region ParentAccountIDSource	
			if (CanDeepLoad(entity, "Account|ParentAccountIDSource", deepLoadType, innerList) 
				&& entity.ParentAccountIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.ParentAccountID ?? (int)0);
				Account tmpEntity = EntityManager.LocateEntity<Account>(EntityLocator.ConstructKeyFromPkItems(typeof(Account), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ParentAccountIDSource = tmpEntity;
				else
					entity.ParentAccountIDSource = DataRepository.AccountProvider.GetByAccountID(transactionManager, (entity.ParentAccountID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ParentAccountIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ParentAccountIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.AccountProvider.DeepLoad(transactionManager, entity.ParentAccountIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ParentAccountIDSource

			#region AccountTypeIDSource	
			if (CanDeepLoad(entity, "AccountType|AccountTypeIDSource", deepLoadType, innerList) 
				&& entity.AccountTypeIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.AccountTypeID ?? (int)0);
				AccountType tmpEntity = EntityManager.LocateEntity<AccountType>(EntityLocator.ConstructKeyFromPkItems(typeof(AccountType), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.AccountTypeIDSource = tmpEntity;
				else
					entity.AccountTypeIDSource = DataRepository.AccountTypeProvider.GetByAccountTypeID(transactionManager, (entity.AccountTypeID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AccountTypeIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.AccountTypeIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.AccountTypeProvider.DeepLoad(transactionManager, entity.AccountTypeIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion AccountTypeIDSource

			#region ReferralAccountIDSource	
			if (CanDeepLoad(entity, "Account|ReferralAccountIDSource", deepLoadType, innerList) 
				&& entity.ReferralAccountIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.ReferralAccountID ?? (int)0);
				Account tmpEntity = EntityManager.LocateEntity<Account>(EntityLocator.ConstructKeyFromPkItems(typeof(Account), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ReferralAccountIDSource = tmpEntity;
				else
					entity.ReferralAccountIDSource = DataRepository.AccountProvider.GetByAccountID(transactionManager, (entity.ReferralAccountID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ReferralAccountIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ReferralAccountIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.AccountProvider.DeepLoad(transactionManager, entity.ReferralAccountIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ReferralAccountIDSource

			#region ReferralCommissionTypeIDSource	
			if (CanDeepLoad(entity, "ReferralCommissionType|ReferralCommissionTypeIDSource", deepLoadType, innerList) 
				&& entity.ReferralCommissionTypeIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.ReferralCommissionTypeID ?? (int)0);
				ReferralCommissionType tmpEntity = EntityManager.LocateEntity<ReferralCommissionType>(EntityLocator.ConstructKeyFromPkItems(typeof(ReferralCommissionType), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ReferralCommissionTypeIDSource = tmpEntity;
				else
					entity.ReferralCommissionTypeIDSource = DataRepository.ReferralCommissionTypeProvider.GetByReferralCommissionTypeID(transactionManager, (entity.ReferralCommissionTypeID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ReferralCommissionTypeIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ReferralCommissionTypeIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.ReferralCommissionTypeProvider.DeepLoad(transactionManager, entity.ReferralCommissionTypeIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ReferralCommissionTypeIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByAccountID methods when available
			
			#region AccountCollectionGetByReferralAccountID
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Account>|AccountCollectionGetByReferralAccountID", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AccountCollectionGetByReferralAccountID' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.AccountCollectionGetByReferralAccountID = DataRepository.AccountProvider.GetByReferralAccountID(transactionManager, entity.AccountID);

				if (deep && entity.AccountCollectionGetByReferralAccountID.Count > 0)
				{
					deepHandles.Add("AccountCollectionGetByReferralAccountID",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Account>) DataRepository.AccountProvider.DeepLoad,
						new object[] { transactionManager, entity.AccountCollectionGetByReferralAccountID, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region CookieMappingCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<CookieMapping>|CookieMappingCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CookieMappingCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CookieMappingCollection = DataRepository.CookieMappingProvider.GetByAccountID(transactionManager, entity.AccountID);

				if (deep && entity.CookieMappingCollection.Count > 0)
				{
					deepHandles.Add("CookieMappingCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<CookieMapping>) DataRepository.CookieMappingProvider.DeepLoad,
						new object[] { transactionManager, entity.CookieMappingCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region TrackingEventCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<TrackingEvent>|TrackingEventCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'TrackingEventCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.TrackingEventCollection = DataRepository.TrackingEventProvider.GetByAccountID(transactionManager, entity.AccountID);

				if (deep && entity.TrackingEventCollection.Count > 0)
				{
					deepHandles.Add("TrackingEventCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<TrackingEvent>) DataRepository.TrackingEventProvider.DeepLoad,
						new object[] { transactionManager, entity.TrackingEventCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region ReviewCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Review>|ReviewCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ReviewCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ReviewCollection = DataRepository.ReviewProvider.GetByAccountID(transactionManager, entity.AccountID);

				if (deep && entity.ReviewCollection.Count > 0)
				{
					deepHandles.Add("ReviewCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Review>) DataRepository.ReviewProvider.DeepLoad,
						new object[] { transactionManager, entity.ReviewCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region WishListCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<WishList>|WishListCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'WishListCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.WishListCollection = DataRepository.WishListProvider.GetByAccountID(transactionManager, entity.AccountID);

				if (deep && entity.WishListCollection.Count > 0)
				{
					deepHandles.Add("WishListCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<WishList>) DataRepository.WishListProvider.DeepLoad,
						new object[] { transactionManager, entity.WishListCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region StoreCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Store>|StoreCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'StoreCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.StoreCollection = DataRepository.StoreProvider.GetByAccountID(transactionManager, entity.AccountID);

				if (deep && entity.StoreCollection.Count > 0)
				{
					deepHandles.Add("StoreCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Store>) DataRepository.StoreProvider.DeepLoad,
						new object[] { transactionManager, entity.StoreCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region OrderCollectionGetByReferralAccountID
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Order>|OrderCollectionGetByReferralAccountID", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'OrderCollectionGetByReferralAccountID' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.OrderCollectionGetByReferralAccountID = DataRepository.OrderProvider.GetByReferralAccountID(transactionManager, entity.AccountID);

				if (deep && entity.OrderCollectionGetByReferralAccountID.Count > 0)
				{
					deepHandles.Add("OrderCollectionGetByReferralAccountID",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Order>) DataRepository.OrderProvider.DeepLoad,
						new object[] { transactionManager, entity.OrderCollectionGetByReferralAccountID, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region AccountPaymentCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<AccountPayment>|AccountPaymentCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AccountPaymentCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.AccountPaymentCollection = DataRepository.AccountPaymentProvider.GetByAccountID(transactionManager, entity.AccountID);

				if (deep && entity.AccountPaymentCollection.Count > 0)
				{
					deepHandles.Add("AccountPaymentCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<AccountPayment>) DataRepository.AccountPaymentProvider.DeepLoad,
						new object[] { transactionManager, entity.AccountPaymentCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region ProductReviewHistoryCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<ProductReviewHistory>|ProductReviewHistoryCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ProductReviewHistoryCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ProductReviewHistoryCollection = DataRepository.ProductReviewHistoryProvider.GetByVendorID(transactionManager, entity.AccountID);

				if (deep && entity.ProductReviewHistoryCollection.Count > 0)
				{
					deepHandles.Add("ProductReviewHistoryCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<ProductReviewHistory>) DataRepository.ProductReviewHistoryProvider.DeepLoad,
						new object[] { transactionManager, entity.ProductReviewHistoryCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region CaseRequestCollectionGetByAccountID
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<CaseRequest>|CaseRequestCollectionGetByAccountID", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CaseRequestCollectionGetByAccountID' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CaseRequestCollectionGetByAccountID = DataRepository.CaseRequestProvider.GetByAccountID(transactionManager, entity.AccountID);

				if (deep && entity.CaseRequestCollectionGetByAccountID.Count > 0)
				{
					deepHandles.Add("CaseRequestCollectionGetByAccountID",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<CaseRequest>) DataRepository.CaseRequestProvider.DeepLoad,
						new object[] { transactionManager, entity.CaseRequestCollectionGetByAccountID, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region AccountCollectionGetByParentAccountID
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Account>|AccountCollectionGetByParentAccountID", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AccountCollectionGetByParentAccountID' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.AccountCollectionGetByParentAccountID = DataRepository.AccountProvider.GetByParentAccountID(transactionManager, entity.AccountID);

				if (deep && entity.AccountCollectionGetByParentAccountID.Count > 0)
				{
					deepHandles.Add("AccountCollectionGetByParentAccountID",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Account>) DataRepository.AccountProvider.DeepLoad,
						new object[] { transactionManager, entity.AccountCollectionGetByParentAccountID, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region AddOnCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<AddOn>|AddOnCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AddOnCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.AddOnCollection = DataRepository.AddOnProvider.GetByAccountID(transactionManager, entity.AccountID);

				if (deep && entity.AddOnCollection.Count > 0)
				{
					deepHandles.Add("AddOnCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<AddOn>) DataRepository.AddOnProvider.DeepLoad,
						new object[] { transactionManager, entity.AddOnCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region OrderCollectionGetByAccountID
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Order>|OrderCollectionGetByAccountID", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'OrderCollectionGetByAccountID' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.OrderCollectionGetByAccountID = DataRepository.OrderProvider.GetByAccountID(transactionManager, entity.AccountID);

				if (deep && entity.OrderCollectionGetByAccountID.Count > 0)
				{
					deepHandles.Add("OrderCollectionGetByAccountID",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Order>) DataRepository.OrderProvider.DeepLoad,
						new object[] { transactionManager, entity.OrderCollectionGetByAccountID, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region ProductCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Product>|ProductCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ProductCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ProductCollection = DataRepository.ProductProvider.GetByAccountID(transactionManager, entity.AccountID);

				if (deep && entity.ProductCollection.Count > 0)
				{
					deepHandles.Add("ProductCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Product>) DataRepository.ProductProvider.DeepLoad,
						new object[] { transactionManager, entity.ProductCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region AddressCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Address>|AddressCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AddressCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.AddressCollection = DataRepository.AddressProvider.GetByAccountID(transactionManager, entity.AccountID);

				if (deep && entity.AddressCollection.Count > 0)
				{
					deepHandles.Add("AddressCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Address>) DataRepository.AddressProvider.DeepLoad,
						new object[] { transactionManager, entity.AddressCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region AccountProfileCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<AccountProfile>|AccountProfileCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AccountProfileCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.AccountProfileCollection = DataRepository.AccountProfileProvider.GetByAccountID(transactionManager, entity.AccountID);

				if (deep && entity.AccountProfileCollection.Count > 0)
				{
					deepHandles.Add("AccountProfileCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<AccountProfile>) DataRepository.AccountProfileProvider.DeepLoad,
						new object[] { transactionManager, entity.AccountProfileCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region CaseRequestCollectionGetByOwnerAccountID
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<CaseRequest>|CaseRequestCollectionGetByOwnerAccountID", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CaseRequestCollectionGetByOwnerAccountID' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CaseRequestCollectionGetByOwnerAccountID = DataRepository.CaseRequestProvider.GetByOwnerAccountID(transactionManager, entity.AccountID);

				if (deep && entity.CaseRequestCollectionGetByOwnerAccountID.Count > 0)
				{
					deepHandles.Add("CaseRequestCollectionGetByOwnerAccountID",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<CaseRequest>) DataRepository.CaseRequestProvider.DeepLoad,
						new object[] { transactionManager, entity.CaseRequestCollectionGetByOwnerAccountID, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region PromotionCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Promotion>|PromotionCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PromotionCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.PromotionCollection = DataRepository.PromotionProvider.GetByAccountID(transactionManager, entity.AccountID);

				if (deep && entity.PromotionCollection.Count > 0)
				{
					deepHandles.Add("PromotionCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Promotion>) DataRepository.PromotionProvider.DeepLoad,
						new object[] { transactionManager, entity.PromotionCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region PaymentTokenCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<PaymentToken>|PaymentTokenCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PaymentTokenCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.PaymentTokenCollection = DataRepository.PaymentTokenProvider.GetByAccountID(transactionManager, entity.AccountID);

				if (deep && entity.PaymentTokenCollection.Count > 0)
				{
					deepHandles.Add("PaymentTokenCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<PaymentToken>) DataRepository.PaymentTokenProvider.DeepLoad,
						new object[] { transactionManager, entity.PaymentTokenCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region NoteCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Note>|NoteCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'NoteCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.NoteCollection = DataRepository.NoteProvider.GetByAccountID(transactionManager, entity.AccountID);

				if (deep && entity.NoteCollection.Count > 0)
				{
					deepHandles.Add("NoteCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Note>) DataRepository.NoteProvider.DeepLoad,
						new object[] { transactionManager, entity.NoteCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region GiftCardCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<GiftCard>|GiftCardCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'GiftCardCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.GiftCardCollection = DataRepository.GiftCardProvider.GetByAccountId(transactionManager, entity.AccountID);

				if (deep && entity.GiftCardCollection.Count > 0)
				{
					deepHandles.Add("GiftCardCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<GiftCard>) DataRepository.GiftCardProvider.DeepLoad,
						new object[] { transactionManager, entity.GiftCardCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.Account object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.Account instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.Account Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.Account entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region ParentAccountIDSource
			if (CanDeepSave(entity, "Account|ParentAccountIDSource", deepSaveType, innerList) 
				&& entity.ParentAccountIDSource != null)
			{
				DataRepository.AccountProvider.Save(transactionManager, entity.ParentAccountIDSource);
				entity.ParentAccountID = entity.ParentAccountIDSource.AccountID;
			}
			#endregion 
			
			#region AccountTypeIDSource
			if (CanDeepSave(entity, "AccountType|AccountTypeIDSource", deepSaveType, innerList) 
				&& entity.AccountTypeIDSource != null)
			{
				DataRepository.AccountTypeProvider.Save(transactionManager, entity.AccountTypeIDSource);
				entity.AccountTypeID = entity.AccountTypeIDSource.AccountTypeID;
			}
			#endregion 
			
			#region ReferralAccountIDSource
			if (CanDeepSave(entity, "Account|ReferralAccountIDSource", deepSaveType, innerList) 
				&& entity.ReferralAccountIDSource != null)
			{
				DataRepository.AccountProvider.Save(transactionManager, entity.ReferralAccountIDSource);
				entity.ReferralAccountID = entity.ReferralAccountIDSource.AccountID;
			}
			#endregion 
			
			#region ReferralCommissionTypeIDSource
			if (CanDeepSave(entity, "ReferralCommissionType|ReferralCommissionTypeIDSource", deepSaveType, innerList) 
				&& entity.ReferralCommissionTypeIDSource != null)
			{
				DataRepository.ReferralCommissionTypeProvider.Save(transactionManager, entity.ReferralCommissionTypeIDSource);
				entity.ReferralCommissionTypeID = entity.ReferralCommissionTypeIDSource.ReferralCommissionTypeID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<Account>
				if (CanDeepSave(entity.AccountCollectionGetByReferralAccountID, "List<Account>|AccountCollectionGetByReferralAccountID", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Account child in entity.AccountCollectionGetByReferralAccountID)
					{
						if(child.ReferralAccountIDSource != null)
						{
							child.ReferralAccountID = child.ReferralAccountIDSource.AccountID;
						}
						else
						{
							child.ReferralAccountID = entity.AccountID;
						}

					}

					if (entity.AccountCollectionGetByReferralAccountID.Count > 0 || entity.AccountCollectionGetByReferralAccountID.DeletedItems.Count > 0)
					{
						//DataRepository.AccountProvider.Save(transactionManager, entity.AccountCollectionGetByReferralAccountID);
						
						deepHandles.Add("AccountCollectionGetByReferralAccountID",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Account >) DataRepository.AccountProvider.DeepSave,
							new object[] { transactionManager, entity.AccountCollectionGetByReferralAccountID, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<CookieMapping>
				if (CanDeepSave(entity.CookieMappingCollection, "List<CookieMapping>|CookieMappingCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(CookieMapping child in entity.CookieMappingCollection)
					{
						if(child.AccountIDSource != null)
						{
							child.AccountID = child.AccountIDSource.AccountID;
						}
						else
						{
							child.AccountID = entity.AccountID;
						}

					}

					if (entity.CookieMappingCollection.Count > 0 || entity.CookieMappingCollection.DeletedItems.Count > 0)
					{
						//DataRepository.CookieMappingProvider.Save(transactionManager, entity.CookieMappingCollection);
						
						deepHandles.Add("CookieMappingCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< CookieMapping >) DataRepository.CookieMappingProvider.DeepSave,
							new object[] { transactionManager, entity.CookieMappingCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<TrackingEvent>
				if (CanDeepSave(entity.TrackingEventCollection, "List<TrackingEvent>|TrackingEventCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(TrackingEvent child in entity.TrackingEventCollection)
					{
						if(child.AccountIDSource != null)
						{
							child.AccountID = child.AccountIDSource.AccountID;
						}
						else
						{
							child.AccountID = entity.AccountID;
						}

					}

					if (entity.TrackingEventCollection.Count > 0 || entity.TrackingEventCollection.DeletedItems.Count > 0)
					{
						//DataRepository.TrackingEventProvider.Save(transactionManager, entity.TrackingEventCollection);
						
						deepHandles.Add("TrackingEventCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< TrackingEvent >) DataRepository.TrackingEventProvider.DeepSave,
							new object[] { transactionManager, entity.TrackingEventCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Review>
				if (CanDeepSave(entity.ReviewCollection, "List<Review>|ReviewCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Review child in entity.ReviewCollection)
					{
						if(child.AccountIDSource != null)
						{
							child.AccountID = child.AccountIDSource.AccountID;
						}
						else
						{
							child.AccountID = entity.AccountID;
						}

					}

					if (entity.ReviewCollection.Count > 0 || entity.ReviewCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ReviewProvider.Save(transactionManager, entity.ReviewCollection);
						
						deepHandles.Add("ReviewCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Review >) DataRepository.ReviewProvider.DeepSave,
							new object[] { transactionManager, entity.ReviewCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<WishList>
				if (CanDeepSave(entity.WishListCollection, "List<WishList>|WishListCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(WishList child in entity.WishListCollection)
					{
						if(child.AccountIDSource != null)
						{
							child.AccountID = child.AccountIDSource.AccountID;
						}
						else
						{
							child.AccountID = entity.AccountID;
						}

					}

					if (entity.WishListCollection.Count > 0 || entity.WishListCollection.DeletedItems.Count > 0)
					{
						//DataRepository.WishListProvider.Save(transactionManager, entity.WishListCollection);
						
						deepHandles.Add("WishListCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< WishList >) DataRepository.WishListProvider.DeepSave,
							new object[] { transactionManager, entity.WishListCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Store>
				if (CanDeepSave(entity.StoreCollection, "List<Store>|StoreCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Store child in entity.StoreCollection)
					{
						if(child.AccountIDSource != null)
						{
							child.AccountID = child.AccountIDSource.AccountID;
						}
						else
						{
							child.AccountID = entity.AccountID;
						}

					}

					if (entity.StoreCollection.Count > 0 || entity.StoreCollection.DeletedItems.Count > 0)
					{
						//DataRepository.StoreProvider.Save(transactionManager, entity.StoreCollection);
						
						deepHandles.Add("StoreCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Store >) DataRepository.StoreProvider.DeepSave,
							new object[] { transactionManager, entity.StoreCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Order>
				if (CanDeepSave(entity.OrderCollectionGetByReferralAccountID, "List<Order>|OrderCollectionGetByReferralAccountID", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Order child in entity.OrderCollectionGetByReferralAccountID)
					{
						if(child.ReferralAccountIDSource != null)
						{
							child.ReferralAccountID = child.ReferralAccountIDSource.AccountID;
						}
						else
						{
							child.ReferralAccountID = entity.AccountID;
						}

					}

					if (entity.OrderCollectionGetByReferralAccountID.Count > 0 || entity.OrderCollectionGetByReferralAccountID.DeletedItems.Count > 0)
					{
						//DataRepository.OrderProvider.Save(transactionManager, entity.OrderCollectionGetByReferralAccountID);
						
						deepHandles.Add("OrderCollectionGetByReferralAccountID",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Order >) DataRepository.OrderProvider.DeepSave,
							new object[] { transactionManager, entity.OrderCollectionGetByReferralAccountID, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<AccountPayment>
				if (CanDeepSave(entity.AccountPaymentCollection, "List<AccountPayment>|AccountPaymentCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(AccountPayment child in entity.AccountPaymentCollection)
					{
						if(child.AccountIDSource != null)
						{
							child.AccountID = child.AccountIDSource.AccountID;
						}
						else
						{
							child.AccountID = entity.AccountID;
						}

					}

					if (entity.AccountPaymentCollection.Count > 0 || entity.AccountPaymentCollection.DeletedItems.Count > 0)
					{
						//DataRepository.AccountPaymentProvider.Save(transactionManager, entity.AccountPaymentCollection);
						
						deepHandles.Add("AccountPaymentCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< AccountPayment >) DataRepository.AccountPaymentProvider.DeepSave,
							new object[] { transactionManager, entity.AccountPaymentCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<ProductReviewHistory>
				if (CanDeepSave(entity.ProductReviewHistoryCollection, "List<ProductReviewHistory>|ProductReviewHistoryCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(ProductReviewHistory child in entity.ProductReviewHistoryCollection)
					{
						if(child.VendorIDSource != null)
						{
							child.VendorID = child.VendorIDSource.AccountID;
						}
						else
						{
							child.VendorID = entity.AccountID;
						}

					}

					if (entity.ProductReviewHistoryCollection.Count > 0 || entity.ProductReviewHistoryCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ProductReviewHistoryProvider.Save(transactionManager, entity.ProductReviewHistoryCollection);
						
						deepHandles.Add("ProductReviewHistoryCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< ProductReviewHistory >) DataRepository.ProductReviewHistoryProvider.DeepSave,
							new object[] { transactionManager, entity.ProductReviewHistoryCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<CaseRequest>
				if (CanDeepSave(entity.CaseRequestCollectionGetByAccountID, "List<CaseRequest>|CaseRequestCollectionGetByAccountID", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(CaseRequest child in entity.CaseRequestCollectionGetByAccountID)
					{
						if(child.AccountIDSource != null)
						{
							child.AccountID = child.AccountIDSource.AccountID;
						}
						else
						{
							child.AccountID = entity.AccountID;
						}

					}

					if (entity.CaseRequestCollectionGetByAccountID.Count > 0 || entity.CaseRequestCollectionGetByAccountID.DeletedItems.Count > 0)
					{
						//DataRepository.CaseRequestProvider.Save(transactionManager, entity.CaseRequestCollectionGetByAccountID);
						
						deepHandles.Add("CaseRequestCollectionGetByAccountID",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< CaseRequest >) DataRepository.CaseRequestProvider.DeepSave,
							new object[] { transactionManager, entity.CaseRequestCollectionGetByAccountID, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Account>
				if (CanDeepSave(entity.AccountCollectionGetByParentAccountID, "List<Account>|AccountCollectionGetByParentAccountID", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Account child in entity.AccountCollectionGetByParentAccountID)
					{
						if(child.ParentAccountIDSource != null)
						{
							child.ParentAccountID = child.ParentAccountIDSource.AccountID;
						}
						else
						{
							child.ParentAccountID = entity.AccountID;
						}

					}

					if (entity.AccountCollectionGetByParentAccountID.Count > 0 || entity.AccountCollectionGetByParentAccountID.DeletedItems.Count > 0)
					{
						//DataRepository.AccountProvider.Save(transactionManager, entity.AccountCollectionGetByParentAccountID);
						
						deepHandles.Add("AccountCollectionGetByParentAccountID",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Account >) DataRepository.AccountProvider.DeepSave,
							new object[] { transactionManager, entity.AccountCollectionGetByParentAccountID, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<AddOn>
				if (CanDeepSave(entity.AddOnCollection, "List<AddOn>|AddOnCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(AddOn child in entity.AddOnCollection)
					{
						if(child.AccountIDSource != null)
						{
							child.AccountID = child.AccountIDSource.AccountID;
						}
						else
						{
							child.AccountID = entity.AccountID;
						}

					}

					if (entity.AddOnCollection.Count > 0 || entity.AddOnCollection.DeletedItems.Count > 0)
					{
						//DataRepository.AddOnProvider.Save(transactionManager, entity.AddOnCollection);
						
						deepHandles.Add("AddOnCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< AddOn >) DataRepository.AddOnProvider.DeepSave,
							new object[] { transactionManager, entity.AddOnCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Order>
				if (CanDeepSave(entity.OrderCollectionGetByAccountID, "List<Order>|OrderCollectionGetByAccountID", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Order child in entity.OrderCollectionGetByAccountID)
					{
						if(child.AccountIDSource != null)
						{
							child.AccountID = child.AccountIDSource.AccountID;
						}
						else
						{
							child.AccountID = entity.AccountID;
						}

					}

					if (entity.OrderCollectionGetByAccountID.Count > 0 || entity.OrderCollectionGetByAccountID.DeletedItems.Count > 0)
					{
						//DataRepository.OrderProvider.Save(transactionManager, entity.OrderCollectionGetByAccountID);
						
						deepHandles.Add("OrderCollectionGetByAccountID",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Order >) DataRepository.OrderProvider.DeepSave,
							new object[] { transactionManager, entity.OrderCollectionGetByAccountID, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Product>
				if (CanDeepSave(entity.ProductCollection, "List<Product>|ProductCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Product child in entity.ProductCollection)
					{
						if(child.AccountIDSource != null)
						{
							child.AccountID = child.AccountIDSource.AccountID;
						}
						else
						{
							child.AccountID = entity.AccountID;
						}

					}

					if (entity.ProductCollection.Count > 0 || entity.ProductCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ProductProvider.Save(transactionManager, entity.ProductCollection);
						
						deepHandles.Add("ProductCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Product >) DataRepository.ProductProvider.DeepSave,
							new object[] { transactionManager, entity.ProductCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Address>
				if (CanDeepSave(entity.AddressCollection, "List<Address>|AddressCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Address child in entity.AddressCollection)
					{
						if(child.AccountIDSource != null)
						{
							child.AccountID = child.AccountIDSource.AccountID;
						}
						else
						{
							child.AccountID = entity.AccountID;
						}

					}

					if (entity.AddressCollection.Count > 0 || entity.AddressCollection.DeletedItems.Count > 0)
					{
						//DataRepository.AddressProvider.Save(transactionManager, entity.AddressCollection);
						
						deepHandles.Add("AddressCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Address >) DataRepository.AddressProvider.DeepSave,
							new object[] { transactionManager, entity.AddressCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<AccountProfile>
				if (CanDeepSave(entity.AccountProfileCollection, "List<AccountProfile>|AccountProfileCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(AccountProfile child in entity.AccountProfileCollection)
					{
						if(child.AccountIDSource != null)
						{
							child.AccountID = child.AccountIDSource.AccountID;
						}
						else
						{
							child.AccountID = entity.AccountID;
						}

					}

					if (entity.AccountProfileCollection.Count > 0 || entity.AccountProfileCollection.DeletedItems.Count > 0)
					{
						//DataRepository.AccountProfileProvider.Save(transactionManager, entity.AccountProfileCollection);
						
						deepHandles.Add("AccountProfileCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< AccountProfile >) DataRepository.AccountProfileProvider.DeepSave,
							new object[] { transactionManager, entity.AccountProfileCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<CaseRequest>
				if (CanDeepSave(entity.CaseRequestCollectionGetByOwnerAccountID, "List<CaseRequest>|CaseRequestCollectionGetByOwnerAccountID", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(CaseRequest child in entity.CaseRequestCollectionGetByOwnerAccountID)
					{
						if(child.OwnerAccountIDSource != null)
						{
							child.OwnerAccountID = child.OwnerAccountIDSource.AccountID;
						}
						else
						{
							child.OwnerAccountID = entity.AccountID;
						}

					}

					if (entity.CaseRequestCollectionGetByOwnerAccountID.Count > 0 || entity.CaseRequestCollectionGetByOwnerAccountID.DeletedItems.Count > 0)
					{
						//DataRepository.CaseRequestProvider.Save(transactionManager, entity.CaseRequestCollectionGetByOwnerAccountID);
						
						deepHandles.Add("CaseRequestCollectionGetByOwnerAccountID",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< CaseRequest >) DataRepository.CaseRequestProvider.DeepSave,
							new object[] { transactionManager, entity.CaseRequestCollectionGetByOwnerAccountID, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Promotion>
				if (CanDeepSave(entity.PromotionCollection, "List<Promotion>|PromotionCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Promotion child in entity.PromotionCollection)
					{
						if(child.AccountIDSource != null)
						{
							child.AccountID = child.AccountIDSource.AccountID;
						}
						else
						{
							child.AccountID = entity.AccountID;
						}

					}

					if (entity.PromotionCollection.Count > 0 || entity.PromotionCollection.DeletedItems.Count > 0)
					{
						//DataRepository.PromotionProvider.Save(transactionManager, entity.PromotionCollection);
						
						deepHandles.Add("PromotionCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Promotion >) DataRepository.PromotionProvider.DeepSave,
							new object[] { transactionManager, entity.PromotionCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<PaymentToken>
				if (CanDeepSave(entity.PaymentTokenCollection, "List<PaymentToken>|PaymentTokenCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(PaymentToken child in entity.PaymentTokenCollection)
					{
						if(child.AccountIDSource != null)
						{
							child.AccountID = child.AccountIDSource.AccountID;
						}
						else
						{
							child.AccountID = entity.AccountID;
						}

					}

					if (entity.PaymentTokenCollection.Count > 0 || entity.PaymentTokenCollection.DeletedItems.Count > 0)
					{
						//DataRepository.PaymentTokenProvider.Save(transactionManager, entity.PaymentTokenCollection);
						
						deepHandles.Add("PaymentTokenCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< PaymentToken >) DataRepository.PaymentTokenProvider.DeepSave,
							new object[] { transactionManager, entity.PaymentTokenCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Note>
				if (CanDeepSave(entity.NoteCollection, "List<Note>|NoteCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Note child in entity.NoteCollection)
					{
						if(child.AccountIDSource != null)
						{
							child.AccountID = child.AccountIDSource.AccountID;
						}
						else
						{
							child.AccountID = entity.AccountID;
						}

					}

					if (entity.NoteCollection.Count > 0 || entity.NoteCollection.DeletedItems.Count > 0)
					{
						//DataRepository.NoteProvider.Save(transactionManager, entity.NoteCollection);
						
						deepHandles.Add("NoteCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Note >) DataRepository.NoteProvider.DeepSave,
							new object[] { transactionManager, entity.NoteCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<GiftCard>
				if (CanDeepSave(entity.GiftCardCollection, "List<GiftCard>|GiftCardCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(GiftCard child in entity.GiftCardCollection)
					{
						if(child.AccountIdSource != null)
						{
							child.AccountId = child.AccountIdSource.AccountID;
						}
						else
						{
							child.AccountId = entity.AccountID;
						}

					}

					if (entity.GiftCardCollection.Count > 0 || entity.GiftCardCollection.DeletedItems.Count > 0)
					{
						//DataRepository.GiftCardProvider.Save(transactionManager, entity.GiftCardCollection);
						
						deepHandles.Add("GiftCardCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< GiftCard >) DataRepository.GiftCardProvider.DeepSave,
							new object[] { transactionManager, entity.GiftCardCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region AccountChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.Account</c>
	///</summary>
	public enum AccountChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Account</c> at ParentAccountIDSource
		///</summary>
		[ChildEntityType(typeof(Account))]
		Account,
		
		///<summary>
		/// Composite Property for <c>AccountType</c> at AccountTypeIDSource
		///</summary>
		[ChildEntityType(typeof(AccountType))]
		AccountType,
		
		///<summary>
		/// Composite Property for <c>ReferralCommissionType</c> at ReferralCommissionTypeIDSource
		///</summary>
		[ChildEntityType(typeof(ReferralCommissionType))]
		ReferralCommissionType,
		///<summary>
		/// Collection of <c>Account</c> as OneToMany for AccountCollection
		///</summary>
		[ChildEntityType(typeof(TList<Account>))]
		AccountCollectionGetByReferralAccountID,
		///<summary>
		/// Collection of <c>Account</c> as OneToMany for CookieMappingCollection
		///</summary>
		[ChildEntityType(typeof(TList<CookieMapping>))]
		CookieMappingCollection,
		///<summary>
		/// Collection of <c>Account</c> as OneToMany for TrackingEventCollection
		///</summary>
		[ChildEntityType(typeof(TList<TrackingEvent>))]
		TrackingEventCollection,
		///<summary>
		/// Collection of <c>Account</c> as OneToMany for ReviewCollection
		///</summary>
		[ChildEntityType(typeof(TList<Review>))]
		ReviewCollection,
		///<summary>
		/// Collection of <c>Account</c> as OneToMany for WishListCollection
		///</summary>
		[ChildEntityType(typeof(TList<WishList>))]
		WishListCollection,
		///<summary>
		/// Collection of <c>Account</c> as OneToMany for StoreCollection
		///</summary>
		[ChildEntityType(typeof(TList<Store>))]
		StoreCollection,
		///<summary>
		/// Collection of <c>Account</c> as OneToMany for OrderCollection
		///</summary>
		[ChildEntityType(typeof(TList<Order>))]
		OrderCollectionGetByReferralAccountID,
		///<summary>
		/// Collection of <c>Account</c> as OneToMany for AccountPaymentCollection
		///</summary>
		[ChildEntityType(typeof(TList<AccountPayment>))]
		AccountPaymentCollection,
		///<summary>
		/// Collection of <c>Account</c> as OneToMany for ProductReviewHistoryCollection
		///</summary>
		[ChildEntityType(typeof(TList<ProductReviewHistory>))]
		ProductReviewHistoryCollection,
		///<summary>
		/// Collection of <c>Account</c> as OneToMany for CaseRequestCollection
		///</summary>
		[ChildEntityType(typeof(TList<CaseRequest>))]
		CaseRequestCollectionGetByAccountID,
		///<summary>
		/// Collection of <c>Account</c> as OneToMany for AccountCollection
		///</summary>
		[ChildEntityType(typeof(TList<Account>))]
		AccountCollectionGetByParentAccountID,
		///<summary>
		/// Collection of <c>Account</c> as OneToMany for AddOnCollection
		///</summary>
		[ChildEntityType(typeof(TList<AddOn>))]
		AddOnCollection,
		///<summary>
		/// Collection of <c>Account</c> as OneToMany for OrderCollection
		///</summary>
		[ChildEntityType(typeof(TList<Order>))]
		OrderCollectionGetByAccountID,
		///<summary>
		/// Collection of <c>Account</c> as OneToMany for ProductCollection
		///</summary>
		[ChildEntityType(typeof(TList<Product>))]
		ProductCollection,
		///<summary>
		/// Collection of <c>Account</c> as OneToMany for AddressCollection
		///</summary>
		[ChildEntityType(typeof(TList<Address>))]
		AddressCollection,
		///<summary>
		/// Collection of <c>Account</c> as OneToMany for AccountProfileCollection
		///</summary>
		[ChildEntityType(typeof(TList<AccountProfile>))]
		AccountProfileCollection,
		///<summary>
		/// Collection of <c>Account</c> as OneToMany for CaseRequestCollection
		///</summary>
		[ChildEntityType(typeof(TList<CaseRequest>))]
		CaseRequestCollectionGetByOwnerAccountID,
		///<summary>
		/// Collection of <c>Account</c> as OneToMany for PromotionCollection
		///</summary>
		[ChildEntityType(typeof(TList<Promotion>))]
		PromotionCollection,
		///<summary>
		/// Collection of <c>Account</c> as OneToMany for PaymentTokenCollection
		///</summary>
		[ChildEntityType(typeof(TList<PaymentToken>))]
		PaymentTokenCollection,
		///<summary>
		/// Collection of <c>Account</c> as OneToMany for NoteCollection
		///</summary>
		[ChildEntityType(typeof(TList<Note>))]
		NoteCollection,
		///<summary>
		/// Collection of <c>Account</c> as OneToMany for GiftCardCollection
		///</summary>
		[ChildEntityType(typeof(TList<GiftCard>))]
		GiftCardCollection,
	}
	
	#endregion AccountChildEntityTypes
	
	#region AccountFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;AccountColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Account"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AccountFilterBuilder : SqlFilterBuilder<AccountColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AccountFilterBuilder class.
		/// </summary>
		public AccountFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the AccountFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AccountFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AccountFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AccountFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AccountFilterBuilder
	
	#region AccountParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;AccountColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Account"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AccountParameterBuilder : ParameterizedSqlFilterBuilder<AccountColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AccountParameterBuilder class.
		/// </summary>
		public AccountParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the AccountParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AccountParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AccountParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AccountParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AccountParameterBuilder
	
	#region AccountSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;AccountColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Account"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class AccountSortBuilder : SqlSortBuilder<AccountColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AccountSqlSortBuilder class.
		/// </summary>
		public AccountSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion AccountSortBuilder
	
} // end namespace
