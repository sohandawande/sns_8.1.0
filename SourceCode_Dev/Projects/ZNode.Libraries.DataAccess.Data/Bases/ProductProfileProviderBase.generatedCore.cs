﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ProductProfileProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class ProductProfileProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.ProductProfile, ZNode.Libraries.DataAccess.Entities.ProductProfileKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProductProfileKey key)
		{
			return Delete(transactionManager, key.ProductProfileID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_productProfileID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _productProfileID)
		{
			return Delete(null, _productProfileID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productProfileID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _productProfileID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductProfile_ZNodeProduct key.
		///		FK_ZNodeProductProfile_ZNodeProduct Description: 
		/// </summary>
		/// <param name="_productID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductProfile objects.</returns>
		public TList<ProductProfile> GetByProductID(System.Int32 _productID)
		{
			int count = -1;
			return GetByProductID(_productID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductProfile_ZNodeProduct key.
		///		FK_ZNodeProductProfile_ZNodeProduct Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductProfile objects.</returns>
		/// <remarks></remarks>
		public TList<ProductProfile> GetByProductID(TransactionManager transactionManager, System.Int32 _productID)
		{
			int count = -1;
			return GetByProductID(transactionManager, _productID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductProfile_ZNodeProduct key.
		///		FK_ZNodeProductProfile_ZNodeProduct Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductProfile objects.</returns>
		public TList<ProductProfile> GetByProductID(TransactionManager transactionManager, System.Int32 _productID, int start, int pageLength)
		{
			int count = -1;
			return GetByProductID(transactionManager, _productID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductProfile_ZNodeProduct key.
		///		fKZNodeProductProfileZNodeProduct Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_productID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductProfile objects.</returns>
		public TList<ProductProfile> GetByProductID(System.Int32 _productID, int start, int pageLength)
		{
			int count =  -1;
			return GetByProductID(null, _productID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductProfile_ZNodeProduct key.
		///		fKZNodeProductProfileZNodeProduct Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_productID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductProfile objects.</returns>
		public TList<ProductProfile> GetByProductID(System.Int32 _productID, int start, int pageLength,out int count)
		{
			return GetByProductID(null, _productID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductProfile_ZNodeProduct key.
		///		FK_ZNodeProductProfile_ZNodeProduct Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductProfile objects.</returns>
		public abstract TList<ProductProfile> GetByProductID(TransactionManager transactionManager, System.Int32 _productID, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductProfile_ZNodeProfile key.
		///		FK_ZNodeProductProfile_ZNodeProfile Description: 
		/// </summary>
		/// <param name="_profileID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductProfile objects.</returns>
		public TList<ProductProfile> GetByProfileID(System.Int32 _profileID)
		{
			int count = -1;
			return GetByProfileID(_profileID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductProfile_ZNodeProfile key.
		///		FK_ZNodeProductProfile_ZNodeProfile Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_profileID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductProfile objects.</returns>
		/// <remarks></remarks>
		public TList<ProductProfile> GetByProfileID(TransactionManager transactionManager, System.Int32 _profileID)
		{
			int count = -1;
			return GetByProfileID(transactionManager, _profileID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductProfile_ZNodeProfile key.
		///		FK_ZNodeProductProfile_ZNodeProfile Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_profileID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductProfile objects.</returns>
		public TList<ProductProfile> GetByProfileID(TransactionManager transactionManager, System.Int32 _profileID, int start, int pageLength)
		{
			int count = -1;
			return GetByProfileID(transactionManager, _profileID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductProfile_ZNodeProfile key.
		///		fKZNodeProductProfileZNodeProfile Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_profileID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductProfile objects.</returns>
		public TList<ProductProfile> GetByProfileID(System.Int32 _profileID, int start, int pageLength)
		{
			int count =  -1;
			return GetByProfileID(null, _profileID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductProfile_ZNodeProfile key.
		///		fKZNodeProductProfileZNodeProfile Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_profileID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductProfile objects.</returns>
		public TList<ProductProfile> GetByProfileID(System.Int32 _profileID, int start, int pageLength,out int count)
		{
			return GetByProfileID(null, _profileID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductProfile_ZNodeProfile key.
		///		FK_ZNodeProductProfile_ZNodeProfile Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_profileID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductProfile objects.</returns>
		public abstract TList<ProductProfile> GetByProfileID(TransactionManager transactionManager, System.Int32 _profileID, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.ProductProfile Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProductProfileKey key, int start, int pageLength)
		{
			return GetByProductProfileID(transactionManager, key.ProductProfileID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodeProductProfile index.
		/// </summary>
		/// <param name="_productProfileID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductProfile"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductProfile GetByProductProfileID(System.Int32 _productProfileID)
		{
			int count = -1;
			return GetByProductProfileID(null,_productProfileID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductProfile index.
		/// </summary>
		/// <param name="_productProfileID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductProfile"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductProfile GetByProductProfileID(System.Int32 _productProfileID, int start, int pageLength)
		{
			int count = -1;
			return GetByProductProfileID(null, _productProfileID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductProfile index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productProfileID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductProfile"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductProfile GetByProductProfileID(TransactionManager transactionManager, System.Int32 _productProfileID)
		{
			int count = -1;
			return GetByProductProfileID(transactionManager, _productProfileID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductProfile index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productProfileID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductProfile"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductProfile GetByProductProfileID(TransactionManager transactionManager, System.Int32 _productProfileID, int start, int pageLength)
		{
			int count = -1;
			return GetByProductProfileID(transactionManager, _productProfileID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductProfile index.
		/// </summary>
		/// <param name="_productProfileID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductProfile"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductProfile GetByProductProfileID(System.Int32 _productProfileID, int start, int pageLength, out int count)
		{
			return GetByProductProfileID(null, _productProfileID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductProfile index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productProfileID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductProfile"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.ProductProfile GetByProductProfileID(TransactionManager transactionManager, System.Int32 _productProfileID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;ProductProfile&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;ProductProfile&gt;"/></returns>
		public static TList<ProductProfile> Fill(IDataReader reader, TList<ProductProfile> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.ProductProfile c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("ProductProfile")
					.Append("|").Append((System.Int32)reader[((int)ProductProfileColumn.ProductProfileID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<ProductProfile>(
					key.ToString(), // EntityTrackingKey
					"ProductProfile",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.ProductProfile();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.ProductProfileID = (System.Int32)reader[((int)ProductProfileColumn.ProductProfileID - 1)];
					c.ProductID = (System.Int32)reader[((int)ProductProfileColumn.ProductID - 1)];
					c.ProfileID = (System.Int32)reader[((int)ProductProfileColumn.ProfileID - 1)];
					c.IncludeInd = (System.Boolean)reader[((int)ProductProfileColumn.IncludeInd - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.ProductProfile"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ProductProfile"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.ProductProfile entity)
		{
			if (!reader.Read()) return;
			
			entity.ProductProfileID = (System.Int32)reader[((int)ProductProfileColumn.ProductProfileID - 1)];
			entity.ProductID = (System.Int32)reader[((int)ProductProfileColumn.ProductID - 1)];
			entity.ProfileID = (System.Int32)reader[((int)ProductProfileColumn.ProfileID - 1)];
			entity.IncludeInd = (System.Boolean)reader[((int)ProductProfileColumn.IncludeInd - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.ProductProfile"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ProductProfile"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.ProductProfile entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.ProductProfileID = (System.Int32)dataRow["ProductProfileID"];
			entity.ProductID = (System.Int32)dataRow["ProductID"];
			entity.ProfileID = (System.Int32)dataRow["ProfileID"];
			entity.IncludeInd = (System.Boolean)dataRow["IncludeInd"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ProductProfile"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.ProductProfile Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProductProfile entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region ProductIDSource	
			if (CanDeepLoad(entity, "Product|ProductIDSource", deepLoadType, innerList) 
				&& entity.ProductIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.ProductID;
				Product tmpEntity = EntityManager.LocateEntity<Product>(EntityLocator.ConstructKeyFromPkItems(typeof(Product), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ProductIDSource = tmpEntity;
				else
					entity.ProductIDSource = DataRepository.ProductProvider.GetByProductID(transactionManager, entity.ProductID);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ProductIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ProductIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.ProductProvider.DeepLoad(transactionManager, entity.ProductIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ProductIDSource

			#region ProfileIDSource	
			if (CanDeepLoad(entity, "Profile|ProfileIDSource", deepLoadType, innerList) 
				&& entity.ProfileIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.ProfileID;
				Profile tmpEntity = EntityManager.LocateEntity<Profile>(EntityLocator.ConstructKeyFromPkItems(typeof(Profile), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ProfileIDSource = tmpEntity;
				else
					entity.ProfileIDSource = DataRepository.ProfileProvider.GetByProfileID(transactionManager, entity.ProfileID);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ProfileIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ProfileIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.ProfileProvider.DeepLoad(transactionManager, entity.ProfileIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ProfileIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.ProductProfile object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.ProductProfile instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.ProductProfile Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProductProfile entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region ProductIDSource
			if (CanDeepSave(entity, "Product|ProductIDSource", deepSaveType, innerList) 
				&& entity.ProductIDSource != null)
			{
				DataRepository.ProductProvider.Save(transactionManager, entity.ProductIDSource);
				entity.ProductID = entity.ProductIDSource.ProductID;
			}
			#endregion 
			
			#region ProfileIDSource
			if (CanDeepSave(entity, "Profile|ProfileIDSource", deepSaveType, innerList) 
				&& entity.ProfileIDSource != null)
			{
				DataRepository.ProfileProvider.Save(transactionManager, entity.ProfileIDSource);
				entity.ProfileID = entity.ProfileIDSource.ProfileID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region ProductProfileChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.ProductProfile</c>
	///</summary>
	public enum ProductProfileChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Product</c> at ProductIDSource
		///</summary>
		[ChildEntityType(typeof(Product))]
		Product,
		
		///<summary>
		/// Composite Property for <c>Profile</c> at ProfileIDSource
		///</summary>
		[ChildEntityType(typeof(Profile))]
		Profile,
	}
	
	#endregion ProductProfileChildEntityTypes
	
	#region ProductProfileFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;ProductProfileColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ProductProfile"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductProfileFilterBuilder : SqlFilterBuilder<ProductProfileColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductProfileFilterBuilder class.
		/// </summary>
		public ProductProfileFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductProfileFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductProfileFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductProfileFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductProfileFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductProfileFilterBuilder
	
	#region ProductProfileParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;ProductProfileColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ProductProfile"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductProfileParameterBuilder : ParameterizedSqlFilterBuilder<ProductProfileColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductProfileParameterBuilder class.
		/// </summary>
		public ProductProfileParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductProfileParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductProfileParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductProfileParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductProfileParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductProfileParameterBuilder
	
	#region ProductProfileSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;ProductProfileColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ProductProfile"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ProductProfileSortBuilder : SqlSortBuilder<ProductProfileColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductProfileSqlSortBuilder class.
		/// </summary>
		public ProductProfileSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ProductProfileSortBuilder
	
} // end namespace
