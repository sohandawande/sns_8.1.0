﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="PaymentTokenAuthorizeProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class PaymentTokenAuthorizeProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.PaymentTokenAuthorize, ZNode.Libraries.DataAccess.Entities.PaymentTokenAuthorizeKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.PaymentTokenAuthorizeKey key)
		{
			return Delete(transactionManager, key.PaymentTokenAuthorizeID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_paymentTokenAuthorizeID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _paymentTokenAuthorizeID)
		{
			return Delete(null, _paymentTokenAuthorizeID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_paymentTokenAuthorizeID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _paymentTokenAuthorizeID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePaymentTokenAuthorize_ZNodePaymentToken key.
		///		FK_ZNodePaymentTokenAuthorize_ZNodePaymentToken Description: 
		/// </summary>
		/// <param name="_paymentTokenID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.PaymentTokenAuthorize objects.</returns>
		public TList<PaymentTokenAuthorize> GetByPaymentTokenID(System.Int32 _paymentTokenID)
		{
			int count = -1;
			return GetByPaymentTokenID(_paymentTokenID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePaymentTokenAuthorize_ZNodePaymentToken key.
		///		FK_ZNodePaymentTokenAuthorize_ZNodePaymentToken Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_paymentTokenID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.PaymentTokenAuthorize objects.</returns>
		/// <remarks></remarks>
		public TList<PaymentTokenAuthorize> GetByPaymentTokenID(TransactionManager transactionManager, System.Int32 _paymentTokenID)
		{
			int count = -1;
			return GetByPaymentTokenID(transactionManager, _paymentTokenID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePaymentTokenAuthorize_ZNodePaymentToken key.
		///		FK_ZNodePaymentTokenAuthorize_ZNodePaymentToken Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_paymentTokenID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.PaymentTokenAuthorize objects.</returns>
		public TList<PaymentTokenAuthorize> GetByPaymentTokenID(TransactionManager transactionManager, System.Int32 _paymentTokenID, int start, int pageLength)
		{
			int count = -1;
			return GetByPaymentTokenID(transactionManager, _paymentTokenID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePaymentTokenAuthorize_ZNodePaymentToken key.
		///		fKZNodePaymentTokenAuthorizeZNodePaymentToken Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_paymentTokenID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.PaymentTokenAuthorize objects.</returns>
		public TList<PaymentTokenAuthorize> GetByPaymentTokenID(System.Int32 _paymentTokenID, int start, int pageLength)
		{
			int count =  -1;
			return GetByPaymentTokenID(null, _paymentTokenID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePaymentTokenAuthorize_ZNodePaymentToken key.
		///		fKZNodePaymentTokenAuthorizeZNodePaymentToken Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_paymentTokenID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.PaymentTokenAuthorize objects.</returns>
		public TList<PaymentTokenAuthorize> GetByPaymentTokenID(System.Int32 _paymentTokenID, int start, int pageLength,out int count)
		{
			return GetByPaymentTokenID(null, _paymentTokenID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePaymentTokenAuthorize_ZNodePaymentToken key.
		///		FK_ZNodePaymentTokenAuthorize_ZNodePaymentToken Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_paymentTokenID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.PaymentTokenAuthorize objects.</returns>
		public abstract TList<PaymentTokenAuthorize> GetByPaymentTokenID(TransactionManager transactionManager, System.Int32 _paymentTokenID, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.PaymentTokenAuthorize Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.PaymentTokenAuthorizeKey key, int start, int pageLength)
		{
			return GetByPaymentTokenAuthorizeID(transactionManager, key.PaymentTokenAuthorizeID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodePaymentTokenAuthorize index.
		/// </summary>
		/// <param name="_paymentTokenAuthorizeID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.PaymentTokenAuthorize"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.PaymentTokenAuthorize GetByPaymentTokenAuthorizeID(System.Int32 _paymentTokenAuthorizeID)
		{
			int count = -1;
			return GetByPaymentTokenAuthorizeID(null,_paymentTokenAuthorizeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodePaymentTokenAuthorize index.
		/// </summary>
		/// <param name="_paymentTokenAuthorizeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.PaymentTokenAuthorize"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.PaymentTokenAuthorize GetByPaymentTokenAuthorizeID(System.Int32 _paymentTokenAuthorizeID, int start, int pageLength)
		{
			int count = -1;
			return GetByPaymentTokenAuthorizeID(null, _paymentTokenAuthorizeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodePaymentTokenAuthorize index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_paymentTokenAuthorizeID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.PaymentTokenAuthorize"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.PaymentTokenAuthorize GetByPaymentTokenAuthorizeID(TransactionManager transactionManager, System.Int32 _paymentTokenAuthorizeID)
		{
			int count = -1;
			return GetByPaymentTokenAuthorizeID(transactionManager, _paymentTokenAuthorizeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodePaymentTokenAuthorize index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_paymentTokenAuthorizeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.PaymentTokenAuthorize"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.PaymentTokenAuthorize GetByPaymentTokenAuthorizeID(TransactionManager transactionManager, System.Int32 _paymentTokenAuthorizeID, int start, int pageLength)
		{
			int count = -1;
			return GetByPaymentTokenAuthorizeID(transactionManager, _paymentTokenAuthorizeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodePaymentTokenAuthorize index.
		/// </summary>
		/// <param name="_paymentTokenAuthorizeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.PaymentTokenAuthorize"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.PaymentTokenAuthorize GetByPaymentTokenAuthorizeID(System.Int32 _paymentTokenAuthorizeID, int start, int pageLength, out int count)
		{
			return GetByPaymentTokenAuthorizeID(null, _paymentTokenAuthorizeID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodePaymentTokenAuthorize index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_paymentTokenAuthorizeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.PaymentTokenAuthorize"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.PaymentTokenAuthorize GetByPaymentTokenAuthorizeID(TransactionManager transactionManager, System.Int32 _paymentTokenAuthorizeID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;PaymentTokenAuthorize&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;PaymentTokenAuthorize&gt;"/></returns>
		public static TList<PaymentTokenAuthorize> Fill(IDataReader reader, TList<PaymentTokenAuthorize> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.PaymentTokenAuthorize c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("PaymentTokenAuthorize")
					.Append("|").Append((System.Int32)reader[((int)PaymentTokenAuthorizeColumn.PaymentTokenAuthorizeID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<PaymentTokenAuthorize>(
					key.ToString(), // EntityTrackingKey
					"PaymentTokenAuthorize",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.PaymentTokenAuthorize();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.PaymentTokenAuthorizeID = (System.Int32)reader[((int)PaymentTokenAuthorizeColumn.PaymentTokenAuthorizeID - 1)];
					c.PaymentTokenID = (System.Int32)reader[((int)PaymentTokenAuthorizeColumn.PaymentTokenID - 1)];
					c.PaymentProfileID = (reader.IsDBNull(((int)PaymentTokenAuthorizeColumn.PaymentProfileID - 1)))?null:(System.String)reader[((int)PaymentTokenAuthorizeColumn.PaymentProfileID - 1)];
					c.CustomerProfileID = (reader.IsDBNull(((int)PaymentTokenAuthorizeColumn.CustomerProfileID - 1)))?null:(System.String)reader[((int)PaymentTokenAuthorizeColumn.CustomerProfileID - 1)];
					c.ShippingProfileID = (reader.IsDBNull(((int)PaymentTokenAuthorizeColumn.ShippingProfileID - 1)))?null:(System.String)reader[((int)PaymentTokenAuthorizeColumn.ShippingProfileID - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.PaymentTokenAuthorize"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.PaymentTokenAuthorize"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.PaymentTokenAuthorize entity)
		{
			if (!reader.Read()) return;
			
			entity.PaymentTokenAuthorizeID = (System.Int32)reader[((int)PaymentTokenAuthorizeColumn.PaymentTokenAuthorizeID - 1)];
			entity.PaymentTokenID = (System.Int32)reader[((int)PaymentTokenAuthorizeColumn.PaymentTokenID - 1)];
			entity.PaymentProfileID = (reader.IsDBNull(((int)PaymentTokenAuthorizeColumn.PaymentProfileID - 1)))?null:(System.String)reader[((int)PaymentTokenAuthorizeColumn.PaymentProfileID - 1)];
			entity.CustomerProfileID = (reader.IsDBNull(((int)PaymentTokenAuthorizeColumn.CustomerProfileID - 1)))?null:(System.String)reader[((int)PaymentTokenAuthorizeColumn.CustomerProfileID - 1)];
			entity.ShippingProfileID = (reader.IsDBNull(((int)PaymentTokenAuthorizeColumn.ShippingProfileID - 1)))?null:(System.String)reader[((int)PaymentTokenAuthorizeColumn.ShippingProfileID - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.PaymentTokenAuthorize"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.PaymentTokenAuthorize"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.PaymentTokenAuthorize entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.PaymentTokenAuthorizeID = (System.Int32)dataRow["PaymentTokenAuthorizeID"];
			entity.PaymentTokenID = (System.Int32)dataRow["PaymentTokenID"];
			entity.PaymentProfileID = Convert.IsDBNull(dataRow["PaymentProfileID"]) ? null : (System.String)dataRow["PaymentProfileID"];
			entity.CustomerProfileID = Convert.IsDBNull(dataRow["CustomerProfileID"]) ? null : (System.String)dataRow["CustomerProfileID"];
			entity.ShippingProfileID = Convert.IsDBNull(dataRow["ShippingProfileID"]) ? null : (System.String)dataRow["ShippingProfileID"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.PaymentTokenAuthorize"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.PaymentTokenAuthorize Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.PaymentTokenAuthorize entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region PaymentTokenIDSource	
			if (CanDeepLoad(entity, "PaymentToken|PaymentTokenIDSource", deepLoadType, innerList) 
				&& entity.PaymentTokenIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.PaymentTokenID;
				PaymentToken tmpEntity = EntityManager.LocateEntity<PaymentToken>(EntityLocator.ConstructKeyFromPkItems(typeof(PaymentToken), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.PaymentTokenIDSource = tmpEntity;
				else
					entity.PaymentTokenIDSource = DataRepository.PaymentTokenProvider.GetByPaymentTokenID(transactionManager, entity.PaymentTokenID);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PaymentTokenIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.PaymentTokenIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.PaymentTokenProvider.DeepLoad(transactionManager, entity.PaymentTokenIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion PaymentTokenIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.PaymentTokenAuthorize object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.PaymentTokenAuthorize instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.PaymentTokenAuthorize Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.PaymentTokenAuthorize entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region PaymentTokenIDSource
			if (CanDeepSave(entity, "PaymentToken|PaymentTokenIDSource", deepSaveType, innerList) 
				&& entity.PaymentTokenIDSource != null)
			{
				DataRepository.PaymentTokenProvider.Save(transactionManager, entity.PaymentTokenIDSource);
				entity.PaymentTokenID = entity.PaymentTokenIDSource.PaymentTokenID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region PaymentTokenAuthorizeChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.PaymentTokenAuthorize</c>
	///</summary>
	public enum PaymentTokenAuthorizeChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>PaymentToken</c> at PaymentTokenIDSource
		///</summary>
		[ChildEntityType(typeof(PaymentToken))]
		PaymentToken,
	}
	
	#endregion PaymentTokenAuthorizeChildEntityTypes
	
	#region PaymentTokenAuthorizeFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;PaymentTokenAuthorizeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PaymentTokenAuthorize"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PaymentTokenAuthorizeFilterBuilder : SqlFilterBuilder<PaymentTokenAuthorizeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PaymentTokenAuthorizeFilterBuilder class.
		/// </summary>
		public PaymentTokenAuthorizeFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the PaymentTokenAuthorizeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PaymentTokenAuthorizeFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PaymentTokenAuthorizeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PaymentTokenAuthorizeFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PaymentTokenAuthorizeFilterBuilder
	
	#region PaymentTokenAuthorizeParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;PaymentTokenAuthorizeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PaymentTokenAuthorize"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PaymentTokenAuthorizeParameterBuilder : ParameterizedSqlFilterBuilder<PaymentTokenAuthorizeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PaymentTokenAuthorizeParameterBuilder class.
		/// </summary>
		public PaymentTokenAuthorizeParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the PaymentTokenAuthorizeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PaymentTokenAuthorizeParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PaymentTokenAuthorizeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PaymentTokenAuthorizeParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PaymentTokenAuthorizeParameterBuilder
	
	#region PaymentTokenAuthorizeSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;PaymentTokenAuthorizeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PaymentTokenAuthorize"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class PaymentTokenAuthorizeSortBuilder : SqlSortBuilder<PaymentTokenAuthorizeColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PaymentTokenAuthorizeSqlSortBuilder class.
		/// </summary>
		public PaymentTokenAuthorizeSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion PaymentTokenAuthorizeSortBuilder
	
} // end namespace
