﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="CasePriorityProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class CasePriorityProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.CasePriority, ZNode.Libraries.DataAccess.Entities.CasePriorityKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.CasePriorityKey key)
		{
			return Delete(transactionManager, key.CasePriorityID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_casePriorityID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _casePriorityID)
		{
			return Delete(null, _casePriorityID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_casePriorityID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _casePriorityID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.CasePriority Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.CasePriorityKey key, int start, int pageLength)
		{
			return GetByCasePriorityID(transactionManager, key.CasePriorityID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_CasePriority index.
		/// </summary>
		/// <param name="_casePriorityID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.CasePriority"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.CasePriority GetByCasePriorityID(System.Int32 _casePriorityID)
		{
			int count = -1;
			return GetByCasePriorityID(null,_casePriorityID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CasePriority index.
		/// </summary>
		/// <param name="_casePriorityID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.CasePriority"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.CasePriority GetByCasePriorityID(System.Int32 _casePriorityID, int start, int pageLength)
		{
			int count = -1;
			return GetByCasePriorityID(null, _casePriorityID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CasePriority index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_casePriorityID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.CasePriority"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.CasePriority GetByCasePriorityID(TransactionManager transactionManager, System.Int32 _casePriorityID)
		{
			int count = -1;
			return GetByCasePriorityID(transactionManager, _casePriorityID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CasePriority index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_casePriorityID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.CasePriority"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.CasePriority GetByCasePriorityID(TransactionManager transactionManager, System.Int32 _casePriorityID, int start, int pageLength)
		{
			int count = -1;
			return GetByCasePriorityID(transactionManager, _casePriorityID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CasePriority index.
		/// </summary>
		/// <param name="_casePriorityID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.CasePriority"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.CasePriority GetByCasePriorityID(System.Int32 _casePriorityID, int start, int pageLength, out int count)
		{
			return GetByCasePriorityID(null, _casePriorityID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CasePriority index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_casePriorityID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.CasePriority"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.CasePriority GetByCasePriorityID(TransactionManager transactionManager, System.Int32 _casePriorityID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;CasePriority&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;CasePriority&gt;"/></returns>
		public static TList<CasePriority> Fill(IDataReader reader, TList<CasePriority> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.CasePriority c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("CasePriority")
					.Append("|").Append((System.Int32)reader[((int)CasePriorityColumn.CasePriorityID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<CasePriority>(
					key.ToString(), // EntityTrackingKey
					"CasePriority",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.CasePriority();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.CasePriorityID = (System.Int32)reader[((int)CasePriorityColumn.CasePriorityID - 1)];
					c.OriginalCasePriorityID = c.CasePriorityID;
					c.CasePriorityNme = (System.String)reader[((int)CasePriorityColumn.CasePriorityNme - 1)];
					c.ViewOrder = (System.Int32)reader[((int)CasePriorityColumn.ViewOrder - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.CasePriority"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.CasePriority"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.CasePriority entity)
		{
			if (!reader.Read()) return;
			
			entity.CasePriorityID = (System.Int32)reader[((int)CasePriorityColumn.CasePriorityID - 1)];
			entity.OriginalCasePriorityID = (System.Int32)reader["CasePriorityID"];
			entity.CasePriorityNme = (System.String)reader[((int)CasePriorityColumn.CasePriorityNme - 1)];
			entity.ViewOrder = (System.Int32)reader[((int)CasePriorityColumn.ViewOrder - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.CasePriority"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.CasePriority"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.CasePriority entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.CasePriorityID = (System.Int32)dataRow["CasePriorityID"];
			entity.OriginalCasePriorityID = (System.Int32)dataRow["CasePriorityID"];
			entity.CasePriorityNme = (System.String)dataRow["CasePriorityNme"];
			entity.ViewOrder = (System.Int32)dataRow["ViewOrder"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.CasePriority"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.CasePriority Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.CasePriority entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByCasePriorityID methods when available
			
			#region CaseRequestCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<CaseRequest>|CaseRequestCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CaseRequestCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CaseRequestCollection = DataRepository.CaseRequestProvider.GetByCasePriorityID(transactionManager, entity.CasePriorityID);

				if (deep && entity.CaseRequestCollection.Count > 0)
				{
					deepHandles.Add("CaseRequestCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<CaseRequest>) DataRepository.CaseRequestProvider.DeepLoad,
						new object[] { transactionManager, entity.CaseRequestCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.CasePriority object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.CasePriority instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.CasePriority Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.CasePriority entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<CaseRequest>
				if (CanDeepSave(entity.CaseRequestCollection, "List<CaseRequest>|CaseRequestCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(CaseRequest child in entity.CaseRequestCollection)
					{
						if(child.CasePriorityIDSource != null)
						{
							child.CasePriorityID = child.CasePriorityIDSource.CasePriorityID;
						}
						else
						{
							child.CasePriorityID = entity.CasePriorityID;
						}

					}

					if (entity.CaseRequestCollection.Count > 0 || entity.CaseRequestCollection.DeletedItems.Count > 0)
					{
						//DataRepository.CaseRequestProvider.Save(transactionManager, entity.CaseRequestCollection);
						
						deepHandles.Add("CaseRequestCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< CaseRequest >) DataRepository.CaseRequestProvider.DeepSave,
							new object[] { transactionManager, entity.CaseRequestCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region CasePriorityChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.CasePriority</c>
	///</summary>
	public enum CasePriorityChildEntityTypes
	{
		///<summary>
		/// Collection of <c>CasePriority</c> as OneToMany for CaseRequestCollection
		///</summary>
		[ChildEntityType(typeof(TList<CaseRequest>))]
		CaseRequestCollection,
	}
	
	#endregion CasePriorityChildEntityTypes
	
	#region CasePriorityFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;CasePriorityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CasePriority"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CasePriorityFilterBuilder : SqlFilterBuilder<CasePriorityColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CasePriorityFilterBuilder class.
		/// </summary>
		public CasePriorityFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CasePriorityFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CasePriorityFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CasePriorityFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CasePriorityFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CasePriorityFilterBuilder
	
	#region CasePriorityParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;CasePriorityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CasePriority"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CasePriorityParameterBuilder : ParameterizedSqlFilterBuilder<CasePriorityColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CasePriorityParameterBuilder class.
		/// </summary>
		public CasePriorityParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CasePriorityParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CasePriorityParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CasePriorityParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CasePriorityParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CasePriorityParameterBuilder
	
	#region CasePrioritySortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;CasePriorityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CasePriority"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class CasePrioritySortBuilder : SqlSortBuilder<CasePriorityColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CasePrioritySqlSortBuilder class.
		/// </summary>
		public CasePrioritySortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion CasePrioritySortBuilder
	
} // end namespace
