﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="TrackingOutboundProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class TrackingOutboundProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.TrackingOutbound, ZNode.Libraries.DataAccess.Entities.TrackingOutboundKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.TrackingOutboundKey key)
		{
			return Delete(transactionManager, key.TrackingOutboundID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_trackingOutboundID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _trackingOutboundID)
		{
			return Delete(null, _trackingOutboundID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_trackingOutboundID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _trackingOutboundID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeTrackingOutbound_ZNodeProduct key.
		///		FK_ZNodeTrackingOutbound_ZNodeProduct Description: 
		/// </summary>
		/// <param name="_productID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.TrackingOutbound objects.</returns>
		public TList<TrackingOutbound> GetByProductID(System.Int32? _productID)
		{
			int count = -1;
			return GetByProductID(_productID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeTrackingOutbound_ZNodeProduct key.
		///		FK_ZNodeTrackingOutbound_ZNodeProduct Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.TrackingOutbound objects.</returns>
		/// <remarks></remarks>
		public TList<TrackingOutbound> GetByProductID(TransactionManager transactionManager, System.Int32? _productID)
		{
			int count = -1;
			return GetByProductID(transactionManager, _productID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeTrackingOutbound_ZNodeProduct key.
		///		FK_ZNodeTrackingOutbound_ZNodeProduct Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.TrackingOutbound objects.</returns>
		public TList<TrackingOutbound> GetByProductID(TransactionManager transactionManager, System.Int32? _productID, int start, int pageLength)
		{
			int count = -1;
			return GetByProductID(transactionManager, _productID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeTrackingOutbound_ZNodeProduct key.
		///		fKZNodeTrackingOutboundZNodeProduct Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_productID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.TrackingOutbound objects.</returns>
		public TList<TrackingOutbound> GetByProductID(System.Int32? _productID, int start, int pageLength)
		{
			int count =  -1;
			return GetByProductID(null, _productID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeTrackingOutbound_ZNodeProduct key.
		///		fKZNodeTrackingOutboundZNodeProduct Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_productID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.TrackingOutbound objects.</returns>
		public TList<TrackingOutbound> GetByProductID(System.Int32? _productID, int start, int pageLength,out int count)
		{
			return GetByProductID(null, _productID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeTrackingOutbound_ZNodeProduct key.
		///		FK_ZNodeTrackingOutbound_ZNodeProduct Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.TrackingOutbound objects.</returns>
		public abstract TList<TrackingOutbound> GetByProductID(TransactionManager transactionManager, System.Int32? _productID, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.TrackingOutbound Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.TrackingOutboundKey key, int start, int pageLength)
		{
			return GetByTrackingOutboundID(transactionManager, key.TrackingOutboundID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodeTrackingOutbound index.
		/// </summary>
		/// <param name="_trackingOutboundID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.TrackingOutbound"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.TrackingOutbound GetByTrackingOutboundID(System.Int32 _trackingOutboundID)
		{
			int count = -1;
			return GetByTrackingOutboundID(null,_trackingOutboundID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeTrackingOutbound index.
		/// </summary>
		/// <param name="_trackingOutboundID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.TrackingOutbound"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.TrackingOutbound GetByTrackingOutboundID(System.Int32 _trackingOutboundID, int start, int pageLength)
		{
			int count = -1;
			return GetByTrackingOutboundID(null, _trackingOutboundID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeTrackingOutbound index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_trackingOutboundID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.TrackingOutbound"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.TrackingOutbound GetByTrackingOutboundID(TransactionManager transactionManager, System.Int32 _trackingOutboundID)
		{
			int count = -1;
			return GetByTrackingOutboundID(transactionManager, _trackingOutboundID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeTrackingOutbound index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_trackingOutboundID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.TrackingOutbound"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.TrackingOutbound GetByTrackingOutboundID(TransactionManager transactionManager, System.Int32 _trackingOutboundID, int start, int pageLength)
		{
			int count = -1;
			return GetByTrackingOutboundID(transactionManager, _trackingOutboundID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeTrackingOutbound index.
		/// </summary>
		/// <param name="_trackingOutboundID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.TrackingOutbound"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.TrackingOutbound GetByTrackingOutboundID(System.Int32 _trackingOutboundID, int start, int pageLength, out int count)
		{
			return GetByTrackingOutboundID(null, _trackingOutboundID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeTrackingOutbound index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_trackingOutboundID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.TrackingOutbound"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.TrackingOutbound GetByTrackingOutboundID(TransactionManager transactionManager, System.Int32 _trackingOutboundID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;TrackingOutbound&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;TrackingOutbound&gt;"/></returns>
		public static TList<TrackingOutbound> Fill(IDataReader reader, TList<TrackingOutbound> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.TrackingOutbound c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("TrackingOutbound")
					.Append("|").Append((System.Int32)reader[((int)TrackingOutboundColumn.TrackingOutboundID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<TrackingOutbound>(
					key.ToString(), // EntityTrackingKey
					"TrackingOutbound",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.TrackingOutbound();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.TrackingOutboundID = (System.Int32)reader[((int)TrackingOutboundColumn.TrackingOutboundID - 1)];
					c.ProductID = (reader.IsDBNull(((int)TrackingOutboundColumn.ProductID - 1)))?null:(System.Int32?)reader[((int)TrackingOutboundColumn.ProductID - 1)];
					c.Date = (reader.IsDBNull(((int)TrackingOutboundColumn.Date - 1)))?null:(System.DateTime?)reader[((int)TrackingOutboundColumn.Date - 1)];
					c.FromUrl = (reader.IsDBNull(((int)TrackingOutboundColumn.FromUrl - 1)))?null:(System.String)reader[((int)TrackingOutboundColumn.FromUrl - 1)];
					c.ToUrl = (reader.IsDBNull(((int)TrackingOutboundColumn.ToUrl - 1)))?null:(System.String)reader[((int)TrackingOutboundColumn.ToUrl - 1)];
					c.EventName = (reader.IsDBNull(((int)TrackingOutboundColumn.EventName - 1)))?null:(System.String)reader[((int)TrackingOutboundColumn.EventName - 1)];
					c.Price = (reader.IsDBNull(((int)TrackingOutboundColumn.Price - 1)))?null:(System.Decimal?)reader[((int)TrackingOutboundColumn.Price - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.TrackingOutbound"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.TrackingOutbound"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.TrackingOutbound entity)
		{
			if (!reader.Read()) return;
			
			entity.TrackingOutboundID = (System.Int32)reader[((int)TrackingOutboundColumn.TrackingOutboundID - 1)];
			entity.ProductID = (reader.IsDBNull(((int)TrackingOutboundColumn.ProductID - 1)))?null:(System.Int32?)reader[((int)TrackingOutboundColumn.ProductID - 1)];
			entity.Date = (reader.IsDBNull(((int)TrackingOutboundColumn.Date - 1)))?null:(System.DateTime?)reader[((int)TrackingOutboundColumn.Date - 1)];
			entity.FromUrl = (reader.IsDBNull(((int)TrackingOutboundColumn.FromUrl - 1)))?null:(System.String)reader[((int)TrackingOutboundColumn.FromUrl - 1)];
			entity.ToUrl = (reader.IsDBNull(((int)TrackingOutboundColumn.ToUrl - 1)))?null:(System.String)reader[((int)TrackingOutboundColumn.ToUrl - 1)];
			entity.EventName = (reader.IsDBNull(((int)TrackingOutboundColumn.EventName - 1)))?null:(System.String)reader[((int)TrackingOutboundColumn.EventName - 1)];
			entity.Price = (reader.IsDBNull(((int)TrackingOutboundColumn.Price - 1)))?null:(System.Decimal?)reader[((int)TrackingOutboundColumn.Price - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.TrackingOutbound"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.TrackingOutbound"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.TrackingOutbound entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.TrackingOutboundID = (System.Int32)dataRow["TrackingOutboundID"];
			entity.ProductID = Convert.IsDBNull(dataRow["ProductID"]) ? null : (System.Int32?)dataRow["ProductID"];
			entity.Date = Convert.IsDBNull(dataRow["Date"]) ? null : (System.DateTime?)dataRow["Date"];
			entity.FromUrl = Convert.IsDBNull(dataRow["FromUrl"]) ? null : (System.String)dataRow["FromUrl"];
			entity.ToUrl = Convert.IsDBNull(dataRow["ToUrl"]) ? null : (System.String)dataRow["ToUrl"];
			entity.EventName = Convert.IsDBNull(dataRow["EventName"]) ? null : (System.String)dataRow["EventName"];
			entity.Price = Convert.IsDBNull(dataRow["Price"]) ? null : (System.Decimal?)dataRow["Price"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.TrackingOutbound"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.TrackingOutbound Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.TrackingOutbound entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region ProductIDSource	
			if (CanDeepLoad(entity, "Product|ProductIDSource", deepLoadType, innerList) 
				&& entity.ProductIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.ProductID ?? (int)0);
				Product tmpEntity = EntityManager.LocateEntity<Product>(EntityLocator.ConstructKeyFromPkItems(typeof(Product), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ProductIDSource = tmpEntity;
				else
					entity.ProductIDSource = DataRepository.ProductProvider.GetByProductID(transactionManager, (entity.ProductID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ProductIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ProductIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.ProductProvider.DeepLoad(transactionManager, entity.ProductIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ProductIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.TrackingOutbound object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.TrackingOutbound instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.TrackingOutbound Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.TrackingOutbound entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region ProductIDSource
			if (CanDeepSave(entity, "Product|ProductIDSource", deepSaveType, innerList) 
				&& entity.ProductIDSource != null)
			{
				DataRepository.ProductProvider.Save(transactionManager, entity.ProductIDSource);
				entity.ProductID = entity.ProductIDSource.ProductID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region TrackingOutboundChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.TrackingOutbound</c>
	///</summary>
	public enum TrackingOutboundChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Product</c> at ProductIDSource
		///</summary>
		[ChildEntityType(typeof(Product))]
		Product,
	}
	
	#endregion TrackingOutboundChildEntityTypes
	
	#region TrackingOutboundFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;TrackingOutboundColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TrackingOutbound"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TrackingOutboundFilterBuilder : SqlFilterBuilder<TrackingOutboundColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TrackingOutboundFilterBuilder class.
		/// </summary>
		public TrackingOutboundFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the TrackingOutboundFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TrackingOutboundFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TrackingOutboundFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TrackingOutboundFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TrackingOutboundFilterBuilder
	
	#region TrackingOutboundParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;TrackingOutboundColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TrackingOutbound"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TrackingOutboundParameterBuilder : ParameterizedSqlFilterBuilder<TrackingOutboundColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TrackingOutboundParameterBuilder class.
		/// </summary>
		public TrackingOutboundParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the TrackingOutboundParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TrackingOutboundParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TrackingOutboundParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TrackingOutboundParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TrackingOutboundParameterBuilder
	
	#region TrackingOutboundSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;TrackingOutboundColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TrackingOutbound"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class TrackingOutboundSortBuilder : SqlSortBuilder<TrackingOutboundColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TrackingOutboundSqlSortBuilder class.
		/// </summary>
		public TrackingOutboundSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion TrackingOutboundSortBuilder
	
} // end namespace
