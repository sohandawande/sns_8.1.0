﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="CaseRequestProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class CaseRequestProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.CaseRequest, ZNode.Libraries.DataAccess.Entities.CaseRequestKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.CaseRequestKey key)
		{
			return Delete(transactionManager, key.CaseID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_caseID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _caseID)
		{
			return Delete(null, _caseID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_caseID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _caseID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.CaseRequest Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.CaseRequestKey key, int start, int pageLength)
		{
			return GetByCaseID(transactionManager, key.CaseID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeCaseRequest_AccountID index.
		/// </summary>
		/// <param name="_accountID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;CaseRequest&gt;"/> class.</returns>
		public TList<CaseRequest> GetByAccountID(System.Int32? _accountID)
		{
			int count = -1;
			return GetByAccountID(null,_accountID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCaseRequest_AccountID index.
		/// </summary>
		/// <param name="_accountID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CaseRequest&gt;"/> class.</returns>
		public TList<CaseRequest> GetByAccountID(System.Int32? _accountID, int start, int pageLength)
		{
			int count = -1;
			return GetByAccountID(null, _accountID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCaseRequest_AccountID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CaseRequest&gt;"/> class.</returns>
		public TList<CaseRequest> GetByAccountID(TransactionManager transactionManager, System.Int32? _accountID)
		{
			int count = -1;
			return GetByAccountID(transactionManager, _accountID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCaseRequest_AccountID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CaseRequest&gt;"/> class.</returns>
		public TList<CaseRequest> GetByAccountID(TransactionManager transactionManager, System.Int32? _accountID, int start, int pageLength)
		{
			int count = -1;
			return GetByAccountID(transactionManager, _accountID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCaseRequest_AccountID index.
		/// </summary>
		/// <param name="_accountID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CaseRequest&gt;"/> class.</returns>
		public TList<CaseRequest> GetByAccountID(System.Int32? _accountID, int start, int pageLength, out int count)
		{
			return GetByAccountID(null, _accountID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCaseRequest_AccountID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;CaseRequest&gt;"/> class.</returns>
		public abstract TList<CaseRequest> GetByAccountID(TransactionManager transactionManager, System.Int32? _accountID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeCaseRequest_CasePriorityID index.
		/// </summary>
		/// <param name="_casePriorityID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;CaseRequest&gt;"/> class.</returns>
		public TList<CaseRequest> GetByCasePriorityID(System.Int32 _casePriorityID)
		{
			int count = -1;
			return GetByCasePriorityID(null,_casePriorityID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCaseRequest_CasePriorityID index.
		/// </summary>
		/// <param name="_casePriorityID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CaseRequest&gt;"/> class.</returns>
		public TList<CaseRequest> GetByCasePriorityID(System.Int32 _casePriorityID, int start, int pageLength)
		{
			int count = -1;
			return GetByCasePriorityID(null, _casePriorityID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCaseRequest_CasePriorityID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_casePriorityID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CaseRequest&gt;"/> class.</returns>
		public TList<CaseRequest> GetByCasePriorityID(TransactionManager transactionManager, System.Int32 _casePriorityID)
		{
			int count = -1;
			return GetByCasePriorityID(transactionManager, _casePriorityID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCaseRequest_CasePriorityID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_casePriorityID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CaseRequest&gt;"/> class.</returns>
		public TList<CaseRequest> GetByCasePriorityID(TransactionManager transactionManager, System.Int32 _casePriorityID, int start, int pageLength)
		{
			int count = -1;
			return GetByCasePriorityID(transactionManager, _casePriorityID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCaseRequest_CasePriorityID index.
		/// </summary>
		/// <param name="_casePriorityID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CaseRequest&gt;"/> class.</returns>
		public TList<CaseRequest> GetByCasePriorityID(System.Int32 _casePriorityID, int start, int pageLength, out int count)
		{
			return GetByCasePriorityID(null, _casePriorityID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCaseRequest_CasePriorityID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_casePriorityID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;CaseRequest&gt;"/> class.</returns>
		public abstract TList<CaseRequest> GetByCasePriorityID(TransactionManager transactionManager, System.Int32 _casePriorityID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeCaseRequest_CaseStatusID index.
		/// </summary>
		/// <param name="_caseStatusID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;CaseRequest&gt;"/> class.</returns>
		public TList<CaseRequest> GetByCaseStatusID(System.Int32 _caseStatusID)
		{
			int count = -1;
			return GetByCaseStatusID(null,_caseStatusID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCaseRequest_CaseStatusID index.
		/// </summary>
		/// <param name="_caseStatusID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CaseRequest&gt;"/> class.</returns>
		public TList<CaseRequest> GetByCaseStatusID(System.Int32 _caseStatusID, int start, int pageLength)
		{
			int count = -1;
			return GetByCaseStatusID(null, _caseStatusID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCaseRequest_CaseStatusID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_caseStatusID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CaseRequest&gt;"/> class.</returns>
		public TList<CaseRequest> GetByCaseStatusID(TransactionManager transactionManager, System.Int32 _caseStatusID)
		{
			int count = -1;
			return GetByCaseStatusID(transactionManager, _caseStatusID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCaseRequest_CaseStatusID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_caseStatusID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CaseRequest&gt;"/> class.</returns>
		public TList<CaseRequest> GetByCaseStatusID(TransactionManager transactionManager, System.Int32 _caseStatusID, int start, int pageLength)
		{
			int count = -1;
			return GetByCaseStatusID(transactionManager, _caseStatusID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCaseRequest_CaseStatusID index.
		/// </summary>
		/// <param name="_caseStatusID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CaseRequest&gt;"/> class.</returns>
		public TList<CaseRequest> GetByCaseStatusID(System.Int32 _caseStatusID, int start, int pageLength, out int count)
		{
			return GetByCaseStatusID(null, _caseStatusID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCaseRequest_CaseStatusID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_caseStatusID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;CaseRequest&gt;"/> class.</returns>
		public abstract TList<CaseRequest> GetByCaseStatusID(TransactionManager transactionManager, System.Int32 _caseStatusID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeCaseRequest_CaseTypeID index.
		/// </summary>
		/// <param name="_caseTypeID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;CaseRequest&gt;"/> class.</returns>
		public TList<CaseRequest> GetByCaseTypeID(System.Int32 _caseTypeID)
		{
			int count = -1;
			return GetByCaseTypeID(null,_caseTypeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCaseRequest_CaseTypeID index.
		/// </summary>
		/// <param name="_caseTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CaseRequest&gt;"/> class.</returns>
		public TList<CaseRequest> GetByCaseTypeID(System.Int32 _caseTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetByCaseTypeID(null, _caseTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCaseRequest_CaseTypeID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_caseTypeID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CaseRequest&gt;"/> class.</returns>
		public TList<CaseRequest> GetByCaseTypeID(TransactionManager transactionManager, System.Int32 _caseTypeID)
		{
			int count = -1;
			return GetByCaseTypeID(transactionManager, _caseTypeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCaseRequest_CaseTypeID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_caseTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CaseRequest&gt;"/> class.</returns>
		public TList<CaseRequest> GetByCaseTypeID(TransactionManager transactionManager, System.Int32 _caseTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetByCaseTypeID(transactionManager, _caseTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCaseRequest_CaseTypeID index.
		/// </summary>
		/// <param name="_caseTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CaseRequest&gt;"/> class.</returns>
		public TList<CaseRequest> GetByCaseTypeID(System.Int32 _caseTypeID, int start, int pageLength, out int count)
		{
			return GetByCaseTypeID(null, _caseTypeID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCaseRequest_CaseTypeID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_caseTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;CaseRequest&gt;"/> class.</returns>
		public abstract TList<CaseRequest> GetByCaseTypeID(TransactionManager transactionManager, System.Int32 _caseTypeID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeCaseRequest_CreateDte index.
		/// </summary>
		/// <param name="_createDte"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;CaseRequest&gt;"/> class.</returns>
		public TList<CaseRequest> GetByCreateDte(System.DateTime _createDte)
		{
			int count = -1;
			return GetByCreateDte(null,_createDte, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCaseRequest_CreateDte index.
		/// </summary>
		/// <param name="_createDte"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CaseRequest&gt;"/> class.</returns>
		public TList<CaseRequest> GetByCreateDte(System.DateTime _createDte, int start, int pageLength)
		{
			int count = -1;
			return GetByCreateDte(null, _createDte, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCaseRequest_CreateDte index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_createDte"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CaseRequest&gt;"/> class.</returns>
		public TList<CaseRequest> GetByCreateDte(TransactionManager transactionManager, System.DateTime _createDte)
		{
			int count = -1;
			return GetByCreateDte(transactionManager, _createDte, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCaseRequest_CreateDte index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_createDte"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CaseRequest&gt;"/> class.</returns>
		public TList<CaseRequest> GetByCreateDte(TransactionManager transactionManager, System.DateTime _createDte, int start, int pageLength)
		{
			int count = -1;
			return GetByCreateDte(transactionManager, _createDte, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCaseRequest_CreateDte index.
		/// </summary>
		/// <param name="_createDte"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CaseRequest&gt;"/> class.</returns>
		public TList<CaseRequest> GetByCreateDte(System.DateTime _createDte, int start, int pageLength, out int count)
		{
			return GetByCreateDte(null, _createDte, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCaseRequest_CreateDte index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_createDte"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;CaseRequest&gt;"/> class.</returns>
		public abstract TList<CaseRequest> GetByCreateDte(TransactionManager transactionManager, System.DateTime _createDte, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeCaseRequest_EmailID index.
		/// </summary>
		/// <param name="_emailID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;CaseRequest&gt;"/> class.</returns>
		public TList<CaseRequest> GetByEmailID(System.String _emailID)
		{
			int count = -1;
			return GetByEmailID(null,_emailID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCaseRequest_EmailID index.
		/// </summary>
		/// <param name="_emailID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CaseRequest&gt;"/> class.</returns>
		public TList<CaseRequest> GetByEmailID(System.String _emailID, int start, int pageLength)
		{
			int count = -1;
			return GetByEmailID(null, _emailID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCaseRequest_EmailID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_emailID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CaseRequest&gt;"/> class.</returns>
		public TList<CaseRequest> GetByEmailID(TransactionManager transactionManager, System.String _emailID)
		{
			int count = -1;
			return GetByEmailID(transactionManager, _emailID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCaseRequest_EmailID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_emailID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CaseRequest&gt;"/> class.</returns>
		public TList<CaseRequest> GetByEmailID(TransactionManager transactionManager, System.String _emailID, int start, int pageLength)
		{
			int count = -1;
			return GetByEmailID(transactionManager, _emailID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCaseRequest_EmailID index.
		/// </summary>
		/// <param name="_emailID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CaseRequest&gt;"/> class.</returns>
		public TList<CaseRequest> GetByEmailID(System.String _emailID, int start, int pageLength, out int count)
		{
			return GetByEmailID(null, _emailID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCaseRequest_EmailID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_emailID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;CaseRequest&gt;"/> class.</returns>
		public abstract TList<CaseRequest> GetByEmailID(TransactionManager transactionManager, System.String _emailID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeCaseRequest_LastName index.
		/// </summary>
		/// <param name="_lastName"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;CaseRequest&gt;"/> class.</returns>
		public TList<CaseRequest> GetByLastName(System.String _lastName)
		{
			int count = -1;
			return GetByLastName(null,_lastName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCaseRequest_LastName index.
		/// </summary>
		/// <param name="_lastName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CaseRequest&gt;"/> class.</returns>
		public TList<CaseRequest> GetByLastName(System.String _lastName, int start, int pageLength)
		{
			int count = -1;
			return GetByLastName(null, _lastName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCaseRequest_LastName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_lastName"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CaseRequest&gt;"/> class.</returns>
		public TList<CaseRequest> GetByLastName(TransactionManager transactionManager, System.String _lastName)
		{
			int count = -1;
			return GetByLastName(transactionManager, _lastName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCaseRequest_LastName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_lastName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CaseRequest&gt;"/> class.</returns>
		public TList<CaseRequest> GetByLastName(TransactionManager transactionManager, System.String _lastName, int start, int pageLength)
		{
			int count = -1;
			return GetByLastName(transactionManager, _lastName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCaseRequest_LastName index.
		/// </summary>
		/// <param name="_lastName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CaseRequest&gt;"/> class.</returns>
		public TList<CaseRequest> GetByLastName(System.String _lastName, int start, int pageLength, out int count)
		{
			return GetByLastName(null, _lastName, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCaseRequest_LastName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_lastName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;CaseRequest&gt;"/> class.</returns>
		public abstract TList<CaseRequest> GetByLastName(TransactionManager transactionManager, System.String _lastName, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeCaseRequest_OwnerAccountID index.
		/// </summary>
		/// <param name="_ownerAccountID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;CaseRequest&gt;"/> class.</returns>
		public TList<CaseRequest> GetByOwnerAccountID(System.Int32? _ownerAccountID)
		{
			int count = -1;
			return GetByOwnerAccountID(null,_ownerAccountID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCaseRequest_OwnerAccountID index.
		/// </summary>
		/// <param name="_ownerAccountID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CaseRequest&gt;"/> class.</returns>
		public TList<CaseRequest> GetByOwnerAccountID(System.Int32? _ownerAccountID, int start, int pageLength)
		{
			int count = -1;
			return GetByOwnerAccountID(null, _ownerAccountID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCaseRequest_OwnerAccountID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_ownerAccountID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CaseRequest&gt;"/> class.</returns>
		public TList<CaseRequest> GetByOwnerAccountID(TransactionManager transactionManager, System.Int32? _ownerAccountID)
		{
			int count = -1;
			return GetByOwnerAccountID(transactionManager, _ownerAccountID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCaseRequest_OwnerAccountID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_ownerAccountID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CaseRequest&gt;"/> class.</returns>
		public TList<CaseRequest> GetByOwnerAccountID(TransactionManager transactionManager, System.Int32? _ownerAccountID, int start, int pageLength)
		{
			int count = -1;
			return GetByOwnerAccountID(transactionManager, _ownerAccountID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCaseRequest_OwnerAccountID index.
		/// </summary>
		/// <param name="_ownerAccountID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CaseRequest&gt;"/> class.</returns>
		public TList<CaseRequest> GetByOwnerAccountID(System.Int32? _ownerAccountID, int start, int pageLength, out int count)
		{
			return GetByOwnerAccountID(null, _ownerAccountID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCaseRequest_OwnerAccountID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_ownerAccountID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;CaseRequest&gt;"/> class.</returns>
		public abstract TList<CaseRequest> GetByOwnerAccountID(TransactionManager transactionManager, System.Int32? _ownerAccountID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeCaseRequest_PortalID index.
		/// </summary>
		/// <param name="_portalID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;CaseRequest&gt;"/> class.</returns>
		public TList<CaseRequest> GetByPortalID(System.Int32 _portalID)
		{
			int count = -1;
			return GetByPortalID(null,_portalID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCaseRequest_PortalID index.
		/// </summary>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CaseRequest&gt;"/> class.</returns>
		public TList<CaseRequest> GetByPortalID(System.Int32 _portalID, int start, int pageLength)
		{
			int count = -1;
			return GetByPortalID(null, _portalID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCaseRequest_PortalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CaseRequest&gt;"/> class.</returns>
		public TList<CaseRequest> GetByPortalID(TransactionManager transactionManager, System.Int32 _portalID)
		{
			int count = -1;
			return GetByPortalID(transactionManager, _portalID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCaseRequest_PortalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CaseRequest&gt;"/> class.</returns>
		public TList<CaseRequest> GetByPortalID(TransactionManager transactionManager, System.Int32 _portalID, int start, int pageLength)
		{
			int count = -1;
			return GetByPortalID(transactionManager, _portalID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCaseRequest_PortalID index.
		/// </summary>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CaseRequest&gt;"/> class.</returns>
		public TList<CaseRequest> GetByPortalID(System.Int32 _portalID, int start, int pageLength, out int count)
		{
			return GetByPortalID(null, _portalID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCaseRequest_PortalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;CaseRequest&gt;"/> class.</returns>
		public abstract TList<CaseRequest> GetByPortalID(TransactionManager transactionManager, System.Int32 _portalID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_Case index.
		/// </summary>
		/// <param name="_caseID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.CaseRequest"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.CaseRequest GetByCaseID(System.Int32 _caseID)
		{
			int count = -1;
			return GetByCaseID(null,_caseID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Case index.
		/// </summary>
		/// <param name="_caseID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.CaseRequest"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.CaseRequest GetByCaseID(System.Int32 _caseID, int start, int pageLength)
		{
			int count = -1;
			return GetByCaseID(null, _caseID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Case index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_caseID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.CaseRequest"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.CaseRequest GetByCaseID(TransactionManager transactionManager, System.Int32 _caseID)
		{
			int count = -1;
			return GetByCaseID(transactionManager, _caseID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Case index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_caseID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.CaseRequest"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.CaseRequest GetByCaseID(TransactionManager transactionManager, System.Int32 _caseID, int start, int pageLength)
		{
			int count = -1;
			return GetByCaseID(transactionManager, _caseID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Case index.
		/// </summary>
		/// <param name="_caseID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.CaseRequest"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.CaseRequest GetByCaseID(System.Int32 _caseID, int start, int pageLength, out int count)
		{
			return GetByCaseID(null, _caseID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Case index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_caseID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.CaseRequest"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.CaseRequest GetByCaseID(TransactionManager transactionManager, System.Int32 _caseID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;CaseRequest&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;CaseRequest&gt;"/></returns>
		public static TList<CaseRequest> Fill(IDataReader reader, TList<CaseRequest> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.CaseRequest c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("CaseRequest")
					.Append("|").Append((System.Int32)reader[((int)CaseRequestColumn.CaseID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<CaseRequest>(
					key.ToString(), // EntityTrackingKey
					"CaseRequest",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.CaseRequest();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.CaseID = (System.Int32)reader[((int)CaseRequestColumn.CaseID - 1)];
					c.PortalID = (System.Int32)reader[((int)CaseRequestColumn.PortalID - 1)];
					c.AccountID = (reader.IsDBNull(((int)CaseRequestColumn.AccountID - 1)))?null:(System.Int32?)reader[((int)CaseRequestColumn.AccountID - 1)];
					c.OwnerAccountID = (reader.IsDBNull(((int)CaseRequestColumn.OwnerAccountID - 1)))?null:(System.Int32?)reader[((int)CaseRequestColumn.OwnerAccountID - 1)];
					c.CaseStatusID = (System.Int32)reader[((int)CaseRequestColumn.CaseStatusID - 1)];
					c.CasePriorityID = (System.Int32)reader[((int)CaseRequestColumn.CasePriorityID - 1)];
					c.CaseTypeID = (System.Int32)reader[((int)CaseRequestColumn.CaseTypeID - 1)];
					c.CaseOrigin = (reader.IsDBNull(((int)CaseRequestColumn.CaseOrigin - 1)))?null:(System.String)reader[((int)CaseRequestColumn.CaseOrigin - 1)];
					c.Title = (System.String)reader[((int)CaseRequestColumn.Title - 1)];
					c.Description = (System.String)reader[((int)CaseRequestColumn.Description - 1)];
					c.FirstName = (reader.IsDBNull(((int)CaseRequestColumn.FirstName - 1)))?null:(System.String)reader[((int)CaseRequestColumn.FirstName - 1)];
					c.LastName = (reader.IsDBNull(((int)CaseRequestColumn.LastName - 1)))?null:(System.String)reader[((int)CaseRequestColumn.LastName - 1)];
					c.CompanyName = (reader.IsDBNull(((int)CaseRequestColumn.CompanyName - 1)))?null:(System.String)reader[((int)CaseRequestColumn.CompanyName - 1)];
					c.EmailID = (reader.IsDBNull(((int)CaseRequestColumn.EmailID - 1)))?null:(System.String)reader[((int)CaseRequestColumn.EmailID - 1)];
					c.PhoneNumber = (reader.IsDBNull(((int)CaseRequestColumn.PhoneNumber - 1)))?null:(System.String)reader[((int)CaseRequestColumn.PhoneNumber - 1)];
					c.CreateDte = (System.DateTime)reader[((int)CaseRequestColumn.CreateDte - 1)];
					c.CreateUser = (System.String)reader[((int)CaseRequestColumn.CreateUser - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.CaseRequest"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.CaseRequest"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.CaseRequest entity)
		{
			if (!reader.Read()) return;
			
			entity.CaseID = (System.Int32)reader[((int)CaseRequestColumn.CaseID - 1)];
			entity.PortalID = (System.Int32)reader[((int)CaseRequestColumn.PortalID - 1)];
			entity.AccountID = (reader.IsDBNull(((int)CaseRequestColumn.AccountID - 1)))?null:(System.Int32?)reader[((int)CaseRequestColumn.AccountID - 1)];
			entity.OwnerAccountID = (reader.IsDBNull(((int)CaseRequestColumn.OwnerAccountID - 1)))?null:(System.Int32?)reader[((int)CaseRequestColumn.OwnerAccountID - 1)];
			entity.CaseStatusID = (System.Int32)reader[((int)CaseRequestColumn.CaseStatusID - 1)];
			entity.CasePriorityID = (System.Int32)reader[((int)CaseRequestColumn.CasePriorityID - 1)];
			entity.CaseTypeID = (System.Int32)reader[((int)CaseRequestColumn.CaseTypeID - 1)];
			entity.CaseOrigin = (reader.IsDBNull(((int)CaseRequestColumn.CaseOrigin - 1)))?null:(System.String)reader[((int)CaseRequestColumn.CaseOrigin - 1)];
			entity.Title = (System.String)reader[((int)CaseRequestColumn.Title - 1)];
			entity.Description = (System.String)reader[((int)CaseRequestColumn.Description - 1)];
			entity.FirstName = (reader.IsDBNull(((int)CaseRequestColumn.FirstName - 1)))?null:(System.String)reader[((int)CaseRequestColumn.FirstName - 1)];
			entity.LastName = (reader.IsDBNull(((int)CaseRequestColumn.LastName - 1)))?null:(System.String)reader[((int)CaseRequestColumn.LastName - 1)];
			entity.CompanyName = (reader.IsDBNull(((int)CaseRequestColumn.CompanyName - 1)))?null:(System.String)reader[((int)CaseRequestColumn.CompanyName - 1)];
			entity.EmailID = (reader.IsDBNull(((int)CaseRequestColumn.EmailID - 1)))?null:(System.String)reader[((int)CaseRequestColumn.EmailID - 1)];
			entity.PhoneNumber = (reader.IsDBNull(((int)CaseRequestColumn.PhoneNumber - 1)))?null:(System.String)reader[((int)CaseRequestColumn.PhoneNumber - 1)];
			entity.CreateDte = (System.DateTime)reader[((int)CaseRequestColumn.CreateDte - 1)];
			entity.CreateUser = (System.String)reader[((int)CaseRequestColumn.CreateUser - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.CaseRequest"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.CaseRequest"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.CaseRequest entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.CaseID = (System.Int32)dataRow["CaseID"];
			entity.PortalID = (System.Int32)dataRow["PortalID"];
			entity.AccountID = Convert.IsDBNull(dataRow["AccountID"]) ? null : (System.Int32?)dataRow["AccountID"];
			entity.OwnerAccountID = Convert.IsDBNull(dataRow["OwnerAccountID"]) ? null : (System.Int32?)dataRow["OwnerAccountID"];
			entity.CaseStatusID = (System.Int32)dataRow["CaseStatusID"];
			entity.CasePriorityID = (System.Int32)dataRow["CasePriorityID"];
			entity.CaseTypeID = (System.Int32)dataRow["CaseTypeID"];
			entity.CaseOrigin = Convert.IsDBNull(dataRow["CaseOrigin"]) ? null : (System.String)dataRow["CaseOrigin"];
			entity.Title = (System.String)dataRow["Title"];
			entity.Description = (System.String)dataRow["Description"];
			entity.FirstName = Convert.IsDBNull(dataRow["FirstName"]) ? null : (System.String)dataRow["FirstName"];
			entity.LastName = Convert.IsDBNull(dataRow["LastName"]) ? null : (System.String)dataRow["LastName"];
			entity.CompanyName = Convert.IsDBNull(dataRow["CompanyName"]) ? null : (System.String)dataRow["CompanyName"];
			entity.EmailID = Convert.IsDBNull(dataRow["EmailID"]) ? null : (System.String)dataRow["EmailID"];
			entity.PhoneNumber = Convert.IsDBNull(dataRow["PhoneNumber"]) ? null : (System.String)dataRow["PhoneNumber"];
			entity.CreateDte = (System.DateTime)dataRow["CreateDte"];
			entity.CreateUser = (System.String)dataRow["CreateUser"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.CaseRequest"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.CaseRequest Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.CaseRequest entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region CaseStatusIDSource	
			if (CanDeepLoad(entity, "CaseStatus|CaseStatusIDSource", deepLoadType, innerList) 
				&& entity.CaseStatusIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.CaseStatusID;
				CaseStatus tmpEntity = EntityManager.LocateEntity<CaseStatus>(EntityLocator.ConstructKeyFromPkItems(typeof(CaseStatus), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CaseStatusIDSource = tmpEntity;
				else
					entity.CaseStatusIDSource = DataRepository.CaseStatusProvider.GetByCaseStatusID(transactionManager, entity.CaseStatusID);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CaseStatusIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CaseStatusIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CaseStatusProvider.DeepLoad(transactionManager, entity.CaseStatusIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CaseStatusIDSource

			#region CaseTypeIDSource	
			if (CanDeepLoad(entity, "CaseType|CaseTypeIDSource", deepLoadType, innerList) 
				&& entity.CaseTypeIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.CaseTypeID;
				CaseType tmpEntity = EntityManager.LocateEntity<CaseType>(EntityLocator.ConstructKeyFromPkItems(typeof(CaseType), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CaseTypeIDSource = tmpEntity;
				else
					entity.CaseTypeIDSource = DataRepository.CaseTypeProvider.GetByCaseTypeID(transactionManager, entity.CaseTypeID);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CaseTypeIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CaseTypeIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CaseTypeProvider.DeepLoad(transactionManager, entity.CaseTypeIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CaseTypeIDSource

			#region PortalIDSource	
			if (CanDeepLoad(entity, "Portal|PortalIDSource", deepLoadType, innerList) 
				&& entity.PortalIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.PortalID;
				Portal tmpEntity = EntityManager.LocateEntity<Portal>(EntityLocator.ConstructKeyFromPkItems(typeof(Portal), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.PortalIDSource = tmpEntity;
				else
					entity.PortalIDSource = DataRepository.PortalProvider.GetByPortalID(transactionManager, entity.PortalID);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PortalIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.PortalIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.PortalProvider.DeepLoad(transactionManager, entity.PortalIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion PortalIDSource

			#region AccountIDSource	
			if (CanDeepLoad(entity, "Account|AccountIDSource", deepLoadType, innerList) 
				&& entity.AccountIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.AccountID ?? (int)0);
				Account tmpEntity = EntityManager.LocateEntity<Account>(EntityLocator.ConstructKeyFromPkItems(typeof(Account), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.AccountIDSource = tmpEntity;
				else
					entity.AccountIDSource = DataRepository.AccountProvider.GetByAccountID(transactionManager, (entity.AccountID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AccountIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.AccountIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.AccountProvider.DeepLoad(transactionManager, entity.AccountIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion AccountIDSource

			#region OwnerAccountIDSource	
			if (CanDeepLoad(entity, "Account|OwnerAccountIDSource", deepLoadType, innerList) 
				&& entity.OwnerAccountIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.OwnerAccountID ?? (int)0);
				Account tmpEntity = EntityManager.LocateEntity<Account>(EntityLocator.ConstructKeyFromPkItems(typeof(Account), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.OwnerAccountIDSource = tmpEntity;
				else
					entity.OwnerAccountIDSource = DataRepository.AccountProvider.GetByAccountID(transactionManager, (entity.OwnerAccountID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'OwnerAccountIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.OwnerAccountIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.AccountProvider.DeepLoad(transactionManager, entity.OwnerAccountIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion OwnerAccountIDSource

			#region CasePriorityIDSource	
			if (CanDeepLoad(entity, "CasePriority|CasePriorityIDSource", deepLoadType, innerList) 
				&& entity.CasePriorityIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.CasePriorityID;
				CasePriority tmpEntity = EntityManager.LocateEntity<CasePriority>(EntityLocator.ConstructKeyFromPkItems(typeof(CasePriority), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CasePriorityIDSource = tmpEntity;
				else
					entity.CasePriorityIDSource = DataRepository.CasePriorityProvider.GetByCasePriorityID(transactionManager, entity.CasePriorityID);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CasePriorityIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CasePriorityIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CasePriorityProvider.DeepLoad(transactionManager, entity.CasePriorityIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CasePriorityIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByCaseID methods when available
			
			#region NoteCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Note>|NoteCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'NoteCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.NoteCollection = DataRepository.NoteProvider.GetByCaseID(transactionManager, entity.CaseID);

				if (deep && entity.NoteCollection.Count > 0)
				{
					deepHandles.Add("NoteCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Note>) DataRepository.NoteProvider.DeepLoad,
						new object[] { transactionManager, entity.NoteCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.CaseRequest object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.CaseRequest instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.CaseRequest Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.CaseRequest entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region CaseStatusIDSource
			if (CanDeepSave(entity, "CaseStatus|CaseStatusIDSource", deepSaveType, innerList) 
				&& entity.CaseStatusIDSource != null)
			{
				DataRepository.CaseStatusProvider.Save(transactionManager, entity.CaseStatusIDSource);
				entity.CaseStatusID = entity.CaseStatusIDSource.CaseStatusID;
			}
			#endregion 
			
			#region CaseTypeIDSource
			if (CanDeepSave(entity, "CaseType|CaseTypeIDSource", deepSaveType, innerList) 
				&& entity.CaseTypeIDSource != null)
			{
				DataRepository.CaseTypeProvider.Save(transactionManager, entity.CaseTypeIDSource);
				entity.CaseTypeID = entity.CaseTypeIDSource.CaseTypeID;
			}
			#endregion 
			
			#region PortalIDSource
			if (CanDeepSave(entity, "Portal|PortalIDSource", deepSaveType, innerList) 
				&& entity.PortalIDSource != null)
			{
				DataRepository.PortalProvider.Save(transactionManager, entity.PortalIDSource);
				entity.PortalID = entity.PortalIDSource.PortalID;
			}
			#endregion 
			
			#region AccountIDSource
			if (CanDeepSave(entity, "Account|AccountIDSource", deepSaveType, innerList) 
				&& entity.AccountIDSource != null)
			{
				DataRepository.AccountProvider.Save(transactionManager, entity.AccountIDSource);
				entity.AccountID = entity.AccountIDSource.AccountID;
			}
			#endregion 
			
			#region OwnerAccountIDSource
			if (CanDeepSave(entity, "Account|OwnerAccountIDSource", deepSaveType, innerList) 
				&& entity.OwnerAccountIDSource != null)
			{
				DataRepository.AccountProvider.Save(transactionManager, entity.OwnerAccountIDSource);
				entity.OwnerAccountID = entity.OwnerAccountIDSource.AccountID;
			}
			#endregion 
			
			#region CasePriorityIDSource
			if (CanDeepSave(entity, "CasePriority|CasePriorityIDSource", deepSaveType, innerList) 
				&& entity.CasePriorityIDSource != null)
			{
				DataRepository.CasePriorityProvider.Save(transactionManager, entity.CasePriorityIDSource);
				entity.CasePriorityID = entity.CasePriorityIDSource.CasePriorityID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<Note>
				if (CanDeepSave(entity.NoteCollection, "List<Note>|NoteCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Note child in entity.NoteCollection)
					{
						if(child.CaseIDSource != null)
						{
							child.CaseID = child.CaseIDSource.CaseID;
						}
						else
						{
							child.CaseID = entity.CaseID;
						}

					}

					if (entity.NoteCollection.Count > 0 || entity.NoteCollection.DeletedItems.Count > 0)
					{
						//DataRepository.NoteProvider.Save(transactionManager, entity.NoteCollection);
						
						deepHandles.Add("NoteCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Note >) DataRepository.NoteProvider.DeepSave,
							new object[] { transactionManager, entity.NoteCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region CaseRequestChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.CaseRequest</c>
	///</summary>
	public enum CaseRequestChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>CaseStatus</c> at CaseStatusIDSource
		///</summary>
		[ChildEntityType(typeof(CaseStatus))]
		CaseStatus,
		
		///<summary>
		/// Composite Property for <c>CaseType</c> at CaseTypeIDSource
		///</summary>
		[ChildEntityType(typeof(CaseType))]
		CaseType,
		
		///<summary>
		/// Composite Property for <c>Portal</c> at PortalIDSource
		///</summary>
		[ChildEntityType(typeof(Portal))]
		Portal,
		
		///<summary>
		/// Composite Property for <c>Account</c> at AccountIDSource
		///</summary>
		[ChildEntityType(typeof(Account))]
		Account,
		
		///<summary>
		/// Composite Property for <c>CasePriority</c> at CasePriorityIDSource
		///</summary>
		[ChildEntityType(typeof(CasePriority))]
		CasePriority,
		///<summary>
		/// Collection of <c>CaseRequest</c> as OneToMany for NoteCollection
		///</summary>
		[ChildEntityType(typeof(TList<Note>))]
		NoteCollection,
	}
	
	#endregion CaseRequestChildEntityTypes
	
	#region CaseRequestFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;CaseRequestColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CaseRequest"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CaseRequestFilterBuilder : SqlFilterBuilder<CaseRequestColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CaseRequestFilterBuilder class.
		/// </summary>
		public CaseRequestFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CaseRequestFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CaseRequestFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CaseRequestFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CaseRequestFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CaseRequestFilterBuilder
	
	#region CaseRequestParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;CaseRequestColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CaseRequest"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CaseRequestParameterBuilder : ParameterizedSqlFilterBuilder<CaseRequestColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CaseRequestParameterBuilder class.
		/// </summary>
		public CaseRequestParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CaseRequestParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CaseRequestParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CaseRequestParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CaseRequestParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CaseRequestParameterBuilder
	
	#region CaseRequestSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;CaseRequestColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CaseRequest"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class CaseRequestSortBuilder : SqlSortBuilder<CaseRequestColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CaseRequestSqlSortBuilder class.
		/// </summary>
		public CaseRequestSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion CaseRequestSortBuilder
	
} // end namespace
