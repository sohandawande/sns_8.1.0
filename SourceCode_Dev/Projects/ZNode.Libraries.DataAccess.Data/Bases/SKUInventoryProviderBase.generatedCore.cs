﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="SKUInventoryProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class SKUInventoryProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.SKUInventory, ZNode.Libraries.DataAccess.Entities.SKUInventoryKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.SKUInventoryKey key)
		{
			return Delete(transactionManager, key.SKUInventoryID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_sKUInventoryID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _sKUInventoryID)
		{
			return Delete(null, _sKUInventoryID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sKUInventoryID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _sKUInventoryID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.SKUInventory Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.SKUInventoryKey key, int start, int pageLength)
		{
			return GetBySKUInventoryID(transactionManager, key.SKUInventoryID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeSKUInventory index.
		/// </summary>
		/// <param name="_sKU"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SKUInventory"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.SKUInventory GetBySKU(System.String _sKU)
		{
			int count = -1;
			return GetBySKU(null,_sKU, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSKUInventory index.
		/// </summary>
		/// <param name="_sKU"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SKUInventory"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.SKUInventory GetBySKU(System.String _sKU, int start, int pageLength)
		{
			int count = -1;
			return GetBySKU(null, _sKU, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSKUInventory index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sKU"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SKUInventory"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.SKUInventory GetBySKU(TransactionManager transactionManager, System.String _sKU)
		{
			int count = -1;
			return GetBySKU(transactionManager, _sKU, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSKUInventory index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sKU"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SKUInventory"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.SKUInventory GetBySKU(TransactionManager transactionManager, System.String _sKU, int start, int pageLength)
		{
			int count = -1;
			return GetBySKU(transactionManager, _sKU, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSKUInventory index.
		/// </summary>
		/// <param name="_sKU"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SKUInventory"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.SKUInventory GetBySKU(System.String _sKU, int start, int pageLength, out int count)
		{
			return GetBySKU(null, _sKU, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSKUInventory index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sKU"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SKUInventory"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.SKUInventory GetBySKU(TransactionManager transactionManager, System.String _sKU, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeSKUInventory_QuantityOnHand index.
		/// </summary>
		/// <param name="_quantityOnHand"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;SKUInventory&gt;"/> class.</returns>
		public TList<SKUInventory> GetByQuantityOnHand(System.Int32? _quantityOnHand)
		{
			int count = -1;
			return GetByQuantityOnHand(null,_quantityOnHand, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSKUInventory_QuantityOnHand index.
		/// </summary>
		/// <param name="_quantityOnHand"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;SKUInventory&gt;"/> class.</returns>
		public TList<SKUInventory> GetByQuantityOnHand(System.Int32? _quantityOnHand, int start, int pageLength)
		{
			int count = -1;
			return GetByQuantityOnHand(null, _quantityOnHand, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSKUInventory_QuantityOnHand index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_quantityOnHand"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;SKUInventory&gt;"/> class.</returns>
		public TList<SKUInventory> GetByQuantityOnHand(TransactionManager transactionManager, System.Int32? _quantityOnHand)
		{
			int count = -1;
			return GetByQuantityOnHand(transactionManager, _quantityOnHand, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSKUInventory_QuantityOnHand index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_quantityOnHand"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;SKUInventory&gt;"/> class.</returns>
		public TList<SKUInventory> GetByQuantityOnHand(TransactionManager transactionManager, System.Int32? _quantityOnHand, int start, int pageLength)
		{
			int count = -1;
			return GetByQuantityOnHand(transactionManager, _quantityOnHand, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSKUInventory_QuantityOnHand index.
		/// </summary>
		/// <param name="_quantityOnHand"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;SKUInventory&gt;"/> class.</returns>
		public TList<SKUInventory> GetByQuantityOnHand(System.Int32? _quantityOnHand, int start, int pageLength, out int count)
		{
			return GetByQuantityOnHand(null, _quantityOnHand, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSKUInventory_QuantityOnHand index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_quantityOnHand"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;SKUInventory&gt;"/> class.</returns>
		public abstract TList<SKUInventory> GetByQuantityOnHand(TransactionManager transactionManager, System.Int32? _quantityOnHand, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodeSKUInventory index.
		/// </summary>
		/// <param name="_sKUInventoryID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SKUInventory"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.SKUInventory GetBySKUInventoryID(System.Int32 _sKUInventoryID)
		{
			int count = -1;
			return GetBySKUInventoryID(null,_sKUInventoryID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeSKUInventory index.
		/// </summary>
		/// <param name="_sKUInventoryID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SKUInventory"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.SKUInventory GetBySKUInventoryID(System.Int32 _sKUInventoryID, int start, int pageLength)
		{
			int count = -1;
			return GetBySKUInventoryID(null, _sKUInventoryID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeSKUInventory index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sKUInventoryID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SKUInventory"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.SKUInventory GetBySKUInventoryID(TransactionManager transactionManager, System.Int32 _sKUInventoryID)
		{
			int count = -1;
			return GetBySKUInventoryID(transactionManager, _sKUInventoryID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeSKUInventory index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sKUInventoryID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SKUInventory"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.SKUInventory GetBySKUInventoryID(TransactionManager transactionManager, System.Int32 _sKUInventoryID, int start, int pageLength)
		{
			int count = -1;
			return GetBySKUInventoryID(transactionManager, _sKUInventoryID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeSKUInventory index.
		/// </summary>
		/// <param name="_sKUInventoryID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SKUInventory"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.SKUInventory GetBySKUInventoryID(System.Int32 _sKUInventoryID, int start, int pageLength, out int count)
		{
			return GetBySKUInventoryID(null, _sKUInventoryID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeSKUInventory index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sKUInventoryID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SKUInventory"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.SKUInventory GetBySKUInventoryID(TransactionManager transactionManager, System.Int32 _sKUInventoryID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;SKUInventory&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;SKUInventory&gt;"/></returns>
		public static TList<SKUInventory> Fill(IDataReader reader, TList<SKUInventory> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.SKUInventory c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("SKUInventory")
					.Append("|").Append((System.Int32)reader[((int)SKUInventoryColumn.SKUInventoryID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<SKUInventory>(
					key.ToString(), // EntityTrackingKey
					"SKUInventory",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.SKUInventory();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.SKUInventoryID = (System.Int32)reader[((int)SKUInventoryColumn.SKUInventoryID - 1)];
					c.SKU = (System.String)reader[((int)SKUInventoryColumn.SKU - 1)];
					c.QuantityOnHand = (reader.IsDBNull(((int)SKUInventoryColumn.QuantityOnHand - 1)))?null:(System.Int32?)reader[((int)SKUInventoryColumn.QuantityOnHand - 1)];
					c.ReOrderLevel = (reader.IsDBNull(((int)SKUInventoryColumn.ReOrderLevel - 1)))?null:(System.Int32?)reader[((int)SKUInventoryColumn.ReOrderLevel - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.SKUInventory"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.SKUInventory"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.SKUInventory entity)
		{
			if (!reader.Read()) return;
			
			entity.SKUInventoryID = (System.Int32)reader[((int)SKUInventoryColumn.SKUInventoryID - 1)];
			entity.SKU = (System.String)reader[((int)SKUInventoryColumn.SKU - 1)];
			entity.QuantityOnHand = (reader.IsDBNull(((int)SKUInventoryColumn.QuantityOnHand - 1)))?null:(System.Int32?)reader[((int)SKUInventoryColumn.QuantityOnHand - 1)];
			entity.ReOrderLevel = (reader.IsDBNull(((int)SKUInventoryColumn.ReOrderLevel - 1)))?null:(System.Int32?)reader[((int)SKUInventoryColumn.ReOrderLevel - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.SKUInventory"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.SKUInventory"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.SKUInventory entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.SKUInventoryID = (System.Int32)dataRow["SKUInventoryID"];
			entity.SKU = (System.String)dataRow["SKU"];
			entity.QuantityOnHand = Convert.IsDBNull(dataRow["QuantityOnHand"]) ? null : (System.Int32?)dataRow["QuantityOnHand"];
			entity.ReOrderLevel = Convert.IsDBNull(dataRow["ReOrderLevel"]) ? null : (System.Int32?)dataRow["ReOrderLevel"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.SKUInventory"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.SKUInventory Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.SKUInventory entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetBySKUInventoryID methods when available
			
			#region AddOnValueCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<AddOnValue>|AddOnValueCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AddOnValueCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.AddOnValueCollection = DataRepository.AddOnValueProvider.GetBySKU(transactionManager, entity.SKU);

				if (deep && entity.AddOnValueCollection.Count > 0)
				{
					deepHandles.Add("AddOnValueCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<AddOnValue>) DataRepository.AddOnValueProvider.DeepLoad,
						new object[] { transactionManager, entity.AddOnValueCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region SKUCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<SKU>|SKUCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SKUCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.SKUCollection = DataRepository.SKUProvider.GetBySKU(transactionManager, entity.SKU);

				if (deep && entity.SKUCollection.Count > 0)
				{
					deepHandles.Add("SKUCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<SKU>) DataRepository.SKUProvider.DeepLoad,
						new object[] { transactionManager, entity.SKUCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.SKUInventory object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.SKUInventory instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.SKUInventory Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.SKUInventory entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<AddOnValue>
				if (CanDeepSave(entity.AddOnValueCollection, "List<AddOnValue>|AddOnValueCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(AddOnValue child in entity.AddOnValueCollection)
					{
						if(child.SKUSource != null)
						{
							child.SKU = child.SKUSource.SKU;
						}
						else
						{
							child.SKU = entity.SKU;
						}

					}

					if (entity.AddOnValueCollection.Count > 0 || entity.AddOnValueCollection.DeletedItems.Count > 0)
					{
						//DataRepository.AddOnValueProvider.Save(transactionManager, entity.AddOnValueCollection);
						
						deepHandles.Add("AddOnValueCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< AddOnValue >) DataRepository.AddOnValueProvider.DeepSave,
							new object[] { transactionManager, entity.AddOnValueCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<SKU>
				if (CanDeepSave(entity.SKUCollection, "List<SKU>|SKUCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(SKU child in entity.SKUCollection)
					{
						if(child.SKUSource != null)
						{
							child.SKU = child.SKUSource.SKU;
						}
						else
						{
							child.SKU = entity.SKU;
						}

					}

					if (entity.SKUCollection.Count > 0 || entity.SKUCollection.DeletedItems.Count > 0)
					{
						//DataRepository.SKUProvider.Save(transactionManager, entity.SKUCollection);
						
						deepHandles.Add("SKUCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< SKU >) DataRepository.SKUProvider.DeepSave,
							new object[] { transactionManager, entity.SKUCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region SKUInventoryChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.SKUInventory</c>
	///</summary>
	public enum SKUInventoryChildEntityTypes
	{
		///<summary>
		/// Collection of <c>SKUInventory</c> as OneToMany for AddOnValueCollection
		///</summary>
		[ChildEntityType(typeof(TList<AddOnValue>))]
		AddOnValueCollection,
		///<summary>
		/// Collection of <c>SKUInventory</c> as OneToMany for SKUCollection
		///</summary>
		[ChildEntityType(typeof(TList<SKU>))]
		SKUCollection,
	}
	
	#endregion SKUInventoryChildEntityTypes
	
	#region SKUInventoryFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;SKUInventoryColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SKUInventory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SKUInventoryFilterBuilder : SqlFilterBuilder<SKUInventoryColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SKUInventoryFilterBuilder class.
		/// </summary>
		public SKUInventoryFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SKUInventoryFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SKUInventoryFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SKUInventoryFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SKUInventoryFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SKUInventoryFilterBuilder
	
	#region SKUInventoryParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;SKUInventoryColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SKUInventory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SKUInventoryParameterBuilder : ParameterizedSqlFilterBuilder<SKUInventoryColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SKUInventoryParameterBuilder class.
		/// </summary>
		public SKUInventoryParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SKUInventoryParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SKUInventoryParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SKUInventoryParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SKUInventoryParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SKUInventoryParameterBuilder
	
	#region SKUInventorySortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;SKUInventoryColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SKUInventory"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class SKUInventorySortBuilder : SqlSortBuilder<SKUInventoryColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SKUInventorySqlSortBuilder class.
		/// </summary>
		public SKUInventorySortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion SKUInventorySortBuilder
	
} // end namespace
