﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ProductTierProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class ProductTierProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.ProductTier, ZNode.Libraries.DataAccess.Entities.ProductTierKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProductTierKey key)
		{
			return Delete(transactionManager, key.ProductTierID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_productTierID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _productTierID)
		{
			return Delete(null, _productTierID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productTierID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _productTierID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductTier_ZNodeProduct key.
		///		FK_ZNodeProductTier_ZNodeProduct Description: 
		/// </summary>
		/// <param name="_productID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductTier objects.</returns>
		public TList<ProductTier> GetByProductID(System.Int32 _productID)
		{
			int count = -1;
			return GetByProductID(_productID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductTier_ZNodeProduct key.
		///		FK_ZNodeProductTier_ZNodeProduct Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductTier objects.</returns>
		/// <remarks></remarks>
		public TList<ProductTier> GetByProductID(TransactionManager transactionManager, System.Int32 _productID)
		{
			int count = -1;
			return GetByProductID(transactionManager, _productID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductTier_ZNodeProduct key.
		///		FK_ZNodeProductTier_ZNodeProduct Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductTier objects.</returns>
		public TList<ProductTier> GetByProductID(TransactionManager transactionManager, System.Int32 _productID, int start, int pageLength)
		{
			int count = -1;
			return GetByProductID(transactionManager, _productID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductTier_ZNodeProduct key.
		///		fKZNodeProductTierZNodeProduct Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_productID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductTier objects.</returns>
		public TList<ProductTier> GetByProductID(System.Int32 _productID, int start, int pageLength)
		{
			int count =  -1;
			return GetByProductID(null, _productID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductTier_ZNodeProduct key.
		///		fKZNodeProductTierZNodeProduct Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_productID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductTier objects.</returns>
		public TList<ProductTier> GetByProductID(System.Int32 _productID, int start, int pageLength,out int count)
		{
			return GetByProductID(null, _productID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductTier_ZNodeProduct key.
		///		FK_ZNodeProductTier_ZNodeProduct Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductTier objects.</returns>
		public abstract TList<ProductTier> GetByProductID(TransactionManager transactionManager, System.Int32 _productID, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.ProductTier Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProductTierKey key, int start, int pageLength)
		{
			return GetByProductTierID(transactionManager, key.ProductTierID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodeProductTier index.
		/// </summary>
		/// <param name="_productTierID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductTier"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductTier GetByProductTierID(System.Int32 _productTierID)
		{
			int count = -1;
			return GetByProductTierID(null,_productTierID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductTier index.
		/// </summary>
		/// <param name="_productTierID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductTier"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductTier GetByProductTierID(System.Int32 _productTierID, int start, int pageLength)
		{
			int count = -1;
			return GetByProductTierID(null, _productTierID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductTier index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productTierID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductTier"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductTier GetByProductTierID(TransactionManager transactionManager, System.Int32 _productTierID)
		{
			int count = -1;
			return GetByProductTierID(transactionManager, _productTierID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductTier index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productTierID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductTier"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductTier GetByProductTierID(TransactionManager transactionManager, System.Int32 _productTierID, int start, int pageLength)
		{
			int count = -1;
			return GetByProductTierID(transactionManager, _productTierID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductTier index.
		/// </summary>
		/// <param name="_productTierID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductTier"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductTier GetByProductTierID(System.Int32 _productTierID, int start, int pageLength, out int count)
		{
			return GetByProductTierID(null, _productTierID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductTier index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productTierID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductTier"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.ProductTier GetByProductTierID(TransactionManager transactionManager, System.Int32 _productTierID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;ProductTier&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;ProductTier&gt;"/></returns>
		public static TList<ProductTier> Fill(IDataReader reader, TList<ProductTier> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.ProductTier c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("ProductTier")
					.Append("|").Append((System.Int32)reader[((int)ProductTierColumn.ProductTierID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<ProductTier>(
					key.ToString(), // EntityTrackingKey
					"ProductTier",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.ProductTier();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.ProductTierID = (System.Int32)reader[((int)ProductTierColumn.ProductTierID - 1)];
					c.ProductID = (System.Int32)reader[((int)ProductTierColumn.ProductID - 1)];
					c.ProfileID = (reader.IsDBNull(((int)ProductTierColumn.ProfileID - 1)))?null:(System.Int32?)reader[((int)ProductTierColumn.ProfileID - 1)];
					c.TierStart = (System.Int32)reader[((int)ProductTierColumn.TierStart - 1)];
					c.TierEnd = (System.Int32)reader[((int)ProductTierColumn.TierEnd - 1)];
					c.Price = (System.Decimal)reader[((int)ProductTierColumn.Price - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.ProductTier"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ProductTier"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.ProductTier entity)
		{
			if (!reader.Read()) return;
			
			entity.ProductTierID = (System.Int32)reader[((int)ProductTierColumn.ProductTierID - 1)];
			entity.ProductID = (System.Int32)reader[((int)ProductTierColumn.ProductID - 1)];
			entity.ProfileID = (reader.IsDBNull(((int)ProductTierColumn.ProfileID - 1)))?null:(System.Int32?)reader[((int)ProductTierColumn.ProfileID - 1)];
			entity.TierStart = (System.Int32)reader[((int)ProductTierColumn.TierStart - 1)];
			entity.TierEnd = (System.Int32)reader[((int)ProductTierColumn.TierEnd - 1)];
			entity.Price = (System.Decimal)reader[((int)ProductTierColumn.Price - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.ProductTier"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ProductTier"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.ProductTier entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.ProductTierID = (System.Int32)dataRow["ProductTierID"];
			entity.ProductID = (System.Int32)dataRow["ProductID"];
			entity.ProfileID = Convert.IsDBNull(dataRow["ProfileID"]) ? null : (System.Int32?)dataRow["ProfileID"];
			entity.TierStart = (System.Int32)dataRow["TierStart"];
			entity.TierEnd = (System.Int32)dataRow["TierEnd"];
			entity.Price = (System.Decimal)dataRow["Price"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ProductTier"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.ProductTier Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProductTier entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region ProductIDSource	
			if (CanDeepLoad(entity, "Product|ProductIDSource", deepLoadType, innerList) 
				&& entity.ProductIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.ProductID;
				Product tmpEntity = EntityManager.LocateEntity<Product>(EntityLocator.ConstructKeyFromPkItems(typeof(Product), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ProductIDSource = tmpEntity;
				else
					entity.ProductIDSource = DataRepository.ProductProvider.GetByProductID(transactionManager, entity.ProductID);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ProductIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ProductIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.ProductProvider.DeepLoad(transactionManager, entity.ProductIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ProductIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.ProductTier object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.ProductTier instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.ProductTier Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProductTier entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region ProductIDSource
			if (CanDeepSave(entity, "Product|ProductIDSource", deepSaveType, innerList) 
				&& entity.ProductIDSource != null)
			{
				DataRepository.ProductProvider.Save(transactionManager, entity.ProductIDSource);
				entity.ProductID = entity.ProductIDSource.ProductID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region ProductTierChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.ProductTier</c>
	///</summary>
	public enum ProductTierChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Product</c> at ProductIDSource
		///</summary>
		[ChildEntityType(typeof(Product))]
		Product,
	}
	
	#endregion ProductTierChildEntityTypes
	
	#region ProductTierFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;ProductTierColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ProductTier"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductTierFilterBuilder : SqlFilterBuilder<ProductTierColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductTierFilterBuilder class.
		/// </summary>
		public ProductTierFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductTierFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductTierFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductTierFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductTierFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductTierFilterBuilder
	
	#region ProductTierParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;ProductTierColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ProductTier"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductTierParameterBuilder : ParameterizedSqlFilterBuilder<ProductTierColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductTierParameterBuilder class.
		/// </summary>
		public ProductTierParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductTierParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductTierParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductTierParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductTierParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductTierParameterBuilder
	
	#region ProductTierSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;ProductTierColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ProductTier"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ProductTierSortBuilder : SqlSortBuilder<ProductTierColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductTierSqlSortBuilder class.
		/// </summary>
		public ProductTierSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ProductTierSortBuilder
	
} // end namespace
