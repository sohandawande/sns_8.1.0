﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ManufacturerProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class ManufacturerProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.Manufacturer, ZNode.Libraries.DataAccess.Entities.ManufacturerKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ManufacturerKey key)
		{
			return Delete(transactionManager, key.ManufacturerID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_manufacturerID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _manufacturerID)
		{
			return Delete(null, _manufacturerID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_manufacturerID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _manufacturerID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.Manufacturer Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ManufacturerKey key, int start, int pageLength)
		{
			return GetByManufacturerID(transactionManager, key.ManufacturerID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeManufacturer_ActiveInd index.
		/// </summary>
		/// <param name="_activeInd"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Manufacturer&gt;"/> class.</returns>
		public TList<Manufacturer> GetByActiveInd(System.Boolean _activeInd)
		{
			int count = -1;
			return GetByActiveInd(null,_activeInd, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeManufacturer_ActiveInd index.
		/// </summary>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Manufacturer&gt;"/> class.</returns>
		public TList<Manufacturer> GetByActiveInd(System.Boolean _activeInd, int start, int pageLength)
		{
			int count = -1;
			return GetByActiveInd(null, _activeInd, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeManufacturer_ActiveInd index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_activeInd"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Manufacturer&gt;"/> class.</returns>
		public TList<Manufacturer> GetByActiveInd(TransactionManager transactionManager, System.Boolean _activeInd)
		{
			int count = -1;
			return GetByActiveInd(transactionManager, _activeInd, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeManufacturer_ActiveInd index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Manufacturer&gt;"/> class.</returns>
		public TList<Manufacturer> GetByActiveInd(TransactionManager transactionManager, System.Boolean _activeInd, int start, int pageLength)
		{
			int count = -1;
			return GetByActiveInd(transactionManager, _activeInd, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeManufacturer_ActiveInd index.
		/// </summary>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Manufacturer&gt;"/> class.</returns>
		public TList<Manufacturer> GetByActiveInd(System.Boolean _activeInd, int start, int pageLength, out int count)
		{
			return GetByActiveInd(null, _activeInd, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeManufacturer_ActiveInd index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Manufacturer&gt;"/> class.</returns>
		public abstract TList<Manufacturer> GetByActiveInd(TransactionManager transactionManager, System.Boolean _activeInd, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeManufacturer_PortalID index.
		/// </summary>
		/// <param name="_portalID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Manufacturer&gt;"/> class.</returns>
		public TList<Manufacturer> GetByPortalID(System.Int32? _portalID)
		{
			int count = -1;
			return GetByPortalID(null,_portalID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeManufacturer_PortalID index.
		/// </summary>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Manufacturer&gt;"/> class.</returns>
		public TList<Manufacturer> GetByPortalID(System.Int32? _portalID, int start, int pageLength)
		{
			int count = -1;
			return GetByPortalID(null, _portalID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeManufacturer_PortalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Manufacturer&gt;"/> class.</returns>
		public TList<Manufacturer> GetByPortalID(TransactionManager transactionManager, System.Int32? _portalID)
		{
			int count = -1;
			return GetByPortalID(transactionManager, _portalID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeManufacturer_PortalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Manufacturer&gt;"/> class.</returns>
		public TList<Manufacturer> GetByPortalID(TransactionManager transactionManager, System.Int32? _portalID, int start, int pageLength)
		{
			int count = -1;
			return GetByPortalID(transactionManager, _portalID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeManufacturer_PortalID index.
		/// </summary>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Manufacturer&gt;"/> class.</returns>
		public TList<Manufacturer> GetByPortalID(System.Int32? _portalID, int start, int pageLength, out int count)
		{
			return GetByPortalID(null, _portalID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeManufacturer_PortalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Manufacturer&gt;"/> class.</returns>
		public abstract TList<Manufacturer> GetByPortalID(TransactionManager transactionManager, System.Int32? _portalID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_SC_Manufacturer index.
		/// </summary>
		/// <param name="_manufacturerID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Manufacturer"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Manufacturer GetByManufacturerID(System.Int32 _manufacturerID)
		{
			int count = -1;
			return GetByManufacturerID(null,_manufacturerID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SC_Manufacturer index.
		/// </summary>
		/// <param name="_manufacturerID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Manufacturer"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Manufacturer GetByManufacturerID(System.Int32 _manufacturerID, int start, int pageLength)
		{
			int count = -1;
			return GetByManufacturerID(null, _manufacturerID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SC_Manufacturer index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_manufacturerID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Manufacturer"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Manufacturer GetByManufacturerID(TransactionManager transactionManager, System.Int32 _manufacturerID)
		{
			int count = -1;
			return GetByManufacturerID(transactionManager, _manufacturerID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SC_Manufacturer index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_manufacturerID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Manufacturer"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Manufacturer GetByManufacturerID(TransactionManager transactionManager, System.Int32 _manufacturerID, int start, int pageLength)
		{
			int count = -1;
			return GetByManufacturerID(transactionManager, _manufacturerID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SC_Manufacturer index.
		/// </summary>
		/// <param name="_manufacturerID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Manufacturer"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Manufacturer GetByManufacturerID(System.Int32 _manufacturerID, int start, int pageLength, out int count)
		{
			return GetByManufacturerID(null, _manufacturerID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SC_Manufacturer index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_manufacturerID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Manufacturer"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.Manufacturer GetByManufacturerID(TransactionManager transactionManager, System.Int32 _manufacturerID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;Manufacturer&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;Manufacturer&gt;"/></returns>
		public static TList<Manufacturer> Fill(IDataReader reader, TList<Manufacturer> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.Manufacturer c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("Manufacturer")
					.Append("|").Append((System.Int32)reader[((int)ManufacturerColumn.ManufacturerID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<Manufacturer>(
					key.ToString(), // EntityTrackingKey
					"Manufacturer",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.Manufacturer();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.ManufacturerID = (System.Int32)reader[((int)ManufacturerColumn.ManufacturerID - 1)];
					c.Name = (reader.IsDBNull(((int)ManufacturerColumn.Name - 1)))?null:(System.String)reader[((int)ManufacturerColumn.Name - 1)];
					c.Description = (reader.IsDBNull(((int)ManufacturerColumn.Description - 1)))?null:(System.String)reader[((int)ManufacturerColumn.Description - 1)];
					c.WebsiteLink = (reader.IsDBNull(((int)ManufacturerColumn.WebsiteLink - 1)))?null:(System.String)reader[((int)ManufacturerColumn.WebsiteLink - 1)];
					c.EmailID = (reader.IsDBNull(((int)ManufacturerColumn.EmailID - 1)))?null:(System.String)reader[((int)ManufacturerColumn.EmailID - 1)];
					c.IsDropShipper = (reader.IsDBNull(((int)ManufacturerColumn.IsDropShipper - 1)))?null:(System.Boolean?)reader[((int)ManufacturerColumn.IsDropShipper - 1)];
					c.EmailNotificationTemplate = (reader.IsDBNull(((int)ManufacturerColumn.EmailNotificationTemplate - 1)))?null:(System.String)reader[((int)ManufacturerColumn.EmailNotificationTemplate - 1)];
					c.DisplayOrder = (System.Int32)reader[((int)ManufacturerColumn.DisplayOrder - 1)];
					c.ActiveInd = (System.Boolean)reader[((int)ManufacturerColumn.ActiveInd - 1)];
					c.Custom1 = (reader.IsDBNull(((int)ManufacturerColumn.Custom1 - 1)))?null:(System.String)reader[((int)ManufacturerColumn.Custom1 - 1)];
					c.Custom2 = (reader.IsDBNull(((int)ManufacturerColumn.Custom2 - 1)))?null:(System.String)reader[((int)ManufacturerColumn.Custom2 - 1)];
					c.Custom3 = (reader.IsDBNull(((int)ManufacturerColumn.Custom3 - 1)))?null:(System.String)reader[((int)ManufacturerColumn.Custom3 - 1)];
					c.PortalID = (reader.IsDBNull(((int)ManufacturerColumn.PortalID - 1)))?null:(System.Int32?)reader[((int)ManufacturerColumn.PortalID - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.Manufacturer"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.Manufacturer"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.Manufacturer entity)
		{
			if (!reader.Read()) return;
			
			entity.ManufacturerID = (System.Int32)reader[((int)ManufacturerColumn.ManufacturerID - 1)];
			entity.Name = (reader.IsDBNull(((int)ManufacturerColumn.Name - 1)))?null:(System.String)reader[((int)ManufacturerColumn.Name - 1)];
			entity.Description = (reader.IsDBNull(((int)ManufacturerColumn.Description - 1)))?null:(System.String)reader[((int)ManufacturerColumn.Description - 1)];
			entity.WebsiteLink = (reader.IsDBNull(((int)ManufacturerColumn.WebsiteLink - 1)))?null:(System.String)reader[((int)ManufacturerColumn.WebsiteLink - 1)];
			entity.EmailID = (reader.IsDBNull(((int)ManufacturerColumn.EmailID - 1)))?null:(System.String)reader[((int)ManufacturerColumn.EmailID - 1)];
			entity.IsDropShipper = (reader.IsDBNull(((int)ManufacturerColumn.IsDropShipper - 1)))?null:(System.Boolean?)reader[((int)ManufacturerColumn.IsDropShipper - 1)];
			entity.EmailNotificationTemplate = (reader.IsDBNull(((int)ManufacturerColumn.EmailNotificationTemplate - 1)))?null:(System.String)reader[((int)ManufacturerColumn.EmailNotificationTemplate - 1)];
			entity.DisplayOrder = (System.Int32)reader[((int)ManufacturerColumn.DisplayOrder - 1)];
			entity.ActiveInd = (System.Boolean)reader[((int)ManufacturerColumn.ActiveInd - 1)];
			entity.Custom1 = (reader.IsDBNull(((int)ManufacturerColumn.Custom1 - 1)))?null:(System.String)reader[((int)ManufacturerColumn.Custom1 - 1)];
			entity.Custom2 = (reader.IsDBNull(((int)ManufacturerColumn.Custom2 - 1)))?null:(System.String)reader[((int)ManufacturerColumn.Custom2 - 1)];
			entity.Custom3 = (reader.IsDBNull(((int)ManufacturerColumn.Custom3 - 1)))?null:(System.String)reader[((int)ManufacturerColumn.Custom3 - 1)];
			entity.PortalID = (reader.IsDBNull(((int)ManufacturerColumn.PortalID - 1)))?null:(System.Int32?)reader[((int)ManufacturerColumn.PortalID - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.Manufacturer"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.Manufacturer"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.Manufacturer entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.ManufacturerID = (System.Int32)dataRow["ManufacturerID"];
			entity.Name = Convert.IsDBNull(dataRow["Name"]) ? null : (System.String)dataRow["Name"];
			entity.Description = Convert.IsDBNull(dataRow["Description"]) ? null : (System.String)dataRow["Description"];
			entity.WebsiteLink = Convert.IsDBNull(dataRow["WebsiteLink"]) ? null : (System.String)dataRow["WebsiteLink"];
			entity.EmailID = Convert.IsDBNull(dataRow["EmailID"]) ? null : (System.String)dataRow["EmailID"];
			entity.IsDropShipper = Convert.IsDBNull(dataRow["IsDropShipper"]) ? null : (System.Boolean?)dataRow["IsDropShipper"];
			entity.EmailNotificationTemplate = Convert.IsDBNull(dataRow["EmailNotificationTemplate"]) ? null : (System.String)dataRow["EmailNotificationTemplate"];
			entity.DisplayOrder = (System.Int32)dataRow["DisplayOrder"];
			entity.ActiveInd = (System.Boolean)dataRow["ActiveInd"];
			entity.Custom1 = Convert.IsDBNull(dataRow["Custom1"]) ? null : (System.String)dataRow["Custom1"];
			entity.Custom2 = Convert.IsDBNull(dataRow["Custom2"]) ? null : (System.String)dataRow["Custom2"];
			entity.Custom3 = Convert.IsDBNull(dataRow["Custom3"]) ? null : (System.String)dataRow["Custom3"];
			entity.PortalID = Convert.IsDBNull(dataRow["PortalID"]) ? null : (System.Int32?)dataRow["PortalID"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.Manufacturer"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.Manufacturer Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.Manufacturer entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region PortalIDSource	
			if (CanDeepLoad(entity, "Portal|PortalIDSource", deepLoadType, innerList) 
				&& entity.PortalIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.PortalID ?? (int)0);
				Portal tmpEntity = EntityManager.LocateEntity<Portal>(EntityLocator.ConstructKeyFromPkItems(typeof(Portal), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.PortalIDSource = tmpEntity;
				else
					entity.PortalIDSource = DataRepository.PortalProvider.GetByPortalID(transactionManager, (entity.PortalID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PortalIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.PortalIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.PortalProvider.DeepLoad(transactionManager, entity.PortalIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion PortalIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByManufacturerID methods when available
			
			#region ProductCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Product>|ProductCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ProductCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ProductCollection = DataRepository.ProductProvider.GetByManufacturerID(transactionManager, entity.ManufacturerID);

				if (deep && entity.ProductCollection.Count > 0)
				{
					deepHandles.Add("ProductCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Product>) DataRepository.ProductProvider.DeepLoad,
						new object[] { transactionManager, entity.ProductCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region PromotionCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Promotion>|PromotionCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PromotionCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.PromotionCollection = DataRepository.PromotionProvider.GetByManufacturerID(transactionManager, entity.ManufacturerID);

				if (deep && entity.PromotionCollection.Count > 0)
				{
					deepHandles.Add("PromotionCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Promotion>) DataRepository.PromotionProvider.DeepLoad,
						new object[] { transactionManager, entity.PromotionCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.Manufacturer object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.Manufacturer instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.Manufacturer Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.Manufacturer entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region PortalIDSource
			if (CanDeepSave(entity, "Portal|PortalIDSource", deepSaveType, innerList) 
				&& entity.PortalIDSource != null)
			{
				DataRepository.PortalProvider.Save(transactionManager, entity.PortalIDSource);
				entity.PortalID = entity.PortalIDSource.PortalID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<Product>
				if (CanDeepSave(entity.ProductCollection, "List<Product>|ProductCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Product child in entity.ProductCollection)
					{
						if(child.ManufacturerIDSource != null)
						{
							child.ManufacturerID = child.ManufacturerIDSource.ManufacturerID;
						}
						else
						{
							child.ManufacturerID = entity.ManufacturerID;
						}

					}

					if (entity.ProductCollection.Count > 0 || entity.ProductCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ProductProvider.Save(transactionManager, entity.ProductCollection);
						
						deepHandles.Add("ProductCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Product >) DataRepository.ProductProvider.DeepSave,
							new object[] { transactionManager, entity.ProductCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Promotion>
				if (CanDeepSave(entity.PromotionCollection, "List<Promotion>|PromotionCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Promotion child in entity.PromotionCollection)
					{
						if(child.ManufacturerIDSource != null)
						{
							child.ManufacturerID = child.ManufacturerIDSource.ManufacturerID;
						}
						else
						{
							child.ManufacturerID = entity.ManufacturerID;
						}

					}

					if (entity.PromotionCollection.Count > 0 || entity.PromotionCollection.DeletedItems.Count > 0)
					{
						//DataRepository.PromotionProvider.Save(transactionManager, entity.PromotionCollection);
						
						deepHandles.Add("PromotionCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Promotion >) DataRepository.PromotionProvider.DeepSave,
							new object[] { transactionManager, entity.PromotionCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region ManufacturerChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.Manufacturer</c>
	///</summary>
	public enum ManufacturerChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Portal</c> at PortalIDSource
		///</summary>
		[ChildEntityType(typeof(Portal))]
		Portal,
		///<summary>
		/// Collection of <c>Manufacturer</c> as OneToMany for ProductCollection
		///</summary>
		[ChildEntityType(typeof(TList<Product>))]
		ProductCollection,
		///<summary>
		/// Collection of <c>Manufacturer</c> as OneToMany for PromotionCollection
		///</summary>
		[ChildEntityType(typeof(TList<Promotion>))]
		PromotionCollection,
	}
	
	#endregion ManufacturerChildEntityTypes
	
	#region ManufacturerFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;ManufacturerColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Manufacturer"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ManufacturerFilterBuilder : SqlFilterBuilder<ManufacturerColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ManufacturerFilterBuilder class.
		/// </summary>
		public ManufacturerFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ManufacturerFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ManufacturerFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ManufacturerFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ManufacturerFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ManufacturerFilterBuilder
	
	#region ManufacturerParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;ManufacturerColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Manufacturer"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ManufacturerParameterBuilder : ParameterizedSqlFilterBuilder<ManufacturerColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ManufacturerParameterBuilder class.
		/// </summary>
		public ManufacturerParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ManufacturerParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ManufacturerParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ManufacturerParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ManufacturerParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ManufacturerParameterBuilder
	
	#region ManufacturerSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;ManufacturerColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Manufacturer"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ManufacturerSortBuilder : SqlSortBuilder<ManufacturerColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ManufacturerSqlSortBuilder class.
		/// </summary>
		public ManufacturerSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ManufacturerSortBuilder
	
} // end namespace
