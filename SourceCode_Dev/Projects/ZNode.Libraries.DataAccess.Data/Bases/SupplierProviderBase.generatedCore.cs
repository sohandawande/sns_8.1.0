﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="SupplierProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class SupplierProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.Supplier, ZNode.Libraries.DataAccess.Entities.SupplierKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.SupplierKey key)
		{
			return Delete(transactionManager, key.SupplierID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_supplierID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _supplierID)
		{
			return Delete(null, _supplierID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_supplierID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _supplierID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.Supplier Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.SupplierKey key, int start, int pageLength)
		{
			return GetBySupplierID(transactionManager, key.SupplierID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeSupplier index.
		/// </summary>
		/// <param name="_externalSupplierNo"></param>
		/// <param name="_name"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Supplier&gt;"/> class.</returns>
		public TList<Supplier> GetByExternalSupplierNoName(System.String _externalSupplierNo, System.String _name)
		{
			int count = -1;
			return GetByExternalSupplierNoName(null,_externalSupplierNo, _name, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSupplier index.
		/// </summary>
		/// <param name="_externalSupplierNo"></param>
		/// <param name="_name"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Supplier&gt;"/> class.</returns>
		public TList<Supplier> GetByExternalSupplierNoName(System.String _externalSupplierNo, System.String _name, int start, int pageLength)
		{
			int count = -1;
			return GetByExternalSupplierNoName(null, _externalSupplierNo, _name, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSupplier index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_externalSupplierNo"></param>
		/// <param name="_name"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Supplier&gt;"/> class.</returns>
		public TList<Supplier> GetByExternalSupplierNoName(TransactionManager transactionManager, System.String _externalSupplierNo, System.String _name)
		{
			int count = -1;
			return GetByExternalSupplierNoName(transactionManager, _externalSupplierNo, _name, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSupplier index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_externalSupplierNo"></param>
		/// <param name="_name"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Supplier&gt;"/> class.</returns>
		public TList<Supplier> GetByExternalSupplierNoName(TransactionManager transactionManager, System.String _externalSupplierNo, System.String _name, int start, int pageLength)
		{
			int count = -1;
			return GetByExternalSupplierNoName(transactionManager, _externalSupplierNo, _name, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSupplier index.
		/// </summary>
		/// <param name="_externalSupplierNo"></param>
		/// <param name="_name"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Supplier&gt;"/> class.</returns>
		public TList<Supplier> GetByExternalSupplierNoName(System.String _externalSupplierNo, System.String _name, int start, int pageLength, out int count)
		{
			return GetByExternalSupplierNoName(null, _externalSupplierNo, _name, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSupplier index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_externalSupplierNo"></param>
		/// <param name="_name"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Supplier&gt;"/> class.</returns>
		public abstract TList<Supplier> GetByExternalSupplierNoName(TransactionManager transactionManager, System.String _externalSupplierNo, System.String _name, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeSupplier_ActiveInd index.
		/// </summary>
		/// <param name="_activeInd"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Supplier&gt;"/> class.</returns>
		public TList<Supplier> GetByActiveInd(System.Boolean _activeInd)
		{
			int count = -1;
			return GetByActiveInd(null,_activeInd, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSupplier_ActiveInd index.
		/// </summary>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Supplier&gt;"/> class.</returns>
		public TList<Supplier> GetByActiveInd(System.Boolean _activeInd, int start, int pageLength)
		{
			int count = -1;
			return GetByActiveInd(null, _activeInd, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSupplier_ActiveInd index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_activeInd"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Supplier&gt;"/> class.</returns>
		public TList<Supplier> GetByActiveInd(TransactionManager transactionManager, System.Boolean _activeInd)
		{
			int count = -1;
			return GetByActiveInd(transactionManager, _activeInd, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSupplier_ActiveInd index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Supplier&gt;"/> class.</returns>
		public TList<Supplier> GetByActiveInd(TransactionManager transactionManager, System.Boolean _activeInd, int start, int pageLength)
		{
			int count = -1;
			return GetByActiveInd(transactionManager, _activeInd, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSupplier_ActiveInd index.
		/// </summary>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Supplier&gt;"/> class.</returns>
		public TList<Supplier> GetByActiveInd(System.Boolean _activeInd, int start, int pageLength, out int count)
		{
			return GetByActiveInd(null, _activeInd, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSupplier_ActiveInd index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Supplier&gt;"/> class.</returns>
		public abstract TList<Supplier> GetByActiveInd(TransactionManager transactionManager, System.Boolean _activeInd, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeSupplier_ExternalID index.
		/// </summary>
		/// <param name="_externalID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Supplier&gt;"/> class.</returns>
		public TList<Supplier> GetByExternalID(System.String _externalID)
		{
			int count = -1;
			return GetByExternalID(null,_externalID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSupplier_ExternalID index.
		/// </summary>
		/// <param name="_externalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Supplier&gt;"/> class.</returns>
		public TList<Supplier> GetByExternalID(System.String _externalID, int start, int pageLength)
		{
			int count = -1;
			return GetByExternalID(null, _externalID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSupplier_ExternalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_externalID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Supplier&gt;"/> class.</returns>
		public TList<Supplier> GetByExternalID(TransactionManager transactionManager, System.String _externalID)
		{
			int count = -1;
			return GetByExternalID(transactionManager, _externalID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSupplier_ExternalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_externalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Supplier&gt;"/> class.</returns>
		public TList<Supplier> GetByExternalID(TransactionManager transactionManager, System.String _externalID, int start, int pageLength)
		{
			int count = -1;
			return GetByExternalID(transactionManager, _externalID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSupplier_ExternalID index.
		/// </summary>
		/// <param name="_externalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Supplier&gt;"/> class.</returns>
		public TList<Supplier> GetByExternalID(System.String _externalID, int start, int pageLength, out int count)
		{
			return GetByExternalID(null, _externalID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSupplier_ExternalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_externalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Supplier&gt;"/> class.</returns>
		public abstract TList<Supplier> GetByExternalID(TransactionManager transactionManager, System.String _externalID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeSupplier_ExternalSupplierNo index.
		/// </summary>
		/// <param name="_externalSupplierNo"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Supplier&gt;"/> class.</returns>
		public TList<Supplier> GetByExternalSupplierNo(System.String _externalSupplierNo)
		{
			int count = -1;
			return GetByExternalSupplierNo(null,_externalSupplierNo, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSupplier_ExternalSupplierNo index.
		/// </summary>
		/// <param name="_externalSupplierNo"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Supplier&gt;"/> class.</returns>
		public TList<Supplier> GetByExternalSupplierNo(System.String _externalSupplierNo, int start, int pageLength)
		{
			int count = -1;
			return GetByExternalSupplierNo(null, _externalSupplierNo, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSupplier_ExternalSupplierNo index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_externalSupplierNo"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Supplier&gt;"/> class.</returns>
		public TList<Supplier> GetByExternalSupplierNo(TransactionManager transactionManager, System.String _externalSupplierNo)
		{
			int count = -1;
			return GetByExternalSupplierNo(transactionManager, _externalSupplierNo, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSupplier_ExternalSupplierNo index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_externalSupplierNo"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Supplier&gt;"/> class.</returns>
		public TList<Supplier> GetByExternalSupplierNo(TransactionManager transactionManager, System.String _externalSupplierNo, int start, int pageLength)
		{
			int count = -1;
			return GetByExternalSupplierNo(transactionManager, _externalSupplierNo, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSupplier_ExternalSupplierNo index.
		/// </summary>
		/// <param name="_externalSupplierNo"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Supplier&gt;"/> class.</returns>
		public TList<Supplier> GetByExternalSupplierNo(System.String _externalSupplierNo, int start, int pageLength, out int count)
		{
			return GetByExternalSupplierNo(null, _externalSupplierNo, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSupplier_ExternalSupplierNo index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_externalSupplierNo"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Supplier&gt;"/> class.</returns>
		public abstract TList<Supplier> GetByExternalSupplierNo(TransactionManager transactionManager, System.String _externalSupplierNo, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeSupplier_Name index.
		/// </summary>
		/// <param name="_name"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Supplier&gt;"/> class.</returns>
		public TList<Supplier> GetByName(System.String _name)
		{
			int count = -1;
			return GetByName(null,_name, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSupplier_Name index.
		/// </summary>
		/// <param name="_name"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Supplier&gt;"/> class.</returns>
		public TList<Supplier> GetByName(System.String _name, int start, int pageLength)
		{
			int count = -1;
			return GetByName(null, _name, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSupplier_Name index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_name"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Supplier&gt;"/> class.</returns>
		public TList<Supplier> GetByName(TransactionManager transactionManager, System.String _name)
		{
			int count = -1;
			return GetByName(transactionManager, _name, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSupplier_Name index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_name"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Supplier&gt;"/> class.</returns>
		public TList<Supplier> GetByName(TransactionManager transactionManager, System.String _name, int start, int pageLength)
		{
			int count = -1;
			return GetByName(transactionManager, _name, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSupplier_Name index.
		/// </summary>
		/// <param name="_name"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Supplier&gt;"/> class.</returns>
		public TList<Supplier> GetByName(System.String _name, int start, int pageLength, out int count)
		{
			return GetByName(null, _name, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSupplier_Name index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_name"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Supplier&gt;"/> class.</returns>
		public abstract TList<Supplier> GetByName(TransactionManager transactionManager, System.String _name, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeSupplier_PortalID index.
		/// </summary>
		/// <param name="_portalID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Supplier&gt;"/> class.</returns>
		public TList<Supplier> GetByPortalID(System.Int32? _portalID)
		{
			int count = -1;
			return GetByPortalID(null,_portalID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSupplier_PortalID index.
		/// </summary>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Supplier&gt;"/> class.</returns>
		public TList<Supplier> GetByPortalID(System.Int32? _portalID, int start, int pageLength)
		{
			int count = -1;
			return GetByPortalID(null, _portalID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSupplier_PortalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Supplier&gt;"/> class.</returns>
		public TList<Supplier> GetByPortalID(TransactionManager transactionManager, System.Int32? _portalID)
		{
			int count = -1;
			return GetByPortalID(transactionManager, _portalID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSupplier_PortalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Supplier&gt;"/> class.</returns>
		public TList<Supplier> GetByPortalID(TransactionManager transactionManager, System.Int32? _portalID, int start, int pageLength)
		{
			int count = -1;
			return GetByPortalID(transactionManager, _portalID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSupplier_PortalID index.
		/// </summary>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Supplier&gt;"/> class.</returns>
		public TList<Supplier> GetByPortalID(System.Int32? _portalID, int start, int pageLength, out int count)
		{
			return GetByPortalID(null, _portalID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSupplier_PortalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Supplier&gt;"/> class.</returns>
		public abstract TList<Supplier> GetByPortalID(TransactionManager transactionManager, System.Int32? _portalID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeSupplier_SupplierTypeID index.
		/// </summary>
		/// <param name="_supplierTypeID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Supplier&gt;"/> class.</returns>
		public TList<Supplier> GetBySupplierTypeID(System.Int32? _supplierTypeID)
		{
			int count = -1;
			return GetBySupplierTypeID(null,_supplierTypeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSupplier_SupplierTypeID index.
		/// </summary>
		/// <param name="_supplierTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Supplier&gt;"/> class.</returns>
		public TList<Supplier> GetBySupplierTypeID(System.Int32? _supplierTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetBySupplierTypeID(null, _supplierTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSupplier_SupplierTypeID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_supplierTypeID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Supplier&gt;"/> class.</returns>
		public TList<Supplier> GetBySupplierTypeID(TransactionManager transactionManager, System.Int32? _supplierTypeID)
		{
			int count = -1;
			return GetBySupplierTypeID(transactionManager, _supplierTypeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSupplier_SupplierTypeID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_supplierTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Supplier&gt;"/> class.</returns>
		public TList<Supplier> GetBySupplierTypeID(TransactionManager transactionManager, System.Int32? _supplierTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetBySupplierTypeID(transactionManager, _supplierTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSupplier_SupplierTypeID index.
		/// </summary>
		/// <param name="_supplierTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Supplier&gt;"/> class.</returns>
		public TList<Supplier> GetBySupplierTypeID(System.Int32? _supplierTypeID, int start, int pageLength, out int count)
		{
			return GetBySupplierTypeID(null, _supplierTypeID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSupplier_SupplierTypeID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_supplierTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Supplier&gt;"/> class.</returns>
		public abstract TList<Supplier> GetBySupplierTypeID(TransactionManager transactionManager, System.Int32? _supplierTypeID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodeSupplier index.
		/// </summary>
		/// <param name="_supplierID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Supplier"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Supplier GetBySupplierID(System.Int32 _supplierID)
		{
			int count = -1;
			return GetBySupplierID(null,_supplierID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeSupplier index.
		/// </summary>
		/// <param name="_supplierID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Supplier"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Supplier GetBySupplierID(System.Int32 _supplierID, int start, int pageLength)
		{
			int count = -1;
			return GetBySupplierID(null, _supplierID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeSupplier index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_supplierID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Supplier"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Supplier GetBySupplierID(TransactionManager transactionManager, System.Int32 _supplierID)
		{
			int count = -1;
			return GetBySupplierID(transactionManager, _supplierID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeSupplier index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_supplierID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Supplier"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Supplier GetBySupplierID(TransactionManager transactionManager, System.Int32 _supplierID, int start, int pageLength)
		{
			int count = -1;
			return GetBySupplierID(transactionManager, _supplierID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeSupplier index.
		/// </summary>
		/// <param name="_supplierID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Supplier"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Supplier GetBySupplierID(System.Int32 _supplierID, int start, int pageLength, out int count)
		{
			return GetBySupplierID(null, _supplierID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeSupplier index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_supplierID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Supplier"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.Supplier GetBySupplierID(TransactionManager transactionManager, System.Int32 _supplierID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;Supplier&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;Supplier&gt;"/></returns>
		public static TList<Supplier> Fill(IDataReader reader, TList<Supplier> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.Supplier c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("Supplier")
					.Append("|").Append((System.Int32)reader[((int)SupplierColumn.SupplierID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<Supplier>(
					key.ToString(), // EntityTrackingKey
					"Supplier",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.Supplier();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.SupplierID = (System.Int32)reader[((int)SupplierColumn.SupplierID - 1)];
					c.SupplierTypeID = (reader.IsDBNull(((int)SupplierColumn.SupplierTypeID - 1)))?null:(System.Int32?)reader[((int)SupplierColumn.SupplierTypeID - 1)];
					c.ExternalSupplierNo = (reader.IsDBNull(((int)SupplierColumn.ExternalSupplierNo - 1)))?null:(System.String)reader[((int)SupplierColumn.ExternalSupplierNo - 1)];
					c.Name = (System.String)reader[((int)SupplierColumn.Name - 1)];
					c.Description = (reader.IsDBNull(((int)SupplierColumn.Description - 1)))?null:(System.String)reader[((int)SupplierColumn.Description - 1)];
					c.ContactFirstName = (reader.IsDBNull(((int)SupplierColumn.ContactFirstName - 1)))?null:(System.String)reader[((int)SupplierColumn.ContactFirstName - 1)];
					c.ContactLastName = (reader.IsDBNull(((int)SupplierColumn.ContactLastName - 1)))?null:(System.String)reader[((int)SupplierColumn.ContactLastName - 1)];
					c.ContactPhone = (reader.IsDBNull(((int)SupplierColumn.ContactPhone - 1)))?null:(System.String)reader[((int)SupplierColumn.ContactPhone - 1)];
					c.ContactEmail = (reader.IsDBNull(((int)SupplierColumn.ContactEmail - 1)))?null:(System.String)reader[((int)SupplierColumn.ContactEmail - 1)];
					c.NotificationEmailID = (reader.IsDBNull(((int)SupplierColumn.NotificationEmailID - 1)))?null:(System.String)reader[((int)SupplierColumn.NotificationEmailID - 1)];
					c.EmailNotificationTemplate = (reader.IsDBNull(((int)SupplierColumn.EmailNotificationTemplate - 1)))?null:(System.String)reader[((int)SupplierColumn.EmailNotificationTemplate - 1)];
					c.EnableEmailNotification = (System.Boolean)reader[((int)SupplierColumn.EnableEmailNotification - 1)];
					c.DisplayOrder = (reader.IsDBNull(((int)SupplierColumn.DisplayOrder - 1)))?null:(System.Int32?)reader[((int)SupplierColumn.DisplayOrder - 1)];
					c.ActiveInd = (System.Boolean)reader[((int)SupplierColumn.ActiveInd - 1)];
					c.Custom1 = (reader.IsDBNull(((int)SupplierColumn.Custom1 - 1)))?null:(System.String)reader[((int)SupplierColumn.Custom1 - 1)];
					c.Custom2 = (reader.IsDBNull(((int)SupplierColumn.Custom2 - 1)))?null:(System.String)reader[((int)SupplierColumn.Custom2 - 1)];
					c.Custom3 = (reader.IsDBNull(((int)SupplierColumn.Custom3 - 1)))?null:(System.String)reader[((int)SupplierColumn.Custom3 - 1)];
					c.Custom4 = (reader.IsDBNull(((int)SupplierColumn.Custom4 - 1)))?null:(System.String)reader[((int)SupplierColumn.Custom4 - 1)];
					c.Custom5 = (reader.IsDBNull(((int)SupplierColumn.Custom5 - 1)))?null:(System.String)reader[((int)SupplierColumn.Custom5 - 1)];
					c.PortalID = (reader.IsDBNull(((int)SupplierColumn.PortalID - 1)))?null:(System.Int32?)reader[((int)SupplierColumn.PortalID - 1)];
					c.ExternalID = (reader.IsDBNull(((int)SupplierColumn.ExternalID - 1)))?null:(System.String)reader[((int)SupplierColumn.ExternalID - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.Supplier"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.Supplier"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.Supplier entity)
		{
			if (!reader.Read()) return;
			
			entity.SupplierID = (System.Int32)reader[((int)SupplierColumn.SupplierID - 1)];
			entity.SupplierTypeID = (reader.IsDBNull(((int)SupplierColumn.SupplierTypeID - 1)))?null:(System.Int32?)reader[((int)SupplierColumn.SupplierTypeID - 1)];
			entity.ExternalSupplierNo = (reader.IsDBNull(((int)SupplierColumn.ExternalSupplierNo - 1)))?null:(System.String)reader[((int)SupplierColumn.ExternalSupplierNo - 1)];
			entity.Name = (System.String)reader[((int)SupplierColumn.Name - 1)];
			entity.Description = (reader.IsDBNull(((int)SupplierColumn.Description - 1)))?null:(System.String)reader[((int)SupplierColumn.Description - 1)];
			entity.ContactFirstName = (reader.IsDBNull(((int)SupplierColumn.ContactFirstName - 1)))?null:(System.String)reader[((int)SupplierColumn.ContactFirstName - 1)];
			entity.ContactLastName = (reader.IsDBNull(((int)SupplierColumn.ContactLastName - 1)))?null:(System.String)reader[((int)SupplierColumn.ContactLastName - 1)];
			entity.ContactPhone = (reader.IsDBNull(((int)SupplierColumn.ContactPhone - 1)))?null:(System.String)reader[((int)SupplierColumn.ContactPhone - 1)];
			entity.ContactEmail = (reader.IsDBNull(((int)SupplierColumn.ContactEmail - 1)))?null:(System.String)reader[((int)SupplierColumn.ContactEmail - 1)];
			entity.NotificationEmailID = (reader.IsDBNull(((int)SupplierColumn.NotificationEmailID - 1)))?null:(System.String)reader[((int)SupplierColumn.NotificationEmailID - 1)];
			entity.EmailNotificationTemplate = (reader.IsDBNull(((int)SupplierColumn.EmailNotificationTemplate - 1)))?null:(System.String)reader[((int)SupplierColumn.EmailNotificationTemplate - 1)];
			entity.EnableEmailNotification = (System.Boolean)reader[((int)SupplierColumn.EnableEmailNotification - 1)];
			entity.DisplayOrder = (reader.IsDBNull(((int)SupplierColumn.DisplayOrder - 1)))?null:(System.Int32?)reader[((int)SupplierColumn.DisplayOrder - 1)];
			entity.ActiveInd = (System.Boolean)reader[((int)SupplierColumn.ActiveInd - 1)];
			entity.Custom1 = (reader.IsDBNull(((int)SupplierColumn.Custom1 - 1)))?null:(System.String)reader[((int)SupplierColumn.Custom1 - 1)];
			entity.Custom2 = (reader.IsDBNull(((int)SupplierColumn.Custom2 - 1)))?null:(System.String)reader[((int)SupplierColumn.Custom2 - 1)];
			entity.Custom3 = (reader.IsDBNull(((int)SupplierColumn.Custom3 - 1)))?null:(System.String)reader[((int)SupplierColumn.Custom3 - 1)];
			entity.Custom4 = (reader.IsDBNull(((int)SupplierColumn.Custom4 - 1)))?null:(System.String)reader[((int)SupplierColumn.Custom4 - 1)];
			entity.Custom5 = (reader.IsDBNull(((int)SupplierColumn.Custom5 - 1)))?null:(System.String)reader[((int)SupplierColumn.Custom5 - 1)];
			entity.PortalID = (reader.IsDBNull(((int)SupplierColumn.PortalID - 1)))?null:(System.Int32?)reader[((int)SupplierColumn.PortalID - 1)];
			entity.ExternalID = (reader.IsDBNull(((int)SupplierColumn.ExternalID - 1)))?null:(System.String)reader[((int)SupplierColumn.ExternalID - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.Supplier"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.Supplier"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.Supplier entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.SupplierID = (System.Int32)dataRow["SupplierID"];
			entity.SupplierTypeID = Convert.IsDBNull(dataRow["SupplierTypeID"]) ? null : (System.Int32?)dataRow["SupplierTypeID"];
			entity.ExternalSupplierNo = Convert.IsDBNull(dataRow["ExternalSupplierNo"]) ? null : (System.String)dataRow["ExternalSupplierNo"];
			entity.Name = (System.String)dataRow["Name"];
			entity.Description = Convert.IsDBNull(dataRow["Description"]) ? null : (System.String)dataRow["Description"];
			entity.ContactFirstName = Convert.IsDBNull(dataRow["ContactFirstName"]) ? null : (System.String)dataRow["ContactFirstName"];
			entity.ContactLastName = Convert.IsDBNull(dataRow["ContactLastName"]) ? null : (System.String)dataRow["ContactLastName"];
			entity.ContactPhone = Convert.IsDBNull(dataRow["ContactPhone"]) ? null : (System.String)dataRow["ContactPhone"];
			entity.ContactEmail = Convert.IsDBNull(dataRow["ContactEmail"]) ? null : (System.String)dataRow["ContactEmail"];
			entity.NotificationEmailID = Convert.IsDBNull(dataRow["NotificationEmailID"]) ? null : (System.String)dataRow["NotificationEmailID"];
			entity.EmailNotificationTemplate = Convert.IsDBNull(dataRow["EmailNotificationTemplate"]) ? null : (System.String)dataRow["EmailNotificationTemplate"];
			entity.EnableEmailNotification = (System.Boolean)dataRow["EnableEmailNotification"];
			entity.DisplayOrder = Convert.IsDBNull(dataRow["DisplayOrder"]) ? null : (System.Int32?)dataRow["DisplayOrder"];
			entity.ActiveInd = (System.Boolean)dataRow["ActiveInd"];
			entity.Custom1 = Convert.IsDBNull(dataRow["Custom1"]) ? null : (System.String)dataRow["Custom1"];
			entity.Custom2 = Convert.IsDBNull(dataRow["Custom2"]) ? null : (System.String)dataRow["Custom2"];
			entity.Custom3 = Convert.IsDBNull(dataRow["Custom3"]) ? null : (System.String)dataRow["Custom3"];
			entity.Custom4 = Convert.IsDBNull(dataRow["Custom4"]) ? null : (System.String)dataRow["Custom4"];
			entity.Custom5 = Convert.IsDBNull(dataRow["Custom5"]) ? null : (System.String)dataRow["Custom5"];
			entity.PortalID = Convert.IsDBNull(dataRow["PortalID"]) ? null : (System.Int32?)dataRow["PortalID"];
			entity.ExternalID = Convert.IsDBNull(dataRow["ExternalID"]) ? null : (System.String)dataRow["ExternalID"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.Supplier"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.Supplier Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.Supplier entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region PortalIDSource	
			if (CanDeepLoad(entity, "Portal|PortalIDSource", deepLoadType, innerList) 
				&& entity.PortalIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.PortalID ?? (int)0);
				Portal tmpEntity = EntityManager.LocateEntity<Portal>(EntityLocator.ConstructKeyFromPkItems(typeof(Portal), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.PortalIDSource = tmpEntity;
				else
					entity.PortalIDSource = DataRepository.PortalProvider.GetByPortalID(transactionManager, (entity.PortalID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PortalIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.PortalIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.PortalProvider.DeepLoad(transactionManager, entity.PortalIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion PortalIDSource

			#region SupplierTypeIDSource	
			if (CanDeepLoad(entity, "SupplierType|SupplierTypeIDSource", deepLoadType, innerList) 
				&& entity.SupplierTypeIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.SupplierTypeID ?? (int)0);
				SupplierType tmpEntity = EntityManager.LocateEntity<SupplierType>(EntityLocator.ConstructKeyFromPkItems(typeof(SupplierType), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.SupplierTypeIDSource = tmpEntity;
				else
					entity.SupplierTypeIDSource = DataRepository.SupplierTypeProvider.GetBySupplierTypeID(transactionManager, (entity.SupplierTypeID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SupplierTypeIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.SupplierTypeIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.SupplierTypeProvider.DeepLoad(transactionManager, entity.SupplierTypeIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion SupplierTypeIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetBySupplierID methods when available
			
			#region SKUCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<SKU>|SKUCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SKUCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.SKUCollection = DataRepository.SKUProvider.GetBySupplierID(transactionManager, entity.SupplierID);

				if (deep && entity.SKUCollection.Count > 0)
				{
					deepHandles.Add("SKUCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<SKU>) DataRepository.SKUProvider.DeepLoad,
						new object[] { transactionManager, entity.SKUCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region AddOnValueCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<AddOnValue>|AddOnValueCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AddOnValueCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.AddOnValueCollection = DataRepository.AddOnValueProvider.GetBySupplierID(transactionManager, entity.SupplierID);

				if (deep && entity.AddOnValueCollection.Count > 0)
				{
					deepHandles.Add("AddOnValueCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<AddOnValue>) DataRepository.AddOnValueProvider.DeepLoad,
						new object[] { transactionManager, entity.AddOnValueCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region ProductCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Product>|ProductCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ProductCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ProductCollection = DataRepository.ProductProvider.GetBySupplierID(transactionManager, entity.SupplierID);

				if (deep && entity.ProductCollection.Count > 0)
				{
					deepHandles.Add("ProductCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Product>) DataRepository.ProductProvider.DeepLoad,
						new object[] { transactionManager, entity.ProductCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.Supplier object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.Supplier instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.Supplier Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.Supplier entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region PortalIDSource
			if (CanDeepSave(entity, "Portal|PortalIDSource", deepSaveType, innerList) 
				&& entity.PortalIDSource != null)
			{
				DataRepository.PortalProvider.Save(transactionManager, entity.PortalIDSource);
				entity.PortalID = entity.PortalIDSource.PortalID;
			}
			#endregion 
			
			#region SupplierTypeIDSource
			if (CanDeepSave(entity, "SupplierType|SupplierTypeIDSource", deepSaveType, innerList) 
				&& entity.SupplierTypeIDSource != null)
			{
				DataRepository.SupplierTypeProvider.Save(transactionManager, entity.SupplierTypeIDSource);
				entity.SupplierTypeID = entity.SupplierTypeIDSource.SupplierTypeID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<SKU>
				if (CanDeepSave(entity.SKUCollection, "List<SKU>|SKUCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(SKU child in entity.SKUCollection)
					{
						if(child.SupplierIDSource != null)
						{
							child.SupplierID = child.SupplierIDSource.SupplierID;
						}
						else
						{
							child.SupplierID = entity.SupplierID;
						}

					}

					if (entity.SKUCollection.Count > 0 || entity.SKUCollection.DeletedItems.Count > 0)
					{
						//DataRepository.SKUProvider.Save(transactionManager, entity.SKUCollection);
						
						deepHandles.Add("SKUCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< SKU >) DataRepository.SKUProvider.DeepSave,
							new object[] { transactionManager, entity.SKUCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<AddOnValue>
				if (CanDeepSave(entity.AddOnValueCollection, "List<AddOnValue>|AddOnValueCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(AddOnValue child in entity.AddOnValueCollection)
					{
						if(child.SupplierIDSource != null)
						{
							child.SupplierID = child.SupplierIDSource.SupplierID;
						}
						else
						{
							child.SupplierID = entity.SupplierID;
						}

					}

					if (entity.AddOnValueCollection.Count > 0 || entity.AddOnValueCollection.DeletedItems.Count > 0)
					{
						//DataRepository.AddOnValueProvider.Save(transactionManager, entity.AddOnValueCollection);
						
						deepHandles.Add("AddOnValueCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< AddOnValue >) DataRepository.AddOnValueProvider.DeepSave,
							new object[] { transactionManager, entity.AddOnValueCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Product>
				if (CanDeepSave(entity.ProductCollection, "List<Product>|ProductCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Product child in entity.ProductCollection)
					{
						if(child.SupplierIDSource != null)
						{
							child.SupplierID = child.SupplierIDSource.SupplierID;
						}
						else
						{
							child.SupplierID = entity.SupplierID;
						}

					}

					if (entity.ProductCollection.Count > 0 || entity.ProductCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ProductProvider.Save(transactionManager, entity.ProductCollection);
						
						deepHandles.Add("ProductCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Product >) DataRepository.ProductProvider.DeepSave,
							new object[] { transactionManager, entity.ProductCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region SupplierChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.Supplier</c>
	///</summary>
	public enum SupplierChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Portal</c> at PortalIDSource
		///</summary>
		[ChildEntityType(typeof(Portal))]
		Portal,
		
		///<summary>
		/// Composite Property for <c>SupplierType</c> at SupplierTypeIDSource
		///</summary>
		[ChildEntityType(typeof(SupplierType))]
		SupplierType,
		///<summary>
		/// Collection of <c>Supplier</c> as OneToMany for SKUCollection
		///</summary>
		[ChildEntityType(typeof(TList<SKU>))]
		SKUCollection,
		///<summary>
		/// Collection of <c>Supplier</c> as OneToMany for AddOnValueCollection
		///</summary>
		[ChildEntityType(typeof(TList<AddOnValue>))]
		AddOnValueCollection,
		///<summary>
		/// Collection of <c>Supplier</c> as OneToMany for ProductCollection
		///</summary>
		[ChildEntityType(typeof(TList<Product>))]
		ProductCollection,
	}
	
	#endregion SupplierChildEntityTypes
	
	#region SupplierFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;SupplierColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Supplier"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SupplierFilterBuilder : SqlFilterBuilder<SupplierColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SupplierFilterBuilder class.
		/// </summary>
		public SupplierFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SupplierFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SupplierFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SupplierFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SupplierFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SupplierFilterBuilder
	
	#region SupplierParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;SupplierColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Supplier"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SupplierParameterBuilder : ParameterizedSqlFilterBuilder<SupplierColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SupplierParameterBuilder class.
		/// </summary>
		public SupplierParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SupplierParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SupplierParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SupplierParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SupplierParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SupplierParameterBuilder
	
	#region SupplierSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;SupplierColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Supplier"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class SupplierSortBuilder : SqlSortBuilder<SupplierColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SupplierSqlSortBuilder class.
		/// </summary>
		public SupplierSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion SupplierSortBuilder
	
} // end namespace
