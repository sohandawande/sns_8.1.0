﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ShippingServiceCodeProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class ShippingServiceCodeProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.ShippingServiceCode, ZNode.Libraries.DataAccess.Entities.ShippingServiceCodeKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ShippingServiceCodeKey key)
		{
			return Delete(transactionManager, key.ShippingServiceCodeID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_shippingServiceCodeID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _shippingServiceCodeID)
		{
			return Delete(null, _shippingServiceCodeID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_shippingServiceCodeID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _shippingServiceCodeID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.ShippingServiceCode Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ShippingServiceCodeKey key, int start, int pageLength)
		{
			return GetByShippingServiceCodeID(transactionManager, key.ShippingServiceCodeID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeShippingServiceCode_ActiveInd index.
		/// </summary>
		/// <param name="_activeInd"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;ShippingServiceCode&gt;"/> class.</returns>
		public TList<ShippingServiceCode> GetByActiveInd(System.Boolean _activeInd)
		{
			int count = -1;
			return GetByActiveInd(null,_activeInd, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeShippingServiceCode_ActiveInd index.
		/// </summary>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ShippingServiceCode&gt;"/> class.</returns>
		public TList<ShippingServiceCode> GetByActiveInd(System.Boolean _activeInd, int start, int pageLength)
		{
			int count = -1;
			return GetByActiveInd(null, _activeInd, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeShippingServiceCode_ActiveInd index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_activeInd"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ShippingServiceCode&gt;"/> class.</returns>
		public TList<ShippingServiceCode> GetByActiveInd(TransactionManager transactionManager, System.Boolean _activeInd)
		{
			int count = -1;
			return GetByActiveInd(transactionManager, _activeInd, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeShippingServiceCode_ActiveInd index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ShippingServiceCode&gt;"/> class.</returns>
		public TList<ShippingServiceCode> GetByActiveInd(TransactionManager transactionManager, System.Boolean _activeInd, int start, int pageLength)
		{
			int count = -1;
			return GetByActiveInd(transactionManager, _activeInd, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeShippingServiceCode_ActiveInd index.
		/// </summary>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ShippingServiceCode&gt;"/> class.</returns>
		public TList<ShippingServiceCode> GetByActiveInd(System.Boolean _activeInd, int start, int pageLength, out int count)
		{
			return GetByActiveInd(null, _activeInd, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeShippingServiceCode_ActiveInd index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;ShippingServiceCode&gt;"/> class.</returns>
		public abstract TList<ShippingServiceCode> GetByActiveInd(TransactionManager transactionManager, System.Boolean _activeInd, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeShippingServiceCode_ShippingTypeID index.
		/// </summary>
		/// <param name="_shippingTypeID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;ShippingServiceCode&gt;"/> class.</returns>
		public TList<ShippingServiceCode> GetByShippingTypeID(System.Int32 _shippingTypeID)
		{
			int count = -1;
			return GetByShippingTypeID(null,_shippingTypeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeShippingServiceCode_ShippingTypeID index.
		/// </summary>
		/// <param name="_shippingTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ShippingServiceCode&gt;"/> class.</returns>
		public TList<ShippingServiceCode> GetByShippingTypeID(System.Int32 _shippingTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetByShippingTypeID(null, _shippingTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeShippingServiceCode_ShippingTypeID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_shippingTypeID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ShippingServiceCode&gt;"/> class.</returns>
		public TList<ShippingServiceCode> GetByShippingTypeID(TransactionManager transactionManager, System.Int32 _shippingTypeID)
		{
			int count = -1;
			return GetByShippingTypeID(transactionManager, _shippingTypeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeShippingServiceCode_ShippingTypeID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_shippingTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ShippingServiceCode&gt;"/> class.</returns>
		public TList<ShippingServiceCode> GetByShippingTypeID(TransactionManager transactionManager, System.Int32 _shippingTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetByShippingTypeID(transactionManager, _shippingTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeShippingServiceCode_ShippingTypeID index.
		/// </summary>
		/// <param name="_shippingTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ShippingServiceCode&gt;"/> class.</returns>
		public TList<ShippingServiceCode> GetByShippingTypeID(System.Int32 _shippingTypeID, int start, int pageLength, out int count)
		{
			return GetByShippingTypeID(null, _shippingTypeID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeShippingServiceCode_ShippingTypeID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_shippingTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;ShippingServiceCode&gt;"/> class.</returns>
		public abstract TList<ShippingServiceCode> GetByShippingTypeID(TransactionManager transactionManager, System.Int32 _shippingTypeID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodeShippingServiceCode index.
		/// </summary>
		/// <param name="_shippingServiceCodeID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ShippingServiceCode"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ShippingServiceCode GetByShippingServiceCodeID(System.Int32 _shippingServiceCodeID)
		{
			int count = -1;
			return GetByShippingServiceCodeID(null,_shippingServiceCodeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeShippingServiceCode index.
		/// </summary>
		/// <param name="_shippingServiceCodeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ShippingServiceCode"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ShippingServiceCode GetByShippingServiceCodeID(System.Int32 _shippingServiceCodeID, int start, int pageLength)
		{
			int count = -1;
			return GetByShippingServiceCodeID(null, _shippingServiceCodeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeShippingServiceCode index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_shippingServiceCodeID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ShippingServiceCode"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ShippingServiceCode GetByShippingServiceCodeID(TransactionManager transactionManager, System.Int32 _shippingServiceCodeID)
		{
			int count = -1;
			return GetByShippingServiceCodeID(transactionManager, _shippingServiceCodeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeShippingServiceCode index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_shippingServiceCodeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ShippingServiceCode"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ShippingServiceCode GetByShippingServiceCodeID(TransactionManager transactionManager, System.Int32 _shippingServiceCodeID, int start, int pageLength)
		{
			int count = -1;
			return GetByShippingServiceCodeID(transactionManager, _shippingServiceCodeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeShippingServiceCode index.
		/// </summary>
		/// <param name="_shippingServiceCodeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ShippingServiceCode"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ShippingServiceCode GetByShippingServiceCodeID(System.Int32 _shippingServiceCodeID, int start, int pageLength, out int count)
		{
			return GetByShippingServiceCodeID(null, _shippingServiceCodeID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeShippingServiceCode index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_shippingServiceCodeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ShippingServiceCode"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.ShippingServiceCode GetByShippingServiceCodeID(TransactionManager transactionManager, System.Int32 _shippingServiceCodeID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;ShippingServiceCode&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;ShippingServiceCode&gt;"/></returns>
		public static TList<ShippingServiceCode> Fill(IDataReader reader, TList<ShippingServiceCode> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.ShippingServiceCode c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("ShippingServiceCode")
					.Append("|").Append((System.Int32)reader[((int)ShippingServiceCodeColumn.ShippingServiceCodeID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<ShippingServiceCode>(
					key.ToString(), // EntityTrackingKey
					"ShippingServiceCode",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.ShippingServiceCode();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.ShippingServiceCodeID = (System.Int32)reader[((int)ShippingServiceCodeColumn.ShippingServiceCodeID - 1)];
					c.ShippingTypeID = (System.Int32)reader[((int)ShippingServiceCodeColumn.ShippingTypeID - 1)];
					c.Code = (System.String)reader[((int)ShippingServiceCodeColumn.Code - 1)];
					c.Description = (System.String)reader[((int)ShippingServiceCodeColumn.Description - 1)];
					c.DisplayOrder = (reader.IsDBNull(((int)ShippingServiceCodeColumn.DisplayOrder - 1)))?null:(System.Int32?)reader[((int)ShippingServiceCodeColumn.DisplayOrder - 1)];
					c.ActiveInd = (System.Boolean)reader[((int)ShippingServiceCodeColumn.ActiveInd - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.ShippingServiceCode"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ShippingServiceCode"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.ShippingServiceCode entity)
		{
			if (!reader.Read()) return;
			
			entity.ShippingServiceCodeID = (System.Int32)reader[((int)ShippingServiceCodeColumn.ShippingServiceCodeID - 1)];
			entity.ShippingTypeID = (System.Int32)reader[((int)ShippingServiceCodeColumn.ShippingTypeID - 1)];
			entity.Code = (System.String)reader[((int)ShippingServiceCodeColumn.Code - 1)];
			entity.Description = (System.String)reader[((int)ShippingServiceCodeColumn.Description - 1)];
			entity.DisplayOrder = (reader.IsDBNull(((int)ShippingServiceCodeColumn.DisplayOrder - 1)))?null:(System.Int32?)reader[((int)ShippingServiceCodeColumn.DisplayOrder - 1)];
			entity.ActiveInd = (System.Boolean)reader[((int)ShippingServiceCodeColumn.ActiveInd - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.ShippingServiceCode"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ShippingServiceCode"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.ShippingServiceCode entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.ShippingServiceCodeID = (System.Int32)dataRow["ShippingServiceCodeID"];
			entity.ShippingTypeID = (System.Int32)dataRow["ShippingTypeID"];
			entity.Code = (System.String)dataRow["Code"];
			entity.Description = (System.String)dataRow["Description"];
			entity.DisplayOrder = Convert.IsDBNull(dataRow["DisplayOrder"]) ? null : (System.Int32?)dataRow["DisplayOrder"];
			entity.ActiveInd = (System.Boolean)dataRow["ActiveInd"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ShippingServiceCode"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.ShippingServiceCode Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ShippingServiceCode entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region ShippingTypeIDSource	
			if (CanDeepLoad(entity, "ShippingType|ShippingTypeIDSource", deepLoadType, innerList) 
				&& entity.ShippingTypeIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.ShippingTypeID;
				ShippingType tmpEntity = EntityManager.LocateEntity<ShippingType>(EntityLocator.ConstructKeyFromPkItems(typeof(ShippingType), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ShippingTypeIDSource = tmpEntity;
				else
					entity.ShippingTypeIDSource = DataRepository.ShippingTypeProvider.GetByShippingTypeID(transactionManager, entity.ShippingTypeID);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ShippingTypeIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ShippingTypeIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.ShippingTypeProvider.DeepLoad(transactionManager, entity.ShippingTypeIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ShippingTypeIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.ShippingServiceCode object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.ShippingServiceCode instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.ShippingServiceCode Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ShippingServiceCode entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region ShippingTypeIDSource
			if (CanDeepSave(entity, "ShippingType|ShippingTypeIDSource", deepSaveType, innerList) 
				&& entity.ShippingTypeIDSource != null)
			{
				DataRepository.ShippingTypeProvider.Save(transactionManager, entity.ShippingTypeIDSource);
				entity.ShippingTypeID = entity.ShippingTypeIDSource.ShippingTypeID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region ShippingServiceCodeChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.ShippingServiceCode</c>
	///</summary>
	public enum ShippingServiceCodeChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>ShippingType</c> at ShippingTypeIDSource
		///</summary>
		[ChildEntityType(typeof(ShippingType))]
		ShippingType,
	}
	
	#endregion ShippingServiceCodeChildEntityTypes
	
	#region ShippingServiceCodeFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;ShippingServiceCodeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ShippingServiceCode"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ShippingServiceCodeFilterBuilder : SqlFilterBuilder<ShippingServiceCodeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ShippingServiceCodeFilterBuilder class.
		/// </summary>
		public ShippingServiceCodeFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ShippingServiceCodeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ShippingServiceCodeFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ShippingServiceCodeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ShippingServiceCodeFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ShippingServiceCodeFilterBuilder
	
	#region ShippingServiceCodeParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;ShippingServiceCodeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ShippingServiceCode"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ShippingServiceCodeParameterBuilder : ParameterizedSqlFilterBuilder<ShippingServiceCodeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ShippingServiceCodeParameterBuilder class.
		/// </summary>
		public ShippingServiceCodeParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ShippingServiceCodeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ShippingServiceCodeParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ShippingServiceCodeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ShippingServiceCodeParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ShippingServiceCodeParameterBuilder
	
	#region ShippingServiceCodeSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;ShippingServiceCodeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ShippingServiceCode"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ShippingServiceCodeSortBuilder : SqlSortBuilder<ShippingServiceCodeColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ShippingServiceCodeSqlSortBuilder class.
		/// </summary>
		public ShippingServiceCodeSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ShippingServiceCodeSortBuilder
	
} // end namespace
