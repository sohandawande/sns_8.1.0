﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="LuceneIndexMonitorProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class LuceneIndexMonitorProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.LuceneIndexMonitor, ZNode.Libraries.DataAccess.Entities.LuceneIndexMonitorKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.LuceneIndexMonitorKey key)
		{
			return Delete(transactionManager, key.LuceneIndexMonitorID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_luceneIndexMonitorID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int64 _luceneIndexMonitorID)
		{
			return Delete(null, _luceneIndexMonitorID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_luceneIndexMonitorID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int64 _luceneIndexMonitorID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.LuceneIndexMonitor Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.LuceneIndexMonitorKey key, int start, int pageLength)
		{
			return GetByLuceneIndexMonitorID(transactionManager, key.LuceneIndexMonitorID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodeLuceneIndexMonitor index.
		/// </summary>
		/// <param name="_luceneIndexMonitorID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneIndexMonitor"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.LuceneIndexMonitor GetByLuceneIndexMonitorID(System.Int64 _luceneIndexMonitorID)
		{
			int count = -1;
			return GetByLuceneIndexMonitorID(null,_luceneIndexMonitorID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeLuceneIndexMonitor index.
		/// </summary>
		/// <param name="_luceneIndexMonitorID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneIndexMonitor"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.LuceneIndexMonitor GetByLuceneIndexMonitorID(System.Int64 _luceneIndexMonitorID, int start, int pageLength)
		{
			int count = -1;
			return GetByLuceneIndexMonitorID(null, _luceneIndexMonitorID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeLuceneIndexMonitor index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_luceneIndexMonitorID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneIndexMonitor"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.LuceneIndexMonitor GetByLuceneIndexMonitorID(TransactionManager transactionManager, System.Int64 _luceneIndexMonitorID)
		{
			int count = -1;
			return GetByLuceneIndexMonitorID(transactionManager, _luceneIndexMonitorID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeLuceneIndexMonitor index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_luceneIndexMonitorID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneIndexMonitor"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.LuceneIndexMonitor GetByLuceneIndexMonitorID(TransactionManager transactionManager, System.Int64 _luceneIndexMonitorID, int start, int pageLength)
		{
			int count = -1;
			return GetByLuceneIndexMonitorID(transactionManager, _luceneIndexMonitorID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeLuceneIndexMonitor index.
		/// </summary>
		/// <param name="_luceneIndexMonitorID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneIndexMonitor"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.LuceneIndexMonitor GetByLuceneIndexMonitorID(System.Int64 _luceneIndexMonitorID, int start, int pageLength, out int count)
		{
			return GetByLuceneIndexMonitorID(null, _luceneIndexMonitorID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeLuceneIndexMonitor index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_luceneIndexMonitorID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneIndexMonitor"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.LuceneIndexMonitor GetByLuceneIndexMonitorID(TransactionManager transactionManager, System.Int64 _luceneIndexMonitorID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;LuceneIndexMonitor&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;LuceneIndexMonitor&gt;"/></returns>
		public static TList<LuceneIndexMonitor> Fill(IDataReader reader, TList<LuceneIndexMonitor> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.LuceneIndexMonitor c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("LuceneIndexMonitor")
					.Append("|").Append((System.Int64)reader[((int)LuceneIndexMonitorColumn.LuceneIndexMonitorID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<LuceneIndexMonitor>(
					key.ToString(), // EntityTrackingKey
					"LuceneIndexMonitor",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.LuceneIndexMonitor();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.LuceneIndexMonitorID = (System.Int64)reader[((int)LuceneIndexMonitorColumn.LuceneIndexMonitorID - 1)];
					c.SourceID = (System.Int32)reader[((int)LuceneIndexMonitorColumn.SourceID - 1)];
					c.SourceType = (System.String)reader[((int)LuceneIndexMonitorColumn.SourceType - 1)];
					c.SourceTransactionType = (System.String)reader[((int)LuceneIndexMonitorColumn.SourceTransactionType - 1)];
					c.TransactionDateTime = (System.DateTime)reader[((int)LuceneIndexMonitorColumn.TransactionDateTime - 1)];
					c.IsDuplicate = (reader.IsDBNull(((int)LuceneIndexMonitorColumn.IsDuplicate - 1)))?null:(System.Boolean?)reader[((int)LuceneIndexMonitorColumn.IsDuplicate - 1)];
					c.AffectedType = (System.String)reader[((int)LuceneIndexMonitorColumn.AffectedType - 1)];
					c.IndexerStatusChangedBy = (reader.IsDBNull(((int)LuceneIndexMonitorColumn.IndexerStatusChangedBy - 1)))?null:(System.String)reader[((int)LuceneIndexMonitorColumn.IndexerStatusChangedBy - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneIndexMonitor"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.LuceneIndexMonitor"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.LuceneIndexMonitor entity)
		{
			if (!reader.Read()) return;
			
			entity.LuceneIndexMonitorID = (System.Int64)reader[((int)LuceneIndexMonitorColumn.LuceneIndexMonitorID - 1)];
			entity.SourceID = (System.Int32)reader[((int)LuceneIndexMonitorColumn.SourceID - 1)];
			entity.SourceType = (System.String)reader[((int)LuceneIndexMonitorColumn.SourceType - 1)];
			entity.SourceTransactionType = (System.String)reader[((int)LuceneIndexMonitorColumn.SourceTransactionType - 1)];
			entity.TransactionDateTime = (System.DateTime)reader[((int)LuceneIndexMonitorColumn.TransactionDateTime - 1)];
			entity.IsDuplicate = (reader.IsDBNull(((int)LuceneIndexMonitorColumn.IsDuplicate - 1)))?null:(System.Boolean?)reader[((int)LuceneIndexMonitorColumn.IsDuplicate - 1)];
			entity.AffectedType = (System.String)reader[((int)LuceneIndexMonitorColumn.AffectedType - 1)];
			entity.IndexerStatusChangedBy = (reader.IsDBNull(((int)LuceneIndexMonitorColumn.IndexerStatusChangedBy - 1)))?null:(System.String)reader[((int)LuceneIndexMonitorColumn.IndexerStatusChangedBy - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneIndexMonitor"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.LuceneIndexMonitor"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.LuceneIndexMonitor entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.LuceneIndexMonitorID = (System.Int64)dataRow["LuceneIndexMonitorID"];
			entity.SourceID = (System.Int32)dataRow["SourceID"];
			entity.SourceType = (System.String)dataRow["SourceType"];
			entity.SourceTransactionType = (System.String)dataRow["SourceTransactionType"];
			entity.TransactionDateTime = (System.DateTime)dataRow["TransactionDateTime"];
			entity.IsDuplicate = Convert.IsDBNull(dataRow["IsDuplicate"]) ? null : (System.Boolean?)dataRow["IsDuplicate"];
			entity.AffectedType = (System.String)dataRow["AffectedType"];
			entity.IndexerStatusChangedBy = Convert.IsDBNull(dataRow["IndexerStatusChangedBy"]) ? null : (System.String)dataRow["IndexerStatusChangedBy"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.LuceneIndexMonitor"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.LuceneIndexMonitor Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.LuceneIndexMonitor entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByLuceneIndexMonitorID methods when available
			
			#region LuceneIndexServerStatusCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<LuceneIndexServerStatus>|LuceneIndexServerStatusCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'LuceneIndexServerStatusCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.LuceneIndexServerStatusCollection = DataRepository.LuceneIndexServerStatusProvider.GetByLuceneIndexMonitorID(transactionManager, entity.LuceneIndexMonitorID);

				if (deep && entity.LuceneIndexServerStatusCollection.Count > 0)
				{
					deepHandles.Add("LuceneIndexServerStatusCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<LuceneIndexServerStatus>) DataRepository.LuceneIndexServerStatusProvider.DeepLoad,
						new object[] { transactionManager, entity.LuceneIndexServerStatusCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.LuceneIndexMonitor object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.LuceneIndexMonitor instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.LuceneIndexMonitor Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.LuceneIndexMonitor entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<LuceneIndexServerStatus>
				if (CanDeepSave(entity.LuceneIndexServerStatusCollection, "List<LuceneIndexServerStatus>|LuceneIndexServerStatusCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(LuceneIndexServerStatus child in entity.LuceneIndexServerStatusCollection)
					{
						if(child.LuceneIndexMonitorIDSource != null)
						{
							child.LuceneIndexMonitorID = child.LuceneIndexMonitorIDSource.LuceneIndexMonitorID;
						}
						else
						{
							child.LuceneIndexMonitorID = entity.LuceneIndexMonitorID;
						}

					}

					if (entity.LuceneIndexServerStatusCollection.Count > 0 || entity.LuceneIndexServerStatusCollection.DeletedItems.Count > 0)
					{
						//DataRepository.LuceneIndexServerStatusProvider.Save(transactionManager, entity.LuceneIndexServerStatusCollection);
						
						deepHandles.Add("LuceneIndexServerStatusCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< LuceneIndexServerStatus >) DataRepository.LuceneIndexServerStatusProvider.DeepSave,
							new object[] { transactionManager, entity.LuceneIndexServerStatusCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region LuceneIndexMonitorChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.LuceneIndexMonitor</c>
	///</summary>
	public enum LuceneIndexMonitorChildEntityTypes
	{
		///<summary>
		/// Collection of <c>LuceneIndexMonitor</c> as OneToMany for LuceneIndexServerStatusCollection
		///</summary>
		[ChildEntityType(typeof(TList<LuceneIndexServerStatus>))]
		LuceneIndexServerStatusCollection,
	}
	
	#endregion LuceneIndexMonitorChildEntityTypes
	
	#region LuceneIndexMonitorFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;LuceneIndexMonitorColumn&gt;"/> class
	/// that is used exclusively with a <see cref="LuceneIndexMonitor"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class LuceneIndexMonitorFilterBuilder : SqlFilterBuilder<LuceneIndexMonitorColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the LuceneIndexMonitorFilterBuilder class.
		/// </summary>
		public LuceneIndexMonitorFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the LuceneIndexMonitorFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public LuceneIndexMonitorFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the LuceneIndexMonitorFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public LuceneIndexMonitorFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion LuceneIndexMonitorFilterBuilder
	
	#region LuceneIndexMonitorParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;LuceneIndexMonitorColumn&gt;"/> class
	/// that is used exclusively with a <see cref="LuceneIndexMonitor"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class LuceneIndexMonitorParameterBuilder : ParameterizedSqlFilterBuilder<LuceneIndexMonitorColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the LuceneIndexMonitorParameterBuilder class.
		/// </summary>
		public LuceneIndexMonitorParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the LuceneIndexMonitorParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public LuceneIndexMonitorParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the LuceneIndexMonitorParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public LuceneIndexMonitorParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion LuceneIndexMonitorParameterBuilder
	
	#region LuceneIndexMonitorSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;LuceneIndexMonitorColumn&gt;"/> class
	/// that is used exclusively with a <see cref="LuceneIndexMonitor"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class LuceneIndexMonitorSortBuilder : SqlSortBuilder<LuceneIndexMonitorColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the LuceneIndexMonitorSqlSortBuilder class.
		/// </summary>
		public LuceneIndexMonitorSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion LuceneIndexMonitorSortBuilder
	
} // end namespace
