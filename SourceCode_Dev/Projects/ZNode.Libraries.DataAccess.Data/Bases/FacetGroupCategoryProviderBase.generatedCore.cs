﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="FacetGroupCategoryProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class FacetGroupCategoryProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.FacetGroupCategory, ZNode.Libraries.DataAccess.Entities.FacetGroupCategoryKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.FacetGroupCategoryKey key)
		{
			return Delete(transactionManager, key.FacetGroupID, key.CategoryID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_facetGroupID">. Primary Key.</param>
		/// <param name="_categoryID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _facetGroupID, System.Int32 _categoryID)
		{
			return Delete(null, _facetGroupID, _categoryID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_facetGroupID">. Primary Key.</param>
		/// <param name="_categoryID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _facetGroupID, System.Int32 _categoryID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeFacetGroupCategory__ZNodeFacetGroup key.
		///		FK__ZNodeFacetGroupCategory__ZNodeFacetGroup Description: 
		/// </summary>
		/// <param name="_facetGroupID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.FacetGroupCategory objects.</returns>
		public TList<FacetGroupCategory> GetByFacetGroupID(System.Int32 _facetGroupID)
		{
			int count = -1;
			return GetByFacetGroupID(_facetGroupID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeFacetGroupCategory__ZNodeFacetGroup key.
		///		FK__ZNodeFacetGroupCategory__ZNodeFacetGroup Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_facetGroupID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.FacetGroupCategory objects.</returns>
		/// <remarks></remarks>
		public TList<FacetGroupCategory> GetByFacetGroupID(TransactionManager transactionManager, System.Int32 _facetGroupID)
		{
			int count = -1;
			return GetByFacetGroupID(transactionManager, _facetGroupID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeFacetGroupCategory__ZNodeFacetGroup key.
		///		FK__ZNodeFacetGroupCategory__ZNodeFacetGroup Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_facetGroupID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.FacetGroupCategory objects.</returns>
		public TList<FacetGroupCategory> GetByFacetGroupID(TransactionManager transactionManager, System.Int32 _facetGroupID, int start, int pageLength)
		{
			int count = -1;
			return GetByFacetGroupID(transactionManager, _facetGroupID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeFacetGroupCategory__ZNodeFacetGroup key.
		///		fKZNodeFacetGroupCategoryZNodeFacetGroup Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_facetGroupID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.FacetGroupCategory objects.</returns>
		public TList<FacetGroupCategory> GetByFacetGroupID(System.Int32 _facetGroupID, int start, int pageLength)
		{
			int count =  -1;
			return GetByFacetGroupID(null, _facetGroupID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeFacetGroupCategory__ZNodeFacetGroup key.
		///		fKZNodeFacetGroupCategoryZNodeFacetGroup Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_facetGroupID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.FacetGroupCategory objects.</returns>
		public TList<FacetGroupCategory> GetByFacetGroupID(System.Int32 _facetGroupID, int start, int pageLength,out int count)
		{
			return GetByFacetGroupID(null, _facetGroupID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeFacetGroupCategory__ZNodeFacetGroup key.
		///		FK__ZNodeFacetGroupCategory__ZNodeFacetGroup Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_facetGroupID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.FacetGroupCategory objects.</returns>
		public abstract TList<FacetGroupCategory> GetByFacetGroupID(TransactionManager transactionManager, System.Int32 _facetGroupID, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.FacetGroupCategory Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.FacetGroupCategoryKey key, int start, int pageLength)
		{
			return GetByFacetGroupIDCategoryID(transactionManager, key.FacetGroupID, key.CategoryID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_FacetGroupCategory index.
		/// </summary>
		/// <param name="_categoryID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;FacetGroupCategory&gt;"/> class.</returns>
		public TList<FacetGroupCategory> GetByCategoryID(System.Int32 _categoryID)
		{
			int count = -1;
			return GetByCategoryID(null,_categoryID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_FacetGroupCategory index.
		/// </summary>
		/// <param name="_categoryID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;FacetGroupCategory&gt;"/> class.</returns>
		public TList<FacetGroupCategory> GetByCategoryID(System.Int32 _categoryID, int start, int pageLength)
		{
			int count = -1;
			return GetByCategoryID(null, _categoryID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_FacetGroupCategory index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_categoryID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;FacetGroupCategory&gt;"/> class.</returns>
		public TList<FacetGroupCategory> GetByCategoryID(TransactionManager transactionManager, System.Int32 _categoryID)
		{
			int count = -1;
			return GetByCategoryID(transactionManager, _categoryID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_FacetGroupCategory index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_categoryID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;FacetGroupCategory&gt;"/> class.</returns>
		public TList<FacetGroupCategory> GetByCategoryID(TransactionManager transactionManager, System.Int32 _categoryID, int start, int pageLength)
		{
			int count = -1;
			return GetByCategoryID(transactionManager, _categoryID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_FacetGroupCategory index.
		/// </summary>
		/// <param name="_categoryID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;FacetGroupCategory&gt;"/> class.</returns>
		public TList<FacetGroupCategory> GetByCategoryID(System.Int32 _categoryID, int start, int pageLength, out int count)
		{
			return GetByCategoryID(null, _categoryID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_FacetGroupCategory index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_categoryID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;FacetGroupCategory&gt;"/> class.</returns>
		public abstract TList<FacetGroupCategory> GetByCategoryID(TransactionManager transactionManager, System.Int32 _categoryID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK__ZNodeFacet__7BE60A8A60997385 index.
		/// </summary>
		/// <param name="_facetGroupID"></param>
		/// <param name="_categoryID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.FacetGroupCategory"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.FacetGroupCategory GetByFacetGroupIDCategoryID(System.Int32 _facetGroupID, System.Int32 _categoryID)
		{
			int count = -1;
			return GetByFacetGroupIDCategoryID(null,_facetGroupID, _categoryID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK__ZNodeFacet__7BE60A8A60997385 index.
		/// </summary>
		/// <param name="_facetGroupID"></param>
		/// <param name="_categoryID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.FacetGroupCategory"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.FacetGroupCategory GetByFacetGroupIDCategoryID(System.Int32 _facetGroupID, System.Int32 _categoryID, int start, int pageLength)
		{
			int count = -1;
			return GetByFacetGroupIDCategoryID(null, _facetGroupID, _categoryID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK__ZNodeFacet__7BE60A8A60997385 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_facetGroupID"></param>
		/// <param name="_categoryID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.FacetGroupCategory"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.FacetGroupCategory GetByFacetGroupIDCategoryID(TransactionManager transactionManager, System.Int32 _facetGroupID, System.Int32 _categoryID)
		{
			int count = -1;
			return GetByFacetGroupIDCategoryID(transactionManager, _facetGroupID, _categoryID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK__ZNodeFacet__7BE60A8A60997385 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_facetGroupID"></param>
		/// <param name="_categoryID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.FacetGroupCategory"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.FacetGroupCategory GetByFacetGroupIDCategoryID(TransactionManager transactionManager, System.Int32 _facetGroupID, System.Int32 _categoryID, int start, int pageLength)
		{
			int count = -1;
			return GetByFacetGroupIDCategoryID(transactionManager, _facetGroupID, _categoryID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK__ZNodeFacet__7BE60A8A60997385 index.
		/// </summary>
		/// <param name="_facetGroupID"></param>
		/// <param name="_categoryID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.FacetGroupCategory"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.FacetGroupCategory GetByFacetGroupIDCategoryID(System.Int32 _facetGroupID, System.Int32 _categoryID, int start, int pageLength, out int count)
		{
			return GetByFacetGroupIDCategoryID(null, _facetGroupID, _categoryID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK__ZNodeFacet__7BE60A8A60997385 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_facetGroupID"></param>
		/// <param name="_categoryID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.FacetGroupCategory"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.FacetGroupCategory GetByFacetGroupIDCategoryID(TransactionManager transactionManager, System.Int32 _facetGroupID, System.Int32 _categoryID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;FacetGroupCategory&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;FacetGroupCategory&gt;"/></returns>
		public static TList<FacetGroupCategory> Fill(IDataReader reader, TList<FacetGroupCategory> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.FacetGroupCategory c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("FacetGroupCategory")
					.Append("|").Append((System.Int32)reader[((int)FacetGroupCategoryColumn.FacetGroupID - 1)])
					.Append("|").Append((System.Int32)reader[((int)FacetGroupCategoryColumn.CategoryID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<FacetGroupCategory>(
					key.ToString(), // EntityTrackingKey
					"FacetGroupCategory",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.FacetGroupCategory();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.FacetGroupID = (System.Int32)reader[((int)FacetGroupCategoryColumn.FacetGroupID - 1)];
					c.OriginalFacetGroupID = c.FacetGroupID;
					c.CategoryID = (System.Int32)reader[((int)FacetGroupCategoryColumn.CategoryID - 1)];
					c.OriginalCategoryID = c.CategoryID;
					c.CategoryDisplayOrder = (reader.IsDBNull(((int)FacetGroupCategoryColumn.CategoryDisplayOrder - 1)))?null:(System.Int32?)reader[((int)FacetGroupCategoryColumn.CategoryDisplayOrder - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.FacetGroupCategory"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.FacetGroupCategory"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.FacetGroupCategory entity)
		{
			if (!reader.Read()) return;
			
			entity.FacetGroupID = (System.Int32)reader[((int)FacetGroupCategoryColumn.FacetGroupID - 1)];
			entity.OriginalFacetGroupID = (System.Int32)reader["FacetGroupID"];
			entity.CategoryID = (System.Int32)reader[((int)FacetGroupCategoryColumn.CategoryID - 1)];
			entity.OriginalCategoryID = (System.Int32)reader["CategoryID"];
			entity.CategoryDisplayOrder = (reader.IsDBNull(((int)FacetGroupCategoryColumn.CategoryDisplayOrder - 1)))?null:(System.Int32?)reader[((int)FacetGroupCategoryColumn.CategoryDisplayOrder - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.FacetGroupCategory"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.FacetGroupCategory"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.FacetGroupCategory entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.FacetGroupID = (System.Int32)dataRow["FacetGroupID"];
			entity.OriginalFacetGroupID = (System.Int32)dataRow["FacetGroupID"];
			entity.CategoryID = (System.Int32)dataRow["CategoryID"];
			entity.OriginalCategoryID = (System.Int32)dataRow["CategoryID"];
			entity.CategoryDisplayOrder = Convert.IsDBNull(dataRow["CategoryDisplayOrder"]) ? null : (System.Int32?)dataRow["CategoryDisplayOrder"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.FacetGroupCategory"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.FacetGroupCategory Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.FacetGroupCategory entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region CategoryIDSource	
			if (CanDeepLoad(entity, "Category|CategoryIDSource", deepLoadType, innerList) 
				&& entity.CategoryIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.CategoryID;
				Category tmpEntity = EntityManager.LocateEntity<Category>(EntityLocator.ConstructKeyFromPkItems(typeof(Category), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CategoryIDSource = tmpEntity;
				else
					entity.CategoryIDSource = DataRepository.CategoryProvider.GetByCategoryID(transactionManager, entity.CategoryID);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CategoryIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CategoryIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CategoryProvider.DeepLoad(transactionManager, entity.CategoryIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CategoryIDSource

			#region FacetGroupIDSource	
			if (CanDeepLoad(entity, "FacetGroup|FacetGroupIDSource", deepLoadType, innerList) 
				&& entity.FacetGroupIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.FacetGroupID;
				FacetGroup tmpEntity = EntityManager.LocateEntity<FacetGroup>(EntityLocator.ConstructKeyFromPkItems(typeof(FacetGroup), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.FacetGroupIDSource = tmpEntity;
				else
					entity.FacetGroupIDSource = DataRepository.FacetGroupProvider.GetByFacetGroupID(transactionManager, entity.FacetGroupID);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'FacetGroupIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.FacetGroupIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.FacetGroupProvider.DeepLoad(transactionManager, entity.FacetGroupIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion FacetGroupIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.FacetGroupCategory object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.FacetGroupCategory instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.FacetGroupCategory Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.FacetGroupCategory entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region CategoryIDSource
			if (CanDeepSave(entity, "Category|CategoryIDSource", deepSaveType, innerList) 
				&& entity.CategoryIDSource != null)
			{
				DataRepository.CategoryProvider.Save(transactionManager, entity.CategoryIDSource);
				entity.CategoryID = entity.CategoryIDSource.CategoryID;
			}
			#endregion 
			
			#region FacetGroupIDSource
			if (CanDeepSave(entity, "FacetGroup|FacetGroupIDSource", deepSaveType, innerList) 
				&& entity.FacetGroupIDSource != null)
			{
				DataRepository.FacetGroupProvider.Save(transactionManager, entity.FacetGroupIDSource);
				entity.FacetGroupID = entity.FacetGroupIDSource.FacetGroupID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region FacetGroupCategoryChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.FacetGroupCategory</c>
	///</summary>
	public enum FacetGroupCategoryChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Category</c> at CategoryIDSource
		///</summary>
		[ChildEntityType(typeof(Category))]
		Category,
		
		///<summary>
		/// Composite Property for <c>FacetGroup</c> at FacetGroupIDSource
		///</summary>
		[ChildEntityType(typeof(FacetGroup))]
		FacetGroup,
	}
	
	#endregion FacetGroupCategoryChildEntityTypes
	
	#region FacetGroupCategoryFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;FacetGroupCategoryColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FacetGroupCategory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FacetGroupCategoryFilterBuilder : SqlFilterBuilder<FacetGroupCategoryColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FacetGroupCategoryFilterBuilder class.
		/// </summary>
		public FacetGroupCategoryFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the FacetGroupCategoryFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FacetGroupCategoryFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FacetGroupCategoryFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FacetGroupCategoryFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FacetGroupCategoryFilterBuilder
	
	#region FacetGroupCategoryParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;FacetGroupCategoryColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FacetGroupCategory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FacetGroupCategoryParameterBuilder : ParameterizedSqlFilterBuilder<FacetGroupCategoryColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FacetGroupCategoryParameterBuilder class.
		/// </summary>
		public FacetGroupCategoryParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the FacetGroupCategoryParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FacetGroupCategoryParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FacetGroupCategoryParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FacetGroupCategoryParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FacetGroupCategoryParameterBuilder
	
	#region FacetGroupCategorySortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;FacetGroupCategoryColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FacetGroupCategory"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class FacetGroupCategorySortBuilder : SqlSortBuilder<FacetGroupCategoryColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FacetGroupCategorySqlSortBuilder class.
		/// </summary>
		public FacetGroupCategorySortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion FacetGroupCategorySortBuilder
	
} // end namespace
