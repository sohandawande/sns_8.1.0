﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="FacetGroupProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class FacetGroupProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.FacetGroup, ZNode.Libraries.DataAccess.Entities.FacetGroupKey>
	{		
		#region Get from Many To Many Relationship Functions
		#region GetByCategoryIDFromFacetGroupCategory
		
		/// <summary>
		///		Gets ZNodeFacetGroup objects from the datasource by CategoryID in the
		///		ZNodeFacetGroupCategory table. Table ZNodeFacetGroup is related to table ZNodeCategory
		///		through the (M:N) relationship defined in the ZNodeFacetGroupCategory table.
		/// </summary>
		/// <param name="_categoryID"></param>
		/// <returns>Returns a typed collection of FacetGroup objects.</returns>
		public TList<FacetGroup> GetByCategoryIDFromFacetGroupCategory(System.Int32 _categoryID)
		{
			int count = -1;
			return GetByCategoryIDFromFacetGroupCategory(null,_categoryID, 0, int.MaxValue, out count);
			
		}
		
		/// <summary>
		///		Gets ZNode.Libraries.DataAccess.Entities.FacetGroup objects from the datasource by CategoryID in the
		///		ZNodeFacetGroupCategory table. Table ZNodeFacetGroup is related to table ZNodeCategory
		///		through the (M:N) relationship defined in the ZNodeFacetGroupCategory table.
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_categoryID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a TList of FacetGroup objects.</returns>
		public TList<FacetGroup> GetByCategoryIDFromFacetGroupCategory(System.Int32 _categoryID, int start, int pageLength)
		{
			int count = -1;
			return GetByCategoryIDFromFacetGroupCategory(null, _categoryID, start, pageLength, out count);
		}
		
		/// <summary>
		///		Gets FacetGroup objects from the datasource by CategoryID in the
		///		ZNodeFacetGroupCategory table. Table ZNodeFacetGroup is related to table ZNodeCategory
		///		through the (M:N) relationship defined in the ZNodeFacetGroupCategory table.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_categoryID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNodeFacetGroup objects.</returns>
		public TList<FacetGroup> GetByCategoryIDFromFacetGroupCategory(TransactionManager transactionManager, System.Int32 _categoryID)
		{
			int count = -1;
			return GetByCategoryIDFromFacetGroupCategory(transactionManager, _categoryID, 0, int.MaxValue, out count);
		}
		
		
		/// <summary>
		///		Gets FacetGroup objects from the datasource by CategoryID in the
		///		ZNodeFacetGroupCategory table. Table ZNodeFacetGroup is related to table ZNodeCategory
		///		through the (M:N) relationship defined in the ZNodeFacetGroupCategory table.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_categoryID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNodeFacetGroup objects.</returns>
		public TList<FacetGroup> GetByCategoryIDFromFacetGroupCategory(TransactionManager transactionManager, System.Int32 _categoryID,int start, int pageLength)
		{
			int count = -1;
			return GetByCategoryIDFromFacetGroupCategory(transactionManager, _categoryID, start, pageLength, out count);
		}
		
		/// <summary>
		///		Gets FacetGroup objects from the datasource by CategoryID in the
		///		ZNodeFacetGroupCategory table. Table ZNodeFacetGroup is related to table ZNodeCategory
		///		through the (M:N) relationship defined in the ZNodeFacetGroupCategory table.
		/// </summary>
		/// <param name="_categoryID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNodeFacetGroup objects.</returns>
		public TList<FacetGroup> GetByCategoryIDFromFacetGroupCategory(System.Int32 _categoryID,int start, int pageLength, out int count)
		{
			
			return GetByCategoryIDFromFacetGroupCategory(null, _categoryID, start, pageLength, out count);
		}


		/// <summary>
		///		Gets ZNodeFacetGroup objects from the datasource by CategoryID in the
		///		ZNodeFacetGroupCategory table. Table ZNodeFacetGroup is related to table ZNodeCategory
		///		through the (M:N) relationship defined in the ZNodeFacetGroupCategory table.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <param name="_categoryID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a TList of FacetGroup objects.</returns>
		public abstract TList<FacetGroup> GetByCategoryIDFromFacetGroupCategory(TransactionManager transactionManager,System.Int32 _categoryID, int start, int pageLength, out int count);
		
		#endregion GetByCategoryIDFromFacetGroupCategory
		
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.FacetGroupKey key)
		{
			return Delete(transactionManager, key.FacetGroupID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_facetGroupID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _facetGroupID)
		{
			return Delete(null, _facetGroupID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_facetGroupID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _facetGroupID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeFacetGroup__ZNodeCatalog key.
		///		FK__ZNodeFacetGroup__ZNodeCatalog Description: 
		/// </summary>
		/// <param name="_catalogID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.FacetGroup objects.</returns>
		public TList<FacetGroup> GetByCatalogID(System.Int32? _catalogID)
		{
			int count = -1;
			return GetByCatalogID(_catalogID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeFacetGroup__ZNodeCatalog key.
		///		FK__ZNodeFacetGroup__ZNodeCatalog Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_catalogID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.FacetGroup objects.</returns>
		/// <remarks></remarks>
		public TList<FacetGroup> GetByCatalogID(TransactionManager transactionManager, System.Int32? _catalogID)
		{
			int count = -1;
			return GetByCatalogID(transactionManager, _catalogID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeFacetGroup__ZNodeCatalog key.
		///		FK__ZNodeFacetGroup__ZNodeCatalog Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_catalogID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.FacetGroup objects.</returns>
		public TList<FacetGroup> GetByCatalogID(TransactionManager transactionManager, System.Int32? _catalogID, int start, int pageLength)
		{
			int count = -1;
			return GetByCatalogID(transactionManager, _catalogID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeFacetGroup__ZNodeCatalog key.
		///		fKZNodeFacetGroupZNodeCatalog Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_catalogID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.FacetGroup objects.</returns>
		public TList<FacetGroup> GetByCatalogID(System.Int32? _catalogID, int start, int pageLength)
		{
			int count =  -1;
			return GetByCatalogID(null, _catalogID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeFacetGroup__ZNodeCatalog key.
		///		fKZNodeFacetGroupZNodeCatalog Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_catalogID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.FacetGroup objects.</returns>
		public TList<FacetGroup> GetByCatalogID(System.Int32? _catalogID, int start, int pageLength,out int count)
		{
			return GetByCatalogID(null, _catalogID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeFacetGroup__ZNodeCatalog key.
		///		FK__ZNodeFacetGroup__ZNodeCatalog Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_catalogID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.FacetGroup objects.</returns>
		public abstract TList<FacetGroup> GetByCatalogID(TransactionManager transactionManager, System.Int32? _catalogID, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeFacetGroup_ZNodeFacetControlType key.
		///		FK_ZNodeFacetGroup_ZNodeFacetControlType Description: 
		/// </summary>
		/// <param name="_controlTypeID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.FacetGroup objects.</returns>
		public TList<FacetGroup> GetByControlTypeID(System.Int32? _controlTypeID)
		{
			int count = -1;
			return GetByControlTypeID(_controlTypeID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeFacetGroup_ZNodeFacetControlType key.
		///		FK_ZNodeFacetGroup_ZNodeFacetControlType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_controlTypeID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.FacetGroup objects.</returns>
		/// <remarks></remarks>
		public TList<FacetGroup> GetByControlTypeID(TransactionManager transactionManager, System.Int32? _controlTypeID)
		{
			int count = -1;
			return GetByControlTypeID(transactionManager, _controlTypeID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeFacetGroup_ZNodeFacetControlType key.
		///		FK_ZNodeFacetGroup_ZNodeFacetControlType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_controlTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.FacetGroup objects.</returns>
		public TList<FacetGroup> GetByControlTypeID(TransactionManager transactionManager, System.Int32? _controlTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetByControlTypeID(transactionManager, _controlTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeFacetGroup_ZNodeFacetControlType key.
		///		fKZNodeFacetGroupZNodeFacetControlType Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_controlTypeID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.FacetGroup objects.</returns>
		public TList<FacetGroup> GetByControlTypeID(System.Int32? _controlTypeID, int start, int pageLength)
		{
			int count =  -1;
			return GetByControlTypeID(null, _controlTypeID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeFacetGroup_ZNodeFacetControlType key.
		///		fKZNodeFacetGroupZNodeFacetControlType Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_controlTypeID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.FacetGroup objects.</returns>
		public TList<FacetGroup> GetByControlTypeID(System.Int32? _controlTypeID, int start, int pageLength,out int count)
		{
			return GetByControlTypeID(null, _controlTypeID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeFacetGroup_ZNodeFacetControlType key.
		///		FK_ZNodeFacetGroup_ZNodeFacetControlType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_controlTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.FacetGroup objects.</returns>
		public abstract TList<FacetGroup> GetByControlTypeID(TransactionManager transactionManager, System.Int32? _controlTypeID, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.FacetGroup Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.FacetGroupKey key, int start, int pageLength)
		{
			return GetByFacetGroupID(transactionManager, key.FacetGroupID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK__ZNodeFacet__DA7699285BD4BE68 index.
		/// </summary>
		/// <param name="_facetGroupID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.FacetGroup"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.FacetGroup GetByFacetGroupID(System.Int32 _facetGroupID)
		{
			int count = -1;
			return GetByFacetGroupID(null,_facetGroupID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK__ZNodeFacet__DA7699285BD4BE68 index.
		/// </summary>
		/// <param name="_facetGroupID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.FacetGroup"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.FacetGroup GetByFacetGroupID(System.Int32 _facetGroupID, int start, int pageLength)
		{
			int count = -1;
			return GetByFacetGroupID(null, _facetGroupID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK__ZNodeFacet__DA7699285BD4BE68 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_facetGroupID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.FacetGroup"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.FacetGroup GetByFacetGroupID(TransactionManager transactionManager, System.Int32 _facetGroupID)
		{
			int count = -1;
			return GetByFacetGroupID(transactionManager, _facetGroupID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK__ZNodeFacet__DA7699285BD4BE68 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_facetGroupID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.FacetGroup"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.FacetGroup GetByFacetGroupID(TransactionManager transactionManager, System.Int32 _facetGroupID, int start, int pageLength)
		{
			int count = -1;
			return GetByFacetGroupID(transactionManager, _facetGroupID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK__ZNodeFacet__DA7699285BD4BE68 index.
		/// </summary>
		/// <param name="_facetGroupID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.FacetGroup"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.FacetGroup GetByFacetGroupID(System.Int32 _facetGroupID, int start, int pageLength, out int count)
		{
			return GetByFacetGroupID(null, _facetGroupID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK__ZNodeFacet__DA7699285BD4BE68 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_facetGroupID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.FacetGroup"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.FacetGroup GetByFacetGroupID(TransactionManager transactionManager, System.Int32 _facetGroupID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;FacetGroup&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;FacetGroup&gt;"/></returns>
		public static TList<FacetGroup> Fill(IDataReader reader, TList<FacetGroup> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.FacetGroup c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("FacetGroup")
					.Append("|").Append((System.Int32)reader[((int)FacetGroupColumn.FacetGroupID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<FacetGroup>(
					key.ToString(), // EntityTrackingKey
					"FacetGroup",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.FacetGroup();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.FacetGroupID = (System.Int32)reader[((int)FacetGroupColumn.FacetGroupID - 1)];
					c.FacetGroupLabel = (reader.IsDBNull(((int)FacetGroupColumn.FacetGroupLabel - 1)))?null:(System.String)reader[((int)FacetGroupColumn.FacetGroupLabel - 1)];
					c.ControlTypeID = (reader.IsDBNull(((int)FacetGroupColumn.ControlTypeID - 1)))?null:(System.Int32?)reader[((int)FacetGroupColumn.ControlTypeID - 1)];
					c.CatalogID = (reader.IsDBNull(((int)FacetGroupColumn.CatalogID - 1)))?null:(System.Int32?)reader[((int)FacetGroupColumn.CatalogID - 1)];
					c.DisplayOrder = (reader.IsDBNull(((int)FacetGroupColumn.DisplayOrder - 1)))?null:(System.Int32?)reader[((int)FacetGroupColumn.DisplayOrder - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.FacetGroup"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.FacetGroup"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.FacetGroup entity)
		{
			if (!reader.Read()) return;
			
			entity.FacetGroupID = (System.Int32)reader[((int)FacetGroupColumn.FacetGroupID - 1)];
			entity.FacetGroupLabel = (reader.IsDBNull(((int)FacetGroupColumn.FacetGroupLabel - 1)))?null:(System.String)reader[((int)FacetGroupColumn.FacetGroupLabel - 1)];
			entity.ControlTypeID = (reader.IsDBNull(((int)FacetGroupColumn.ControlTypeID - 1)))?null:(System.Int32?)reader[((int)FacetGroupColumn.ControlTypeID - 1)];
			entity.CatalogID = (reader.IsDBNull(((int)FacetGroupColumn.CatalogID - 1)))?null:(System.Int32?)reader[((int)FacetGroupColumn.CatalogID - 1)];
			entity.DisplayOrder = (reader.IsDBNull(((int)FacetGroupColumn.DisplayOrder - 1)))?null:(System.Int32?)reader[((int)FacetGroupColumn.DisplayOrder - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.FacetGroup"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.FacetGroup"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.FacetGroup entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.FacetGroupID = (System.Int32)dataRow["FacetGroupID"];
			entity.FacetGroupLabel = Convert.IsDBNull(dataRow["FacetGroupLabel"]) ? null : (System.String)dataRow["FacetGroupLabel"];
			entity.ControlTypeID = Convert.IsDBNull(dataRow["ControlTypeID"]) ? null : (System.Int32?)dataRow["ControlTypeID"];
			entity.CatalogID = Convert.IsDBNull(dataRow["CatalogID"]) ? null : (System.Int32?)dataRow["CatalogID"];
			entity.DisplayOrder = Convert.IsDBNull(dataRow["DisplayOrder"]) ? null : (System.Int32?)dataRow["DisplayOrder"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.FacetGroup"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.FacetGroup Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.FacetGroup entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region CatalogIDSource	
			if (CanDeepLoad(entity, "Catalog|CatalogIDSource", deepLoadType, innerList) 
				&& entity.CatalogIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.CatalogID ?? (int)0);
				Catalog tmpEntity = EntityManager.LocateEntity<Catalog>(EntityLocator.ConstructKeyFromPkItems(typeof(Catalog), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CatalogIDSource = tmpEntity;
				else
					entity.CatalogIDSource = DataRepository.CatalogProvider.GetByCatalogID(transactionManager, (entity.CatalogID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CatalogIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CatalogIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CatalogProvider.DeepLoad(transactionManager, entity.CatalogIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CatalogIDSource

			#region ControlTypeIDSource	
			if (CanDeepLoad(entity, "FacetControlType|ControlTypeIDSource", deepLoadType, innerList) 
				&& entity.ControlTypeIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.ControlTypeID ?? (int)0);
				FacetControlType tmpEntity = EntityManager.LocateEntity<FacetControlType>(EntityLocator.ConstructKeyFromPkItems(typeof(FacetControlType), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ControlTypeIDSource = tmpEntity;
				else
					entity.ControlTypeIDSource = DataRepository.FacetControlTypeProvider.GetByControlTypeID(transactionManager, (entity.ControlTypeID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ControlTypeIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ControlTypeIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.FacetControlTypeProvider.DeepLoad(transactionManager, entity.ControlTypeIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ControlTypeIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByFacetGroupID methods when available
			
			#region FacetGroupCategoryCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<FacetGroupCategory>|FacetGroupCategoryCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'FacetGroupCategoryCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.FacetGroupCategoryCollection = DataRepository.FacetGroupCategoryProvider.GetByFacetGroupID(transactionManager, entity.FacetGroupID);

				if (deep && entity.FacetGroupCategoryCollection.Count > 0)
				{
					deepHandles.Add("FacetGroupCategoryCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<FacetGroupCategory>) DataRepository.FacetGroupCategoryProvider.DeepLoad,
						new object[] { transactionManager, entity.FacetGroupCategoryCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region CategoryIDCategoryCollection_From_FacetGroupCategory
			// RelationshipType.ManyToMany
			if (CanDeepLoad(entity, "List<Category>|CategoryIDCategoryCollection_From_FacetGroupCategory", deepLoadType, innerList))
			{
				entity.CategoryIDCategoryCollection_From_FacetGroupCategory = DataRepository.CategoryProvider.GetByFacetGroupIDFromFacetGroupCategory(transactionManager, entity.FacetGroupID);			 
		
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CategoryIDCategoryCollection_From_FacetGroupCategory' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CategoryIDCategoryCollection_From_FacetGroupCategory != null)
				{
					deepHandles.Add("CategoryIDCategoryCollection_From_FacetGroupCategory",
						new KeyValuePair<Delegate, object>((DeepLoadHandle< Category >) DataRepository.CategoryProvider.DeepLoad,
						new object[] { transactionManager, entity.CategoryIDCategoryCollection_From_FacetGroupCategory, deep, deepLoadType, childTypes, innerList }
					));
				}
			}
			#endregion
			
			
			
			#region FacetCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Facet>|FacetCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'FacetCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.FacetCollection = DataRepository.FacetProvider.GetByFacetGroupID(transactionManager, entity.FacetGroupID);

				if (deep && entity.FacetCollection.Count > 0)
				{
					deepHandles.Add("FacetCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Facet>) DataRepository.FacetProvider.DeepLoad,
						new object[] { transactionManager, entity.FacetCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.FacetGroup object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.FacetGroup instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.FacetGroup Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.FacetGroup entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region CatalogIDSource
			if (CanDeepSave(entity, "Catalog|CatalogIDSource", deepSaveType, innerList) 
				&& entity.CatalogIDSource != null)
			{
				DataRepository.CatalogProvider.Save(transactionManager, entity.CatalogIDSource);
				entity.CatalogID = entity.CatalogIDSource.CatalogID;
			}
			#endregion 
			
			#region ControlTypeIDSource
			if (CanDeepSave(entity, "FacetControlType|ControlTypeIDSource", deepSaveType, innerList) 
				&& entity.ControlTypeIDSource != null)
			{
				DataRepository.FacetControlTypeProvider.Save(transactionManager, entity.ControlTypeIDSource);
				entity.ControlTypeID = entity.ControlTypeIDSource.ControlTypeID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();

			#region CategoryIDCategoryCollection_From_FacetGroupCategory>
			if (CanDeepSave(entity.CategoryIDCategoryCollection_From_FacetGroupCategory, "List<Category>|CategoryIDCategoryCollection_From_FacetGroupCategory", deepSaveType, innerList))
			{
				if (entity.CategoryIDCategoryCollection_From_FacetGroupCategory.Count > 0 || entity.CategoryIDCategoryCollection_From_FacetGroupCategory.DeletedItems.Count > 0)
				{
					DataRepository.CategoryProvider.Save(transactionManager, entity.CategoryIDCategoryCollection_From_FacetGroupCategory); 
					deepHandles.Add("CategoryIDCategoryCollection_From_FacetGroupCategory",
						new KeyValuePair<Delegate, object>((DeepSaveHandle<Category>) DataRepository.CategoryProvider.DeepSave,
						new object[] { transactionManager, entity.CategoryIDCategoryCollection_From_FacetGroupCategory, deepSaveType, childTypes, innerList }
					));
				}
			}
			#endregion 
	
			#region List<FacetGroupCategory>
				if (CanDeepSave(entity.FacetGroupCategoryCollection, "List<FacetGroupCategory>|FacetGroupCategoryCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(FacetGroupCategory child in entity.FacetGroupCategoryCollection)
					{
						if(child.FacetGroupIDSource != null)
						{
								child.FacetGroupID = child.FacetGroupIDSource.FacetGroupID;
						}

						if(child.CategoryIDSource != null)
						{
								child.CategoryID = child.CategoryIDSource.CategoryID;
						}

					}

					if (entity.FacetGroupCategoryCollection.Count > 0 || entity.FacetGroupCategoryCollection.DeletedItems.Count > 0)
					{
						//DataRepository.FacetGroupCategoryProvider.Save(transactionManager, entity.FacetGroupCategoryCollection);
						
						deepHandles.Add("FacetGroupCategoryCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< FacetGroupCategory >) DataRepository.FacetGroupCategoryProvider.DeepSave,
							new object[] { transactionManager, entity.FacetGroupCategoryCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Facet>
				if (CanDeepSave(entity.FacetCollection, "List<Facet>|FacetCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Facet child in entity.FacetCollection)
					{
						if(child.FacetGroupIDSource != null)
						{
							child.FacetGroupID = child.FacetGroupIDSource.FacetGroupID;
						}
						else
						{
							child.FacetGroupID = entity.FacetGroupID;
						}

					}

					if (entity.FacetCollection.Count > 0 || entity.FacetCollection.DeletedItems.Count > 0)
					{
						//DataRepository.FacetProvider.Save(transactionManager, entity.FacetCollection);
						
						deepHandles.Add("FacetCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Facet >) DataRepository.FacetProvider.DeepSave,
							new object[] { transactionManager, entity.FacetCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region FacetGroupChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.FacetGroup</c>
	///</summary>
	public enum FacetGroupChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Catalog</c> at CatalogIDSource
		///</summary>
		[ChildEntityType(typeof(Catalog))]
		Catalog,
		
		///<summary>
		/// Composite Property for <c>FacetControlType</c> at ControlTypeIDSource
		///</summary>
		[ChildEntityType(typeof(FacetControlType))]
		FacetControlType,
		///<summary>
		/// Collection of <c>FacetGroup</c> as OneToMany for FacetGroupCategoryCollection
		///</summary>
		[ChildEntityType(typeof(TList<FacetGroupCategory>))]
		FacetGroupCategoryCollection,
		///<summary>
		/// Collection of <c>FacetGroup</c> as ManyToMany for CategoryCollection_From_FacetGroupCategory
		///</summary>
		[ChildEntityType(typeof(TList<Category>))]
		CategoryIDCategoryCollection_From_FacetGroupCategory,
		///<summary>
		/// Collection of <c>FacetGroup</c> as OneToMany for FacetCollection
		///</summary>
		[ChildEntityType(typeof(TList<Facet>))]
		FacetCollection,
	}
	
	#endregion FacetGroupChildEntityTypes
	
	#region FacetGroupFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;FacetGroupColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FacetGroup"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FacetGroupFilterBuilder : SqlFilterBuilder<FacetGroupColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FacetGroupFilterBuilder class.
		/// </summary>
		public FacetGroupFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the FacetGroupFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FacetGroupFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FacetGroupFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FacetGroupFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FacetGroupFilterBuilder
	
	#region FacetGroupParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;FacetGroupColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FacetGroup"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FacetGroupParameterBuilder : ParameterizedSqlFilterBuilder<FacetGroupColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FacetGroupParameterBuilder class.
		/// </summary>
		public FacetGroupParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the FacetGroupParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FacetGroupParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FacetGroupParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FacetGroupParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FacetGroupParameterBuilder
	
	#region FacetGroupSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;FacetGroupColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FacetGroup"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class FacetGroupSortBuilder : SqlSortBuilder<FacetGroupColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FacetGroupSqlSortBuilder class.
		/// </summary>
		public FacetGroupSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion FacetGroupSortBuilder
	
} // end namespace
