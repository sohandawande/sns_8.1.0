﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="GiftCardHistoryProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class GiftCardHistoryProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.GiftCardHistory, ZNode.Libraries.DataAccess.Entities.GiftCardHistoryKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.GiftCardHistoryKey key)
		{
			return Delete(transactionManager, key.GiftCardHistoryID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_giftCardHistoryID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _giftCardHistoryID)
		{
			return Delete(null, _giftCardHistoryID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_giftCardHistoryID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _giftCardHistoryID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeGiftCardHistory_ZNodeGiftCard key.
		///		FK_ZNodeGiftCardHistory_ZNodeGiftCard Description: 
		/// </summary>
		/// <param name="_giftCardID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.GiftCardHistory objects.</returns>
		public TList<GiftCardHistory> GetByGiftCardID(System.Int32 _giftCardID)
		{
			int count = -1;
			return GetByGiftCardID(_giftCardID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeGiftCardHistory_ZNodeGiftCard key.
		///		FK_ZNodeGiftCardHistory_ZNodeGiftCard Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_giftCardID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.GiftCardHistory objects.</returns>
		/// <remarks></remarks>
		public TList<GiftCardHistory> GetByGiftCardID(TransactionManager transactionManager, System.Int32 _giftCardID)
		{
			int count = -1;
			return GetByGiftCardID(transactionManager, _giftCardID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeGiftCardHistory_ZNodeGiftCard key.
		///		FK_ZNodeGiftCardHistory_ZNodeGiftCard Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_giftCardID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.GiftCardHistory objects.</returns>
		public TList<GiftCardHistory> GetByGiftCardID(TransactionManager transactionManager, System.Int32 _giftCardID, int start, int pageLength)
		{
			int count = -1;
			return GetByGiftCardID(transactionManager, _giftCardID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeGiftCardHistory_ZNodeGiftCard key.
		///		fKZNodeGiftCardHistoryZNodeGiftCard Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_giftCardID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.GiftCardHistory objects.</returns>
		public TList<GiftCardHistory> GetByGiftCardID(System.Int32 _giftCardID, int start, int pageLength)
		{
			int count =  -1;
			return GetByGiftCardID(null, _giftCardID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeGiftCardHistory_ZNodeGiftCard key.
		///		fKZNodeGiftCardHistoryZNodeGiftCard Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_giftCardID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.GiftCardHistory objects.</returns>
		public TList<GiftCardHistory> GetByGiftCardID(System.Int32 _giftCardID, int start, int pageLength,out int count)
		{
			return GetByGiftCardID(null, _giftCardID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeGiftCardHistory_ZNodeGiftCard key.
		///		FK_ZNodeGiftCardHistory_ZNodeGiftCard Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_giftCardID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.GiftCardHistory objects.</returns>
		public abstract TList<GiftCardHistory> GetByGiftCardID(TransactionManager transactionManager, System.Int32 _giftCardID, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeGiftCardHistory_ZNodeOrder key.
		///		FK_ZNodeGiftCardHistory_ZNodeOrder Description: 
		/// </summary>
		/// <param name="_orderID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.GiftCardHistory objects.</returns>
		public TList<GiftCardHistory> GetByOrderID(System.Int32 _orderID)
		{
			int count = -1;
			return GetByOrderID(_orderID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeGiftCardHistory_ZNodeOrder key.
		///		FK_ZNodeGiftCardHistory_ZNodeOrder Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_orderID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.GiftCardHistory objects.</returns>
		/// <remarks></remarks>
		public TList<GiftCardHistory> GetByOrderID(TransactionManager transactionManager, System.Int32 _orderID)
		{
			int count = -1;
			return GetByOrderID(transactionManager, _orderID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeGiftCardHistory_ZNodeOrder key.
		///		FK_ZNodeGiftCardHistory_ZNodeOrder Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_orderID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.GiftCardHistory objects.</returns>
		public TList<GiftCardHistory> GetByOrderID(TransactionManager transactionManager, System.Int32 _orderID, int start, int pageLength)
		{
			int count = -1;
			return GetByOrderID(transactionManager, _orderID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeGiftCardHistory_ZNodeOrder key.
		///		fKZNodeGiftCardHistoryZNodeOrder Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_orderID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.GiftCardHistory objects.</returns>
		public TList<GiftCardHistory> GetByOrderID(System.Int32 _orderID, int start, int pageLength)
		{
			int count =  -1;
			return GetByOrderID(null, _orderID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeGiftCardHistory_ZNodeOrder key.
		///		fKZNodeGiftCardHistoryZNodeOrder Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_orderID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.GiftCardHistory objects.</returns>
		public TList<GiftCardHistory> GetByOrderID(System.Int32 _orderID, int start, int pageLength,out int count)
		{
			return GetByOrderID(null, _orderID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeGiftCardHistory_ZNodeOrder key.
		///		FK_ZNodeGiftCardHistory_ZNodeOrder Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_orderID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.GiftCardHistory objects.</returns>
		public abstract TList<GiftCardHistory> GetByOrderID(TransactionManager transactionManager, System.Int32 _orderID, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.GiftCardHistory Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.GiftCardHistoryKey key, int start, int pageLength)
		{
			return GetByGiftCardHistoryID(transactionManager, key.GiftCardHistoryID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodeGiftCardHistory index.
		/// </summary>
		/// <param name="_giftCardHistoryID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.GiftCardHistory"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.GiftCardHistory GetByGiftCardHistoryID(System.Int32 _giftCardHistoryID)
		{
			int count = -1;
			return GetByGiftCardHistoryID(null,_giftCardHistoryID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeGiftCardHistory index.
		/// </summary>
		/// <param name="_giftCardHistoryID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.GiftCardHistory"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.GiftCardHistory GetByGiftCardHistoryID(System.Int32 _giftCardHistoryID, int start, int pageLength)
		{
			int count = -1;
			return GetByGiftCardHistoryID(null, _giftCardHistoryID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeGiftCardHistory index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_giftCardHistoryID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.GiftCardHistory"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.GiftCardHistory GetByGiftCardHistoryID(TransactionManager transactionManager, System.Int32 _giftCardHistoryID)
		{
			int count = -1;
			return GetByGiftCardHistoryID(transactionManager, _giftCardHistoryID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeGiftCardHistory index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_giftCardHistoryID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.GiftCardHistory"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.GiftCardHistory GetByGiftCardHistoryID(TransactionManager transactionManager, System.Int32 _giftCardHistoryID, int start, int pageLength)
		{
			int count = -1;
			return GetByGiftCardHistoryID(transactionManager, _giftCardHistoryID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeGiftCardHistory index.
		/// </summary>
		/// <param name="_giftCardHistoryID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.GiftCardHistory"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.GiftCardHistory GetByGiftCardHistoryID(System.Int32 _giftCardHistoryID, int start, int pageLength, out int count)
		{
			return GetByGiftCardHistoryID(null, _giftCardHistoryID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeGiftCardHistory index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_giftCardHistoryID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.GiftCardHistory"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.GiftCardHistory GetByGiftCardHistoryID(TransactionManager transactionManager, System.Int32 _giftCardHistoryID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;GiftCardHistory&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;GiftCardHistory&gt;"/></returns>
		public static TList<GiftCardHistory> Fill(IDataReader reader, TList<GiftCardHistory> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.GiftCardHistory c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("GiftCardHistory")
					.Append("|").Append((System.Int32)reader[((int)GiftCardHistoryColumn.GiftCardHistoryID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<GiftCardHistory>(
					key.ToString(), // EntityTrackingKey
					"GiftCardHistory",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.GiftCardHistory();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.GiftCardHistoryID = (System.Int32)reader[((int)GiftCardHistoryColumn.GiftCardHistoryID - 1)];
					c.GiftCardID = (System.Int32)reader[((int)GiftCardHistoryColumn.GiftCardID - 1)];
					c.TransactionDate = (System.DateTime)reader[((int)GiftCardHistoryColumn.TransactionDate - 1)];
					c.TransactionAmount = (System.Decimal)reader[((int)GiftCardHistoryColumn.TransactionAmount - 1)];
					c.OrderID = (System.Int32)reader[((int)GiftCardHistoryColumn.OrderID - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.GiftCardHistory"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.GiftCardHistory"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.GiftCardHistory entity)
		{
			if (!reader.Read()) return;
			
			entity.GiftCardHistoryID = (System.Int32)reader[((int)GiftCardHistoryColumn.GiftCardHistoryID - 1)];
			entity.GiftCardID = (System.Int32)reader[((int)GiftCardHistoryColumn.GiftCardID - 1)];
			entity.TransactionDate = (System.DateTime)reader[((int)GiftCardHistoryColumn.TransactionDate - 1)];
			entity.TransactionAmount = (System.Decimal)reader[((int)GiftCardHistoryColumn.TransactionAmount - 1)];
			entity.OrderID = (System.Int32)reader[((int)GiftCardHistoryColumn.OrderID - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.GiftCardHistory"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.GiftCardHistory"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.GiftCardHistory entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.GiftCardHistoryID = (System.Int32)dataRow["GiftCardHistoryID"];
			entity.GiftCardID = (System.Int32)dataRow["GiftCardID"];
			entity.TransactionDate = (System.DateTime)dataRow["TransactionDate"];
			entity.TransactionAmount = (System.Decimal)dataRow["TransactionAmount"];
			entity.OrderID = (System.Int32)dataRow["OrderID"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.GiftCardHistory"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.GiftCardHistory Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.GiftCardHistory entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region GiftCardIDSource	
			if (CanDeepLoad(entity, "GiftCard|GiftCardIDSource", deepLoadType, innerList) 
				&& entity.GiftCardIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.GiftCardID;
				GiftCard tmpEntity = EntityManager.LocateEntity<GiftCard>(EntityLocator.ConstructKeyFromPkItems(typeof(GiftCard), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.GiftCardIDSource = tmpEntity;
				else
					entity.GiftCardIDSource = DataRepository.GiftCardProvider.GetByGiftCardId(transactionManager, entity.GiftCardID);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'GiftCardIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.GiftCardIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.GiftCardProvider.DeepLoad(transactionManager, entity.GiftCardIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion GiftCardIDSource

			#region OrderIDSource	
			if (CanDeepLoad(entity, "Order|OrderIDSource", deepLoadType, innerList) 
				&& entity.OrderIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.OrderID;
				Order tmpEntity = EntityManager.LocateEntity<Order>(EntityLocator.ConstructKeyFromPkItems(typeof(Order), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.OrderIDSource = tmpEntity;
				else
					entity.OrderIDSource = DataRepository.OrderProvider.GetByOrderID(transactionManager, entity.OrderID);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'OrderIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.OrderIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.OrderProvider.DeepLoad(transactionManager, entity.OrderIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion OrderIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.GiftCardHistory object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.GiftCardHistory instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.GiftCardHistory Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.GiftCardHistory entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region GiftCardIDSource
			if (CanDeepSave(entity, "GiftCard|GiftCardIDSource", deepSaveType, innerList) 
				&& entity.GiftCardIDSource != null)
			{
				DataRepository.GiftCardProvider.Save(transactionManager, entity.GiftCardIDSource);
				entity.GiftCardID = entity.GiftCardIDSource.GiftCardId;
			}
			#endregion 
			
			#region OrderIDSource
			if (CanDeepSave(entity, "Order|OrderIDSource", deepSaveType, innerList) 
				&& entity.OrderIDSource != null)
			{
				DataRepository.OrderProvider.Save(transactionManager, entity.OrderIDSource);
				entity.OrderID = entity.OrderIDSource.OrderID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region GiftCardHistoryChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.GiftCardHistory</c>
	///</summary>
	public enum GiftCardHistoryChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>GiftCard</c> at GiftCardIDSource
		///</summary>
		[ChildEntityType(typeof(GiftCard))]
		GiftCard,
		
		///<summary>
		/// Composite Property for <c>Order</c> at OrderIDSource
		///</summary>
		[ChildEntityType(typeof(Order))]
		Order,
	}
	
	#endregion GiftCardHistoryChildEntityTypes
	
	#region GiftCardHistoryFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;GiftCardHistoryColumn&gt;"/> class
	/// that is used exclusively with a <see cref="GiftCardHistory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class GiftCardHistoryFilterBuilder : SqlFilterBuilder<GiftCardHistoryColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the GiftCardHistoryFilterBuilder class.
		/// </summary>
		public GiftCardHistoryFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the GiftCardHistoryFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public GiftCardHistoryFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the GiftCardHistoryFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public GiftCardHistoryFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion GiftCardHistoryFilterBuilder
	
	#region GiftCardHistoryParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;GiftCardHistoryColumn&gt;"/> class
	/// that is used exclusively with a <see cref="GiftCardHistory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class GiftCardHistoryParameterBuilder : ParameterizedSqlFilterBuilder<GiftCardHistoryColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the GiftCardHistoryParameterBuilder class.
		/// </summary>
		public GiftCardHistoryParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the GiftCardHistoryParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public GiftCardHistoryParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the GiftCardHistoryParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public GiftCardHistoryParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion GiftCardHistoryParameterBuilder
	
	#region GiftCardHistorySortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;GiftCardHistoryColumn&gt;"/> class
	/// that is used exclusively with a <see cref="GiftCardHistory"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class GiftCardHistorySortBuilder : SqlSortBuilder<GiftCardHistoryColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the GiftCardHistorySqlSortBuilder class.
		/// </summary>
		public GiftCardHistorySortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion GiftCardHistorySortBuilder
	
} // end namespace
