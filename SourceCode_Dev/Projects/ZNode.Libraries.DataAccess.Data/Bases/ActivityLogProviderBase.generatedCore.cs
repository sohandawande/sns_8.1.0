﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ActivityLogProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class ActivityLogProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.ActivityLog, ZNode.Libraries.DataAccess.Entities.ActivityLogKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ActivityLogKey key)
		{
			return Delete(transactionManager, key.ActivityLogID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_activityLogID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _activityLogID)
		{
			return Delete(null, _activityLogID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_activityLogID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _activityLogID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeActivityLog_ZNodeActivityLogType key.
		///		FK_ZNodeActivityLog_ZNodeActivityLogType Description: 
		/// </summary>
		/// <param name="_activityLogTypeID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ActivityLog objects.</returns>
		public TList<ActivityLog> GetByActivityLogTypeID(System.Int32 _activityLogTypeID)
		{
			int count = -1;
			return GetByActivityLogTypeID(_activityLogTypeID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeActivityLog_ZNodeActivityLogType key.
		///		FK_ZNodeActivityLog_ZNodeActivityLogType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_activityLogTypeID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ActivityLog objects.</returns>
		/// <remarks></remarks>
		public TList<ActivityLog> GetByActivityLogTypeID(TransactionManager transactionManager, System.Int32 _activityLogTypeID)
		{
			int count = -1;
			return GetByActivityLogTypeID(transactionManager, _activityLogTypeID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeActivityLog_ZNodeActivityLogType key.
		///		FK_ZNodeActivityLog_ZNodeActivityLogType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_activityLogTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ActivityLog objects.</returns>
		public TList<ActivityLog> GetByActivityLogTypeID(TransactionManager transactionManager, System.Int32 _activityLogTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetByActivityLogTypeID(transactionManager, _activityLogTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeActivityLog_ZNodeActivityLogType key.
		///		fKZNodeActivityLogZNodeActivityLogType Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_activityLogTypeID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ActivityLog objects.</returns>
		public TList<ActivityLog> GetByActivityLogTypeID(System.Int32 _activityLogTypeID, int start, int pageLength)
		{
			int count =  -1;
			return GetByActivityLogTypeID(null, _activityLogTypeID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeActivityLog_ZNodeActivityLogType key.
		///		fKZNodeActivityLogZNodeActivityLogType Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_activityLogTypeID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ActivityLog objects.</returns>
		public TList<ActivityLog> GetByActivityLogTypeID(System.Int32 _activityLogTypeID, int start, int pageLength,out int count)
		{
			return GetByActivityLogTypeID(null, _activityLogTypeID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeActivityLog_ZNodeActivityLogType key.
		///		FK_ZNodeActivityLog_ZNodeActivityLogType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_activityLogTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ActivityLog objects.</returns>
		public abstract TList<ActivityLog> GetByActivityLogTypeID(TransactionManager transactionManager, System.Int32 _activityLogTypeID, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.ActivityLog Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ActivityLogKey key, int start, int pageLength)
		{
			return GetByActivityLogID(transactionManager, key.ActivityLogID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IDX_ZNodeActivityLog_PortalID index.
		/// </summary>
		/// <param name="_portalID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;ActivityLog&gt;"/> class.</returns>
		public TList<ActivityLog> GetByPortalID(System.Int32? _portalID)
		{
			int count = -1;
			return GetByPortalID(null,_portalID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IDX_ZNodeActivityLog_PortalID index.
		/// </summary>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ActivityLog&gt;"/> class.</returns>
		public TList<ActivityLog> GetByPortalID(System.Int32? _portalID, int start, int pageLength)
		{
			int count = -1;
			return GetByPortalID(null, _portalID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IDX_ZNodeActivityLog_PortalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ActivityLog&gt;"/> class.</returns>
		public TList<ActivityLog> GetByPortalID(TransactionManager transactionManager, System.Int32? _portalID)
		{
			int count = -1;
			return GetByPortalID(transactionManager, _portalID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IDX_ZNodeActivityLog_PortalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ActivityLog&gt;"/> class.</returns>
		public TList<ActivityLog> GetByPortalID(TransactionManager transactionManager, System.Int32? _portalID, int start, int pageLength)
		{
			int count = -1;
			return GetByPortalID(transactionManager, _portalID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IDX_ZNodeActivityLog_PortalID index.
		/// </summary>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ActivityLog&gt;"/> class.</returns>
		public TList<ActivityLog> GetByPortalID(System.Int32? _portalID, int start, int pageLength, out int count)
		{
			return GetByPortalID(null, _portalID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IDX_ZNodeActivityLog_PortalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;ActivityLog&gt;"/> class.</returns>
		public abstract TList<ActivityLog> GetByPortalID(TransactionManager transactionManager, System.Int32? _portalID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_Data1 index.
		/// </summary>
		/// <param name="_data1"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;ActivityLog&gt;"/> class.</returns>
		public TList<ActivityLog> GetByData1(System.String _data1)
		{
			int count = -1;
			return GetByData1(null,_data1, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Data1 index.
		/// </summary>
		/// <param name="_data1"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ActivityLog&gt;"/> class.</returns>
		public TList<ActivityLog> GetByData1(System.String _data1, int start, int pageLength)
		{
			int count = -1;
			return GetByData1(null, _data1, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Data1 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_data1"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ActivityLog&gt;"/> class.</returns>
		public TList<ActivityLog> GetByData1(TransactionManager transactionManager, System.String _data1)
		{
			int count = -1;
			return GetByData1(transactionManager, _data1, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Data1 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_data1"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ActivityLog&gt;"/> class.</returns>
		public TList<ActivityLog> GetByData1(TransactionManager transactionManager, System.String _data1, int start, int pageLength)
		{
			int count = -1;
			return GetByData1(transactionManager, _data1, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Data1 index.
		/// </summary>
		/// <param name="_data1"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ActivityLog&gt;"/> class.</returns>
		public TList<ActivityLog> GetByData1(System.String _data1, int start, int pageLength, out int count)
		{
			return GetByData1(null, _data1, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Data1 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_data1"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;ActivityLog&gt;"/> class.</returns>
		public abstract TList<ActivityLog> GetByData1(TransactionManager transactionManager, System.String _data1, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_Data2 index.
		/// </summary>
		/// <param name="_data2"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;ActivityLog&gt;"/> class.</returns>
		public TList<ActivityLog> GetByData2(System.String _data2)
		{
			int count = -1;
			return GetByData2(null,_data2, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Data2 index.
		/// </summary>
		/// <param name="_data2"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ActivityLog&gt;"/> class.</returns>
		public TList<ActivityLog> GetByData2(System.String _data2, int start, int pageLength)
		{
			int count = -1;
			return GetByData2(null, _data2, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Data2 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_data2"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ActivityLog&gt;"/> class.</returns>
		public TList<ActivityLog> GetByData2(TransactionManager transactionManager, System.String _data2)
		{
			int count = -1;
			return GetByData2(transactionManager, _data2, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Data2 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_data2"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ActivityLog&gt;"/> class.</returns>
		public TList<ActivityLog> GetByData2(TransactionManager transactionManager, System.String _data2, int start, int pageLength)
		{
			int count = -1;
			return GetByData2(transactionManager, _data2, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Data2 index.
		/// </summary>
		/// <param name="_data2"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ActivityLog&gt;"/> class.</returns>
		public TList<ActivityLog> GetByData2(System.String _data2, int start, int pageLength, out int count)
		{
			return GetByData2(null, _data2, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Data2 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_data2"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;ActivityLog&gt;"/> class.</returns>
		public abstract TList<ActivityLog> GetByData2(TransactionManager transactionManager, System.String _data2, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_Data3 index.
		/// </summary>
		/// <param name="_data3"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;ActivityLog&gt;"/> class.</returns>
		public TList<ActivityLog> GetByData3(System.String _data3)
		{
			int count = -1;
			return GetByData3(null,_data3, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Data3 index.
		/// </summary>
		/// <param name="_data3"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ActivityLog&gt;"/> class.</returns>
		public TList<ActivityLog> GetByData3(System.String _data3, int start, int pageLength)
		{
			int count = -1;
			return GetByData3(null, _data3, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Data3 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_data3"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ActivityLog&gt;"/> class.</returns>
		public TList<ActivityLog> GetByData3(TransactionManager transactionManager, System.String _data3)
		{
			int count = -1;
			return GetByData3(transactionManager, _data3, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Data3 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_data3"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ActivityLog&gt;"/> class.</returns>
		public TList<ActivityLog> GetByData3(TransactionManager transactionManager, System.String _data3, int start, int pageLength)
		{
			int count = -1;
			return GetByData3(transactionManager, _data3, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Data3 index.
		/// </summary>
		/// <param name="_data3"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ActivityLog&gt;"/> class.</returns>
		public TList<ActivityLog> GetByData3(System.String _data3, int start, int pageLength, out int count)
		{
			return GetByData3(null, _data3, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Data3 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_data3"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;ActivityLog&gt;"/> class.</returns>
		public abstract TList<ActivityLog> GetByData3(TransactionManager transactionManager, System.String _data3, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodeActivityLog index.
		/// </summary>
		/// <param name="_activityLogID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ActivityLog"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ActivityLog GetByActivityLogID(System.Int32 _activityLogID)
		{
			int count = -1;
			return GetByActivityLogID(null,_activityLogID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeActivityLog index.
		/// </summary>
		/// <param name="_activityLogID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ActivityLog"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ActivityLog GetByActivityLogID(System.Int32 _activityLogID, int start, int pageLength)
		{
			int count = -1;
			return GetByActivityLogID(null, _activityLogID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeActivityLog index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_activityLogID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ActivityLog"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ActivityLog GetByActivityLogID(TransactionManager transactionManager, System.Int32 _activityLogID)
		{
			int count = -1;
			return GetByActivityLogID(transactionManager, _activityLogID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeActivityLog index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_activityLogID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ActivityLog"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ActivityLog GetByActivityLogID(TransactionManager transactionManager, System.Int32 _activityLogID, int start, int pageLength)
		{
			int count = -1;
			return GetByActivityLogID(transactionManager, _activityLogID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeActivityLog index.
		/// </summary>
		/// <param name="_activityLogID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ActivityLog"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ActivityLog GetByActivityLogID(System.Int32 _activityLogID, int start, int pageLength, out int count)
		{
			return GetByActivityLogID(null, _activityLogID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeActivityLog index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_activityLogID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ActivityLog"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.ActivityLog GetByActivityLogID(TransactionManager transactionManager, System.Int32 _activityLogID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;ActivityLog&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;ActivityLog&gt;"/></returns>
		public static TList<ActivityLog> Fill(IDataReader reader, TList<ActivityLog> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.ActivityLog c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("ActivityLog")
					.Append("|").Append((System.Int32)reader[((int)ActivityLogColumn.ActivityLogID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<ActivityLog>(
					key.ToString(), // EntityTrackingKey
					"ActivityLog",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.ActivityLog();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.ActivityLogID = (System.Int32)reader[((int)ActivityLogColumn.ActivityLogID - 1)];
					c.ActivityLogTypeID = (System.Int32)reader[((int)ActivityLogColumn.ActivityLogTypeID - 1)];
					c.CreateDte = (System.DateTime)reader[((int)ActivityLogColumn.CreateDte - 1)];
					c.EndDte = (reader.IsDBNull(((int)ActivityLogColumn.EndDte - 1)))?null:(System.DateTime?)reader[((int)ActivityLogColumn.EndDte - 1)];
					c.PortalID = (reader.IsDBNull(((int)ActivityLogColumn.PortalID - 1)))?null:(System.Int32?)reader[((int)ActivityLogColumn.PortalID - 1)];
					c.URL = (reader.IsDBNull(((int)ActivityLogColumn.URL - 1)))?null:(System.String)reader[((int)ActivityLogColumn.URL - 1)];
					c.Data1 = (reader.IsDBNull(((int)ActivityLogColumn.Data1 - 1)))?null:(System.String)reader[((int)ActivityLogColumn.Data1 - 1)];
					c.Data2 = (reader.IsDBNull(((int)ActivityLogColumn.Data2 - 1)))?null:(System.String)reader[((int)ActivityLogColumn.Data2 - 1)];
					c.Data3 = (reader.IsDBNull(((int)ActivityLogColumn.Data3 - 1)))?null:(System.String)reader[((int)ActivityLogColumn.Data3 - 1)];
					c.Status = (reader.IsDBNull(((int)ActivityLogColumn.Status - 1)))?null:(System.String)reader[((int)ActivityLogColumn.Status - 1)];
					c.LongData = (reader.IsDBNull(((int)ActivityLogColumn.LongData - 1)))?null:(System.String)reader[((int)ActivityLogColumn.LongData - 1)];
					c.Source = (reader.IsDBNull(((int)ActivityLogColumn.Source - 1)))?null:(System.String)reader[((int)ActivityLogColumn.Source - 1)];
					c.Target = (reader.IsDBNull(((int)ActivityLogColumn.Target - 1)))?null:(System.String)reader[((int)ActivityLogColumn.Target - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.ActivityLog"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ActivityLog"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.ActivityLog entity)
		{
			if (!reader.Read()) return;
			
			entity.ActivityLogID = (System.Int32)reader[((int)ActivityLogColumn.ActivityLogID - 1)];
			entity.ActivityLogTypeID = (System.Int32)reader[((int)ActivityLogColumn.ActivityLogTypeID - 1)];
			entity.CreateDte = (System.DateTime)reader[((int)ActivityLogColumn.CreateDte - 1)];
			entity.EndDte = (reader.IsDBNull(((int)ActivityLogColumn.EndDte - 1)))?null:(System.DateTime?)reader[((int)ActivityLogColumn.EndDte - 1)];
			entity.PortalID = (reader.IsDBNull(((int)ActivityLogColumn.PortalID - 1)))?null:(System.Int32?)reader[((int)ActivityLogColumn.PortalID - 1)];
			entity.URL = (reader.IsDBNull(((int)ActivityLogColumn.URL - 1)))?null:(System.String)reader[((int)ActivityLogColumn.URL - 1)];
			entity.Data1 = (reader.IsDBNull(((int)ActivityLogColumn.Data1 - 1)))?null:(System.String)reader[((int)ActivityLogColumn.Data1 - 1)];
			entity.Data2 = (reader.IsDBNull(((int)ActivityLogColumn.Data2 - 1)))?null:(System.String)reader[((int)ActivityLogColumn.Data2 - 1)];
			entity.Data3 = (reader.IsDBNull(((int)ActivityLogColumn.Data3 - 1)))?null:(System.String)reader[((int)ActivityLogColumn.Data3 - 1)];
			entity.Status = (reader.IsDBNull(((int)ActivityLogColumn.Status - 1)))?null:(System.String)reader[((int)ActivityLogColumn.Status - 1)];
			entity.LongData = (reader.IsDBNull(((int)ActivityLogColumn.LongData - 1)))?null:(System.String)reader[((int)ActivityLogColumn.LongData - 1)];
			entity.Source = (reader.IsDBNull(((int)ActivityLogColumn.Source - 1)))?null:(System.String)reader[((int)ActivityLogColumn.Source - 1)];
			entity.Target = (reader.IsDBNull(((int)ActivityLogColumn.Target - 1)))?null:(System.String)reader[((int)ActivityLogColumn.Target - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.ActivityLog"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ActivityLog"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.ActivityLog entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.ActivityLogID = (System.Int32)dataRow["ActivityLogID"];
			entity.ActivityLogTypeID = (System.Int32)dataRow["ActivityLogTypeID"];
			entity.CreateDte = (System.DateTime)dataRow["CreateDte"];
			entity.EndDte = Convert.IsDBNull(dataRow["EndDte"]) ? null : (System.DateTime?)dataRow["EndDte"];
			entity.PortalID = Convert.IsDBNull(dataRow["PortalID"]) ? null : (System.Int32?)dataRow["PortalID"];
			entity.URL = Convert.IsDBNull(dataRow["URL"]) ? null : (System.String)dataRow["URL"];
			entity.Data1 = Convert.IsDBNull(dataRow["Data1"]) ? null : (System.String)dataRow["Data1"];
			entity.Data2 = Convert.IsDBNull(dataRow["Data2"]) ? null : (System.String)dataRow["Data2"];
			entity.Data3 = Convert.IsDBNull(dataRow["Data3"]) ? null : (System.String)dataRow["Data3"];
			entity.Status = Convert.IsDBNull(dataRow["Status"]) ? null : (System.String)dataRow["Status"];
			entity.LongData = Convert.IsDBNull(dataRow["LongData"]) ? null : (System.String)dataRow["LongData"];
			entity.Source = Convert.IsDBNull(dataRow["Source"]) ? null : (System.String)dataRow["Source"];
			entity.Target = Convert.IsDBNull(dataRow["Target"]) ? null : (System.String)dataRow["Target"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ActivityLog"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.ActivityLog Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ActivityLog entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region ActivityLogTypeIDSource	
			if (CanDeepLoad(entity, "ActivityLogType|ActivityLogTypeIDSource", deepLoadType, innerList) 
				&& entity.ActivityLogTypeIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.ActivityLogTypeID;
				ActivityLogType tmpEntity = EntityManager.LocateEntity<ActivityLogType>(EntityLocator.ConstructKeyFromPkItems(typeof(ActivityLogType), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ActivityLogTypeIDSource = tmpEntity;
				else
					entity.ActivityLogTypeIDSource = DataRepository.ActivityLogTypeProvider.GetByActivityLogTypeID(transactionManager, entity.ActivityLogTypeID);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ActivityLogTypeIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ActivityLogTypeIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.ActivityLogTypeProvider.DeepLoad(transactionManager, entity.ActivityLogTypeIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ActivityLogTypeIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.ActivityLog object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.ActivityLog instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.ActivityLog Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ActivityLog entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region ActivityLogTypeIDSource
			if (CanDeepSave(entity, "ActivityLogType|ActivityLogTypeIDSource", deepSaveType, innerList) 
				&& entity.ActivityLogTypeIDSource != null)
			{
				DataRepository.ActivityLogTypeProvider.Save(transactionManager, entity.ActivityLogTypeIDSource);
				entity.ActivityLogTypeID = entity.ActivityLogTypeIDSource.ActivityLogTypeID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region ActivityLogChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.ActivityLog</c>
	///</summary>
	public enum ActivityLogChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>ActivityLogType</c> at ActivityLogTypeIDSource
		///</summary>
		[ChildEntityType(typeof(ActivityLogType))]
		ActivityLogType,
	}
	
	#endregion ActivityLogChildEntityTypes
	
	#region ActivityLogFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;ActivityLogColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ActivityLog"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ActivityLogFilterBuilder : SqlFilterBuilder<ActivityLogColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ActivityLogFilterBuilder class.
		/// </summary>
		public ActivityLogFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ActivityLogFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ActivityLogFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ActivityLogFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ActivityLogFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ActivityLogFilterBuilder
	
	#region ActivityLogParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;ActivityLogColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ActivityLog"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ActivityLogParameterBuilder : ParameterizedSqlFilterBuilder<ActivityLogColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ActivityLogParameterBuilder class.
		/// </summary>
		public ActivityLogParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ActivityLogParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ActivityLogParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ActivityLogParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ActivityLogParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ActivityLogParameterBuilder
	
	#region ActivityLogSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;ActivityLogColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ActivityLog"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ActivityLogSortBuilder : SqlSortBuilder<ActivityLogColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ActivityLogSqlSortBuilder class.
		/// </summary>
		public ActivityLogSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ActivityLogSortBuilder
	
} // end namespace
