﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="RMARequestProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class RMARequestProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.RMARequest, ZNode.Libraries.DataAccess.Entities.RMARequestKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.RMARequestKey key)
		{
			return Delete(transactionManager, key.RMARequestID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_rMARequestID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _rMARequestID)
		{
			return Delete(null, _rMARequestID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_rMARequestID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _rMARequestID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeRMARequest_ZNodeRequestStatus key.
		///		FK_ZNodeRMARequest_ZNodeRequestStatus Description: 
		/// </summary>
		/// <param name="_requestStatusID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.RMARequest objects.</returns>
		public TList<RMARequest> GetByRequestStatusID(System.Int32? _requestStatusID)
		{
			int count = -1;
			return GetByRequestStatusID(_requestStatusID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeRMARequest_ZNodeRequestStatus key.
		///		FK_ZNodeRMARequest_ZNodeRequestStatus Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_requestStatusID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.RMARequest objects.</returns>
		/// <remarks></remarks>
		public TList<RMARequest> GetByRequestStatusID(TransactionManager transactionManager, System.Int32? _requestStatusID)
		{
			int count = -1;
			return GetByRequestStatusID(transactionManager, _requestStatusID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeRMARequest_ZNodeRequestStatus key.
		///		FK_ZNodeRMARequest_ZNodeRequestStatus Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_requestStatusID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.RMARequest objects.</returns>
		public TList<RMARequest> GetByRequestStatusID(TransactionManager transactionManager, System.Int32? _requestStatusID, int start, int pageLength)
		{
			int count = -1;
			return GetByRequestStatusID(transactionManager, _requestStatusID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeRMARequest_ZNodeRequestStatus key.
		///		fKZNodeRMARequestZNodeRequestStatus Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_requestStatusID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.RMARequest objects.</returns>
		public TList<RMARequest> GetByRequestStatusID(System.Int32? _requestStatusID, int start, int pageLength)
		{
			int count =  -1;
			return GetByRequestStatusID(null, _requestStatusID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeRMARequest_ZNodeRequestStatus key.
		///		fKZNodeRMARequestZNodeRequestStatus Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_requestStatusID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.RMARequest objects.</returns>
		public TList<RMARequest> GetByRequestStatusID(System.Int32? _requestStatusID, int start, int pageLength,out int count)
		{
			return GetByRequestStatusID(null, _requestStatusID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeRMARequest_ZNodeRequestStatus key.
		///		FK_ZNodeRMARequest_ZNodeRequestStatus Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_requestStatusID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.RMARequest objects.</returns>
		public abstract TList<RMARequest> GetByRequestStatusID(TransactionManager transactionManager, System.Int32? _requestStatusID, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.RMARequest Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.RMARequestKey key, int start, int pageLength)
		{
			return GetByRMARequestID(transactionManager, key.RMARequestID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodeRMARequest_1 index.
		/// </summary>
		/// <param name="_rMARequestID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.RMARequest"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.RMARequest GetByRMARequestID(System.Int32 _rMARequestID)
		{
			int count = -1;
			return GetByRMARequestID(null,_rMARequestID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeRMARequest_1 index.
		/// </summary>
		/// <param name="_rMARequestID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.RMARequest"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.RMARequest GetByRMARequestID(System.Int32 _rMARequestID, int start, int pageLength)
		{
			int count = -1;
			return GetByRMARequestID(null, _rMARequestID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeRMARequest_1 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_rMARequestID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.RMARequest"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.RMARequest GetByRMARequestID(TransactionManager transactionManager, System.Int32 _rMARequestID)
		{
			int count = -1;
			return GetByRMARequestID(transactionManager, _rMARequestID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeRMARequest_1 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_rMARequestID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.RMARequest"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.RMARequest GetByRMARequestID(TransactionManager transactionManager, System.Int32 _rMARequestID, int start, int pageLength)
		{
			int count = -1;
			return GetByRMARequestID(transactionManager, _rMARequestID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeRMARequest_1 index.
		/// </summary>
		/// <param name="_rMARequestID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.RMARequest"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.RMARequest GetByRMARequestID(System.Int32 _rMARequestID, int start, int pageLength, out int count)
		{
			return GetByRMARequestID(null, _rMARequestID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeRMARequest_1 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_rMARequestID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.RMARequest"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.RMARequest GetByRMARequestID(TransactionManager transactionManager, System.Int32 _rMARequestID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key UQ__ZNodeRMA__9ADA6BE07BE56230 index.
		/// </summary>
		/// <param name="_requestNumber"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.RMARequest"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.RMARequest GetByRequestNumber(System.String _requestNumber)
		{
			int count = -1;
			return GetByRequestNumber(null,_requestNumber, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UQ__ZNodeRMA__9ADA6BE07BE56230 index.
		/// </summary>
		/// <param name="_requestNumber"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.RMARequest"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.RMARequest GetByRequestNumber(System.String _requestNumber, int start, int pageLength)
		{
			int count = -1;
			return GetByRequestNumber(null, _requestNumber, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UQ__ZNodeRMA__9ADA6BE07BE56230 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_requestNumber"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.RMARequest"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.RMARequest GetByRequestNumber(TransactionManager transactionManager, System.String _requestNumber)
		{
			int count = -1;
			return GetByRequestNumber(transactionManager, _requestNumber, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UQ__ZNodeRMA__9ADA6BE07BE56230 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_requestNumber"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.RMARequest"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.RMARequest GetByRequestNumber(TransactionManager transactionManager, System.String _requestNumber, int start, int pageLength)
		{
			int count = -1;
			return GetByRequestNumber(transactionManager, _requestNumber, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UQ__ZNodeRMA__9ADA6BE07BE56230 index.
		/// </summary>
		/// <param name="_requestNumber"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.RMARequest"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.RMARequest GetByRequestNumber(System.String _requestNumber, int start, int pageLength, out int count)
		{
			return GetByRequestNumber(null, _requestNumber, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the UQ__ZNodeRMA__9ADA6BE07BE56230 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_requestNumber"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.RMARequest"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.RMARequest GetByRequestNumber(TransactionManager transactionManager, System.String _requestNumber, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;RMARequest&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;RMARequest&gt;"/></returns>
		public static TList<RMARequest> Fill(IDataReader reader, TList<RMARequest> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.RMARequest c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("RMARequest")
					.Append("|").Append((System.Int32)reader[((int)RMARequestColumn.RMARequestID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<RMARequest>(
					key.ToString(), // EntityTrackingKey
					"RMARequest",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.RMARequest();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.RMARequestID = (System.Int32)reader[((int)RMARequestColumn.RMARequestID - 1)];
					c.RequestDate = (System.DateTime)reader[((int)RMARequestColumn.RequestDate - 1)];
					c.Comments = (reader.IsDBNull(((int)RMARequestColumn.Comments - 1)))?null:(System.String)reader[((int)RMARequestColumn.Comments - 1)];
					c.RequestStatusID = (reader.IsDBNull(((int)RMARequestColumn.RequestStatusID - 1)))?null:(System.Int32?)reader[((int)RMARequestColumn.RequestStatusID - 1)];
					c.UpdatedDate = (reader.IsDBNull(((int)RMARequestColumn.UpdatedDate - 1)))?null:(System.DateTime?)reader[((int)RMARequestColumn.UpdatedDate - 1)];
					c.CreatedBy = (reader.IsDBNull(((int)RMARequestColumn.CreatedBy - 1)))?null:(System.String)reader[((int)RMARequestColumn.CreatedBy - 1)];
					c.RequestNumber = (reader.IsDBNull(((int)RMARequestColumn.RequestNumber - 1)))?null:(System.String)reader[((int)RMARequestColumn.RequestNumber - 1)];
					c.TaxCost = (reader.IsDBNull(((int)RMARequestColumn.TaxCost - 1)))?null:(System.Decimal?)reader[((int)RMARequestColumn.TaxCost - 1)];
					c.Discount = (reader.IsDBNull(((int)RMARequestColumn.Discount - 1)))?null:(System.Decimal?)reader[((int)RMARequestColumn.Discount - 1)];
					c.SubTotal = (reader.IsDBNull(((int)RMARequestColumn.SubTotal - 1)))?null:(System.Decimal?)reader[((int)RMARequestColumn.SubTotal - 1)];
					c.Total = (reader.IsDBNull(((int)RMARequestColumn.Total - 1)))?null:(System.Decimal?)reader[((int)RMARequestColumn.Total - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.RMARequest"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.RMARequest"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.RMARequest entity)
		{
			if (!reader.Read()) return;
			
			entity.RMARequestID = (System.Int32)reader[((int)RMARequestColumn.RMARequestID - 1)];
			entity.RequestDate = (System.DateTime)reader[((int)RMARequestColumn.RequestDate - 1)];
			entity.Comments = (reader.IsDBNull(((int)RMARequestColumn.Comments - 1)))?null:(System.String)reader[((int)RMARequestColumn.Comments - 1)];
			entity.RequestStatusID = (reader.IsDBNull(((int)RMARequestColumn.RequestStatusID - 1)))?null:(System.Int32?)reader[((int)RMARequestColumn.RequestStatusID - 1)];
			entity.UpdatedDate = (reader.IsDBNull(((int)RMARequestColumn.UpdatedDate - 1)))?null:(System.DateTime?)reader[((int)RMARequestColumn.UpdatedDate - 1)];
			entity.CreatedBy = (reader.IsDBNull(((int)RMARequestColumn.CreatedBy - 1)))?null:(System.String)reader[((int)RMARequestColumn.CreatedBy - 1)];
			entity.RequestNumber = (reader.IsDBNull(((int)RMARequestColumn.RequestNumber - 1)))?null:(System.String)reader[((int)RMARequestColumn.RequestNumber - 1)];
			entity.TaxCost = (reader.IsDBNull(((int)RMARequestColumn.TaxCost - 1)))?null:(System.Decimal?)reader[((int)RMARequestColumn.TaxCost - 1)];
			entity.Discount = (reader.IsDBNull(((int)RMARequestColumn.Discount - 1)))?null:(System.Decimal?)reader[((int)RMARequestColumn.Discount - 1)];
			entity.SubTotal = (reader.IsDBNull(((int)RMARequestColumn.SubTotal - 1)))?null:(System.Decimal?)reader[((int)RMARequestColumn.SubTotal - 1)];
			entity.Total = (reader.IsDBNull(((int)RMARequestColumn.Total - 1)))?null:(System.Decimal?)reader[((int)RMARequestColumn.Total - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.RMARequest"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.RMARequest"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.RMARequest entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.RMARequestID = (System.Int32)dataRow["RMARequestID"];
			entity.RequestDate = (System.DateTime)dataRow["RequestDate"];
			entity.Comments = Convert.IsDBNull(dataRow["Comments"]) ? null : (System.String)dataRow["Comments"];
			entity.RequestStatusID = Convert.IsDBNull(dataRow["RequestStatusID"]) ? null : (System.Int32?)dataRow["RequestStatusID"];
			entity.UpdatedDate = Convert.IsDBNull(dataRow["UpdatedDate"]) ? null : (System.DateTime?)dataRow["UpdatedDate"];
			entity.CreatedBy = Convert.IsDBNull(dataRow["CreatedBy"]) ? null : (System.String)dataRow["CreatedBy"];
			entity.RequestNumber = Convert.IsDBNull(dataRow["RequestNumber"]) ? null : (System.String)dataRow["RequestNumber"];
			entity.TaxCost = Convert.IsDBNull(dataRow["TaxCost"]) ? null : (System.Decimal?)dataRow["TaxCost"];
			entity.Discount = Convert.IsDBNull(dataRow["Discount"]) ? null : (System.Decimal?)dataRow["Discount"];
			entity.SubTotal = Convert.IsDBNull(dataRow["SubTotal"]) ? null : (System.Decimal?)dataRow["SubTotal"];
			entity.Total = Convert.IsDBNull(dataRow["Total"]) ? null : (System.Decimal?)dataRow["Total"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.RMARequest"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.RMARequest Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.RMARequest entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region RequestStatusIDSource	
			if (CanDeepLoad(entity, "RequestStatus|RequestStatusIDSource", deepLoadType, innerList) 
				&& entity.RequestStatusIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.RequestStatusID ?? (int)0);
				RequestStatus tmpEntity = EntityManager.LocateEntity<RequestStatus>(EntityLocator.ConstructKeyFromPkItems(typeof(RequestStatus), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.RequestStatusIDSource = tmpEntity;
				else
					entity.RequestStatusIDSource = DataRepository.RequestStatusProvider.GetByRequestStatusID(transactionManager, (entity.RequestStatusID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'RequestStatusIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.RequestStatusIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.RequestStatusProvider.DeepLoad(transactionManager, entity.RequestStatusIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion RequestStatusIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.RMARequest object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.RMARequest instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.RMARequest Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.RMARequest entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region RequestStatusIDSource
			if (CanDeepSave(entity, "RequestStatus|RequestStatusIDSource", deepSaveType, innerList) 
				&& entity.RequestStatusIDSource != null)
			{
				DataRepository.RequestStatusProvider.Save(transactionManager, entity.RequestStatusIDSource);
				entity.RequestStatusID = entity.RequestStatusIDSource.RequestStatusID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region RMARequestChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.RMARequest</c>
	///</summary>
	public enum RMARequestChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>RequestStatus</c> at RequestStatusIDSource
		///</summary>
		[ChildEntityType(typeof(RequestStatus))]
		RequestStatus,
	}
	
	#endregion RMARequestChildEntityTypes
	
	#region RMARequestFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;RMARequestColumn&gt;"/> class
	/// that is used exclusively with a <see cref="RMARequest"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class RMARequestFilterBuilder : SqlFilterBuilder<RMARequestColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the RMARequestFilterBuilder class.
		/// </summary>
		public RMARequestFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the RMARequestFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public RMARequestFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the RMARequestFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public RMARequestFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion RMARequestFilterBuilder
	
	#region RMARequestParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;RMARequestColumn&gt;"/> class
	/// that is used exclusively with a <see cref="RMARequest"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class RMARequestParameterBuilder : ParameterizedSqlFilterBuilder<RMARequestColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the RMARequestParameterBuilder class.
		/// </summary>
		public RMARequestParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the RMARequestParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public RMARequestParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the RMARequestParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public RMARequestParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion RMARequestParameterBuilder
	
	#region RMARequestSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;RMARequestColumn&gt;"/> class
	/// that is used exclusively with a <see cref="RMARequest"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class RMARequestSortBuilder : SqlSortBuilder<RMARequestColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the RMARequestSqlSortBuilder class.
		/// </summary>
		public RMARequestSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion RMARequestSortBuilder
	
} // end namespace
