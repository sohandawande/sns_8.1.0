﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="LuceneIndexServerStatusProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class LuceneIndexServerStatusProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.LuceneIndexServerStatus, ZNode.Libraries.DataAccess.Entities.LuceneIndexServerStatusKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.LuceneIndexServerStatusKey key)
		{
			return Delete(transactionManager, key.ID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_iD">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _iD)
		{
			return Delete(null, _iD);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_iD">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _iD);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeLuceneIndexServerStatus_LuceneIndexMonitorID key.
		///		FK_ZNodeLuceneIndexServerStatus_LuceneIndexMonitorID Description: 
		/// </summary>
		/// <param name="_luceneIndexMonitorID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.LuceneIndexServerStatus objects.</returns>
		public TList<LuceneIndexServerStatus> GetByLuceneIndexMonitorID(System.Int64 _luceneIndexMonitorID)
		{
			int count = -1;
			return GetByLuceneIndexMonitorID(_luceneIndexMonitorID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeLuceneIndexServerStatus_LuceneIndexMonitorID key.
		///		FK_ZNodeLuceneIndexServerStatus_LuceneIndexMonitorID Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_luceneIndexMonitorID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.LuceneIndexServerStatus objects.</returns>
		/// <remarks></remarks>
		public TList<LuceneIndexServerStatus> GetByLuceneIndexMonitorID(TransactionManager transactionManager, System.Int64 _luceneIndexMonitorID)
		{
			int count = -1;
			return GetByLuceneIndexMonitorID(transactionManager, _luceneIndexMonitorID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeLuceneIndexServerStatus_LuceneIndexMonitorID key.
		///		FK_ZNodeLuceneIndexServerStatus_LuceneIndexMonitorID Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_luceneIndexMonitorID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.LuceneIndexServerStatus objects.</returns>
		public TList<LuceneIndexServerStatus> GetByLuceneIndexMonitorID(TransactionManager transactionManager, System.Int64 _luceneIndexMonitorID, int start, int pageLength)
		{
			int count = -1;
			return GetByLuceneIndexMonitorID(transactionManager, _luceneIndexMonitorID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeLuceneIndexServerStatus_LuceneIndexMonitorID key.
		///		fKZNodeLuceneIndexServerStatusLuceneIndexMonitorID Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_luceneIndexMonitorID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.LuceneIndexServerStatus objects.</returns>
		public TList<LuceneIndexServerStatus> GetByLuceneIndexMonitorID(System.Int64 _luceneIndexMonitorID, int start, int pageLength)
		{
			int count =  -1;
			return GetByLuceneIndexMonitorID(null, _luceneIndexMonitorID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeLuceneIndexServerStatus_LuceneIndexMonitorID key.
		///		fKZNodeLuceneIndexServerStatusLuceneIndexMonitorID Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_luceneIndexMonitorID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.LuceneIndexServerStatus objects.</returns>
		public TList<LuceneIndexServerStatus> GetByLuceneIndexMonitorID(System.Int64 _luceneIndexMonitorID, int start, int pageLength,out int count)
		{
			return GetByLuceneIndexMonitorID(null, _luceneIndexMonitorID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeLuceneIndexServerStatus_LuceneIndexMonitorID key.
		///		FK_ZNodeLuceneIndexServerStatus_LuceneIndexMonitorID Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_luceneIndexMonitorID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.LuceneIndexServerStatus objects.</returns>
		public abstract TList<LuceneIndexServerStatus> GetByLuceneIndexMonitorID(TransactionManager transactionManager, System.Int64 _luceneIndexMonitorID, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.LuceneIndexServerStatus Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.LuceneIndexServerStatusKey key, int start, int pageLength)
		{
			return GetByID(transactionManager, key.ID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodeLuceneIndexServerStatus index.
		/// </summary>
		/// <param name="_iD"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneIndexServerStatus"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.LuceneIndexServerStatus GetByID(System.Int32 _iD)
		{
			int count = -1;
			return GetByID(null,_iD, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeLuceneIndexServerStatus index.
		/// </summary>
		/// <param name="_iD"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneIndexServerStatus"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.LuceneIndexServerStatus GetByID(System.Int32 _iD, int start, int pageLength)
		{
			int count = -1;
			return GetByID(null, _iD, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeLuceneIndexServerStatus index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_iD"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneIndexServerStatus"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.LuceneIndexServerStatus GetByID(TransactionManager transactionManager, System.Int32 _iD)
		{
			int count = -1;
			return GetByID(transactionManager, _iD, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeLuceneIndexServerStatus index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_iD"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneIndexServerStatus"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.LuceneIndexServerStatus GetByID(TransactionManager transactionManager, System.Int32 _iD, int start, int pageLength)
		{
			int count = -1;
			return GetByID(transactionManager, _iD, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeLuceneIndexServerStatus index.
		/// </summary>
		/// <param name="_iD"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneIndexServerStatus"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.LuceneIndexServerStatus GetByID(System.Int32 _iD, int start, int pageLength, out int count)
		{
			return GetByID(null, _iD, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeLuceneIndexServerStatus index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_iD"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneIndexServerStatus"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.LuceneIndexServerStatus GetByID(TransactionManager transactionManager, System.Int32 _iD, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;LuceneIndexServerStatus&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;LuceneIndexServerStatus&gt;"/></returns>
		public static TList<LuceneIndexServerStatus> Fill(IDataReader reader, TList<LuceneIndexServerStatus> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.LuceneIndexServerStatus c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("LuceneIndexServerStatus")
					.Append("|").Append((System.Int32)reader[((int)LuceneIndexServerStatusColumn.ID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<LuceneIndexServerStatus>(
					key.ToString(), // EntityTrackingKey
					"LuceneIndexServerStatus",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.LuceneIndexServerStatus();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.ID = (System.Int32)reader[((int)LuceneIndexServerStatusColumn.ID - 1)];
					c.ServerName = (System.String)reader[((int)LuceneIndexServerStatusColumn.ServerName - 1)];
					c.LuceneIndexMonitorID = (System.Int64)reader[((int)LuceneIndexServerStatusColumn.LuceneIndexMonitorID - 1)];
					c.Status = (reader.IsDBNull(((int)LuceneIndexServerStatusColumn.Status - 1)))?null:(System.Int32?)reader[((int)LuceneIndexServerStatusColumn.Status - 1)];
					c.StartTime = (reader.IsDBNull(((int)LuceneIndexServerStatusColumn.StartTime - 1)))?null:(System.DateTime?)reader[((int)LuceneIndexServerStatusColumn.StartTime - 1)];
					c.EndTime = (reader.IsDBNull(((int)LuceneIndexServerStatusColumn.EndTime - 1)))?null:(System.DateTime?)reader[((int)LuceneIndexServerStatusColumn.EndTime - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneIndexServerStatus"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.LuceneIndexServerStatus"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.LuceneIndexServerStatus entity)
		{
			if (!reader.Read()) return;
			
			entity.ID = (System.Int32)reader[((int)LuceneIndexServerStatusColumn.ID - 1)];
			entity.ServerName = (System.String)reader[((int)LuceneIndexServerStatusColumn.ServerName - 1)];
			entity.LuceneIndexMonitorID = (System.Int64)reader[((int)LuceneIndexServerStatusColumn.LuceneIndexMonitorID - 1)];
			entity.Status = (reader.IsDBNull(((int)LuceneIndexServerStatusColumn.Status - 1)))?null:(System.Int32?)reader[((int)LuceneIndexServerStatusColumn.Status - 1)];
			entity.StartTime = (reader.IsDBNull(((int)LuceneIndexServerStatusColumn.StartTime - 1)))?null:(System.DateTime?)reader[((int)LuceneIndexServerStatusColumn.StartTime - 1)];
			entity.EndTime = (reader.IsDBNull(((int)LuceneIndexServerStatusColumn.EndTime - 1)))?null:(System.DateTime?)reader[((int)LuceneIndexServerStatusColumn.EndTime - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneIndexServerStatus"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.LuceneIndexServerStatus"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.LuceneIndexServerStatus entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.ID = (System.Int32)dataRow["ID"];
			entity.ServerName = (System.String)dataRow["ServerName"];
			entity.LuceneIndexMonitorID = (System.Int64)dataRow["LuceneIndexMonitorID"];
			entity.Status = Convert.IsDBNull(dataRow["Status"]) ? null : (System.Int32?)dataRow["Status"];
			entity.StartTime = Convert.IsDBNull(dataRow["StartTime"]) ? null : (System.DateTime?)dataRow["StartTime"];
			entity.EndTime = Convert.IsDBNull(dataRow["EndTime"]) ? null : (System.DateTime?)dataRow["EndTime"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.LuceneIndexServerStatus"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.LuceneIndexServerStatus Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.LuceneIndexServerStatus entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region LuceneIndexMonitorIDSource	
			if (CanDeepLoad(entity, "LuceneIndexMonitor|LuceneIndexMonitorIDSource", deepLoadType, innerList) 
				&& entity.LuceneIndexMonitorIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.LuceneIndexMonitorID;
				LuceneIndexMonitor tmpEntity = EntityManager.LocateEntity<LuceneIndexMonitor>(EntityLocator.ConstructKeyFromPkItems(typeof(LuceneIndexMonitor), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.LuceneIndexMonitorIDSource = tmpEntity;
				else
					entity.LuceneIndexMonitorIDSource = DataRepository.LuceneIndexMonitorProvider.GetByLuceneIndexMonitorID(transactionManager, entity.LuceneIndexMonitorID);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'LuceneIndexMonitorIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.LuceneIndexMonitorIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.LuceneIndexMonitorProvider.DeepLoad(transactionManager, entity.LuceneIndexMonitorIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion LuceneIndexMonitorIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.LuceneIndexServerStatus object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.LuceneIndexServerStatus instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.LuceneIndexServerStatus Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.LuceneIndexServerStatus entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region LuceneIndexMonitorIDSource
			if (CanDeepSave(entity, "LuceneIndexMonitor|LuceneIndexMonitorIDSource", deepSaveType, innerList) 
				&& entity.LuceneIndexMonitorIDSource != null)
			{
				DataRepository.LuceneIndexMonitorProvider.Save(transactionManager, entity.LuceneIndexMonitorIDSource);
				entity.LuceneIndexMonitorID = entity.LuceneIndexMonitorIDSource.LuceneIndexMonitorID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region LuceneIndexServerStatusChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.LuceneIndexServerStatus</c>
	///</summary>
	public enum LuceneIndexServerStatusChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>LuceneIndexMonitor</c> at LuceneIndexMonitorIDSource
		///</summary>
		[ChildEntityType(typeof(LuceneIndexMonitor))]
		LuceneIndexMonitor,
	}
	
	#endregion LuceneIndexServerStatusChildEntityTypes
	
	#region LuceneIndexServerStatusFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;LuceneIndexServerStatusColumn&gt;"/> class
	/// that is used exclusively with a <see cref="LuceneIndexServerStatus"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class LuceneIndexServerStatusFilterBuilder : SqlFilterBuilder<LuceneIndexServerStatusColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the LuceneIndexServerStatusFilterBuilder class.
		/// </summary>
		public LuceneIndexServerStatusFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the LuceneIndexServerStatusFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public LuceneIndexServerStatusFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the LuceneIndexServerStatusFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public LuceneIndexServerStatusFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion LuceneIndexServerStatusFilterBuilder
	
	#region LuceneIndexServerStatusParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;LuceneIndexServerStatusColumn&gt;"/> class
	/// that is used exclusively with a <see cref="LuceneIndexServerStatus"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class LuceneIndexServerStatusParameterBuilder : ParameterizedSqlFilterBuilder<LuceneIndexServerStatusColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the LuceneIndexServerStatusParameterBuilder class.
		/// </summary>
		public LuceneIndexServerStatusParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the LuceneIndexServerStatusParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public LuceneIndexServerStatusParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the LuceneIndexServerStatusParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public LuceneIndexServerStatusParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion LuceneIndexServerStatusParameterBuilder
	
	#region LuceneIndexServerStatusSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;LuceneIndexServerStatusColumn&gt;"/> class
	/// that is used exclusively with a <see cref="LuceneIndexServerStatus"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class LuceneIndexServerStatusSortBuilder : SqlSortBuilder<LuceneIndexServerStatusColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the LuceneIndexServerStatusSqlSortBuilder class.
		/// </summary>
		public LuceneIndexServerStatusSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion LuceneIndexServerStatusSortBuilder
	
} // end namespace
