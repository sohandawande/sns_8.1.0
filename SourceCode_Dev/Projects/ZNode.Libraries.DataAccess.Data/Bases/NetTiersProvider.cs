﻿#region Using directives

using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Configuration.Provider;

using ZNode.Libraries.DataAccess.Entities;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// The base class to implements to create a .NetTiers provider.
	///</summary>
	public abstract class NetTiersProvider : NetTiersProviderBase
	{
		
		///<summary>
		/// Current SKUProfileEffectiveProviderBase instance.
		///</summary>
		public virtual SKUProfileEffectiveProviderBase SKUProfileEffectiveProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current TagsProviderBase instance.
		///</summary>
		public virtual TagsProviderBase TagsProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ProductTypeAttributeProviderBase instance.
		///</summary>
		public virtual ProductTypeAttributeProviderBase ProductTypeAttributeProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ProductImageProviderBase instance.
		///</summary>
		public virtual ProductImageProviderBase ProductImageProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current AddOnProviderBase instance.
		///</summary>
		public virtual AddOnProviderBase AddOnProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current CSSProviderBase instance.
		///</summary>
		public virtual CSSProviderBase CSSProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current PriceListProviderBase instance.
		///</summary>
		public virtual PriceListProviderBase PriceListProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ProductHighlightProviderBase instance.
		///</summary>
		public virtual ProductHighlightProviderBase ProductHighlightProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current CategoryProviderBase instance.
		///</summary>
		public virtual CategoryProviderBase CategoryProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current CaseTypeProviderBase instance.
		///</summary>
		public virtual CaseTypeProviderBase CaseTypeProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ResetPasswordProviderBase instance.
		///</summary>
		public virtual ResetPasswordProviderBase ResetPasswordProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current CategoryProfileProviderBase instance.
		///</summary>
		public virtual CategoryProfileProviderBase CategoryProfileProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current AccountProfileProviderBase instance.
		///</summary>
		public virtual AccountProfileProviderBase AccountProfileProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ProductReviewHistoryProviderBase instance.
		///</summary>
		public virtual ProductReviewHistoryProviderBase ProductReviewHistoryProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current GatewayProviderBase instance.
		///</summary>
		public virtual GatewayProviderBase GatewayProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current SavedCartLineItemProviderBase instance.
		///</summary>
		public virtual SavedCartLineItemProviderBase SavedCartLineItemProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current SKUInventoryProviderBase instance.
		///</summary>
		public virtual SKUInventoryProviderBase SKUInventoryProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current WishListProviderBase instance.
		///</summary>
		public virtual WishListProviderBase WishListProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ProductCategoryProviderBase instance.
		///</summary>
		public virtual ProductCategoryProviderBase ProductCategoryProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ContentPageProviderBase instance.
		///</summary>
		public virtual ContentPageProviderBase ContentPageProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ProductAttributeProviderBase instance.
		///</summary>
		public virtual ProductAttributeProviderBase ProductAttributeProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current PortalCatalogProviderBase instance.
		///</summary>
		public virtual PortalCatalogProviderBase PortalCatalogProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ContentPageRevisionProviderBase instance.
		///</summary>
		public virtual ContentPageRevisionProviderBase ContentPageRevisionProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current SKUAttributeProviderBase instance.
		///</summary>
		public virtual SKUAttributeProviderBase SKUAttributeProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ShippingProviderBase instance.
		///</summary>
		public virtual ShippingProviderBase ShippingProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ProductAddOnProviderBase instance.
		///</summary>
		public virtual ProductAddOnProviderBase ProductAddOnProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current HighlightProviderBase instance.
		///</summary>
		public virtual HighlightProviderBase HighlightProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current CategoryNodeProviderBase instance.
		///</summary>
		public virtual CategoryNodeProviderBase CategoryNodeProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current AddressProviderBase instance.
		///</summary>
		public virtual AddressProviderBase AddressProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ActivityLogProviderBase instance.
		///</summary>
		public virtual ActivityLogProviderBase ActivityLogProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current FacetProviderBase instance.
		///</summary>
		public virtual FacetProviderBase FacetProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current HighlightTypeProviderBase instance.
		///</summary>
		public virtual HighlightTypeProviderBase HighlightTypeProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ProductProfileProviderBase instance.
		///</summary>
		public virtual ProductProfileProviderBase ProductProfileProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current CaseRequestProviderBase instance.
		///</summary>
		public virtual CaseRequestProviderBase CaseRequestProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ReviewProviderBase instance.
		///</summary>
		public virtual ReviewProviderBase ReviewProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current TrackingOutboundProviderBase instance.
		///</summary>
		public virtual TrackingOutboundProviderBase TrackingOutboundProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ActivityLogTypeProviderBase instance.
		///</summary>
		public virtual ActivityLogTypeProviderBase ActivityLogTypeProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current PortalProfileProviderBase instance.
		///</summary>
		public virtual PortalProfileProviderBase PortalProfileProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current FacetGroupProviderBase instance.
		///</summary>
		public virtual FacetGroupProviderBase FacetGroupProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current LuceneDocumentMappingProviderBase instance.
		///</summary>
		public virtual LuceneDocumentMappingProviderBase LuceneDocumentMappingProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current FacetControlTypeProviderBase instance.
		///</summary>
		public virtual FacetControlTypeProviderBase FacetControlTypeProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current DomainProviderBase instance.
		///</summary>
		public virtual DomainProviderBase DomainProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current FacetProductSKUProviderBase instance.
		///</summary>
		public virtual FacetProductSKUProviderBase FacetProductSKUProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current DiscountTypeProviderBase instance.
		///</summary>
		public virtual DiscountTypeProviderBase DiscountTypeProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current PortalCountryProviderBase instance.
		///</summary>
		public virtual PortalCountryProviderBase PortalCountryProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ZipCodeProviderBase instance.
		///</summary>
		public virtual ZipCodeProviderBase ZipCodeProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current SKUProfileProviderBase instance.
		///</summary>
		public virtual SKUProfileProviderBase SKUProfileProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current TrackingProviderBase instance.
		///</summary>
		public virtual TrackingProviderBase TrackingProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current LuceneGlobalProductBoostProviderBase instance.
		///</summary>
		public virtual LuceneGlobalProductBoostProviderBase LuceneGlobalProductBoostProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current CasePriorityProviderBase instance.
		///</summary>
		public virtual CasePriorityProviderBase CasePriorityProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current AddOnValueProviderBase instance.
		///</summary>
		public virtual AddOnValueProviderBase AddOnValueProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current PromotionProviderBase instance.
		///</summary>
		public virtual PromotionProviderBase PromotionProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ProductTierProviderBase instance.
		///</summary>
		public virtual ProductTierProviderBase ProductTierProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current TaxRuleProviderBase instance.
		///</summary>
		public virtual TaxRuleProviderBase TaxRuleProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ShippingRuleProviderBase instance.
		///</summary>
		public virtual ShippingRuleProviderBase ShippingRuleProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current LuceneGlobalProductCategoryBoostProviderBase instance.
		///</summary>
		public virtual LuceneGlobalProductCategoryBoostProviderBase LuceneGlobalProductCategoryBoostProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current SavedCartProviderBase instance.
		///</summary>
		public virtual SavedCartProviderBase SavedCartProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ProductCrossSellProviderBase instance.
		///</summary>
		public virtual ProductCrossSellProviderBase ProductCrossSellProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current LuceneGlobalProductBrandBoostProviderBase instance.
		///</summary>
		public virtual LuceneGlobalProductBrandBoostProviderBase LuceneGlobalProductBrandBoostProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current TaxRuleTypeProviderBase instance.
		///</summary>
		public virtual TaxRuleTypeProviderBase TaxRuleTypeProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current FacetGroupCategoryProviderBase instance.
		///</summary>
		public virtual FacetGroupCategoryProviderBase FacetGroupCategoryProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current AttributeTypeProviderBase instance.
		///</summary>
		public virtual AttributeTypeProviderBase AttributeTypeProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current NoteProviderBase instance.
		///</summary>
		public virtual NoteProviderBase NoteProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current TrackingEventProviderBase instance.
		///</summary>
		public virtual TrackingEventProviderBase TrackingEventProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current LuceneGlobalProductCatalogBoostProviderBase instance.
		///</summary>
		public virtual LuceneGlobalProductCatalogBoostProviderBase LuceneGlobalProductCatalogBoostProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current OrderStateProviderBase instance.
		///</summary>
		public virtual OrderStateProviderBase OrderStateProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current OrderProcessingTypeProviderBase instance.
		///</summary>
		public virtual OrderProcessingTypeProviderBase OrderProcessingTypeProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current StoreProviderBase instance.
		///</summary>
		public virtual StoreProviderBase StoreProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current LocaleProviderBase instance.
		///</summary>
		public virtual LocaleProviderBase LocaleProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ProductReviewStateProviderBase instance.
		///</summary>
		public virtual ProductReviewStateProviderBase ProductReviewStateProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current AccountTypeProviderBase instance.
		///</summary>
		public virtual AccountTypeProviderBase AccountTypeProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ShippingTypeProviderBase instance.
		///</summary>
		public virtual ShippingTypeProviderBase ShippingTypeProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current StateProviderBase instance.
		///</summary>
		public virtual StateProviderBase StateProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current CurrencyTypeProviderBase instance.
		///</summary>
		public virtual CurrencyTypeProviderBase CurrencyTypeProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ProductRelationTypeProviderBase instance.
		///</summary>
		public virtual ProductRelationTypeProviderBase ProductRelationTypeProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ShippingRuleTypeProviderBase instance.
		///</summary>
		public virtual ShippingRuleTypeProviderBase ShippingRuleTypeProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current PortalProviderBase instance.
		///</summary>
		public virtual PortalProviderBase PortalProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ProductExtensionOptionProductProviderBase instance.
		///</summary>
		public virtual ProductExtensionOptionProductProviderBase ProductExtensionOptionProductProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ReferralCommissionTypeProviderBase instance.
		///</summary>
		public virtual ReferralCommissionTypeProviderBase ReferralCommissionTypeProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current TaxClassProviderBase instance.
		///</summary>
		public virtual TaxClassProviderBase TaxClassProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ProductExtensionRelationProviderBase instance.
		///</summary>
		public virtual ProductExtensionRelationProviderBase ProductExtensionRelationProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ProfileProviderBase instance.
		///</summary>
		public virtual ProfileProviderBase ProfileProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current RMARequestProviderBase instance.
		///</summary>
		public virtual RMARequestProviderBase RMARequestProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current PaymentSettingProviderBase instance.
		///</summary>
		public virtual PaymentSettingProviderBase PaymentSettingProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current OrderShipmentProviderBase instance.
		///</summary>
		public virtual OrderShipmentProviderBase OrderShipmentProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ProductExtensionOptionProviderBase instance.
		///</summary>
		public virtual ProductExtensionOptionProviderBase ProductExtensionOptionProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current OrderLineItemProviderBase instance.
		///</summary>
		public virtual OrderLineItemProviderBase OrderLineItemProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ReasonForReturnProviderBase instance.
		///</summary>
		public virtual ReasonForReturnProviderBase ReasonForReturnProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current SourceModificationAuditProviderBase instance.
		///</summary>
		public virtual SourceModificationAuditProviderBase SourceModificationAuditProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current UserDefinedViewProviderBase instance.
		///</summary>
		public virtual UserDefinedViewProviderBase UserDefinedViewProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current RMARequestItemProviderBase instance.
		///</summary>
		public virtual RMARequestItemProviderBase RMARequestItemProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current WorkflowProviderBase instance.
		///</summary>
		public virtual WorkflowProviderBase WorkflowProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current CustomerPricingProviderBase instance.
		///</summary>
		public virtual CustomerPricingProviderBase CustomerPricingProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current SourceModificationTypeProviderBase instance.
		///</summary>
		public virtual SourceModificationTypeProviderBase SourceModificationTypeProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ApplicationSettingProviderBase instance.
		///</summary>
		public virtual ApplicationSettingProviderBase ApplicationSettingProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current PaymentTokenProviderBase instance.
		///</summary>
		public virtual PaymentTokenProviderBase PaymentTokenProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current RequestStatusProviderBase instance.
		///</summary>
		public virtual RequestStatusProviderBase RequestStatusProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current UrlRedirectProviderBase instance.
		///</summary>
		public virtual UrlRedirectProviderBase UrlRedirectProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current OrderDetailProductExtensionOptionProviderBase instance.
		///</summary>
		public virtual OrderDetailProductExtensionOptionProviderBase OrderDetailProductExtensionOptionProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current AccountProviderBase instance.
		///</summary>
		public virtual AccountProviderBase AccountProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ThemeProviderBase instance.
		///</summary>
		public virtual ThemeProviderBase ThemeProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current OrderLineItemRelationshipTypeProviderBase instance.
		///</summary>
		public virtual OrderLineItemRelationshipTypeProviderBase OrderLineItemRelationshipTypeProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current SourceModificationStatusProviderBase instance.
		///</summary>
		public virtual SourceModificationStatusProviderBase SourceModificationStatusProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current LuceneIndexStatusNamesProviderBase instance.
		///</summary>
		public virtual LuceneIndexStatusNamesProviderBase LuceneIndexStatusNamesProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current DigitalAssetProviderBase instance.
		///</summary>
		public virtual DigitalAssetProviderBase DigitalAssetProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current OrderProviderBase instance.
		///</summary>
		public virtual OrderProviderBase OrderProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current SourceTypeProviderBase instance.
		///</summary>
		public virtual SourceTypeProviderBase SourceTypeProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current PasswordLogProviderBase instance.
		///</summary>
		public virtual PasswordLogProviderBase PasswordLogProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current LuceneIndexMonitorProviderBase instance.
		///</summary>
		public virtual LuceneIndexMonitorProviderBase LuceneIndexMonitorProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current MultifrontProviderBase instance.
		///</summary>
		public virtual MultifrontProviderBase MultifrontProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current GiftCardProviderBase instance.
		///</summary>
		public virtual GiftCardProviderBase GiftCardProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current GiftCardHistoryProviderBase instance.
		///</summary>
		public virtual GiftCardHistoryProviderBase GiftCardHistoryProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current MessageTypeProviderBase instance.
		///</summary>
		public virtual MessageTypeProviderBase MessageTypeProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current MessageConfigProviderBase instance.
		///</summary>
		public virtual MessageConfigProviderBase MessageConfigProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ProductTypeProviderBase instance.
		///</summary>
		public virtual ProductTypeProviderBase ProductTypeProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current CatalogProviderBase instance.
		///</summary>
		public virtual CatalogProviderBase CatalogProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ManufacturerProviderBase instance.
		///</summary>
		public virtual ManufacturerProviderBase ManufacturerProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current SupplierProviderBase instance.
		///</summary>
		public virtual SupplierProviderBase SupplierProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ProductProviderBase instance.
		///</summary>
		public virtual ProductProviderBase ProductProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current PaymentStatusProviderBase instance.
		///</summary>
		public virtual PaymentStatusProviderBase PaymentStatusProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current SKUProviderBase instance.
		///</summary>
		public virtual SKUProviderBase SKUProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current CookieMappingProviderBase instance.
		///</summary>
		public virtual CookieMappingProviderBase CookieMappingProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ReferralCommissionProviderBase instance.
		///</summary>
		public virtual ReferralCommissionProviderBase ReferralCommissionProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current MasterPageProviderBase instance.
		///</summary>
		public virtual MasterPageProviderBase MasterPageProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current CountryProviderBase instance.
		///</summary>
		public virtual CountryProviderBase CountryProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current SupplierTypeProviderBase instance.
		///</summary>
		public virtual SupplierTypeProviderBase SupplierTypeProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current CaseStatusProviderBase instance.
		///</summary>
		public virtual CaseStatusProviderBase CaseStatusProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ShippingServiceCodeProviderBase instance.
		///</summary>
		public virtual ShippingServiceCodeProviderBase ShippingServiceCodeProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current PaymentTokenAuthorizeProviderBase instance.
		///</summary>
		public virtual PaymentTokenAuthorizeProviderBase PaymentTokenAuthorizeProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current RMAConfigurationProviderBase instance.
		///</summary>
		public virtual RMAConfigurationProviderBase RMAConfigurationProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current LuceneServerConfigurationStatusProviderBase instance.
		///</summary>
		public virtual LuceneServerConfigurationStatusProviderBase LuceneServerConfigurationStatusProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current PortalWorkflowProviderBase instance.
		///</summary>
		public virtual PortalWorkflowProviderBase PortalWorkflowProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current AccountPaymentProviderBase instance.
		///</summary>
		public virtual AccountPaymentProviderBase AccountPaymentProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ProductImageTypeProviderBase instance.
		///</summary>
		public virtual ProductImageTypeProviderBase ProductImageTypeProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ProductExtensionProviderBase instance.
		///</summary>
		public virtual ProductExtensionProviderBase ProductExtensionProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current SavedCartLineItemProductExtensionOptionProviderBase instance.
		///</summary>
		public virtual SavedCartLineItemProductExtensionOptionProviderBase SavedCartLineItemProductExtensionOptionProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current PaymentTypeProviderBase instance.
		///</summary>
		public virtual PaymentTypeProviderBase PaymentTypeProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ParentChildProductProviderBase instance.
		///</summary>
		public virtual ParentChildProductProviderBase ParentChildProductProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current LuceneIndexServerStatusProviderBase instance.
		///</summary>
		public virtual LuceneIndexServerStatusProviderBase LuceneIndexServerStatusProvider{get {throw new NotImplementedException();}}
		
		
		///<summary>
		/// Current VwZnodeCategoriesCatalogsProviderBase instance.
		///</summary>
		public virtual VwZnodeCategoriesCatalogsProviderBase VwZnodeCategoriesCatalogsProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current VwZnodeCategoriesPortalsProviderBase instance.
		///</summary>
		public virtual VwZnodeCategoriesPortalsProviderBase VwZnodeCategoriesPortalsProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current VwZnodeProductsCatalogsProviderBase instance.
		///</summary>
		public virtual VwZnodeProductsCatalogsProviderBase VwZnodeProductsCatalogsProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current VwZnodeProductsCategoriesProviderBase instance.
		///</summary>
		public virtual VwZnodeProductsCategoriesProviderBase VwZnodeProductsCategoriesProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current VwZnodeProductsCategoriesPromotionsProviderBase instance.
		///</summary>
		public virtual VwZnodeProductsCategoriesPromotionsProviderBase VwZnodeProductsCategoriesPromotionsProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current VwZnodeProductsPortalsProviderBase instance.
		///</summary>
		public virtual VwZnodeProductsPortalsProviderBase VwZnodeProductsPortalsProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current VwZnodeProductsPromotionsProviderBase instance.
		///</summary>
		public virtual VwZnodeProductsPromotionsProviderBase VwZnodeProductsPromotionsProvider{get {throw new NotImplementedException();}}
		
	}
}
