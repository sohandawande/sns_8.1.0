﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="CategoryProfileProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class CategoryProfileProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.CategoryProfile, ZNode.Libraries.DataAccess.Entities.CategoryProfileKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.CategoryProfileKey key)
		{
			return Delete(transactionManager, key.CategoryProfileID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_categoryProfileID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _categoryProfileID)
		{
			return Delete(null, _categoryProfileID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_categoryProfileID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _categoryProfileID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeCategoryProfile_ZNodeCategory key.
		///		FK_ZNodeCategoryProfile_ZNodeCategory Description: 
		/// </summary>
		/// <param name="_categoryID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.CategoryProfile objects.</returns>
		public TList<CategoryProfile> GetByCategoryID(System.Int32 _categoryID)
		{
			int count = -1;
			return GetByCategoryID(_categoryID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeCategoryProfile_ZNodeCategory key.
		///		FK_ZNodeCategoryProfile_ZNodeCategory Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_categoryID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.CategoryProfile objects.</returns>
		/// <remarks></remarks>
		public TList<CategoryProfile> GetByCategoryID(TransactionManager transactionManager, System.Int32 _categoryID)
		{
			int count = -1;
			return GetByCategoryID(transactionManager, _categoryID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeCategoryProfile_ZNodeCategory key.
		///		FK_ZNodeCategoryProfile_ZNodeCategory Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_categoryID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.CategoryProfile objects.</returns>
		public TList<CategoryProfile> GetByCategoryID(TransactionManager transactionManager, System.Int32 _categoryID, int start, int pageLength)
		{
			int count = -1;
			return GetByCategoryID(transactionManager, _categoryID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeCategoryProfile_ZNodeCategory key.
		///		fKZNodeCategoryProfileZNodeCategory Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_categoryID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.CategoryProfile objects.</returns>
		public TList<CategoryProfile> GetByCategoryID(System.Int32 _categoryID, int start, int pageLength)
		{
			int count =  -1;
			return GetByCategoryID(null, _categoryID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeCategoryProfile_ZNodeCategory key.
		///		fKZNodeCategoryProfileZNodeCategory Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_categoryID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.CategoryProfile objects.</returns>
		public TList<CategoryProfile> GetByCategoryID(System.Int32 _categoryID, int start, int pageLength,out int count)
		{
			return GetByCategoryID(null, _categoryID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeCategoryProfile_ZNodeCategory key.
		///		FK_ZNodeCategoryProfile_ZNodeCategory Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_categoryID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.CategoryProfile objects.</returns>
		public abstract TList<CategoryProfile> GetByCategoryID(TransactionManager transactionManager, System.Int32 _categoryID, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeCategoryProfile_ZNodeProfile key.
		///		FK_ZNodeCategoryProfile_ZNodeProfile Description: 
		/// </summary>
		/// <param name="_profileID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.CategoryProfile objects.</returns>
		public TList<CategoryProfile> GetByProfileID(System.Int32 _profileID)
		{
			int count = -1;
			return GetByProfileID(_profileID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeCategoryProfile_ZNodeProfile key.
		///		FK_ZNodeCategoryProfile_ZNodeProfile Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_profileID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.CategoryProfile objects.</returns>
		/// <remarks></remarks>
		public TList<CategoryProfile> GetByProfileID(TransactionManager transactionManager, System.Int32 _profileID)
		{
			int count = -1;
			return GetByProfileID(transactionManager, _profileID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeCategoryProfile_ZNodeProfile key.
		///		FK_ZNodeCategoryProfile_ZNodeProfile Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_profileID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.CategoryProfile objects.</returns>
		public TList<CategoryProfile> GetByProfileID(TransactionManager transactionManager, System.Int32 _profileID, int start, int pageLength)
		{
			int count = -1;
			return GetByProfileID(transactionManager, _profileID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeCategoryProfile_ZNodeProfile key.
		///		fKZNodeCategoryProfileZNodeProfile Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_profileID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.CategoryProfile objects.</returns>
		public TList<CategoryProfile> GetByProfileID(System.Int32 _profileID, int start, int pageLength)
		{
			int count =  -1;
			return GetByProfileID(null, _profileID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeCategoryProfile_ZNodeProfile key.
		///		fKZNodeCategoryProfileZNodeProfile Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_profileID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.CategoryProfile objects.</returns>
		public TList<CategoryProfile> GetByProfileID(System.Int32 _profileID, int start, int pageLength,out int count)
		{
			return GetByProfileID(null, _profileID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeCategoryProfile_ZNodeProfile key.
		///		FK_ZNodeCategoryProfile_ZNodeProfile Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_profileID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.CategoryProfile objects.</returns>
		public abstract TList<CategoryProfile> GetByProfileID(TransactionManager transactionManager, System.Int32 _profileID, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.CategoryProfile Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.CategoryProfileKey key, int start, int pageLength)
		{
			return GetByCategoryProfileID(transactionManager, key.CategoryProfileID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodeCategoryProfile index.
		/// </summary>
		/// <param name="_categoryProfileID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.CategoryProfile"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.CategoryProfile GetByCategoryProfileID(System.Int32 _categoryProfileID)
		{
			int count = -1;
			return GetByCategoryProfileID(null,_categoryProfileID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeCategoryProfile index.
		/// </summary>
		/// <param name="_categoryProfileID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.CategoryProfile"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.CategoryProfile GetByCategoryProfileID(System.Int32 _categoryProfileID, int start, int pageLength)
		{
			int count = -1;
			return GetByCategoryProfileID(null, _categoryProfileID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeCategoryProfile index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_categoryProfileID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.CategoryProfile"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.CategoryProfile GetByCategoryProfileID(TransactionManager transactionManager, System.Int32 _categoryProfileID)
		{
			int count = -1;
			return GetByCategoryProfileID(transactionManager, _categoryProfileID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeCategoryProfile index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_categoryProfileID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.CategoryProfile"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.CategoryProfile GetByCategoryProfileID(TransactionManager transactionManager, System.Int32 _categoryProfileID, int start, int pageLength)
		{
			int count = -1;
			return GetByCategoryProfileID(transactionManager, _categoryProfileID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeCategoryProfile index.
		/// </summary>
		/// <param name="_categoryProfileID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.CategoryProfile"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.CategoryProfile GetByCategoryProfileID(System.Int32 _categoryProfileID, int start, int pageLength, out int count)
		{
			return GetByCategoryProfileID(null, _categoryProfileID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeCategoryProfile index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_categoryProfileID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.CategoryProfile"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.CategoryProfile GetByCategoryProfileID(TransactionManager transactionManager, System.Int32 _categoryProfileID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;CategoryProfile&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;CategoryProfile&gt;"/></returns>
		public static TList<CategoryProfile> Fill(IDataReader reader, TList<CategoryProfile> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.CategoryProfile c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("CategoryProfile")
					.Append("|").Append((System.Int32)reader[((int)CategoryProfileColumn.CategoryProfileID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<CategoryProfile>(
					key.ToString(), // EntityTrackingKey
					"CategoryProfile",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.CategoryProfile();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.CategoryProfileID = (System.Int32)reader[((int)CategoryProfileColumn.CategoryProfileID - 1)];
					c.CategoryID = (System.Int32)reader[((int)CategoryProfileColumn.CategoryID - 1)];
					c.ProfileID = (System.Int32)reader[((int)CategoryProfileColumn.ProfileID - 1)];
					c.EffectiveDate = (System.DateTime)reader[((int)CategoryProfileColumn.EffectiveDate - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.CategoryProfile"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.CategoryProfile"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.CategoryProfile entity)
		{
			if (!reader.Read()) return;
			
			entity.CategoryProfileID = (System.Int32)reader[((int)CategoryProfileColumn.CategoryProfileID - 1)];
			entity.CategoryID = (System.Int32)reader[((int)CategoryProfileColumn.CategoryID - 1)];
			entity.ProfileID = (System.Int32)reader[((int)CategoryProfileColumn.ProfileID - 1)];
			entity.EffectiveDate = (System.DateTime)reader[((int)CategoryProfileColumn.EffectiveDate - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.CategoryProfile"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.CategoryProfile"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.CategoryProfile entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.CategoryProfileID = (System.Int32)dataRow["CategoryProfileID"];
			entity.CategoryID = (System.Int32)dataRow["CategoryID"];
			entity.ProfileID = (System.Int32)dataRow["ProfileID"];
			entity.EffectiveDate = (System.DateTime)dataRow["EffectiveDate"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.CategoryProfile"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.CategoryProfile Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.CategoryProfile entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region CategoryIDSource	
			if (CanDeepLoad(entity, "Category|CategoryIDSource", deepLoadType, innerList) 
				&& entity.CategoryIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.CategoryID;
				Category tmpEntity = EntityManager.LocateEntity<Category>(EntityLocator.ConstructKeyFromPkItems(typeof(Category), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CategoryIDSource = tmpEntity;
				else
					entity.CategoryIDSource = DataRepository.CategoryProvider.GetByCategoryID(transactionManager, entity.CategoryID);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CategoryIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CategoryIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CategoryProvider.DeepLoad(transactionManager, entity.CategoryIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CategoryIDSource

			#region ProfileIDSource	
			if (CanDeepLoad(entity, "Profile|ProfileIDSource", deepLoadType, innerList) 
				&& entity.ProfileIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.ProfileID;
				Profile tmpEntity = EntityManager.LocateEntity<Profile>(EntityLocator.ConstructKeyFromPkItems(typeof(Profile), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ProfileIDSource = tmpEntity;
				else
					entity.ProfileIDSource = DataRepository.ProfileProvider.GetByProfileID(transactionManager, entity.ProfileID);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ProfileIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ProfileIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.ProfileProvider.DeepLoad(transactionManager, entity.ProfileIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ProfileIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.CategoryProfile object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.CategoryProfile instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.CategoryProfile Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.CategoryProfile entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region CategoryIDSource
			if (CanDeepSave(entity, "Category|CategoryIDSource", deepSaveType, innerList) 
				&& entity.CategoryIDSource != null)
			{
				DataRepository.CategoryProvider.Save(transactionManager, entity.CategoryIDSource);
				entity.CategoryID = entity.CategoryIDSource.CategoryID;
			}
			#endregion 
			
			#region ProfileIDSource
			if (CanDeepSave(entity, "Profile|ProfileIDSource", deepSaveType, innerList) 
				&& entity.ProfileIDSource != null)
			{
				DataRepository.ProfileProvider.Save(transactionManager, entity.ProfileIDSource);
				entity.ProfileID = entity.ProfileIDSource.ProfileID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region CategoryProfileChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.CategoryProfile</c>
	///</summary>
	public enum CategoryProfileChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Category</c> at CategoryIDSource
		///</summary>
		[ChildEntityType(typeof(Category))]
		Category,
		
		///<summary>
		/// Composite Property for <c>Profile</c> at ProfileIDSource
		///</summary>
		[ChildEntityType(typeof(Profile))]
		Profile,
	}
	
	#endregion CategoryProfileChildEntityTypes
	
	#region CategoryProfileFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;CategoryProfileColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CategoryProfile"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CategoryProfileFilterBuilder : SqlFilterBuilder<CategoryProfileColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CategoryProfileFilterBuilder class.
		/// </summary>
		public CategoryProfileFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CategoryProfileFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CategoryProfileFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CategoryProfileFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CategoryProfileFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CategoryProfileFilterBuilder
	
	#region CategoryProfileParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;CategoryProfileColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CategoryProfile"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CategoryProfileParameterBuilder : ParameterizedSqlFilterBuilder<CategoryProfileColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CategoryProfileParameterBuilder class.
		/// </summary>
		public CategoryProfileParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CategoryProfileParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CategoryProfileParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CategoryProfileParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CategoryProfileParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CategoryProfileParameterBuilder
	
	#region CategoryProfileSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;CategoryProfileColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CategoryProfile"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class CategoryProfileSortBuilder : SqlSortBuilder<CategoryProfileColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CategoryProfileSqlSortBuilder class.
		/// </summary>
		public CategoryProfileSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion CategoryProfileSortBuilder
	
} // end namespace
