﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="LuceneGlobalProductCategoryBoostProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class LuceneGlobalProductCategoryBoostProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductCategoryBoost, ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductCategoryBoostKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductCategoryBoostKey key)
		{
			return Delete(transactionManager, key.LuceneGlobalProductCategoryBoostID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_luceneGlobalProductCategoryBoostID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _luceneGlobalProductCategoryBoostID)
		{
			return Delete(null, _luceneGlobalProductCategoryBoostID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_luceneGlobalProductCategoryBoostID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _luceneGlobalProductCategoryBoostID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductCategoryBoost Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductCategoryBoostKey key, int start, int pageLength)
		{
			return GetByLuceneGlobalProductCategoryBoostID(transactionManager, key.LuceneGlobalProductCategoryBoostID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeLuceneGlobalProductCategoryBoost index.
		/// </summary>
		/// <param name="_productCategoryID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductCategoryBoost"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductCategoryBoost GetByProductCategoryID(System.Int32 _productCategoryID)
		{
			int count = -1;
			return GetByProductCategoryID(null,_productCategoryID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeLuceneGlobalProductCategoryBoost index.
		/// </summary>
		/// <param name="_productCategoryID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductCategoryBoost"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductCategoryBoost GetByProductCategoryID(System.Int32 _productCategoryID, int start, int pageLength)
		{
			int count = -1;
			return GetByProductCategoryID(null, _productCategoryID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeLuceneGlobalProductCategoryBoost index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productCategoryID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductCategoryBoost"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductCategoryBoost GetByProductCategoryID(TransactionManager transactionManager, System.Int32 _productCategoryID)
		{
			int count = -1;
			return GetByProductCategoryID(transactionManager, _productCategoryID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeLuceneGlobalProductCategoryBoost index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productCategoryID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductCategoryBoost"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductCategoryBoost GetByProductCategoryID(TransactionManager transactionManager, System.Int32 _productCategoryID, int start, int pageLength)
		{
			int count = -1;
			return GetByProductCategoryID(transactionManager, _productCategoryID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeLuceneGlobalProductCategoryBoost index.
		/// </summary>
		/// <param name="_productCategoryID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductCategoryBoost"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductCategoryBoost GetByProductCategoryID(System.Int32 _productCategoryID, int start, int pageLength, out int count)
		{
			return GetByProductCategoryID(null, _productCategoryID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeLuceneGlobalProductCategoryBoost index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productCategoryID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductCategoryBoost"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductCategoryBoost GetByProductCategoryID(TransactionManager transactionManager, System.Int32 _productCategoryID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodeProductCategoryBoost index.
		/// </summary>
		/// <param name="_luceneGlobalProductCategoryBoostID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductCategoryBoost"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductCategoryBoost GetByLuceneGlobalProductCategoryBoostID(System.Int32 _luceneGlobalProductCategoryBoostID)
		{
			int count = -1;
			return GetByLuceneGlobalProductCategoryBoostID(null,_luceneGlobalProductCategoryBoostID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductCategoryBoost index.
		/// </summary>
		/// <param name="_luceneGlobalProductCategoryBoostID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductCategoryBoost"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductCategoryBoost GetByLuceneGlobalProductCategoryBoostID(System.Int32 _luceneGlobalProductCategoryBoostID, int start, int pageLength)
		{
			int count = -1;
			return GetByLuceneGlobalProductCategoryBoostID(null, _luceneGlobalProductCategoryBoostID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductCategoryBoost index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_luceneGlobalProductCategoryBoostID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductCategoryBoost"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductCategoryBoost GetByLuceneGlobalProductCategoryBoostID(TransactionManager transactionManager, System.Int32 _luceneGlobalProductCategoryBoostID)
		{
			int count = -1;
			return GetByLuceneGlobalProductCategoryBoostID(transactionManager, _luceneGlobalProductCategoryBoostID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductCategoryBoost index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_luceneGlobalProductCategoryBoostID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductCategoryBoost"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductCategoryBoost GetByLuceneGlobalProductCategoryBoostID(TransactionManager transactionManager, System.Int32 _luceneGlobalProductCategoryBoostID, int start, int pageLength)
		{
			int count = -1;
			return GetByLuceneGlobalProductCategoryBoostID(transactionManager, _luceneGlobalProductCategoryBoostID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductCategoryBoost index.
		/// </summary>
		/// <param name="_luceneGlobalProductCategoryBoostID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductCategoryBoost"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductCategoryBoost GetByLuceneGlobalProductCategoryBoostID(System.Int32 _luceneGlobalProductCategoryBoostID, int start, int pageLength, out int count)
		{
			return GetByLuceneGlobalProductCategoryBoostID(null, _luceneGlobalProductCategoryBoostID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductCategoryBoost index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_luceneGlobalProductCategoryBoostID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductCategoryBoost"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductCategoryBoost GetByLuceneGlobalProductCategoryBoostID(TransactionManager transactionManager, System.Int32 _luceneGlobalProductCategoryBoostID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;LuceneGlobalProductCategoryBoost&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;LuceneGlobalProductCategoryBoost&gt;"/></returns>
		public static TList<LuceneGlobalProductCategoryBoost> Fill(IDataReader reader, TList<LuceneGlobalProductCategoryBoost> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductCategoryBoost c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("LuceneGlobalProductCategoryBoost")
					.Append("|").Append((System.Int32)reader[((int)LuceneGlobalProductCategoryBoostColumn.LuceneGlobalProductCategoryBoostID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<LuceneGlobalProductCategoryBoost>(
					key.ToString(), // EntityTrackingKey
					"LuceneGlobalProductCategoryBoost",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductCategoryBoost();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.LuceneGlobalProductCategoryBoostID = (System.Int32)reader[((int)LuceneGlobalProductCategoryBoostColumn.LuceneGlobalProductCategoryBoostID - 1)];
					c.ProductCategoryID = (System.Int32)reader[((int)LuceneGlobalProductCategoryBoostColumn.ProductCategoryID - 1)];
					c.Boost = (System.Double)reader[((int)LuceneGlobalProductCategoryBoostColumn.Boost - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductCategoryBoost"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductCategoryBoost"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductCategoryBoost entity)
		{
			if (!reader.Read()) return;
			
			entity.LuceneGlobalProductCategoryBoostID = (System.Int32)reader[((int)LuceneGlobalProductCategoryBoostColumn.LuceneGlobalProductCategoryBoostID - 1)];
			entity.ProductCategoryID = (System.Int32)reader[((int)LuceneGlobalProductCategoryBoostColumn.ProductCategoryID - 1)];
			entity.Boost = (System.Double)reader[((int)LuceneGlobalProductCategoryBoostColumn.Boost - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductCategoryBoost"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductCategoryBoost"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductCategoryBoost entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.LuceneGlobalProductCategoryBoostID = (System.Int32)dataRow["LuceneGlobalProductCategoryBoostID"];
			entity.ProductCategoryID = (System.Int32)dataRow["ProductCategoryID"];
			entity.Boost = (System.Double)dataRow["Boost"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductCategoryBoost"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductCategoryBoost Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductCategoryBoost entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region ProductCategoryIDSource	
			if (CanDeepLoad(entity, "ProductCategory|ProductCategoryIDSource", deepLoadType, innerList) 
				&& entity.ProductCategoryIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.ProductCategoryID;
				ProductCategory tmpEntity = EntityManager.LocateEntity<ProductCategory>(EntityLocator.ConstructKeyFromPkItems(typeof(ProductCategory), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ProductCategoryIDSource = tmpEntity;
				else
					entity.ProductCategoryIDSource = DataRepository.ProductCategoryProvider.GetByProductCategoryID(transactionManager, entity.ProductCategoryID);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ProductCategoryIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ProductCategoryIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.ProductCategoryProvider.DeepLoad(transactionManager, entity.ProductCategoryIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ProductCategoryIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductCategoryBoost object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductCategoryBoost instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductCategoryBoost Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductCategoryBoost entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region ProductCategoryIDSource
			if (CanDeepSave(entity, "ProductCategory|ProductCategoryIDSource", deepSaveType, innerList) 
				&& entity.ProductCategoryIDSource != null)
			{
				DataRepository.ProductCategoryProvider.Save(transactionManager, entity.ProductCategoryIDSource);
				entity.ProductCategoryID = entity.ProductCategoryIDSource.ProductCategoryID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region LuceneGlobalProductCategoryBoostChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductCategoryBoost</c>
	///</summary>
	public enum LuceneGlobalProductCategoryBoostChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>ProductCategory</c> at ProductCategoryIDSource
		///</summary>
		[ChildEntityType(typeof(ProductCategory))]
		ProductCategory,
	}
	
	#endregion LuceneGlobalProductCategoryBoostChildEntityTypes
	
	#region LuceneGlobalProductCategoryBoostFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;LuceneGlobalProductCategoryBoostColumn&gt;"/> class
	/// that is used exclusively with a <see cref="LuceneGlobalProductCategoryBoost"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class LuceneGlobalProductCategoryBoostFilterBuilder : SqlFilterBuilder<LuceneGlobalProductCategoryBoostColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the LuceneGlobalProductCategoryBoostFilterBuilder class.
		/// </summary>
		public LuceneGlobalProductCategoryBoostFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the LuceneGlobalProductCategoryBoostFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public LuceneGlobalProductCategoryBoostFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the LuceneGlobalProductCategoryBoostFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public LuceneGlobalProductCategoryBoostFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion LuceneGlobalProductCategoryBoostFilterBuilder
	
	#region LuceneGlobalProductCategoryBoostParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;LuceneGlobalProductCategoryBoostColumn&gt;"/> class
	/// that is used exclusively with a <see cref="LuceneGlobalProductCategoryBoost"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class LuceneGlobalProductCategoryBoostParameterBuilder : ParameterizedSqlFilterBuilder<LuceneGlobalProductCategoryBoostColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the LuceneGlobalProductCategoryBoostParameterBuilder class.
		/// </summary>
		public LuceneGlobalProductCategoryBoostParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the LuceneGlobalProductCategoryBoostParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public LuceneGlobalProductCategoryBoostParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the LuceneGlobalProductCategoryBoostParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public LuceneGlobalProductCategoryBoostParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion LuceneGlobalProductCategoryBoostParameterBuilder
	
	#region LuceneGlobalProductCategoryBoostSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;LuceneGlobalProductCategoryBoostColumn&gt;"/> class
	/// that is used exclusively with a <see cref="LuceneGlobalProductCategoryBoost"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class LuceneGlobalProductCategoryBoostSortBuilder : SqlSortBuilder<LuceneGlobalProductCategoryBoostColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the LuceneGlobalProductCategoryBoostSqlSortBuilder class.
		/// </summary>
		public LuceneGlobalProductCategoryBoostSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion LuceneGlobalProductCategoryBoostSortBuilder
	
} // end namespace
