﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ProductAttributeProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class ProductAttributeProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.ProductAttribute, ZNode.Libraries.DataAccess.Entities.ProductAttributeKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProductAttributeKey key)
		{
			return Delete(transactionManager, key.AttributeId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_attributeId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _attributeId)
		{
			return Delete(null, _attributeId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_attributeId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _attributeId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.ProductAttribute Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProductAttributeKey key, int start, int pageLength)
		{
			return GetByAttributeId(transactionManager, key.AttributeId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_1 index.
		/// </summary>
		/// <param name="_attributeTypeId"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;ProductAttribute&gt;"/> class.</returns>
		public TList<ProductAttribute> GetByAttributeTypeId(System.Int32 _attributeTypeId)
		{
			int count = -1;
			return GetByAttributeTypeId(null,_attributeTypeId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_1 index.
		/// </summary>
		/// <param name="_attributeTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ProductAttribute&gt;"/> class.</returns>
		public TList<ProductAttribute> GetByAttributeTypeId(System.Int32 _attributeTypeId, int start, int pageLength)
		{
			int count = -1;
			return GetByAttributeTypeId(null, _attributeTypeId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_1 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_attributeTypeId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ProductAttribute&gt;"/> class.</returns>
		public TList<ProductAttribute> GetByAttributeTypeId(TransactionManager transactionManager, System.Int32 _attributeTypeId)
		{
			int count = -1;
			return GetByAttributeTypeId(transactionManager, _attributeTypeId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_1 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_attributeTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ProductAttribute&gt;"/> class.</returns>
		public TList<ProductAttribute> GetByAttributeTypeId(TransactionManager transactionManager, System.Int32 _attributeTypeId, int start, int pageLength)
		{
			int count = -1;
			return GetByAttributeTypeId(transactionManager, _attributeTypeId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_1 index.
		/// </summary>
		/// <param name="_attributeTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ProductAttribute&gt;"/> class.</returns>
		public TList<ProductAttribute> GetByAttributeTypeId(System.Int32 _attributeTypeId, int start, int pageLength, out int count)
		{
			return GetByAttributeTypeId(null, _attributeTypeId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_1 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_attributeTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;ProductAttribute&gt;"/> class.</returns>
		public abstract TList<ProductAttribute> GetByAttributeTypeId(TransactionManager transactionManager, System.Int32 _attributeTypeId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_SC_Attribute index.
		/// </summary>
		/// <param name="_attributeId"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductAttribute"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductAttribute GetByAttributeId(System.Int32 _attributeId)
		{
			int count = -1;
			return GetByAttributeId(null,_attributeId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SC_Attribute index.
		/// </summary>
		/// <param name="_attributeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductAttribute"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductAttribute GetByAttributeId(System.Int32 _attributeId, int start, int pageLength)
		{
			int count = -1;
			return GetByAttributeId(null, _attributeId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SC_Attribute index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_attributeId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductAttribute"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductAttribute GetByAttributeId(TransactionManager transactionManager, System.Int32 _attributeId)
		{
			int count = -1;
			return GetByAttributeId(transactionManager, _attributeId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SC_Attribute index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_attributeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductAttribute"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductAttribute GetByAttributeId(TransactionManager transactionManager, System.Int32 _attributeId, int start, int pageLength)
		{
			int count = -1;
			return GetByAttributeId(transactionManager, _attributeId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SC_Attribute index.
		/// </summary>
		/// <param name="_attributeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductAttribute"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductAttribute GetByAttributeId(System.Int32 _attributeId, int start, int pageLength, out int count)
		{
			return GetByAttributeId(null, _attributeId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SC_Attribute index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_attributeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductAttribute"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.ProductAttribute GetByAttributeId(TransactionManager transactionManager, System.Int32 _attributeId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;ProductAttribute&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;ProductAttribute&gt;"/></returns>
		public static TList<ProductAttribute> Fill(IDataReader reader, TList<ProductAttribute> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.ProductAttribute c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("ProductAttribute")
					.Append("|").Append((System.Int32)reader[((int)ProductAttributeColumn.AttributeId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<ProductAttribute>(
					key.ToString(), // EntityTrackingKey
					"ProductAttribute",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.ProductAttribute();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.AttributeId = (System.Int32)reader[((int)ProductAttributeColumn.AttributeId - 1)];
					c.AttributeTypeId = (System.Int32)reader[((int)ProductAttributeColumn.AttributeTypeId - 1)];
					c.Name = (System.String)reader[((int)ProductAttributeColumn.Name - 1)];
					c.ExternalId = (reader.IsDBNull(((int)ProductAttributeColumn.ExternalId - 1)))?null:(System.String)reader[((int)ProductAttributeColumn.ExternalId - 1)];
					c.DisplayOrder = (System.Int32)reader[((int)ProductAttributeColumn.DisplayOrder - 1)];
					c.IsActive = (System.Boolean)reader[((int)ProductAttributeColumn.IsActive - 1)];
					c.OldAttributeId = (reader.IsDBNull(((int)ProductAttributeColumn.OldAttributeId - 1)))?null:(System.Int32?)reader[((int)ProductAttributeColumn.OldAttributeId - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.ProductAttribute"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ProductAttribute"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.ProductAttribute entity)
		{
			if (!reader.Read()) return;
			
			entity.AttributeId = (System.Int32)reader[((int)ProductAttributeColumn.AttributeId - 1)];
			entity.AttributeTypeId = (System.Int32)reader[((int)ProductAttributeColumn.AttributeTypeId - 1)];
			entity.Name = (System.String)reader[((int)ProductAttributeColumn.Name - 1)];
			entity.ExternalId = (reader.IsDBNull(((int)ProductAttributeColumn.ExternalId - 1)))?null:(System.String)reader[((int)ProductAttributeColumn.ExternalId - 1)];
			entity.DisplayOrder = (System.Int32)reader[((int)ProductAttributeColumn.DisplayOrder - 1)];
			entity.IsActive = (System.Boolean)reader[((int)ProductAttributeColumn.IsActive - 1)];
			entity.OldAttributeId = (reader.IsDBNull(((int)ProductAttributeColumn.OldAttributeId - 1)))?null:(System.Int32?)reader[((int)ProductAttributeColumn.OldAttributeId - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.ProductAttribute"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ProductAttribute"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.ProductAttribute entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.AttributeId = (System.Int32)dataRow["AttributeId"];
			entity.AttributeTypeId = (System.Int32)dataRow["AttributeTypeId"];
			entity.Name = (System.String)dataRow["Name"];
			entity.ExternalId = Convert.IsDBNull(dataRow["ExternalId"]) ? null : (System.String)dataRow["ExternalId"];
			entity.DisplayOrder = (System.Int32)dataRow["DisplayOrder"];
			entity.IsActive = (System.Boolean)dataRow["IsActive"];
			entity.OldAttributeId = Convert.IsDBNull(dataRow["OldAttributeId"]) ? null : (System.Int32?)dataRow["OldAttributeId"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ProductAttribute"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.ProductAttribute Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProductAttribute entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region AttributeTypeIdSource	
			if (CanDeepLoad(entity, "AttributeType|AttributeTypeIdSource", deepLoadType, innerList) 
				&& entity.AttributeTypeIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.AttributeTypeId;
				AttributeType tmpEntity = EntityManager.LocateEntity<AttributeType>(EntityLocator.ConstructKeyFromPkItems(typeof(AttributeType), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.AttributeTypeIdSource = tmpEntity;
				else
					entity.AttributeTypeIdSource = DataRepository.AttributeTypeProvider.GetByAttributeTypeId(transactionManager, entity.AttributeTypeId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AttributeTypeIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.AttributeTypeIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.AttributeTypeProvider.DeepLoad(transactionManager, entity.AttributeTypeIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion AttributeTypeIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByAttributeId methods when available
			
			#region SKUAttributeCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<SKUAttribute>|SKUAttributeCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SKUAttributeCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.SKUAttributeCollection = DataRepository.SKUAttributeProvider.GetByAttributeId(transactionManager, entity.AttributeId);

				if (deep && entity.SKUAttributeCollection.Count > 0)
				{
					deepHandles.Add("SKUAttributeCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<SKUAttribute>) DataRepository.SKUAttributeProvider.DeepLoad,
						new object[] { transactionManager, entity.SKUAttributeCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.ProductAttribute object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.ProductAttribute instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.ProductAttribute Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProductAttribute entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region AttributeTypeIdSource
			if (CanDeepSave(entity, "AttributeType|AttributeTypeIdSource", deepSaveType, innerList) 
				&& entity.AttributeTypeIdSource != null)
			{
				DataRepository.AttributeTypeProvider.Save(transactionManager, entity.AttributeTypeIdSource);
				entity.AttributeTypeId = entity.AttributeTypeIdSource.AttributeTypeId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<SKUAttribute>
				if (CanDeepSave(entity.SKUAttributeCollection, "List<SKUAttribute>|SKUAttributeCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(SKUAttribute child in entity.SKUAttributeCollection)
					{
						if(child.AttributeIdSource != null)
						{
							child.AttributeId = child.AttributeIdSource.AttributeId;
						}
						else
						{
							child.AttributeId = entity.AttributeId;
						}

					}

					if (entity.SKUAttributeCollection.Count > 0 || entity.SKUAttributeCollection.DeletedItems.Count > 0)
					{
						//DataRepository.SKUAttributeProvider.Save(transactionManager, entity.SKUAttributeCollection);
						
						deepHandles.Add("SKUAttributeCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< SKUAttribute >) DataRepository.SKUAttributeProvider.DeepSave,
							new object[] { transactionManager, entity.SKUAttributeCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region ProductAttributeChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.ProductAttribute</c>
	///</summary>
	public enum ProductAttributeChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>AttributeType</c> at AttributeTypeIdSource
		///</summary>
		[ChildEntityType(typeof(AttributeType))]
		AttributeType,
		///<summary>
		/// Collection of <c>ProductAttribute</c> as OneToMany for SKUAttributeCollection
		///</summary>
		[ChildEntityType(typeof(TList<SKUAttribute>))]
		SKUAttributeCollection,
	}
	
	#endregion ProductAttributeChildEntityTypes
	
	#region ProductAttributeFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;ProductAttributeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ProductAttribute"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductAttributeFilterBuilder : SqlFilterBuilder<ProductAttributeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductAttributeFilterBuilder class.
		/// </summary>
		public ProductAttributeFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductAttributeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductAttributeFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductAttributeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductAttributeFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductAttributeFilterBuilder
	
	#region ProductAttributeParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;ProductAttributeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ProductAttribute"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductAttributeParameterBuilder : ParameterizedSqlFilterBuilder<ProductAttributeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductAttributeParameterBuilder class.
		/// </summary>
		public ProductAttributeParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductAttributeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductAttributeParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductAttributeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductAttributeParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductAttributeParameterBuilder
	
	#region ProductAttributeSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;ProductAttributeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ProductAttribute"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ProductAttributeSortBuilder : SqlSortBuilder<ProductAttributeColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductAttributeSqlSortBuilder class.
		/// </summary>
		public ProductAttributeSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ProductAttributeSortBuilder
	
} // end namespace
