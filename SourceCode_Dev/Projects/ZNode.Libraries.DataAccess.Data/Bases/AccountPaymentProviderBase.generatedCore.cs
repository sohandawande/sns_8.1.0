﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="AccountPaymentProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class AccountPaymentProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.AccountPayment, ZNode.Libraries.DataAccess.Entities.AccountPaymentKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.AccountPaymentKey key)
		{
			return Delete(transactionManager, key.AccountPaymentID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_accountPaymentID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _accountPaymentID)
		{
			return Delete(null, _accountPaymentID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountPaymentID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _accountPaymentID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAccountPayment_ZNodeAccount key.
		///		FK_ZNodeAccountPayment_ZNodeAccount Description: 
		/// </summary>
		/// <param name="_accountID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AccountPayment objects.</returns>
		public TList<AccountPayment> GetByAccountID(System.Int32 _accountID)
		{
			int count = -1;
			return GetByAccountID(_accountID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAccountPayment_ZNodeAccount key.
		///		FK_ZNodeAccountPayment_ZNodeAccount Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AccountPayment objects.</returns>
		/// <remarks></remarks>
		public TList<AccountPayment> GetByAccountID(TransactionManager transactionManager, System.Int32 _accountID)
		{
			int count = -1;
			return GetByAccountID(transactionManager, _accountID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAccountPayment_ZNodeAccount key.
		///		FK_ZNodeAccountPayment_ZNodeAccount Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AccountPayment objects.</returns>
		public TList<AccountPayment> GetByAccountID(TransactionManager transactionManager, System.Int32 _accountID, int start, int pageLength)
		{
			int count = -1;
			return GetByAccountID(transactionManager, _accountID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAccountPayment_ZNodeAccount key.
		///		fKZNodeAccountPaymentZNodeAccount Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_accountID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AccountPayment objects.</returns>
		public TList<AccountPayment> GetByAccountID(System.Int32 _accountID, int start, int pageLength)
		{
			int count =  -1;
			return GetByAccountID(null, _accountID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAccountPayment_ZNodeAccount key.
		///		fKZNodeAccountPaymentZNodeAccount Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_accountID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AccountPayment objects.</returns>
		public TList<AccountPayment> GetByAccountID(System.Int32 _accountID, int start, int pageLength,out int count)
		{
			return GetByAccountID(null, _accountID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAccountPayment_ZNodeAccount key.
		///		FK_ZNodeAccountPayment_ZNodeAccount Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AccountPayment objects.</returns>
		public abstract TList<AccountPayment> GetByAccountID(TransactionManager transactionManager, System.Int32 _accountID, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAccountPayment_ZNodeOrder key.
		///		FK_ZNodeAccountPayment_ZNodeOrder Description: 
		/// </summary>
		/// <param name="_orderID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AccountPayment objects.</returns>
		public TList<AccountPayment> GetByOrderID(System.Int32? _orderID)
		{
			int count = -1;
			return GetByOrderID(_orderID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAccountPayment_ZNodeOrder key.
		///		FK_ZNodeAccountPayment_ZNodeOrder Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_orderID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AccountPayment objects.</returns>
		/// <remarks></remarks>
		public TList<AccountPayment> GetByOrderID(TransactionManager transactionManager, System.Int32? _orderID)
		{
			int count = -1;
			return GetByOrderID(transactionManager, _orderID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAccountPayment_ZNodeOrder key.
		///		FK_ZNodeAccountPayment_ZNodeOrder Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_orderID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AccountPayment objects.</returns>
		public TList<AccountPayment> GetByOrderID(TransactionManager transactionManager, System.Int32? _orderID, int start, int pageLength)
		{
			int count = -1;
			return GetByOrderID(transactionManager, _orderID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAccountPayment_ZNodeOrder key.
		///		fKZNodeAccountPaymentZNodeOrder Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_orderID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AccountPayment objects.</returns>
		public TList<AccountPayment> GetByOrderID(System.Int32? _orderID, int start, int pageLength)
		{
			int count =  -1;
			return GetByOrderID(null, _orderID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAccountPayment_ZNodeOrder key.
		///		fKZNodeAccountPaymentZNodeOrder Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_orderID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AccountPayment objects.</returns>
		public TList<AccountPayment> GetByOrderID(System.Int32? _orderID, int start, int pageLength,out int count)
		{
			return GetByOrderID(null, _orderID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAccountPayment_ZNodeOrder key.
		///		FK_ZNodeAccountPayment_ZNodeOrder Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_orderID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AccountPayment objects.</returns>
		public abstract TList<AccountPayment> GetByOrderID(TransactionManager transactionManager, System.Int32? _orderID, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.AccountPayment Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.AccountPaymentKey key, int start, int pageLength)
		{
			return GetByAccountPaymentID(transactionManager, key.AccountPaymentID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodeAccountPayment index.
		/// </summary>
		/// <param name="_accountPaymentID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.AccountPayment"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.AccountPayment GetByAccountPaymentID(System.Int32 _accountPaymentID)
		{
			int count = -1;
			return GetByAccountPaymentID(null,_accountPaymentID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeAccountPayment index.
		/// </summary>
		/// <param name="_accountPaymentID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.AccountPayment"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.AccountPayment GetByAccountPaymentID(System.Int32 _accountPaymentID, int start, int pageLength)
		{
			int count = -1;
			return GetByAccountPaymentID(null, _accountPaymentID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeAccountPayment index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountPaymentID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.AccountPayment"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.AccountPayment GetByAccountPaymentID(TransactionManager transactionManager, System.Int32 _accountPaymentID)
		{
			int count = -1;
			return GetByAccountPaymentID(transactionManager, _accountPaymentID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeAccountPayment index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountPaymentID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.AccountPayment"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.AccountPayment GetByAccountPaymentID(TransactionManager transactionManager, System.Int32 _accountPaymentID, int start, int pageLength)
		{
			int count = -1;
			return GetByAccountPaymentID(transactionManager, _accountPaymentID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeAccountPayment index.
		/// </summary>
		/// <param name="_accountPaymentID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.AccountPayment"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.AccountPayment GetByAccountPaymentID(System.Int32 _accountPaymentID, int start, int pageLength, out int count)
		{
			return GetByAccountPaymentID(null, _accountPaymentID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeAccountPayment index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountPaymentID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.AccountPayment"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.AccountPayment GetByAccountPaymentID(TransactionManager transactionManager, System.Int32 _accountPaymentID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;AccountPayment&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;AccountPayment&gt;"/></returns>
		public static TList<AccountPayment> Fill(IDataReader reader, TList<AccountPayment> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.AccountPayment c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("AccountPayment")
					.Append("|").Append((System.Int32)reader[((int)AccountPaymentColumn.AccountPaymentID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<AccountPayment>(
					key.ToString(), // EntityTrackingKey
					"AccountPayment",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.AccountPayment();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.AccountPaymentID = (System.Int32)reader[((int)AccountPaymentColumn.AccountPaymentID - 1)];
					c.AccountID = (System.Int32)reader[((int)AccountPaymentColumn.AccountID - 1)];
					c.OrderID = (reader.IsDBNull(((int)AccountPaymentColumn.OrderID - 1)))?null:(System.Int32?)reader[((int)AccountPaymentColumn.OrderID - 1)];
					c.TransactionID = (reader.IsDBNull(((int)AccountPaymentColumn.TransactionID - 1)))?null:(System.String)reader[((int)AccountPaymentColumn.TransactionID - 1)];
					c.ReceivedDate = (reader.IsDBNull(((int)AccountPaymentColumn.ReceivedDate - 1)))?null:(System.DateTime?)reader[((int)AccountPaymentColumn.ReceivedDate - 1)];
					c.Description = (reader.IsDBNull(((int)AccountPaymentColumn.Description - 1)))?null:(System.String)reader[((int)AccountPaymentColumn.Description - 1)];
					c.Amount = (reader.IsDBNull(((int)AccountPaymentColumn.Amount - 1)))?null:(System.Decimal?)reader[((int)AccountPaymentColumn.Amount - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.AccountPayment"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.AccountPayment"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.AccountPayment entity)
		{
			if (!reader.Read()) return;
			
			entity.AccountPaymentID = (System.Int32)reader[((int)AccountPaymentColumn.AccountPaymentID - 1)];
			entity.AccountID = (System.Int32)reader[((int)AccountPaymentColumn.AccountID - 1)];
			entity.OrderID = (reader.IsDBNull(((int)AccountPaymentColumn.OrderID - 1)))?null:(System.Int32?)reader[((int)AccountPaymentColumn.OrderID - 1)];
			entity.TransactionID = (reader.IsDBNull(((int)AccountPaymentColumn.TransactionID - 1)))?null:(System.String)reader[((int)AccountPaymentColumn.TransactionID - 1)];
			entity.ReceivedDate = (reader.IsDBNull(((int)AccountPaymentColumn.ReceivedDate - 1)))?null:(System.DateTime?)reader[((int)AccountPaymentColumn.ReceivedDate - 1)];
			entity.Description = (reader.IsDBNull(((int)AccountPaymentColumn.Description - 1)))?null:(System.String)reader[((int)AccountPaymentColumn.Description - 1)];
			entity.Amount = (reader.IsDBNull(((int)AccountPaymentColumn.Amount - 1)))?null:(System.Decimal?)reader[((int)AccountPaymentColumn.Amount - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.AccountPayment"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.AccountPayment"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.AccountPayment entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.AccountPaymentID = (System.Int32)dataRow["AccountPaymentID"];
			entity.AccountID = (System.Int32)dataRow["AccountID"];
			entity.OrderID = Convert.IsDBNull(dataRow["OrderID"]) ? null : (System.Int32?)dataRow["OrderID"];
			entity.TransactionID = Convert.IsDBNull(dataRow["TransactionID"]) ? null : (System.String)dataRow["TransactionID"];
			entity.ReceivedDate = Convert.IsDBNull(dataRow["ReceivedDate"]) ? null : (System.DateTime?)dataRow["ReceivedDate"];
			entity.Description = Convert.IsDBNull(dataRow["Description"]) ? null : (System.String)dataRow["Description"];
			entity.Amount = Convert.IsDBNull(dataRow["Amount"]) ? null : (System.Decimal?)dataRow["Amount"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.AccountPayment"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.AccountPayment Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.AccountPayment entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region AccountIDSource	
			if (CanDeepLoad(entity, "Account|AccountIDSource", deepLoadType, innerList) 
				&& entity.AccountIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.AccountID;
				Account tmpEntity = EntityManager.LocateEntity<Account>(EntityLocator.ConstructKeyFromPkItems(typeof(Account), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.AccountIDSource = tmpEntity;
				else
					entity.AccountIDSource = DataRepository.AccountProvider.GetByAccountID(transactionManager, entity.AccountID);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AccountIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.AccountIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.AccountProvider.DeepLoad(transactionManager, entity.AccountIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion AccountIDSource

			#region OrderIDSource	
			if (CanDeepLoad(entity, "Order|OrderIDSource", deepLoadType, innerList) 
				&& entity.OrderIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.OrderID ?? (int)0);
				Order tmpEntity = EntityManager.LocateEntity<Order>(EntityLocator.ConstructKeyFromPkItems(typeof(Order), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.OrderIDSource = tmpEntity;
				else
					entity.OrderIDSource = DataRepository.OrderProvider.GetByOrderID(transactionManager, (entity.OrderID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'OrderIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.OrderIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.OrderProvider.DeepLoad(transactionManager, entity.OrderIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion OrderIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.AccountPayment object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.AccountPayment instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.AccountPayment Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.AccountPayment entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region AccountIDSource
			if (CanDeepSave(entity, "Account|AccountIDSource", deepSaveType, innerList) 
				&& entity.AccountIDSource != null)
			{
				DataRepository.AccountProvider.Save(transactionManager, entity.AccountIDSource);
				entity.AccountID = entity.AccountIDSource.AccountID;
			}
			#endregion 
			
			#region OrderIDSource
			if (CanDeepSave(entity, "Order|OrderIDSource", deepSaveType, innerList) 
				&& entity.OrderIDSource != null)
			{
				DataRepository.OrderProvider.Save(transactionManager, entity.OrderIDSource);
				entity.OrderID = entity.OrderIDSource.OrderID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region AccountPaymentChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.AccountPayment</c>
	///</summary>
	public enum AccountPaymentChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Account</c> at AccountIDSource
		///</summary>
		[ChildEntityType(typeof(Account))]
		Account,
		
		///<summary>
		/// Composite Property for <c>Order</c> at OrderIDSource
		///</summary>
		[ChildEntityType(typeof(Order))]
		Order,
	}
	
	#endregion AccountPaymentChildEntityTypes
	
	#region AccountPaymentFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;AccountPaymentColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AccountPayment"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AccountPaymentFilterBuilder : SqlFilterBuilder<AccountPaymentColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AccountPaymentFilterBuilder class.
		/// </summary>
		public AccountPaymentFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the AccountPaymentFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AccountPaymentFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AccountPaymentFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AccountPaymentFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AccountPaymentFilterBuilder
	
	#region AccountPaymentParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;AccountPaymentColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AccountPayment"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AccountPaymentParameterBuilder : ParameterizedSqlFilterBuilder<AccountPaymentColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AccountPaymentParameterBuilder class.
		/// </summary>
		public AccountPaymentParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the AccountPaymentParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AccountPaymentParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AccountPaymentParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AccountPaymentParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AccountPaymentParameterBuilder
	
	#region AccountPaymentSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;AccountPaymentColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AccountPayment"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class AccountPaymentSortBuilder : SqlSortBuilder<AccountPaymentColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AccountPaymentSqlSortBuilder class.
		/// </summary>
		public AccountPaymentSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion AccountPaymentSortBuilder
	
} // end namespace
