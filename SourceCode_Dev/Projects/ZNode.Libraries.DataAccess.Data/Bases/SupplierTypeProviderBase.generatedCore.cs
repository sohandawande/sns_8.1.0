﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="SupplierTypeProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class SupplierTypeProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.SupplierType, ZNode.Libraries.DataAccess.Entities.SupplierTypeKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.SupplierTypeKey key)
		{
			return Delete(transactionManager, key.SupplierTypeID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_supplierTypeID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _supplierTypeID)
		{
			return Delete(null, _supplierTypeID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_supplierTypeID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _supplierTypeID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.SupplierType Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.SupplierTypeKey key, int start, int pageLength)
		{
			return GetBySupplierTypeID(transactionManager, key.SupplierTypeID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeSupplierType_ActiveInd index.
		/// </summary>
		/// <param name="_activeInd"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;SupplierType&gt;"/> class.</returns>
		public TList<SupplierType> GetByActiveInd(System.Boolean _activeInd)
		{
			int count = -1;
			return GetByActiveInd(null,_activeInd, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSupplierType_ActiveInd index.
		/// </summary>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;SupplierType&gt;"/> class.</returns>
		public TList<SupplierType> GetByActiveInd(System.Boolean _activeInd, int start, int pageLength)
		{
			int count = -1;
			return GetByActiveInd(null, _activeInd, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSupplierType_ActiveInd index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_activeInd"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;SupplierType&gt;"/> class.</returns>
		public TList<SupplierType> GetByActiveInd(TransactionManager transactionManager, System.Boolean _activeInd)
		{
			int count = -1;
			return GetByActiveInd(transactionManager, _activeInd, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSupplierType_ActiveInd index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;SupplierType&gt;"/> class.</returns>
		public TList<SupplierType> GetByActiveInd(TransactionManager transactionManager, System.Boolean _activeInd, int start, int pageLength)
		{
			int count = -1;
			return GetByActiveInd(transactionManager, _activeInd, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSupplierType_ActiveInd index.
		/// </summary>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;SupplierType&gt;"/> class.</returns>
		public TList<SupplierType> GetByActiveInd(System.Boolean _activeInd, int start, int pageLength, out int count)
		{
			return GetByActiveInd(null, _activeInd, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSupplierType_ActiveInd index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;SupplierType&gt;"/> class.</returns>
		public abstract TList<SupplierType> GetByActiveInd(TransactionManager transactionManager, System.Boolean _activeInd, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeSupplierType_ClassName index.
		/// </summary>
		/// <param name="_className"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;SupplierType&gt;"/> class.</returns>
		public TList<SupplierType> GetByClassName(System.String _className)
		{
			int count = -1;
			return GetByClassName(null,_className, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSupplierType_ClassName index.
		/// </summary>
		/// <param name="_className"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;SupplierType&gt;"/> class.</returns>
		public TList<SupplierType> GetByClassName(System.String _className, int start, int pageLength)
		{
			int count = -1;
			return GetByClassName(null, _className, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSupplierType_ClassName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_className"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;SupplierType&gt;"/> class.</returns>
		public TList<SupplierType> GetByClassName(TransactionManager transactionManager, System.String _className)
		{
			int count = -1;
			return GetByClassName(transactionManager, _className, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSupplierType_ClassName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_className"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;SupplierType&gt;"/> class.</returns>
		public TList<SupplierType> GetByClassName(TransactionManager transactionManager, System.String _className, int start, int pageLength)
		{
			int count = -1;
			return GetByClassName(transactionManager, _className, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSupplierType_ClassName index.
		/// </summary>
		/// <param name="_className"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;SupplierType&gt;"/> class.</returns>
		public TList<SupplierType> GetByClassName(System.String _className, int start, int pageLength, out int count)
		{
			return GetByClassName(null, _className, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSupplierType_ClassName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_className"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;SupplierType&gt;"/> class.</returns>
		public abstract TList<SupplierType> GetByClassName(TransactionManager transactionManager, System.String _className, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodeSupplierType index.
		/// </summary>
		/// <param name="_supplierTypeID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SupplierType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.SupplierType GetBySupplierTypeID(System.Int32 _supplierTypeID)
		{
			int count = -1;
			return GetBySupplierTypeID(null,_supplierTypeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeSupplierType index.
		/// </summary>
		/// <param name="_supplierTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SupplierType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.SupplierType GetBySupplierTypeID(System.Int32 _supplierTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetBySupplierTypeID(null, _supplierTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeSupplierType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_supplierTypeID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SupplierType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.SupplierType GetBySupplierTypeID(TransactionManager transactionManager, System.Int32 _supplierTypeID)
		{
			int count = -1;
			return GetBySupplierTypeID(transactionManager, _supplierTypeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeSupplierType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_supplierTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SupplierType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.SupplierType GetBySupplierTypeID(TransactionManager transactionManager, System.Int32 _supplierTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetBySupplierTypeID(transactionManager, _supplierTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeSupplierType index.
		/// </summary>
		/// <param name="_supplierTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SupplierType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.SupplierType GetBySupplierTypeID(System.Int32 _supplierTypeID, int start, int pageLength, out int count)
		{
			return GetBySupplierTypeID(null, _supplierTypeID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeSupplierType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_supplierTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SupplierType"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.SupplierType GetBySupplierTypeID(TransactionManager transactionManager, System.Int32 _supplierTypeID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;SupplierType&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;SupplierType&gt;"/></returns>
		public static TList<SupplierType> Fill(IDataReader reader, TList<SupplierType> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.SupplierType c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("SupplierType")
					.Append("|").Append((System.Int32)reader[((int)SupplierTypeColumn.SupplierTypeID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<SupplierType>(
					key.ToString(), // EntityTrackingKey
					"SupplierType",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.SupplierType();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.SupplierTypeID = (System.Int32)reader[((int)SupplierTypeColumn.SupplierTypeID - 1)];
					c.ClassName = (reader.IsDBNull(((int)SupplierTypeColumn.ClassName - 1)))?null:(System.String)reader[((int)SupplierTypeColumn.ClassName - 1)];
					c.Name = (reader.IsDBNull(((int)SupplierTypeColumn.Name - 1)))?null:(System.String)reader[((int)SupplierTypeColumn.Name - 1)];
					c.Description = (reader.IsDBNull(((int)SupplierTypeColumn.Description - 1)))?null:(System.String)reader[((int)SupplierTypeColumn.Description - 1)];
					c.ActiveInd = (System.Boolean)reader[((int)SupplierTypeColumn.ActiveInd - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.SupplierType"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.SupplierType"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.SupplierType entity)
		{
			if (!reader.Read()) return;
			
			entity.SupplierTypeID = (System.Int32)reader[((int)SupplierTypeColumn.SupplierTypeID - 1)];
			entity.ClassName = (reader.IsDBNull(((int)SupplierTypeColumn.ClassName - 1)))?null:(System.String)reader[((int)SupplierTypeColumn.ClassName - 1)];
			entity.Name = (reader.IsDBNull(((int)SupplierTypeColumn.Name - 1)))?null:(System.String)reader[((int)SupplierTypeColumn.Name - 1)];
			entity.Description = (reader.IsDBNull(((int)SupplierTypeColumn.Description - 1)))?null:(System.String)reader[((int)SupplierTypeColumn.Description - 1)];
			entity.ActiveInd = (System.Boolean)reader[((int)SupplierTypeColumn.ActiveInd - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.SupplierType"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.SupplierType"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.SupplierType entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.SupplierTypeID = (System.Int32)dataRow["SupplierTypeID"];
			entity.ClassName = Convert.IsDBNull(dataRow["ClassName"]) ? null : (System.String)dataRow["ClassName"];
			entity.Name = Convert.IsDBNull(dataRow["Name"]) ? null : (System.String)dataRow["Name"];
			entity.Description = Convert.IsDBNull(dataRow["Description"]) ? null : (System.String)dataRow["Description"];
			entity.ActiveInd = (System.Boolean)dataRow["ActiveInd"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.SupplierType"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.SupplierType Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.SupplierType entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetBySupplierTypeID methods when available
			
			#region SupplierCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Supplier>|SupplierCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SupplierCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.SupplierCollection = DataRepository.SupplierProvider.GetBySupplierTypeID(transactionManager, entity.SupplierTypeID);

				if (deep && entity.SupplierCollection.Count > 0)
				{
					deepHandles.Add("SupplierCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Supplier>) DataRepository.SupplierProvider.DeepLoad,
						new object[] { transactionManager, entity.SupplierCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.SupplierType object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.SupplierType instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.SupplierType Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.SupplierType entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<Supplier>
				if (CanDeepSave(entity.SupplierCollection, "List<Supplier>|SupplierCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Supplier child in entity.SupplierCollection)
					{
						if(child.SupplierTypeIDSource != null)
						{
							child.SupplierTypeID = child.SupplierTypeIDSource.SupplierTypeID;
						}
						else
						{
							child.SupplierTypeID = entity.SupplierTypeID;
						}

					}

					if (entity.SupplierCollection.Count > 0 || entity.SupplierCollection.DeletedItems.Count > 0)
					{
						//DataRepository.SupplierProvider.Save(transactionManager, entity.SupplierCollection);
						
						deepHandles.Add("SupplierCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Supplier >) DataRepository.SupplierProvider.DeepSave,
							new object[] { transactionManager, entity.SupplierCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region SupplierTypeChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.SupplierType</c>
	///</summary>
	public enum SupplierTypeChildEntityTypes
	{
		///<summary>
		/// Collection of <c>SupplierType</c> as OneToMany for SupplierCollection
		///</summary>
		[ChildEntityType(typeof(TList<Supplier>))]
		SupplierCollection,
	}
	
	#endregion SupplierTypeChildEntityTypes
	
	#region SupplierTypeFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;SupplierTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SupplierType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SupplierTypeFilterBuilder : SqlFilterBuilder<SupplierTypeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SupplierTypeFilterBuilder class.
		/// </summary>
		public SupplierTypeFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SupplierTypeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SupplierTypeFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SupplierTypeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SupplierTypeFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SupplierTypeFilterBuilder
	
	#region SupplierTypeParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;SupplierTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SupplierType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SupplierTypeParameterBuilder : ParameterizedSqlFilterBuilder<SupplierTypeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SupplierTypeParameterBuilder class.
		/// </summary>
		public SupplierTypeParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SupplierTypeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SupplierTypeParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SupplierTypeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SupplierTypeParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SupplierTypeParameterBuilder
	
	#region SupplierTypeSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;SupplierTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SupplierType"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class SupplierTypeSortBuilder : SqlSortBuilder<SupplierTypeColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SupplierTypeSqlSortBuilder class.
		/// </summary>
		public SupplierTypeSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion SupplierTypeSortBuilder
	
} // end namespace
