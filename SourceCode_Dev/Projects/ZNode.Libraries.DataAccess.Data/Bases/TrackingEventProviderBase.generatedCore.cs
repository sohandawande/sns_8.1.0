﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="TrackingEventProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class TrackingEventProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.TrackingEvent, ZNode.Libraries.DataAccess.Entities.TrackingEventKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.TrackingEventKey key)
		{
			return Delete(transactionManager, key.TrackingEventID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_trackingEventID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _trackingEventID)
		{
			return Delete(null, _trackingEventID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_trackingEventID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _trackingEventID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeTrackingEvent_ZNodeAccount key.
		///		FK_ZNodeTrackingEvent_ZNodeAccount Description: 
		/// </summary>
		/// <param name="_accountID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.TrackingEvent objects.</returns>
		public TList<TrackingEvent> GetByAccountID(System.Int32? _accountID)
		{
			int count = -1;
			return GetByAccountID(_accountID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeTrackingEvent_ZNodeAccount key.
		///		FK_ZNodeTrackingEvent_ZNodeAccount Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.TrackingEvent objects.</returns>
		/// <remarks></remarks>
		public TList<TrackingEvent> GetByAccountID(TransactionManager transactionManager, System.Int32? _accountID)
		{
			int count = -1;
			return GetByAccountID(transactionManager, _accountID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeTrackingEvent_ZNodeAccount key.
		///		FK_ZNodeTrackingEvent_ZNodeAccount Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.TrackingEvent objects.</returns>
		public TList<TrackingEvent> GetByAccountID(TransactionManager transactionManager, System.Int32? _accountID, int start, int pageLength)
		{
			int count = -1;
			return GetByAccountID(transactionManager, _accountID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeTrackingEvent_ZNodeAccount key.
		///		fKZNodeTrackingEventZNodeAccount Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_accountID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.TrackingEvent objects.</returns>
		public TList<TrackingEvent> GetByAccountID(System.Int32? _accountID, int start, int pageLength)
		{
			int count =  -1;
			return GetByAccountID(null, _accountID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeTrackingEvent_ZNodeAccount key.
		///		fKZNodeTrackingEventZNodeAccount Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_accountID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.TrackingEvent objects.</returns>
		public TList<TrackingEvent> GetByAccountID(System.Int32? _accountID, int start, int pageLength,out int count)
		{
			return GetByAccountID(null, _accountID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeTrackingEvent_ZNodeAccount key.
		///		FK_ZNodeTrackingEvent_ZNodeAccount Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.TrackingEvent objects.</returns>
		public abstract TList<TrackingEvent> GetByAccountID(TransactionManager transactionManager, System.Int32? _accountID, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeTrackingEvent_ZNodeTracking key.
		///		FK_ZNodeTrackingEvent_ZNodeTracking Description: 
		/// </summary>
		/// <param name="_trackingID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.TrackingEvent objects.</returns>
		public TList<TrackingEvent> GetByTrackingID(System.Int32 _trackingID)
		{
			int count = -1;
			return GetByTrackingID(_trackingID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeTrackingEvent_ZNodeTracking key.
		///		FK_ZNodeTrackingEvent_ZNodeTracking Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_trackingID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.TrackingEvent objects.</returns>
		/// <remarks></remarks>
		public TList<TrackingEvent> GetByTrackingID(TransactionManager transactionManager, System.Int32 _trackingID)
		{
			int count = -1;
			return GetByTrackingID(transactionManager, _trackingID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeTrackingEvent_ZNodeTracking key.
		///		FK_ZNodeTrackingEvent_ZNodeTracking Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_trackingID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.TrackingEvent objects.</returns>
		public TList<TrackingEvent> GetByTrackingID(TransactionManager transactionManager, System.Int32 _trackingID, int start, int pageLength)
		{
			int count = -1;
			return GetByTrackingID(transactionManager, _trackingID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeTrackingEvent_ZNodeTracking key.
		///		fKZNodeTrackingEventZNodeTracking Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_trackingID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.TrackingEvent objects.</returns>
		public TList<TrackingEvent> GetByTrackingID(System.Int32 _trackingID, int start, int pageLength)
		{
			int count =  -1;
			return GetByTrackingID(null, _trackingID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeTrackingEvent_ZNodeTracking key.
		///		fKZNodeTrackingEventZNodeTracking Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_trackingID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.TrackingEvent objects.</returns>
		public TList<TrackingEvent> GetByTrackingID(System.Int32 _trackingID, int start, int pageLength,out int count)
		{
			return GetByTrackingID(null, _trackingID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeTrackingEvent_ZNodeTracking key.
		///		FK_ZNodeTrackingEvent_ZNodeTracking Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_trackingID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.TrackingEvent objects.</returns>
		public abstract TList<TrackingEvent> GetByTrackingID(TransactionManager transactionManager, System.Int32 _trackingID, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.TrackingEvent Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.TrackingEventKey key, int start, int pageLength)
		{
			return GetByTrackingEventID(transactionManager, key.TrackingEventID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodeTrackingEvent index.
		/// </summary>
		/// <param name="_trackingEventID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.TrackingEvent"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.TrackingEvent GetByTrackingEventID(System.Int32 _trackingEventID)
		{
			int count = -1;
			return GetByTrackingEventID(null,_trackingEventID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeTrackingEvent index.
		/// </summary>
		/// <param name="_trackingEventID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.TrackingEvent"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.TrackingEvent GetByTrackingEventID(System.Int32 _trackingEventID, int start, int pageLength)
		{
			int count = -1;
			return GetByTrackingEventID(null, _trackingEventID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeTrackingEvent index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_trackingEventID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.TrackingEvent"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.TrackingEvent GetByTrackingEventID(TransactionManager transactionManager, System.Int32 _trackingEventID)
		{
			int count = -1;
			return GetByTrackingEventID(transactionManager, _trackingEventID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeTrackingEvent index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_trackingEventID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.TrackingEvent"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.TrackingEvent GetByTrackingEventID(TransactionManager transactionManager, System.Int32 _trackingEventID, int start, int pageLength)
		{
			int count = -1;
			return GetByTrackingEventID(transactionManager, _trackingEventID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeTrackingEvent index.
		/// </summary>
		/// <param name="_trackingEventID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.TrackingEvent"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.TrackingEvent GetByTrackingEventID(System.Int32 _trackingEventID, int start, int pageLength, out int count)
		{
			return GetByTrackingEventID(null, _trackingEventID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeTrackingEvent index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_trackingEventID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.TrackingEvent"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.TrackingEvent GetByTrackingEventID(TransactionManager transactionManager, System.Int32 _trackingEventID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;TrackingEvent&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;TrackingEvent&gt;"/></returns>
		public static TList<TrackingEvent> Fill(IDataReader reader, TList<TrackingEvent> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.TrackingEvent c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("TrackingEvent")
					.Append("|").Append((System.Int32)reader[((int)TrackingEventColumn.TrackingEventID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<TrackingEvent>(
					key.ToString(), // EntityTrackingKey
					"TrackingEvent",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.TrackingEvent();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.TrackingEventID = (System.Int32)reader[((int)TrackingEventColumn.TrackingEventID - 1)];
					c.TrackingID = (System.Int32)reader[((int)TrackingEventColumn.TrackingID - 1)];
					c.Date = (reader.IsDBNull(((int)TrackingEventColumn.Date - 1)))?null:(System.DateTime?)reader[((int)TrackingEventColumn.Date - 1)];
					c.SafeNameEvent = (reader.IsDBNull(((int)TrackingEventColumn.SafeNameEvent - 1)))?null:(System.String)reader[((int)TrackingEventColumn.SafeNameEvent - 1)];
					c.RefererDomain = (reader.IsDBNull(((int)TrackingEventColumn.RefererDomain - 1)))?null:(System.String)reader[((int)TrackingEventColumn.RefererDomain - 1)];
					c.RefererQuery = (reader.IsDBNull(((int)TrackingEventColumn.RefererQuery - 1)))?null:(System.String)reader[((int)TrackingEventColumn.RefererQuery - 1)];
					c.AccountID = (reader.IsDBNull(((int)TrackingEventColumn.AccountID - 1)))?null:(System.Int32?)reader[((int)TrackingEventColumn.AccountID - 1)];
					c.OrderID = (reader.IsDBNull(((int)TrackingEventColumn.OrderID - 1)))?null:(System.Int32?)reader[((int)TrackingEventColumn.OrderID - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.TrackingEvent"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.TrackingEvent"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.TrackingEvent entity)
		{
			if (!reader.Read()) return;
			
			entity.TrackingEventID = (System.Int32)reader[((int)TrackingEventColumn.TrackingEventID - 1)];
			entity.TrackingID = (System.Int32)reader[((int)TrackingEventColumn.TrackingID - 1)];
			entity.Date = (reader.IsDBNull(((int)TrackingEventColumn.Date - 1)))?null:(System.DateTime?)reader[((int)TrackingEventColumn.Date - 1)];
			entity.SafeNameEvent = (reader.IsDBNull(((int)TrackingEventColumn.SafeNameEvent - 1)))?null:(System.String)reader[((int)TrackingEventColumn.SafeNameEvent - 1)];
			entity.RefererDomain = (reader.IsDBNull(((int)TrackingEventColumn.RefererDomain - 1)))?null:(System.String)reader[((int)TrackingEventColumn.RefererDomain - 1)];
			entity.RefererQuery = (reader.IsDBNull(((int)TrackingEventColumn.RefererQuery - 1)))?null:(System.String)reader[((int)TrackingEventColumn.RefererQuery - 1)];
			entity.AccountID = (reader.IsDBNull(((int)TrackingEventColumn.AccountID - 1)))?null:(System.Int32?)reader[((int)TrackingEventColumn.AccountID - 1)];
			entity.OrderID = (reader.IsDBNull(((int)TrackingEventColumn.OrderID - 1)))?null:(System.Int32?)reader[((int)TrackingEventColumn.OrderID - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.TrackingEvent"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.TrackingEvent"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.TrackingEvent entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.TrackingEventID = (System.Int32)dataRow["TrackingEventID"];
			entity.TrackingID = (System.Int32)dataRow["TrackingID"];
			entity.Date = Convert.IsDBNull(dataRow["Date"]) ? null : (System.DateTime?)dataRow["Date"];
			entity.SafeNameEvent = Convert.IsDBNull(dataRow["Event"]) ? null : (System.String)dataRow["Event"];
			entity.RefererDomain = Convert.IsDBNull(dataRow["RefererDomain"]) ? null : (System.String)dataRow["RefererDomain"];
			entity.RefererQuery = Convert.IsDBNull(dataRow["RefererQuery"]) ? null : (System.String)dataRow["RefererQuery"];
			entity.AccountID = Convert.IsDBNull(dataRow["AccountID"]) ? null : (System.Int32?)dataRow["AccountID"];
			entity.OrderID = Convert.IsDBNull(dataRow["OrderID"]) ? null : (System.Int32?)dataRow["OrderID"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.TrackingEvent"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.TrackingEvent Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.TrackingEvent entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region AccountIDSource	
			if (CanDeepLoad(entity, "Account|AccountIDSource", deepLoadType, innerList) 
				&& entity.AccountIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.AccountID ?? (int)0);
				Account tmpEntity = EntityManager.LocateEntity<Account>(EntityLocator.ConstructKeyFromPkItems(typeof(Account), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.AccountIDSource = tmpEntity;
				else
					entity.AccountIDSource = DataRepository.AccountProvider.GetByAccountID(transactionManager, (entity.AccountID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AccountIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.AccountIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.AccountProvider.DeepLoad(transactionManager, entity.AccountIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion AccountIDSource

			#region TrackingIDSource	
			if (CanDeepLoad(entity, "Tracking|TrackingIDSource", deepLoadType, innerList) 
				&& entity.TrackingIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.TrackingID;
				Tracking tmpEntity = EntityManager.LocateEntity<Tracking>(EntityLocator.ConstructKeyFromPkItems(typeof(Tracking), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.TrackingIDSource = tmpEntity;
				else
					entity.TrackingIDSource = DataRepository.TrackingProvider.GetByTrackingID(transactionManager, entity.TrackingID);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'TrackingIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.TrackingIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.TrackingProvider.DeepLoad(transactionManager, entity.TrackingIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion TrackingIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.TrackingEvent object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.TrackingEvent instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.TrackingEvent Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.TrackingEvent entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region AccountIDSource
			if (CanDeepSave(entity, "Account|AccountIDSource", deepSaveType, innerList) 
				&& entity.AccountIDSource != null)
			{
				DataRepository.AccountProvider.Save(transactionManager, entity.AccountIDSource);
				entity.AccountID = entity.AccountIDSource.AccountID;
			}
			#endregion 
			
			#region TrackingIDSource
			if (CanDeepSave(entity, "Tracking|TrackingIDSource", deepSaveType, innerList) 
				&& entity.TrackingIDSource != null)
			{
				DataRepository.TrackingProvider.Save(transactionManager, entity.TrackingIDSource);
				entity.TrackingID = entity.TrackingIDSource.TrackingID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region TrackingEventChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.TrackingEvent</c>
	///</summary>
	public enum TrackingEventChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Account</c> at AccountIDSource
		///</summary>
		[ChildEntityType(typeof(Account))]
		Account,
		
		///<summary>
		/// Composite Property for <c>Tracking</c> at TrackingIDSource
		///</summary>
		[ChildEntityType(typeof(Tracking))]
		Tracking,
	}
	
	#endregion TrackingEventChildEntityTypes
	
	#region TrackingEventFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;TrackingEventColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TrackingEvent"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TrackingEventFilterBuilder : SqlFilterBuilder<TrackingEventColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TrackingEventFilterBuilder class.
		/// </summary>
		public TrackingEventFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the TrackingEventFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TrackingEventFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TrackingEventFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TrackingEventFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TrackingEventFilterBuilder
	
	#region TrackingEventParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;TrackingEventColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TrackingEvent"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TrackingEventParameterBuilder : ParameterizedSqlFilterBuilder<TrackingEventColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TrackingEventParameterBuilder class.
		/// </summary>
		public TrackingEventParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the TrackingEventParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TrackingEventParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TrackingEventParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TrackingEventParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TrackingEventParameterBuilder
	
	#region TrackingEventSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;TrackingEventColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TrackingEvent"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class TrackingEventSortBuilder : SqlSortBuilder<TrackingEventColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TrackingEventSqlSortBuilder class.
		/// </summary>
		public TrackingEventSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion TrackingEventSortBuilder
	
} // end namespace
