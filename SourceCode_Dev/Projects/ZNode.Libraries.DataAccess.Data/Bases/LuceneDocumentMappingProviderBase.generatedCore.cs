﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="LuceneDocumentMappingProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class LuceneDocumentMappingProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.LuceneDocumentMapping, ZNode.Libraries.DataAccess.Entities.LuceneDocumentMappingKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.LuceneDocumentMappingKey key)
		{
			return Delete(transactionManager, key.LuceneDocumentMappingID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_luceneDocumentMappingID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _luceneDocumentMappingID)
		{
			return Delete(null, _luceneDocumentMappingID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_luceneDocumentMappingID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _luceneDocumentMappingID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.LuceneDocumentMapping Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.LuceneDocumentMappingKey key, int start, int pageLength)
		{
			return GetByLuceneDocumentMappingID(transactionManager, key.LuceneDocumentMappingID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodeDocumentMapping index.
		/// </summary>
		/// <param name="_luceneDocumentMappingID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneDocumentMapping"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.LuceneDocumentMapping GetByLuceneDocumentMappingID(System.Int32 _luceneDocumentMappingID)
		{
			int count = -1;
			return GetByLuceneDocumentMappingID(null,_luceneDocumentMappingID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeDocumentMapping index.
		/// </summary>
		/// <param name="_luceneDocumentMappingID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneDocumentMapping"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.LuceneDocumentMapping GetByLuceneDocumentMappingID(System.Int32 _luceneDocumentMappingID, int start, int pageLength)
		{
			int count = -1;
			return GetByLuceneDocumentMappingID(null, _luceneDocumentMappingID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeDocumentMapping index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_luceneDocumentMappingID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneDocumentMapping"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.LuceneDocumentMapping GetByLuceneDocumentMappingID(TransactionManager transactionManager, System.Int32 _luceneDocumentMappingID)
		{
			int count = -1;
			return GetByLuceneDocumentMappingID(transactionManager, _luceneDocumentMappingID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeDocumentMapping index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_luceneDocumentMappingID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneDocumentMapping"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.LuceneDocumentMapping GetByLuceneDocumentMappingID(TransactionManager transactionManager, System.Int32 _luceneDocumentMappingID, int start, int pageLength)
		{
			int count = -1;
			return GetByLuceneDocumentMappingID(transactionManager, _luceneDocumentMappingID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeDocumentMapping index.
		/// </summary>
		/// <param name="_luceneDocumentMappingID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneDocumentMapping"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.LuceneDocumentMapping GetByLuceneDocumentMappingID(System.Int32 _luceneDocumentMappingID, int start, int pageLength, out int count)
		{
			return GetByLuceneDocumentMappingID(null, _luceneDocumentMappingID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeDocumentMapping index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_luceneDocumentMappingID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneDocumentMapping"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.LuceneDocumentMapping GetByLuceneDocumentMappingID(TransactionManager transactionManager, System.Int32 _luceneDocumentMappingID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;LuceneDocumentMapping&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;LuceneDocumentMapping&gt;"/></returns>
		public static TList<LuceneDocumentMapping> Fill(IDataReader reader, TList<LuceneDocumentMapping> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.LuceneDocumentMapping c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("LuceneDocumentMapping")
					.Append("|").Append((System.Int32)reader[((int)LuceneDocumentMappingColumn.LuceneDocumentMappingID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<LuceneDocumentMapping>(
					key.ToString(), // EntityTrackingKey
					"LuceneDocumentMapping",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.LuceneDocumentMapping();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.LuceneDocumentMappingID = (System.Int32)reader[((int)LuceneDocumentMappingColumn.LuceneDocumentMappingID - 1)];
					c.PropertyName = (System.String)reader[((int)LuceneDocumentMappingColumn.PropertyName - 1)];
					c.DocumentName = (System.String)reader[((int)LuceneDocumentMappingColumn.DocumentName - 1)];
					c.IsIndexed = (System.Boolean)reader[((int)LuceneDocumentMappingColumn.IsIndexed - 1)];
					c.IsFaceted = (System.Boolean)reader[((int)LuceneDocumentMappingColumn.IsFaceted - 1)];
					c.IsStored = (System.Boolean)reader[((int)LuceneDocumentMappingColumn.IsStored - 1)];
					c.IsBoosted = (System.Boolean)reader[((int)LuceneDocumentMappingColumn.IsBoosted - 1)];
					c.FieldBoostable = (System.Boolean)reader[((int)LuceneDocumentMappingColumn.FieldBoostable - 1)];
					c.Boost = (reader.IsDBNull(((int)LuceneDocumentMappingColumn.Boost - 1)))?null:(System.Double?)reader[((int)LuceneDocumentMappingColumn.Boost - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneDocumentMapping"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.LuceneDocumentMapping"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.LuceneDocumentMapping entity)
		{
			if (!reader.Read()) return;
			
			entity.LuceneDocumentMappingID = (System.Int32)reader[((int)LuceneDocumentMappingColumn.LuceneDocumentMappingID - 1)];
			entity.PropertyName = (System.String)reader[((int)LuceneDocumentMappingColumn.PropertyName - 1)];
			entity.DocumentName = (System.String)reader[((int)LuceneDocumentMappingColumn.DocumentName - 1)];
			entity.IsIndexed = (System.Boolean)reader[((int)LuceneDocumentMappingColumn.IsIndexed - 1)];
			entity.IsFaceted = (System.Boolean)reader[((int)LuceneDocumentMappingColumn.IsFaceted - 1)];
			entity.IsStored = (System.Boolean)reader[((int)LuceneDocumentMappingColumn.IsStored - 1)];
			entity.IsBoosted = (System.Boolean)reader[((int)LuceneDocumentMappingColumn.IsBoosted - 1)];
			entity.FieldBoostable = (System.Boolean)reader[((int)LuceneDocumentMappingColumn.FieldBoostable - 1)];
			entity.Boost = (reader.IsDBNull(((int)LuceneDocumentMappingColumn.Boost - 1)))?null:(System.Double?)reader[((int)LuceneDocumentMappingColumn.Boost - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneDocumentMapping"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.LuceneDocumentMapping"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.LuceneDocumentMapping entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.LuceneDocumentMappingID = (System.Int32)dataRow["LuceneDocumentMappingID"];
			entity.PropertyName = (System.String)dataRow["PropertyName"];
			entity.DocumentName = (System.String)dataRow["DocumentName"];
			entity.IsIndexed = (System.Boolean)dataRow["IsIndexed"];
			entity.IsFaceted = (System.Boolean)dataRow["IsFaceted"];
			entity.IsStored = (System.Boolean)dataRow["IsStored"];
			entity.IsBoosted = (System.Boolean)dataRow["IsBoosted"];
			entity.FieldBoostable = (System.Boolean)dataRow["FieldBoostable"];
			entity.Boost = Convert.IsDBNull(dataRow["Boost"]) ? null : (System.Double?)dataRow["Boost"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.LuceneDocumentMapping"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.LuceneDocumentMapping Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.LuceneDocumentMapping entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.LuceneDocumentMapping object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.LuceneDocumentMapping instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.LuceneDocumentMapping Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.LuceneDocumentMapping entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region LuceneDocumentMappingChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.LuceneDocumentMapping</c>
	///</summary>
	public enum LuceneDocumentMappingChildEntityTypes
	{
	}
	
	#endregion LuceneDocumentMappingChildEntityTypes
	
	#region LuceneDocumentMappingFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;LuceneDocumentMappingColumn&gt;"/> class
	/// that is used exclusively with a <see cref="LuceneDocumentMapping"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class LuceneDocumentMappingFilterBuilder : SqlFilterBuilder<LuceneDocumentMappingColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the LuceneDocumentMappingFilterBuilder class.
		/// </summary>
		public LuceneDocumentMappingFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the LuceneDocumentMappingFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public LuceneDocumentMappingFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the LuceneDocumentMappingFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public LuceneDocumentMappingFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion LuceneDocumentMappingFilterBuilder
	
	#region LuceneDocumentMappingParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;LuceneDocumentMappingColumn&gt;"/> class
	/// that is used exclusively with a <see cref="LuceneDocumentMapping"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class LuceneDocumentMappingParameterBuilder : ParameterizedSqlFilterBuilder<LuceneDocumentMappingColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the LuceneDocumentMappingParameterBuilder class.
		/// </summary>
		public LuceneDocumentMappingParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the LuceneDocumentMappingParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public LuceneDocumentMappingParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the LuceneDocumentMappingParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public LuceneDocumentMappingParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion LuceneDocumentMappingParameterBuilder
	
	#region LuceneDocumentMappingSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;LuceneDocumentMappingColumn&gt;"/> class
	/// that is used exclusively with a <see cref="LuceneDocumentMapping"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class LuceneDocumentMappingSortBuilder : SqlSortBuilder<LuceneDocumentMappingColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the LuceneDocumentMappingSqlSortBuilder class.
		/// </summary>
		public LuceneDocumentMappingSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion LuceneDocumentMappingSortBuilder
	
} // end namespace
