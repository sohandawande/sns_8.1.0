﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="AddOnValueProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class AddOnValueProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.AddOnValue, ZNode.Libraries.DataAccess.Entities.AddOnValueKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.AddOnValueKey key)
		{
			return Delete(transactionManager, key.AddOnValueID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_addOnValueID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _addOnValueID)
		{
			return Delete(null, _addOnValueID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_addOnValueID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _addOnValueID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAddOnValue_ZNodeShippingRuleType key.
		///		FK_ZNodeAddOnValue_ZNodeShippingRuleType Description: 
		/// </summary>
		/// <param name="_shippingRuleTypeID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AddOnValue objects.</returns>
		public TList<AddOnValue> GetByShippingRuleTypeID(System.Int32? _shippingRuleTypeID)
		{
			int count = -1;
			return GetByShippingRuleTypeID(_shippingRuleTypeID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAddOnValue_ZNodeShippingRuleType key.
		///		FK_ZNodeAddOnValue_ZNodeShippingRuleType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_shippingRuleTypeID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AddOnValue objects.</returns>
		/// <remarks></remarks>
		public TList<AddOnValue> GetByShippingRuleTypeID(TransactionManager transactionManager, System.Int32? _shippingRuleTypeID)
		{
			int count = -1;
			return GetByShippingRuleTypeID(transactionManager, _shippingRuleTypeID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAddOnValue_ZNodeShippingRuleType key.
		///		FK_ZNodeAddOnValue_ZNodeShippingRuleType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_shippingRuleTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AddOnValue objects.</returns>
		public TList<AddOnValue> GetByShippingRuleTypeID(TransactionManager transactionManager, System.Int32? _shippingRuleTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetByShippingRuleTypeID(transactionManager, _shippingRuleTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAddOnValue_ZNodeShippingRuleType key.
		///		fKZNodeAddOnValueZNodeShippingRuleType Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_shippingRuleTypeID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AddOnValue objects.</returns>
		public TList<AddOnValue> GetByShippingRuleTypeID(System.Int32? _shippingRuleTypeID, int start, int pageLength)
		{
			int count =  -1;
			return GetByShippingRuleTypeID(null, _shippingRuleTypeID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAddOnValue_ZNodeShippingRuleType key.
		///		fKZNodeAddOnValueZNodeShippingRuleType Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_shippingRuleTypeID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AddOnValue objects.</returns>
		public TList<AddOnValue> GetByShippingRuleTypeID(System.Int32? _shippingRuleTypeID, int start, int pageLength,out int count)
		{
			return GetByShippingRuleTypeID(null, _shippingRuleTypeID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAddOnValue_ZNodeShippingRuleType key.
		///		FK_ZNodeAddOnValue_ZNodeShippingRuleType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_shippingRuleTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AddOnValue objects.</returns>
		public abstract TList<AddOnValue> GetByShippingRuleTypeID(TransactionManager transactionManager, System.Int32? _shippingRuleTypeID, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAddOnValue_ZNodeSKUInventory key.
		///		FK_ZNodeAddOnValue_ZNodeSKUInventory Description: 
		/// </summary>
		/// <param name="_sKU"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AddOnValue objects.</returns>
		public TList<AddOnValue> GetBySKU(System.String _sKU)
		{
			int count = -1;
			return GetBySKU(_sKU, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAddOnValue_ZNodeSKUInventory key.
		///		FK_ZNodeAddOnValue_ZNodeSKUInventory Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sKU"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AddOnValue objects.</returns>
		/// <remarks></remarks>
		public TList<AddOnValue> GetBySKU(TransactionManager transactionManager, System.String _sKU)
		{
			int count = -1;
			return GetBySKU(transactionManager, _sKU, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAddOnValue_ZNodeSKUInventory key.
		///		FK_ZNodeAddOnValue_ZNodeSKUInventory Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sKU"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AddOnValue objects.</returns>
		public TList<AddOnValue> GetBySKU(TransactionManager transactionManager, System.String _sKU, int start, int pageLength)
		{
			int count = -1;
			return GetBySKU(transactionManager, _sKU, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAddOnValue_ZNodeSKUInventory key.
		///		fKZNodeAddOnValueZNodeSKUInventory Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_sKU"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AddOnValue objects.</returns>
		public TList<AddOnValue> GetBySKU(System.String _sKU, int start, int pageLength)
		{
			int count =  -1;
			return GetBySKU(null, _sKU, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAddOnValue_ZNodeSKUInventory key.
		///		fKZNodeAddOnValueZNodeSKUInventory Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_sKU"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AddOnValue objects.</returns>
		public TList<AddOnValue> GetBySKU(System.String _sKU, int start, int pageLength,out int count)
		{
			return GetBySKU(null, _sKU, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAddOnValue_ZNodeSKUInventory key.
		///		FK_ZNodeAddOnValue_ZNodeSKUInventory Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sKU"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AddOnValue objects.</returns>
		public abstract TList<AddOnValue> GetBySKU(TransactionManager transactionManager, System.String _sKU, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAddOnValue_ZNodeSupplier key.
		///		FK_ZNodeAddOnValue_ZNodeSupplier Description: 
		/// </summary>
		/// <param name="_supplierID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AddOnValue objects.</returns>
		public TList<AddOnValue> GetBySupplierID(System.Int32? _supplierID)
		{
			int count = -1;
			return GetBySupplierID(_supplierID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAddOnValue_ZNodeSupplier key.
		///		FK_ZNodeAddOnValue_ZNodeSupplier Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_supplierID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AddOnValue objects.</returns>
		/// <remarks></remarks>
		public TList<AddOnValue> GetBySupplierID(TransactionManager transactionManager, System.Int32? _supplierID)
		{
			int count = -1;
			return GetBySupplierID(transactionManager, _supplierID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAddOnValue_ZNodeSupplier key.
		///		FK_ZNodeAddOnValue_ZNodeSupplier Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_supplierID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AddOnValue objects.</returns>
		public TList<AddOnValue> GetBySupplierID(TransactionManager transactionManager, System.Int32? _supplierID, int start, int pageLength)
		{
			int count = -1;
			return GetBySupplierID(transactionManager, _supplierID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAddOnValue_ZNodeSupplier key.
		///		fKZNodeAddOnValueZNodeSupplier Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_supplierID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AddOnValue objects.</returns>
		public TList<AddOnValue> GetBySupplierID(System.Int32? _supplierID, int start, int pageLength)
		{
			int count =  -1;
			return GetBySupplierID(null, _supplierID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAddOnValue_ZNodeSupplier key.
		///		fKZNodeAddOnValueZNodeSupplier Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_supplierID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AddOnValue objects.</returns>
		public TList<AddOnValue> GetBySupplierID(System.Int32? _supplierID, int start, int pageLength,out int count)
		{
			return GetBySupplierID(null, _supplierID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAddOnValue_ZNodeSupplier key.
		///		FK_ZNodeAddOnValue_ZNodeSupplier Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_supplierID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AddOnValue objects.</returns>
		public abstract TList<AddOnValue> GetBySupplierID(TransactionManager transactionManager, System.Int32? _supplierID, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAddOnValue_ZNodeTaxClass key.
		///		FK_ZNodeAddOnValue_ZNodeTaxClass Description: 
		/// </summary>
		/// <param name="_taxClassID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AddOnValue objects.</returns>
		public TList<AddOnValue> GetByTaxClassID(System.Int32? _taxClassID)
		{
			int count = -1;
			return GetByTaxClassID(_taxClassID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAddOnValue_ZNodeTaxClass key.
		///		FK_ZNodeAddOnValue_ZNodeTaxClass Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_taxClassID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AddOnValue objects.</returns>
		/// <remarks></remarks>
		public TList<AddOnValue> GetByTaxClassID(TransactionManager transactionManager, System.Int32? _taxClassID)
		{
			int count = -1;
			return GetByTaxClassID(transactionManager, _taxClassID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAddOnValue_ZNodeTaxClass key.
		///		FK_ZNodeAddOnValue_ZNodeTaxClass Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_taxClassID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AddOnValue objects.</returns>
		public TList<AddOnValue> GetByTaxClassID(TransactionManager transactionManager, System.Int32? _taxClassID, int start, int pageLength)
		{
			int count = -1;
			return GetByTaxClassID(transactionManager, _taxClassID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAddOnValue_ZNodeTaxClass key.
		///		fKZNodeAddOnValueZNodeTaxClass Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_taxClassID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AddOnValue objects.</returns>
		public TList<AddOnValue> GetByTaxClassID(System.Int32? _taxClassID, int start, int pageLength)
		{
			int count =  -1;
			return GetByTaxClassID(null, _taxClassID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAddOnValue_ZNodeTaxClass key.
		///		fKZNodeAddOnValueZNodeTaxClass Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_taxClassID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AddOnValue objects.</returns>
		public TList<AddOnValue> GetByTaxClassID(System.Int32? _taxClassID, int start, int pageLength,out int count)
		{
			return GetByTaxClassID(null, _taxClassID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAddOnValue_ZNodeTaxClass key.
		///		FK_ZNodeAddOnValue_ZNodeTaxClass Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_taxClassID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AddOnValue objects.</returns>
		public abstract TList<AddOnValue> GetByTaxClassID(TransactionManager transactionManager, System.Int32? _taxClassID, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductAddOnValue_ZNodeProductAddOn key.
		///		FK_ZNodeProductAddOnValue_ZNodeProductAddOn Description: 
		/// </summary>
		/// <param name="_addOnID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AddOnValue objects.</returns>
		public TList<AddOnValue> GetByAddOnID(System.Int32 _addOnID)
		{
			int count = -1;
			return GetByAddOnID(_addOnID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductAddOnValue_ZNodeProductAddOn key.
		///		FK_ZNodeProductAddOnValue_ZNodeProductAddOn Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_addOnID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AddOnValue objects.</returns>
		/// <remarks></remarks>
		public TList<AddOnValue> GetByAddOnID(TransactionManager transactionManager, System.Int32 _addOnID)
		{
			int count = -1;
			return GetByAddOnID(transactionManager, _addOnID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductAddOnValue_ZNodeProductAddOn key.
		///		FK_ZNodeProductAddOnValue_ZNodeProductAddOn Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_addOnID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AddOnValue objects.</returns>
		public TList<AddOnValue> GetByAddOnID(TransactionManager transactionManager, System.Int32 _addOnID, int start, int pageLength)
		{
			int count = -1;
			return GetByAddOnID(transactionManager, _addOnID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductAddOnValue_ZNodeProductAddOn key.
		///		fKZNodeProductAddOnValueZNodeProductAddOn Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_addOnID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AddOnValue objects.</returns>
		public TList<AddOnValue> GetByAddOnID(System.Int32 _addOnID, int start, int pageLength)
		{
			int count =  -1;
			return GetByAddOnID(null, _addOnID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductAddOnValue_ZNodeProductAddOn key.
		///		fKZNodeProductAddOnValueZNodeProductAddOn Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_addOnID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AddOnValue objects.</returns>
		public TList<AddOnValue> GetByAddOnID(System.Int32 _addOnID, int start, int pageLength,out int count)
		{
			return GetByAddOnID(null, _addOnID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductAddOnValue_ZNodeProductAddOn key.
		///		FK_ZNodeProductAddOnValue_ZNodeProductAddOn Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_addOnID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AddOnValue objects.</returns>
		public abstract TList<AddOnValue> GetByAddOnID(TransactionManager transactionManager, System.Int32 _addOnID, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.AddOnValue Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.AddOnValueKey key, int start, int pageLength)
		{
			return GetByAddOnValueID(transactionManager, key.AddOnValueID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeAddOnValue index.
		/// </summary>
		/// <param name="_externalProductID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;AddOnValue&gt;"/> class.</returns>
		public TList<AddOnValue> GetByExternalProductID(System.Int32? _externalProductID)
		{
			int count = -1;
			return GetByExternalProductID(null,_externalProductID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAddOnValue index.
		/// </summary>
		/// <param name="_externalProductID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;AddOnValue&gt;"/> class.</returns>
		public TList<AddOnValue> GetByExternalProductID(System.Int32? _externalProductID, int start, int pageLength)
		{
			int count = -1;
			return GetByExternalProductID(null, _externalProductID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAddOnValue index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_externalProductID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;AddOnValue&gt;"/> class.</returns>
		public TList<AddOnValue> GetByExternalProductID(TransactionManager transactionManager, System.Int32? _externalProductID)
		{
			int count = -1;
			return GetByExternalProductID(transactionManager, _externalProductID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAddOnValue index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_externalProductID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;AddOnValue&gt;"/> class.</returns>
		public TList<AddOnValue> GetByExternalProductID(TransactionManager transactionManager, System.Int32? _externalProductID, int start, int pageLength)
		{
			int count = -1;
			return GetByExternalProductID(transactionManager, _externalProductID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAddOnValue index.
		/// </summary>
		/// <param name="_externalProductID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;AddOnValue&gt;"/> class.</returns>
		public TList<AddOnValue> GetByExternalProductID(System.Int32? _externalProductID, int start, int pageLength, out int count)
		{
			return GetByExternalProductID(null, _externalProductID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAddOnValue index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_externalProductID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;AddOnValue&gt;"/> class.</returns>
		public abstract TList<AddOnValue> GetByExternalProductID(TransactionManager transactionManager, System.Int32? _externalProductID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodeProductAddOnValue index.
		/// </summary>
		/// <param name="_addOnValueID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.AddOnValue"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.AddOnValue GetByAddOnValueID(System.Int32 _addOnValueID)
		{
			int count = -1;
			return GetByAddOnValueID(null,_addOnValueID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductAddOnValue index.
		/// </summary>
		/// <param name="_addOnValueID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.AddOnValue"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.AddOnValue GetByAddOnValueID(System.Int32 _addOnValueID, int start, int pageLength)
		{
			int count = -1;
			return GetByAddOnValueID(null, _addOnValueID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductAddOnValue index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_addOnValueID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.AddOnValue"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.AddOnValue GetByAddOnValueID(TransactionManager transactionManager, System.Int32 _addOnValueID)
		{
			int count = -1;
			return GetByAddOnValueID(transactionManager, _addOnValueID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductAddOnValue index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_addOnValueID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.AddOnValue"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.AddOnValue GetByAddOnValueID(TransactionManager transactionManager, System.Int32 _addOnValueID, int start, int pageLength)
		{
			int count = -1;
			return GetByAddOnValueID(transactionManager, _addOnValueID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductAddOnValue index.
		/// </summary>
		/// <param name="_addOnValueID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.AddOnValue"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.AddOnValue GetByAddOnValueID(System.Int32 _addOnValueID, int start, int pageLength, out int count)
		{
			return GetByAddOnValueID(null, _addOnValueID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductAddOnValue index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_addOnValueID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.AddOnValue"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.AddOnValue GetByAddOnValueID(TransactionManager transactionManager, System.Int32 _addOnValueID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;AddOnValue&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;AddOnValue&gt;"/></returns>
		public static TList<AddOnValue> Fill(IDataReader reader, TList<AddOnValue> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.AddOnValue c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("AddOnValue")
					.Append("|").Append((System.Int32)reader[((int)AddOnValueColumn.AddOnValueID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<AddOnValue>(
					key.ToString(), // EntityTrackingKey
					"AddOnValue",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.AddOnValue();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.AddOnValueID = (System.Int32)reader[((int)AddOnValueColumn.AddOnValueID - 1)];
					c.AddOnID = (System.Int32)reader[((int)AddOnValueColumn.AddOnID - 1)];
					c.Name = (System.String)reader[((int)AddOnValueColumn.Name - 1)];
					c.Description = (System.String)reader[((int)AddOnValueColumn.Description - 1)];
					c.SKU = (System.String)reader[((int)AddOnValueColumn.SKU - 1)];
					c.DefaultInd = (System.Boolean)reader[((int)AddOnValueColumn.DefaultInd - 1)];
					c.DisplayOrder = (System.Int32)reader[((int)AddOnValueColumn.DisplayOrder - 1)];
					c.ImageFile = (System.String)reader[((int)AddOnValueColumn.ImageFile - 1)];
					c.ImageAltTag = (reader.IsDBNull(((int)AddOnValueColumn.ImageAltTag - 1)))?null:(System.String)reader[((int)AddOnValueColumn.ImageAltTag - 1)];
					c.RetailPrice = (System.Decimal)reader[((int)AddOnValueColumn.RetailPrice - 1)];
					c.SalePrice = (reader.IsDBNull(((int)AddOnValueColumn.SalePrice - 1)))?null:(System.Decimal?)reader[((int)AddOnValueColumn.SalePrice - 1)];
					c.WholesalePrice = (reader.IsDBNull(((int)AddOnValueColumn.WholesalePrice - 1)))?null:(System.Decimal?)reader[((int)AddOnValueColumn.WholesalePrice - 1)];
					c.RecurringBillingInd = (System.Boolean)reader[((int)AddOnValueColumn.RecurringBillingInd - 1)];
					c.RecurringBillingInstallmentInd = (System.Boolean)reader[((int)AddOnValueColumn.RecurringBillingInstallmentInd - 1)];
					c.RecurringBillingPeriod = (reader.IsDBNull(((int)AddOnValueColumn.RecurringBillingPeriod - 1)))?null:(System.String)reader[((int)AddOnValueColumn.RecurringBillingPeriod - 1)];
					c.RecurringBillingFrequency = (reader.IsDBNull(((int)AddOnValueColumn.RecurringBillingFrequency - 1)))?null:(System.String)reader[((int)AddOnValueColumn.RecurringBillingFrequency - 1)];
					c.RecurringBillingTotalCycles = (reader.IsDBNull(((int)AddOnValueColumn.RecurringBillingTotalCycles - 1)))?null:(System.Int32?)reader[((int)AddOnValueColumn.RecurringBillingTotalCycles - 1)];
					c.RecurringBillingInitialAmount = (reader.IsDBNull(((int)AddOnValueColumn.RecurringBillingInitialAmount - 1)))?null:(System.Decimal?)reader[((int)AddOnValueColumn.RecurringBillingInitialAmount - 1)];
					c.Weight = (System.Decimal)reader[((int)AddOnValueColumn.Weight - 1)];
					c.Length = (reader.IsDBNull(((int)AddOnValueColumn.Length - 1)))?null:(System.Decimal?)reader[((int)AddOnValueColumn.Length - 1)];
					c.Height = (reader.IsDBNull(((int)AddOnValueColumn.Height - 1)))?null:(System.Decimal?)reader[((int)AddOnValueColumn.Height - 1)];
					c.Width = (reader.IsDBNull(((int)AddOnValueColumn.Width - 1)))?null:(System.Decimal?)reader[((int)AddOnValueColumn.Width - 1)];
					c.ShippingRuleTypeID = (reader.IsDBNull(((int)AddOnValueColumn.ShippingRuleTypeID - 1)))?null:(System.Int32?)reader[((int)AddOnValueColumn.ShippingRuleTypeID - 1)];
					c.FreeShippingInd = (reader.IsDBNull(((int)AddOnValueColumn.FreeShippingInd - 1)))?null:(System.Boolean?)reader[((int)AddOnValueColumn.FreeShippingInd - 1)];
					c.WebServiceDownloadDte = (reader.IsDBNull(((int)AddOnValueColumn.WebServiceDownloadDte - 1)))?null:(System.DateTime?)reader[((int)AddOnValueColumn.WebServiceDownloadDte - 1)];
					c.UpdateDte = (reader.IsDBNull(((int)AddOnValueColumn.UpdateDte - 1)))?null:(System.DateTime?)reader[((int)AddOnValueColumn.UpdateDte - 1)];
					c.SupplierID = (reader.IsDBNull(((int)AddOnValueColumn.SupplierID - 1)))?null:(System.Int32?)reader[((int)AddOnValueColumn.SupplierID - 1)];
					c.TaxClassID = (reader.IsDBNull(((int)AddOnValueColumn.TaxClassID - 1)))?null:(System.Int32?)reader[((int)AddOnValueColumn.TaxClassID - 1)];
					c.ExternalProductID = (reader.IsDBNull(((int)AddOnValueColumn.ExternalProductID - 1)))?null:(System.Int32?)reader[((int)AddOnValueColumn.ExternalProductID - 1)];
					c.ExternalProductAPIID = (reader.IsDBNull(((int)AddOnValueColumn.ExternalProductAPIID - 1)))?null:(System.String)reader[((int)AddOnValueColumn.ExternalProductAPIID - 1)];
					c.ExternalID = (reader.IsDBNull(((int)AddOnValueColumn.ExternalID - 1)))?null:(System.String)reader[((int)AddOnValueColumn.ExternalID - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.AddOnValue"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.AddOnValue"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.AddOnValue entity)
		{
			if (!reader.Read()) return;
			
			entity.AddOnValueID = (System.Int32)reader[((int)AddOnValueColumn.AddOnValueID - 1)];
			entity.AddOnID = (System.Int32)reader[((int)AddOnValueColumn.AddOnID - 1)];
			entity.Name = (System.String)reader[((int)AddOnValueColumn.Name - 1)];
			entity.Description = (System.String)reader[((int)AddOnValueColumn.Description - 1)];
			entity.SKU = (System.String)reader[((int)AddOnValueColumn.SKU - 1)];
			entity.DefaultInd = (System.Boolean)reader[((int)AddOnValueColumn.DefaultInd - 1)];
			entity.DisplayOrder = (System.Int32)reader[((int)AddOnValueColumn.DisplayOrder - 1)];
			entity.ImageFile = (System.String)reader[((int)AddOnValueColumn.ImageFile - 1)];
			entity.ImageAltTag = (reader.IsDBNull(((int)AddOnValueColumn.ImageAltTag - 1)))?null:(System.String)reader[((int)AddOnValueColumn.ImageAltTag - 1)];
			entity.RetailPrice = (System.Decimal)reader[((int)AddOnValueColumn.RetailPrice - 1)];
			entity.SalePrice = (reader.IsDBNull(((int)AddOnValueColumn.SalePrice - 1)))?null:(System.Decimal?)reader[((int)AddOnValueColumn.SalePrice - 1)];
			entity.WholesalePrice = (reader.IsDBNull(((int)AddOnValueColumn.WholesalePrice - 1)))?null:(System.Decimal?)reader[((int)AddOnValueColumn.WholesalePrice - 1)];
			entity.RecurringBillingInd = (System.Boolean)reader[((int)AddOnValueColumn.RecurringBillingInd - 1)];
			entity.RecurringBillingInstallmentInd = (System.Boolean)reader[((int)AddOnValueColumn.RecurringBillingInstallmentInd - 1)];
			entity.RecurringBillingPeriod = (reader.IsDBNull(((int)AddOnValueColumn.RecurringBillingPeriod - 1)))?null:(System.String)reader[((int)AddOnValueColumn.RecurringBillingPeriod - 1)];
			entity.RecurringBillingFrequency = (reader.IsDBNull(((int)AddOnValueColumn.RecurringBillingFrequency - 1)))?null:(System.String)reader[((int)AddOnValueColumn.RecurringBillingFrequency - 1)];
			entity.RecurringBillingTotalCycles = (reader.IsDBNull(((int)AddOnValueColumn.RecurringBillingTotalCycles - 1)))?null:(System.Int32?)reader[((int)AddOnValueColumn.RecurringBillingTotalCycles - 1)];
			entity.RecurringBillingInitialAmount = (reader.IsDBNull(((int)AddOnValueColumn.RecurringBillingInitialAmount - 1)))?null:(System.Decimal?)reader[((int)AddOnValueColumn.RecurringBillingInitialAmount - 1)];
			entity.Weight = (System.Decimal)reader[((int)AddOnValueColumn.Weight - 1)];
			entity.Length = (reader.IsDBNull(((int)AddOnValueColumn.Length - 1)))?null:(System.Decimal?)reader[((int)AddOnValueColumn.Length - 1)];
			entity.Height = (reader.IsDBNull(((int)AddOnValueColumn.Height - 1)))?null:(System.Decimal?)reader[((int)AddOnValueColumn.Height - 1)];
			entity.Width = (reader.IsDBNull(((int)AddOnValueColumn.Width - 1)))?null:(System.Decimal?)reader[((int)AddOnValueColumn.Width - 1)];
			entity.ShippingRuleTypeID = (reader.IsDBNull(((int)AddOnValueColumn.ShippingRuleTypeID - 1)))?null:(System.Int32?)reader[((int)AddOnValueColumn.ShippingRuleTypeID - 1)];
			entity.FreeShippingInd = (reader.IsDBNull(((int)AddOnValueColumn.FreeShippingInd - 1)))?null:(System.Boolean?)reader[((int)AddOnValueColumn.FreeShippingInd - 1)];
			entity.WebServiceDownloadDte = (reader.IsDBNull(((int)AddOnValueColumn.WebServiceDownloadDte - 1)))?null:(System.DateTime?)reader[((int)AddOnValueColumn.WebServiceDownloadDte - 1)];
			entity.UpdateDte = (reader.IsDBNull(((int)AddOnValueColumn.UpdateDte - 1)))?null:(System.DateTime?)reader[((int)AddOnValueColumn.UpdateDte - 1)];
			entity.SupplierID = (reader.IsDBNull(((int)AddOnValueColumn.SupplierID - 1)))?null:(System.Int32?)reader[((int)AddOnValueColumn.SupplierID - 1)];
			entity.TaxClassID = (reader.IsDBNull(((int)AddOnValueColumn.TaxClassID - 1)))?null:(System.Int32?)reader[((int)AddOnValueColumn.TaxClassID - 1)];
			entity.ExternalProductID = (reader.IsDBNull(((int)AddOnValueColumn.ExternalProductID - 1)))?null:(System.Int32?)reader[((int)AddOnValueColumn.ExternalProductID - 1)];
			entity.ExternalProductAPIID = (reader.IsDBNull(((int)AddOnValueColumn.ExternalProductAPIID - 1)))?null:(System.String)reader[((int)AddOnValueColumn.ExternalProductAPIID - 1)];
			entity.ExternalID = (reader.IsDBNull(((int)AddOnValueColumn.ExternalID - 1)))?null:(System.String)reader[((int)AddOnValueColumn.ExternalID - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.AddOnValue"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.AddOnValue"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.AddOnValue entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.AddOnValueID = (System.Int32)dataRow["AddOnValueID"];
			entity.AddOnID = (System.Int32)dataRow["AddOnID"];
			entity.Name = (System.String)dataRow["Name"];
			entity.Description = (System.String)dataRow["Description"];
			entity.SKU = (System.String)dataRow["SKU"];
			entity.DefaultInd = (System.Boolean)dataRow["DefaultInd"];
			entity.DisplayOrder = (System.Int32)dataRow["DisplayOrder"];
			entity.ImageFile = (System.String)dataRow["ImageFile"];
			entity.ImageAltTag = Convert.IsDBNull(dataRow["ImageAltTag"]) ? null : (System.String)dataRow["ImageAltTag"];
			entity.RetailPrice = (System.Decimal)dataRow["RetailPrice"];
			entity.SalePrice = Convert.IsDBNull(dataRow["SalePrice"]) ? null : (System.Decimal?)dataRow["SalePrice"];
			entity.WholesalePrice = Convert.IsDBNull(dataRow["WholesalePrice"]) ? null : (System.Decimal?)dataRow["WholesalePrice"];
			entity.RecurringBillingInd = (System.Boolean)dataRow["RecurringBillingInd"];
			entity.RecurringBillingInstallmentInd = (System.Boolean)dataRow["RecurringBillingInstallmentInd"];
			entity.RecurringBillingPeriod = Convert.IsDBNull(dataRow["RecurringBillingPeriod"]) ? null : (System.String)dataRow["RecurringBillingPeriod"];
			entity.RecurringBillingFrequency = Convert.IsDBNull(dataRow["RecurringBillingFrequency"]) ? null : (System.String)dataRow["RecurringBillingFrequency"];
			entity.RecurringBillingTotalCycles = Convert.IsDBNull(dataRow["RecurringBillingTotalCycles"]) ? null : (System.Int32?)dataRow["RecurringBillingTotalCycles"];
			entity.RecurringBillingInitialAmount = Convert.IsDBNull(dataRow["RecurringBillingInitialAmount"]) ? null : (System.Decimal?)dataRow["RecurringBillingInitialAmount"];
			entity.Weight = (System.Decimal)dataRow["Weight"];
			entity.Length = Convert.IsDBNull(dataRow["Length"]) ? null : (System.Decimal?)dataRow["Length"];
			entity.Height = Convert.IsDBNull(dataRow["Height"]) ? null : (System.Decimal?)dataRow["Height"];
			entity.Width = Convert.IsDBNull(dataRow["Width"]) ? null : (System.Decimal?)dataRow["Width"];
			entity.ShippingRuleTypeID = Convert.IsDBNull(dataRow["ShippingRuleTypeID"]) ? null : (System.Int32?)dataRow["ShippingRuleTypeID"];
			entity.FreeShippingInd = Convert.IsDBNull(dataRow["FreeShippingInd"]) ? null : (System.Boolean?)dataRow["FreeShippingInd"];
			entity.WebServiceDownloadDte = Convert.IsDBNull(dataRow["WebServiceDownloadDte"]) ? null : (System.DateTime?)dataRow["WebServiceDownloadDte"];
			entity.UpdateDte = Convert.IsDBNull(dataRow["UpdateDte"]) ? null : (System.DateTime?)dataRow["UpdateDte"];
			entity.SupplierID = Convert.IsDBNull(dataRow["SupplierID"]) ? null : (System.Int32?)dataRow["SupplierID"];
			entity.TaxClassID = Convert.IsDBNull(dataRow["TaxClassID"]) ? null : (System.Int32?)dataRow["TaxClassID"];
			entity.ExternalProductID = Convert.IsDBNull(dataRow["ExternalProductID"]) ? null : (System.Int32?)dataRow["ExternalProductID"];
			entity.ExternalProductAPIID = Convert.IsDBNull(dataRow["ExternalProductAPIID"]) ? null : (System.String)dataRow["ExternalProductAPIID"];
			entity.ExternalID = Convert.IsDBNull(dataRow["ExternalID"]) ? null : (System.String)dataRow["ExternalID"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.AddOnValue"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.AddOnValue Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.AddOnValue entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region ShippingRuleTypeIDSource	
			if (CanDeepLoad(entity, "ShippingRuleType|ShippingRuleTypeIDSource", deepLoadType, innerList) 
				&& entity.ShippingRuleTypeIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.ShippingRuleTypeID ?? (int)0);
				ShippingRuleType tmpEntity = EntityManager.LocateEntity<ShippingRuleType>(EntityLocator.ConstructKeyFromPkItems(typeof(ShippingRuleType), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ShippingRuleTypeIDSource = tmpEntity;
				else
					entity.ShippingRuleTypeIDSource = DataRepository.ShippingRuleTypeProvider.GetByShippingRuleTypeID(transactionManager, (entity.ShippingRuleTypeID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ShippingRuleTypeIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ShippingRuleTypeIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.ShippingRuleTypeProvider.DeepLoad(transactionManager, entity.ShippingRuleTypeIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ShippingRuleTypeIDSource

			#region SKUSource	
			if (CanDeepLoad(entity, "SKUInventory|SKUSource", deepLoadType, innerList) 
				&& entity.SKUSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.SKU;
				SKUInventory tmpEntity = EntityManager.LocateEntity<SKUInventory>(EntityLocator.ConstructKeyFromPkItems(typeof(SKUInventory), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.SKUSource = tmpEntity;
				else
					entity.SKUSource = DataRepository.SKUInventoryProvider.GetBySKU(transactionManager, entity.SKU);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SKUSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.SKUSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.SKUInventoryProvider.DeepLoad(transactionManager, entity.SKUSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion SKUSource

			#region SupplierIDSource	
			if (CanDeepLoad(entity, "Supplier|SupplierIDSource", deepLoadType, innerList) 
				&& entity.SupplierIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.SupplierID ?? (int)0);
				Supplier tmpEntity = EntityManager.LocateEntity<Supplier>(EntityLocator.ConstructKeyFromPkItems(typeof(Supplier), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.SupplierIDSource = tmpEntity;
				else
					entity.SupplierIDSource = DataRepository.SupplierProvider.GetBySupplierID(transactionManager, (entity.SupplierID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SupplierIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.SupplierIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.SupplierProvider.DeepLoad(transactionManager, entity.SupplierIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion SupplierIDSource

			#region TaxClassIDSource	
			if (CanDeepLoad(entity, "TaxClass|TaxClassIDSource", deepLoadType, innerList) 
				&& entity.TaxClassIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.TaxClassID ?? (int)0);
				TaxClass tmpEntity = EntityManager.LocateEntity<TaxClass>(EntityLocator.ConstructKeyFromPkItems(typeof(TaxClass), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.TaxClassIDSource = tmpEntity;
				else
					entity.TaxClassIDSource = DataRepository.TaxClassProvider.GetByTaxClassID(transactionManager, (entity.TaxClassID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'TaxClassIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.TaxClassIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.TaxClassProvider.DeepLoad(transactionManager, entity.TaxClassIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion TaxClassIDSource

			#region AddOnIDSource	
			if (CanDeepLoad(entity, "AddOn|AddOnIDSource", deepLoadType, innerList) 
				&& entity.AddOnIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.AddOnID;
				AddOn tmpEntity = EntityManager.LocateEntity<AddOn>(EntityLocator.ConstructKeyFromPkItems(typeof(AddOn), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.AddOnIDSource = tmpEntity;
				else
					entity.AddOnIDSource = DataRepository.AddOnProvider.GetByAddOnID(transactionManager, entity.AddOnID);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AddOnIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.AddOnIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.AddOnProvider.DeepLoad(transactionManager, entity.AddOnIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion AddOnIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByAddOnValueID methods when available
			
			#region PromotionCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Promotion>|PromotionCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PromotionCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.PromotionCollection = DataRepository.PromotionProvider.GetByAddOnValueID(transactionManager, entity.AddOnValueID);

				if (deep && entity.PromotionCollection.Count > 0)
				{
					deepHandles.Add("PromotionCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Promotion>) DataRepository.PromotionProvider.DeepLoad,
						new object[] { transactionManager, entity.PromotionCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.AddOnValue object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.AddOnValue instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.AddOnValue Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.AddOnValue entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region ShippingRuleTypeIDSource
			if (CanDeepSave(entity, "ShippingRuleType|ShippingRuleTypeIDSource", deepSaveType, innerList) 
				&& entity.ShippingRuleTypeIDSource != null)
			{
				DataRepository.ShippingRuleTypeProvider.Save(transactionManager, entity.ShippingRuleTypeIDSource);
				entity.ShippingRuleTypeID = entity.ShippingRuleTypeIDSource.ShippingRuleTypeID;
			}
			#endregion 
			
			#region SKUSource
			if (CanDeepSave(entity, "SKUInventory|SKUSource", deepSaveType, innerList) 
				&& entity.SKUSource != null)
			{
				DataRepository.SKUInventoryProvider.Save(transactionManager, entity.SKUSource);
				entity.SKU = entity.SKUSource.SKU;
			}
			#endregion 
			
			#region SupplierIDSource
			if (CanDeepSave(entity, "Supplier|SupplierIDSource", deepSaveType, innerList) 
				&& entity.SupplierIDSource != null)
			{
				DataRepository.SupplierProvider.Save(transactionManager, entity.SupplierIDSource);
				entity.SupplierID = entity.SupplierIDSource.SupplierID;
			}
			#endregion 
			
			#region TaxClassIDSource
			if (CanDeepSave(entity, "TaxClass|TaxClassIDSource", deepSaveType, innerList) 
				&& entity.TaxClassIDSource != null)
			{
				DataRepository.TaxClassProvider.Save(transactionManager, entity.TaxClassIDSource);
				entity.TaxClassID = entity.TaxClassIDSource.TaxClassID;
			}
			#endregion 
			
			#region AddOnIDSource
			if (CanDeepSave(entity, "AddOn|AddOnIDSource", deepSaveType, innerList) 
				&& entity.AddOnIDSource != null)
			{
				DataRepository.AddOnProvider.Save(transactionManager, entity.AddOnIDSource);
				entity.AddOnID = entity.AddOnIDSource.AddOnID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<Promotion>
				if (CanDeepSave(entity.PromotionCollection, "List<Promotion>|PromotionCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Promotion child in entity.PromotionCollection)
					{
						if(child.AddOnValueIDSource != null)
						{
							child.AddOnValueID = child.AddOnValueIDSource.AddOnValueID;
						}
						else
						{
							child.AddOnValueID = entity.AddOnValueID;
						}

					}

					if (entity.PromotionCollection.Count > 0 || entity.PromotionCollection.DeletedItems.Count > 0)
					{
						//DataRepository.PromotionProvider.Save(transactionManager, entity.PromotionCollection);
						
						deepHandles.Add("PromotionCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Promotion >) DataRepository.PromotionProvider.DeepSave,
							new object[] { transactionManager, entity.PromotionCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region AddOnValueChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.AddOnValue</c>
	///</summary>
	public enum AddOnValueChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>ShippingRuleType</c> at ShippingRuleTypeIDSource
		///</summary>
		[ChildEntityType(typeof(ShippingRuleType))]
		ShippingRuleType,
		
		///<summary>
		/// Composite Property for <c>SKUInventory</c> at SKUSource
		///</summary>
		[ChildEntityType(typeof(SKUInventory))]
		SKUInventory,
		
		///<summary>
		/// Composite Property for <c>Supplier</c> at SupplierIDSource
		///</summary>
		[ChildEntityType(typeof(Supplier))]
		Supplier,
		
		///<summary>
		/// Composite Property for <c>TaxClass</c> at TaxClassIDSource
		///</summary>
		[ChildEntityType(typeof(TaxClass))]
		TaxClass,
		
		///<summary>
		/// Composite Property for <c>AddOn</c> at AddOnIDSource
		///</summary>
		[ChildEntityType(typeof(AddOn))]
		AddOn,
		///<summary>
		/// Collection of <c>AddOnValue</c> as OneToMany for PromotionCollection
		///</summary>
		[ChildEntityType(typeof(TList<Promotion>))]
		PromotionCollection,
	}
	
	#endregion AddOnValueChildEntityTypes
	
	#region AddOnValueFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;AddOnValueColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AddOnValue"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AddOnValueFilterBuilder : SqlFilterBuilder<AddOnValueColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AddOnValueFilterBuilder class.
		/// </summary>
		public AddOnValueFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the AddOnValueFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AddOnValueFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AddOnValueFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AddOnValueFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AddOnValueFilterBuilder
	
	#region AddOnValueParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;AddOnValueColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AddOnValue"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AddOnValueParameterBuilder : ParameterizedSqlFilterBuilder<AddOnValueColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AddOnValueParameterBuilder class.
		/// </summary>
		public AddOnValueParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the AddOnValueParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AddOnValueParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AddOnValueParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AddOnValueParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AddOnValueParameterBuilder
	
	#region AddOnValueSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;AddOnValueColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AddOnValue"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class AddOnValueSortBuilder : SqlSortBuilder<AddOnValueColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AddOnValueSqlSortBuilder class.
		/// </summary>
		public AddOnValueSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion AddOnValueSortBuilder
	
} // end namespace
