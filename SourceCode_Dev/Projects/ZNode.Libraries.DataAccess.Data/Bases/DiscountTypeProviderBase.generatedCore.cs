﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="DiscountTypeProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class DiscountTypeProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.DiscountType, ZNode.Libraries.DataAccess.Entities.DiscountTypeKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.DiscountTypeKey key)
		{
			return Delete(transactionManager, key.DiscountTypeID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_discountTypeID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _discountTypeID)
		{
			return Delete(null, _discountTypeID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_discountTypeID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _discountTypeID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.DiscountType Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.DiscountTypeKey key, int start, int pageLength)
		{
			return GetByDiscountTypeID(transactionManager, key.DiscountTypeID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeDiscountType_ActiveInd index.
		/// </summary>
		/// <param name="_activeInd"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;DiscountType&gt;"/> class.</returns>
		public TList<DiscountType> GetByActiveInd(System.Boolean _activeInd)
		{
			int count = -1;
			return GetByActiveInd(null,_activeInd, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeDiscountType_ActiveInd index.
		/// </summary>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;DiscountType&gt;"/> class.</returns>
		public TList<DiscountType> GetByActiveInd(System.Boolean _activeInd, int start, int pageLength)
		{
			int count = -1;
			return GetByActiveInd(null, _activeInd, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeDiscountType_ActiveInd index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_activeInd"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;DiscountType&gt;"/> class.</returns>
		public TList<DiscountType> GetByActiveInd(TransactionManager transactionManager, System.Boolean _activeInd)
		{
			int count = -1;
			return GetByActiveInd(transactionManager, _activeInd, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeDiscountType_ActiveInd index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;DiscountType&gt;"/> class.</returns>
		public TList<DiscountType> GetByActiveInd(TransactionManager transactionManager, System.Boolean _activeInd, int start, int pageLength)
		{
			int count = -1;
			return GetByActiveInd(transactionManager, _activeInd, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeDiscountType_ActiveInd index.
		/// </summary>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;DiscountType&gt;"/> class.</returns>
		public TList<DiscountType> GetByActiveInd(System.Boolean _activeInd, int start, int pageLength, out int count)
		{
			return GetByActiveInd(null, _activeInd, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeDiscountType_ActiveInd index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;DiscountType&gt;"/> class.</returns>
		public abstract TList<DiscountType> GetByActiveInd(TransactionManager transactionManager, System.Boolean _activeInd, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeDiscountType_ClassName index.
		/// </summary>
		/// <param name="_className"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;DiscountType&gt;"/> class.</returns>
		public TList<DiscountType> GetByClassName(System.String _className)
		{
			int count = -1;
			return GetByClassName(null,_className, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeDiscountType_ClassName index.
		/// </summary>
		/// <param name="_className"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;DiscountType&gt;"/> class.</returns>
		public TList<DiscountType> GetByClassName(System.String _className, int start, int pageLength)
		{
			int count = -1;
			return GetByClassName(null, _className, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeDiscountType_ClassName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_className"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;DiscountType&gt;"/> class.</returns>
		public TList<DiscountType> GetByClassName(TransactionManager transactionManager, System.String _className)
		{
			int count = -1;
			return GetByClassName(transactionManager, _className, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeDiscountType_ClassName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_className"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;DiscountType&gt;"/> class.</returns>
		public TList<DiscountType> GetByClassName(TransactionManager transactionManager, System.String _className, int start, int pageLength)
		{
			int count = -1;
			return GetByClassName(transactionManager, _className, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeDiscountType_ClassName index.
		/// </summary>
		/// <param name="_className"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;DiscountType&gt;"/> class.</returns>
		public TList<DiscountType> GetByClassName(System.String _className, int start, int pageLength, out int count)
		{
			return GetByClassName(null, _className, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeDiscountType_ClassName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_className"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;DiscountType&gt;"/> class.</returns>
		public abstract TList<DiscountType> GetByClassName(TransactionManager transactionManager, System.String _className, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeDiscountType_ClassType index.
		/// </summary>
		/// <param name="_classType"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;DiscountType&gt;"/> class.</returns>
		public TList<DiscountType> GetByClassType(System.String _classType)
		{
			int count = -1;
			return GetByClassType(null,_classType, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeDiscountType_ClassType index.
		/// </summary>
		/// <param name="_classType"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;DiscountType&gt;"/> class.</returns>
		public TList<DiscountType> GetByClassType(System.String _classType, int start, int pageLength)
		{
			int count = -1;
			return GetByClassType(null, _classType, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeDiscountType_ClassType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_classType"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;DiscountType&gt;"/> class.</returns>
		public TList<DiscountType> GetByClassType(TransactionManager transactionManager, System.String _classType)
		{
			int count = -1;
			return GetByClassType(transactionManager, _classType, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeDiscountType_ClassType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_classType"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;DiscountType&gt;"/> class.</returns>
		public TList<DiscountType> GetByClassType(TransactionManager transactionManager, System.String _classType, int start, int pageLength)
		{
			int count = -1;
			return GetByClassType(transactionManager, _classType, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeDiscountType_ClassType index.
		/// </summary>
		/// <param name="_classType"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;DiscountType&gt;"/> class.</returns>
		public TList<DiscountType> GetByClassType(System.String _classType, int start, int pageLength, out int count)
		{
			return GetByClassType(null, _classType, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeDiscountType_ClassType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_classType"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;DiscountType&gt;"/> class.</returns>
		public abstract TList<DiscountType> GetByClassType(TransactionManager transactionManager, System.String _classType, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodeDiscountType index.
		/// </summary>
		/// <param name="_discountTypeID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.DiscountType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.DiscountType GetByDiscountTypeID(System.Int32 _discountTypeID)
		{
			int count = -1;
			return GetByDiscountTypeID(null,_discountTypeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeDiscountType index.
		/// </summary>
		/// <param name="_discountTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.DiscountType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.DiscountType GetByDiscountTypeID(System.Int32 _discountTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetByDiscountTypeID(null, _discountTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeDiscountType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_discountTypeID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.DiscountType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.DiscountType GetByDiscountTypeID(TransactionManager transactionManager, System.Int32 _discountTypeID)
		{
			int count = -1;
			return GetByDiscountTypeID(transactionManager, _discountTypeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeDiscountType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_discountTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.DiscountType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.DiscountType GetByDiscountTypeID(TransactionManager transactionManager, System.Int32 _discountTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetByDiscountTypeID(transactionManager, _discountTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeDiscountType index.
		/// </summary>
		/// <param name="_discountTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.DiscountType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.DiscountType GetByDiscountTypeID(System.Int32 _discountTypeID, int start, int pageLength, out int count)
		{
			return GetByDiscountTypeID(null, _discountTypeID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeDiscountType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_discountTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.DiscountType"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.DiscountType GetByDiscountTypeID(TransactionManager transactionManager, System.Int32 _discountTypeID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;DiscountType&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;DiscountType&gt;"/></returns>
		public static TList<DiscountType> Fill(IDataReader reader, TList<DiscountType> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.DiscountType c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("DiscountType")
					.Append("|").Append((System.Int32)reader[((int)DiscountTypeColumn.DiscountTypeID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<DiscountType>(
					key.ToString(), // EntityTrackingKey
					"DiscountType",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.DiscountType();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.DiscountTypeID = (System.Int32)reader[((int)DiscountTypeColumn.DiscountTypeID - 1)];
					c.ClassType = (reader.IsDBNull(((int)DiscountTypeColumn.ClassType - 1)))?null:(System.String)reader[((int)DiscountTypeColumn.ClassType - 1)];
					c.ClassName = (reader.IsDBNull(((int)DiscountTypeColumn.ClassName - 1)))?null:(System.String)reader[((int)DiscountTypeColumn.ClassName - 1)];
					c.Name = (reader.IsDBNull(((int)DiscountTypeColumn.Name - 1)))?null:(System.String)reader[((int)DiscountTypeColumn.Name - 1)];
					c.Description = (reader.IsDBNull(((int)DiscountTypeColumn.Description - 1)))?null:(System.String)reader[((int)DiscountTypeColumn.Description - 1)];
					c.ActiveInd = (System.Boolean)reader[((int)DiscountTypeColumn.ActiveInd - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.DiscountType"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.DiscountType"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.DiscountType entity)
		{
			if (!reader.Read()) return;
			
			entity.DiscountTypeID = (System.Int32)reader[((int)DiscountTypeColumn.DiscountTypeID - 1)];
			entity.ClassType = (reader.IsDBNull(((int)DiscountTypeColumn.ClassType - 1)))?null:(System.String)reader[((int)DiscountTypeColumn.ClassType - 1)];
			entity.ClassName = (reader.IsDBNull(((int)DiscountTypeColumn.ClassName - 1)))?null:(System.String)reader[((int)DiscountTypeColumn.ClassName - 1)];
			entity.Name = (reader.IsDBNull(((int)DiscountTypeColumn.Name - 1)))?null:(System.String)reader[((int)DiscountTypeColumn.Name - 1)];
			entity.Description = (reader.IsDBNull(((int)DiscountTypeColumn.Description - 1)))?null:(System.String)reader[((int)DiscountTypeColumn.Description - 1)];
			entity.ActiveInd = (System.Boolean)reader[((int)DiscountTypeColumn.ActiveInd - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.DiscountType"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.DiscountType"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.DiscountType entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.DiscountTypeID = (System.Int32)dataRow["DiscountTypeID"];
			entity.ClassType = Convert.IsDBNull(dataRow["ClassType"]) ? null : (System.String)dataRow["ClassType"];
			entity.ClassName = Convert.IsDBNull(dataRow["ClassName"]) ? null : (System.String)dataRow["ClassName"];
			entity.Name = Convert.IsDBNull(dataRow["Name"]) ? null : (System.String)dataRow["Name"];
			entity.Description = Convert.IsDBNull(dataRow["Description"]) ? null : (System.String)dataRow["Description"];
			entity.ActiveInd = (System.Boolean)dataRow["ActiveInd"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.DiscountType"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.DiscountType Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.DiscountType entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByDiscountTypeID methods when available
			
			#region PromotionCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Promotion>|PromotionCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PromotionCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.PromotionCollection = DataRepository.PromotionProvider.GetByDiscountTypeID(transactionManager, entity.DiscountTypeID);

				if (deep && entity.PromotionCollection.Count > 0)
				{
					deepHandles.Add("PromotionCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Promotion>) DataRepository.PromotionProvider.DeepLoad,
						new object[] { transactionManager, entity.PromotionCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.DiscountType object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.DiscountType instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.DiscountType Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.DiscountType entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<Promotion>
				if (CanDeepSave(entity.PromotionCollection, "List<Promotion>|PromotionCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Promotion child in entity.PromotionCollection)
					{
						if(child.DiscountTypeIDSource != null)
						{
							child.DiscountTypeID = child.DiscountTypeIDSource.DiscountTypeID;
						}
						else
						{
							child.DiscountTypeID = entity.DiscountTypeID;
						}

					}

					if (entity.PromotionCollection.Count > 0 || entity.PromotionCollection.DeletedItems.Count > 0)
					{
						//DataRepository.PromotionProvider.Save(transactionManager, entity.PromotionCollection);
						
						deepHandles.Add("PromotionCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Promotion >) DataRepository.PromotionProvider.DeepSave,
							new object[] { transactionManager, entity.PromotionCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region DiscountTypeChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.DiscountType</c>
	///</summary>
	public enum DiscountTypeChildEntityTypes
	{
		///<summary>
		/// Collection of <c>DiscountType</c> as OneToMany for PromotionCollection
		///</summary>
		[ChildEntityType(typeof(TList<Promotion>))]
		PromotionCollection,
	}
	
	#endregion DiscountTypeChildEntityTypes
	
	#region DiscountTypeFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;DiscountTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="DiscountType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class DiscountTypeFilterBuilder : SqlFilterBuilder<DiscountTypeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the DiscountTypeFilterBuilder class.
		/// </summary>
		public DiscountTypeFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the DiscountTypeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public DiscountTypeFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the DiscountTypeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public DiscountTypeFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion DiscountTypeFilterBuilder
	
	#region DiscountTypeParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;DiscountTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="DiscountType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class DiscountTypeParameterBuilder : ParameterizedSqlFilterBuilder<DiscountTypeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the DiscountTypeParameterBuilder class.
		/// </summary>
		public DiscountTypeParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the DiscountTypeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public DiscountTypeParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the DiscountTypeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public DiscountTypeParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion DiscountTypeParameterBuilder
	
	#region DiscountTypeSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;DiscountTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="DiscountType"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class DiscountTypeSortBuilder : SqlSortBuilder<DiscountTypeColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the DiscountTypeSqlSortBuilder class.
		/// </summary>
		public DiscountTypeSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion DiscountTypeSortBuilder
	
} // end namespace
