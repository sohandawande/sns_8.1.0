﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="CurrencyTypeProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class CurrencyTypeProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.CurrencyType, ZNode.Libraries.DataAccess.Entities.CurrencyTypeKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.CurrencyTypeKey key)
		{
			return Delete(transactionManager, key.CurrencyTypeID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_currencyTypeID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _currencyTypeID)
		{
			return Delete(null, _currencyTypeID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_currencyTypeID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _currencyTypeID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.CurrencyType Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.CurrencyTypeKey key, int start, int pageLength)
		{
			return GetByCurrencyTypeID(transactionManager, key.CurrencyTypeID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodeCurrencyType index.
		/// </summary>
		/// <param name="_currencyTypeID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.CurrencyType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.CurrencyType GetByCurrencyTypeID(System.Int32 _currencyTypeID)
		{
			int count = -1;
			return GetByCurrencyTypeID(null,_currencyTypeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeCurrencyType index.
		/// </summary>
		/// <param name="_currencyTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.CurrencyType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.CurrencyType GetByCurrencyTypeID(System.Int32 _currencyTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetByCurrencyTypeID(null, _currencyTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeCurrencyType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_currencyTypeID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.CurrencyType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.CurrencyType GetByCurrencyTypeID(TransactionManager transactionManager, System.Int32 _currencyTypeID)
		{
			int count = -1;
			return GetByCurrencyTypeID(transactionManager, _currencyTypeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeCurrencyType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_currencyTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.CurrencyType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.CurrencyType GetByCurrencyTypeID(TransactionManager transactionManager, System.Int32 _currencyTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetByCurrencyTypeID(transactionManager, _currencyTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeCurrencyType index.
		/// </summary>
		/// <param name="_currencyTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.CurrencyType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.CurrencyType GetByCurrencyTypeID(System.Int32 _currencyTypeID, int start, int pageLength, out int count)
		{
			return GetByCurrencyTypeID(null, _currencyTypeID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeCurrencyType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_currencyTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.CurrencyType"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.CurrencyType GetByCurrencyTypeID(TransactionManager transactionManager, System.Int32 _currencyTypeID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;CurrencyType&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;CurrencyType&gt;"/></returns>
		public static TList<CurrencyType> Fill(IDataReader reader, TList<CurrencyType> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.CurrencyType c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("CurrencyType")
					.Append("|").Append((System.Int32)reader[((int)CurrencyTypeColumn.CurrencyTypeID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<CurrencyType>(
					key.ToString(), // EntityTrackingKey
					"CurrencyType",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.CurrencyType();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.CurrencyTypeID = (System.Int32)reader[((int)CurrencyTypeColumn.CurrencyTypeID - 1)];
					c.OriginalCurrencyTypeID = c.CurrencyTypeID;
					c.Name = (System.String)reader[((int)CurrencyTypeColumn.Name - 1)];
					c.Description = (reader.IsDBNull(((int)CurrencyTypeColumn.Description - 1)))?null:(System.String)reader[((int)CurrencyTypeColumn.Description - 1)];
					c.CurrencySuffix = (reader.IsDBNull(((int)CurrencyTypeColumn.CurrencySuffix - 1)))?null:(System.String)reader[((int)CurrencyTypeColumn.CurrencySuffix - 1)];
					c.Symbol = (reader.IsDBNull(((int)CurrencyTypeColumn.Symbol - 1)))?null:(System.String)reader[((int)CurrencyTypeColumn.Symbol - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.CurrencyType"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.CurrencyType"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.CurrencyType entity)
		{
			if (!reader.Read()) return;
			
			entity.CurrencyTypeID = (System.Int32)reader[((int)CurrencyTypeColumn.CurrencyTypeID - 1)];
			entity.OriginalCurrencyTypeID = (System.Int32)reader["CurrencyTypeID"];
			entity.Name = (System.String)reader[((int)CurrencyTypeColumn.Name - 1)];
			entity.Description = (reader.IsDBNull(((int)CurrencyTypeColumn.Description - 1)))?null:(System.String)reader[((int)CurrencyTypeColumn.Description - 1)];
			entity.CurrencySuffix = (reader.IsDBNull(((int)CurrencyTypeColumn.CurrencySuffix - 1)))?null:(System.String)reader[((int)CurrencyTypeColumn.CurrencySuffix - 1)];
			entity.Symbol = (reader.IsDBNull(((int)CurrencyTypeColumn.Symbol - 1)))?null:(System.String)reader[((int)CurrencyTypeColumn.Symbol - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.CurrencyType"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.CurrencyType"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.CurrencyType entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.CurrencyTypeID = (System.Int32)dataRow["CurrencyTypeID"];
			entity.OriginalCurrencyTypeID = (System.Int32)dataRow["CurrencyTypeID"];
			entity.Name = (System.String)dataRow["Name"];
			entity.Description = Convert.IsDBNull(dataRow["Description"]) ? null : (System.String)dataRow["Description"];
			entity.CurrencySuffix = Convert.IsDBNull(dataRow["CurrencySuffix"]) ? null : (System.String)dataRow["CurrencySuffix"];
			entity.Symbol = Convert.IsDBNull(dataRow["symbol"]) ? null : (System.String)dataRow["symbol"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.CurrencyType"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.CurrencyType Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.CurrencyType entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByCurrencyTypeID methods when available
			
			#region PortalCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Portal>|PortalCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PortalCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.PortalCollection = DataRepository.PortalProvider.GetByCurrencyTypeID(transactionManager, entity.CurrencyTypeID);

				if (deep && entity.PortalCollection.Count > 0)
				{
					deepHandles.Add("PortalCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Portal>) DataRepository.PortalProvider.DeepLoad,
						new object[] { transactionManager, entity.PortalCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.CurrencyType object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.CurrencyType instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.CurrencyType Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.CurrencyType entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<Portal>
				if (CanDeepSave(entity.PortalCollection, "List<Portal>|PortalCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Portal child in entity.PortalCollection)
					{
						if(child.CurrencyTypeIDSource != null)
						{
							child.CurrencyTypeID = child.CurrencyTypeIDSource.CurrencyTypeID;
						}
						else
						{
							child.CurrencyTypeID = entity.CurrencyTypeID;
						}

					}

					if (entity.PortalCollection.Count > 0 || entity.PortalCollection.DeletedItems.Count > 0)
					{
						//DataRepository.PortalProvider.Save(transactionManager, entity.PortalCollection);
						
						deepHandles.Add("PortalCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Portal >) DataRepository.PortalProvider.DeepSave,
							new object[] { transactionManager, entity.PortalCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region CurrencyTypeChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.CurrencyType</c>
	///</summary>
	public enum CurrencyTypeChildEntityTypes
	{
		///<summary>
		/// Collection of <c>CurrencyType</c> as OneToMany for PortalCollection
		///</summary>
		[ChildEntityType(typeof(TList<Portal>))]
		PortalCollection,
	}
	
	#endregion CurrencyTypeChildEntityTypes
	
	#region CurrencyTypeFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;CurrencyTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CurrencyType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CurrencyTypeFilterBuilder : SqlFilterBuilder<CurrencyTypeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CurrencyTypeFilterBuilder class.
		/// </summary>
		public CurrencyTypeFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CurrencyTypeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CurrencyTypeFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CurrencyTypeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CurrencyTypeFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CurrencyTypeFilterBuilder
	
	#region CurrencyTypeParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;CurrencyTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CurrencyType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CurrencyTypeParameterBuilder : ParameterizedSqlFilterBuilder<CurrencyTypeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CurrencyTypeParameterBuilder class.
		/// </summary>
		public CurrencyTypeParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CurrencyTypeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CurrencyTypeParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CurrencyTypeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CurrencyTypeParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CurrencyTypeParameterBuilder
	
	#region CurrencyTypeSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;CurrencyTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CurrencyType"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class CurrencyTypeSortBuilder : SqlSortBuilder<CurrencyTypeColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CurrencyTypeSqlSortBuilder class.
		/// </summary>
		public CurrencyTypeSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion CurrencyTypeSortBuilder
	
} // end namespace
