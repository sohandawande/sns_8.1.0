﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="SavedCartLineItemProductExtensionOptionProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class SavedCartLineItemProductExtensionOptionProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.SavedCartLineItemProductExtensionOption, ZNode.Libraries.DataAccess.Entities.SavedCartLineItemProductExtensionOptionKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.SavedCartLineItemProductExtensionOptionKey key)
		{
			return Delete(transactionManager, key.SavedCartLineItemProductExtensionOptionID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_savedCartLineItemProductExtensionOptionID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _savedCartLineItemProductExtensionOptionID)
		{
			return Delete(null, _savedCartLineItemProductExtensionOptionID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_savedCartLineItemProductExtensionOptionID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _savedCartLineItemProductExtensionOptionID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.SavedCartLineItemProductExtensionOption Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.SavedCartLineItemProductExtensionOptionKey key, int start, int pageLength)
		{
			return GetBySavedCartLineItemProductExtensionOptionID(transactionManager, key.SavedCartLineItemProductExtensionOptionID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodeSavedCartLineItemProductExtensionOption index.
		/// </summary>
		/// <param name="_savedCartLineItemProductExtensionOptionID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SavedCartLineItemProductExtensionOption"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.SavedCartLineItemProductExtensionOption GetBySavedCartLineItemProductExtensionOptionID(System.Int32 _savedCartLineItemProductExtensionOptionID)
		{
			int count = -1;
			return GetBySavedCartLineItemProductExtensionOptionID(null,_savedCartLineItemProductExtensionOptionID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeSavedCartLineItemProductExtensionOption index.
		/// </summary>
		/// <param name="_savedCartLineItemProductExtensionOptionID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SavedCartLineItemProductExtensionOption"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.SavedCartLineItemProductExtensionOption GetBySavedCartLineItemProductExtensionOptionID(System.Int32 _savedCartLineItemProductExtensionOptionID, int start, int pageLength)
		{
			int count = -1;
			return GetBySavedCartLineItemProductExtensionOptionID(null, _savedCartLineItemProductExtensionOptionID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeSavedCartLineItemProductExtensionOption index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_savedCartLineItemProductExtensionOptionID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SavedCartLineItemProductExtensionOption"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.SavedCartLineItemProductExtensionOption GetBySavedCartLineItemProductExtensionOptionID(TransactionManager transactionManager, System.Int32 _savedCartLineItemProductExtensionOptionID)
		{
			int count = -1;
			return GetBySavedCartLineItemProductExtensionOptionID(transactionManager, _savedCartLineItemProductExtensionOptionID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeSavedCartLineItemProductExtensionOption index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_savedCartLineItemProductExtensionOptionID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SavedCartLineItemProductExtensionOption"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.SavedCartLineItemProductExtensionOption GetBySavedCartLineItemProductExtensionOptionID(TransactionManager transactionManager, System.Int32 _savedCartLineItemProductExtensionOptionID, int start, int pageLength)
		{
			int count = -1;
			return GetBySavedCartLineItemProductExtensionOptionID(transactionManager, _savedCartLineItemProductExtensionOptionID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeSavedCartLineItemProductExtensionOption index.
		/// </summary>
		/// <param name="_savedCartLineItemProductExtensionOptionID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SavedCartLineItemProductExtensionOption"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.SavedCartLineItemProductExtensionOption GetBySavedCartLineItemProductExtensionOptionID(System.Int32 _savedCartLineItemProductExtensionOptionID, int start, int pageLength, out int count)
		{
			return GetBySavedCartLineItemProductExtensionOptionID(null, _savedCartLineItemProductExtensionOptionID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeSavedCartLineItemProductExtensionOption index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_savedCartLineItemProductExtensionOptionID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SavedCartLineItemProductExtensionOption"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.SavedCartLineItemProductExtensionOption GetBySavedCartLineItemProductExtensionOptionID(TransactionManager transactionManager, System.Int32 _savedCartLineItemProductExtensionOptionID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;SavedCartLineItemProductExtensionOption&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;SavedCartLineItemProductExtensionOption&gt;"/></returns>
		public static TList<SavedCartLineItemProductExtensionOption> Fill(IDataReader reader, TList<SavedCartLineItemProductExtensionOption> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.SavedCartLineItemProductExtensionOption c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("SavedCartLineItemProductExtensionOption")
					.Append("|").Append((System.Int32)reader[((int)SavedCartLineItemProductExtensionOptionColumn.SavedCartLineItemProductExtensionOptionID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<SavedCartLineItemProductExtensionOption>(
					key.ToString(), // EntityTrackingKey
					"SavedCartLineItemProductExtensionOption",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.SavedCartLineItemProductExtensionOption();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.SavedCartLineItemProductExtensionOptionID = (System.Int32)reader[((int)SavedCartLineItemProductExtensionOptionColumn.SavedCartLineItemProductExtensionOptionID - 1)];
					c.SavedCartLineItemID = (System.Int32)reader[((int)SavedCartLineItemProductExtensionOptionColumn.SavedCartLineItemID - 1)];
					c.ProductExtensionOptionID = (System.Int32)reader[((int)SavedCartLineItemProductExtensionOptionColumn.ProductExtensionOptionID - 1)];
					c.OptionData = (reader.IsDBNull(((int)SavedCartLineItemProductExtensionOptionColumn.OptionData - 1)))?null:(System.String)reader[((int)SavedCartLineItemProductExtensionOptionColumn.OptionData - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.SavedCartLineItemProductExtensionOption"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.SavedCartLineItemProductExtensionOption"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.SavedCartLineItemProductExtensionOption entity)
		{
			if (!reader.Read()) return;
			
			entity.SavedCartLineItemProductExtensionOptionID = (System.Int32)reader[((int)SavedCartLineItemProductExtensionOptionColumn.SavedCartLineItemProductExtensionOptionID - 1)];
			entity.SavedCartLineItemID = (System.Int32)reader[((int)SavedCartLineItemProductExtensionOptionColumn.SavedCartLineItemID - 1)];
			entity.ProductExtensionOptionID = (System.Int32)reader[((int)SavedCartLineItemProductExtensionOptionColumn.ProductExtensionOptionID - 1)];
			entity.OptionData = (reader.IsDBNull(((int)SavedCartLineItemProductExtensionOptionColumn.OptionData - 1)))?null:(System.String)reader[((int)SavedCartLineItemProductExtensionOptionColumn.OptionData - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.SavedCartLineItemProductExtensionOption"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.SavedCartLineItemProductExtensionOption"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.SavedCartLineItemProductExtensionOption entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.SavedCartLineItemProductExtensionOptionID = (System.Int32)dataRow["SavedCartLineItemProductExtensionOptionID"];
			entity.SavedCartLineItemID = (System.Int32)dataRow["SavedCartLineItemID"];
			entity.ProductExtensionOptionID = (System.Int32)dataRow["ProductExtensionOptionID"];
			entity.OptionData = Convert.IsDBNull(dataRow["OptionData"]) ? null : (System.String)dataRow["OptionData"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.SavedCartLineItemProductExtensionOption"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.SavedCartLineItemProductExtensionOption Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.SavedCartLineItemProductExtensionOption entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.SavedCartLineItemProductExtensionOption object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.SavedCartLineItemProductExtensionOption instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.SavedCartLineItemProductExtensionOption Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.SavedCartLineItemProductExtensionOption entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region SavedCartLineItemProductExtensionOptionChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.SavedCartLineItemProductExtensionOption</c>
	///</summary>
	public enum SavedCartLineItemProductExtensionOptionChildEntityTypes
	{
	}
	
	#endregion SavedCartLineItemProductExtensionOptionChildEntityTypes
	
	#region SavedCartLineItemProductExtensionOptionFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;SavedCartLineItemProductExtensionOptionColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SavedCartLineItemProductExtensionOption"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SavedCartLineItemProductExtensionOptionFilterBuilder : SqlFilterBuilder<SavedCartLineItemProductExtensionOptionColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SavedCartLineItemProductExtensionOptionFilterBuilder class.
		/// </summary>
		public SavedCartLineItemProductExtensionOptionFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SavedCartLineItemProductExtensionOptionFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SavedCartLineItemProductExtensionOptionFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SavedCartLineItemProductExtensionOptionFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SavedCartLineItemProductExtensionOptionFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SavedCartLineItemProductExtensionOptionFilterBuilder
	
	#region SavedCartLineItemProductExtensionOptionParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;SavedCartLineItemProductExtensionOptionColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SavedCartLineItemProductExtensionOption"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SavedCartLineItemProductExtensionOptionParameterBuilder : ParameterizedSqlFilterBuilder<SavedCartLineItemProductExtensionOptionColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SavedCartLineItemProductExtensionOptionParameterBuilder class.
		/// </summary>
		public SavedCartLineItemProductExtensionOptionParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SavedCartLineItemProductExtensionOptionParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SavedCartLineItemProductExtensionOptionParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SavedCartLineItemProductExtensionOptionParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SavedCartLineItemProductExtensionOptionParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SavedCartLineItemProductExtensionOptionParameterBuilder
	
	#region SavedCartLineItemProductExtensionOptionSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;SavedCartLineItemProductExtensionOptionColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SavedCartLineItemProductExtensionOption"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class SavedCartLineItemProductExtensionOptionSortBuilder : SqlSortBuilder<SavedCartLineItemProductExtensionOptionColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SavedCartLineItemProductExtensionOptionSqlSortBuilder class.
		/// </summary>
		public SavedCartLineItemProductExtensionOptionSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion SavedCartLineItemProductExtensionOptionSortBuilder
	
} // end namespace
