﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="SKUProfileEffectiveProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class SKUProfileEffectiveProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.SKUProfileEffective, ZNode.Libraries.DataAccess.Entities.SKUProfileEffectiveKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.SKUProfileEffectiveKey key)
		{
			return Delete(transactionManager, key.SkuProfileEffectiveID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_skuProfileEffectiveID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _skuProfileEffectiveID)
		{
			return Delete(null, _skuProfileEffectiveID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_skuProfileEffectiveID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _skuProfileEffectiveID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeSKUProfileEffective_ZNodeProfile key.
		///		FK_ZNodeSKUProfileEffective_ZNodeProfile Description: 
		/// </summary>
		/// <param name="_profileId"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.SKUProfileEffective objects.</returns>
		public TList<SKUProfileEffective> GetByProfileId(System.Int32 _profileId)
		{
			int count = -1;
			return GetByProfileId(_profileId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeSKUProfileEffective_ZNodeProfile key.
		///		FK_ZNodeSKUProfileEffective_ZNodeProfile Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_profileId"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.SKUProfileEffective objects.</returns>
		/// <remarks></remarks>
		public TList<SKUProfileEffective> GetByProfileId(TransactionManager transactionManager, System.Int32 _profileId)
		{
			int count = -1;
			return GetByProfileId(transactionManager, _profileId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeSKUProfileEffective_ZNodeProfile key.
		///		FK_ZNodeSKUProfileEffective_ZNodeProfile Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_profileId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.SKUProfileEffective objects.</returns>
		public TList<SKUProfileEffective> GetByProfileId(TransactionManager transactionManager, System.Int32 _profileId, int start, int pageLength)
		{
			int count = -1;
			return GetByProfileId(transactionManager, _profileId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeSKUProfileEffective_ZNodeProfile key.
		///		fKZNodeSKUProfileEffectiveZNodeProfile Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_profileId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.SKUProfileEffective objects.</returns>
		public TList<SKUProfileEffective> GetByProfileId(System.Int32 _profileId, int start, int pageLength)
		{
			int count =  -1;
			return GetByProfileId(null, _profileId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeSKUProfileEffective_ZNodeProfile key.
		///		fKZNodeSKUProfileEffectiveZNodeProfile Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_profileId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.SKUProfileEffective objects.</returns>
		public TList<SKUProfileEffective> GetByProfileId(System.Int32 _profileId, int start, int pageLength,out int count)
		{
			return GetByProfileId(null, _profileId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeSKUProfileEffective_ZNodeProfile key.
		///		FK_ZNodeSKUProfileEffective_ZNodeProfile Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_profileId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.SKUProfileEffective objects.</returns>
		public abstract TList<SKUProfileEffective> GetByProfileId(TransactionManager transactionManager, System.Int32 _profileId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeSKUProfileEffective_ZNodeSKU key.
		///		FK_ZNodeSKUProfileEffective_ZNodeSKU Description: 
		/// </summary>
		/// <param name="_skuId"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.SKUProfileEffective objects.</returns>
		public TList<SKUProfileEffective> GetBySkuId(System.Int32 _skuId)
		{
			int count = -1;
			return GetBySkuId(_skuId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeSKUProfileEffective_ZNodeSKU key.
		///		FK_ZNodeSKUProfileEffective_ZNodeSKU Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_skuId"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.SKUProfileEffective objects.</returns>
		/// <remarks></remarks>
		public TList<SKUProfileEffective> GetBySkuId(TransactionManager transactionManager, System.Int32 _skuId)
		{
			int count = -1;
			return GetBySkuId(transactionManager, _skuId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeSKUProfileEffective_ZNodeSKU key.
		///		FK_ZNodeSKUProfileEffective_ZNodeSKU Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_skuId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.SKUProfileEffective objects.</returns>
		public TList<SKUProfileEffective> GetBySkuId(TransactionManager transactionManager, System.Int32 _skuId, int start, int pageLength)
		{
			int count = -1;
			return GetBySkuId(transactionManager, _skuId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeSKUProfileEffective_ZNodeSKU key.
		///		fKZNodeSKUProfileEffectiveZNodeSKU Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_skuId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.SKUProfileEffective objects.</returns>
		public TList<SKUProfileEffective> GetBySkuId(System.Int32 _skuId, int start, int pageLength)
		{
			int count =  -1;
			return GetBySkuId(null, _skuId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeSKUProfileEffective_ZNodeSKU key.
		///		fKZNodeSKUProfileEffectiveZNodeSKU Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_skuId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.SKUProfileEffective objects.</returns>
		public TList<SKUProfileEffective> GetBySkuId(System.Int32 _skuId, int start, int pageLength,out int count)
		{
			return GetBySkuId(null, _skuId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeSKUProfileEffective_ZNodeSKU key.
		///		FK_ZNodeSKUProfileEffective_ZNodeSKU Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_skuId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.SKUProfileEffective objects.</returns>
		public abstract TList<SKUProfileEffective> GetBySkuId(TransactionManager transactionManager, System.Int32 _skuId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.SKUProfileEffective Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.SKUProfileEffectiveKey key, int start, int pageLength)
		{
			return GetBySkuProfileEffectiveID(transactionManager, key.SkuProfileEffectiveID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK__ZNodeSKU__87015C634D005615 index.
		/// </summary>
		/// <param name="_skuProfileEffectiveID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SKUProfileEffective"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.SKUProfileEffective GetBySkuProfileEffectiveID(System.Int32 _skuProfileEffectiveID)
		{
			int count = -1;
			return GetBySkuProfileEffectiveID(null,_skuProfileEffectiveID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK__ZNodeSKU__87015C634D005615 index.
		/// </summary>
		/// <param name="_skuProfileEffectiveID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SKUProfileEffective"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.SKUProfileEffective GetBySkuProfileEffectiveID(System.Int32 _skuProfileEffectiveID, int start, int pageLength)
		{
			int count = -1;
			return GetBySkuProfileEffectiveID(null, _skuProfileEffectiveID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK__ZNodeSKU__87015C634D005615 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_skuProfileEffectiveID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SKUProfileEffective"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.SKUProfileEffective GetBySkuProfileEffectiveID(TransactionManager transactionManager, System.Int32 _skuProfileEffectiveID)
		{
			int count = -1;
			return GetBySkuProfileEffectiveID(transactionManager, _skuProfileEffectiveID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK__ZNodeSKU__87015C634D005615 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_skuProfileEffectiveID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SKUProfileEffective"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.SKUProfileEffective GetBySkuProfileEffectiveID(TransactionManager transactionManager, System.Int32 _skuProfileEffectiveID, int start, int pageLength)
		{
			int count = -1;
			return GetBySkuProfileEffectiveID(transactionManager, _skuProfileEffectiveID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK__ZNodeSKU__87015C634D005615 index.
		/// </summary>
		/// <param name="_skuProfileEffectiveID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SKUProfileEffective"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.SKUProfileEffective GetBySkuProfileEffectiveID(System.Int32 _skuProfileEffectiveID, int start, int pageLength, out int count)
		{
			return GetBySkuProfileEffectiveID(null, _skuProfileEffectiveID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK__ZNodeSKU__87015C634D005615 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_skuProfileEffectiveID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SKUProfileEffective"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.SKUProfileEffective GetBySkuProfileEffectiveID(TransactionManager transactionManager, System.Int32 _skuProfileEffectiveID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;SKUProfileEffective&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;SKUProfileEffective&gt;"/></returns>
		public static TList<SKUProfileEffective> Fill(IDataReader reader, TList<SKUProfileEffective> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.SKUProfileEffective c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("SKUProfileEffective")
					.Append("|").Append((System.Int32)reader[((int)SKUProfileEffectiveColumn.SkuProfileEffectiveID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<SKUProfileEffective>(
					key.ToString(), // EntityTrackingKey
					"SKUProfileEffective",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.SKUProfileEffective();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.SkuProfileEffectiveID = (System.Int32)reader[((int)SKUProfileEffectiveColumn.SkuProfileEffectiveID - 1)];
					c.SkuId = (System.Int32)reader[((int)SKUProfileEffectiveColumn.SkuId - 1)];
					c.ProfileId = (System.Int32)reader[((int)SKUProfileEffectiveColumn.ProfileId - 1)];
					c.EffectiveDate = (System.DateTime)reader[((int)SKUProfileEffectiveColumn.EffectiveDate - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.SKUProfileEffective"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.SKUProfileEffective"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.SKUProfileEffective entity)
		{
			if (!reader.Read()) return;
			
			entity.SkuProfileEffectiveID = (System.Int32)reader[((int)SKUProfileEffectiveColumn.SkuProfileEffectiveID - 1)];
			entity.SkuId = (System.Int32)reader[((int)SKUProfileEffectiveColumn.SkuId - 1)];
			entity.ProfileId = (System.Int32)reader[((int)SKUProfileEffectiveColumn.ProfileId - 1)];
			entity.EffectiveDate = (System.DateTime)reader[((int)SKUProfileEffectiveColumn.EffectiveDate - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.SKUProfileEffective"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.SKUProfileEffective"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.SKUProfileEffective entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.SkuProfileEffectiveID = (System.Int32)dataRow["SkuProfileEffectiveID"];
			entity.SkuId = (System.Int32)dataRow["SkuId"];
			entity.ProfileId = (System.Int32)dataRow["ProfileId"];
			entity.EffectiveDate = (System.DateTime)dataRow["EffectiveDate"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.SKUProfileEffective"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.SKUProfileEffective Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.SKUProfileEffective entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region ProfileIdSource	
			if (CanDeepLoad(entity, "Profile|ProfileIdSource", deepLoadType, innerList) 
				&& entity.ProfileIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.ProfileId;
				Profile tmpEntity = EntityManager.LocateEntity<Profile>(EntityLocator.ConstructKeyFromPkItems(typeof(Profile), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ProfileIdSource = tmpEntity;
				else
					entity.ProfileIdSource = DataRepository.ProfileProvider.GetByProfileID(transactionManager, entity.ProfileId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ProfileIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ProfileIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.ProfileProvider.DeepLoad(transactionManager, entity.ProfileIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ProfileIdSource

			#region SkuIdSource	
			if (CanDeepLoad(entity, "SKU|SkuIdSource", deepLoadType, innerList) 
				&& entity.SkuIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.SkuId;
				SKU tmpEntity = EntityManager.LocateEntity<SKU>(EntityLocator.ConstructKeyFromPkItems(typeof(SKU), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.SkuIdSource = tmpEntity;
				else
					entity.SkuIdSource = DataRepository.SKUProvider.GetBySKUID(transactionManager, entity.SkuId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SkuIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.SkuIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.SKUProvider.DeepLoad(transactionManager, entity.SkuIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion SkuIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.SKUProfileEffective object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.SKUProfileEffective instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.SKUProfileEffective Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.SKUProfileEffective entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region ProfileIdSource
			if (CanDeepSave(entity, "Profile|ProfileIdSource", deepSaveType, innerList) 
				&& entity.ProfileIdSource != null)
			{
				DataRepository.ProfileProvider.Save(transactionManager, entity.ProfileIdSource);
				entity.ProfileId = entity.ProfileIdSource.ProfileID;
			}
			#endregion 
			
			#region SkuIdSource
			if (CanDeepSave(entity, "SKU|SkuIdSource", deepSaveType, innerList) 
				&& entity.SkuIdSource != null)
			{
				DataRepository.SKUProvider.Save(transactionManager, entity.SkuIdSource);
				entity.SkuId = entity.SkuIdSource.SKUID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region SKUProfileEffectiveChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.SKUProfileEffective</c>
	///</summary>
	public enum SKUProfileEffectiveChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Profile</c> at ProfileIdSource
		///</summary>
		[ChildEntityType(typeof(Profile))]
		Profile,
		
		///<summary>
		/// Composite Property for <c>SKU</c> at SkuIdSource
		///</summary>
		[ChildEntityType(typeof(SKU))]
		SKU,
	}
	
	#endregion SKUProfileEffectiveChildEntityTypes
	
	#region SKUProfileEffectiveFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;SKUProfileEffectiveColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SKUProfileEffective"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SKUProfileEffectiveFilterBuilder : SqlFilterBuilder<SKUProfileEffectiveColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SKUProfileEffectiveFilterBuilder class.
		/// </summary>
		public SKUProfileEffectiveFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SKUProfileEffectiveFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SKUProfileEffectiveFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SKUProfileEffectiveFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SKUProfileEffectiveFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SKUProfileEffectiveFilterBuilder
	
	#region SKUProfileEffectiveParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;SKUProfileEffectiveColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SKUProfileEffective"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SKUProfileEffectiveParameterBuilder : ParameterizedSqlFilterBuilder<SKUProfileEffectiveColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SKUProfileEffectiveParameterBuilder class.
		/// </summary>
		public SKUProfileEffectiveParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SKUProfileEffectiveParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SKUProfileEffectiveParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SKUProfileEffectiveParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SKUProfileEffectiveParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SKUProfileEffectiveParameterBuilder
	
	#region SKUProfileEffectiveSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;SKUProfileEffectiveColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SKUProfileEffective"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class SKUProfileEffectiveSortBuilder : SqlSortBuilder<SKUProfileEffectiveColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SKUProfileEffectiveSqlSortBuilder class.
		/// </summary>
		public SKUProfileEffectiveSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion SKUProfileEffectiveSortBuilder
	
} // end namespace
