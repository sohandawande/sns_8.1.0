﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="LocaleProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class LocaleProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.Locale, ZNode.Libraries.DataAccess.Entities.LocaleKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.LocaleKey key)
		{
			return Delete(transactionManager, key.LocaleID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_localeID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _localeID)
		{
			return Delete(null, _localeID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_localeID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _localeID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.Locale Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.LocaleKey key, int start, int pageLength)
		{
			return GetByLocaleID(transactionManager, key.LocaleID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodeLocale index.
		/// </summary>
		/// <param name="_localeID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Locale"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Locale GetByLocaleID(System.Int32 _localeID)
		{
			int count = -1;
			return GetByLocaleID(null,_localeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeLocale index.
		/// </summary>
		/// <param name="_localeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Locale"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Locale GetByLocaleID(System.Int32 _localeID, int start, int pageLength)
		{
			int count = -1;
			return GetByLocaleID(null, _localeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeLocale index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_localeID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Locale"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Locale GetByLocaleID(TransactionManager transactionManager, System.Int32 _localeID)
		{
			int count = -1;
			return GetByLocaleID(transactionManager, _localeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeLocale index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_localeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Locale"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Locale GetByLocaleID(TransactionManager transactionManager, System.Int32 _localeID, int start, int pageLength)
		{
			int count = -1;
			return GetByLocaleID(transactionManager, _localeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeLocale index.
		/// </summary>
		/// <param name="_localeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Locale"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Locale GetByLocaleID(System.Int32 _localeID, int start, int pageLength, out int count)
		{
			return GetByLocaleID(null, _localeID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeLocale index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_localeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Locale"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.Locale GetByLocaleID(TransactionManager transactionManager, System.Int32 _localeID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;Locale&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;Locale&gt;"/></returns>
		public static TList<Locale> Fill(IDataReader reader, TList<Locale> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.Locale c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("Locale")
					.Append("|").Append((System.Int32)reader[((int)LocaleColumn.LocaleID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<Locale>(
					key.ToString(), // EntityTrackingKey
					"Locale",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.Locale();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.LocaleID = (System.Int32)reader[((int)LocaleColumn.LocaleID - 1)];
					c.LocaleCode = (System.String)reader[((int)LocaleColumn.LocaleCode - 1)];
					c.LocaleDescription = (System.String)reader[((int)LocaleColumn.LocaleDescription - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.Locale"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.Locale"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.Locale entity)
		{
			if (!reader.Read()) return;
			
			entity.LocaleID = (System.Int32)reader[((int)LocaleColumn.LocaleID - 1)];
			entity.LocaleCode = (System.String)reader[((int)LocaleColumn.LocaleCode - 1)];
			entity.LocaleDescription = (System.String)reader[((int)LocaleColumn.LocaleDescription - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.Locale"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.Locale"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.Locale entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.LocaleID = (System.Int32)dataRow["LocaleID"];
			entity.LocaleCode = (System.String)dataRow["LocaleCode"];
			entity.LocaleDescription = (System.String)dataRow["LocaleDescription"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.Locale"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.Locale Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.Locale entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByLocaleID methods when available
			
			#region PortalCatalogCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<PortalCatalog>|PortalCatalogCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PortalCatalogCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.PortalCatalogCollection = DataRepository.PortalCatalogProvider.GetByLocaleID(transactionManager, entity.LocaleID);

				if (deep && entity.PortalCatalogCollection.Count > 0)
				{
					deepHandles.Add("PortalCatalogCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<PortalCatalog>) DataRepository.PortalCatalogProvider.DeepLoad,
						new object[] { transactionManager, entity.PortalCatalogCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region PortalCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Portal>|PortalCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PortalCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.PortalCollection = DataRepository.PortalProvider.GetByLocaleID(transactionManager, entity.LocaleID);

				if (deep && entity.PortalCollection.Count > 0)
				{
					deepHandles.Add("PortalCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Portal>) DataRepository.PortalProvider.DeepLoad,
						new object[] { transactionManager, entity.PortalCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region HighlightCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Highlight>|HighlightCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'HighlightCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.HighlightCollection = DataRepository.HighlightProvider.GetByLocaleId(transactionManager, entity.LocaleID);

				if (deep && entity.HighlightCollection.Count > 0)
				{
					deepHandles.Add("HighlightCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Highlight>) DataRepository.HighlightProvider.DeepLoad,
						new object[] { transactionManager, entity.HighlightCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region AddOnCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<AddOn>|AddOnCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AddOnCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.AddOnCollection = DataRepository.AddOnProvider.GetByLocaleId(transactionManager, entity.LocaleID);

				if (deep && entity.AddOnCollection.Count > 0)
				{
					deepHandles.Add("AddOnCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<AddOn>) DataRepository.AddOnProvider.DeepLoad,
						new object[] { transactionManager, entity.AddOnCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region AttributeTypeCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<AttributeType>|AttributeTypeCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AttributeTypeCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.AttributeTypeCollection = DataRepository.AttributeTypeProvider.GetByLocaleId(transactionManager, entity.LocaleID);

				if (deep && entity.AttributeTypeCollection.Count > 0)
				{
					deepHandles.Add("AttributeTypeCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<AttributeType>) DataRepository.AttributeTypeProvider.DeepLoad,
						new object[] { transactionManager, entity.AttributeTypeCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.Locale object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.Locale instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.Locale Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.Locale entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<PortalCatalog>
				if (CanDeepSave(entity.PortalCatalogCollection, "List<PortalCatalog>|PortalCatalogCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(PortalCatalog child in entity.PortalCatalogCollection)
					{
						if(child.LocaleIDSource != null)
						{
							child.LocaleID = child.LocaleIDSource.LocaleID;
						}
						else
						{
							child.LocaleID = entity.LocaleID;
						}

					}

					if (entity.PortalCatalogCollection.Count > 0 || entity.PortalCatalogCollection.DeletedItems.Count > 0)
					{
						//DataRepository.PortalCatalogProvider.Save(transactionManager, entity.PortalCatalogCollection);
						
						deepHandles.Add("PortalCatalogCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< PortalCatalog >) DataRepository.PortalCatalogProvider.DeepSave,
							new object[] { transactionManager, entity.PortalCatalogCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Portal>
				if (CanDeepSave(entity.PortalCollection, "List<Portal>|PortalCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Portal child in entity.PortalCollection)
					{
						if(child.LocaleIDSource != null)
						{
							child.LocaleID = child.LocaleIDSource.LocaleID;
						}
						else
						{
							child.LocaleID = entity.LocaleID;
						}

					}

					if (entity.PortalCollection.Count > 0 || entity.PortalCollection.DeletedItems.Count > 0)
					{
						//DataRepository.PortalProvider.Save(transactionManager, entity.PortalCollection);
						
						deepHandles.Add("PortalCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Portal >) DataRepository.PortalProvider.DeepSave,
							new object[] { transactionManager, entity.PortalCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Highlight>
				if (CanDeepSave(entity.HighlightCollection, "List<Highlight>|HighlightCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Highlight child in entity.HighlightCollection)
					{
						if(child.LocaleIdSource != null)
						{
							child.LocaleId = child.LocaleIdSource.LocaleID;
						}
						else
						{
							child.LocaleId = entity.LocaleID;
						}

					}

					if (entity.HighlightCollection.Count > 0 || entity.HighlightCollection.DeletedItems.Count > 0)
					{
						//DataRepository.HighlightProvider.Save(transactionManager, entity.HighlightCollection);
						
						deepHandles.Add("HighlightCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Highlight >) DataRepository.HighlightProvider.DeepSave,
							new object[] { transactionManager, entity.HighlightCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<AddOn>
				if (CanDeepSave(entity.AddOnCollection, "List<AddOn>|AddOnCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(AddOn child in entity.AddOnCollection)
					{
						if(child.LocaleIdSource != null)
						{
							child.LocaleId = child.LocaleIdSource.LocaleID;
						}
						else
						{
							child.LocaleId = entity.LocaleID;
						}

					}

					if (entity.AddOnCollection.Count > 0 || entity.AddOnCollection.DeletedItems.Count > 0)
					{
						//DataRepository.AddOnProvider.Save(transactionManager, entity.AddOnCollection);
						
						deepHandles.Add("AddOnCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< AddOn >) DataRepository.AddOnProvider.DeepSave,
							new object[] { transactionManager, entity.AddOnCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<AttributeType>
				if (CanDeepSave(entity.AttributeTypeCollection, "List<AttributeType>|AttributeTypeCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(AttributeType child in entity.AttributeTypeCollection)
					{
						if(child.LocaleIdSource != null)
						{
							child.LocaleId = child.LocaleIdSource.LocaleID;
						}
						else
						{
							child.LocaleId = entity.LocaleID;
						}

					}

					if (entity.AttributeTypeCollection.Count > 0 || entity.AttributeTypeCollection.DeletedItems.Count > 0)
					{
						//DataRepository.AttributeTypeProvider.Save(transactionManager, entity.AttributeTypeCollection);
						
						deepHandles.Add("AttributeTypeCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< AttributeType >) DataRepository.AttributeTypeProvider.DeepSave,
							new object[] { transactionManager, entity.AttributeTypeCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region LocaleChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.Locale</c>
	///</summary>
	public enum LocaleChildEntityTypes
	{
		///<summary>
		/// Collection of <c>Locale</c> as OneToMany for PortalCatalogCollection
		///</summary>
		[ChildEntityType(typeof(TList<PortalCatalog>))]
		PortalCatalogCollection,
		///<summary>
		/// Collection of <c>Locale</c> as OneToMany for PortalCollection
		///</summary>
		[ChildEntityType(typeof(TList<Portal>))]
		PortalCollection,
		///<summary>
		/// Collection of <c>Locale</c> as OneToMany for HighlightCollection
		///</summary>
		[ChildEntityType(typeof(TList<Highlight>))]
		HighlightCollection,
		///<summary>
		/// Collection of <c>Locale</c> as OneToMany for AddOnCollection
		///</summary>
		[ChildEntityType(typeof(TList<AddOn>))]
		AddOnCollection,
		///<summary>
		/// Collection of <c>Locale</c> as OneToMany for AttributeTypeCollection
		///</summary>
		[ChildEntityType(typeof(TList<AttributeType>))]
		AttributeTypeCollection,
	}
	
	#endregion LocaleChildEntityTypes
	
	#region LocaleFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;LocaleColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Locale"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class LocaleFilterBuilder : SqlFilterBuilder<LocaleColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the LocaleFilterBuilder class.
		/// </summary>
		public LocaleFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the LocaleFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public LocaleFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the LocaleFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public LocaleFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion LocaleFilterBuilder
	
	#region LocaleParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;LocaleColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Locale"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class LocaleParameterBuilder : ParameterizedSqlFilterBuilder<LocaleColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the LocaleParameterBuilder class.
		/// </summary>
		public LocaleParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the LocaleParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public LocaleParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the LocaleParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public LocaleParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion LocaleParameterBuilder
	
	#region LocaleSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;LocaleColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Locale"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class LocaleSortBuilder : SqlSortBuilder<LocaleColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the LocaleSqlSortBuilder class.
		/// </summary>
		public LocaleSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion LocaleSortBuilder
	
} // end namespace
