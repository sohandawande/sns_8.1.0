﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="PortalProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class PortalProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.Portal, ZNode.Libraries.DataAccess.Entities.PortalKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.PortalKey key)
		{
			return Delete(transactionManager, key.PortalID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_portalID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _portalID)
		{
			return Delete(null, _portalID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _portalID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePortal_ZNodeCurrencyType key.
		///		FK_ZNodePortal_ZNodeCurrencyType Description: 
		/// </summary>
		/// <param name="_currencyTypeID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Portal objects.</returns>
		public TList<Portal> GetByCurrencyTypeID(System.Int32? _currencyTypeID)
		{
			int count = -1;
			return GetByCurrencyTypeID(_currencyTypeID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePortal_ZNodeCurrencyType key.
		///		FK_ZNodePortal_ZNodeCurrencyType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_currencyTypeID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Portal objects.</returns>
		/// <remarks></remarks>
		public TList<Portal> GetByCurrencyTypeID(TransactionManager transactionManager, System.Int32? _currencyTypeID)
		{
			int count = -1;
			return GetByCurrencyTypeID(transactionManager, _currencyTypeID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePortal_ZNodeCurrencyType key.
		///		FK_ZNodePortal_ZNodeCurrencyType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_currencyTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Portal objects.</returns>
		public TList<Portal> GetByCurrencyTypeID(TransactionManager transactionManager, System.Int32? _currencyTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetByCurrencyTypeID(transactionManager, _currencyTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePortal_ZNodeCurrencyType key.
		///		fKZNodePortalZNodeCurrencyType Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_currencyTypeID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Portal objects.</returns>
		public TList<Portal> GetByCurrencyTypeID(System.Int32? _currencyTypeID, int start, int pageLength)
		{
			int count =  -1;
			return GetByCurrencyTypeID(null, _currencyTypeID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePortal_ZNodeCurrencyType key.
		///		fKZNodePortalZNodeCurrencyType Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_currencyTypeID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Portal objects.</returns>
		public TList<Portal> GetByCurrencyTypeID(System.Int32? _currencyTypeID, int start, int pageLength,out int count)
		{
			return GetByCurrencyTypeID(null, _currencyTypeID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePortal_ZNodeCurrencyType key.
		///		FK_ZNodePortal_ZNodeCurrencyType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_currencyTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Portal objects.</returns>
		public abstract TList<Portal> GetByCurrencyTypeID(TransactionManager transactionManager, System.Int32? _currencyTypeID, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePortal_ZNodeOrderState key.
		///		FK_ZNodePortal_ZNodeOrderState Description: 
		/// </summary>
		/// <param name="_defaultOrderStateID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Portal objects.</returns>
		public TList<Portal> GetByDefaultOrderStateID(System.Int32? _defaultOrderStateID)
		{
			int count = -1;
			return GetByDefaultOrderStateID(_defaultOrderStateID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePortal_ZNodeOrderState key.
		///		FK_ZNodePortal_ZNodeOrderState Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_defaultOrderStateID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Portal objects.</returns>
		/// <remarks></remarks>
		public TList<Portal> GetByDefaultOrderStateID(TransactionManager transactionManager, System.Int32? _defaultOrderStateID)
		{
			int count = -1;
			return GetByDefaultOrderStateID(transactionManager, _defaultOrderStateID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePortal_ZNodeOrderState key.
		///		FK_ZNodePortal_ZNodeOrderState Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_defaultOrderStateID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Portal objects.</returns>
		public TList<Portal> GetByDefaultOrderStateID(TransactionManager transactionManager, System.Int32? _defaultOrderStateID, int start, int pageLength)
		{
			int count = -1;
			return GetByDefaultOrderStateID(transactionManager, _defaultOrderStateID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePortal_ZNodeOrderState key.
		///		fKZNodePortalZNodeOrderState Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_defaultOrderStateID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Portal objects.</returns>
		public TList<Portal> GetByDefaultOrderStateID(System.Int32? _defaultOrderStateID, int start, int pageLength)
		{
			int count =  -1;
			return GetByDefaultOrderStateID(null, _defaultOrderStateID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePortal_ZNodeOrderState key.
		///		fKZNodePortalZNodeOrderState Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_defaultOrderStateID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Portal objects.</returns>
		public TList<Portal> GetByDefaultOrderStateID(System.Int32? _defaultOrderStateID, int start, int pageLength,out int count)
		{
			return GetByDefaultOrderStateID(null, _defaultOrderStateID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePortal_ZNodeOrderState key.
		///		FK_ZNodePortal_ZNodeOrderState Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_defaultOrderStateID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Portal objects.</returns>
		public abstract TList<Portal> GetByDefaultOrderStateID(TransactionManager transactionManager, System.Int32? _defaultOrderStateID, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePortal_ZNodeProductReviewState key.
		///		FK_ZNodePortal_ZNodeProductReviewState Description: 
		/// </summary>
		/// <param name="_defaultProductReviewStateID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Portal objects.</returns>
		public TList<Portal> GetByDefaultProductReviewStateID(System.Int32? _defaultProductReviewStateID)
		{
			int count = -1;
			return GetByDefaultProductReviewStateID(_defaultProductReviewStateID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePortal_ZNodeProductReviewState key.
		///		FK_ZNodePortal_ZNodeProductReviewState Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_defaultProductReviewStateID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Portal objects.</returns>
		/// <remarks></remarks>
		public TList<Portal> GetByDefaultProductReviewStateID(TransactionManager transactionManager, System.Int32? _defaultProductReviewStateID)
		{
			int count = -1;
			return GetByDefaultProductReviewStateID(transactionManager, _defaultProductReviewStateID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePortal_ZNodeProductReviewState key.
		///		FK_ZNodePortal_ZNodeProductReviewState Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_defaultProductReviewStateID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Portal objects.</returns>
		public TList<Portal> GetByDefaultProductReviewStateID(TransactionManager transactionManager, System.Int32? _defaultProductReviewStateID, int start, int pageLength)
		{
			int count = -1;
			return GetByDefaultProductReviewStateID(transactionManager, _defaultProductReviewStateID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePortal_ZNodeProductReviewState key.
		///		fKZNodePortalZNodeProductReviewState Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_defaultProductReviewStateID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Portal objects.</returns>
		public TList<Portal> GetByDefaultProductReviewStateID(System.Int32? _defaultProductReviewStateID, int start, int pageLength)
		{
			int count =  -1;
			return GetByDefaultProductReviewStateID(null, _defaultProductReviewStateID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePortal_ZNodeProductReviewState key.
		///		fKZNodePortalZNodeProductReviewState Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_defaultProductReviewStateID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Portal objects.</returns>
		public TList<Portal> GetByDefaultProductReviewStateID(System.Int32? _defaultProductReviewStateID, int start, int pageLength,out int count)
		{
			return GetByDefaultProductReviewStateID(null, _defaultProductReviewStateID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePortal_ZNodeProductReviewState key.
		///		FK_ZNodePortal_ZNodeProductReviewState Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_defaultProductReviewStateID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Portal objects.</returns>
		public abstract TList<Portal> GetByDefaultProductReviewStateID(TransactionManager transactionManager, System.Int32? _defaultProductReviewStateID, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.Portal Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.PortalKey key, int start, int pageLength)
		{
			return GetByPortalID(transactionManager, key.PortalID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodePortal_ActiveInd index.
		/// </summary>
		/// <param name="_activeInd"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Portal&gt;"/> class.</returns>
		public TList<Portal> GetByActiveInd(System.Boolean _activeInd)
		{
			int count = -1;
			return GetByActiveInd(null,_activeInd, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePortal_ActiveInd index.
		/// </summary>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Portal&gt;"/> class.</returns>
		public TList<Portal> GetByActiveInd(System.Boolean _activeInd, int start, int pageLength)
		{
			int count = -1;
			return GetByActiveInd(null, _activeInd, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePortal_ActiveInd index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_activeInd"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Portal&gt;"/> class.</returns>
		public TList<Portal> GetByActiveInd(TransactionManager transactionManager, System.Boolean _activeInd)
		{
			int count = -1;
			return GetByActiveInd(transactionManager, _activeInd, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePortal_ActiveInd index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Portal&gt;"/> class.</returns>
		public TList<Portal> GetByActiveInd(TransactionManager transactionManager, System.Boolean _activeInd, int start, int pageLength)
		{
			int count = -1;
			return GetByActiveInd(transactionManager, _activeInd, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePortal_ActiveInd index.
		/// </summary>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Portal&gt;"/> class.</returns>
		public TList<Portal> GetByActiveInd(System.Boolean _activeInd, int start, int pageLength, out int count)
		{
			return GetByActiveInd(null, _activeInd, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePortal_ActiveInd index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Portal&gt;"/> class.</returns>
		public abstract TList<Portal> GetByActiveInd(TransactionManager transactionManager, System.Boolean _activeInd, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodePortal_ExternalID index.
		/// </summary>
		/// <param name="_externalID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Portal&gt;"/> class.</returns>
		public TList<Portal> GetByExternalID(System.String _externalID)
		{
			int count = -1;
			return GetByExternalID(null,_externalID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePortal_ExternalID index.
		/// </summary>
		/// <param name="_externalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Portal&gt;"/> class.</returns>
		public TList<Portal> GetByExternalID(System.String _externalID, int start, int pageLength)
		{
			int count = -1;
			return GetByExternalID(null, _externalID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePortal_ExternalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_externalID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Portal&gt;"/> class.</returns>
		public TList<Portal> GetByExternalID(TransactionManager transactionManager, System.String _externalID)
		{
			int count = -1;
			return GetByExternalID(transactionManager, _externalID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePortal_ExternalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_externalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Portal&gt;"/> class.</returns>
		public TList<Portal> GetByExternalID(TransactionManager transactionManager, System.String _externalID, int start, int pageLength)
		{
			int count = -1;
			return GetByExternalID(transactionManager, _externalID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePortal_ExternalID index.
		/// </summary>
		/// <param name="_externalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Portal&gt;"/> class.</returns>
		public TList<Portal> GetByExternalID(System.String _externalID, int start, int pageLength, out int count)
		{
			return GetByExternalID(null, _externalID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePortal_ExternalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_externalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Portal&gt;"/> class.</returns>
		public abstract TList<Portal> GetByExternalID(TransactionManager transactionManager, System.String _externalID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodePortal_LocaleID index.
		/// </summary>
		/// <param name="_localeID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Portal&gt;"/> class.</returns>
		public TList<Portal> GetByLocaleID(System.Int32 _localeID)
		{
			int count = -1;
			return GetByLocaleID(null,_localeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePortal_LocaleID index.
		/// </summary>
		/// <param name="_localeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Portal&gt;"/> class.</returns>
		public TList<Portal> GetByLocaleID(System.Int32 _localeID, int start, int pageLength)
		{
			int count = -1;
			return GetByLocaleID(null, _localeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePortal_LocaleID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_localeID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Portal&gt;"/> class.</returns>
		public TList<Portal> GetByLocaleID(TransactionManager transactionManager, System.Int32 _localeID)
		{
			int count = -1;
			return GetByLocaleID(transactionManager, _localeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePortal_LocaleID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_localeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Portal&gt;"/> class.</returns>
		public TList<Portal> GetByLocaleID(TransactionManager transactionManager, System.Int32 _localeID, int start, int pageLength)
		{
			int count = -1;
			return GetByLocaleID(transactionManager, _localeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePortal_LocaleID index.
		/// </summary>
		/// <param name="_localeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Portal&gt;"/> class.</returns>
		public TList<Portal> GetByLocaleID(System.Int32 _localeID, int start, int pageLength, out int count)
		{
			return GetByLocaleID(null, _localeID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePortal_LocaleID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_localeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Portal&gt;"/> class.</returns>
		public abstract TList<Portal> GetByLocaleID(TransactionManager transactionManager, System.Int32 _localeID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_Portals index.
		/// </summary>
		/// <param name="_portalID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Portal"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Portal GetByPortalID(System.Int32 _portalID)
		{
			int count = -1;
			return GetByPortalID(null,_portalID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Portals index.
		/// </summary>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Portal"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Portal GetByPortalID(System.Int32 _portalID, int start, int pageLength)
		{
			int count = -1;
			return GetByPortalID(null, _portalID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Portals index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Portal"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Portal GetByPortalID(TransactionManager transactionManager, System.Int32 _portalID)
		{
			int count = -1;
			return GetByPortalID(transactionManager, _portalID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Portals index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Portal"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Portal GetByPortalID(TransactionManager transactionManager, System.Int32 _portalID, int start, int pageLength)
		{
			int count = -1;
			return GetByPortalID(transactionManager, _portalID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Portals index.
		/// </summary>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Portal"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Portal GetByPortalID(System.Int32 _portalID, int start, int pageLength, out int count)
		{
			return GetByPortalID(null, _portalID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Portals index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Portal"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.Portal GetByPortalID(TransactionManager transactionManager, System.Int32 _portalID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;Portal&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;Portal&gt;"/></returns>
		public static TList<Portal> Fill(IDataReader reader, TList<Portal> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.Portal c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("Portal")
					.Append("|").Append((System.Int32)reader[((int)PortalColumn.PortalID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<Portal>(
					key.ToString(), // EntityTrackingKey
					"Portal",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.Portal();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.PortalID = (System.Int32)reader[((int)PortalColumn.PortalID - 1)];
					c.CompanyName = (System.String)reader[((int)PortalColumn.CompanyName - 1)];
					c.StoreName = (System.String)reader[((int)PortalColumn.StoreName - 1)];
					c.LogoPath = (reader.IsDBNull(((int)PortalColumn.LogoPath - 1)))?null:(System.String)reader[((int)PortalColumn.LogoPath - 1)];
					c.UseSSL = (System.Boolean)reader[((int)PortalColumn.UseSSL - 1)];
					c.AdminEmail = (reader.IsDBNull(((int)PortalColumn.AdminEmail - 1)))?null:(System.String)reader[((int)PortalColumn.AdminEmail - 1)];
					c.SalesEmail = (reader.IsDBNull(((int)PortalColumn.SalesEmail - 1)))?null:(System.String)reader[((int)PortalColumn.SalesEmail - 1)];
					c.CustomerServiceEmail = (reader.IsDBNull(((int)PortalColumn.CustomerServiceEmail - 1)))?null:(System.String)reader[((int)PortalColumn.CustomerServiceEmail - 1)];
					c.SalesPhoneNumber = (reader.IsDBNull(((int)PortalColumn.SalesPhoneNumber - 1)))?null:(System.String)reader[((int)PortalColumn.SalesPhoneNumber - 1)];
					c.CustomerServicePhoneNumber = (reader.IsDBNull(((int)PortalColumn.CustomerServicePhoneNumber - 1)))?null:(System.String)reader[((int)PortalColumn.CustomerServicePhoneNumber - 1)];
					c.ImageNotAvailablePath = (System.String)reader[((int)PortalColumn.ImageNotAvailablePath - 1)];
					c.MaxCatalogDisplayColumns = (System.Byte)reader[((int)PortalColumn.MaxCatalogDisplayColumns - 1)];
					c.MaxCatalogDisplayItems = (System.Int32)reader[((int)PortalColumn.MaxCatalogDisplayItems - 1)];
					c.MaxCatalogCategoryDisplayThumbnails = (System.Int32)reader[((int)PortalColumn.MaxCatalogCategoryDisplayThumbnails - 1)];
					c.MaxCatalogItemSmallThumbnailWidth = (System.Int32)reader[((int)PortalColumn.MaxCatalogItemSmallThumbnailWidth - 1)];
					c.MaxCatalogItemSmallWidth = (System.Int32)reader[((int)PortalColumn.MaxCatalogItemSmallWidth - 1)];
					c.MaxCatalogItemMediumWidth = (System.Int32)reader[((int)PortalColumn.MaxCatalogItemMediumWidth - 1)];
					c.MaxCatalogItemThumbnailWidth = (System.Int32)reader[((int)PortalColumn.MaxCatalogItemThumbnailWidth - 1)];
					c.MaxCatalogItemLargeWidth = (System.Int32)reader[((int)PortalColumn.MaxCatalogItemLargeWidth - 1)];
					c.MaxCatalogItemCrossSellWidth = (System.Int32)reader[((int)PortalColumn.MaxCatalogItemCrossSellWidth - 1)];
					c.ShowSwatchInCategory = (System.Boolean)reader[((int)PortalColumn.ShowSwatchInCategory - 1)];
					c.ShowAlternateImageInCategory = (System.Boolean)reader[((int)PortalColumn.ShowAlternateImageInCategory - 1)];
					c.ActiveInd = (System.Boolean)reader[((int)PortalColumn.ActiveInd - 1)];
					c.SMTPServer = (reader.IsDBNull(((int)PortalColumn.SMTPServer - 1)))?null:(System.String)reader[((int)PortalColumn.SMTPServer - 1)];
					c.SMTPUserName = (reader.IsDBNull(((int)PortalColumn.SMTPUserName - 1)))?null:(System.String)reader[((int)PortalColumn.SMTPUserName - 1)];
					c.SMTPPassword = (reader.IsDBNull(((int)PortalColumn.SMTPPassword - 1)))?null:(System.String)reader[((int)PortalColumn.SMTPPassword - 1)];
					c.SMTPPort = (reader.IsDBNull(((int)PortalColumn.SMTPPort - 1)))?null:(System.Int32?)reader[((int)PortalColumn.SMTPPort - 1)];
					c.SiteWideBottomJavascript = (reader.IsDBNull(((int)PortalColumn.SiteWideBottomJavascript - 1)))?null:(System.String)reader[((int)PortalColumn.SiteWideBottomJavascript - 1)];
					c.SiteWideTopJavascript = (reader.IsDBNull(((int)PortalColumn.SiteWideTopJavascript - 1)))?null:(System.String)reader[((int)PortalColumn.SiteWideTopJavascript - 1)];
					c.OrderReceiptAffiliateJavascript = (reader.IsDBNull(((int)PortalColumn.OrderReceiptAffiliateJavascript - 1)))?null:(System.String)reader[((int)PortalColumn.OrderReceiptAffiliateJavascript - 1)];
					c.SiteWideAnalyticsJavascript = (reader.IsDBNull(((int)PortalColumn.SiteWideAnalyticsJavascript - 1)))?null:(System.String)reader[((int)PortalColumn.SiteWideAnalyticsJavascript - 1)];
					c.GoogleAnalyticsCode = (reader.IsDBNull(((int)PortalColumn.GoogleAnalyticsCode - 1)))?null:(System.String)reader[((int)PortalColumn.GoogleAnalyticsCode - 1)];
					c.UPSUserName = (reader.IsDBNull(((int)PortalColumn.UPSUserName - 1)))?null:(System.String)reader[((int)PortalColumn.UPSUserName - 1)];
					c.UPSPassword = (reader.IsDBNull(((int)PortalColumn.UPSPassword - 1)))?null:(System.String)reader[((int)PortalColumn.UPSPassword - 1)];
					c.UPSKey = (reader.IsDBNull(((int)PortalColumn.UPSKey - 1)))?null:(System.String)reader[((int)PortalColumn.UPSKey - 1)];
					c.ShippingOriginZipCode = (reader.IsDBNull(((int)PortalColumn.ShippingOriginZipCode - 1)))?null:(System.String)reader[((int)PortalColumn.ShippingOriginZipCode - 1)];
					c.MasterPage = (reader.IsDBNull(((int)PortalColumn.MasterPage - 1)))?null:(System.String)reader[((int)PortalColumn.MasterPage - 1)];
					c.ShopByPriceMin = (System.Int32)reader[((int)PortalColumn.ShopByPriceMin - 1)];
					c.ShopByPriceMax = (System.Int32)reader[((int)PortalColumn.ShopByPriceMax - 1)];
					c.ShopByPriceIncrement = (System.Int32)reader[((int)PortalColumn.ShopByPriceIncrement - 1)];
					c.FedExAccountNumber = (reader.IsDBNull(((int)PortalColumn.FedExAccountNumber - 1)))?null:(System.String)reader[((int)PortalColumn.FedExAccountNumber - 1)];
					c.FedExMeterNumber = (reader.IsDBNull(((int)PortalColumn.FedExMeterNumber - 1)))?null:(System.String)reader[((int)PortalColumn.FedExMeterNumber - 1)];
					c.FedExProductionKey = (reader.IsDBNull(((int)PortalColumn.FedExProductionKey - 1)))?null:(System.String)reader[((int)PortalColumn.FedExProductionKey - 1)];
					c.FedExSecurityCode = (reader.IsDBNull(((int)PortalColumn.FedExSecurityCode - 1)))?null:(System.String)reader[((int)PortalColumn.FedExSecurityCode - 1)];
					c.FedExCSPKey = (reader.IsDBNull(((int)PortalColumn.FedExCSPKey - 1)))?null:(System.String)reader[((int)PortalColumn.FedExCSPKey - 1)];
					c.FedExCSPPassword = (reader.IsDBNull(((int)PortalColumn.FedExCSPPassword - 1)))?null:(System.String)reader[((int)PortalColumn.FedExCSPPassword - 1)];
					c.FedExClientProductId = (reader.IsDBNull(((int)PortalColumn.FedExClientProductId - 1)))?null:(System.String)reader[((int)PortalColumn.FedExClientProductId - 1)];
					c.FedExClientProductVersion = (reader.IsDBNull(((int)PortalColumn.FedExClientProductVersion - 1)))?null:(System.String)reader[((int)PortalColumn.FedExClientProductVersion - 1)];
					c.FedExDropoffType = (reader.IsDBNull(((int)PortalColumn.FedExDropoffType - 1)))?null:(System.String)reader[((int)PortalColumn.FedExDropoffType - 1)];
					c.FedExPackagingType = (reader.IsDBNull(((int)PortalColumn.FedExPackagingType - 1)))?null:(System.String)reader[((int)PortalColumn.FedExPackagingType - 1)];
					c.FedExUseDiscountRate = (reader.IsDBNull(((int)PortalColumn.FedExUseDiscountRate - 1)))?null:(System.Boolean?)reader[((int)PortalColumn.FedExUseDiscountRate - 1)];
					c.FedExAddInsurance = (reader.IsDBNull(((int)PortalColumn.FedExAddInsurance - 1)))?null:(System.Boolean?)reader[((int)PortalColumn.FedExAddInsurance - 1)];
					c.ShippingOriginAddress1 = (reader.IsDBNull(((int)PortalColumn.ShippingOriginAddress1 - 1)))?null:(System.String)reader[((int)PortalColumn.ShippingOriginAddress1 - 1)];
					c.ShippingOriginAddress2 = (reader.IsDBNull(((int)PortalColumn.ShippingOriginAddress2 - 1)))?null:(System.String)reader[((int)PortalColumn.ShippingOriginAddress2 - 1)];
					c.ShippingOriginCity = (reader.IsDBNull(((int)PortalColumn.ShippingOriginCity - 1)))?null:(System.String)reader[((int)PortalColumn.ShippingOriginCity - 1)];
					c.ShippingOriginStateCode = (reader.IsDBNull(((int)PortalColumn.ShippingOriginStateCode - 1)))?null:(System.String)reader[((int)PortalColumn.ShippingOriginStateCode - 1)];
					c.ShippingOriginCountryCode = (reader.IsDBNull(((int)PortalColumn.ShippingOriginCountryCode - 1)))?null:(System.String)reader[((int)PortalColumn.ShippingOriginCountryCode - 1)];
					c.ShippingOriginPhone = (reader.IsDBNull(((int)PortalColumn.ShippingOriginPhone - 1)))?null:(System.String)reader[((int)PortalColumn.ShippingOriginPhone - 1)];
					c.CurrencyTypeID = (reader.IsDBNull(((int)PortalColumn.CurrencyTypeID - 1)))?null:(System.Int32?)reader[((int)PortalColumn.CurrencyTypeID - 1)];
					c.WeightUnit = (reader.IsDBNull(((int)PortalColumn.WeightUnit - 1)))?null:(System.String)reader[((int)PortalColumn.WeightUnit - 1)];
					c.DimensionUnit = (reader.IsDBNull(((int)PortalColumn.DimensionUnit - 1)))?null:(System.String)reader[((int)PortalColumn.DimensionUnit - 1)];
					c.EmailListLogin = (reader.IsDBNull(((int)PortalColumn.EmailListLogin - 1)))?null:(System.String)reader[((int)PortalColumn.EmailListLogin - 1)];
					c.EmailListPassword = (reader.IsDBNull(((int)PortalColumn.EmailListPassword - 1)))?null:(System.String)reader[((int)PortalColumn.EmailListPassword - 1)];
					c.EmailListDefaultList = (reader.IsDBNull(((int)PortalColumn.EmailListDefaultList - 1)))?null:(System.String)reader[((int)PortalColumn.EmailListDefaultList - 1)];
					c.ShippingTaxable = (System.Boolean)reader[((int)PortalColumn.ShippingTaxable - 1)];
					c.DefaultOrderStateID = (reader.IsDBNull(((int)PortalColumn.DefaultOrderStateID - 1)))?null:(System.Int32?)reader[((int)PortalColumn.DefaultOrderStateID - 1)];
					c.DefaultReviewStatus = (reader.IsDBNull(((int)PortalColumn.DefaultReviewStatus - 1)))?null:(System.String)reader[((int)PortalColumn.DefaultReviewStatus - 1)];
					c.DefaultAnonymousProfileID = (reader.IsDBNull(((int)PortalColumn.DefaultAnonymousProfileID - 1)))?null:(System.Int32?)reader[((int)PortalColumn.DefaultAnonymousProfileID - 1)];
					c.DefaultRegisteredProfileID = (reader.IsDBNull(((int)PortalColumn.DefaultRegisteredProfileID - 1)))?null:(System.Int32?)reader[((int)PortalColumn.DefaultRegisteredProfileID - 1)];
					c.InclusiveTax = (System.Boolean)reader[((int)PortalColumn.InclusiveTax - 1)];
					c.SeoDefaultProductTitle = (reader.IsDBNull(((int)PortalColumn.SeoDefaultProductTitle - 1)))?null:(System.String)reader[((int)PortalColumn.SeoDefaultProductTitle - 1)];
					c.SeoDefaultProductDescription = (reader.IsDBNull(((int)PortalColumn.SeoDefaultProductDescription - 1)))?null:(System.String)reader[((int)PortalColumn.SeoDefaultProductDescription - 1)];
					c.SeoDefaultProductKeyword = (reader.IsDBNull(((int)PortalColumn.SeoDefaultProductKeyword - 1)))?null:(System.String)reader[((int)PortalColumn.SeoDefaultProductKeyword - 1)];
					c.SeoDefaultCategoryTitle = (reader.IsDBNull(((int)PortalColumn.SeoDefaultCategoryTitle - 1)))?null:(System.String)reader[((int)PortalColumn.SeoDefaultCategoryTitle - 1)];
					c.SeoDefaultCategoryDescription = (reader.IsDBNull(((int)PortalColumn.SeoDefaultCategoryDescription - 1)))?null:(System.String)reader[((int)PortalColumn.SeoDefaultCategoryDescription - 1)];
					c.SeoDefaultCategoryKeyword = (reader.IsDBNull(((int)PortalColumn.SeoDefaultCategoryKeyword - 1)))?null:(System.String)reader[((int)PortalColumn.SeoDefaultCategoryKeyword - 1)];
					c.SeoDefaultContentTitle = (reader.IsDBNull(((int)PortalColumn.SeoDefaultContentTitle - 1)))?null:(System.String)reader[((int)PortalColumn.SeoDefaultContentTitle - 1)];
					c.SeoDefaultContentDescription = (reader.IsDBNull(((int)PortalColumn.SeoDefaultContentDescription - 1)))?null:(System.String)reader[((int)PortalColumn.SeoDefaultContentDescription - 1)];
					c.SeoDefaultContentKeyword = (reader.IsDBNull(((int)PortalColumn.SeoDefaultContentKeyword - 1)))?null:(System.String)reader[((int)PortalColumn.SeoDefaultContentKeyword - 1)];
					c.TimeZoneOffset = (reader.IsDBNull(((int)PortalColumn.TimeZoneOffset - 1)))?null:(System.String)reader[((int)PortalColumn.TimeZoneOffset - 1)];
					c.LocaleID = (System.Int32)reader[((int)PortalColumn.LocaleID - 1)];
					c.SplashCategoryID = (reader.IsDBNull(((int)PortalColumn.SplashCategoryID - 1)))?null:(System.Int32?)reader[((int)PortalColumn.SplashCategoryID - 1)];
					c.SplashImageFile = (reader.IsDBNull(((int)PortalColumn.SplashImageFile - 1)))?null:(System.String)reader[((int)PortalColumn.SplashImageFile - 1)];
					c.MobileTheme = (reader.IsDBNull(((int)PortalColumn.MobileTheme - 1)))?null:(System.String)reader[((int)PortalColumn.MobileTheme - 1)];
					c.PersistentCartEnabled = (reader.IsDBNull(((int)PortalColumn.PersistentCartEnabled - 1)))?null:(System.Boolean?)reader[((int)PortalColumn.PersistentCartEnabled - 1)];
					c.DefaultProductReviewStateID = (reader.IsDBNull(((int)PortalColumn.DefaultProductReviewStateID - 1)))?null:(System.Int32?)reader[((int)PortalColumn.DefaultProductReviewStateID - 1)];
					c.UseDynamicDisplayOrder = (reader.IsDBNull(((int)PortalColumn.UseDynamicDisplayOrder - 1)))?null:(System.Boolean?)reader[((int)PortalColumn.UseDynamicDisplayOrder - 1)];
					c.EnableAddressValidation = (reader.IsDBNull(((int)PortalColumn.EnableAddressValidation - 1)))?null:(System.Boolean?)reader[((int)PortalColumn.EnableAddressValidation - 1)];
					c.RequireValidatedAddress = (reader.IsDBNull(((int)PortalColumn.RequireValidatedAddress - 1)))?null:(System.Boolean?)reader[((int)PortalColumn.RequireValidatedAddress - 1)];
					c.EnablePIMS = (reader.IsDBNull(((int)PortalColumn.EnablePIMS - 1)))?null:(System.Boolean?)reader[((int)PortalColumn.EnablePIMS - 1)];
					c.ExternalID = (reader.IsDBNull(((int)PortalColumn.ExternalID - 1)))?null:(System.String)reader[((int)PortalColumn.ExternalID - 1)];
					c.EnableCustomerPricing = (reader.IsDBNull(((int)PortalColumn.EnableCustomerPricing - 1)))?null:(System.Boolean?)reader[((int)PortalColumn.EnableCustomerPricing - 1)];
					c.IsEnableCompare = (reader.IsDBNull(((int)PortalColumn.IsEnableCompare - 1)))?null:(System.Boolean?)reader[((int)PortalColumn.IsEnableCompare - 1)];
					c.CompareType = (reader.IsDBNull(((int)PortalColumn.CompareType - 1)))?null:(System.String)reader[((int)PortalColumn.CompareType - 1)];
					c.IsMultipleCouponAllowed = (System.Boolean)reader[((int)PortalColumn.IsMultipleCouponAllowed - 1)];
					c.MobileLogoPath = (reader.IsDBNull(((int)PortalColumn.MobileLogoPath - 1)))?null:(System.String)reader[((int)PortalColumn.MobileLogoPath - 1)];
					c.IsEnableSinglePageCheckout = (reader.IsDBNull(((int)PortalColumn.IsEnableSinglePageCheckout - 1)))?null:(System.Boolean?)reader[((int)PortalColumn.IsEnableSinglePageCheckout - 1)];
					c.EnableSSLForSMTP = (reader.IsDBNull(((int)PortalColumn.EnableSSLForSMTP - 1)))?null:(System.Boolean?)reader[((int)PortalColumn.EnableSSLForSMTP - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.Portal"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.Portal"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.Portal entity)
		{
			if (!reader.Read()) return;
			
			entity.PortalID = (System.Int32)reader[((int)PortalColumn.PortalID - 1)];
			entity.CompanyName = (System.String)reader[((int)PortalColumn.CompanyName - 1)];
			entity.StoreName = (System.String)reader[((int)PortalColumn.StoreName - 1)];
			entity.LogoPath = (reader.IsDBNull(((int)PortalColumn.LogoPath - 1)))?null:(System.String)reader[((int)PortalColumn.LogoPath - 1)];
			entity.UseSSL = (System.Boolean)reader[((int)PortalColumn.UseSSL - 1)];
			entity.AdminEmail = (reader.IsDBNull(((int)PortalColumn.AdminEmail - 1)))?null:(System.String)reader[((int)PortalColumn.AdminEmail - 1)];
			entity.SalesEmail = (reader.IsDBNull(((int)PortalColumn.SalesEmail - 1)))?null:(System.String)reader[((int)PortalColumn.SalesEmail - 1)];
			entity.CustomerServiceEmail = (reader.IsDBNull(((int)PortalColumn.CustomerServiceEmail - 1)))?null:(System.String)reader[((int)PortalColumn.CustomerServiceEmail - 1)];
			entity.SalesPhoneNumber = (reader.IsDBNull(((int)PortalColumn.SalesPhoneNumber - 1)))?null:(System.String)reader[((int)PortalColumn.SalesPhoneNumber - 1)];
			entity.CustomerServicePhoneNumber = (reader.IsDBNull(((int)PortalColumn.CustomerServicePhoneNumber - 1)))?null:(System.String)reader[((int)PortalColumn.CustomerServicePhoneNumber - 1)];
			entity.ImageNotAvailablePath = (System.String)reader[((int)PortalColumn.ImageNotAvailablePath - 1)];
			entity.MaxCatalogDisplayColumns = (System.Byte)reader[((int)PortalColumn.MaxCatalogDisplayColumns - 1)];
			entity.MaxCatalogDisplayItems = (System.Int32)reader[((int)PortalColumn.MaxCatalogDisplayItems - 1)];
			entity.MaxCatalogCategoryDisplayThumbnails = (System.Int32)reader[((int)PortalColumn.MaxCatalogCategoryDisplayThumbnails - 1)];
			entity.MaxCatalogItemSmallThumbnailWidth = (System.Int32)reader[((int)PortalColumn.MaxCatalogItemSmallThumbnailWidth - 1)];
			entity.MaxCatalogItemSmallWidth = (System.Int32)reader[((int)PortalColumn.MaxCatalogItemSmallWidth - 1)];
			entity.MaxCatalogItemMediumWidth = (System.Int32)reader[((int)PortalColumn.MaxCatalogItemMediumWidth - 1)];
			entity.MaxCatalogItemThumbnailWidth = (System.Int32)reader[((int)PortalColumn.MaxCatalogItemThumbnailWidth - 1)];
			entity.MaxCatalogItemLargeWidth = (System.Int32)reader[((int)PortalColumn.MaxCatalogItemLargeWidth - 1)];
			entity.MaxCatalogItemCrossSellWidth = (System.Int32)reader[((int)PortalColumn.MaxCatalogItemCrossSellWidth - 1)];
			entity.ShowSwatchInCategory = (System.Boolean)reader[((int)PortalColumn.ShowSwatchInCategory - 1)];
			entity.ShowAlternateImageInCategory = (System.Boolean)reader[((int)PortalColumn.ShowAlternateImageInCategory - 1)];
			entity.ActiveInd = (System.Boolean)reader[((int)PortalColumn.ActiveInd - 1)];
			entity.SMTPServer = (reader.IsDBNull(((int)PortalColumn.SMTPServer - 1)))?null:(System.String)reader[((int)PortalColumn.SMTPServer - 1)];
			entity.SMTPUserName = (reader.IsDBNull(((int)PortalColumn.SMTPUserName - 1)))?null:(System.String)reader[((int)PortalColumn.SMTPUserName - 1)];
			entity.SMTPPassword = (reader.IsDBNull(((int)PortalColumn.SMTPPassword - 1)))?null:(System.String)reader[((int)PortalColumn.SMTPPassword - 1)];
			entity.SMTPPort = (reader.IsDBNull(((int)PortalColumn.SMTPPort - 1)))?null:(System.Int32?)reader[((int)PortalColumn.SMTPPort - 1)];
			entity.SiteWideBottomJavascript = (reader.IsDBNull(((int)PortalColumn.SiteWideBottomJavascript - 1)))?null:(System.String)reader[((int)PortalColumn.SiteWideBottomJavascript - 1)];
			entity.SiteWideTopJavascript = (reader.IsDBNull(((int)PortalColumn.SiteWideTopJavascript - 1)))?null:(System.String)reader[((int)PortalColumn.SiteWideTopJavascript - 1)];
			entity.OrderReceiptAffiliateJavascript = (reader.IsDBNull(((int)PortalColumn.OrderReceiptAffiliateJavascript - 1)))?null:(System.String)reader[((int)PortalColumn.OrderReceiptAffiliateJavascript - 1)];
			entity.SiteWideAnalyticsJavascript = (reader.IsDBNull(((int)PortalColumn.SiteWideAnalyticsJavascript - 1)))?null:(System.String)reader[((int)PortalColumn.SiteWideAnalyticsJavascript - 1)];
			entity.GoogleAnalyticsCode = (reader.IsDBNull(((int)PortalColumn.GoogleAnalyticsCode - 1)))?null:(System.String)reader[((int)PortalColumn.GoogleAnalyticsCode - 1)];
			entity.UPSUserName = (reader.IsDBNull(((int)PortalColumn.UPSUserName - 1)))?null:(System.String)reader[((int)PortalColumn.UPSUserName - 1)];
			entity.UPSPassword = (reader.IsDBNull(((int)PortalColumn.UPSPassword - 1)))?null:(System.String)reader[((int)PortalColumn.UPSPassword - 1)];
			entity.UPSKey = (reader.IsDBNull(((int)PortalColumn.UPSKey - 1)))?null:(System.String)reader[((int)PortalColumn.UPSKey - 1)];
			entity.ShippingOriginZipCode = (reader.IsDBNull(((int)PortalColumn.ShippingOriginZipCode - 1)))?null:(System.String)reader[((int)PortalColumn.ShippingOriginZipCode - 1)];
			entity.MasterPage = (reader.IsDBNull(((int)PortalColumn.MasterPage - 1)))?null:(System.String)reader[((int)PortalColumn.MasterPage - 1)];
			entity.ShopByPriceMin = (System.Int32)reader[((int)PortalColumn.ShopByPriceMin - 1)];
			entity.ShopByPriceMax = (System.Int32)reader[((int)PortalColumn.ShopByPriceMax - 1)];
			entity.ShopByPriceIncrement = (System.Int32)reader[((int)PortalColumn.ShopByPriceIncrement - 1)];
			entity.FedExAccountNumber = (reader.IsDBNull(((int)PortalColumn.FedExAccountNumber - 1)))?null:(System.String)reader[((int)PortalColumn.FedExAccountNumber - 1)];
			entity.FedExMeterNumber = (reader.IsDBNull(((int)PortalColumn.FedExMeterNumber - 1)))?null:(System.String)reader[((int)PortalColumn.FedExMeterNumber - 1)];
			entity.FedExProductionKey = (reader.IsDBNull(((int)PortalColumn.FedExProductionKey - 1)))?null:(System.String)reader[((int)PortalColumn.FedExProductionKey - 1)];
			entity.FedExSecurityCode = (reader.IsDBNull(((int)PortalColumn.FedExSecurityCode - 1)))?null:(System.String)reader[((int)PortalColumn.FedExSecurityCode - 1)];
			entity.FedExCSPKey = (reader.IsDBNull(((int)PortalColumn.FedExCSPKey - 1)))?null:(System.String)reader[((int)PortalColumn.FedExCSPKey - 1)];
			entity.FedExCSPPassword = (reader.IsDBNull(((int)PortalColumn.FedExCSPPassword - 1)))?null:(System.String)reader[((int)PortalColumn.FedExCSPPassword - 1)];
			entity.FedExClientProductId = (reader.IsDBNull(((int)PortalColumn.FedExClientProductId - 1)))?null:(System.String)reader[((int)PortalColumn.FedExClientProductId - 1)];
			entity.FedExClientProductVersion = (reader.IsDBNull(((int)PortalColumn.FedExClientProductVersion - 1)))?null:(System.String)reader[((int)PortalColumn.FedExClientProductVersion - 1)];
			entity.FedExDropoffType = (reader.IsDBNull(((int)PortalColumn.FedExDropoffType - 1)))?null:(System.String)reader[((int)PortalColumn.FedExDropoffType - 1)];
			entity.FedExPackagingType = (reader.IsDBNull(((int)PortalColumn.FedExPackagingType - 1)))?null:(System.String)reader[((int)PortalColumn.FedExPackagingType - 1)];
			entity.FedExUseDiscountRate = (reader.IsDBNull(((int)PortalColumn.FedExUseDiscountRate - 1)))?null:(System.Boolean?)reader[((int)PortalColumn.FedExUseDiscountRate - 1)];
			entity.FedExAddInsurance = (reader.IsDBNull(((int)PortalColumn.FedExAddInsurance - 1)))?null:(System.Boolean?)reader[((int)PortalColumn.FedExAddInsurance - 1)];
			entity.ShippingOriginAddress1 = (reader.IsDBNull(((int)PortalColumn.ShippingOriginAddress1 - 1)))?null:(System.String)reader[((int)PortalColumn.ShippingOriginAddress1 - 1)];
			entity.ShippingOriginAddress2 = (reader.IsDBNull(((int)PortalColumn.ShippingOriginAddress2 - 1)))?null:(System.String)reader[((int)PortalColumn.ShippingOriginAddress2 - 1)];
			entity.ShippingOriginCity = (reader.IsDBNull(((int)PortalColumn.ShippingOriginCity - 1)))?null:(System.String)reader[((int)PortalColumn.ShippingOriginCity - 1)];
			entity.ShippingOriginStateCode = (reader.IsDBNull(((int)PortalColumn.ShippingOriginStateCode - 1)))?null:(System.String)reader[((int)PortalColumn.ShippingOriginStateCode - 1)];
			entity.ShippingOriginCountryCode = (reader.IsDBNull(((int)PortalColumn.ShippingOriginCountryCode - 1)))?null:(System.String)reader[((int)PortalColumn.ShippingOriginCountryCode - 1)];
			entity.ShippingOriginPhone = (reader.IsDBNull(((int)PortalColumn.ShippingOriginPhone - 1)))?null:(System.String)reader[((int)PortalColumn.ShippingOriginPhone - 1)];
			entity.CurrencyTypeID = (reader.IsDBNull(((int)PortalColumn.CurrencyTypeID - 1)))?null:(System.Int32?)reader[((int)PortalColumn.CurrencyTypeID - 1)];
			entity.WeightUnit = (reader.IsDBNull(((int)PortalColumn.WeightUnit - 1)))?null:(System.String)reader[((int)PortalColumn.WeightUnit - 1)];
			entity.DimensionUnit = (reader.IsDBNull(((int)PortalColumn.DimensionUnit - 1)))?null:(System.String)reader[((int)PortalColumn.DimensionUnit - 1)];
			entity.EmailListLogin = (reader.IsDBNull(((int)PortalColumn.EmailListLogin - 1)))?null:(System.String)reader[((int)PortalColumn.EmailListLogin - 1)];
			entity.EmailListPassword = (reader.IsDBNull(((int)PortalColumn.EmailListPassword - 1)))?null:(System.String)reader[((int)PortalColumn.EmailListPassword - 1)];
			entity.EmailListDefaultList = (reader.IsDBNull(((int)PortalColumn.EmailListDefaultList - 1)))?null:(System.String)reader[((int)PortalColumn.EmailListDefaultList - 1)];
			entity.ShippingTaxable = (System.Boolean)reader[((int)PortalColumn.ShippingTaxable - 1)];
			entity.DefaultOrderStateID = (reader.IsDBNull(((int)PortalColumn.DefaultOrderStateID - 1)))?null:(System.Int32?)reader[((int)PortalColumn.DefaultOrderStateID - 1)];
			entity.DefaultReviewStatus = (reader.IsDBNull(((int)PortalColumn.DefaultReviewStatus - 1)))?null:(System.String)reader[((int)PortalColumn.DefaultReviewStatus - 1)];
			entity.DefaultAnonymousProfileID = (reader.IsDBNull(((int)PortalColumn.DefaultAnonymousProfileID - 1)))?null:(System.Int32?)reader[((int)PortalColumn.DefaultAnonymousProfileID - 1)];
			entity.DefaultRegisteredProfileID = (reader.IsDBNull(((int)PortalColumn.DefaultRegisteredProfileID - 1)))?null:(System.Int32?)reader[((int)PortalColumn.DefaultRegisteredProfileID - 1)];
			entity.InclusiveTax = (System.Boolean)reader[((int)PortalColumn.InclusiveTax - 1)];
			entity.SeoDefaultProductTitle = (reader.IsDBNull(((int)PortalColumn.SeoDefaultProductTitle - 1)))?null:(System.String)reader[((int)PortalColumn.SeoDefaultProductTitle - 1)];
			entity.SeoDefaultProductDescription = (reader.IsDBNull(((int)PortalColumn.SeoDefaultProductDescription - 1)))?null:(System.String)reader[((int)PortalColumn.SeoDefaultProductDescription - 1)];
			entity.SeoDefaultProductKeyword = (reader.IsDBNull(((int)PortalColumn.SeoDefaultProductKeyword - 1)))?null:(System.String)reader[((int)PortalColumn.SeoDefaultProductKeyword - 1)];
			entity.SeoDefaultCategoryTitle = (reader.IsDBNull(((int)PortalColumn.SeoDefaultCategoryTitle - 1)))?null:(System.String)reader[((int)PortalColumn.SeoDefaultCategoryTitle - 1)];
			entity.SeoDefaultCategoryDescription = (reader.IsDBNull(((int)PortalColumn.SeoDefaultCategoryDescription - 1)))?null:(System.String)reader[((int)PortalColumn.SeoDefaultCategoryDescription - 1)];
			entity.SeoDefaultCategoryKeyword = (reader.IsDBNull(((int)PortalColumn.SeoDefaultCategoryKeyword - 1)))?null:(System.String)reader[((int)PortalColumn.SeoDefaultCategoryKeyword - 1)];
			entity.SeoDefaultContentTitle = (reader.IsDBNull(((int)PortalColumn.SeoDefaultContentTitle - 1)))?null:(System.String)reader[((int)PortalColumn.SeoDefaultContentTitle - 1)];
			entity.SeoDefaultContentDescription = (reader.IsDBNull(((int)PortalColumn.SeoDefaultContentDescription - 1)))?null:(System.String)reader[((int)PortalColumn.SeoDefaultContentDescription - 1)];
			entity.SeoDefaultContentKeyword = (reader.IsDBNull(((int)PortalColumn.SeoDefaultContentKeyword - 1)))?null:(System.String)reader[((int)PortalColumn.SeoDefaultContentKeyword - 1)];
			entity.TimeZoneOffset = (reader.IsDBNull(((int)PortalColumn.TimeZoneOffset - 1)))?null:(System.String)reader[((int)PortalColumn.TimeZoneOffset - 1)];
			entity.LocaleID = (System.Int32)reader[((int)PortalColumn.LocaleID - 1)];
			entity.SplashCategoryID = (reader.IsDBNull(((int)PortalColumn.SplashCategoryID - 1)))?null:(System.Int32?)reader[((int)PortalColumn.SplashCategoryID - 1)];
			entity.SplashImageFile = (reader.IsDBNull(((int)PortalColumn.SplashImageFile - 1)))?null:(System.String)reader[((int)PortalColumn.SplashImageFile - 1)];
			entity.MobileTheme = (reader.IsDBNull(((int)PortalColumn.MobileTheme - 1)))?null:(System.String)reader[((int)PortalColumn.MobileTheme - 1)];
			entity.PersistentCartEnabled = (reader.IsDBNull(((int)PortalColumn.PersistentCartEnabled - 1)))?null:(System.Boolean?)reader[((int)PortalColumn.PersistentCartEnabled - 1)];
			entity.DefaultProductReviewStateID = (reader.IsDBNull(((int)PortalColumn.DefaultProductReviewStateID - 1)))?null:(System.Int32?)reader[((int)PortalColumn.DefaultProductReviewStateID - 1)];
			entity.UseDynamicDisplayOrder = (reader.IsDBNull(((int)PortalColumn.UseDynamicDisplayOrder - 1)))?null:(System.Boolean?)reader[((int)PortalColumn.UseDynamicDisplayOrder - 1)];
			entity.EnableAddressValidation = (reader.IsDBNull(((int)PortalColumn.EnableAddressValidation - 1)))?null:(System.Boolean?)reader[((int)PortalColumn.EnableAddressValidation - 1)];
			entity.RequireValidatedAddress = (reader.IsDBNull(((int)PortalColumn.RequireValidatedAddress - 1)))?null:(System.Boolean?)reader[((int)PortalColumn.RequireValidatedAddress - 1)];
			entity.EnablePIMS = (reader.IsDBNull(((int)PortalColumn.EnablePIMS - 1)))?null:(System.Boolean?)reader[((int)PortalColumn.EnablePIMS - 1)];
			entity.ExternalID = (reader.IsDBNull(((int)PortalColumn.ExternalID - 1)))?null:(System.String)reader[((int)PortalColumn.ExternalID - 1)];
			entity.EnableCustomerPricing = (reader.IsDBNull(((int)PortalColumn.EnableCustomerPricing - 1)))?null:(System.Boolean?)reader[((int)PortalColumn.EnableCustomerPricing - 1)];
			entity.IsEnableCompare = (reader.IsDBNull(((int)PortalColumn.IsEnableCompare - 1)))?null:(System.Boolean?)reader[((int)PortalColumn.IsEnableCompare - 1)];
			entity.CompareType = (reader.IsDBNull(((int)PortalColumn.CompareType - 1)))?null:(System.String)reader[((int)PortalColumn.CompareType - 1)];
			entity.IsMultipleCouponAllowed = (System.Boolean)reader[((int)PortalColumn.IsMultipleCouponAllowed - 1)];
			entity.MobileLogoPath = (reader.IsDBNull(((int)PortalColumn.MobileLogoPath - 1)))?null:(System.String)reader[((int)PortalColumn.MobileLogoPath - 1)];
			entity.IsEnableSinglePageCheckout = (reader.IsDBNull(((int)PortalColumn.IsEnableSinglePageCheckout - 1)))?null:(System.Boolean?)reader[((int)PortalColumn.IsEnableSinglePageCheckout - 1)];
			entity.EnableSSLForSMTP = (reader.IsDBNull(((int)PortalColumn.EnableSSLForSMTP - 1)))?null:(System.Boolean?)reader[((int)PortalColumn.EnableSSLForSMTP - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.Portal"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.Portal"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.Portal entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.PortalID = (System.Int32)dataRow["PortalID"];
			entity.CompanyName = (System.String)dataRow["CompanyName"];
			entity.StoreName = (System.String)dataRow["StoreName"];
			entity.LogoPath = Convert.IsDBNull(dataRow["LogoPath"]) ? null : (System.String)dataRow["LogoPath"];
			entity.UseSSL = (System.Boolean)dataRow["UseSSL"];
			entity.AdminEmail = Convert.IsDBNull(dataRow["AdminEmail"]) ? null : (System.String)dataRow["AdminEmail"];
			entity.SalesEmail = Convert.IsDBNull(dataRow["SalesEmail"]) ? null : (System.String)dataRow["SalesEmail"];
			entity.CustomerServiceEmail = Convert.IsDBNull(dataRow["CustomerServiceEmail"]) ? null : (System.String)dataRow["CustomerServiceEmail"];
			entity.SalesPhoneNumber = Convert.IsDBNull(dataRow["SalesPhoneNumber"]) ? null : (System.String)dataRow["SalesPhoneNumber"];
			entity.CustomerServicePhoneNumber = Convert.IsDBNull(dataRow["CustomerServicePhoneNumber"]) ? null : (System.String)dataRow["CustomerServicePhoneNumber"];
			entity.ImageNotAvailablePath = (System.String)dataRow["ImageNotAvailablePath"];
			entity.MaxCatalogDisplayColumns = (System.Byte)dataRow["MaxCatalogDisplayColumns"];
			entity.MaxCatalogDisplayItems = (System.Int32)dataRow["MaxCatalogDisplayItems"];
			entity.MaxCatalogCategoryDisplayThumbnails = (System.Int32)dataRow["MaxCatalogCategoryDisplayThumbnails"];
			entity.MaxCatalogItemSmallThumbnailWidth = (System.Int32)dataRow["MaxCatalogItemSmallThumbnailWidth"];
			entity.MaxCatalogItemSmallWidth = (System.Int32)dataRow["MaxCatalogItemSmallWidth"];
			entity.MaxCatalogItemMediumWidth = (System.Int32)dataRow["MaxCatalogItemMediumWidth"];
			entity.MaxCatalogItemThumbnailWidth = (System.Int32)dataRow["MaxCatalogItemThumbnailWidth"];
			entity.MaxCatalogItemLargeWidth = (System.Int32)dataRow["MaxCatalogItemLargeWidth"];
			entity.MaxCatalogItemCrossSellWidth = (System.Int32)dataRow["MaxCatalogItemCrossSellWidth"];
			entity.ShowSwatchInCategory = (System.Boolean)dataRow["ShowSwatchInCategory"];
			entity.ShowAlternateImageInCategory = (System.Boolean)dataRow["ShowAlternateImageInCategory"];
			entity.ActiveInd = (System.Boolean)dataRow["ActiveInd"];
			entity.SMTPServer = Convert.IsDBNull(dataRow["SMTPServer"]) ? null : (System.String)dataRow["SMTPServer"];
			entity.SMTPUserName = Convert.IsDBNull(dataRow["SMTPUserName"]) ? null : (System.String)dataRow["SMTPUserName"];
			entity.SMTPPassword = Convert.IsDBNull(dataRow["SMTPPassword"]) ? null : (System.String)dataRow["SMTPPassword"];
			entity.SMTPPort = Convert.IsDBNull(dataRow["SMTPPort"]) ? null : (System.Int32?)dataRow["SMTPPort"];
			entity.SiteWideBottomJavascript = Convert.IsDBNull(dataRow["SiteWideBottomJavascript"]) ? null : (System.String)dataRow["SiteWideBottomJavascript"];
			entity.SiteWideTopJavascript = Convert.IsDBNull(dataRow["SiteWideTopJavascript"]) ? null : (System.String)dataRow["SiteWideTopJavascript"];
			entity.OrderReceiptAffiliateJavascript = Convert.IsDBNull(dataRow["OrderReceiptAffiliateJavascript"]) ? null : (System.String)dataRow["OrderReceiptAffiliateJavascript"];
			entity.SiteWideAnalyticsJavascript = Convert.IsDBNull(dataRow["SiteWideAnalyticsJavascript"]) ? null : (System.String)dataRow["SiteWideAnalyticsJavascript"];
			entity.GoogleAnalyticsCode = Convert.IsDBNull(dataRow["GoogleAnalyticsCode"]) ? null : (System.String)dataRow["GoogleAnalyticsCode"];
			entity.UPSUserName = Convert.IsDBNull(dataRow["UPSUserName"]) ? null : (System.String)dataRow["UPSUserName"];
			entity.UPSPassword = Convert.IsDBNull(dataRow["UPSPassword"]) ? null : (System.String)dataRow["UPSPassword"];
			entity.UPSKey = Convert.IsDBNull(dataRow["UPSKey"]) ? null : (System.String)dataRow["UPSKey"];
			entity.ShippingOriginZipCode = Convert.IsDBNull(dataRow["ShippingOriginZipCode"]) ? null : (System.String)dataRow["ShippingOriginZipCode"];
			entity.MasterPage = Convert.IsDBNull(dataRow["MasterPage"]) ? null : (System.String)dataRow["MasterPage"];
			entity.ShopByPriceMin = (System.Int32)dataRow["ShopByPriceMin"];
			entity.ShopByPriceMax = (System.Int32)dataRow["ShopByPriceMax"];
			entity.ShopByPriceIncrement = (System.Int32)dataRow["ShopByPriceIncrement"];
			entity.FedExAccountNumber = Convert.IsDBNull(dataRow["FedExAccountNumber"]) ? null : (System.String)dataRow["FedExAccountNumber"];
			entity.FedExMeterNumber = Convert.IsDBNull(dataRow["FedExMeterNumber"]) ? null : (System.String)dataRow["FedExMeterNumber"];
			entity.FedExProductionKey = Convert.IsDBNull(dataRow["FedExProductionKey"]) ? null : (System.String)dataRow["FedExProductionKey"];
			entity.FedExSecurityCode = Convert.IsDBNull(dataRow["FedExSecurityCode"]) ? null : (System.String)dataRow["FedExSecurityCode"];
			entity.FedExCSPKey = Convert.IsDBNull(dataRow["FedExCSPKey"]) ? null : (System.String)dataRow["FedExCSPKey"];
			entity.FedExCSPPassword = Convert.IsDBNull(dataRow["FedExCSPPassword"]) ? null : (System.String)dataRow["FedExCSPPassword"];
			entity.FedExClientProductId = Convert.IsDBNull(dataRow["FedExClientProductId"]) ? null : (System.String)dataRow["FedExClientProductId"];
			entity.FedExClientProductVersion = Convert.IsDBNull(dataRow["FedExClientProductVersion"]) ? null : (System.String)dataRow["FedExClientProductVersion"];
			entity.FedExDropoffType = Convert.IsDBNull(dataRow["FedExDropoffType"]) ? null : (System.String)dataRow["FedExDropoffType"];
			entity.FedExPackagingType = Convert.IsDBNull(dataRow["FedExPackagingType"]) ? null : (System.String)dataRow["FedExPackagingType"];
			entity.FedExUseDiscountRate = Convert.IsDBNull(dataRow["FedExUseDiscountRate"]) ? null : (System.Boolean?)dataRow["FedExUseDiscountRate"];
			entity.FedExAddInsurance = Convert.IsDBNull(dataRow["FedExAddInsurance"]) ? null : (System.Boolean?)dataRow["FedExAddInsurance"];
			entity.ShippingOriginAddress1 = Convert.IsDBNull(dataRow["ShippingOriginAddress1"]) ? null : (System.String)dataRow["ShippingOriginAddress1"];
			entity.ShippingOriginAddress2 = Convert.IsDBNull(dataRow["ShippingOriginAddress2"]) ? null : (System.String)dataRow["ShippingOriginAddress2"];
			entity.ShippingOriginCity = Convert.IsDBNull(dataRow["ShippingOriginCity"]) ? null : (System.String)dataRow["ShippingOriginCity"];
			entity.ShippingOriginStateCode = Convert.IsDBNull(dataRow["ShippingOriginStateCode"]) ? null : (System.String)dataRow["ShippingOriginStateCode"];
			entity.ShippingOriginCountryCode = Convert.IsDBNull(dataRow["ShippingOriginCountryCode"]) ? null : (System.String)dataRow["ShippingOriginCountryCode"];
			entity.ShippingOriginPhone = Convert.IsDBNull(dataRow["ShippingOriginPhone"]) ? null : (System.String)dataRow["ShippingOriginPhone"];
			entity.CurrencyTypeID = Convert.IsDBNull(dataRow["CurrencyTypeID"]) ? null : (System.Int32?)dataRow["CurrencyTypeID"];
			entity.WeightUnit = Convert.IsDBNull(dataRow["WeightUnit"]) ? null : (System.String)dataRow["WeightUnit"];
			entity.DimensionUnit = Convert.IsDBNull(dataRow["DimensionUnit"]) ? null : (System.String)dataRow["DimensionUnit"];
			entity.EmailListLogin = Convert.IsDBNull(dataRow["EmailListLogin"]) ? null : (System.String)dataRow["EmailListLogin"];
			entity.EmailListPassword = Convert.IsDBNull(dataRow["EmailListPassword"]) ? null : (System.String)dataRow["EmailListPassword"];
			entity.EmailListDefaultList = Convert.IsDBNull(dataRow["EmailListDefaultList"]) ? null : (System.String)dataRow["EmailListDefaultList"];
			entity.ShippingTaxable = (System.Boolean)dataRow["ShippingTaxable"];
			entity.DefaultOrderStateID = Convert.IsDBNull(dataRow["DefaultOrderStateID"]) ? null : (System.Int32?)dataRow["DefaultOrderStateID"];
			entity.DefaultReviewStatus = Convert.IsDBNull(dataRow["DefaultReviewStatus"]) ? null : (System.String)dataRow["DefaultReviewStatus"];
			entity.DefaultAnonymousProfileID = Convert.IsDBNull(dataRow["DefaultAnonymousProfileID"]) ? null : (System.Int32?)dataRow["DefaultAnonymousProfileID"];
			entity.DefaultRegisteredProfileID = Convert.IsDBNull(dataRow["DefaultRegisteredProfileID"]) ? null : (System.Int32?)dataRow["DefaultRegisteredProfileID"];
			entity.InclusiveTax = (System.Boolean)dataRow["InclusiveTax"];
			entity.SeoDefaultProductTitle = Convert.IsDBNull(dataRow["SeoDefaultProductTitle"]) ? null : (System.String)dataRow["SeoDefaultProductTitle"];
			entity.SeoDefaultProductDescription = Convert.IsDBNull(dataRow["SeoDefaultProductDescription"]) ? null : (System.String)dataRow["SeoDefaultProductDescription"];
			entity.SeoDefaultProductKeyword = Convert.IsDBNull(dataRow["SeoDefaultProductKeyword"]) ? null : (System.String)dataRow["SeoDefaultProductKeyword"];
			entity.SeoDefaultCategoryTitle = Convert.IsDBNull(dataRow["SeoDefaultCategoryTitle"]) ? null : (System.String)dataRow["SeoDefaultCategoryTitle"];
			entity.SeoDefaultCategoryDescription = Convert.IsDBNull(dataRow["SeoDefaultCategoryDescription"]) ? null : (System.String)dataRow["SeoDefaultCategoryDescription"];
			entity.SeoDefaultCategoryKeyword = Convert.IsDBNull(dataRow["SeoDefaultCategoryKeyword"]) ? null : (System.String)dataRow["SeoDefaultCategoryKeyword"];
			entity.SeoDefaultContentTitle = Convert.IsDBNull(dataRow["SeoDefaultContentTitle"]) ? null : (System.String)dataRow["SeoDefaultContentTitle"];
			entity.SeoDefaultContentDescription = Convert.IsDBNull(dataRow["SeoDefaultContentDescription"]) ? null : (System.String)dataRow["SeoDefaultContentDescription"];
			entity.SeoDefaultContentKeyword = Convert.IsDBNull(dataRow["SeoDefaultContentKeyword"]) ? null : (System.String)dataRow["SeoDefaultContentKeyword"];
			entity.TimeZoneOffset = Convert.IsDBNull(dataRow["TimeZoneOffset"]) ? null : (System.String)dataRow["TimeZoneOffset"];
			entity.LocaleID = (System.Int32)dataRow["LocaleID"];
			entity.SplashCategoryID = Convert.IsDBNull(dataRow["SplashCategoryID"]) ? null : (System.Int32?)dataRow["SplashCategoryID"];
			entity.SplashImageFile = Convert.IsDBNull(dataRow["SplashImageFile"]) ? null : (System.String)dataRow["SplashImageFile"];
			entity.MobileTheme = Convert.IsDBNull(dataRow["MobileTheme"]) ? null : (System.String)dataRow["MobileTheme"];
			entity.PersistentCartEnabled = Convert.IsDBNull(dataRow["PersistentCartEnabled"]) ? null : (System.Boolean?)dataRow["PersistentCartEnabled"];
			entity.DefaultProductReviewStateID = Convert.IsDBNull(dataRow["DefaultProductReviewStateID"]) ? null : (System.Int32?)dataRow["DefaultProductReviewStateID"];
			entity.UseDynamicDisplayOrder = Convert.IsDBNull(dataRow["UseDynamicDisplayOrder"]) ? null : (System.Boolean?)dataRow["UseDynamicDisplayOrder"];
			entity.EnableAddressValidation = Convert.IsDBNull(dataRow["EnableAddressValidation"]) ? null : (System.Boolean?)dataRow["EnableAddressValidation"];
			entity.RequireValidatedAddress = Convert.IsDBNull(dataRow["RequireValidatedAddress"]) ? null : (System.Boolean?)dataRow["RequireValidatedAddress"];
			entity.EnablePIMS = Convert.IsDBNull(dataRow["EnablePIMS"]) ? null : (System.Boolean?)dataRow["EnablePIMS"];
			entity.ExternalID = Convert.IsDBNull(dataRow["ExternalID"]) ? null : (System.String)dataRow["ExternalID"];
			entity.EnableCustomerPricing = Convert.IsDBNull(dataRow["EnableCustomerPricing"]) ? null : (System.Boolean?)dataRow["EnableCustomerPricing"];
			entity.IsEnableCompare = Convert.IsDBNull(dataRow["IsEnableCompare"]) ? null : (System.Boolean?)dataRow["IsEnableCompare"];
			entity.CompareType = Convert.IsDBNull(dataRow["CompareType"]) ? null : (System.String)dataRow["CompareType"];
			entity.IsMultipleCouponAllowed = (System.Boolean)dataRow["IsMultipleCouponAllowed"];
			entity.MobileLogoPath = Convert.IsDBNull(dataRow["MobileLogoPath"]) ? null : (System.String)dataRow["MobileLogoPath"];
			entity.IsEnableSinglePageCheckout = Convert.IsDBNull(dataRow["IsEnableSinglePageCheckout"]) ? null : (System.Boolean?)dataRow["IsEnableSinglePageCheckout"];
			entity.EnableSSLForSMTP = Convert.IsDBNull(dataRow["EnableSSLForSMTP"]) ? null : (System.Boolean?)dataRow["EnableSSLForSMTP"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.Portal"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.Portal Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.Portal entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region CurrencyTypeIDSource	
			if (CanDeepLoad(entity, "CurrencyType|CurrencyTypeIDSource", deepLoadType, innerList) 
				&& entity.CurrencyTypeIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.CurrencyTypeID ?? (int)0);
				CurrencyType tmpEntity = EntityManager.LocateEntity<CurrencyType>(EntityLocator.ConstructKeyFromPkItems(typeof(CurrencyType), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CurrencyTypeIDSource = tmpEntity;
				else
					entity.CurrencyTypeIDSource = DataRepository.CurrencyTypeProvider.GetByCurrencyTypeID(transactionManager, (entity.CurrencyTypeID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CurrencyTypeIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CurrencyTypeIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CurrencyTypeProvider.DeepLoad(transactionManager, entity.CurrencyTypeIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CurrencyTypeIDSource

			#region LocaleIDSource	
			if (CanDeepLoad(entity, "Locale|LocaleIDSource", deepLoadType, innerList) 
				&& entity.LocaleIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.LocaleID;
				Locale tmpEntity = EntityManager.LocateEntity<Locale>(EntityLocator.ConstructKeyFromPkItems(typeof(Locale), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.LocaleIDSource = tmpEntity;
				else
					entity.LocaleIDSource = DataRepository.LocaleProvider.GetByLocaleID(transactionManager, entity.LocaleID);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'LocaleIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.LocaleIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.LocaleProvider.DeepLoad(transactionManager, entity.LocaleIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion LocaleIDSource

			#region DefaultOrderStateIDSource	
			if (CanDeepLoad(entity, "OrderState|DefaultOrderStateIDSource", deepLoadType, innerList) 
				&& entity.DefaultOrderStateIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.DefaultOrderStateID ?? (int)0);
				OrderState tmpEntity = EntityManager.LocateEntity<OrderState>(EntityLocator.ConstructKeyFromPkItems(typeof(OrderState), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.DefaultOrderStateIDSource = tmpEntity;
				else
					entity.DefaultOrderStateIDSource = DataRepository.OrderStateProvider.GetByOrderStateID(transactionManager, (entity.DefaultOrderStateID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'DefaultOrderStateIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.DefaultOrderStateIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.OrderStateProvider.DeepLoad(transactionManager, entity.DefaultOrderStateIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion DefaultOrderStateIDSource

			#region DefaultProductReviewStateIDSource	
			if (CanDeepLoad(entity, "ProductReviewState|DefaultProductReviewStateIDSource", deepLoadType, innerList) 
				&& entity.DefaultProductReviewStateIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.DefaultProductReviewStateID ?? (int)0);
				ProductReviewState tmpEntity = EntityManager.LocateEntity<ProductReviewState>(EntityLocator.ConstructKeyFromPkItems(typeof(ProductReviewState), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.DefaultProductReviewStateIDSource = tmpEntity;
				else
					entity.DefaultProductReviewStateIDSource = DataRepository.ProductReviewStateProvider.GetByReviewStateID(transactionManager, (entity.DefaultProductReviewStateID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'DefaultProductReviewStateIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.DefaultProductReviewStateIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.ProductReviewStateProvider.DeepLoad(transactionManager, entity.DefaultProductReviewStateIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion DefaultProductReviewStateIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByPortalID methods when available
			
			#region PortalCountryCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<PortalCountry>|PortalCountryCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PortalCountryCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.PortalCountryCollection = DataRepository.PortalCountryProvider.GetByPortalID(transactionManager, entity.PortalID);

				if (deep && entity.PortalCountryCollection.Count > 0)
				{
					deepHandles.Add("PortalCountryCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<PortalCountry>) DataRepository.PortalCountryProvider.DeepLoad,
						new object[] { transactionManager, entity.PortalCountryCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region CookieMappingCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<CookieMapping>|CookieMappingCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CookieMappingCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CookieMappingCollection = DataRepository.CookieMappingProvider.GetByPortalID(transactionManager, entity.PortalID);

				if (deep && entity.CookieMappingCollection.Count > 0)
				{
					deepHandles.Add("CookieMappingCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<CookieMapping>) DataRepository.CookieMappingProvider.DeepLoad,
						new object[] { transactionManager, entity.CookieMappingCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region ContentPageCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<ContentPage>|ContentPageCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ContentPageCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ContentPageCollection = DataRepository.ContentPageProvider.GetByPortalID(transactionManager, entity.PortalID);

				if (deep && entity.ContentPageCollection.Count > 0)
				{
					deepHandles.Add("ContentPageCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<ContentPage>) DataRepository.ContentPageProvider.DeepLoad,
						new object[] { transactionManager, entity.ContentPageCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region PortalProfileCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<PortalProfile>|PortalProfileCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PortalProfileCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.PortalProfileCollection = DataRepository.PortalProfileProvider.GetByPortalID(transactionManager, entity.PortalID);

				if (deep && entity.PortalProfileCollection.Count > 0)
				{
					deepHandles.Add("PortalProfileCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<PortalProfile>) DataRepository.PortalProfileProvider.DeepLoad,
						new object[] { transactionManager, entity.PortalProfileCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region TaxRuleCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<TaxRule>|TaxRuleCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'TaxRuleCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.TaxRuleCollection = DataRepository.TaxRuleProvider.GetByPortalID(transactionManager, entity.PortalID);

				if (deep && entity.TaxRuleCollection.Count > 0)
				{
					deepHandles.Add("TaxRuleCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<TaxRule>) DataRepository.TaxRuleProvider.DeepLoad,
						new object[] { transactionManager, entity.TaxRuleCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region AttributeTypeCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<AttributeType>|AttributeTypeCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AttributeTypeCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.AttributeTypeCollection = DataRepository.AttributeTypeProvider.GetByPortalID(transactionManager, entity.PortalID);

				if (deep && entity.AttributeTypeCollection.Count > 0)
				{
					deepHandles.Add("AttributeTypeCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<AttributeType>) DataRepository.AttributeTypeProvider.DeepLoad,
						new object[] { transactionManager, entity.AttributeTypeCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region ProductTypeCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<ProductType>|ProductTypeCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ProductTypeCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ProductTypeCollection = DataRepository.ProductTypeProvider.GetByPortalID(transactionManager, entity.PortalID);

				if (deep && entity.ProductTypeCollection.Count > 0)
				{
					deepHandles.Add("ProductTypeCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<ProductType>) DataRepository.ProductTypeProvider.DeepLoad,
						new object[] { transactionManager, entity.ProductTypeCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region CaseRequestCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<CaseRequest>|CaseRequestCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CaseRequestCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CaseRequestCollection = DataRepository.CaseRequestProvider.GetByPortalID(transactionManager, entity.PortalID);

				if (deep && entity.CaseRequestCollection.Count > 0)
				{
					deepHandles.Add("CaseRequestCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<CaseRequest>) DataRepository.CaseRequestProvider.DeepLoad,
						new object[] { transactionManager, entity.CaseRequestCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region SupplierCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Supplier>|SupplierCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SupplierCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.SupplierCollection = DataRepository.SupplierProvider.GetByPortalID(transactionManager, entity.PortalID);

				if (deep && entity.SupplierCollection.Count > 0)
				{
					deepHandles.Add("SupplierCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Supplier>) DataRepository.SupplierProvider.DeepLoad,
						new object[] { transactionManager, entity.SupplierCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region CatalogCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Catalog>|CatalogCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CatalogCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CatalogCollection = DataRepository.CatalogProvider.GetByPortalID(transactionManager, entity.PortalID);

				if (deep && entity.CatalogCollection.Count > 0)
				{
					deepHandles.Add("CatalogCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Catalog>) DataRepository.CatalogProvider.DeepLoad,
						new object[] { transactionManager, entity.CatalogCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region AddOnCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<AddOn>|AddOnCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AddOnCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.AddOnCollection = DataRepository.AddOnProvider.GetByPortalID(transactionManager, entity.PortalID);

				if (deep && entity.AddOnCollection.Count > 0)
				{
					deepHandles.Add("AddOnCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<AddOn>) DataRepository.AddOnProvider.DeepLoad,
						new object[] { transactionManager, entity.AddOnCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region TaxClassCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<TaxClass>|TaxClassCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'TaxClassCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.TaxClassCollection = DataRepository.TaxClassProvider.GetByPortalID(transactionManager, entity.PortalID);

				if (deep && entity.TaxClassCollection.Count > 0)
				{
					deepHandles.Add("TaxClassCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<TaxClass>) DataRepository.TaxClassProvider.DeepLoad,
						new object[] { transactionManager, entity.TaxClassCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region TaxRuleTypeCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<TaxRuleType>|TaxRuleTypeCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'TaxRuleTypeCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.TaxRuleTypeCollection = DataRepository.TaxRuleTypeProvider.GetByPortalID(transactionManager, entity.PortalID);

				if (deep && entity.TaxRuleTypeCollection.Count > 0)
				{
					deepHandles.Add("TaxRuleTypeCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<TaxRuleType>) DataRepository.TaxRuleTypeProvider.DeepLoad,
						new object[] { transactionManager, entity.TaxRuleTypeCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region CategoryCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Category>|CategoryCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CategoryCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CategoryCollection = DataRepository.CategoryProvider.GetByPortalID(transactionManager, entity.PortalID);

				if (deep && entity.CategoryCollection.Count > 0)
				{
					deepHandles.Add("CategoryCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Category>) DataRepository.CategoryProvider.DeepLoad,
						new object[] { transactionManager, entity.CategoryCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region OrderCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Order>|OrderCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'OrderCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.OrderCollection = DataRepository.OrderProvider.GetByPortalId(transactionManager, entity.PortalID);

				if (deep && entity.OrderCollection.Count > 0)
				{
					deepHandles.Add("OrderCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Order>) DataRepository.OrderProvider.DeepLoad,
						new object[] { transactionManager, entity.OrderCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region ProductCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Product>|ProductCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ProductCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ProductCollection = DataRepository.ProductProvider.GetByPortalID(transactionManager, entity.PortalID);

				if (deep && entity.ProductCollection.Count > 0)
				{
					deepHandles.Add("ProductCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Product>) DataRepository.ProductProvider.DeepLoad,
						new object[] { transactionManager, entity.ProductCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region ManufacturerCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Manufacturer>|ManufacturerCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ManufacturerCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ManufacturerCollection = DataRepository.ManufacturerProvider.GetByPortalID(transactionManager, entity.PortalID);

				if (deep && entity.ManufacturerCollection.Count > 0)
				{
					deepHandles.Add("ManufacturerCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Manufacturer>) DataRepository.ManufacturerProvider.DeepLoad,
						new object[] { transactionManager, entity.ManufacturerCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region PromotionCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Promotion>|PromotionCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PromotionCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.PromotionCollection = DataRepository.PromotionProvider.GetByPortalID(transactionManager, entity.PortalID);

				if (deep && entity.PromotionCollection.Count > 0)
				{
					deepHandles.Add("PromotionCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Promotion>) DataRepository.PromotionProvider.DeepLoad,
						new object[] { transactionManager, entity.PromotionCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region HighlightCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Highlight>|HighlightCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'HighlightCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.HighlightCollection = DataRepository.HighlightProvider.GetByPortalID(transactionManager, entity.PortalID);

				if (deep && entity.HighlightCollection.Count > 0)
				{
					deepHandles.Add("HighlightCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Highlight>) DataRepository.HighlightProvider.DeepLoad,
						new object[] { transactionManager, entity.HighlightCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region TrackingCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Tracking>|TrackingCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'TrackingCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.TrackingCollection = DataRepository.TrackingProvider.GetByPortalID(transactionManager, entity.PortalID);

				if (deep && entity.TrackingCollection.Count > 0)
				{
					deepHandles.Add("TrackingCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Tracking>) DataRepository.TrackingProvider.DeepLoad,
						new object[] { transactionManager, entity.TrackingCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region DomainCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Domain>|DomainCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'DomainCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.DomainCollection = DataRepository.DomainProvider.GetByPortalID(transactionManager, entity.PortalID);

				if (deep && entity.DomainCollection.Count > 0)
				{
					deepHandles.Add("DomainCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Domain>) DataRepository.DomainProvider.DeepLoad,
						new object[] { transactionManager, entity.DomainCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region PortalCatalogCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<PortalCatalog>|PortalCatalogCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PortalCatalogCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.PortalCatalogCollection = DataRepository.PortalCatalogProvider.GetByPortalID(transactionManager, entity.PortalID);

				if (deep && entity.PortalCatalogCollection.Count > 0)
				{
					deepHandles.Add("PortalCatalogCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<PortalCatalog>) DataRepository.PortalCatalogProvider.DeepLoad,
						new object[] { transactionManager, entity.PortalCatalogCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region GiftCardCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<GiftCard>|GiftCardCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'GiftCardCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.GiftCardCollection = DataRepository.GiftCardProvider.GetByPortalId(transactionManager, entity.PortalID);

				if (deep && entity.GiftCardCollection.Count > 0)
				{
					deepHandles.Add("GiftCardCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<GiftCard>) DataRepository.GiftCardProvider.DeepLoad,
						new object[] { transactionManager, entity.GiftCardCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.Portal object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.Portal instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.Portal Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.Portal entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region CurrencyTypeIDSource
			if (CanDeepSave(entity, "CurrencyType|CurrencyTypeIDSource", deepSaveType, innerList) 
				&& entity.CurrencyTypeIDSource != null)
			{
				DataRepository.CurrencyTypeProvider.Save(transactionManager, entity.CurrencyTypeIDSource);
				entity.CurrencyTypeID = entity.CurrencyTypeIDSource.CurrencyTypeID;
			}
			#endregion 
			
			#region LocaleIDSource
			if (CanDeepSave(entity, "Locale|LocaleIDSource", deepSaveType, innerList) 
				&& entity.LocaleIDSource != null)
			{
				DataRepository.LocaleProvider.Save(transactionManager, entity.LocaleIDSource);
				entity.LocaleID = entity.LocaleIDSource.LocaleID;
			}
			#endregion 
			
			#region DefaultOrderStateIDSource
			if (CanDeepSave(entity, "OrderState|DefaultOrderStateIDSource", deepSaveType, innerList) 
				&& entity.DefaultOrderStateIDSource != null)
			{
				DataRepository.OrderStateProvider.Save(transactionManager, entity.DefaultOrderStateIDSource);
				entity.DefaultOrderStateID = entity.DefaultOrderStateIDSource.OrderStateID;
			}
			#endregion 
			
			#region DefaultProductReviewStateIDSource
			if (CanDeepSave(entity, "ProductReviewState|DefaultProductReviewStateIDSource", deepSaveType, innerList) 
				&& entity.DefaultProductReviewStateIDSource != null)
			{
				DataRepository.ProductReviewStateProvider.Save(transactionManager, entity.DefaultProductReviewStateIDSource);
				entity.DefaultProductReviewStateID = entity.DefaultProductReviewStateIDSource.ReviewStateID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<PortalCountry>
				if (CanDeepSave(entity.PortalCountryCollection, "List<PortalCountry>|PortalCountryCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(PortalCountry child in entity.PortalCountryCollection)
					{
						if(child.PortalIDSource != null)
						{
							child.PortalID = child.PortalIDSource.PortalID;
						}
						else
						{
							child.PortalID = entity.PortalID;
						}

					}

					if (entity.PortalCountryCollection.Count > 0 || entity.PortalCountryCollection.DeletedItems.Count > 0)
					{
						//DataRepository.PortalCountryProvider.Save(transactionManager, entity.PortalCountryCollection);
						
						deepHandles.Add("PortalCountryCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< PortalCountry >) DataRepository.PortalCountryProvider.DeepSave,
							new object[] { transactionManager, entity.PortalCountryCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<CookieMapping>
				if (CanDeepSave(entity.CookieMappingCollection, "List<CookieMapping>|CookieMappingCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(CookieMapping child in entity.CookieMappingCollection)
					{
						if(child.PortalIDSource != null)
						{
							child.PortalID = child.PortalIDSource.PortalID;
						}
						else
						{
							child.PortalID = entity.PortalID;
						}

					}

					if (entity.CookieMappingCollection.Count > 0 || entity.CookieMappingCollection.DeletedItems.Count > 0)
					{
						//DataRepository.CookieMappingProvider.Save(transactionManager, entity.CookieMappingCollection);
						
						deepHandles.Add("CookieMappingCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< CookieMapping >) DataRepository.CookieMappingProvider.DeepSave,
							new object[] { transactionManager, entity.CookieMappingCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<ContentPage>
				if (CanDeepSave(entity.ContentPageCollection, "List<ContentPage>|ContentPageCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(ContentPage child in entity.ContentPageCollection)
					{
						if(child.PortalIDSource != null)
						{
							child.PortalID = child.PortalIDSource.PortalID;
						}
						else
						{
							child.PortalID = entity.PortalID;
						}

					}

					if (entity.ContentPageCollection.Count > 0 || entity.ContentPageCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ContentPageProvider.Save(transactionManager, entity.ContentPageCollection);
						
						deepHandles.Add("ContentPageCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< ContentPage >) DataRepository.ContentPageProvider.DeepSave,
							new object[] { transactionManager, entity.ContentPageCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<PortalProfile>
				if (CanDeepSave(entity.PortalProfileCollection, "List<PortalProfile>|PortalProfileCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(PortalProfile child in entity.PortalProfileCollection)
					{
						if(child.PortalIDSource != null)
						{
							child.PortalID = child.PortalIDSource.PortalID;
						}
						else
						{
							child.PortalID = entity.PortalID;
						}

					}

					if (entity.PortalProfileCollection.Count > 0 || entity.PortalProfileCollection.DeletedItems.Count > 0)
					{
						//DataRepository.PortalProfileProvider.Save(transactionManager, entity.PortalProfileCollection);
						
						deepHandles.Add("PortalProfileCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< PortalProfile >) DataRepository.PortalProfileProvider.DeepSave,
							new object[] { transactionManager, entity.PortalProfileCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<TaxRule>
				if (CanDeepSave(entity.TaxRuleCollection, "List<TaxRule>|TaxRuleCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(TaxRule child in entity.TaxRuleCollection)
					{
						if(child.PortalIDSource != null)
						{
							child.PortalID = child.PortalIDSource.PortalID;
						}
						else
						{
							child.PortalID = entity.PortalID;
						}

					}

					if (entity.TaxRuleCollection.Count > 0 || entity.TaxRuleCollection.DeletedItems.Count > 0)
					{
						//DataRepository.TaxRuleProvider.Save(transactionManager, entity.TaxRuleCollection);
						
						deepHandles.Add("TaxRuleCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< TaxRule >) DataRepository.TaxRuleProvider.DeepSave,
							new object[] { transactionManager, entity.TaxRuleCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<AttributeType>
				if (CanDeepSave(entity.AttributeTypeCollection, "List<AttributeType>|AttributeTypeCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(AttributeType child in entity.AttributeTypeCollection)
					{
						if(child.PortalIDSource != null)
						{
							child.PortalID = child.PortalIDSource.PortalID;
						}
						else
						{
							child.PortalID = entity.PortalID;
						}

					}

					if (entity.AttributeTypeCollection.Count > 0 || entity.AttributeTypeCollection.DeletedItems.Count > 0)
					{
						//DataRepository.AttributeTypeProvider.Save(transactionManager, entity.AttributeTypeCollection);
						
						deepHandles.Add("AttributeTypeCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< AttributeType >) DataRepository.AttributeTypeProvider.DeepSave,
							new object[] { transactionManager, entity.AttributeTypeCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<ProductType>
				if (CanDeepSave(entity.ProductTypeCollection, "List<ProductType>|ProductTypeCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(ProductType child in entity.ProductTypeCollection)
					{
						if(child.PortalIDSource != null)
						{
							child.PortalID = child.PortalIDSource.PortalID;
						}
						else
						{
							child.PortalID = entity.PortalID;
						}

					}

					if (entity.ProductTypeCollection.Count > 0 || entity.ProductTypeCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ProductTypeProvider.Save(transactionManager, entity.ProductTypeCollection);
						
						deepHandles.Add("ProductTypeCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< ProductType >) DataRepository.ProductTypeProvider.DeepSave,
							new object[] { transactionManager, entity.ProductTypeCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<CaseRequest>
				if (CanDeepSave(entity.CaseRequestCollection, "List<CaseRequest>|CaseRequestCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(CaseRequest child in entity.CaseRequestCollection)
					{
						if(child.PortalIDSource != null)
						{
							child.PortalID = child.PortalIDSource.PortalID;
						}
						else
						{
							child.PortalID = entity.PortalID;
						}

					}

					if (entity.CaseRequestCollection.Count > 0 || entity.CaseRequestCollection.DeletedItems.Count > 0)
					{
						//DataRepository.CaseRequestProvider.Save(transactionManager, entity.CaseRequestCollection);
						
						deepHandles.Add("CaseRequestCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< CaseRequest >) DataRepository.CaseRequestProvider.DeepSave,
							new object[] { transactionManager, entity.CaseRequestCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Supplier>
				if (CanDeepSave(entity.SupplierCollection, "List<Supplier>|SupplierCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Supplier child in entity.SupplierCollection)
					{
						if(child.PortalIDSource != null)
						{
							child.PortalID = child.PortalIDSource.PortalID;
						}
						else
						{
							child.PortalID = entity.PortalID;
						}

					}

					if (entity.SupplierCollection.Count > 0 || entity.SupplierCollection.DeletedItems.Count > 0)
					{
						//DataRepository.SupplierProvider.Save(transactionManager, entity.SupplierCollection);
						
						deepHandles.Add("SupplierCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Supplier >) DataRepository.SupplierProvider.DeepSave,
							new object[] { transactionManager, entity.SupplierCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Catalog>
				if (CanDeepSave(entity.CatalogCollection, "List<Catalog>|CatalogCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Catalog child in entity.CatalogCollection)
					{
						if(child.PortalIDSource != null)
						{
							child.PortalID = child.PortalIDSource.PortalID;
						}
						else
						{
							child.PortalID = entity.PortalID;
						}

					}

					if (entity.CatalogCollection.Count > 0 || entity.CatalogCollection.DeletedItems.Count > 0)
					{
						//DataRepository.CatalogProvider.Save(transactionManager, entity.CatalogCollection);
						
						deepHandles.Add("CatalogCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Catalog >) DataRepository.CatalogProvider.DeepSave,
							new object[] { transactionManager, entity.CatalogCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<AddOn>
				if (CanDeepSave(entity.AddOnCollection, "List<AddOn>|AddOnCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(AddOn child in entity.AddOnCollection)
					{
						if(child.PortalIDSource != null)
						{
							child.PortalID = child.PortalIDSource.PortalID;
						}
						else
						{
							child.PortalID = entity.PortalID;
						}

					}

					if (entity.AddOnCollection.Count > 0 || entity.AddOnCollection.DeletedItems.Count > 0)
					{
						//DataRepository.AddOnProvider.Save(transactionManager, entity.AddOnCollection);
						
						deepHandles.Add("AddOnCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< AddOn >) DataRepository.AddOnProvider.DeepSave,
							new object[] { transactionManager, entity.AddOnCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<TaxClass>
				if (CanDeepSave(entity.TaxClassCollection, "List<TaxClass>|TaxClassCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(TaxClass child in entity.TaxClassCollection)
					{
						if(child.PortalIDSource != null)
						{
							child.PortalID = child.PortalIDSource.PortalID;
						}
						else
						{
							child.PortalID = entity.PortalID;
						}

					}

					if (entity.TaxClassCollection.Count > 0 || entity.TaxClassCollection.DeletedItems.Count > 0)
					{
						//DataRepository.TaxClassProvider.Save(transactionManager, entity.TaxClassCollection);
						
						deepHandles.Add("TaxClassCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< TaxClass >) DataRepository.TaxClassProvider.DeepSave,
							new object[] { transactionManager, entity.TaxClassCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<TaxRuleType>
				if (CanDeepSave(entity.TaxRuleTypeCollection, "List<TaxRuleType>|TaxRuleTypeCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(TaxRuleType child in entity.TaxRuleTypeCollection)
					{
						if(child.PortalIDSource != null)
						{
							child.PortalID = child.PortalIDSource.PortalID;
						}
						else
						{
							child.PortalID = entity.PortalID;
						}

					}

					if (entity.TaxRuleTypeCollection.Count > 0 || entity.TaxRuleTypeCollection.DeletedItems.Count > 0)
					{
						//DataRepository.TaxRuleTypeProvider.Save(transactionManager, entity.TaxRuleTypeCollection);
						
						deepHandles.Add("TaxRuleTypeCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< TaxRuleType >) DataRepository.TaxRuleTypeProvider.DeepSave,
							new object[] { transactionManager, entity.TaxRuleTypeCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Category>
				if (CanDeepSave(entity.CategoryCollection, "List<Category>|CategoryCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Category child in entity.CategoryCollection)
					{
						if(child.PortalIDSource != null)
						{
							child.PortalID = child.PortalIDSource.PortalID;
						}
						else
						{
							child.PortalID = entity.PortalID;
						}

					}

					if (entity.CategoryCollection.Count > 0 || entity.CategoryCollection.DeletedItems.Count > 0)
					{
						//DataRepository.CategoryProvider.Save(transactionManager, entity.CategoryCollection);
						
						deepHandles.Add("CategoryCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Category >) DataRepository.CategoryProvider.DeepSave,
							new object[] { transactionManager, entity.CategoryCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Order>
				if (CanDeepSave(entity.OrderCollection, "List<Order>|OrderCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Order child in entity.OrderCollection)
					{
						if(child.PortalIdSource != null)
						{
							child.PortalId = child.PortalIdSource.PortalID;
						}
						else
						{
							child.PortalId = entity.PortalID;
						}

					}

					if (entity.OrderCollection.Count > 0 || entity.OrderCollection.DeletedItems.Count > 0)
					{
						//DataRepository.OrderProvider.Save(transactionManager, entity.OrderCollection);
						
						deepHandles.Add("OrderCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Order >) DataRepository.OrderProvider.DeepSave,
							new object[] { transactionManager, entity.OrderCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Product>
				if (CanDeepSave(entity.ProductCollection, "List<Product>|ProductCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Product child in entity.ProductCollection)
					{
						if(child.PortalIDSource != null)
						{
							child.PortalID = child.PortalIDSource.PortalID;
						}
						else
						{
							child.PortalID = entity.PortalID;
						}

					}

					if (entity.ProductCollection.Count > 0 || entity.ProductCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ProductProvider.Save(transactionManager, entity.ProductCollection);
						
						deepHandles.Add("ProductCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Product >) DataRepository.ProductProvider.DeepSave,
							new object[] { transactionManager, entity.ProductCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Manufacturer>
				if (CanDeepSave(entity.ManufacturerCollection, "List<Manufacturer>|ManufacturerCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Manufacturer child in entity.ManufacturerCollection)
					{
						if(child.PortalIDSource != null)
						{
							child.PortalID = child.PortalIDSource.PortalID;
						}
						else
						{
							child.PortalID = entity.PortalID;
						}

					}

					if (entity.ManufacturerCollection.Count > 0 || entity.ManufacturerCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ManufacturerProvider.Save(transactionManager, entity.ManufacturerCollection);
						
						deepHandles.Add("ManufacturerCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Manufacturer >) DataRepository.ManufacturerProvider.DeepSave,
							new object[] { transactionManager, entity.ManufacturerCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Promotion>
				if (CanDeepSave(entity.PromotionCollection, "List<Promotion>|PromotionCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Promotion child in entity.PromotionCollection)
					{
						if(child.PortalIDSource != null)
						{
							child.PortalID = child.PortalIDSource.PortalID;
						}
						else
						{
							child.PortalID = entity.PortalID;
						}

					}

					if (entity.PromotionCollection.Count > 0 || entity.PromotionCollection.DeletedItems.Count > 0)
					{
						//DataRepository.PromotionProvider.Save(transactionManager, entity.PromotionCollection);
						
						deepHandles.Add("PromotionCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Promotion >) DataRepository.PromotionProvider.DeepSave,
							new object[] { transactionManager, entity.PromotionCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Highlight>
				if (CanDeepSave(entity.HighlightCollection, "List<Highlight>|HighlightCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Highlight child in entity.HighlightCollection)
					{
						if(child.PortalIDSource != null)
						{
							child.PortalID = child.PortalIDSource.PortalID;
						}
						else
						{
							child.PortalID = entity.PortalID;
						}

					}

					if (entity.HighlightCollection.Count > 0 || entity.HighlightCollection.DeletedItems.Count > 0)
					{
						//DataRepository.HighlightProvider.Save(transactionManager, entity.HighlightCollection);
						
						deepHandles.Add("HighlightCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Highlight >) DataRepository.HighlightProvider.DeepSave,
							new object[] { transactionManager, entity.HighlightCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Tracking>
				if (CanDeepSave(entity.TrackingCollection, "List<Tracking>|TrackingCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Tracking child in entity.TrackingCollection)
					{
						if(child.PortalIDSource != null)
						{
							child.PortalID = child.PortalIDSource.PortalID;
						}
						else
						{
							child.PortalID = entity.PortalID;
						}

					}

					if (entity.TrackingCollection.Count > 0 || entity.TrackingCollection.DeletedItems.Count > 0)
					{
						//DataRepository.TrackingProvider.Save(transactionManager, entity.TrackingCollection);
						
						deepHandles.Add("TrackingCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Tracking >) DataRepository.TrackingProvider.DeepSave,
							new object[] { transactionManager, entity.TrackingCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Domain>
				if (CanDeepSave(entity.DomainCollection, "List<Domain>|DomainCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Domain child in entity.DomainCollection)
					{
						if(child.PortalIDSource != null)
						{
							child.PortalID = child.PortalIDSource.PortalID;
						}
						else
						{
							child.PortalID = entity.PortalID;
						}

					}

					if (entity.DomainCollection.Count > 0 || entity.DomainCollection.DeletedItems.Count > 0)
					{
						//DataRepository.DomainProvider.Save(transactionManager, entity.DomainCollection);
						
						deepHandles.Add("DomainCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Domain >) DataRepository.DomainProvider.DeepSave,
							new object[] { transactionManager, entity.DomainCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<PortalCatalog>
				if (CanDeepSave(entity.PortalCatalogCollection, "List<PortalCatalog>|PortalCatalogCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(PortalCatalog child in entity.PortalCatalogCollection)
					{
						if(child.PortalIDSource != null)
						{
							child.PortalID = child.PortalIDSource.PortalID;
						}
						else
						{
							child.PortalID = entity.PortalID;
						}

					}

					if (entity.PortalCatalogCollection.Count > 0 || entity.PortalCatalogCollection.DeletedItems.Count > 0)
					{
						//DataRepository.PortalCatalogProvider.Save(transactionManager, entity.PortalCatalogCollection);
						
						deepHandles.Add("PortalCatalogCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< PortalCatalog >) DataRepository.PortalCatalogProvider.DeepSave,
							new object[] { transactionManager, entity.PortalCatalogCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<GiftCard>
				if (CanDeepSave(entity.GiftCardCollection, "List<GiftCard>|GiftCardCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(GiftCard child in entity.GiftCardCollection)
					{
						if(child.PortalIdSource != null)
						{
							child.PortalId = child.PortalIdSource.PortalID;
						}
						else
						{
							child.PortalId = entity.PortalID;
						}

					}

					if (entity.GiftCardCollection.Count > 0 || entity.GiftCardCollection.DeletedItems.Count > 0)
					{
						//DataRepository.GiftCardProvider.Save(transactionManager, entity.GiftCardCollection);
						
						deepHandles.Add("GiftCardCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< GiftCard >) DataRepository.GiftCardProvider.DeepSave,
							new object[] { transactionManager, entity.GiftCardCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region PortalChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.Portal</c>
	///</summary>
	public enum PortalChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>CurrencyType</c> at CurrencyTypeIDSource
		///</summary>
		[ChildEntityType(typeof(CurrencyType))]
		CurrencyType,
		
		///<summary>
		/// Composite Property for <c>Locale</c> at LocaleIDSource
		///</summary>
		[ChildEntityType(typeof(Locale))]
		Locale,
		
		///<summary>
		/// Composite Property for <c>OrderState</c> at DefaultOrderStateIDSource
		///</summary>
		[ChildEntityType(typeof(OrderState))]
		OrderState,
		
		///<summary>
		/// Composite Property for <c>ProductReviewState</c> at DefaultProductReviewStateIDSource
		///</summary>
		[ChildEntityType(typeof(ProductReviewState))]
		ProductReviewState,
		///<summary>
		/// Collection of <c>Portal</c> as OneToMany for PortalCountryCollection
		///</summary>
		[ChildEntityType(typeof(TList<PortalCountry>))]
		PortalCountryCollection,
		///<summary>
		/// Collection of <c>Portal</c> as OneToMany for CookieMappingCollection
		///</summary>
		[ChildEntityType(typeof(TList<CookieMapping>))]
		CookieMappingCollection,
		///<summary>
		/// Collection of <c>Portal</c> as OneToMany for ContentPageCollection
		///</summary>
		[ChildEntityType(typeof(TList<ContentPage>))]
		ContentPageCollection,
		///<summary>
		/// Collection of <c>Portal</c> as OneToMany for PortalProfileCollection
		///</summary>
		[ChildEntityType(typeof(TList<PortalProfile>))]
		PortalProfileCollection,
		///<summary>
		/// Collection of <c>Portal</c> as OneToMany for TaxRuleCollection
		///</summary>
		[ChildEntityType(typeof(TList<TaxRule>))]
		TaxRuleCollection,
		///<summary>
		/// Collection of <c>Portal</c> as OneToMany for AttributeTypeCollection
		///</summary>
		[ChildEntityType(typeof(TList<AttributeType>))]
		AttributeTypeCollection,
		///<summary>
		/// Collection of <c>Portal</c> as OneToMany for ProductTypeCollection
		///</summary>
		[ChildEntityType(typeof(TList<ProductType>))]
		ProductTypeCollection,
		///<summary>
		/// Collection of <c>Portal</c> as OneToMany for CaseRequestCollection
		///</summary>
		[ChildEntityType(typeof(TList<CaseRequest>))]
		CaseRequestCollection,
		///<summary>
		/// Collection of <c>Portal</c> as OneToMany for SupplierCollection
		///</summary>
		[ChildEntityType(typeof(TList<Supplier>))]
		SupplierCollection,
		///<summary>
		/// Collection of <c>Portal</c> as OneToMany for CatalogCollection
		///</summary>
		[ChildEntityType(typeof(TList<Catalog>))]
		CatalogCollection,
		///<summary>
		/// Collection of <c>Portal</c> as OneToMany for AddOnCollection
		///</summary>
		[ChildEntityType(typeof(TList<AddOn>))]
		AddOnCollection,
		///<summary>
		/// Collection of <c>Portal</c> as OneToMany for TaxClassCollection
		///</summary>
		[ChildEntityType(typeof(TList<TaxClass>))]
		TaxClassCollection,
		///<summary>
		/// Collection of <c>Portal</c> as OneToMany for TaxRuleTypeCollection
		///</summary>
		[ChildEntityType(typeof(TList<TaxRuleType>))]
		TaxRuleTypeCollection,
		///<summary>
		/// Collection of <c>Portal</c> as OneToMany for CategoryCollection
		///</summary>
		[ChildEntityType(typeof(TList<Category>))]
		CategoryCollection,
		///<summary>
		/// Collection of <c>Portal</c> as OneToMany for OrderCollection
		///</summary>
		[ChildEntityType(typeof(TList<Order>))]
		OrderCollection,
		///<summary>
		/// Collection of <c>Portal</c> as OneToMany for ProductCollection
		///</summary>
		[ChildEntityType(typeof(TList<Product>))]
		ProductCollection,
		///<summary>
		/// Collection of <c>Portal</c> as OneToMany for ManufacturerCollection
		///</summary>
		[ChildEntityType(typeof(TList<Manufacturer>))]
		ManufacturerCollection,
		///<summary>
		/// Collection of <c>Portal</c> as OneToMany for PromotionCollection
		///</summary>
		[ChildEntityType(typeof(TList<Promotion>))]
		PromotionCollection,
		///<summary>
		/// Collection of <c>Portal</c> as OneToMany for HighlightCollection
		///</summary>
		[ChildEntityType(typeof(TList<Highlight>))]
		HighlightCollection,
		///<summary>
		/// Collection of <c>Portal</c> as OneToMany for TrackingCollection
		///</summary>
		[ChildEntityType(typeof(TList<Tracking>))]
		TrackingCollection,
		///<summary>
		/// Collection of <c>Portal</c> as OneToMany for DomainCollection
		///</summary>
		[ChildEntityType(typeof(TList<Domain>))]
		DomainCollection,
		///<summary>
		/// Collection of <c>Portal</c> as OneToMany for PortalCatalogCollection
		///</summary>
		[ChildEntityType(typeof(TList<PortalCatalog>))]
		PortalCatalogCollection,
		///<summary>
		/// Collection of <c>Portal</c> as OneToMany for GiftCardCollection
		///</summary>
		[ChildEntityType(typeof(TList<GiftCard>))]
		GiftCardCollection,
	}
	
	#endregion PortalChildEntityTypes
	
	#region PortalFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;PortalColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Portal"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PortalFilterBuilder : SqlFilterBuilder<PortalColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PortalFilterBuilder class.
		/// </summary>
		public PortalFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the PortalFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PortalFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PortalFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PortalFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PortalFilterBuilder
	
	#region PortalParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;PortalColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Portal"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PortalParameterBuilder : ParameterizedSqlFilterBuilder<PortalColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PortalParameterBuilder class.
		/// </summary>
		public PortalParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the PortalParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PortalParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PortalParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PortalParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PortalParameterBuilder
	
	#region PortalSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;PortalColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Portal"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class PortalSortBuilder : SqlSortBuilder<PortalColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PortalSqlSortBuilder class.
		/// </summary>
		public PortalSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion PortalSortBuilder
	
} // end namespace
