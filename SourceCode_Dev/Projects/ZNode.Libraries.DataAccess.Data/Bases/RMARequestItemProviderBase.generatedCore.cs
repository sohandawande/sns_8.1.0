﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="RMARequestItemProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class RMARequestItemProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.RMARequestItem, ZNode.Libraries.DataAccess.Entities.RMARequestItemKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.RMARequestItemKey key)
		{
			return Delete(transactionManager, key.RMARequestItemID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_rMARequestItemID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _rMARequestItemID)
		{
			return Delete(null, _rMARequestItemID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_rMARequestItemID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _rMARequestItemID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeRMAR__Reaso__5D21AF45 key.
		///		FK__ZNodeRMAR__Reaso__5D21AF45 Description: 
		/// </summary>
		/// <param name="_reasonForReturnID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.RMARequestItem objects.</returns>
		public TList<RMARequestItem> GetByReasonForReturnID(System.Int32? _reasonForReturnID)
		{
			int count = -1;
			return GetByReasonForReturnID(_reasonForReturnID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeRMAR__Reaso__5D21AF45 key.
		///		FK__ZNodeRMAR__Reaso__5D21AF45 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_reasonForReturnID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.RMARequestItem objects.</returns>
		/// <remarks></remarks>
		public TList<RMARequestItem> GetByReasonForReturnID(TransactionManager transactionManager, System.Int32? _reasonForReturnID)
		{
			int count = -1;
			return GetByReasonForReturnID(transactionManager, _reasonForReturnID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeRMAR__Reaso__5D21AF45 key.
		///		FK__ZNodeRMAR__Reaso__5D21AF45 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_reasonForReturnID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.RMARequestItem objects.</returns>
		public TList<RMARequestItem> GetByReasonForReturnID(TransactionManager transactionManager, System.Int32? _reasonForReturnID, int start, int pageLength)
		{
			int count = -1;
			return GetByReasonForReturnID(transactionManager, _reasonForReturnID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeRMAR__Reaso__5D21AF45 key.
		///		fKZNodeRMARReaso5D21AF45 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_reasonForReturnID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.RMARequestItem objects.</returns>
		public TList<RMARequestItem> GetByReasonForReturnID(System.Int32? _reasonForReturnID, int start, int pageLength)
		{
			int count =  -1;
			return GetByReasonForReturnID(null, _reasonForReturnID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeRMAR__Reaso__5D21AF45 key.
		///		fKZNodeRMARReaso5D21AF45 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_reasonForReturnID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.RMARequestItem objects.</returns>
		public TList<RMARequestItem> GetByReasonForReturnID(System.Int32? _reasonForReturnID, int start, int pageLength,out int count)
		{
			return GetByReasonForReturnID(null, _reasonForReturnID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeRMAR__Reaso__5D21AF45 key.
		///		FK__ZNodeRMAR__Reaso__5D21AF45 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_reasonForReturnID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.RMARequestItem objects.</returns>
		public abstract TList<RMARequestItem> GetByReasonForReturnID(TransactionManager transactionManager, System.Int32? _reasonForReturnID, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeRMARequestItem_ZNodeOrderLineItem key.
		///		FK_ZNodeRMARequestItem_ZNodeOrderLineItem Description: 
		/// </summary>
		/// <param name="_orderLineItemID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.RMARequestItem objects.</returns>
		public TList<RMARequestItem> GetByOrderLineItemID(System.Int32? _orderLineItemID)
		{
			int count = -1;
			return GetByOrderLineItemID(_orderLineItemID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeRMARequestItem_ZNodeOrderLineItem key.
		///		FK_ZNodeRMARequestItem_ZNodeOrderLineItem Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_orderLineItemID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.RMARequestItem objects.</returns>
		/// <remarks></remarks>
		public TList<RMARequestItem> GetByOrderLineItemID(TransactionManager transactionManager, System.Int32? _orderLineItemID)
		{
			int count = -1;
			return GetByOrderLineItemID(transactionManager, _orderLineItemID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeRMARequestItem_ZNodeOrderLineItem key.
		///		FK_ZNodeRMARequestItem_ZNodeOrderLineItem Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_orderLineItemID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.RMARequestItem objects.</returns>
		public TList<RMARequestItem> GetByOrderLineItemID(TransactionManager transactionManager, System.Int32? _orderLineItemID, int start, int pageLength)
		{
			int count = -1;
			return GetByOrderLineItemID(transactionManager, _orderLineItemID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeRMARequestItem_ZNodeOrderLineItem key.
		///		fKZNodeRMARequestItemZNodeOrderLineItem Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_orderLineItemID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.RMARequestItem objects.</returns>
		public TList<RMARequestItem> GetByOrderLineItemID(System.Int32? _orderLineItemID, int start, int pageLength)
		{
			int count =  -1;
			return GetByOrderLineItemID(null, _orderLineItemID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeRMARequestItem_ZNodeOrderLineItem key.
		///		fKZNodeRMARequestItemZNodeOrderLineItem Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_orderLineItemID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.RMARequestItem objects.</returns>
		public TList<RMARequestItem> GetByOrderLineItemID(System.Int32? _orderLineItemID, int start, int pageLength,out int count)
		{
			return GetByOrderLineItemID(null, _orderLineItemID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeRMARequestItem_ZNodeOrderLineItem key.
		///		FK_ZNodeRMARequestItem_ZNodeOrderLineItem Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_orderLineItemID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.RMARequestItem objects.</returns>
		public abstract TList<RMARequestItem> GetByOrderLineItemID(TransactionManager transactionManager, System.Int32? _orderLineItemID, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.RMARequestItem Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.RMARequestItemKey key, int start, int pageLength)
		{
			return GetByRMARequestItemID(transactionManager, key.RMARequestItemID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodeRMARequestItem index.
		/// </summary>
		/// <param name="_rMARequestItemID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.RMARequestItem"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.RMARequestItem GetByRMARequestItemID(System.Int32 _rMARequestItemID)
		{
			int count = -1;
			return GetByRMARequestItemID(null,_rMARequestItemID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeRMARequestItem index.
		/// </summary>
		/// <param name="_rMARequestItemID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.RMARequestItem"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.RMARequestItem GetByRMARequestItemID(System.Int32 _rMARequestItemID, int start, int pageLength)
		{
			int count = -1;
			return GetByRMARequestItemID(null, _rMARequestItemID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeRMARequestItem index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_rMARequestItemID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.RMARequestItem"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.RMARequestItem GetByRMARequestItemID(TransactionManager transactionManager, System.Int32 _rMARequestItemID)
		{
			int count = -1;
			return GetByRMARequestItemID(transactionManager, _rMARequestItemID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeRMARequestItem index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_rMARequestItemID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.RMARequestItem"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.RMARequestItem GetByRMARequestItemID(TransactionManager transactionManager, System.Int32 _rMARequestItemID, int start, int pageLength)
		{
			int count = -1;
			return GetByRMARequestItemID(transactionManager, _rMARequestItemID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeRMARequestItem index.
		/// </summary>
		/// <param name="_rMARequestItemID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.RMARequestItem"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.RMARequestItem GetByRMARequestItemID(System.Int32 _rMARequestItemID, int start, int pageLength, out int count)
		{
			return GetByRMARequestItemID(null, _rMARequestItemID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeRMARequestItem index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_rMARequestItemID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.RMARequestItem"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.RMARequestItem GetByRMARequestItemID(TransactionManager transactionManager, System.Int32 _rMARequestItemID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;RMARequestItem&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;RMARequestItem&gt;"/></returns>
		public static TList<RMARequestItem> Fill(IDataReader reader, TList<RMARequestItem> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.RMARequestItem c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("RMARequestItem")
					.Append("|").Append((System.Int32)reader[((int)RMARequestItemColumn.RMARequestItemID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<RMARequestItem>(
					key.ToString(), // EntityTrackingKey
					"RMARequestItem",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.RMARequestItem();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.RMARequestItemID = (System.Int32)reader[((int)RMARequestItemColumn.RMARequestItemID - 1)];
					c.RMARequestID = (reader.IsDBNull(((int)RMARequestItemColumn.RMARequestID - 1)))?null:(System.Int32?)reader[((int)RMARequestItemColumn.RMARequestID - 1)];
					c.OrderLineItemID = (reader.IsDBNull(((int)RMARequestItemColumn.OrderLineItemID - 1)))?null:(System.Int32?)reader[((int)RMARequestItemColumn.OrderLineItemID - 1)];
					c.IsReceived = (reader.IsDBNull(((int)RMARequestItemColumn.IsReceived - 1)))?null:(System.Boolean?)reader[((int)RMARequestItemColumn.IsReceived - 1)];
					c.IsReturnable = (reader.IsDBNull(((int)RMARequestItemColumn.IsReturnable - 1)))?null:(System.Boolean?)reader[((int)RMARequestItemColumn.IsReturnable - 1)];
					c.GiftCardId = (reader.IsDBNull(((int)RMARequestItemColumn.GiftCardId - 1)))?null:(System.Int32?)reader[((int)RMARequestItemColumn.GiftCardId - 1)];
					c.Quantity = (reader.IsDBNull(((int)RMARequestItemColumn.Quantity - 1)))?null:(System.Int32?)reader[((int)RMARequestItemColumn.Quantity - 1)];
					c.Price = (reader.IsDBNull(((int)RMARequestItemColumn.Price - 1)))?null:(System.Decimal?)reader[((int)RMARequestItemColumn.Price - 1)];
					c.ReasonForReturnID = (reader.IsDBNull(((int)RMARequestItemColumn.ReasonForReturnID - 1)))?null:(System.Int32?)reader[((int)RMARequestItemColumn.ReasonForReturnID - 1)];
					c.TansactionId = (reader.IsDBNull(((int)RMARequestItemColumn.TansactionId - 1)))?null:(System.String)reader[((int)RMARequestItemColumn.TansactionId - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.RMARequestItem"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.RMARequestItem"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.RMARequestItem entity)
		{
			if (!reader.Read()) return;
			
			entity.RMARequestItemID = (System.Int32)reader[((int)RMARequestItemColumn.RMARequestItemID - 1)];
			entity.RMARequestID = (reader.IsDBNull(((int)RMARequestItemColumn.RMARequestID - 1)))?null:(System.Int32?)reader[((int)RMARequestItemColumn.RMARequestID - 1)];
			entity.OrderLineItemID = (reader.IsDBNull(((int)RMARequestItemColumn.OrderLineItemID - 1)))?null:(System.Int32?)reader[((int)RMARequestItemColumn.OrderLineItemID - 1)];
			entity.IsReceived = (reader.IsDBNull(((int)RMARequestItemColumn.IsReceived - 1)))?null:(System.Boolean?)reader[((int)RMARequestItemColumn.IsReceived - 1)];
			entity.IsReturnable = (reader.IsDBNull(((int)RMARequestItemColumn.IsReturnable - 1)))?null:(System.Boolean?)reader[((int)RMARequestItemColumn.IsReturnable - 1)];
			entity.GiftCardId = (reader.IsDBNull(((int)RMARequestItemColumn.GiftCardId - 1)))?null:(System.Int32?)reader[((int)RMARequestItemColumn.GiftCardId - 1)];
			entity.Quantity = (reader.IsDBNull(((int)RMARequestItemColumn.Quantity - 1)))?null:(System.Int32?)reader[((int)RMARequestItemColumn.Quantity - 1)];
			entity.Price = (reader.IsDBNull(((int)RMARequestItemColumn.Price - 1)))?null:(System.Decimal?)reader[((int)RMARequestItemColumn.Price - 1)];
			entity.ReasonForReturnID = (reader.IsDBNull(((int)RMARequestItemColumn.ReasonForReturnID - 1)))?null:(System.Int32?)reader[((int)RMARequestItemColumn.ReasonForReturnID - 1)];
			entity.TansactionId = (reader.IsDBNull(((int)RMARequestItemColumn.TansactionId - 1)))?null:(System.String)reader[((int)RMARequestItemColumn.TansactionId - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.RMARequestItem"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.RMARequestItem"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.RMARequestItem entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.RMARequestItemID = (System.Int32)dataRow["RMARequestItemID"];
			entity.RMARequestID = Convert.IsDBNull(dataRow["RMARequestID"]) ? null : (System.Int32?)dataRow["RMARequestID"];
			entity.OrderLineItemID = Convert.IsDBNull(dataRow["OrderLineItemID"]) ? null : (System.Int32?)dataRow["OrderLineItemID"];
			entity.IsReceived = Convert.IsDBNull(dataRow["IsReceived"]) ? null : (System.Boolean?)dataRow["IsReceived"];
			entity.IsReturnable = Convert.IsDBNull(dataRow["IsReturnable"]) ? null : (System.Boolean?)dataRow["IsReturnable"];
			entity.GiftCardId = Convert.IsDBNull(dataRow["GiftCardId"]) ? null : (System.Int32?)dataRow["GiftCardId"];
			entity.Quantity = Convert.IsDBNull(dataRow["Quantity"]) ? null : (System.Int32?)dataRow["Quantity"];
			entity.Price = Convert.IsDBNull(dataRow["Price"]) ? null : (System.Decimal?)dataRow["Price"];
			entity.ReasonForReturnID = Convert.IsDBNull(dataRow["ReasonForReturnID"]) ? null : (System.Int32?)dataRow["ReasonForReturnID"];
			entity.TansactionId = Convert.IsDBNull(dataRow["TansactionId"]) ? null : (System.String)dataRow["TansactionId"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.RMARequestItem"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.RMARequestItem Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.RMARequestItem entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region ReasonForReturnIDSource	
			if (CanDeepLoad(entity, "ReasonForReturn|ReasonForReturnIDSource", deepLoadType, innerList) 
				&& entity.ReasonForReturnIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.ReasonForReturnID ?? (int)0);
				ReasonForReturn tmpEntity = EntityManager.LocateEntity<ReasonForReturn>(EntityLocator.ConstructKeyFromPkItems(typeof(ReasonForReturn), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ReasonForReturnIDSource = tmpEntity;
				else
					entity.ReasonForReturnIDSource = DataRepository.ReasonForReturnProvider.GetByReasonForReturnID(transactionManager, (entity.ReasonForReturnID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ReasonForReturnIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ReasonForReturnIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.ReasonForReturnProvider.DeepLoad(transactionManager, entity.ReasonForReturnIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ReasonForReturnIDSource

			#region OrderLineItemIDSource	
			if (CanDeepLoad(entity, "OrderLineItem|OrderLineItemIDSource", deepLoadType, innerList) 
				&& entity.OrderLineItemIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.OrderLineItemID ?? (int)0);
				OrderLineItem tmpEntity = EntityManager.LocateEntity<OrderLineItem>(EntityLocator.ConstructKeyFromPkItems(typeof(OrderLineItem), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.OrderLineItemIDSource = tmpEntity;
				else
					entity.OrderLineItemIDSource = DataRepository.OrderLineItemProvider.GetByOrderLineItemID(transactionManager, (entity.OrderLineItemID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'OrderLineItemIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.OrderLineItemIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.OrderLineItemProvider.DeepLoad(transactionManager, entity.OrderLineItemIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion OrderLineItemIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.RMARequestItem object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.RMARequestItem instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.RMARequestItem Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.RMARequestItem entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region ReasonForReturnIDSource
			if (CanDeepSave(entity, "ReasonForReturn|ReasonForReturnIDSource", deepSaveType, innerList) 
				&& entity.ReasonForReturnIDSource != null)
			{
				DataRepository.ReasonForReturnProvider.Save(transactionManager, entity.ReasonForReturnIDSource);
				entity.ReasonForReturnID = entity.ReasonForReturnIDSource.ReasonForReturnID;
			}
			#endregion 
			
			#region OrderLineItemIDSource
			if (CanDeepSave(entity, "OrderLineItem|OrderLineItemIDSource", deepSaveType, innerList) 
				&& entity.OrderLineItemIDSource != null)
			{
				DataRepository.OrderLineItemProvider.Save(transactionManager, entity.OrderLineItemIDSource);
				entity.OrderLineItemID = entity.OrderLineItemIDSource.OrderLineItemID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region RMARequestItemChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.RMARequestItem</c>
	///</summary>
	public enum RMARequestItemChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>ReasonForReturn</c> at ReasonForReturnIDSource
		///</summary>
		[ChildEntityType(typeof(ReasonForReturn))]
		ReasonForReturn,
		
		///<summary>
		/// Composite Property for <c>OrderLineItem</c> at OrderLineItemIDSource
		///</summary>
		[ChildEntityType(typeof(OrderLineItem))]
		OrderLineItem,
	}
	
	#endregion RMARequestItemChildEntityTypes
	
	#region RMARequestItemFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;RMARequestItemColumn&gt;"/> class
	/// that is used exclusively with a <see cref="RMARequestItem"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class RMARequestItemFilterBuilder : SqlFilterBuilder<RMARequestItemColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the RMARequestItemFilterBuilder class.
		/// </summary>
		public RMARequestItemFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the RMARequestItemFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public RMARequestItemFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the RMARequestItemFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public RMARequestItemFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion RMARequestItemFilterBuilder
	
	#region RMARequestItemParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;RMARequestItemColumn&gt;"/> class
	/// that is used exclusively with a <see cref="RMARequestItem"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class RMARequestItemParameterBuilder : ParameterizedSqlFilterBuilder<RMARequestItemColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the RMARequestItemParameterBuilder class.
		/// </summary>
		public RMARequestItemParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the RMARequestItemParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public RMARequestItemParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the RMARequestItemParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public RMARequestItemParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion RMARequestItemParameterBuilder
	
	#region RMARequestItemSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;RMARequestItemColumn&gt;"/> class
	/// that is used exclusively with a <see cref="RMARequestItem"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class RMARequestItemSortBuilder : SqlSortBuilder<RMARequestItemColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the RMARequestItemSqlSortBuilder class.
		/// </summary>
		public RMARequestItemSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion RMARequestItemSortBuilder
	
} // end namespace
