﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="CategoryNodeProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class CategoryNodeProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.CategoryNode, ZNode.Libraries.DataAccess.Entities.CategoryNodeKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.CategoryNodeKey key)
		{
			return Delete(transactionManager, key.CategoryNodeID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_categoryNodeID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _categoryNodeID)
		{
			return Delete(null, _categoryNodeID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_categoryNodeID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _categoryNodeID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeCate__CSSID__715DB21C key.
		///		FK__ZNodeCate__CSSID__715DB21C Description: 
		/// </summary>
		/// <param name="_cSSID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.CategoryNode objects.</returns>
		public TList<CategoryNode> GetByCSSID(System.Int32? _cSSID)
		{
			int count = -1;
			return GetByCSSID(_cSSID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeCate__CSSID__715DB21C key.
		///		FK__ZNodeCate__CSSID__715DB21C Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_cSSID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.CategoryNode objects.</returns>
		/// <remarks></remarks>
		public TList<CategoryNode> GetByCSSID(TransactionManager transactionManager, System.Int32? _cSSID)
		{
			int count = -1;
			return GetByCSSID(transactionManager, _cSSID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeCate__CSSID__715DB21C key.
		///		FK__ZNodeCate__CSSID__715DB21C Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_cSSID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.CategoryNode objects.</returns>
		public TList<CategoryNode> GetByCSSID(TransactionManager transactionManager, System.Int32? _cSSID, int start, int pageLength)
		{
			int count = -1;
			return GetByCSSID(transactionManager, _cSSID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeCate__CSSID__715DB21C key.
		///		fKZNodeCateCSSID715DB21C Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_cSSID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.CategoryNode objects.</returns>
		public TList<CategoryNode> GetByCSSID(System.Int32? _cSSID, int start, int pageLength)
		{
			int count =  -1;
			return GetByCSSID(null, _cSSID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeCate__CSSID__715DB21C key.
		///		fKZNodeCateCSSID715DB21C Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_cSSID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.CategoryNode objects.</returns>
		public TList<CategoryNode> GetByCSSID(System.Int32? _cSSID, int start, int pageLength,out int count)
		{
			return GetByCSSID(null, _cSSID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeCate__CSSID__715DB21C key.
		///		FK__ZNodeCate__CSSID__715DB21C Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_cSSID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.CategoryNode objects.</returns>
		public abstract TList<CategoryNode> GetByCSSID(TransactionManager transactionManager, System.Int32? _cSSID, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeCate__Maste__7251D655 key.
		///		FK__ZNodeCate__Maste__7251D655 Description: 
		/// </summary>
		/// <param name="_masterPageID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.CategoryNode objects.</returns>
		public TList<CategoryNode> GetByMasterPageID(System.Int32? _masterPageID)
		{
			int count = -1;
			return GetByMasterPageID(_masterPageID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeCate__Maste__7251D655 key.
		///		FK__ZNodeCate__Maste__7251D655 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_masterPageID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.CategoryNode objects.</returns>
		/// <remarks></remarks>
		public TList<CategoryNode> GetByMasterPageID(TransactionManager transactionManager, System.Int32? _masterPageID)
		{
			int count = -1;
			return GetByMasterPageID(transactionManager, _masterPageID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeCate__Maste__7251D655 key.
		///		FK__ZNodeCate__Maste__7251D655 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_masterPageID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.CategoryNode objects.</returns>
		public TList<CategoryNode> GetByMasterPageID(TransactionManager transactionManager, System.Int32? _masterPageID, int start, int pageLength)
		{
			int count = -1;
			return GetByMasterPageID(transactionManager, _masterPageID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeCate__Maste__7251D655 key.
		///		fKZNodeCateMaste7251D655 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_masterPageID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.CategoryNode objects.</returns>
		public TList<CategoryNode> GetByMasterPageID(System.Int32? _masterPageID, int start, int pageLength)
		{
			int count =  -1;
			return GetByMasterPageID(null, _masterPageID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeCate__Maste__7251D655 key.
		///		fKZNodeCateMaste7251D655 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_masterPageID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.CategoryNode objects.</returns>
		public TList<CategoryNode> GetByMasterPageID(System.Int32? _masterPageID, int start, int pageLength,out int count)
		{
			return GetByMasterPageID(null, _masterPageID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeCate__Maste__7251D655 key.
		///		FK__ZNodeCate__Maste__7251D655 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_masterPageID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.CategoryNode objects.</returns>
		public abstract TList<CategoryNode> GetByMasterPageID(TransactionManager transactionManager, System.Int32? _masterPageID, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeCate__Theme__7345FA8E key.
		///		FK__ZNodeCate__Theme__7345FA8E Description: 
		/// </summary>
		/// <param name="_themeID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.CategoryNode objects.</returns>
		public TList<CategoryNode> GetByThemeID(System.Int32? _themeID)
		{
			int count = -1;
			return GetByThemeID(_themeID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeCate__Theme__7345FA8E key.
		///		FK__ZNodeCate__Theme__7345FA8E Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_themeID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.CategoryNode objects.</returns>
		/// <remarks></remarks>
		public TList<CategoryNode> GetByThemeID(TransactionManager transactionManager, System.Int32? _themeID)
		{
			int count = -1;
			return GetByThemeID(transactionManager, _themeID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeCate__Theme__7345FA8E key.
		///		FK__ZNodeCate__Theme__7345FA8E Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_themeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.CategoryNode objects.</returns>
		public TList<CategoryNode> GetByThemeID(TransactionManager transactionManager, System.Int32? _themeID, int start, int pageLength)
		{
			int count = -1;
			return GetByThemeID(transactionManager, _themeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeCate__Theme__7345FA8E key.
		///		fKZNodeCateTheme7345FA8E Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_themeID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.CategoryNode objects.</returns>
		public TList<CategoryNode> GetByThemeID(System.Int32? _themeID, int start, int pageLength)
		{
			int count =  -1;
			return GetByThemeID(null, _themeID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeCate__Theme__7345FA8E key.
		///		fKZNodeCateTheme7345FA8E Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_themeID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.CategoryNode objects.</returns>
		public TList<CategoryNode> GetByThemeID(System.Int32? _themeID, int start, int pageLength,out int count)
		{
			return GetByThemeID(null, _themeID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodeCate__Theme__7345FA8E key.
		///		FK__ZNodeCate__Theme__7345FA8E Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_themeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.CategoryNode objects.</returns>
		public abstract TList<CategoryNode> GetByThemeID(TransactionManager transactionManager, System.Int32? _themeID, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.CategoryNode Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.CategoryNodeKey key, int start, int pageLength)
		{
			return GetByCategoryNodeID(transactionManager, key.CategoryNodeID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeCategoryNode_ActiveInd index.
		/// </summary>
		/// <param name="_activeInd"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;CategoryNode&gt;"/> class.</returns>
		public TList<CategoryNode> GetByActiveInd(System.Boolean _activeInd)
		{
			int count = -1;
			return GetByActiveInd(null,_activeInd, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCategoryNode_ActiveInd index.
		/// </summary>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CategoryNode&gt;"/> class.</returns>
		public TList<CategoryNode> GetByActiveInd(System.Boolean _activeInd, int start, int pageLength)
		{
			int count = -1;
			return GetByActiveInd(null, _activeInd, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCategoryNode_ActiveInd index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_activeInd"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CategoryNode&gt;"/> class.</returns>
		public TList<CategoryNode> GetByActiveInd(TransactionManager transactionManager, System.Boolean _activeInd)
		{
			int count = -1;
			return GetByActiveInd(transactionManager, _activeInd, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCategoryNode_ActiveInd index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CategoryNode&gt;"/> class.</returns>
		public TList<CategoryNode> GetByActiveInd(TransactionManager transactionManager, System.Boolean _activeInd, int start, int pageLength)
		{
			int count = -1;
			return GetByActiveInd(transactionManager, _activeInd, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCategoryNode_ActiveInd index.
		/// </summary>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CategoryNode&gt;"/> class.</returns>
		public TList<CategoryNode> GetByActiveInd(System.Boolean _activeInd, int start, int pageLength, out int count)
		{
			return GetByActiveInd(null, _activeInd, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCategoryNode_ActiveInd index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;CategoryNode&gt;"/> class.</returns>
		public abstract TList<CategoryNode> GetByActiveInd(TransactionManager transactionManager, System.Boolean _activeInd, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeCategoryNode_CatalogID index.
		/// </summary>
		/// <param name="_catalogID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;CategoryNode&gt;"/> class.</returns>
		public TList<CategoryNode> GetByCatalogID(System.Int32? _catalogID)
		{
			int count = -1;
			return GetByCatalogID(null,_catalogID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCategoryNode_CatalogID index.
		/// </summary>
		/// <param name="_catalogID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CategoryNode&gt;"/> class.</returns>
		public TList<CategoryNode> GetByCatalogID(System.Int32? _catalogID, int start, int pageLength)
		{
			int count = -1;
			return GetByCatalogID(null, _catalogID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCategoryNode_CatalogID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_catalogID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CategoryNode&gt;"/> class.</returns>
		public TList<CategoryNode> GetByCatalogID(TransactionManager transactionManager, System.Int32? _catalogID)
		{
			int count = -1;
			return GetByCatalogID(transactionManager, _catalogID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCategoryNode_CatalogID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_catalogID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CategoryNode&gt;"/> class.</returns>
		public TList<CategoryNode> GetByCatalogID(TransactionManager transactionManager, System.Int32? _catalogID, int start, int pageLength)
		{
			int count = -1;
			return GetByCatalogID(transactionManager, _catalogID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCategoryNode_CatalogID index.
		/// </summary>
		/// <param name="_catalogID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CategoryNode&gt;"/> class.</returns>
		public TList<CategoryNode> GetByCatalogID(System.Int32? _catalogID, int start, int pageLength, out int count)
		{
			return GetByCatalogID(null, _catalogID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCategoryNode_CatalogID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_catalogID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;CategoryNode&gt;"/> class.</returns>
		public abstract TList<CategoryNode> GetByCatalogID(TransactionManager transactionManager, System.Int32? _catalogID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeCategoryNode_CatalogParentActive index.
		/// </summary>
		/// <param name="_catalogID"></param>
		/// <param name="_parentCategoryNodeID"></param>
		/// <param name="_activeInd"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;CategoryNode&gt;"/> class.</returns>
		public TList<CategoryNode> GetByCatalogIDParentCategoryNodeIDActiveInd(System.Int32? _catalogID, System.Int32? _parentCategoryNodeID, System.Boolean _activeInd)
		{
			int count = -1;
			return GetByCatalogIDParentCategoryNodeIDActiveInd(null,_catalogID, _parentCategoryNodeID, _activeInd, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCategoryNode_CatalogParentActive index.
		/// </summary>
		/// <param name="_catalogID"></param>
		/// <param name="_parentCategoryNodeID"></param>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CategoryNode&gt;"/> class.</returns>
		public TList<CategoryNode> GetByCatalogIDParentCategoryNodeIDActiveInd(System.Int32? _catalogID, System.Int32? _parentCategoryNodeID, System.Boolean _activeInd, int start, int pageLength)
		{
			int count = -1;
			return GetByCatalogIDParentCategoryNodeIDActiveInd(null, _catalogID, _parentCategoryNodeID, _activeInd, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCategoryNode_CatalogParentActive index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_catalogID"></param>
		/// <param name="_parentCategoryNodeID"></param>
		/// <param name="_activeInd"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CategoryNode&gt;"/> class.</returns>
		public TList<CategoryNode> GetByCatalogIDParentCategoryNodeIDActiveInd(TransactionManager transactionManager, System.Int32? _catalogID, System.Int32? _parentCategoryNodeID, System.Boolean _activeInd)
		{
			int count = -1;
			return GetByCatalogIDParentCategoryNodeIDActiveInd(transactionManager, _catalogID, _parentCategoryNodeID, _activeInd, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCategoryNode_CatalogParentActive index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_catalogID"></param>
		/// <param name="_parentCategoryNodeID"></param>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CategoryNode&gt;"/> class.</returns>
		public TList<CategoryNode> GetByCatalogIDParentCategoryNodeIDActiveInd(TransactionManager transactionManager, System.Int32? _catalogID, System.Int32? _parentCategoryNodeID, System.Boolean _activeInd, int start, int pageLength)
		{
			int count = -1;
			return GetByCatalogIDParentCategoryNodeIDActiveInd(transactionManager, _catalogID, _parentCategoryNodeID, _activeInd, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCategoryNode_CatalogParentActive index.
		/// </summary>
		/// <param name="_catalogID"></param>
		/// <param name="_parentCategoryNodeID"></param>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CategoryNode&gt;"/> class.</returns>
		public TList<CategoryNode> GetByCatalogIDParentCategoryNodeIDActiveInd(System.Int32? _catalogID, System.Int32? _parentCategoryNodeID, System.Boolean _activeInd, int start, int pageLength, out int count)
		{
			return GetByCatalogIDParentCategoryNodeIDActiveInd(null, _catalogID, _parentCategoryNodeID, _activeInd, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCategoryNode_CatalogParentActive index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_catalogID"></param>
		/// <param name="_parentCategoryNodeID"></param>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;CategoryNode&gt;"/> class.</returns>
		public abstract TList<CategoryNode> GetByCatalogIDParentCategoryNodeIDActiveInd(TransactionManager transactionManager, System.Int32? _catalogID, System.Int32? _parentCategoryNodeID, System.Boolean _activeInd, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeCategoryNode_CategoryID index.
		/// </summary>
		/// <param name="_categoryID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;CategoryNode&gt;"/> class.</returns>
		public TList<CategoryNode> GetByCategoryID(System.Int32 _categoryID)
		{
			int count = -1;
			return GetByCategoryID(null,_categoryID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCategoryNode_CategoryID index.
		/// </summary>
		/// <param name="_categoryID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CategoryNode&gt;"/> class.</returns>
		public TList<CategoryNode> GetByCategoryID(System.Int32 _categoryID, int start, int pageLength)
		{
			int count = -1;
			return GetByCategoryID(null, _categoryID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCategoryNode_CategoryID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_categoryID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CategoryNode&gt;"/> class.</returns>
		public TList<CategoryNode> GetByCategoryID(TransactionManager transactionManager, System.Int32 _categoryID)
		{
			int count = -1;
			return GetByCategoryID(transactionManager, _categoryID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCategoryNode_CategoryID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_categoryID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CategoryNode&gt;"/> class.</returns>
		public TList<CategoryNode> GetByCategoryID(TransactionManager transactionManager, System.Int32 _categoryID, int start, int pageLength)
		{
			int count = -1;
			return GetByCategoryID(transactionManager, _categoryID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCategoryNode_CategoryID index.
		/// </summary>
		/// <param name="_categoryID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CategoryNode&gt;"/> class.</returns>
		public TList<CategoryNode> GetByCategoryID(System.Int32 _categoryID, int start, int pageLength, out int count)
		{
			return GetByCategoryID(null, _categoryID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCategoryNode_CategoryID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_categoryID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;CategoryNode&gt;"/> class.</returns>
		public abstract TList<CategoryNode> GetByCategoryID(TransactionManager transactionManager, System.Int32 _categoryID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZnodeCategoryNode_ParentCategoryNodeID index.
		/// </summary>
		/// <param name="_parentCategoryNodeID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;CategoryNode&gt;"/> class.</returns>
		public TList<CategoryNode> GetByParentCategoryNodeID(System.Int32? _parentCategoryNodeID)
		{
			int count = -1;
			return GetByParentCategoryNodeID(null,_parentCategoryNodeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZnodeCategoryNode_ParentCategoryNodeID index.
		/// </summary>
		/// <param name="_parentCategoryNodeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CategoryNode&gt;"/> class.</returns>
		public TList<CategoryNode> GetByParentCategoryNodeID(System.Int32? _parentCategoryNodeID, int start, int pageLength)
		{
			int count = -1;
			return GetByParentCategoryNodeID(null, _parentCategoryNodeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZnodeCategoryNode_ParentCategoryNodeID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_parentCategoryNodeID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CategoryNode&gt;"/> class.</returns>
		public TList<CategoryNode> GetByParentCategoryNodeID(TransactionManager transactionManager, System.Int32? _parentCategoryNodeID)
		{
			int count = -1;
			return GetByParentCategoryNodeID(transactionManager, _parentCategoryNodeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZnodeCategoryNode_ParentCategoryNodeID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_parentCategoryNodeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CategoryNode&gt;"/> class.</returns>
		public TList<CategoryNode> GetByParentCategoryNodeID(TransactionManager transactionManager, System.Int32? _parentCategoryNodeID, int start, int pageLength)
		{
			int count = -1;
			return GetByParentCategoryNodeID(transactionManager, _parentCategoryNodeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZnodeCategoryNode_ParentCategoryNodeID index.
		/// </summary>
		/// <param name="_parentCategoryNodeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CategoryNode&gt;"/> class.</returns>
		public TList<CategoryNode> GetByParentCategoryNodeID(System.Int32? _parentCategoryNodeID, int start, int pageLength, out int count)
		{
			return GetByParentCategoryNodeID(null, _parentCategoryNodeID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZnodeCategoryNode_ParentCategoryNodeID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_parentCategoryNodeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;CategoryNode&gt;"/> class.</returns>
		public abstract TList<CategoryNode> GetByParentCategoryNodeID(TransactionManager transactionManager, System.Int32? _parentCategoryNodeID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodeCategoryCategory index.
		/// </summary>
		/// <param name="_categoryNodeID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.CategoryNode"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.CategoryNode GetByCategoryNodeID(System.Int32 _categoryNodeID)
		{
			int count = -1;
			return GetByCategoryNodeID(null,_categoryNodeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeCategoryCategory index.
		/// </summary>
		/// <param name="_categoryNodeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.CategoryNode"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.CategoryNode GetByCategoryNodeID(System.Int32 _categoryNodeID, int start, int pageLength)
		{
			int count = -1;
			return GetByCategoryNodeID(null, _categoryNodeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeCategoryCategory index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_categoryNodeID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.CategoryNode"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.CategoryNode GetByCategoryNodeID(TransactionManager transactionManager, System.Int32 _categoryNodeID)
		{
			int count = -1;
			return GetByCategoryNodeID(transactionManager, _categoryNodeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeCategoryCategory index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_categoryNodeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.CategoryNode"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.CategoryNode GetByCategoryNodeID(TransactionManager transactionManager, System.Int32 _categoryNodeID, int start, int pageLength)
		{
			int count = -1;
			return GetByCategoryNodeID(transactionManager, _categoryNodeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeCategoryCategory index.
		/// </summary>
		/// <param name="_categoryNodeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.CategoryNode"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.CategoryNode GetByCategoryNodeID(System.Int32 _categoryNodeID, int start, int pageLength, out int count)
		{
			return GetByCategoryNodeID(null, _categoryNodeID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeCategoryCategory index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_categoryNodeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.CategoryNode"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.CategoryNode GetByCategoryNodeID(TransactionManager transactionManager, System.Int32 _categoryNodeID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;CategoryNode&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;CategoryNode&gt;"/></returns>
		public static TList<CategoryNode> Fill(IDataReader reader, TList<CategoryNode> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.CategoryNode c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("CategoryNode")
					.Append("|").Append((System.Int32)reader[((int)CategoryNodeColumn.CategoryNodeID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<CategoryNode>(
					key.ToString(), // EntityTrackingKey
					"CategoryNode",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.CategoryNode();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.CategoryNodeID = (System.Int32)reader[((int)CategoryNodeColumn.CategoryNodeID - 1)];
					c.CatalogID = (reader.IsDBNull(((int)CategoryNodeColumn.CatalogID - 1)))?null:(System.Int32?)reader[((int)CategoryNodeColumn.CatalogID - 1)];
					c.CategoryID = (System.Int32)reader[((int)CategoryNodeColumn.CategoryID - 1)];
					c.ParentCategoryNodeID = (reader.IsDBNull(((int)CategoryNodeColumn.ParentCategoryNodeID - 1)))?null:(System.Int32?)reader[((int)CategoryNodeColumn.ParentCategoryNodeID - 1)];
					c.BeginDate = (reader.IsDBNull(((int)CategoryNodeColumn.BeginDate - 1)))?null:(System.DateTime?)reader[((int)CategoryNodeColumn.BeginDate - 1)];
					c.EndDate = (reader.IsDBNull(((int)CategoryNodeColumn.EndDate - 1)))?null:(System.DateTime?)reader[((int)CategoryNodeColumn.EndDate - 1)];
					c.DisplayOrder = (reader.IsDBNull(((int)CategoryNodeColumn.DisplayOrder - 1)))?null:(System.Int32?)reader[((int)CategoryNodeColumn.DisplayOrder - 1)];
					c.ActiveInd = (System.Boolean)reader[((int)CategoryNodeColumn.ActiveInd - 1)];
					c.ThemeID = (reader.IsDBNull(((int)CategoryNodeColumn.ThemeID - 1)))?null:(System.Int32?)reader[((int)CategoryNodeColumn.ThemeID - 1)];
					c.CSSID = (reader.IsDBNull(((int)CategoryNodeColumn.CSSID - 1)))?null:(System.Int32?)reader[((int)CategoryNodeColumn.CSSID - 1)];
					c.MasterPageID = (reader.IsDBNull(((int)CategoryNodeColumn.MasterPageID - 1)))?null:(System.Int32?)reader[((int)CategoryNodeColumn.MasterPageID - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.CategoryNode"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.CategoryNode"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.CategoryNode entity)
		{
			if (!reader.Read()) return;
			
			entity.CategoryNodeID = (System.Int32)reader[((int)CategoryNodeColumn.CategoryNodeID - 1)];
			entity.CatalogID = (reader.IsDBNull(((int)CategoryNodeColumn.CatalogID - 1)))?null:(System.Int32?)reader[((int)CategoryNodeColumn.CatalogID - 1)];
			entity.CategoryID = (System.Int32)reader[((int)CategoryNodeColumn.CategoryID - 1)];
			entity.ParentCategoryNodeID = (reader.IsDBNull(((int)CategoryNodeColumn.ParentCategoryNodeID - 1)))?null:(System.Int32?)reader[((int)CategoryNodeColumn.ParentCategoryNodeID - 1)];
			entity.BeginDate = (reader.IsDBNull(((int)CategoryNodeColumn.BeginDate - 1)))?null:(System.DateTime?)reader[((int)CategoryNodeColumn.BeginDate - 1)];
			entity.EndDate = (reader.IsDBNull(((int)CategoryNodeColumn.EndDate - 1)))?null:(System.DateTime?)reader[((int)CategoryNodeColumn.EndDate - 1)];
			entity.DisplayOrder = (reader.IsDBNull(((int)CategoryNodeColumn.DisplayOrder - 1)))?null:(System.Int32?)reader[((int)CategoryNodeColumn.DisplayOrder - 1)];
			entity.ActiveInd = (System.Boolean)reader[((int)CategoryNodeColumn.ActiveInd - 1)];
			entity.ThemeID = (reader.IsDBNull(((int)CategoryNodeColumn.ThemeID - 1)))?null:(System.Int32?)reader[((int)CategoryNodeColumn.ThemeID - 1)];
			entity.CSSID = (reader.IsDBNull(((int)CategoryNodeColumn.CSSID - 1)))?null:(System.Int32?)reader[((int)CategoryNodeColumn.CSSID - 1)];
			entity.MasterPageID = (reader.IsDBNull(((int)CategoryNodeColumn.MasterPageID - 1)))?null:(System.Int32?)reader[((int)CategoryNodeColumn.MasterPageID - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.CategoryNode"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.CategoryNode"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.CategoryNode entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.CategoryNodeID = (System.Int32)dataRow["CategoryNodeID"];
			entity.CatalogID = Convert.IsDBNull(dataRow["CatalogID"]) ? null : (System.Int32?)dataRow["CatalogID"];
			entity.CategoryID = (System.Int32)dataRow["CategoryID"];
			entity.ParentCategoryNodeID = Convert.IsDBNull(dataRow["ParentCategoryNodeID"]) ? null : (System.Int32?)dataRow["ParentCategoryNodeID"];
			entity.BeginDate = Convert.IsDBNull(dataRow["BeginDate"]) ? null : (System.DateTime?)dataRow["BeginDate"];
			entity.EndDate = Convert.IsDBNull(dataRow["EndDate"]) ? null : (System.DateTime?)dataRow["EndDate"];
			entity.DisplayOrder = Convert.IsDBNull(dataRow["DisplayOrder"]) ? null : (System.Int32?)dataRow["DisplayOrder"];
			entity.ActiveInd = (System.Boolean)dataRow["ActiveInd"];
			entity.ThemeID = Convert.IsDBNull(dataRow["ThemeID"]) ? null : (System.Int32?)dataRow["ThemeID"];
			entity.CSSID = Convert.IsDBNull(dataRow["CSSID"]) ? null : (System.Int32?)dataRow["CSSID"];
			entity.MasterPageID = Convert.IsDBNull(dataRow["MasterPageID"]) ? null : (System.Int32?)dataRow["MasterPageID"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.CategoryNode"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.CategoryNode Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.CategoryNode entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region CSSIDSource	
			if (CanDeepLoad(entity, "CSS|CSSIDSource", deepLoadType, innerList) 
				&& entity.CSSIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.CSSID ?? (int)0);
				CSS tmpEntity = EntityManager.LocateEntity<CSS>(EntityLocator.ConstructKeyFromPkItems(typeof(CSS), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CSSIDSource = tmpEntity;
				else
					entity.CSSIDSource = DataRepository.CSSProvider.GetByCSSID(transactionManager, (entity.CSSID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CSSIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CSSIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CSSProvider.DeepLoad(transactionManager, entity.CSSIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CSSIDSource

			#region MasterPageIDSource	
			if (CanDeepLoad(entity, "MasterPage|MasterPageIDSource", deepLoadType, innerList) 
				&& entity.MasterPageIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.MasterPageID ?? (int)0);
				MasterPage tmpEntity = EntityManager.LocateEntity<MasterPage>(EntityLocator.ConstructKeyFromPkItems(typeof(MasterPage), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.MasterPageIDSource = tmpEntity;
				else
					entity.MasterPageIDSource = DataRepository.MasterPageProvider.GetByMasterPageID(transactionManager, (entity.MasterPageID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'MasterPageIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.MasterPageIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.MasterPageProvider.DeepLoad(transactionManager, entity.MasterPageIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion MasterPageIDSource

			#region ThemeIDSource	
			if (CanDeepLoad(entity, "Theme|ThemeIDSource", deepLoadType, innerList) 
				&& entity.ThemeIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.ThemeID ?? (int)0);
				Theme tmpEntity = EntityManager.LocateEntity<Theme>(EntityLocator.ConstructKeyFromPkItems(typeof(Theme), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ThemeIDSource = tmpEntity;
				else
					entity.ThemeIDSource = DataRepository.ThemeProvider.GetByThemeID(transactionManager, (entity.ThemeID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ThemeIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ThemeIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.ThemeProvider.DeepLoad(transactionManager, entity.ThemeIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ThemeIDSource

			#region CategoryIDSource	
			if (CanDeepLoad(entity, "Category|CategoryIDSource", deepLoadType, innerList) 
				&& entity.CategoryIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.CategoryID;
				Category tmpEntity = EntityManager.LocateEntity<Category>(EntityLocator.ConstructKeyFromPkItems(typeof(Category), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CategoryIDSource = tmpEntity;
				else
					entity.CategoryIDSource = DataRepository.CategoryProvider.GetByCategoryID(transactionManager, entity.CategoryID);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CategoryIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CategoryIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CategoryProvider.DeepLoad(transactionManager, entity.CategoryIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CategoryIDSource

			#region ParentCategoryNodeIDSource	
			if (CanDeepLoad(entity, "CategoryNode|ParentCategoryNodeIDSource", deepLoadType, innerList) 
				&& entity.ParentCategoryNodeIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.ParentCategoryNodeID ?? (int)0);
				CategoryNode tmpEntity = EntityManager.LocateEntity<CategoryNode>(EntityLocator.ConstructKeyFromPkItems(typeof(CategoryNode), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ParentCategoryNodeIDSource = tmpEntity;
				else
					entity.ParentCategoryNodeIDSource = DataRepository.CategoryNodeProvider.GetByCategoryNodeID(transactionManager, (entity.ParentCategoryNodeID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ParentCategoryNodeIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ParentCategoryNodeIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CategoryNodeProvider.DeepLoad(transactionManager, entity.ParentCategoryNodeIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ParentCategoryNodeIDSource

			#region CatalogIDSource	
			if (CanDeepLoad(entity, "Catalog|CatalogIDSource", deepLoadType, innerList) 
				&& entity.CatalogIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.CatalogID ?? (int)0);
				Catalog tmpEntity = EntityManager.LocateEntity<Catalog>(EntityLocator.ConstructKeyFromPkItems(typeof(Catalog), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CatalogIDSource = tmpEntity;
				else
					entity.CatalogIDSource = DataRepository.CatalogProvider.GetByCatalogID(transactionManager, (entity.CatalogID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CatalogIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CatalogIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CatalogProvider.DeepLoad(transactionManager, entity.CatalogIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CatalogIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByCategoryNodeID methods when available
			
			#region CategoryNodeCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<CategoryNode>|CategoryNodeCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CategoryNodeCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CategoryNodeCollection = DataRepository.CategoryNodeProvider.GetByParentCategoryNodeID(transactionManager, entity.CategoryNodeID);

				if (deep && entity.CategoryNodeCollection.Count > 0)
				{
					deepHandles.Add("CategoryNodeCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<CategoryNode>) DataRepository.CategoryNodeProvider.DeepLoad,
						new object[] { transactionManager, entity.CategoryNodeCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.CategoryNode object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.CategoryNode instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.CategoryNode Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.CategoryNode entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region CSSIDSource
			if (CanDeepSave(entity, "CSS|CSSIDSource", deepSaveType, innerList) 
				&& entity.CSSIDSource != null)
			{
				DataRepository.CSSProvider.Save(transactionManager, entity.CSSIDSource);
				entity.CSSID = entity.CSSIDSource.CSSID;
			}
			#endregion 
			
			#region MasterPageIDSource
			if (CanDeepSave(entity, "MasterPage|MasterPageIDSource", deepSaveType, innerList) 
				&& entity.MasterPageIDSource != null)
			{
				DataRepository.MasterPageProvider.Save(transactionManager, entity.MasterPageIDSource);
				entity.MasterPageID = entity.MasterPageIDSource.MasterPageID;
			}
			#endregion 
			
			#region ThemeIDSource
			if (CanDeepSave(entity, "Theme|ThemeIDSource", deepSaveType, innerList) 
				&& entity.ThemeIDSource != null)
			{
				DataRepository.ThemeProvider.Save(transactionManager, entity.ThemeIDSource);
				entity.ThemeID = entity.ThemeIDSource.ThemeID;
			}
			#endregion 
			
			#region CategoryIDSource
			if (CanDeepSave(entity, "Category|CategoryIDSource", deepSaveType, innerList) 
				&& entity.CategoryIDSource != null)
			{
				DataRepository.CategoryProvider.Save(transactionManager, entity.CategoryIDSource);
				entity.CategoryID = entity.CategoryIDSource.CategoryID;
			}
			#endregion 
			
			#region ParentCategoryNodeIDSource
			if (CanDeepSave(entity, "CategoryNode|ParentCategoryNodeIDSource", deepSaveType, innerList) 
				&& entity.ParentCategoryNodeIDSource != null)
			{
				DataRepository.CategoryNodeProvider.Save(transactionManager, entity.ParentCategoryNodeIDSource);
				entity.ParentCategoryNodeID = entity.ParentCategoryNodeIDSource.CategoryNodeID;
			}
			#endregion 
			
			#region CatalogIDSource
			if (CanDeepSave(entity, "Catalog|CatalogIDSource", deepSaveType, innerList) 
				&& entity.CatalogIDSource != null)
			{
				DataRepository.CatalogProvider.Save(transactionManager, entity.CatalogIDSource);
				entity.CatalogID = entity.CatalogIDSource.CatalogID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<CategoryNode>
				if (CanDeepSave(entity.CategoryNodeCollection, "List<CategoryNode>|CategoryNodeCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(CategoryNode child in entity.CategoryNodeCollection)
					{
						if(child.ParentCategoryNodeIDSource != null)
						{
							child.ParentCategoryNodeID = child.ParentCategoryNodeIDSource.CategoryNodeID;
						}
						else
						{
							child.ParentCategoryNodeID = entity.CategoryNodeID;
						}

					}

					if (entity.CategoryNodeCollection.Count > 0 || entity.CategoryNodeCollection.DeletedItems.Count > 0)
					{
						//DataRepository.CategoryNodeProvider.Save(transactionManager, entity.CategoryNodeCollection);
						
						deepHandles.Add("CategoryNodeCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< CategoryNode >) DataRepository.CategoryNodeProvider.DeepSave,
							new object[] { transactionManager, entity.CategoryNodeCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region CategoryNodeChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.CategoryNode</c>
	///</summary>
	public enum CategoryNodeChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>CSS</c> at CSSIDSource
		///</summary>
		[ChildEntityType(typeof(CSS))]
		CSS,
		
		///<summary>
		/// Composite Property for <c>MasterPage</c> at MasterPageIDSource
		///</summary>
		[ChildEntityType(typeof(MasterPage))]
		MasterPage,
		
		///<summary>
		/// Composite Property for <c>Theme</c> at ThemeIDSource
		///</summary>
		[ChildEntityType(typeof(Theme))]
		Theme,
		
		///<summary>
		/// Composite Property for <c>Category</c> at CategoryIDSource
		///</summary>
		[ChildEntityType(typeof(Category))]
		Category,
		
		///<summary>
		/// Composite Property for <c>CategoryNode</c> at ParentCategoryNodeIDSource
		///</summary>
		[ChildEntityType(typeof(CategoryNode))]
		CategoryNode,
		
		///<summary>
		/// Composite Property for <c>Catalog</c> at CatalogIDSource
		///</summary>
		[ChildEntityType(typeof(Catalog))]
		Catalog,
		///<summary>
		/// Collection of <c>CategoryNode</c> as OneToMany for CategoryNodeCollection
		///</summary>
		[ChildEntityType(typeof(TList<CategoryNode>))]
		CategoryNodeCollection,
	}
	
	#endregion CategoryNodeChildEntityTypes
	
	#region CategoryNodeFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;CategoryNodeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CategoryNode"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CategoryNodeFilterBuilder : SqlFilterBuilder<CategoryNodeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CategoryNodeFilterBuilder class.
		/// </summary>
		public CategoryNodeFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CategoryNodeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CategoryNodeFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CategoryNodeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CategoryNodeFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CategoryNodeFilterBuilder
	
	#region CategoryNodeParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;CategoryNodeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CategoryNode"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CategoryNodeParameterBuilder : ParameterizedSqlFilterBuilder<CategoryNodeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CategoryNodeParameterBuilder class.
		/// </summary>
		public CategoryNodeParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CategoryNodeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CategoryNodeParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CategoryNodeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CategoryNodeParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CategoryNodeParameterBuilder
	
	#region CategoryNodeSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;CategoryNodeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CategoryNode"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class CategoryNodeSortBuilder : SqlSortBuilder<CategoryNodeColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CategoryNodeSqlSortBuilder class.
		/// </summary>
		public CategoryNodeSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion CategoryNodeSortBuilder
	
} // end namespace
