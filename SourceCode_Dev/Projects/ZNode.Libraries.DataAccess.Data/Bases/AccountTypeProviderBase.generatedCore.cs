﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="AccountTypeProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class AccountTypeProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.AccountType, ZNode.Libraries.DataAccess.Entities.AccountTypeKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.AccountTypeKey key)
		{
			return Delete(transactionManager, key.AccountTypeID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_accountTypeID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _accountTypeID)
		{
			return Delete(null, _accountTypeID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountTypeID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _accountTypeID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.AccountType Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.AccountTypeKey key, int start, int pageLength)
		{
			return GetByAccountTypeID(transactionManager, key.AccountTypeID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_AccountType index.
		/// </summary>
		/// <param name="_accountTypeID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.AccountType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.AccountType GetByAccountTypeID(System.Int32 _accountTypeID)
		{
			int count = -1;
			return GetByAccountTypeID(null,_accountTypeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AccountType index.
		/// </summary>
		/// <param name="_accountTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.AccountType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.AccountType GetByAccountTypeID(System.Int32 _accountTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetByAccountTypeID(null, _accountTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AccountType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountTypeID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.AccountType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.AccountType GetByAccountTypeID(TransactionManager transactionManager, System.Int32 _accountTypeID)
		{
			int count = -1;
			return GetByAccountTypeID(transactionManager, _accountTypeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AccountType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.AccountType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.AccountType GetByAccountTypeID(TransactionManager transactionManager, System.Int32 _accountTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetByAccountTypeID(transactionManager, _accountTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AccountType index.
		/// </summary>
		/// <param name="_accountTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.AccountType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.AccountType GetByAccountTypeID(System.Int32 _accountTypeID, int start, int pageLength, out int count)
		{
			return GetByAccountTypeID(null, _accountTypeID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AccountType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.AccountType"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.AccountType GetByAccountTypeID(TransactionManager transactionManager, System.Int32 _accountTypeID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;AccountType&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;AccountType&gt;"/></returns>
		public static TList<AccountType> Fill(IDataReader reader, TList<AccountType> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.AccountType c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("AccountType")
					.Append("|").Append((System.Int32)reader[((int)AccountTypeColumn.AccountTypeID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<AccountType>(
					key.ToString(), // EntityTrackingKey
					"AccountType",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.AccountType();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.AccountTypeID = (System.Int32)reader[((int)AccountTypeColumn.AccountTypeID - 1)];
					c.OriginalAccountTypeID = c.AccountTypeID;
					c.AccountTypeNme = (System.String)reader[((int)AccountTypeColumn.AccountTypeNme - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.AccountType"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.AccountType"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.AccountType entity)
		{
			if (!reader.Read()) return;
			
			entity.AccountTypeID = (System.Int32)reader[((int)AccountTypeColumn.AccountTypeID - 1)];
			entity.OriginalAccountTypeID = (System.Int32)reader["AccountTypeID"];
			entity.AccountTypeNme = (System.String)reader[((int)AccountTypeColumn.AccountTypeNme - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.AccountType"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.AccountType"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.AccountType entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.AccountTypeID = (System.Int32)dataRow["AccountTypeID"];
			entity.OriginalAccountTypeID = (System.Int32)dataRow["AccountTypeID"];
			entity.AccountTypeNme = (System.String)dataRow["AccountTypeNme"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.AccountType"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.AccountType Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.AccountType entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByAccountTypeID methods when available
			
			#region AccountCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Account>|AccountCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AccountCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.AccountCollection = DataRepository.AccountProvider.GetByAccountTypeID(transactionManager, entity.AccountTypeID);

				if (deep && entity.AccountCollection.Count > 0)
				{
					deepHandles.Add("AccountCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Account>) DataRepository.AccountProvider.DeepLoad,
						new object[] { transactionManager, entity.AccountCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.AccountType object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.AccountType instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.AccountType Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.AccountType entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<Account>
				if (CanDeepSave(entity.AccountCollection, "List<Account>|AccountCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Account child in entity.AccountCollection)
					{
						if(child.AccountTypeIDSource != null)
						{
							child.AccountTypeID = child.AccountTypeIDSource.AccountTypeID;
						}
						else
						{
							child.AccountTypeID = entity.AccountTypeID;
						}

					}

					if (entity.AccountCollection.Count > 0 || entity.AccountCollection.DeletedItems.Count > 0)
					{
						//DataRepository.AccountProvider.Save(transactionManager, entity.AccountCollection);
						
						deepHandles.Add("AccountCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Account >) DataRepository.AccountProvider.DeepSave,
							new object[] { transactionManager, entity.AccountCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region AccountTypeChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.AccountType</c>
	///</summary>
	public enum AccountTypeChildEntityTypes
	{
		///<summary>
		/// Collection of <c>AccountType</c> as OneToMany for AccountCollection
		///</summary>
		[ChildEntityType(typeof(TList<Account>))]
		AccountCollection,
	}
	
	#endregion AccountTypeChildEntityTypes
	
	#region AccountTypeFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;AccountTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AccountType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AccountTypeFilterBuilder : SqlFilterBuilder<AccountTypeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AccountTypeFilterBuilder class.
		/// </summary>
		public AccountTypeFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the AccountTypeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AccountTypeFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AccountTypeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AccountTypeFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AccountTypeFilterBuilder
	
	#region AccountTypeParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;AccountTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AccountType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AccountTypeParameterBuilder : ParameterizedSqlFilterBuilder<AccountTypeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AccountTypeParameterBuilder class.
		/// </summary>
		public AccountTypeParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the AccountTypeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AccountTypeParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AccountTypeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AccountTypeParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AccountTypeParameterBuilder
	
	#region AccountTypeSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;AccountTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AccountType"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class AccountTypeSortBuilder : SqlSortBuilder<AccountTypeColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AccountTypeSqlSortBuilder class.
		/// </summary>
		public AccountTypeSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion AccountTypeSortBuilder
	
} // end namespace
