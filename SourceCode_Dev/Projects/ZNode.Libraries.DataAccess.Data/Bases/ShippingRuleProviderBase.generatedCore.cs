﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ShippingRuleProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class ShippingRuleProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.ShippingRule, ZNode.Libraries.DataAccess.Entities.ShippingRuleKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ShippingRuleKey key)
		{
			return Delete(transactionManager, key.ShippingRuleID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_shippingRuleID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _shippingRuleID)
		{
			return Delete(null, _shippingRuleID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_shippingRuleID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _shippingRuleID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.ShippingRule Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ShippingRuleKey key, int start, int pageLength)
		{
			return GetByShippingRuleID(transactionManager, key.ShippingRuleID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeShippingRule_ExternalID index.
		/// </summary>
		/// <param name="_externalID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;ShippingRule&gt;"/> class.</returns>
		public TList<ShippingRule> GetByExternalID(System.String _externalID)
		{
			int count = -1;
			return GetByExternalID(null,_externalID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeShippingRule_ExternalID index.
		/// </summary>
		/// <param name="_externalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ShippingRule&gt;"/> class.</returns>
		public TList<ShippingRule> GetByExternalID(System.String _externalID, int start, int pageLength)
		{
			int count = -1;
			return GetByExternalID(null, _externalID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeShippingRule_ExternalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_externalID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ShippingRule&gt;"/> class.</returns>
		public TList<ShippingRule> GetByExternalID(TransactionManager transactionManager, System.String _externalID)
		{
			int count = -1;
			return GetByExternalID(transactionManager, _externalID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeShippingRule_ExternalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_externalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ShippingRule&gt;"/> class.</returns>
		public TList<ShippingRule> GetByExternalID(TransactionManager transactionManager, System.String _externalID, int start, int pageLength)
		{
			int count = -1;
			return GetByExternalID(transactionManager, _externalID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeShippingRule_ExternalID index.
		/// </summary>
		/// <param name="_externalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ShippingRule&gt;"/> class.</returns>
		public TList<ShippingRule> GetByExternalID(System.String _externalID, int start, int pageLength, out int count)
		{
			return GetByExternalID(null, _externalID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeShippingRule_ExternalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_externalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;ShippingRule&gt;"/> class.</returns>
		public abstract TList<ShippingRule> GetByExternalID(TransactionManager transactionManager, System.String _externalID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeShippingRule_ShippingID index.
		/// </summary>
		/// <param name="_shippingID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;ShippingRule&gt;"/> class.</returns>
		public TList<ShippingRule> GetByShippingID(System.Int32 _shippingID)
		{
			int count = -1;
			return GetByShippingID(null,_shippingID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeShippingRule_ShippingID index.
		/// </summary>
		/// <param name="_shippingID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ShippingRule&gt;"/> class.</returns>
		public TList<ShippingRule> GetByShippingID(System.Int32 _shippingID, int start, int pageLength)
		{
			int count = -1;
			return GetByShippingID(null, _shippingID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeShippingRule_ShippingID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_shippingID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ShippingRule&gt;"/> class.</returns>
		public TList<ShippingRule> GetByShippingID(TransactionManager transactionManager, System.Int32 _shippingID)
		{
			int count = -1;
			return GetByShippingID(transactionManager, _shippingID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeShippingRule_ShippingID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_shippingID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ShippingRule&gt;"/> class.</returns>
		public TList<ShippingRule> GetByShippingID(TransactionManager transactionManager, System.Int32 _shippingID, int start, int pageLength)
		{
			int count = -1;
			return GetByShippingID(transactionManager, _shippingID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeShippingRule_ShippingID index.
		/// </summary>
		/// <param name="_shippingID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ShippingRule&gt;"/> class.</returns>
		public TList<ShippingRule> GetByShippingID(System.Int32 _shippingID, int start, int pageLength, out int count)
		{
			return GetByShippingID(null, _shippingID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeShippingRule_ShippingID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_shippingID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;ShippingRule&gt;"/> class.</returns>
		public abstract TList<ShippingRule> GetByShippingID(TransactionManager transactionManager, System.Int32 _shippingID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeShippingRule_ShippingRuleTypeID index.
		/// </summary>
		/// <param name="_shippingRuleTypeID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;ShippingRule&gt;"/> class.</returns>
		public TList<ShippingRule> GetByShippingRuleTypeID(System.Int32 _shippingRuleTypeID)
		{
			int count = -1;
			return GetByShippingRuleTypeID(null,_shippingRuleTypeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeShippingRule_ShippingRuleTypeID index.
		/// </summary>
		/// <param name="_shippingRuleTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ShippingRule&gt;"/> class.</returns>
		public TList<ShippingRule> GetByShippingRuleTypeID(System.Int32 _shippingRuleTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetByShippingRuleTypeID(null, _shippingRuleTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeShippingRule_ShippingRuleTypeID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_shippingRuleTypeID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ShippingRule&gt;"/> class.</returns>
		public TList<ShippingRule> GetByShippingRuleTypeID(TransactionManager transactionManager, System.Int32 _shippingRuleTypeID)
		{
			int count = -1;
			return GetByShippingRuleTypeID(transactionManager, _shippingRuleTypeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeShippingRule_ShippingRuleTypeID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_shippingRuleTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ShippingRule&gt;"/> class.</returns>
		public TList<ShippingRule> GetByShippingRuleTypeID(TransactionManager transactionManager, System.Int32 _shippingRuleTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetByShippingRuleTypeID(transactionManager, _shippingRuleTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeShippingRule_ShippingRuleTypeID index.
		/// </summary>
		/// <param name="_shippingRuleTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ShippingRule&gt;"/> class.</returns>
		public TList<ShippingRule> GetByShippingRuleTypeID(System.Int32 _shippingRuleTypeID, int start, int pageLength, out int count)
		{
			return GetByShippingRuleTypeID(null, _shippingRuleTypeID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeShippingRule_ShippingRuleTypeID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_shippingRuleTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;ShippingRule&gt;"/> class.</returns>
		public abstract TList<ShippingRule> GetByShippingRuleTypeID(TransactionManager transactionManager, System.Int32 _shippingRuleTypeID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_SC_ShippingRule index.
		/// </summary>
		/// <param name="_shippingRuleID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ShippingRule"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ShippingRule GetByShippingRuleID(System.Int32 _shippingRuleID)
		{
			int count = -1;
			return GetByShippingRuleID(null,_shippingRuleID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SC_ShippingRule index.
		/// </summary>
		/// <param name="_shippingRuleID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ShippingRule"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ShippingRule GetByShippingRuleID(System.Int32 _shippingRuleID, int start, int pageLength)
		{
			int count = -1;
			return GetByShippingRuleID(null, _shippingRuleID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SC_ShippingRule index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_shippingRuleID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ShippingRule"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ShippingRule GetByShippingRuleID(TransactionManager transactionManager, System.Int32 _shippingRuleID)
		{
			int count = -1;
			return GetByShippingRuleID(transactionManager, _shippingRuleID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SC_ShippingRule index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_shippingRuleID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ShippingRule"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ShippingRule GetByShippingRuleID(TransactionManager transactionManager, System.Int32 _shippingRuleID, int start, int pageLength)
		{
			int count = -1;
			return GetByShippingRuleID(transactionManager, _shippingRuleID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SC_ShippingRule index.
		/// </summary>
		/// <param name="_shippingRuleID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ShippingRule"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ShippingRule GetByShippingRuleID(System.Int32 _shippingRuleID, int start, int pageLength, out int count)
		{
			return GetByShippingRuleID(null, _shippingRuleID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SC_ShippingRule index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_shippingRuleID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ShippingRule"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.ShippingRule GetByShippingRuleID(TransactionManager transactionManager, System.Int32 _shippingRuleID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;ShippingRule&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;ShippingRule&gt;"/></returns>
		public static TList<ShippingRule> Fill(IDataReader reader, TList<ShippingRule> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.ShippingRule c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("ShippingRule")
					.Append("|").Append((System.Int32)reader[((int)ShippingRuleColumn.ShippingRuleID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<ShippingRule>(
					key.ToString(), // EntityTrackingKey
					"ShippingRule",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.ShippingRule();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.ShippingRuleID = (System.Int32)reader[((int)ShippingRuleColumn.ShippingRuleID - 1)];
					c.ShippingID = (System.Int32)reader[((int)ShippingRuleColumn.ShippingID - 1)];
					c.ShippingRuleTypeID = (System.Int32)reader[((int)ShippingRuleColumn.ShippingRuleTypeID - 1)];
					c.ClassName = (reader.IsDBNull(((int)ShippingRuleColumn.ClassName - 1)))?null:(System.String)reader[((int)ShippingRuleColumn.ClassName - 1)];
					c.LowerLimit = (reader.IsDBNull(((int)ShippingRuleColumn.LowerLimit - 1)))?null:(System.Decimal?)reader[((int)ShippingRuleColumn.LowerLimit - 1)];
					c.UpperLimit = (reader.IsDBNull(((int)ShippingRuleColumn.UpperLimit - 1)))?null:(System.Decimal?)reader[((int)ShippingRuleColumn.UpperLimit - 1)];
					c.BaseCost = (System.Decimal)reader[((int)ShippingRuleColumn.BaseCost - 1)];
					c.PerItemCost = (System.Decimal)reader[((int)ShippingRuleColumn.PerItemCost - 1)];
					c.Custom1 = (reader.IsDBNull(((int)ShippingRuleColumn.Custom1 - 1)))?null:(System.String)reader[((int)ShippingRuleColumn.Custom1 - 1)];
					c.Custom2 = (reader.IsDBNull(((int)ShippingRuleColumn.Custom2 - 1)))?null:(System.String)reader[((int)ShippingRuleColumn.Custom2 - 1)];
					c.Custom3 = (reader.IsDBNull(((int)ShippingRuleColumn.Custom3 - 1)))?null:(System.String)reader[((int)ShippingRuleColumn.Custom3 - 1)];
					c.ExternalID = (reader.IsDBNull(((int)ShippingRuleColumn.ExternalID - 1)))?null:(System.String)reader[((int)ShippingRuleColumn.ExternalID - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.ShippingRule"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ShippingRule"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.ShippingRule entity)
		{
			if (!reader.Read()) return;
			
			entity.ShippingRuleID = (System.Int32)reader[((int)ShippingRuleColumn.ShippingRuleID - 1)];
			entity.ShippingID = (System.Int32)reader[((int)ShippingRuleColumn.ShippingID - 1)];
			entity.ShippingRuleTypeID = (System.Int32)reader[((int)ShippingRuleColumn.ShippingRuleTypeID - 1)];
			entity.ClassName = (reader.IsDBNull(((int)ShippingRuleColumn.ClassName - 1)))?null:(System.String)reader[((int)ShippingRuleColumn.ClassName - 1)];
			entity.LowerLimit = (reader.IsDBNull(((int)ShippingRuleColumn.LowerLimit - 1)))?null:(System.Decimal?)reader[((int)ShippingRuleColumn.LowerLimit - 1)];
			entity.UpperLimit = (reader.IsDBNull(((int)ShippingRuleColumn.UpperLimit - 1)))?null:(System.Decimal?)reader[((int)ShippingRuleColumn.UpperLimit - 1)];
			entity.BaseCost = (System.Decimal)reader[((int)ShippingRuleColumn.BaseCost - 1)];
			entity.PerItemCost = (System.Decimal)reader[((int)ShippingRuleColumn.PerItemCost - 1)];
			entity.Custom1 = (reader.IsDBNull(((int)ShippingRuleColumn.Custom1 - 1)))?null:(System.String)reader[((int)ShippingRuleColumn.Custom1 - 1)];
			entity.Custom2 = (reader.IsDBNull(((int)ShippingRuleColumn.Custom2 - 1)))?null:(System.String)reader[((int)ShippingRuleColumn.Custom2 - 1)];
			entity.Custom3 = (reader.IsDBNull(((int)ShippingRuleColumn.Custom3 - 1)))?null:(System.String)reader[((int)ShippingRuleColumn.Custom3 - 1)];
			entity.ExternalID = (reader.IsDBNull(((int)ShippingRuleColumn.ExternalID - 1)))?null:(System.String)reader[((int)ShippingRuleColumn.ExternalID - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.ShippingRule"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ShippingRule"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.ShippingRule entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.ShippingRuleID = (System.Int32)dataRow["ShippingRuleID"];
			entity.ShippingID = (System.Int32)dataRow["ShippingID"];
			entity.ShippingRuleTypeID = (System.Int32)dataRow["ShippingRuleTypeID"];
			entity.ClassName = Convert.IsDBNull(dataRow["ClassName"]) ? null : (System.String)dataRow["ClassName"];
			entity.LowerLimit = Convert.IsDBNull(dataRow["LowerLimit"]) ? null : (System.Decimal?)dataRow["LowerLimit"];
			entity.UpperLimit = Convert.IsDBNull(dataRow["UpperLimit"]) ? null : (System.Decimal?)dataRow["UpperLimit"];
			entity.BaseCost = (System.Decimal)dataRow["BaseCost"];
			entity.PerItemCost = (System.Decimal)dataRow["PerItemCost"];
			entity.Custom1 = Convert.IsDBNull(dataRow["Custom1"]) ? null : (System.String)dataRow["Custom1"];
			entity.Custom2 = Convert.IsDBNull(dataRow["Custom2"]) ? null : (System.String)dataRow["Custom2"];
			entity.Custom3 = Convert.IsDBNull(dataRow["Custom3"]) ? null : (System.String)dataRow["Custom3"];
			entity.ExternalID = Convert.IsDBNull(dataRow["ExternalID"]) ? null : (System.String)dataRow["ExternalID"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ShippingRule"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.ShippingRule Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ShippingRule entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region ShippingIDSource	
			if (CanDeepLoad(entity, "Shipping|ShippingIDSource", deepLoadType, innerList) 
				&& entity.ShippingIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.ShippingID;
				Shipping tmpEntity = EntityManager.LocateEntity<Shipping>(EntityLocator.ConstructKeyFromPkItems(typeof(Shipping), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ShippingIDSource = tmpEntity;
				else
					entity.ShippingIDSource = DataRepository.ShippingProvider.GetByShippingID(transactionManager, entity.ShippingID);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ShippingIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ShippingIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.ShippingProvider.DeepLoad(transactionManager, entity.ShippingIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ShippingIDSource

			#region ShippingRuleTypeIDSource	
			if (CanDeepLoad(entity, "ShippingRuleType|ShippingRuleTypeIDSource", deepLoadType, innerList) 
				&& entity.ShippingRuleTypeIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.ShippingRuleTypeID;
				ShippingRuleType tmpEntity = EntityManager.LocateEntity<ShippingRuleType>(EntityLocator.ConstructKeyFromPkItems(typeof(ShippingRuleType), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ShippingRuleTypeIDSource = tmpEntity;
				else
					entity.ShippingRuleTypeIDSource = DataRepository.ShippingRuleTypeProvider.GetByShippingRuleTypeID(transactionManager, entity.ShippingRuleTypeID);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ShippingRuleTypeIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ShippingRuleTypeIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.ShippingRuleTypeProvider.DeepLoad(transactionManager, entity.ShippingRuleTypeIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ShippingRuleTypeIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.ShippingRule object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.ShippingRule instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.ShippingRule Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ShippingRule entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region ShippingIDSource
			if (CanDeepSave(entity, "Shipping|ShippingIDSource", deepSaveType, innerList) 
				&& entity.ShippingIDSource != null)
			{
				DataRepository.ShippingProvider.Save(transactionManager, entity.ShippingIDSource);
				entity.ShippingID = entity.ShippingIDSource.ShippingID;
			}
			#endregion 
			
			#region ShippingRuleTypeIDSource
			if (CanDeepSave(entity, "ShippingRuleType|ShippingRuleTypeIDSource", deepSaveType, innerList) 
				&& entity.ShippingRuleTypeIDSource != null)
			{
				DataRepository.ShippingRuleTypeProvider.Save(transactionManager, entity.ShippingRuleTypeIDSource);
				entity.ShippingRuleTypeID = entity.ShippingRuleTypeIDSource.ShippingRuleTypeID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region ShippingRuleChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.ShippingRule</c>
	///</summary>
	public enum ShippingRuleChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Shipping</c> at ShippingIDSource
		///</summary>
		[ChildEntityType(typeof(Shipping))]
		Shipping,
		
		///<summary>
		/// Composite Property for <c>ShippingRuleType</c> at ShippingRuleTypeIDSource
		///</summary>
		[ChildEntityType(typeof(ShippingRuleType))]
		ShippingRuleType,
	}
	
	#endregion ShippingRuleChildEntityTypes
	
	#region ShippingRuleFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;ShippingRuleColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ShippingRule"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ShippingRuleFilterBuilder : SqlFilterBuilder<ShippingRuleColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ShippingRuleFilterBuilder class.
		/// </summary>
		public ShippingRuleFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ShippingRuleFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ShippingRuleFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ShippingRuleFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ShippingRuleFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ShippingRuleFilterBuilder
	
	#region ShippingRuleParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;ShippingRuleColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ShippingRule"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ShippingRuleParameterBuilder : ParameterizedSqlFilterBuilder<ShippingRuleColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ShippingRuleParameterBuilder class.
		/// </summary>
		public ShippingRuleParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ShippingRuleParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ShippingRuleParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ShippingRuleParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ShippingRuleParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ShippingRuleParameterBuilder
	
	#region ShippingRuleSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;ShippingRuleColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ShippingRule"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ShippingRuleSortBuilder : SqlSortBuilder<ShippingRuleColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ShippingRuleSqlSortBuilder class.
		/// </summary>
		public ShippingRuleSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ShippingRuleSortBuilder
	
} // end namespace
