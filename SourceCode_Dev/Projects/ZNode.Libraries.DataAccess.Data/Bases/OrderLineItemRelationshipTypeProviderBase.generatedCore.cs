﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="OrderLineItemRelationshipTypeProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class OrderLineItemRelationshipTypeProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.OrderLineItemRelationshipType, ZNode.Libraries.DataAccess.Entities.OrderLineItemRelationshipTypeKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.OrderLineItemRelationshipTypeKey key)
		{
			return Delete(transactionManager, key.OrderLineItemRelationshipTypeID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_orderLineItemRelationshipTypeID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _orderLineItemRelationshipTypeID)
		{
			return Delete(null, _orderLineItemRelationshipTypeID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_orderLineItemRelationshipTypeID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _orderLineItemRelationshipTypeID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.OrderLineItemRelationshipType Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.OrderLineItemRelationshipTypeKey key, int start, int pageLength)
		{
			return GetByOrderLineItemRelationshipTypeID(transactionManager, key.OrderLineItemRelationshipTypeID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodeOrderLineItemRelationshipType index.
		/// </summary>
		/// <param name="_orderLineItemRelationshipTypeID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.OrderLineItemRelationshipType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.OrderLineItemRelationshipType GetByOrderLineItemRelationshipTypeID(System.Int32 _orderLineItemRelationshipTypeID)
		{
			int count = -1;
			return GetByOrderLineItemRelationshipTypeID(null,_orderLineItemRelationshipTypeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeOrderLineItemRelationshipType index.
		/// </summary>
		/// <param name="_orderLineItemRelationshipTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.OrderLineItemRelationshipType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.OrderLineItemRelationshipType GetByOrderLineItemRelationshipTypeID(System.Int32 _orderLineItemRelationshipTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetByOrderLineItemRelationshipTypeID(null, _orderLineItemRelationshipTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeOrderLineItemRelationshipType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_orderLineItemRelationshipTypeID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.OrderLineItemRelationshipType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.OrderLineItemRelationshipType GetByOrderLineItemRelationshipTypeID(TransactionManager transactionManager, System.Int32 _orderLineItemRelationshipTypeID)
		{
			int count = -1;
			return GetByOrderLineItemRelationshipTypeID(transactionManager, _orderLineItemRelationshipTypeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeOrderLineItemRelationshipType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_orderLineItemRelationshipTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.OrderLineItemRelationshipType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.OrderLineItemRelationshipType GetByOrderLineItemRelationshipTypeID(TransactionManager transactionManager, System.Int32 _orderLineItemRelationshipTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetByOrderLineItemRelationshipTypeID(transactionManager, _orderLineItemRelationshipTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeOrderLineItemRelationshipType index.
		/// </summary>
		/// <param name="_orderLineItemRelationshipTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.OrderLineItemRelationshipType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.OrderLineItemRelationshipType GetByOrderLineItemRelationshipTypeID(System.Int32 _orderLineItemRelationshipTypeID, int start, int pageLength, out int count)
		{
			return GetByOrderLineItemRelationshipTypeID(null, _orderLineItemRelationshipTypeID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeOrderLineItemRelationshipType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_orderLineItemRelationshipTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.OrderLineItemRelationshipType"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.OrderLineItemRelationshipType GetByOrderLineItemRelationshipTypeID(TransactionManager transactionManager, System.Int32 _orderLineItemRelationshipTypeID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;OrderLineItemRelationshipType&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;OrderLineItemRelationshipType&gt;"/></returns>
		public static TList<OrderLineItemRelationshipType> Fill(IDataReader reader, TList<OrderLineItemRelationshipType> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.OrderLineItemRelationshipType c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("OrderLineItemRelationshipType")
					.Append("|").Append((System.Int32)reader[((int)OrderLineItemRelationshipTypeColumn.OrderLineItemRelationshipTypeID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<OrderLineItemRelationshipType>(
					key.ToString(), // EntityTrackingKey
					"OrderLineItemRelationshipType",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.OrderLineItemRelationshipType();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.OrderLineItemRelationshipTypeID = (System.Int32)reader[((int)OrderLineItemRelationshipTypeColumn.OrderLineItemRelationshipTypeID - 1)];
					c.Name = (System.String)reader[((int)OrderLineItemRelationshipTypeColumn.Name - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.OrderLineItemRelationshipType"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.OrderLineItemRelationshipType"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.OrderLineItemRelationshipType entity)
		{
			if (!reader.Read()) return;
			
			entity.OrderLineItemRelationshipTypeID = (System.Int32)reader[((int)OrderLineItemRelationshipTypeColumn.OrderLineItemRelationshipTypeID - 1)];
			entity.Name = (System.String)reader[((int)OrderLineItemRelationshipTypeColumn.Name - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.OrderLineItemRelationshipType"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.OrderLineItemRelationshipType"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.OrderLineItemRelationshipType entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.OrderLineItemRelationshipTypeID = (System.Int32)dataRow["OrderLineItemRelationshipTypeID"];
			entity.Name = (System.String)dataRow["Name"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.OrderLineItemRelationshipType"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.OrderLineItemRelationshipType Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.OrderLineItemRelationshipType entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByOrderLineItemRelationshipTypeID methods when available
			
			#region SavedCartLineItemCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<SavedCartLineItem>|SavedCartLineItemCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SavedCartLineItemCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.SavedCartLineItemCollection = DataRepository.SavedCartLineItemProvider.GetByOrderLineItemRelationshipTypeID(transactionManager, entity.OrderLineItemRelationshipTypeID);

				if (deep && entity.SavedCartLineItemCollection.Count > 0)
				{
					deepHandles.Add("SavedCartLineItemCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<SavedCartLineItem>) DataRepository.SavedCartLineItemProvider.DeepLoad,
						new object[] { transactionManager, entity.SavedCartLineItemCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region OrderLineItemCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<OrderLineItem>|OrderLineItemCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'OrderLineItemCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.OrderLineItemCollection = DataRepository.OrderLineItemProvider.GetByOrderLineItemRelationshipTypeID(transactionManager, entity.OrderLineItemRelationshipTypeID);

				if (deep && entity.OrderLineItemCollection.Count > 0)
				{
					deepHandles.Add("OrderLineItemCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<OrderLineItem>) DataRepository.OrderLineItemProvider.DeepLoad,
						new object[] { transactionManager, entity.OrderLineItemCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.OrderLineItemRelationshipType object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.OrderLineItemRelationshipType instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.OrderLineItemRelationshipType Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.OrderLineItemRelationshipType entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<SavedCartLineItem>
				if (CanDeepSave(entity.SavedCartLineItemCollection, "List<SavedCartLineItem>|SavedCartLineItemCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(SavedCartLineItem child in entity.SavedCartLineItemCollection)
					{
						if(child.OrderLineItemRelationshipTypeIDSource != null)
						{
							child.OrderLineItemRelationshipTypeID = child.OrderLineItemRelationshipTypeIDSource.OrderLineItemRelationshipTypeID;
						}
						else
						{
							child.OrderLineItemRelationshipTypeID = entity.OrderLineItemRelationshipTypeID;
						}

					}

					if (entity.SavedCartLineItemCollection.Count > 0 || entity.SavedCartLineItemCollection.DeletedItems.Count > 0)
					{
						//DataRepository.SavedCartLineItemProvider.Save(transactionManager, entity.SavedCartLineItemCollection);
						
						deepHandles.Add("SavedCartLineItemCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< SavedCartLineItem >) DataRepository.SavedCartLineItemProvider.DeepSave,
							new object[] { transactionManager, entity.SavedCartLineItemCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<OrderLineItem>
				if (CanDeepSave(entity.OrderLineItemCollection, "List<OrderLineItem>|OrderLineItemCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(OrderLineItem child in entity.OrderLineItemCollection)
					{
						if(child.OrderLineItemRelationshipTypeIDSource != null)
						{
							child.OrderLineItemRelationshipTypeID = child.OrderLineItemRelationshipTypeIDSource.OrderLineItemRelationshipTypeID;
						}
						else
						{
							child.OrderLineItemRelationshipTypeID = entity.OrderLineItemRelationshipTypeID;
						}

					}

					if (entity.OrderLineItemCollection.Count > 0 || entity.OrderLineItemCollection.DeletedItems.Count > 0)
					{
						//DataRepository.OrderLineItemProvider.Save(transactionManager, entity.OrderLineItemCollection);
						
						deepHandles.Add("OrderLineItemCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< OrderLineItem >) DataRepository.OrderLineItemProvider.DeepSave,
							new object[] { transactionManager, entity.OrderLineItemCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region OrderLineItemRelationshipTypeChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.OrderLineItemRelationshipType</c>
	///</summary>
	public enum OrderLineItemRelationshipTypeChildEntityTypes
	{
		///<summary>
		/// Collection of <c>OrderLineItemRelationshipType</c> as OneToMany for SavedCartLineItemCollection
		///</summary>
		[ChildEntityType(typeof(TList<SavedCartLineItem>))]
		SavedCartLineItemCollection,
		///<summary>
		/// Collection of <c>OrderLineItemRelationshipType</c> as OneToMany for OrderLineItemCollection
		///</summary>
		[ChildEntityType(typeof(TList<OrderLineItem>))]
		OrderLineItemCollection,
	}
	
	#endregion OrderLineItemRelationshipTypeChildEntityTypes
	
	#region OrderLineItemRelationshipTypeFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;OrderLineItemRelationshipTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="OrderLineItemRelationshipType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class OrderLineItemRelationshipTypeFilterBuilder : SqlFilterBuilder<OrderLineItemRelationshipTypeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OrderLineItemRelationshipTypeFilterBuilder class.
		/// </summary>
		public OrderLineItemRelationshipTypeFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the OrderLineItemRelationshipTypeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public OrderLineItemRelationshipTypeFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the OrderLineItemRelationshipTypeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public OrderLineItemRelationshipTypeFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion OrderLineItemRelationshipTypeFilterBuilder
	
	#region OrderLineItemRelationshipTypeParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;OrderLineItemRelationshipTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="OrderLineItemRelationshipType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class OrderLineItemRelationshipTypeParameterBuilder : ParameterizedSqlFilterBuilder<OrderLineItemRelationshipTypeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OrderLineItemRelationshipTypeParameterBuilder class.
		/// </summary>
		public OrderLineItemRelationshipTypeParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the OrderLineItemRelationshipTypeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public OrderLineItemRelationshipTypeParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the OrderLineItemRelationshipTypeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public OrderLineItemRelationshipTypeParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion OrderLineItemRelationshipTypeParameterBuilder
	
	#region OrderLineItemRelationshipTypeSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;OrderLineItemRelationshipTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="OrderLineItemRelationshipType"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class OrderLineItemRelationshipTypeSortBuilder : SqlSortBuilder<OrderLineItemRelationshipTypeColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OrderLineItemRelationshipTypeSqlSortBuilder class.
		/// </summary>
		public OrderLineItemRelationshipTypeSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion OrderLineItemRelationshipTypeSortBuilder
	
} // end namespace
