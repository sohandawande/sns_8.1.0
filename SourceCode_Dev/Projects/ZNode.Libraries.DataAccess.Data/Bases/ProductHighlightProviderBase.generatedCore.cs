﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ProductHighlightProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class ProductHighlightProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.ProductHighlight, ZNode.Libraries.DataAccess.Entities.ProductHighlightKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProductHighlightKey key)
		{
			return Delete(transactionManager, key.ProductHighlightID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_productHighlightID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _productHighlightID)
		{
			return Delete(null, _productHighlightID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productHighlightID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _productHighlightID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.ProductHighlight Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProductHighlightKey key, int start, int pageLength)
		{
			return GetByProductHighlightID(transactionManager, key.ProductHighlightID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_Highlight index.
		/// </summary>
		/// <param name="_highlightID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;ProductHighlight&gt;"/> class.</returns>
		public TList<ProductHighlight> GetByHighlightID(System.Int32 _highlightID)
		{
			int count = -1;
			return GetByHighlightID(null,_highlightID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Highlight index.
		/// </summary>
		/// <param name="_highlightID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ProductHighlight&gt;"/> class.</returns>
		public TList<ProductHighlight> GetByHighlightID(System.Int32 _highlightID, int start, int pageLength)
		{
			int count = -1;
			return GetByHighlightID(null, _highlightID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Highlight index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_highlightID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ProductHighlight&gt;"/> class.</returns>
		public TList<ProductHighlight> GetByHighlightID(TransactionManager transactionManager, System.Int32 _highlightID)
		{
			int count = -1;
			return GetByHighlightID(transactionManager, _highlightID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Highlight index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_highlightID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ProductHighlight&gt;"/> class.</returns>
		public TList<ProductHighlight> GetByHighlightID(TransactionManager transactionManager, System.Int32 _highlightID, int start, int pageLength)
		{
			int count = -1;
			return GetByHighlightID(transactionManager, _highlightID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Highlight index.
		/// </summary>
		/// <param name="_highlightID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ProductHighlight&gt;"/> class.</returns>
		public TList<ProductHighlight> GetByHighlightID(System.Int32 _highlightID, int start, int pageLength, out int count)
		{
			return GetByHighlightID(null, _highlightID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Highlight index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_highlightID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;ProductHighlight&gt;"/> class.</returns>
		public abstract TList<ProductHighlight> GetByHighlightID(TransactionManager transactionManager, System.Int32 _highlightID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_Product index.
		/// </summary>
		/// <param name="_productID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;ProductHighlight&gt;"/> class.</returns>
		public TList<ProductHighlight> GetByProductID(System.Int32 _productID)
		{
			int count = -1;
			return GetByProductID(null,_productID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Product index.
		/// </summary>
		/// <param name="_productID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ProductHighlight&gt;"/> class.</returns>
		public TList<ProductHighlight> GetByProductID(System.Int32 _productID, int start, int pageLength)
		{
			int count = -1;
			return GetByProductID(null, _productID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Product index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ProductHighlight&gt;"/> class.</returns>
		public TList<ProductHighlight> GetByProductID(TransactionManager transactionManager, System.Int32 _productID)
		{
			int count = -1;
			return GetByProductID(transactionManager, _productID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Product index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ProductHighlight&gt;"/> class.</returns>
		public TList<ProductHighlight> GetByProductID(TransactionManager transactionManager, System.Int32 _productID, int start, int pageLength)
		{
			int count = -1;
			return GetByProductID(transactionManager, _productID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Product index.
		/// </summary>
		/// <param name="_productID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ProductHighlight&gt;"/> class.</returns>
		public TList<ProductHighlight> GetByProductID(System.Int32 _productID, int start, int pageLength, out int count)
		{
			return GetByProductID(null, _productID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Product index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;ProductHighlight&gt;"/> class.</returns>
		public abstract TList<ProductHighlight> GetByProductID(TransactionManager transactionManager, System.Int32 _productID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_SC_ProductHighlight index.
		/// </summary>
		/// <param name="_productHighlightID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductHighlight"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductHighlight GetByProductHighlightID(System.Int32 _productHighlightID)
		{
			int count = -1;
			return GetByProductHighlightID(null,_productHighlightID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SC_ProductHighlight index.
		/// </summary>
		/// <param name="_productHighlightID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductHighlight"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductHighlight GetByProductHighlightID(System.Int32 _productHighlightID, int start, int pageLength)
		{
			int count = -1;
			return GetByProductHighlightID(null, _productHighlightID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SC_ProductHighlight index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productHighlightID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductHighlight"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductHighlight GetByProductHighlightID(TransactionManager transactionManager, System.Int32 _productHighlightID)
		{
			int count = -1;
			return GetByProductHighlightID(transactionManager, _productHighlightID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SC_ProductHighlight index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productHighlightID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductHighlight"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductHighlight GetByProductHighlightID(TransactionManager transactionManager, System.Int32 _productHighlightID, int start, int pageLength)
		{
			int count = -1;
			return GetByProductHighlightID(transactionManager, _productHighlightID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SC_ProductHighlight index.
		/// </summary>
		/// <param name="_productHighlightID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductHighlight"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductHighlight GetByProductHighlightID(System.Int32 _productHighlightID, int start, int pageLength, out int count)
		{
			return GetByProductHighlightID(null, _productHighlightID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SC_ProductHighlight index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productHighlightID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductHighlight"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.ProductHighlight GetByProductHighlightID(TransactionManager transactionManager, System.Int32 _productHighlightID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;ProductHighlight&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;ProductHighlight&gt;"/></returns>
		public static TList<ProductHighlight> Fill(IDataReader reader, TList<ProductHighlight> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.ProductHighlight c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("ProductHighlight")
					.Append("|").Append((System.Int32)reader[((int)ProductHighlightColumn.ProductHighlightID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<ProductHighlight>(
					key.ToString(), // EntityTrackingKey
					"ProductHighlight",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.ProductHighlight();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.ProductHighlightID = (System.Int32)reader[((int)ProductHighlightColumn.ProductHighlightID - 1)];
					c.ProductID = (System.Int32)reader[((int)ProductHighlightColumn.ProductID - 1)];
					c.HighlightID = (System.Int32)reader[((int)ProductHighlightColumn.HighlightID - 1)];
					c.DisplayOrder = (reader.IsDBNull(((int)ProductHighlightColumn.DisplayOrder - 1)))?null:(System.Int32?)reader[((int)ProductHighlightColumn.DisplayOrder - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.ProductHighlight"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ProductHighlight"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.ProductHighlight entity)
		{
			if (!reader.Read()) return;
			
			entity.ProductHighlightID = (System.Int32)reader[((int)ProductHighlightColumn.ProductHighlightID - 1)];
			entity.ProductID = (System.Int32)reader[((int)ProductHighlightColumn.ProductID - 1)];
			entity.HighlightID = (System.Int32)reader[((int)ProductHighlightColumn.HighlightID - 1)];
			entity.DisplayOrder = (reader.IsDBNull(((int)ProductHighlightColumn.DisplayOrder - 1)))?null:(System.Int32?)reader[((int)ProductHighlightColumn.DisplayOrder - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.ProductHighlight"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ProductHighlight"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.ProductHighlight entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.ProductHighlightID = (System.Int32)dataRow["ProductHighlightID"];
			entity.ProductID = (System.Int32)dataRow["ProductID"];
			entity.HighlightID = (System.Int32)dataRow["HighlightID"];
			entity.DisplayOrder = Convert.IsDBNull(dataRow["DisplayOrder"]) ? null : (System.Int32?)dataRow["DisplayOrder"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ProductHighlight"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.ProductHighlight Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProductHighlight entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region HighlightIDSource	
			if (CanDeepLoad(entity, "Highlight|HighlightIDSource", deepLoadType, innerList) 
				&& entity.HighlightIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.HighlightID;
				Highlight tmpEntity = EntityManager.LocateEntity<Highlight>(EntityLocator.ConstructKeyFromPkItems(typeof(Highlight), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.HighlightIDSource = tmpEntity;
				else
					entity.HighlightIDSource = DataRepository.HighlightProvider.GetByHighlightID(transactionManager, entity.HighlightID);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'HighlightIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.HighlightIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.HighlightProvider.DeepLoad(transactionManager, entity.HighlightIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion HighlightIDSource

			#region ProductIDSource	
			if (CanDeepLoad(entity, "Product|ProductIDSource", deepLoadType, innerList) 
				&& entity.ProductIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.ProductID;
				Product tmpEntity = EntityManager.LocateEntity<Product>(EntityLocator.ConstructKeyFromPkItems(typeof(Product), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ProductIDSource = tmpEntity;
				else
					entity.ProductIDSource = DataRepository.ProductProvider.GetByProductID(transactionManager, entity.ProductID);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ProductIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ProductIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.ProductProvider.DeepLoad(transactionManager, entity.ProductIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ProductIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.ProductHighlight object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.ProductHighlight instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.ProductHighlight Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProductHighlight entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region HighlightIDSource
			if (CanDeepSave(entity, "Highlight|HighlightIDSource", deepSaveType, innerList) 
				&& entity.HighlightIDSource != null)
			{
				DataRepository.HighlightProvider.Save(transactionManager, entity.HighlightIDSource);
				entity.HighlightID = entity.HighlightIDSource.HighlightID;
			}
			#endregion 
			
			#region ProductIDSource
			if (CanDeepSave(entity, "Product|ProductIDSource", deepSaveType, innerList) 
				&& entity.ProductIDSource != null)
			{
				DataRepository.ProductProvider.Save(transactionManager, entity.ProductIDSource);
				entity.ProductID = entity.ProductIDSource.ProductID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region ProductHighlightChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.ProductHighlight</c>
	///</summary>
	public enum ProductHighlightChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Highlight</c> at HighlightIDSource
		///</summary>
		[ChildEntityType(typeof(Highlight))]
		Highlight,
		
		///<summary>
		/// Composite Property for <c>Product</c> at ProductIDSource
		///</summary>
		[ChildEntityType(typeof(Product))]
		Product,
	}
	
	#endregion ProductHighlightChildEntityTypes
	
	#region ProductHighlightFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;ProductHighlightColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ProductHighlight"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductHighlightFilterBuilder : SqlFilterBuilder<ProductHighlightColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductHighlightFilterBuilder class.
		/// </summary>
		public ProductHighlightFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductHighlightFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductHighlightFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductHighlightFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductHighlightFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductHighlightFilterBuilder
	
	#region ProductHighlightParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;ProductHighlightColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ProductHighlight"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductHighlightParameterBuilder : ParameterizedSqlFilterBuilder<ProductHighlightColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductHighlightParameterBuilder class.
		/// </summary>
		public ProductHighlightParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductHighlightParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductHighlightParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductHighlightParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductHighlightParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductHighlightParameterBuilder
	
	#region ProductHighlightSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;ProductHighlightColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ProductHighlight"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ProductHighlightSortBuilder : SqlSortBuilder<ProductHighlightColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductHighlightSqlSortBuilder class.
		/// </summary>
		public ProductHighlightSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ProductHighlightSortBuilder
	
} // end namespace
