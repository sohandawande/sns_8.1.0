﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="AddOnProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class AddOnProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.AddOn, ZNode.Libraries.DataAccess.Entities.AddOnKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.AddOnKey key)
		{
			return Delete(transactionManager, key.AddOnID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_addOnID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _addOnID)
		{
			return Delete(null, _addOnID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_addOnID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _addOnID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAddOn_ZNodeAccount key.
		///		FK_ZNodeAddOn_ZNodeAccount Description: 
		/// </summary>
		/// <param name="_accountID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AddOn objects.</returns>
		public TList<AddOn> GetByAccountID(System.Int32? _accountID)
		{
			int count = -1;
			return GetByAccountID(_accountID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAddOn_ZNodeAccount key.
		///		FK_ZNodeAddOn_ZNodeAccount Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AddOn objects.</returns>
		/// <remarks></remarks>
		public TList<AddOn> GetByAccountID(TransactionManager transactionManager, System.Int32? _accountID)
		{
			int count = -1;
			return GetByAccountID(transactionManager, _accountID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAddOn_ZNodeAccount key.
		///		FK_ZNodeAddOn_ZNodeAccount Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AddOn objects.</returns>
		public TList<AddOn> GetByAccountID(TransactionManager transactionManager, System.Int32? _accountID, int start, int pageLength)
		{
			int count = -1;
			return GetByAccountID(transactionManager, _accountID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAddOn_ZNodeAccount key.
		///		fKZNodeAddOnZNodeAccount Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_accountID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AddOn objects.</returns>
		public TList<AddOn> GetByAccountID(System.Int32? _accountID, int start, int pageLength)
		{
			int count =  -1;
			return GetByAccountID(null, _accountID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAddOn_ZNodeAccount key.
		///		fKZNodeAddOnZNodeAccount Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_accountID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AddOn objects.</returns>
		public TList<AddOn> GetByAccountID(System.Int32? _accountID, int start, int pageLength,out int count)
		{
			return GetByAccountID(null, _accountID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAddOn_ZNodeAccount key.
		///		FK_ZNodeAddOn_ZNodeAccount Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AddOn objects.</returns>
		public abstract TList<AddOn> GetByAccountID(TransactionManager transactionManager, System.Int32? _accountID, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAddOn_ZNodeLocale key.
		///		FK_ZNodeAddOn_ZNodeLocale Description: 
		/// </summary>
		/// <param name="_localeId"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AddOn objects.</returns>
		public TList<AddOn> GetByLocaleId(System.Int32 _localeId)
		{
			int count = -1;
			return GetByLocaleId(_localeId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAddOn_ZNodeLocale key.
		///		FK_ZNodeAddOn_ZNodeLocale Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_localeId"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AddOn objects.</returns>
		/// <remarks></remarks>
		public TList<AddOn> GetByLocaleId(TransactionManager transactionManager, System.Int32 _localeId)
		{
			int count = -1;
			return GetByLocaleId(transactionManager, _localeId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAddOn_ZNodeLocale key.
		///		FK_ZNodeAddOn_ZNodeLocale Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_localeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AddOn objects.</returns>
		public TList<AddOn> GetByLocaleId(TransactionManager transactionManager, System.Int32 _localeId, int start, int pageLength)
		{
			int count = -1;
			return GetByLocaleId(transactionManager, _localeId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAddOn_ZNodeLocale key.
		///		fKZNodeAddOnZNodeLocale Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_localeId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AddOn objects.</returns>
		public TList<AddOn> GetByLocaleId(System.Int32 _localeId, int start, int pageLength)
		{
			int count =  -1;
			return GetByLocaleId(null, _localeId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAddOn_ZNodeLocale key.
		///		fKZNodeAddOnZNodeLocale Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_localeId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AddOn objects.</returns>
		public TList<AddOn> GetByLocaleId(System.Int32 _localeId, int start, int pageLength,out int count)
		{
			return GetByLocaleId(null, _localeId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAddOn_ZNodeLocale key.
		///		FK_ZNodeAddOn_ZNodeLocale Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_localeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AddOn objects.</returns>
		public abstract TList<AddOn> GetByLocaleId(TransactionManager transactionManager, System.Int32 _localeId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAddOn_ZNodePortal key.
		///		FK_ZNodeAddOn_ZNodePortal Description: 
		/// </summary>
		/// <param name="_portalID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AddOn objects.</returns>
		public TList<AddOn> GetByPortalID(System.Int32? _portalID)
		{
			int count = -1;
			return GetByPortalID(_portalID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAddOn_ZNodePortal key.
		///		FK_ZNodeAddOn_ZNodePortal Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AddOn objects.</returns>
		/// <remarks></remarks>
		public TList<AddOn> GetByPortalID(TransactionManager transactionManager, System.Int32? _portalID)
		{
			int count = -1;
			return GetByPortalID(transactionManager, _portalID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAddOn_ZNodePortal key.
		///		FK_ZNodeAddOn_ZNodePortal Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AddOn objects.</returns>
		public TList<AddOn> GetByPortalID(TransactionManager transactionManager, System.Int32? _portalID, int start, int pageLength)
		{
			int count = -1;
			return GetByPortalID(transactionManager, _portalID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAddOn_ZNodePortal key.
		///		fKZNodeAddOnZNodePortal Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_portalID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AddOn objects.</returns>
		public TList<AddOn> GetByPortalID(System.Int32? _portalID, int start, int pageLength)
		{
			int count =  -1;
			return GetByPortalID(null, _portalID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAddOn_ZNodePortal key.
		///		fKZNodeAddOnZNodePortal Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_portalID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AddOn objects.</returns>
		public TList<AddOn> GetByPortalID(System.Int32? _portalID, int start, int pageLength,out int count)
		{
			return GetByPortalID(null, _portalID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeAddOn_ZNodePortal key.
		///		FK_ZNodeAddOn_ZNodePortal Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AddOn objects.</returns>
		public abstract TList<AddOn> GetByPortalID(TransactionManager transactionManager, System.Int32? _portalID, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.AddOn Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.AddOnKey key, int start, int pageLength)
		{
			return GetByAddOnID(transactionManager, key.AddOnID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeAddOn index.
		/// </summary>
		/// <param name="_externalID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;AddOn&gt;"/> class.</returns>
		public TList<AddOn> GetByExternalID(System.Int32? _externalID)
		{
			int count = -1;
			return GetByExternalID(null,_externalID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAddOn index.
		/// </summary>
		/// <param name="_externalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;AddOn&gt;"/> class.</returns>
		public TList<AddOn> GetByExternalID(System.Int32? _externalID, int start, int pageLength)
		{
			int count = -1;
			return GetByExternalID(null, _externalID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAddOn index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_externalID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;AddOn&gt;"/> class.</returns>
		public TList<AddOn> GetByExternalID(TransactionManager transactionManager, System.Int32? _externalID)
		{
			int count = -1;
			return GetByExternalID(transactionManager, _externalID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAddOn index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_externalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;AddOn&gt;"/> class.</returns>
		public TList<AddOn> GetByExternalID(TransactionManager transactionManager, System.Int32? _externalID, int start, int pageLength)
		{
			int count = -1;
			return GetByExternalID(transactionManager, _externalID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAddOn index.
		/// </summary>
		/// <param name="_externalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;AddOn&gt;"/> class.</returns>
		public TList<AddOn> GetByExternalID(System.Int32? _externalID, int start, int pageLength, out int count)
		{
			return GetByExternalID(null, _externalID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAddOn index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_externalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;AddOn&gt;"/> class.</returns>
		public abstract TList<AddOn> GetByExternalID(TransactionManager transactionManager, System.Int32? _externalID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodeProductAddOn index.
		/// </summary>
		/// <param name="_addOnID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.AddOn"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.AddOn GetByAddOnID(System.Int32 _addOnID)
		{
			int count = -1;
			return GetByAddOnID(null,_addOnID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductAddOn index.
		/// </summary>
		/// <param name="_addOnID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.AddOn"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.AddOn GetByAddOnID(System.Int32 _addOnID, int start, int pageLength)
		{
			int count = -1;
			return GetByAddOnID(null, _addOnID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductAddOn index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_addOnID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.AddOn"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.AddOn GetByAddOnID(TransactionManager transactionManager, System.Int32 _addOnID)
		{
			int count = -1;
			return GetByAddOnID(transactionManager, _addOnID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductAddOn index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_addOnID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.AddOn"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.AddOn GetByAddOnID(TransactionManager transactionManager, System.Int32 _addOnID, int start, int pageLength)
		{
			int count = -1;
			return GetByAddOnID(transactionManager, _addOnID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductAddOn index.
		/// </summary>
		/// <param name="_addOnID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.AddOn"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.AddOn GetByAddOnID(System.Int32 _addOnID, int start, int pageLength, out int count)
		{
			return GetByAddOnID(null, _addOnID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductAddOn index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_addOnID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.AddOn"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.AddOn GetByAddOnID(TransactionManager transactionManager, System.Int32 _addOnID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;AddOn&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;AddOn&gt;"/></returns>
		public static TList<AddOn> Fill(IDataReader reader, TList<AddOn> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.AddOn c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("AddOn")
					.Append("|").Append((System.Int32)reader[((int)AddOnColumn.AddOnID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<AddOn>(
					key.ToString(), // EntityTrackingKey
					"AddOn",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.AddOn();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.AddOnID = (System.Int32)reader[((int)AddOnColumn.AddOnID - 1)];
					c.ProductID = (System.Int32)reader[((int)AddOnColumn.ProductID - 1)];
					c.Title = (System.String)reader[((int)AddOnColumn.Title - 1)];
					c.Name = (System.String)reader[((int)AddOnColumn.Name - 1)];
					c.Description = (System.String)reader[((int)AddOnColumn.Description - 1)];
					c.DisplayOrder = (System.Int32)reader[((int)AddOnColumn.DisplayOrder - 1)];
					c.DisplayType = (System.String)reader[((int)AddOnColumn.DisplayType - 1)];
					c.OptionalInd = (System.Boolean)reader[((int)AddOnColumn.OptionalInd - 1)];
					c.AllowBackOrder = (System.Boolean)reader[((int)AddOnColumn.AllowBackOrder - 1)];
					c.InStockMsg = (System.String)reader[((int)AddOnColumn.InStockMsg - 1)];
					c.OutOfStockMsg = (System.String)reader[((int)AddOnColumn.OutOfStockMsg - 1)];
					c.BackOrderMsg = (System.String)reader[((int)AddOnColumn.BackOrderMsg - 1)];
					c.PromptMsg = (System.String)reader[((int)AddOnColumn.PromptMsg - 1)];
					c.TrackInventoryInd = (System.Boolean)reader[((int)AddOnColumn.TrackInventoryInd - 1)];
					c.LocaleId = (System.Int32)reader[((int)AddOnColumn.LocaleId - 1)];
					c.ExternalID = (reader.IsDBNull(((int)AddOnColumn.ExternalID - 1)))?null:(System.Int32?)reader[((int)AddOnColumn.ExternalID - 1)];
					c.ExternalAPIID = (reader.IsDBNull(((int)AddOnColumn.ExternalAPIID - 1)))?null:(System.String)reader[((int)AddOnColumn.ExternalAPIID - 1)];
					c.AccountID = (reader.IsDBNull(((int)AddOnColumn.AccountID - 1)))?null:(System.Int32?)reader[((int)AddOnColumn.AccountID - 1)];
					c.PortalID = (reader.IsDBNull(((int)AddOnColumn.PortalID - 1)))?null:(System.Int32?)reader[((int)AddOnColumn.PortalID - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.AddOn"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.AddOn"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.AddOn entity)
		{
			if (!reader.Read()) return;
			
			entity.AddOnID = (System.Int32)reader[((int)AddOnColumn.AddOnID - 1)];
			entity.ProductID = (System.Int32)reader[((int)AddOnColumn.ProductID - 1)];
			entity.Title = (System.String)reader[((int)AddOnColumn.Title - 1)];
			entity.Name = (System.String)reader[((int)AddOnColumn.Name - 1)];
			entity.Description = (System.String)reader[((int)AddOnColumn.Description - 1)];
			entity.DisplayOrder = (System.Int32)reader[((int)AddOnColumn.DisplayOrder - 1)];
			entity.DisplayType = (System.String)reader[((int)AddOnColumn.DisplayType - 1)];
			entity.OptionalInd = (System.Boolean)reader[((int)AddOnColumn.OptionalInd - 1)];
			entity.AllowBackOrder = (System.Boolean)reader[((int)AddOnColumn.AllowBackOrder - 1)];
			entity.InStockMsg = (System.String)reader[((int)AddOnColumn.InStockMsg - 1)];
			entity.OutOfStockMsg = (System.String)reader[((int)AddOnColumn.OutOfStockMsg - 1)];
			entity.BackOrderMsg = (System.String)reader[((int)AddOnColumn.BackOrderMsg - 1)];
			entity.PromptMsg = (System.String)reader[((int)AddOnColumn.PromptMsg - 1)];
			entity.TrackInventoryInd = (System.Boolean)reader[((int)AddOnColumn.TrackInventoryInd - 1)];
			entity.LocaleId = (System.Int32)reader[((int)AddOnColumn.LocaleId - 1)];
			entity.ExternalID = (reader.IsDBNull(((int)AddOnColumn.ExternalID - 1)))?null:(System.Int32?)reader[((int)AddOnColumn.ExternalID - 1)];
			entity.ExternalAPIID = (reader.IsDBNull(((int)AddOnColumn.ExternalAPIID - 1)))?null:(System.String)reader[((int)AddOnColumn.ExternalAPIID - 1)];
			entity.AccountID = (reader.IsDBNull(((int)AddOnColumn.AccountID - 1)))?null:(System.Int32?)reader[((int)AddOnColumn.AccountID - 1)];
			entity.PortalID = (reader.IsDBNull(((int)AddOnColumn.PortalID - 1)))?null:(System.Int32?)reader[((int)AddOnColumn.PortalID - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.AddOn"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.AddOn"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.AddOn entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.AddOnID = (System.Int32)dataRow["AddOnID"];
			entity.ProductID = (System.Int32)dataRow["ProductID"];
			entity.Title = (System.String)dataRow["Title"];
			entity.Name = (System.String)dataRow["Name"];
			entity.Description = (System.String)dataRow["Description"];
			entity.DisplayOrder = (System.Int32)dataRow["DisplayOrder"];
			entity.DisplayType = (System.String)dataRow["DisplayType"];
			entity.OptionalInd = (System.Boolean)dataRow["OptionalInd"];
			entity.AllowBackOrder = (System.Boolean)dataRow["AllowBackOrder"];
			entity.InStockMsg = (System.String)dataRow["InStockMsg"];
			entity.OutOfStockMsg = (System.String)dataRow["OutOfStockMsg"];
			entity.BackOrderMsg = (System.String)dataRow["BackOrderMsg"];
			entity.PromptMsg = (System.String)dataRow["PromptMsg"];
			entity.TrackInventoryInd = (System.Boolean)dataRow["TrackInventoryInd"];
			entity.LocaleId = (System.Int32)dataRow["LocaleId"];
			entity.ExternalID = Convert.IsDBNull(dataRow["ExternalID"]) ? null : (System.Int32?)dataRow["ExternalID"];
			entity.ExternalAPIID = Convert.IsDBNull(dataRow["ExternalAPIID"]) ? null : (System.String)dataRow["ExternalAPIID"];
			entity.AccountID = Convert.IsDBNull(dataRow["AccountID"]) ? null : (System.Int32?)dataRow["AccountID"];
			entity.PortalID = Convert.IsDBNull(dataRow["PortalID"]) ? null : (System.Int32?)dataRow["PortalID"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.AddOn"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.AddOn Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.AddOn entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region AccountIDSource	
			if (CanDeepLoad(entity, "Account|AccountIDSource", deepLoadType, innerList) 
				&& entity.AccountIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.AccountID ?? (int)0);
				Account tmpEntity = EntityManager.LocateEntity<Account>(EntityLocator.ConstructKeyFromPkItems(typeof(Account), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.AccountIDSource = tmpEntity;
				else
					entity.AccountIDSource = DataRepository.AccountProvider.GetByAccountID(transactionManager, (entity.AccountID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AccountIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.AccountIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.AccountProvider.DeepLoad(transactionManager, entity.AccountIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion AccountIDSource

			#region LocaleIdSource	
			if (CanDeepLoad(entity, "Locale|LocaleIdSource", deepLoadType, innerList) 
				&& entity.LocaleIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.LocaleId;
				Locale tmpEntity = EntityManager.LocateEntity<Locale>(EntityLocator.ConstructKeyFromPkItems(typeof(Locale), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.LocaleIdSource = tmpEntity;
				else
					entity.LocaleIdSource = DataRepository.LocaleProvider.GetByLocaleID(transactionManager, entity.LocaleId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'LocaleIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.LocaleIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.LocaleProvider.DeepLoad(transactionManager, entity.LocaleIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion LocaleIdSource

			#region PortalIDSource	
			if (CanDeepLoad(entity, "Portal|PortalIDSource", deepLoadType, innerList) 
				&& entity.PortalIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.PortalID ?? (int)0);
				Portal tmpEntity = EntityManager.LocateEntity<Portal>(EntityLocator.ConstructKeyFromPkItems(typeof(Portal), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.PortalIDSource = tmpEntity;
				else
					entity.PortalIDSource = DataRepository.PortalProvider.GetByPortalID(transactionManager, (entity.PortalID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PortalIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.PortalIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.PortalProvider.DeepLoad(transactionManager, entity.PortalIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion PortalIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByAddOnID methods when available
			
			#region AddOnValueCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<AddOnValue>|AddOnValueCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AddOnValueCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.AddOnValueCollection = DataRepository.AddOnValueProvider.GetByAddOnID(transactionManager, entity.AddOnID);

				if (deep && entity.AddOnValueCollection.Count > 0)
				{
					deepHandles.Add("AddOnValueCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<AddOnValue>) DataRepository.AddOnValueProvider.DeepLoad,
						new object[] { transactionManager, entity.AddOnValueCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region ProductAddOnCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<ProductAddOn>|ProductAddOnCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ProductAddOnCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ProductAddOnCollection = DataRepository.ProductAddOnProvider.GetByAddOnID(transactionManager, entity.AddOnID);

				if (deep && entity.ProductAddOnCollection.Count > 0)
				{
					deepHandles.Add("ProductAddOnCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<ProductAddOn>) DataRepository.ProductAddOnProvider.DeepLoad,
						new object[] { transactionManager, entity.ProductAddOnCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.AddOn object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.AddOn instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.AddOn Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.AddOn entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region AccountIDSource
			if (CanDeepSave(entity, "Account|AccountIDSource", deepSaveType, innerList) 
				&& entity.AccountIDSource != null)
			{
				DataRepository.AccountProvider.Save(transactionManager, entity.AccountIDSource);
				entity.AccountID = entity.AccountIDSource.AccountID;
			}
			#endregion 
			
			#region LocaleIdSource
			if (CanDeepSave(entity, "Locale|LocaleIdSource", deepSaveType, innerList) 
				&& entity.LocaleIdSource != null)
			{
				DataRepository.LocaleProvider.Save(transactionManager, entity.LocaleIdSource);
				entity.LocaleId = entity.LocaleIdSource.LocaleID;
			}
			#endregion 
			
			#region PortalIDSource
			if (CanDeepSave(entity, "Portal|PortalIDSource", deepSaveType, innerList) 
				&& entity.PortalIDSource != null)
			{
				DataRepository.PortalProvider.Save(transactionManager, entity.PortalIDSource);
				entity.PortalID = entity.PortalIDSource.PortalID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<AddOnValue>
				if (CanDeepSave(entity.AddOnValueCollection, "List<AddOnValue>|AddOnValueCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(AddOnValue child in entity.AddOnValueCollection)
					{
						if(child.AddOnIDSource != null)
						{
							child.AddOnID = child.AddOnIDSource.AddOnID;
						}
						else
						{
							child.AddOnID = entity.AddOnID;
						}

					}

					if (entity.AddOnValueCollection.Count > 0 || entity.AddOnValueCollection.DeletedItems.Count > 0)
					{
						//DataRepository.AddOnValueProvider.Save(transactionManager, entity.AddOnValueCollection);
						
						deepHandles.Add("AddOnValueCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< AddOnValue >) DataRepository.AddOnValueProvider.DeepSave,
							new object[] { transactionManager, entity.AddOnValueCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<ProductAddOn>
				if (CanDeepSave(entity.ProductAddOnCollection, "List<ProductAddOn>|ProductAddOnCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(ProductAddOn child in entity.ProductAddOnCollection)
					{
						if(child.AddOnIDSource != null)
						{
							child.AddOnID = child.AddOnIDSource.AddOnID;
						}
						else
						{
							child.AddOnID = entity.AddOnID;
						}

					}

					if (entity.ProductAddOnCollection.Count > 0 || entity.ProductAddOnCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ProductAddOnProvider.Save(transactionManager, entity.ProductAddOnCollection);
						
						deepHandles.Add("ProductAddOnCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< ProductAddOn >) DataRepository.ProductAddOnProvider.DeepSave,
							new object[] { transactionManager, entity.ProductAddOnCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region AddOnChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.AddOn</c>
	///</summary>
	public enum AddOnChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Account</c> at AccountIDSource
		///</summary>
		[ChildEntityType(typeof(Account))]
		Account,
		
		///<summary>
		/// Composite Property for <c>Locale</c> at LocaleIdSource
		///</summary>
		[ChildEntityType(typeof(Locale))]
		Locale,
		
		///<summary>
		/// Composite Property for <c>Portal</c> at PortalIDSource
		///</summary>
		[ChildEntityType(typeof(Portal))]
		Portal,
		///<summary>
		/// Collection of <c>AddOn</c> as OneToMany for AddOnValueCollection
		///</summary>
		[ChildEntityType(typeof(TList<AddOnValue>))]
		AddOnValueCollection,
		///<summary>
		/// Collection of <c>AddOn</c> as OneToMany for ProductAddOnCollection
		///</summary>
		[ChildEntityType(typeof(TList<ProductAddOn>))]
		ProductAddOnCollection,
	}
	
	#endregion AddOnChildEntityTypes
	
	#region AddOnFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;AddOnColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AddOn"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AddOnFilterBuilder : SqlFilterBuilder<AddOnColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AddOnFilterBuilder class.
		/// </summary>
		public AddOnFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the AddOnFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AddOnFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AddOnFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AddOnFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AddOnFilterBuilder
	
	#region AddOnParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;AddOnColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AddOn"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AddOnParameterBuilder : ParameterizedSqlFilterBuilder<AddOnColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AddOnParameterBuilder class.
		/// </summary>
		public AddOnParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the AddOnParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AddOnParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AddOnParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AddOnParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AddOnParameterBuilder
	
	#region AddOnSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;AddOnColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AddOn"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class AddOnSortBuilder : SqlSortBuilder<AddOnColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AddOnSqlSortBuilder class.
		/// </summary>
		public AddOnSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion AddOnSortBuilder
	
} // end namespace
