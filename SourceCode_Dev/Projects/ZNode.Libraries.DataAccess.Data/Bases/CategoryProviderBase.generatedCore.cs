﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="CategoryProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class CategoryProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.Category, ZNode.Libraries.DataAccess.Entities.CategoryKey>
	{		
		#region Get from Many To Many Relationship Functions
		#region GetByFacetGroupIDFromFacetGroupCategory
		
		/// <summary>
		///		Gets ZNodeCategory objects from the datasource by FacetGroupID in the
		///		ZNodeFacetGroupCategory table. Table ZNodeCategory is related to table ZNodeFacetGroup
		///		through the (M:N) relationship defined in the ZNodeFacetGroupCategory table.
		/// </summary>
		/// <param name="_facetGroupID"></param>
		/// <returns>Returns a typed collection of Category objects.</returns>
		public TList<Category> GetByFacetGroupIDFromFacetGroupCategory(System.Int32 _facetGroupID)
		{
			int count = -1;
			return GetByFacetGroupIDFromFacetGroupCategory(null,_facetGroupID, 0, int.MaxValue, out count);
			
		}
		
		/// <summary>
		///		Gets ZNode.Libraries.DataAccess.Entities.Category objects from the datasource by FacetGroupID in the
		///		ZNodeFacetGroupCategory table. Table ZNodeCategory is related to table ZNodeFacetGroup
		///		through the (M:N) relationship defined in the ZNodeFacetGroupCategory table.
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_facetGroupID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a TList of Category objects.</returns>
		public TList<Category> GetByFacetGroupIDFromFacetGroupCategory(System.Int32 _facetGroupID, int start, int pageLength)
		{
			int count = -1;
			return GetByFacetGroupIDFromFacetGroupCategory(null, _facetGroupID, start, pageLength, out count);
		}
		
		/// <summary>
		///		Gets Category objects from the datasource by FacetGroupID in the
		///		ZNodeFacetGroupCategory table. Table ZNodeCategory is related to table ZNodeFacetGroup
		///		through the (M:N) relationship defined in the ZNodeFacetGroupCategory table.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_facetGroupID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNodeCategory objects.</returns>
		public TList<Category> GetByFacetGroupIDFromFacetGroupCategory(TransactionManager transactionManager, System.Int32 _facetGroupID)
		{
			int count = -1;
			return GetByFacetGroupIDFromFacetGroupCategory(transactionManager, _facetGroupID, 0, int.MaxValue, out count);
		}
		
		
		/// <summary>
		///		Gets Category objects from the datasource by FacetGroupID in the
		///		ZNodeFacetGroupCategory table. Table ZNodeCategory is related to table ZNodeFacetGroup
		///		through the (M:N) relationship defined in the ZNodeFacetGroupCategory table.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_facetGroupID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNodeCategory objects.</returns>
		public TList<Category> GetByFacetGroupIDFromFacetGroupCategory(TransactionManager transactionManager, System.Int32 _facetGroupID,int start, int pageLength)
		{
			int count = -1;
			return GetByFacetGroupIDFromFacetGroupCategory(transactionManager, _facetGroupID, start, pageLength, out count);
		}
		
		/// <summary>
		///		Gets Category objects from the datasource by FacetGroupID in the
		///		ZNodeFacetGroupCategory table. Table ZNodeCategory is related to table ZNodeFacetGroup
		///		through the (M:N) relationship defined in the ZNodeFacetGroupCategory table.
		/// </summary>
		/// <param name="_facetGroupID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNodeCategory objects.</returns>
		public TList<Category> GetByFacetGroupIDFromFacetGroupCategory(System.Int32 _facetGroupID,int start, int pageLength, out int count)
		{
			
			return GetByFacetGroupIDFromFacetGroupCategory(null, _facetGroupID, start, pageLength, out count);
		}


		/// <summary>
		///		Gets ZNodeCategory objects from the datasource by FacetGroupID in the
		///		ZNodeFacetGroupCategory table. Table ZNodeCategory is related to table ZNodeFacetGroup
		///		through the (M:N) relationship defined in the ZNodeFacetGroupCategory table.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <param name="_facetGroupID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a TList of Category objects.</returns>
		public abstract TList<Category> GetByFacetGroupIDFromFacetGroupCategory(TransactionManager transactionManager,System.Int32 _facetGroupID, int start, int pageLength, out int count);
		
		#endregion GetByFacetGroupIDFromFacetGroupCategory
		
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.CategoryKey key)
		{
			return Delete(transactionManager, key.CategoryID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_categoryID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _categoryID)
		{
			return Delete(null, _categoryID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_categoryID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _categoryID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.Category Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.CategoryKey key, int start, int pageLength)
		{
			return GetByCategoryID(transactionManager, key.CategoryID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeCategory_ExternalID index.
		/// </summary>
		/// <param name="_externalID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Category&gt;"/> class.</returns>
		public TList<Category> GetByExternalID(System.String _externalID)
		{
			int count = -1;
			return GetByExternalID(null,_externalID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCategory_ExternalID index.
		/// </summary>
		/// <param name="_externalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Category&gt;"/> class.</returns>
		public TList<Category> GetByExternalID(System.String _externalID, int start, int pageLength)
		{
			int count = -1;
			return GetByExternalID(null, _externalID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCategory_ExternalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_externalID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Category&gt;"/> class.</returns>
		public TList<Category> GetByExternalID(TransactionManager transactionManager, System.String _externalID)
		{
			int count = -1;
			return GetByExternalID(transactionManager, _externalID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCategory_ExternalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_externalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Category&gt;"/> class.</returns>
		public TList<Category> GetByExternalID(TransactionManager transactionManager, System.String _externalID, int start, int pageLength)
		{
			int count = -1;
			return GetByExternalID(transactionManager, _externalID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCategory_ExternalID index.
		/// </summary>
		/// <param name="_externalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Category&gt;"/> class.</returns>
		public TList<Category> GetByExternalID(System.String _externalID, int start, int pageLength, out int count)
		{
			return GetByExternalID(null, _externalID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCategory_ExternalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_externalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Category&gt;"/> class.</returns>
		public abstract TList<Category> GetByExternalID(TransactionManager transactionManager, System.String _externalID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeCategory_PortalID index.
		/// </summary>
		/// <param name="_portalID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Category&gt;"/> class.</returns>
		public TList<Category> GetByPortalID(System.Int32? _portalID)
		{
			int count = -1;
			return GetByPortalID(null,_portalID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCategory_PortalID index.
		/// </summary>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Category&gt;"/> class.</returns>
		public TList<Category> GetByPortalID(System.Int32? _portalID, int start, int pageLength)
		{
			int count = -1;
			return GetByPortalID(null, _portalID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCategory_PortalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Category&gt;"/> class.</returns>
		public TList<Category> GetByPortalID(TransactionManager transactionManager, System.Int32? _portalID)
		{
			int count = -1;
			return GetByPortalID(transactionManager, _portalID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCategory_PortalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Category&gt;"/> class.</returns>
		public TList<Category> GetByPortalID(TransactionManager transactionManager, System.Int32? _portalID, int start, int pageLength)
		{
			int count = -1;
			return GetByPortalID(transactionManager, _portalID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCategory_PortalID index.
		/// </summary>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Category&gt;"/> class.</returns>
		public TList<Category> GetByPortalID(System.Int32? _portalID, int start, int pageLength, out int count)
		{
			return GetByPortalID(null, _portalID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCategory_PortalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Category&gt;"/> class.</returns>
		public abstract TList<Category> GetByPortalID(TransactionManager transactionManager, System.Int32? _portalID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeCategory_SEOURL index.
		/// </summary>
		/// <param name="_sEOURL"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Category&gt;"/> class.</returns>
		public TList<Category> GetBySEOURL(System.String _sEOURL)
		{
			int count = -1;
			return GetBySEOURL(null,_sEOURL, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCategory_SEOURL index.
		/// </summary>
		/// <param name="_sEOURL"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Category&gt;"/> class.</returns>
		public TList<Category> GetBySEOURL(System.String _sEOURL, int start, int pageLength)
		{
			int count = -1;
			return GetBySEOURL(null, _sEOURL, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCategory_SEOURL index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sEOURL"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Category&gt;"/> class.</returns>
		public TList<Category> GetBySEOURL(TransactionManager transactionManager, System.String _sEOURL)
		{
			int count = -1;
			return GetBySEOURL(transactionManager, _sEOURL, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCategory_SEOURL index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sEOURL"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Category&gt;"/> class.</returns>
		public TList<Category> GetBySEOURL(TransactionManager transactionManager, System.String _sEOURL, int start, int pageLength)
		{
			int count = -1;
			return GetBySEOURL(transactionManager, _sEOURL, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCategory_SEOURL index.
		/// </summary>
		/// <param name="_sEOURL"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Category&gt;"/> class.</returns>
		public TList<Category> GetBySEOURL(System.String _sEOURL, int start, int pageLength, out int count)
		{
			return GetBySEOURL(null, _sEOURL, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCategory_SEOURL index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sEOURL"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Category&gt;"/> class.</returns>
		public abstract TList<Category> GetBySEOURL(TransactionManager transactionManager, System.String _sEOURL, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeCategory_VisibleInd index.
		/// </summary>
		/// <param name="_visibleInd"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Category&gt;"/> class.</returns>
		public TList<Category> GetByVisibleInd(System.Boolean _visibleInd)
		{
			int count = -1;
			return GetByVisibleInd(null,_visibleInd, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCategory_VisibleInd index.
		/// </summary>
		/// <param name="_visibleInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Category&gt;"/> class.</returns>
		public TList<Category> GetByVisibleInd(System.Boolean _visibleInd, int start, int pageLength)
		{
			int count = -1;
			return GetByVisibleInd(null, _visibleInd, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCategory_VisibleInd index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_visibleInd"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Category&gt;"/> class.</returns>
		public TList<Category> GetByVisibleInd(TransactionManager transactionManager, System.Boolean _visibleInd)
		{
			int count = -1;
			return GetByVisibleInd(transactionManager, _visibleInd, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCategory_VisibleInd index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_visibleInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Category&gt;"/> class.</returns>
		public TList<Category> GetByVisibleInd(TransactionManager transactionManager, System.Boolean _visibleInd, int start, int pageLength)
		{
			int count = -1;
			return GetByVisibleInd(transactionManager, _visibleInd, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCategory_VisibleInd index.
		/// </summary>
		/// <param name="_visibleInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Category&gt;"/> class.</returns>
		public TList<Category> GetByVisibleInd(System.Boolean _visibleInd, int start, int pageLength, out int count)
		{
			return GetByVisibleInd(null, _visibleInd, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCategory_VisibleInd index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_visibleInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Category&gt;"/> class.</returns>
		public abstract TList<Category> GetByVisibleInd(TransactionManager transactionManager, System.Boolean _visibleInd, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key SC_Category_PK index.
		/// </summary>
		/// <param name="_categoryID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Category"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Category GetByCategoryID(System.Int32 _categoryID)
		{
			int count = -1;
			return GetByCategoryID(null,_categoryID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the SC_Category_PK index.
		/// </summary>
		/// <param name="_categoryID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Category"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Category GetByCategoryID(System.Int32 _categoryID, int start, int pageLength)
		{
			int count = -1;
			return GetByCategoryID(null, _categoryID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the SC_Category_PK index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_categoryID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Category"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Category GetByCategoryID(TransactionManager transactionManager, System.Int32 _categoryID)
		{
			int count = -1;
			return GetByCategoryID(transactionManager, _categoryID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the SC_Category_PK index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_categoryID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Category"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Category GetByCategoryID(TransactionManager transactionManager, System.Int32 _categoryID, int start, int pageLength)
		{
			int count = -1;
			return GetByCategoryID(transactionManager, _categoryID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the SC_Category_PK index.
		/// </summary>
		/// <param name="_categoryID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Category"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Category GetByCategoryID(System.Int32 _categoryID, int start, int pageLength, out int count)
		{
			return GetByCategoryID(null, _categoryID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the SC_Category_PK index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_categoryID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Category"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.Category GetByCategoryID(TransactionManager transactionManager, System.Int32 _categoryID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;Category&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;Category&gt;"/></returns>
		public static TList<Category> Fill(IDataReader reader, TList<Category> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.Category c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("Category")
					.Append("|").Append((System.Int32)reader[((int)CategoryColumn.CategoryID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<Category>(
					key.ToString(), // EntityTrackingKey
					"Category",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.Category();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.CategoryID = (System.Int32)reader[((int)CategoryColumn.CategoryID - 1)];
					c.Name = (System.String)reader[((int)CategoryColumn.Name - 1)];
					c.Title = (reader.IsDBNull(((int)CategoryColumn.Title - 1)))?null:(System.String)reader[((int)CategoryColumn.Title - 1)];
					c.ShortDescription = (reader.IsDBNull(((int)CategoryColumn.ShortDescription - 1)))?null:(System.String)reader[((int)CategoryColumn.ShortDescription - 1)];
					c.Description = (reader.IsDBNull(((int)CategoryColumn.Description - 1)))?null:(System.String)reader[((int)CategoryColumn.Description - 1)];
					c.ImageFile = (reader.IsDBNull(((int)CategoryColumn.ImageFile - 1)))?null:(System.String)reader[((int)CategoryColumn.ImageFile - 1)];
					c.ImageAltTag = (reader.IsDBNull(((int)CategoryColumn.ImageAltTag - 1)))?null:(System.String)reader[((int)CategoryColumn.ImageAltTag - 1)];
					c.VisibleInd = (System.Boolean)reader[((int)CategoryColumn.VisibleInd - 1)];
					c.SubCategoryGridVisibleInd = (System.Boolean)reader[((int)CategoryColumn.SubCategoryGridVisibleInd - 1)];
					c.SEOTitle = (reader.IsDBNull(((int)CategoryColumn.SEOTitle - 1)))?null:(System.String)reader[((int)CategoryColumn.SEOTitle - 1)];
					c.SEOKeywords = (reader.IsDBNull(((int)CategoryColumn.SEOKeywords - 1)))?null:(System.String)reader[((int)CategoryColumn.SEOKeywords - 1)];
					c.SEODescription = (reader.IsDBNull(((int)CategoryColumn.SEODescription - 1)))?null:(System.String)reader[((int)CategoryColumn.SEODescription - 1)];
					c.AlternateDescription = (reader.IsDBNull(((int)CategoryColumn.AlternateDescription - 1)))?null:(System.String)reader[((int)CategoryColumn.AlternateDescription - 1)];
					c.DisplayOrder = (reader.IsDBNull(((int)CategoryColumn.DisplayOrder - 1)))?null:(System.Int32?)reader[((int)CategoryColumn.DisplayOrder - 1)];
					c.Custom1 = (reader.IsDBNull(((int)CategoryColumn.Custom1 - 1)))?null:(System.String)reader[((int)CategoryColumn.Custom1 - 1)];
					c.Custom2 = (reader.IsDBNull(((int)CategoryColumn.Custom2 - 1)))?null:(System.String)reader[((int)CategoryColumn.Custom2 - 1)];
					c.Custom3 = (reader.IsDBNull(((int)CategoryColumn.Custom3 - 1)))?null:(System.String)reader[((int)CategoryColumn.Custom3 - 1)];
					c.SEOURL = (reader.IsDBNull(((int)CategoryColumn.SEOURL - 1)))?null:(System.String)reader[((int)CategoryColumn.SEOURL - 1)];
					c.PortalID = (reader.IsDBNull(((int)CategoryColumn.PortalID - 1)))?null:(System.Int32?)reader[((int)CategoryColumn.PortalID - 1)];
					c.CreateDate = (reader.IsDBNull(((int)CategoryColumn.CreateDate - 1)))?null:(System.DateTime?)reader[((int)CategoryColumn.CreateDate - 1)];
					c.UpdateDate = (reader.IsDBNull(((int)CategoryColumn.UpdateDate - 1)))?null:(System.DateTime?)reader[((int)CategoryColumn.UpdateDate - 1)];
					c.ExternalID = (reader.IsDBNull(((int)CategoryColumn.ExternalID - 1)))?null:(System.String)reader[((int)CategoryColumn.ExternalID - 1)];
					c.CategoryBanner = (reader.IsDBNull(((int)CategoryColumn.CategoryBanner - 1)))?null:(System.String)reader[((int)CategoryColumn.CategoryBanner - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.Category"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.Category"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.Category entity)
		{
			if (!reader.Read()) return;
			
			entity.CategoryID = (System.Int32)reader[((int)CategoryColumn.CategoryID - 1)];
			entity.Name = (System.String)reader[((int)CategoryColumn.Name - 1)];
			entity.Title = (reader.IsDBNull(((int)CategoryColumn.Title - 1)))?null:(System.String)reader[((int)CategoryColumn.Title - 1)];
			entity.ShortDescription = (reader.IsDBNull(((int)CategoryColumn.ShortDescription - 1)))?null:(System.String)reader[((int)CategoryColumn.ShortDescription - 1)];
			entity.Description = (reader.IsDBNull(((int)CategoryColumn.Description - 1)))?null:(System.String)reader[((int)CategoryColumn.Description - 1)];
			entity.ImageFile = (reader.IsDBNull(((int)CategoryColumn.ImageFile - 1)))?null:(System.String)reader[((int)CategoryColumn.ImageFile - 1)];
			entity.ImageAltTag = (reader.IsDBNull(((int)CategoryColumn.ImageAltTag - 1)))?null:(System.String)reader[((int)CategoryColumn.ImageAltTag - 1)];
			entity.VisibleInd = (System.Boolean)reader[((int)CategoryColumn.VisibleInd - 1)];
			entity.SubCategoryGridVisibleInd = (System.Boolean)reader[((int)CategoryColumn.SubCategoryGridVisibleInd - 1)];
			entity.SEOTitle = (reader.IsDBNull(((int)CategoryColumn.SEOTitle - 1)))?null:(System.String)reader[((int)CategoryColumn.SEOTitle - 1)];
			entity.SEOKeywords = (reader.IsDBNull(((int)CategoryColumn.SEOKeywords - 1)))?null:(System.String)reader[((int)CategoryColumn.SEOKeywords - 1)];
			entity.SEODescription = (reader.IsDBNull(((int)CategoryColumn.SEODescription - 1)))?null:(System.String)reader[((int)CategoryColumn.SEODescription - 1)];
			entity.AlternateDescription = (reader.IsDBNull(((int)CategoryColumn.AlternateDescription - 1)))?null:(System.String)reader[((int)CategoryColumn.AlternateDescription - 1)];
			entity.DisplayOrder = (reader.IsDBNull(((int)CategoryColumn.DisplayOrder - 1)))?null:(System.Int32?)reader[((int)CategoryColumn.DisplayOrder - 1)];
			entity.Custom1 = (reader.IsDBNull(((int)CategoryColumn.Custom1 - 1)))?null:(System.String)reader[((int)CategoryColumn.Custom1 - 1)];
			entity.Custom2 = (reader.IsDBNull(((int)CategoryColumn.Custom2 - 1)))?null:(System.String)reader[((int)CategoryColumn.Custom2 - 1)];
			entity.Custom3 = (reader.IsDBNull(((int)CategoryColumn.Custom3 - 1)))?null:(System.String)reader[((int)CategoryColumn.Custom3 - 1)];
			entity.SEOURL = (reader.IsDBNull(((int)CategoryColumn.SEOURL - 1)))?null:(System.String)reader[((int)CategoryColumn.SEOURL - 1)];
			entity.PortalID = (reader.IsDBNull(((int)CategoryColumn.PortalID - 1)))?null:(System.Int32?)reader[((int)CategoryColumn.PortalID - 1)];
			entity.CreateDate = (reader.IsDBNull(((int)CategoryColumn.CreateDate - 1)))?null:(System.DateTime?)reader[((int)CategoryColumn.CreateDate - 1)];
			entity.UpdateDate = (reader.IsDBNull(((int)CategoryColumn.UpdateDate - 1)))?null:(System.DateTime?)reader[((int)CategoryColumn.UpdateDate - 1)];
			entity.ExternalID = (reader.IsDBNull(((int)CategoryColumn.ExternalID - 1)))?null:(System.String)reader[((int)CategoryColumn.ExternalID - 1)];
			entity.CategoryBanner = (reader.IsDBNull(((int)CategoryColumn.CategoryBanner - 1)))?null:(System.String)reader[((int)CategoryColumn.CategoryBanner - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.Category"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.Category"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.Category entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.CategoryID = (System.Int32)dataRow["CategoryID"];
			entity.Name = (System.String)dataRow["Name"];
			entity.Title = Convert.IsDBNull(dataRow["Title"]) ? null : (System.String)dataRow["Title"];
			entity.ShortDescription = Convert.IsDBNull(dataRow["ShortDescription"]) ? null : (System.String)dataRow["ShortDescription"];
			entity.Description = Convert.IsDBNull(dataRow["Description"]) ? null : (System.String)dataRow["Description"];
			entity.ImageFile = Convert.IsDBNull(dataRow["ImageFile"]) ? null : (System.String)dataRow["ImageFile"];
			entity.ImageAltTag = Convert.IsDBNull(dataRow["ImageAltTag"]) ? null : (System.String)dataRow["ImageAltTag"];
			entity.VisibleInd = (System.Boolean)dataRow["VisibleInd"];
			entity.SubCategoryGridVisibleInd = (System.Boolean)dataRow["SubCategoryGridVisibleInd"];
			entity.SEOTitle = Convert.IsDBNull(dataRow["SEOTitle"]) ? null : (System.String)dataRow["SEOTitle"];
			entity.SEOKeywords = Convert.IsDBNull(dataRow["SEOKeywords"]) ? null : (System.String)dataRow["SEOKeywords"];
			entity.SEODescription = Convert.IsDBNull(dataRow["SEODescription"]) ? null : (System.String)dataRow["SEODescription"];
			entity.AlternateDescription = Convert.IsDBNull(dataRow["AlternateDescription"]) ? null : (System.String)dataRow["AlternateDescription"];
			entity.DisplayOrder = Convert.IsDBNull(dataRow["DisplayOrder"]) ? null : (System.Int32?)dataRow["DisplayOrder"];
			entity.Custom1 = Convert.IsDBNull(dataRow["Custom1"]) ? null : (System.String)dataRow["Custom1"];
			entity.Custom2 = Convert.IsDBNull(dataRow["Custom2"]) ? null : (System.String)dataRow["Custom2"];
			entity.Custom3 = Convert.IsDBNull(dataRow["Custom3"]) ? null : (System.String)dataRow["Custom3"];
			entity.SEOURL = Convert.IsDBNull(dataRow["SEOURL"]) ? null : (System.String)dataRow["SEOURL"];
			entity.PortalID = Convert.IsDBNull(dataRow["PortalID"]) ? null : (System.Int32?)dataRow["PortalID"];
			entity.CreateDate = Convert.IsDBNull(dataRow["CreateDate"]) ? null : (System.DateTime?)dataRow["CreateDate"];
			entity.UpdateDate = Convert.IsDBNull(dataRow["UpdateDate"]) ? null : (System.DateTime?)dataRow["UpdateDate"];
			entity.ExternalID = Convert.IsDBNull(dataRow["ExternalID"]) ? null : (System.String)dataRow["ExternalID"];
			entity.CategoryBanner = Convert.IsDBNull(dataRow["CategoryBanner"]) ? null : (System.String)dataRow["CategoryBanner"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.Category"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.Category Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.Category entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region PortalIDSource	
			if (CanDeepLoad(entity, "Portal|PortalIDSource", deepLoadType, innerList) 
				&& entity.PortalIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.PortalID ?? (int)0);
				Portal tmpEntity = EntityManager.LocateEntity<Portal>(EntityLocator.ConstructKeyFromPkItems(typeof(Portal), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.PortalIDSource = tmpEntity;
				else
					entity.PortalIDSource = DataRepository.PortalProvider.GetByPortalID(transactionManager, (entity.PortalID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PortalIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.PortalIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.PortalProvider.DeepLoad(transactionManager, entity.PortalIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion PortalIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByCategoryID methods when available
			
			#region ProductCategoryCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<ProductCategory>|ProductCategoryCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ProductCategoryCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ProductCategoryCollection = DataRepository.ProductCategoryProvider.GetByCategoryID(transactionManager, entity.CategoryID);

				if (deep && entity.ProductCategoryCollection.Count > 0)
				{
					deepHandles.Add("ProductCategoryCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<ProductCategory>) DataRepository.ProductCategoryProvider.DeepLoad,
						new object[] { transactionManager, entity.ProductCategoryCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region FacetGroupCategoryCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<FacetGroupCategory>|FacetGroupCategoryCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'FacetGroupCategoryCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.FacetGroupCategoryCollection = DataRepository.FacetGroupCategoryProvider.GetByCategoryID(transactionManager, entity.CategoryID);

				if (deep && entity.FacetGroupCategoryCollection.Count > 0)
				{
					deepHandles.Add("FacetGroupCategoryCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<FacetGroupCategory>) DataRepository.FacetGroupCategoryProvider.DeepLoad,
						new object[] { transactionManager, entity.FacetGroupCategoryCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region PromotionCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Promotion>|PromotionCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PromotionCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.PromotionCollection = DataRepository.PromotionProvider.GetByCategoryID(transactionManager, entity.CategoryID);

				if (deep && entity.PromotionCollection.Count > 0)
				{
					deepHandles.Add("PromotionCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Promotion>) DataRepository.PromotionProvider.DeepLoad,
						new object[] { transactionManager, entity.PromotionCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region CategoryProfileCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<CategoryProfile>|CategoryProfileCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CategoryProfileCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CategoryProfileCollection = DataRepository.CategoryProfileProvider.GetByCategoryID(transactionManager, entity.CategoryID);

				if (deep && entity.CategoryProfileCollection.Count > 0)
				{
					deepHandles.Add("CategoryProfileCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<CategoryProfile>) DataRepository.CategoryProfileProvider.DeepLoad,
						new object[] { transactionManager, entity.CategoryProfileCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region FacetGroupIDFacetGroupCollection_From_FacetGroupCategory
			// RelationshipType.ManyToMany
			if (CanDeepLoad(entity, "List<FacetGroup>|FacetGroupIDFacetGroupCollection_From_FacetGroupCategory", deepLoadType, innerList))
			{
				entity.FacetGroupIDFacetGroupCollection_From_FacetGroupCategory = DataRepository.FacetGroupProvider.GetByCategoryIDFromFacetGroupCategory(transactionManager, entity.CategoryID);			 
		
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'FacetGroupIDFacetGroupCollection_From_FacetGroupCategory' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.FacetGroupIDFacetGroupCollection_From_FacetGroupCategory != null)
				{
					deepHandles.Add("FacetGroupIDFacetGroupCollection_From_FacetGroupCategory",
						new KeyValuePair<Delegate, object>((DeepLoadHandle< FacetGroup >) DataRepository.FacetGroupProvider.DeepLoad,
						new object[] { transactionManager, entity.FacetGroupIDFacetGroupCollection_From_FacetGroupCategory, deep, deepLoadType, childTypes, innerList }
					));
				}
			}
			#endregion
			
			
			
			#region CategoryNodeCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<CategoryNode>|CategoryNodeCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CategoryNodeCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CategoryNodeCollection = DataRepository.CategoryNodeProvider.GetByCategoryID(transactionManager, entity.CategoryID);

				if (deep && entity.CategoryNodeCollection.Count > 0)
				{
					deepHandles.Add("CategoryNodeCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<CategoryNode>) DataRepository.CategoryNodeProvider.DeepLoad,
						new object[] { transactionManager, entity.CategoryNodeCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.Category object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.Category instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.Category Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.Category entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region PortalIDSource
			if (CanDeepSave(entity, "Portal|PortalIDSource", deepSaveType, innerList) 
				&& entity.PortalIDSource != null)
			{
				DataRepository.PortalProvider.Save(transactionManager, entity.PortalIDSource);
				entity.PortalID = entity.PortalIDSource.PortalID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();

			#region FacetGroupIDFacetGroupCollection_From_FacetGroupCategory>
			if (CanDeepSave(entity.FacetGroupIDFacetGroupCollection_From_FacetGroupCategory, "List<FacetGroup>|FacetGroupIDFacetGroupCollection_From_FacetGroupCategory", deepSaveType, innerList))
			{
				if (entity.FacetGroupIDFacetGroupCollection_From_FacetGroupCategory.Count > 0 || entity.FacetGroupIDFacetGroupCollection_From_FacetGroupCategory.DeletedItems.Count > 0)
				{
					DataRepository.FacetGroupProvider.Save(transactionManager, entity.FacetGroupIDFacetGroupCollection_From_FacetGroupCategory); 
					deepHandles.Add("FacetGroupIDFacetGroupCollection_From_FacetGroupCategory",
						new KeyValuePair<Delegate, object>((DeepSaveHandle<FacetGroup>) DataRepository.FacetGroupProvider.DeepSave,
						new object[] { transactionManager, entity.FacetGroupIDFacetGroupCollection_From_FacetGroupCategory, deepSaveType, childTypes, innerList }
					));
				}
			}
			#endregion 
	
			#region List<ProductCategory>
				if (CanDeepSave(entity.ProductCategoryCollection, "List<ProductCategory>|ProductCategoryCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(ProductCategory child in entity.ProductCategoryCollection)
					{
						if(child.CategoryIDSource != null)
						{
							child.CategoryID = child.CategoryIDSource.CategoryID;
						}
						else
						{
							child.CategoryID = entity.CategoryID;
						}

					}

					if (entity.ProductCategoryCollection.Count > 0 || entity.ProductCategoryCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ProductCategoryProvider.Save(transactionManager, entity.ProductCategoryCollection);
						
						deepHandles.Add("ProductCategoryCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< ProductCategory >) DataRepository.ProductCategoryProvider.DeepSave,
							new object[] { transactionManager, entity.ProductCategoryCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<FacetGroupCategory>
				if (CanDeepSave(entity.FacetGroupCategoryCollection, "List<FacetGroupCategory>|FacetGroupCategoryCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(FacetGroupCategory child in entity.FacetGroupCategoryCollection)
					{
						if(child.CategoryIDSource != null)
						{
								child.CategoryID = child.CategoryIDSource.CategoryID;
						}

						if(child.FacetGroupIDSource != null)
						{
								child.FacetGroupID = child.FacetGroupIDSource.FacetGroupID;
						}

					}

					if (entity.FacetGroupCategoryCollection.Count > 0 || entity.FacetGroupCategoryCollection.DeletedItems.Count > 0)
					{
						//DataRepository.FacetGroupCategoryProvider.Save(transactionManager, entity.FacetGroupCategoryCollection);
						
						deepHandles.Add("FacetGroupCategoryCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< FacetGroupCategory >) DataRepository.FacetGroupCategoryProvider.DeepSave,
							new object[] { transactionManager, entity.FacetGroupCategoryCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Promotion>
				if (CanDeepSave(entity.PromotionCollection, "List<Promotion>|PromotionCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Promotion child in entity.PromotionCollection)
					{
						if(child.CategoryIDSource != null)
						{
							child.CategoryID = child.CategoryIDSource.CategoryID;
						}
						else
						{
							child.CategoryID = entity.CategoryID;
						}

					}

					if (entity.PromotionCollection.Count > 0 || entity.PromotionCollection.DeletedItems.Count > 0)
					{
						//DataRepository.PromotionProvider.Save(transactionManager, entity.PromotionCollection);
						
						deepHandles.Add("PromotionCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Promotion >) DataRepository.PromotionProvider.DeepSave,
							new object[] { transactionManager, entity.PromotionCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<CategoryProfile>
				if (CanDeepSave(entity.CategoryProfileCollection, "List<CategoryProfile>|CategoryProfileCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(CategoryProfile child in entity.CategoryProfileCollection)
					{
						if(child.CategoryIDSource != null)
						{
							child.CategoryID = child.CategoryIDSource.CategoryID;
						}
						else
						{
							child.CategoryID = entity.CategoryID;
						}

					}

					if (entity.CategoryProfileCollection.Count > 0 || entity.CategoryProfileCollection.DeletedItems.Count > 0)
					{
						//DataRepository.CategoryProfileProvider.Save(transactionManager, entity.CategoryProfileCollection);
						
						deepHandles.Add("CategoryProfileCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< CategoryProfile >) DataRepository.CategoryProfileProvider.DeepSave,
							new object[] { transactionManager, entity.CategoryProfileCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<CategoryNode>
				if (CanDeepSave(entity.CategoryNodeCollection, "List<CategoryNode>|CategoryNodeCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(CategoryNode child in entity.CategoryNodeCollection)
					{
						if(child.CategoryIDSource != null)
						{
							child.CategoryID = child.CategoryIDSource.CategoryID;
						}
						else
						{
							child.CategoryID = entity.CategoryID;
						}

					}

					if (entity.CategoryNodeCollection.Count > 0 || entity.CategoryNodeCollection.DeletedItems.Count > 0)
					{
						//DataRepository.CategoryNodeProvider.Save(transactionManager, entity.CategoryNodeCollection);
						
						deepHandles.Add("CategoryNodeCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< CategoryNode >) DataRepository.CategoryNodeProvider.DeepSave,
							new object[] { transactionManager, entity.CategoryNodeCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region CategoryChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.Category</c>
	///</summary>
	public enum CategoryChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Portal</c> at PortalIDSource
		///</summary>
		[ChildEntityType(typeof(Portal))]
		Portal,
		///<summary>
		/// Collection of <c>Category</c> as OneToMany for ProductCategoryCollection
		///</summary>
		[ChildEntityType(typeof(TList<ProductCategory>))]
		ProductCategoryCollection,
		///<summary>
		/// Collection of <c>Category</c> as OneToMany for FacetGroupCategoryCollection
		///</summary>
		[ChildEntityType(typeof(TList<FacetGroupCategory>))]
		FacetGroupCategoryCollection,
		///<summary>
		/// Collection of <c>Category</c> as OneToMany for PromotionCollection
		///</summary>
		[ChildEntityType(typeof(TList<Promotion>))]
		PromotionCollection,
		///<summary>
		/// Collection of <c>Category</c> as OneToMany for CategoryProfileCollection
		///</summary>
		[ChildEntityType(typeof(TList<CategoryProfile>))]
		CategoryProfileCollection,
		///<summary>
		/// Collection of <c>Category</c> as ManyToMany for FacetGroupCollection_From_FacetGroupCategory
		///</summary>
		[ChildEntityType(typeof(TList<FacetGroup>))]
		FacetGroupIDFacetGroupCollection_From_FacetGroupCategory,
		///<summary>
		/// Collection of <c>Category</c> as OneToMany for CategoryNodeCollection
		///</summary>
		[ChildEntityType(typeof(TList<CategoryNode>))]
		CategoryNodeCollection,
	}
	
	#endregion CategoryChildEntityTypes
	
	#region CategoryFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;CategoryColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Category"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CategoryFilterBuilder : SqlFilterBuilder<CategoryColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CategoryFilterBuilder class.
		/// </summary>
		public CategoryFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CategoryFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CategoryFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CategoryFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CategoryFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CategoryFilterBuilder
	
	#region CategoryParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;CategoryColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Category"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CategoryParameterBuilder : ParameterizedSqlFilterBuilder<CategoryColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CategoryParameterBuilder class.
		/// </summary>
		public CategoryParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CategoryParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CategoryParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CategoryParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CategoryParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CategoryParameterBuilder
	
	#region CategorySortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;CategoryColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Category"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class CategorySortBuilder : SqlSortBuilder<CategoryColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CategorySqlSortBuilder class.
		/// </summary>
		public CategorySortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion CategorySortBuilder
	
} // end namespace
