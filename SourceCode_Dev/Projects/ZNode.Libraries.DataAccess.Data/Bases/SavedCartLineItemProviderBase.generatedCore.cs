﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="SavedCartLineItemProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class SavedCartLineItemProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.SavedCartLineItem, ZNode.Libraries.DataAccess.Entities.SavedCartLineItemKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.SavedCartLineItemKey key)
		{
			return Delete(transactionManager, key.SavedCartLineItemID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_savedCartLineItemID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _savedCartLineItemID)
		{
			return Delete(null, _savedCartLineItemID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_savedCartLineItemID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _savedCartLineItemID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeSavedCartLineItem_ZNodeOrderLineItemRelationshipType key.
		///		FK_ZNodeSavedCartLineItem_ZNodeOrderLineItemRelationshipType Description: 
		/// </summary>
		/// <param name="_orderLineItemRelationshipTypeID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.SavedCartLineItem objects.</returns>
		public TList<SavedCartLineItem> GetByOrderLineItemRelationshipTypeID(System.Int32? _orderLineItemRelationshipTypeID)
		{
			int count = -1;
			return GetByOrderLineItemRelationshipTypeID(_orderLineItemRelationshipTypeID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeSavedCartLineItem_ZNodeOrderLineItemRelationshipType key.
		///		FK_ZNodeSavedCartLineItem_ZNodeOrderLineItemRelationshipType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_orderLineItemRelationshipTypeID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.SavedCartLineItem objects.</returns>
		/// <remarks></remarks>
		public TList<SavedCartLineItem> GetByOrderLineItemRelationshipTypeID(TransactionManager transactionManager, System.Int32? _orderLineItemRelationshipTypeID)
		{
			int count = -1;
			return GetByOrderLineItemRelationshipTypeID(transactionManager, _orderLineItemRelationshipTypeID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeSavedCartLineItem_ZNodeOrderLineItemRelationshipType key.
		///		FK_ZNodeSavedCartLineItem_ZNodeOrderLineItemRelationshipType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_orderLineItemRelationshipTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.SavedCartLineItem objects.</returns>
		public TList<SavedCartLineItem> GetByOrderLineItemRelationshipTypeID(TransactionManager transactionManager, System.Int32? _orderLineItemRelationshipTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetByOrderLineItemRelationshipTypeID(transactionManager, _orderLineItemRelationshipTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeSavedCartLineItem_ZNodeOrderLineItemRelationshipType key.
		///		fKZNodeSavedCartLineItemZNodeOrderLineItemRelationshipType Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_orderLineItemRelationshipTypeID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.SavedCartLineItem objects.</returns>
		public TList<SavedCartLineItem> GetByOrderLineItemRelationshipTypeID(System.Int32? _orderLineItemRelationshipTypeID, int start, int pageLength)
		{
			int count =  -1;
			return GetByOrderLineItemRelationshipTypeID(null, _orderLineItemRelationshipTypeID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeSavedCartLineItem_ZNodeOrderLineItemRelationshipType key.
		///		fKZNodeSavedCartLineItemZNodeOrderLineItemRelationshipType Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_orderLineItemRelationshipTypeID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.SavedCartLineItem objects.</returns>
		public TList<SavedCartLineItem> GetByOrderLineItemRelationshipTypeID(System.Int32? _orderLineItemRelationshipTypeID, int start, int pageLength,out int count)
		{
			return GetByOrderLineItemRelationshipTypeID(null, _orderLineItemRelationshipTypeID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeSavedCartLineItem_ZNodeOrderLineItemRelationshipType key.
		///		FK_ZNodeSavedCartLineItem_ZNodeOrderLineItemRelationshipType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_orderLineItemRelationshipTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.SavedCartLineItem objects.</returns>
		public abstract TList<SavedCartLineItem> GetByOrderLineItemRelationshipTypeID(TransactionManager transactionManager, System.Int32? _orderLineItemRelationshipTypeID, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.SavedCartLineItem Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.SavedCartLineItemKey key, int start, int pageLength)
		{
			return GetBySavedCartLineItemID(transactionManager, key.SavedCartLineItemID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IDX_ZNodeSavedCartLineItem_ParentSavedCartLineItemID index.
		/// </summary>
		/// <param name="_parentSavedCartLineItemID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;SavedCartLineItem&gt;"/> class.</returns>
		public TList<SavedCartLineItem> GetByParentSavedCartLineItemID(System.Int32? _parentSavedCartLineItemID)
		{
			int count = -1;
			return GetByParentSavedCartLineItemID(null,_parentSavedCartLineItemID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IDX_ZNodeSavedCartLineItem_ParentSavedCartLineItemID index.
		/// </summary>
		/// <param name="_parentSavedCartLineItemID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;SavedCartLineItem&gt;"/> class.</returns>
		public TList<SavedCartLineItem> GetByParentSavedCartLineItemID(System.Int32? _parentSavedCartLineItemID, int start, int pageLength)
		{
			int count = -1;
			return GetByParentSavedCartLineItemID(null, _parentSavedCartLineItemID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IDX_ZNodeSavedCartLineItem_ParentSavedCartLineItemID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_parentSavedCartLineItemID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;SavedCartLineItem&gt;"/> class.</returns>
		public TList<SavedCartLineItem> GetByParentSavedCartLineItemID(TransactionManager transactionManager, System.Int32? _parentSavedCartLineItemID)
		{
			int count = -1;
			return GetByParentSavedCartLineItemID(transactionManager, _parentSavedCartLineItemID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IDX_ZNodeSavedCartLineItem_ParentSavedCartLineItemID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_parentSavedCartLineItemID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;SavedCartLineItem&gt;"/> class.</returns>
		public TList<SavedCartLineItem> GetByParentSavedCartLineItemID(TransactionManager transactionManager, System.Int32? _parentSavedCartLineItemID, int start, int pageLength)
		{
			int count = -1;
			return GetByParentSavedCartLineItemID(transactionManager, _parentSavedCartLineItemID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IDX_ZNodeSavedCartLineItem_ParentSavedCartLineItemID index.
		/// </summary>
		/// <param name="_parentSavedCartLineItemID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;SavedCartLineItem&gt;"/> class.</returns>
		public TList<SavedCartLineItem> GetByParentSavedCartLineItemID(System.Int32? _parentSavedCartLineItemID, int start, int pageLength, out int count)
		{
			return GetByParentSavedCartLineItemID(null, _parentSavedCartLineItemID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IDX_ZNodeSavedCartLineItem_ParentSavedCartLineItemID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_parentSavedCartLineItemID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;SavedCartLineItem&gt;"/> class.</returns>
		public abstract TList<SavedCartLineItem> GetByParentSavedCartLineItemID(TransactionManager transactionManager, System.Int32? _parentSavedCartLineItemID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IDX_ZNodeSavedCartLineItem_SavedCartID index.
		/// </summary>
		/// <param name="_savedCartID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;SavedCartLineItem&gt;"/> class.</returns>
		public TList<SavedCartLineItem> GetBySavedCartID(System.Int32 _savedCartID)
		{
			int count = -1;
			return GetBySavedCartID(null,_savedCartID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IDX_ZNodeSavedCartLineItem_SavedCartID index.
		/// </summary>
		/// <param name="_savedCartID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;SavedCartLineItem&gt;"/> class.</returns>
		public TList<SavedCartLineItem> GetBySavedCartID(System.Int32 _savedCartID, int start, int pageLength)
		{
			int count = -1;
			return GetBySavedCartID(null, _savedCartID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IDX_ZNodeSavedCartLineItem_SavedCartID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_savedCartID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;SavedCartLineItem&gt;"/> class.</returns>
		public TList<SavedCartLineItem> GetBySavedCartID(TransactionManager transactionManager, System.Int32 _savedCartID)
		{
			int count = -1;
			return GetBySavedCartID(transactionManager, _savedCartID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IDX_ZNodeSavedCartLineItem_SavedCartID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_savedCartID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;SavedCartLineItem&gt;"/> class.</returns>
		public TList<SavedCartLineItem> GetBySavedCartID(TransactionManager transactionManager, System.Int32 _savedCartID, int start, int pageLength)
		{
			int count = -1;
			return GetBySavedCartID(transactionManager, _savedCartID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IDX_ZNodeSavedCartLineItem_SavedCartID index.
		/// </summary>
		/// <param name="_savedCartID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;SavedCartLineItem&gt;"/> class.</returns>
		public TList<SavedCartLineItem> GetBySavedCartID(System.Int32 _savedCartID, int start, int pageLength, out int count)
		{
			return GetBySavedCartID(null, _savedCartID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IDX_ZNodeSavedCartLineItem_SavedCartID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_savedCartID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;SavedCartLineItem&gt;"/> class.</returns>
		public abstract TList<SavedCartLineItem> GetBySavedCartID(TransactionManager transactionManager, System.Int32 _savedCartID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodeSavedCartLineItem index.
		/// </summary>
		/// <param name="_savedCartLineItemID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SavedCartLineItem"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.SavedCartLineItem GetBySavedCartLineItemID(System.Int32 _savedCartLineItemID)
		{
			int count = -1;
			return GetBySavedCartLineItemID(null,_savedCartLineItemID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeSavedCartLineItem index.
		/// </summary>
		/// <param name="_savedCartLineItemID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SavedCartLineItem"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.SavedCartLineItem GetBySavedCartLineItemID(System.Int32 _savedCartLineItemID, int start, int pageLength)
		{
			int count = -1;
			return GetBySavedCartLineItemID(null, _savedCartLineItemID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeSavedCartLineItem index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_savedCartLineItemID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SavedCartLineItem"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.SavedCartLineItem GetBySavedCartLineItemID(TransactionManager transactionManager, System.Int32 _savedCartLineItemID)
		{
			int count = -1;
			return GetBySavedCartLineItemID(transactionManager, _savedCartLineItemID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeSavedCartLineItem index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_savedCartLineItemID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SavedCartLineItem"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.SavedCartLineItem GetBySavedCartLineItemID(TransactionManager transactionManager, System.Int32 _savedCartLineItemID, int start, int pageLength)
		{
			int count = -1;
			return GetBySavedCartLineItemID(transactionManager, _savedCartLineItemID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeSavedCartLineItem index.
		/// </summary>
		/// <param name="_savedCartLineItemID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SavedCartLineItem"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.SavedCartLineItem GetBySavedCartLineItemID(System.Int32 _savedCartLineItemID, int start, int pageLength, out int count)
		{
			return GetBySavedCartLineItemID(null, _savedCartLineItemID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeSavedCartLineItem index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_savedCartLineItemID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SavedCartLineItem"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.SavedCartLineItem GetBySavedCartLineItemID(TransactionManager transactionManager, System.Int32 _savedCartLineItemID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;SavedCartLineItem&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;SavedCartLineItem&gt;"/></returns>
		public static TList<SavedCartLineItem> Fill(IDataReader reader, TList<SavedCartLineItem> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.SavedCartLineItem c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("SavedCartLineItem")
					.Append("|").Append((System.Int32)reader[((int)SavedCartLineItemColumn.SavedCartLineItemID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<SavedCartLineItem>(
					key.ToString(), // EntityTrackingKey
					"SavedCartLineItem",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.SavedCartLineItem();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.SavedCartLineItemID = (System.Int32)reader[((int)SavedCartLineItemColumn.SavedCartLineItemID - 1)];
					c.SavedCartID = (System.Int32)reader[((int)SavedCartLineItemColumn.SavedCartID - 1)];
					c.SKUID = (System.Int32)reader[((int)SavedCartLineItemColumn.SKUID - 1)];
					c.Quantity = (System.Int32)reader[((int)SavedCartLineItemColumn.Quantity - 1)];
					c.ParentSavedCartLineItemID = (reader.IsDBNull(((int)SavedCartLineItemColumn.ParentSavedCartLineItemID - 1)))?null:(System.Int32?)reader[((int)SavedCartLineItemColumn.ParentSavedCartLineItemID - 1)];
					c.OrderLineItemRelationshipTypeID = (reader.IsDBNull(((int)SavedCartLineItemColumn.OrderLineItemRelationshipTypeID - 1)))?null:(System.Int32?)reader[((int)SavedCartLineItemColumn.OrderLineItemRelationshipTypeID - 1)];
					c.CustomeText = (reader.IsDBNull(((int)SavedCartLineItemColumn.CustomeText - 1)))?null:(System.String)reader[((int)SavedCartLineItemColumn.CustomeText - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.SavedCartLineItem"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.SavedCartLineItem"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.SavedCartLineItem entity)
		{
			if (!reader.Read()) return;
			
			entity.SavedCartLineItemID = (System.Int32)reader[((int)SavedCartLineItemColumn.SavedCartLineItemID - 1)];
			entity.SavedCartID = (System.Int32)reader[((int)SavedCartLineItemColumn.SavedCartID - 1)];
			entity.SKUID = (System.Int32)reader[((int)SavedCartLineItemColumn.SKUID - 1)];
			entity.Quantity = (System.Int32)reader[((int)SavedCartLineItemColumn.Quantity - 1)];
			entity.ParentSavedCartLineItemID = (reader.IsDBNull(((int)SavedCartLineItemColumn.ParentSavedCartLineItemID - 1)))?null:(System.Int32?)reader[((int)SavedCartLineItemColumn.ParentSavedCartLineItemID - 1)];
			entity.OrderLineItemRelationshipTypeID = (reader.IsDBNull(((int)SavedCartLineItemColumn.OrderLineItemRelationshipTypeID - 1)))?null:(System.Int32?)reader[((int)SavedCartLineItemColumn.OrderLineItemRelationshipTypeID - 1)];
			entity.CustomeText = (reader.IsDBNull(((int)SavedCartLineItemColumn.CustomeText - 1)))?null:(System.String)reader[((int)SavedCartLineItemColumn.CustomeText - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.SavedCartLineItem"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.SavedCartLineItem"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.SavedCartLineItem entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.SavedCartLineItemID = (System.Int32)dataRow["SavedCartLineItemID"];
			entity.SavedCartID = (System.Int32)dataRow["SavedCartID"];
			entity.SKUID = (System.Int32)dataRow["SKUID"];
			entity.Quantity = (System.Int32)dataRow["Quantity"];
			entity.ParentSavedCartLineItemID = Convert.IsDBNull(dataRow["ParentSavedCartLineItemID"]) ? null : (System.Int32?)dataRow["ParentSavedCartLineItemID"];
			entity.OrderLineItemRelationshipTypeID = Convert.IsDBNull(dataRow["OrderLineItemRelationshipTypeID"]) ? null : (System.Int32?)dataRow["OrderLineItemRelationshipTypeID"];
			entity.CustomeText = Convert.IsDBNull(dataRow["CustomeText"]) ? null : (System.String)dataRow["CustomeText"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.SavedCartLineItem"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.SavedCartLineItem Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.SavedCartLineItem entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region OrderLineItemRelationshipTypeIDSource	
			if (CanDeepLoad(entity, "OrderLineItemRelationshipType|OrderLineItemRelationshipTypeIDSource", deepLoadType, innerList) 
				&& entity.OrderLineItemRelationshipTypeIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.OrderLineItemRelationshipTypeID ?? (int)0);
				OrderLineItemRelationshipType tmpEntity = EntityManager.LocateEntity<OrderLineItemRelationshipType>(EntityLocator.ConstructKeyFromPkItems(typeof(OrderLineItemRelationshipType), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.OrderLineItemRelationshipTypeIDSource = tmpEntity;
				else
					entity.OrderLineItemRelationshipTypeIDSource = DataRepository.OrderLineItemRelationshipTypeProvider.GetByOrderLineItemRelationshipTypeID(transactionManager, (entity.OrderLineItemRelationshipTypeID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'OrderLineItemRelationshipTypeIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.OrderLineItemRelationshipTypeIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.OrderLineItemRelationshipTypeProvider.DeepLoad(transactionManager, entity.OrderLineItemRelationshipTypeIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion OrderLineItemRelationshipTypeIDSource

			#region SavedCartIDSource	
			if (CanDeepLoad(entity, "SavedCart|SavedCartIDSource", deepLoadType, innerList) 
				&& entity.SavedCartIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.SavedCartID;
				SavedCart tmpEntity = EntityManager.LocateEntity<SavedCart>(EntityLocator.ConstructKeyFromPkItems(typeof(SavedCart), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.SavedCartIDSource = tmpEntity;
				else
					entity.SavedCartIDSource = DataRepository.SavedCartProvider.GetBySavedCartID(transactionManager, entity.SavedCartID);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SavedCartIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.SavedCartIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.SavedCartProvider.DeepLoad(transactionManager, entity.SavedCartIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion SavedCartIDSource

			#region ParentSavedCartLineItemIDSource	
			if (CanDeepLoad(entity, "SavedCartLineItem|ParentSavedCartLineItemIDSource", deepLoadType, innerList) 
				&& entity.ParentSavedCartLineItemIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.ParentSavedCartLineItemID ?? (int)0);
				SavedCartLineItem tmpEntity = EntityManager.LocateEntity<SavedCartLineItem>(EntityLocator.ConstructKeyFromPkItems(typeof(SavedCartLineItem), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ParentSavedCartLineItemIDSource = tmpEntity;
				else
					entity.ParentSavedCartLineItemIDSource = DataRepository.SavedCartLineItemProvider.GetBySavedCartLineItemID(transactionManager, (entity.ParentSavedCartLineItemID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ParentSavedCartLineItemIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ParentSavedCartLineItemIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.SavedCartLineItemProvider.DeepLoad(transactionManager, entity.ParentSavedCartLineItemIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ParentSavedCartLineItemIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetBySavedCartLineItemID methods when available
			
			#region SavedCartLineItemCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<SavedCartLineItem>|SavedCartLineItemCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SavedCartLineItemCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.SavedCartLineItemCollection = DataRepository.SavedCartLineItemProvider.GetByParentSavedCartLineItemID(transactionManager, entity.SavedCartLineItemID);

				if (deep && entity.SavedCartLineItemCollection.Count > 0)
				{
					deepHandles.Add("SavedCartLineItemCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<SavedCartLineItem>) DataRepository.SavedCartLineItemProvider.DeepLoad,
						new object[] { transactionManager, entity.SavedCartLineItemCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.SavedCartLineItem object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.SavedCartLineItem instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.SavedCartLineItem Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.SavedCartLineItem entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region OrderLineItemRelationshipTypeIDSource
			if (CanDeepSave(entity, "OrderLineItemRelationshipType|OrderLineItemRelationshipTypeIDSource", deepSaveType, innerList) 
				&& entity.OrderLineItemRelationshipTypeIDSource != null)
			{
				DataRepository.OrderLineItemRelationshipTypeProvider.Save(transactionManager, entity.OrderLineItemRelationshipTypeIDSource);
				entity.OrderLineItemRelationshipTypeID = entity.OrderLineItemRelationshipTypeIDSource.OrderLineItemRelationshipTypeID;
			}
			#endregion 
			
			#region SavedCartIDSource
			if (CanDeepSave(entity, "SavedCart|SavedCartIDSource", deepSaveType, innerList) 
				&& entity.SavedCartIDSource != null)
			{
				DataRepository.SavedCartProvider.Save(transactionManager, entity.SavedCartIDSource);
				entity.SavedCartID = entity.SavedCartIDSource.SavedCartID;
			}
			#endregion 
			
			#region ParentSavedCartLineItemIDSource
			if (CanDeepSave(entity, "SavedCartLineItem|ParentSavedCartLineItemIDSource", deepSaveType, innerList) 
				&& entity.ParentSavedCartLineItemIDSource != null)
			{
				DataRepository.SavedCartLineItemProvider.Save(transactionManager, entity.ParentSavedCartLineItemIDSource);
				entity.ParentSavedCartLineItemID = entity.ParentSavedCartLineItemIDSource.SavedCartLineItemID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<SavedCartLineItem>
				if (CanDeepSave(entity.SavedCartLineItemCollection, "List<SavedCartLineItem>|SavedCartLineItemCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(SavedCartLineItem child in entity.SavedCartLineItemCollection)
					{
						if(child.ParentSavedCartLineItemIDSource != null)
						{
							child.ParentSavedCartLineItemID = child.ParentSavedCartLineItemIDSource.SavedCartLineItemID;
						}
						else
						{
							child.ParentSavedCartLineItemID = entity.SavedCartLineItemID;
						}

					}

					if (entity.SavedCartLineItemCollection.Count > 0 || entity.SavedCartLineItemCollection.DeletedItems.Count > 0)
					{
						//DataRepository.SavedCartLineItemProvider.Save(transactionManager, entity.SavedCartLineItemCollection);
						
						deepHandles.Add("SavedCartLineItemCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< SavedCartLineItem >) DataRepository.SavedCartLineItemProvider.DeepSave,
							new object[] { transactionManager, entity.SavedCartLineItemCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region SavedCartLineItemChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.SavedCartLineItem</c>
	///</summary>
	public enum SavedCartLineItemChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>OrderLineItemRelationshipType</c> at OrderLineItemRelationshipTypeIDSource
		///</summary>
		[ChildEntityType(typeof(OrderLineItemRelationshipType))]
		OrderLineItemRelationshipType,
		
		///<summary>
		/// Composite Property for <c>SavedCart</c> at SavedCartIDSource
		///</summary>
		[ChildEntityType(typeof(SavedCart))]
		SavedCart,
		
		///<summary>
		/// Composite Property for <c>SavedCartLineItem</c> at ParentSavedCartLineItemIDSource
		///</summary>
		[ChildEntityType(typeof(SavedCartLineItem))]
		SavedCartLineItem,
		///<summary>
		/// Collection of <c>SavedCartLineItem</c> as OneToMany for SavedCartLineItemCollection
		///</summary>
		[ChildEntityType(typeof(TList<SavedCartLineItem>))]
		SavedCartLineItemCollection,
	}
	
	#endregion SavedCartLineItemChildEntityTypes
	
	#region SavedCartLineItemFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;SavedCartLineItemColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SavedCartLineItem"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SavedCartLineItemFilterBuilder : SqlFilterBuilder<SavedCartLineItemColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SavedCartLineItemFilterBuilder class.
		/// </summary>
		public SavedCartLineItemFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SavedCartLineItemFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SavedCartLineItemFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SavedCartLineItemFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SavedCartLineItemFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SavedCartLineItemFilterBuilder
	
	#region SavedCartLineItemParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;SavedCartLineItemColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SavedCartLineItem"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SavedCartLineItemParameterBuilder : ParameterizedSqlFilterBuilder<SavedCartLineItemColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SavedCartLineItemParameterBuilder class.
		/// </summary>
		public SavedCartLineItemParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SavedCartLineItemParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SavedCartLineItemParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SavedCartLineItemParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SavedCartLineItemParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SavedCartLineItemParameterBuilder
	
	#region SavedCartLineItemSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;SavedCartLineItemColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SavedCartLineItem"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class SavedCartLineItemSortBuilder : SqlSortBuilder<SavedCartLineItemColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SavedCartLineItemSqlSortBuilder class.
		/// </summary>
		public SavedCartLineItemSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion SavedCartLineItemSortBuilder
	
} // end namespace
