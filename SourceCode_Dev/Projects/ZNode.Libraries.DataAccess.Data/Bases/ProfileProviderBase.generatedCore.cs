﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ProfileProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class ProfileProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.Profile, ZNode.Libraries.DataAccess.Entities.ProfileKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProfileKey key)
		{
			return Delete(transactionManager, key.ProfileID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_profileID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _profileID)
		{
			return Delete(null, _profileID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_profileID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _profileID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProfile_ZNodeTaxClass key.
		///		FK_ZNodeProfile_ZNodeTaxClass Description: 
		/// </summary>
		/// <param name="_taxClassID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Profile objects.</returns>
		public TList<Profile> GetByTaxClassID(System.Int32? _taxClassID)
		{
			int count = -1;
			return GetByTaxClassID(_taxClassID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProfile_ZNodeTaxClass key.
		///		FK_ZNodeProfile_ZNodeTaxClass Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_taxClassID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Profile objects.</returns>
		/// <remarks></remarks>
		public TList<Profile> GetByTaxClassID(TransactionManager transactionManager, System.Int32? _taxClassID)
		{
			int count = -1;
			return GetByTaxClassID(transactionManager, _taxClassID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProfile_ZNodeTaxClass key.
		///		FK_ZNodeProfile_ZNodeTaxClass Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_taxClassID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Profile objects.</returns>
		public TList<Profile> GetByTaxClassID(TransactionManager transactionManager, System.Int32? _taxClassID, int start, int pageLength)
		{
			int count = -1;
			return GetByTaxClassID(transactionManager, _taxClassID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProfile_ZNodeTaxClass key.
		///		fKZNodeProfileZNodeTaxClass Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_taxClassID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Profile objects.</returns>
		public TList<Profile> GetByTaxClassID(System.Int32? _taxClassID, int start, int pageLength)
		{
			int count =  -1;
			return GetByTaxClassID(null, _taxClassID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProfile_ZNodeTaxClass key.
		///		fKZNodeProfileZNodeTaxClass Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_taxClassID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Profile objects.</returns>
		public TList<Profile> GetByTaxClassID(System.Int32? _taxClassID, int start, int pageLength,out int count)
		{
			return GetByTaxClassID(null, _taxClassID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProfile_ZNodeTaxClass key.
		///		FK_ZNodeProfile_ZNodeTaxClass Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_taxClassID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Profile objects.</returns>
		public abstract TList<Profile> GetByTaxClassID(TransactionManager transactionManager, System.Int32? _taxClassID, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.Profile Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProfileKey key, int start, int pageLength)
		{
			return GetByProfileID(transactionManager, key.ProfileID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_Name index.
		/// </summary>
		/// <param name="_name"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Profile&gt;"/> class.</returns>
		public TList<Profile> GetByName(System.String _name)
		{
			int count = -1;
			return GetByName(null,_name, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Name index.
		/// </summary>
		/// <param name="_name"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Profile&gt;"/> class.</returns>
		public TList<Profile> GetByName(System.String _name, int start, int pageLength)
		{
			int count = -1;
			return GetByName(null, _name, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Name index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_name"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Profile&gt;"/> class.</returns>
		public TList<Profile> GetByName(TransactionManager transactionManager, System.String _name)
		{
			int count = -1;
			return GetByName(transactionManager, _name, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Name index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_name"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Profile&gt;"/> class.</returns>
		public TList<Profile> GetByName(TransactionManager transactionManager, System.String _name, int start, int pageLength)
		{
			int count = -1;
			return GetByName(transactionManager, _name, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Name index.
		/// </summary>
		/// <param name="_name"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Profile&gt;"/> class.</returns>
		public TList<Profile> GetByName(System.String _name, int start, int pageLength, out int count)
		{
			return GetByName(null, _name, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Name index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_name"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Profile&gt;"/> class.</returns>
		public abstract TList<Profile> GetByName(TransactionManager transactionManager, System.String _name, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_SC_Profile index.
		/// </summary>
		/// <param name="_profileID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Profile"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Profile GetByProfileID(System.Int32 _profileID)
		{
			int count = -1;
			return GetByProfileID(null,_profileID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SC_Profile index.
		/// </summary>
		/// <param name="_profileID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Profile"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Profile GetByProfileID(System.Int32 _profileID, int start, int pageLength)
		{
			int count = -1;
			return GetByProfileID(null, _profileID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SC_Profile index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_profileID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Profile"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Profile GetByProfileID(TransactionManager transactionManager, System.Int32 _profileID)
		{
			int count = -1;
			return GetByProfileID(transactionManager, _profileID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SC_Profile index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_profileID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Profile"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Profile GetByProfileID(TransactionManager transactionManager, System.Int32 _profileID, int start, int pageLength)
		{
			int count = -1;
			return GetByProfileID(transactionManager, _profileID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SC_Profile index.
		/// </summary>
		/// <param name="_profileID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Profile"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Profile GetByProfileID(System.Int32 _profileID, int start, int pageLength, out int count)
		{
			return GetByProfileID(null, _profileID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SC_Profile index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_profileID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Profile"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.Profile GetByProfileID(TransactionManager transactionManager, System.Int32 _profileID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;Profile&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;Profile&gt;"/></returns>
		public static TList<Profile> Fill(IDataReader reader, TList<Profile> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.Profile c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("Profile")
					.Append("|").Append((System.Int32)reader[((int)ProfileColumn.ProfileID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<Profile>(
					key.ToString(), // EntityTrackingKey
					"Profile",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.Profile();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.ProfileID = (System.Int32)reader[((int)ProfileColumn.ProfileID - 1)];
					c.DefaultExternalAccountNo = (reader.IsDBNull(((int)ProfileColumn.DefaultExternalAccountNo - 1)))?null:(System.String)reader[((int)ProfileColumn.DefaultExternalAccountNo - 1)];
					c.Name = (System.String)reader[((int)ProfileColumn.Name - 1)];
					c.UseWholesalePricing = (reader.IsDBNull(((int)ProfileColumn.UseWholesalePricing - 1)))?null:(System.Boolean?)reader[((int)ProfileColumn.UseWholesalePricing - 1)];
					c.EmailList = (reader.IsDBNull(((int)ProfileColumn.EmailList - 1)))?null:(System.String)reader[((int)ProfileColumn.EmailList - 1)];
					c.TaxExempt = (System.Boolean)reader[((int)ProfileColumn.TaxExempt - 1)];
					c.TaxClassID = (reader.IsDBNull(((int)ProfileColumn.TaxClassID - 1)))?null:(System.Int32?)reader[((int)ProfileColumn.TaxClassID - 1)];
					c.ShowPricing = (System.Boolean)reader[((int)ProfileColumn.ShowPricing - 1)];
					c.ShowOnPartnerSignup = (System.Boolean)reader[((int)ProfileColumn.ShowOnPartnerSignup - 1)];
					c.Weighting = (reader.IsDBNull(((int)ProfileColumn.Weighting - 1)))?null:(System.Decimal?)reader[((int)ProfileColumn.Weighting - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.Profile"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.Profile"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.Profile entity)
		{
			if (!reader.Read()) return;
			
			entity.ProfileID = (System.Int32)reader[((int)ProfileColumn.ProfileID - 1)];
			entity.DefaultExternalAccountNo = (reader.IsDBNull(((int)ProfileColumn.DefaultExternalAccountNo - 1)))?null:(System.String)reader[((int)ProfileColumn.DefaultExternalAccountNo - 1)];
			entity.Name = (System.String)reader[((int)ProfileColumn.Name - 1)];
			entity.UseWholesalePricing = (reader.IsDBNull(((int)ProfileColumn.UseWholesalePricing - 1)))?null:(System.Boolean?)reader[((int)ProfileColumn.UseWholesalePricing - 1)];
			entity.EmailList = (reader.IsDBNull(((int)ProfileColumn.EmailList - 1)))?null:(System.String)reader[((int)ProfileColumn.EmailList - 1)];
			entity.TaxExempt = (System.Boolean)reader[((int)ProfileColumn.TaxExempt - 1)];
			entity.TaxClassID = (reader.IsDBNull(((int)ProfileColumn.TaxClassID - 1)))?null:(System.Int32?)reader[((int)ProfileColumn.TaxClassID - 1)];
			entity.ShowPricing = (System.Boolean)reader[((int)ProfileColumn.ShowPricing - 1)];
			entity.ShowOnPartnerSignup = (System.Boolean)reader[((int)ProfileColumn.ShowOnPartnerSignup - 1)];
			entity.Weighting = (reader.IsDBNull(((int)ProfileColumn.Weighting - 1)))?null:(System.Decimal?)reader[((int)ProfileColumn.Weighting - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.Profile"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.Profile"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.Profile entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.ProfileID = (System.Int32)dataRow["ProfileID"];
			entity.DefaultExternalAccountNo = Convert.IsDBNull(dataRow["DefaultExternalAccountNo"]) ? null : (System.String)dataRow["DefaultExternalAccountNo"];
			entity.Name = (System.String)dataRow["Name"];
			entity.UseWholesalePricing = Convert.IsDBNull(dataRow["UseWholesalePricing"]) ? null : (System.Boolean?)dataRow["UseWholesalePricing"];
			entity.EmailList = Convert.IsDBNull(dataRow["EmailList"]) ? null : (System.String)dataRow["EmailList"];
			entity.TaxExempt = (System.Boolean)dataRow["TaxExempt"];
			entity.TaxClassID = Convert.IsDBNull(dataRow["TaxClassID"]) ? null : (System.Int32?)dataRow["TaxClassID"];
			entity.ShowPricing = (System.Boolean)dataRow["ShowPricing"];
			entity.ShowOnPartnerSignup = (System.Boolean)dataRow["ShowOnPartnerSignup"];
			entity.Weighting = Convert.IsDBNull(dataRow["Weighting"]) ? null : (System.Decimal?)dataRow["Weighting"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.Profile"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.Profile Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.Profile entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region TaxClassIDSource	
			if (CanDeepLoad(entity, "TaxClass|TaxClassIDSource", deepLoadType, innerList) 
				&& entity.TaxClassIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.TaxClassID ?? (int)0);
				TaxClass tmpEntity = EntityManager.LocateEntity<TaxClass>(EntityLocator.ConstructKeyFromPkItems(typeof(TaxClass), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.TaxClassIDSource = tmpEntity;
				else
					entity.TaxClassIDSource = DataRepository.TaxClassProvider.GetByTaxClassID(transactionManager, (entity.TaxClassID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'TaxClassIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.TaxClassIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.TaxClassProvider.DeepLoad(transactionManager, entity.TaxClassIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion TaxClassIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByProfileID methods when available
			
			#region PromotionCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Promotion>|PromotionCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PromotionCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.PromotionCollection = DataRepository.PromotionProvider.GetByProfileID(transactionManager, entity.ProfileID);

				if (deep && entity.PromotionCollection.Count > 0)
				{
					deepHandles.Add("PromotionCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Promotion>) DataRepository.PromotionProvider.DeepLoad,
						new object[] { transactionManager, entity.PromotionCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region SKUProfileCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<SKUProfile>|SKUProfileCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SKUProfileCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.SKUProfileCollection = DataRepository.SKUProfileProvider.GetByProfileID(transactionManager, entity.ProfileID);

				if (deep && entity.SKUProfileCollection.Count > 0)
				{
					deepHandles.Add("SKUProfileCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<SKUProfile>) DataRepository.SKUProfileProvider.DeepLoad,
						new object[] { transactionManager, entity.SKUProfileCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region PaymentSettingCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<PaymentSetting>|PaymentSettingCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PaymentSettingCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.PaymentSettingCollection = DataRepository.PaymentSettingProvider.GetByProfileID(transactionManager, entity.ProfileID);

				if (deep && entity.PaymentSettingCollection.Count > 0)
				{
					deepHandles.Add("PaymentSettingCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<PaymentSetting>) DataRepository.PaymentSettingProvider.DeepLoad,
						new object[] { transactionManager, entity.PaymentSettingCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region PortalProfileCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<PortalProfile>|PortalProfileCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PortalProfileCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.PortalProfileCollection = DataRepository.PortalProfileProvider.GetByProfileID(transactionManager, entity.ProfileID);

				if (deep && entity.PortalProfileCollection.Count > 0)
				{
					deepHandles.Add("PortalProfileCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<PortalProfile>) DataRepository.PortalProfileProvider.DeepLoad,
						new object[] { transactionManager, entity.PortalProfileCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region CategoryProfileCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<CategoryProfile>|CategoryProfileCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CategoryProfileCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CategoryProfileCollection = DataRepository.CategoryProfileProvider.GetByProfileID(transactionManager, entity.ProfileID);

				if (deep && entity.CategoryProfileCollection.Count > 0)
				{
					deepHandles.Add("CategoryProfileCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<CategoryProfile>) DataRepository.CategoryProfileProvider.DeepLoad,
						new object[] { transactionManager, entity.CategoryProfileCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region AccountProfileCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<AccountProfile>|AccountProfileCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AccountProfileCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.AccountProfileCollection = DataRepository.AccountProfileProvider.GetByProfileID(transactionManager, entity.ProfileID);

				if (deep && entity.AccountProfileCollection.Count > 0)
				{
					deepHandles.Add("AccountProfileCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<AccountProfile>) DataRepository.AccountProfileProvider.DeepLoad,
						new object[] { transactionManager, entity.AccountProfileCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region SKUProfileEffectiveCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<SKUProfileEffective>|SKUProfileEffectiveCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SKUProfileEffectiveCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.SKUProfileEffectiveCollection = DataRepository.SKUProfileEffectiveProvider.GetByProfileId(transactionManager, entity.ProfileID);

				if (deep && entity.SKUProfileEffectiveCollection.Count > 0)
				{
					deepHandles.Add("SKUProfileEffectiveCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<SKUProfileEffective>) DataRepository.SKUProfileEffectiveProvider.DeepLoad,
						new object[] { transactionManager, entity.SKUProfileEffectiveCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region ProductProfileCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<ProductProfile>|ProductProfileCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ProductProfileCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ProductProfileCollection = DataRepository.ProductProfileProvider.GetByProfileID(transactionManager, entity.ProfileID);

				if (deep && entity.ProductProfileCollection.Count > 0)
				{
					deepHandles.Add("ProductProfileCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<ProductProfile>) DataRepository.ProductProfileProvider.DeepLoad,
						new object[] { transactionManager, entity.ProductProfileCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region ShippingCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Shipping>|ShippingCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ShippingCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ShippingCollection = DataRepository.ShippingProvider.GetByProfileID(transactionManager, entity.ProfileID);

				if (deep && entity.ShippingCollection.Count > 0)
				{
					deepHandles.Add("ShippingCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Shipping>) DataRepository.ShippingProvider.DeepLoad,
						new object[] { transactionManager, entity.ShippingCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.Profile object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.Profile instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.Profile Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.Profile entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region TaxClassIDSource
			if (CanDeepSave(entity, "TaxClass|TaxClassIDSource", deepSaveType, innerList) 
				&& entity.TaxClassIDSource != null)
			{
				DataRepository.TaxClassProvider.Save(transactionManager, entity.TaxClassIDSource);
				entity.TaxClassID = entity.TaxClassIDSource.TaxClassID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<Promotion>
				if (CanDeepSave(entity.PromotionCollection, "List<Promotion>|PromotionCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Promotion child in entity.PromotionCollection)
					{
						if(child.ProfileIDSource != null)
						{
							child.ProfileID = child.ProfileIDSource.ProfileID;
						}
						else
						{
							child.ProfileID = entity.ProfileID;
						}

					}

					if (entity.PromotionCollection.Count > 0 || entity.PromotionCollection.DeletedItems.Count > 0)
					{
						//DataRepository.PromotionProvider.Save(transactionManager, entity.PromotionCollection);
						
						deepHandles.Add("PromotionCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Promotion >) DataRepository.PromotionProvider.DeepSave,
							new object[] { transactionManager, entity.PromotionCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<SKUProfile>
				if (CanDeepSave(entity.SKUProfileCollection, "List<SKUProfile>|SKUProfileCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(SKUProfile child in entity.SKUProfileCollection)
					{
						if(child.ProfileIDSource != null)
						{
							child.ProfileID = child.ProfileIDSource.ProfileID;
						}
						else
						{
							child.ProfileID = entity.ProfileID;
						}

					}

					if (entity.SKUProfileCollection.Count > 0 || entity.SKUProfileCollection.DeletedItems.Count > 0)
					{
						//DataRepository.SKUProfileProvider.Save(transactionManager, entity.SKUProfileCollection);
						
						deepHandles.Add("SKUProfileCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< SKUProfile >) DataRepository.SKUProfileProvider.DeepSave,
							new object[] { transactionManager, entity.SKUProfileCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<PaymentSetting>
				if (CanDeepSave(entity.PaymentSettingCollection, "List<PaymentSetting>|PaymentSettingCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(PaymentSetting child in entity.PaymentSettingCollection)
					{
						if(child.ProfileIDSource != null)
						{
							child.ProfileID = child.ProfileIDSource.ProfileID;
						}
						else
						{
							child.ProfileID = entity.ProfileID;
						}

					}

					if (entity.PaymentSettingCollection.Count > 0 || entity.PaymentSettingCollection.DeletedItems.Count > 0)
					{
						//DataRepository.PaymentSettingProvider.Save(transactionManager, entity.PaymentSettingCollection);
						
						deepHandles.Add("PaymentSettingCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< PaymentSetting >) DataRepository.PaymentSettingProvider.DeepSave,
							new object[] { transactionManager, entity.PaymentSettingCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<PortalProfile>
				if (CanDeepSave(entity.PortalProfileCollection, "List<PortalProfile>|PortalProfileCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(PortalProfile child in entity.PortalProfileCollection)
					{
						if(child.ProfileIDSource != null)
						{
							child.ProfileID = child.ProfileIDSource.ProfileID;
						}
						else
						{
							child.ProfileID = entity.ProfileID;
						}

					}

					if (entity.PortalProfileCollection.Count > 0 || entity.PortalProfileCollection.DeletedItems.Count > 0)
					{
						//DataRepository.PortalProfileProvider.Save(transactionManager, entity.PortalProfileCollection);
						
						deepHandles.Add("PortalProfileCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< PortalProfile >) DataRepository.PortalProfileProvider.DeepSave,
							new object[] { transactionManager, entity.PortalProfileCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<CategoryProfile>
				if (CanDeepSave(entity.CategoryProfileCollection, "List<CategoryProfile>|CategoryProfileCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(CategoryProfile child in entity.CategoryProfileCollection)
					{
						if(child.ProfileIDSource != null)
						{
							child.ProfileID = child.ProfileIDSource.ProfileID;
						}
						else
						{
							child.ProfileID = entity.ProfileID;
						}

					}

					if (entity.CategoryProfileCollection.Count > 0 || entity.CategoryProfileCollection.DeletedItems.Count > 0)
					{
						//DataRepository.CategoryProfileProvider.Save(transactionManager, entity.CategoryProfileCollection);
						
						deepHandles.Add("CategoryProfileCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< CategoryProfile >) DataRepository.CategoryProfileProvider.DeepSave,
							new object[] { transactionManager, entity.CategoryProfileCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<AccountProfile>
				if (CanDeepSave(entity.AccountProfileCollection, "List<AccountProfile>|AccountProfileCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(AccountProfile child in entity.AccountProfileCollection)
					{
						if(child.ProfileIDSource != null)
						{
							child.ProfileID = child.ProfileIDSource.ProfileID;
						}
						else
						{
							child.ProfileID = entity.ProfileID;
						}

					}

					if (entity.AccountProfileCollection.Count > 0 || entity.AccountProfileCollection.DeletedItems.Count > 0)
					{
						//DataRepository.AccountProfileProvider.Save(transactionManager, entity.AccountProfileCollection);
						
						deepHandles.Add("AccountProfileCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< AccountProfile >) DataRepository.AccountProfileProvider.DeepSave,
							new object[] { transactionManager, entity.AccountProfileCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<SKUProfileEffective>
				if (CanDeepSave(entity.SKUProfileEffectiveCollection, "List<SKUProfileEffective>|SKUProfileEffectiveCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(SKUProfileEffective child in entity.SKUProfileEffectiveCollection)
					{
						if(child.ProfileIdSource != null)
						{
							child.ProfileId = child.ProfileIdSource.ProfileID;
						}
						else
						{
							child.ProfileId = entity.ProfileID;
						}

					}

					if (entity.SKUProfileEffectiveCollection.Count > 0 || entity.SKUProfileEffectiveCollection.DeletedItems.Count > 0)
					{
						//DataRepository.SKUProfileEffectiveProvider.Save(transactionManager, entity.SKUProfileEffectiveCollection);
						
						deepHandles.Add("SKUProfileEffectiveCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< SKUProfileEffective >) DataRepository.SKUProfileEffectiveProvider.DeepSave,
							new object[] { transactionManager, entity.SKUProfileEffectiveCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<ProductProfile>
				if (CanDeepSave(entity.ProductProfileCollection, "List<ProductProfile>|ProductProfileCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(ProductProfile child in entity.ProductProfileCollection)
					{
						if(child.ProfileIDSource != null)
						{
							child.ProfileID = child.ProfileIDSource.ProfileID;
						}
						else
						{
							child.ProfileID = entity.ProfileID;
						}

					}

					if (entity.ProductProfileCollection.Count > 0 || entity.ProductProfileCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ProductProfileProvider.Save(transactionManager, entity.ProductProfileCollection);
						
						deepHandles.Add("ProductProfileCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< ProductProfile >) DataRepository.ProductProfileProvider.DeepSave,
							new object[] { transactionManager, entity.ProductProfileCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Shipping>
				if (CanDeepSave(entity.ShippingCollection, "List<Shipping>|ShippingCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Shipping child in entity.ShippingCollection)
					{
						if(child.ProfileIDSource != null)
						{
							child.ProfileID = child.ProfileIDSource.ProfileID;
						}
						else
						{
							child.ProfileID = entity.ProfileID;
						}

					}

					if (entity.ShippingCollection.Count > 0 || entity.ShippingCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ShippingProvider.Save(transactionManager, entity.ShippingCollection);
						
						deepHandles.Add("ShippingCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Shipping >) DataRepository.ShippingProvider.DeepSave,
							new object[] { transactionManager, entity.ShippingCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region ProfileChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.Profile</c>
	///</summary>
	public enum ProfileChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>TaxClass</c> at TaxClassIDSource
		///</summary>
		[ChildEntityType(typeof(TaxClass))]
		TaxClass,
		///<summary>
		/// Collection of <c>Profile</c> as OneToMany for PromotionCollection
		///</summary>
		[ChildEntityType(typeof(TList<Promotion>))]
		PromotionCollection,
		///<summary>
		/// Collection of <c>Profile</c> as OneToMany for SKUProfileCollection
		///</summary>
		[ChildEntityType(typeof(TList<SKUProfile>))]
		SKUProfileCollection,
		///<summary>
		/// Collection of <c>Profile</c> as OneToMany for PaymentSettingCollection
		///</summary>
		[ChildEntityType(typeof(TList<PaymentSetting>))]
		PaymentSettingCollection,
		///<summary>
		/// Collection of <c>Profile</c> as OneToMany for PortalProfileCollection
		///</summary>
		[ChildEntityType(typeof(TList<PortalProfile>))]
		PortalProfileCollection,
		///<summary>
		/// Collection of <c>Profile</c> as OneToMany for CategoryProfileCollection
		///</summary>
		[ChildEntityType(typeof(TList<CategoryProfile>))]
		CategoryProfileCollection,
		///<summary>
		/// Collection of <c>Profile</c> as OneToMany for AccountProfileCollection
		///</summary>
		[ChildEntityType(typeof(TList<AccountProfile>))]
		AccountProfileCollection,
		///<summary>
		/// Collection of <c>Profile</c> as OneToMany for SKUProfileEffectiveCollection
		///</summary>
		[ChildEntityType(typeof(TList<SKUProfileEffective>))]
		SKUProfileEffectiveCollection,
		///<summary>
		/// Collection of <c>Profile</c> as OneToMany for ProductProfileCollection
		///</summary>
		[ChildEntityType(typeof(TList<ProductProfile>))]
		ProductProfileCollection,
		///<summary>
		/// Collection of <c>Profile</c> as OneToMany for ShippingCollection
		///</summary>
		[ChildEntityType(typeof(TList<Shipping>))]
		ShippingCollection,
	}
	
	#endregion ProfileChildEntityTypes
	
	#region ProfileFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;ProfileColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Profile"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProfileFilterBuilder : SqlFilterBuilder<ProfileColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProfileFilterBuilder class.
		/// </summary>
		public ProfileFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProfileFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProfileFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProfileFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProfileFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProfileFilterBuilder
	
	#region ProfileParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;ProfileColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Profile"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProfileParameterBuilder : ParameterizedSqlFilterBuilder<ProfileColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProfileParameterBuilder class.
		/// </summary>
		public ProfileParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProfileParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProfileParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProfileParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProfileParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProfileParameterBuilder
	
	#region ProfileSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;ProfileColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Profile"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ProfileSortBuilder : SqlSortBuilder<ProfileColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProfileSqlSortBuilder class.
		/// </summary>
		public ProfileSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ProfileSortBuilder
	
} // end namespace
