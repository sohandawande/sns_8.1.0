﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="CustomerPricingProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class CustomerPricingProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.CustomerPricing, ZNode.Libraries.DataAccess.Entities.CustomerPricingKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.CustomerPricingKey key)
		{
			return Delete(transactionManager, key.CustomerPricingID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_customerPricingID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _customerPricingID)
		{
			return Delete(null, _customerPricingID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_customerPricingID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _customerPricingID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.CustomerPricing Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.CustomerPricingKey key, int start, int pageLength)
		{
			return GetByCustomerPricingID(transactionManager, key.CustomerPricingID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZnodeCustomerPricing_ExternalAccountNo index.
		/// </summary>
		/// <param name="_externalAccountNo"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;CustomerPricing&gt;"/> class.</returns>
		public TList<CustomerPricing> GetByExternalAccountNo(System.String _externalAccountNo)
		{
			int count = -1;
			return GetByExternalAccountNo(null,_externalAccountNo, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZnodeCustomerPricing_ExternalAccountNo index.
		/// </summary>
		/// <param name="_externalAccountNo"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CustomerPricing&gt;"/> class.</returns>
		public TList<CustomerPricing> GetByExternalAccountNo(System.String _externalAccountNo, int start, int pageLength)
		{
			int count = -1;
			return GetByExternalAccountNo(null, _externalAccountNo, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZnodeCustomerPricing_ExternalAccountNo index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_externalAccountNo"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CustomerPricing&gt;"/> class.</returns>
		public TList<CustomerPricing> GetByExternalAccountNo(TransactionManager transactionManager, System.String _externalAccountNo)
		{
			int count = -1;
			return GetByExternalAccountNo(transactionManager, _externalAccountNo, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZnodeCustomerPricing_ExternalAccountNo index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_externalAccountNo"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CustomerPricing&gt;"/> class.</returns>
		public TList<CustomerPricing> GetByExternalAccountNo(TransactionManager transactionManager, System.String _externalAccountNo, int start, int pageLength)
		{
			int count = -1;
			return GetByExternalAccountNo(transactionManager, _externalAccountNo, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZnodeCustomerPricing_ExternalAccountNo index.
		/// </summary>
		/// <param name="_externalAccountNo"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CustomerPricing&gt;"/> class.</returns>
		public TList<CustomerPricing> GetByExternalAccountNo(System.String _externalAccountNo, int start, int pageLength, out int count)
		{
			return GetByExternalAccountNo(null, _externalAccountNo, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZnodeCustomerPricing_ExternalAccountNo index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_externalAccountNo"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;CustomerPricing&gt;"/> class.</returns>
		public abstract TList<CustomerPricing> GetByExternalAccountNo(TransactionManager transactionManager, System.String _externalAccountNo, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZnodeCustomerPricing_SKUExternalID index.
		/// </summary>
		/// <param name="_sKUExternalID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;CustomerPricing&gt;"/> class.</returns>
		public TList<CustomerPricing> GetBySKUExternalID(System.String _sKUExternalID)
		{
			int count = -1;
			return GetBySKUExternalID(null,_sKUExternalID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZnodeCustomerPricing_SKUExternalID index.
		/// </summary>
		/// <param name="_sKUExternalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CustomerPricing&gt;"/> class.</returns>
		public TList<CustomerPricing> GetBySKUExternalID(System.String _sKUExternalID, int start, int pageLength)
		{
			int count = -1;
			return GetBySKUExternalID(null, _sKUExternalID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZnodeCustomerPricing_SKUExternalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sKUExternalID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CustomerPricing&gt;"/> class.</returns>
		public TList<CustomerPricing> GetBySKUExternalID(TransactionManager transactionManager, System.String _sKUExternalID)
		{
			int count = -1;
			return GetBySKUExternalID(transactionManager, _sKUExternalID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZnodeCustomerPricing_SKUExternalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sKUExternalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CustomerPricing&gt;"/> class.</returns>
		public TList<CustomerPricing> GetBySKUExternalID(TransactionManager transactionManager, System.String _sKUExternalID, int start, int pageLength)
		{
			int count = -1;
			return GetBySKUExternalID(transactionManager, _sKUExternalID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZnodeCustomerPricing_SKUExternalID index.
		/// </summary>
		/// <param name="_sKUExternalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;CustomerPricing&gt;"/> class.</returns>
		public TList<CustomerPricing> GetBySKUExternalID(System.String _sKUExternalID, int start, int pageLength, out int count)
		{
			return GetBySKUExternalID(null, _sKUExternalID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZnodeCustomerPricing_SKUExternalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sKUExternalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;CustomerPricing&gt;"/> class.</returns>
		public abstract TList<CustomerPricing> GetBySKUExternalID(TransactionManager transactionManager, System.String _sKUExternalID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodeCustomerPricing index.
		/// </summary>
		/// <param name="_customerPricingID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.CustomerPricing"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.CustomerPricing GetByCustomerPricingID(System.Int32 _customerPricingID)
		{
			int count = -1;
			return GetByCustomerPricingID(null,_customerPricingID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeCustomerPricing index.
		/// </summary>
		/// <param name="_customerPricingID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.CustomerPricing"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.CustomerPricing GetByCustomerPricingID(System.Int32 _customerPricingID, int start, int pageLength)
		{
			int count = -1;
			return GetByCustomerPricingID(null, _customerPricingID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeCustomerPricing index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_customerPricingID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.CustomerPricing"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.CustomerPricing GetByCustomerPricingID(TransactionManager transactionManager, System.Int32 _customerPricingID)
		{
			int count = -1;
			return GetByCustomerPricingID(transactionManager, _customerPricingID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeCustomerPricing index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_customerPricingID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.CustomerPricing"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.CustomerPricing GetByCustomerPricingID(TransactionManager transactionManager, System.Int32 _customerPricingID, int start, int pageLength)
		{
			int count = -1;
			return GetByCustomerPricingID(transactionManager, _customerPricingID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeCustomerPricing index.
		/// </summary>
		/// <param name="_customerPricingID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.CustomerPricing"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.CustomerPricing GetByCustomerPricingID(System.Int32 _customerPricingID, int start, int pageLength, out int count)
		{
			return GetByCustomerPricingID(null, _customerPricingID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeCustomerPricing index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_customerPricingID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.CustomerPricing"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.CustomerPricing GetByCustomerPricingID(TransactionManager transactionManager, System.Int32 _customerPricingID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;CustomerPricing&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;CustomerPricing&gt;"/></returns>
		public static TList<CustomerPricing> Fill(IDataReader reader, TList<CustomerPricing> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.CustomerPricing c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("CustomerPricing")
					.Append("|").Append((System.Int32)reader[((int)CustomerPricingColumn.CustomerPricingID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<CustomerPricing>(
					key.ToString(), // EntityTrackingKey
					"CustomerPricing",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.CustomerPricing();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.CustomerPricingID = (System.Int32)reader[((int)CustomerPricingColumn.CustomerPricingID - 1)];
					c.ExternalAccountNo = (reader.IsDBNull(((int)CustomerPricingColumn.ExternalAccountNo - 1)))?null:(System.String)reader[((int)CustomerPricingColumn.ExternalAccountNo - 1)];
					c.NegotiatedPrice = (reader.IsDBNull(((int)CustomerPricingColumn.NegotiatedPrice - 1)))?null:(System.Decimal?)reader[((int)CustomerPricingColumn.NegotiatedPrice - 1)];
					c.SKUExternalID = (reader.IsDBNull(((int)CustomerPricingColumn.SKUExternalID - 1)))?null:(System.String)reader[((int)CustomerPricingColumn.SKUExternalID - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.CustomerPricing"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.CustomerPricing"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.CustomerPricing entity)
		{
			if (!reader.Read()) return;
			
			entity.CustomerPricingID = (System.Int32)reader[((int)CustomerPricingColumn.CustomerPricingID - 1)];
			entity.ExternalAccountNo = (reader.IsDBNull(((int)CustomerPricingColumn.ExternalAccountNo - 1)))?null:(System.String)reader[((int)CustomerPricingColumn.ExternalAccountNo - 1)];
			entity.NegotiatedPrice = (reader.IsDBNull(((int)CustomerPricingColumn.NegotiatedPrice - 1)))?null:(System.Decimal?)reader[((int)CustomerPricingColumn.NegotiatedPrice - 1)];
			entity.SKUExternalID = (reader.IsDBNull(((int)CustomerPricingColumn.SKUExternalID - 1)))?null:(System.String)reader[((int)CustomerPricingColumn.SKUExternalID - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.CustomerPricing"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.CustomerPricing"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.CustomerPricing entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.CustomerPricingID = (System.Int32)dataRow["CustomerPricingID"];
			entity.ExternalAccountNo = Convert.IsDBNull(dataRow["ExternalAccountNo"]) ? null : (System.String)dataRow["ExternalAccountNo"];
			entity.NegotiatedPrice = Convert.IsDBNull(dataRow["NegotiatedPrice"]) ? null : (System.Decimal?)dataRow["NegotiatedPrice"];
			entity.SKUExternalID = Convert.IsDBNull(dataRow["SKUExternalID"]) ? null : (System.String)dataRow["SKUExternalID"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.CustomerPricing"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.CustomerPricing Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.CustomerPricing entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.CustomerPricing object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.CustomerPricing instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.CustomerPricing Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.CustomerPricing entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region CustomerPricingChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.CustomerPricing</c>
	///</summary>
	public enum CustomerPricingChildEntityTypes
	{
	}
	
	#endregion CustomerPricingChildEntityTypes
	
	#region CustomerPricingFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;CustomerPricingColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CustomerPricing"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CustomerPricingFilterBuilder : SqlFilterBuilder<CustomerPricingColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CustomerPricingFilterBuilder class.
		/// </summary>
		public CustomerPricingFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CustomerPricingFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CustomerPricingFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CustomerPricingFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CustomerPricingFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CustomerPricingFilterBuilder
	
	#region CustomerPricingParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;CustomerPricingColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CustomerPricing"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CustomerPricingParameterBuilder : ParameterizedSqlFilterBuilder<CustomerPricingColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CustomerPricingParameterBuilder class.
		/// </summary>
		public CustomerPricingParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CustomerPricingParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CustomerPricingParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CustomerPricingParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CustomerPricingParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CustomerPricingParameterBuilder
	
	#region CustomerPricingSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;CustomerPricingColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CustomerPricing"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class CustomerPricingSortBuilder : SqlSortBuilder<CustomerPricingColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CustomerPricingSqlSortBuilder class.
		/// </summary>
		public CustomerPricingSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion CustomerPricingSortBuilder
	
} // end namespace
