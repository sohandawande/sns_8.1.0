﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="PortalCatalogProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class PortalCatalogProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.PortalCatalog, ZNode.Libraries.DataAccess.Entities.PortalCatalogKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.PortalCatalogKey key)
		{
			return Delete(transactionManager, key.PortalCatalogID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_portalCatalogID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _portalCatalogID)
		{
			return Delete(null, _portalCatalogID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalCatalogID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _portalCatalogID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodePort__CSSID__6BA4D8C6 key.
		///		FK__ZNodePort__CSSID__6BA4D8C6 Description: 
		/// </summary>
		/// <param name="_cSSID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.PortalCatalog objects.</returns>
		public TList<PortalCatalog> GetByCSSID(System.Int32? _cSSID)
		{
			int count = -1;
			return GetByCSSID(_cSSID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodePort__CSSID__6BA4D8C6 key.
		///		FK__ZNodePort__CSSID__6BA4D8C6 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_cSSID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.PortalCatalog objects.</returns>
		/// <remarks></remarks>
		public TList<PortalCatalog> GetByCSSID(TransactionManager transactionManager, System.Int32? _cSSID)
		{
			int count = -1;
			return GetByCSSID(transactionManager, _cSSID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodePort__CSSID__6BA4D8C6 key.
		///		FK__ZNodePort__CSSID__6BA4D8C6 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_cSSID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.PortalCatalog objects.</returns>
		public TList<PortalCatalog> GetByCSSID(TransactionManager transactionManager, System.Int32? _cSSID, int start, int pageLength)
		{
			int count = -1;
			return GetByCSSID(transactionManager, _cSSID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodePort__CSSID__6BA4D8C6 key.
		///		fKZNodePortCSSID6BA4D8C6 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_cSSID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.PortalCatalog objects.</returns>
		public TList<PortalCatalog> GetByCSSID(System.Int32? _cSSID, int start, int pageLength)
		{
			int count =  -1;
			return GetByCSSID(null, _cSSID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodePort__CSSID__6BA4D8C6 key.
		///		fKZNodePortCSSID6BA4D8C6 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_cSSID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.PortalCatalog objects.</returns>
		public TList<PortalCatalog> GetByCSSID(System.Int32? _cSSID, int start, int pageLength,out int count)
		{
			return GetByCSSID(null, _cSSID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodePort__CSSID__6BA4D8C6 key.
		///		FK__ZNodePort__CSSID__6BA4D8C6 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_cSSID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.PortalCatalog objects.</returns>
		public abstract TList<PortalCatalog> GetByCSSID(TransactionManager transactionManager, System.Int32? _cSSID, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodePort__Theme__6C98FCFF key.
		///		FK__ZNodePort__Theme__6C98FCFF Description: 
		/// </summary>
		/// <param name="_themeID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.PortalCatalog objects.</returns>
		public TList<PortalCatalog> GetByThemeID(System.Int32? _themeID)
		{
			int count = -1;
			return GetByThemeID(_themeID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodePort__Theme__6C98FCFF key.
		///		FK__ZNodePort__Theme__6C98FCFF Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_themeID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.PortalCatalog objects.</returns>
		/// <remarks></remarks>
		public TList<PortalCatalog> GetByThemeID(TransactionManager transactionManager, System.Int32? _themeID)
		{
			int count = -1;
			return GetByThemeID(transactionManager, _themeID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodePort__Theme__6C98FCFF key.
		///		FK__ZNodePort__Theme__6C98FCFF Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_themeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.PortalCatalog objects.</returns>
		public TList<PortalCatalog> GetByThemeID(TransactionManager transactionManager, System.Int32? _themeID, int start, int pageLength)
		{
			int count = -1;
			return GetByThemeID(transactionManager, _themeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodePort__Theme__6C98FCFF key.
		///		fKZNodePortTheme6C98FCFF Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_themeID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.PortalCatalog objects.</returns>
		public TList<PortalCatalog> GetByThemeID(System.Int32? _themeID, int start, int pageLength)
		{
			int count =  -1;
			return GetByThemeID(null, _themeID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodePort__Theme__6C98FCFF key.
		///		fKZNodePortTheme6C98FCFF Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_themeID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.PortalCatalog objects.</returns>
		public TList<PortalCatalog> GetByThemeID(System.Int32? _themeID, int start, int pageLength,out int count)
		{
			return GetByThemeID(null, _themeID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK__ZNodePort__Theme__6C98FCFF key.
		///		FK__ZNodePort__Theme__6C98FCFF Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_themeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.PortalCatalog objects.</returns>
		public abstract TList<PortalCatalog> GetByThemeID(TransactionManager transactionManager, System.Int32? _themeID, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePortalCatalog_ZNodeCatalog1 key.
		///		FK_ZNodePortalCatalog_ZNodeCatalog1 Description: 
		/// </summary>
		/// <param name="_catalogID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.PortalCatalog objects.</returns>
		public TList<PortalCatalog> GetByCatalogID(System.Int32 _catalogID)
		{
			int count = -1;
			return GetByCatalogID(_catalogID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePortalCatalog_ZNodeCatalog1 key.
		///		FK_ZNodePortalCatalog_ZNodeCatalog1 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_catalogID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.PortalCatalog objects.</returns>
		/// <remarks></remarks>
		public TList<PortalCatalog> GetByCatalogID(TransactionManager transactionManager, System.Int32 _catalogID)
		{
			int count = -1;
			return GetByCatalogID(transactionManager, _catalogID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePortalCatalog_ZNodeCatalog1 key.
		///		FK_ZNodePortalCatalog_ZNodeCatalog1 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_catalogID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.PortalCatalog objects.</returns>
		public TList<PortalCatalog> GetByCatalogID(TransactionManager transactionManager, System.Int32 _catalogID, int start, int pageLength)
		{
			int count = -1;
			return GetByCatalogID(transactionManager, _catalogID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePortalCatalog_ZNodeCatalog1 key.
		///		fKZNodePortalCatalogZNodeCatalog1 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_catalogID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.PortalCatalog objects.</returns>
		public TList<PortalCatalog> GetByCatalogID(System.Int32 _catalogID, int start, int pageLength)
		{
			int count =  -1;
			return GetByCatalogID(null, _catalogID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePortalCatalog_ZNodeCatalog1 key.
		///		fKZNodePortalCatalogZNodeCatalog1 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_catalogID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.PortalCatalog objects.</returns>
		public TList<PortalCatalog> GetByCatalogID(System.Int32 _catalogID, int start, int pageLength,out int count)
		{
			return GetByCatalogID(null, _catalogID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePortalCatalog_ZNodeCatalog1 key.
		///		FK_ZNodePortalCatalog_ZNodeCatalog1 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_catalogID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.PortalCatalog objects.</returns>
		public abstract TList<PortalCatalog> GetByCatalogID(TransactionManager transactionManager, System.Int32 _catalogID, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePortalCatalog_ZNodeLocale key.
		///		FK_ZNodePortalCatalog_ZNodeLocale Description: 
		/// </summary>
		/// <param name="_localeID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.PortalCatalog objects.</returns>
		public TList<PortalCatalog> GetByLocaleID(System.Int32 _localeID)
		{
			int count = -1;
			return GetByLocaleID(_localeID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePortalCatalog_ZNodeLocale key.
		///		FK_ZNodePortalCatalog_ZNodeLocale Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_localeID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.PortalCatalog objects.</returns>
		/// <remarks></remarks>
		public TList<PortalCatalog> GetByLocaleID(TransactionManager transactionManager, System.Int32 _localeID)
		{
			int count = -1;
			return GetByLocaleID(transactionManager, _localeID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePortalCatalog_ZNodeLocale key.
		///		FK_ZNodePortalCatalog_ZNodeLocale Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_localeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.PortalCatalog objects.</returns>
		public TList<PortalCatalog> GetByLocaleID(TransactionManager transactionManager, System.Int32 _localeID, int start, int pageLength)
		{
			int count = -1;
			return GetByLocaleID(transactionManager, _localeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePortalCatalog_ZNodeLocale key.
		///		fKZNodePortalCatalogZNodeLocale Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_localeID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.PortalCatalog objects.</returns>
		public TList<PortalCatalog> GetByLocaleID(System.Int32 _localeID, int start, int pageLength)
		{
			int count =  -1;
			return GetByLocaleID(null, _localeID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePortalCatalog_ZNodeLocale key.
		///		fKZNodePortalCatalogZNodeLocale Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_localeID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.PortalCatalog objects.</returns>
		public TList<PortalCatalog> GetByLocaleID(System.Int32 _localeID, int start, int pageLength,out int count)
		{
			return GetByLocaleID(null, _localeID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePortalCatalog_ZNodeLocale key.
		///		FK_ZNodePortalCatalog_ZNodeLocale Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_localeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.PortalCatalog objects.</returns>
		public abstract TList<PortalCatalog> GetByLocaleID(TransactionManager transactionManager, System.Int32 _localeID, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePortalCatalog_ZNodePortal key.
		///		FK_ZNodePortalCatalog_ZNodePortal Description: 
		/// </summary>
		/// <param name="_portalID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.PortalCatalog objects.</returns>
		public TList<PortalCatalog> GetByPortalID(System.Int32 _portalID)
		{
			int count = -1;
			return GetByPortalID(_portalID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePortalCatalog_ZNodePortal key.
		///		FK_ZNodePortalCatalog_ZNodePortal Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.PortalCatalog objects.</returns>
		/// <remarks></remarks>
		public TList<PortalCatalog> GetByPortalID(TransactionManager transactionManager, System.Int32 _portalID)
		{
			int count = -1;
			return GetByPortalID(transactionManager, _portalID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePortalCatalog_ZNodePortal key.
		///		FK_ZNodePortalCatalog_ZNodePortal Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.PortalCatalog objects.</returns>
		public TList<PortalCatalog> GetByPortalID(TransactionManager transactionManager, System.Int32 _portalID, int start, int pageLength)
		{
			int count = -1;
			return GetByPortalID(transactionManager, _portalID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePortalCatalog_ZNodePortal key.
		///		fKZNodePortalCatalogZNodePortal Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_portalID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.PortalCatalog objects.</returns>
		public TList<PortalCatalog> GetByPortalID(System.Int32 _portalID, int start, int pageLength)
		{
			int count =  -1;
			return GetByPortalID(null, _portalID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePortalCatalog_ZNodePortal key.
		///		fKZNodePortalCatalogZNodePortal Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_portalID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.PortalCatalog objects.</returns>
		public TList<PortalCatalog> GetByPortalID(System.Int32 _portalID, int start, int pageLength,out int count)
		{
			return GetByPortalID(null, _portalID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePortalCatalog_ZNodePortal key.
		///		FK_ZNodePortalCatalog_ZNodePortal Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.PortalCatalog objects.</returns>
		public abstract TList<PortalCatalog> GetByPortalID(TransactionManager transactionManager, System.Int32 _portalID, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.PortalCatalog Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.PortalCatalogKey key, int start, int pageLength)
		{
			return GetByPortalCatalogID(transactionManager, key.PortalCatalogID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodePortalCatalog_PortalID_CatalogID index.
		/// </summary>
		/// <param name="_portalID"></param>
		/// <param name="_catalogID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.PortalCatalog"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.PortalCatalog GetByPortalIDCatalogID(System.Int32 _portalID, System.Int32 _catalogID)
		{
			int count = -1;
			return GetByPortalIDCatalogID(null,_portalID, _catalogID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePortalCatalog_PortalID_CatalogID index.
		/// </summary>
		/// <param name="_portalID"></param>
		/// <param name="_catalogID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.PortalCatalog"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.PortalCatalog GetByPortalIDCatalogID(System.Int32 _portalID, System.Int32 _catalogID, int start, int pageLength)
		{
			int count = -1;
			return GetByPortalIDCatalogID(null, _portalID, _catalogID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePortalCatalog_PortalID_CatalogID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <param name="_catalogID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.PortalCatalog"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.PortalCatalog GetByPortalIDCatalogID(TransactionManager transactionManager, System.Int32 _portalID, System.Int32 _catalogID)
		{
			int count = -1;
			return GetByPortalIDCatalogID(transactionManager, _portalID, _catalogID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePortalCatalog_PortalID_CatalogID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <param name="_catalogID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.PortalCatalog"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.PortalCatalog GetByPortalIDCatalogID(TransactionManager transactionManager, System.Int32 _portalID, System.Int32 _catalogID, int start, int pageLength)
		{
			int count = -1;
			return GetByPortalIDCatalogID(transactionManager, _portalID, _catalogID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePortalCatalog_PortalID_CatalogID index.
		/// </summary>
		/// <param name="_portalID"></param>
		/// <param name="_catalogID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.PortalCatalog"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.PortalCatalog GetByPortalIDCatalogID(System.Int32 _portalID, System.Int32 _catalogID, int start, int pageLength, out int count)
		{
			return GetByPortalIDCatalogID(null, _portalID, _catalogID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePortalCatalog_PortalID_CatalogID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <param name="_catalogID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.PortalCatalog"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.PortalCatalog GetByPortalIDCatalogID(TransactionManager transactionManager, System.Int32 _portalID, System.Int32 _catalogID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodePortalCatalog index.
		/// </summary>
		/// <param name="_portalCatalogID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.PortalCatalog"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.PortalCatalog GetByPortalCatalogID(System.Int32 _portalCatalogID)
		{
			int count = -1;
			return GetByPortalCatalogID(null,_portalCatalogID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodePortalCatalog index.
		/// </summary>
		/// <param name="_portalCatalogID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.PortalCatalog"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.PortalCatalog GetByPortalCatalogID(System.Int32 _portalCatalogID, int start, int pageLength)
		{
			int count = -1;
			return GetByPortalCatalogID(null, _portalCatalogID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodePortalCatalog index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalCatalogID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.PortalCatalog"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.PortalCatalog GetByPortalCatalogID(TransactionManager transactionManager, System.Int32 _portalCatalogID)
		{
			int count = -1;
			return GetByPortalCatalogID(transactionManager, _portalCatalogID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodePortalCatalog index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalCatalogID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.PortalCatalog"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.PortalCatalog GetByPortalCatalogID(TransactionManager transactionManager, System.Int32 _portalCatalogID, int start, int pageLength)
		{
			int count = -1;
			return GetByPortalCatalogID(transactionManager, _portalCatalogID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodePortalCatalog index.
		/// </summary>
		/// <param name="_portalCatalogID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.PortalCatalog"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.PortalCatalog GetByPortalCatalogID(System.Int32 _portalCatalogID, int start, int pageLength, out int count)
		{
			return GetByPortalCatalogID(null, _portalCatalogID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodePortalCatalog index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalCatalogID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.PortalCatalog"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.PortalCatalog GetByPortalCatalogID(TransactionManager transactionManager, System.Int32 _portalCatalogID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;PortalCatalog&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;PortalCatalog&gt;"/></returns>
		public static TList<PortalCatalog> Fill(IDataReader reader, TList<PortalCatalog> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.PortalCatalog c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("PortalCatalog")
					.Append("|").Append((System.Int32)reader[((int)PortalCatalogColumn.PortalCatalogID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<PortalCatalog>(
					key.ToString(), // EntityTrackingKey
					"PortalCatalog",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.PortalCatalog();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.PortalCatalogID = (System.Int32)reader[((int)PortalCatalogColumn.PortalCatalogID - 1)];
					c.PortalID = (System.Int32)reader[((int)PortalCatalogColumn.PortalID - 1)];
					c.CatalogID = (System.Int32)reader[((int)PortalCatalogColumn.CatalogID - 1)];
					c.LocaleID = (System.Int32)reader[((int)PortalCatalogColumn.LocaleID - 1)];
					c.ThemeID = (reader.IsDBNull(((int)PortalCatalogColumn.ThemeID - 1)))?null:(System.Int32?)reader[((int)PortalCatalogColumn.ThemeID - 1)];
					c.CSSID = (reader.IsDBNull(((int)PortalCatalogColumn.CSSID - 1)))?null:(System.Int32?)reader[((int)PortalCatalogColumn.CSSID - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.PortalCatalog"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.PortalCatalog"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.PortalCatalog entity)
		{
			if (!reader.Read()) return;
			
			entity.PortalCatalogID = (System.Int32)reader[((int)PortalCatalogColumn.PortalCatalogID - 1)];
			entity.PortalID = (System.Int32)reader[((int)PortalCatalogColumn.PortalID - 1)];
			entity.CatalogID = (System.Int32)reader[((int)PortalCatalogColumn.CatalogID - 1)];
			entity.LocaleID = (System.Int32)reader[((int)PortalCatalogColumn.LocaleID - 1)];
			entity.ThemeID = (reader.IsDBNull(((int)PortalCatalogColumn.ThemeID - 1)))?null:(System.Int32?)reader[((int)PortalCatalogColumn.ThemeID - 1)];
			entity.CSSID = (reader.IsDBNull(((int)PortalCatalogColumn.CSSID - 1)))?null:(System.Int32?)reader[((int)PortalCatalogColumn.CSSID - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.PortalCatalog"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.PortalCatalog"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.PortalCatalog entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.PortalCatalogID = (System.Int32)dataRow["PortalCatalogID"];
			entity.PortalID = (System.Int32)dataRow["PortalID"];
			entity.CatalogID = (System.Int32)dataRow["CatalogID"];
			entity.LocaleID = (System.Int32)dataRow["LocaleID"];
			entity.ThemeID = Convert.IsDBNull(dataRow["ThemeID"]) ? null : (System.Int32?)dataRow["ThemeID"];
			entity.CSSID = Convert.IsDBNull(dataRow["CSSID"]) ? null : (System.Int32?)dataRow["CSSID"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.PortalCatalog"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.PortalCatalog Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.PortalCatalog entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region CSSIDSource	
			if (CanDeepLoad(entity, "CSS|CSSIDSource", deepLoadType, innerList) 
				&& entity.CSSIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.CSSID ?? (int)0);
				CSS tmpEntity = EntityManager.LocateEntity<CSS>(EntityLocator.ConstructKeyFromPkItems(typeof(CSS), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CSSIDSource = tmpEntity;
				else
					entity.CSSIDSource = DataRepository.CSSProvider.GetByCSSID(transactionManager, (entity.CSSID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CSSIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CSSIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CSSProvider.DeepLoad(transactionManager, entity.CSSIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CSSIDSource

			#region ThemeIDSource	
			if (CanDeepLoad(entity, "Theme|ThemeIDSource", deepLoadType, innerList) 
				&& entity.ThemeIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.ThemeID ?? (int)0);
				Theme tmpEntity = EntityManager.LocateEntity<Theme>(EntityLocator.ConstructKeyFromPkItems(typeof(Theme), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ThemeIDSource = tmpEntity;
				else
					entity.ThemeIDSource = DataRepository.ThemeProvider.GetByThemeID(transactionManager, (entity.ThemeID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ThemeIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ThemeIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.ThemeProvider.DeepLoad(transactionManager, entity.ThemeIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ThemeIDSource

			#region CatalogIDSource	
			if (CanDeepLoad(entity, "Catalog|CatalogIDSource", deepLoadType, innerList) 
				&& entity.CatalogIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.CatalogID;
				Catalog tmpEntity = EntityManager.LocateEntity<Catalog>(EntityLocator.ConstructKeyFromPkItems(typeof(Catalog), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CatalogIDSource = tmpEntity;
				else
					entity.CatalogIDSource = DataRepository.CatalogProvider.GetByCatalogID(transactionManager, entity.CatalogID);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CatalogIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CatalogIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CatalogProvider.DeepLoad(transactionManager, entity.CatalogIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CatalogIDSource

			#region LocaleIDSource	
			if (CanDeepLoad(entity, "Locale|LocaleIDSource", deepLoadType, innerList) 
				&& entity.LocaleIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.LocaleID;
				Locale tmpEntity = EntityManager.LocateEntity<Locale>(EntityLocator.ConstructKeyFromPkItems(typeof(Locale), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.LocaleIDSource = tmpEntity;
				else
					entity.LocaleIDSource = DataRepository.LocaleProvider.GetByLocaleID(transactionManager, entity.LocaleID);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'LocaleIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.LocaleIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.LocaleProvider.DeepLoad(transactionManager, entity.LocaleIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion LocaleIDSource

			#region PortalIDSource	
			if (CanDeepLoad(entity, "Portal|PortalIDSource", deepLoadType, innerList) 
				&& entity.PortalIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.PortalID;
				Portal tmpEntity = EntityManager.LocateEntity<Portal>(EntityLocator.ConstructKeyFromPkItems(typeof(Portal), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.PortalIDSource = tmpEntity;
				else
					entity.PortalIDSource = DataRepository.PortalProvider.GetByPortalID(transactionManager, entity.PortalID);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PortalIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.PortalIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.PortalProvider.DeepLoad(transactionManager, entity.PortalIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion PortalIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.PortalCatalog object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.PortalCatalog instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.PortalCatalog Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.PortalCatalog entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region CSSIDSource
			if (CanDeepSave(entity, "CSS|CSSIDSource", deepSaveType, innerList) 
				&& entity.CSSIDSource != null)
			{
				DataRepository.CSSProvider.Save(transactionManager, entity.CSSIDSource);
				entity.CSSID = entity.CSSIDSource.CSSID;
			}
			#endregion 
			
			#region ThemeIDSource
			if (CanDeepSave(entity, "Theme|ThemeIDSource", deepSaveType, innerList) 
				&& entity.ThemeIDSource != null)
			{
				DataRepository.ThemeProvider.Save(transactionManager, entity.ThemeIDSource);
				entity.ThemeID = entity.ThemeIDSource.ThemeID;
			}
			#endregion 
			
			#region CatalogIDSource
			if (CanDeepSave(entity, "Catalog|CatalogIDSource", deepSaveType, innerList) 
				&& entity.CatalogIDSource != null)
			{
				DataRepository.CatalogProvider.Save(transactionManager, entity.CatalogIDSource);
				entity.CatalogID = entity.CatalogIDSource.CatalogID;
			}
			#endregion 
			
			#region LocaleIDSource
			if (CanDeepSave(entity, "Locale|LocaleIDSource", deepSaveType, innerList) 
				&& entity.LocaleIDSource != null)
			{
				DataRepository.LocaleProvider.Save(transactionManager, entity.LocaleIDSource);
				entity.LocaleID = entity.LocaleIDSource.LocaleID;
			}
			#endregion 
			
			#region PortalIDSource
			if (CanDeepSave(entity, "Portal|PortalIDSource", deepSaveType, innerList) 
				&& entity.PortalIDSource != null)
			{
				DataRepository.PortalProvider.Save(transactionManager, entity.PortalIDSource);
				entity.PortalID = entity.PortalIDSource.PortalID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region PortalCatalogChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.PortalCatalog</c>
	///</summary>
	public enum PortalCatalogChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>CSS</c> at CSSIDSource
		///</summary>
		[ChildEntityType(typeof(CSS))]
		CSS,
		
		///<summary>
		/// Composite Property for <c>Theme</c> at ThemeIDSource
		///</summary>
		[ChildEntityType(typeof(Theme))]
		Theme,
		
		///<summary>
		/// Composite Property for <c>Catalog</c> at CatalogIDSource
		///</summary>
		[ChildEntityType(typeof(Catalog))]
		Catalog,
		
		///<summary>
		/// Composite Property for <c>Locale</c> at LocaleIDSource
		///</summary>
		[ChildEntityType(typeof(Locale))]
		Locale,
		
		///<summary>
		/// Composite Property for <c>Portal</c> at PortalIDSource
		///</summary>
		[ChildEntityType(typeof(Portal))]
		Portal,
	}
	
	#endregion PortalCatalogChildEntityTypes
	
	#region PortalCatalogFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;PortalCatalogColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PortalCatalog"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PortalCatalogFilterBuilder : SqlFilterBuilder<PortalCatalogColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PortalCatalogFilterBuilder class.
		/// </summary>
		public PortalCatalogFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the PortalCatalogFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PortalCatalogFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PortalCatalogFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PortalCatalogFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PortalCatalogFilterBuilder
	
	#region PortalCatalogParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;PortalCatalogColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PortalCatalog"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PortalCatalogParameterBuilder : ParameterizedSqlFilterBuilder<PortalCatalogColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PortalCatalogParameterBuilder class.
		/// </summary>
		public PortalCatalogParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the PortalCatalogParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PortalCatalogParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PortalCatalogParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PortalCatalogParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PortalCatalogParameterBuilder
	
	#region PortalCatalogSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;PortalCatalogColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PortalCatalog"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class PortalCatalogSortBuilder : SqlSortBuilder<PortalCatalogColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PortalCatalogSqlSortBuilder class.
		/// </summary>
		public PortalCatalogSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion PortalCatalogSortBuilder
	
} // end namespace
