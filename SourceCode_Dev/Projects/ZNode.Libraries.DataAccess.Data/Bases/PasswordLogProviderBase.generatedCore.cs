﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="PasswordLogProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class PasswordLogProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.PasswordLog, ZNode.Libraries.DataAccess.Entities.PasswordLogKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.PasswordLogKey key)
		{
			return Delete(transactionManager, key.PasswordLogID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_passwordLogID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _passwordLogID)
		{
			return Delete(null, _passwordLogID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_passwordLogID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _passwordLogID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.PasswordLog Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.PasswordLogKey key, int start, int pageLength)
		{
			return GetByPasswordLogID(transactionManager, key.PasswordLogID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodePasswordLog index.
		/// </summary>
		/// <param name="_userID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;PasswordLog&gt;"/> class.</returns>
		public TList<PasswordLog> GetByUserID(System.Guid _userID)
		{
			int count = -1;
			return GetByUserID(null,_userID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePasswordLog index.
		/// </summary>
		/// <param name="_userID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;PasswordLog&gt;"/> class.</returns>
		public TList<PasswordLog> GetByUserID(System.Guid _userID, int start, int pageLength)
		{
			int count = -1;
			return GetByUserID(null, _userID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePasswordLog index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;PasswordLog&gt;"/> class.</returns>
		public TList<PasswordLog> GetByUserID(TransactionManager transactionManager, System.Guid _userID)
		{
			int count = -1;
			return GetByUserID(transactionManager, _userID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePasswordLog index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;PasswordLog&gt;"/> class.</returns>
		public TList<PasswordLog> GetByUserID(TransactionManager transactionManager, System.Guid _userID, int start, int pageLength)
		{
			int count = -1;
			return GetByUserID(transactionManager, _userID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePasswordLog index.
		/// </summary>
		/// <param name="_userID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;PasswordLog&gt;"/> class.</returns>
		public TList<PasswordLog> GetByUserID(System.Guid _userID, int start, int pageLength, out int count)
		{
			return GetByUserID(null, _userID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodePasswordLog index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;PasswordLog&gt;"/> class.</returns>
		public abstract TList<PasswordLog> GetByUserID(TransactionManager transactionManager, System.Guid _userID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodePasswordLog index.
		/// </summary>
		/// <param name="_passwordLogID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.PasswordLog"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.PasswordLog GetByPasswordLogID(System.Int32 _passwordLogID)
		{
			int count = -1;
			return GetByPasswordLogID(null,_passwordLogID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodePasswordLog index.
		/// </summary>
		/// <param name="_passwordLogID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.PasswordLog"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.PasswordLog GetByPasswordLogID(System.Int32 _passwordLogID, int start, int pageLength)
		{
			int count = -1;
			return GetByPasswordLogID(null, _passwordLogID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodePasswordLog index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_passwordLogID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.PasswordLog"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.PasswordLog GetByPasswordLogID(TransactionManager transactionManager, System.Int32 _passwordLogID)
		{
			int count = -1;
			return GetByPasswordLogID(transactionManager, _passwordLogID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodePasswordLog index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_passwordLogID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.PasswordLog"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.PasswordLog GetByPasswordLogID(TransactionManager transactionManager, System.Int32 _passwordLogID, int start, int pageLength)
		{
			int count = -1;
			return GetByPasswordLogID(transactionManager, _passwordLogID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodePasswordLog index.
		/// </summary>
		/// <param name="_passwordLogID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.PasswordLog"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.PasswordLog GetByPasswordLogID(System.Int32 _passwordLogID, int start, int pageLength, out int count)
		{
			return GetByPasswordLogID(null, _passwordLogID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodePasswordLog index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_passwordLogID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.PasswordLog"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.PasswordLog GetByPasswordLogID(TransactionManager transactionManager, System.Int32 _passwordLogID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;PasswordLog&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;PasswordLog&gt;"/></returns>
		public static TList<PasswordLog> Fill(IDataReader reader, TList<PasswordLog> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.PasswordLog c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("PasswordLog")
					.Append("|").Append((System.Int32)reader[((int)PasswordLogColumn.PasswordLogID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<PasswordLog>(
					key.ToString(), // EntityTrackingKey
					"PasswordLog",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.PasswordLog();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.PasswordLogID = (System.Int32)reader[((int)PasswordLogColumn.PasswordLogID - 1)];
					c.UserID = (System.Guid)reader[((int)PasswordLogColumn.UserID - 1)];
					c.Password = (System.String)reader[((int)PasswordLogColumn.Password - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.PasswordLog"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.PasswordLog"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.PasswordLog entity)
		{
			if (!reader.Read()) return;
			
			entity.PasswordLogID = (System.Int32)reader[((int)PasswordLogColumn.PasswordLogID - 1)];
			entity.UserID = (System.Guid)reader[((int)PasswordLogColumn.UserID - 1)];
			entity.Password = (System.String)reader[((int)PasswordLogColumn.Password - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.PasswordLog"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.PasswordLog"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.PasswordLog entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.PasswordLogID = (System.Int32)dataRow["PasswordLogID"];
			entity.UserID = (System.Guid)dataRow["UserID"];
			entity.Password = (System.String)dataRow["Password"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.PasswordLog"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.PasswordLog Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.PasswordLog entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.PasswordLog object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.PasswordLog instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.PasswordLog Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.PasswordLog entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region PasswordLogChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.PasswordLog</c>
	///</summary>
	public enum PasswordLogChildEntityTypes
	{
	}
	
	#endregion PasswordLogChildEntityTypes
	
	#region PasswordLogFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;PasswordLogColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PasswordLog"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PasswordLogFilterBuilder : SqlFilterBuilder<PasswordLogColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PasswordLogFilterBuilder class.
		/// </summary>
		public PasswordLogFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the PasswordLogFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PasswordLogFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PasswordLogFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PasswordLogFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PasswordLogFilterBuilder
	
	#region PasswordLogParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;PasswordLogColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PasswordLog"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PasswordLogParameterBuilder : ParameterizedSqlFilterBuilder<PasswordLogColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PasswordLogParameterBuilder class.
		/// </summary>
		public PasswordLogParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the PasswordLogParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PasswordLogParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PasswordLogParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PasswordLogParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PasswordLogParameterBuilder
	
	#region PasswordLogSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;PasswordLogColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PasswordLog"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class PasswordLogSortBuilder : SqlSortBuilder<PasswordLogColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PasswordLogSqlSortBuilder class.
		/// </summary>
		public PasswordLogSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion PasswordLogSortBuilder
	
} // end namespace
